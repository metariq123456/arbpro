﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using fwkArbProMobile;
using fwkArbProMobile.dbContext;
using DGS.ArbPro.Types;
using DGS.ArbPro.DAL;
using log4net;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;

namespace DGS.ArbPro.Service.Interface
{
    public class ArbProAuthProvider : CredentialsAuthProvider
    {
        private ILog Logger;

        public ArbProAuthProvider()
            : base()
        {
            Logger = log4net.LogManager.GetLogger("ArbProService");
        }

        public override bool TryAuthenticate(IServiceBase authService, string Username, string Password)
        {
            try
            {
                Logger.InfoFormat("ArbProAuthProvider:TryAuthenticate:Username='{0}'", Username);
                string dbName = ConfigurationManager.AppSettings["MasterDatabase"];
                DAL.MobileUser usrContext = new DAL.MobileUser(dbName);
                Types.MobileUser usr = usrContext.GetForEmail(Username);
                bool valid = usr.PasswordMatch(Password);

                if (valid)
                {
                    usr.UserGuid = Guid.NewGuid();

                    int extensionTime = int.Parse(ConfigurationManager.AppSettings["UserSessionLifetime"]);
                    usr.GuidExpirationDate = DateTime.Now.AddMinutes(extensionTime);
                    usrContext.Save(usr);
                }

                Logger.DebugFormat("ArbProAuthProvider:TryAuthenticate:Username='{0}' Authentication Status='{1}'", Username,valid?"Authenticated":"Failed");
                return valid;
                //var Manager = new ArbProManager(false, 0, "ArbProService");
                //fwkArbProMobile.dbContext.EntityUserAccount User;
                //User = Manager.UserAccountInstance(Username, Password);
                //if (User.ClientOrganisation != 0
                //    && User.DatabaseString.Length > 0
                //    && User.DisplayName.Length > 0
                //    && User.EmailAddress.Length > 0
                //    && User.UserId != 0)
                //    return true;
                //else
                //    return false;
            }
            catch (Exception ex) 
            {
                Logger.ErrorFormat("ArbProAuthProvider:Error='{0}'", ex.ToString());
                throw ex;
            }
        }

        public override void OnAuthenticated(IServiceBase AuthService, IAuthSession Session, IOAuthTokens Tokens, Dictionary<string, string> AuthInfo)
        {
            try
            {
                base.OnAuthenticated(AuthService, Session, Tokens, AuthInfo);

                //var Manager = new ArbProManager(false, 0, "ArbProService");
                //fwkArbProMobile.dbContext.EntityUserAccount User;

                string dbName = ConfigurationManager.AppSettings["MasterDatabase"];
                DAL.MobileUser usrContext = new DAL.MobileUser(dbName);
                Types.MobileUser User = usrContext.GetForEmail(Session.UserAuthName);

               // User = Manager.UserAccountInstance(Session.UserAuthName);
                Session.UserAuthId = User.Id.ToString();
                Session.DisplayName = User.EmailAddress;
                Session.Email = User.ClientDatabaseName;
                Session.Sequence = User.UserGuid.ToString();

                Logger.InfoFormat("ArbProAuthProvider:UserId {0} logged in as {1}", Session.UserAuthId, Session.DisplayName);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("OnAuthenticated:Error='{0}'", ex.ToString());
                throw ex;
            }
        }

        public override bool IsAuthorized(IAuthSession session, IOAuthTokens tokens, Auth request = null)
        {
            string dtbName = ConfigurationManager.AppSettings["MasterDatabase"];

            int extensionTime = int.Parse(ConfigurationManager.AppSettings["UserSessionLifetime"]);

            string userEmail = session.DisplayName;

            if (!string.IsNullOrWhiteSpace(userEmail))
            {
                DAL.MobileUser usrContext = new DAL.MobileUser(dtbName);
                Types.MobileUser usr = usrContext.GetForEmail(userEmail);
                if (usr.UserGuid == Guid.Empty || usr.GuidExpirationDate < DateTime.Now)
                {
                    usr.UserGuid = Guid.NewGuid();
                }

                usr.GuidExpirationDate = DateTime.Now.AddMinutes(extensionTime);
                usrContext.Save(usr);
            }

            return base.IsAuthorized(session, tokens, request);
        }
    }
}