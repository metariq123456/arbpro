﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class QuoteUpdate : ArbProServiceBase
    {
        public object any(Model.Operations.QuoteUpdate request)
        {
            MobileManager m;

            String ClientId = "";
            int userId = -1;
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
                int.TryParse(ServiceExtensions.GetSession(this).UserAuthId, out userId);
            }
            catch { }

            try
            {
                BaseLogger.DebugFormat("{0}QuoteUpdate begun for clientId {1} quote {2}",LogPrefix, ClientId, request.Quote.QuoteId);
                m = new MobileManager(false, "Service", ClientId);

                if (request.Quote.ToolsV3 != null)
                {
                    m.QuoteUpdateV3(request.Quote, request.UserCR);
                }
                else
                {
                    m.QuoteUpdate(request.Quote, request.UserCR);
                }

                BaseLogger.DebugFormat("{0}QuoteUpdate completed for clientId {1} quote {2}", LogPrefix, ClientId, request.Quote.QuoteId);
                return new Model.Operations.QuoteUpdate_Response();
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}QuoteUpdate:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}