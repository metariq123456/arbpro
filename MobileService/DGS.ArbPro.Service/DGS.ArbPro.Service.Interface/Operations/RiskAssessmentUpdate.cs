﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class RiskAssessmentUpdate : ArbProServiceBase
    {
        public object any(Model.Operations.RiskAssessmentUpdate request)
        {
            MobileManager m;

            String ClientId = "";
            int userId = -1;
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
                int.TryParse(ServiceExtensions.GetSession(this).UserAuthId, out userId);
            }
            catch { }

            try
            {
                BaseLogger.InfoFormat("{0}RiskAssessmentUpdate: Upload received", LogPrefix);
                m = new MobileManager(false, "Service", ClientId);
               
                m.RiskUpdate(request.Risk,userId, request.UserCR);
                return new Model.Operations.RiskAssessmentUpdate_Response();
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}RiskAssessmentUpdate:Exception='{1}'",LogPrefix , ex.ToString());
                throw (ex);
            }
        }
    }
}