﻿using DGS.ArbPro.Service.Interface;
using ServiceStack.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGS.ArbPro.Interface.Operations
{
    public class AcceptQuote : ArbProServiceBase
    {
        public object any(Model.Operations.QuoteAccept request)
        {
            bool IsAccepted = false;
            MobileManager m;
            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                BaseLogger.InfoFormat("{0}Get AcceptedQuote: Set Quote Accepte", LogPrefix);
                m = new MobileManager(false, "Service", ClientId);
                IsAccepted = m.AcceptQuote(request.QuoteId);
                return new Model.Operations.QuoteAccept_Response() { IsAccept = IsAccepted };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}AcceptedQuote:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        } 
    }
}