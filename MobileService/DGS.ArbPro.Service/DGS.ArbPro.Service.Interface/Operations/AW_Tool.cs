﻿using DGS.ArbPro.Model.Operations;
using DGS.ArbPro.Service.Interface;
using ServiceStack.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGS.ArbPro.Interface.Operations
{
 
    //Get All Tools 
    public class AW_Tool : ArbProServiceBase
    {
        public object any(Model.Operations.AW_Tool request)
        {
            MobileManager m;
            List<Tool> ReturnValue;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                m = new MobileManager(false, "Service", ClientId);
                BaseLogger.InfoFormat("{0}Tool: Download requested", LogPrefix);
                ReturnValue = m.GetAllTool();

                if (ReturnValue == null)
                    ReturnValue = new List<Tool>();

                BaseLogger.DebugFormat("{0}ToolList: Return item count = {1}", LogPrefix, ReturnValue != null ? ReturnValue.Count.ToString() : "Nothing");
                return new Model.Operations.AW_Tool_Response() { Tools = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}ToolList:Exception='{1}'",LogPrefix, ex.ToString());
                throw (ex);
            }
        }

    }
}