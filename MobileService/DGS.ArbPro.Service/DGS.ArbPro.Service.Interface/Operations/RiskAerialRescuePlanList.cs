﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class RiskAerialRescuePlanList : ArbProServiceBase
    {
        public object any(Model.Operations.RiskAerialRescuePlanList request)
        {
            MobileManager m;
            List<Model.Types.RiskAerialRescuePlan> ReturnValue;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                BaseLogger.InfoFormat("{0}RiskAerialRescuePlanList: Download requested", LogPrefix);
                m = new MobileManager(false, "Service", ClientId);
                ReturnValue = m.RiskAerialRescuePlanList();

                if (ReturnValue == null)
                    ReturnValue = new List<Model.Types.RiskAerialRescuePlan>();

                BaseLogger.DebugFormat("{0}RiskAerialRescuePlanList: Return item count = {1}", LogPrefix, ReturnValue != null ? ReturnValue.Count.ToString() : "Nothing");
                return new Model.Operations.RiskAerialRescuePlanList_Response() { Plans = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("RiskAerialRescuePlanList:Exception='{1}'", ex.ToString());
                throw (ex);
            }
        }
    }
}