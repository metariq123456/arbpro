﻿namespace DGS.ArbPro.Interface.Operations
{
    using System;

    using DGS.ArbPro.Service.Interface;

    using ServiceStack.ServiceInterface;

    /// <summary>
    /// The client update.
    /// </summary>
    public class ClientUpdate : ArbProServiceBase
    {
        /// <summary>
        /// The any.
        /// </summary>
        /// <param name="request"> The request. </param>
        /// <returns> The <see cref="object"/>. </returns>
        public object Any(ArbPro.Service.Model.Operations.ClientUpdate request)
        {
            string clientId = string.Empty;
            try
            {
                clientId = this.GetSession().Email;
            }
            catch (Exception ex)
            {
                this.BaseLogger.ErrorFormat("{0}RiskAssessmentV3:Exception='{1}'", this.LogPrefix, ex);
            }

            try
            {
                this.BaseLogger.InfoFormat("{0}RiskAssessmentV3: Upload received", this.LogPrefix);
                var m = new MobileManager(false, "Service", clientId);

                m.ClientUpdate(request.Client);

                return new ArbPro.Service.Model.Operations.ClientUpdateResponse();
            }
            catch (Exception ex)
            {
                this.BaseLogger.ErrorFormat("{0}RiskAssessmentV3:Exception='{1}'", this.LogPrefix, ex);
                throw;
            }
        }
    }
}