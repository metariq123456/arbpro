﻿using ServiceStack.ServiceInterface;
using System;
using System.Collections.Generic;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class RiskCriterionSectionListV2 : ArbProServiceBase
    {
        public object any(Model.Operations.RiskCriterionSectionListV2 request)
        {
            MobileManager m;
            List<Model.Types.AssessmentSection> ReturnValue;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                m = new MobileManager(false, "Service", ClientId);

                BaseLogger.InfoFormat("{0}RiskCriterionSectionListV2: Download requested", LogPrefix);
                ReturnValue = m.RiskSectionListV2();

                if (ReturnValue == null)
                    ReturnValue = new List<Model.Types.AssessmentSection>();

                BaseLogger.DebugFormat("{0}RiskCriterionSectionListV2: Return item count = {1}", LogPrefix, ReturnValue != null ? ReturnValue.Count.ToString() : "Nothing");
                return new Model.Operations.RiskCriterionSectionListV2_Response() { Sections = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}RiskCriterionSectionListV2:Exception='{1}'",LogPrefix , ex.ToString());
                throw (ex);
            }
        }
    }
}