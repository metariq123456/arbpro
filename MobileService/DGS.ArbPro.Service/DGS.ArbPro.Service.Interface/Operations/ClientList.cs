﻿namespace DGS.ArbPro.Service.Interface.Operations
{
    using System;
    using System.Collections.Generic;

    using DGS.ArbPro.Service.Model.Operations;
    using DGS.ArbPro.Service.Model.Types;

    using ServiceStack.ServiceInterface;

    /// <summary>
    /// The client list.
    /// </summary>
    public class ClientList : ArbProServiceBase
    {
        /// <summary>
        /// The any.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        public object Any(Model.Operations.ClientList request)
        {
            string clientId;
            try
            {
                clientId = this.GetSession().Email;
            }
            catch (Exception ex)
            {
                this.BaseLogger.ErrorFormat("{0}ClientList:Exception='{1}'", this.LogPrefix, ex);
                throw;
            }

            try
            {
                MobileManager m = new MobileManager(false, "Service", clientId);
                this.BaseLogger.InfoFormat("{0}ClientList: Download requested", this.LogPrefix);

                List<Client> returnValue = m.ClientsList() ?? new List<Client>();

                this.BaseLogger.DebugFormat("{0}ClientList: Return item count = {1}", this.LogPrefix, returnValue.Count);
                return new ClientListResponse { Clients = returnValue };
            }
            catch (Exception ex)
            {
                this.BaseLogger.ErrorFormat("{0}ClientList:Exception='{1}'", this.LogPrefix, ex);
                throw;
            }
        }
    }
}