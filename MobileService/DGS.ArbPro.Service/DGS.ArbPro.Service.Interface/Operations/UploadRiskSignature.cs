﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using DGS.ArbPro.Model.Types;
using ServiceStack.CacheAccess;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class UploadRiskSignature : ArbProServiceBase
    {
        public object any(ArbPro.Service.Model.Operations.UploadRiskSignature request)
        {
            MobileManager m;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                int count = Request.Files.Count();
                BaseLogger.InfoFormat("{0}UploadRiskSignature: {1} file recieved in upload", LogPrefix, count);
                if (count > 0)
                {
                    m = new MobileManager(false, "Service", ClientId);
                    var File = Request.Files[0];
                    BinaryReader Reader = new BinaryReader(File.InputStream);
                    Byte[] Image = Reader.ReadBytes((int)File.InputStream.Length);

                    int QuoteNo = int.Parse(File.FileName.Split('.')[0]);

                    m.RiskSignatureImageSave(QuoteNo, Image);

                    return new Model.Operations.UploadRiskSignature_Response();
                }
                else
                {
                    BaseLogger.WarnFormat("{0}UploadRiskSignature  - no image file received in request",LogPrefix);
                    throw new Exception("UploadRiskSignature  - no image file received in request");
                }
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}Risk Assessment Signature Upload :Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}