﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RiskAssessmentUpdateV2.cs" company="Microsec Ltd">
//   Copyright 2015 Microsec Ltd
// </copyright>
// <summary>
//   The risk assessment update v 2.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.Service.Interface.Operations
{
    using System;

    using ServiceStack.ServiceInterface;

    /// <summary>
    /// The risk assessment update v 2.
    /// </summary>
    public class RiskAssessmentUpdateV2 : ArbProServiceBase
    {
        #region Public Methods and Operators

        /// <summary>
        /// The any.
        /// </summary>
        /// <param name="request">
        /// The request.
        /// </param>
        /// <returns>
        /// The <see cref="object"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        public object any(Model.Operations.RiskAssessmentUpdate request)
        {
            MobileManager m;

            string clientId = string.Empty;
            int userId = -1;
            try
            {
                clientId = ServiceExtensions.GetSession(this).Email;
                int.TryParse(ServiceExtensions.GetSession(this).UserAuthId, out userId);
            }
            catch
            {
            }

            try
            {
                this.BaseLogger.InfoFormat("{0}RiskAssessmentUpdateV2: Upload received", this.LogPrefix);
                m = new MobileManager(false, "Service", clientId);

                m.RiskUpdateV2(request.Risk, userId, request.UserCR);

                return new Model.Operations.RiskAssessmentUpdateV2_Response();
            }
            catch (Exception ex)
            {
                this.BaseLogger.ErrorFormat("{0}RiskAssessmentUpdateV2:Exception='{1}'", this.LogPrefix, ex.ToString());
                throw ex;
            }
        }

        #endregion
    }
}