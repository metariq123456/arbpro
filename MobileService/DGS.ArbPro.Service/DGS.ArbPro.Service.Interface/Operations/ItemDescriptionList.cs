﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class ItemDescriptionList : ArbProServiceBase
    {
        public object any(Model.Operations.ItemDescriptionList request)
        {
            MobileManager m;
            List<Model.Types.ItemDescription> ReturnValue;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                BaseLogger.InfoFormat("{0}ItemDescriptionList: Download requested", LogPrefix);
                m = new MobileManager(false, "Service", ClientId);
                ReturnValue = m.ItemDescriptionList();

                if (ReturnValue == null)
                    ReturnValue = new List<Model.Types.ItemDescription>();

                BaseLogger.DebugFormat("{0}ItemDescriptionList: Return item count = {1}", LogPrefix, ReturnValue != null ? ReturnValue.Count.ToString() : "Nothing");
                return new Model.Operations.ItemDescriptionList_Response() { Items = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}ItemDescriptionList:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}