﻿using DGS.ArbPro.Service.Interface;
using ServiceStack.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace DGS.ArbPro.Interface.Operations
{
    public class BookedWorkItemWithHeader : ArbProServiceBase
    {
        public object any(ArbPro.Service.Model.Operations.BookingDetail request)
        {
            MobileManager m;
            DGS.ArbPro.Service.Model.Operations.BookingHeader objBooking = null;
            //    new Model.Operations.BookingHeader()
            //{
            //    QuoteId = 1,
            //    SiteAddress = "Test Address",
            //    ClientName = "Test Clinet",
            //    Email = "anjjd",
            //    Mobile = "65656",
            //    Telephone="9898" 
            //};

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                BaseLogger.InfoFormat("{0}Booking Detail: Download requested", LogPrefix);

                m = new MobileManager(false, "Service", ClientId);
                BaseLogger.InfoFormat(Convert.ToString(m));
                objBooking = m.GetBookingDetail(request.QuoteId, request.JobId, request.JobSiteId, request.WorkId);
                return new DGS.ArbPro.Service.Model.Operations.BookingDetail_Response() { BookingDetail = objBooking };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}Booking Detail:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        }

    }
}