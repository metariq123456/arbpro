﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Web;
using DGS.ArbPro.Model.Types;
using ServiceStack.CacheAccess;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class UploadQuoteItemImage : ArbProServiceBase
    {
        public object any(ArbPro.Service.Model.Operations.UploadQuoteItemImage request)
        {
            MobileManager m;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }
            
            try
            {
                int count = Request.Files.Count();
                BaseLogger.InfoFormat("{0}UploadQuoteItemImage: {1} file recieved in upload", LogPrefix, count);
                m = new MobileManager(false, "Service", ClientId);

                if (count > 0)
                {
                    var File = Request.Files[0];
                    if (File.InputStream.Length > 0)
                    {
                        BinaryReader Reader = new BinaryReader(File.InputStream);
                        Byte[] Image = Reader.ReadBytes((int)File.InputStream.Length);
                        m.QuoteImageSave(File.FileName.Split('.')[0], Image);
                    }
                    else
                    {
                        Byte[] ImageBytes = System.IO.File.ReadAllBytes( AppDomain.CurrentDomain.BaseDirectory + @"Images\img_MobileUploadProblem.png");
                        m.QuoteImageSave(File.FileName.Split('.')[0], ImageBytes);
                        BaseLogger.WarnFormat("UploadQuoteItemImage  - there was a problem uploading the image. File.InputStream.length = 0.");
                        throw new Exception("UploadQuoteItemImage  - there was a problem uploading the image. File.InputStream.length = 0.");
                    }
                }
                else
                {
                    BaseLogger.WarnFormat("UploadQuoteItemImage  - no image file received in request");
                    throw new Exception("UploadQuoteItemImage  - no image file received in request");
                }

                return new Model.Operations.UploadQuoteItemImage_Response();
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}UploadQuoteItemImage:Exception='{1}'", LogPrefix, ex.ToString());
                throw ex;
            }
        }
    }
}