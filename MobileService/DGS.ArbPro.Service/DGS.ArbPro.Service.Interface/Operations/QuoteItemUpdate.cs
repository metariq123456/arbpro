﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class QuoteItemUpdate : ArbProServiceBase
    {
        public object any(Model.Operations.QuoteItemUpdate request)
        {
            MobileManager m;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                BaseLogger.DebugFormat("{0}QuoteItemUpdate begun for clientId {1} quote {2}", LogPrefix, ClientId, request.QuoteId);
                m = new MobileManager(false, "Service", ClientId);

                m.QuoteItemUpdate(request.QuoteId,request.Item, request.UserCR);

                BaseLogger.DebugFormat("{0}QuoteItemUpdate completed for clientId {1} quote {2}", LogPrefix, ClientId, request.QuoteId);
                return new Model.Operations.QuoteItemUpdate_Response();
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}QuoteUpdate:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}
