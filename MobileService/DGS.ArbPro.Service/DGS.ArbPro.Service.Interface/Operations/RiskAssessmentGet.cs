﻿using System;
using DGS.ArbPro.Service.Interface;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class RiskAssessmentGet : ArbProServiceBase
    {
        /// <summary>
        /// The any.
        /// </summary>
        /// <param name="request"> The request. </param>
        /// <returns> The <see cref="object"/>. </returns>
        public object Any(Service.Model.Operations.RiskAssessmentGet request)
        {
            MobileManager m;
            Model.Types.Risk retVal;

            string clientId = string.Empty;

            try
            {
                clientId = ServiceExtensions.GetSession(this).Email;
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}RiskAssessmentGet:Exception='{1}'", this.LogPrefix, ex);
            }

            try
            {
                BaseLogger.InfoFormat("{0}RiskAssessmentGet: Request received", this.LogPrefix);
                m = new MobileManager(false, "Service", clientId);

                retVal = m.RiskAssessmentGetForTemplateId(request.TemplateId, request.AppointmentId);

                return new Service.Model.Operations.RiskAssessmentGetResponse() { RiskAssessment = retVal };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}RiskAssessmentGet:Exception='{1}'", this.LogPrefix, ex);
                throw (ex);
            }
        }
    }
}