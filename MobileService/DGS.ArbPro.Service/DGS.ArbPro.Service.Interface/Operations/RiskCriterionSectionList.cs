﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class RiskCriterionSectionList : ArbProServiceBase
    {
        public object any(Model.Operations.RiskCriterionSectionList request)
        {
            MobileManager m;
            List<Model.Types.AssessmentSection> ReturnValue;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                m = new MobileManager(false, "Service", ClientId);

                BaseLogger.InfoFormat("{0}RiskCriterionSectionList: Download requested", LogPrefix);
                ReturnValue = m.RiskSectionList();

                if (ReturnValue == null)
                    ReturnValue = new List<Model.Types.AssessmentSection>();

                BaseLogger.DebugFormat("{0}RiskCriterionSectionList: Return item count = {1}", LogPrefix, ReturnValue != null ? ReturnValue.Count.ToString() : "Nothing");
                return new Model.Operations.RiskCriterionSectionList_Response() { Sections = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}RiskCriterionSectionList:Exception='{1}'",LogPrefix , ex.ToString());
                throw (ex);
            }
        }
    }
}