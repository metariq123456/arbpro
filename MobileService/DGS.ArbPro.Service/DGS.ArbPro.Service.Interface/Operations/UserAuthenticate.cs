﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class UserAuthenticate : ArbProServiceBase
    {
        public object any(ArbPro.Service.Model.Operations.UserAuthenticate request)
        {
            MobileManager m;
            Model.Types.User ReturnValue;

            try
            {
                m = new MobileManager(false, "Service");
                ReturnValue = m.AuthenticateUser(request.Username, request.Password);
                BaseLogger.InfoFormat("UserAuthenticate:Request:Username='{0}' Password='{1}' Returned {2}", request.Username, request.Password, ReturnValue.DisplayName);
                return new Model.Operations.UserAuthenticate_Response() { User = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("UserAuthenticate:Exception='{0}'", ex.ToString());
                throw (ex);
            }
        }
    }
}