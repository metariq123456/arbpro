﻿namespace DGS.ArbPro.Interface.Operations
{
    using System;

    using DGS.ArbPro.Service.Interface;

    using ServiceStack.ServiceInterface;

    /// <summary>
    /// The risk assessment templates.
    /// </summary>
    public class RiskAssessmentUpdateV3 : ArbProServiceBase
    {
        /// <summary>
        /// The any.
        /// </summary>
        /// <param name="request"> The request. </param>
        /// <returns> The <see cref="object"/>. </returns>
        public object Any(ArbPro.Service.Model.Operations.RiskAssessmentUpdateV3 request)
        {
            MobileManager m;

            string clientId = string.Empty;
            int userId = -1;
            try
            {
                clientId = ServiceExtensions.GetSession(this).Email;
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}RiskAssessmentV3:Exception='{1}'", this.LogPrefix, ex);
            }

            try
            {
                BaseLogger.InfoFormat("{0}RiskAssessmentV3: Upload received", this.LogPrefix);
                m = new MobileManager(false, "Service", clientId);

                m.RiskAssessmentUpdateV3(request.Risk);

                return new ArbPro.Service.Model.Operations.RiskAssessmentUpdateV3Response();
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}RiskAssessmentV3:Exception='{1}'", this.LogPrefix, ex);
                throw (ex);
            }
        }
    }
}