﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class AppointmentGetIdListForUser : ArbProServiceBase
    {
        public object any(ArbPro.Service.Model.Operations.AppointmentGetIdListForUser request)
        {
            MobileManager m;
            List<Model.Types.Appointment> ReturnValue;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            } catch {}

            try
            {
                BaseLogger.InfoFormat("{0}AppointmentGetIdListForUser: Download request received", LogPrefix);
                m = new MobileManager(false, "Service", ClientId);
                ReturnValue = m.GetAppointmentsDownloadListForUser(request.UserId);

                if (ReturnValue == null)
                    ReturnValue = new List<Model.Types.Appointment>();

                BaseLogger.DebugFormat("{0}AppointmentGetIdListForUser:Request:UserId={1} Returned {2} appointments",LogPrefix, request.UserId, ReturnValue.Count);

                return new Model.Operations.AppointmentGetIdListForUser_Response() { IdList = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}AppointmentGetIdListForUser:Exception='{1}'",LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}