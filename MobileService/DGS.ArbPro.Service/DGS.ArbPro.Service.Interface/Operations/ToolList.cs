﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class ToolList : ArbProServiceBase
    {
        public object any(Model.Operations.ToolList request)
        {
            MobileManager m;
            List<Model.Types.Tool> ReturnValue;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                m = new MobileManager(false, "Service", ClientId);
                BaseLogger.InfoFormat("{0}ToolList: Download requested", LogPrefix);
                ReturnValue = m.ToolListV3();

                if (ReturnValue == null)
                    ReturnValue = new List<Model.Types.Tool>();

                BaseLogger.DebugFormat("{0}ToolList: Return item count = {1}", LogPrefix, ReturnValue != null ? ReturnValue.Count.ToString() : "Nothing");
                return new Model.Operations.ToolList_Response() { Tools = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}ToolList:Exception='{1}'",LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}