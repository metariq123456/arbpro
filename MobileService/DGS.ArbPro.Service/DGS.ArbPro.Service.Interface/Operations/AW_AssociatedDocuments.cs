﻿using DGS.ArbPro.Model.Operations;
using DGS.ArbPro.Service.Interface;
using ServiceStack.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGS.ArbPro.Interface.Operations
{
    public class AW_AssociatedDocuments : ArbProServiceBase
    {
        public object any(Model.Operations.AW_AssociatedDocuments request)
        {
            MobileManager m;
            List<AssociatedDocument> ReturnValue;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                BaseLogger.InfoFormat("{0}AssociatedDocuments: Get Document request received", LogPrefix);
                m = new MobileManager(false, "Service", ClientId);
                ReturnValue = m.GetAllDocument(request.ClientId, request.JobId, request.JobSiteId);

                //if (ReturnValue == null)
                    ReturnValue = new List<AssociatedDocument>();

                    BaseLogger.DebugFormat("{0}AssociatedDocuments:Request:ClientId={1} Returned {2} Documents", LogPrefix, request.ClientId, ReturnValue.Count);

                return new Model.Operations.AW_AssociatedDocuments_Response() { Documents = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}AssociatedDocuments:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}