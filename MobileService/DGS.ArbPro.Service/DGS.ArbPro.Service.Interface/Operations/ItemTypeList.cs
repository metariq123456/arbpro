﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class ItemTypeList : ArbProServiceBase
    {
        public object any(Model.Operations.ItemTypeList request)
        {
            MobileManager m;
            List<Model.Types.ItemType> ReturnValue;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                BaseLogger.InfoFormat("{0}ItemTypeList: Download requested", LogPrefix);
                m = new MobileManager(false, "Service", ClientId);
                ReturnValue = m.ItemTypeList();

                if (ReturnValue == null)
                    ReturnValue = new List<Model.Types.ItemType>();

                BaseLogger.DebugFormat("{0}ItemTypeList: Return item count = {1}", LogPrefix, ReturnValue != null ? ReturnValue.Count.ToString() : "Nothing");
                return new Model.Operations.ItemTypeList_Response() { Result = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}ItemTypeList:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}