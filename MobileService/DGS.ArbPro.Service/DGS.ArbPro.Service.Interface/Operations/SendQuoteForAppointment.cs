﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Service.Interface;

namespace DGS.ArbPro.Interface.Operations
{
    using System.Web.Services;

    using DGS.ArbPro.Types.Enums;

    using ServiceStack.Messaging.Rcon;
    using ServiceStack.ServiceInterface;

    public class SendQuoteForAppointment : ArbProServiceBase
    {
        public object any(ArbPro.Service.Model.Operations.SendQuoteForAppointment request)
        {
            MobileManager m;

            String database = "";
            int userId = -1;
            try
            {
                database = ServiceExtensions.GetSession(this).Email;
                int.TryParse(ServiceExtensions.GetSession(this).UserAuthId, out userId);
            }
            catch { }

            try
            {
                BaseLogger.InfoFormat("{0}SendQuoteForAppointment: Upload received", LogPrefix);
                
                //  Get the required paths from the config file
                var outputPath = @"C:\"; // NOTE: Depricated
                BaseLogger.InfoFormat("SendQuoteForAppointment: Output path = {0}", outputPath);

                var layoutPath = System.Web.HttpContext.Current.Server.MapPath("/ReportLayouts/");
                BaseLogger.InfoFormat("SendQuoteForAppointment: layoutPath path = {0}", layoutPath);

                //  Log the appointment Id for this call and attempt to generate and then send the email documents
                BaseLogger.InfoFormat("{0}SendQuoteForAppointment: Attempting to send Quote for Appointment Id: {1} and SMS option {2}", LogPrefix,request.Options.AppointmentId,request.Options.SendSms);
                var quoteId = DGS.ArbPro.BL.QuoteReportHandler.SendEmailForAppointmentId(
                    database,
                    outputPath,
                    layoutPath,
                    request.Options.AppointmentId,
                    userId,
                    request.Options.SendSms);

                //  If we get here then everything should have completed and the email will have been dispatched to Elastic
                BaseLogger.InfoFormat("{0}SendQuoteForAppointment: Quote for Appointment Id: {1} queued for disptach. Generated Quote Id {2}", LogPrefix, request.Options.AppointmentId, quoteId);

                //  Check if the generated quote is to be accepted and update as required
                if (request.Options.MarkAccepted)
                {
                    BaseLogger.InfoFormat("{0}SendQuoteForAppointment: Attempting to mark Quote Id: {1} as Accepted", LogPrefix, quoteId);
                    BL.WorkFlowHandlers.QuoteHandler.SetQuoteStatus(database, quoteId, (int)QuoteStatusEnum.Accepted);
                    BaseLogger.InfoFormat("{0}SendQuoteForAppointment: Quote Id: {1} has been marked as Accepted", LogPrefix, quoteId);
                }
                else
                {
                    BaseLogger.InfoFormat("{0}SendQuoteForAppointment: Attempting to mark Quote Id: {1} as Sent", LogPrefix, quoteId);
                    BL.WorkFlowHandlers.QuoteHandler.SetQuoteStatus(database, quoteId, (int)QuoteStatusEnum.Sent);
                    BaseLogger.InfoFormat("{0}SendQuoteForAppointment: Quote Id: {1} has been marked as Sent", LogPrefix, quoteId);
                }

                BaseLogger.InfoFormat("{0}SendQuoteForAppointment: Processing completed", LogPrefix);
                return new ArbPro.Service.Model.Operations.SendQuoteForAppointment_Response();
            }
            catch (Exception ex)
            {

                BaseLogger.ErrorFormat("{0}SendQuoteForAppointment:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}
