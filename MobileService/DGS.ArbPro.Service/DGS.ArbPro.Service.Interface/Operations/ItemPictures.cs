﻿using DGS.ArbPro.Service.Interface;
using ServiceStack.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGS.ArbPro.Interface.Operations
{
    public class ItemPictures : ArbProServiceBase
    {
        public object any(Model.Operations.ItemPicture request)
        {
            MobileManager m;
            bool result = false;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                m = new MobileManager(false, "Service", ClientId);
                BaseLogger.InfoFormat("{0}Image: Upload requested", LogPrefix);
                result = m.SaveImage(request.lstItemaImage);

                BaseLogger.DebugFormat("Image Uploaded!!");
                return new Model.Operations.ItemPicture_Response() { Result = result };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}ImageUploading:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}