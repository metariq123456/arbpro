﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class SendVersion : ArbProServiceBase
    {
        public object any(ArbPro.Service.Model.Operations.SendVersion request)
        {
            //MobileManager m;
            //List<Model.Types.Appointment> ReturnValue;

            try
            {
                BaseLogger.InfoFormat("{0}AppVersion: {1}", LogPrefix, request.AppVersion);

                return new Model.Operations.SendVersion_Response();
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}SendVersion:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}