﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class AppointmentMarkDownloaded : ArbProServiceBase
    {
        public object any(ArbPro.Service.Model.Operations.AppointmentMarkDownloaded request)
        {
            MobileManager m;
            Boolean ReturnValue;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                m = new MobileManager(false, "Service", ClientId);
                ReturnValue = m.MarkAppointmentDownloaded(request.AppointmentId);

                BaseLogger.InfoFormat("{0}AppointmentMarkDownload:Request:AppointmentId={1} Returned:'{2}'",LogPrefix, request.AppointmentId.ToString(), ReturnValue.ToString());

                return new Model.Operations.AppointmentMarkDownloaded_Response() { Result = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}AppointmentMarkDownloaded:Exception='{1}'",LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}