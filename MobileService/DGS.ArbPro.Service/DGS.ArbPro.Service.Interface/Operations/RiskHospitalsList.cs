﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class RiskHospitalsList : ArbProServiceBase
    {
        public object any(Model.Operations.RiskHospitalsList request)
        {
            MobileManager m;
            List<Model.Types.RiskHospital> ReturnValue;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                m = new MobileManager(false, "Service", ClientId);
                BaseLogger.InfoFormat("{0}RiskHospitalsList: Download requested", LogPrefix);
                ReturnValue = m.RiskHospitalList();

                if (ReturnValue == null)
                    ReturnValue = new List<Model.Types.RiskHospital>();

                BaseLogger.DebugFormat("{0}RiskHospitalsList: Return item count = {1}", LogPrefix, ReturnValue!= null ?ReturnValue.Count.ToString():"Nothing");
                return new Model.Operations.RiskHospitalsList_Response() { Hospitals = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}RiskHospitalsList:Exception='{1}'",LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}