﻿using DGS.ArbPro.Service.Interface;
using ServiceStack.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGS.ArbPro.Interface.Operations
{
    public class AW_NewJobItem : ArbProServiceBase
    {
        public object any(Model.Operations.AW_NewJobItem request)
        {
            MobileManager m;
            int JobItemId = -1;

            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                BaseLogger.InfoFormat("{0}AddNewJob: Get Document request received", LogPrefix);
                m = new MobileManager(false, "Service", ClientId);
                m.AddJobItem(request);

                return new Model.Operations.AW_NewJobItem() {};
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}AssociatedDocuments:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }

            return JobItemId;
        }
    }
}