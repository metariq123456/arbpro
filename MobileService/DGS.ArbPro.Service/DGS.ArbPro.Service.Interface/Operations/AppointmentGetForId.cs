﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGS.ArbPro.Interface.Operations
{
    public class AppointmentGetForId : ArbProServiceBase
    {
        public object any(ArbPro.Model.Operations.AppointmentGetForId request)
        {
            MobileManager m;
            Model.Types.Appointment ReturnValue;

            try
            {
                m = new MobileManager(false, "Service", request.ClientId);
                ReturnValue = m.GetAppointmentForID(request.AppointmentId);

                if (ReturnValue == null)
                    ReturnValue = new Model.Types.Appointment();

                BaseLogger.InfoFormat("AppointmentGetForId:Request:AppointmentId={0} Returned:'{1}'", request.AppointmentId, ReturnValue.Id);

                return new Model.Operations.AppointmentGetForId_Response() { Appointment = ReturnValue };
            }
            catch (Exception ex) 
            {
                BaseLogger.ErrorFormat("AppointmentGetForId:Exception='{0}'", ex.ToString());
                throw (ex);
            }
        }
    }
}