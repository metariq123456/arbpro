﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface.Operations
{
    public class GetCurrentUser : ArbProServiceBase
    {
        public object any(ArbPro.Service.Model.Operations.GetCurrentUser request)
        {
            MobileManager m;
            Model.Types.User ReturnValue;

            try
            {
                m = new MobileManager(false, "Service");
                ReturnValue = new Model.Types.User()
                {
                    UserName = ServiceExtensions.GetSession(this).UserAuthName,
                    DisplayName = ServiceExtensions.GetSession(this).DisplayName,
                    Id = int.Parse(ServiceExtensions.GetSession(this).UserAuthId),
                    ClientId = 2,
                    Password = "Password1",
                    UserGuid = Guid.Parse(ServiceExtensions.GetSession(this).Sequence)
                };

                BaseLogger.InfoFormat("{0}UserAuthenticate:Request:Returned {1}",LogPrefix, ReturnValue.DisplayName);
                return new Model.Operations.GetCurrentUser_Response() { User = ReturnValue };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}UserAuthenticate:Exception='{1}'",LogPrefix, ex.ToString());
                throw (ex);
            }
        }
    }
}