﻿using DGS.ArbPro.Service.Interface;
using ServiceStack.ServiceInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGS.ArbPro.Interface.Operations
{
    public class AcceptedQuote : ArbProServiceBase
    {
         public object any(Model.Operations.AcceptedJob request)
        {
            MobileManager m;
            List<Model.Operations.AcceptedQuote> lstQuote = null;
            String ClientId = "";
            try
            {
                ClientId = ServiceExtensions.GetSession(this).Email;
            }
            catch { }

            try
            {
                BaseLogger.InfoFormat("{0}Get AcceptedQuote: Get AcceptedQuote request received", LogPrefix);
                m = new MobileManager(false, "Service", ClientId);
                lstQuote = m.GetAcceptedQuoteList();
                return new Model.Operations.AcceptedJob_Response() { AcceptedQuoteDetail = lstQuote };
            }
            catch (Exception ex)
            {
                BaseLogger.ErrorFormat("{0}AcceptedQuote:Exception='{1}'", LogPrefix, ex.ToString());
                throw (ex);
            }
        } 
    }
}