using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Configuration;

using fwkArbProMobile;

using GeoCoding;
using GeoCoding.Google;

using log4net;

using mobCommonCode;

namespace DGS.ArbPro.Service.Interface
{
    using System.Data.SqlTypes;

    using DGS.ArbPro.DAL;
    using DGS.ArbPro.Service.Model.Types;

    using fwkArbProMobile.dbContext;

    using ServiceStack.Common.Utils;

    using Quote = DGS.ArbPro.Service.Model.Types.Quote;
    using QuoteItem = DGS.ArbPro.Service.Model.Types.QuoteItem;
    using Risk = DGS.ArbPro.Service.Model.Types.Risk;
    using RiskAdditionalItem = DGS.ArbPro.Service.Model.Types.RiskAdditionalItem;
    using RiskCriterion = DGS.ArbPro.Service.Model.Types.RiskCriterion;
    using RiskSection = DGS.ArbPro.Service.Model.Types.RiskSection;
    using Tool = DGS.ArbPro.Service.Model.Types.Tool;
    using User = DGS.ArbPro.Service.Model.Types.User;
    using DGS.ArbPro.Model.Operations;
    using System.Text;

    public class MobileManager : baseBusinessClass
    {
        private static ILog log = LogManager.GetLogger("ArbProService");


        string CLASS_NAME = "::BusinessLogic:MobileManager:";
        string _ClientConnString = string.Empty;
        string _AuditUserName = "Service";
        private string clientDBName = string.Empty;


        public MobileManager(bool UseCache, string AuditUserName, string ConnString = "")
            : base(UseCache)
        {
            this.clientDBName = ConnString;
            this._ClientConnString = ConnString;
            if (!string.IsNullOrEmpty(ConnString)) this.ConnectionString = this.GetConnectionStringForClient(ConnString);
            this._AuditUserName = AuditUserName;
        }

        private string DecodeSafeURL(string value)
        {
            log.DebugFormat("DecodeSafeURL: Parameters: encoded value= {0}", value);
            string tmp = HttpUtility.UrlDecode(value);
            log.DebugFormat("DecodeSafeURL: Decoded value= {0}", tmp);
            if (tmp == null)
                tmp = string.Empty;
            return tmp;
        }


        private string CacheKeyPrefix(string ObjectTypeName)
        {
            return ObjectTypeName + "~";
        }


        public string GetConnectionStringForClient(string DBName)
        {
            string ReturnValue;

            log.DebugFormat("Database Name = {0}", DBName);

            ReturnValue = this.ConnectionString;
            ReturnValue = ReturnValue.Contains("ArbProMaster_UAT") ? ReturnValue.Replace("ArbProMaster_UAT", "{0}") : ReturnValue.Replace("ArbProMaster", "{0}");


            try
            {
                ReturnValue = string.Format(ReturnValue, DBName);
                log.DebugFormat("Connection: {0}", DBName);
                return ReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }


        #region User
        public User AuthenticateUser(string Username, string Password)
        {
            DataUserAccount dataObject = new DataUserAccount(this.ConnectionString, this._AuditUserName);
            User ReturnValue = null;


            try
            {
                this.CacheKey = this.CacheKeyPrefix("User");
                if (this.UseCache == false) this.RemoveFromCache(this.CacheKey);
                this.CacheKey += Username.ToString() + ":" + Password.ToString();


                if (this.UseCache)
                    ReturnValue = (Model.Types.User)this.RetrieveFromCache();


                if (ReturnValue == null)
                {
                    DataRow dr = dataObject.MobileGetUser(Username, Password);


                    ReturnValue = new Model.Types.User();
                    ReturnValue.Id = this.DBIntegerValue(dr["Id"], 0);
                    ReturnValue.UserName = this.DBStringValue(dr["UserName"], string.Empty);
                    ReturnValue.DisplayName = this.DBStringValue(dr["DisplayName"], string.Empty);
                    ReturnValue.Password = this.DBStringValue(dr["Password"], string.Empty);
                    ReturnValue.ClientId = this.DBIntegerValue(dr["ClientId"], 0);
                }

                this.AddToCache(ReturnValue);
                return ReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
            finally
            {
                dataObject = null;
            }
        }

        #endregion


        #region Appointment
        public List<Appointment> GetAppointmentsDownloadListForUser(int UserId)
        {
            DataAppointment dataObject = new DataAppointment(this.ConnectionString, this._AuditUserName);
            List<Appointment> ReturnValue = null;

            try
            {
                this.CacheKey = this.CacheKeyPrefix("AppointmentsDownloadList");
                if (this.UseCache == false) this.RemoveFromCache(this.CacheKey);
                this.CacheKey += UserId.ToString();


                if (this.UseCache)
                    ReturnValue = (List<Appointment>)this.RetrieveFromCache();


                if (ReturnValue == null)
                {
                    var ds = dataObject.MobileDownloadListForUser(UserId);
                    DataTable dt = new DataTable();
                    if (ds != null && ds.Tables.Count > 0) dt = ds.Tables[0];
                    ReturnValue = new List<Appointment>();


                    log.InfoFormat("GetAppointmentsDownloadListForUser:Parameters:UserId={0} {1} appointments found", UserId, dt.Rows.Count);


                    foreach (DataRow dr in dt.Rows)
                    {
                        Appointment app = new Appointment();
                        app.Id = this.DBIntegerValue(dr["MasterCalendarId"], 0);
                        app.MasterCalendarId = this.DBIntegerValue(dr["MasterCalendarId"], 0);
                        app.ClientIdNo = this.DBIntegerValue(dr["ClientIdNo"], 0);
                        app.JobIdNo = this.DBIntegerValue(dr["JobIdNo"], 0);
                        app.StartDateTime = this.DBDateValue(dr["StartDateTime"], DateTime.MinValue);
                        app.QuoteNo = this.DBIntegerValue(dr["QuoteNo"], 0);
                        app.Duration = (float)this.DBDecimalValue(dr["Duration"], 0);
                        app.Subject = this.DBStringValue(dr["Subject"], string.Empty);
                        app.UserCR = this.DBStringValue(dr["UserCR"], string.Empty);
                        app.SurveyorId = this.DBIntegerValue(dr["SurveyorId"], 0);
                        app.Comments = this.DBStringValue(dr["AppointmentComments"], string.Empty);
                        app.ClientName = this.DBStringValue(dr["ClientName"], string.Empty);
                        app.SiteStreetNo = this.DBStringValue(dr["ClientStreetNo"], string.Empty);
                        app.SiteAddress1 = this.DBStringValue(dr["ClientAddress1"], string.Empty);
                        app.SiteAddress2 = this.DBStringValue(dr["ClientAddress2"], string.Empty);
                        app.SiteAddressTown = this.DBStringValue(dr["ClientAddressTown"], string.Empty);
                        app.SiteAddressCounty = this.DBStringValue(dr["ClientAddressCounty"], string.Empty);
                        app.SiteAddressPostCode = this.DBStringValue(dr["ClientAddressPostCode"], string.Empty);
                        app.SiteId = this.DBIntegerValue(dr["SiteId"], 0);
                        app.ClientStreetNo = this.DBStringValue(dr["SiteStreetNo"], string.Empty);
                        app.ClientAddress1 = this.DBStringValue(dr["SiteAddress1"], string.Empty);
                        app.ClientAddress2 = this.DBStringValue(dr["SiteAddress2"], string.Empty);
                        app.ClientAddressTown = this.DBStringValue(dr["SiteAddressTown"], string.Empty);
                        app.ClientAddressCounty = this.DBStringValue(dr["SiteAddressCounty"], string.Empty);
                        app.ClientAddressPostCode = this.DBStringValue(dr["SiteAddressPostCode"], string.Empty);
                        app.ClientAddressLocation.Latitude = (float)this.DBDecimalValue(dr["ClientAddressLat"], 0);
                        app.ClientAddressLocation.Longitude = (float)this.DBDecimalValue(dr["ClientAddressLong"], 0);
                        app.ClientContactId = this.DBIntegerValue(dr["ClientContactId"], 0);
                        app.ClientEmail = this.DBStringValue(dr["ClientEmail"], string.Empty);
                        app.ClientTelephone = this.DBStringValue(dr["ClientTelephone"], string.Empty);
                        app.ClientMobile = this.DBStringValue(dr["ClientMobile"], string.Empty);
                        app.Title = this.DBStringValue(dr["Title"], string.Empty);
                        app.FirstName = this.DBStringValue(dr["FirstName"], string.Empty);
                        app.Surname = this.DBStringValue(dr["Surname"], string.Empty);
                        app.ResidentialCommercial = (Appointment.enumResidentialCommercial)this.DBIntegerValue(dr["ResidentialCommercial"], 0);
                        app.RiskTemplateId = this.DBIntegerValue(dr["RiskTemplateId"], 1);

                        log.DebugFormat("GetAppointmentsDownloadListForUser:UserId={0} QuoteID={1}", UserId, app.QuoteNo);



                        if (app.ClientAddressLocation.Latitude == 0 && app.ClientAddressLocation.Longitude == 0)
                        {
                            try
                            {
                                log.DebugFormat("Geocoding for QuoteID {0} Address={1}", app.QuoteNo, app.ClientAddress1 + ", " + app.ClientAddressPostCode);
                                IGeoCoder GeoCoder = new GoogleGeoCoder();
                                var Addresses = GeoCoder.GeoCode(app.ClientAddress1 + ", " + app.ClientAddressPostCode);
                                foreach (Address A in Addresses)
                                {
                                    app.ClientAddressLocation.Latitude = A.Coordinates.Latitude;
                                    app.ClientAddressLocation.Longitude = A.Coordinates.Longitude;
                                }

                                dataObject.MobileSaveLatLong(app.QuoteNo,
                                    decimal.Parse(app.ClientAddressLocation.Latitude.ToString()),
                                    decimal.Parse(app.ClientAddressLocation.Longitude.ToString()));
                            }
                            catch (Exception ex)
                            {
                                log.ErrorFormat("GetAppointmentsDownloadListForUser: Error={0}", ex.ToString());
                            }
                        }


                        ReturnValue.Add(app);
                    }

                    this.AddToCache(ReturnValue);
                }

                return ReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        public bool MarkAppointmentDownloaded(int AppointmentId)
        {
#if DEBUG

            // if (String.IsNullOrEmpty(_ClientConnString)) ConnectionString = GetConnectionStringForClient("ArbProClient2_Dev");
#endif
            DataAppointment dataObject = new DataAppointment(this.ConnectionString, this._AuditUserName);
            bool ReturnValue = false;

            try
            {
                this.CacheKey = this.CacheKeyPrefix("MarkAppointmentDownloaded");
                if (this.UseCache == false)
                {
                    this.RemoveFromCache(this.CacheKey);
                }

                this.CacheKey += AppointmentId.ToString();

                if (this.UseCache)
                {
                    ReturnValue = (Boolean)this.RetrieveFromCache();
                }

                if (ReturnValue == false)
                {
                    ReturnValue = dataObject.MobileMarkDownloaded(AppointmentId);
                    this.AddToCache(ReturnValue);
                }

                return ReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        #endregion


        #region Quote
        public void QuoteImageSave(string Guid, byte[] Picture)
        {
            log.DebugFormat("QuoteImageSave:Parameters:Guid={0} Picture= {1} Bytes", Guid, Picture.Length);
#if DEBUG

            // if (String.IsNullOrEmpty(_ClientConnString)) ConnectionString = GetConnectionStringForClient("ArbProClient2_Dev");
#endif
            DataQuote dataObject = new DataQuote(this.ConnectionString, this._AuditUserName);

            try
            {
                dataObject.MobileQuoteItemImageSave(Guid, Picture);
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error saving image: {0}", ex.ToString());
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        public void QuoteUpdateV3(Quote Quote, string UserCR)
        {

            log.DebugFormat("************* QUOTE UPDATE V3 *************");
            log.DebugFormat("QuoteUpdateV3:parameters for Quote (ID)={0} & UserCR= {1} are:", Quote.QuoteId, UserCR);
            log.DebugFormat("Staff = {0} : Time = {1} : Tools = {2}, comments={3}", Quote.StaffNumber, Quote.TimeHrs, Quote.Tools, Quote.Comments);
            if (string.IsNullOrEmpty(UserCR)) UserCR = string.Empty;
            DataQuote dataObject = new DataQuote(this.ConnectionString, this._AuditUserName);

            if (string.IsNullOrWhiteSpace(Quote.Comments) || Quote.Comments.ToLower().Equals("null"))
            {
                Quote.Comments = string.Empty;
            }

            try
            {
                dataObject.MobileQuoteUpdate(
                    Quote.QuoteId,
                    (Decimal)Quote.TimeHrs,
                    Quote.StaffNumber,
                    this.DecodeSafeURL(Quote.Comments),
                    UserCR);
                log.DebugFormat(
                    "QuoteUpdateV3: Item updates beginning for {0} items:",
                    Quote.Items != null ? Quote.Items.Count : 0);

                // NOTE: The service is logging an exception at this point - Test to see if the items list is being populated
                if (Quote.Items != null && Quote.Items.Count > 0)
                {
                    foreach (QuoteItem item in Quote.Items)
                    {
                        // Call to the quoteiem update procedure
                        this.QuoteItemUpdate(Quote.QuoteId, item, UserCR);
                    }
                }

                // The quote has been updated now update the tools for the quote. Check if the list has been instantiated
                if (Quote.ToolsV3 != null)
                {
                    foreach (Tool tool in Quote.ToolsV3)
                    {
                        try
                        {
                            log.DebugFormat(
                                "QuoteUpdateV3: Tool updateing: Passed values => Appointment Id = {0}, Tool Id = {1}, Tool Name: {2}, Current Stock = {3}, Number Required = {4}",
                                Quote.QuoteId,
                                tool.Id,
                                tool.Name,
                                tool.Stock,
                                tool.NumberRequired);

                            dataObject.MobileQuoteToolUpdateV3(
                                Quote.QuoteId,
                                tool.Id,
                                tool.Name,
                                tool.Stock,
                                tool.NumberRequired);
                        }
                        catch (Exception ex)
                        {
                            // Do not cause the exception to bubble up
                            log.ErrorFormat("Failed to update tool {0} - {1}", tool.Name, ex);
                        }
                    }
                }

                // The Quote has been updated. Now update the Risk Assessment
                dataObject.MobileQuoteCloseconnection();
                log.DebugFormat("************* UPDATE CLOSED *************");
            }
            catch (Exception ex)
            {
                string strException = AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex);

                // log.FatalFormat(strException);
                throw new Exception(strException);
            }
        }

        public void QuoteUpdate(Quote Quote, string UserCR)
        {

            log.DebugFormat("************* QUOTE UPDATE *************");
            log.DebugFormat("QuoteUpdate:parameters for Quote (ID)={0} & UserCR= {1} are:", Quote.QuoteId, UserCR);
            log.DebugFormat("Staff = {0} : Time = {1} : Tools = {2}, comments={3}", Quote.StaffNumber, Quote.TimeHrs, Quote.Tools, Quote.Comments);
            if (string.IsNullOrEmpty(UserCR)) UserCR = string.Empty;
            DataQuote dataObject = new DataQuote(this.ConnectionString, this._AuditUserName);

            try
            {
                dataObject.MobileQuoteUpdate(
                    Quote.QuoteId,
                    (Decimal)Quote.TimeHrs,
                    Quote.StaffNumber,
                    this.DecodeSafeURL(Quote.Comments),
                    UserCR);
                log.DebugFormat(
                    "QuoteUpdate: Item updates beginning for {0} items:",
                    Quote.Items != null ? Quote.Items.Count : 0);

                // NOTE: The service is logging an exception at this point - Test to see if the items list is being populated
                if (Quote.Items != null && Quote.Items.Count > 0)
                {
                    foreach (QuoteItem item in Quote.Items)
                    {
                        // Call to the quoteiem update procedure
                        this.QuoteItemUpdate(Quote.QuoteId, item, UserCR);
                    }
                }

                // The quote has been updated now update the tools for the quote. Check if the list has been instantiated
                if (Quote.Tools != null)
                {
                    /*
                     * The tool string is uploaded as n:Name or n:Names when n > 1
                     * This is to be passed before being sent to the stored procedure to facilitate simplicity
                     */
                    string toolName = string.Empty;
                    int number = 1;
                    int delimiterIndex = 0;
                    foreach (string Tool in Quote.Tools)
                    {
                        try
                        {
                            delimiterIndex = Tool.IndexOf(":");
                            int.TryParse(Tool.Substring(0, delimiterIndex), out number);
                            toolName = Tool.Substring(delimiterIndex + 1);
                            if (number > 1)
                            {
                                if (toolName.Length > 2)
                                {
                                    toolName = toolName.Substring(0, toolName.Length - 1); // Strip the 's' of the end
                                }
                            }

                            log.DebugFormat(
                                "QuoteUpdate: Tool updateing: {0}, Passed values => Name: {1}, Number: {2}",
                                Tool,
                                toolName,
                                number);

                            dataObject.MobileQuoteToolUpdate(Quote.QuoteId, toolName, number);
                        }
                        catch (Exception ex)
                        {
                            // Do not cause the exception to bubble up
                            log.ErrorFormat("Failed to update tool {0} - {1}", Tool, ex);
                        }
                    }
                }

                // The Quote has been updated. Now update the Risk Assessment
                dataObject.MobileQuoteCloseconnection();
                log.DebugFormat("************* UPDATE CLOSED *************");
            }
            catch (Exception ex)
            {
                string strException = AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex);

                // log.FatalFormat(strException);
                throw new Exception(strException);
            }
        }

        public void QuoteItemUpdate(int quoteId, QuoteItem item, string UserCR)
        {


            log.DebugFormat("QuoteItemUpdate:parameters for Quote (ID)={0} & UserCR= {1} are:", quoteId, UserCR);
            if (string.IsNullOrEmpty(UserCR)) UserCR = string.Empty;

            DataQuote dataObject = new DataQuote(this.ConnectionString, this._AuditUserName);

            int TPO = 0;
            try
            {
                log.DebugFormat(
                    "QuoteUpdate:Item update - parameters: Number={0}, Type={1}, Value={2}, Description={3}, Presevation={4} Location={5}, GUID={6}",
                    this.DecodeSafeURL(item.ItemNumber),
                    this.DecodeSafeURL(item.ItemType),
                    item.ItemValue,
                    this.DecodeSafeURL(item.WorkDescription),
                    item.PreservationOrderStatus.ToString(),
                    item.Location != null
                        ? string.Format("{0},{1}", item.Location.Latitude, item.Location.Longitude)
                        : "Not Set",
                    item.ItemGuid != null ? item.ItemGuid : "NULL");

                if (item.ItemGuid == null || item.ItemGuid.Length == 0)
                {
                    log.DebugFormat(
                        "QuoteUpdate: Guid not valid: {0}",
                        item.ItemGuid == null
                            ? "Null value recieved"
                            : string.Format("{0} Bytes recieved", item.ItemGuid.Length));
                    throw new Exception(
                        string.Format("Invalid reference for Quote Item Guid in Quote (Id) = {0}", quoteId));
                }

                // The following is required due to the default values for the enum being incorrect
                // Parsing the enum value to an int would give Yes a value of 0
                switch (item.PreservationOrderStatus)
                {
                    case Model.Types.QuoteItem.enumPreservationOrderStatus.Unknown:
                        TPO = 0;
                        break;
                    case Model.Types.QuoteItem.enumPreservationOrderStatus.No:
                        TPO = 1;
                        break;
                    case Model.Types.QuoteItem.enumPreservationOrderStatus.Yes:
                        TPO = 2;
                        break;
                }

                if (item.Location != null)
                {
                    dataObject.MobileQuoteItemUpdate(
                        quoteId,
                        this.DecodeSafeURL(item.ItemNumber),
                        this.DecodeSafeURL(item.ItemType),
                        this.DecodeSafeURL(item.WorkDescription),
                        (Decimal)item.ItemValue,
                        item.ItemGuid,
                        TPO,
                        (Decimal)item.Location.Latitude,
                        (Decimal)item.Location.Longitude,
                        UserCR);
                }
                else
                {
                    dataObject.MobileQuoteItemUpdate(
                        quoteId,
                        this.DecodeSafeURL(item.ItemNumber),
                        this.DecodeSafeURL(item.ItemType),
                        this.DecodeSafeURL(item.WorkDescription),
                        (Decimal)item.ItemValue,
                        item.ItemGuid,
                        TPO,
                        0,
                        0,
                        UserCR);
                }

                log.DebugFormat(
                    "QuoteUpdate:Item update completed for Item No={0}, GUID={1}",
                    item.ItemNumber,
                    item.ItemGuid);

                dataObject.MobileQuoteCloseconnection();
            }
            catch (Exception ex)
            {
                string strException = AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex);

                // log.FatalFormat(strException);
                throw new Exception(strException);
            }
        }

        /// <summary>
        /// The client update.
        /// </summary>
        /// <param name="client">
        /// The client.
        /// </param>
        /// <exception cref="Exception">
        /// </exception>
        public void ClientUpdate(Model.Types.Client client)
        {
            DAL.Client dtbClient = new DAL.Client(this.clientDBName);

            try
            {
                Types.Client webClient = this.ConvertToWebClient(client);

                int success = dtbClient.Save(webClient);

                if (success < 1)
                {
                    throw new Exception("Client insert or update failed.");
                }
            }
            catch (Exception ex)
            {
                log.FatalFormat(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
                throw new Exception(string.Format("Client update or insert failed: {0}", ex.Message));
            }
        }

        /// <summary>
        /// The risk update.
        /// </summary>
        /// <param name="risk"> The risk. </param>
        /// <param name="userId"> The user id. </param>
        /// <param name="userCr"> The user cr. </param>
        /// <exception cref="Exception"> Exception ex </exception>
        public void RiskAssessmentUpdateV3(Risk risk)
        {
            DAL.Risk dtbRisk = new DAL.Risk(this.clientDBName);
            DAL.RiskSection dtbRiskSection = new DAL.RiskSection(this.clientDBName);
            DAL.RiskCriterion dtbRiskCriterion = new DAL.RiskCriterion(this.clientDBName);

            DataRisk dataObject = new DataRisk(this.ConnectionString, this._AuditUserName);
            DataTable dt = new DataTable();

            log.DebugFormat("RiskAssessmentUpdateV3:Parameters: Risk (For Appointment Id)={0}", risk.AppointmentId);
            try
            {
                log.DebugFormat("RiskAssessmentUpdateV3:Updating Risk");
                var ds = dataObject.MobileQuoteGetOrCreate(risk.AppointmentId);

                if (ds != null && ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];
                }

                risk.QuoteId = this.DBIntegerValue(dt.Rows[0]["QuoteRecId"], -1);
                risk.ClientId = this.DBIntegerValue(dt.Rows[0]["ClientId"], -1);
                risk.JobSiteId = this.DBIntegerValue(dt.Rows[0]["JobSiteId"], -1);
                risk.AssessingStaffId = this.DBIntegerValue(dt.Rows[0]["StaffId"], -1);
                risk.ReferenceDoc = this.DBStringValue(dt.Rows[0]["ReferenceDoc"], string.Empty);
                risk.LastUpdatedBy = this.DBStringValue(dt.Rows[0]["LastUpdatedBy"], string.Empty);

                Types.Risk riskObj = this.ConvertToWebRiskAssessment(risk);

                int riskAssessmentId = dtbRisk.Save(riskObj);

                foreach (Types.RiskSection s in riskObj.AssessmentSections)
                {
                    s.RiskAssessmentId = riskAssessmentId;
                    int sectionId = dtbRiskSection.Save(s);

                    foreach (Types.RiskCriterion c in s.Criteria)
                    {
                        c.RiskSectionId = sectionId;

                        dtbRiskCriterion.Save(c);
                    }
                }

                log.DebugFormat(
                    "RiskAssessmentUpdateV3:Updating AdditionalRisks (Count = {0})",
                    risk.AdditionalRisks != null ? risk.AdditionalRisks.Count.ToString() : "NULL");

                if (risk.AdditionalRisks != null)
                {
                    foreach (RiskAdditionalItem other in risk.AdditionalRisks)
                    {
                        dataObject.MobileAdditionalRisksInsert(
                            risk.AppointmentId,
                            this.DecodeSafeURL(other.AssessedRisk),
                            this.DecodeSafeURL(other.ControlRequired));
                    }
                }

                dataObject.MobileRiskCloseconnection();
            }
            catch (Exception ex)
            {
                log.FatalFormat(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
                throw new Exception(string.Format("Risk assessment upload failed: {0}, {1}", ex.Message, dt));
            }
        }

        /// <summary>
        /// Updates risk assessment
        /// </summary>
        /// <param name="Risk"></param>
        /// <param name="userId"></param>
        /// <param name="UserCR"></param>
        public void RiskUpdate(RiskAssessment Risk, int userId, string UserCR)
        {
            if (string.IsNullOrEmpty(UserCR)) UserCR = string.Empty;
            DataRisk dataObject = new DataRisk(this.ConnectionString, this._AuditUserName);

            log.DebugFormat("RiskUpdate:Parameters: Risk (For Quote Id)={0}, UserCR={1}", Risk.QuoteNo, UserCR);
            try
            {
                log.DebugFormat("RiskUpdate:Creating Risk reference dictionaries");
                List<AssessmentItem> allItems = new List<AssessmentItem>();

                foreach (AssessmentSection s in Risk.Criterion)
                {
                    allItems.AddRange(s.AssessmentItems);
                }

                Dictionary<string, bool> controlRequired = new Dictionary<string, bool>();
                Dictionary<string, string> genericRef = new Dictionary<string, string>();
                Dictionary<string, string> notes = new Dictionary<string, string>();


                DataSet ds = dataObject.MobileRiskSectionList();


                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    this.AddRiskKeyValueParams(r["AssessmentItem"].ToString(), r["Description"].ToString(), allItems, controlRequired, genericRef);
                }




                AssessmentSection section = null;
                string notesValue = string.Empty;
                section = Risk.Criterion.Find(s => s.Title.ToLower() == "site considerations");
                if (section != null)
                {
                    notesValue = section.AdditionalDetail == null ? string.Empty : this.DecodeSafeURL(section.AdditionalDetail);
                    notes.Add("RiskSiteControlMeasuresNecessary", notesValue);
                }

                section = Risk.Criterion.Find(s => s.Title.ToLower() == "environmental");
                if (section != null)
                {
                    notesValue = section.AdditionalDetail == null ? string.Empty : this.DecodeSafeURL(section.AdditionalDetail);
                    notes.Add("RiskEnvironmentalControlMeasuresNecessary", this.DecodeSafeURL(section.AdditionalDetail));
                }

                section = Risk.Criterion.Find(s => s.Title.ToLower() == "operations");
                if (section != null)
                {
                    notesValue = section.AdditionalDetail == null ? string.Empty : this.DecodeSafeURL(section.AdditionalDetail);
                    notes.Add("RiskOperationsControlMeasuresNecessary", this.DecodeSafeURL(section.AdditionalDetail));
                }

                section = Risk.Criterion.Find(s => s.Title.ToLower() == "machinery");
                if (section != null)
                {
                    notesValue = section.AdditionalDetail == null ? string.Empty : this.DecodeSafeURL(section.AdditionalDetail);
                    notes.Add("RiskMachinerControlMeasuresNecessary", this.DecodeSafeURL(section.AdditionalDetail));
                }

                section = Risk.Criterion.Find(s => s.Title.ToLower() == "working at height");
                if (section != null)
                {
                    notesValue = section.AdditionalDetail == null ? string.Empty : this.DecodeSafeURL(section.AdditionalDetail);
                    notes.Add("RiskWorking@HeightControlMeasures", this.DecodeSafeURL(section.AdditionalDetail));
                }

                log.DebugFormat("RiskUpdate:Updating Risk");
                dataObject.MobileRiskAssessmentUpdate(Risk.QuoteNo, userId, Risk.NearestAnE, Risk.AerialRescuePlan, this.DecodeSafeURL(Risk.MobileSignal), Risk.GridReference, this.DecodeSafeURL(Risk.RescueAccess), this.DecodeSafeURL(Risk.RescueAccess), Risk.CompletionDate, controlRequired, genericRef, notes);

                log.DebugFormat("RiskUpdate:Updating AdditionalRisks (Count = {0})", Risk.FurtherRisks != null ? Risk.FurtherRisks.Count.ToString() : "NULL");
                foreach (AssessmentAdditionalItem other in Risk.FurtherRisks)
                {
                    dataObject.MobileAdditionalRisksInsert(Risk.QuoteNo, this.DecodeSafeURL(other.Detail), this.DecodeSafeURL(other.Control));
                }

                dataObject.MobileRiskCloseconnection();
            }
            catch (Exception ex)
            {
                log.FatalFormat(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
                throw new Exception(string.Format("Risk assessment upload failed: {0}", ex.Message));
            }
        }

        private void AddRiskKeyValueParams(string key, string fieldTitle, List<AssessmentItem> items, Dictionary<string, bool> control, Dictionary<string, string> reference)
        {
            log.DebugFormat("AddRiskKeyValueParams:Evaluating Key - {0} Field - {1}", key, fieldTitle);
            AssessmentItem currentItem = items.Find(itm => itm.Title.ToLower() == fieldTitle.ToLower());
            if (currentItem != null)
            {
                log.DebugFormat("AddRiskKeyValueParams: Item found in reference list, adding values ({0}) to paramater  dictionaries ", string.Format("{0} - Ref: {1}", currentItem.ControlRequired.ToString(), currentItem.RefCode));
                control.Add(key, currentItem.ControlRequired);
                reference.Add(key + "Ref", currentItem.RefCode);
            }
            else
            {
                log.DebugFormat("AddRiskKeyValueParams: Field - {0} not found in list", fieldTitle);
            }
        }


        public void RiskSignatureImageSave(int QuoteNo, byte[] Picture)
        {
#if DEBUG

            // if (String.IsNullOrEmpty(_ClientConnString)) ConnectionString = GetConnectionStringForClient("ArbProClient2_Dev");
#endif
            DataRisk dataObject = new DataRisk(this.ConnectionString, this._AuditUserName);

            try
            {
                dataObject.MobileRiskSignatureImageSave(QuoteNo, Picture);
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        #endregion

        #region Risk Assessment
        public void RiskSignatureSave(string Guid, byte[] Picture)
        {
#if DEBUG
            if (string.IsNullOrEmpty(this._ClientConnString)) this.ConnectionString = this.GetConnectionStringForClient("ArbProClient2_Dev");
#endif
            DataQuote dataObject = new DataQuote(this.ConnectionString, this._AuditUserName);


            try
            {
                dataObject.MobileQuoteItemImageSave(Guid, Picture);
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        /// <summary>
        /// Method to get a risk assessment for a template ID
        /// </summary>
        /// <param name="templateId">The risk template ID</param>
        /// <param name="appointmentId">The appointment ID</param>
        /// <returns>Risk Assessment</returns>
        public Risk RiskAssessmentGetForTemplateId(int templateId, int appointmentId)
        {
            DAL.Templates.Risk dbRisk = new DAL.Templates.Risk(this.clientDBName);
            DAL.Risk dtbRiskAssessment = new DAL.Risk(this.clientDBName);

            try
            {
                Risk riskAssessment = this.ConvertToMobileRiskAssessment(dtbRiskAssessment.GetForAppointmentId(appointmentId));

                if (riskAssessment != null && riskAssessment.Id > 0)
                {
                    return riskAssessment;
                }

                riskAssessment = new Model.Types.Risk();

                Types.Templates.Risk riskTemplate = dbRisk.GetTemplate(templateId);

                riskAssessment.ReferenceDoc = riskTemplate.ReferenceDoc;

                riskAssessment.AssessmentSections = new List<RiskSection>();

                foreach (var s in riskTemplate.TemplateSections)
                {
                    RiskSection section = new RiskSection();

                    section.Title = s.Title;
                    section.Chapter = s.ChapterRef;
                    section.ReferenceDoc = s.ReferenceDoc;
                    section.Description = s.Description;

                    section.Criteria = new List<RiskCriterion>();

                    foreach (var c in s.TemplateCriteria)
                    {
                        RiskCriterion criterion = new RiskCriterion();

                        criterion.AssessmentPhase = c.AssessmentPhase;
                        criterion.ChapterRef = c.ChapterRef;
                        criterion.Criterion = c.Criterion;

                        section.Criteria.Add(criterion);
                    }

                    riskAssessment.AssessmentSections.Add(section);
                }

                return riskAssessment;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        public List<AssessmentSection> RiskSectionList()
        {
#if DEBUG

            // if (String.IsNullOrEmpty(_ClientConnString)) ConnectionString = GetConnectionStringForClient("ArbProClient2_Dev");
#endif
            DataRisk dataObject = new DataRisk(this.ConnectionString, this._AuditUserName);
            List<AssessmentSection> ReturnValue = null;

            try
            {
                this.CacheKey = this.CacheKeyPrefix("RiskSectionList");
                if (this.UseCache == false)
                {
                    this.RemoveFromCache(this.CacheKey);
                }

                if (this.UseCache)
                {
                    ReturnValue = (List<AssessmentSection>)this.RetrieveFromCache();
                }

                if (ReturnValue == null)
                {
                    DataTable dt = dataObject.MobileRiskSectionList().Tables[0];
                    ReturnValue = new List<AssessmentSection>();

                    AssessmentSection section = null;
                    AssessmentItem item = null;

                    string currentSection = string.Empty;
                    string lastSection = string.Empty;

                    foreach (DataRow row in dt.Rows)
                    {
                        // Only 1st phase criterion are required for download
                        if (this.DBIntegerValue(row["AssessmentPhase"], 1) < 2)
                        {
                            currentSection = this.DBStringValue(row["AssessmentSection"], string.Empty);
                            if (section == null || currentSection != lastSection)
                            {
                                if (section != null)
                                {
                                    ReturnValue.Add(section);
                                }

                                section = new AssessmentSection();
                                section.AssessmentItems = new List<AssessmentItem>();
                                section.Title = currentSection;
                                lastSection = currentSection;
                            }

                            item = new AssessmentItem();

                            item.Title = this.DBStringValue(row["Description"], string.Empty);
                            item.RefCode = this.DBStringValue(row["Reference"], string.Empty);

                            section.AssessmentItems.Add(item);
                        }
                    }

                    if (!ReturnValue.Contains(section))
                    {
                        ReturnValue.Add(section);
                    }

                    this.AddToCache(ReturnValue);
                }

                return ReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        #endregion

        #region Enquiry

        /// <summary>
        /// Sets up an enquiry, job and appointment ready for download. Used for mobile app quote creation.
        /// </summary>
        /// <param name="client"> The client. </param>
        /// <param name="mobileUserId"> The mobile user id. </param>
        /// <param name="reporterName"> The reporter name. </param>
        /// <param name="source"> The source. </param>
        /// <param name="comments"> The comments. </param>
        /// <param name="apptStart"> The appointment start. </param>
        /// <param name="apptEnd"> The appointment end. </param>
        /// <param name="streetNo"> The street no. </param>
        /// <param name="address1"> The address 1. </param>
        /// <param name="address2"> The address 2. </param>
        /// <param name="town"> The town. </param>
        /// <param name="county"> The county. </param>
        /// <param name="postCode"> The post code. </param>
        /// <exception cref="Exception"> Exceptions at various stages of the job and appointment creation related to unsuccessful operations. </exception>
        public void CreateSalesEnquiry(Model.Types.Client client, int mobileUserId, string reporterName, string source, string comments, DateTime apptStart, DateTime apptEnd, string streetNo, string address1, string address2, string town, string county, string postCode)
        {
            var dtbClient = new DAL.Client(this.clientDBName);

            if (client.Id < 1)
            {
                Types.Client webClient = this.ConvertToWebClient(client);

                client.Id = dtbClient.Save(webClient);
            }

            Types.Client enqClient = dtbClient.GetForClientId(client.Id);

            var dtbStaff = new DAL.Staff(this.clientDBName);
            Types.Staff staff = dtbStaff.GetForMobileId(mobileUserId);

            var dtbjob = new DAL.Job(this.clientDBName);
            Types.Job newJob = new Types.Job()
            {
                ClientId = enqClient.Id,
                ContactId = enqClient.Contacts.Find(ct => ct.ContactType == Types.Enums.ContactTypeEnum.Default).Id,
                Source = source,
                Comments = comments,
                ReporterID = mobileUserId,
                Reporter = reporterName,
                SurveyorId = staff.Id
            };

            newJob.Id = dtbjob.Save(newJob);

            if (newJob.Id == -1)
            {
                throw new Exception(string.Format("Could not save Job. Client Id: {0}, Reporter/Mobile User Id: {1}, Surveyor Id: {2}.", enqClient.Id, mobileUserId, staff.Id));
            }

            DAL.ClientSite dtbSite = new DAL.ClientSite(this.clientDBName);
            Types.ClientSite clientSite = null;
            int siteId = dtbSite.CheckIfExistsReturnId(enqClient.Id, streetNo, address1, address2, town, county, postCode);
            if (siteId == -1)
            {
                clientSite = new Types.ClientSite()
                {
                    HouseNameNo = streetNo,
                    Address1 = address1,
                    Address2 = address2,
                    Town = town,
                    County = county,
                    PostCode = postCode,
                    ClientId = enqClient.Id,
                    ContactId = newJob.ContactId,
                    Reference = string.Empty
                };
                clientSite.Id = dtbSite.Save(clientSite);
            }
            else
            {
                clientSite = dtbSite.GetForId(siteId);
            }

            DAL.JobSite dtnJobSite = new DAL.JobSite(this.clientDBName);
            Types.JobSite newJSite = new Types.JobSite()
            {
                ClientId = enqClient.Id,
                JobId = newJob.Id,
                Site = clientSite,
                ContactId = newJob.ContactId
            };

            newJSite.Id = dtnJobSite.Save(newJSite);
            siteId = newJSite.Id;

            if (siteId == -1)
            {
                throw new Exception(string.Format("Could not save Job Site. Client Id: {0}, Job Id: {1}, Contact Id: {2}.", enqClient.Id, newJob.Id, newJob.ContactId));
            }

            if (apptStart != DateTime.MinValue && apptEnd >= apptStart)
            {
                var dtbCalendarQuote = new DAL.Calendar(this.clientDBName);
                Types.CalendarQuote newQuote = new Types.CalendarQuote()
                {
                    StartDateTime = apptStart,
                    EndDateTime = apptEnd,
                    SurveyorID = staff.Id,
                    JobSiteID = siteId,
                    Completed = false,
                    LastUpdated = DateTime.Now,
                    LastDownloaded = DateTime.MinValue,
                    UploadTime = DateTime.MinValue,
                    TemplateId = 1
                };

                if (!dtbCalendarQuote.SaveQuote(newQuote))
                {
                    throw new Exception(string.Format("Could not save Appointment. Client Id: {0}, Job Site Id: {1}, Surveyor Id: {2}.", enqClient.Id, siteId, staff.Id));
                }
            }

            log.DebugFormat("({0}:{1}) New Job {2} Saved", staff.Id, reporterName, newJob.Id);
        }

        #endregion

        #region Lists

        public List<ItemDescription> ItemDescriptionList()
        {
#if DEBUG

            // if (String.IsNullOrEmpty(_ClientConnString)) ConnectionString = GetConnectionStringForClient("ArbProClient2_Dev");
#endif
            DataItem dataObject = new DataItem(this.ConnectionString, this._AuditUserName);
            List<ItemDescription> ReturnValue = null;

            try
            {
                this.CacheKey = this.CacheKeyPrefix("ItemDescriptionList");
                if (this.UseCache == false)
                {
                    this.RemoveFromCache(this.CacheKey);
                }

                if (this.UseCache)
                {
                    ReturnValue = (List<ItemDescription>)this.RetrieveFromCache();
                }

                if (ReturnValue == null)
                {
                    DataTable dt = dataObject.MobileItemDescriptionList().Tables[0];
                    ReturnValue = new List<ItemDescription>();
                    foreach (DataRow row in dt.Rows)
                    {
                        ItemDescription ItemDesc = new ItemDescription();
                        ItemDesc.Id = this.DBIntegerValue(row["Id"], 0);
                        ItemDesc.Description = this.DBStringValue(row["Description"], string.Empty);
                        ReturnValue.Add(ItemDesc);
                    }

                    this.AddToCache(ReturnValue);
                }

                return ReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        public List<ItemType> ItemTypeList()
        {
#if DEBUG

            // if (String.IsNullOrEmpty(_ClientConnString)) ConnectionString = GetConnectionStringForClient("ArbProClient2_Dev");
#endif
            DataItem dataObject = new DataItem(this.ConnectionString, this._AuditUserName);
            List<ItemType> ReturnValue = null;

            try
            {
                this.CacheKey = this.CacheKeyPrefix("ItemTypeList");
                if (this.UseCache == false)
                {
                    this.RemoveFromCache(this.CacheKey);
                }

                if (this.UseCache)
                {
                    ReturnValue = (List<ItemType>)this.RetrieveFromCache();
                }

                if (ReturnValue == null)
                {
                    DataTable dt = dataObject.MobileItemTypeList().Tables[0];
                    ReturnValue = new List<ItemType>();
                    foreach (DataRow row in dt.Rows)
                    {
                        ItemType ItemDesc = new ItemType();
                        ItemDesc.CommonName = this.DBStringValue(row["CommonName"], string.Empty);
                        ItemDesc.TechnicalName = this.DBStringValue(row["TechnicalName"], string.Empty);
                        ReturnValue.Add(ItemDesc);
                    }

                    this.AddToCache(ReturnValue);
                }

                return ReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        public List<Tool> ToolList()
        {
#if DEBUG

            // if (String.IsNullOrEmpty(_ClientConnString)) ConnectionString = GetConnectionStringForClient("ArbProClient2_Dev");
#endif
            DataItem dataObject = new DataItem(this.ConnectionString, this._AuditUserName);
            List<Tool> ReturnValue = null;

            try
            {
                this.CacheKey = this.CacheKeyPrefix("ToolList");
                if (this.UseCache == false)
                {
                    this.RemoveFromCache(this.CacheKey);
                }

                if (this.UseCache)
                {
                    ReturnValue = (List<Tool>)this.RetrieveFromCache();
                }

                if (ReturnValue == null)
                {
                    DataTable dt = dataObject.MobileToolList().Tables[0];
                    ReturnValue = new List<Tool>();
                    foreach (DataRow row in dt.Rows)
                    {
                        Tool Tool = new Model.Types.Tool();
                        Tool.Id = this.DBIntegerValue(row["Id"], 0);
                        Tool.Name = this.DBStringValue(row["Name"], string.Empty);
                        ReturnValue.Add(Tool);
                    }

                    this.AddToCache(ReturnValue);
                }

                return ReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        /// <summary>
        /// Gets the tools list
        /// </summary>
        /// <returns>List of tools</returns>
        public List<Tool> ToolListV3()
        {
            DataItem dataObject = new DataItem(this.ConnectionString, this._AuditUserName);
            List<Tool> returnValue = new List<Tool>();

            try
            {
                DataTable dt = dataObject.MobileToolListV3().Tables[0];

                foreach (DataRow row in dt.Rows)
                {
                    Tool tool = new Model.Types.Tool();
                    tool.Id = this.DBIntegerValue(row["Id"], 0);
                    tool.Name = this.DBStringValue(row["Name"], string.Empty);
                    tool.Stock = this.DBIntegerValue(row["Stock"], 0);
                    returnValue.Add(tool);
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        public List<Model.Types.Client> ClientsList()
        {
            DAL.Client dtbClients = new DAL.Client(this.clientDBName);

            List<Model.Types.Client> clientList = this.ConvertToMobileClientList(dtbClients.GetLightClientAll());

            return clientList;
        }

        public List<RiskHospital> RiskHospitalList()
        {
#if DEBUG

            // if (String.IsNullOrEmpty(_ClientConnString)) ConnectionString = GetConnectionStringForClient("ArbProClient2_Dev");
#endif
            DataRisk dataObject = new DataRisk(this.ConnectionString, this._AuditUserName);
            List<RiskHospital> ReturnValue = null;

            try
            {
                this.CacheKey = this.CacheKeyPrefix("RiskHospitalList");
                if (this.UseCache == false)
                {
                    this.RemoveFromCache(this.CacheKey);
                }

                if (this.UseCache)
                {
                    ReturnValue = (List<RiskHospital>)this.RetrieveFromCache();
                }

                if (ReturnValue == null)
                {
                    DataTable dt = dataObject.MobileHospitalList().Tables[0];
                    ReturnValue = new List<RiskHospital>();
                    foreach (DataRow row in dt.Rows)
                    {
                        RiskHospital Hospital = new RiskHospital();
                        Hospital.HospitalId = this.DBIntegerValue(row["ID"], 0);
                        Hospital.Name = this.DBStringValue(row["RiskHospital"], string.Empty);
                        ReturnValue.Add(Hospital);
                    }

                    this.AddToCache(ReturnValue);
                }

                return ReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        public List<RiskAerialRescuePlan> RiskAerialRescuePlanList()
        {
#if DEBUG

            // if (String.IsNullOrEmpty(_ClientConnString)) ConnectionString = GetConnectionStringForClient("ArbProClient2_Dev");
#endif
            DataRisk dataObject = new DataRisk(this.ConnectionString, this._AuditUserName);
            List<RiskAerialRescuePlan> ReturnValue = null;

            try
            {
                this.CacheKey = this.CacheKeyPrefix("RiskAerialRescuePlanList");
                if (this.UseCache == false)
                {
                    this.RemoveFromCache(this.CacheKey);
                }

                if (this.UseCache)
                {
                    ReturnValue = (List<RiskAerialRescuePlan>)this.RetrieveFromCache();
                }

                if (ReturnValue == null)
                {
                    DataTable dt = dataObject.MobileAerialRescuePlanList().Tables[0];
                    ReturnValue = new List<RiskAerialRescuePlan>();
                    foreach (DataRow row in dt.Rows)
                    {
                        RiskAerialRescuePlan Plan = new RiskAerialRescuePlan();
                        Plan.PlanId = this.DBIntegerValue(row["ID"], 0);
                        Plan.Value = this.DBStringValue(row["AerialRescuePlan"], string.Empty);
                        ReturnValue.Add(Plan);
                    }

                    this.AddToCache(ReturnValue);
                }

                return ReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        #endregion

        #region Conversion Methods

        #region Risk Assessment

        #region Mobile Conversion

        private Risk ConvertToMobileRiskAssessment(Types.Risk riskObj)
        {
            Risk obj = new Model.Types.Risk();

            obj.Id = riskObj.Id;
            obj.ClientId = riskObj.ClientId;
            obj.AppointmentId = riskObj.AppointmentId;
            obj.JobSiteId = riskObj.JobSiteId;
            obj.AssessingStaffId = riskObj.AssessingStaffId;
            obj.AerialPlanId = riskObj.AerialPlanId;
            obj.TeamLeaderStaffId = riskObj.TeamLeaderStaffId;
            obj.CS38Certifed = riskObj.CS38Certifed;
            obj.AssessmentDate = riskObj.AssessmentDate;
            obj.WorkDate = riskObj.WorkDate;
            obj.CompletionDate = riskObj.CompletionDate;
            obj.NearestHospital = riskObj.NearestHospital;
            obj.ReferenceDoc = riskObj.ReferenceDoc;
            obj.MobileSignal = riskObj.MobileSignal;
            obj.SiteAccess = riskObj.SiteAccess;
            obj.GridReference = riskObj.GridReference;
            obj.SatisfactoryCompletion = riskObj.SatisfactoryCompletion;
            obj.ReferralRequired = riskObj.ReferralRequired;
            obj.ReferralCompleted = riskObj.ReferralCompleted;
            obj.ReferralComments = riskObj.ReferralComments;
            obj.AuditId = riskObj.AuditId;
            obj.Completed = riskObj.Completed;
            obj.RescueNomaniee = riskObj.RescueNomaniee;
            obj.RescuePlan = riskObj.RescuePlan;
            obj.LastUpdatedDate = riskObj.LastUpdatedDate;
            obj.LastUpdatedBy = riskObj.LastUpdatedBy;
            obj.ManagementComments = riskObj.ManagementComments;
            obj.AssessmentSections = this.ConvertToMobileRiskSections(riskObj.AssessmentSections);

            return obj;
        }

        private List<RiskSection> ConvertToMobileRiskSections(List<Types.RiskSection> riskSections)
        {
            List<RiskSection> retVal = new List<RiskSection>();
            foreach (Types.RiskSection riskObj in riskSections)
            {
                RiskSection obj = new RiskSection();

                obj.Id = riskObj.Id;
                obj.RiskAssessmentId = riskObj.RiskAssessmentId;
                obj.Title = riskObj.Title;
                obj.Description = riskObj.Description;
                obj.ReferenceDoc = riskObj.ReferenceDoc;
                obj.Chapter = riskObj.Chapter;
                obj.Comments = riskObj.Comments;
                obj.Criteria = this.ConvertToMobileRiskCriteria(riskObj.Criteria);

                retVal.Add(obj);
            }

            return retVal;
        }

        private List<RiskCriterion> ConvertToMobileRiskCriteria(List<Types.RiskCriterion> riskCriteria)
        {
            List<RiskCriterion> retVal = new List<RiskCriterion>();
            foreach (Types.RiskCriterion riskObj in riskCriteria)
            {
                RiskCriterion obj = new RiskCriterion();

                obj.Id = riskObj.Id;
                obj.RiskSectionId = riskObj.RiskSectionId;
                obj.Criterion = riskObj.Criterion;
                obj.ChapterRef = riskObj.ChapterRef;
                obj.AssessmentPhase = riskObj.AssessmentPhase;
                obj.ControlRequired = (int)riskObj.ControlRequired;
                obj.OnDayControlRequired = (int)riskObj.OnDayControlRequired;

                retVal.Add(obj);
            }

            return retVal;
        }

        #endregion

        #region Web Conversion

        private Types.Risk ConvertToWebRiskAssessment(Risk riskObj)
        {
            Types.Risk obj = new Types.Risk();

            obj.Id = riskObj.Id;
            obj.QuoteId = riskObj.QuoteId;
            obj.ClientId = riskObj.ClientId;
            obj.AppointmentId = riskObj.AppointmentId;
            obj.JobSiteId = riskObj.JobSiteId;
            obj.AssessingStaffId = riskObj.AssessingStaffId;
            obj.AssignedAerialStaffId = -1;
            obj.AerialPlanId = riskObj.AerialPlanId;
            obj.AerialPlan = riskObj.AerialPlan;
            obj.TeamLeaderStaffId = riskObj.TeamLeaderStaffId;
            obj.CS38Certifed = riskObj.CS38Certifed;
            if (riskObj.AssessmentDate == DateTime.MinValue)
            {
                riskObj.AssessmentDate = DateTime.Now;
            }

            obj.AssessmentDate = riskObj.AssessmentDate;
            if (riskObj.WorkDate == DateTime.MinValue)
            {
                riskObj.WorkDate = (DateTime)SqlDateTime.MinValue;
            }

            obj.WorkDate = riskObj.WorkDate;
            if (riskObj.CompletionDate == DateTime.MinValue)
            {
                riskObj.CompletionDate = (DateTime)SqlDateTime.MinValue;
            }

            obj.CompletionDate = riskObj.CompletionDate;
            obj.NearestHospital = this.DecodeSafeURL(riskObj.NearestHospital);
            obj.ReferenceDoc = riskObj.ReferenceDoc ?? string.Empty;
            obj.MobileSignal = this.DecodeSafeURL(riskObj.MobileSignal);
            obj.SiteAccess = this.DecodeSafeURL(riskObj.SiteAccess);
            obj.GridReference = this.DecodeSafeURL(riskObj.GridReference);
            obj.SatisfactoryCompletion = riskObj.SatisfactoryCompletion;
            obj.ReferralRequired = riskObj.ReferralRequired;
            obj.ReferralCompleted = riskObj.ReferralCompleted;
            obj.ReferralComments = riskObj.ReferralComments ?? string.Empty;
            obj.AuditId = riskObj.AuditId;
            obj.Completed = riskObj.Completed;
            obj.RescueNomaniee = this.DecodeSafeURL(riskObj.RescueNomaniee);
            obj.RescuePlan = riskObj.RescuePlan ?? string.Empty;
            obj.LastUpdatedDate = DateTime.Now;
            obj.LastUpdatedBy = riskObj.LastUpdatedBy ?? string.Empty;
            obj.ManagementComments = riskObj.ManagementComments ?? string.Empty;
            obj.AssessorSignatureImage = new byte[0];
            obj.AssessmentSections = this.ConvertToWebRiskSections(riskObj.AssessmentSections);

            return obj;
        }

        private List<Types.RiskSection> ConvertToWebRiskSections(List<RiskSection> riskSections)
        {
            List<Types.RiskSection> retVal = new List<Types.RiskSection>();
            foreach (RiskSection riskObj in riskSections)
            {
                Types.RiskSection obj = new Types.RiskSection();

                obj.Id = riskObj.Id;
                obj.RiskAssessmentId = riskObj.RiskAssessmentId;
                obj.Title = this.DecodeSafeURL(riskObj.Title);
                obj.Description = this.DecodeSafeURL(riskObj.Description);
                obj.ReferenceDoc = this.DecodeSafeURL(riskObj.ReferenceDoc);
                obj.Chapter = this.DecodeSafeURL(riskObj.Chapter);
                obj.Comments = this.DecodeSafeURL(riskObj.Comments);
                obj.Criteria = this.ConvertToWebRiskCriteria(riskObj.Criteria);

                retVal.Add(obj);
            }

            return retVal;
        }

        private List<Types.RiskCriterion> ConvertToWebRiskCriteria(List<RiskCriterion> riskCriteria)
        {
            List<Types.RiskCriterion> retVal = new List<Types.RiskCriterion>();
            foreach (RiskCriterion riskObj in riskCriteria)
            {
                Types.RiskCriterion obj = new Types.RiskCriterion();

                obj.Id = riskObj.Id;
                obj.RiskSectionId = riskObj.RiskSectionId;
                obj.Criterion = this.DecodeSafeURL(riskObj.Criterion);
                obj.ChapterRef = this.DecodeSafeURL(riskObj.ChapterRef);
                obj.AssessmentPhase = riskObj.AssessmentPhase;
                obj.ControlRequired = (Types.Enums.RiskCriterionStatusEnum)riskObj.ControlRequired;
                obj.OnDayControlRequired = (Types.Enums.RiskCriterionStatusEnum)riskObj.OnDayControlRequired;

                retVal.Add(obj);
            }

            return retVal;
        }

        #endregion

        #endregion

        #region Client

        #region Mobile Conversion

        /// <summary>
        /// The convert to mobile client list.
        /// </summary>
        /// <param name="objList">
        /// The web client list.
        /// </param>
        /// <returns>
        /// The mobile client list
        /// </returns>
        private List<Model.Types.Client> ConvertToMobileClientList(List<Types.Client> objList)
        {
            return objList.Select(this.ConvertToMobileClient).ToList();
        }

        /// <summary>
        /// The convert to mobile client single.
        /// </summary>
        /// <param name="obj">
        /// The web client object
        /// </param>
        /// <returns>
        /// The mobile client object
        /// </returns>
        private Model.Types.Client ConvertToMobileClient(Types.Client obj)
        {
            Model.Types.Client retVal = new Model.Types.Client();

            retVal.Id = obj.Id;
            retVal.AltAccountNo = obj.AltAccountNo;
            retVal.Title = obj.Title;
            retVal.Forename = obj.Forename;
            retVal.Surname = obj.Surname;
            retVal.BusinessName = obj.BusinessName;
            retVal.HouseNameNumber = obj.HouseNameNumber;
            retVal.Address1 = obj.Address1;
            retVal.Address2 = obj.Address2;
            retVal.Town = obj.Town;
            retVal.County = obj.County;
            retVal.PostCode = obj.PostCode;
            retVal.Sector = obj.Sector;
            retVal.Telephone = obj.Telephone;
            retVal.Mobile = obj.Mobile;
            retVal.Email = obj.Email;
            retVal.WebAddress = obj.WebAddress;
            retVal.AccountType = (int)obj.AccountType;
            retVal.DateOpened = obj.DateOpened;
            retVal.AddressLongitude = obj.AddressLongitude;
            retVal.AddressLatitude = obj.AddressLatitude;
            retVal.TermsAgreed = obj.TermsAgreed;
            retVal.SendSmsMessage = obj.SendSmsMessage;
            retVal.IncludeInMarketing = obj.IncludeInMarketing;

            return retVal;
        }

        #endregion

        #region Service Conversion

        /// <summary>
        /// The convert to web client list.
        /// </summary>
        /// <param name="objList">
        /// The mobile client list.
        /// </param>
        /// <returns>
        /// The web client list
        /// </returns>
        private List<Types.Client> ConvertToWebClientList(List<Model.Types.Client> objList)
        {
            return objList.Select(this.ConvertToWebClient).ToList();
        }

        /// <summary>
        /// The convert to web client single.
        /// </summary>
        /// <param name="obj">
        /// The mobile client object
        /// </param>
        /// <returns>
        /// The web client object
        /// </returns>
        private Types.Client ConvertToWebClient(Model.Types.Client obj)
        {
            Types.Client retVal = new Types.Client();

            retVal.Id = obj.Id;
            retVal.AltAccountNo = this.DecodeSafeURL(obj.AltAccountNo);
            retVal.Title = this.DecodeSafeURL(obj.Title);
            retVal.Forename = this.DecodeSafeURL(obj.Forename);
            retVal.Surname = this.DecodeSafeURL(obj.Surname);
            retVal.BusinessName = this.DecodeSafeURL(obj.BusinessName);
            retVal.HouseNameNumber = this.DecodeSafeURL(obj.HouseNameNumber);
            retVal.Address1 = this.DecodeSafeURL(obj.Address1);
            retVal.Address2 = this.DecodeSafeURL(obj.Address2);
            retVal.Town = this.DecodeSafeURL(obj.Town);
            retVal.County = this.DecodeSafeURL(obj.County);
            retVal.PostCode = this.DecodeSafeURL(obj.PostCode);
            retVal.Sector = this.DecodeSafeURL(obj.Sector);
            retVal.Telephone = this.DecodeSafeURL(obj.Telephone);
            retVal.Mobile = this.DecodeSafeURL(obj.Mobile);
            retVal.Email = this.DecodeSafeURL(obj.Email);
            retVal.WebAddress = this.DecodeSafeURL(obj.WebAddress);
            retVal.AccountType = (Types.Enums.ClientTypeEnum)obj.AccountType;
            retVal.AddressLongitude = obj.AddressLongitude;
            retVal.AddressLatitude = obj.AddressLatitude;
            retVal.TermsAgreed = obj.TermsAgreed;
            retVal.SendSmsMessage = obj.SendSmsMessage;
            retVal.IncludeInMarketing = obj.IncludeInMarketing;
            if (obj.DateOpened == DateTime.MinValue)
            {
                obj.DateOpened = (DateTime)SqlDateTime.MinValue;
            }

            retVal.DateOpened = obj.DateOpened;

            return retVal;
        }

        #endregion

        #endregion

        #endregion

        #region GetAcceptedQuote

        public List<AcceptedQuote> GetAcceptedQuoteList()
        {
            DataItem dataObject = new DataItem(this.ConnectionString, this._AuditUserName);
            List<AcceptedQuote> lstQuote = null;
            try
            {
                DataSet ds = dataObject.GetAcceptedJobPendingBookHeader();
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = dataObject.GetAcceptedJobPendingBookHeader().Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        lstQuote = new List<AcceptedQuote>();
                        foreach (DataRow row in dt.Rows)
                        {
                            var objAcceptedQuote = new AcceptedQuote();
                            objAcceptedQuote.QuoteId = this.DBIntegerValue(row["QuoteId"], 0);
                            objAcceptedQuote.SiteId = this.DBIntegerValue(row["SiteId"], 0);
                            objAcceptedQuote.WorkId = this.DBIntegerValue(row["Id"], 0);
                            objAcceptedQuote.ClientName = this.DBStringValue(row["ClientName"], string.Empty);
                            objAcceptedQuote.SiteAddress = this.DBStringValue(row["SiteAddress"], string.Empty);
                            objAcceptedQuote.Contactdtl = this.DBStringValue(row["Contactdtl"], string.Empty);
                            objAcceptedQuote.JobId = this.DBIntegerValue(row["ParentId"], 0);
                            objAcceptedQuote.Status = this.DBIntegerValue(row["QuoteStatus"], -1);
                            objAcceptedQuote.QuoteRefNo = this.DBStringValue(row["QuoteRefNo"], string.Empty);
                            objAcceptedQuote.CalendarWorkID = this.DBIntegerValue(row["CalendarWorkID"], -1);
                            lstQuote.Add(objAcceptedQuote);
                        }
                    }
                }
                else
                {
                    lstQuote = new List<AcceptedQuote>();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
            return lstQuote;
        }
        #endregion

        #region

        public bool AcceptQuote(int QuoteId)
        {
            DataQuote dataObject = new DataQuote(this.ConnectionString, this._AuditUserName);
            try
            {
                dataObject.QuoteAccept(QuoteId);
                return true;
            }
            catch(Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
                return false;
            }

        }
        #endregion

        #region BookingDetail

        public DGS.ArbPro.Service.Model.Operations.BookingHeader GetBookingDetail(int QuoteId, int JobId, int JobSiteId, int WorkId)
        {
            DataItem dataObject = new DataItem(this.ConnectionString, this._AuditUserName);
            DGS.ArbPro.Service.Model.Operations.BookingHeader objBookingDetail = new Model.Operations.BookingHeader();
            DataSet ds = dataObject.BookingHeaderDetail(QuoteId, JobId, JobSiteId);
            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dt = dataObject.BookingHeaderDetail(QuoteId, JobId, JobSiteId).Tables[0];
                foreach (DataRow row in dt.Rows)
                {
                    objBookingDetail.QuoteId = this.DBIntegerValue(row["QuoteId"], 0);
                    objBookingDetail.ClientName = this.DBStringValue(row["ClientName"], string.Empty);
                    objBookingDetail.SiteAddress = this.DBStringValue(row["SiteAddress"], string.Empty);
                    objBookingDetail.Telephone = this.DBStringValue(row["Telephone"], string.Empty);
                    objBookingDetail.Mobile = this.DBStringValue(row["Mobile"], string.Empty);
                    objBookingDetail.Email = this.DBStringValue(row["Email"], string.Empty);
                    objBookingDetail.SiteId = this.DBIntegerValue(row["SiteId"], 0);
                    objBookingDetail.SiteId = this.DBIntegerValue(row["SiteId"], 0);
                    objBookingDetail.QuoteRefNo = this.DBStringValue(row["QuoteRefNo"], string.Empty);
                }
                if (objBookingDetail != null)
                {
                    DataSet dsbook = dataObject.BookedWorkItem(WorkId);
                    if (dsbook != null && dsbook.Tables.Count > 0)
                    {
                        DataTable dtItem = dataObject.BookedWorkItem(WorkId).Tables[0];
                        objBookingDetail.BookedWorkItem = new List<DGS.ArbPro.Service.Model.Operations.BookedItem>();
                        foreach (DataRow row in dtItem.Rows)
                        {
                            var objItem = new DGS.ArbPro.Service.Model.Operations.BookedItem();
                            objItem.Id = this.DBIntegerValue(row["Id"], 0);
                            objItem.WorkId = this.DBIntegerValue(row["WorkId"], 0);
                            objItem.Code = this.DBStringValue(row["Code"], string.Empty);
                            objItem.Name = this.DBStringValue(row["Name"], string.Empty);
                            objItem.Descriptioin = this.DBStringValue(row["WorkDescription"], string.Empty);
                            objItem.Value = this.DBDecimalValue(row["Value"], 0);
                            objBookingDetail.BookedWorkItem.Add(objItem);
                        }
                    }
                    else
                    {
                        objBookingDetail.BookedWorkItem = null;
                    }
                }
            }
            else
            {
                objBookingDetail.QuoteId = -1;
                objBookingDetail.SiteAddress = "";
                objBookingDetail.ClientName = "";
                objBookingDetail.Email = "";
                objBookingDetail.Mobile = "";
                objBookingDetail.Telephone = "";
                objBookingDetail.BookedWorkItem = null;
                objBookingDetail.SiteId = -1;
            }
            return objBookingDetail;
        }

        //public void SaveBookWork(ArbPro.Model.Operations.BookingDetail BookingDetail)
        //{
        //    bool result = false;
        //    DataItem dataObject = new DataItem(this.ConnectionString, this._AuditUserName);
        //    try
        //    {
        //        foreach (var item in lstImage)
        //        {
        //            result = dataObject.SaveItemImage(item.ImageGuid, item.JobItemId, item.ParentId, (int)item.ParentItemType, item.ImageBytes);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));

        //    }
        //}

        public bool SaveImage(List<ItemImage> lstImage)
        {
            bool result = false;
            DataItem dataObject = new DataItem(this.ConnectionString, this._AuditUserName);
            try
            {
                foreach (var item in lstImage)
                {
                    result = dataObject.SaveItemImage(item.ImageGuid, item.JobItemId, item.ParentId, (int)item.ParentItemType, item.ImageBytes);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));

            }
            return result;
        }

        #endregion

        #region Mobile Web Service

        public List<DGS.ArbPro.Model.Operations.Tool> GetAllTool()
        {
            DataItem dataObject = new DataItem(this.ConnectionString, this._AuditUserName);
            List<DGS.ArbPro.Model.Operations.Tool> returnValue = new List<DGS.ArbPro.Model.Operations.Tool>();

            try
            {
                DataTable dt = dataObject.WA_Tools().Tables[0];

                foreach (DataRow row in dt.Rows)
                {
                    DGS.ArbPro.Model.Operations.Tool tool = new DGS.ArbPro.Model.Operations.Tool();
                    tool.Id = this.DBIntegerValue(row["Id"], 0);
                    tool.Name = this.DBStringValue(row["Name"], string.Empty);
                    tool.Stock = this.DBIntegerValue(row["Stock"], 0);
                    returnValue.Add(tool);
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }

        public List<AssociatedDocument> GetAllDocument(int ClientId, int JobId, int JobSiteId)
        {
            DataItem dataObject = new DataItem(this.ConnectionString, this._AuditUserName);
            List<AssociatedDocument> returnValue = new List<AssociatedDocument>();

            try
            {
                DataTable dt = dataObject.AW_AssociatedDocuments(ClientId, JobId, JobSiteId).Tables[0];

                foreach (DataRow row in dt.Rows)
                {
                    AssociatedDocument document = new AssociatedDocument();
                    document.Id = this.DBIntegerValue(row["Id"], 0);
                    document.ClientId = this.DBIntegerValue(row["ClientId"], 0);
                    document.JobId = this.DBIntegerValue(row["JobId"], 0);
                    document.JobSiteId = this.DBIntegerValue(row["JobSiteId"], 0);
                    document.ItemType = (DGS.ArbPro.Types.Enums.ReportTypeEnum)this.DBIntegerValue(row["ItemType"], 0);
                    document.DispatchMethod = (DGS.ArbPro.Types.Enums.DocDispatchMethodEnum)this.DBIntegerValue(row["DispatchMethod"], 0);
                    document.DateCreated = this.DBDateValue(row["DateCreated"], DateTime.Now);
                    document.Document = Encoding.Unicode.GetBytes(Convert.ToString(row["Document"]));
                    document.EmailReference = this.DBStringValue(row["EmailReference"], "");
                    returnValue.Add(document);
                }
                return returnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex));
            }
        }


        public void AddJobItem(AW_NewJobItem item)
        {
            log.DebugFormat("AddJobItem:parameters for JobItem (ID)={0} & UserCR= {1} are:", item.JobId);

            DataItem dataObject = new DataItem(this.ConnectionString, this._AuditUserName);

            try
            {
                log.DebugFormat(
                    "Add New Job Item:Item - parameters: JobId={0}, Type={1}, Value={2}, Description={3}, Presevation={4}",
                     item.JobId, item.JobSiteId, item.Value, item.Description);

                dataObject.AW_AddJobItem(item.JobSiteId, item.JobSiteId, item.TPOId, item.Value, item.VatRate, item.Code, item.Name, item.Description, item.Latitude, item.Longitude, item.ImageGuid);

                log.DebugFormat(
                    "AddJobITem:Item Added");

            }
            catch (Exception ex)
            {
                string strException = AppLogging.BuildExceptionMessage(this.CLASS_NAME, this.CacheKey, ex);

                // log.FatalFormat(strException);
                throw new Exception(strException);
            }
        }
        #endregion
    }
}