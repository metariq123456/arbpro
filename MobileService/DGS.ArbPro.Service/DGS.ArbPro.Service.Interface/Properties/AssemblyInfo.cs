﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DGS.ArbPro.Service")]
[assembly: AssemblyDescription("Mobile Service")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("ArbPro Software")]
[assembly: AssemblyProduct("DGS.ArbPro.Service")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("97d5f0c8-b2a0-48c2-b57a-dc393545fa2e")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("3.28.0.*")]
[assembly: AssemblyFileVersion("3.28.0.*")]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Log4Net.xml", Watch = true)]
