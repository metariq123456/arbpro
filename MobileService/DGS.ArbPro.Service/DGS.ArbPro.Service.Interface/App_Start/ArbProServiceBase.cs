﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using ServiceStack.ServiceInterface;

namespace DGS.ArbPro.Service.Interface
{
    [ServiceStack.ServiceInterface.Authenticate]
    public class ArbProServiceBase : ServiceStack.ServiceInterface.Service
    {
        private ILog _BaseLogger;
        public ILog BaseLogger
        {
            get { return _BaseLogger; }
        }

        private ILog _Logger;
        private ILog Logger
        {
            get { return _Logger; }
        }

        public ArbProServiceBase()
            : base()
        {
            _Logger = log4net.LogManager.GetLogger(this.GetType().Name);
            _BaseLogger = log4net.LogManager.GetLogger("ArbProService");
        }

        public string LogPrefix
        {
            get
            {
                try
                {
                    string clientId = ServiceExtensions.GetSession(this).Email;
                    string userId =  ServiceExtensions.GetSession(this).UserAuthId;
                    return string.Format("Client:{0} UserID: {1} - ", clientId, userId);
                }
                catch (Exception ex)
                {
                    _BaseLogger.WarnFormat("LogPrefix error: {0}", ex.ToString());
                    return "";
                }
            }
        }
    }
}