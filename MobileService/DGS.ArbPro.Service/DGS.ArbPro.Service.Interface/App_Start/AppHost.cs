﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections.Generic;
using ServiceStack.Configuration;
using ServiceStack.OrmLite;
using ServiceStack.ServiceInterface;
using ServiceStack.ServiceInterface.Auth;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.WebHost.Endpoints;
using log4net;
using Funq;
using DGS.ArbPro.Service.Interface.Operations;

[assembly: WebActivator.PreApplicationStartMethod(typeof(DGS.ArbPro.Service.Interface.AppHost), "Start")]

namespace DGS.ArbPro.Service.Interface
{
    public class AppHost : AppHostBase
    {
        private ILog Logger;
        public AppHost()
            : base("ArbProService", typeof(AppointmentMarkDownloaded).Assembly)
        {
            Logger = log4net.LogManager.GetLogger("ArbProService");
            Logger.Info("Service startup");
        }

        public override void Configure(Container container)
        {
            ServiceStack.Text.JsConfig.EmitCamelCaseNames = true;
            SetConfig(new EndpointHostConfig
            {
                DebugMode = true,
            });

            ConfigureAuth(container);
        }

        private void ConfigureAuth(Container container)
        {
            var AppSettings = new AppSettings();
            Plugins.Add(new AuthFeature(() => new AuthUserSession(), new IAuthProvider[] {
                new ArbProAuthProvider()
            }));
        }

        public static void Start()
        {
            new AppHost().Init();
        }
    }
}