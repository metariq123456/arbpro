﻿Imports System.Web.Caching
Imports System.Configuration

Public MustInherit Class AppCache
    Private Const CLASS_NAME As String = "AppCache::"
    Private mUseCacheMasterSetting As Boolean = False
    Private mUseCache As Boolean = False
    Private mCacheKey As String = "?"

    Public Sub New(ByVal UseCache As Boolean)
        Me.mUseCache = UseCache

        Try
            mUseCacheMasterSetting = (ConfigurationManager.AppSettings("UseCache").ToLower = "true")
        Catch ex As Exception
            mUseCacheMasterSetting = False
        End Try
    End Sub

    Protected Property UseCache() As Boolean
        Get
            Return mUseCache AndAlso mUseCacheMasterSetting
        End Get
        Set(value As Boolean)
            mUseCache = value
        End Set
    End Property
    Protected Property CacheKey() As String
        Get
            Return mCacheKey
        End Get
        Set(value As String)
            mCacheKey = value
        End Set
    End Property

    Protected Sub AddToCache(ByVal item As Object, ByVal AbsoluteExpirationMinutes As Integer)
        Dim c As System.Web.Caching.Cache = System.Web.HttpRuntime.Cache
        Dim container As Hashtable = Nothing
        Dim subKeys As String() = Nothing
        Try
            If UseCache = False Then Exit Sub

            subKeys = CacheKey.Split("~"c)
            container = TryCast(c.Item(subKeys(0)), Hashtable)
            If container Is Nothing Then
                container = New Hashtable
                c.Insert(subKeys(0), container, Nothing, Now.AddMinutes(20), Cache.NoSlidingExpiration)
            End If

            If container.ContainsKey(CacheKey) Then
                container.Remove(CacheKey)
            End If
            container.Add(CacheKey, item)
            c.Insert(CacheKey, item, Nothing, Now.AddMinutes(AbsoluteExpirationMinutes), Cache.NoSlidingExpiration)
        Catch ex As Exception
            Throw New Exception(CLASS_NAME & "AddToCache: " & ex.Message)
        End Try
    End Sub
    Protected Sub AddToCache(ByVal Item As Object)
        Dim c As System.Web.Caching.Cache = System.Web.HttpRuntime.Cache
        Try
            Me.AddToCache(Item, 20)
        Catch ex As Exception
            Throw New Exception(CLASS_NAME & "AddToCache: " & ex.Message)
        End Try
    End Sub
    Protected Sub RemoveFromCache(Key As String)
        Dim c As System.Web.Caching.Cache = System.Web.HttpRuntime.Cache
        Dim subKeys As String() = Nothing
        Try
            If mUseCacheMasterSetting = False Then Exit Sub
            c.Remove(Key.Replace("~", ""))
        Catch ex As Exception
        End Try
    End Sub
    Protected Function RetrieveFromCache() As Object
        Dim c As System.Web.Caching.Cache = System.Web.HttpRuntime.Cache
        Dim subKeys As String() = Nothing
        Dim container As Hashtable = Nothing
        Try
            If UseCache = False Then Return Nothing

            subKeys = CacheKey.Split("~"c)
            container = TryCast(c.Item(subKeys(0)), Hashtable)
            If Not container Is Nothing AndAlso subKeys.Length = 2 Then
                Return container(CacheKey)
            End If

            Return Nothing

        Catch ex As Exception
            Return Nothing
        End Try
    End Function
End Class
