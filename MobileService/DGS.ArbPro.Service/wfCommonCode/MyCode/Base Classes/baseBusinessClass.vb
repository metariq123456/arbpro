﻿Imports System.Configuration

Public Class baseBusinessClass
    Inherits AppCache

    Public Sub New(ByVal UseCache As Boolean)
        Me.New(UseCache, "MasterConnection")
    End Sub
    Public Sub New(ByVal UseCache As Boolean, ByVal DatabaseConnectionString As String)
        MyBase.New(UseCache)
        Try
            ConnectionString = ConfigurationManager.AppSettings(DatabaseConnectionString)
        Catch ex As Exception
            DatabaseConnectionString = ""
        End Try
    End Sub

    Private mConnectionString As String = String.Empty
    Protected Property ConnectionString() As String
        Get
            Return mConnectionString
        End Get
        Set(value As String)
            mConnectionString = value
        End Set
    End Property

    Public Function DBIntegerValue(ByVal Value As Object, ByVal DefaultValue As Integer) As Integer
        If Value Is Nothing OrElse Value Is DBNull.Value Then
            Return DefaultValue
        Else
            Try
                Return CInt(Value)
            Catch ex As Exception
                Return DefaultValue
            End Try
        End If
    End Function

    Public Function DBStringValue(ByVal Value As Object, ByVal DefaultValue As String) As String
        If Value Is Nothing OrElse Value Is DBNull.Value Then
            Return DefaultValue
        Else
            Try
                Return Value.ToString
            Catch ex As Exception
                Return DefaultValue
            End Try
        End If
    End Function

    Public Function DBBooleanValue(ByVal Value As Object, ByVal DefaultValue As Boolean) As Boolean
        If Value Is Nothing OrElse Value Is DBNull.Value Then
            Return DefaultValue
        Else
            Try
                Return CType(Value, Boolean)
            Catch ex As Exception
                Return DefaultValue
            End Try
        End If
    End Function

    Public Function DBDateValue(ByVal Value As Object, ByVal DefaultValue As Date) As Date
        If Value Is Nothing OrElse Value Is DBNull.Value Then
            Return DefaultValue
        Else
            Try
                Return CType(Value, Date)
            Catch ex As Exception
                Return DefaultValue
            End Try
        End If
    End Function

    Public Function DBDecimalValue(ByVal Value As Object, ByVal DefaultValue As Decimal) As Decimal
        If Value Is Nothing OrElse Value Is DBNull.Value Then
            Return DefaultValue
        Else
            Try
                Return CType(Value, Decimal)
            Catch ex As Exception
                Return DefaultValue
            End Try
        End If
    End Function

    Public Function IsPopulated(ByVal ds As DataSet, Optional ByVal TableIndex As Integer = 0) As Boolean
        If ds Is Nothing Then Return False
        Return IsPopulated(ds.Tables(TableIndex))
    End Function
    Public Function IsPopulated(ByVal dt As DataTable) As Boolean
        If dt Is Nothing Then Return False
        Return dt.Rows.Count > 0
    End Function

    Public Function CreateTransaction() As DistributedTransaction
        Dim tx As DistributedTransaction
        Try
            tx = New DistributedTransaction(ConnectionString)
            Return tx
        Catch ex As Exception
            Return Nothing
        End Try
    End Function
End Class
