﻿Imports Microsoft.VisualBasic
Imports System.Data

Public Class DistributedTransaction
    Implements IDisposable

    Private _happy As Boolean = True
    Private _done As Boolean = False
    Private _sqlTransaction As SqlClient.SqlTransaction
    Private _sqlConnection As SqlClient.SqlConnection
    Private _disposed As Boolean = False

    Public Sub New(ByVal ConnectionString As String)
        Try
            _sqlConnection = New SqlClient.SqlConnection(ConnectionString)
            OpenConnection()
        Catch ex As Exception
            CloseConnection()
        End Try
    End Sub

    Public ReadOnly Property SqlTransaction As SqlClient.SqlTransaction
        Get
            If _disposed Then Throw New ObjectDisposedException("Transaction")
            If _done Then Throw New InvalidOperationException("Transaction is closed and can no longer be used")
            Return _sqlTransaction
        End Get
    End Property

    Public Sub DisableCommit()
        If _disposed Then Throw New ObjectDisposedException("Transaction")
        'System.Diagnostics.Debug.WriteLine("DEBUG: 7.DisableCommit DistX Transaction")
        Me._happy = False
    End Sub
    Public Sub Commit()
        If _disposed Then Throw New ObjectDisposedException("SqlTransaction")

        If _done Then Throw New InvalidOperationException("Transaction has been committed/rolled back")

        If Not _happy Then Throw New InvalidOperationException("Transaction has commit disabled and cannot be committed")

        Try
            _sqlTransaction.Commit()
            'System.Diagnostics.Debug.WriteLine("DEBUG: 8.Commiting DistX Transaction")
        Catch ex As Exception
            Throw
        Finally
            _done = True
            CloseConnection()
        End Try
    End Sub
    Public Sub Abort()
        Try
            If _happy Then
                If _disposed Then Throw New ObjectDisposedException("SqlTransaction")

                If _done Then Throw New InvalidOperationException("Transaction has yet been committed/rolled back")

                _sqlTransaction.Rollback()
                'System.Diagnostics.Debug.WriteLine("DEBUG: 9.Aborting DistX Transaction")
                _happy = False
            End If
        Catch ex As Exception
            Throw
        Finally
            _done = True
            CloseConnection()
        End Try
    End Sub

    Public Sub Dispose() Implements System.IDisposable.Dispose
        Dispose(True)

        GC.SuppressFinalize(Me)
    End Sub
    Protected Overrides Sub Finalize()
        Dispose(False)
    End Sub
    Protected Sub Dispose(ByVal Disposing As Boolean)
        If _disposed Then Exit Sub

        If Disposing Then
            Try
                If Not _done Then
                    If Me._happy Then
                        _sqlTransaction.Commit()
                        'System.Diagnostics.Debug.WriteLine("DEBUG: Commiting DistX Transaction")
                    Else
                        _sqlTransaction.Rollback()
                        'System.Diagnostics.Debug.WriteLine("DEBUG: Rollback DistX Transaction")
                    End If
                End If
            Catch ex As Exception
            End Try

            Try
                CloseConnection()
            Catch ex As Exception
            End Try
        End If

        _disposed = True
    End Sub

    Private Sub OpenConnection()
        If Not _sqlConnection Is Nothing Then
            _sqlConnection.Open()
            'System.Diagnostics.Debug.WriteLine("DEBUG: 4.Opening DistX Connection")
            _sqlTransaction = _sqlConnection.BeginTransaction
            'System.Diagnostics.Debug.WriteLine("DEBUG: 6.Begin Transaction")
        End If
    End Sub
    Private Sub CloseConnection()
        If Not _sqlConnection Is Nothing AndAlso _sqlConnection.State = ConnectionState.Open Then
            _sqlConnection.Close()
            _sqlConnection.Dispose()
            'System.Diagnostics.Debug.WriteLine("DEBUG: 5.Closing DistX Connection")
        End If
        If Not _sqlTransaction Is Nothing Then
            _sqlTransaction.Dispose()
        End If
    End Sub
End Class
