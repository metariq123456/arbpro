﻿<Serializable()> _
Public MustInherit Class baseEntity

    Private mId As Integer = 0
    Private mValidationError As String = String.Empty
    Public ValidationErrors As New ArrayList
    Public DataArray As New ArrayList

    Public Enum enumInstance
        Insert
        Update
        Save 'both insert and update
        List
        Retreive
        Delete
    End Enum

    Public Property Id() As Integer
        Get
            Return mId
        End Get
        Set(ByVal value As Integer)
            mId = value
        End Set
    End Property

    Public MustOverride Function IsValid(ByVal Instance As enumInstance) As Boolean

    Protected Function DBValue(ByVal row As DataRow, ByVal ColumnName As String, ByVal DefaultValue As Integer) As Integer
        Return DBIntegerValue(row.Item(ColumnName), DefaultValue)
    End Function
    Protected Function DBValue(ByVal row As DataRow, ByVal ColumnName As String, ByVal DefaultValue As String) As String
        Return DBStringValue(row.Item(ColumnName), DefaultValue)
    End Function
    Protected Function DBValue(ByVal row As DataRow, ByVal ColumnName As String, ByVal DefaultValue As Boolean) As Boolean
        Return DBBooleanValue(row.Item(ColumnName), DefaultValue)
    End Function
    Protected Function DBValue(ByVal row As DataRow, ByVal ColumnName As String, ByVal DefaultValue As Date) As Date
        Return DBDateValue(row.Item(ColumnName), DefaultValue)
    End Function
    Protected Function DBValue(ByVal row As DataRow, ByVal ColumnName As String, ByVal DefaultValue As Decimal) As Decimal
        Return DBDecimalValue(row.Item(ColumnName), DefaultValue)
    End Function

    'Simple function to return an integer conversion of a database field value.
    'If the value is null, or there is an error in the conversion, then a 'default' integer value is returned
    Public Function DBIntegerValue(ByVal Value As Object, ByVal DefaultValue As Integer) As Integer
        If Value Is Nothing OrElse Value Is DBNull.Value Then
            Return DefaultValue
        Else
            Try
                Return CInt(Value)
            Catch ex As Exception
                Return DefaultValue
            End Try
        End If
    End Function
    'Simple function to return a string conversion of a database field value.
    'If the value is null, or there is an error in the conversion, then a 'default' string value is returned
    Public Function DBStringValue(ByVal Value As Object, ByVal DefaultValue As String) As String
        If Value Is Nothing OrElse Value Is DBNull.Value Then
            Return DefaultValue
        Else
            Try
                Return Value.ToString
            Catch ex As Exception
                Return DefaultValue
            End Try
        End If
    End Function
    'Simple function to return a boolean conversion of a database field value.
    'If the value is null, or there is an error in the conversion, then a 'default' boolean value is returned
    Public Function DBBooleanValue(ByVal Value As Object, ByVal DefaultValue As Boolean) As Boolean
        If Value Is Nothing OrElse Value Is DBNull.Value Then
            Return DefaultValue
        Else
            Try
                Return CType(Value, Boolean)
            Catch ex As Exception
                Return DefaultValue
            End Try
        End If
    End Function
    'Simple function to return a date conversion of a database field value.
    'If the value is null, or there is an error in the conversion, then a 'default' date value is returned
    Public Function DBDateValue(ByVal Value As Object, ByVal DefaultValue As Date) As Date
        If Value Is Nothing OrElse Value Is DBNull.Value Then
            Return DefaultValue
        Else
            Try
                Return CType(Value, Date)
            Catch ex As Exception
                Return DefaultValue
            End Try
        End If
    End Function
    'Simple function to return a deciaml conversion of a database field value.
    'If the value is null, or there is an error in the conversion, then a 'default' deciaml value is returned
    Public Function DBDecimalValue(ByVal Value As Object, ByVal DefaultValue As Decimal) As Decimal
        If Value Is Nothing OrElse Value Is DBNull.Value Then
            Return DefaultValue
        Else
            Try
                Return CType(Value, Decimal)
            Catch ex As Exception
                Return DefaultValue
            End Try
        End If
    End Function
End Class
