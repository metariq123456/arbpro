﻿Imports Microsoft.VisualBasic

Imports System
Imports System.Data
Imports System.Data.SqlClient

'Date            -  29/11/2007
'Author          -  Adrian Collins
'Description     -  This class is the base data access object.
'                -  It has various procedures for accessing the database.
'                -  It can be created with stand alone connection or as part of a distributed transaction.


Public MustInherit Class baseDataObject
    Private _ConnectionString As String
    Private _Parameters As New Collection
    Protected _Connection As SqlConnection
    Private _Command As SqlCommand
    Protected _distTransaction As DistributedTransaction = Nothing
    Protected _auditUserName As String = String.Empty

    Private Const DataBaseColumnListQuery As String = "SELECT TABLE_NAME, COLUMN_NAME, COLUMN_DEFAULT, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION, NUMERIC_SCALE" _
                                                & " FROM INFORMATION_SCHEMA.COLUMNS" _
                                                & " WHERE TABLE_NAME <> 'sysdiagrams'" _
                                                & " ORDER BY TABLE_NAME, ORDINAL_POSITION"

    Private Const DatabaseConstraintListQuery As String = "select kcu.TABLE_NAME, kcu.COLUMN_NAME, tc.CONSTRAINT_TYPE, kcu.ORDINAL_POSITION" _
                                                & " from INFORMATION_SCHEMA.TABLE_CONSTRAINTS as tc" _
                                                & " left join INFORMATION_SCHEMA.KEY_COLUMN_USAGE as kcu" _
                                                & " on kcu.CONSTRAINT_SCHEMA = tc.CONSTRAINT_SCHEMA" _
                                                & " and kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME" _
                                                & " and kcu.TABLE_SCHEMA = tc.TABLE_SCHEMA" _
                                                & " and kcu.TABLE_NAME = tc.TABLE_NAME" _
                                                & " where tc.CONSTRAINT_TYPE in ( 'PRIMARY KEY', 'UNIQUE' )" _
                                                & " and kcu.TABLE_NAME <> 'sysdiagrams'" _
                                                & " order by kcu.TABLE_SCHEMA, kcu.TABLE_NAME, tc.CONSTRAINT_TYPE, kcu.CONSTRAINT_NAME, kcu.ORDINAL_POSITION"

    Private Const DatabaseForeignKeyListQuery As String = "SELECT " & _
                                                    " KCU1.TABLE_NAME AS 'FK_TABLE_NAME' " & _
                                                    " , KCU1.COLUMN_NAME AS 'FK_COLUMN_NAME' " & _
                                                    " , KCU2.TABLE_NAME AS 'UQ_TABLE_NAME' " & _
                                                    " , KCU2.COLUMN_NAME AS 'UQ_COLUMN_NAME' " & _
                                                    " FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS RC " & _
                                                    " JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU1 " & _
                                                    " ON KCU1.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG  " & _
                                                    " AND KCU1.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA " & _
                                                    " AND KCU1.CONSTRAINT_NAME = RC.CONSTRAINT_NAME " & _
                                                    " JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU2 " & _
                                                    " ON KCU2.CONSTRAINT_CATALOG =  " & _
                                                    " RC.UNIQUE_CONSTRAINT_CATALOG  " & _
                                                    " AND KCU2.CONSTRAINT_SCHEMA =  " & _
                                                    " RC.UNIQUE_CONSTRAINT_SCHEMA " & _
                                                    " AND KCU2.CONSTRAINT_NAME =  " & _
                                                    " RC.UNIQUE_CONSTRAINT_NAME " & _
                                                    " AND KCU2.ORDINAL_POSITION = KCU1.ORDINAL_POSITION "

    Private Const DatabaseIdentityColumnListQuery As String = "select table_name, column_name" _
                                                & " from information_schema.columns where columnproperty(object_id(table_schema + '.' + table_name), " _
                                                & " column_name,'IsIdentity') = 1 and table_name <> 'sysdiagrams' order by table_name"

    Public Sub New(ByVal ConnectionString As String, AuditUserName As String)
        _ConnectionString = ConnectionString
        Dim logger As log4net.ILog = log4net.LogManager.GetLogger("ConnectionBase")
        _Connection = New SqlConnection(_ConnectionString)

        logger.DebugFormat("Connection string = {0}", _ConnectionString)
        _auditUserName = AuditUserName
    End Sub
    Public Sub New(ByVal transaction As DistributedTransaction, AuditUserName As String)
        _distTransaction = transaction
        _auditUserName = AuditUserName
    End Sub

    Protected Sub ClearParameters()
        'Clears down the parameters collection
        If Not _Parameters Is Nothing Then
            _Parameters = Nothing
            _Parameters = New Collection
        End If
    End Sub

    Protected Function ParametersCount() As Integer
        If _Parameters Is Nothing Then Return -1
        Return _Parameters.Count
    End Function

    Protected Sub OpenConnection()
        If Not _Connection Is Nothing Then
            _Connection.Open()
            'System.Diagnostics.Debug.WriteLine("Debug: 1.Opening Connection")
        End If
    End Sub
    Protected Sub CloseConnection(ByVal DisableTransaction As Boolean)
        If Not _Connection Is Nothing AndAlso _Connection.State = ConnectionState.Open Then
            _Connection.Close()
            'System.Diagnostics.Debug.WriteLine("Debug: 2.Closing Connection")
        End If
        If DisableTransaction AndAlso (Not _distTransaction Is Nothing) Then _distTransaction.DisableCommit()
    End Sub
    Protected Sub AddParameter(ByVal Name As String, ByVal Value As Integer, Optional ByVal NullConversion As Boolean = True, Optional ByVal Direction As ParameterDirection = ParameterDirection.Input)
        If NullConversion Then
            AddParameter(Name, SqlDbType.Int, 4, Direction, IntNull(Value))
        Else
            AddParameter(Name, SqlDbType.Int, 4, Direction, Value)
        End If
    End Sub
    Protected Sub AddParameter(ByVal Name As String, ByVal Value As Byte(), Optional ByVal NullConversion As Boolean = True, Optional ByVal Direction As ParameterDirection = ParameterDirection.Input)
        If NullConversion Then
            AddParameter(Name, SqlDbType.Image, Value.Length, Direction, ImageNull(Value))
        Else
            AddParameter(Name, SqlDbType.Image, Value.Length, Direction, Value)
        End If
    End Sub
    Protected Sub AddParameter(ByVal Name As String, ByVal Value As Date, Optional ByVal NullConversion As Boolean = True, Optional ByVal Direction As ParameterDirection = ParameterDirection.Input)
        If NullConversion Then
            AddParameter(Name, SqlDbType.DateTime, 8, Direction, DateNull(Value))
        Else
            AddParameter(Name, SqlDbType.DateTime, 8, Direction, Value)
        End If
    End Sub
    Protected Sub AddParameter(ByVal Name As String, ByVal Value As String, ByVal Length As Integer, Optional ByVal NullConversion As Boolean = True, Optional ByVal Direction As ParameterDirection = ParameterDirection.Input, Optional TrimToLength As Boolean = True)
        If TrimToLength AndAlso Not Value Is Nothing AndAlso Value.Length > Length AndAlso Value.Length > 0 Then Value = Value.Substring(0, Length - 1)
        If NullConversion Then
            AddParameter(Name, SqlDbType.VarChar, Length, Direction, StringNull(Value))
        Else
            AddParameter(Name, SqlDbType.VarChar, Length, Direction, Value)
        End If
    End Sub
    Protected Sub AddParameter(ByVal Name As String, ByVal Value As String, Optional ByVal Direction As ParameterDirection = ParameterDirection.Input)
        If Value Is Nothing Then
            AddParameter(Name, SqlDbType.VarChar, 0, Direction, StringNull(Value))
        Else
            AddParameter(Name, SqlDbType.VarChar, Value.Length, Direction, Value)
        End If
    End Sub
    Protected Sub AddParameter(ByVal Name As String, ByVal Value As Decimal, Optional ByVal NullConversion As Boolean = True, Optional ByVal Direction As ParameterDirection = ParameterDirection.Input)
        If NullConversion Then
            AddParameter(Name, SqlDbType.Decimal, 46, 38, 8, ParameterDirection.Input, DecimalNull(Value))
        Else
            AddParameter(Name, SqlDbType.Decimal, 46, 38, 8, ParameterDirection.Input, Value)
        End If
    End Sub
    Protected Sub AddParameter(ByVal Name As String, ByVal Value As Boolean)
        AddParameter(Name, SqlDbType.Bit, 1, ParameterDirection.Input, IIf(Value, 1, 0))
    End Sub
    Protected Sub AddParameter(ByVal Name As String, ByVal Value As Boolean?)
        If IsNothing(Value) Then
            AddParameter(Name, SqlDbType.Bit, 1, ParameterDirection.Input, DBNull.Value)
        Else
            AddParameter(Name, SqlDbType.Bit, 1, ParameterDirection.Input, IIf(CBool(Value), 1, 0))

        End If

    End Sub
    Protected Sub AddParameter(ByVal Name As String, ByVal SQLDataType As SqlDbType, ByVal Size As Integer, ByVal Direction As ParameterDirection, ByVal Value As Object)
        'Creates and Adds a parameter to the parameters collection
        If Name.StartsWith("@") = False Then Name = "@" & Name
        Dim Parameter As New SqlParameter(Name, SQLDataType, Size)
        Parameter.Direction = Direction
        If Not Direction = ParameterDirection.ReturnValue Then Parameter.Value = Value
        _Parameters.Add(Parameter, Name)
    End Sub
    Protected Sub AddParameter(ByVal Name As String, ByVal SQLDataType As SqlDbType, ByVal Size As Integer, ByVal Precission As Integer, ByVal Scale As Integer, ByVal Direction As ParameterDirection, ByVal Value As Object)
        'Creates and Adds a parameter to the parameters collection
        If Name.StartsWith("@") = False Then Name = "@" & Name
        Dim Parameter As New SqlParameter(Name, SQLDataType, Size)
        If SQLDataType = SqlDbType.Decimal Then
            Parameter.Precision = CByte(Precission)
            Parameter.Scale = CByte(Scale)
        End If
        Parameter.Direction = Direction
        If Not Direction = ParameterDirection.ReturnValue Then Parameter.Value = Value
        _Parameters.Add(Parameter, Name)
    End Sub

    Protected Sub AddParameter(ByRef param As SqlParameter)
        'Add a predefined parameter to the parameters collection
        If param.ParameterName.StartsWith("@") = False Then param.ParameterName = "@" & param.ParameterName
        _Parameters.Add(param, param.ParameterName)
    End Sub

    Protected Overloads Function RunProcedure(ByVal ProcedureName As String, ByRef RowsAffected As Integer) As Boolean
        'Runs a query and returns a boolean indicating the success/failure of the operation
        'Requires an integer value byref which passes the number of records affected by the query
        'Usually used for inserting/updating/deleting data
        Dim Command As SqlCommand
        Try
            OpenConnection()
            Command = BuildQueryCommand(ProcedureName)
            'System.Diagnostics.Debug.WriteLine("Debug: 3.Running Command " & Command.CommandText)
            RowsAffected = Command.ExecuteNonQuery()
            CloseConnection(False)
            Return True
        Catch ex As Exception
            CloseConnection(True)
            Command = Nothing
            Throw New Exception(ex.Message)
        End Try
    End Function

    Protected Overloads Function RunProcedure(ByVal ProcedureName As String) As SqlDataReader
        'Executes a stored procedure and returns a data reader containing the relevant data
        Dim dataReader As SqlDataReader
        Try
            OpenConnection()
            Dim Command As SqlCommand = BuildQueryCommand(ProcedureName)
            'System.Diagnostics.Debug.WriteLine("Debug: 3.Running Command " & Command.CommandText)
            dataReader = Command.ExecuteReader(CommandBehavior.CloseConnection)
            Return dataReader
        Catch ex As Exception
            CloseConnection(True)
            Throw New Exception("RunProcedure - " + ex.Message)
        End Try
    End Function

    Protected Overloads Function RunProcedure_KeepConnectionAlive(ByVal ProcedureName As String) As SqlDataReader
        Dim dataReader As SqlDataReader
        Try
            Try
                OpenConnection()
            Catch
            End Try
            Dim Command As SqlCommand = BuildQueryCommand(ProcedureName)
            'System.Diagnostics.Debug.WriteLine("Debug: 3.Running Command " & Command.CommandText)
            'dataReader = 
            Command.ExecuteNonQuery()
            Return Nothing 'dataReader

        Catch ex As Exception
            CloseConnection(True)
            Throw New Exception("RunProcedure - " + ex.Message)
        End Try
    End Function

    Protected Overloads Function RunProcedure(ByVal ProcedureName As String, ByVal TableName As String) As DataSet
        'Executes a stored procedure and returns a data set containing the relevant data

        Dim dataSet As DataSet = New DataSet
        Try
            OpenConnection()
            Dim dataAdaptor As SqlDataAdapter = New SqlDataAdapter
            dataAdaptor.SelectCommand = BuildQueryCommand(ProcedureName)
            'System.Diagnostics.Debug.WriteLine("Debug: 3.Running Command " & dataAdaptor.SelectCommand.CommandText)
            dataAdaptor.Fill(dataSet, TableName)
            CloseConnection(False)
            Return dataSet
        Catch ex As Exception
            CloseConnection(True)
            Throw New Exception("RunProcedure - " + TableName + ", " + ex.Message)
        End Try
    End Function

    Protected Overloads Sub RunProcedure(ByVal ProcedureName As String, ByRef dataSet As DataSet, ByVal TableName As String)
        'Executes a stored procedure and fills a dataset that is passed in

        Try
            OpenConnection()
            Dim dataAdaptor As SqlDataAdapter = New SqlDataAdapter
            dataAdaptor.SelectCommand = BuildQueryCommand(ProcedureName)
            'System.Diagnostics.Debug.WriteLine("Debug: 3.Running Command " & dataAdaptor.SelectCommand.CommandText)
            dataAdaptor.Fill(dataSet, TableName)
            CloseConnection(False)
        Catch ex As Exception
            CloseConnection(True)
            Throw New Exception(ex.Message)
        End Try
    End Sub

    Protected Overloads Function RunProcedureForXML(ByVal ProcedureName As String) As String
        'Executes a stored procedure and returns an XML string of the results set
        Dim xmlReader As Xml.XmlReader
        Dim retVal As String = ""
        Try
            OpenConnection()
            Dim Command As SqlCommand = BuildQueryCommand(ProcedureName)
            'System.Diagnostics.Debug.WriteLine("Debug: 3.Running Command " & Command.CommandText)
            xmlReader = Command.ExecuteXmlReader()
            xmlReader.Read()
            Do While Not xmlReader.EOF
                retVal += xmlReader.ReadOuterXml()
            Loop
            CloseConnection(False)
            Return retVal
        Catch ex As Exception
            CloseConnection(True)
            Throw New Exception(ex.Message)
        End Try
    End Function

    Protected Overloads Function RunSQL(ByVal SQL As String) As Integer
        Dim Command As SqlCommand
        Dim rowsAffected As Integer
        Dim sqlStatments() As String = SQL.Split(New String() {" GO "}, StringSplitOptions.RemoveEmptyEntries)

        Try
            For Each statement As String In sqlStatments
                OpenConnection()
                Command = BuildQueryCommand(statement, CommandType.Text)
                'System.Diagnostics.Debug.WriteLine("Debug: 3.Running Command " & Command.CommandText)
                rowsAffected = Command.ExecuteNonQuery()
                CloseConnection(False)
            Next
            Return rowsAffected
        Catch ex As Exception
            CloseConnection(True)
            Command = Nothing
            Throw New Exception(ex.Message)
        End Try
    End Function
    Protected Overloads Function RunSQL(ByVal SQL As String, ByVal TableName As String) As DataSet
        Dim dataSet As DataSet = New DataSet
        Try
            OpenConnection()
            Dim dataAdaptor As SqlDataAdapter = New SqlDataAdapter
            dataAdaptor.SelectCommand = BuildQueryCommand(SQL, CommandType.Text)
            'System.Diagnostics.Debug.WriteLine("Debug: 3.Running Command " & dataAdaptor.SelectCommand.CommandText)
            dataAdaptor.Fill(dataSet, TableName)
            CloseConnection(False)
            Return dataSet
        Catch ex As Exception
            CloseConnection(True)
            Throw New Exception(ex.Message)
        End Try
    End Function

    Protected ReadOnly Property ParameterValue(ByVal Name As String) As String
        'Returns the value of a parameter
        Get
            If Name.StartsWith("@") = False Then Name = "@" & Name
            Dim _sqlParam As SqlParameter = CType(_Parameters.Item(Name), SqlParameter)
            Return CType(_sqlParam.Value, String)
        End Get
    End Property

    Private Function BuildQueryCommand(ByVal ProcedureName As String, Optional ByVal CmdType As CommandType = CommandType.StoredProcedure) As SqlCommand
        'Builds a command object from the collection of parameters

        _Command = New SqlCommand

        If _distTransaction Is Nothing Then
            _Command.Connection = _Connection
            _Command.CommandText = ProcedureName
        Else
            _Command.Connection = _distTransaction.SqlTransaction.Connection
            _Command.CommandText = ProcedureName
            _Command.Transaction = _distTransaction.SqlTransaction
        End If
        _Command.CommandType = CmdType
        _Command.CommandTimeout = 60

        For Each Parameter As SqlParameter In _Parameters
            _Command.Parameters.Add(Parameter)
        Next

        Return _Command
    End Function

    Protected Function ColumnList() As DataSet
        Return RunSQL(DataBaseColumnListQuery, "DatabaseColumns")
    End Function
    Protected Function ConstraintList() As DataSet
        Return RunSQL(DatabaseConstraintListQuery, "DatabaseConstraints")
    End Function
    Protected Function IdentityList() As DataSet
        Return RunSQL(DatabaseIdentityColumnListQuery, "DatabaseIdentities")
    End Function
    Protected Function ForeignKeyList() As DataSet
        Return RunSQL(DatabaseForeignKeyListQuery, "DatabaseForeignKeys")
    End Function

    Protected Function DateSafe(ByVal DateToCheck As Date) As Date
        If DateToCheck < CDate("01/01/1800") Then
            Return CDate("01/01/1800")
        Else
            Return DateToCheck
        End If
    End Function
    Protected Function DateNull(ByVal DateToCheck As Date) As Object
        If DateToCheck < CDate("01/01/1900") Then
            Return DBNull.Value
        ElseIf DateToCheck > CDate("31/12/2051") Then
            Return DBNull.Value
        Else
            Return DateToCheck
        End If
    End Function
    Protected Function IntNull(ByVal IntToCheck As Integer, Optional ByVal MinValue As Integer = 1) As Object
        If IntToCheck < MinValue Then
            Return DBNull.Value
        Else
            Return IntToCheck
        End If
    End Function
    Protected Function ImageNull(ByVal ImageToCheck As Byte(), Optional ByVal MinValue As Integer = 1) As Object
        If ImageToCheck.Length < MinValue Then
            Return DBNull.Value
        Else
            Return ImageToCheck
        End If
    End Function
    Protected Function PosIntNull(ByVal IntToCheck As Integer) As Object
        If IntToCheck <= 0 Then
            Return DBNull.Value
        Else
            Return IntToCheck
        End If
    End Function
    Protected Function StringNull(ByVal StringToCheck As String) As Object
        If StringToCheck Is Nothing OrElse StringToCheck.Length = 0 Then
            Return DBNull.Value
        Else
            Return StringToCheck
        End If
    End Function
    Protected Function DecimalNull(ByVal DecimalToCheck As Decimal, Optional ByVal MinValue As Integer = 1) As Object
        If DecimalToCheck < MinValue Then
            Return DBNull.Value
        Else
            Return DecimalToCheck
        End If
    End Function

    Protected Function AddMinTime(value As Date) As Date
        Dim returnValue As Date = Date.MinValue
        If IsDate(value) AndAlso value > Date.MinValue Then
            returnValue = DateValue(value.ToShortDateString)
            returnValue = returnValue.AddSeconds(1)
        End If
        Return returnValue
    End Function
    Protected Function AddMaxTime(value As Date) As Date
        Dim returnValue As Date = Date.MinValue
        If IsDate(value) AndAlso value > Date.MinValue Then
            returnValue = DateValue(value.ToShortDateString)
            returnValue = returnValue.AddHours(23).AddMinutes(59).AddSeconds(59)
        End If
        Return returnValue
    End Function
End Class

