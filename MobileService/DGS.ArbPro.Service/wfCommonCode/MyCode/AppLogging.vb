﻿Public Module AppLogging
    Public Function LogError(ByVal PageName As String, ByVal MethodName As String, ByVal exp As Exception) As String
        'This is the generic error handler for the application.
        'This function should be called by (and only by) event handlers (i.e. page.load, button.click)
        'This function is the proxy for how the error message is dealt with at an application level (i.e. logging, emailing etc.)
        'It will pass back a formatted, user friendly message that should be displayed on the screen via the master page errormessage property
        'The message may be formatted differently if the application is in DevelopmentMode.

        Dim errRef As String = String.Empty
        Dim retVal As String = String.Empty
        Dim message As String = String.Empty
        Dim errorPriority As Integer = 0 'wsEventLogger.enumPriority.IsError
        Dim fatalPriority As Integer = 0 'wsEventLogger.enumPriority.IsFatal
        Try

            If TypeOf (exp) Is Threading.ThreadAbortException OrElse exp.Message.IndexOf("Thread was being aborted") > -1 Then
                'These errors can be ignored. They are caused by redirecting o another page from an event handler.
                'If the event did not have an error trapping mechanism these would be handled and ignored
                'by IIS. But because we handle all errors ourself we have to deal with this error type.
            Else
                'Create somekind of unique reference number for the user to specify.
                errRef = Guid.NewGuid.ToString.Substring(0, 7)

                'Create the generic to be returned if in dev mode.
                message = PageName & " - " & MethodName & " - " & exp.Message

                'Try and log the error to the event log.
                'If there is an error doing this append the error to the error message
                Try
                    If ConfigSetting(enumConfigValues.wfUSEEVENTLOG.ToString).ToLower = "true" Then
                        System.Diagnostics.EventLog.WriteEntry(ConfigSetting(enumConfigValues.wfSYSTEMNAME.ToString), message)
                    End If
                Catch ex As Exception
                    If retVal.Length > 0 Then retVal += "<br />"
                    If DevMode() = True Then retVal += "Unable to log error to event log: " & ex.Message
                End Try

                If message.Length > 0 Then
                    Try
                        'wsDevelopIT.wsLogMessage(DevelopITws.enumMessageType.Error, DevelopITws.enumMessagePriority.High, PageName, MethodName, message)
                    Catch ex As Exception
                        If retVal.Length > 0 Then retVal += "<br />"
                        If DevMode() = True Then retVal += "Unable to log error to web service: " & ex.Message
                    End Try
                End If

                'Set up the return message depending on who is using the application
                If DevMode() = True Then
                    If retVal.Length > 0 Then retVal += "<br />"
                    retVal += message
                Else
                    retVal = "An error has occured and logged with reference: " & errRef
                End If
            End If

        Catch ex As Exception
            'error here should be dealt with in a non-standard way (i.e. just logged) 
            'so as to not cause a cyclic error.
        End Try

        Return retVal
    End Function
    'Public Sub LogMessage(ByVal MessageType As DevelopITws.enumMessageType, ByVal MessagePriority As DevelopITws.enumMessagePriority, ByVal ModuleName As String, ByVal MethodName As String, ByVal MessageText As String)
    '    Try
    '        wsDevelopIT.wsLogMessage(MessageType, MessagePriority, ModuleName, MethodName, MessageText)
    '    Catch ex As Exception
    '    End Try
    'End Sub

    Public Function BuildExceptionMessage(ByVal FunctionName As String, ByVal exp As Exception) As String
        Dim returnValue As String = String.Empty
        Try
            Return BuildExceptionMessage("", FunctionName, exp)
        Catch ex As Exception
            returnValue = "Uknown Error!"
        End Try

        If returnValue.Length > 0 Then returnValue += ": "
        Return returnValue
    End Function
    Public Function BuildExceptionMessage(ByVal ModuleName As String, ByVal FunctionName As String, ByVal exp As Exception) As String
        Dim returnValue As String = String.Empty
        Try
            If ModuleName Is Nothing Then ModuleName = String.Empty
            returnValue = ModuleName

            If FunctionName Is Nothing Then FunctionName = String.Empty
            If returnValue.Length > 0 Then returnValue += ": "
            returnValue += FunctionName

            If Not exp Is Nothing Then
                If returnValue.Length > 0 Then returnValue += ": "

                returnValue += exp.Message
            End If
        Catch ex As Exception
            returnValue = "Uknown Error!"
        End Try

        If returnValue.Length > 0 Then returnValue += ": "
        Return returnValue
    End Function
End Module
