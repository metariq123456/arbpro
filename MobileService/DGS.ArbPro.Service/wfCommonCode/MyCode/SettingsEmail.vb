﻿Imports System.Net.Mail

Public Class SettingsEmail
    Public FromEmailAddress As String = String.Empty
    Public ToEmailAddress As String = String.Empty
    Public ReplyEmailAddress As String = String.Empty
    Public BccEmailAddress As String = String.Empty
    Public Subject As String = String.Empty
    Public Message As String = String.Empty
    Public Attachments As New ArrayList
    Public Priority As MailPriority = MailPriority.Normal
    Public IsHTML As Boolean = False
    Public Success As Boolean = False
End Class
