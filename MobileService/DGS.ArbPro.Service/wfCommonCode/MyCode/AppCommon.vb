﻿Imports System.Net.Mail

Public Enum enumConfigValues
    wfDEVMODE
    wfUSECACHE
    wfUSEEVENTLOG
    wfSYSTEMNAME
    wfTESTUSERS
    wfSYSTEMKEY
    wfSYSTEMENVIRONMENT
    wfREDIRECTEMAILSTO

End Enum

Public Module AppCommon
    Public Sub ConcatenateValidationError(ByRef errorString As String, ByVal ToConcatenate As String)
        Try
            If errorString.Length > 0 Then errorString += vbCrLf
            errorString += "* " & ToConcatenate
        Catch ex As Exception
        End Try
    End Sub




    Public Sub SetCtrlFocus(ByVal AppPage As System.Web.UI.Page, ByVal Ctrl As System.Web.UI.Control)
        Try
            SetCtrlFocus(AppPage, Ctrl.ClientID)
        Catch ex As Exception
            'Ignore any errors
        End Try
    End Sub
    Public Sub SetCtrlFocus(ByVal AppPage As System.Web.UI.Page, ByVal CtrlClientId As String)
        Try
            AppPage.ClientScript.RegisterStartupScript(AppPage.GetType, "SetFocus", "<script>document.getElementById('" & CtrlClientId & "').focus();</script>")
        Catch ex As Exception
            'Ignore any errors
        End Try
    End Sub
    Public Sub ScriptMessage(ByVal AppPage As System.Web.UI.Page, ByVal Message As String)
        Try
            Message = Message.Replace("'", "")
            AppPage.ClientScript.RegisterStartupScript(AppPage.GetType, "ScreenMessage", "<script>alert('" & Message & "');</script>")
        Catch ex As Exception
            'Ignore any errors
        End Try
    End Sub

    Public Function DevMode() As Boolean
        Try
            Return False 'ConfigSetting(enumConfigValues.wfDEVMODE.ToString).ToLower = "true" OrElse ManagerUser.ManualDevMode
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Function ConfigSetting(ByVal Key As String) As String
        'This is a simple function for retrieving values from the config file
        'The function requires the config value key
        Dim retVal As String = String.Empty
        Try
            retVal = System.Configuration.ConfigurationManager.AppSettings(Key)
            If retVal Is Nothing Then retVal = String.Empty
        Catch ex As Exception
            'An error within this function simply returns and empty value
            retVal = String.Empty
        Finally
        End Try
        Return retVal.Trim
    End Function
    Public Function IsPopulated(ByVal data As DataSet) As Boolean
        Return IsPopulated(data.Tables(0))
    End Function
    Public Function IsPopulated(ByVal data As DataTable) As Boolean
        If data Is Nothing Then Return False
        Return data.Rows.Count > 0
    End Function
    Public Function NothingToString(ByVal Value As Object, Optional ByVal DefaultValue As String = "") As String
        Try
            If Value Is Nothing Then
                Return DefaultValue
            Else
                Return Value.ToString
            End If
        Catch ex As Exception
            Return DefaultValue
        End Try
    End Function
    Public Function SendEmail(ByVal info As SettingsEmail) As Boolean
        Dim retval As Boolean = False
        Dim redirectEmail As String = String.Empty
        Dim redirectEmails As String()
        Dim email As New MailMessage
        Dim smtp As New SmtpClient
        Try
            redirectEmail = ConfigSetting(enumConfigValues.wfREDIRECTEMAILSTO.ToString)
            If redirectEmail.Length = 0 Then redirectEmail = info.ToEmailAddress

            If redirectEmail.Contains(";") Then
                redirectEmails = redirectEmail.Split(";"c)
                For Each redirectEmail In redirectEmails
                    If Not redirectEmail Is Nothing AndAlso redirectEmail.Trim.Length > 0 Then
                        email.To.Add(New MailAddress(redirectEmail))
                    End If
                Next
            Else
                email.To.Add(New MailAddress(redirectEmail))
            End If

            If info.ReplyEmailAddress.Trim.Length > 0 Then
                email.ReplyToList.Add(New MailAddress(info.ReplyEmailAddress))
            End If

            email.From = New MailAddress(info.FromEmailAddress)

            If info.BccEmailAddress.Trim.Length > 0 Then
                email.Bcc.Add(New MailAddress(redirectEmail))
            End If

            If info.IsHTML Then
                email.Body = info.Message.Replace(vbCrLf, "<br />")
                email.IsBodyHtml = info.IsHTML
            Else
                email.Body = info.Message
                email.IsBodyHtml = info.IsHTML
            End If

            email.Priority = info.Priority
            email.Subject = info.Subject

            smtp.Send(email)
            info.Success = True
            Return True
        Catch ex As Exception
            Debug.Print("SEND EMAIL: " & ex.Message)
            Return False
        Finally
            email = Nothing
            smtp = Nothing
        End Try
    End Function

    Public Function FormatDate(ByVal dateValue As String) As String
        Dim retVal As String = dateValue
        Try
            If IsDate(dateValue) Then
                retVal = FormatDate(CType(dateValue, Date))
            End If

            Return retVal
        Catch ex As Exception
            Return dateValue
        End Try
    End Function
    Public Function FormatDate(ByVal dateValue As Date) As String
        Dim retVal As String = ""

        If dateValue > Date.MinValue Then
            retVal = dateValue.ToShortDateString
        End If

        Return retVal
    End Function

    Public Function FormatDateTime(ByVal dateTimeValue As String) As String
        Dim theDate As String = String.Empty
        Dim theTime As String = String.Empty
        Try
            theDate = dateTimeValue.Split(" "c)(0)
            If IsDate(theDate) Then
                theDate = CType(theDate, Date).ToShortDateString
            End If

            theTime = dateTimeValue.Split(" "c)(1)
            If theTime.Length > 5 Then theTime = theTime.Substring(0, 5)

            Return theDate & " " & theTime
        Catch ex As Exception
            Return theDate
        End Try
    End Function
    Public Function DefaultDate(ByVal DateToCheck As Date) As String
        Try
            If DateToCheck < CDate("01/01/1900") Then
                Return String.Empty
            Else
                Return DateToCheck.ToShortDateString
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function
    Public Function DefaultDate(ByVal DateToCheck As String) As Date
        Try
            If IsDate(DateToCheck) AndAlso CDate(DateToCheck) < CDate("01/01/1900") Then
                Return Date.MinValue
            Else
                Return CDate(DateToCheck)
            End If
        Catch ex As Exception
            Return Date.MinValue
        End Try
    End Function
End Module

