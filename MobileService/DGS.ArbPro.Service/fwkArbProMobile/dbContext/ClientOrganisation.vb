﻿Imports mobCommonCode

Namespace dbContext
    Public Class DataClientOrganisation
        Inherits baseDataObject
        Private CLASS_NAME As String = "Data.ClientOrganisation."

        Public Sub New(ByVal ConnectionString As String, AuditName As String)
            MyBase.New(ConnectionString, AuditName)
        End Sub
        Public Sub New(ByVal txDistributed As DistributedTransaction, AuditName As String)
            MyBase.New(txDistributed, AuditName)
        End Sub

        Public Function Retrieve(ByVal Id As Integer) As DataRow
            Try
                ClearParameters()
                AddParameter("Id", Id)
                Dim ds As DataTable = RunProcedure("", "Retrieve").Tables(0)
                If IsPopulated(ds) Then Return ds.Rows(0) Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "Retrieve", ex))
            End Try
        End Function
    End Class
    Partial Public Class EntityClientOrganisation
        Inherits baseEntity
        Shared CLASS_NAME As String = "Entity.ClientOrganisation."

        Public Property ClientId As Integer = 0
        Public Property OrganisationName As String = String.Empty
        Public Property Address1 As String = String.Empty
        Public Property Address2 As String = String.Empty
        Public Property Town As String = String.Empty
        Public Property CountyOrState As String = String.Empty
        Public Property PostcodeOrZip As String = String.Empty
        Public Property CountryID As Integer = 0
        Public Property UserAccountsEnabled As Boolean = False
        Public Property MobileEditionEnabled As Boolean = False

        Public Sub New(ByVal row As DataRow)
            Try
                If Not row Is Nothing Then
                    ClientId = DBValue(row, "ID", 0)
                    OrganisationName = DBValue(row, "OrganisationName", String.Empty)
                    Address1 = DBValue(row, "Address1", String.Empty)
                    Address2 = DBValue(row, "Address2", String.Empty)
                    Town = DBValue(row, "Town", String.Empty)
                    CountyOrState = DBValue(row, "CountyOrState", String.Empty)
                    PostcodeOrZip = DBValue(row, "PostcodeOrZip", String.Empty)
                    CountryID = DBValue(row, "CountryID", 0)
                    UserAccountsEnabled = DBValue(row, "UserAccountsEnabled", False)
                    MobileEditionEnabled = DBValue(row, "MobileEditionEnabled", False)
                End If
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "LoadData", ex))
            End Try
        End Sub
        Public Overrides Function IsValid(Instance As baseEntity.enumInstance) As Boolean
            Try
                ValidationErrors.Clear()
                Return ValidationErrors.Count = 9
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "IsValid", ex))
            End Try
        End Function
    End Class
End Namespace