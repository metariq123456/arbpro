﻿Imports System.Configuration
Imports mobCommonCode

Namespace dbContext
    Public Class DataItem
        Inherits baseDataObject

        Private CLASS_NAME As String = "Data.Item."

        Public Sub New(ByVal ConnectionString As String, AuditUserName As String)
            MyBase.New(ConnectionString, AuditUserName)
        End Sub
        Public Sub New(ByVal txDistributed As DistributedTransaction, AuditName As String)
            MyBase.New(txDistributed, AuditName)
        End Sub

#Region "Mobile"
        Public Function MobileItemDescriptionList() As DataSet
            Try
                ClearParameters()
                Dim ds As DataSet = RunProcedure("MOB_ItemDescriptionList", "ItemDescriptionList")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileItemDescriptionList", ex))
            End Try
        End Function
        Public Function MobileItemTypeList() As DataSet
            Try
                ClearParameters()
                Dim ds As DataSet = RunProcedure("MOB_ItemTypeList", "ItemTypeList")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileItemTypeList", ex))
            End Try
        End Function
        Public Function MobileToolList() As DataSet
            Try
                ClearParameters()
                Dim ds As DataSet = RunProcedure("MOB_ToolList", "ToolList")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileToolList", ex))
            End Try
        End Function
        Public Function MobileToolListV3() As DataSet
            Try
                ClearParameters()
                Dim ds As DataSet = RunProcedure("MOB_ToolListV3", "ToolList")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileToolListV3", ex))
            End Try
        End Function
        Public Function BookingHeaderDetail(ByVal QuoteId As Integer, ByVal JobId As Integer, ByVal JobSiteId As Integer) As DataSet
            Try
                ClearParameters()

                AddParameter("QuoteId", QuoteId)                AddParameter("JobId", JobId)                AddParameter("JobSiteId", JobSiteId)
                Dim ds As DataSet = RunProcedure("BookDetail_GetBookDetailHeader", "BookingDetail")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "BookingHeaderDetail", ex))
            End Try
        End Function


        Public Function BookedWorkItem(ByVal WorkId As Integer) As DataSet
            Try
                ClearParameters()

                AddParameter("WorkId", WorkId)                Dim ds As DataSet = RunProcedure("BookDetail_GetBookedWorkItem", "BookedWorkItem")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "BookingHeaderDetail", ex))
            End Try
        End Function


        Public Function WA_Tools() As DataSet
            Try
                ClearParameters()
                Dim ds As DataSet = RunProcedure("AW_GetAllToolList", "ToolList")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "AW_Tools", ex))

            End Try
        End Function


        Public Function AW_AssociatedDocuments(ByVal ClientId As Integer, ByVal JobId As Integer, ByVal JobSiteId As Integer) As DataSet
            Try                ClearParameters()

                AddParameter("ClientId", ClientId)                AddParameter("JobId", JobId)                AddParameter("JobSiteId", JobSiteId)
                Dim ds As DataSet = RunProcedure("AW_GetAllDocument", "Documents")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "AW_Documents", ex))
            End Try
        End Function

        Public Function AW_AddJobItem(JobId As Integer, JobSiteId As Integer, TPOId As Integer, Value As Decimal, VatRate As Decimal,
                                 Code As String, Name As String, Description As String, Latitude As Decimal, Longitude As Decimal, ImageGuid As String)
            Try
                ClearParameters()
                AddParameter("JobId", JobId)
                AddParameter("JobSiteId", JobSiteId)
                AddParameter("TPOId", TPOId)
                AddParameter("Value", Value)
                AddParameter("VatRate", VatRate)
                AddParameter("Code", Code)
                AddParameter("Name", Name)
                AddParameter("Description", Description)
                AddParameter("Latitude", Latitude)
                AddParameter("Longitude", Longitude)
                AddParameter("ImageGuid", ImageGuid)
                RunProcedure_KeepConnectionAlive("AW_AddNewJobItem")

            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "Add New JobItem", ex))
            End Try
        End Function

        Public Function GetAcceptedJobPendingBookHeader() As DataSet
            Try
                ClearParameters()
                Dim ds As DataSet = RunProcedure("AcceptedJob_PendingBookHeader", "AcceptedQuote")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "GetAcceptedJobPendingBookHeader", ex))
            End Try
        End Function

        Public Function SaveItemImage(ImageGuid As String, JobItemId As Integer, ParentId As Integer, ParentItemType As Integer, Image As Byte) As Boolean
            Try
                ClearParameters()
                AddParameter("Guid", ImageGuid)
                AddParameter("JobItemId", JobItemId)
                AddParameter("ParentId", ParentId)
                AddParameter("ParentItemType", ParentItemType)
                AddParameter("Image", Image)
                RunProcedure_KeepConnectionAlive("ItemPicture_Insert")
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function


#End Region
    End Class
End Namespace

