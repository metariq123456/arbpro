﻿Imports System.Configuration
Imports mobCommonCode

Namespace dbContext
    Partial Public Class DataCalendarItem
        Inherits baseDataObject
        Private CLASS_NAME As String = "Data.CalendarItem."

        Public Sub New(ByVal ConnectionString As String, AuditName As String)
            MyBase.New(ConnectionString, AuditName)
        End Sub
        Public Sub New(ByVal txDistributed As DistributedTransaction, AuditName As String)
            MyBase.New(txDistributed, AuditName)
        End Sub

#Region "Mobile"
        Public Function MobileDownloadQuoteListForUser(ByVal userId As Integer, ByVal fromDateTime As DateTime, ByVal toDateTime As DateTime) As DataSet
            Try
                ClearParameters()
                AddParameter("UserId", userId)
                AddParameter("StartDate", fromDateTime)
                AddParameter("EndDate", toDateTime)

                Dim ds As DataSet = RunProcedure("MOB_DownloadCalendarQuoteItemListForUser", "DownloadCalendarQuoteItemListForUser")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "DownloadCalendarQuoteItemListForUser", ex))
            End Try
        End Function

        Public Function MobileDownloadWorkItemList(ByVal fromDateTime As DateTime, ByVal toDateTime As DateTime) As DataSet
            Try
                ClearParameters()
                AddParameter("StartDate", fromDateTime)
                AddParameter("EndDate", toDateTime)
                Dim ds As DataSet = RunProcedure("MOB_DownloadCalendarWorkItemList", "DownloadCalendarWorkItemList")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "DownloadCalendarWorkItemListForUser", ex))
            End Try
        End Function

        Public Function MobileDownloadGenericListForUser(ByVal userId As Integer, ByVal fromDateTime As DateTime, ByVal toDateTime As DateTime) As DataSet
            Try
                ClearParameters()
                AddParameter("UserId", userId)
                AddParameter("StartDate", fromDateTime)
                AddParameter("EndDate", toDateTime)
                Dim ds As DataSet = RunProcedure("MOB_DownloadCalendarGenericItemListForUser", "DownloadCalendarGenericItemListForUser")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "DownloadCalendarGenericItemListForUser", ex))
            End Try
        End Function
#End Region
    End Class
   
End Namespace
