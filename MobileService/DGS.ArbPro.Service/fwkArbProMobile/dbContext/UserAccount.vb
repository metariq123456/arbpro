﻿Imports mobCommonCode
Imports System.Security.Cryptography

Namespace dbContext
    Public Class DataUserAccount
        Inherits baseDataObject
        Private CLASS_NAME As String = "Data.UserAccount."

        Public Sub New(ByVal ConnectionString As String, AuditName As String)
            MyBase.New(ConnectionString, AuditName)
        End Sub
        Public Sub New(ByVal txDistributed As DistributedTransaction, AuditName As String)
            MyBase.New(txDistributed, AuditName)
        End Sub

        Public Function Retrieve(ByVal Username As String, Password As String) As DataRow
            Try
                'This modification of the mobile service is to enable the current mobile version to authenticate
                'against a version 3 arb pro user account
                ClearParameters()
                AddParameter("EmailAddress", Username, Username.Length)
                Dim hash As String = VersionThreeHash(Password)
                AddParameter("Password", Password, hash.Length)
                'Dim ds As DataTable = RunProcedure("uspUserAccountMobileLogin", "Retrieve").Tables(0)
                Dim ds As DataTable = RunProcedure("ArbMobile_ValidateUser", "Retrieve").Tables(0)
                If IsPopulated(ds) Then Return ds.Rows(0) Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "Retrieve", ex))
            End Try
        End Function
        Public Function Retrieve(ByVal UserName As String) As DataRow
            Try
                ClearParameters()
                AddParameter("UserName", UserName, 255)
                Dim ds As DataTable = RunProcedure("MOB_GetUserAccount", "Retrieve").Tables(0)
                If IsPopulated(ds) Then Return ds.Rows(0) Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "Retrieve", ex))
            End Try
        End Function
#Region "Mobile"
        Public Function MobileGetUser(ByVal Username As String, Password As String) As DataRow
            Try
                ClearParameters()
                AddParameter("Username", Username, Username.Length)
                AddParameter("Password", Password, Password.Length)
                Dim ds As DataTable = RunProcedure("MOB_GetUserAccount", "MobileGetUser").Tables(0)
                If IsPopulated(ds) Then Return ds.Rows(0) Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileGetUser", ex))
            End Try
        End Function
#End Region

        Private Function VersionThreeHash(ByVal password As String) As String
            ' Create a new instance of the hash crypto service provider.
            Dim hashAlg As New SHA256CryptoServiceProvider()

            Dim salt As String = ""
            ' Convert the data to hash to an array of Bytes - note we're using the salt here too
            Dim bytValue() As Byte
            bytValue = System.Text.Encoding.UTF8.GetBytes(password + salt)

            ' Compute the Hash. This returns an array of Bytes.
            Dim bytHash() As Byte = hashAlg.ComputeHash(bytValue)

            ' Convert the hash value to a base64-encoded string, 
            Dim base64 As String = Convert.ToBase64String(bytHash)

            Return base64
        End Function
    End Class
    Partial Public Class EntityUserAccount
        Inherits baseEntity
        Shared CLASS_NAME As String = "Entity.UserAccount."

        Public Property UserId As Integer = 0
        Public Property ClientOrganisation As Integer = 0
        Public Property EmailAddress As String = String.Empty
        Public Property DatabaseString As String = String.Empty
        Public Property DisplayName As String = String.Empty

        Public Sub New(ByVal row As DataRow)
            Try
                If Not row Is Nothing Then
                    UserId = DBValue(row, "UserAccountID", 0)
                    ClientOrganisation = DBValue(row, "ClientOrganisationID", 0)
                    EmailAddress = DBValue(row, "EmailAddress", String.Empty)
                    DatabaseString = DBValue(row, "ClientDatabaseName", String.Empty)
                    DisplayName = DBValue(row, "DisplayName", String.Empty)
                End If
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "LoadData", ex))
            End Try
        End Sub
        Public Shared Function LoadFromTable(ByVal table As DataTable) As List(Of EntityUserAccount)
            Try
                Dim list As New List(Of EntityUserAccount)
                If Not table Is Nothing Then
                    For Each row As DataRow In table.Rows
                        list.Add(New EntityUserAccount(row))
                    Next
                End If
                Return list
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "LoadFromTable", ex))
            End Try
        End Function
        Public Overrides Function IsValid(Instance As baseEntity.enumInstance) As Boolean
            Try
                ValidationErrors.Clear()
                Return ValidationErrors.Count = 0
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "IsValid", ex))
            End Try
        End Function

    End Class
End Namespace