﻿Imports System.Configuration
Imports mobCommonCode

Namespace dbContext
    Public Class DataRisk
        Inherits baseDataObject
        Private CLASS_NAME As String = "Data.Risk."

#Region "Constructors"
        Public Sub New(ByVal ConnectionString As String, AuditUserName As String)
            MyBase.New(ConnectionString, AuditUserName)
        End Sub
#End Region

        Public Function MobileAerialRescuePlanList() As DataSet
            Try
                ClearParameters()
                Dim ds As DataSet = RunProcedure("RiskAerialRescuePlanList", "RiskAerialRescuePlanList")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileAerialRescuePlanList", ex))
            End Try
        End Function

        Public Function MobileHospitalList() As DataSet
            Try
                ClearParameters()
                Dim ds As DataSet = RunProcedure("RiskHospitalList", "RiskHospitalList")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileHospitalList", ex))
            End Try
        End Function
        Public Function MobileRiskTemplateList() As DataSet
            Try
                ClearParameters()
                Dim ds As DataSet = RunProcedure("Template_Risk_GetAll", "RiskTemplateList")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileRiskSectionList", ex))
            End Try
        End Function

        Public Function MobileRiskSectionForRiskId(ByVal riskId As Integer) As DataSet
            Try
                ClearParameters()

                AddParameter("TemplateId", riskId)
                Dim ds As DataSet = RunProcedure("Template_RiskSection_GetForRiskID", "MobileRiskSectionForRiskId")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileRiskSectionList", ex))
            End Try
        End Function

        Public Function MobileRiskCriteriaForRiskId(ByVal riskId As Integer, ByVal sectionId As Integer) As DataSet
            Try
                ClearParameters()

                AddParameter("TemplateId", riskId)
                AddParameter("SectionId", sectionId)
                Dim ds As DataSet = RunProcedure("Template_RiskCriterion_GetForRisk", "MobileRiskCriteriaForRiskId")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileRiskSectionList", ex))
            End Try
        End Function
        Public Function MobileRiskSectionList() As DataSet
            Try
                ClearParameters()
                Dim ds As DataSet = RunProcedure("RiskReferenceList", "RiskSectionList")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileRiskSectionList", ex))
            End Try
        End Function

        Public Sub MobileAdditionalRisksInsert(ByVal QuoteNo As Integer, ByVal AssessedRisk As String, ByVal Control As String)
            Try
                ClearParameters()
                AddParameter("QuoteNo", QuoteNo)
                AddParameter("AssessedRisk", AssessedRisk, ParameterDirection.Input)
                AddParameter("ControlMeasures", Control, ParameterDirection.Input)
                RunProcedure_KeepConnectionAlive("RiskAdditionalRiskInsert")
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileAdditionalRisks", ex))
            End Try
        End Sub

        Public Sub MobileRiskSectionCriterionUpdateV3(ByVal assessmentId As Integer, ByVal sectionTemplateId As Integer, ByVal criterionName As String, ByVal value As Boolean)
            Try
                ClearParameters()
                AddParameter("AssessmentId", assessmentId)
                AddParameter("SectionTemplateId", sectionTemplateId)
                AddParameter("CriterionName", criterionName)

                AddParameter("Value", value)
                RunProcedure_KeepConnectionAlive("Mob_RiskCriterionUpdate")
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "Mob_RiskCriterionUpdate", ex))
            End Try
        End Sub
        Public Function MobileQuoteGetOrCreate(ByVal appointmentNo As Integer) As DataSet
            Try
                ClearParameters()
                AddParameter("Appointment", appointmentNo)
                Dim ds As DataSet = RunProcedure("Mob_Quote_GetOrCreate", "MobileQuoteGetOrCreate")                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "Mob_Quote_GetOrCreate", ex))
            End Try
        End Function

        Public Sub MobileRiskAssessmentUpdate(ByVal QuoteNo As Integer, ByVal UserId As Integer, ByVal HospitalId As Integer, ByVal AerialRescueID As Integer, ByVal MobileSignalLevel As String, ByVal GridRef As String, ByVal SiteAccess As String, ByVal RescuePlan As String, ByVal CompletedOn As DateTime, ByVal ControlRequired As Dictionary(Of String, Boolean), ByVal GenericChapter As Dictionary(Of String, String), ByVal SectionNotes As Dictionary(Of String, String))
            Try
                ClearParameters()

                AddParameter("QuoteValueNo", QuoteNo)
                AddParameter("RiskEmergencyNearestAandE", HospitalId)
                AddParameter("RiskEmergencyArialRescuePlan", AerialRescueID)
                AddParameter("RiskAssessmentCompletedBy", UserId) 'This value is now the user ID of the session user and needs to be transformed back to the Id of the staff member
                AddParameter("RiskEmergencyMobilePhoneSignal", MobileSignalLevel, ParameterDirection.Input)
                AddParameter("RiskEmergencyRescuePlan", RescuePlan, ParameterDirection.Input)
                AddParameter("RiskEmergencySiteAccessForRescue", SiteAccess, ParameterDirection.Input)
                AddParameter("RiskDateQuote", CompletedOn)
                AddParameter("RiskSiteToPublic", ControlRequired.Item("RiskSiteToPublic"))
                AddParameter("RiskSiteTrafficManagement", ControlRequired.Item("RiskSiteTrafficManagement"))
                AddParameter("RiskSiteOverheadLinesUndergroundServices", ControlRequired.Item("RiskSiteOverheadLinesUndergroundServices"))
                AddParameter("RiskSiteBuildingOrObstructions", ControlRequired.Item("RiskSiteBuildingOrObstructions"))
                AddParameter("RiskSiteSensitiveEmotionallyCharged", ControlRequired.Item("RiskSiteSensitiveEmotionallyCharged"))
                AddParameter("RiskEnvironmentalWaterCourse", ControlRequired.Item("RiskEnvironmentalWaterCourse"))
                AddParameter("RiskEnvironmentalProtectedSpeciesHabitat", ControlRequired.Item("RiskEnvironmentalProtectedSpeciesHabitat"))
                AddParameter("RiskEnvironmentalNoiseDust", ControlRequired.Item("RiskEnvironmentalNoiseDust"))
                AddParameter("RiskOperationsChainSaw", ControlRequired.Item("RiskOperationsChainSaw"))
                AddParameter("RiskOperationsTreeClimbing", ControlRequired.Item("RiskOperationsTreeClimbing"))
                AddParameter("RiskOperationsChainsawInTree", ControlRequired.Item("RiskOperationsChainsawInTree"))
                AddParameter("RiskOperationsBranchRemoval", ControlRequired.Item("RiskOperationsBranchRemoval"))
                AddParameter("RiskOperationsLoweringTechniquesNecessary", ControlRequired.Item("RiskOperationsLoweringTechniquesNecessary"))
                AddParameter("RiskOperationsTreeFelling", ControlRequired.Item("RiskOperationsTreeFelling"))
                AddParameter("RiskOperationsDeLimbingAndCrossCuting", ControlRequired.Item("RiskOperationsDe-LimbingAndCrossCuting"))
                AddParameter("RiskOperationsStormDamage", ControlRequired.Item("RiskOperationsStormDamage"))
                AddParameter("RiskOperationsChemicalApplication", ControlRequired.Item("RiskOperationsChemicalApplication"))
                AddParameter("RiskMachineryBrushWoodChipper", ControlRequired.Item("RiskMachineryBrushWoodChipper"))
                AddParameter("RiskMachineryStumpGrinder", ControlRequired.Item("RiskMachineryStumpGrinder"))
                AddParameter("RiskMachineryWinch", ControlRequired.Item("RiskMachineryWinch"))
                AddParameter("RiskMachineryBrushCutterClearingSaw", ControlRequired.Item("RiskMachineryBrushCutterClearingSaw"))
                AddParameter("RiskMachineryHedgeTrimmers", ControlRequired.Item("RiskMachineryHedgeTrimmers"))
                AddParameter("RiskMachineryMechanisedPolePrunner", ControlRequired.Item("RiskMachineryMechanisedPolePrunner"))
                AddParameter("RiskMachineryMobileElevatedWorkPlatform", ControlRequired.Item("RiskMachineryMobileElevatedWorkPlatform"))
                AddParameter("RiskMachineryCrane", ControlRequired.Item("RiskMachineryCrane"))
                AddParameter("RiskMachineryMountedHydraulicLoadingArm", ControlRequired.Item("RiskMachineryMountedHydraulicLoadingArm"))
                AddParameter("RiskMachineryOther", ControlRequired.Item("RiskMachineryOther"))
                AddParameter("RiskWorkingAtHeightMewp", ControlRequired.Item("RiskWorkingAtHeightMewp"))
                AddParameter("RiskWorkingAtHeightIsItCostEffective", ControlRequired.Item("RiskWorkingAtHeightIsItCostEffective"))
                AddParameter("RiskWorkingAtHeightLadder", ControlRequired.Item("RiskWorkingAtHeightLadder"))
                AddParameter("RiskWorkingAtHeightFallingObjects", ControlRequired.Item("RiskWorkingAtHeightFallingObjects"))
                AddParameter("RiskOperationsOther", ControlRequired.Item("RiskOperationsOther"))
                AddParameter("RiskWorkingAtHeightRopeandHarness", ControlRequired.Item("RiskWorking@HeightRopeandHarness"))
                AddParameter("RiskWorkingAtHeightCompletedFromGround", ControlRequired.Item("RiskWorking@HeightCompletedFromGround"))
                AddParameter("RiskWorkingAtHeightSafeToClimb", ControlRequired.Item("RiskWorking@HeightSafeToClimb"))
                AddParameter("RiskWorkingAtHeightUsePoleSaw", ControlRequired.Item("RiskWorking@HeightUsePoleSaw"))
                AddParameter("RiskWorkingAtHeightStraightFell", ControlRequired.Item("RiskWorkingAtHeightStraightFell"))
                AddParameter("RiskOpperationsOther", ControlRequired.Item("RiskOpperationsOther"))
                AddParameter("RiskSiteRoadsFootpathsPublicAreas", ControlRequired.Item("RiskSiteRoadsFootpathsPublicAreas"))
                AddParameter("RiskSiteToPublicRef", GenericChapter.Item("RiskSiteToPublicRef"), 20)
                AddParameter("RiskSiteTrafficManagementRef", GenericChapter.Item("RiskSiteTrafficManagementRef"), 20)
                AddParameter("RiskSiteOverheadLinesUndergroundServicesRef", GenericChapter.Item("RiskSiteOverheadLinesUndergroundServicesRef"), 20)
                AddParameter("RiskSiteBuildingOrObstructionsRef", GenericChapter.Item("RiskSiteBuildingOrObstructionsRef"), 20)
                AddParameter("RiskSiteSensitiveEmotionallyChargedRef", GenericChapter.Item("RiskSiteSensitiveEmotionallyChargedRef"), 20)
                AddParameter("RiskEnvironmentalWaterCourseRef", GenericChapter.Item("RiskEnvironmentalWaterCourseRef"), 20)
                AddParameter("RiskEnvironmentalProtectedSpeciesHabitatRef", GenericChapter.Item("RiskEnvironmentalProtectedSpeciesHabitatRef"), 20)
                AddParameter("RiskEnvironmentalNoiseDustRef", GenericChapter.Item("RiskEnvironmentalNoiseDustRef"), 20)
                AddParameter("RiskOperationsChainSawRef", GenericChapter.Item("RiskOperationsChainSawRef"), 20)
                AddParameter("RiskOperationsTreeClimbingRef", GenericChapter.Item("RiskOperationsTreeClimbingRef"), 20)
                AddParameter("RiskOperationsChainsawInTreeRef", GenericChapter.Item("RiskOperationsChainsawInTreeRef"), 20)
                AddParameter("RiskOperationsBranchRemovalRef", GenericChapter.Item("RiskOperationsBranchRemovalRef"), 20)
                AddParameter("RiskOperationsLoweringTechniquesNecessaryRef", GenericChapter.Item("RiskOperationsLoweringTechniquesNecessaryRef"), 20)
                AddParameter("RiskOperationsTreeFellingRef", GenericChapter.Item("RiskOperationsTreeFellingRef"), 20)
                AddParameter("RiskOperationsDeLimbingAndCrossCutingRef", GenericChapter.Item("RiskOperationsDe-LimbingAndCrossCutingRef"), 20)
                AddParameter("RiskOperationsStormDamageRef", GenericChapter.Item("RiskOperationsStormDamageRef"), 20)
                AddParameter("RiskOperationsChemicalApplicationRef", GenericChapter.Item("RiskOperationsChemicalApplicationRef"), 20)
                AddParameter("RiskMachineryBrushWoodChipperRef", GenericChapter.Item("RiskMachineryBrushWoodChipperRef"), 20)
                AddParameter("RiskMachineryStumpGrinderRef", GenericChapter.Item("RiskMachineryStumpGrinderRef"), 20)
                AddParameter("RiskMachineryWinchRef", GenericChapter.Item("RiskMachineryWinchRef"), 20)
                AddParameter("RiskMachineryBrushCutterClearingSawRef", GenericChapter.Item("RiskMachineryBrushCutterClearingSawRef"), 20)
                AddParameter("RiskMachineryHedgeTrimmersRef", GenericChapter.Item("RiskMachineryHedgeTrimmersRef"), 20)
                AddParameter("RiskMachineryMechanisedPolePrunnerRef", GenericChapter.Item("RiskMachineryMechanisedPolePrunnerRef"), 20)
                AddParameter("RiskMachineryMobileElevatedWorkPlatformRef", GenericChapter.Item("RiskMachineryMobileElevatedWorkPlatformRef"), 20)
                AddParameter("RiskMachineryCraneRef", GenericChapter.Item("RiskMachineryCraneRef"), 20)
                AddParameter("RiskMachineryMountedHydraulicLoadingArmRef", GenericChapter.Item("RiskMachineryMountedHydraulicLoadingArmRef"), 20)
                AddParameter("RiskMachineryOtherRef", GenericChapter.Item("RiskMachineryOtherRef"), 20)
                AddParameter("RiskWorkingAtHeightMewpRef", GenericChapter.Item("RiskWorkingAtHeightMewpRef"), 20)
                AddParameter("RiskWorkingAtHeightIsItCostEffectiveRef", GenericChapter.Item("RiskWorkingAtHeightIsItCostEffectiveRef"), 20)
                AddParameter("RiskWorkingAtHeightLadderRef", GenericChapter.Item("RiskWorkingAtHeightLadderRef"), 20)
                AddParameter("RiskWorkingAtHeightFallingObjectsRef", GenericChapter.Item("RiskWorkingAtHeightFallingObjectsRef"), 20)
                AddParameter("RiskOperationsOtherRef", GenericChapter.Item("RiskOperationsOtherRef"), 20)
                AddParameter("RiskWorking@HeightRopeandHarnessRef", GenericChapter.Item("RiskWorking@HeightRopeandHarnessRef"), 20)
                AddParameter("RiskWorking@HeightCompletedFromGroundRef", GenericChapter.Item("RiskWorking@HeightCompletedFromGroundRef"), 20)
                AddParameter("RiskWorking@HeightSafeToClimbRef", GenericChapter.Item("RiskWorking@HeightSafeToClimbRef"), 20)
                AddParameter("RiskWorking@HeightUsePoleSawRef", GenericChapter.Item("RiskWorking@HeightUsePoleSawRef"), 20)
                AddParameter("RiskWorkingAtHeightStraightFellRef", GenericChapter.Item("RiskWorkingAtHeightStraightFellRef"), 20)
                AddParameter("RiskOpperationsOtherRef", GenericChapter.Item("RiskOpperationsOtherRef"), 20)
                AddParameter("RiskSiteRoadsFootpathsPublicAreasRef", GenericChapter.Item("RiskSiteRoadsFootpathsPublicAreasRef"), 20)
                AddParameter("RiskSiteControlMeasuresNecessary", SectionNotes.Item("RiskSiteControlMeasuresNecessary"), ParameterDirection.Input)
                AddParameter("RiskEnvironmentalControlMeasuresNecessary", SectionNotes.Item("RiskEnvironmentalControlMeasuresNecessary"), ParameterDirection.Input)
                AddParameter("RiskOperationsControlMeasuresNecessary", SectionNotes.Item("RiskOperationsControlMeasuresNecessary"), ParameterDirection.Input)
                AddParameter("RiskMachinerControlMeasuresNecessary", SectionNotes.Item("RiskMachinerControlMeasuresNecessary"), ParameterDirection.Input)
                AddParameter("RiskWorking@HeightControlMeasures", SectionNotes.Item("RiskWorking@HeightControlMeasures"), ParameterDirection.Input)
                AddParameter("GridRef", GridRef, 20)

                RunProcedure_KeepConnectionAlive("Mob_RiskAssessmentUpdate")
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileRiskAssessmentUpdate", ex))
            End Try
        End Sub

        Public Sub MobileRiskSignatureImageSave(ByVal QuoteNo As Integer, ByVal Picture As Byte())
            Try
                ClearParameters()
                AddParameter("QuoteNo", QuoteNo)
                AddParameter("Image", Picture)
                RunProcedure("MOB_RiskSignaturSave")
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileRiskSignatureImageSave", ex))
            End Try
        End Sub
        Public Sub MobileRiskCloseconnection()
            Try
                CloseConnection(False)
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileQuoteCloseConnection", ex))
            End Try
        End Sub
    End Class
End Namespace

