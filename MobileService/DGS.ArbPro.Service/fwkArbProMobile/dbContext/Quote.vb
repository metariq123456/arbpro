﻿Imports System.Configuration
Imports mobCommonCode

Namespace dbContext
    Public Class DataQuote
        Inherits baseDataObject
        Private CLASS_NAME As String = "Data.Quote."

        Public Sub New(ByVal ConnectionString As String, AuditUserName As String)
            MyBase.New(ConnectionString, AuditUserName)
        End Sub
        Public Sub New(ByVal txDistributed As DistributedTransaction, AuditName As String)
            MyBase.New(txDistributed, AuditName)
        End Sub

#Region "Mobile"
        Public Sub MobileQuoteUpdate(ByVal QuoteId As Integer, TimeHrs As Decimal, StaffId As Integer, Comments As String, UserCR As String)
            Try
                If Comments Is Nothing Then
                    Comments = String.Empty
                End If
                ClearParameters()
                AddParameter("UserId", StaffId)
                AddParameter("QuoteId", QuoteId)
                AddParameter("TimeHrs", TimeHrs, False)
                AddParameter("StaffNumber", StaffId)
                AddParameter("Comments", Comments, ParameterDirection.Input)
                AddParameter("UserCR", UserCR, ParameterDirection.Input)
                RunProcedure_KeepConnectionAlive("MOB_QuoteUpdate")
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileQuoteUpdate", ex))
            End Try
        End Sub
        Public Sub MobileQuoteItemUpdate(ByVal QuoteId As Integer, ItemId As String, Type As String, Description As String, Value As Decimal, ItemGuid As String, OrderStatus As Integer, Latitude As Decimal, Longitude As Decimal, UserCR As String)
            Try
                ClearParameters()
                AddParameter("QuoteId", QuoteId, False)
                AddParameter("ItemId", ItemId, ParameterDirection.Input)
                AddParameter("Type", Type, Type.Length)
                AddParameter("Description", Description, ParameterDirection.Input)
                AddParameter("Value", Value, False)
                AddParameter("ItemGuid", ItemGuid, 25)
                AddParameter("OrderStatus", OrderStatus, False)
                If Latitude = 0 And Longitude = 0 Then
                    AddParameter("Latitude", -1)
                    AddParameter("Longitude", -1)
                Else
                    AddParameter("Latitude", Latitude, False)
                    AddParameter("Longitude", Longitude, False)
                End If
                AddParameter("UserCR", UserCR, ParameterDirection.Input)
                RunProcedure_KeepConnectionAlive("MOB_QuoteItemUpdate")
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileQuoteItemUpdate", ex))
            End Try
        End Sub
        Public Sub MobileQuoteToolUpdate(ByVal QuoteId As Integer, Tool As String, Number As Integer)
            Try
                ClearParameters()
                AddParameter("QuoteId", QuoteId)
                AddParameter("Tool", Tool, ParameterDirection.Input)
                AddParameter("Number", Number)
                RunProcedure_KeepConnectionAlive("MOB_QuoteToolUpdate")
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileQuoteToolUpdate", ex))
            End Try
        End Sub
        Public Sub MobileQuoteToolUpdateV3(ByVal appointmentId As Integer, ByVal toolId As Integer, toolName As String, ByVal stock As Integer, numberRequired As Integer)
            Try
                ClearParameters()
                AddParameter("AppointmentId", appointmentId)
                AddParameter("ToolId", toolId)
                AddParameter("ToolName", toolName, ParameterDirection.Input)
                AddParameter("Stock", stock)
                AddParameter("Number", numberRequired)
                RunProcedure_KeepConnectionAlive("MOB_QuoteToolUpdateV3")
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileQuoteToolUpdateV3", ex))
            End Try
        End Sub
        Public Sub MobileQuoteItemImageSave(ByVal Guid As String, ByVal Picture As Byte())
            Try
                ClearParameters()
                AddParameter("Guid", Guid, 25)
                AddParameter("Picture", Picture)
                RunProcedure("MOB_ItemSave")
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileQuoteCloseConnection", ex))
            End Try
        End Sub
        Public Sub MobileQuoteCloseconnection()
            Try
                CloseConnection(False)
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileQuoteCloseConnection", ex))
            End Try
        End Sub
#End Region


        Public Sub QuoteAccept(ByVal QuoteId As Integer)
            Try

                ClearParameters()
                AddParameter("Id", QuoteId)
                AddParameter("Status", 3)
                RunProcedure("Quote_UpdateStatus")
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "Quote_UpdateStatus", ex))
            End Try
        End Sub


    End Class
End Namespace
