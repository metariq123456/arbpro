﻿Imports System.Configuration
Imports mobCommonCode

Namespace dbContext
    Partial Public Class DataAppointment
        Inherits baseDataObject
        Private CLASS_NAME As String = "Data.Appointment."

        Public Sub New(ByVal ConnectionString As String, AuditName As String)
            MyBase.New(ConnectionString, AuditName)
        End Sub
        Public Sub New(ByVal txDistributed As DistributedTransaction, AuditName As String)
            MyBase.New(txDistributed, AuditName)
        End Sub

#Region "Mobile"
        Public Function MobileMarkDownloaded(ByVal AppointmentId As Integer) As Boolean
            Try
                ClearParameters()
                AddParameter("AppointmentId", AppointmentId)
                Dim ds As DataSet = RunProcedure("MOB_MarkDownloaded", "MarkDownloaded")
                If IsPopulated(ds) Then Return True Else Return False
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileMarkDownloaded", ex))
            End Try
        End Function
        Public Function MobileDownloadListForUser(ByVal UserId As Integer) As DataSet
            Try
                ClearParameters()
                AddParameter("UserId", UserId)
                Dim ds As DataSet = RunProcedure("MOB_DownloadListForUser", "DownloadListForUser")
                If IsPopulated(ds) Then Return ds Else Return Nothing
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileDownloadListForUser", ex))
            End Try
        End Function
        Public Sub MobileSaveLatLong(ByVal Id As Integer, Lat As Decimal, Lng As Decimal)
            Try
                ClearParameters()
                AddParameter("@JobSiteId", Id)
                AddParameter("Lat", Lat, False)
                AddParameter("Lng", Lng, False)
                RunProcedure("MOB_AppointmentSaveLatLng", "SaveLatLng")
            Catch ex As Exception
                Throw New Exception(BuildExceptionMessage(CLASS_NAME, "MobileSaveLatLng", ex))
            End Try
        End Sub
#End Region
    End Class
    Partial Public Class EntityAppointment
        Inherits baseEntity
        Shared CLASS_NAME As String = "Entity.Appointment."



        Public Overrides Function IsValid(Instance As baseEntity.enumInstance) As Boolean
            Return False
        End Function
    End Class
End Namespace
