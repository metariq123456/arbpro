﻿Imports mobCommonCode

Partial Public Class ArbProManager
    Inherits baseBusinessClass

    Private CLASS_NAME As String = "::BusinessLogic:ArbProManager:"
    Private _ClientID As Integer = 0
    Private _AuditUserName As String = "Unknown"

    Private Enum enumTraceOutputLevel
        Debug
        Warning
        [Error]
    End Enum

    Public Sub New(ByVal UseCachedData As Boolean, ClientId As Integer, AuditUserName As String)
        MyBase.New(UseCachedData)
        _ClientID = ClientId
        _AuditUserName = AuditUserName
        TraceOutput(String.Format("Using Cachec: {0}", UseCachedData.ToString.ToUpper), enumTraceOutputLevel.Debug)
    End Sub

    Private Sub TraceOutput(ByVal Text As String, ByVal Level As enumTraceOutputLevel, Optional ByVal err As Exception = Nothing)
        If Level = enumTraceOutputLevel.Error Then
            Debug.WriteLine("ERROR: " & Text.ToUpper)
            If Not err Is Nothing Then Debug.WriteLine(err.Message)
        ElseIf Level = enumTraceOutputLevel.Warning Then
            Debug.WriteLine("WARNING: " & Text)
        Else
            Debug.WriteLine("DEBUG: " & Text)
        End If
    End Sub
    Private Function CacheKeyPrefix(ObjectTypeName As String) As String
        Return ObjectTypeName & _ClientID.ToString & "~"
    End Function
    Private Function GetDistributedTransaction(tx As DistributedTransaction) As DistributedTransaction
        If tx Is Nothing Then tx = New DistributedTransaction(ConnectionString)
        Return tx
    End Function

#Region "UserAccount"
    Public Function UserAccountInstance(ByVal Username As String, Password As String) As dbContext.EntityUserAccount
        Dim dataObject As New dbContext.DataUserAccount(ConnectionString, _AuditUserName)
        Dim entityObject As dbContext.EntityUserAccount
        Try
            CacheKey = CacheKeyPrefix("UserAccount")
            If Username Is Nothing OrElse Password Is Nothing _
                OrElse Username.Trim.Length = 0 OrElse Password.Trim.Length = 0 _
                Then Return Nothing

            entityObject = New dbContext.EntityUserAccount(dataObject.Retrieve(Username, Password))
            Return entityObject
        Catch ex As Exception
            TraceOutput(CacheKey, enumTraceOutputLevel.Error, ex)
            Throw New Exception(BuildExceptionMessage(CLASS_NAME, "UserAccountInstance", ex))
        Finally
            dataObject = Nothing
            entityObject = Nothing
        End Try
    End Function
    Public Function UserAccountInstance(ByVal UserName As String) As dbContext.EntityUserAccount
        Dim dataObject As New dbContext.DataUserAccount(ConnectionString, _AuditUserName)
        Dim entityObject As dbContext.EntityUserAccount
        Try
            CacheKey = CacheKeyPrefix("UserAccount")

            entityObject = New dbContext.EntityUserAccount(dataObject.Retrieve(UserName))
            Return entityObject
        Catch ex As Exception
            TraceOutput(CacheKey, enumTraceOutputLevel.Error, ex)
            Throw New Exception(BuildExceptionMessage(CLASS_NAME, "UserAccountInstance", ex))
        Finally
            dataObject = Nothing
            entityObject = Nothing
        End Try
    End Function
#End Region
End Class
