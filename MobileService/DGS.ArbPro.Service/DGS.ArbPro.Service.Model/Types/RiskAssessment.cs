﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Types
{
    public class RiskAssessment
    {
        public int NearestAnE { get; set; }
        public string MobileSignal { get; set; }
        public string RescueAccess { get; set; }
        public int AerialRescuePlan { get; set; }
        public string FirstAider { get; set; }
        public string FirstAidEquipment { get; set; }
        public string GridReference { get; set; }
        public string AerialRescuePerson { get; set; }

        public bool CS38Competent { get; set; }

        public int QuoteNo { get; set; }
        public int UserId { get; set; }

		public DateTime CompletionDate {get;set;}

        public List<AssessmentSection> Criterion { get; set; }
        public List<AssessmentAdditionalItem> FurtherRisks { get; set; }
    }
}
