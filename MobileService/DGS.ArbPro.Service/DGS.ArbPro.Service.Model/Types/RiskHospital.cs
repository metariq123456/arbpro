﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DGS.ArbPro.Service.Model.Types
{
    [DataContract]
    public class RiskHospital
    {
        [DataMember]
        public int HospitalId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string UserCR { get; set; }
    }
}
