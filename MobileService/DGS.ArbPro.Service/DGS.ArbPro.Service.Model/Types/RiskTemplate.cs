﻿namespace DGS.ArbPro.Service.Model.Types
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The risk template.
    /// </summary>
    public class RiskTemplate
    {
        #region Risk Assessment

        /// <summary>
        /// Gets or sets the nearest AnE.
        /// </summary>
        public int NearestAnE { get; set; }

        /// <summary>
        /// Gets or sets the mobile signal.
        /// </summary>
        public string MobileSignal { get; set; }

        /// <summary>
        /// Gets or sets the rescue access.
        /// </summary>
        public string RescueAccess { get; set; }

        /// <summary>
        /// Gets or sets the aerial rescue plan.
        /// </summary>
        public int AerialRescuePlan { get; set; }

        /// <summary>
        /// Gets or sets the first aider.
        /// </summary>
        public string FirstAider { get; set; }

        /// <summary>
        /// Gets or sets the first aid equipment.
        /// </summary>
        public string FirstAidEquipment { get; set; }

        /// <summary>
        /// Gets or sets the grid reference.
        /// </summary>
        public string GridReference { get; set; }

        /// <summary>
        /// Gets or sets the aerial rescue person.
        /// </summary>
        public string AerialRescuePerson { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether it is cs 38 competent.
        /// </summary>
        public bool Cs38Competent { get; set; }

        /// <summary>
        /// Gets or sets the quote no.
        /// </summary>
        public int QuoteNo { get; set; }

        /// <summary>
        /// Gets or sets the user id.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets the completion date.
        /// </summary>
        public DateTime CompletionDate { get; set; }

        #endregion

        #region Risk Template

        /// <summary>
        /// The s.
        /// </summary>
        private List<RiskSection> s = new List<RiskSection>(); 

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the ref doc.
        /// </summary>
        public string RefDoc { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        #endregion

        /// <summary>
        /// Gets or sets the sections.
        /// </summary>
        public List<RiskSection> Sections
        {
            get
            {
                return this.s;
            }

            set
            {
                this.s = value;
            }
        }

        /// <summary>
        /// Gets or sets the further risks.
        /// </summary>
        public List<AssessmentAdditionalItem> FurtherRisks { get; set; }
    }
}
