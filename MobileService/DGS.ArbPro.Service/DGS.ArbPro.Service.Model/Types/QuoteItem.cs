﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DGS.ArbPro.Service.Model.Types
{
	[DataContract]
	public class QuoteItem
	{
		[DataContract]
		public enum enumPreservationOrderStatus
		{
			[EnumMember]
			Yes,
			[EnumMember]
			No,
			[EnumMember]
			Unknown
		}

		[DataMember]
		public string ItemNumber { get; set; }
		// The maximum length of an ItemType, as limited in the database
		public static int ItemType_MaxLength = 50;

		[DataMember]
		public string ItemType { get; set; }

		[DataMember]
		public float ItemValue { get; set; }

		public static int WorkDescription_MaxLength = 1000;

		[DataMember]
		public string WorkDescription { get; set; }

		[DataMember]
		public string ItemGuid { get; set; }

		[DataMember]
		public enumPreservationOrderStatus PreservationOrderStatus { get; set; }

		[DataMember]
		public GeoLocation Location { get; set; }
	}
}
