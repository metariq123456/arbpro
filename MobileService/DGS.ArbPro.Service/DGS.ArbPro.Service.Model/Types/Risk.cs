﻿// //*********************************************************************
// // $Id$
// // $Author$
// // $Date$
// // $Change$
// //*********************************************************************
//
//
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DGS.ArbPro.Service.Model.Types
{
	public class Risk
	{
		#region Members

		int id = -1;
        int clientId = -1;
        int quoteId = -1;
		int appointmentId = -1;
		int jobSiteId = -1;
		int assessingStaffId = -1;
		int aerialPlanId = -1;
		int teamLeaderStaffId = -1;
		int auditId = -1;

		bool cs38Certified = false;
		bool satisfactoryCompletion = false;
		bool referralRequired = false;
		bool referralCompleted = false;
		bool completed = false;

		DateTime assessmentDate;
        DateTime workDate;
		DateTime completionDate;
		DateTime lastUpdatedDate;

		string nearestHospital = string.Empty;
        string aerialPlan = string.Empty;
		string refDoc = string.Empty;
		string mobileSignal = string.Empty;
		string siteAccess = string.Empty;
		string gridReference = string.Empty;
		string referralComments = string.Empty;
		string managementComments = string.Empty;
		string lastUpdatedBy = string.Empty;
		string aerialRescuePlan = string.Empty;
		string rescueNomaniee = string.Empty;

		List<RiskSection> sections = new List<RiskSection>();
		List<RiskAdditionalItem> additionalRisks = new List<RiskAdditionalItem>();

		#endregion

		#region Constructors

		public Risk()
		{
		}

		#endregion

		#region Properties

		[DataMember]
		public int Id
		{
			get { return id; }
			set { id = value; }
		}

        [DataMember]
        public int QuoteId
        {
            get { return quoteId; }
            set { quoteId = value; }
        }

		[DataMember]
		public int ClientId
		{
			get { return clientId; }
			set { clientId = value; }
		}

		[DataMember]
		public int AppointmentId
		{
			get { return appointmentId; }
			set { appointmentId = value; }
		}

		[DataMember]
		public int JobSiteId
		{
			get { return jobSiteId; }
			set { jobSiteId = value;}
		}

		[DataMember]
		public int AssessingStaffId
		{
			get { return assessingStaffId; }
			set { assessingStaffId = value; }
		}

		[DataMember]
		public int AerialPlanId
		{
			get { return aerialPlanId; }
			set { aerialPlanId = value; }
		}

		[DataMember]
		public int TeamLeaderStaffId
		{
			get { return teamLeaderStaffId;}
			set { teamLeaderStaffId = value;}
		}

		[DataMember]
		public int AuditId
		{
			get { return auditId; }
			set { auditId = value; }
		}

		[DataMember]
		public bool CS38Certifed
		{
			get { return cs38Certified;}
			set { cs38Certified = value;}
		}

		[DataMember]
		public bool SatisfactoryCompletion
		{
			get { return satisfactoryCompletion;}
			set { satisfactoryCompletion = value; }
		}

		[DataMember]
		public bool ReferralRequired
		{
			get { return referralRequired; }
			set { referralRequired = value; }
		}

		[DataMember]
		public bool ReferralCompleted
		{
			get { return referralCompleted; }
			set { referralCompleted = value; }
		}

		[DataMember]
		public bool Completed
		{
			get { return completed; }
			set { completed = value; }
		}

		[DataMember]
		public DateTime AssessmentDate
		{
			get { return assessmentDate; }
			set { assessmentDate = value; }
		}

		[DataMember]
		public DateTime WorkDate
		{
			get { return workDate; }
			set { workDate = value; }
		}

		[DataMember]
		public DateTime CompletionDate
		{
			get { return completionDate; }
			set { completionDate = value; }
		}

		[DataMember]
		public DateTime LastUpdatedDate
		{
			get { return lastUpdatedDate; }
			set { lastUpdatedDate = value; }
		}

		[DataMember]
		public string NearestHospital
		{
			get { return nearestHospital;}
			set { nearestHospital = value; }
		}

        [DataMember]
        public string AerialPlan
        {
            get { return aerialPlan; }
            set { aerialPlan = value; }
        }


		[DataMember]
		public string ReferenceDoc
		{
			get { return refDoc; }
			set { refDoc = value; }
		}

		[DataMember]
		public string MobileSignal
		{
			get { return mobileSignal; }
			set { mobileSignal = value; }
		}

		[DataMember]
		public string SiteAccess
		{
			get { return siteAccess; }
			set { siteAccess = value; }
		}

		[DataMember]
		public string GridReference
		{
			get { return gridReference; }
			set { gridReference = value; }
		}

		[DataMember]
		public string ReferralComments
		{
			get { return referralComments; }
			set { referralComments = value; }
		}

		[DataMember]
		public string ManagementComments
		{
			get { return managementComments; }
			set { managementComments = value; }
		}

		[DataMember]
		public string LastUpdatedBy
		{
			get { return lastUpdatedBy; }
			set { lastUpdatedBy = value; }
		}

		[DataMember]
		public string RescuePlan
		{
			get { return aerialRescuePlan; }
			set { aerialRescuePlan = value; }
		}

		[DataMember]
		public string RescueNomaniee
		{
			get { return rescueNomaniee; }
			set { rescueNomaniee = value; }
		}

		[DataMember]
		public List<RiskSection> AssessmentSections
		{
			get { return sections; }
			set { sections = value; }
		}

		[DataMember]
		public List<RiskAdditionalItem> AdditionalRisks
		{
			get { return additionalRisks; }
			set { additionalRisks = value; }
		}

		#endregion

	}
}

