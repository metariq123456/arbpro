﻿// //*********************************************************************
// // $Id$
// // $Author$
// // $Date$
// // $Change$
// //*********************************************************************
//
//
using System;
using System.Runtime.Serialization;

namespace DGS.ArbPro.Service.Model.Types
{
	public class RiskCriterion
	{
		#region Members

		int id = -1;
		int riskSectionId = -1;
		int assessmentPhase = -1;

		string criterion = string.Empty;
		string chapterRef = string.Empty;

		int controlRequired = 0;
		int onDayControlRequired = 0;

		#endregion

		#region Constructors

		public RiskCriterion()
		{
		}

		#endregion

		#region Properties

		[DataMember]
		public int Id
		{
			get { return id; }
			set { id = value; }
		}

		[DataMember]
		public int RiskSectionId
		{
            get { return riskSectionId; }
            set { riskSectionId = value; }
		}

		[DataMember]
		public int AssessmentPhase
		{
			get { return assessmentPhase; }
			set { assessmentPhase = value; }
		}

		[DataMember]
		public string Criterion
		{
			get { return criterion; }
			set { criterion = value; }
		}

		[DataMember]
		public string ChapterRef
		{
			get { return chapterRef; }
			set { chapterRef = value; }
		}

		[DataMember]
		public int ControlRequired
		{
			get { return controlRequired; }
			set { controlRequired = value; }
		}

		[DataMember]
		public int OnDayControlRequired
		{
			get { return onDayControlRequired; }
			set { onDayControlRequired = value; }
		}

		#endregion
	}
}

