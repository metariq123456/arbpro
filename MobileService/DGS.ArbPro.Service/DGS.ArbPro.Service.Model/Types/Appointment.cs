﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Types
{
    [DataContract]
    public class Appointment
    {
        public enum enumResidentialCommercial
        {
            Residential,
            Commercial
        }

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int MasterCalendarId { get; set; }
        [DataMember]
        public int ClientIdNo { get; set; }
        [DataMember]
        public int JobIdNo { get; set; }
        [DataMember]
        public DateTime StartDateTime { get; set; }
        [DataMember]
        public int QuoteNo { get; set; }
        [DataMember]
        public float Duration { get; set; }
        [DataMember]
        public String Subject { get; set; }
        [DataMember]
        public String UserCR { get; set; }
        [DataMember]
        public int SurveyorId { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public String ClientName { get; set; }
        [DataMember]
        public String Title { get; set; }
        [DataMember]
        public String FirstName { get; set; }
        [DataMember]
        public String Surname { get; set; }
        [DataMember]
        public String ClientStreetNo { get; set; }
        [DataMember]
        public String ClientAddress1 { get; set; }
        [DataMember]
        public String ClientAddress2 { get; set; }
        [DataMember]
        public String ClientAddressTown { get; set; }
        [DataMember]
        public String ClientAddressCounty { get; set; }
        [DataMember]
        public String ClientAddressPostCode { get; set; }
        [DataMember]
        public int SiteId { get; set; }
        [DataMember]
        public String SiteStreetNo { get; set; }
        [DataMember]
        public String SiteAddress1 { get; set; }
        [DataMember]
        public String SiteAddress2 { get; set; }
        [DataMember]
        public String SiteAddressTown { get; set; }
        [DataMember]
        public String SiteAddressCounty { get; set; }
        [DataMember]
        public String SiteAddressPostCode { get; set; }
        [DataMember]
        public GeoLocation ClientAddressLocation { get; set; }
        [DataMember]
        public String ClientEmail { get; set; }
        [DataMember]
        public String ClientTelephone { get; set; }
		[DataMember]
		public String ClientMobile { get; set; }
        [DataMember]
        public enumResidentialCommercial ResidentialCommercial { get; set; }
        [DataMember]
        public Quote AppointmentQuote { get; set; }
        [DataMember]
        public int RiskTemplateId { get; set; }
        [DataMember]
        public int ClientContactId { get; set; }
		
        public Appointment()
        {
            ClientAddressLocation = new GeoLocation();
        }

        public override String ToString()
        {
            return String.Format("{0} - {1}hrs [{2} - {3}, {4}]", StartDateTime.ToString("dd/MM/yy"), Duration, ClientName, ClientAddress1, ClientAddressPostCode);
        }
    }
}
