﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Types
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public String UserName { get; set; }
        [DataMember]
        public String DisplayName { get; set; }
        [DataMember]
        public String Password { get; set; }
        [DataMember]
        public int ClientId { get; set; }
        [DataMember]
        public Guid UserGuid { get; set; }
    }
}
