﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Types
{
    [DataContract]
    public class Quote
    {
        [DataMember]
        public int QuoteId { get; set; }
        [DataMember]
        public float TimeHrs { get; set; }
        [DataMember]
        public int StaffNumber { get; set; }
        [DataMember]
        public string Comments { get; set; }
        [DataMember]
        public List<QuoteItem> Items { get; set; }
        [DataMember]
        public List<string> Tools { get; set; }
        [DataMember]
        public List<Tool> ToolsV3 { get; set; }
        [DataMember]
        public Service.Model.Types.Risk Assessment {get;set;}
    }
}
