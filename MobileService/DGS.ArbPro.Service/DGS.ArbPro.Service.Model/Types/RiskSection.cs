﻿// //*********************************************************************
// // $Id$
// // $Author$
// // $Date$
// // $Change$
// //*********************************************************************
//
//
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DGS.ArbPro.Service.Model.Types
{
	public class RiskSection
	{
		#region Members

		int id = -1;
		int riskAssessmentId = -1;

		string title = string.Empty;
		string description = string.Empty;
		string referenceDoc = string.Empty;
		string chapter = string.Empty;
		string comments = string.Empty;

		List<RiskCriterion> sectionCriteria = new List<RiskCriterion>();

		#endregion

		#region Constructors

		public RiskSection()
		{
		}

		#endregion

		#region Properties

		[DataMember]
		public int Id
		{
			get { return id; }
			set { id = value; }
		}

		[DataMember]
		public int RiskAssessmentId
		{
			get { return riskAssessmentId; }
			set { riskAssessmentId = value; }
		}

		[DataMember]
		public string Title
		{
			get { return title; }
			set { title = value; }
		}

		[DataMember]
		public string Description
		{
			get { return description; }
			set { description = value; }
		}

		[DataMember]
		public string ReferenceDoc
		{
			get { return referenceDoc; }
			set { referenceDoc = value; }
		}

		[DataMember]
		public string Chapter
		{
			get { return chapter; }
			set { chapter = value; }
		}

		[DataMember]
		public string Comments
		{
			get { return comments; }
			set { comments = value; }
		}

		[DataMember]
		public List<RiskCriterion> Criteria
		{
			get { return sectionCriteria; }
			set { sectionCriteria = value; }
		}

		#endregion
	}
}

