﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Types
{
    public class AssessmentSection
    {
        public string Title { get; set; }
        public string AdditionalDetail { get; set; }
        public List<AssessmentItem> AssessmentItems { get; set; }
    }
}
