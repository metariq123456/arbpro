﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Types
{
    using System.Runtime.Serialization;

    public class Client
    {
        #region Members

        private int id = -1;
        private string _AltAccountNo = "";
        private string _Forename = "";
        private string _Surname = "";
        private string _Title = "";
        private string _HouseNameOrNumber = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private string _Town = "";
        private string _County = "";
        private string _PostCode = "";
        private string _TelephoneNumber = "";
        private string _Mobile = "";
        private string _Email = "";
        private string _BusinessName = "";

        private int _AccountType = 10;
        private DateTime _DateOpened = DateTime.MinValue;

        private int _MultiRefLink = 0;
        private string _CoCode = "";
        private string _Sector = "";
        private string _WebAddress = "";
        private string _PaymentDays = "";
        private decimal _CreditLimit = 0.00M;
        private bool _TermsAgreed = false;
        private string _VATNo = "";
        private double _AddressLatitude = 0.00f;
        private double _AddressLongitude = 0.00f;
        private bool _SendSmsMessage = true;
        private bool _IncludeInMarketing = true;
        #endregion

        #region Constructors

        #endregion

        #region Properties

        [DataMember]
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        [DataMember]
        public string AltAccountNo
        {
            get { return _AltAccountNo; }
            set { _AltAccountNo = value; }
        }

        [DataMember]
        public string Forename
        {
            get { return _Forename; }
            set { _Forename = value; }
        }

        [DataMember]
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        [DataMember]
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        [DataMember]
        public string HouseNameNumber
        {
            get { return _HouseNameOrNumber; }
            set { _HouseNameOrNumber = value; }
        }

        [DataMember]
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        [DataMember]
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        [DataMember]
        public string Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        [DataMember]
        public string County
        {
            get { return _County; }
            set { _County = value; }
        }

        [DataMember]
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        [DataMember]
        public string Telephone
        {
            get { return _TelephoneNumber; }
            set { _TelephoneNumber = value; }
        }

        [DataMember]
        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }

        [DataMember]
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        [DataMember]
        public string BusinessName
        {
            get { return _BusinessName; }
            set { _BusinessName = value; }
        }

        [DataMember]
        public int AccountType
        {
            get { return _AccountType; }
            set { _AccountType = value; }
        }

        [DataMember]
        public DateTime DateOpened
        {
            get { return _DateOpened; }
            set { _DateOpened = value; }
        }

        [DataMember]
        public double AddressLatitude
        {
            get { return _AddressLatitude; }
            set { _AddressLatitude = value; }
        }

        [DataMember]
        public double AddressLongitude
        {
            get { return _AddressLongitude; }
            set { _AddressLongitude = value; }
        }

        [DataMember]
        public bool SendSmsMessage
        {
            get { return _SendSmsMessage; }
            set { _SendSmsMessage = value; }
        }

        [DataMember]
        public bool IncludeInMarketing
        {
            get { return this._IncludeInMarketing; }
            set { this._IncludeInMarketing = value; }
        }

        [DataMember]
        public int MultiRefLink
        {
            get { return _MultiRefLink; }
            set { _MultiRefLink = value; }
        }

        [DataMember]
        public string CoCode
        {
            get { return _CoCode; }
            set { _CoCode = value; }
        }

        [DataMember]
        public string Sector
        {
            get { return _Sector; }
            set { _Sector = value; }
        }

        [DataMember]
        public string WebAddress
        {
            get { return _WebAddress; }
            set { _WebAddress = value; }
        }

        [DataMember]
        public string PaymentDays
        {
            get { return _PaymentDays; }
            set { _PaymentDays = value; }
        }

        [DataMember]
        public decimal CreditLimit
        {
            get { return _CreditLimit; }
            set { _CreditLimit = value; }
        }

        [DataMember]
        public bool TermsAgreed
        {
            get { return _TermsAgreed; }
            set { _TermsAgreed = value; }
        }

        [DataMember]
        public string VATNo
        {
            get { return _VATNo; }
            set { _VATNo = value; }
        }

        #endregion
    }
}
