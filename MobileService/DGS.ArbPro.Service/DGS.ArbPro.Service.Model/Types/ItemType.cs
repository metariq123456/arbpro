﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Types
{
	[DataContract]
	public class ItemType
	{
		[DataMember]
		public String CommonName { get; set; }

		[DataMember]
		public String TechnicalName { get; set; }
	}
}
