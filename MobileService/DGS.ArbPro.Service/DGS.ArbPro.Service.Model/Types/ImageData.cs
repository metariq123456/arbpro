﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Model.Types
{
    [DataContract]
    public class ImageData
    {
        [DataMember]
        public String ItemGuid;
        [DataMember]
        public int CurrentBlobs;
        [DataMember]
        public int TotalBlobs;
        [DataMember]
        public String FileName;

        public ImageData(String Guid, int PacketCount)
        {
            ItemGuid = Guid;
            TotalBlobs = PacketCount;
            FileName = String.Format("ImageUpload_{0}.txt", ItemGuid);
            CurrentBlobs = 0;

            File.Create(FileName).Close();

            String StrEmptyBytes = "";
            for (int i = 1; i < TotalBlobs; i++)
            {
                StrEmptyBytes += ",";
            }

            File.WriteAllText(FileName, StrEmptyBytes);
        }

        public byte[] BuildImage()
        {
            String Bytes = File.ReadAllText(FileName);
            String[] ByteStrings = Bytes.Split(',');
            List<Byte> Data = new List<Byte>();
            foreach (String s in ByteStrings)
            {
                Data.AddRange(System.Text.Encoding.UTF8.GetBytes(s));
            }
            return Data.ToArray();
        }

        public void AddBytes(int BlobID, byte[] Data)
        {
            String Bytes = File.ReadAllText(FileName);
            String[] ByteStrings = Bytes.Split(',');
            ByteStrings[BlobID - 1] = System.Text.Encoding.UTF8.GetString(Data);
            CurrentBlobs++;
            Bytes = "";
            foreach (String s in ByteStrings)
            {
                Bytes += s + ",";
            }
            Bytes = Bytes.Substring(0, Bytes.Length - 1);
            File.Delete(FileName);
            File.Create(FileName).Close();
            File.WriteAllText(FileName, Bytes);
        }

        public void DeleteFile()
        {
            File.Delete(FileName);
        }
    }
}
