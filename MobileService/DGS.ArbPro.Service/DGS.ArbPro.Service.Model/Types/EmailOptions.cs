﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Types
{
    public class EmailOptions
    {
        public int AppointmentId { get; set; }

        public bool IncludeItemValues { get; set; }

        public bool IncludeQuoteTotal { get; set; }

        public bool SendSms { get; set; }

        public bool MarkAccepted { get; set; }
    }
    
}
