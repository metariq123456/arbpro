﻿// //*********************************************************************
// // $Id$
// // $Author$
// // $Date$
// // $Change$
// //*********************************************************************
//
//
using System;
using System.Runtime.Serialization;

namespace DGS.ArbPro.Service.Model.Types
{
	public class RiskAdditionalItem
	{
		#region Members

		int id = -1;
		int riskAssessmentId = -1;

		string assessedRisk = string.Empty;
		string controlRequired = string.Empty;

		#endregion

		#region Constructors

		public RiskAdditionalItem()
		{
		}

		#endregion

		#region Properties

		[DataMember]
		public int Id
		{
			get { return id; }
			set { id = value; }
		}

		[DataMember]
		public int RiskAssessmentId
		{
			get { return riskAssessmentId; }
			set { riskAssessmentId = value; }
		}

		[DataMember]
		public string AssessedRisk
		{
			get { return assessedRisk; }
			set { assessedRisk = value; }
		}

		[DataMember]
		public string ControlRequired
		{
			get { return controlRequired; }
			set { controlRequired = value; }
		}

		#endregion
	}
}

