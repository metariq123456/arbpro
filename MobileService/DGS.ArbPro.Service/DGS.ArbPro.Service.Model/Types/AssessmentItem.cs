﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Types
{
    public class AssessmentItem
    {
        public string Title { get; set; }
        public string RefCode { get; set; }
        public bool ControlRequired { get; set; }
    }
}
