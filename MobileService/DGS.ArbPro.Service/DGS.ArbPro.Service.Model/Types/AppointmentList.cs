﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Types
{
    [DataContract]
    public class AppointmentList
    {
        [DataMember]
        public int AppointmentId { get; set; }
        [DataMember]
        public DateTime LastUpdatedDate { get; set; }
    }
}
