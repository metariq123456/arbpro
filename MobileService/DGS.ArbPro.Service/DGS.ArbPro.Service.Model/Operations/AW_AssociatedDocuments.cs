﻿using DGS.ArbPro.Service.Model.Operations.Responses;
using ServiceStack.ServiceHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Model.Operations
{
    [DataContract]
    public class AW_AssociatedDocuments : IReturn<AW_AssociatedDocuments_Response>
    {
        [DataMember]
        public int ClientId { get; set; }

        [DataMember]
        public int JobId { get; set; }

        [DataMember]
        public int JobSiteId { get; set; }


    }
    [DataContract]
    public class AW_AssociatedDocuments_Response
    {
        [DataMember]
        public List<AssociatedDocument> Documents { get; set; }

        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }

    [DataContract]
    public class AssociatedDocument
    {

        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public int ClientId { get; set; }
        [DataMember]
        public int JobId { get; set; }
        [DataMember]
        public int JobSiteId { get; set; }
        [DataMember]
        public DGS.ArbPro.Types.Enums.ReportTypeEnum ItemType { get; set; }
        [DataMember]
        public string DocType { get; set; }
        [DataMember]
        public DGS.ArbPro.Types.Enums.DocDispatchMethodEnum DispatchMethod { get; set; }
        [DataMember]
        public DateTime DateCreated { get; set; }
        [DataMember]
        public DateTime DateSent { get; set; }
        [DataMember]
        public byte[] Document { get; set; }
        [DataMember]
        public string EmailReference { get; set; }
    }
}
