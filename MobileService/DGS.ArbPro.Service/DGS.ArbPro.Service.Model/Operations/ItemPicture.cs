﻿using DGS.ArbPro.Service.Model.Operations.Responses;
using ServiceStack.ServiceHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Model.Operations
{
    [DataContract]
    public class ItemPicture : IReturn<ItemPicture_Response>
    {
        [DataMember]
        public List<ItemImage> lstItemaImage { get; set; }
    }

    [DataContract]
    public class ItemPicture_Response
    {
        [DataMember]
        public bool Result { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }

    }

    public class ItemImage
    {
        public string ImageGuid { get; set; }

        public int JobItemId { get; set; }

        public int ParentId { get; set; }

        public DGS.ArbPro.Types.Enums.ItemTypeEnum ParentItemType { get; set; }

        public byte ImageBytes { get; set; }

    }
}
