﻿using System.Runtime.Serialization;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    public class RiskAssessmentGet : IReturn<RiskAssessmentGetResponse>
    {
        /// <summary>
        /// Gets or sets the risk.
        /// </summary>
        [DataMember]
        public Model.Types.Risk RiskAssessment { get; set; }

        /// <summary>
        /// Gets or sets the template id.
        /// </summary>
        [DataMember]
        public int TemplateId { get; set; }

        /// <summary>
        /// Gets or sets the appointment id.
        /// </summary>
        [DataMember]
        public int AppointmentId { get; set; }
    }

    /// <summary>
    /// The risk assessment get response.
    /// </summary>
    [DataContract]
    public class RiskAssessmentGetResponse
    {
        /// <summary>
        /// Gets or sets the risk.
        /// </summary>
        [DataMember]
        public Model.Types.Risk RiskAssessment { get; set; }

        /// <summary>
        /// Gets or sets the response status.
        /// </summary>
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
