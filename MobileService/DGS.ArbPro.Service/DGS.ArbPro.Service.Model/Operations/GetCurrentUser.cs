﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class GetCurrentUser : IReturn<GetCurrentUser_Response>
    {
    }

    [DataContract]
    public class GetCurrentUser_Response
    {
        [DataMember]
        public Model.Types.User User { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
