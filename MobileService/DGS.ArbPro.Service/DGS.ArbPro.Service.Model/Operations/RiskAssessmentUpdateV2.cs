﻿namespace DGS.ArbPro.Service.Model.Operations
{
    using System;
    using System.Runtime.Serialization;
    using ServiceStack.ServiceHost;
    using ServiceStack.ServiceInterface.ServiceModel;


    [DataContract]
    public class RiskAssessmentUpdateV2 : IReturn<RiskAssessmentUpdateV2_Response>
    {
        [DataMember]
        public Types.RiskAssessment Risk { get; set; }
        [DataMember]
        public String UserCR { get; set; }
    }
    [DataContract]
    public class RiskAssessmentUpdateV2_Response
    {
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}

