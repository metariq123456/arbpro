﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Model.Operations
{
    [DataContract]
    public class AppointmentGetForId
    {
        [DataMember]
        public int AppointmentId { get; set; }
        [DataMember]
        public int ClientId { get; set; }
    }
    
    [DataContract]
    public class AppointmentGetForId_Response
    {
        [DataMember]
        public Model.Types.Appointment Appointment { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
