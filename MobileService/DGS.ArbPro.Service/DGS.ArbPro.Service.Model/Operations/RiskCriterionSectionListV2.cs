﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class RiskCriterionSectionListV2 : IReturn<RiskCriterionSectionListV2_Response>
    {
    }

    /// <summary>
    /// Service Stack response that returns a list of Risk Assessment sections.
    /// Each section will contain one or more risk assessment criterion (items) that require assessment.
    /// </summary>
    [DataContract]
    public class RiskCriterionSectionListV2_Response
    {
        [DataMember]
        public List<Model.Types.AssessmentSection> Sections { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
