﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class UploadRiskSignature : IReturn<UploadRiskSignature_Response>, IRequiresRequestStream
    {
        [DataMember]
        public int QuoteNo { get; set; }

        public System.IO.Stream RequestStream
        {
            get;
            set;
        }
    }

    [DataContract]
    public class UploadRiskSignature_Response
    {
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
