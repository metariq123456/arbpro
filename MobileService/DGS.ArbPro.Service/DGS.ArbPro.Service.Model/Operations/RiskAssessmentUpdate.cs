﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class RiskAssessmentUpdate : IReturn<RiskAssessmentUpdate_Response>
    {
        [DataMember]
        public Model.Types.RiskAssessment Risk { get; set; }
        [DataMember]
        public String UserCR { get; set; }
    }
    [DataContract]
    public class RiskAssessmentUpdate_Response
    {
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
