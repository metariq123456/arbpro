﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class SendVersion : IReturn<SendVersion_Response>
    {
        [DataMember]
        public string AppVersion { get; set; }
    }

    [DataContract]
    public class SendVersion_Response

    {

    }
}
