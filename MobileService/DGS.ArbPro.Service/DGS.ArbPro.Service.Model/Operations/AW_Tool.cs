﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceInterface.ServiceModel;
using ServiceStack.ServiceHost;
namespace DGS.ArbPro.Model.Operations
{
    //Get Tool
    [DataContract]
    public class AW_Tool : IReturn<AW_Tool_Response>
    {
    }
    [DataContract]
    public class AW_Tool_Response
    {
        [DataMember]
        public List<Tool> Tools { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }

    [DataContract]
    public class Tool
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Stock { get; set; }
    }

}
