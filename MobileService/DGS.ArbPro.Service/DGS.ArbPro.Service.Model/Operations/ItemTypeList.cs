﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class ItemTypeList : IReturn<ItemTypeList_Response>
    {
    }
    [DataContract]
    public class ItemTypeList_Response
    {
        [DataMember]
        public List<Model.Types.ItemType> Result { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
