﻿namespace DGS.ArbPro.Service.Model.Operations
{
    using System.Runtime.Serialization;

    using ServiceStack.ServiceHost;
    using ServiceStack.ServiceInterface.ServiceModel;

    /// <summary>
    /// The risk assessment templates.
    /// </summary>
    [DataContract]
    public class RiskAssessmentUpdateV3 : IReturn<RiskAssessmentUpdateV3Response>
    {
        /// <summary>
        /// Gets or sets the risk.
        /// </summary>
        [DataMember]
        public Model.Types.Risk Risk { get; set; }
    }

    /// <summary>
    /// The risk assessment template response.
    /// </summary>
    [DataContract]
    public class RiskAssessmentUpdateV3Response
    {
        /// <summary>
        /// Gets or sets the response status.
        /// </summary>
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
