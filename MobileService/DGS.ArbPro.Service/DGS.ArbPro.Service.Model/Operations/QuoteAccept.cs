﻿using DGS.ArbPro.Service.Model.Operations.Responses;
using ServiceStack.ServiceHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Model.Operations
{
    [DataContract]
    public class QuoteAccept : IReturn<QuoteAccept_Response>
    {
        [DataMember]
        public int QuoteId { get; set; }
    }


    [DataContract]
    public class QuoteAccept_Response
    {
        [DataMember]
        public bool IsAccept { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }

}
