﻿using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Model.Operations
{
    public class AW_NewJobItem : IReturn<AW_NewJobItem_Response>
    {
        [DataMember]
        public int JobId { get; set; }

        [DataMember]
        public int JobSiteId { get; set; }

        [DataMember]
        public int TPOId { get; set; }

        [DataMember]
        public decimal Value { get; set; }

        [DataMember]
        public decimal VatRate{get;set;}

        [DataMember]
        public string Code {get;set;}

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public decimal Latitude { get; set; }

        [DataMember]
        public decimal Longitude { get; set; }

        [DataMember]
        public string ImageGuid { get; set; }

    }
    [DataContract]
    public class AW_NewJobItem_Response
    {
        [DataMember]
        public List<String> Id { get; set; }

        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
