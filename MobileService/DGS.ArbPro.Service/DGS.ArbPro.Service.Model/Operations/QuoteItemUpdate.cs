﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class QuoteItemUpdate : IReturn<QuoteItemUpdate_Response>
    {
        [DataMember]
        public int QuoteId { get; set; }
        [DataMember]
        public Model.Types.QuoteItem Item { get; set; }
        [DataMember]
        public String UserCR { get; set; }
    }
    [DataContract]
    public class QuoteItemUpdate_Response
    {
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
