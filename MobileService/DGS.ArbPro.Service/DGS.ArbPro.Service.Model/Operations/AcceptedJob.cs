﻿using DGS.ArbPro.Service.Model.Operations.Responses;
using DGS.ArbPro.Types.Enums;
using ServiceStack.ServiceHost;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Model.Operations
{
    [DataContract]
    public class AcceptedJob : IReturn<AcceptedJob_Response>
    {

    }

    [DataContract]
    public class AcceptedJob_Response
    {
        [DataMember]
        public List<AcceptedQuote> AcceptedQuoteDetail { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }

    public class AcceptedQuote
    {
        public int JobId { get; set; }
        public int QuoteId { get; set; }
        public int SiteId { get; set; }
        public int WorkId { get; set; }
        public string ClientName { get; set; }
        public string SiteAddress { get; set; }
        public string Contactdtl { get; set; }
        public decimal Value { get; set; }
        public int Status{ get; set; }
        public string QuoteRefNo { get; set; }
        public int CalendarWorkID { get; set; }

    }
}
