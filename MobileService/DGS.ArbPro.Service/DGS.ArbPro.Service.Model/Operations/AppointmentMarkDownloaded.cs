﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class AppointmentMarkDownloaded : IReturn<AppointmentMarkDownloaded_Response>
    {
        [DataMember]
        public int AppointmentId { get; set; }
    }
    [DataContract]
    public class AppointmentMarkDownloaded_Response
    {
        [DataMember]
        public Boolean Result { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
