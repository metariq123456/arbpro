﻿namespace DGS.ArbPro.Service.Model.Operations
{
    using System.Runtime.Serialization;

    using ServiceStack.ServiceHost;
    using ServiceStack.ServiceInterface.ServiceModel;

    /// <summary>
    /// The client object.
    /// </summary>
    [DataContract]
    public class ClientUpdate : IReturn<ClientUpdateResponse>
    {
        /// <summary>
        /// Gets or sets the risk.
        /// </summary>
        [DataMember]
        public Model.Types.Client Client { get; set; }
    }

    /// <summary>
    /// The client update response.
    /// </summary>
    [DataContract]
    public class ClientUpdateResponse
    {
        /// <summary>
        /// Gets or sets the response status.
        /// </summary>
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
