﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class ItemDescriptionList : IReturn<ItemDescriptionList_Response>
    {
    }
    [DataContract]
    public class ItemDescriptionList_Response
    {
        [DataMember]
        public List<Model.Types.ItemDescription> Items { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
