﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class AppointmentGetIdListForUser : IReturn<AppointmentGetIdListForUser_Response>
    {
        [DataMember]
        public int UserId { get; set; }
    }

    [DataContract]
    public class AppointmentGetIdListForUser_Response
    {
        [DataMember]
        public List<Types.Appointment> IdList { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
