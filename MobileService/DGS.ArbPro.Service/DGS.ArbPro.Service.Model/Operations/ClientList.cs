﻿namespace DGS.ArbPro.Service.Model.Operations
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using ServiceStack.ServiceHost;
    using ServiceStack.ServiceInterface.ServiceModel;

    /// <summary>
    /// The client list.
    /// </summary>
    [DataContract]
    public class ClientList : IReturn<ClientListResponse>
    {
    }

    /// <summary>
    /// The client list response.
    /// </summary>
    [DataContract]
    public class ClientListResponse
    {
        [DataMember]
        public List<Model.Types.Client> Clients { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
