﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class ToolList : IReturn<ToolList_Response>
    {
    }
    [DataContract]
    public class ToolList_Response
    {
        [DataMember]
        public List<Model.Types.Tool> Tools { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
