﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class UploadQuoteItemImage : IReturn<UploadQuoteItemImage_Response>, IRequiresRequestStream
    {
        //[DataMember]
        //public String ItemGuid { get; set; }
        //[DataMember]
        //public int PacketCount { get; set; }
        //[DataMember]
        //public int PacketNo { get; set; }
        //[DataMember]
        //public byte[] ImageBytes { get; set; }

        public System.IO.Stream RequestStream
        {
            get;
            set;
        }
    }

    [DataContract]
    public class UploadQuoteItemImage_Response
    {
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
