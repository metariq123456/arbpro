﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Operations
{
    using System.Runtime.Serialization;
    using ServiceStack.ServiceHost;
    using ServiceStack.ServiceInterface.ServiceModel;

    /// <summary>
    /// The risk assessment templates.
    /// </summary>
    [DataContract]
    public class SendQuoteForAppointment : IReturn<SendQuoteForAppointment_Response>
    {
        /// <summary>
        /// Gets or sets the the appointment Id.
        /// </summary>
        [DataMember]
        public Model.Types.EmailOptions Options { get; set; }


    }

    /// <summary>
    /// The risk assessment template response.
    /// </summary>
    [DataContract]
    public class SendQuoteForAppointment_Response
    {
        /// <summary>
        /// Gets or sets the response status.
        /// </summary>
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
