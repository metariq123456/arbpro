﻿
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class BookingDetail : IReturn<BookingDetail_Response>
    {
        [DataMember]
        public int QuoteId { get; set; }

        [DataMember]
        public int JobId { get; set; }

        [DataMember]
        public int JobSiteId { get; set; }

        [DataMember]
        public int WorkId { get; set; }

    }
    [DataContract]
    public class BookingDetail_Response
    {
        [DataMember]
        public BookingHeader BookingDetail { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }

    public class BookingHeader
    {
        public int QuoteId { get; set; }
        public string ClientName { get; set; }
        public string SiteAddress { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public int SiteId { get; set; }
        public string QuoteRefNo { get; set; }
        public List<BookedItem> BookedWorkItem { get; set; }

    }
    public class BookedItem
    {
        public int Id { get; set; }
        public int WorkId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Descriptioin { get; set; }
        public decimal Value { get; set; }
    }

}
