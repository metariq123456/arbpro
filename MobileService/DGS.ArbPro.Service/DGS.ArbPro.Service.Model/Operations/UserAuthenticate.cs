﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class UserAuthenticate : IReturn<UserAuthenticate_Response>
    {
        [DataMember]
        public String Username { get; set; }
        [DataMember]
        public String Password { get; set; }
    }
    [DataContract]
    public class UserAuthenticate_Response
    {
        [DataMember]
        public Model.Types.User User { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
