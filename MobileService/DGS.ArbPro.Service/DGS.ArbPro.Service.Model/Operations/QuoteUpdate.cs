﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class QuoteUpdate : IReturn<QuoteUpdate_Response>
    {
        [DataMember]
        public Model.Types.Quote Quote { get; set; }
        [DataMember]
        public String UserCR { get; set; }
    }
    [DataContract]
    public class QuoteUpdate_Response
    {
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
