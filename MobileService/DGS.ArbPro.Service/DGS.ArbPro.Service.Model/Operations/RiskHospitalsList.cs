﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class RiskHospitalsList : IReturn<RiskHospitalsList_Response>
    {
    }
    [DataContract]
    public class RiskHospitalsList_Response
    {
        [DataMember]
        public List<Model.Types.RiskHospital> Hospitals { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }
    }
}
