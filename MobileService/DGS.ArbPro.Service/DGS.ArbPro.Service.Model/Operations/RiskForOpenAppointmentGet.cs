﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ServiceStack.ServiceHost;
using ServiceStack.ServiceInterface.ServiceModel;

namespace DGS.ArbPro.Service.Model.Operations
{
    [DataContract]
    public class RiskForOpenAppointmentGet : IReturn<RiskForOpenAppointmentGet_Response>
    {
        [DataMember]
        public int UserId { get; set; }
    }

    [DataContract]
    public class RiskForOpenAppointmentGet_Response
    {
        [DataMember]
        public List<Model.Types.RiskAssessment> Assessments { get; set; }
        [DataMember]
        public ResponseStatus ResponseStatus { get; set; }

    }
}
