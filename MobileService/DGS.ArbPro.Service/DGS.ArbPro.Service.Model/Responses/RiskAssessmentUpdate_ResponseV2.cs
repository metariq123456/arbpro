﻿
namespace DGS.ArbPro.Service.Model.Operations.Responses
{
    public class RiskAssessmentUpdateV2_Response
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
