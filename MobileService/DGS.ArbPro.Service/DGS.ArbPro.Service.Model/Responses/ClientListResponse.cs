﻿namespace DGS.ArbPro.Service.Model.Operations.Responses
{
    using System.Collections.Generic;

    /// <summary>
    /// The client list response.
    /// </summary>
    public class ClientListResponse
    {
        public List<Model.Types.Client> Clients { get; set; }

        public ResponseStatus ResponseStatus { get; set; }
    }
}
