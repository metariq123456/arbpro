﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Service.Model;

namespace DGS.ArbPro.Service.Model.Operations.Responses
{
    public class AppointmentGetIdListForUser_Response
    {
        public List<Model.Types.Appointment> IdList { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
