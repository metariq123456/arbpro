﻿
namespace DGS.ArbPro.Service.Model.Operations.Responses
{
    public class RiskAssessmentUpdate_Response
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
