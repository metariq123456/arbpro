﻿// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
using System;
using DGS.ArbPro.Service.Model.Operations.Responses;
using DGS.ArbPro.Service.Model.Types;

namespace DGS.ArbPro.Service.Model.Operations.Responses
{
	public class RiskAssessmentGetResponse
	{
		public Risk RiskAssessment { get; set; }
		public ResponseStatus ResponseStatus { get; set; }
	}
}

