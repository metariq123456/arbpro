﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Operations.Responses
{
    public class GetCurrentUser_Response
    {
        public Service.Model.Types.User User { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
