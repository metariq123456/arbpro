﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Operations.Responses
{
    public class ItemDescriptionList_Response
    {
        public List<Service.Model.Types.ItemDescription> Items { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
