﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Operations.Responses
{
    public class ResponseError
    {
        #region Constructors
        public ResponseError(){}
        #endregion

        #region Properties
        public string ErrorCode { get; set; }
        public string FieldName { get; set; }
        public string Message { get; set; }
        #endregion
    }
}
