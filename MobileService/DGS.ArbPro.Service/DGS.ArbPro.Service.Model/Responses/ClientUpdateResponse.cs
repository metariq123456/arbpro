﻿namespace DGS.ArbPro.Service.Model.Operations.Responses
{
    public class ClientUpdateResponse
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
