﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Operations.Responses
{
    public class ResponseStatus
    {
        #region Constructors
        public ResponseStatus() { }

        public ResponseStatus(string errorCode)
        {
            this.ErrorCode = errorCode;
        }

        public ResponseStatus(string errorCode, string message)
        {
            this.ErrorCode = errorCode;
            this.Message = message;
        }
        #endregion

        #region Properties
        public string ErrorCode { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public List<ResponseError> Errors { get; set; }
        #endregion
    }
}
