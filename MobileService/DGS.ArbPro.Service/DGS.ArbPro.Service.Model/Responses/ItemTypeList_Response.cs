﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Operations.Responses
{
    public class ItemTypeList_Response
    {
        public List<Service.Model.Types.ItemType> Result { get; set; }
        public ResponseStatus ResponseStatus { get; set; }
    }
}
