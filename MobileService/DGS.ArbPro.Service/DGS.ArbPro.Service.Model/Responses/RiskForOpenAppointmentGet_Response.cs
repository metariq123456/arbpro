// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/MobileService/DGS.ArbPro.Service/DGS.ArbPro.Service.Model/Responses/RiskForOpenAppointmentGet_Response.cs#2 $
// $Author: Sean.Veck $
// $Date: 2014/07/31 $
// $Change: 22208 $
// *********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Service.Model;

namespace DGS.ArbPro.Service.Model.Operations.Responses
{
	public class RiskForOpenAppointmentGet_Response
	{
		public List<Model.Types.RiskAssessment> Assessments { get; set; }
		public ResponseStatus ResponseStatus { get; set; }
	}
}

