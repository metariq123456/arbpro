﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Operations.Responses
{
    public class UploadQuoteItemImage_Response
    {
        public ResponseStatus ResponseStatus { get; set; }
    }
}
