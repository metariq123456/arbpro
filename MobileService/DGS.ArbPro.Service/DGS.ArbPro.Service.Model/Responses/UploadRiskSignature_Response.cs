// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/MobileService/DGS.ArbPro.Service/DGS.ArbPro.Service.Model/Responses/UploadRiskSignature_Response.cs#2 $
// $Author: Sean.Veck $
// $Date: 2014/07/31 $
// $Change: 22208 $
// *********************************************************************
using System;

namespace DGS.ArbPro.Service.Model.Operations.Responses
{
	public class UploadRiskSignature_Response
	{
        public ResponseStatus ResponseStatus { get; set; }
	}
}

