﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DGS.ArbPro.Service.Test
{
    [TestClass]
    public class TestHostBase
        : ArbPro.Service.Interface.AppHost
    {
        protected const string InMemoryDb = ":memory:";

        public TestHostBase()
            : base()
        {
            
        }
    }
}
