﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DGS.ArbPro.Service.Test.Tests
{
    [TestClass]
    public class Quote : TestHostBase
    {
        [TestMethod]
        public void TestQuoteUpdate()
        {
            //Model.Types.Quote TestQuote = new Model.Types.Quote()
            //{
            //    QuoteId = 64,
            //    StaffNumber = 1,
            //    TimeHrs = 0.0f,
            //    Comments = "This is a comment",
            //    Tools = new string[] { "Hammer", "Axe", "Machine Gun", "Tomahawk" },
            //    Items = new Model.Types.QuoteItem[] { 
            //    new Model.Types.QuoteItem() { 
            //        ItemNumber = "1", 
            //        ItemType = "Enormo-Tree", 
            //        WorkDescription = "Burn it to the ground", 
            //        ItemValue = 10.0f, 
            //        Location = new Model.Types.GeoLocation() { 
            //            Latitude = 0, 
            //            Longitude = 0 
            //        }, 
            //        PreservationOrderStatus = Model.Types.QuoteItem.enumPreservationOrderStatus.Unknown, 
            //        Picture = new byte[0] 
            //    } 
            //}
            //};

            var Request = new ArbPro.Service.Model.Operations.QuoteUpdate() { Quote = null, UserCR = "7" };
            var Handler = new ArbPro.Service.Interface.Operations.QuoteUpdate();
            var Response = (ArbPro.Service.Model.Operations.QuoteUpdate_Response)Handler.any(Request);
            Assert.IsNotNull(Response);
        }
    }
}
