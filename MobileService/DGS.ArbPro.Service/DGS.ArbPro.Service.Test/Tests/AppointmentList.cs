﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DGS.ArbPro.Service.Test.Tests
{
    [TestClass]
    public class AppointmentList : TestHostBase
    {
        [TestMethod]
        public void TestGetAppointmentList()
        {
            var Request = new ArbPro.Service.Model.Operations.AppointmentGetIdListForUser() { UserId = 1 };
            var Handler = new ArbPro.Service.Interface.Operations.AppointmentGetIdListForUser();
            var Response = (ArbPro.Service.Model.Operations.AppointmentGetIdListForUser_Response)Handler.any(Request);
            Assert.IsTrue(Response.IdList.Count > 0);

            foreach (Model.Types.Appointment app in Response.IdList)
            {
                var R = new ArbPro.Service.Model.Operations.AppointmentMarkDownloaded() { AppointmentId = app.MasterCalendarId };
                var H = new ArbPro.Service.Interface.Operations.AppointmentMarkDownloaded();
                var Rsp = (ArbPro.Service.Model.Operations.AppointmentMarkDownloaded_Response)H.any(R);
                Assert.IsTrue(Rsp.Result);
                System.Diagnostics.Debug.WriteLine("Appointment: " + app.ToString());
            }
        }
    }
}
