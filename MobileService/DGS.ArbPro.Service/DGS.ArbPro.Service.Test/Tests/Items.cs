﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Service.Model.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DGS.ArbPro.Service.Test.Tests
{
    [TestClass]
    public class Items : TestHostBase
    {
        [TestMethod]
        public void TestItemLists()
        {
            var RequestItemDesc = new ArbPro.Service.Model.Operations.ItemDescriptionList();
            var HandlerItemDesc = new ArbPro.Service.Interface.Operations.ItemDescriptionList();
            var ResponseItemDesc = (ArbPro.Service.Model.Operations.ItemDescriptionList_Response)HandlerItemDesc.any(RequestItemDesc);
            Assert.IsTrue(ResponseItemDesc.Items.Count > 0);
            foreach (ItemDescription i in ResponseItemDesc.Items)
            {
                System.Diagnostics.Debug.WriteLine("Desc: " + i.Description);
            }

            var RequestItemType = new ArbPro.Service.Model.Operations.ItemTypeList();
            var HandlerItemType = new ArbPro.Service.Interface.Operations.ItemTypeList();
            var ResponseItemType = (ArbPro.Service.Model.Operations.ItemTypeList_Response)HandlerItemType.any(RequestItemType);
            Assert.IsTrue(ResponseItemType.Result.Count > 0);
            foreach (ItemType i in ResponseItemType.Result)
            {
                System.Diagnostics.Debug.WriteLine("Type: " + i.CommonName + " (" + i.TechnicalName + ")");
            }

            var RequestTool = new ArbPro.Service.Model.Operations.ToolList();
            var HandlerTool = new ArbPro.Service.Interface.Operations.ToolList();
            var ResponseTool = (ArbPro.Service.Model.Operations.ToolList_Response)HandlerTool.any(RequestTool);
            Assert.IsTrue(ResponseTool.Tools.Count > 0);
            foreach (Tool t in ResponseTool.Tools)
            {
                System.Diagnostics.Debug.WriteLine("Tool: " + t.Name);
            }
        }
    }
}
