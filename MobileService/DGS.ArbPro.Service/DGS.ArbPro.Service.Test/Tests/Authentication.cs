using System;
using DGS.ArbPro.Service.Interface;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServiceStack.ServiceClient.Web;
using ServiceStack.ServiceInterface.Auth;

namespace DGS.ArbPro.Service.Test.Tests
{
    [TestClass]
    public class Authentication : TestHostBase
    {
        [TestMethod]
        public void AuthenticateConnect()
        {
            ArbProAuthProvider auth = new ArbProAuthProvider();



            Assert.IsTrue(auth.TryAuthenticate(null, "jon1@microsec.co.uk", "ArbPr012"));
            Assert.IsFalse(auth.TryAuthenticate(null, "mobile_test@microsec.co.u", "Password1"));
        }
    }
}