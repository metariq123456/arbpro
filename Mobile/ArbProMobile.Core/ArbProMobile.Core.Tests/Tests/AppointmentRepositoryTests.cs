using NUnit.Framework;
using System.Collections.Generic;
using BL = DGS.ArbPro.Mobile.BL;
using DAL = DGS.ArbPro.Mobile.DAL;
using Model = DGS.ArbPro.Service.Model;
using Service = ArbProMobile.Core.ServiceLayer;

namespace ArbProMobile.Core.Tests
{
    [TestFixture]
    public class ApplicationManagerTests
    {
        [Test]
        public void TestAppointmentSave()
        {
            int ReturnValue;
            int NewId1;
            int NewId2;

            int AppointmentCount;
            List<BL.Appointment> Appointments;

            Model.Types.Appointment ModelAppointment;
            BL.Appointment BlAppointment;
            BL.Appointment BLAppointment2;
            BL.Appointment BLAppointment3;

            // Clear down the database so we're starting afresh
            DAL.Repository.Instance().ResetDatabase();


            //Save a new appointment
            ModelAppointment = new Model.Types.Appointment();
            ModelAppointment.Id = 1;
            ModelAppointment.ClientName = "Joe Bloggs";
            ModelAppointment.ClientAddressLocation = new Model.Types.GeoLocation();
            ModelAppointment.ClientAddressLocation.Latitude = 52.40f;
            ModelAppointment.ClientAddressLocation.Longitude = 1.56f;
            BlAppointment = new BL.Appointment(ModelAppointment);
            ReturnValue = DAL.Repository.Instance().AppointmentSave(BlAppointment);

            // check that a record was saved
            Assert.True(ReturnValue > 0);

            // save the appointment's RecId
            NewId1 = BlAppointment.RecId;

            // Save a second appointment
            ModelAppointment.Id = 100;
            ModelAppointment.ClientName = "Ethel Bloggs";
            ModelAppointment.ClientAddressLocation = new Model.Types.GeoLocation();
            ModelAppointment.ClientAddressLocation.Latitude = 54.40f;
            ModelAppointment.ClientAddressLocation.Longitude = 1.58f;
            BlAppointment = new BL.Appointment(ModelAppointment);
            ReturnValue = DAL.Repository.Instance().AppointmentSave(BlAppointment);

            // check that a record was saved
            Assert.True(ReturnValue > 0);

            // save the appointment's RecId
            NewId2 = BlAppointment.RecId;

            // Check that we have appointments in the database
            Appointments = DAL.Repository.Instance().AppointmentList();
            Assert.That(Appointments.Count > 0);

            // retrieve the first appointment, and make sure its properties match
            BLAppointment2 = DAL.Repository.Instance().AppointmentGet(NewId1);
            Assert.That(BLAppointment2.ClientName == "Joe Bloggs");
            Assert.AreEqual(BLAppointment2.ClientAddressLocation.Latitude, 52.40D);
            Assert.AreEqual(BLAppointment2.ClientAddressLocation.Longitude, 1.56D);

            // retrieve the second appointment, and make sure its properties match
            BLAppointment3 = DAL.Repository.Instance().AppointmentGet(NewId2);
            Assert.That(BLAppointment3.ClientName == "Ethel Bloggs");
            Assert.AreEqual(BLAppointment3.ClientAddressLocation.Latitude, 54.40D);
            Assert.AreEqual(BLAppointment3.ClientAddressLocation.Longitude, 1.58D);

            // make sure the second appointment doesn't have the same data as the first one
            Assert.AreNotEqual(BLAppointment2.ClientName, BLAppointment3.ClientName);
        }

        [Test]
        public void TestQuoteSave()
        {
            BL.Quote Quote1;
            BL.Quote Quote2;
            BL.QuoteItem QuoteItem1;

            int Quote1Id;

            int ReturnValue;

            // Clear down the database so we're starting afresh
            DAL.Repository.Instance().ResetDatabase();

            Quote1 = new BL.Quote();
            Quote1.QuoteId = 1;
            Quote1.Comments = "Comments1";

            QuoteItem1 = new BL.QuoteItem();
            QuoteItem1.ItemNumber = "T1";
            QuoteItem1.ItemType = "Beech";
            QuoteItem1.ItemValue = 23.4f;

            Quote1.Items.Add(QuoteItem1);
            
            ReturnValue = DAL.Repository.Instance().QuoteSave(Quote1);
            Assert.That(ReturnValue == 1);
            Quote1Id = Quote1.RecId;

            Quote2 = DAL.Repository.Instance().QuoteGet(Quote1Id);
            Assert.That(Quote2 != null);
            Assert.That(Quote2.Comments == "Comments1");
            Assert.That(Quote2.Items.Count == 1);
            Assert.That(Quote2.Items[0].ItemNumber == "T1");
        }
        
    }
}
