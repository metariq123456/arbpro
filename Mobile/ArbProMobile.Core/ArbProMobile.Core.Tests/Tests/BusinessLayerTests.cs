using System;
using System.Collections.Generic;
using NUnit.Framework;
using BL = DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.Core.Tests
{
	[TestFixture]
    public class BusinessLayerTests
	{
		[Test]
        public void AppointmentDownloadLogicTest ()
		{
			try
			{
				BL.BusinessLayer.DatabaseReset();
				BL.BusinessLayer.LoginToService("mobile_test@microsec.co.uk","Password1");
				Assert.True (true);
				BL.BusinessLayer.CancelActivity = false;
				int count = BL.BusinessLayer.AppointmentListDownload(1);
				Assert.That (count > 0);
				List<BL.Appointment> appointments = BL.BusinessLayer.AppointmentList();
				Assert.That (appointments != null);
				Assert.That (appointments.Count == count);
			}
			catch
			{
				Assert.Fail ();
			}
		}

		[Test]
		public void  ReferenceDataDownloadTest()
		{
			try
			{
				BL.BusinessLayer.DatabaseReset();
				BL.BusinessLayer.LoginToService("mobile_test@microsec.co.uk","Password1");
				Assert.True (true);
				BL.BusinessLayer.CancelActivity = false;
				BL.BusinessLayer.ReferenceDataDownload();
				List<BL.ItemType> types = BL.BusinessLayer.ItemTypeList();
				Assert.That (types != null);
				Assert.That (types.Count > 0);
				List<BL.ItemDescription> des = BL.BusinessLayer.ItemDescriptionList();
				Assert.That (des != null);
				Assert.That (des.Count > 0);
				List<BL.Tool> tools = BL.BusinessLayer.ToolList();
				Assert.That (tools != null);
				Assert.That (tools.Count > 0);
			}
			catch
			{
				Assert.Fail ();
			}
		}

		[Test]
		public void FullUpdateLogicTest ()
		{
			try
			{
				BL.BusinessLayer.DatabaseReset();
				BL.BusinessLayer.LoginToService("mobile_test@microsec.co.uk","Password1");
				Assert.True (true);
				BL.BusinessLayer.CancelActivity = false;
				int count = BL.BusinessLayer.AppointmentListDownload(1);
				Assert.That (count > 0);
				List<BL.Appointment> appointments = BL.BusinessLayer.AppointmentList();
				Assert.That (appointments != null);
				Assert.That (appointments.Count == count);
				List<BL.ItemType> types = BL.BusinessLayer.ItemTypeList();
				Assert.That (types != null);
				Assert.That (types.Count > 0);
				List<BL.ItemDescription> des = BL.BusinessLayer.ItemDescriptionList();
				Assert.That (des != null);
				Assert.That (des.Count > 0);
				List<BL.Tool> tools = BL.BusinessLayer.ToolList();
				Assert.That (tools != null);
				Assert.That (tools.Count > 0);
			}
			catch
			{
				Assert.Fail ();
			}
		}
	}
}
