using System;
using NUnit.Framework;
using System.Collections.Generic;
using BL = DGS.ArbPro.Mobile.BL;
using DAL = DGS.ArbPro.Mobile.DAL;
using Model = DGS.ArbPro.Service.Model;
using DGS.ArbPro.Mobile.Core.ServiceLayer;
using Types = DGS.ArbPro.Service.Model.Types;

namespace DGS.ArbPro.Mobile.Core.Tests.iOS
{
	[TestFixture]
    public class RestSharpServiceTests
	{
		
        [Test]
        public void TestConnection ()
		{
			RestConnection con = new RestConnection();
			con.GetConnection ("mobile_test@microsec.co.uk","Password1");
			
			Assert.That (con != null);
			Assert.That(con.Connected);
			
        }
		
		/// <summary>
		/// /*Tests the creation of a connection and then the appoinment download.*/
		/// </summary>
		[Test]
		public void TestAuthAndAppoinmentDownload()
		{
			//Create the connection
			RestConnection con = new RestConnection();
			con.GetConnection ("mobile_test@microsec.co.uk","Password1");
			
			Assert.That (con != null);
			Assert.That(con.Connected);
			
			//Get the current user
			BL.User user = con.AuthenticateUser();
			
			Assert.That(user != null);
			Assert.That (user.Id > 0);
			
			List<BL.Appointment> lst = con.GetAppointmentListForUser(1);
 			Assert.That (lst != null);
			Assert.That (lst.Count > 0);
		}
		
		[Test]
		/// <summary>
		/// Tests the appoinment download.
		/// </summary>
		public void TestAppoinmentDownload()
		{
			//Create the connection
			RestConnection con = new RestConnection();
			con.GetConnection ();
			
			Assert.That (con != null);
			Assert.That(con.Connected);
			
			List<BL.Appointment> lst = con.GetAppointmentListForUser(1);
			Assert.That (lst != null);
			Assert.That (lst.Count > 0);
		}
		
		[Test]
		/// <summary>
		/// Tests the get discription with authentication.
		/// </summary>
		public void TestGetDiscriptionWithAuthentication()
		{
			RestConnection con = new RestConnection();
			con.GetConnection ("mobile_test@microsec.co.uk","Password1");
			
			Assert.That (con != null);
			Assert.That(con.Connected);
			
			
			//Get the current user
			BL.User user = con.AuthenticateUser();
			
			Assert.That(user != null);
			Assert.That (user.Id > 0);
			
			List<BL.ItemDescription> lst = con.GetItemDescriptionListForClient();
			Assert.That (lst != null);
			Assert.That (lst.Count > 0);
		}
		
		[Test]
		/// <summary>
		/// Tests the get discription.
		/// </summary>
		public void TestGetDiscription()
		{
			RestConnection con = new RestConnection();
			con.GetConnection ();
			
			Assert.That (con != null);
			Assert.That(con.Connected);
			
			List<BL.ItemDescription> lst = con.GetItemDescriptionListForClient();
			Assert.That (lst != null);
			Assert.That (lst.Count > 0);
		}
		
		[Test]
		/// <summary>
		/// Tests the get Item Type list with authentication.
		/// </summary>
		public void TestGetItemTypeWithAuthentication()
		{
			RestConnection con = new RestConnection();
			con.GetConnection ("mobile_test@microsec.co.uk","Password1");
			
			Assert.That (con != null);
			Assert.That(con.Connected);
			
			
			//Get the current user
			BL.User user = con.AuthenticateUser();
			
			Assert.That(user != null);
			Assert.That (user.Id > 0);
			
			List<BL.ItemType> lst = con.GetItemTypeListForClient();
			Assert.That (lst != null);
			Assert.That (lst.Count > 0);
		}
		
		[Test]
		/// <summary>
		/// Tests the get Item Type list.
		/// </summary>
		public void TestGetItemType()
		{
			RestConnection con = new RestConnection();
			con.GetConnection ();
			
			Assert.That (con != null);
			Assert.That(con.Connected);
			
			List<BL.ItemType> lst = con.GetItemTypeListForClient();
			Assert.That (lst != null);
			Assert.That (lst.Count > 0);
		}
		
		[Test]
		/// <summary>
		/// Tests the get Tools list with authentication.
		/// </summary>
		public void TestGetToolsWithAuthentication()
		{
			RestConnection con = new RestConnection();
			con.GetConnection ("mobile_test@microsec.co.uk","Password1");
			
			Assert.That (con != null);
			Assert.That(con.Connected);
			
			
			//Get the current user
			BL.User user = con.AuthenticateUser();
			
			Assert.That(user != null);
			Assert.That (user.Id > 0);
			
			List<BL.Tool> lst = con.GetToolListForClient();
			Assert.That (lst != null);
			Assert.That (lst.Count > 0);
		}
		
		[Test]
		/// <summary>
		/// Tests the get Tools list.
		/// </summary>
		public void TestGetTools()
		{
			RestConnection con = new RestConnection();
			con.GetConnection ();
			
			Assert.That (con != null);
			Assert.That(con.Connected);
			
			List<BL.Tool> lst = con.GetToolListForClient();
			Assert.That (lst != null);
			Assert.That (lst.Count > 0);
		}
	}
}
