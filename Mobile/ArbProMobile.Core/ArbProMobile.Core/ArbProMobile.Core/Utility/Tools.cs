using System;
using System.Reflection;
using System.Collections.Generic;

namespace Microsec.Tools
{
    public static class ObjectTools
    {
        /// <summary>
        /// Migrates properties from one object to another, with an optional list of names of properties which 
        /// should be ignored.
        /// </summary>
        /// <param name="SourceObject">Source object.</param>
        /// <param name="TargetObject">Target object.</param>
        /// <param name="IgnoreProperties">String array of names of properties which should be ignored.</param>
     public static void MigrateProperties(Object sourceObject, Object targetObject, string[] ignoreProperties)
        {
            List<string> IgnoreList = new List<string>();
            PropertyInfo TargetProperty;

            // Add the ignore properties to a List so we can use Contains on it - probably faster than
            // manually searching the array in the loop further down.
            foreach (string S in ignoreProperties)
            {
                IgnoreList.Add(S);
            }


            PropertyInfo[] SourceProperties = sourceObject.GetType().GetProperties();
            for(int index = 0; index < SourceProperties.Length; index ++)
            {
				PropertyInfo Pi = SourceProperties[index];
                if (!IgnoreList.Contains(Pi.Name))
                {
                    TargetProperty = targetObject.GetType().GetProperty(Pi.Name);
                    if (TargetProperty != null)
                    {
                        TargetProperty.SetValue(targetObject, Pi.GetValue(sourceObject,SourceProperties ),SourceProperties);
                    }
                }
            }
        }
    }
}

