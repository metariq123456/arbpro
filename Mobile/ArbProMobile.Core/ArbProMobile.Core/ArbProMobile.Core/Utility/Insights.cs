// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Utility/Insights.cs#4 $
// // $Author: James $
// // $Date: 2015/10/18 $
// // $Change: 25439 $
// //*********************************************************************
//
//
using Xamarin;
using DGS.ArbPro.Mobile.Core.Enums;
using DGS.ArbPro.Mobile.BL;
using System;

namespace DGS.ArbPro.Mobile.Core
{
	public static class ArbProInsights
	{
		public static bool LevelOn (LoggingLevel level)
		{
			return (Settings.InsightsEnabled && Xamarin.Insights.IsInitialized && Globals.LoggingLevel >= level);
		}

		/// <summary>
		/// Initialises the Xamarin insights api.
		/// </summary>
		/// <param name="apiKey">The API key for the application.</param>
		public static void InitialiseInsights (string apiKey)
		{
			//Initialise Insights with API key provided
			Insights.Initialize (apiKey);

			// Create an anonymous identity for Insights to handle any reports from before login
			Insights.Identify ("Anonymous", "Username", "Unknown");
			Core.ArbProInsights.LogActivity (string.Format ("Service URI={0}", BL.Settings.ServiceURI), DGS.ArbPro.Mobile.Core.Enums.LoggingLevel.Info);
		}

		/// <summary>
		/// Sets the identity for the Xamain Insights user.
		/// </summary>
		/// <param name="userName">User name.</param>
		/// <param name="displayName">Display name.</param>
		public static void SetIdentity(string userName, string displayName)
		{
			//Ensure that the identity is set unless logging is turned off
			if (LevelOn (LoggingLevel.Fatal)) {
				Xamarin.Insights.Identify (userName, Xamarin.Insights.Traits.Name, displayName);
			}
		}

		/// <summary>
		/// Logs the activity message to insights if the current level of logging permits.
		/// </summary>
		/// <param name="activity">Activity message to be logged</param>
		/// <param name="logLevel">Min log level setting required to log the activity</param>
		public static void LogActivity (string activity, LoggingLevel logLevel)
		{
			if (LevelOn (logLevel)) {
				Xamarin.Insights.Track (activity);
			}
		}

		private static Xamarin.ITrackHandle _TimerHandle = null;
		/// <summary>
		/// Gets a newly created activity timer for the activity name if the loggin level permits.
		/// NOTE: This method can return a null reference if the logging level is set higher than specified.
		/// </summary>
		/// <returns>The a Xamarin.Insights ITrackHandler implementation</returns>
		/// <param name="activityName">Activity name.</param>
		/// <param name="logLevel">Log level, defaults to Fatal to ensure that the activity timer is returned unless logging is turned off</param>
		public static Xamarin.ITrackHandle GetActivityTimer(string activityName, LoggingLevel logLevel = LoggingLevel.Fatal)
		{
			if (LevelOn (logLevel)) {
				_TimerHandle = Xamarin.Insights.TrackTime (activityName);
				_TimerHandle.Start();
			} else {
				_TimerHandle = null;
			}
			return _TimerHandle;
		}

		/// <summary>
		/// Logs an ERROR level error to the insights portal.
		/// </summary>
		/// <param name="ex">Ex.</param>
		/// <param name="logLevel">Log level. Defaults to Fatal</param>
		/// <param name="message">An additional message that will be logged to insights seperatly. Defaults to an empty string</param>
		public static void  LogErrorError(Exception ex, LoggingLevel logLevel = LoggingLevel.Fatal, string message = "")
		{
			LogError (ex, Insights.Severity.Error, logLevel, message);
		}

		/// <summary>
		/// Logs an error, with a specified severity, to the insights portal, if the log level matches.
		/// </summary>
		/// <param name="ex">Ex.</param>
		/// <param name="severityLevel">Severity level. Xamarin Insights severity level.</param>
		/// <param name="logLevel">Log level. The logging level to use.</param>
		/// <param name="message">Optional additional message.</param>
		public static void LogError(Exception ex, Xamarin.Insights.Severity severityLevel, LoggingLevel logLevel = LoggingLevel.Fatal, string message = "(No specific message)")
		{
			if(LevelOn(logLevel))
			{
				Xamarin.Insights.Report(ex, severityLevel);
				if(!string.IsNullOrEmpty(message))
					Xamarin.Insights.Track (string.Format("Level {0} error '{1}' Exception details:{2}",severityLevel,message,ex.ToString()));
			}
		}
	}
}

