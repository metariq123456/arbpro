//*********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/DataAccessLayer/Repository.cs#21 $
// $Author: ivan $
// $Date: 2016/10/13 $
// $Change: 27237 $
//*********************************************************************
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using DGS.ArbPro.Mobile.BL;
using DGS.ArbPro.Mobile.Core.Enums;
using DGS.ArbPro.Mobile.Service;

namespace DGS.ArbPro.Mobile.DAL
{
    /// <summary>
    /// The Repository provides the interface to the database layer.
    /// </summary>
    public class Repository
    {
        private DL.Database _Db;
        private static Repository _Instance;

        /// <summary>
        /// Indicates whether this repository is for demo data
        /// </summary>
        /// <value><c>true</c> if demo repository; otherwise, <c>false</c>.</value>
        private bool DemoRepository { get; set; }

        #region Constructors

        public static Repository Instance ()
        {
            bool UseDemoData;
            UseDemoData = (Globals.LoginStatus == Globals.LoggedInStatus.NotLoggedIn);

            if (_Instance == null || _Instance.DemoRepository != UseDemoData) {
                _Instance = new Repository (UseDemoData);
            }
            return _Instance;
        }

        Repository (bool DemoData)
        {
            DemoRepository = DemoData;

            if (DemoRepository) {
                DatabaseReset ();
                TestDataCreate ();
            } else {
                _Db = new DGS.ArbPro.Mobile.DL.Database (DatabaseFilePath);
            }
        }

        private string DatabaseFilePath {
            get {
                string DatabaseFilename;
                if (!DemoRepository) {
                    DatabaseFilename = "ArbProMobile-live.db3";
                } else
                    DatabaseFilename = "ArbProMobile-demo.db3";


#if NETFX_CORE
                var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, DatabaseFilename);
#else

#if SILVERLIGHT
                // Windows Phone expects a local path, not absolute
                var path = DatabaseFilename;
#else

#if __ANDROID__
                // Just use whatever directory SpecialFolder.Personal returns
                string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); ;
#else
                // we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
                // (they don't want non-user-generated data in Documents)
                string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
                string libraryPath = Path.Combine (documentsPath, "../Library/"); // Library folder
#endif
                var path = Path.Combine (libraryPath, DatabaseFilename);
#endif

#endif
                return path;
            }
        }

        #endregion

        #region General methods

        public void DatabaseClose ()
        {
            _Db.Close ();
            _Instance = null;
        }

        /// <summary>
        /// Deletes the database file and creates a new one.
        /// </summary>
        /// <remarks>USE WITH CAUTION! This will - as the name suggests - DELETE ALL DATA!</remarks>
        public void DatabaseReset ()
        {
            if (File.Exists (DatabaseFilePath)) {
                if (_Db != null) {
                    _Db.Close ();
                }
                File.Delete (DatabaseFilePath);
            }

            _Db = new DL.Database (DatabaseFilePath);
        }

        #endregion

        #region Appointment Methods

        /// <summary>
        /// Lists all appointments in the database without any filtering applied
        /// </summary>
        /// <returns>The list.</returns>
        public List<Appointment> AppointmentList ()
        {
            List<Appointment> lst = (List<Appointment>)_Db.GetItems<Appointment> ();
            return lst;

        }

        /// <summary>
        /// Lists all appointments, with the filters set up in Globals applied
        /// </summary>
        /// <returns>The list filtered.</returns>
        public List<Appointment> AppointmentListFiltered ()
        {
            List<Appointment> ReturnValue;
            List<AppointmentStatus> Statuses = new List<AppointmentStatus> ();
            ReturnValue = new List<Appointment> ();
            SQLite.TableQuery<Appointment> Query;
            DateTime TodayStart;
            DateTime TodayEnd;

            // Create an array of statuses to search on, depending on the filters set
            if (Globals.ListFilter.AppointmentStatusPending)
                Statuses.Add (AppointmentStatus.Pending);

            if (Globals.ListFilter.AppointmentStatusCompleted)
                Statuses.Add (AppointmentStatus.Completed);

            if (Globals.ListFilter.AppointmentStatusCancelled)
                Statuses.Add (AppointmentStatus.Cancelled);

            if (Globals.ListFilter.AppointmentStatusPartiallyUploaded)
                Statuses.Add (AppointmentStatus.PartiallyUploaded);

            if (Globals.ListFilter.AppointmentStatusUploaded)
                Statuses.Add (AppointmentStatus.Uploaded);

            // We should build the query dynamically, but the following works.
            if (Globals.ListFilter.TodaysAppointments) {
                TodayStart = DateTime.Now.Date;
                TodayEnd = DateTime.Now.Date;
                TodayEnd = TodayEnd.AddDays (1);
                TodayEnd = TodayEnd.AddSeconds (-1);

                Query = _Db.Table<Appointment> ().Where (a => Statuses.Contains (a.Status) && a.StartDateTime >= TodayStart && a.StartDateTime <= TodayEnd).OrderBy (a => a.StartDateTime);
            } else {
                Query = _Db.Table<Appointment> ().Where (a => Statuses.Contains (a.Status)).OrderBy (a => a.StartDateTime);
            }

            foreach (Appointment a in Query) {
                if (a.StartDateTime.Kind == DateTimeKind.Unspecified || a.StartDateTime.Kind == DateTimeKind.Utc) {
                    a.StartDateTime = DateTime.SpecifyKind (a.StartDateTime.ToLocalTime (), DateTimeKind.Local);
                }
                ReturnValue.Add (a);
            }

            return ReturnValue;
        }

        /// <summary>
        /// Retrieves the appointment with the record id supplied
        /// </summary>
        /// <returns>The get.</returns>
        /// <param name="id">Identifier.</param>
        public Appointment AppointmentGet (int id)
        {
            Appointment ReturnValue;
            ReturnValue = _Db.GetItem<Appointment> (id);

            if (ReturnValue.LocationLatitude != 0.0D) {
                ReturnValue.ClientAddressLocation = new GeoLocation (ReturnValue.LocationLatitude, ReturnValue.LocationLongitude);
            }

            ReturnValue.Dirty = false;

            return ReturnValue;
        }

        public bool AppointmentExistsForServiceId (int id)
        {
            Appointment a = _Db.GetAppointmentForServiceId<Appointment> (id);

            if (a == null)
                return false;
            else
                return true;

        }

        public Appointment AppointmentForServiceId (int id)
        {
            return _Db.GetAppointmentForServiceId<Appointment> (id);
        }

		public List<Appointment> AppointmentGetNotPending()
		{
			List<Appointment> ReturnValue;
			ReturnValue = new List<Appointment>();
			SQLite.TableQuery<Appointment> AppointmentQuery;

			AppointmentQuery = _Db.Table<Appointment>().Where(a => (a.Status != AppointmentStatus.Pending));

			foreach (Appointment a in AppointmentQuery)
			{
				ReturnValue.Add(a);
			}

			return ReturnValue;
		}

        public int AppointmentSave (Appointment item)
        {
            int ReturnValue;


            if (item.ClientAddressLocation != null) {
                item.LocationLatitude = item.ClientAddressLocation.Latitude;
                item.LocationLongitude = item.ClientAddressLocation.Longitude;
            }

            ReturnValue = _Db.SaveItem (item);

            item.Dirty = false;

            return ReturnValue;
        }

        public bool AppointmentDelete (Quote quote)
        {
            try {
                this.RiskAssementDeleteForQuote (quote);
                _Db.Execute ("DELETE from QuoteItem WHERE QuoteRecId = ?", quote.RecId);
                _Db.Execute ("DELETE from QuoteTool WHERE QuoteRecId = ?", quote.RecId);
                _Db.Execute ("DELETE from Appointment WHERE QuoteNo = ?", quote.QuoteId);
                _Db.Execute ("DELETE from Quote WHERE RecId = ?", quote.RecId);
                return true;

            } catch (Exception ex) {
                if (Core.ArbProInsights.LevelOn (LoggingLevel.Error)) {
                    Xamarin.Insights.Report (ex, Xamarin.Insights.Severity.Error);
                }
                return false;
            }
        }

        #endregion

        #region Quote Methods

        public Quote QuoteGet (int id)
        {
            Quote ReturnValue;
            ReturnValue = _Db.GetItem<Quote> (id);

            ReturnValue.Assessment = this.RiskAssessmentForQuoteGet (ReturnValue.QuoteId);

            var query = _Db.Table<QuoteItem> ().Where (qi => qi.QuoteRecId == id);
            foreach (QuoteItem i in query) {
                if (i.LocationLatitude != 0.0D) {
                    i.Location = new GeoLocation (i.LocationLatitude, i.LocationLongitude);
                }

                ReturnValue.Items.Add (i);
            }

            var query1 = _Db.Table<QuoteTool> ().Where (qi => qi.QuoteRecId == id);
            foreach (var i in query1) {
                ReturnValue.Tools.Add (i);
            }

            ReturnValue.Dirty = false;

            return ReturnValue;
        }

        public Quote QuoteForAppoinmentGet (int quoteNo)
        {
            Quote returnValue = null;

            returnValue = _Db.Table<Quote> ().FirstOrDefault (q => q.QuoteId == quoteNo);

            if (returnValue != null) {

                returnValue.Assessment = this.RiskAssessmentForQuoteGet (returnValue.QuoteId);

                var query = _Db.Table<QuoteItem> ().Where (qi => qi.QuoteRecId == returnValue.RecId);
                foreach (QuoteItem i in query) {
                    if (i.LocationLatitude != 0.0D) {
                        i.Location = new GeoLocation (i.LocationLatitude, i.LocationLongitude);
                    }

                    returnValue.Items.Add (i);
                }

                var query1 = _Db.Table<QuoteTool> ().Where (qi => qi.QuoteRecId == returnValue.RecId);
                foreach (var i in query1) {
                    returnValue.Tools.Add (i);
                }
            }

            return returnValue;
        }

        public List<Quote> QuoteForAppointmentNotPendingList ()
        {
            List<Quote> ReturnValue;
            ReturnValue = new List<Quote> ();
            SQLite.TableQuery<Appointment> AppointmentQuery;

            AppointmentQuery = _Db.Table<Appointment> ().Where (a => (a.Status != AppointmentStatus.Pending));

            SQLite.TableQuery<Quote> QuoteQuery;
            foreach (Appointment a in AppointmentQuery) {

                QuoteQuery = _Db.Table<Quote> ().Where (q => q.QuoteId == a.QuoteNo);
                foreach (Quote q in QuoteQuery) {
                    ReturnValue.Add (this.QuoteGet (q.RecId));
                }
            }
            return ReturnValue;
        }

        public List<Quote> QuotesForDeletion ()
        {
            List<Quote> ReturnValue;
            ReturnValue = new List<Quote> ();
            SQLite.TableQuery<Appointment> AppointmentQuery;

            DateTime UploadTimeToDeleteBefore;
            TimeSpan AgeToDelete;

            // If the retain days isn't 0, calculate the time before which to delete from now
            if (BL.Settings.DataRetainDays != 0) {
                AgeToDelete = new TimeSpan (BL.Settings.DataRetainDays, 0, 0, 0);
                UploadTimeToDeleteBefore = DateTime.Now.Subtract (AgeToDelete);
                // Else make the time before which to delete one minute in the future - this makes sure we delete appointment uploaded
                // in the current minute
            } else {
                AgeToDelete = new TimeSpan (BL.Settings.DataRetainDays, 0, 0, 1, 0);
                UploadTimeToDeleteBefore = DateTime.Now.Add (AgeToDelete);
            }

            // Candidates are appointments which were uploaded before the time above, or cancelled and their
            // appointment time before the time calculated above.
            AppointmentQuery = _Db.Table<Appointment> ().Where (a => (
                (a.UploadTime < UploadTimeToDeleteBefore && a.Status == AppointmentStatus.Uploaded) ||
                (a.CancelTime < UploadTimeToDeleteBefore && a.Status == AppointmentStatus.Cancelled)));

            SQLite.TableQuery<Quote> QuoteQuery;
            foreach (Appointment a in AppointmentQuery) {

                QuoteQuery = _Db.Table<Quote> ().Where (q => q.QuoteId == a.QuoteNo);
                foreach (Quote q in QuoteQuery) {
                    ReturnValue.Add (this.QuoteGet (q.RecId));
                }
            }
            return ReturnValue;
        }

        public int QuoteSave (Quote item)
        {
            int ReturnValue;

            // Note that we need to begin a transaction for obvious rollback reasons - and also makes database updates
            // much faster.
            _Db.BeginTransaction ();
            ReturnValue = _Db.SaveItem (item);


            item.Assessment.QuoteNo = item.QuoteId;
            RiskAssessmentSave (item.Assessment);

            foreach (QuoteItem MyItem in item.Items) {
                MyItem.QuoteRecId = item.RecId;
                QuoteItemSave (MyItem);
            }

            foreach (QuoteItem MyItem in item.DeletedItems) {
                // We should in theory be able to use _Db.DeleteItem but this doesn't work
                _Db.Execute ("DELETE from QuoteItem WHERE RecId = ?", MyItem.RecId);
            }

            foreach (QuoteTool MyTool in item.Tools) {
                MyTool.QuoteRecId = item.RecId;
                _Db.SaveItem (MyTool);
            }

            foreach (QuoteTool MyTool in item.DeletedTools) {
                // We should in theory be able to use _Db.DeleteTool but this doesn't work
                _Db.Execute ("DELETE from QuoteTool WHERE RecId = ?", MyTool.RecId);
            }

            _Db.Commit ();

            item.Dirty = false;

            return ReturnValue;
        }

        #endregion

        #region RiskAssementMethods

        public void RiskAssementDeleteUploaded ()
        {
            SQLite.TableQuery<RiskAssessment> ItemQuery;

            ItemQuery = _Db.Table<RiskAssessment> ().Where (item => item.HasUploaded == true && item.HasUploadedSignature == true);
            foreach (RiskAssessment q in ItemQuery) {
                foreach (AssessmentSection s in q.Criterion) {
                    _Db.Execute ("DELETE from AssessmentItem WHERE SectionRecId = ?", s.RecId);
                }
                _Db.Execute ("DELETE from AssessmentAdditionalItem WHERE AssessmentRecId = ?", q.RecId);
                _Db.Execute ("DELETE from AssessmentSection WHERE AssessmentRecId = ?", q.RecId);
                // We should in theory be able to use _Db.DeleteItem but this doesn't work
                _Db.Execute ("DELETE from RiskAssessment WHERE RecId = ?", q.RecId);
            }
        }

        public void RiskAssementDeleteForQuote (Quote q)
        {
            SQLite.TableQuery<RiskAssessment> ItemQuery;

            ItemQuery = _Db.Table<RiskAssessment> ().Where (item => item.QuoteNo == q.QuoteId);

            foreach (RiskAssessment ra in ItemQuery) {
                foreach (AssessmentSection s in ra.Criterion) {
                    _Db.Execute ("DELETE from AssessmentItem WHERE SectionRecId = ?", s.RecId);
                }
                _Db.Execute ("DELETE from AssessmentAdditionalItem WHERE AssessmentRecId = ?", ra.RecId);
                _Db.Execute ("DELETE from AssessmentSection WHERE AssessmentRecId = ?", ra.RecId);
                _Db.Execute ("DELETE from RiskAssessment WHERE RecId = ?", ra.RecId);
            }
        }

        public int RiskAssessmentSave (RiskAssessment assessment)
        {
            int returnValue;
            returnValue = _Db.SaveItem (assessment);

            foreach (AssessmentSection s in assessment.Criterion) {
                s.AssessmentRecId = assessment.RecId;
                AssessmentSectionSave (s);
            }

            foreach (AssessmentAdditionalItem item in assessment.FurtherRisks) {
                item.AssessmentRecId = assessment.RecId;
                _Db.SaveItem (item);
            }

            return returnValue;
        }

        public RiskAssessment RiskAssessmentForQuoteGet (int quoteNo)
        {
            RiskAssessment retVal = null;
            retVal = _Db.Table<RiskAssessment> ().FirstOrDefault (q => q.QuoteNo == quoteNo);

            if (retVal == null)
                return null;

            retVal.Criterion = LoadAssessmentSections (retVal.RecId);

            var otherQuery = _Db.Table<AssessmentAdditionalItem> ().Where (oth => oth.AssessmentRecId == retVal.RecId);
            foreach (AssessmentAdditionalItem itm in otherQuery) {
                retVal.FurtherRisks.Add (itm);
            }

            return retVal;
        }

        public List<RiskAssessment> UploadedRiskAssessmentsGet ()
        {
            List<RiskAssessment> retVal = new List<RiskAssessment> ();
            SQLite.TableQuery<RiskAssessment> Query;

            Query = _Db.Table<RiskAssessment> ().Where (a => a.HasUploaded == true);

            foreach (RiskAssessment r in Query) {
                retVal.Add (r);
            }

            return retVal;
        }

        public List<AssessmentSection> LoadAssessmentSectionsTemplate ()
        {
            List<AssessmentSection> data = LoadAssessmentSections (-1);
            List<AssessmentSection> retVal = new List<AssessmentSection> ();

            AssessmentSection sec = null;
            AssessmentItem item = null;

            foreach (AssessmentSection s in data) {
                sec = new AssessmentSection ();
                sec.Title = s.Title;
                sec.AdditionalDetail = s.AdditionalDetail;
                sec.AssessmentItems = new List<AssessmentItem> ();

                foreach (AssessmentItem i in s.AssessmentItems) {
                    item = new AssessmentItem ();
                    item.Title = i.Title;
                    item.RefCode = i.RefCode;

                    sec.AssessmentItems.Add (item);
                }
                retVal.Add (sec);
            }
            return retVal;
        }

        public List<AssessmentSection> LoadAssessmentSections (int assessmentRecId)
        {
            List<AssessmentSection> retVal = new List<AssessmentSection> ();
            var query = _Db.Table<AssessmentSection> ().Where (qi => qi.AssessmentRecId == assessmentRecId);
            foreach (AssessmentSection i in query) {
                var itemsQuery = _Db.Table<AssessmentItem> ().Where (itm => itm.SectionRecId == i.RecId);
                foreach (AssessmentItem item in itemsQuery) {
                    i.AssessmentItems.Add (item);
                }
                retVal.Add (i);
            }
            return retVal;
        }

        public int AssessmentSectionSave (AssessmentSection criterion)
        {
            int retValue;
            retValue = _Db.SaveItem (criterion);

            foreach (AssessmentItem itm in criterion.AssessmentItems) {
                itm.SectionRecId = criterion.RecId;
                _Db.SaveItem (itm);
            }
            return retValue;
        }

        public void RiskAssessmentDeleteUploaded ()
        {
            SQLite.TableQuery<RiskAssessment> RiskQuery;
            SQLite.TableQuery<AssessmentSection> SectionQuery;
            //SQLite.TableQuery<AssessmentItem> CriterionQuery;

            RiskQuery = _Db.Table<RiskAssessment> ().Where (item => item.HasUploaded == true);
            foreach (RiskAssessment r in RiskQuery) {
                //Delete all sections and items for the uploaded risk assessment
                SectionQuery = _Db.Table<AssessmentSection> ().Where (item => item.AssessmentRecId == r.RecId);
                foreach (AssessmentSection s in SectionQuery) {
                    _Db.Execute ("DELETE from AssessmentItem WHERE SectionRecId = ?", s.RecId);
                }
                _Db.Execute ("DELETE from AssessmentSection WHERE AssessmentRecId = ?", r.RecId);

                //Only delete the risk assessment record if the signature has been uploaded.
                if (r.HasUploadedSignature)
                    _Db.Execute ("DELETE from RiskAssessment WHERE RecId = ?", r.RecId);
            }
        }

        #endregion

        #region QuoteItem Methods

        /// <summary>
        /// Saves a QuoteItem
        /// </summary>
        /// <returns>The number of records saved</returns>
        /// <param name="item">The item to save</param>
        /// <remarks>
        /// We need to transfer the Location to the persisted lat and long fields before saving
        /// </remarks>

        public int QuoteItemSave (QuoteItem item)
        {
            if (item.Location != null && item.Location.Longitude != 0) {
                item.LocationLatitude = item.Location.Latitude;
                item.LocationLongitude = item.Location.Longitude;
            }
            return _Db.SaveItem (item);
        }

        /// <summary>
        /// Get a list of all quote items that have been marked as uploaded
        /// </summary>
        /// <returns>The list of items uploaded get.</returns>
        public List<QuoteItem> QuoteItemsUploadedGet ()
        {
            List<QuoteItem> ReturnValue = new List<QuoteItem> ();
            SQLite.TableQuery<QuoteItem> ItemQuery;

            ItemQuery = _Db.Table<QuoteItem> ().Where (item => item.Uploaded == true);

            foreach (QuoteItem q in ItemQuery) {
                ReturnValue.Add (q);
            }

            return ReturnValue;
        }

        public void QuoteItemDeleteUploaded ()
        {
            SQLite.TableQuery<QuoteItem> ItemQuery;

            ItemQuery = _Db.Table<QuoteItem> ().Where (item => item.Uploaded == true && item.PictureUploaded == true);
            foreach (QuoteItem q in ItemQuery) {
                // We should in theory be able to use _Db.DeleteItem but this doesn't work
                _Db.Execute ("DELETE from QuoteItem WHERE RecId = ?", q.RecId);
            }
        }

        /// <summary>
        /// Method to get a list of all quote items that are on the device that have a picture associated with them
        /// </summary>
        /// <returns>The quote items list.</returns>
        public List<QuoteItem> ValidQuoteItemsGet ()
        {
            List<QuoteItem> lst = (List<QuoteItem>)_Db.GetItems<QuoteItem> ();
            return lst;
        }

        #endregion

        #region ItemType Methods

        public int ItemTypeSaveList (List<ItemType> items)
        {
            int ReturnValue = 0;
            _Db.BeginTransaction ();
            foreach (ItemType Item in items) {
                ReturnValue += _Db.SaveItem (Item);
            }
            _Db.Commit ();

            return ReturnValue;
        }

        public int HosptialSaveList (List<RiskHospital> items)
        {
            int ReturnValue = 0;
            _Db.BeginTransaction ();
            foreach (RiskHospital Item in items) {
                ReturnValue += _Db.SaveItem (Item);
            }
            _Db.Commit ();

            return ReturnValue;
        }

        public int AerialPlanSaveList (List<RiskAerialRescuePlan> items)
        {
            int ReturnValue = 0;
            _Db.BeginTransaction ();
            foreach (RiskAerialRescuePlan Item in items) {
                ReturnValue += _Db.SaveItem (Item);
            }
            _Db.Commit ();

            return ReturnValue;
        }

        public List<ItemType> ItemTypeList ()
        {
            List<ItemType> ReturnValue = new List<ItemType> ();
            var query = _Db.Table<ItemType> ().OrderBy (it => it.CommonName);

            foreach (var i in query) {
                ReturnValue.Add (i);
            }
            return ReturnValue;
            //            return (List<ItemType>)_Db.GetItems<ItemType>();
        }

        #endregion

        #region Tool Methods

        public int ToolSaveList (List<Tool> items)
        {
            int ReturnValue = 0;
            _Db.BeginTransaction ();
            foreach (Tool Item in items) {
                ReturnValue += _Db.SaveItem (Item);
            }
            _Db.Commit ();

            return ReturnValue;
        }

        public List<Tool> ToolList ()
        {
            List<Tool> ReturnValue = new List<Tool> ();
            var query = _Db.Table<Tool> ().OrderBy (it => it.Name);

            foreach (var i in query) {
                ReturnValue.Add (i);
            }
            return ReturnValue;

        }

        public List<RiskHospital> HospitalList ()
        {
            List<RiskHospital> ReturnValue = new List<RiskHospital> ();
            var query = _Db.Table<RiskHospital> ().OrderBy (it => it.Name);

            foreach (var i in query) {
                ReturnValue.Add (i);
            }
            return ReturnValue;

        }

        public List<RiskAerialRescuePlan> AerialRescuePlanList ()
        {
            List<RiskAerialRescuePlan> ReturnValue = new List<RiskAerialRescuePlan> ();
            var query = _Db.Table<RiskAerialRescuePlan> ().OrderBy (it => it.Plan);

            foreach (var i in query) {
                ReturnValue.Add (i);
            }
            return ReturnValue;

        }

        public List<BL.Client> ClientList ()
        {
            List<BL.Client> ReturnValue = new List<BL.Client> ();
            var query = _Db.Table<BL.Client> ().OrderBy (it => it.Surname).ThenBy (it => it.Forename).ThenBy (it => it.BusinessName);

            foreach (var i in query) {
                ReturnValue.Add (i);
            }
            return ReturnValue;

        }

        #endregion

        #region ItemDescription Methods

        public int ItemDescriptionSaveList (List<ItemDescription> items)
        {
            int ReturnValue = 0;
            _Db.BeginTransaction ();
            foreach (ItemDescription Item in items) {
                ReturnValue += _Db.SaveItem (Item);
            }
            _Db.Commit ();

            return ReturnValue;
        }

        public List<ItemDescription> ItemDescriptionList ()
        {
            List<ItemDescription> ReturnValue = new List<ItemDescription> ();
            var query = _Db.Table<ItemDescription> ().OrderBy (it => it.Description);

            foreach (var i in query) {
                ReturnValue.Add (i);
            }
            return ReturnValue;

        }

        #endregion

        #region CalendarItem Methods

        public int CalendarItemSaveList (List<BL.CalendarItem> items)
        {
            int ReturnValue = 0;
            _Db.BeginTransaction ();
			_Db.Execute("DELETE FROM CalendarItem");
            foreach (BL.CalendarItem Item in items) {
                ReturnValue += _Db.SaveItem (Item);
            }
            _Db.Commit ();

            return ReturnValue;
        }

		public int CalendarItemSave(BL.CalendarItem item)
		{
			int ReturnValue = 0;
			_Db.BeginTransaction();

			if (item.Summary.Equals("Pending Mobile Quote Appointment"))
			{
				_Db.Execute("DELETE FROM CalendarItem WHERE Summary = ?", item.Summary);
			}

			ReturnValue += _Db.SaveItem(item);

			_Db.Commit();

			return ReturnValue;
		}

        public List<BL.CalendarItem> CalendarItemList ()
        {
            List<BL.CalendarItem> ReturnValue = new List<BL.CalendarItem> ();
            var query = _Db.Table<BL.CalendarItem> ().OrderBy (it => it.StartTime);

            foreach (var i in query) {
                // Ensure that we've got the correct kind of dates
                if (i.StartTime.Kind == DateTimeKind.Unspecified || i.StartTime.Kind == DateTimeKind.Utc) {
                    i.StartTime = DateTime.SpecifyKind (i.StartTime.ToLocalTime (), DateTimeKind.Local);
                }

                if (i.EndTime.Kind == DateTimeKind.Unspecified || i.EndTime.Kind == DateTimeKind.Utc) {
                    i.EndTime = DateTime.SpecifyKind (i.EndTime.ToLocalTime (), DateTimeKind.Local);
                }
                ReturnValue.Add (i);
            }
            return ReturnValue;

        }

        #endregion

        #region RiskTemplate Methods

        public int RiskTemplateSaveList(List<BL.RiskTemplate> items)
		{
			int ReturnValue = 0;
			_Db.BeginTransaction();
			_Db.Execute("DELETE FROM RiskTemplate");
			foreach (BL.RiskTemplate Item in items)
			{
				ReturnValue += _Db.SaveItem(Item);
			}
			_Db.Commit();

			return ReturnValue;
		}

		public List<BL.RiskTemplate> RiskTemplateList()
		{
			List<BL.RiskTemplate> ReturnValue = new List<BL.RiskTemplate>();
			var query = _Db.Table<BL.RiskTemplate>().OrderBy(it => it.Id);

			foreach (var i in query)
			{
				ReturnValue.Add(i);
			}

			return ReturnValue;
		}

		#endregion

		#region Reference Data Methods

		public bool SaveRiskReferenceData (List<AssessmentSection> sections, List<RiskHospital> hospitals, List<RiskAerialRescuePlan> plans)
        {
            try {
                _Db.BeginTransaction ();
                SQLite.TableQuery<AssessmentSection> deletetbl = _Db.Table<AssessmentSection> ().Where (itm => itm.AssessmentRecId == -1);

                foreach (AssessmentSection t in deletetbl) {
                    _Db.Execute ("DELETE from AssessmentItem WHERE SectionRecId = ?", t.RecId);
                }
                _Db.Execute ("DELETE from AssessmentSection WHERE AssessmentRecId = ?", -1);

                foreach (AssessmentSection s in sections) {
                    s.AssessmentRecId = -1;
                    AssessmentSectionSave (s);
                }

                // Clear the ItemTypes table
                _Db.Execute ("DELETE FROM RiskHospital");
                foreach (RiskHospital item in hospitals) {
                    this.SaveRiskHospital (item);
                }

                _Db.Execute ("DELETE FROM RiskAerialRescuePlan");
                foreach (RiskAerialRescuePlan item in plans) {
                    this.SaveRiskAerialRescuePlan (item);
                }

                _Db.Commit ();
                return true;
            } catch (Exception) {
                return false;
            }

        }

        public bool SaveReferenceData (List<ItemType> types, List<ItemDescription> descriptions, List<Tool> tools, List<BL.Client> clients)
        {
            try {
                _Db.BeginTransaction ();

                // Clear the ItemTypes table
                _Db.Execute ("DELETE FROM ItemType");
                foreach (ItemType item in types) {
                    this.SaveItemType (item);
                }

                // Clear the ItemDescriptions table
                _Db.Execute ("DELETE FROM ItemDescription");
                foreach (ItemDescription item in descriptions) {
                    this.SaveItemDescription (item);
                }

                // Clear the Tools table
                _Db.Execute ("DELETE FROM Tool");
                foreach (Tool item in tools) {
                    this.SaveTool (item);
                }

                // Clear the Clients table
                _Db.Execute ("DELETE FROM Client");
                foreach (BL.Client item in clients) {
                    this.SaveClient (item);
                }

                _Db.Commit ();
                return true;
            } catch (Exception) {
                _Db.Rollback ();
                return false;
            }

        }

        #region Save reference data items

        public int SaveItemType (ItemType item)
        {
            return _Db.SaveItem<ItemType> (item);
        }

        public int SaveItemDescription (ItemDescription item)
        {
            return _Db.SaveItem<ItemDescription> (item);
        }

        public int SaveTool (Tool item)
        {
            return _Db.SaveItem<Tool> (item);
        }

        public int SaveClient (BL.Client item)
        {
            return _Db.SaveItem<BL.Client> (item);
        }

        public int SaveRiskAerialRescuePlan (RiskAerialRescuePlan item)
        {
            return _Db.SaveItem<RiskAerialRescuePlan> (item);
        }

        public int SaveRiskHospital (RiskHospital item)
        {
            return _Db.SaveItem<RiskHospital> (item);
        }

        #endregion

        #region Delete reference data items

        public void DeleteItemType (ItemType item)
        {
            _Db.Delete<ItemType> (item);
        }

        public void DeleteItemDescription (ItemDescription item)
        {
            _Db.Delete<ItemDescription> (item);
        }

        public void DeleteTool (Tool item)
        {
            _Db.Delete<Tool> (item);
        }

        #endregion

        #endregion

        #region Utility Methods

        public void SetupTest ()
        {
            Appointment apt = (Appointment)_Db.GetItems<Appointment> ().FirstOrDefault ();
            Quote qt = QuoteForAppoinmentGet (apt.QuoteNo);

            List<ItemType> types = ItemTypeList ();
            List<ItemDescription> descriptions = ItemDescriptionList ();

            //int toolsCount = tools.Count - 1;
            int typesCount = types.Count - 1;
            int desCount = descriptions.Count - 1;

            Random myRandom;
            myRandom = new Random ();
            //MyRandom.Next (0, array.Length - 1)
            QuoteItem itm = null;
            int start = qt.Items.Count + 1;
            int testCount = start + 21;
            for (int index = start; index < testCount; index++) {
                itm = new QuoteItem ();

                itm.ItemNumber = string.Format ("Test{0}", index);
                itm.ItemType = types [myRandom.Next (0, typesCount)].CommonName;
                itm.WorkDescription = descriptions [myRandom.Next (0, desCount)].Description;
                itm.ItemValue = index * 10;
                itm.PreservationOrderStatus = EnumPreservationOrderStatus.No;

                qt.Items.Add (itm);
            }

            QuoteSave (qt);
        }


        public void TestDataCreate ()
        {
            Xamarin.ITrackHandle TrackHandle = null;
            if (Settings.InsightsEnabled) {
                TrackHandle = Xamarin.Insights.TrackTime ("Generating demo data");
            }

            BL.Appointment Appt;
            BL.Quote Quote;
            //BL.RiskAssessment Assessment;

            string [] Titles = { "Mr", "Mrs", "Dr" };
			string [] FirstNames = { "A.", "J.", "S.", "C.", "B.", "K." };
            string [] Names = { "Arthur", "Jones", "Smith", "Snodgrass", "Cameron", "Butler" };
            string [] HouseNumbers = { "1", "2", "30", "45", "100", "35", "Dunroamin", "Sunnyplace", "Elfwood", "Glebe House" };
            string [] Streets = { "High Street", "Little Lane", "Beeding Lane", "West Road", "Havant Road" };
            string [] Towns = { "Havant", "Bitterne", "Bishops Waltham", "East Meon", "Hurn", "Warblington" };
            string [] Counties = { "Hants", "Wilts", "West Sussex" };
            string [] PostcodeOutward = { "PO10", "PO9", "SO23", "SO1", "BO33", "PO19" };
            string [] PostcodeInward = { "1GG", "6JY", "4EE", "6TT", "2QE", "9PO" };
            string [] TelnoArea = { "023 92", "023 80", "01243" };
            string [] Telno = { "223566", "554789", "118934", "445578" };
            string [] EmailName = { "jgeee", "arthur1", "bobrr", "jgl", "jobbyee" };
            string [] EmailDomain = { "btinertnet.com", "gmail.com", "yahoo.co.uk", "waitrose.com" };
            string [] Subjects = {
                "Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum.",
                "Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum.",
                "Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Duis sagittis ipsum.  Nulla quis sem at nibh elementum imperdiet. "
            };
            string [] ItemTypes = {
                "Dogwood:Cornus sanguinea",
                "Bay:Laurus nobilis",
                "Hazel:Corylus avellana",
                "Irish Ivy:Hedera hibernica",
                "Ash:",
                "Willow:",
                "Silver birch:",
                "Beech:"
            };
            string [] ItemDescriptions = { "Trim back", "Fell and remove stump", "Fell", "Prune", "Annihilate" };
            decimal [] ItemValues = { 12.0m, 55m, 150m, 15m, 90m, 150m };
            int [] ToolNumbers = { 1, 2, 3, 4 };
            string [] Tools = { "Van", "Chainsaw", "Hand saw", "Chipper", "Stump grinder", "Teapot" };
            string [] AppointmentComments = {
                "",
                "The owner is very precious about his lawn, so do not walk on it unless it is very dry. Also he has dogs which are allowed to roam and mustn't be let out of the back garden.",
                "Beware of the sheep.",
                "Beware of the cows.",
                "Beware of the llamas.",
                "Beware of the cat.",
                "Customer will be out - go round the back.",
                "Very precious about lawn."
            };
            EnumPreservationOrderStatus [] TPOStatuses = {
                EnumPreservationOrderStatus.Yes,
                EnumPreservationOrderStatus.No,
                EnumPreservationOrderStatus.Unknown
            };
            AppointmentStatus [] AppointmentStatuses = { AppointmentStatus.Pending, AppointmentStatus.Completed, AppointmentStatus.Uploaded };

            CalendarItemType [] CalendarItemTypes = { CalendarItemType.Work, CalendarItemType.Quote, CalendarItemType.Generic };

            int RecsSaved;
            DateTime TmpDate;
            DateTime ApptDate;

            TmpDate = System.DateTime.Now.AddDays (-1);
            ApptDate = new DateTime (TmpDate.Year, TmpDate.Month, TmpDate.Day, 18, 0, 0);

            for (var i = 1; i <= 30; i++) {

				string title = RandomString(Titles);
				string firstName = RandomString(FirstNames);
				string surname = RandomString(Names);

				Appt = new Appointment()
				{
					Id = i,
					// Loop through the statuses
					Status = AppointmentStatuses[i % 3],
					Title = title,
					FirstName = firstName,
					Surname = surname,
					ClientName = string.Format("{0} {1} {2}", title, firstName, surname),
					ClientAddress1 = string.Format("{0} {1}", RandomString(HouseNumbers), RandomString(Streets)),
					ClientAddressCounty = string.Format("{0}", RandomString(Counties)),
					ClientAddressPostCode = string.Format("{0} {1}", RandomString(PostcodeOutward), RandomString(PostcodeInward)),
					ClientAddressTown = string.Format("{0}", RandomString(Towns)),
					ClientEmail = string.Format("{0}@{1}", RandomString(EmailName), RandomString(EmailDomain)),
					ClientIdNo = i,
					ClientTelephone = string.Format("{0} {1}", RandomString(TelnoArea), RandomString(Telno)),
					ClientMobile = string.Format("{0} {1}", RandomString(TelnoArea), RandomString(Telno)),
					MasterCalendarId = i,
					Subject = string.Format(RandomString(Subjects)),
					SurveyorId = 1,
					Comments = AppointmentComments[i % AppointmentComments.Length],
					UserCR = "1",
					Duration = 30,
					StartDateTime = ApptDate,
					QuoteNo = i
                };

                Appt.ClientAddressLocation = new GeoLocation ();
                Appt.ClientAddressLocation.Latitude = 51.179531;
                Appt.ClientAddressLocation.Longitude = -1.826355;

                // Subtract one hour on for each appointment, and move on one day every 10 appointments - 
                // resetting back to the start time
                if (i % 10 == 0) {
                    ApptDate = ApptDate.AddDays (1);
                    ApptDate = ApptDate.AddHours (+10);
                }

                ApptDate = ApptDate.AddHours (-1);

                RecsSaved = AppointmentSave (Appt);
                if (RecsSaved == 0) {
                    throw new Exception (string.Format ("Error saving test appointment {0}", i));
                }

                // Create a Quote for the appointment
                Quote = new Quote () {
                    AppointmentRecId = Appt.RecId,
                    Comments = String.Format ("Comments for quote {0}", i),
                    QuoteId = i,
                    StaffNumber = 1
                };


                // Add QuoteItems
                for (var j = 1; j <= 5; j++) {
                    string MyItemType;
                    MyItemType = RandomString (ItemTypes);
                    var QuoteItem = new QuoteItem () {
                        ItemNumber = string.Format ("T{0}", j),
                        ItemType = MyItemType.Split (':') [0],
                        WorkDescription = RandomString (ItemDescriptions),
                        ItemValue = RandomDecimal (ItemValues),
                        PreservationOrderStatus = RandomTPO (TPOStatuses),
                    };
                    Quote.Items.Add (QuoteItem);
                }

                for (var j = 1; j <= 3; j++) {
                    var QuoteTool = new QuoteTool () {
                        Name = RandomString (Tools),
                        Number = RandomInt (ToolNumbers),

                    };
                    Quote.Tools.Add (QuoteTool);
                }

                //Add a new debug Risk Assessment
                Quote.Assessment = new RiskAssessment (true);

                RecsSaved = QuoteSave (Quote);
                if (RecsSaved == 0) {
                    throw new Exception (string.Format ("Error saving test quote {0}", i));
                }
                Thread.Sleep (100);
            }

            BL.RiskHospital Hospital;
            List<BL.RiskHospital> HospitalsToSave = new List<BL.RiskHospital> ();
            Hospital = new RiskHospital () { Id = 1, Name = "St James" };
            HospitalsToSave.Add (Hospital);
            Hospital = new RiskHospital () { Id = 2, Name = "Queen Alexandra" };
            HospitalsToSave.Add (Hospital);
            Hospital = new RiskHospital () { Id = 3, Name = "St Richards" };
            HospitalsToSave.Add (Hospital);
            Hospital = new RiskHospital () { Id = 4, Name = "St Wilfrids" };
            HospitalsToSave.Add (Hospital);
            Hospital = new RiskHospital () { Id = 5, Name = "Royal Sussex" };
            HospitalsToSave.Add (Hospital);
            Hospital = new RiskHospital () { Id = 6, Name = "Royal Eessex" };
            HospitalsToSave.Add (Hospital);
            Hospital = new RiskHospital () { Id = 7, Name = "Colchester General" };
            HospitalsToSave.Add (Hospital);
            Hospital = new RiskHospital () { Id = 8, Name = "Southampton General" };
            HospitalsToSave.Add (Hospital);
            Hospital = new RiskHospital () { Id = 9, Name = "St Geroge War Memorial" };
            HospitalsToSave.Add (Hospital);
            Hospital = new RiskHospital () { Id = 10, Name = "St Trinians" };
            HospitalsToSave.Add (Hospital);

            HosptialSaveList (HospitalsToSave);


            BL.RiskAerialRescuePlan Plan;
            List<BL.RiskAerialRescuePlan> PlansToSave = new List<BL.RiskAerialRescuePlan> ();
            Plan = new RiskAerialRescuePlan () {
                Id = 1,
                Plan = "Spare kit available and nominated rescue person. Rescue line installed"
            };
            PlansToSave.Add (Plan);
            Plan = new RiskAerialRescuePlan () { Id = 1, Plan = "Spare kit available. Rescue line installed" };
            PlansToSave.Add (Plan);
            Plan = new RiskAerialRescuePlan () { Id = 1, Plan = "Nominated rescue person. Rescue line installed" };
            PlansToSave.Add (Plan);
            Plan = new RiskAerialRescuePlan () { Id = 1, Plan = "Rescue line installed" };
            PlansToSave.Add (Plan);
            Plan = new RiskAerialRescuePlan () { Id = 1, Plan = "Spare kit available and nominated rescue person." };
            PlansToSave.Add (Plan);

            AerialPlanSaveList (PlansToSave);

            // Save the item types
            BL.ItemType It;
            List<ItemType> ItemTypesToSave = new List<ItemType> ();
            It = new BL.ItemType () { CommonName = "English oak", TechnicalName = "Quercus robur" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Lucombe oak", TechnicalName = "Quercus X hispanica Lucombeana " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Hybrid oak", TechnicalName = "Quercus x munziii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Nikau palm", TechnicalName = "Rhopalostylis sapida" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Stags Horn ", TechnicalName = "Rhus typhina " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Robinia", TechnicalName = "Robinia pseudoacacia" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Robinia", TechnicalName = "Robinia pseudoacacia frisia" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Willow", TechnicalName = "Salix" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "White Willow ", TechnicalName = "Salix alba " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Eared willow", TechnicalName = "Salix aurita" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Weeping willow ", TechnicalName = "Salix babylonica " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Goat willow", TechnicalName = "Salix caprea" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Crack Willow ", TechnicalName = "Salix fragilis " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Contorted willow", TechnicalName = "Salix matsudana Tortuosa " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Elder ", TechnicalName = "Sambucus nigra" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Pagoda tree", TechnicalName = "Saphora japonica" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Umbrella tree", TechnicalName = "Schefflera actinophylla" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Coast Redwood", TechnicalName = "Sequoia sempervirens" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Wellingtonia ", TechnicalName = "Sequoiadendron giganteum" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Judas Tree ", TechnicalName = "Sercis silquatrum " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Whitebeam", TechnicalName = "Sorbus aria" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Rowan", TechnicalName = "Sorbus aucuparia" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "service tree", TechnicalName = "Sorbus domestica" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Sitka mountain ash", TechnicalName = "Sorbus sitchensis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Wild Service Tree", TechnicalName = "Sorbus torminalis " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Bastard Service Tree", TechnicalName = "Sorbus x thuringiaca" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Huamanquero", TechnicalName = "Styloceras laurifolium" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Swamp Cypress", TechnicalName = "taxodium distichum" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Swamp cypress", TechnicalName = "Taxodium distichum" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Fastigate yew  ", TechnicalName = "Taxus bacata fastigiata" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Yew", TechnicalName = "Taxus baccata " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Purple pod terminalia", TechnicalName = "Terminalia prunioides" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Silver terminalia", TechnicalName = "Terminalia sericea" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Damson", TechnicalName = "Terminalia seriocarpa" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Test", TechnicalName = "Test" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Western red cedar", TechnicalName = "Thuja plicata" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Lime ", TechnicalName = "Tilia " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Small leaved lime ", TechnicalName = "Tilia cordata " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Common lime", TechnicalName = "Tilia europea " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Large Leaved Lime ", TechnicalName = "Tilia platyphyllos " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Silver lime ", TechnicalName = "Tillia tomentosa " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Hythe", TechnicalName = "Tom Tom Tree" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Hemlock ", TechnicalName = "Tsuga" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Eastern hemlock  ", TechnicalName = "Tsuga canadensis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Carolina hemlock  ", TechnicalName = "Tsuga carolinniana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Western hemlock", TechnicalName = "Tsuga heterophylla" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Mountain hemlock", TechnicalName = "Tsuga mertensiana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Elm ", TechnicalName = "Ulmus" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Wych elm", TechnicalName = "Ulmus glabra" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "English Elm ", TechnicalName = "Ulmus procera" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Kamahi", TechnicalName = "Weinmannia racemosa" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Palm tree", TechnicalName = "Welfia regia" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Joshua tree", TechnicalName = "Yucca brevifolia" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Yucca", TechnicalName = "Yucca carnerosana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Fir", TechnicalName = "Abies " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Pacific silver fir", TechnicalName = "Abies amabilis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "White fir", TechnicalName = "Abies concolor" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Grand fir ", TechnicalName = "Abies grandis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Guatemalan Fir", TechnicalName = "Abies guatemalensis var. jaliscana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Subalpine fir", TechnicalName = "Abies lasiocarpa" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Noble Fir", TechnicalName = "Abies noblis " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Causican Fir ", TechnicalName = "Abies nordmanniana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Bulls-horn Acacia", TechnicalName = "Acacia collinsii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Mimosa", TechnicalName = "Acacia dealbata" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Camel thorn acacia", TechnicalName = "Acacia erioloba" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Robinia ", TechnicalName = "Acacia False  " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Aromo", TechnicalName = "Acacia farnesiana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Blackwood", TechnicalName = "Acacia melanoxylon" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Black thorn", TechnicalName = "Acacia mellifera" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Prickly wattle", TechnicalName = "Acacia tetragonphylla" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Umbrella thorn acacia", TechnicalName = "Acacia tortilis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Maple ", TechnicalName = "Acer " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Field Maple ", TechnicalName = "Acer campestre " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Paper bark maple  ", TechnicalName = "Acer griseum " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Beech", TechnicalName = "Fagus " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Common Beech", TechnicalName = "Fagus sylvatica  " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Weeping beech", TechnicalName = "Fagus sylvatica pendula" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Purple beech", TechnicalName = "Fagus sylvatica purpurea" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Fig tree", TechnicalName = "Ficus bullenei" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Namaqua fig", TechnicalName = "Ficus cordata" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Laurel fig", TechnicalName = "Ficus ilicina" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Oje - a fig tree", TechnicalName = "Ficus insipida" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Sycamore fig", TechnicalName = "Ficus sycomorus" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Curtain fig", TechnicalName = "Ficus virens" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Ash", TechnicalName = "Fraxinus " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "White Ash ", TechnicalName = "Fraxinus americana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Narrow Leaved Ash ", TechnicalName = "Fraxinus angustifolia" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Common Ash", TechnicalName = "Fraxinus excelsior " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Weeping ash", TechnicalName = "Fraxinus excelsior Pendula" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Mana ash", TechnicalName = "Fraxinus ornus" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Kebacar", TechnicalName = "Gluta pubescens" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Snowdrop Tree", TechnicalName = "Halesia carolina" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Pacra", TechnicalName = "Hesperomeles lanuginosa" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Holly", TechnicalName = "Ilex aquifolium " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Walnut", TechnicalName = "Juglans regia" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Black walnut ", TechnicalName = "Juglnas nigra" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Juniper ", TechnicalName = "Juniperus " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Canary Island juniper", TechnicalName = "Juniperus cedrus" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Chinese Juniper ", TechnicalName = "Juniperus chinensis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Juniper", TechnicalName = "Juniperus communis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Weeping Juniper ", TechnicalName = "Juniperus flaccida" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Jalisco juniper", TechnicalName = "Juniperus jaliscana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Japanese Maple ", TechnicalName = "Acer palmatum " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Purple Japanese Maple", TechnicalName = "Acer palmatum autropurpureum" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Norway Maple ", TechnicalName = "Acer platanoides " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Crimson King ", TechnicalName = "Acer platanoides Crimson King" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Drummond Norway maple", TechnicalName = "Acer platanoides drummondii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Sycamore", TechnicalName = "Acer pseudoplatanus" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Red maple", TechnicalName = "Acer rubrum " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Silver maple", TechnicalName = "Acer saccharinum " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Sugar Maple ", TechnicalName = "Acer saccharum " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Baobab", TechnicalName = "Adansonia digitata" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Common Horse chestnut  ", TechnicalName = "Aesculus hippocastanum " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Tree Of Heaven ", TechnicalName = "Alianthus altissima" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Italian Alder ", TechnicalName = "Alnus cordata" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Common Alder", TechnicalName = "Alnus glutinosa " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Grey Alder", TechnicalName = "Alnus incana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Red alder", TechnicalName = "Alnus rubra" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "mespilus snowy", TechnicalName = "Amelanchier " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "White pear", TechnicalName = "Apodytes dimidiata" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Monkey puzzle", TechnicalName = "Araucaria araucana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Hoop pine", TechnicalName = "Araucaria cunninghamii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Red tulip oak", TechnicalName = "Argyrodendron peralatum" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Pencil pine", TechnicalName = "Athrotaxis cupressoides" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Yellow birch  ", TechnicalName = "Betual alleghaniensis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Paper birch ", TechnicalName = "Betual papyrifera" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Himalayan birch ", TechnicalName = "Betual utilis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Birch ", TechnicalName = "Betula" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Birch", TechnicalName = "Betula Jacquemontii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Dwarf birch", TechnicalName = "Betula nana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Silver birch ", TechnicalName = "Betula pendula" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Downy birch", TechnicalName = "Betula pubescens" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Large-leafed orange mangrove", TechnicalName = "Bruguiera gymnorhiza" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "White cypress pine", TechnicalName = "Callitris columellaris" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Southern cypress pine", TechnicalName = "Callitris columnaris" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Japanese Cedar", TechnicalName = "Calocedrus decurrens" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Hornbeam ", TechnicalName = "Carpinus " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Hornbeam ", TechnicalName = "Carpinus betula " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Hickory ", TechnicalName = "Carya " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Shag Bark Hickory  ", TechnicalName = "Carya ovata " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Sweet Chestnut", TechnicalName = "Castanea sativa " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Indian Bean ", TechnicalName = "Catalpa bignoniodes" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Cedar", TechnicalName = "Cedrus" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Atlas cedar", TechnicalName = "Cedrus atlantica" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Blue Atlas cedar ", TechnicalName = "Cedrus atlantica glauca" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Deodar cedar ", TechnicalName = "Cedrus deodara" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Katsura Tree", TechnicalName = "cerciiphyllum japonicum " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Lawson cypress", TechnicalName = "Chamaecyparis lawsoniana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Alaska cedar", TechnicalName = "Chamaecyparis nootkaensis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Lawson cypress", TechnicalName = "chamecyparis lawsoniana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Bamboo", TechnicalName = "Chusquea quila" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Huayruro", TechnicalName = "Citharexyllum dentatum" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Buttercup Tree", TechnicalName = "Cochlospermum vitifolium" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Large-leaved commiphora", TechnicalName = "Commiphora anacardifolia" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Hazel", TechnicalName = "Corylus avellana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Hawthorn ", TechnicalName = "Crataegus monogyna " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Chinese Fir ", TechnicalName = "Cunninghamia lanceolata" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Leyland Cypress ", TechnicalName = "Cupressus leylandii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Monterey Cypress ", TechnicalName = "Cupressus macrocarpa" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Nootka Cypress ", TechnicalName = "Cupressus nootkatensis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Silver tree fern", TechnicalName = "Cyathea dealbata" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Quince", TechnicalName = "Cydonia oblonga" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Kahikatea", TechnicalName = "Dacrycarpus dacrydioides" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Dragon tree", TechnicalName = "Dracaena draco" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Canelo", TechnicalName = "Drimys winteri" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Heather tree", TechnicalName = "Erica arborea" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "River red gum", TechnicalName = "Eucalyptus camaldulensis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Sugar gum", TechnicalName = "Eucalyptus cladocalyx" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Red flowering gum", TechnicalName = "Eucalyptus ficifolia" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Swamp gum", TechnicalName = "Eucalyptus grandis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Blue mountain ash", TechnicalName = "Eucalyptus oreades" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Scarlet gum", TechnicalName = "Eucalyptus phoenicia" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Mountain ash", TechnicalName = "Eucalyptus regnans" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Dawyck beech", TechnicalName = "Fagus sylvatica dawyck" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Plum", TechnicalName = "Prunus domestica " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Almond", TechnicalName = "Prunus dulcis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Potugese laurel", TechnicalName = "Prunus lusitanica" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Bird cherry", TechnicalName = "Prunus padus" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Peach", TechnicalName = "Prunus persica " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Sargent cherry", TechnicalName = "Prunus sargentii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Blackthorn", TechnicalName = "Prunus spinosa" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Willow leaved Pear", TechnicalName = "Pryrus salicifolia " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Douglas fir", TechnicalName = "Pseudotsuga menziesii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Pear", TechnicalName = "Pyrus " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Common Pear ", TechnicalName = "Pyrus communis " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Oak", TechnicalName = "Quercus " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Algerian oak", TechnicalName = "Quercus canariensis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Chestnut-leaved oak ", TechnicalName = "Quercus castenofolia " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Turkey oak", TechnicalName = "Quercus cerris " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Scarlet oak ", TechnicalName = "Quercus coccinea" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Turbinella oak", TechnicalName = "Quercus dumosa turbinella" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Northern pin oak ", TechnicalName = "Quercus ellipsoidalis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Holm oak ", TechnicalName = "Quercus ilex " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Shingle oak ", TechnicalName = "Quercus imbricaria" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Laurel oak", TechnicalName = "Quercus laurifolia" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Burr oak", TechnicalName = "Quercus macrocarpa" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Pin oak", TechnicalName = "Quercus palustris " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Sessile oak", TechnicalName = "Quercus petraea" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Willow oak", TechnicalName = "Quercus phellos" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Pencil cedar", TechnicalName = "Juniperus virginiana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Laburnam", TechnicalName = "Laburnum anagyroides" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Larch ", TechnicalName = "Larix " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "European larch", TechnicalName = "Larix decidua" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Japanese larch", TechnicalName = "Larix kamempferi" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Subalpine larch", TechnicalName = "Larix lyalli" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Bay", TechnicalName = "Laurus nobilis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Manuka or tea tree", TechnicalName = "Leptospermum scoparium" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Privet", TechnicalName = "Ligustrum" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Sweet Gum ", TechnicalName = "Liquidambar styraciflua" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Tulip Tree", TechnicalName = "Liriodendron tulipifera" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Gingko ", TechnicalName = "Madenhair tree" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Ringwood", TechnicalName = "Maerua schinzii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Apple ", TechnicalName = "Malus" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Crab Apple", TechnicalName = "Malus sylvestris " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Medlar ", TechnicalName = "Mespilus germanica " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Dawn redwood", TechnicalName = "metasequoia glyptostroboides" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Ohia", TechnicalName = "Metrosideros polymorpha" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Mullberry ", TechnicalName = "Morus " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "White Mullberry ", TechnicalName = "Morus Alba " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Black Mullberry  ", TechnicalName = "Morus Nigra " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Ñire", TechnicalName = "Nothofagus antarctica" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Myrtle beech", TechnicalName = "Nothofagus cunninghamii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Red beech", TechnicalName = "Nothofagus fusca" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Silver beech", TechnicalName = "Nothofagus menziesii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Antarctic Beech", TechnicalName = "Nothofagus moorei" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Roble beech", TechnicalName = "Nothofagus obliua" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () {
                CommonName = "Mountain beech",
                TechnicalName = "Nothofagus solandri var. cliffortioides"
            };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Nipah palm", TechnicalName = "Nypa fruticans" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Balsa", TechnicalName = "Ochroma pyramidale" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Persian ironwood", TechnicalName = "Parrota persica " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Weeping wattle", TechnicalName = "Peltophorum africanum" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Norway spruce", TechnicalName = "Picea abies" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Brewers Weeping Spruce", TechnicalName = "Picea Breweriana pendula" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Engelmann spruce", TechnicalName = "Picea engelmannii" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "White spruce", TechnicalName = "Picea glauca" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Sitka spruce", TechnicalName = "Picea sitchensis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Pine", TechnicalName = "Pinus " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Canary pine", TechnicalName = "Pinus canariensis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Arolla Pine", TechnicalName = "Pinus cembra" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Pinyon pine", TechnicalName = "Pinus cembroides" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Lodgepole pine", TechnicalName = "Pinus contorta" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Decumbent pine", TechnicalName = "Pinus culminicola" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Jeffrey pine", TechnicalName = "Pinus jeffreyi" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Korean pine ", TechnicalName = "Pinus koraiensis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Bosnina pine ", TechnicalName = "Pinus lecodermis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Montezuma pine", TechnicalName = "Pinus montezumae" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Bishop pine", TechnicalName = "Pinus mourciata" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Austrian pine", TechnicalName = "Pinus nigra" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Corsican pine", TechnicalName = "Pinus nigra corsicana " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Japanese white pine ", TechnicalName = "Pinus paviflora" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "maritime pine ", TechnicalName = "Pinus pinaster " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Stone pine ", TechnicalName = "Pinus pinea" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Western yellow pine ", TechnicalName = "Pinus ponderosa" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Monterey pine", TechnicalName = "Pinus radiata " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Weymouth Pine", TechnicalName = "Pinus Strobus" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Scots pine", TechnicalName = "Pinus sylvestris" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Chinese pine ", TechnicalName = "Pinus tabuliformis" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Ocote pine", TechnicalName = "Pinus teocote" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Bhutan pine", TechnicalName = "Pinus wallichiana" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Oriental plane ", TechnicalName = "Platanus orientalis " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "London plane", TechnicalName = "Platanus x hispanica " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Balsam Poplar", TechnicalName = "Populus " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Poplar", TechnicalName = "Populus " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Black poplar ", TechnicalName = "Populus Nigra  " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Aspen", TechnicalName = "Populus tremula" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Lombardy poplar", TechnicalName = "Poulus lombardy " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Cherry ", TechnicalName = "Prunus " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Wild Cherry ", TechnicalName = "Prunus avium " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Purple leaved plum", TechnicalName = "Prunus cerasifera " };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Bigleaf maple", TechnicalName = "Acer macrophyllum" };
            ItemTypesToSave.Add (It);
            It = new BL.ItemType () { CommonName = "Box Elder ", TechnicalName = "Acer negundo" };
            ItemTypesToSave.Add (It);
            //            foreach (string MyType in ItemTypes)
            //            {
            //
            //                var It = new BL.ItemType() {CommonName = MyType.Split(':')[0], TechnicalName=MyType.Split(':')[1]};
            //                ItemTypesToSave.Add(It);
            //
            //            }

            ItemTypeSaveList (ItemTypesToSave);

            // Save the tools
            BL.Tool ThisTool;
            List<Tool> ToolsToSave = new List<Tool> ();
            ThisTool = new BL.Tool () { Name = "Land Rover" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Hand Pruning Tools" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Climbing kits" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Cabstar" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Planting Tools / Materials" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "MEWP" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Towed Chipper " };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Tracked Chipper" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Stump Grinder " };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Chainsaw Top Handled" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Chainsaw Medium" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Chainsaw Large" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Hedge Cutters" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Clear up Kit" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Grubbing tools" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = " Lowering Kit Small" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Lowering Kit Large" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Full Chapter 8 Signage" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Men @ Work & Cones" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Ladders" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Steps" };
            ToolsToSave.Add (ThisTool);
            ThisTool = new BL.Tool () { Name = "Power Barrow" };
            ToolsToSave.Add (ThisTool);

            ToolSaveList (ToolsToSave);

            List<BL.ItemDescription> DescsToAdd = new List<BL.ItemDescription> ();
            BL.ItemDescription Desc;

            Desc = new BL.ItemDescription () { Description = "Fell to ground level and poison stump to inhibit regrowth" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Fell to ground level and grind out stump thoroughly" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Fell to ground level in a safe and controlled manner" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Grind out Stump. Rake up grindings in situ." };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Grind out Stump and clear all excess stump grindings to leave level ground." };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Inspection with suitable decay detection equipment" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Make new Pollard @   metres from ground level" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "None recommended" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Reduce back selected branches to provide 2 metres clearance from" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Remove epicormic growth from main stem" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Remove major deadwood roadside only (Deadwood > 25mm in diameter )" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Remove major deadwood throughout whole crown area (Deadwood > 25mm in diameter )" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Re-Pollard back to previous pollard points" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Crown Lift by removing all lower growth to provide 5 metres clearance from ground level" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "(Hedge) Reduce height of hedge by up to   metre/s and trim both sides of hedge to a neat and tidy finish" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "(Hedge) Reduce height of hedge by up to   metre/s and trim garden side to a neat and tidy finish." };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "(Hedge) Reduce height of hedge by up to   metre/s, creating flat top" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Crown Lift by removing lower growth to provide 4 metres clearance from ground level" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Add or Amend work descriptions, lists are ordered alphabetically, keep your work  categories groups by starting with the same letter for each type" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Crown Reduce - Reducing the height and spread of the tree by up to 1 metre" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Crown reduce by [%]. Radial spread and height to be reduced by around metres. Care will be taken to ensure good natural shape and balance. " };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Crown Thin by removing inner branches including dead, weak and crossing growth to reduce crown density by up to 15[%]" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Crown Thin by removing inner branches including dead, weak and crossing growth to reduce crown density by up to 10[%]" };
            DescsToAdd.Add (Desc);
            Desc = new BL.ItemDescription () { Description = "Crown Lift by removing lower growth to give 3 metres clearance from ground level" };
            DescsToAdd.Add (Desc);

            ItemDescriptionSaveList (DescsToAdd);

            List<BL.CalendarItem> CalendarItemsToAdd = new List<BL.CalendarItem> ();
            for (int CalendarItemCount = 0; CalendarItemCount < 50; CalendarItemCount++) {
                DateTime StartTime = DateTime.Now.Date.AddDays (-25 + CalendarItemCount).AddHours(10);
                DateTime EndTime;
                CalendarItemType ThisItemType;
                string ThisSummary;
                if (CalendarItemCount % 3 == 0) {
                    EndTime = StartTime.AddHours (6);
                    ThisItemType = CalendarItemType.Work;
                    ThisSummary = string.Format ("Work booked for job {0}", CalendarItemCount);
                    var WorkItem = new BL.CalendarItem () {
                        StartTime = StartTime,
                        EndTime = EndTime,
                        ItemType = (DGS.ArbPro.Mobile.Service.CalendarItemType)ThisItemType,
                        Summary = ThisSummary,
                    };

                    CalendarItemsToAdd.Add (WorkItem);
                }

                EndTime = StartTime.AddHours (1);
                ThisItemType = CalendarItemType.Quote;
                ThisSummary = string.Format ("Quote booked for client {0}", CalendarItemCount);
                var Item = new BL.CalendarItem () {
                    StartTime = StartTime,
                    EndTime = EndTime,
                    ItemType = (DGS.ArbPro.Mobile.Service.CalendarItemType)ThisItemType,
                    Summary = ThisSummary,
                };

                CalendarItemsToAdd.Add (Item);
            }

            CalendarItemSaveList (CalendarItemsToAdd);

            if (TrackHandle != null) {
                TrackHandle.Stop ();
            }

			List<BL.Client> clientList = new List<BL.Client>();
			string[] a = { "George", "Jane", "Bob", "Alice", "Jon", "Michelle", "Edward", "Amanda", "Malcolm", "Lizzy", "David", "Jenny" };
			string[] b = { "Johnson", "Smith", "Ross", "Cooper", "Ferguson", "Grey", "Davidson", "Stewart", "West", "Hale", "Goodson", "Bride" };
			string[] c = { "Corp.", "Ltd.", "& Co.", "Solutions" };

			for (int i = 1; i < 20; i++)
			{
				BL.Client demoClient = new BL.Client();
				demoClient.Id = i;
				demoClient.AccountType = 0;
				demoClient.HouseNameNumber = i.ToString();
				demoClient.Address1 = "Demo Street";
				demoClient.Address2 = string.Empty;
				demoClient.Title = string.Empty;
				demoClient.Forename = RandomString(a);
				demoClient.Surname = RandomString(b);
				demoClient.BusinessName = RandomString(b) + RandomString(c);
				demoClient.Town = "Demoville";
				demoClient.Email = demoClient.Forename + "@demo.com";
				demoClient.County = "Demoshire";
				demoClient.IncludeInMarketing = true;
				demoClient.SendSmsMessage = true;
				demoClient.Mobile = "1234567890";
				demoClient.Telephone = "1234567890";

				clientList.Add(demoClient);
			}

			SaveAllClients(clientList);

			List<BL.RiskTemplate> templateList = new List<BL.RiskTemplate>();

			for (int i = 1; i < 20; i++)
			{
				BL.RiskTemplate demoTemplate = new BL.RiskTemplate();

				demoTemplate.Id = i;
				demoTemplate.Name = "Risk Assessment Template " + i;
				demoTemplate.Description = "Template description " + i;
				demoTemplate.RefDoc = "1." + i;

				templateList.Add(demoTemplate);
			}

			RiskTemplateSaveList(templateList);
        }

        private AppointmentStatus RandomAppointmentStatus (AppointmentStatus [] appointmentStatusArray)
        {
            Random MyRandom;
            MyRandom = new Random ((int)DateTime.Now.Ticks);

            return appointmentStatusArray [MyRandom.Next (0, appointmentStatusArray.Length - 1)];
        }

        private EnumPreservationOrderStatus RandomTPO (EnumPreservationOrderStatus [] tPOArray)
        {
            Random MyRandom;
            MyRandom = new Random ((int)DateTime.Now.Ticks);

            return tPOArray [MyRandom.Next (0, tPOArray.Length - 1)];
        }

        private string RandomString (string [] stringArray)
        {
            Random MyRandom;
            MyRandom = new Random ((int)DateTime.Now.Ticks);

            return stringArray [MyRandom.Next (0, stringArray.Length - 1)];
        }

        private decimal RandomDecimal (decimal [] array)
        {
            Random MyRandom;
            MyRandom = new Random ();
            return array [MyRandom.Next (0, array.Length - 1)];
        }

        private int RandomInt (int [] array)
        {
            Random MyRandom;
            MyRandom = new Random ();
            return array [MyRandom.Next (0, array.Length - 1)];
        }

        #endregion

        #region Client Methods

        public BL.Client ClientGetForId (int clientId)
        {
			BL.Client returnValue = null;

            if (clientId > 0) {
                returnValue = _Db.Table<BL.Client> ().FirstOrDefault (c => c.Id == clientId);
            } else {
                returnValue = new BL.Client();
            }

            return returnValue;
        }

        public bool SaveAllClients (List<BL.Client> clients)
        {
            try {
                _Db.BeginTransaction ();

                // Clear the Clients table
                _Db.Execute ("DELETE FROM Client");
                foreach (BL.Client item in clients) {
                    this.SaveClient (item);
                }

                _Db.Commit ();
                return true;
            } catch (Exception) {
                _Db.Rollback ();
                return false;
            }

        }

        #endregion
    }
}

