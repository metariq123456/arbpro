// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/DataAccessLayer/SettingsRepository.cs#9 $
// // $Author: Jon $
// // $Date: 2015/05/20 $
// // $Change: 24664 $
// //*********************************************************************
//
//
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.IO;
using DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.DAL
{
	public class SettingsRepository
	{
		private DL.SettingsDataBase _Db;
        private static SettingsRepository _Instance;
        
        #region Constructors
        public static SettingsRepository Instance()
        {
            if (_Instance == null)
            {
                _Instance = new SettingsRepository();
            }
            return _Instance;
        }

        SettingsRepository()
        {
			_Db = new DGS.ArbPro.Mobile.DL.SettingsDataBase(DatabaseFilePath);
        }
        #endregion
        
        public static string DatabaseFilePath {
			get { 
				string DatabaseFilename= "ArbProMobile-settings.db3";


#if NETFX_CORE
                var path = Path.Combine(Windows.Storage.ApplicationData.Current.LocalFolder.Path, DatabaseFilename);
#else

#if SILVERLIGHT
                // Windows Phone expects a local path, not absolute
                var path = DatabaseFilename;
#else

#if __ANDROID__
                // Just use whatever directory SpecialFolder.Personal returns
                string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); ;
#else
				// we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
				// (they don't want non-user-generated data in Documents)
				string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
				string libraryPath = Path.Combine (documentsPath, "../Library/"); // Library folder
#endif
				var path = Path.Combine (libraryPath, DatabaseFilename);
#endif      

#endif
				return path;    
			}
		}
		
		#region Settings
		public Settings SettingsGet (Settings newSettings)
		{
			Settings retVal = _Db.GetSettings();
			if(retVal == null)
			{
				_Db.SaveItem<Settings>(newSettings);
				retVal = newSettings;
			}
			return retVal;
		}
		
		public void SettingsSet(Settings s)
		{
			_Db.SaveItem<Settings>(s);
		}
		#endregion
		
		#region User
		public User CurrentUserGet()
		{
			User retVal = _Db.GetUser();
			return retVal;
		}
		
		public void UserSet(User u)
		{
			_Db.SaveItem<User>(u);
		}
		#endregion
	}
}

