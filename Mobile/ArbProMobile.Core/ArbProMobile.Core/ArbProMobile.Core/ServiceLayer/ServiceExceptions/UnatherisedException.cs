// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/ServiceLayer/ServiceExceptions/UnatherisedException.cs#9 $
// // $Author: Jon $
// // $Date: 2015/05/20 $
// // $Change: 24664 $
// //*********************************************************************
//
//
using System;

namespace DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions
{
	public class UnatherisedException : Exception
	{
		public UnatherisedException (): base("Login Required")
		{
		}
	}
}

