// //*********************************************************************
// // $Id$
// // $Author$
// // $Date$
// // $Change$
// //*********************************************************************
//
using System;
using System.Collections.Generic;
using Interfaces = DGS.ArbPro.Mobile.Interfaces;
using BL = DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.Core.ServiceLayer
{
	public abstract class BaseConnection : Interfaces.IServiceConnection
	{
		#region Static Members
		private static Interfaces.IServiceConnection _Connection;
		#endregion
		#region Members
		private bool _IsConnected = false;
		private string _StrLastError = "";
		private string _StrUsername = "";
		protected int _TimeOut = 30000;
		#endregion
		#region Constructors
		public BaseConnection ()
		{
		}
		#endregion
		#region Abstract Methods And IConnectionManager Implementation		
		public abstract BL.User AuthenticateUser (string username);

		public abstract BL.User AuthenticateUser ();

		public abstract Interfaces.IServiceConnection GetConnection (string username, string password);

		public abstract Interfaces.IServiceConnection GetConnection ();
		//Get Data Service Calls
		public abstract List<BL.Appointment> GetAppointmentListForUser (int userId);

		public abstract List<BL.RiskAssessment> GetRisksListForOpenAppointments (int userId);

		public abstract List<BL.ItemDescription> GetItemDescriptionListForClient ();

		public abstract List<BL.ItemType> GetItemTypeListForClient ();

		public abstract List<BL.Tool> GetToolListForClient ();

		public abstract List<BL.Client> GetClientList();

		public abstract List<BL.AssessmentSection> GetRiskAssessmentCriteriaList ();

		public abstract List<BL.AssessmentSection> GetRiskAssessment(int templateId, int appointmentId);

		public abstract List<BL.RiskHospital> GetRiskHospitalList ();

		public abstract List<BL.RiskAerialRescuePlan> GetRiskAerialRescuePlanList ();

		public abstract List<BL.CalendarItem> GetCalendarAppointments();

		//Set Data Sevice Calls
		public abstract bool SetAppoinmentDownloaded (int appointmentId);

		public abstract bool SetQuoteUpdatedForClient (DGS.ArbPro.Mobile.BL.Quote quote);

		public abstract bool SetQuoteItemUpdateForClient(int quoteId, DGS.ArbPro.Mobile.BL.QuoteItem item);

		public abstract bool SetRiskUpdatedForQuote (BL.RiskAssessment assessment);

		public abstract bool SetClientUpdateOrInsert(BL.Client client);

		public abstract bool SetQuoteItemPictureUpload (string quoteItemGUID, int packetCount, int packetNo, byte[] imageArray);

		public abstract bool SetQuoteItemPictureUpload (string name, byte[] bytes, string filename);

		public abstract bool SetRiskSignatureUpload (int quoteNo, byte[] bytes, string filename);

		public abstract void SendVersion (string version);
		#endregion
		#region Properties
		protected Interfaces.IServiceConnection Connection {
			get{ return _Connection;}
			set{ _Connection = value;}
		}

		public bool Connected {
			get { return _IsConnected; }
			set {
				_IsConnected = value;
				if (!value)
					_Connection = null;
			}
		}

		public string LastError {
			get { return _StrLastError; }
			protected set { _StrLastError = value;}
		}

		public string UserName {
			get { return _StrUsername; }
			protected set { _StrUsername = value;}
		}
		#endregion
	}
}

