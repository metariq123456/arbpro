// //*********************************************************************
// // $Id$
// // $Author$
// // $Date$
// // $Change$
// //*********************************************************************
//
using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using ArbProMobile.Core;
using DGS.ArbPro.Mobile;
using DGS.ArbPro.Mobile.Core.Enums;
using DGS.ArbPro.Service.Model;
using RestSharp;
using RestSharp.Serializers;
using Xamarin;
using BL = DGS.ArbPro.Mobile.BL;
using Interfaces = DGS.ArbPro.Mobile.Interfaces;
using Responses = DGS.ArbPro.Service.Model.Operations.Responses;
using Tools = Microsec.Tools.ObjectTools;
using Types = DGS.ArbPro.Service.Model.Types;

namespace DGS.ArbPro.Mobile.Core.ServiceLayer
{
	public class RestConnection : BaseConnection
	{

		#region Private Memebrs

		private class ServiceCallConstraints
		{
			public string CONST_AUTH { get { return "Auth"; } }

			public string CONST_GET_CURRENT_USER  { get { return "GetCurrentUser"; } }

			public string CONST_GET_APPOINMENT_ID_LIST  { get { return "AppointmentGetIdListForUser"; } }
		
			public string CONST_GET_RISK_LIST  { get { return "RiskForOpenAppointmentGet"; } }

			public string CONST_GET_ITEM_DESCRIPTION_LIST  { get { return "ItemDescriptionList"; } }

			public string CONST_GET_ITEM_TYPE_LIST { get { return "ItemTypeList"; } }

			public string CONST_GET_TOOL_LIST  { get { return "ToolList"; } }

			public string CONST_GET_CLIENT_LIST { get { return "ClientList"; } }

			public string CONST_GET_HOSPITAL_LIST  { get { return "RiskHospitalsList"; } }

			public string CONST_GET_RISKCRITERIA_LIST  { get { return "RiskCriterionSectionList"; } }

			public string CONST_GET_RISKASSESSMENT_LIST { get { return "RiskAssessmentGet"; } }

			public string CONST_GET_AERIALPLAN_LIST  { get { return "RiskAerialRescuePlanList"; } }

			public string CONST_SET_QUOTE_UPDATED  { get { return "QuoteUpdate"; } }

			public string CONST_SET_QUOTEITEM_UPDATED  { get { return "QuoteItemUpdate"; } }

			public string CONST_SET_APPOINMENT_DOWNLOADED  { get { return "AppointmentMarkDownloaded"; } }

			public string CONST_SET_QUOTEITEM_PIC { get { return "UploadQuoteItemImage"; } }

			public string CONST_SET_RISKSIGNATURE_PIC { get { return "UploadRiskSignature"; } }

			public string CONST_SET_RISK_UPDATED { get { return "RiskAssessmentUpdate"; } }

			public string CONST_SET_RISK_UPDATEDV3 { get { return "RiskAssessmentUpdateV3"; } }

			public string CONST_SET_CLIENT_UPDATEORINSERT { get { return "ClientUpdate"; } }

			public string CONST_SET_SEND_VERSION { get { return "SendVersion"; } }
		}

		private ServiceCallConstraints ServiceCalls = new ServiceCallConstraints ();

		#endregion

		#region Constructors

		public RestConnection ()
		{
			this.Connected = false;
			System.Net.ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
			System.Net.ServicePointManager.Expect100Continue = true;
			System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls;
		}

		#endregion

		#region Members

		private ArbRestClient _ServiceClient = null;

		#endregion

		#region Private Methods

		/// <summary>
		/// Connect the specified new_username and password.
		/// </summary>
		/// <param name="new_username">New_username.</param>
		/// <param name="password">Password.</param>
		private void Connect (string new_username, string password)
		{
			if (BL.Settings.ServiceURI.Length < 5) {
				this.Connected = false;
				return;
			}
			//Create the Rest service client
			_ServiceClient = new ArbRestClient (BL.Settings.ServiceURI);
			//Create the authentication request
			RestRequest auth_request = new RestRequest (ServiceCalls.CONST_AUTH, Method.POST);
			//Add the parameters to the request
			auth_request.AddParameter ("UserName", new_username);
			auth_request.AddParameter ("Password", password);
			//Execute the request
			RestResponse response = ExecuteRequest (auth_request, false);
			if (this.Connected) {
				//The user has been logged in.
				//Clear any existing keys
				BL.Globals.AuthenticationKeys.Clear ();
				//Add the authentication keys to the global dictionary
				foreach (RestResponseCookie cookie in response.Cookies) {
					BL.Globals.AuthenticationKeys.Add (cookie.Name, cookie.Value);
				}
				//Update the global user
				//globals.CurrentUser = this.UserGet();
				//Update the user info within the settings.
				BL.Settings.Username = new_username;

			}
		}

		/// <summary>
		/// Setup a base connection to use session authentication.
		/// </summary>
		private void Connect ()
		{
			//Create the Rest service client
			_ServiceClient = new ArbRestClient (BL.Settings.ServiceURI);
		}

		/// <summary>
		/// Adds the authentication keys.
		/// </summary>
		/// <returns>The authentication.</returns>
		/// <param name="request">Request.</param>
		private RestRequest AddAuthentication (RestRequest request)
		{
			//Add the time out
			request.Timeout = _TimeOut;
			
			//Add the authentication cookies
			Dictionary<string, string> auth_keys = BL.Globals.AuthenticationKeys;
			
			foreach (var key_value in auth_keys) {
				request.AddCookie (key_value.Key, key_value.Value);
			}
            
			return request;
		}

		/// <summary>
		/// Adds the authentication.
		/// </summary>
		/// <returns>The authentication.</returns>
		/// <param name="request">Request.</param>
		/// <param name="keys">Keys.</param>
		private RestRequest AddAuthentication (RestRequest request, Dictionary<string, string> auth_keys)
		{
			foreach (var key_value in auth_keys) {
				request.AddCookie (key_value.Key, key_value.Value);
			}
			return request;
		}

		/// <summary>
		/// Executes the request.
		/// </summary>
		/// <returns>The request.</returns>
		/// <param name="request">Request.</param>
		/// <param name="authenticate">If set to <c>true</c> authenticate.</param>
		private RestResponse ExecuteRequest (RestRequest request, bool authenticate = true)
		{
			if (authenticate) {
				request = this.AddAuthentication (request);
			} else {
				//Add the time out
				request.Timeout = _TimeOut;
			}
            
			//Setup the Rest response
			RestResponse response = null;
            
			try {
				response = (RestResponse)_ServiceClient.Execute (request);
				if (response.StatusCode != System.Net.HttpStatusCode.OK) {
					this.Connected = false;
					this.LastError = response.StatusCode.ToString ();
				} else {
					this.Connected = true;
				}
			} catch (Exception ex) {
				Core.ArbProInsights.LogErrorError (ex);
				this.Connected = false;
				this.LastError = ex.Message;
			}
			return response;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Gets or creates the connection.
		/// </summary>
		/// <returns>The connection.</returns>
		/// <param name="username">Username.</param>
		/// <param name="password">Password.</param>
		public override Interfaces.IServiceConnection GetConnection (string username, string password)
		{
			//Create the singleton instance if required
			if (this.Connection == null) {
				this.Connection = this;
			}

			//Authenticate the user
			if (!this.Connected || this.UserName != username) {
				this.Connect (username, password);
			}

			return this.Connection;
		}

		/// <summary>
		/// Gets the connection.
		/// </summary>
		/// <returns>The connection.</returns>
		public override Interfaces.IServiceConnection GetConnection ()
		{
			if (this.Connection == null) {
				this.Connection = this;
			}
            
			if (!this.Connected) {
				this.Connect ();
				this.AuthenticateUser ();
			}
            
			return this.Connection;
		}
		//Authentication
		/// <summary>
		/// Attempts to authenticate the user if the device has authentication keys but there is no connection.
		/// </summary>
		/// <returns>The user.</returns>
		/// <param name="username">Username.</param>
		public override BL.User AuthenticateUser (string username)
		{
			Dictionary<string,string> keys = BL.Globals.AuthenticationKeys;
			if (keys == null || keys.Count == 0)
				return null;
				
			try {
				RestRequest request = new RestRequest (ServiceCalls.CONST_GET_CURRENT_USER, Method.POST);
				//Add authentication to the request
				request = this.AddAuthentication (request);
				
				//Execute the request
				RestResponse<Responses.GetCurrentUser_Response> response =
					(RestResponse<Responses.GetCurrentUser_Response>)_ServiceClient.Execute<Responses.GetCurrentUser_Response> (request);
					
				if (response.Data != null && (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.Accepted)) {
					this.Connected = true;
					BL.User user = new BL.User ();
					
					user.Id = response.Data.User.Id;
					user.DisplayName = response.Data.User.DisplayName;
					user.ClientId = response.Data.User.ClientId;
					user.UserName = response.Data.User.UserName;
					user.Password = response.Data.User.Password;
					
					return user;
				} else {
					this.Connected = false;
					return null;
				}
				
			} catch (Exception) {
				this.Connected = false;
				return null;
			}
		}

		/// <summary>
		/// Authenticates the user.
		/// </summary>
		/// <returns>The user.</returns>
		public override BL.User AuthenticateUser ()
		{
			Dictionary<string,string> auth_keys = BL.Globals.AuthenticationKeys;
			if (auth_keys == null || auth_keys.Count == 0)
				return null;
				
			try {
				RestRequest request = new RestRequest (ServiceCalls.CONST_GET_CURRENT_USER, Method.POST);
				//Add authentication to the request
				request = this.AddAuthentication (request, auth_keys);
				
				//Execute the request
				RestResponse<Responses.GetCurrentUser_Response> response =
					(RestResponse<Responses.GetCurrentUser_Response>)_ServiceClient.Execute<Responses.GetCurrentUser_Response> (request);
					
				if (response.Data != null) {// && (response.StatusCode == System.Net.HttpStatusCode.OK || response.StatusCode == System.Net.HttpStatusCode.Accepted))
					this.Connected = true;
					BL.User user = new BL.User ();
					user.Id = response.Data.User.Id;
					user.DisplayName = response.Data.User.DisplayName;
					user.ClientId = response.Data.User.ClientId;
					user.UserName = response.Data.User.UserName;
					user.Password = response.Data.User.Password;
					user.UserGuid = response.Data.User.UserGuid;
					return user;
				} else {
					this.Connected = false;
					return null;
				}
				
			} catch (Exception) {
				this.Connected = false;
				return null;
			}
		}

		#region Get Data Service Calls

		/// <summary>
		/// Gets the appointment identifier list for user.
		/// </summary>
		/// <returns>The appointment identifier list for user.</returns>
		/// <param name="userId">User identifier.</param>
		public override List<BL.Appointment> GetAppointmentListForUser (int userId)
		{
			List<BL.Appointment> returnList = new List<BL.Appointment> ();
			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_GET_APPOINMENT_ID_LIST, Method.POST);
			//Add authentication to the request
			request = this.AddAuthentication (request);
			//Add the requiured parameters - NOTE: The order added is important
			request.AddParameter ("UserId", userId.ToString ());
			
			//Execute the request
			RestResponse<Responses.AppointmentGetIdListForUser_Response> response =
				(RestResponse<Responses.AppointmentGetIdListForUser_Response>)_ServiceClient.Execute<Responses.AppointmentGetIdListForUser_Response> (request);
			
			if (response.Data == null || response.Data.IdList == null)
				throw new Exception ("Data Download Error");
			
			BL.Appointment a = null;
			foreach (Types.Appointment appointment in response.Data.IdList) {
				a = new BL.Appointment ();

				a.Id = appointment.Id;
				a.MasterCalendarId = appointment.MasterCalendarId;
				a.ClientIdNo = appointment.ClientIdNo;
				a.JobIdNo = appointment.JobIdNo;
				a.StartDateTime = appointment.StartDateTime;
				a.QuoteNo = appointment.QuoteNo;
				a.Duration = appointment.Duration;
				a.Subject = appointment.Subject;
				a.UserCR = appointment.UserCR;
				a.SurveyorId = appointment.SurveyorId;
				a.Comments = appointment.Comments;
				a.ClientName = appointment.ClientName;
				a.Title = appointment.Title;
				a.FirstName = appointment.FirstName;
				a.Surname = appointment.Surname;
				a.ClientStreetNo = appointment.ClientStreetNo;
				a.ClientAddress1 = appointment.ClientAddress1;
				a.ClientAddress2 = appointment.ClientAddress2;
				a.ClientAddressTown = appointment.ClientAddressTown;
				a.ClientAddressCounty = appointment.ClientAddressCounty;
				a.ClientAddressPostCode = appointment.ClientAddressPostCode;
				a.ClientAddressLocation = new BL.GeoLocation (appointment.ClientAddressLocation.Latitude, appointment.ClientAddressLocation.Longitude);
				a.ClientEmail = appointment.ClientEmail;
				a.ClientTelephone = appointment.ClientTelephone;
				a.ClientMobile = appointment.ClientMobile;
				a.RiskTemplateId = appointment.RiskTemplateId;
				a.ClientContactId = appointment.ClientContactId;

				returnList.Add (a);
			}
			
			return returnList;
		}

		/// <summary>
		/// Gets all risk assessments for open appointments.
		/// </summary>
		/// <returns>The risks assessment list for open appointments.</returns>
		/// <param name="userId">User identifier.</param>
		public override List<BL.RiskAssessment> GetRisksListForOpenAppointments (int userId)
		{			
			RestRequest request = new RestRequest (ServiceCalls.CONST_GET_RISK_LIST, Method.POST);
			//Add authentication to the request
			request = this.AddAuthentication (request);
			//Add the requiured parameters - NOTE: The order added is important
			request.AddParameter ("UserId", userId.ToString ());
			
			//Execute the request
			RestResponse<Responses.RiskForOpenAppointmentGet_Response> response =
				(RestResponse<Responses.RiskForOpenAppointmentGet_Response>)_ServiceClient.Execute<Responses.RiskForOpenAppointmentGet_Response> (request);
			
			if (response.Data == null || response.Data.Assessments == null)
				throw new Exception ("Data Download Error");
			
			return RiskAssessmentListConvert (response.Data.Assessments);
		}

		/// <summary>
		/// Gets the item description list for client.
		/// </summary>
		/// <returns>The item description list for client.</returns>
		/// <param name="clientId">Client identifier.</param>
		public override List<BL.ItemDescription> GetItemDescriptionListForClient ()
		{
			List<BL.ItemDescription> returnList = new List<BL.ItemDescription> ();
			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_GET_ITEM_DESCRIPTION_LIST, Method.POST);
			//Add authentication to the request
			request = this.AddAuthentication (request);
			//Add the requiured parameters - NOTE: The order added is important
			//request.AddParameter("ClientId", clientId.ToString ());
			
			//Execute the request
			RestResponse<Responses.ItemDescriptionList_Response> response =
				(RestResponse<Responses.ItemDescriptionList_Response>)_ServiceClient.Execute<Responses.ItemDescriptionList_Response> (request);
			
			BL.ItemDescription d = null;
			foreach (Types.ItemDescription item in response.Data.Items) {
				d = new BL.ItemDescription ();
				d.Id = item.Id;
				d.Description = item.Description;
				//Tools.MigrateProperties(item,d,ignor);
				returnList.Add (d);
			}
			
			return returnList;
		}

		/// <summary>
		/// Gets the item type list for client.
		/// </summary>
		/// <returns>The item type list for client.</returns>
		/// <param name="clientID">Client I.</param>
		public override List<BL.ItemType> GetItemTypeListForClient ()
		{
			List<BL.ItemType> returnList = new List<BL.ItemType> ();
			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_GET_ITEM_TYPE_LIST, Method.POST);
			//Add authentication to the request
			request = this.AddAuthentication (request);
			//Add the requiured parameters - NOTE: The order added is important
			//request.AddParameter("ClientId", clientId.ToString ());
			
			//Execute the request
			RestResponse<Responses.ItemTypeList_Response> response =
				(RestResponse<Responses.ItemTypeList_Response>)_ServiceClient.Execute<Responses.ItemTypeList_Response> (request);
			
			BL.ItemType i = null;
			foreach (Types.ItemType item in response.Data.Result) {
				i = new BL.ItemType ();
				;
				//Tools.MigrateProperties(item,i,ignor);
				i.CommonName = item.CommonName;
				i.TechnicalName = item.TechnicalName;
				returnList.Add (i);
			}
			
			return returnList;
		}

		/// <summary>
		/// Gets the tool list for client.
		/// </summary>
		/// <returns>The tool list for client.</returns>
		/// <param name="clientId">Client identifier.</param>
		public override List<BL.Tool> GetToolListForClient ()
		{
			List<BL.Tool> returnList = new List<BL.Tool> ();
			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_GET_TOOL_LIST, Method.POST);
			//Add authentication to the request
			request = this.AddAuthentication (request);
			//Add the requiured parameters - NOTE: The order added is important
			//request.AddParameter("ClientId", clientId.ToString ());
			
			//Execute the request
			RestResponse<Responses.ToolList_Response> response =
				(RestResponse<Responses.ToolList_Response>)_ServiceClient.Execute<Responses.ToolList_Response> (request);
			
			BL.Tool t = null;
			foreach (Types.Tool tool in response.Data.Tools) {
				t = new BL.Tool ();
				//Tools.MigrateProperties(tool,t,ignor);
				t.Id = tool.Id;
				t.Name = tool.Name;
				t.Stock = tool.Stock;
				returnList.Add (t);
			}
			
			return returnList;
		}

		/// <summary>
		/// Gets the client list for user.
		/// </summary>
		/// <returns>The client list for user.</returns>
		public override List<BL.Client> GetClientList()
		{
			List<BL.Client> returnList = new List<BL.Client>();
			//Setup Rest Request Object
			RestRequest request = new RestRequest(ServiceCalls.CONST_GET_CLIENT_LIST, Method.POST);
			//Add authentication to the request
			request = this.AddAuthentication(request);

			//Execute the request
			RestResponse<Responses.ClientListResponse> response =
				                      (RestResponse<Responses.ClientListResponse>)_ServiceClient.Execute<Responses.ClientListResponse>(request);

			BL.Client c = null;
			foreach (Types.Client client in response.Data.Clients)
			{
				c = new BL.Client();

				c.Id = client.Id;
				c.AltAccountNo = client.AltAccountNo;
				c.Title = client.Title;
				c.Forename = client.Forename;
				c.Surname = client.Surname;
				c.BusinessName = client.BusinessName;
				c.HouseNameNumber = client.HouseNameNumber;
				c.Address1 = client.Address1;
				c.Address2 = client.Address2;
				c.Town = client.Town;
				c.County = client.County;
				c.PostCode = client.PostCode;
				c.Sector = client.Sector;
				c.Telephone = client.Telephone;
				c.Mobile = client.Mobile;
				c.Email = client.Email;
				c.WebAddress = client.WebAddress;
				c.AccountType = client.AccountType;
				c.DateOpened = client.DateOpened;
				c.AddressLatitude = client.AddressLatitude;
				c.AddressLongitude = client.AddressLongitude;
				c.TermsAgreed = client.TermsAgreed;
				c.SendSmsMessage = client.SendSmsMessage;
				c.IncludeInMarketing = client.IncludeInMarketing;

				returnList.Add(c);
			}

			return returnList;
		}

		/// <summary>
		/// Gets the list of Risk Assessment Sections
		/// </summary>
		/// <returns>The section and items list for risk assessments.</returns>
		public override List<BL.AssessmentSection> GetRiskAssessmentCriteriaList ()
		{
			List<BL.AssessmentSection> returnList = new List<BL.AssessmentSection> ();
			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_GET_RISKCRITERIA_LIST);
			//Add authentication to the request
			request = this.AddAuthentication (request);
			
			//Execute the request
			RestResponse<Responses.RiskCriterionSectionList_Response> response =
				(RestResponse<Responses.RiskCriterionSectionList_Response>)_ServiceClient.Execute<Responses.RiskCriterionSectionList_Response> (request);
			
			try {
				foreach (Types.AssessmentSection section in response.Data.Sections) {
					returnList.Add (RiskCriteriaConvert (section));
				}
			
				return returnList;
			} catch (Exception ex) {
				throw ex;
			}
			
		}

		/// <summary>
		/// Gets the  Risk Assessments
		/// </summary>
		/// <returns>The risk assessments.</returns>
		public override List<BL.AssessmentSection> GetRiskAssessment(int templateId, int appointmentId)
		{
			List<BL.AssessmentSection> returnList = new List<BL.AssessmentSection>();
			//Setup Rest Request Object
			RestRequest request = new RestRequest(ServiceCalls.CONST_GET_RISKASSESSMENT_LIST, Method.POST);
			//Add authentication to the request
			request = this.AddAuthentication(request);
			//request.AddParameter("RiskAssessment", new Types.Risk().ToString());
			request.AddParameter("TemplateId", templateId.ToString());
			request.AddParameter("AppointmentId", appointmentId.ToString());

			//Execute the request
			RestResponse<Responses.RiskAssessmentGetResponse> response =
				(RestResponse<Responses.RiskAssessmentGetResponse>)_ServiceClient.Execute<Responses.RiskAssessmentGetResponse>(request);

			try
			{
				returnList = RiskConvert(response.Data.RiskAssessment);

				return returnList;
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}

		/// <summary>
		/// Gets the list of A&E Hospitals located on the server.
		/// </summary>
		/// <returns>The Hospitals list for risk assessments.</returns>
		public override List<BL.RiskHospital> GetRiskHospitalList ()
		{
			List<BL.RiskHospital> returnList = new List<BL.RiskHospital> ();
			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_GET_HOSPITAL_LIST, Method.POST);
			//Add authentication to the request
			request = this.AddAuthentication (request);
			
			//Execute the request
			RestResponse<Responses.RiskHospitalsList_Response> response =
				(RestResponse<Responses.RiskHospitalsList_Response>)_ServiceClient.Execute<Responses.RiskHospitalsList_Response> (request);
			
			BL.RiskHospital h = null;
			foreach (Types.RiskHospital hospital in response.Data.Hospitals) {
				h = new BL.RiskHospital ();
				h.Id = hospital.HospitalId;
				h.Name = hospital.Name;
				returnList.Add (h);
			}
			
			return returnList;
		}

		public override List<BL.RiskAerialRescuePlan> GetRiskAerialRescuePlanList ()
		{
			List<BL.RiskAerialRescuePlan> returnList = new List<BL.RiskAerialRescuePlan> ();
			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_GET_AERIALPLAN_LIST, Method.POST);
			//Add authentication to the request
			request = this.AddAuthentication (request);
			
			//Execute the request
			RestResponse<Responses.RiskAerialRescuePlanList_Response> response =
				(RestResponse<Responses.RiskAerialRescuePlanList_Response>)_ServiceClient.Execute<Responses.RiskAerialRescuePlanList_Response> (request);
			
			BL.RiskAerialRescuePlan p = null;
			foreach (Types.RiskAerialRescuePlan plan in response.Data.Plans) {
				p = new BL.RiskAerialRescuePlan ();
				p.Id = plan.PlanId;
				p.Plan = plan.Value;
				returnList.Add (p);
			}
			
			return returnList;
		}

		#endregion

		#region Set Data Sevice Calls

		/// <summary>
		/// Sets the appoinment downloaded.
		/// </summary>
		/// <returns><c>true</c>, if appoinment downloaded was set, <c>false</c> otherwise.</returns>
		/// <param name="appointmentId">Appointment identifier.</param>
		/// <param name="clientId">Client identifier.</param>
		public override bool SetAppoinmentDownloaded (int appointmentId)
		{
			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_SET_APPOINMENT_DOWNLOADED, Method.POST);
			//Add authentication
			request = this.AddAuthentication (request);
			//Add the requiured parameters - NOTE: The order added is important
			request.AddParameter ("AppointmentId", appointmentId.ToString ());

			//Execute the request
			RestResponse<Responses.AppointmentMarkDownloaded_Response> response =
				(RestResponse<Responses.AppointmentMarkDownloaded_Response>)_ServiceClient.Execute<Responses.AppointmentMarkDownloaded_Response> (request);
			
			if (response.Data == null)
				return false;
			else
				return response.Data.Result;
		}

		/// <summary>
		/// Updates the Service with new data for a quote.
		/// </summary>
		/// <param name="clientId">Client identifier.</param>
		/// <param name="quote">Quote object.</param>
		public override bool SetQuoteUpdatedForClient (BL.Quote quote)
		{	
			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_SET_QUOTE_UPDATED, Method.POST);
			//Add authentication
			request = this.AddAuthentication (request);
			string json = request.JsonSerializer.Serialize (QuoteConvert (quote));
			//Service stack requires the quotes to be replaced
			json = json.Replace (@"""", string.Empty);
			request.RequestFormat = DataFormat.Json;
			request.AddParameter ("Quote", json);
			
			//Execute the request
			RestResponse<Responses.QuoteUpdate_Response> response =
				(RestResponse<Responses.QuoteUpdate_Response>)_ServiceClient.Execute<Responses.QuoteUpdate_Response> (request);
             
             
			#if DEBUG
			Console.WriteLine ("QuoteItemUpload status = {0}", response.ResponseStatus.ToString ());
			#endif
			
			if (response.StatusCode != System.Net.HttpStatusCode.OK || response.ResponseStatus != ResponseStatus.Completed || (response.Data != null && response.Data.ResponseStatus != null)) {
				if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
					throw new Exceptions.UnauthorisedException ();
					
				return false;
			} else
				return true; 	
		}

		public override bool SetQuoteItemUpdateForClient (int quoteId, DGS.ArbPro.Mobile.BL.QuoteItem item)
		{

			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_SET_QUOTEITEM_UPDATED, Method.POST);
			//Add authentication
			request = this.AddAuthentication (request);
			string json = request.JsonSerializer.Serialize (ConvertAQuoteItem (item));
			//Service stack requires the quotes to be replaced
			json = json.Replace (@"""", string.Empty);
			request.RequestFormat = DataFormat.Json;
			request.AddParameter ("QuoteId", quoteId);
			request.AddParameter ("Item", json);

			//Execute the request
			RestResponse<Responses.QuoteItemUpdate_Response> response =
				(RestResponse<Responses.QuoteItemUpdate_Response>)_ServiceClient.Execute<Responses.QuoteItemUpdate_Response> (request);


			#if DEBUG
			Console.WriteLine ("QuoteItemUpload status = {0}", response.ResponseStatus.ToString ());
			#endif

			if (response.StatusCode != System.Net.HttpStatusCode.OK || response.ResponseStatus != ResponseStatus.Completed || (response.Data != null && response.Data.ResponseStatus != null)) {
				if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
					throw new Exceptions.UnauthorisedException ();

				return false;
			} else
				return true; 	
		}

		/// <summary>
		/// Updates the Service with new data for a Risk.
		/// </summary>
		/// <param name="clientId">Client identifier.</param>
		/// <param name="quote">Quote object.</param>
		public override bool SetRiskUpdatedForQuote (BL.RiskAssessment assessment)
		{	
			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_SET_RISK_UPDATEDV3, Method.POST);
			//Add authentication
			request = this.AddAuthentication (request);
			string json = request.JsonSerializer.Serialize (RiskAssessmentConvert (assessment));
			//Service stack requires the quotes to be replaced
			json = json.Replace (@"""", string.Empty);
			request.RequestFormat = DataFormat.Json;
			request.AddParameter ("Risk", json);
			
			//Execute the request
			RestResponse<Responses.QuoteUpdate_Response> response =
				(RestResponse<Responses.QuoteUpdate_Response>)_ServiceClient.Execute<Responses.QuoteUpdate_Response> (request);
             
             
			#if DEBUG
			Console.WriteLine ("RiskAssessmentUpload status = {0}", response.ResponseStatus.ToString ());
			#endif
			
			if (response.StatusCode != System.Net.HttpStatusCode.OK || response.ResponseStatus != ResponseStatus.Completed || (response.Data != null && response.Data.ResponseStatus != null)) {
				if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
					throw new Exceptions.UnauthorisedException ();
					
				return false;
			} else
				return true; 	
			
			
		}

		/// <summary>
		/// Sets the client update or insert.
		/// </summary>
		/// <returns>The client update or insert.</returns>
		/// <param name="client">Client.</param>
		public override bool SetClientUpdateOrInsert(BL.Client client)
		{
			RestRequest request = new RestRequest(ServiceCalls.CONST_SET_CLIENT_UPDATEORINSERT, Method.POST);
			request = this.AddAuthentication(request);
			string json = request.JsonSerializer.Serialize(ClientConvert(client));
			//Service stack requires the quotes to be replaced
			json = json.Replace(@"""", string.Empty);
			request.RequestFormat = DataFormat.Json;
			request.AddParameter("Client", json);

			//Execute the request
			RestResponse<Responses.ClientUpdateResponse> response = (RestResponse<Responses.ClientUpdateResponse>)_ServiceClient.Execute<Responses.ClientUpdateResponse>(request);

			if (response.StatusCode != System.Net.HttpStatusCode.OK || response.ResponseStatus != ResponseStatus.Completed || (response.Data != null && response.Data.ResponseStatus != null))
			{
				if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
					throw new Exceptions.UnauthorisedException();

				return false;
			}
			else
				return true;
		}

		public override bool SetQuoteItemPictureUpload (string quoteItemGUID, int packetCount, int packetNo, byte[] imageArray)
		{
			RestRequest request = new RestRequest (ServiceCalls.CONST_SET_QUOTEITEM_PIC, Method.POST);
			//Add authentication
			request = this.AddAuthentication (request);
			request.RequestFormat = DataFormat.Json;
            
			Types.QuoteItemPicture pic = new Types.QuoteItemPicture ();
            
			pic.ItemGuid = quoteItemGUID;
			pic.PacketCount = packetCount;
			pic.PacketNo = packetNo;
			pic.ImageBytes = imageArray;
            
			string json = request.JsonSerializer.Serialize (pic);
			json = json.Replace (@"""", string.Empty);
			request.AddBody (json);
            
			try {
				RestResponse<Responses.UploadQuoteItemImage_Response> response =
					(RestResponse<Responses.UploadQuoteItemImage_Response>)_ServiceClient.Execute<Responses.UploadQuoteItemImage_Response> (request);

				//RestResponse response = (RestResponse)_ServiceClient.Execute(request);
					
				#if DEBUG
				Console.WriteLine ("Picture Upload status for packet {0}/{1} = {2}", packetNo.ToString (), packetCount.ToString (), response.ResponseStatus.ToString ());
				#endif	
				if (response.StatusCode != System.Net.HttpStatusCode.OK || response.ResponseStatus != ResponseStatus.Completed) //|| (response.Data != null && response.Data.ResponseStatus != null))
					throw new Exception (string.Format("Comms Error. Http response status code: {0}", response.StatusCode));//response.Data.ResponseStatus.ErrorCode);
					
				return true;
			} catch (Exception ex) {
				var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();
				log.Debug(string.Format("Error in quote item image upload - {0}.", ex));
				Core.ArbProInsights.LogErrorError (ex);
				#if DEBUG
				Console.WriteLine ("Picture Upload Error: {0}", ex.Message);
				#endif
				return false;
			}
		}

		public override bool SetQuoteItemPictureUpload (string name, byte[] bytes, string filename)//, string filepath)
		{
			RestRequest request = new RestRequest (ServiceCalls.CONST_SET_QUOTEITEM_PIC, Method.POST);
			//Add authentication
			request = this.AddAuthentication (request);
			request.RequestFormat = DataFormat.Json;
            
			//request.AddFile (filename, filepath);
			request.AddFile (name, bytes, filename);
            
			try {
				RestResponse<Responses.UploadQuoteItemImage_Response> response =
					(RestResponse<Responses.UploadQuoteItemImage_Response>)_ServiceClient.Execute<Responses.UploadQuoteItemImage_Response> (request);

				//RestResponse response = (RestResponse)_ServiceClient.Execute(request);
					
				#if DEBUG
				Console.WriteLine ("Picture Upload status for file {0} = {1}", filename, response.ResponseStatus.ToString ());
				#endif	
				if ((response.StatusCode != System.Net.HttpStatusCode.OK || response.ResponseStatus != ResponseStatus.Completed) || (response.Data != null && response.Data.ResponseStatus != null))
					throw new Exception (string.Format("Comms Error. Http response status code: {0}", response.StatusCode));//response.Data.ResponseStatus.ErrorCode);
					
				return true;
			} catch (Exception ex) {
				var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();
				log.Debug(string.Format("Error in quote item image upload - {0}.", ex));
 				Core.ArbProInsights.LogErrorError (ex);
				#if DEBUG
				Console.WriteLine ("Picture Upload Error: {0}", ex.Message);
				#endif
				return false;
			}
		}

		public override bool SetRiskSignatureUpload (int quoteNo, byte[] bytes, string filename)//, string filepath)
		{
			RestRequest request = new RestRequest (ServiceCalls.CONST_SET_RISKSIGNATURE_PIC, Method.POST);
			//Add authentication
			request = this.AddAuthentication (request);
			request.RequestFormat = DataFormat.Json;
            
			//request.AddFile (filename, filepath);
			request.AddFile (quoteNo.ToString (), bytes, quoteNo.ToString ());
            
			try {
				RestResponse<Responses.UploadRiskSignature_Response> response =
					(RestResponse<Responses.UploadRiskSignature_Response>)_ServiceClient.Execute<Responses.UploadRiskSignature_Response> (request);

				//RestResponse response = (RestResponse)_ServiceClient.Execute(request);
					
				#if DEBUG
				Console.WriteLine ("Picture Upload status for file {0} = {1}", filename, response.ResponseStatus.ToString ());
				#endif	
				if (response.StatusCode != System.Net.HttpStatusCode.OK || response.ResponseStatus != ResponseStatus.Completed) //|| (response.Data != null && response.Data.ResponseStatus != null))
					throw new Exception (string.Format("Comms Error. Http response status code: {0}", response.StatusCode));//response.Data.ResponseStatus.ErrorCode);
					
				return true;
			} catch (Exception ex) {
				if (Core.ArbProInsights.LevelOn (LoggingLevel.Error)) {
					Xamarin.Insights.Report (ex, Insights.Severity.Error);
				}
				var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();
				log.Debug(string.Format("Error in risk signature upload - {0}.", ex));
				#if DEBUG
				Console.WriteLine ("Picture Upload Error: {0}", ex.Message);
				#endif
				return false;
			}
		}

/// <summary>
/// Sends the version of the application.
/// </summary>
/// <param name="version">Version.</param>
		public override void SendVersion (string version)
		{
			//Setup Rest Request Object
			RestRequest request = new RestRequest (ServiceCalls.CONST_SET_SEND_VERSION, Method.POST);
			//Add authentication
			request = this.AddAuthentication (request);
			//Add the required parameters - NOTE: The order in which tey are added is important
			request.AddParameter ("AppVersion", version);

			//Execute the request 
			_ServiceClient.Execute<Responses.SendVersion_Response> (request);

		}

		public override List<BL.CalendarItem> GetCalendarAppointments()
		{
			List<BL.CalendarItem> retVal = new List<BL.CalendarItem>();
			DateTime startDate = DateTime.Now.AddMonths(-2);
			DateTime endDate = DateTime.Now.AddMonths(2);

			Service.WCFServiceClientWrapper Client = new Service.WCFServiceClientWrapper();
			List<Service.CalendarItem> Items;
			Items = Client.GetCalendarItemsForUser(BL.Globals.CurrentUser.UserGuid.ToString(), BL.Globals.CurrentUser.Id, startDate, endDate);

			foreach (var item in Items)
			{
				BL.CalendarItem itm = new BL.CalendarItem();

				itm.StartTime = item.StartTime.ToUniversalTime();
				itm.EndTime = item.EndTime.ToUniversalTime();
				itm.Summary = item.Summary;
				itm.Description = item.Description;
				itm.ItemType = item.ItemType;
				itm.AllDay = item.AllDay;

				retVal.Add(itm);
			}

			return retVal;
		}

		#endregion

		#region Conversion Methods

		/// <summary>
		/// Converts a Sevice object to a business object.
		/// This is implemented due to the possiblity of future modifications to the calling processes
		/// </summary>
		/// <returns>A list of risk assessment business objects.</returns>
		/// <param name="data">Data.</param>
		private List<BL.RiskAssessment> RiskAssessmentListConvert (List<Types.RiskAssessment> data)
		{
			List<BL.RiskAssessment> returnList = new List<BL.RiskAssessment> ();
			
			foreach (Types.RiskAssessment risk in data) {
				returnList.Add (RiskAssessmentConvert (risk));
			}
			
			return returnList;
		}

		private BL.AssessmentSection RiskCriteriaConvert (Types.AssessmentSection data)
		{
			BL.AssessmentSection s = null;
			BL.AssessmentItem itms = null;
			
			s = new BL.AssessmentSection (); // => risk.Criterion
			s.Title = data.Title;
			s.AdditionalDetail = data.AdditionalDetail;
			
			foreach (Types.AssessmentItem riskItem in data.AssessmentItems) {
				itms = new BL.AssessmentItem ();
				itms.Title = riskItem.Title;
				itms.RefCode = riskItem.RefCode;
				
				s.AssessmentItems.Add (itms);
			}
				
			return s;
		}

		List<BL.AssessmentSection> RiskConvert(Types.Risk data)
		{
			List<BL.AssessmentSection> returnList = new List<BL.AssessmentSection>();

			foreach (Types.RiskSection section in data.AssessmentSections)
			{
				BL.AssessmentSection sec = new BL.AssessmentSection();
				sec.AdditionalDetail = section.Comments;
				sec.Title = section.Title;
				sec.Chapter = section.Chapter;
				sec.ReferenceDoc = section.ReferenceDoc;
				sec.RiskId = data.Id;
				sec.SectionId = section.Id;
				sec.Description = section.Description;

				foreach (Types.RiskCriterion c in section.Criteria)
				{
					BL.AssessmentItem criterion = new BL.AssessmentItem();

					criterion.RefCode = c.ChapterRef;
					criterion.Title = c.Criterion;
					criterion.CriterionId = c.Id;

					sec.AssessmentItems.Add(criterion);
				}

				returnList.Add(sec);
			}

			return returnList;
		}

		private BL.RiskAssessment RiskAssessmentConvert (Types.RiskAssessment data)
		{
			BL.RiskAssessment returnObj = new BL.RiskAssessment ();
			BL.AssessmentSection s = null;
			
			BL.AssessmentItem itms = null;
			
			returnObj.QuoteNo = data.QuoteNo;
			returnObj.AerialRescuePlan = data.AerialRescuePlan;
			returnObj.Criterion = new List<BL.AssessmentSection> ();
			foreach (Types.AssessmentSection section in data.Criterion) {
				s = new BL.AssessmentSection (); // => risk.Criterion
				s.Title = section.Title;
				s.AdditionalDetail = section.AdditionalDetail;
				
				foreach (Types.AssessmentItem riskItem in section.AssessmentItems) {
					itms = new BL.AssessmentItem ();
					itms.Title = riskItem.Title;
					itms.RefCode = riskItem.RefCode;
					
					s.AssessmentItems.Add (itms);
				}
				
				returnObj.Criterion.Add (s);
			}	
			
			return returnObj;
		}

		private Types.Risk RiskAssessmentConvert (BL.RiskAssessment data)
		{
			Types.Risk returnObj = new Types.Risk();
			
			Types.RiskSection s = null;
			Types.RiskCriterion itms = null;
			
			returnObj.AppointmentId = data.QuoteNo;
			returnObj.RescueNomaniee = data.AerialRescuePerson == null ? string.Empty : MakeStringServiceStackSafe(data.AerialRescuePerson);
			returnObj.AerialPlanId = data.AerialRescuePlan;
			returnObj.NearestHospital = data.HospitalName == null ? string.Empty : MakeStringServiceStackSafe(data.HospitalName);
			returnObj.MobileSignal = data.MobileSignal == null ? string.Empty : MakeStringServiceStackSafe (data.MobileSignal);
			returnObj.CompletionDate = data.CompletionDate;
			returnObj.SiteAccess = data.RescueAccess == null ? string.Empty : MakeStringServiceStackSafe (data.RescueAccess);
			returnObj.CS38Certifed = false;
			returnObj.GridReference = data.GridReference == null ? string.Empty : MakeStringServiceStackSafe(data.GridReference);
			returnObj.AssessingStaffId = BL.Globals.CurrentUser.Id;
			
			returnObj.AssessmentSections = new List<Types.RiskSection> ();
			foreach (BL.AssessmentSection section in data.Criterion) {
				s = new Types.RiskSection (); // => risk.Criterion

				s.Id = section.SectionId;
				s.Title = section.Title == null ? string.Empty : MakeStringServiceStackSafe(section.Title);
				s.Comments = section.AdditionalDetail == null ? string.Empty : MakeStringServiceStackSafe (section.AdditionalDetail);
				s.ReferenceDoc = section.ReferenceDoc == null ? string.Empty : MakeStringServiceStackSafe(section.ReferenceDoc);
				s.Chapter = section.Chapter == null ? string.Empty : MakeStringServiceStackSafe(section.Chapter);
				s.Description = section.Description == null ? string.Empty : MakeStringServiceStackSafe(section.Description);

				s.Criteria = new List<Types.RiskCriterion> ();
				
				foreach (BL.AssessmentItem riskItem in section.AssessmentItems) {
					itms = new Types.RiskCriterion ();

					itms.Id = riskItem.CriterionId;
					itms.Criterion = riskItem.Title == null ? string.Empty : MakeStringServiceStackSafe(riskItem.Title); ;
					itms.ChapterRef = riskItem.RefCode == null ? string.Empty : MakeStringServiceStackSafe(riskItem.RefCode);
					itms.ControlRequired = (int)riskItem.ControlRequired;
					itms.AssessmentPhase = 1;

					s.Criteria.Add (itms);
				}

				returnObj.Id = section.RiskId;
				returnObj.AssessmentSections.Add (s);
			}

			returnObj.AdditionalRisks = new List<Types.RiskAdditionalItem> ();
			Types.RiskAdditionalItem furtherRisk = null;
			foreach (BL.AssessmentAdditionalItem risk in data.FurtherRisks) {
				furtherRisk = new Types.RiskAdditionalItem ();
				furtherRisk.AssessedRisk = MakeStringServiceStackSafe (risk.Detail);
				furtherRisk.ControlRequired = MakeStringServiceStackSafe (risk.Control);
				
				returnObj.AdditionalRisks.Add (furtherRisk);
			}
			
			return returnObj;
		}

		/// <summary>
		/// Converts a Sevice object to a business object.
		/// This is implemented due to the possiblity of future modifications to the calling processes
		/// </summary>
		/// <returns>The convert.</returns>
		/// <param name="data">Data.</param>
		private BL.Quote QuoteConvert (Types.Quote data)
		{
			BL.Quote q = new BL.Quote ();
			
			q.QuoteId = data.QuoteId;
			q.Comments = data.Comments;
			q.StaffNumber = data.StaffNumber;
			q.TimeHrs = data.TimeHrs;
			q.Items = QuoteItemsConvert (data.Items);
			
			return q;
		}

		private Types.Quote QuoteConvert (BL.Quote data)
		{
			//Setup the service type based on the BL object
			Types.Quote obj = new Types.Quote ();
			//Migrate the properties as required
			obj.QuoteId = data.QuoteId;
			obj.StaffNumber = data.StaffNumber;
			//Quote items are no longer uploaded with the quote object
			//obj.Items = QuoteItemsConvert (data.Items);
			//obj.Assessment = RiskAssessmentConvert(data.Assessment);
			obj.TimeHrs = data.TimeHrs;
			obj.Comments = MakeStringServiceStackSafe (data.Comments);
			List<Types.Tool> tools = new List<Types.Tool> ();

			foreach (BL.QuoteTool t in data.Tools) {
				Types.Tool tool = new Types.Tool();

				tool.Id = t.Id;
				tool.Name = t.Name;
				tool.NumberRequired = t.Number;

				tools.Add(tool);
			}

			obj.ToolsV3 = tools;
			
			return obj;
		}

		private List<BL.QuoteItem> QuoteItemsConvert (List<Types.QuoteItem> data)
		{		
			List<BL.QuoteItem> returnList = new List<BL.QuoteItem> ();
			BL.QuoteItem q = null;
			
			if (data != null) {
				foreach (Types.QuoteItem item in data)
				{
					q = new BL.QuoteItem();
					q.ItemNumber = item.ItemNumber;
					q.ItemType = item.ItemType;
					q.ItemValue = (decimal)item.ItemValue;
					q.Location = new BL.GeoLocation(item.Location);

					switch ((int)item.PreservationOrderStatus)
					{
						case 0:
							q.PreservationOrderStatus = BL.EnumPreservationOrderStatus.Yes;
							break;
						case 1:
							q.PreservationOrderStatus = BL.EnumPreservationOrderStatus.No;
							break;
						case 2:
							q.PreservationOrderStatus = BL.EnumPreservationOrderStatus.Unknown;
							break;
						default:
							q.PreservationOrderStatus = BL.EnumPreservationOrderStatus.Unknown;
							break;
					}

					returnList.Add(q);
				}
			}
			return returnList;
		}

		private List<Types.QuoteItem> QuoteItemsConvert (List<BL.QuoteItem> data)
		{		
			List<Types.QuoteItem> typeItems = new List<Types.QuoteItem> ();

			foreach (BL.QuoteItem blItem in data) {
				typeItems.Add (ConvertAQuoteItem (blItem));
			}
			
			return typeItems;
		}

		private Types.QuoteItem ConvertAQuoteItem (BL.QuoteItem blItem)
		{
			Types.QuoteItem typeItem = new Types.QuoteItem ();
			typeItem.ItemGuid = blItem.Guid.ToString ();//.ToString ();
#if DEBUG
			//typeItem.ItemGuid = "7bce375e-74ab-4aef-b818-1905ac625656";				
#endif
			blItem.ItemNumber = blItem.ItemNumber ?? string.Empty;
			blItem.WorkDescription = blItem.WorkDescription ?? string.Empty;
			typeItem.ItemNumber = MakeStringServiceStackSafe (blItem.ItemNumber);
			typeItem.ItemType = MakeStringServiceStackSafe (blItem.ItemType);
			typeItem.ItemValue = (float)blItem.ItemValue;
			if (blItem.Location != null) {
				typeItem.Location = new Types.GeoLocation ();
				typeItem.Location.Latitude = blItem.Location.Latitude;
				typeItem.Location.Longitude = blItem.Location.Longitude;
			} else {
				typeItem.Location = new Types.GeoLocation ();
			}

			switch ((int)blItem.PreservationOrderStatus)
			{
				case 0:
					typeItem.PreservationOrderStatus = Types.QuoteItem.enumPreservationOrderStatus.Unknown;
					break;
				case 1:
					typeItem.PreservationOrderStatus = Types.QuoteItem.enumPreservationOrderStatus.No;
					break;
				case 2:
					typeItem.PreservationOrderStatus = Types.QuoteItem.enumPreservationOrderStatus.Yes;
					break;
				default:
					typeItem.PreservationOrderStatus = Types.QuoteItem.enumPreservationOrderStatus.Unknown;
					break;
			}

			typeItem.WorkDescription = MakeStringServiceStackSafe (blItem.WorkDescription);

			return typeItem;
		}

		Types.Client ClientConvert(BL.Client client)
		{
			Types.Client c = new Types.Client();

			c.Id = client.Id;
			c.AltAccountNo = client.AltAccountNo;
			c.Title = client.Title == null ? string.Empty:MakeStringServiceStackSafe(client.Title);
			c.Forename = client.Forename == null ? string.Empty : MakeStringServiceStackSafe(client.Forename);
			c.Surname = client.Surname == null ? string.Empty : MakeStringServiceStackSafe(client.Surname);
			c.BusinessName = client.BusinessName == null ? string.Empty : MakeStringServiceStackSafe(client.BusinessName);
			c.HouseNameNumber = client.HouseNameNumber == null ? string.Empty : MakeStringServiceStackSafe(client.HouseNameNumber);
			c.Address1 = client.Address1 == null ? string.Empty : MakeStringServiceStackSafe(client.Address1);
			c.Address2 = client.Address2 == null ? string.Empty : MakeStringServiceStackSafe(client.Address2);
			c.Town = client.Town == null ? string.Empty : MakeStringServiceStackSafe(client.Town);
			c.County = client.County == null ? string.Empty : MakeStringServiceStackSafe(client.County);
			c.PostCode = client.PostCode == null ? string.Empty : MakeStringServiceStackSafe(client.PostCode);
			c.Sector = client.Sector == null ? string.Empty : MakeStringServiceStackSafe(client.Sector);
			c.Telephone = client.Telephone == null ? string.Empty : MakeStringServiceStackSafe(client.Telephone);
			c.Mobile = client.Mobile == null ? string.Empty : MakeStringServiceStackSafe(client.Mobile);
			c.Email = client.Email == null ? string.Empty : MakeStringServiceStackSafe(client.Email);
			c.WebAddress = client.WebAddress == null ? string.Empty : MakeStringServiceStackSafe(client.WebAddress);
			c.AccountType = client.AccountType;
			c.DateOpened = client.DateOpened;
			c.AddressLatitude = client.AddressLatitude;
			c.AddressLongitude = client.AddressLongitude;
			c.TermsAgreed = client.TermsAgreed;
			c.SendSmsMessage = client.SendSmsMessage;
			c.IncludeInMarketing = client.IncludeInMarketing;

			return c;
		}

		#endregion

		#endregion

		private string MakeStringServiceStackSafe (string value)
		{
			string tmp = System.Web.HttpUtility.UrlEncode (value);
			return tmp;
			/*string tmp = value.Replace(",",string.Empty)
				.Replace("{","(")
				.Replace("}",")")
				//.Replace("\n"," ")
				.Replace(@"""",string.Empty)
				.Replace("  "," ")
				.Trim();
			return tmp; */
		}
	}
}

