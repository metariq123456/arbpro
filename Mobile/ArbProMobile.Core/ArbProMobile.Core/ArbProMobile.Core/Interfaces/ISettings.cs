using System;
using System.Collections.Generic;
using Enums = DGS.ArbPro.Mobile.Core.Enums;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.Mobile.Interfaces
{
    /// <summary>
    /// The ISettings interface should be implemented by any client, to hold settings which the client 
    /// application must persist across device reboots and/or application shutdowns.
    /// </summary>
    public interface ISettings
    {
        #region User-editable settings

        /// <summary>
        /// The URI of the service
        /// </summary>
        string ServiceURI { get; set; }

        /// <summary>
        /// Gets of sets the number of days to retain uploaded/cancelled quotes.
        /// </summary>
        /// <value>The days for which to hold data.</value>
        int DataRetainDays
        {
            get;
            set;
        }

        #region Filter Settings

        Enums.StatusFilterEnum StatusFilter { get; set; }

        Enums.DateFilterTypeEnum DateFilterType { get; set; }

        DateTime DateFilter { get; set; }

        #endregion

        #endregion

        #region Silently persisted settings

        /// <summary>
        /// The username used for the last login. This should not be presented on the settings screen, but 
        /// should be persisted between device resets.
        /// </summary>
        string Username { get; set; }

        string PasswordHash { get; set; }

        #endregion
        
    }
}
