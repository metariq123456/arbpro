using System;
using System.Collections.Generic;
using DGS.ArbPro.Mobile.Core.Enums;

//using DGS.ArbPro.Mobile.BL.ServiceLayer;
namespace DGS.ArbPro.Mobile.Interfaces
{
    /// <summary>
    /// The IGlobals interface provides a platform-independent mechanism for holding objects and values across the application.
    /// </summary>
    public interface IGlobals
    {        
		/// <summary>
		/// The authentication keys for the last valid login
		/// </summary>
        Dictionary<string,string> AuthenticationKeys { get; set; }
		
		/// <summary>
		/// The date and time of the last user login.
		/// Populated when a user succesfully logins in.
		/// Used when the the application returns to the forground
		/// </summary>
        DateTime LastAppLogin { get; set; }

        LoggingLevel LoggingLevel { get; set; }
    }
}
