using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Enums = DGS.ArbPro.Mobile.Core.Enums;

namespace DGS.ArbPro.Mobile.Interfaces
{
    /// <summary>
    /// The IUserNotificationService interface should be implemented by any client, to enable 
    /// consistancy between platforms for displaying messages and notifications to the user.
    /// </summary>
    public interface IUserNotificationService
    {
		Enums.NotificationResultEnum Result {get;set;}
    
        void ShowMessge(Enums.NotificationTypeEnum msg_type, string msg_body);

        void ShowModalMessage(Enums.NotificationTypeEnum msg_type, string msg_body, EventHandler<EventArgs> msg_dissmissed_handler);

        void StartActivityIndicator();

        void StopActivityIndicator();
        
        void StartActivityIndicator(int max_value);
        
        void IncreaseProcessProgress(int mod_value);
        
        void DisplayProgress();
        
        void PromptForConformation(Enums.NotificationTypeEnum msg_type, string msg_body);
    }
}
