// //*********************************************************************
// // $Id$
// // $Author$
// // $Date$
// // $Change$
// //*********************************************************************
//
using System;
using System.Collections.Generic;
using DGS.ArbPro.Mobile;
using DGS.ArbPro.Service;
using DGS.ArbPro.Mobile.Core.Enums;
using BL = DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.Interfaces
{
	public interface IServiceConnection
	{
		#region Properties
		bool Connected { get; set; }

		string UserName { get; }

		string LastError { get; }
		#endregion
		#region Methods
		IServiceConnection GetConnection (string username, string password);

		IServiceConnection GetConnection ();
		//Authentication
		BL.User AuthenticateUser (string username);

		BL.User AuthenticateUser ();
		//Get Data Service Calls
		List<BL.Appointment> GetAppointmentListForUser (int userId);

		List<BL.RiskAssessment> GetRisksListForOpenAppointments (int userId);

		List<BL.ItemDescription> GetItemDescriptionListForClient ();

		List<BL.ItemType> GetItemTypeListForClient ();

		List<BL.Tool> GetToolListForClient ();

		List<BL.Client> GetClientList();

		List<BL.AssessmentSection> GetRiskAssessmentCriteriaList ();

		List<BL.AssessmentSection> GetRiskAssessment(int templateId, int appointmentId);

		List<BL.RiskHospital> GetRiskHospitalList ();

		List<BL.RiskAerialRescuePlan> GetRiskAerialRescuePlanList ();

		List<BL.CalendarItem> GetCalendarAppointments();

		//Set Data Service Calls
		bool SetAppoinmentDownloaded (int appointmentId);

		bool SetQuoteUpdatedForClient (DGS.ArbPro.Mobile.BL.Quote quote);

		bool SetQuoteItemUpdateForClient(int quoteID, DGS.ArbPro.Mobile.BL.QuoteItem item);

		bool SetRiskUpdatedForQuote (BL.RiskAssessment assessment);

		bool SetClientUpdateOrInsert(BL.Client client);

		bool SetQuoteItemPictureUpload (string quoteItemGUID, int packetCount, int packetNo, byte[] imageArray);

		bool SetQuoteItemPictureUpload (string name, byte[] bytes, string filename);

		bool SetRiskSignatureUpload (int quoteNo, byte[] bytes, string filename);
		// Information Calls
		void SendVersion (string appVersion);
		#endregion
	}
}

