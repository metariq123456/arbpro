//*********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// $Date$
// $Change$
//*********************************************************************
//
using System;
using System.Collections.Generic;
using System.IO;
using Xamarin;
using DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions;
using DGS.ArbPro.Mobile.DAL;
using DGS.ArbPro.Mobile.Core.Enums;
using DGS.ArbPro.Service.Model.Types;
using System.Threading.Tasks;

namespace DGS.ArbPro.Mobile.BL
{
    public static class BusinessLayer
    {

        #region Delegates and Events

        /// <summary>
        /// The reference data downloaded event. This is used to provide a message of the current state of the application
        /// </summary>
        public delegate  void ActivityProcessStageDelegate (string message);

        public static ActivityProcessStageDelegate ActivityProcessStageEvent;

        /// <summary>
        /// The appoinments downloaded event. Used to provide information on the total number of items to be processed
        /// </summary>
        public delegate  void ItemListCountActivityDelegate (int count);

        public static ItemListCountActivityDelegate ItemListCountActivityEvent;

        /// <summary>
        /// The appoinment saved event. Used to notify a succesful save to the database
        /// </summary>
        public delegate  void ItemSavedDelegate (int value);

        public static ItemSavedDelegate ItemSavedEvent;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating if the download process should be canceled.
        /// </summary>
        /// <value><c>true</c> if cancel download; otherwise, <c>false</c>.</value>
        public static bool CancelActivity
        {
            get;
            set;
        }

        /// <summary>
        /// The version of the mobile application
        /// </summary>
        /// <value>The app version.</value>
        public static string AppVersion
        {
            get;
            set;
        }

        public static string ConnectionType
        {
            get;
            set;
        }

		#region database locks

		static Object getAppointmentsFilteredLock = new Object();
		static Object getAppointmentLock = new Object();
		static Object getAppointmentsLock = new Object();
		static Object getTemplatesLock = new Object();
		static Object getQuoteLock = new Object();
		static Object getItemTypesLock = new Object();
		static Object getItemDescriptionsLock = new Object();
		static Object getToolsLock = new Object();
		static Object getHospitalsLock = new Object();
		static Object getRescuePlansLock = new Object();
		static Object getClientsLock = new Object();

		static Object saveAppointmentLock = new Object();
		static Object saveQuoteLock = new Object();
		static Object saveAllClientsLock = new Object();
		static Object saveAppointmentItemsLock = new Object();
		static Object saveClientLock = new Object();
		static Object saveTemplatesLock = new Object();

		static Object refDataLock = new Object();
		static Object refData2Lock = new Object();

		#endregion

        #endregion

        #region Security Methods

        public static bool LocalLogin(string username, string password)
        {
            bool ReturnValue;
            //TODO put real login processing here
            if (username == "laveryj" && password == "password")
            {
                ReturnValue = true;
            } else
            {
                ReturnValue = false;
            }

            if (ReturnValue)
            {
                Globals.LoginStatus = Globals.LoggedInStatus.LoggedInOffline;
                Repository.Instance().DatabaseClose();
            }
            return ReturnValue;
        }

        public static void LocalLogout()
        {
            Globals.LoginStatus = Globals.LoggedInStatus.NotLoggedIn;
            Repository.Instance().DatabaseClose();
        }

        #endregion

        #region Database Managment Methods

        /// <summary>
        /// Reset the Databases.
        /// </summary>
        public static void DatabaseReset()
        {
            DAL.Repository.Instance().DatabaseReset();
        }

        #endregion

        #region Database item methods

        /// <summary>
        /// Get a list of Appointments from the database.
        /// </summary>
        /// <returns>The list.</returns>
        public static List<Appointment> AppointmentList()
        {
			lock (getAppointmentsLock)
			{
				return DAL.Repository.Instance().AppointmentList();
			}
        }

        /// <summary>
        /// Gets a list of appointments, with the filters applied
        /// </summary>
        /// <returns>The list filtered.</returns>
        public static List<Appointment>AppointmentListFiltered()
        {
			lock (getAppointmentsFilteredLock)
			{
				return DAL.Repository.Instance().AppointmentListFiltered();
			}
        }

        public static Appointment AppointmentGet(int appointmentId)
        {
			lock (getAppointmentLock)
			{
				return DAL.Repository.Instance().AppointmentGet(appointmentId);
			}
        }

        /// <summary>
        /// Save an Appoinment to the Database.
        /// </summary>
        /// <returns>The save.</returns>
        /// <param name="appointment">Appointment.</param>
        public static int AppointmentSave(Appointment appointment)
        {
			lock (saveAppointmentLock)
			{
				return DAL.Repository.Instance().AppointmentSave(appointment);
			}
        }

        public static void AppointmentSetupTestItems()
        {
            DAL.Repository.Instance().SetupTest();
        }

        public static int QuoteSave(Quote quote)
        {
			lock (saveQuoteLock)
			{
				return DAL.Repository.Instance().QuoteSave(quote);
			}
        }

        public static Quote QuoteGet(int quoteId)
        {
			lock (getQuoteLock)
			{
				return DAL.Repository.Instance().QuoteGet(quoteId);
			}
        }

        public static List<ItemType> ItemTypeList()
        {
			lock (getItemTypesLock)
			{
				return DAL.Repository.Instance().ItemTypeList();
			}
        }

        public static List<ItemDescription> ItemDescriptionList()
        {
			lock (getItemDescriptionsLock)
			{
				return DAL.Repository.Instance().ItemDescriptionList();
			}
        }

        public static List<Tool> ToolList()
        {
			lock (getToolsLock)
			{
				return DAL.Repository.Instance().ToolList();
			}
        }

        public static List<RiskHospital> HospitalList()
        {
			lock (getHospitalsLock)
			{
				return DAL.Repository.Instance().HospitalList();
			}
        }

        public static List<RiskAerialRescuePlan> AerialRescuePlanList()
        {
			lock (getRescuePlansLock)
			{
				return DAL.Repository.Instance().AerialRescuePlanList();
			}
        }

		public static List<Client> ClientList()
		{
			lock(getClientsLock)
			{
				return DAL.Repository.Instance().ClientList();
			}
		}

		public static List<RiskTemplate> TemplateList()
		{
			lock(getTemplatesLock)
			{
				return DAL.Repository.Instance().RiskTemplateList();
			}
		}

        public static int GetOrphanedPicCount()
        {
            int count = 0;
            List<QuoteItem> items = null;
            //List<FileInfo> deleteFiles = new List<FileInfo> ();
            try
            {
                items = DAL.Repository.Instance().ValidQuoteItemsGet();

                string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/QuoteItemImages";

                DirectoryInfo dirInfo = new DirectoryInfo(filePath);

				if (dirInfo.Exists)
				{
					QuoteItem itm = null;
					//Iterate through all the files in the directory
					foreach (FileInfo file in dirInfo.GetFiles())
					{
						//Check if the process has been cancelled
						if (CancelActivity)
							break;

						//Try to find an item with a GUID the same as the current filename
						itm = items.Find(qi => qi.Guid.ToString() == file.Name.Replace(".png", ""));
						if (itm == null)
						{
							count++;
						}
					}
				}
            } catch (Exception ex)
            {
                Core.ArbProInsights.LogErrorError(ex);
                Console.WriteLine("Error in GetOrphanedPicCount - {0}", ex.ToString());
                count = -1;
            }

            return count;
        }

        public static int DeleteOrphanedPictures()
        {
            Core.ArbProInsights.LogActivity("User deleting orphaned pictures.", LoggingLevel.Debug);
            int count = 0;
            List<QuoteItem> items = null;
            List<FileInfo> files = new List<FileInfo>();
            try
            {
                items = DAL.Repository.Instance().ValidQuoteItemsGet();
				
                string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/QuoteItemImages";

                DirectoryInfo dirInfo = new DirectoryInfo(filePath);
                files.AddRange(dirInfo.GetFiles());
                //Send process notification events
                if (ItemListCountActivityEvent != null)
                    ItemListCountActivityEvent(files.Count);
				
                if (ActivityProcessStageEvent != null)
                    ActivityProcessStageEvent("Deleting Quote Pictures");

                FileInfo aFile = null;
                QuoteItem itm = null;
                //Iterate through all the files in the directory
                for (int index = files.Count - 1; index >= 0; index--)
                {
                    //Check if the process has been cancelled
                    if (CancelActivity)
                        break;

                    aFile = files [index];
                    //Try to find an item with a GUID the same as the current filename
                    itm = items.Find(qi => qi.Guid.ToString() == aFile.Name.Replace(".png", ""));
                    if (itm == null)
                    {
                        files.RemoveAt(index);
                        aFile.Delete();
                        count++;

                        if (ItemSavedEvent != null)
                            ItemSavedEvent(1);
                    }
                }
            } catch (Exception ex)
            {

                Core.ArbProInsights.LogErrorError(ex);
                Console.WriteLine("Error in DeleteOrphanedPictures - {0}", ex.ToString());
            }
            Core.ArbProInsights.LogActivity(string.Format("{0} Orphaned pictures deleted.", count), LoggingLevel.Debug);
            return count;
        }

        #endregion

        #region Service Managment Methods

        #region Download Methods

        /// <summary>
        /// Login to service.
        /// </summary>
        /// <returns>The to service.</returns>
        /// <param name="username">Username.</param>
        /// <param name="password">Password.</param>
        public static BL.User LoginToService(string username, string password)
        {
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();

			ITrackHandle iHandle = Core.ArbProInsights.GetActivityTimer("User Login", LoggingLevel.Debug);
            Interfaces.IServiceConnection con;
            try
            {
                con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>().GetConnection(username, password);
                Globals.CurrentUser = con.AuthenticateUser();

                Core.ArbProInsights.SetIdentity(Globals.CurrentUser.UserName, Globals.CurrentUser.DisplayName);
                Core.ArbProInsights.LogActivity(string.Format("Logged in online as {0} '{1}'", Globals.CurrentUser.UserName, Globals.CurrentUser.DisplayName), LoggingLevel.Info);

                // Send the application version 
                SendVersion(string.Format("{0} ({1})",AppVersion,ConnectionType));

				log.Debug(string.Format("Connection status - {0}.", ConnectionType));
            } catch (Exception ex)
            {
                if (!(ex is NullReferenceException))
                {
                    Core.ArbProInsights.LogErrorError(ex, LoggingLevel.Fatal, "User login error process aborted. Error is being rethrown.");
					log.Debug(string.Format("User login error process aborted. Error is being rethrown. {0}.", ex.Message));
                    Globals.CurrentUser = null;
                    throw ex;
                } else
                {
                    Core.ArbProInsights.LogActivity(string.Format("Login failed for {0}", username), LoggingLevel.Info);
					log.Debug(string.Format("Login failed for {0}. Message - {1}.", username, ex.Message));
                    Globals.CurrentUser = null;
                }
            } finally
            {
                if (iHandle != null)
                {
                    iHandle.Stop();
                }
            }

            return Globals.CurrentUser;
        }

        public static void SendVersion(string version)
        {
            Core.ArbProInsights.LogActivity(string.Format("Sending version '{0}' to server", version), LoggingLevel.Info);
            Interfaces.IServiceConnection con;
            try
            {
                con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>().GetConnection();
                con.SendVersion(version);
            } catch (Exception ex)
            {
                Core.ArbProInsights.LogErrorError(ex);
                throw ex;
            }
        }

        public static void LogoutOfService()
        {
            Interfaces.IServiceConnection con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>();
            if (con.Connected)
            {
                con.Connected = false;
            }

            //Settings.Logout ();
            //Globals.CurrentUser = null;
            Globals.LoginStatus = Globals.LoggedInStatus.NotLoggedIn;
            Globals.AuthenticationKeys = null;
        }

        /// <summary>
        /// Download the list of Appointments.
        /// </summary>
        /// <returns>The total number of appoinments saved to the database durring the download process</returns>
        /// <param name="userId">User identifier.</param>
        /// <param name="fromDate">From date.</param>
        /// <param name="toDate">To date.</param>
        /// <param name="updatedSince">Updated since.</param>
        /// <param name="clientID">Client identifier.</param>
        public static int AppointmentListDownload(int userId)
        {
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();

            Core.ArbProInsights.LogActivity("Downloading appointments", LoggingLevel.Info);

            CancelActivity = false;
            Interfaces.IServiceConnection con;
            int savedCount = 0;
            var Handle = Core.ArbProInsights.GetActivityTimer("Downloading appointments", LoggingLevel.Info);
            try
            {
                List<BL.Appointment> appointments = null;
                con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>().GetConnection();
                //Get the required data
                appointments = con.GetAppointmentListForUser(userId);

				if (appointments == null || appointments.Count == 0)
				{
					log.Debug(string.Format("No appointments found for user id {0}", userId));
					return -1;
				}

				log.Debug(string.Format("{0} appointments downloaded for user id {1}.", appointments.Count, userId));

                //Iterate through each appointment in the list, save it to the database and  raise the appropriate events

                if (BusinessLayer.ItemListCountActivityEvent != null)
                    ItemListCountActivityEvent(appointments.Count);

                savedCount = 0;
                for (int index = 0; index < appointments.Count; index++)
                {
                    if (!DAL.Repository.Instance().AppointmentExistsForServiceId(appointments [index].Id))
                    {
                        BusinessLayer.AppointmentSave(appointments [index]);
                        //Create a quote for the appoinment
                        con.SetAppoinmentDownloaded(appointments [index].Id);
                        appointments [index].AppointmentQuote = new Quote();
                        appointments [index].AppointmentQuote.QuoteId = appointments [index].QuoteNo;
                        appointments [index].AppointmentQuote.AppointmentRecId = appointments [index].RecId;

						appointments[index].AppointmentQuote.Assessment = new RiskAssessment();
						appointments[index].AppointmentQuote.Assessment.QuoteNo = appointments[index].QuoteNo;
                        appointments [index].AppointmentQuote.Assessment.Criterion = con.GetRiskAssessment(appointments[index].RiskTemplateId, appointments[index].Id);

                        if (BusinessLayer.QuoteSave(appointments [index].AppointmentQuote) <= 0)
                        {
							log.Debug(string.Format("Quote save failed for appointment. QuoteId={0}", appointments[index].AppointmentQuote));
                            throw new Exception(string.Format("Quote save failed for appointment. QuoteId={0}", appointments [index].AppointmentQuote));
                        }


                        savedCount++;
                    } else
                    {
                        // we want to update the appointment with any changes from the server - but preserve the status
                        Appointment ApptExisting = DAL.Repository.Instance().AppointmentForServiceId(appointments [index].Id);
                        appointments [index].Status = ApptExisting.Status;
                        appointments [index].RecId = ApptExisting.RecId;

                        BusinessLayer.AppointmentSave(appointments [index]);
                    }

                    if (BusinessLayer.ItemSavedEvent != null)
                        ItemSavedEvent(1);

                    if (CancelActivity)
                        break;
                }

				log.Debug(string.Format("Appointments downloaded: {0}.", savedCount));

                return savedCount;
            } catch (Exception ex)
            {
				log.Debug(string.Format("Error in AppointmentListDownload. User id: {0}. Message: {1}.", userId, ex.Message));
                Core.ArbProInsights.LogErrorError(ex);
                if (ActivityProcessStageEvent != null)
                    ActivityProcessStageEvent("Download Error");

                return 0;
            } finally
            {
				log.Debug(string.Format("Appointments downloaded: {0}.", savedCount));
                Core.ArbProInsights.LogActivity(string.Format("{0} appointments downloaded", savedCount), LoggingLevel.Info);
                /*
		        if (Core.ArbProInsights.LevelOn(LoggingLevel.Info))
		        {
		            Xamarin.Insights.Track(string.Format("{0} appointments downloaded", savedCount));
		        }
				*/
                if (Handle != null)
                {
                    Handle.Stop();
                }
            }
        }

        public static void ReferenceDataDownload()
        {
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();
            var Handle = Core.ArbProInsights.GetActivityTimer("Downloading reference data", LoggingLevel.Info);
            /*
				if (Core.ArbProInsights.LevelOn(LoggingLevel.Info))
            {
                Handle = Xamarin.Insights.TrackTime("Downloading reference data");
            }
			*/
            CancelActivity = false;
            Interfaces.IServiceConnection con = null;
            List<BL.ItemDescription> descriptions = null;
            List<BL.ItemType> types = null;
            List<BL.Tool> tools = null;
            List<BL.AssessmentSection> RiskCriteria = null;
            List<BL.RiskHospital> Hospitals = null;
            List<BL.RiskAerialRescuePlan> RescuePlans = null;
			List<BL.Client> clients = null;
			List<BL.CalendarItem> calendarItems = null;
			
            try
            {
                if (ItemListCountActivityEvent != null)
                    ItemListCountActivityEvent(11);
					
                con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>().GetConnection();
                //Download all reference data and provide status information to any subscribers
                if (!CancelActivity)
                {
                    if (ActivityProcessStageEvent != null)
                        ActivityProcessStageEvent("Risk Criteria Downloading");
					
                    RiskCriteria = con.GetRiskAssessmentCriteriaList();
					log.Debug(string.Format("{0} risk criteria downloaded.", RiskCriteria.Count));
                    
                    if (ItemSavedEvent != null)
                        ItemSavedEvent(1);
                }
                if (!CancelActivity)
                {
                    if (ActivityProcessStageEvent != null)
                        ActivityProcessStageEvent("Hospital List Downloading");
					
                    Hospitals = con.GetRiskHospitalList();
					log.Debug(string.Format("{0} hospitals downloaded.", Hospitals.Count));
                    
                    if (ItemSavedEvent != null)
                        ItemSavedEvent(1);
                }
                if (!CancelActivity)
                {
                    if (ActivityProcessStageEvent != null)
                        ActivityProcessStageEvent("Aerial Rescue Plans Downloading");
					
                    RescuePlans = con.GetRiskAerialRescuePlanList();
					log.Debug(string.Format("{0} rescue plans downloaded.", RescuePlans.Count));
                    
                    if (ItemSavedEvent != null)
                        ItemSavedEvent(1);
                }
                if (!CancelActivity)
                {
                    if (ActivityProcessStageEvent != null)
                        ActivityProcessStageEvent("Updating Risk Assessments");

					lock (refDataLock)
					{
						DAL.Repository.Instance().SaveRiskReferenceData(RiskCriteria, Hospitals, RescuePlans);
					}
                    
                    if (ItemSavedEvent != null)
                        ItemSavedEvent(1);
                }
                if (!CancelActivity)
                {
                    if (ActivityProcessStageEvent != null)
                        ActivityProcessStageEvent("Types Downloading");
					
                    types = con.GetItemTypeListForClient();
					log.Debug(string.Format("{0} types downloaded.", types.Count));
                    
                    if (ItemSavedEvent != null)
                        ItemSavedEvent(1);
                }
                if (!CancelActivity)
                {
                    if (ActivityProcessStageEvent != null)
                        ActivityProcessStageEvent("Descriptions Downloading");
					
                    descriptions = con.GetItemDescriptionListForClient();
					log.Debug(string.Format("{0} item descriptions downloaded.", descriptions.Count));
                    
                    if (ItemSavedEvent != null)
                        ItemSavedEvent(1);
                }
                if (!CancelActivity)
                {
                    if (ActivityProcessStageEvent != null)
                        ActivityProcessStageEvent("Tools Downloading");
					
                    tools = con.GetToolListForClient();
					log.Debug(string.Format("{0} tools downloaded.", tools.Count));
                    
                    if (ItemSavedEvent != null)
                        ItemSavedEvent(1);
                }
				if (!CancelActivity)
				{
					if (ActivityProcessStageEvent != null)
						ActivityProcessStageEvent("Clients Downloading");

					clients = con.GetClientList();
					log.Debug(string.Format("{0} clients downloaded.", clients.Count));

					if (ItemSavedEvent != null)
						ItemSavedEvent(1);
				}
				if (!CancelActivity)
				{
					if (ActivityProcessStageEvent != null)
						ActivityProcessStageEvent("Calendar Appointments Downloading");

					calendarItems = con.GetCalendarAppointments();

					log.Debug(string.Format("{0} calendar appointments downloaded.", calendarItems.Count));

					if (ItemSavedEvent != null)
						ItemSavedEvent(1);
				}
				if (!CancelActivity)
				{
					if (ActivityProcessStageEvent != null)
						ActivityProcessStageEvent("Risk Templates Downloading");

					RiskTemplatesShortenedListDownload();

					if (ItemSavedEvent != null)
						ItemSavedEvent(1);
				}
                if (!CancelActivity)
                {
                    if (ActivityProcessStageEvent != null)
                        ActivityProcessStageEvent("Updating database");

					lock (refData2Lock)
					{
						DAL.Repository.Instance().SaveReferenceData(types, descriptions, tools, clients);
						DAL.Repository.Instance().CalendarItemSaveList(calendarItems);
					}
                    
                    if (ItemSavedEvent != null)
                        ItemSavedEvent(1);
						
                    Settings.HasDownloadedRefData = true;
                }
            } catch (Exception ex)
            {
				log.Debug(string.Format("Error in ReferenceDataDownload. Message: {0}.", ex.Message));
                Core.ArbProInsights.LogErrorError(ex);
                throw ex;
            }

            if (Handle != null)
            {
                Handle.Stop();
            }
        }

        /// <summary>
        /// Download the Item Description list.
        /// </summary>
        /// <returns>The description list download.</returns>
        /// <param name="clientId">Client identifier.</param>
        public static List<BL.ItemDescription> ItemDescriptionListDownload()
        {
            Interfaces.IServiceConnection con;
            try
            {
                con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>().GetConnection();
                return con.GetItemDescriptionListForClient();
            } catch (Exception ex)
            {

                Core.ArbProInsights.LogErrorError(ex);
                throw ex;
            }
        }

        /// <summary>
        /// Download the item type list download
        /// </summary>
        /// <returns>The downloaded list.</returns>
        /// <param name="clientId">Client identifier.</param>
        public static List<BL.ItemType> ItemTypeListDownload()
        {
            Interfaces.IServiceConnection con;
            try
            {
                con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>().GetConnection();
                return con.GetItemTypeListForClient();
            } catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<BL.Tool> ToolListDownload()
        {
            Interfaces.IServiceConnection con;
            try
            {
                con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>().GetConnection();
                return con.GetToolListForClient();
            } catch (Exception ex)
            {

                Core.ArbProInsights.LogErrorError(ex);
                throw ex;
            }
        }

		public static void ClientListDownload()
		{
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();
			var Handle = Core.ArbProInsights.GetActivityTimer("Downloading clients list", LoggingLevel.Info);
			Interfaces.IServiceConnection con;
			List<BL.Client> clients = null;
			CancelActivity = false;
			try
			{
				if (ItemListCountActivityEvent != null)
					ItemListCountActivityEvent(2);
				
				con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>().GetConnection();

				if (!CancelActivity)
				{
					if (ActivityProcessStageEvent != null)
						ActivityProcessStageEvent("Clients Downloading");

					clients = con.GetClientList();
					log.Debug(string.Format("{0} clients downloaded.", clients.Count));

					if (ItemSavedEvent != null)
						ItemSavedEvent(1);
				}

				if (!CancelActivity)
				{
					if (ActivityProcessStageEvent != null)
						ActivityProcessStageEvent("Updating database");
					
					log.Debug("Updating database.");

					lock (saveAllClientsLock)
					{
						DAL.Repository.Instance().SaveAllClients(clients);
					}

					if (ItemSavedEvent != null)
						ItemSavedEvent(1);
				}
			}
			catch (Exception ex)
			{
				log.Debug(string.Format("Error in ReferenceDataDownload. Message: {0}.", ex.Message));
				Core.ArbProInsights.LogErrorError(ex);
				throw (ex);
			}


			if (Handle != null)
			{
				Handle.Stop();
			}
		}

		public static void AppointmentItemsListDownload()
		{
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();
			var Handle = Core.ArbProInsights.GetActivityTimer("Downloading calendar appointments list", LoggingLevel.Info);
			Interfaces.IServiceConnection con;
			List<BL.CalendarItem> calendarItems = null;
			CancelActivity = false;
			try
			{
				if (ItemListCountActivityEvent != null)
					ItemListCountActivityEvent(2);

				con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>().GetConnection();

				if (!CancelActivity)
				{
					if (ActivityProcessStageEvent != null)
						ActivityProcessStageEvent("Calendar Appointments Downloading");

					calendarItems = con.GetCalendarAppointments();

					log.Debug(string.Format("{0} calendar appointments downloaded.", calendarItems.Count));

					if (ItemSavedEvent != null)
						ItemSavedEvent(1);
				}

				if (!CancelActivity && calendarItems.Count > 0)
				{
					if (ActivityProcessStageEvent != null)
						ActivityProcessStageEvent("Updating database");

					log.Debug("Updating database.");

					lock (saveAppointmentItemsLock)
					{
						DAL.Repository.Instance().CalendarItemSaveList(calendarItems);
					}

					if (ItemSavedEvent != null)
						ItemSavedEvent(1);
				}
			}
			catch (DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException e)
			{
				throw e;
			}
			catch (Exception ex)
			{
				log.Debug(string.Format("Error in AppointmentItemsListDownload. Message: {0}.", ex.Message));
				Core.ArbProInsights.LogErrorError(ex);
			}


			if (Handle != null)
			{
				Handle.Stop();
			}
		}

		public static void RiskTemplatesShortenedListDownload()
		{
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();
			List<BL.RiskTemplate> retVal = null;

			try
			{
				log.Debug("Downloading lightened risk template list.");

				Service.WCFServiceClientWrapper Client = new Service.WCFServiceClientWrapper();

				// call to wcf service
				retVal = Client.GetRiskTemplateList();

				log.Debug(string.Format("Completed download lightened risk template list. Risk template count: {0}", retVal.Count));

				log.Debug("Saving lightened risk template list to db.");

				if (retVal.Count > 0)
				{
					lock (saveTemplatesLock)
					{
						DAL.Repository.Instance().RiskTemplateSaveList(retVal);
					}
				}

				log.Debug("Completed saving lightened risk template list to db.");
			}
			catch (DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException e)
			{
				throw e;
			}
			catch (Exception ex)
			{
				log.Debug(string.Format("Error downloading risk template list. Error: {0}.", ex.Message));
				Core.ArbProInsights.LogErrorError(ex);
			}
		}

        #endregion

        #region Upload Methods

        /// <summary>
        /// Mark an Appointment as downloaded.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        public static bool AppointmentMarkDownloaded(int appointmetId)
        {
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();

            Interfaces.IServiceConnection con;
            try
            {
                con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>().GetConnection();
                return con.SetAppoinmentDownloaded(appointmetId);
            } catch (Exception ex)
            {
				log.Debug(string.Format("Error in AppointmentMarkDownloaded: {0}.", ex.Message));
                Core.ArbProInsights.LogErrorError(ex);
                throw ex;
            }
        }

		public static bool AppointmentClientContactUploadNotPending()
		{
			bool retVal = true;

			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();

			if (ActivityProcessStageEvent != null)
				ActivityProcessStageEvent("Updating Contact Details");
			
			// Get a list of all not pending appointments and use it to update client contact details
			List<Appointment> apptForContactUpdate = DAL.Repository.Instance().AppointmentGetNotPending();

			foreach (Appointment a in apptForContactUpdate)
			{
				if (a.ClientContactId > 0 && !string.IsNullOrWhiteSpace(a.ClientAddress1))
				{
					try
					{
						ClientContactUpdate(
							a.ClientContactId,
							a.Title,
							a.FirstName,
							a.Surname,
							a.ClientStreetNo,
							a.ClientAddress1,
							a.ClientAddress2,
							a.ClientAddressTown,
							a.ClientAddressCounty,
							a.ClientAddressPostCode,
							a.ClientTelephone,
							a.ClientMobile,
							a.ClientEmail);
					}
					catch (DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException)
					{
						throw;
					}
					catch (Exception)
					{
						retVal = false;
						log.Debug("Error during client contact update from Upload Completed section.");

						if (ActivityProcessStageEvent != null)
						ActivityProcessStageEvent("Error Updating Client Contact Details");
						throw;
					}
				}
			}

			return retVal;
		}

        /// <summary>
        /// Upload a quote to the service.
        /// </summary>
        /// <param name="clientId">Client identifier.</param>
        /// <param name="quote">Quote.</param>
        public static bool QuoteItemsUpload()
        {
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger> ();
            //Set the activty loop and error control flags
            CancelActivity = false;
            bool uploadError = false;

            //Setup the insights timer to track the time taken to upload
            var insightsHandle = Core.ArbProInsights.GetActivityTimer("Quote Upload", LoggingLevel.Debug);
            /*
			if(Core.ArbProInsights.LevelOn(LoggingLevel.Debug))
			{
				insightsHandle = Xamarin.Insights.TrackTime ("Uploading Quotes");
				insightsHandle.Start;
			}
			*/
            //Raise the events for the title ot the Progress bar
            if (ActivityProcessStageEvent != null)
                ActivityProcessStageEvent("Uploading Quotes");

			log.Debug("Getting list of completed quotes.");
			   
            //Get a list of all completed quotes
            List<Quote> quotes = new List<Quote>();
            quotes = DAL.Repository.Instance().QuoteForAppointmentNotPendingList();

			log.Debug(string.Format("Total of {0} quotes to be uploaded.", quotes.Count));
			
            //Raise the events for the number to be uploaded
            if (ItemListCountActivityEvent != null)
                ItemListCountActivityEvent(quotes.Count);
				
            //Upload the quote object state
            Interfaces.IServiceConnection con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>();
            int failedUploads = 0;
            con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>().GetConnection();
            Core.ArbProInsights.LogActivity(string.Format("Uploading quotes: {0} awaiting upload.", quotes.Count), LoggingLevel.Info);
            foreach (Quote q in quotes)
            {
				log.Debug(string.Format("Beginning quote upload. Appointment id: {0}. Quote uploaded status {1}.", q.QuoteId, q.Uploaded));

                if (CancelActivity)
                    break;
					
                Appointment a = null;
                uploadError = false;

                try
                {
                    a = DAL.Repository.Instance().AppointmentGet(q.AppointmentRecId);
                } catch (Exception ex)
                {
					log.Debug(string.Format("Error while getting appointment for id {0}: {1}.", q.QuoteId, ex.Message));
                    Core.ArbProInsights.LogError(ex, Insights.Severity.Warning, LoggingLevel.Warn);
                }
                //Only attempt to upload completed appointments
                if (a != null && (a.Status == AppointmentStatus.Completed || a.Status == AppointmentStatus.PartiallyUploaded))
                {
					log.Debug(string.Format("Risk assessment upload status for appointment id {0}: {1}.", q.QuoteId, q.Assessment.HasUploaded));
                    //Check if the risk assessment has been uploaded
                    if (!q.Assessment.HasUploaded)
                    {
						try
						{
							//Atempt to upload the risk assessment
							q.Assessment.HasUploaded = con.SetRiskUpdatedForQuote(q.Assessment);

							Core.ArbProInsights.LogActivity(
								string.Format("RiskAssessment upload status: {0}. Appointment id: {1}.", q.Assessment.HasUploaded, q.QuoteId),
								LoggingLevel.Info);

							log.Debug(string.Format("RiskAssessment upload status: {0}. Appointment id: {1}.", q.Assessment.HasUploaded, q.QuoteId));

							//Save the uploaded status
							DAL.Repository.Instance().RiskAssessmentSave(q.Assessment);

						}
						catch (UnauthorisedException uex)
						{
							log.Debug(string.Format("RiskAssessment upload unauthorised exception: {0}.", uex.Message));
							Core.ArbProInsights.LogErrorError(uex);
							throw uex;
						}
						catch (Exception ex)
						{
							log.Debug(string.Format("Error in RiskAssessment upload. Appointment id: {0}. Message: {1}.", q.QuoteId, ex.Message));
							Core.ArbProInsights.LogErrorError(ex);
                        }
                    }

                    if (!q.Uploaded)
                    {
                        try
                        {
                            if (CancelActivity)
                                break;

							log.Debug(string.Format("Beginning quote upload. Appointment id: {0}.", q.QuoteId));

                            //Atempt to upload the main quote details
                            q.Uploaded = con.SetQuoteUpdatedForClient(q);	
                            //Save the upload status
                            DAL.Repository.Instance().QuoteSave(q);

							log.Debug(string.Format("Completed quote details upload. Appointment id: {0}. Quote upload status {1}.", q.QuoteId, q.Uploaded));

                            //Set the appointment status to partially uploaded
                            a.Status = AppointmentStatus.PartiallyUploaded;
                            DAL.Repository.Instance().AppointmentSave(a);

							log.Debug(string.Format("Updating appointment status to PartiallyUploaded. Appointment id: {0}.", q.QuoteId));
                        } catch (Exception ex)
                        {
							log.Debug(string.Format("Error in quote upload. Appointment id: {0}. Message: {1}.", q.QuoteId, ex.Message));
                            Core.ArbProInsights.LogErrorError(ex);
                            if (ex.GetType() == typeof(UnauthorisedException))
                                throw ex;
							else
								uploadError = true;
                        }
                    }

                    //Only upload the quote items if the upload completed and there is no error
                    if (q.Uploaded && !uploadError && !CancelActivity)
                    {
						log.Debug(string.Format("Beginning upload of {0} quote items. Appointment id: {1}.", q.Items.Count, q.QuoteId));

                        foreach (QuoteItem item in q.Items)
                        {
							if (!item.Uploaded)
							{
								try
								{
									//Atempt to upload an item
									item.Uploaded = con.SetQuoteItemUpdateForClient(q.QuoteId, item);
									//Save the upload status
									DAL.Repository.Instance().QuoteItemSave(item);
								}
								catch (Exception ex)
								{
									log.Debug(string.Format("Error uploading quote item. Appointment id: {0}. Message: {1}.", q.QuoteId, ex.Message));
									Core.ArbProInsights.LogErrorError(ex);
									if (ex.GetType() == typeof(UnauthorisedException))
										throw ex;
									else
										uploadError = true;
								}
							}
							else 
							{
								log.Debug(string.Format("Item already uploaded. Name: {0}, Appointment Id: {1}.", item.ItemType, q.QuoteId));
							}
                        }

						log.Debug("Completed upload of quote items.");// Add error flag status
                    }

                    // Set the appointment status to 'Uploaded'
					if (q.UploadCompleted && !uploadError)
                    {
                        try
                        {
							log.Debug(string.Format("Setting appointment status to uploaded. Appointment id: {0}.", q.QuoteId));
                            a.Status = AppointmentStatus.Uploaded;
                            DAL.Repository.Instance().AppointmentSave(a);
                        } catch (Exception ex)
                        {
							log.Debug(string.Format("Error updating appointment status. Appointment id: {0}, Error: {1}.", q.QuoteId, ex.Message));
                            Core.ArbProInsights.LogErrorError(ex);
                        }
                    }

                    if (ItemSavedEvent != null)
                        ItemSavedEvent(1);

                    if (uploadError)
                        failedUploads++;
                }

				log.Debug(string.Format("Completed quote upload. Appointment id: {0}.", q.QuoteId));
            }//Next Quote

            if (CancelActivity)
                return !uploadError;

			log.Debug("Beginning upload of all item pictures.");

            //Upload QuoteItem pictures
            if (ActivityProcessStageEvent != null)
                ActivityProcessStageEvent("Uploading Pictures");
				
            List<QuoteItem> items = new List<QuoteItem>();
            items = DAL.Repository.Instance().QuoteItemsUploadedGet();

            Byte[] imageArray = null;
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/QuoteItemImages/";
            string filename = "";
			
            //Raise the events for the number to be uploaded
            if (ItemListCountActivityEvent != null)
                ItemListCountActivityEvent(items.Count);

			int imageCount = 0;

			log.Debug(string.Format("Total of {0} uploaded items to process for images.", items.Count));

			foreach (QuoteItem itm in items)
            {
				if (CancelActivity)
					break;
				else if (itm == null)
					break;


                filename = string.Format("{0}{1}.png", filePath, itm.Guid.ToString());
                ImageEditing imgEditor = new ImageEditing();
                if (File.Exists(filename))
                {
					imageCount++;
                    try
                    {
                        if (!itm.HasEditedPicture)
                        {
                            imgEditor.ResizeImage(filename);
                        }
                        imageArray = File.ReadAllBytes(filename);
                        itm.PictureUploaded = con.SetQuoteItemPictureUpload(itm.Guid.ToString(), imageArray, itm.Guid.ToString() + ".png");
                        DAL.Repository.Instance().QuoteItemSave(itm);

                        if (itm.PictureUploaded)
                        {
                            File.Delete(filename);

                            if (ItemListCountActivityEvent != null)
                                ItemListCountActivityEvent(1);
                        }

                    } catch (Exception ex)
                    {
                        Core.ArbProInsights.LogErrorError(ex);
                        CancelActivity = true;
						log.Debug(string.Format("Image upload error: {0}", ex.Message));
                        Console.WriteLine("Image upload error: {0}", ex.Message);
                        uploadError = true;
                    }
                } else
                {
                    itm.PictureUploaded = true;
                    DAL.Repository.Instance().QuoteItemSave(itm);
                }
            }

			log.Debug(string.Format("Total of {0} images found.", imageCount));
			log.Debug("Completed upload of item pictures.");
			
            if (!UploadRiskAssessmentSignatures(con))
            {
				if (ActivityProcessStageEvent != null)
				{
					log.Debug("Error uploading risk assessment signatures.");
					ActivityProcessStageEvent("Error Uploading Signatures");
				}
            }

			log.Debug("Cleaning device data.");

            //Upload QuoteItem pictures
            if (ActivityProcessStageEvent != null)
                ActivityProcessStageEvent("Cleaning Device Data");
				
            if (ItemListCountActivityEvent != null)
                ItemListCountActivityEvent(1);

            if (ItemSavedEvent != null)
                ItemSavedEvent(1);

            if (ActivityProcessStageEvent != null)
                ActivityProcessStageEvent(string.Format("{0} Uploads failed", failedUploads));

			log.Debug(string.Format("Upload completed. Total of {0} uploads failed.", failedUploads));

            if (insightsHandle != null)
                insightsHandle.Stop();

            return !uploadError;
        }

        public static bool UploadRiskAssessmentSignatures(Interfaces.IServiceConnection connection)
        {
            bool retVal = true;
            List<RiskAssessment> UploadedAssessments = DAL.Repository.Instance().UploadedRiskAssessmentsGet();
			
            if (UploadedAssessments == null || UploadedAssessments.Count == 0)
                return true;
			
            if (ActivityProcessStageEvent != null)
                ActivityProcessStageEvent("Uploading Signatures");
				
            if (ItemListCountActivityEvent != null)
                ItemListCountActivityEvent(UploadedAssessments.Count);
				
            string signatureFilename = "";
            Byte[] imageSignature = null;
			
            foreach (RiskAssessment r in UploadedAssessments)
            {
                //Get the filename for the signature of the current risk assessment
                signatureFilename = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/SignatureImages/" + r.Guid.ToString() + ".png";
                if (File.Exists(signatureFilename))
                {
                    try
                    {
                        //If the file exists try to upload
                        imageSignature = File.ReadAllBytes(signatureFilename);
                        r.HasUploadedSignature = connection.SetRiskSignatureUpload(r.QuoteNo, imageSignature, r.Guid.ToString() + ".png");
                        DAL.Repository.Instance().RiskAssessmentSave(r);
                        if (r.HasUploadedSignature)
                        {
                            File.Delete(signatureFilename);
							
                            if (ItemListCountActivityEvent != null)
                                ItemListCountActivityEvent(1);
                        }
                    } catch (Exception ex)
                    {
                        Core.ArbProInsights.LogErrorError(ex);
                        retVal = false;
                        Console.WriteLine("Image upload error: {0}", ex.Message);
                    }
                }
            }
            return retVal;
        }

        public static bool DeleteUploadedAndCancelled()
        {
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();

            //Set the activty loop and error control flags
            CancelActivity = false;
            bool DeleteError = false;

			log.Debug("Getting quotes for deletion.");

            //Setup the insights timer to track the time taken to upload
            var insightsHandle = Core.ArbProInsights.GetActivityTimer("Uploaded data delete", LoggingLevel.Debug);

            //Raise the events for the title of the Progress bar
            if (ActivityProcessStageEvent != null)
                ActivityProcessStageEvent("Deleting uploaded data");
            //Get a list of all completed quotes
            List<Quote> quotes = new List<Quote>();
            quotes = DAL.Repository.Instance().QuotesForDeletion();

            //Raise the events for the number to be uploaded
            if (ItemListCountActivityEvent != null)
                ItemListCountActivityEvent(quotes.Count);

			log.Debug(string.Format("Amount of quotes to be deleted: {0}.", quotes.Count));

            Core.ArbProInsights.LogActivity(string.Format("Data cleanup: {0} quotes to be deleted.", quotes.Count), LoggingLevel.Info);

            foreach (Quote q in quotes)
            {
                if (CancelActivity)
                    break;

                Appointment a = null;

                try
                {
                    a = DAL.Repository.Instance().AppointmentGet(q.AppointmentRecId);
                }
				catch (Exception ex)
                {
					log.Debug(string.Format("Error while getting appointment for id {0}: {1}.", q.AppointmentRecId, ex.Message));
                    Core.ArbProInsights.LogError(ex, Insights.Severity.Warning, LoggingLevel.Warn);
                }

                // Delete the Appointment
                DAL.Repository.Instance().AppointmentDelete(q);

            }//Next Quote


			if (CancelActivity)
			{
				log.Debug("Deletion of quotes cancelled.");
				return !DeleteError;
			}

			log.Debug("Deletion of quotes completed.");

            return !DeleteError;        
        }

		public static bool ClientUpload(BL.Client client)
		{
			bool retVal = false;

			if (BL.Globals.DemoData)
			{
				client.Id = client.Id < 0 ? BL.BusinessLayer.ClientList().Count + 1 : client.Id;
				lock (saveClientLock)
				{
					retVal = DAL.Repository.Instance().SaveClient(client) > 0;
				}

				return retVal;
			}

			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();
			Interfaces.IServiceConnection con = TinyIoC.TinyIoCContainer.Current.Resolve<Interfaces.IServiceConnection>();

			try
			{
				retVal = con.SetClientUpdateOrInsert(client);
				if (retVal)
				{
					if (client.Id > 0)
					{
						lock (saveClientLock)
						{
							DAL.Repository.Instance().SaveClient(client);
						}
					}
					else
					{
						ClientListDownload();
					}
				}
			}
			catch (Exception ex)
			{
				log.Debug(string.Format("Error updating client. Client id: {0}, Error: {1}.", client.Id, ex.Message));
				Core.ArbProInsights.LogErrorError(ex);
			}

			return retVal;
		}

		// creates appointment - if successful re-downloads appointment list for user, else throws exception to be caught by appointment new view controller
		public static void AppointmentCreate(BL.Client client, string comments, DateTime apptStart, DateTime apptEnd, string houseNo, string address1, string address2, string town, string county, string postCode, int riskTemplateId)
		{
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();
			bool retVal = false;

			try
			{
				log.Debug(string.Format("Beginning appointment creation. Client Id: {0}, name: {1}", client.Id, client.Forename + " " + client.Surname));

				Service.WCFServiceClientWrapper Client = new Service.WCFServiceClientWrapper();

				// call to wcf service
				retVal = Client.CreateSalesEnquiry(ClientConvert(client), comments, apptStart, apptEnd, houseNo, address1, address2, town, county, postCode, riskTemplateId);

				log.Debug(string.Format("Completed appointment creation. Status: {0}, client name: {1}", retVal, client.Forename + " " + client.Surname));

				if (!retVal)
				{
					throw new Exception(string.Format("There was a problem creating the appointment. Creation status: {0}", retVal));
				}
				else
				{
					AppointmentListDownload(BL.Globals.CurrentUser.Id);
				}
			}
			catch (DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException)
			{
				throw;
			}
			catch (Exception ex)
			{
				log.Debug(string.Format("Error creating appointment. Client Id: {0}, name: {1}, Error: {2}.", client.Id, client.Forename + " " + client.Surname, ex.Message));
				Core.ArbProInsights.LogErrorError(ex);
				throw;
			}
		}

		public static void ClientContactUpdate(int contactId, string title, string firstName, string surname, string houseNo, string address1, string address2, string town, string county, string postCode, string tel, string mobile, string email)
		{
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();
			bool retVal = false;

			try
			{
				log.Debug(string.Format("Beginning client contact update. Contact Id: {0}", contactId));

				Service.WCFServiceClientWrapper Client = new Service.WCFServiceClientWrapper();

				// call to wcf service
				retVal = Client.UpdateClientContact(contactId, title, firstName, surname, houseNo, address1, address2, town, county, postCode, tel, mobile, email);

				log.Debug(string.Format("Completed update of client contact. Status: {0}, contact id: {1}", retVal, contactId));

				if (!retVal)
				{
					throw new Exception(string.Format("There was a problem updating the client contact. status: {0}", retVal));
				}
			}
			catch (DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException)
			{
				throw;
			}
			catch (Exception ex)
			{
				log.Debug(string.Format("Error updating contact. Contact Id: {0}, Error: {1}.", contactId, ex.Message));
				Core.ArbProInsights.LogErrorError(ex);
				throw;
			}
		}

		#endregion

		#endregion

		#region private

		static Service.Client ClientConvert(BL.Client client)
		{
			Service.Client c = new Service.Client();

			c.Id = client.Id;
			c.AltAccountNo = client.AltAccountNo;
			c.Title = client.Title;
			c.Forename = client.Forename;
			c.Surname = client.Surname;
			c.BusinessName = client.BusinessName;
			c.HouseNameNumber = client.HouseNameNumber;
			c.Address1 = client.Address1;
			c.Address2 = client.Address2;
			c.Town = client.Town;
			c.County = client.County;
			c.PostCode = client.PostCode;
			c.Sector = client.Sector;
			c.Telephone = client.Telephone;
			c.Mobile = client.Mobile;
			c.Email = client.Email;
			c.WebAddress = client.WebAddress;
			c.AccountType = client.AccountType;
			c.DateOpened = client.DateOpened;
			c.AddressLatitude = client.AddressLatitude;
			c.AddressLongitude = client.AddressLongitude;
			c.TermsAgreed = client.TermsAgreed;
			c.SendSmsMessage = client.SendSmsMessage;
			c.IncludeInMarketing = client.IncludeInMarketing;

			return c;
		}

		#endregion

    }
}
