using System;
using System.Collections.Generic;
using SQLite;
using System.Security.Cryptography;
using System.Text;

//using MonoTouch.Foundation;
using Interfaces = DGS.ArbPro.Mobile.Interfaces;
using Enums = DGS.ArbPro.Mobile.Core.Enums;
using BL = DGS.ArbPro.Mobile.BL;
using DGS.ArbPro.Mobile.Core.Enums;
using Xamarin;

namespace DGS.ArbPro.Mobile.BL
{
    public enum BuildTypes { Debug, Release }

    //[Preserve(AllMembers = true)]
    public class Settings : Contracts.BusinessEntityBase
    {

#if DEBUG
        static public BuildTypes BuildType = BuildTypes.Debug;
#else
		static public BuildTypes BuildType = BuildTypes.Release;
#endif

        #region Members

        static Settings _CachedSettings = null;

        #endregion

        #region Properties

        public string DbUserName { get; set; }

        public string DbPassword { get; set; }

        public decimal DbVat { get; set; }

        public bool DbReferenceDataDownloaded { get; set; }

        [SQLite.Ignore]
        public User DbCurrentUser { get; set; }

        [SQLite.Ignore]
        public bool OfflineMode { get; set; }

        public bool HasLoggedTooLarge { get; set; }

        #endregion

        #region Constructors

        public Settings ()
        {
            DbVat = 0M;
        }

        #endregion

        private static Settings CurrentSettingsGet ()
        {
            if (_CachedSettings == null) {
                //See if there are any settings in the Db
                _CachedSettings = DAL.SettingsRepository.Instance ().SettingsGet (new Settings ());
                _CachedSettings.DbCurrentUser = DAL.SettingsRepository.Instance ().CurrentUserGet ();
            }
            return _CachedSettings;
        }

        #region Methods

        public static void Logout ()
        {
            Settings s = CurrentSettingsGet ();
            s.DbUserName = "";
            s.DbPassword = "";
            SynchroniseSetting ();
        }

        public static void SavePassword (string password)
        {
            CurrentSettingsGet ().DbPassword = GetHashValue (password);
            SynchroniseSetting ();
        }

        public static bool ValidatePassword (string input)
        {
            try {
                string pwrd = CurrentSettingsGet ().DbPassword;
                if (pwrd == null || pwrd.Length == 0)
                    return false;
                else
                    return pwrd == GetHashValue (input);
            } catch (Exception ex) {
                if (Core.ArbProInsights.LevelOn (LoggingLevel.Error)) {
                    Xamarin.Insights.Report (ex, Insights.Severity.Error);
                }
                return false;
            }
        }

        private static string GetHashValue (string value)
        {
            MD5 hash = MD5.Create ();
            byte [] pwrdHash = hash.ComputeHash (Encoding.UTF8.GetBytes (value));

            StringBuilder sb = new StringBuilder ();

            foreach (byte b in pwrdHash) {
                sb.Append (b.ToString ("x2"));
            }

            return sb.ToString ();
        }

        public static void RefreshCachedSettings ()
        {
            _CachedSettings = DAL.SettingsRepository.Instance ().SettingsGet (_CachedSettings);
        }

        private static void SynchroniseSetting ()
        {
            if (_CachedSettings == null)
                return;

            try {
                DAL.SettingsRepository.Instance ().SettingsSet (_CachedSettings);
            } catch (Exception) {
                Console.WriteLine ("Settings update failure");
            }
        }

        private static void SynchroniseUser ()
        {
            if (_CachedSettings == null)
                return;

            try {
                DAL.SettingsRepository.Instance ().UserSet (_CachedSettings.DbCurrentUser);
            } catch (Exception) {
                Console.WriteLine ("Settings update user failure");
            }
        }

        #endregion

        #region User-editable settings

        /// <summary>
        /// The URI of the service
        /// </summary>
        private static string _ServiceURI;

        [SQLite.Ignore]
        public static string ServiceURI {
            get {
                return _ServiceURI;
            }
            set {
                if (value == null || value.Length == 0)
                    return;

                _ReachabilityURI = value.Replace ("http://", "").Replace ("https://", "");

                // search for a colon - this will be present if we've defined a port on the URI
                // We cannot have a port number on the reachability URI, as it fails.
                // Obviously, this will only work if we also have a service running on the same root URI without a port number
                // specified - i.e reachability relies on a service running on the standard ports at the same URI as the one with
                // port number.
                int index = _ReachabilityURI.IndexOf (":");
                if (index > 0) {
                    _ReachabilityURI = _ReachabilityURI.Substring (0, index);
                } else {
                    index = _ReachabilityURI.IndexOf ("/");
                    if (index > 0)
                        _ReachabilityURI = _ReachabilityURI.Substring (0, index);
                }

                string tmp = "";
                if (!value.EndsWith (@"/json/syncReply/")) {
                    tmp = value + @"/json/syncReply/";
                    tmp = tmp.Replace (@"//", @"/");
                    tmp = tmp.Insert (tmp.IndexOf ("/"), "/");
                } else {
                    tmp = value;
                }
                _ServiceURI = tmp;
                Core.ArbProInsights.LogActivity (string.Format ("Service URI changed to '{0}'", _ServiceURI), DGS.ArbPro.Mobile.Core.Enums.LoggingLevel.Info);
            }
        }


        private static int _DataRetainDays;

        [SQLite.Ignore]
        public static int DataRetainDays {
            get { return _DataRetainDays; }
            set { _DataRetainDays = value; }
        }

        private static string _ReachabilityURI;

        [SQLite.Ignore]
        public static string ReachabilityURI {
            get {
                return _ReachabilityURI;
            }
        }

        [SQLite.Ignore]
        public static decimal VatValue {
            get {
                return CurrentSettingsGet ().DbVat;
            }
            set {
                CurrentSettingsGet ().DbVat = value;
                SynchroniseSetting ();
            }
        }

        static bool _InsightsEnabled = true;

        [SQLite.Ignore]
        public static bool InsightsEnabled {
            get { return _InsightsEnabled; }
            //			 Force insights off for now
            //			get { return false; }
            set {
                if (value) {
                    if (!Xamarin.Insights.IsInitialized) {
                        Core.ArbProInsights.InitialiseInsights ("c782fc95ce323d0b3fafdbb0cbb6027a61386ec3");
                    }
                    Xamarin.Insights.DisableCollection = false;
                    Xamarin.Insights.DisableExceptionCatching = false;
                    Xamarin.Insights.DisableDataTransmission = false;
                } else {
                    if (Xamarin.Insights.IsInitialized) {
                        Xamarin.Insights.DisableCollection = true;
                        Xamarin.Insights.DisableExceptionCatching = true;
                        Xamarin.Insights.DisableDataTransmission = true;
                    }
                }
                _InsightsEnabled = value;
            }
        }

        #endregion

        #region Derived Settings
        public static string WCFServiceURI {
            get {
                try {
                    string ReturnValue;

                    // We base the WCF service URI on the main service URI.
                    // So with a main service URI of "https://cloud.arbprosoftware.com/mobileservice/json/syncReply/"
                    // we should be returning        "https://cloud.arbprosoftware.com/arbprowcf/ArbProService.svc"
                    // Hard coded for testing
                    //string URI = "http://10.211.55.4/arbprowcf/ArbProService.svc";

                    // Get the current main service URI and base the WCF address on this
                    string CurrentServiceURI = ServiceURI;
                    int JsonIndex = CurrentServiceURI.IndexOf (@"/json/syncReply/", StringComparison.CurrentCulture);
                    string RootURI = CurrentServiceURI.Substring (0, JsonIndex);

                    // We need to take the previous part of the address off - e.g. the 'mobile' part
                    ReturnValue = RootURI.Substring (0, RootURI.LastIndexOf ('/')) + "/arbprowcf/ArbProService.svc";
                    return ReturnValue;
                } catch (Exception ex) {
                    throw new Exception (string.Format ("Cannot construct WCFServiceURI. Exception '{0}'", ex.ToString ()));
                }
            }
        }
        #endregion Derived Settings
        #region Silently persisted settings


        /// <summary>
        /// The username used for the last login. This should be presented on the Login screen.
        /// </summary>
        [SQLite.Ignore]
        public static string Username {
            get {
                return CurrentSettingsGet ().DbUserName;
            }
            set {
                CurrentSettingsGet ().DbUserName = value;
                SynchroniseSetting ();
                //SynchroniseSetting(value, KEY_USERNAME);
            }
        }

        /// <summary>
        /// The hash value for the last logged in user. This should not be presented on any screen, but 
        /// should be persisted to enable offline user validation.
        /// </summary>
        [SQLite.Ignore]
        public static string PasswordHash {
            get {
                return CurrentSettingsGet ().DbPassword;
            }
            set {
                CurrentSettingsGet ().DbPassword = value;
                SynchroniseSetting ();
                //SynchroniseSetting(value, KEY_USERNAME);
            }
        }

        [SQLite.Ignore]
        public static bool HasDownloadedRefData {
            get {
                return CurrentSettingsGet ().DbReferenceDataDownloaded;
            }
            set {
                CurrentSettingsGet ().DbReferenceDataDownloaded = value;
                SynchroniseSetting ();
                //SynchroniseSetting(value, KEY_USERNAME);
            }
        }

        [SQLite.Ignore]
        public static bool LoggedTooLarge {
            get {
                return CurrentSettingsGet ().HasLoggedTooLarge;
            }
            set {
                CurrentSettingsGet ().HasLoggedTooLarge = value;
                SynchroniseSetting ();
            }
        }

        [SQLite.Ignore]
        public static User CurrentUser {
            get {
                return CurrentSettingsGet ().DbCurrentUser;
            }
            set {
                CurrentSettingsGet ().DbCurrentUser = value;
                SynchroniseUser ();
                //SynchroniseSetting(value, KEY_USERNAME);
            }
        }

        #endregion
    }
}

