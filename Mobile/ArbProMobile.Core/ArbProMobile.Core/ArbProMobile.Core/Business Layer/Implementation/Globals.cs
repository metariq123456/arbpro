// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Implementation/Globals.cs#12 $
// $Author: James $
// $Date: 2015/05/31 $
// $Change: 24700 $
// *********************************************************************
//
//
using System;
using System.Collections.Generic;

//using MonoTouch.Foundation;
using Interfaces = DGS.ArbPro.Mobile.Interfaces;
using BL = DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.BL
{
	//[Preserve(AllMembers = true)]
	public static class Globals //: Interfaces.IGlobals
	{
		public enum LoggedInStatus
		{
			NotLoggedIn,
			LoggedInOffline,
			LoggedInOnline
		}

		#region Properties

		/// <summary>
		/// The authentication keys for the last valid login
		/// </summary>
		/// <value>The authentication keys.</value>
		private static Dictionary<string, string> _AuthenticationKeys = null;

		public static Dictionary<string,string> AuthenticationKeys {
			get {
				if (_AuthenticationKeys == null)
					_AuthenticationKeys = new Dictionary<string, string> ();
                    
				return _AuthenticationKeys;
			}
			set {
				_AuthenticationKeys = value;
			}
		}

		/// <summary>
		/// Gets or sets the last application login date and time.
		/// </summary>
		/// <value>The last app login.</value>
		private static DateTime _LastAppLogin;

		public static DateTime LastAppLogin {
			get{ return _LastAppLogin; }
			set{ _LastAppLogin = value; }
		}

		private static LoggedInStatus _LoginStatus;

		public static LoggedInStatus LoginStatus {
			get{ return _LoginStatus; }
			set {
				_LoginStatus = value;
				_DemoData = _LoginStatus == LoggedInStatus.NotLoggedIn;
			} 
		}

		public static BL.User CurrentUser { get; set; }

		public static AppointmentListFilter ListFilter {
			get { return AppointmentListFilter.Instance (); }
		}

		public static void Logout ()
		{
			_LoginStatus = LoggedInStatus.NotLoggedIn;
//			CurrentUser = null;
			_DemoData = true;
		}

		private static bool _DemoData;

		public static bool DemoData {
			get{ return _DemoData; }
			set{ _DemoData = value; }
		}

		public static Core.Enums.LoggingLevel LoggingLevel {
			get;
			set;
		}

		#endregion
	}
}

