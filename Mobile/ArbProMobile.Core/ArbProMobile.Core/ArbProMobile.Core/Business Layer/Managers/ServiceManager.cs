﻿using System;
using System.Collections.Generic;
using System.Text;

using TinyIoC;
using TinyMessenger;

namespace RespondMobile.BL.Managers.Service
{
    /// <summary>
    /// The Service manager is a static class which just has an Initialise method. This should be called at application startup
    /// </summary>
    public static class Manager
    {
        private static ITinyMessengerHub Hub;
        private static ISettings Settings;
        private static IGlobals Globals;

        /// <summary>
        /// Sets up the subscriptions for the messages which this manager listens for.
        /// </summary>
        public static void Initialise()
        {
            Hub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();
            Settings = TinyIoCContainer.Current.Resolve<ISettings>();
            Globals = TinyIoCContainer.Current.Resolve<IGlobals>();

            Hub.Subscribe<msgServiceLogin>(ServiceLogin);
            Hub.Subscribe<msgDownloadWorkitems>(DownloadWorkitems);
            Hub.Subscribe<msgDownloadWorkitems_Cancel>(DownloadWorkitems_Cancel);

        }

        #region Message Handlers
        private static void ServiceLogin(msgServiceLogin m)
        {

            // return with an empty error string to indicate success
            Hub.Publish(new msgServiceLogin_Response(Hub, ""));

            // TODO add proper service calls

            //            BL.ServiceLayer.Connection c;
//
//            c = BL.ServiceLayer.Connection.GetConnection(m.Content.Account, m.Content.Username, m.Content.Password);
//            if (c.Connected)
//            {
//                Globals.CurrentUser = c.UserGet();
//            }
//            else
//            {
//                Globals.CurrentUser = null;
//            }


            // Send the data out in a msgServicesGet_Response message
//            Hub.Publish(new msgServiceLogin_Response(Hub, c.LastError));

        }

        static bool CancelDownload;
        static int NumDownloaded;
        private static void DownloadWorkitems(msgDownloadWorkitems m)
        {
            
            Dictionary<int,DateTime> ReturnedItems;
            BL.ServiceLayer.Connection c;

            CancelDownload = false;
            try
            {
				ISettings settings = TinyIoCContainer.Current.Resolve<ISettings>();
				IGlobals globals = TinyIoCContainer.Current.Resolve<IGlobals>();
				try
				{
					globals.LastDateDownloaded = RespondMobile.DAL.WorkItemRepository.GetDateForDownload();
				}
				catch(Exception ex)
				{
					Console.WriteLine (@"Max date error: ",ex.Message);
				}
                NumDownloaded = 0;
                c = BL.ServiceLayer.Connection.GetConnection();
                
                if (c != null && c.Connected)
                {
					//Get the list of ids to download
                    ReturnedItems = c.WorkItemsToDownloadListGet();
                    Hub.Publish(new msgDownloadWorkitems_Max(Hub, ReturnedItems.Count));
                    
                    bool should_download;
                    foreach (KeyValuePair<int,DateTime> pair in ReturnedItems)
                    {
						should_download = true;
						//Check if the item is already on the database
						BL.WorkItem db_item = RespondMobile.DAL.WorkItemRepository.GetWorkItemForActionId (pair.Key);
						if(db_item != null)
						{
							//The item is on the device
							if(db_item.DownLoadedDate >= pair.Value && db_item.LastUpdatedDate <= db_item.DownLoadedDate)
							{
								should_download = false;
								db_item.HasUpdated = false;
							}
							else if(db_item.LastUpdatedDate > db_item.DownLoadedDate && pair.Value > db_item.DownLoadedDate)
							{
								should_download = false;
								db_item.HasSyncIssue = true;
								
							}
							RespondMobile.DAL.WorkItemRepository.SaveWorkItem(db_item);
						}
						
						BL.WorkItem item = null;
						List<BL.Appointment>appointments = new List<Appointment>();
						List<BL.Note> notes = new List<BL.Note>();
						
						if(should_download)
						{
							//Retreave the current item
							item = c.WorkItemGetForActionID(pair.Key);
							//Retreave all appointments for the action
							appointments = c.DiaryGetForActionId(pair.Key);
							//Retreave all notes for the action
							notes = c.NoteGetForActionList(pair.Key);
							
							if (item.CurrentStatus != "Cancelled")
							{
								if(db_item != null)
								{
									//Delete the current item off the local database
									RespondMobile.DAL.WorkItemRepository.DeleteWorkItem(db_item.ID);
								}
								RespondMobile.DAL.WorkItemRepository.SaveWorkItem(item);
								RespondMobile.DAL.WorkItemRepository.SaveAppoinmentList(appointments);
								RespondMobile.DAL.WorkItemRepository.SaveNoteList(notes);
								
								
								c = BL.ServiceLayer.Connection.GetConnection();
								c.WorkItemMarkDownloaded(item.ID);
								settings.LastDownload = item.LastUpdatedDate;
								//c.
							}
							
							NumDownloaded++;
						}
						Hub.Publish(new msgDownloadWorkitems_Progress(Hub, 1));
                        if (CancelDownload)
                            break;
                        
                        
                    }
                }
                else
                {
                    ReturnedItems = new Dictionary<int,DateTime>();
                }
            }
            catch (Exception)
            {
                ReturnedItems = new Dictionary<int,DateTime>();
            }

            // Send the data out in a msgWorkItemsGet_Response message
            Hub.Publish(new msgDownloadWorkitems_Finished(Hub, NumDownloaded));

        }
        /*
        private static void DownloadWorkitems(msgDownloadWorkitems m)
        {
            
            List<RespondMobile.BL.WorkItem> ReturnedItems;
            BL.ServiceLayer.Connection c;

            CancelDownload = false;
            try
            {
                NumDownloaded = 0;
                c = BL.ServiceLayer.Connection.GetConnection();
                if (c != null && c.Connected)
                {
                    ReturnedItems = c.WorkItemListGet();
                    Hub.Publish(new msgDownloadWorkitems_Max(Hub, ReturnedItems.Count));
                    foreach (RespondMobile.BL.WorkItem it in ReturnedItems)
                    {
                        Hub.Publish(new msgDownloadWorkitems_Progress(Hub, 1));

                        if (it.CurrentStatusId != 2)
                        {
                            RespondMobile.DAL.WorkItemRepository.SaveWorkItem(it);
                            c = BL.ServiceLayer.Connection.GetConnection();
                            c.WorkItemMarkDownloaded(it.ActionId);
                        }

                        NumDownloaded++;
                        if (CancelDownload)
                            break;
                    }
                }
                else
                {
                    ReturnedItems = new List<BL.WorkItem>();
                }
            }
            catch (Exception)
            {
                ReturnedItems = new List<BL.WorkItem>();
            }

            // Send the data out in a msgWorkItemsGet_Response message
            Hub.Publish(new msgDownloadWorkitems_Finished(Hub, NumDownloaded));

        }
        */

        private static void DownloadWorkitems_Cancel(msgDownloadWorkitems_Cancel m)
        {
            CancelDownload = true;
        }


        #endregion

    }

    #region Message Classes


    #region msgServiceLogin
    public class msgServiceLogin : GenericTinyMessage<msgServiceLoginArgs>
    {
        public msgServiceLogin(object Sender, msgServiceLoginArgs Content)
            : base(Sender, Content)
        {
        }
    }

    public class msgServiceLogin_Response : GenericTinyMessage<String>
    {
        public msgServiceLogin_Response(object Sender, String Content)
            : base(Sender, Content)
        {

        }
    }

    public class msgServiceLoginArgs
    {
        public msgServiceLoginArgs(string nAccount, string nUsername, string nPassword)
        {
            Account = nAccount;
            Username = nUsername;
            Password = nPassword;
        }

        public string Account
        {
            get;
            set;
        }

        public string Username
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }
    }


    #endregion

    #region msgDownloadWorkitems
    public class msgDownloadWorkitems : ITinyMessage
    {
        /// <summary>
        /// The sender of the message, or null if not supported by the message implementation.
        /// </summary>
        public object Sender { get; private set; }
    }

    public class msgDownloadWorkitems_Max : GenericTinyMessage<int>
    {
        public msgDownloadWorkitems_Max(object Sender, int Content)
            : base(Sender, Content)
        {

        }

    }

    public class msgDownloadWorkitems_Progress : GenericTinyMessage<int>
    {
        public msgDownloadWorkitems_Progress(object Sender, int Content)
            : base(Sender, Content)
        {

        }
    }
    public class msgDownloadWorkitems_Finished : GenericTinyMessage<int>
    {
        public msgDownloadWorkitems_Finished(object Sender, int Content)
            : base(Sender, Content)
        {

        }
    }

    public class msgDownloadWorkitems_Cancel : ITinyMessage
    {
        public object Sender { get; private set; }
    }        

    #endregion
    #endregion


}
