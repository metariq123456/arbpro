// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/PartialClasses/ImageEditing.cs#6 $
// // $Author: Jon $
// // $Date: 2015/05/20 $
// // $Change: 24664 $
// //*********************************************************************
//
//
using System;

namespace DGS.ArbPro.Mobile.BL
{
	public partial class ImageEditing
  	{
		partial void ResizeAndSaveImage(string fileName);

		public void ResizeImage(string filename)
		{
			ResizeAndSaveImage (filename);
	  		//ImageEditing editor = new ImageEditing ();
			//editor.ResizeAndSaveImage (filename);
		}
  }
}

