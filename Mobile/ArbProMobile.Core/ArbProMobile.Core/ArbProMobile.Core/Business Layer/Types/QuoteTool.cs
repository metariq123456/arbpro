//*********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Types/QuoteTool.cs#9 $
// $Author: ivan $
// $Date: 2016/06/16 $
// $Change: 26516 $
//*********************************************************************
using System;
using SQLite;
using Model = DGS.ArbPro.Service.Model;
using Contracts = DGS.ArbPro.Mobile.BL.Contracts;
using Tools = Microsec.Tools;

namespace   DGS.ArbPro.Mobile.BL
{
	/// <summary>
	/// Details for an individual tool on a quote.
	[Serializable]
	public class QuoteTool :BL.Contracts.BusinessEntityBase
	{
		public int Id { get; set; }

		public int QuoteRecId { get; set; }

		public static int Name_MaxLength = 100;

		public string Name { get; set; }

		public int Number { get; set; }

		public override string ToString ()
		{
			return string.Format ("{0}:{1}", Number, Number == 1 ? Name : Name + "s");
		}
	}
}
