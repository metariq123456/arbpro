//*********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Types/Tool.cs#10 $
// $Author: ivan $
// $Date: 2016/06/16 $
// $Change: 26516 $
//*********************************************************************
using System;
using SQLite;
using Model = DGS.ArbPro.Service.Model;
using Contracts = DGS.ArbPro.Mobile.BL.Contracts;
using Tools = Microsec.Tools;

namespace   DGS.ArbPro.Mobile.BL
{
    public class Tool :BL.Contracts.BusinessEntityBase
    {
       
        [Indexed]
        public int QuoteRecId { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

		public int Stock { get; set; }

		public int NumberRequired { get; set; }

    }
}
