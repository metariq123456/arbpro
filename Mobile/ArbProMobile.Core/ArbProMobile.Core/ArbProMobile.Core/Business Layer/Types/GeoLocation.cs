//*********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Types/GeoLocation.cs#8 $
// $Author: Jon $
// $Date: 2015/05/20 $
// $Change: 24664 $
//*********************************************************************
using System.Xml.Serialization;
using System.IO;
using SQLite;
using Model = DGS.ArbPro.Service.Model;
using Contracts = DGS.ArbPro.Mobile.BL.Contracts;
using Tools = Microsec.Tools;

namespace   DGS.ArbPro.Mobile.BL
{
    /// <summary>
    /// Holde geographic location information in the form of latitude and longitude.
    /// </summary>
    /// <remarks>Note that the Geolocation is not stored in the database in its
    /// own table, therefore it does not inherit from BusinessEntityBase
    /// </remarks>
    public class GeoLocation : Contracts.BusinessEntityBase
    {

        #region Constructors

        /// <summary>
        /// No-parameter constructor.
        /// </summary>
        public GeoLocation()
        {
            Latitude = 0.0D;
            Longitude = 0.0D;
            Title = "";
        }

        public GeoLocation(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
            Title = "";
        }

        public GeoLocation(double latitude, double longitude,string title)
        {
            Latitude = latitude;
            Longitude = longitude;
            Title = title;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbProMobile.Core.BL.Appointment"/> class.
        /// </summary>
        /// <param name="a">The DTO Appointment object from which to create it.</param>
        public GeoLocation(Model.Types.GeoLocation modelGeolocation)
        {
            Tools.ObjectTools.MigrateProperties(modelGeolocation, this, new string[] { });
        }

        #endregion

		#region Properties

        [Indexed]
        public string Owner { get; set;}

        public double Latitude
        {
            get;
            set;
        }

        public double Longitude
        {
            get;
            set;
        }

        public string Title { get; set;}
		#endregion

    }
}

