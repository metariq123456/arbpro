// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Types/AppointmentListFilter.cs#10 $
// // $Author: James $
// // $Date: 2016/03/27 $
// // $Change: 26036 $
// //*********************************************************************
//
//
using System;

namespace DGS.ArbPro.Mobile.BL
{
	public class AppointmentListFilter
	{
		#region Fields
		static AppointmentListFilter _instance;
		#endregion
		#region Properties
		/// <summary>
		/// If true, appointment list will be only for today's appointments
		/// </summary>
		public bool TodaysAppointments {
			get;
			set;
		}

		/// <summary>
		/// The date from which to display appointments.
		/// </summary>
		/// <value>From date.</value>
		public DateTime FromDate {
			set;
			get;
		}

		/// <summary>
		/// The date to which to display appointments.
		/// </summary>
		/// <value>To date.</value>
		public DateTime ToDate {
			get;
			set;
		}

		/// <summary>
		/// If set, open appointments will be displayed
		/// </summary>
		/// <value><c>true</c> if appointment status open; otherwise, <c>false</c>.</value>
		public bool AppointmentStatusPending {
			get;
			set;
		}

		/// <summary>
		/// If set, completed appointments will be displayed
		/// </summary>
		/// <value><c>true</c> if appointment status completed; otherwise, <c>false</c>.</value>
		public bool AppointmentStatusCompleted {
			get;
			set;
		}

		/// <summary>
		/// If set, cancelled appointments will be displayed
		/// </summary>
		/// <value><c>true</c> if appointment status cancelled; otherwise, <c>false</c>.</value>
		public bool AppointmentStatusCancelled {
			get;
			set;
		}

		/// <summary>
		/// If set, partially uploaded appointments will be displayed
		/// </summary>
		/// <value><c>true</c> if appointment status is PartiallyUploaded; otherwise, <c>false</c>.</value>
		public bool AppointmentStatusPartiallyUploaded {
			get;
			set;
		}

        /// <summary>
        /// If set, uploaded appointments will be displayed
        /// </summary>
        /// <value><c>true</c> if appointment status is Uploaded; otherwise, <c>false</c>.</value>
        public bool AppointmentStatusUploaded {
            get;
            set;
        }

		#endregion

		#region Constructors
		private AppointmentListFilter ()
		{
			// default status filter to include all statuses
			AppointmentStatusPending = true;
			AppointmentStatusCompleted = true;
			AppointmentStatusCancelled = true;
			AppointmentStatusPartiallyUploaded = true;
            AppointmentStatusUploaded = false;
		}

		public static AppointmentListFilter Instance ()
		{
			if (_instance == null) {
				_instance = new AppointmentListFilter ();
				_instance.Load ();

			}

			return _instance;
		}
		#endregion
		#region Private Methods
		/// <summary>
		/// Loads filter settings from the settings store.
		/// </summary>
		private void Load ()
		{
			//TODO - implement this!
		}

		/// <summary>
		/// Saves filter settings to the settings store.
		/// </summary>
		private void Save ()
		{
			//TODO - implement this!
		}
		#endregion
	}
}

