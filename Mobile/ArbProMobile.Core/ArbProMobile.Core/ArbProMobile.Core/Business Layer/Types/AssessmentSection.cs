// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Types/AssessmentSection.cs#11 $
// // $Author: ivan $
// // $Date: 2016/06/21 $
// // $Change: 26579 $
// //*********************************************************************
//
//
using System;
using System.Collections.Generic;

namespace DGS.ArbPro.Mobile.BL
{
	public class AssessmentSection:BL.Contracts.BusinessEntityBase
	{
		public AssessmentSection ()
		{
			AssessmentItems = new List<AssessmentItem>();
		}
		
		public AssessmentSection (int section)
		{
			AssessmentItems = new List<AssessmentItem>();
			switch (section) {
			case 1:
				CreateSiteSection ();
				break;
			case 2:
				CreateEnviromentSection ();
				break;
			case 3:
				CreateOperationsSection ();
				break;
			case 4:
				CreateMachinerySection ();
				break;
			case 5:
				CreateHeightSection ();
				break;
			}
		}

		public int RiskId { get; set; }
		public int SectionId { get; set; }
		public int AssessmentRecId {get;set;}
		public string Title { get; set; }
		public string AdditionalDetail {get;set;}
		public string Description { get; set; }
		public string ReferenceDoc { get; set; }
		public string Chapter { get; set; }
		
		[SQLite.Ignore]
		public string Info {
			get {
				return "KEY:\n?- Needs to be evaluated.\nN- No risk.\nY- Risk identified.";
			}
		}
		
		[SQLite.Ignore]
		public List<AssessmentItem> AssessmentItems { get; set; }
		
		
		
		private void CreateSiteSection()
		{
			this.Title = "Site Considerations";
			AddDefaultItem("Work site common factors");
			AddDefaultItem("Roads footpaths public areas");
			AddDefaultItem("Other way leaves");
			AddDefaultItem("Overhead lines underground services");
			AddDefaultItem("Building obstructions");
			AddDefaultItem("Sensitive emotionally charged");
			AddDefaultItem("Other");
			
		}
		
		private void CreateEnviromentSection()
		{
			this.Title = "Environmental";
			AddDefaultItem("Noise and dust");
			AddDefaultItem("Water course");
			AddDefaultItem("Protected species");
		}
		
		private void CreateOperationsSection()
		{
			this.Title = "Operations";
			AddDefaultItem("Chainsaws");
			AddDefaultItem("Tree climbing");
			AddDefaultItem("Chainsaw in tree");
			AddDefaultItem("Lowering techniques necessary");
			AddDefaultItem("Tree felling");
			AddDefaultItem("De-limbing and cross cutting");
			AddDefaultItem("Storm damage");
			AddDefaultItem("Chemical application");
			AddDefaultItem("Other");
		}
		
		private void CreateMachinerySection()
		{
			this.Title = "Machinery";
			AddDefaultItem("Brushwood chipper");
			AddDefaultItem("Stump grinder");
			AddDefaultItem("Winch");
			AddDefaultItem("Brush cutters / clearing saws");
			AddDefaultItem("Hedge trimmers");
			AddDefaultItem("Mechanised pole pruner");
			AddDefaultItem("Mobile elevated work platform");
			AddDefaultItem("Crane");
			AddDefaultItem("Lorry - mounted hydraulic loading arm");
			AddDefaultItem("Other");
		}
		
		private void CreateHeightSection()
		{
			this.Title = "Working At Height";
			AddDefaultItem("Straight fell");
			AddDefaultItem("Can we work from a ladder");
			AddDefaultItem("Use a pole saw");
			AddDefaultItem("Use a MEWP(Mobile Elevated Work Platform)");
			AddDefaultItem("MEWP hire cost effective");
			AddDefaultItem("Is the tree safe to climb");
			AddDefaultItem("Rope and harness only option");
		}
		
		private void AddDefaultItem(string title)
		{
			AssessmentItem itm = new AssessmentItem();
			itm.Title = title;
			itm.RefCode = "";
			AssessmentItems.Add (itm);
		}
	}
}

