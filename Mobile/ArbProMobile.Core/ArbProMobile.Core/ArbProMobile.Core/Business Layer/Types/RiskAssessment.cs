// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Types/RiskAssessment.cs#9 $
// // $Author: ivan $
// // $Date: 2016/06/16 $
// // $Change: 26516 $
// //*********************************************************************
//
//
using System;
using System.Collections.Generic;

namespace DGS.ArbPro.Mobile.BL
{
	public class RiskAssessment:BL.Contracts.BusinessEntityBase
	{
		//Const to represent standard sections.
		//This will be modified at a later date to be dynamic
		public const string SECTION_TITLE_SITE = "Site Considerations";
		public const string SECTION_TITLE_ENVIROMENTAL = "Environmental";
		public const string SECTION_TITLE_OPERATIONS = "Operations";
		public const string SECTION_TITLE_MACHINARY = "Machinery";
		public const string SECTION_TITLE_HEIGHT = "Working at Height";
		
		public RiskAssessment ():base()
		{
			Criterion = new List<AssessmentSection>();
			FurtherRisks = new List<AssessmentAdditionalItem>();
		}
	
		public RiskAssessment (bool demo = false):base()
		{
			Criterion = new List<AssessmentSection>();
			FurtherRisks = new List<AssessmentAdditionalItem>();
			
			if(demo)
				InitialiseDemo();
		}

		public int NearestAnE {get;set;}
		public string MobileSignal {get;set;}
		public string RescueAccess {get;set;}
		public int AerialRescuePlan {get;set;}
		public string FirstAider {get;set;}
		public string FirstAidEquipment {get;set;}
		public string AerialRescuePerson {get;set;}
		
		public bool C538Competent {get;set;}
		
		
		
		public DateTime CompletionDate { get; set; }
		
		public int QuoteNo { get; set; }
		
		public double LocationLatitude { get; set; }
        public double LocationLongitude { get; set; }
        public double LocationHeight { get; set; }
        
        //Flage to determin state of signature upload
        public bool HasUploaded {get;set;}
        public bool HasUploadedSignature { get; set; }
		
		[SQLite.Ignore]
		public List<AssessmentSection> Criterion {get;set;}
		[SQLite.Ignore]
		public List<AssessmentAdditionalItem> FurtherRisks {get;set;}


		
		
		public string HospitalName {get;set;}
		public string AerialRescueDetails { get; set; }
		
		private string _GridRef = "";
		[SQLite.Ignore]		
		public string GridReference 
		{
			get
			{
				if(LocationLatitude == 0 && LocationLongitude == 0)
				{
					return "Not Mapped";
				}
				else
				{
					_GridRef = Microsec.Tools.GeoLocationTools.GeoLocationLongLatToOSGridUK (LocationLongitude,LocationLatitude,LocationHeight);
				}
				return _GridRef;
			}
		}
		
		[SQLite.Ignore]
		public bool IsDirty {get;set;}
		
		public GeoLocation GetLocation()
		{
			
			if(LocationLatitude == 0 && LocationLongitude == 0)
			{
				return null;
			}
			
			return new GeoLocation(LocationLatitude,LocationLongitude,string.Format ("Grid Reference: {0}",this.GridReference));
		}
		
		public void SetAssessmentLocation(GeoLocation location)
		{
			LocationLatitude = location.Latitude;
			LocationLongitude = location.Longitude;
		}
		
		private void InitialiseDemo()
		{
			for(int index = 1; index <= 5; index ++)
			{
				Criterion.Add(new AssessmentSection(index));
			}
		}
	}
}

