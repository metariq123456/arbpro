//*********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
//*********************************************************************using System;
using System;
using System.Collections;
using Model = DGS.ArbPro.Service.Model;
using Contracts = DGS.ArbPro.Mobile.BL.Contracts;
using Tools = Microsec.Tools;

namespace   DGS.ArbPro.Mobile.BL
{
    public enum AppointmentStatus
    {
        Pending,
        Completed,
        Cancelled,
        PartiallyUploaded,
        Uploaded,
        RepeatUploadRequested
    }

    public class Appointment :BL.Contracts.BusinessEntityBase
    {
        public static string OwnerName = "Appointment";

        #region Constructors

        public Appointment()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ArbProMobile.Core.BL.Appointment"/> class.
        /// </summary>
        /// <param name="a">The DTO Appointment object from which to create it.</param>
        public Appointment(Model.Types.Appointment modelAppointment)
        {
            Tools.ObjectTools.MigrateProperties(modelAppointment, this, new string[] { "ClientAddressLocation" });
            ClientAddressLocation = new GeoLocation(modelAppointment.ClientAddressLocation);
        }

        #endregion

        #region Properties

        [SQLite.Ignore]
        public bool Editable
        {
            get
            {
                if (this.Status != AppointmentStatus.Uploaded &&
                    this.Status != AppointmentStatus.PartiallyUploaded)
                {
                    return true;
                } else
                {
                    return false;
                }
            }

        }

        /// <summary>
        /// Indicates whether the Quote has been amended since being loaded or the last time it was saved.
        /// </summary>
        /// <value><c>true</c> if dirty; otherwise, <c>false</c>.</value>
        [SQLite.Ignore]
        public bool Dirty { get; set; }

        /// <summary>
        /// The Id of the Appointment in the server database
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }

        public DateTime UploadTime { get; private set; }

        public DateTime CancelTime { get; private set; }

        public int MasterCalendarId { get; set; }

        public int ClientIdNo { get; set; }

        public int JobIdNo { get; set; }

        public DateTime StartDateTime { get; set; }

        public int QuoteNo { get; set; }

        public float Duration { get; set; }

        public String Subject { get; set; }

        public String UserCR { get; set; }

        public int SurveyorId { get; set; }

        public string Comments { get; set; }

        public String ClientName { get; set; }

		public String Title { get; set; }

		public String FirstName { get; set; }

		public String Surname { get; set; }

		public String ClientStreetNo { get; set; }

        public String ClientAddress1 { get; set; }

        public String ClientAddress2 { get; set; }

        public String ClientAddressTown { get; set; }

        public String ClientAddressCounty { get; set; }

        public String ClientAddressPostCode { get; set; }

        [SQLite.Ignore]
        public GeoLocation ClientAddressLocation { get; set; }

        public double LocationLatitude { get; set; }

		public double LocationLongitude { get; set; }

		public int RiskTemplateId { get; set; }

		public int ClientContactId { get; set; }

        public String ClientEmail { get; set; }

        private AppointmentStatus _Status;

        public AppointmentStatus Status
        { 
            get { return _Status; } 
            set
            {
                if (value == AppointmentStatus.Uploaded)
                {
                    this.UploadTime = DateTime.UtcNow;
                }

                if (value == AppointmentStatus.Cancelled)
                {
                    this.CancelTime = DateTime.UtcNow;
                }
                _Status = value;
            }
        }

        public String ClientTelephone { get; set; }

        public String ClientMobile { get; set; }

        [SQLite.Ignore]
        public Quote AppointmentQuote { get; set; }

        /// <summary>
        /// Returns the client's name and address in a formatted string.
        /// </summary>
        /// <value>The client name and address.</value>
        [SQLite.Ignore]
        public String ClientNameAndAddress
        {
            get
            {
                ArrayList Parts = new ArrayList();
                if (ClientName != null && ClientName != "")
                {
                    Parts.Add(ClientName.Trim());
                }

				if (!string.IsNullOrWhiteSpace(ClientStreetNo))
				{
					Parts.Add(ClientStreetNo.Trim());
				}

                if (ClientAddress1 != null && ClientAddress1 != "")
                {
                    Parts.Add(ClientAddress1.Trim());
                }

                if (ClientAddress2 != null && ClientAddress2 != "")
                {
                    Parts.Add(ClientAddress2.Trim());
                }

                if (ClientAddressTown != null && ClientAddressTown != "")
                {
                    Parts.Add(ClientAddressTown.Trim());
                }

                if (ClientAddressCounty != null && ClientAddressCounty != "")
                {
                    Parts.Add(ClientAddressCounty.Trim());
                }

                if (ClientAddressPostCode != null && ClientAddressPostCode != "")
                {
                    Parts.Add(ClientAddressPostCode.Trim());
                }

                return string.Join(", ", Parts.ToArray());
            }
        }
        //        public enumResidentialCommercial ResidentialCommercial { get; set; }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return string.Format("{0:dd/MM/yy HH:mm} {1} {2}", this.StartDateTime, ClientName, ClientAddress1);
        }

        public string IdDisplayString()
        {
            // Note that we need to use an intermediate variable here - using an iif clause in String.Format doesn't work.
            string JobIdNoAsString;
            if (JobIdNo == 0)
            {
                JobIdNoAsString = "(n/a)";
            } else
            {
                JobIdNoAsString = JobIdNo.ToString();
            }
            return string.Format("Client:{0} Job No:{1}", ClientIdNo, JobIdNoAsString);
        }

        #endregion

    }
}

