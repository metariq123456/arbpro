//*********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Types/QuoteItem.cs#10 $
// $Author: ivan $
// $Date: 2016/10/13 $
// $Change: 27237 $
//*********************************************************************
using System;
using SQLite;
using Model = DGS.ArbPro.Service.Model;
using Contracts = DGS.ArbPro.Mobile.BL.Contracts;
using Tools = Microsec.Tools;

namespace   DGS.ArbPro.Mobile.BL
{
    public enum EnumPreservationOrderStatus
    {
        Unknown
        , 
        No
        ,
        Yes
    }
    /// <summary>
    /// Details for an individual item on a quote.
    /// </summary>
    /// <remarks>
    /// QuoteItems are not stored separately in the database, but in a serialized List
    /// as part of the Quote record.
    /// </remarks>
    [Serializable]
    public class QuoteItem :BL.Contracts.BusinessEntityBase
    {
    
        public int QuoteRecId { get; set; }

        public string ItemNumber { get; set; }

        public string ItemType { get; set; }

        public string WorkDescription { get; set; }

        public decimal ItemValue { get; set; }

        public byte[] Picture { get; set; }

		public EnumPreservationOrderStatus PreservationOrderStatus { get; set; }

		public bool HasEditedPicture { get; set; }

        [Ignore]
        public GeoLocation Location { get; set; }

        [Ignore]
        public string PreservationOrderShortStatus
        {
            get{
                string ReturnValue;
                switch (PreservationOrderStatus)
                {
                    case EnumPreservationOrderStatus.Yes:
                        ReturnValue =  "Y";
                        break;
                    case EnumPreservationOrderStatus.No:
                        ReturnValue =  "N";
                        break;
                    case EnumPreservationOrderStatus.Unknown:
                        ReturnValue =  "?";
                        break;
                    default:
                        ReturnValue =  "";
                        break;

                }
                return ReturnValue;
            }
        }

        public double LocationLatitude { get; set; }

        public double LocationLongitude { get; set; }

        public String LocationString { get; set; }

        public override string ToString()
        {
            return string.Format("{0}:{1}:{2}", ItemNumber, ItemType, WorkDescription);
        }
        
        public bool Uploaded {get;set;}
        public bool PictureUploaded {get;set;}
    }
}
