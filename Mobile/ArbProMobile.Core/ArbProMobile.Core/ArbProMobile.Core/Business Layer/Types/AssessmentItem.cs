// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Types/AssessmentItem.cs#9 $
// // $Author: ivan $
// // $Date: 2016/06/16 $
// // $Change: 26516 $
// //*********************************************************************
//
//
using System;

namespace DGS.ArbPro.Mobile.BL
{
	public class AssessmentItem:BL.Contracts.BusinessEntityBase
	{
		public enum ControlRequiredType
		{
			Unknown = 0,
			Yes = 2,
			No = 1
		}

		public AssessmentItem ()
		{
			ControlRequired = ControlRequiredType.Unknown;
		}

		public int CriterionId { get; set; }
		public int SectionRecId {get;set;}
		public string Title {get;set;}
		public string RefCode {get;set;}
		
		public ControlRequiredType ControlRequired {get;set;}
		public int ItemReferenceId { get; set;}
	
		public void toggleControlRequired(){
			if (ControlRequired == ControlRequiredType.Unknown) {
				ControlRequired = ControlRequiredType.No;
			}else if (ControlRequired == ControlRequiredType.No) {
				ControlRequired = ControlRequiredType.Yes;
			}else {
				ControlRequired = ControlRequiredType.Unknown;
			}


		}
	}
}

