//*********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Types/User.cs#13 $
// $Author: ivan $
// $Date: 2016/09/07 $
// $Change: 26900 $
//*********************************************************************
using System;
using Model = DGS.ArbPro.Service.Model;
using Contracts = DGS.ArbPro.Mobile.BL.Contracts;
using Tools = Microsec.Tools;
using DGS.ArbPro.Mobile.Core.Enums;

namespace   DGS.ArbPro.Mobile.BL
{
    public class User :BL.Contracts.BusinessEntityBase
    {
       
        public int Id { get; set; }

        public String UserName { get; set; }

        public String DisplayName { get; set; }
		[SQLite.Ignore]
        public String Password { get; set; }

        public int ClientId { get; set; }

		public Guid UserGuid { get; set; }

		// Default the logging level to info
		private LoggingLevel _LoggingLevel = LoggingLevel.Debug;

		[SQLite.Ignore]
		public Core.Enums.LoggingLevel LoggingLevel {
			get {
				return _LoggingLevel;
			}
			set{ _LoggingLevel = value; }
		}
	}
}
