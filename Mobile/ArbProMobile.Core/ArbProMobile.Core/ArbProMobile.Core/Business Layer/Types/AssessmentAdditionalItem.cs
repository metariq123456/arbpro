// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Types/AssessmentAdditionalItem.cs#9 $
// // $Author: Jon $
// // $Date: 2015/05/20 $
// // $Change: 24664 $
// //*********************************************************************
//
//
using System;

namespace DGS.ArbPro.Mobile.BL
{
	public class AssessmentAdditionalItem :BL.Contracts.BusinessEntityBase 
	{
		public AssessmentAdditionalItem ()
		{
		}
		
		public int AssessmentRecId {get;set;}
		public string Detail { get; set; }
		public string Control {get;set;}
	}
}

