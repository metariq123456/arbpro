//*********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
//*********************************************************************
using System.Collections.Generic;
using SQLite;
using System.Xml.Serialization;
using System.IO;
using Model = DGS.ArbPro.Service.Model;
using Contracts = DGS.ArbPro.Mobile.BL.Contracts;
using Tools = Microsec.Tools;
using DGS.ArbPro.Service.Model.Types;
using System.Linq;

namespace   DGS.ArbPro.Mobile.BL
{
    public class Quote :BL.Contracts.BusinessEntityBase
    {
        #region Properties

        [SQLite.Indexed]
        public int AppointmentRecId { get; set; }

        /// <summary>
        /// Indicates whether the Quote has been amended since being loaded or the last time it was saved.
        /// </summary>
        /// <value><c>true</c> if dirty; otherwise, <c>false</c>.</value>
        [Ignore]
        public bool Dirty { get; set;}

        [Indexed]
        public int QuoteId { get; set; }

        public float TimeHrs { get; set; }

        public int StaffNumber { get; set; }

        public string Comments { get; set; }

	public bool Uploaded { get; set; }

        [SQLite.Ignore]
        public List<QuoteItem> Items { get; set; }

        /// <summary>
        /// List of items which need to be deleted from the database.
        /// </summary>
        /// <value>The deleted items.</value>
        /// <remarks>
        /// This is kept as a separate list, so that UI methods can just deal with the Items list, and not worry about any which are
        /// marked to be deleted.
        /// </remarks>
        [SQLite.Ignore]
        public List<QuoteItem> DeletedItems { get; set; }

        [SQLite.Ignore]
        public List<QuoteTool> Tools { get; set; }

        [SQLite.Ignore]
        public List<QuoteTool> DeletedTools { get; set; }
        
        [SQLite.Ignore]
        public RiskAssessment Assessment {
			get {
				if (_DebugTest == null) {
					_DebugTest = new RiskAssessment (true);
				}
				return _DebugTest;
			}
			set { _DebugTest = value;}
		}

		[SQLite.Ignore]
		public Risk RiskAssessment
		{
			get
			{
				if (debugTest == null)
				{
					debugTest = new Risk();
				}
				return debugTest;
			}
			set { debugTest = value; }
		}

		[SQLite.Ignore]
		public bool UploadCompleted
		{
			get
			{
				return Uploaded && !(Items.Any (itm => itm.Uploaded == false));
			}
		}
        #endregion

		private RiskAssessment _DebugTest = null;

		private Risk debugTest = null;

        #region Constructors
        /// <summary>
        /// No-argument constructor
        /// </summary>
        public Quote():base()
        {
            Items = new List<QuoteItem>();
            DeletedItems = new List<QuoteItem>();

            Tools = new List<QuoteTool>();
            DeletedTools = new List<QuoteTool>();
            
            Assessment = new RiskAssessment();
        }
        
        public Quote(bool demo = false):base()
        {
            Items = new List<QuoteItem>();
            DeletedItems = new List<QuoteItem>();

            Tools = new List<QuoteTool>();
            DeletedTools = new List<QuoteTool>();
            
            Assessment = new RiskAssessment(demo);
        }
        
       

        /// <summary>
        /// Creates a new Quote object from a model object.
        /// </summary>
        /// <param name="modelQuote">Model quote.</param>
        public Quote(Model.Types.Quote modelQuote):base()
        {
            Microsec.Tools.ObjectTools.MigrateProperties(modelQuote, this, new string[] { "Items", "Tools" });
        }
        #endregion

        #region Public Methods
        public void DeleteItem(QuoteItem item)
        {
            // If the item was previously stored in the database, we 
            // need to move it to the list of items to be deleted
            if (item.RecId != 0 )
                DeletedItems.Add(item);
          
            Items.Remove(item);

        }

        public void DeleteTool(QuoteTool item)
        {
            // If the item was previously stored in the database, we 
            // need to move it to the list of items to be deleted
            if (item.RecId != 0 )
                DeletedTools.Add(item);

            Tools.Remove(item);

        }
        
        public decimal GetQuoteValue()
        {
			decimal net = 0;
					
			foreach(QuoteItem itm in this.Items)
			{
				net += itm.ItemValue;
			}
			return net;
        }
        #endregion

    }
}
