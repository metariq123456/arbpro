//*********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Business Layer/Types/ItemDescription.cs#9 $
// $Author: Jon $
// $Date: 2015/05/27 $
// $Change: 24685 $
//*********************************************************************using System;
using Model = DGS.ArbPro.Service.Model;
using Contracts = DGS.ArbPro.Mobile.BL.Contracts;
using Tools = Microsec.Tools;

namespace   DGS.ArbPro.Mobile.BL
{
    public class ItemDescription :BL.Contracts.BusinessEntityBase
    {
       
        public int Id { get; set; }

        public string Description { get; set; }
    }
}
