﻿// //*********************************************************************
// // $Id$
// // $Author$
// // $Date$
// // $Change$
// //*********************************************************************
//
//
using System;
using SQLite;

namespace DGS.ArbPro.Mobile.BL
{
	public class Client : BL.Contracts.BusinessEntityBase
	{
		#region Properties

		[Indexed]
		public int Id { get; set; }

		public string AltAccountNo { get; set; }

		public string Forename { get; set; }

		public string Surname { get; set; }

		public string Title { get; set; }

		public string HouseNameNumber { get; set; }

		public string Address1 { get; set; }

		public string Address2 { get; set; }

		public string Town { get; set; }

		public string County { get; set; }

		public string PostCode { get; set; }

		public string Telephone { get; set; }

		public string Mobile { get; set; }

		public string Email { get; set; }

		public string BusinessName { get; set; }

		public int AccountType { get; set; }

		public DateTime DateOpened { get; set; }

		public double AddressLatitude { get; set; }

		public double AddressLongitude { get; set; }

		public bool SendSmsMessage { get; set; }

		public bool IncludeInMarketing { get; set; }

		public string Sector { get; set; }

		public string WebAddress { get; set; }

		public bool TermsAgreed { get; set; }

		#endregion
	}
}

