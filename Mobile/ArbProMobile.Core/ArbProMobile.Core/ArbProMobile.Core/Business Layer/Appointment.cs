using System;
using System.Reflection;
using Model = DGS.ArbPro.Service.Model;
using Contracts = DGS.ArbPro.Mobile.BL.Contracts;
using Tools = Microsec.Tools;

namespace   DGS.ArbPro.Mobile.BL
{
    public class Appointment :BL.Contracts.IBusinessEntity
    {

        public Appointment() 
        {

        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ArbProMobile.Core.BL.Appointment"/> class.
        /// </summary>
        /// <param name="a">The DTO Appointment object from which to create it.</param>
        public Appointment(Model.Types.Appointment modelAppointment)
        {
            Tools.ObjectTools.MigrateProperties(modelAppointment,this,new string[]{"Geolocation"});
                    }
		#region Properties

        /// <summary>
        /// The unique ID of the Appointment in the mobile database
        /// </summary>
        /// <value>The I.</value>
        public int ID { get; set; }

        /// <summary>
        /// The Id of the Appointment in the server database
        /// </summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }

        public int MasterCalendarId { get; set; }

        public int ClientIdNo { get; set; }

        public DateTime StartDateTime { get; set; }

        public int QuoteNo { get; set; }

        public float Duration { get; set; }

        public String Subject { get; set; }

        public String UserCR { get; set; }

        public int SurveyorId { get; set; }

        public String ClientName { get; set; }

        public String ClientAddress1 { get; set; }

        public String ClientAddress2 { get; set; }

        public String ClientAddressTown { get; set; }

        public String ClientAddressCounty { get; set; }

        public String ClientAddressPostCode { get; set; }
//        public GeoLocation ClientAddressLocation { get; set; }

        public String ClientEmail { get; set; }

        public String ClientTelephone { get; set; }
//        public enumResidentialCommercial ResidentialCommercial { get; set; }


		#endregion

    }
}

