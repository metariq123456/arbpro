using System;

namespace DGS.ArbPro.Mobile.BL.Contracts
{
    public interface IBusinessEntity
    {
        /// <summary>
        /// Unique Id of the object in its table in the database
        /// </summary>
        int RecId { get; set; }

        /// <summary>
        /// Unique GUID for the object -this can be used to reference photographs and other
        /// entities which are not in the database but stored - for example in the file system.
        /// </summary>
        /// <value>The GUID.</value>
        Guid Guid { get; set; }
    }
}

