using System;
using SQLite;

namespace DGS.ArbPro.Mobile.BL.Contracts
{
	/// <summary>
	/// Business entity base class. Provides the ID property.
	/// </summary>
    public abstract class BusinessEntityBase : IBusinessEntity
    {

        public BusinessEntityBase()
        {
            Guid = Guid.NewGuid();

        }
        /// <summary>
		/// Gets or sets the Database ID.
		/// </summary>
        [PrimaryKey, AutoIncrement]
        public int RecId { get; set; }

        /// <summary>
        /// The OwnerString is the string to use when a class is the owner of a general class
        /// - for example a GeoLocation or Photo.
        /// </summary>
        /// <value>The class name without any preceding namespace parts.</value>
        public string OwnerString
        {
            get
            {
                var Parts = this.GetType().ToString().Split('.');

                return String.Format("{0}:{1}", Parts[Parts.Length-1], RecId);
            }
        }

        public Guid Guid
        { get; set; }
    }
}

