using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.Mobile.Core.Enums
{
    public enum NotificationTypeEnum
    {
        Error,
        Warning,
        Information,
        Confirm,
        Login
    }
    
    public enum NotificationResultEnum
    {
		Ok,
		Cancel
    }
}
