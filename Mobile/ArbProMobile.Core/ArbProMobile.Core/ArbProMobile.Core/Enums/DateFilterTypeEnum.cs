// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Enums/DateFilterTypeEnum.cs#8 $
// // $Author: Jon $
// // $Date: 2015/05/20 $
// // $Change: 24664 $
// //*********************************************************************
//
//
using System;

namespace DGS.ArbPro.Mobile.Core.Enums
{
	/// <summary>
	/// Enum to represent how the date field is to be applied as a filter.
	/// On = Appointments on the specified date.
	/// OnOrBefore = Appoinments on or before the specified date.
	/// OnOrAfter = Appoinments on or after the specified date.
	/// Before = Appoinments before the specified date.
	/// After = Appoinments after the specified date.
	/// </summary>
	public enum DateFilterTypeEnum
	{
		OnOnly = 0,
		OnOrBefore = 1,
		OnOrAfter = 2,
		Before = 3,
		After = 4
	}
}

