// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Enums/LoggingLevel.cs#3 $
// // $Author: Jon $
// // $Date: 2015/05/27 $
// // $Change: 24685 $
// //*********************************************************************
//
//
using System;

namespace  DGS.ArbPro.Mobile.Core.Enums
{
	public enum LoggingLevel
	{
		Off = 0,
		Fatal = 1,
		Error = 2,
		Warn = 3,
		Info = 4,
		Debug = 5,
		All = 6
	}
}

