// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Enums/NetworkStatus.cs#9 $
// // $Author: James $
// // $Date: 2015/05/31 $
// // $Change: 24700 $
// //*********************************************************************
//
//
using System;

namespace  DGS.ArbPro.Mobile.Core.Enums
{
  public enum NetworkStatus {
	Unknown,
	NotReachable,
	ReachableViaCarrierDataNetwork,
	ReachableViaWiFiNetwork,
	OfflineOnly
	}
}

