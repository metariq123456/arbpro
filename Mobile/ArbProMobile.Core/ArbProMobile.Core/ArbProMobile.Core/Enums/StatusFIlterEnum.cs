// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/Enums/StatusFIlterEnum.cs#8 $
// // $Author: Jon $
// // $Date: 2015/05/20 $
// // $Change: 24664 $
// //*********************************************************************
//
//
using System;

namespace DGS.ArbPro.Mobile.Core.Enums
{
	/// <summary>
	/// Appointment status filter enum.
	/// None = All Appointments.
	/// Incomplete = Oustanding Appointments.
	/// Carded = Appoinments where no one was available and a card was left.
	/// Completed = Appoinments that have been attended.
	/// </summary>
	public enum StatusFilterEnum
	{
		None = 0,
		Incomplete = 1,
		Carded = 2,
		Completed = 3
	}
}

