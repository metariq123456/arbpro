//*********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/DataLayer/Database.cs#11 $
// $Author: ivan $
// $Date: 2016/09/21 $
// $Change: 27019 $
//*********************************************************************using System.Linq;
using System.Collections.Generic;
using System.Linq;
using System;
using SQLite;
using DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.DL
{
	/// <summary>
	/// RespondDatabase builds on SQLite.Net and represents a specific database, in our case, the Task DB.
	/// It contains methods for retrieval and persistance as well as db creation, all based on the 
	/// underlying ORM.
	/// </summary>
    public class Database : SQLiteConnection
    {
        static object locker = new object();
		/// <summary>
		/// Initializes a new instance of the <see cref="RespondMobile.DL.RespondDatabase"/> RespondDatabase. 
		/// if the database doesn't exist, it will create the database and all the tables.
		/// </summary>
		/// <param name='path'>
		/// Path.
		/// </param>
		/// <remarks>
		/// IMPORTANT - the second parameter to the base constructor is the storeDateTimeAsTicks option. This must be set to false, so that
		/// legacy databases (i.e. databases created prior to the upgrade to SQLite-NET 3) have their datetime columns handled correctly.
		/// </remarks>
		public Database(string path) : base (path,false)
        {
            // create the tables
            CreateTable<Appointment>();
            CreateTable<CalendarItem> ();
            CreateTable<ItemDescription>();
            CreateTable<ItemType>();
            CreateTable<Quote>();
            CreateTable<QuoteItem>();
            CreateTable<QuoteTool>();
            CreateTable<Tool>();
			CreateTable<Client>();
            CreateTable<User>();
            //Create the tables for the risk assessments
            CreateTable<RiskAssessment>();
            CreateTable<AssessmentSection>();
            CreateTable<AssessmentItem>();
            CreateTable<AssessmentAdditionalItem>();
            CreateTable<RiskAerialRescuePlan>();
            CreateTable<RiskHospital>();   
            CreateTable<RiskTemplate>();         
            
			//This table should only ever have a single entry
			//CreateSettingsTable();
			
        }
        #region Generic Methods

        public IEnumerable<T> GetItems<T>() where T : BL.Contracts.IBusinessEntity, new ()
        {
            lock (locker)
            {
                return (from i in Table<T>() select i).ToList();
            }
        }


        public T GetItem<T>(int id) where T : BL.Contracts.IBusinessEntity, new ()
        {
            lock (locker)
            {
                return Table<T>().FirstOrDefault(x => x.RecId == id);
                // Following throws NotSupportedException - thanks aliegeni
                //return (from i in Table<T> ()
                //        where i.RecId == id
                //        select i).FirstOrDefault ();
            }
        }

        public int SaveItem<T>(T item) where T : BL.Contracts.IBusinessEntity
        {
            lock (locker)
            {
                if (item.RecId != 0)
                {
                    Update(item);
                    return item.RecId;
                }
                else
                {
                    return Insert(item);
                }
            }
        }

        public int DeleteItem<T>(int id) where T : BL.Contracts.IBusinessEntity, new ()
        {
            lock (locker)
            {
#if NETFX_CORE
                return Delete(new T() { RecId = id });
#else
                return Delete<T>(new T () { RecId = id });
#endif
            }
        }
        #endregion

        #region Appointment Methods

        public T GetAppointment<T>(int recId) where T : BL.Appointment, new ()
        {
            lock (locker)
            {
                return Table<T>().FirstOrDefault(x => x.RecId == recId);

            }
        }
        
        public T GetAppointmentForServiceId<T>(int id) where T : BL.Appointment, new ()
        {
            lock (locker)
            {
                return Table<T>().FirstOrDefault(x => x.Id == id);

            }
        }
        #endregion
        
//      public int RecId {get;set;}
//		public Guid Guid { get; set; }
//		public string DbUrl {get;set;}
//		public string DbUserName {get;set;}
//		public string DbPassword {get;set;}
//		public DateTime DbDateFileter {get;set;}
//		public Enums.StatusFilterEnum DbStatusFilter {get;set;}
//		public Enums.DateFilterTypeEnum DbDateFilter {get;set;}
        
		
		


    }
}