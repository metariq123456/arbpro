// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.Core/ArbProMobile.Core/ArbProMobile.Core/DataLayer/SettingsDataBase.cs#8 $
// // $Author: Jon $
// // $Date: 2015/05/20 $
// // $Change: 24664 $
// //*********************************************************************
//
//
using System.Collections.Generic;
using System.Linq;
using System;
using SQLite;
using DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.DL
{
	public class SettingsDataBase: SQLiteConnection
    {
        static object locker = new object();
		/// <summary>
		/// Initializes a new instance of the <see cref="RespondMobile.DL.RespondDatabase"/> RespondDatabase. 
		/// if the database doesn't exist, it will create the database and all the tables.
		/// </summary>
		/// <param name='path'>
		/// Path.
		/// </param>
        public SettingsDataBase(string path) : base (path)
        {
            // create the tables
            CreateTable<Settings>();
            CreateTable<User>();
        }
        
        #region Generic Methods

        public Settings GetSettings()
        {
            lock (locker)
            {
                return Table<Settings>().FirstOrDefault();
            }
        }
        
        public User GetUser()
        {
            lock (locker)
            {
                return Table<User>().FirstOrDefault();
            }
        }

		public IEnumerable<T> GetItems<T>() where T : BL.Contracts.IBusinessEntity, new ()
        {
            lock (locker)
            {
                return (from i in Table<T>() select i).ToList();
            }
        }


        public int SaveItem<T>(T item) where T : BL.Contracts.IBusinessEntity
        {
            lock (locker)
            {
                if (item.RecId != 0)
                {
                    Update(item);
                    return item.RecId;
                }
                else
                {
                    return Insert(item);
                }
            }
        }

        public int DeleteItem<T>(int id) where T : BL.Contracts.IBusinessEntity, new ()
        {
            lock (locker)
            {
#if NETFX_CORE
                return Delete(new T() { RecId = id });
#else
                return Delete<T>(new T () { RecId = id });
#endif
            }
        }
        #endregion 
	}
}

