﻿using System;
using System.Collections.Generic;
using System.Text;

using TinyIoC;
using TinyMessenger;

namespace RespondMobile.BL.Managers.WorkItem
{
    /// <summary>
    /// The WorkItem manager is a static class which just has an Initialise method. This should be called at application startup
    /// </summary>
    public static class Manager
    {
        private static ITinyMessengerHub Hub;

        /// <summary>
        /// Sets up the subscriptions for the messages which this manager listens for.
        /// </summary>
        public static void Initialise()
        {
            Hub = TinyIoCContainer.Current.Resolve<ITinyMessengerHub>();

            Hub.Subscribe<msgDatabaseClear>(DatabaseClear);
            Hub.Subscribe<msgWorkItemsGet>(WorkItemsGet);
            Hub.Subscribe<msgWorkItemSave>(WorkItemSave);
            Hub.Subscribe<msgWorkItemGetWithActionId>(WorkItemGetWithActionId);
			Hub.Subscribe<msgAppointmentsGetForAction>(AppointmentsGetForAction);
			Hub.Subscribe<msgAppointmentsGetDiary>(AppointmentsGetDiary);
			Hub.Subscribe<msgAppointmentGetForId>(AppointmentGetForId);
        }

        #region Message Handlers

        private static void DatabaseClear(msgDatabaseClear m)
        {
            RespondMobile.DAL.WorkItemRepository.DeleteAll();
            Hub.Publish(new msgDatabaseClear_Response());
        }

        private static void WorkItemsGet(msgWorkItemsGet m)
        {
            List<RespondMobile.BL.WorkItem> ReturnedItems;

            try
            {
                ReturnedItems = (List<RespondMobile.BL.WorkItem>)RespondMobile.DAL.WorkItemRepository.GetWorkItems();
            }
            catch (Exception e)
            {
                ReturnedItems = new List<BL.WorkItem>();
            }

            // Send the data out in a msgWorkItemsGet_Response message
            Hub.Publish(new msgWorkItemsGet_Response(Hub, ReturnedItems));

        }

        /// <summary>
        /// Handles the msgWorkItemSave message, with the WorkItem as the Content property of the message.
        /// </summary>
        /// <param name="m">The message received</param>
        private static void WorkItemSave(msgWorkItemSave m)
        {
            RespondMobile.DAL.WorkItemRepository.SaveWorkItem(m.Content);

            Hub.Publish(new msgWorkItemSave_Response(Hub, m.Content.ID));

        }

        /// <summary>
        /// Retrieves the WorkItem with the ActionId passed in
        /// </summary>
        /// <param name="m"></param>
        private static void WorkItemGetWithActionId(msgWorkItemGetWithActionId m)
        {
            RespondMobile.BL.WorkItem w;

            w = RespondMobile.DAL.WorkItemRepository.GetWorkItemForActionId(m.Content);

            Hub.Publish(new msgWorkItemGetWithActionId_Response(Hub, w));
        }

		private static void AppointmentsGetForAction(msgAppointmentsGetForAction m)
		{
			List<RespondMobile.BL.Appointment> apps;

			apps = RespondMobile.DAL.WorkItemRepository.GetAppointmentsForAction(m.Content);

			Hub.Publish(new msgAppointmentsGetForAction_Response(Hub, apps));
		}

		private static void AppointmentGetNextForAction(msgAppointmentGetNextForAction m)
		{
			RespondMobile.BL.Appointment a;

			a = RespondMobile.DAL.WorkItemRepository.GetNextAppointmentForAction(m.Content);

			Hub.Publish (new msgAppointmentGetNextForAction_Response(Hub, a));
		}

		private static void AppointmentsGetDiary(msgAppointmentsGetDiary m)
		{
			List<RespondMobile.BL.Appointment> apps;

			apps = RespondMobile.DAL.WorkItemRepository.GetAppointmentsDiary();

			Hub.Publish (new msgAppointmentsGetDiary_Response(Hub, apps));
		}

		private static void AppointmentGetForId(msgAppointmentGetForId m)
		{
			BL.Appointment App;

			App = RespondMobile.DAL.WorkItemRepository.GetAppointmentForId(m.Content);

			Hub.Publish (new msgAppointmentGetForId_Response(Hub, App));
		}
        #endregion

    }

    #region Message Classes

    #region msgDatabaseClear
    /// <summary>
    /// Clears down the database
    /// </summary>
    public class msgDatabaseClear : ITinyMessage
    {
        public msgDatabaseClear()
        {
        }

        public object Sender { get; private set; }
    }

    /// <summary>
    /// The response to msgDatabaseClear
    /// </summary>

    public class msgDatabaseClear_Response : ITinyMessage
    {
        public msgDatabaseClear_Response()
        {
        }

        public object Sender { get; private set; }
    }
    #endregion

    #region msgWorkItemsGet
    /// <summary>
    /// A request for work items.
    /// </summary>
    public class msgWorkItemsGet : ITinyMessage
    {
        public msgWorkItemsGet()
        {
        }

        public object Sender { get; private set; }
    }

    /// <summary>
    /// The response to msgWorkItems
    /// </summary>
    /// <remarks>
    /// The items returned are sent back as a List of WorkItem objects.
    /// </remarks>
    public class msgWorkItemsGet_Response : GenericTinyMessage<List<RespondMobile.BL.WorkItem>>
    {
        private ITinyMessengerHub Hub;
        private bool ReturnValue;

        public msgWorkItemsGet_Response(object Sender, List<RespondMobile.BL.WorkItem> Content)
            : base(Sender, Content)
        {

        }
    }
    #endregion

        #region msgWorkItemGet
    /// <summary>
    /// A request for a single work Item.
    /// </summary>
    public class msgWorkItemGetWithActionId : GenericTinyMessage<int>
    {
        public msgWorkItemGetWithActionId(object Sender, int Content)
            : base(Sender, Content)
        {
        }

    }

    /// <summary>
    /// The response to msgWorkItemGetRequest
    /// </summary>
    /// <remarks>
    /// The Item returned is sent back as the message content
    /// </remarks>
    public class msgWorkItemGetWithActionId_Response : GenericTinyMessage<RespondMobile.BL.WorkItem>
    {
        public msgWorkItemGetWithActionId_Response(object Sender, RespondMobile.BL.WorkItem Content)
            : base(Sender, Content)
        {

        }
    }

	public class msgAppointmentGetForId : GenericTinyMessage<int>
	{
		public msgAppointmentGetForId(object Sender, int Content)
			: base(Sender, Content)
		{ }
	}

	public class msgAppointmentGetForId_Response : GenericTinyMessage<BL.Appointment>
	{
		public msgAppointmentGetForId_Response(object Sender, BL.Appointment Content)
			: base (Sender, Content)
		{ }
	}

	public class msgAppointmentsGetForAction : GenericTinyMessage<int>
	{
		public msgAppointmentsGetForAction(object Sender, int Content)
			: base (Sender, Content)
		{

		}
	}

	public class msgAppointmentsGetForAction_Response : GenericTinyMessage<List<RespondMobile.BL.Appointment>>
	{
		public msgAppointmentsGetForAction_Response(object Sender, List<RespondMobile.BL.Appointment> Content)
			: base (Sender, Content)
		{

		}
	}

	public class msgAppointmentGetNextForAction : GenericTinyMessage<int>
	{
		public msgAppointmentGetNextForAction(object Sender, int Content)
			: base (Sender, Content)
		{

		}
	} 

	public class msgAppointmentGetNextForAction_Response : GenericTinyMessage<RespondMobile.BL.Appointment>
	{
		public msgAppointmentGetNextForAction_Response(Object Sender, RespondMobile.BL.Appointment Content)
			: base (Sender, Content)
		{

		}
	}

	public class msgAppointmentsGetDiary : ITinyMessage
	{
		public msgAppointmentsGetDiary()
		{
		}

		public object Sender { get; private set; }
	}

	public class msgAppointmentsGetDiary_Response : GenericTinyMessage<List<RespondMobile.BL.Appointment>>
	{
		public msgAppointmentsGetDiary_Response(Object Sender, List<RespondMobile.BL.Appointment> Content)
			: base (Sender, Content)
		{

		}
	}

    #endregion

    #region msgWorkItemSave
    public class msgWorkItemSave : GenericTinyMessage<RespondMobile.BL.WorkItem>
    {
        public msgWorkItemSave(object Sender, RespondMobile.BL.WorkItem Content)
            : base(Sender, Content)
        {
        }
    }

    public class msgWorkItemSave_Response : GenericTinyMessage<int>
    {
        public msgWorkItemSave_Response(object Sender, int Content)
            : base(Sender, Content)
        {

        }
    }
    #endregion
    #endregion


}
