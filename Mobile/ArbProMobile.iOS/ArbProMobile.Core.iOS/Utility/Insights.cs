﻿// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.Core.iOS/Utility/Insights.cs#3 $
// // $Author: Jon $
// // $Date: 2015/05/20 $
// // $Change: 24664 $
// //*********************************************************************
//
//
using Xamarin;
using DGS.ArbPro.Mobile.Core.Enums;
using DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.Core
{
	public static class Insights
	{
		public static bool LevelOn (LoggingLevel level)
		{
			return (Settings.InsightsEnabled && Xamarin.Insights.IsInitialized && Globals.LoggingLevel >= level);
		}

		public static void InitialiseInsights ()
		{
			//Initialise Insights with Arb Pro's API key
			Xamarin.Insights.Initialize ("c782fc95ce323d0b3fafdbb0cbb6027a61386ec3");

			// Create an anonymous identity for Insights to handle any reports from before login
			Xamarin.Insights.Identify ("Anonymous", "Username", "Unknown");
		}
	}
}

