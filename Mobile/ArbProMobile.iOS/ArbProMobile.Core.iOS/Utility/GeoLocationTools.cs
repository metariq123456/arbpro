// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.Core.iOS/Utility/GeoLocationTools.cs#8 $
// // $Author: Jon $
// // $Date: 2015/05/20 $
// // $Change: 24664 $
// //*********************************************************************
//
//
using System;
using TDPG.GeoCoordConversion;

namespace  Microsec.Tools
{
	public static class GeoLocationTools
	{
		private static double ConvertToRadians(double angle)
		{
			return (Math.PI / 180D) * angle;
		}
		
		private static string PadNumber(double value, int length)
		{
			string retVal = value.ToString ();
			while(retVal.Length < length)
			{
				retVal = retVal.Insert (0,"0");
			}
			return retVal;
		}
		
		//Test data 51.61430, -3.96382 == SN26401924
		//          51.614301 -3.963824 (OSGB36)
		//          51.614745 -3.964993 (WGS84) 
		
		/// <summary>
		/// Converts  WGS84/GRS80 GPS Longitude and Latitude degrees into an OSGB36 string
		/// </summary>
		/// <returns>OSGB36 string in the format [LL] [Eastings] [Northings]</returns>
		/// <param name="Longitude">Longitude.</param>
		/// <param name="Latitude">Latitude.</param>
		public static string GeoLocationLongLatToOSGridUK(double WGS84Longitude, double WGS84Latitude,double WGS84Height)
		{
			try
			{
			PolarGeoCoordinate tdgpCoOrds = new PolarGeoCoordinate(WGS84Latitude,WGS84Longitude,WGS84Height,AngleUnit.Degrees,CoordinateSystems.WGS84); 
			GridReference grdRef = PolarGeoCoordinate.ChangeToGridReference(tdgpCoOrds);
		
//			//WGS84 to OSGB36 Long and Lat needs a transformation
//			//Helmert transformation constants - This can be out by as much as 5m and anything requiring less than 10m acurasy should use a different transformation
//			//The transformation is given in seconds and not degrees
//			double helemetXTransform = -0.1502/ 3600; 
//			double helemetYTransform = -0.2470/ 3600;
//		
//			double OSLongitude = ConvertToRadians(WGS84Longitude + helemetXTransform) ;
//			double OSLatitude = ConvertToRadians(WGS84Latitude + helemetYTransform);
//		
//			double a = 6377563.396, b = 6356256.909;  
//			//double a = 6378137.000, b = 6356752.3141;         // Airy 1830 major & minor semi-axes
//			double F0 = 0.9996012717;                         // NatGrid scale factor on central meridian
//			double lat0 = ConvertToRadians(49), lon0 = ConvertToRadians(-2);  // NatGrid true origin is 49ºN,2ºW
//			double N0 = -100000, E0 = 400000;                 // northing & easting of true origin, metres
//			double e2 = 1 - (b*b)/(a*a);                      // eccentricity squared
//			double n = (a-b)/(a+b), n2 = n*n, n3 = n*n*n;
//
//			double cosLat = Math.Cos(OSLatitude), sinLat = Math.Sin(OSLatitude);
//			double nu = a*F0/Math.Sqrt(1-e2*sinLat*sinLat);              // transverse radius of curvature
//			double rho = a*F0*(1-e2)/Math.Pow(1-e2*sinLat*sinLat, 1.5);  // meridional radius of curvature
//			double eta2 = nu/rho-1;
//			
//			double Ma = (1 + n + (5/4)*n2 + (5/4)*n3) * (OSLatitude-lat0);
//			double Mb = (3*n + 3*n*n + (21/8)*n3) * Math.Sin(OSLatitude-lat0) * Math.Cos(OSLatitude+lat0);
//			double Mc = ((15/8)*n2 + (15/8)*n3) * Math.Sin(2*(OSLatitude-lat0)) * Math.Cos(2*(OSLatitude+lat0));
//			double Md = (35/24)*n3 * Math.Sin(3*(OSLatitude-lat0)) * Math.Cos(3*(OSLatitude+lat0));
//			double M = b * F0 * (Ma - Mb + Mc - Md);              // meridional arc
//			
//			double cos3lat = cosLat*cosLat*cosLat;
//			double cos5lat = cos3lat*cosLat*cosLat;
//			double tan2lat = Math.Tan(OSLatitude)*Math.Tan(OSLatitude);
//			double tan4lat = tan2lat*tan2lat;
//			
//			double I = M + N0;
//			double II = (nu/2)*sinLat*cosLat;
//			double III = (nu/24)*sinLat*cos3lat*(5-tan2lat+9*eta2);
//			double IIIA = (nu/720)*sinLat*cos5lat*(61-58*tan2lat+tan4lat);
//			double IV = nu*cosLat;
//			double V = (nu/6)*cos3lat*(nu/rho-tan2lat);
//			double VI = (nu/120) * cos5lat * (5 - 18*tan2lat + tan4lat + 14*eta2 - 58*tan2lat*eta2);
//			
//			double dLon = OSLongitude -lon0;
//			double dLon2 = dLon*dLon, dLon3 = dLon2*dLon, dLon4 = dLon3*dLon, dLon5 = dLon4*dLon, dLon6 = dLon5*dLon;
//			
//			double N = I + II*dLon2 + III*dLon4 + IIIA*dLon6;
//			double E = E0 + IV*dLon + V*dLon3 + VI*dLon5;
			
			double e100k = Math.Floor((double)grdRef.Easting/100000), n100k = Math.Floor((double)grdRef.Northing/100000);
  
			if (e100k<0 || e100k>6 || n100k<0 || n100k>12) return "Unknown";

			// translate those into numeric equivalents of the grid letters
			double l1 = (19-n100k) - (19-n100k)%5 + Math.Floor((e100k+10)/5);
			double l2 = (19-n100k)*5%25 + e100k%5;
		
			// compensate for skipped 'I' and build grid letter-pairs
			if (l1 > 7) l1++;
			if (l2 > 7) l2++;
			
			l1 += 65;
			l2 += 65;
			
			//Convert the ascii values to characters
			char letterA = (char)l1;
			char letterB = (char)l2;
			
			string gridCode = string.Format("{0}{1}",letterA,letterB);
		
			// strip 100km-grid indices from easting & northing, and reduce precision
			double E = Math.Floor((grdRef.Easting%100000)/Math.Pow(10,1));
			double N = Math.Floor((grdRef.Northing%100000)/Math.Pow(10,1));
			
			
			string retVal = string.Format ("{0} {1} {2}",gridCode,PadNumber(E,4),PadNumber(N,4));
			return retVal;
			}
			catch(Exception ex)
			{
				Console.WriteLine("GeoLocationTools : " + ex.Message);
				return "Error in calculation";
			}
		}
	}
}

