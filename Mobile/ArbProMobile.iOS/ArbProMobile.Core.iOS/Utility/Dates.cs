﻿// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.Core.iOS/Utility/Dates.cs#1 $
// // $Author: James $
// // $Date: 2015/05/31 $
// // $Change: 24702 $
// //*********************************************************************
//
//
using System;
using Foundation;
using CoreLocation;

namespace Microsec.ExtensionMethods
{
	public static class Dates
	{
		public static DateTime NSDateToDateTime(this NSDate date)
		{
			// NSDate has a wider range than DateTime, so clip
			// the converted date to DateTime.Min|MaxValue.
			double secs = date.SecondsSinceReferenceDate;
			if (secs < -63113904000)
				return DateTime.MinValue;
			if (secs > 252423993599)
				return DateTime.MaxValue;
			return (DateTime) date;
		}

		public static NSDate DateTimeToNSDate(this DateTime date)
		{
			if (date.Kind == DateTimeKind.Unspecified)
				date = DateTime.SpecifyKind (date, DateTimeKind.Local);
					return (NSDate) date;
		}

		public static NSDate DateTimeToNSDate(this DateTime? dateInput)
		{
			DateTime date;

			if (dateInput == null) {
				return (NSDate)null;
			}

			date = (DateTime)dateInput;

			if (date.Kind == DateTimeKind.Unspecified)
				date = DateTime.SpecifyKind (date, DateTimeKind.Local);
			return (NSDate) date;
		}

	}
}

