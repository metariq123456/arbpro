﻿// //*********************************************************************
// // $Id$
// // $Author$
// // $Date$
// // $Change$
// //*********************************************************************
//
//
using System;
using RestSharp;

namespace ArbProMobile.Core
{
	public class ArbRestClient : RestClient
	{
		public ArbRestClient(string serviceURI) : base(serviceURI)
		{
		}

		public override IRestResponse<T> Execute<T>(IRestRequest request)
		{
			var response = base.Execute<T>(request);


			if ((response.StatusCode != System.Net.HttpStatusCode.OK || response.ResponseStatus != ResponseStatus.Completed) && (response.StatusCode == System.Net.HttpStatusCode.Unauthorized))
			{
				throw new DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException();
			}

			return response;
		}
	}
}
