﻿// <copyright file="WCFServiceClientWrapper.cs" company = "Microsec Ltd">
// Copyright Microsec Ltd.
// </copyright>
using System;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Collections.Generic;


namespace DGS.ArbPro.Mobile.Service
{
    public class WCFServiceClientWrapper
    {


        #region Fields

        private IArbProService service;
		Microsec.Mobile.Core.Logging.ILogger log;

        #endregion

        #region Constructor

        public WCFServiceClientWrapper ()
        {
            EndpointAddress EndPoint = new EndpointAddress (DGS.ArbPro.Mobile.BL.Settings.WCFServiceURI);
            BasicHttpBinding binding = CreateBasicHttp ();

            service = new ArbProServiceClient (binding, EndPoint);
			log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();

        }

        #endregion

        #region Public Methods

        public List<CalendarItem> GetCalendarItemsForUser (string token, int userID, DateTime fromDateTime, DateTime toDateTime)
        {
			try
			{
				log.Debug(string.Format("Beginning getting calendar appointments for user: {0}", userID));
				List<CalendarItem> ReturnValue = new List<CalendarItem>();
				CalendarItem[] Items;

				Items = Task.Factory.FromAsync(
				 (asyncCallback, asyncState) =>
					service.BeginGetCalendarItemsForUser(token, userID.ToString(), fromDateTime, toDateTime, asyncCallback, asyncState),
				 (asyncResult) =>
					 service.EndGetCalendarItemsForUser(asyncResult), null).Result;

				foreach (var item in Items)
				{
					ReturnValue.Add(item);
				}

				log.Debug(string.Format("Completed getting calendar appointments for user: {0} appointment count: {1}", userID, ReturnValue.Count));

				return ReturnValue;
			}
			catch (AggregateException e)
			{
				if (SessionExpiredHandlingException(e))
				{
					throw new DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException();
				}

				return new List<CalendarItem>();
			}
			catch (Exception ex)
			{
				Core.ArbProInsights.LogErrorError(ex);
				log.Debug(string.Format("Error getting calendar appointments for user: {0} message: {1}", userID, ex.Message));
				return new List<CalendarItem>();
			}

        }

		public bool CreateSalesEnquiry(Client client, string comments, DateTime apptStart, DateTime apptEnd, string houseNo, string address1, string address2, string town, string county, string postCode, int riskTemplateId)
		{
			try
			{
				log.Debug("Beginning creation of Sales Enquiry.");

				bool retVal = false;

				retVal = Task.Factory.FromAsync(
				 (asyncCallback, asyncState) =>
					service.BeginCreateSalesEnquiry(client, BL.Globals.CurrentUser.Id, BL.Globals.CurrentUser.UserGuid.ToString(), "Mobile", comments, apptStart, apptEnd, houseNo, address1, address2, town, county, postCode, riskTemplateId, asyncCallback, asyncState),
				 (asyncResult) =>
					service.EndCreateSalesEnquiry(asyncResult), null).Result;

				log.Debug(string.Format("Completed creation of Sales Enquiry. Status: {0}", retVal));

				return retVal;
			}
			catch (AggregateException e)
			{
				if (SessionExpiredHandlingException(e))
				{
					throw new DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException();
				}

				return false;
			}
			catch (Exception ex)
			{
				Core.ArbProInsights.LogErrorError(ex);
				log.Debug(string.Format("Error during creation of Sales Enquiry: {0}", ex.Message));
				return false;
			}
		}

		public bool UpdateClientContact(int contactId, string title, string firstName, string surname, string houseNo, string address1, string address2, string town, string county, string postCode, string tel, string mobile, string email)
		{
			try
			{
				log.Debug(string.Format("Beginning update of client contact. Contact Id:D {0}", contactId));

				bool retVal = false;

				retVal = Task.Factory.FromAsync(
				 (asyncCallback, asyncState) =>
					service.BeginSaveClientContact(BL.Globals.CurrentUser.UserGuid.ToString(),title, firstName, surname, contactId, houseNo, address1, address2, town, county, postCode, tel, mobile, email, asyncCallback, asyncState),
				 (asyncResult) =>
					service.EndSaveClientContact(asyncResult), null).Result;

				log.Debug(string.Format("Completed update of client contact. Status: {0}, contact Id: {1}", retVal, contactId));

				return retVal;
			}
			catch (AggregateException e)
			{
				if (SessionExpiredHandlingException(e))
				{
					throw new DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException();
				}

				return false;
			}
			catch (Exception ex)
			{
				Core.ArbProInsights.LogErrorError(ex);
				log.Debug(string.Format("Error during update of client contact: {0}", ex.Message));
				return false;
			}
		}

		public List<BL.RiskTemplate> GetRiskTemplateList()
		{
			try
			{
				List<BL.RiskTemplate> retVal = new List<BL.RiskTemplate>();
				RiskTemplate[] templates;

				templates = Task.Factory.FromAsync(
				 (asyncCallback, asyncState) =>
					service.BeginGetRiskTemplates(BL.Globals.CurrentUser.UserGuid.ToString(), asyncCallback, asyncState),
				 (asyncResult) =>
					service.EndGetRiskTemplates(asyncResult), null).Result;

				foreach (var template in templates)
				{
					BL.RiskTemplate item = new BL.RiskTemplate();

					item.Id = template.Id;
					item.Name = template.Name;
					item.RefDoc = template.RefDoc;
					item.Description = template.Description;

					retVal.Add(item);
				}

				return retVal;
			}
			catch (AggregateException e)
			{
				if (SessionExpiredHandlingException(e))
				{
					throw new DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException();
				}

				return new List<BL.RiskTemplate>();
			}
			catch (Exception ex)
			{
				Core.ArbProInsights.LogErrorError(ex);
				log.Debug(string.Format("Error during update of client contact: {0}", ex.Message));

				return new List<BL.RiskTemplate>();
			}
		}

        #endregion

        #region Private Methods

        private static BasicHttpBinding CreateBasicHttp ()
        {
            BasicHttpBinding binding = new BasicHttpBinding {
                Name = "basicHttpBinding",
                MaxBufferSize = 2147483647,
                MaxReceivedMessageSize = 2147483647
            };
            TimeSpan timeout = new TimeSpan (0, 0, 30);
            binding.SendTimeout = timeout;
            binding.OpenTimeout = timeout;
            binding.ReceiveTimeout = timeout;
            return binding;
        }

		bool SessionExpiredHandlingException(AggregateException ex)
		{
			foreach (Exception e in ex.InnerExceptions)
			{
				if (e is FaultException<ServiceUnauthenticatedException>)
				{
					log.Debug(string.Format("Unauthenticated exception: {0}", e.Message));
					return true;
				}
				else 
				{
					log.Debug(string.Format("Error during update of client contact: {0}", ex.Message));
					Core.ArbProInsights.LogErrorError(ex);
				}
			}

			return false;
		}

        #endregion
    }
}

