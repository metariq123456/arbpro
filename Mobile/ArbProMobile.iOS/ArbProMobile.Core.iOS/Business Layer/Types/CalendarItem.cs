﻿// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.Core.iOS/Business Layer/Types/CalendarItem.cs#1 $
// // $Author: ivan $
// // $Date: 2016/09/07 $
// // $Change: 26900 $
// //*********************************************************************
//
//
using System;
using DGS.ArbPro.Mobile.Service;

namespace DGS.ArbPro.Mobile.BL
{
    public class CalendarItem: BL.Contracts.BusinessEntityBase
    {
        public CalendarItem ()
        {

        }

        public CalendarItem (DGS.ArbPro.Mobile.Service.CalendarItem modelCalendarItem)
        {
            Microsec.Tools.ObjectTools.MigrateProperties (modelCalendarItem,
                                                          this,
                                                          null);
        }

        #region Properties
        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Gets or sets the end time.
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Gets or sets the item type.
        /// </summary>
        public CalendarItemType ItemType { get; set; }

        /// <summary>
        /// Gets or sets the Summary - for display in the calendar view.
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// Gets or sets the description of the appointment, for display in a detailed view.
        /// </summary>
        public string Description { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="T:DGS.ArbPro.Mobile.BL.CalendarItem"/> all day.
		/// </summary>
		/// <value><c>true</c> if all day; otherwise, <c>false</c>.</value>
		public bool AllDay { get; set; }

        public string ColourRGB { get; set; }
        #endregion Properties
    }
}

