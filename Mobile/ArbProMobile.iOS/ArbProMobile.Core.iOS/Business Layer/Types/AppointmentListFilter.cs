// //*********************************************************************
// // $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.Core.iOS/Business Layer/Types/AppointmentListFilter.cs#9 $
// // $Author: Jon $
// // $Date: 2015/05/20 $
// // $Change: 24664 $
// //*********************************************************************
//
//
using System;

namespace DGS.ArbPro.Mobile.BL
{
    public class AppointmentListFilter
    {
        #region Fields
        static AppointmentListFilter _instance;
        #endregion

        #region Properties
        /// <summary>
        /// If true, appointment list will be only for today's appointments
        /// </summary>
        public bool TodaysAppointments
        {
            get;
            set;
        }
        /// <summary>
        /// The date from which to display appointments.
        /// </summary>
        /// <value>From date.</value>
        public DateTime FromDate
        {
            set;
            get;
        }
        /// <summary>
        /// The date to which to display appointments.
        /// </summary>
        /// <value>To date.</value>
        public DateTime ToDate
        {
            get;
            set;
        }
        /// <summary>
        /// If set, open appointments will be displayed
        /// </summary>
        /// <value><c>true</c> if appointment status open; otherwise, <c>false</c>.</value>
        public bool AppointmentStatusPending
        {
            get;
            set;
        }
        /// <summary>
        /// If set, completed appointments will be displayed
        /// </summary>
        /// <value><c>true</c> if appointment status completed; otherwise, <c>false</c>.</value>
        public bool AppointmentStatusCompleted
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        private AppointmentListFilter()
        {
            // default status filter to include all statuses
            AppointmentStatusPending = true;
            AppointmentStatusCompleted = true;
        }

        public static AppointmentListFilter Instance()
        {
            if (_instance == null)
            {
                _instance = new AppointmentListFilter();
                _instance.Load();

            }

            return _instance;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Loads filter settings from the settings store.
        /// </summary>
        private void Load()
        {
            //TODO - implement this!
        }
        /// <summary>
        /// Saves filter settings to the settings store.
        /// </summary>
        private void Save()
        {
            //TODO - implement this!
        }
        #endregion

    }
}

