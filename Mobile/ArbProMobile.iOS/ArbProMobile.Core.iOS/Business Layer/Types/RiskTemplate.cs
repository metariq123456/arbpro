﻿// //*********************************************************************
// // $Id$
// // $Author$
// // $Date$
// // $Change$
// //*********************************************************************
//
//
using System;
namespace DGS.ArbPro.Mobile.BL
{
	public class RiskTemplate: BL.Contracts.BusinessEntityBase
	{
		public RiskTemplate()
		{
		}

		public RiskTemplate(DGS.ArbPro.Mobile.Service.RiskTemplate template)
		{
			Microsec.Tools.ObjectTools.MigrateProperties(template,
														  this,
														  null);
		}

        public int Id { get; set; }
		public string Name { get; set; }
		public string RefDoc { get; set; }
		public string Description { get; set; }
	}
}
