// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.Core.iOS/Business Layer/Implementation/Globals.cs#8 $
// $Author: Jon $
// $Date: 2015/05/20 $
// $Change: 24664 $
// *********************************************************************
//
//
using System;
using System.Collections.Generic;
//using MonoTouch.Foundation;
using Interfaces = DGS.ArbPro.Mobile.Interfaces;
using BL = DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.BL
{
    //[Preserve(AllMembers = true)]
	public static class Globals //: Interfaces.IGlobals
    {


        #region Properties
        /// <summary>
        /// The authentication keys for the last valid login
        /// </summary>
        /// <value>The authentication keys.</value>
        private static Dictionary<string, string> m_AuthenticationKeys = null;

        public static Dictionary<string,string> AuthenticationKeys
        {
            get
            {
                if (m_AuthenticationKeys == null)
                    m_AuthenticationKeys = new Dictionary<string, string>();
                    
                return m_AuthenticationKeys;
            }
            set
            {
                m_AuthenticationKeys = value;
            }
        }
        /// <summary>
        /// Gets or sets the last application login date and time.
        /// </summary>
        /// <value>The last app login.</value>
        private static DateTime m_LastAppLogin;

        public static DateTime LastAppLogin
        {
            get{ return m_LastAppLogin;}
            set{ m_LastAppLogin = value;}
        }

        public static bool LoggedIn { get; set; }

		public static BL.User CurrentUser {get;set;}
#endregion
    }
}

