// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.Core.iOS/Business Layer/PartialClasses/ImageEditing_iOS.cs#10 $
// $Author: ivan $
// $Date: 2016/06/01 $
// $Change: 26393 $
// *********************************************************************
using System;
using CoreGraphics;
using System.IO;
using Foundation;
using UIKit;
using DGS.ArbPro.Mobile.Core.Enums;
using Xamarin;

namespace DGS.ArbPro.Mobile.BL
{
  public partial class ImageEditing
  {

		partial void ResizeAndSaveImage(string fileName)
		{
			if (File.Exists(fileName))
			{
				try
				{
					UIImage rawImg = UIImage.FromFile(fileName);
					UIImage newImg = rawImg.Scale(new CGSize(rawImg.Size.Width / 2, rawImg.Size.Height / 2), 0.25f);
					File.Delete(fileName);
					NSError e = new NSError();
					newImg.AsPNG().Save(fileName, false, out e);
					newImg = null;
					rawImg = null;
				}
				catch (Exception ex)
				{
					if (Core.ArbProInsights.LevelOn(LoggingLevel.Error))
					{
						Xamarin.Insights.Report(ex, Insights.Severity.Error);
					}
					Console.WriteLine("Error resizing and saving image - {0}", ex.Message);
				}
			}
		}
  }
}
