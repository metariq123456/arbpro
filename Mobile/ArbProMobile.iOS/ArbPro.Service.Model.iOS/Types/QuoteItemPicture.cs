// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbPro.Service.Model.iOS/Types/QuoteItemPicture.cs#8 $
// $Author: Jon $
// $Date: 2015/05/20 $
// $Change: 24664 $
// *********************************************************************
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Service.Model.Types
{
    [DataContract]
	public class QuoteItemPicture
	{
		public QuoteItemPicture ()
		{
		}
		
		[DataMember]
		public string ItemGuid {get; set;}
		[DataMember]
		public int PacketCount {get;set;}
		[DataMember]
		public int PacketNo {get; set;}
		[DataMember]
		public byte[] ImageBytes{get;set;}
	}
}

