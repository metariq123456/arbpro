﻿
// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/Calendar/ScheduleAppointmentFactory.cs#3 $
// $Author: ivan $
// $Date: 2016/09/14 $
// $Change: 26962 $
// *********************************************************************
using System;
using System.Collections.Generic;
using DGS.ArbPro.Mobile.BL;
using Foundation;
using Syncfusion.SfSchedule.iOS;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{

    public static class ScheduleAppointmentFactory
    {
        public static NSMutableArray ScheduleAppointmentsFromItems (IEnumerable<CalendarItem> items)
        {
            NSMutableArray ReturnValue = new NSMutableArray ();

            foreach (var item in items) {
                ReturnValue.Add (ScheduleAppointmentFromCalendarItem (item));
            }

            return ReturnValue;
        }

		public static ScheduleAppointment ScheduleAppointmentFromCalendarItem(CalendarItem item)
		{
			ScheduleAppointment ReturnValue;

			NSCalendar calendar = NSCalendar.CurrentCalendar;

			NSDate StartDate = DateTimeToNSDate(item.StartTime.ToUniversalTime());
			NSDate EndDate = DateTimeToNSDate(item.EndTime.ToUniversalTime());

			ReturnValue = new ScheduleAppointment();

			ReturnValue.IsAllDay = item.AllDay;
			ReturnValue.StartTime = StartDate;
			ReturnValue.EndTime = EndDate;

			ReturnValue.Subject = (NSString)item.Summary;

			switch (item.ItemType)
			{
				case DGS.ArbPro.Mobile.Service.CalendarItemType.Quote:
					ReturnValue.AppointmentBackground = UIColor.FromRGB(158, 192, 210);
					break;
				case DGS.ArbPro.Mobile.Service.CalendarItemType.Work:
					ReturnValue.AppointmentBackground = UIColor.FromRGB(158, 192, 75);
					break;
				case DGS.ArbPro.Mobile.Service.CalendarItemType.Generic:
					ReturnValue.AppointmentBackground = UIColor.FromRGB(215, 192, 75);
					break;
				default:
					ReturnValue.AppointmentBackground = UIColor.Yellow;
					break;
			}
			//if (!string.IsNullOrEmpty (item.ColourRGB)) {
			//    ReturnValue.AppointmentBackground = item.ColourRGB.ToUIColor ();
			//}
			return ReturnValue;
		}


		// converts date time object to ns date
		static NSDate DateTimeToNSDate(DateTime date)
		{
			DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(2001, 1, 1, 0, 0, 0));

			return NSDate.FromTimeIntervalSinceReferenceDate((date - reference).TotalSeconds);
		}

    }
}

