// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
//
//
using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;
using DGS.ArbPro.Mobile.BL;
using DGS.ArbPro.Mobile.Core;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to
	// application events from iOS.
	[Register ("AppDelegate")]
	public partial class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		UINavigationController _NavController = null;

		public override UIWindow Window {
			get;
			set;
		}
		//
		// This method is invoked when the application has loaded and is ready to run. In this
		// method you should instantiate the window, load the UI into it and then make the window
		// visible.
		//
		// You have 17 seconds to return from this method, or iOS will terminate your application.
		//
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo ("en-GB");
			var container = TinyIoC.TinyIoCContainer.Current;
			container.Register<Interfaces.IServiceConnection, Core.ServiceLayer.RestConnection> ();
			
			// Set up the RGB colour parts for the navigation bar
			float RedColor = 157.0f / 255.0f;
			float GreenColor = 192.0f / 255.0f;
			float BlueColor = 75.0f / 255.0f;
			
			
//			var defaults = NSUserDefaults.StandardUserDefaults;
//			NSDictionary appDefaults = new NSDictionary ("server_preference", "https://cloud.arbprosoftware.com/mobileservice");
//			defaults.RegisterDefaults (appDefaults);
//			defaults.Synchronize ();
//			BL.Settings.ServiceURI = defaults.StringForKey ("server_preference").ToString ();
//			BL.Settings.InsightsEnabled = defaults.BoolForKey ("insights_enabled");
//
//			// Default to maximum logging for now
//			Globals.LoggingLevel = DGS.ArbPro.Mobile.Core.Enums.LoggingLevel.All;
//
//			BL.BusinessLayer.AppVersion = NSBundle.MainBundle.InfoDictionary ["CFBundleVersion"].ToString ();
			
			#if DEBUG
			Console.WriteLine("Service URL = {0}", BL.Settings.ServiceURI);
			#endif
            
			
			//Create the device screen
			Window = new UIWindow (UIScreen.MainScreen.Bounds);
			//Create the first view controller
			var controller = new MainViewController ();
			//Create the Navigation controller and set the root view
			_NavController = new UINavigationController (controller);

			// Set up the custom colours for ArbPro
			var titleText = new UIStringAttributes ();
			titleText.ForegroundColor = UIColor.White;
			_NavController.NavigationBar.TitleTextAttributes = titleText;
			_NavController.NavigationBar.BarTintColor = new UIColor (RedColor, GreenColor, BlueColor, 1.0f);
			_NavController.NavigationBar.TintColor = UIColor.White;

			//_NavController.Title = controller.Title;
			//Set the Window to use the Navigation controller created
			Window.RootViewController = _NavController;
			//Show the window
			Window.MakeKeyAndVisible ();

			return true;
		}
		// This method is invoked when the application is about to move from active to inactive state.
		// OpenGL applications should use this method to pause.
		public override void OnResignActivation (UIApplication application)
		{
		}
		// This method should be used to release shared resources and it should store the application state.
		// If your application supports background exection this method is called instead of WillTerminate
		// when the user quits.
		public override void DidEnterBackground (UIApplication application)
		{
			Globals.LastAppLogin = DateTime.Now;
		}

		/// This method is called as part of the transtion from background to active state.
		public override void WillEnterForeground (UIApplication application)
		{
			NSUserDefaults defaults = NSUserDefaults.StandardUserDefaults;
			defaults.Synchronize ();

			string url = defaults.StringForKey ("server_preference");
			BL.Settings.ServiceURI = url;

			bool InsightsEnabled = defaults.BoolForKey ("insights_enabled");
			BL.Settings.InsightsEnabled = InsightsEnabled;
            BL.Settings.DataRetainDays = (int)defaults.IntForKey("data_retain_days");



#if DEBUG
			//if ((Globals.LoginStatus != Globals.LoggedInStatus.NotLoggedIn) && Globals.LastAppLogin < DateTime.Now.AddSeconds(-1))
			//	_NavController.PushViewController(new LoginViewController("Please login to use the application."),false);			
#else
			//if ((Globals.LoginStatus != Globals.LoggedInStatus.NotLoggedIn) && Globals.LastAppLogin < DateTime.Now.AddMinutes (-30))
			//	_NavController.PushViewController (new LoginViewController ("Please login to use the application."), false);
#endif
		}
		/// This method is called when the application is about to terminate. Save data, if needed. 
		public override void WillTerminate (UIApplication application)
		{
		}
	}
}

