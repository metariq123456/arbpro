// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/UITools/ConnectionStatus.cs#7 $
// $Author: Jon $
// $Date: 2015/05/20 $
// $Change: 24664 $
// *********************************************************************
using System;
using DGS.ArbPro.Mobile;
using  DGS.ArbPro.Mobile.Core.Enums;


namespace DGS.ArbPro.Mobile.iOS.UI
{
  public static class ConnectionStatus
  {
		public static NetworkStatus CheckReachability()
		{
			try
			{
				return Reachability.RemoteHostStatus(BL.Settings.ReachabilityURI);
			}
			catch
			{
				return NetworkStatus.NotReachable;
			}
		}
  }
}

