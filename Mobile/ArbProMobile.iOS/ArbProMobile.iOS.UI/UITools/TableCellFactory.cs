// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/UITools/TableCellFactory.cs#9 $
// $Author: Jon $
// $Date: 2015/05/27 $
// $Change: 24685 $
// *********************************************************************
//
//
using System;
using UIKit;
using ObjCRuntime;
using Foundation;

namespace Microsec.Tools.iOS.UI
{
    public class TableCellFactory<T> where T : UITableViewCell
    {
        private string _sCellID = @"";
        private string _sXibName = @"";

        public TableCellFactory(string cellID, string xibName)
        {
            _sCellID = cellID;
            _sXibName = xibName;
        }

        public T GetCell(UITableView table_view)
        {
            var cell = table_view.DequeueReusableCell(_sCellID) as T;
			
            if (cell == null)
            {
                cell = Activator.CreateInstance<T>();
                var views = NSBundle.MainBundle.LoadNib(_sXibName, cell, null);
                cell = Runtime.GetNSObject(views.ValueAt (0))as T;
            }
			
            return cell;
        }
    }
}

