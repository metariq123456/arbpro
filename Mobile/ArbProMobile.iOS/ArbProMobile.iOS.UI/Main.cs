// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/Main.cs#14 $
// $Author: ivan $
// $Date: 2016/06/01 $
// $Change: 26393 $
// *********************************************************************
//
//
using System;
using System.Collections.Generic;
using System.Linq;
using Foundation;
using UIKit;
using Xamarin;

namespace ArbProMobile.iOS.UI
{
	public class Application
	{
		// This is the main entry point of the application.
		static void Main (string[] args)
		{
			try
			{
				// if you want to use a different Application Delegate class from "AppDelegate"
				// you can specify it here.
				UIApplication.Main(args, null, "AppDelegate");
			}
			catch (Exception ex)
			{
				DGS.ArbPro.Mobile.Core.ArbProInsights.LogError(ex, Insights.Severity.Critical);
			}
		}
	}
}
