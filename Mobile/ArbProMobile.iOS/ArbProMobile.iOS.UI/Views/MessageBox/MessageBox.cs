// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/Views/MessageBox/MessageBox.cs#8 $
// $Author: Jon $
// $Date: 2015/05/27 $
// $Change: 24685 $
// *********************************************************************
using System;
using CoreGraphics;
using System.Threading.Tasks;
using UIKit;
using Foundation;
using System.Collections.Generic;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public enum MessageBoxResult
	{
		None = 0,
		OK,
		Cancel,
		Yes,
		No
	}

	public enum MessageBoxButton
	{
		OK = 0,
		OKCancel,
		YesNo,
		YesNoCancel
	}

	public static class MessageBox
	{
		private static MessageBoxResult DialogResult { get; set;}

		public static async Task<MessageBoxResult> Show(string messageBoxText, string caption, MessageBoxButton buttonType)
		{
	  		MessageBoxResult result = MessageBoxResult.Cancel;// new TaskCompletionSource<MessageBoxResult> ();
			nint buttonClicked = -1;
			MessageBoxButton button = buttonType;

			string[] otherButtons = null;

			switch (button)
			{
				case MessageBoxButton.OK:
				// TODO - remove this once we've determined we don't need it
//					cancelButton = "";
					otherButtons = new string[1];
					otherButtons[0] = "OK";
					break;

					case MessageBoxButton.OKCancel:
					otherButtons = new string[1];
					otherButtons[0] = "OK";
					break;

					case MessageBoxButton.YesNo:
				// TODO - remove this once we've determined we don't need it
//					cancelButton = "";
					otherButtons = new string[2];
					otherButtons[0] = "Yes";
					otherButtons[1] = "No";
					break;

					case MessageBoxButton.YesNoCancel:
					otherButtons = new string[2];
					otherButtons[0] = "Yes";
					otherButtons[1] = "No";
					break;
			}

			buttonClicked = await AsyncShowMessage(messageBoxText,caption,otherButtons);

			switch (button)
			{
		case MessageBoxButton.OK:
		  result = MessageBoxResult.OK;//.SetResult(MessageBoxResult.OK);
					break;

					case MessageBoxButton.OKCancel:
					if (buttonClicked == 1)
						result = MessageBoxResult.OK;//result.SetResult(MessageBoxResult.OK);
					break;

					case MessageBoxButton.YesNo:
					if (buttonClicked == 0)
						result = MessageBoxResult.Yes;//                 result.SetResult(MessageBoxResult.Yes);
					else
						result = MessageBoxResult.No;//                result.SetResult(MessageBoxResult.No);
					break;

					case MessageBoxButton.YesNoCancel:
					if (buttonClicked == 1)
						result = MessageBoxResult.Yes;//                 result.SetResult(MessageBoxResult.Yes);
					else if (buttonClicked == 2)
						result = MessageBoxResult.No;//                result.SetResult(MessageBoxResult.No);
					break;

				default:
					result = MessageBoxResult.Cancel;//result.SetResult(MessageBoxResult.Cancel);
					break;
						
			}

			return result; //result.Task;
		}

		public static async Task<MessageBoxResult> Show(string messageBoxText)
		{
			return await Show(messageBoxText, "", MessageBoxButton.OK);
		}
		
		public static async Task<MessageBoxResult> Show(string messageBoxText, string caption)
		{
			return await Show(messageBoxText, caption, MessageBoxButton.OK);
		}

		private static Task<nint> AsyncShowMessage(string messageBoxText, string caption, params string[] buttons)
		{
			var alert = new UIAlertView(caption, messageBoxText, null, null, buttons);
	  		var result = new TaskCompletionSource<nint> ();

	  		alert.Show ();

			alert.Clicked += (object sender, UIButtonEventArgs e) => 
			{
				result.SetResult(e.ButtonIndex);
			};
	  		return result.Task;
		}
	}
}

