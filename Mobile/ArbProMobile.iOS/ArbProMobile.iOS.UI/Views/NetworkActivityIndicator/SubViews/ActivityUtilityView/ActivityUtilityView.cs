using System;
using CoreGraphics;

using Foundation;
using UIKit;
using CoreAnimation;


namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class ActivityUtilityView : UIView
	{		
		public string TaskText
		{
			get{return this.ActivityLabel.Text;}
			set{this.ActivityLabel.Text = value;}
		}
		
		public string ActionText
		{
			get{return this.CaptionLabel.Text;}
			set{this.CaptionLabel.Text = value;}
		}
		
		public ActivityUtilityView () : base ()
		{
			Initialise();
		}
		
		public ActivityUtilityView(IntPtr handle): base (handle)
		{
			Initialise();
		}
		
		private void Initialise()
		{
			UIBezierPath corner_mask = UIBezierPath.FromRoundedRect (this.Bounds,UIRectCorner.AllCorners,new CGSize(25f,25f));
			CAShapeLayer layer = new CAShapeLayer();
			layer.Frame = this.Bounds;
			layer.Path = corner_mask.CGPath;
			this.Layer.Mask = layer;
		}
		
	}
}

