// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("ActivityUtilityView")]
    partial class ActivityUtilityView
    {
        [Outlet]
        UIKit.UILabel ActivityLabel { get; set; }


        [Outlet]
        UIKit.UILabel CaptionLabel { get; set; }


        [Outlet]
        UIKit.UIActivityIndicatorView Indicator { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ActivityLabel != null) {
                ActivityLabel.Dispose ();
                ActivityLabel = null;
            }

            if (CaptionLabel != null) {
                CaptionLabel.Dispose ();
                CaptionLabel = null;
            }

            if (Indicator != null) {
                Indicator.Dispose ();
                Indicator = null;
            }
        }
    }
}