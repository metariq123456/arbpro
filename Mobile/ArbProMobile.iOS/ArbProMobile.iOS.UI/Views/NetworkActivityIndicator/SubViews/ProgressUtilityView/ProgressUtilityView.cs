using System;
using CoreGraphics;

using Foundation;
using UIKit;
using CoreAnimation;

//using RespondMobile.BL.Managers.Service;

using TinyIoC;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class ProgressUtilityView : UIView
	{
		public delegate void DialogButtonClicked(string caption);
		public event DialogButtonClicked DialogButtonClickedEvent;
		
		private float m_FltMinValue = 0;
		private float m_FltMaxValue = 100;
		// TODO - remove this once we've determined we don't need it
//		private bool m_HasSetMaxValue = false;
		private string m_StrTitle = @"Loading. Please wait..";
		
		UILabel _LblMinValue = null;
		UILabel _LblMaxValue = null;
		UILabel _LblTitle = null;
		UIProgressView _ProgressView = null;
		UIActivityIndicatorView _ActivityIndicator = null;
		UIButton _BtnCancel = null;
		
		public float MinValue
		{
			get
			{
				return m_FltMinValue;
			}
			set
			{
				m_FltMinValue = value;
				//LblMinValue.Text = value.ToString ();
				_LblMinValue.Text = m_FltMinValue.ToString ();
			}
		}
		
		public float MaxValue
		{
			get
			{
				return m_FltMaxValue;
			}
			set
			{
				m_FltMaxValue = value;
				_ProgressView.SetProgress (0f,false);
				_LblMaxValue.Text = value.ToString ();
			}
		}
		
		public string ViewTitle {
			get { return m_StrTitle;}
			set {
				m_StrTitle = value;
				_LblTitle.Text = value;
			}
		}
		
		public ProgressUtilityView () : base (new CGRect(0,0,248,120))
		{
			Initialise();
		}
		
		public ProgressUtilityView(IntPtr handle): base (handle)
		{
			Initialise();
		}
		
		private void Initialise()
		{
			UIColor ArbPro = new UIColor(157.0f/255.0f,192.0f/255.0f,75.0f/255.0f,1f);
			this.BackgroundColor = UIColor.White;
			//Construct the UIIndicator
			_ActivityIndicator = new UIActivityIndicatorView(new CGRect(6,8,20,20));
			_ActivityIndicator.ActivityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray;
			_ActivityIndicator.Color = ArbPro;
			
			//Construct title label
			_LblTitle = new UILabel(new CGRect(26,8,200,21));
			_LblTitle.TextAlignment = UITextAlignment.Center;
			//Construct min value label
			_LblMinValue = new UILabel(new CGRect(6,59,100,21));
			_LblMinValue.TextAlignment = UITextAlignment.Left;
			//Construct max value label
			_LblMaxValue = new UILabel(new CGRect(141,59,100,21));
			_LblMaxValue.TextAlignment = UITextAlignment.Right;
			//Construct progress view
			_ProgressView = new UIProgressView(new CGRect(6,42,235,9));
			_ProgressView.ProgressTintColor = ArbPro;
			//Construct Cancel button
			_BtnCancel = UIButton.FromType(UIButtonType.RoundedRect);
			_BtnCancel.Frame = new CGRect(12,80,223,30);
			_BtnCancel.SetTitle("Cancel", UIControlState.Normal);
			_BtnCancel.SetTitleColor (ArbPro, UIControlState.Normal);
			_BtnCancel.SetTitleColor (UIColor.Black,UIControlState.Selected);
			_BtnCancel.TintColor = ArbPro;
			_BtnCancel.TouchUpInside += CancelButtonClicked;
			//Add the views.
			this.AddSubviews (new UIView[]{_LblTitle, _ActivityIndicator, _LblMinValue, _LblMaxValue, _ProgressView, _BtnCancel});
			//Set the look of the view
			UIBezierPath corner_mask = UIBezierPath.FromRoundedRect (this.Bounds,UIRectCorner.AllCorners,new CGSize(25f,25f));
			CAShapeLayer layer = new CAShapeLayer();
			layer.Frame = this.Bounds;
			layer.Path = corner_mask.CGPath;
			this.Layer.Mask = layer;
		}
		
		
		public void show(string title)
		{
			this.Hidden = false;
			_BtnCancel.SetTitle ("Cancel", UIControlState.Normal);
			ViewTitle = title;
			MinValue = 0f;
			MaxValue = 0f;
			_ActivityIndicator.StartAnimating ();
		}
		
		public void SetCompleted(string caption)
		{
			 _ActivityIndicator.StopAnimating ();
			 _LblTitle.Text = caption;
			 _BtnCancel.SetTitle ("Ok", UIControlState.Normal);
		}
		
		public void AddToCurrentProgress (float value)
		{
			if (MinValue + value <= MaxValue) {
				this.MinValue = m_FltMinValue + value;
				float pos = MinValue / MaxValue;
				pos = pos < 1 ? pos : 1;
				_ProgressView.SetProgress (pos, true);
			}
			else
			{
				MinValue = MaxValue;
				_ProgressView.SetProgress(1f,true);
			}
		}
		
		public void CancelButtonClicked(object sender, EventArgs e)
		{
			if(DialogButtonClickedEvent != null)
			{
				DialogButtonClickedEvent(_BtnCancel.Title(UIControlState.Normal));
			}
		}
		
		
	}
}

