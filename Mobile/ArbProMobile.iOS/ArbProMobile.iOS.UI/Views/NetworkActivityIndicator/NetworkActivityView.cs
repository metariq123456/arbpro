// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/Views/NetworkActivityIndicator/NetworkActivityView.cs#10 $
// $Author: ivan $
// $Date: 2016/06/01 $
// $Change: 26393 $
// *********************************************************************

using System;
using CoreGraphics;
using Foundation;
using UIKit;
using CoreAnimation;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class NetworkActivityView : UIView
	{
		#region Members
		ActivityUtilityView _ActivityView = null;
		ProgressUtilityView _ProgressView = null;
		CGPoint _PortraitCenter;
		CGPoint _LanscapeCenter;
		#endregion
		
		#region Properties

		public string ActivityDetail
		{
			get{ return _ActivityView.TaskText;}
			set{ _ActivityView.TaskText = value;}
		}

		public ProgressUtilityView ProgressView {
			get{ return _ProgressView;}
		}
		
		#endregion
		
		#region Constructors
		public NetworkActivityView (IntPtr handle) : base (handle)
		{
			InitialiseView();
		}

		public NetworkActivityView () : base()
		{
			InitialiseView();
		}
		#endregion
		
		#region Private Methods
		private CGPoint GetScreenCenter()
		{
			if(UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft ||
			 UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight )
			 {
				return _LanscapeCenter;
			 }
			 else
			 {
				return _PortraitCenter;
			 }
		}
		
		void InitialiseView()
		{
			nfloat x = UIScreen.MainScreen.Bounds.Width / 2;
			nfloat y = UIScreen.MainScreen.Bounds.Height / 2;
			
			_PortraitCenter = new CGPoint(x,y);
			_LanscapeCenter = new CGPoint(y,x);
			
			//Create the network indicator
			IntPtr intptr_1 = NSBundle.MainBundle.LoadNib ("ActivityUtilityView",null, null).ValueAt (0);
			_ActivityView = new ActivityUtilityView(intptr_1);
			this.AddSubview (_ActivityView);
			_ActivityView.Center = GetScreenCenter();
			_ActivityView.Hidden = true;
			//Create the progress indiciator
//			IntPtr intptr_2 = NSBundle.MainBundle.LoadNib ("ProgressUtilityView",null, null).ValueAt (0);
//			_ProgressView = new ProgressUtilityView(intptr_2);
			_ProgressView = new ProgressUtilityView();
			this.AddSubview (_ProgressView);
			_ProgressView.Center = GetScreenCenter();
			_ProgressView.Hidden = true;
		}
		#endregion
		
		#region Overrides
		public override void LayoutSubviews ()
		{
			CGPoint center = GetScreenCenter();
			_ActivityView.Center = center;
			_ProgressView.Center = center;
			base.LayoutSubviews ();
		}
		#endregion
		
		#region Public Methods
		public void DisplayNetworkIndicator(bool hidden)
		{
			_ActivityView.Center = GetScreenCenter();
			this.Hidden = hidden;
			_ActivityView.Hidden = hidden;
		}
		
		public void SetMaxProgressValue(int value)
		{
			InvokeOnMainThread (delegate
			{
				_ProgressView.Hidden = false;
				//_ProgressView.MaxValue = value;
			});
		}
		
		public void SetMinProgressValue(int value)
		{
			InvokeOnMainThread (delegate
			{
				_ProgressView.Hidden = false;
				//_ProgressView.MinValue = value;
			});
		}
		
		
		public void ShowProgress(string title)
		{
			InvokeOnMainThread(delegate
			{
				_ProgressView.Center = GetScreenCenter();
				this.Hidden = false;
				_ProgressView.show(title);
			});
		} 
		// TODO check whether we needed async on this
		public void SetMaxValue(int value)
		{
			InvokeOnMainThread (delegate
			{
				_ProgressView.MaxValue = (float)value;
			});
		}

		// TODO check whether we needed async on this
		public void SetMinValue(int value)
		{
			InvokeOnMainThread (delegate
			{
				_ProgressView.MinValue = (float)value;
			});
		}

		// TODO check whether we needed async on this
		public void SetProgressTitle(string value)
		{
			InvokeOnMainThread (delegate
			{
				_ProgressView.ViewTitle = value;
			});
		}

		// TODO check whether we needed async on this
		public void IncreaseProgress(int value)
		{
			InvokeOnMainThread (delegate
			{
				_ProgressView.AddToCurrentProgress(value);
			});
		}
		
		public void DisplayCompletedStatus(string caption)
		{
			InvokeOnMainThread (delegate
			{
				_ProgressView.SetCompleted(caption);
			});
		}
		
		public void HideProgress()
		{
			_ProgressView.Hidden = true;
			this.Hidden = true;
		}

		public static NetworkActivityView LoadFromNib(NSObject owner)
		{
			var vs = NSBundle.MainBundle.LoadNib("NetworkActivityView", owner, null);

			var v = ObjCRuntime.Runtime.GetNSObject(vs.ValueAt(0)) as NetworkActivityView;

			return v;
		}
		#endregion
	}
}

