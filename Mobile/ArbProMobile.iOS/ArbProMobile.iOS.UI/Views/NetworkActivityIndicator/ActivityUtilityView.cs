using System;
using System.Drawing;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreAnimation;


namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class ActivityUtilityView : UIView
	{
		UIControl m_MyController = null;
		
		public string TaskText
		{
			get{return this.ActivityLabel.Text;}
			set{this.ActivityLabel.Text = value;}
		}
		
		public string ActionText
		{
			get{return this.CaptionLabel.Text;}
			set{this.CaptionLabel.Text = value;}
		}
	
		/*
		public ActivityUtilityView () : base ("ActivityUtilityView", null)
		{
		}
		*/
		
		public ActivityUtilityView () : base ()
		{
			Initialise();
		}
		
		public ActivityUtilityView(IntPtr handle): base (handle)
		{
			Initialise();
			Console.WriteLine(@"ActivityUtilityView created");
		}
		
		private void Initialise()
		{
			//Set the look of the view
			UIBezierPath corner_mask = UIBezierPath.FromRoundedRect (this.Bounds,UIRectCorner.AllCorners,new SizeF(25f,25f));
			CAShapeLayer layer = new CAShapeLayer();
			layer.Frame = this.Bounds;
			layer.Path = corner_mask.CGPath;
			this.Layer.Mask = layer;	
			/*
			CGAffineTransform tf = new CGAffineTransform();// (1.5f,1.5f);
			tf.Scale (1.5f,1.5f);
			this.ActivityIndicator.Transform = tf;
			*/
		}
		
		public void Show()
		{
			InvokeOnMainThread (delegate
			{
				this.Center = new PointF(this.Superview.Center.X,this.Superview.Center.Y);
				this.Hidden = false;
				UIApplication.SharedApplication.NetworkActivityIndicatorVisible = true;
				//this.Indicator.StartAnimating ();
			});
			
		}
		
		public void Dismiss()
		{
			using (NSObject obj = new NSObject()) 
			{
				obj.BeginInvokeOnMainThread (delegate 
				{
					UIApplication.SharedApplication.NetworkActivityIndicatorVisible = false;
					this.Hidden = true;
				});
			}
		}
		/*
		public override void Draw (RectangleF rect)
		{
			Console.WriteLine (@"Acivity Utility view draw entered");
			InvokeOnMainThread(delegate
			{
				base.Draw (rect);
				this.Center = new PointF(this.Superview.Center.X, this.Superview.Center.Y);
			});
		}
		*/
		
	}
}

