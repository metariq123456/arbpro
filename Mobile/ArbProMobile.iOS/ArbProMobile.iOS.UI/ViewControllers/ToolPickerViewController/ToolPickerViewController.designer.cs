// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("ToolPickerViewController")]
    partial class ToolPickerViewController
    {
        [Outlet]
        UIKit.UITableView DataTable { get; set; }


        [Outlet]
        DGS.ArbPro.Mobile.iOS.UI.NetworkActivityView NetworkView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (DataTable != null) {
                DataTable.Dispose ();
                DataTable = null;
            }

            if (NetworkView != null) {
                NetworkView.Dispose ();
                NetworkView = null;
            }
        }
    }
}