﻿using System;
using CoreGraphics;
using System.Collections.Generic;
using Foundation;
using UIKit;
using DGS.ArbPro.Mobile.BL;
using System.Threading.Tasks;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class ToolPickerViewController : UIViewController
	{
		#region Delegates and Events
		public delegate void ToolsSelectedDelegate(List<QuoteTool> selections);
		public event ToolsSelectedDelegate ToolsSelectedEvent;
		#endregion

		#region Members
		private List<QuoteTool> _CurrentTools = null;
		private List<QuoteTool> _NewToolsSelections = null;
		private ToolPickerViewSource _DataSource = null;
		private UIBarButtonItem _BtnDone;

		class ToolPickerViewSource :UITableViewSource
		{

			#region Members

			const string CONST_CELL_NAME = "ToolCell";
			ToolPickerViewController _Controller;

			#endregion

			#region Properties

			public List<BL.Tool> _AvailableTools { get; set; }

			#endregion

			#region Constructors

			public ToolPickerViewSource (ToolPickerViewController controller, List<Tool> availableTools) 
			{
				_Controller = controller;
				_AvailableTools = availableTools;
				//RefreshAsync();
				//Refresh();
			}

			#endregion

			#region Overrides

			public override nint NumberOfSections (UITableView tableView)
			{
				return 1;
			}

			public override string TitleForHeader (UITableView tableView, nint section)
			{
				return "Available Tools";
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				return _AvailableTools.Count;
			}

			public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
			{
				return 50;
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				//Get a table cell from the factory
				UITableViewCell cell = tableView.DequeueReusableCell (CONST_CELL_NAME);

				if(cell == null)
				{
					cell = new UITableViewCell (UITableViewCellStyle.Default, CONST_CELL_NAME);
				}

				Tool t = _AvailableTools [indexPath.Row];

				if(_Controller.SelectedTools.Find(qt => qt.Name == t.Name) == null)
					cell.Accessory = UITableViewCellAccessory.None;
				else
					cell.Accessory = UITableViewCellAccessory.Checkmark;

				cell.TextLabel.Text = t.Name;

				return cell;
			}

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				Tool t = _AvailableTools[indexPath.Row];
				_Controller.ToolSelected (t);
			}

			#endregion
		}

		#endregion

		#region Properties

		private List<QuoteTool> CurrentTools
		{
			get{ return _CurrentTools; }
		}

		private List<QuoteTool> SelectedTools
		{
			get{ return _NewToolsSelections; }
		}

		#endregion

		#region Constructors

		public ToolPickerViewController (List<QuoteTool> selectedTools) : base ("ToolPickerViewController", null)
		{
			_CurrentTools = selectedTools;
			_NewToolsSelections = new List<QuoteTool>();
			_BtnDone = new UIBarButtonItem ("Done", UIBarButtonItemStyle.Plain, BtnDoneClicked);
			this.NavigationItem.RightBarButtonItem = _BtnDone;
		}

		#endregion

		#region Overriden Methods

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewWillAppear(bool animated)
		{
			if (BL.Globals.DemoData) {
				NavigationItem.Prompt = "Demonstration Mode";
			} else {
				NavigationItem.Prompt = null;
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear (animated);
			RefreshData ();
		}

		#endregion

		#region Private Methods

		private void ToolSelected(Tool selected)
		{
			QuoteTool qt = _NewToolsSelections.Find (t => t.Name == selected.Name);
			if (qt != null)
				_NewToolsSelections.Remove (qt);
			else
			{
				qt = new QuoteTool ();
				qt.Name = selected.Name;
				qt.Number = 1;
				qt.Id = selected.Id;
				_NewToolsSelections.Add (qt);
			}
			DataTable.ReloadData ();
		}

		void RefreshData ()
		{
			SetActivityStatus (true);
			PopulateTableSource ();
		}

		private async void PopulateTableSource()
		{
			try
			{
				_DataSource = await PopulateTableSourceTask ();
				DataTable.Source = _DataSource;
				DataTable.ReloadData();
			}
			catch (Exception)
			{
				UIAlertView alert = new UIAlertView ("Error", "Error populating Tools table. Please navigate to the prieviouse page and try again", null, "Ok");
				alert.Show ();
			}
			SetActivityStatus (false);
		}

		private Task<ToolPickerViewSource>PopulateTableSourceTask()
		{
			var taskResult = new TaskCompletionSource<ToolPickerViewSource> ();
			List<Tool> allTools = BL.BusinessLayer.ToolList ();
			List<Tool> availableTools = allTools.FindAll (t => CurrentTools.Find (qt => qt.Name == t.Name) == null);
			taskResult.SetResult (new ToolPickerViewSource (this, availableTools));
			return taskResult.Task;
		}

		private void SetActivityStatus(bool working)
		{
			this.NetworkView.DisplayNetworkIndicator (!working);
			//this.NavigationItem
			_BtnDone.Enabled = !working;
		}

		private void BtnDoneClicked(object sender, EventArgs e)
		{
			if (_NewToolsSelections != null && _NewToolsSelections.Count > 0)
			{
				if (ToolsSelectedEvent != null)
				{
					ToolsSelectedEvent (_NewToolsSelections);
					ToolsSelectedEvent = null;
				}
			}

			NavigationController.PopViewController (true);
		}

		#endregion
	}
}

