// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	[Register ("AppointmentNewViewController")]
	partial class AppointmentNewViewController
	{
		[Outlet]
		DGS.ArbPro.Mobile.iOS.UI.NetworkActivityView NetworkActivityView { get; set; }

		[Outlet]
		UIKit.UITableView NewAppointmentTable { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (NewAppointmentTable != null) {
				NewAppointmentTable.Dispose ();
				NewAppointmentTable = null;
			}

			if (NetworkActivityView != null) {
				NetworkActivityView.Dispose ();
				NetworkActivityView = null;
			}
		}
	}
}
