﻿// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
using System;
using System.Threading.Tasks;
using DGS.ArbPro.Mobile.BL;
using DGS.ArbPro.Mobile.Core.Enums;
using Foundation;

using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class AppointmentNewViewController : UIViewController
	{
		#region properties
		UIBarButtonItem _BtnDone;

		// The following properties are used for client and job site creation
		int clientId = -1;
		string clientName = "New or Existing Client for Appointment";
		string houseNo = string.Empty;
		string address1 = string.Empty;
		string address2 = string.Empty;
		string town = string.Empty;
		string county = string.Empty;
		string postCode = string.Empty;
		BL.Client Client;
		BL.CalendarItem itm;
		BL.RiskTemplate risk;

		// The following properties are used for appointment creation
		DateTime startDate = DateTime.MinValue;
		DateTime endDate = DateTime.MinValue;

		// Used for any notes or comments to be added to the appointment
		string commentText = string.Empty;
		NetworkStatus _NetworkStatus = NetworkStatus.NotReachable;

		#endregion

		#region Members

		const int SECTION_CLIENT = 0;
		const int ROW_CLIENT = 0;
		const int SECTION_JOBSITE = 1;
		const int ROW_JOBSITE = 0;
		const int SECTION_TIME = 2;
		const int ROW_TIME = 0;
		const int SECTION_COMMENTS = 4;
		const int ROW_COMMENTS = 0;
		const int SECTION_RISK = 3;
		const int ROW_RISK = 0;

		#endregion

		public AppointmentNewViewController() : base("AppointmentNewViewController", null)
		{
			this.Title = @"New Appointment";

			_BtnDone = new UIBarButtonItem("Save", UIBarButtonItemStyle.Plain, this.BtnDoneClicked);
			this.NavigationItem.RightBarButtonItem = _BtnDone;
		}

		#region Overrides

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		// checking if it is demonstration mode and making calls to load the data
		public override void ViewWillAppear(bool animated)
		{
			NewAppointmentTableSource MySource;
			base.ViewWillAppear(animated);

			Title = "New Appointment";


			if (BL.Globals.DemoData)
			{
				NavigationItem.Prompt = "Demonstration Mode";
			}
			else
			{
				NavigationItem.Prompt = null;
			}

			CheckConnectivity();

			MySource = new NewAppointmentTableSource(this);
			NewAppointmentTable.Source = MySource;
			NewAppointmentTable.ReloadData();
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			if (_NetworkStatus == NetworkStatus.NotReachable)
			{
				DisplayNoConnectionStatus();
			}
			else
			{
				SyncTemplatesAndAppointments();
			}
		}

		#endregion

		#region Public Methods


		async void CheckConnectivity()
		{
			await AsyncCheckConnectivity().ConfigureAwait(true);
		}

		async Task AsyncCheckConnectivity()
		{
			await Task.Run(() => _NetworkStatus = ConnectionStatus.CheckReachability()).ConfigureAwait(false);// BusinessLayer.AppointmentListDownload (Globals.CurrentUser.Id)).ConfigureAwait (false);
		}

		void DisplayNoConnectionStatus()
		{
			UIAlertView alert = new UIAlertView();
			alert.Title = @"No network connection available";
			alert.AddButton("Ok");
			alert.Message = "This feature requires internet connectivity.\nPlease try again later.";
			alert.Show();
		}

		// Navigation method to go to the client list view controller on the press of the select client button from the table
		public void SelectOrAddNewClient()
		{
			NavigationItem.Title = "New Appt";
			this.NavigationController.PushViewController(new ClientListViewController(true), true);
		}

		// Navigation method to go to the appointment job site view controller on the press of the site details button from the table
		public void UpdateJobSite()
		{
			NavigationItem.Title = "New Appt";
			this.NavigationController.PushViewController(new AppointmentJobSiteViewController(clientName, houseNo, address1, address2, town, county, postCode), true);
		}

		// Navigation method to go to the appointment calendar view controller on the press of the date and time button from the table
		public void SelectDateAndTime()
		{
			NavigationItem.Title = "New Appt";
			this.NavigationController.PushViewController(new AppointmentCalendarViewController(itm),true);
		}

		// Navigation method to go to the notes view controller on the press of the comments button from the table
		public void AddOrEditComments(int index)
		{
			NotesInputViewController controller = new NotesInputViewController();
			controller.Enabled = true;
			controller.ViewTitle = "Comments";
			controller.Index = index;
			controller.CurrentText = commentText;
			controller.KeyboardType = UIKeyboardType.Default;
			controller.DidInputTextEvent += CommentTextInputSaved;

			NavigationItem.Title = "New Appt";
			NavigationController.PushViewController(controller, true);
		}

		public void SelectRiskTemplate()
		{
			NavigationItem.Title = "New Appt";
			this.NavigationController.PushViewController(new AppointmentRiskTemplate(), true);
		}

		public void GetRiskTemplateInfo(BL.RiskTemplate r)
		{
			this.risk = r;
		}

		// method to store the job site details from the job site view controller
		public void StoreJobSiteDetails(string hNo, string addr1, string addr2, string twn, string cnty, string pCode)
		{
			houseNo = hNo;
			address1 = addr1;
			address2 = addr2;
			town = twn;
			county = cnty;
			postCode = pCode;
		}

		// method to store the client object fro mthe client list view controller
		public void GetClientForAppt(BL.Client client)
		{
			Client = client;

			if (Client.AccountType == 0)
			{
				clientName = Client.Forename + " " + Client.Surname;
			}
			else
			{
				clientName = Client.BusinessName;
			}

			if (clientId != Client.Id || Client.Id == -1)
			{
				houseNo = Client.HouseNameNumber;
				address1 = Client.Address1;
				address2 = Client.Address2;
				town = Client.Town;
				county = Client.County;
				postCode = Client.PostCode;
			}

			clientId = Client.Id;
		}

		// method to store the start and end date and time from the appointment calendar view controller
		public void GetDateAndTimeForAppt(BL.CalendarItem item)
		{
			itm = item;
			startDate = item.StartTime;
			endDate = item.EndTime;
		}

		#endregion

		#region private methods

		async void SyncTemplatesAndAppointments()
		{
			try
			{
				await DownloadCalendarAppointmentsAsync().ConfigureAwait(true);
				await GetRiskTemplatesShortenedListAsync().ConfigureAwait(true);
			}
			catch (DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException)
			{
				// do nothing here as background sync of appointments and templates is not vital to operation of application
			}
		}

		void ShowLoginController (string message = "")
		{
			UIAlertView alert = new UIAlertView();
			alert.Title = "Authentication Error";
			alert.Message = "Your login on this device is no longer valid. \n (Have you logged in from another device?) \n Please login again.";
			alert.AddButton("Ok");
			alert.Show();

			alert.Clicked += (object senders, UIButtonEventArgs es) =>
			{
				LoginViewController controller = new LoginViewController(message, false);
				this.NavigationController.PushViewController(controller, true);
			};
		}

		// calls an async task method to create the appointment, should it pass a validation check first
		async void BtnDoneClicked(object Sender, EventArgs e)
		{
			if (this.CheckForConnectivityOnSave())
			{
				try
				{
					NetworkActivityView.ShowProgress("Saving New Appointment,");
					await CreateAppointment().ConfigureAwait(true);
					NetworkActivityView.DisplayCompletedStatus("Completed");
					this.NavigationController.PopViewController(true);
				}
				catch (DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException)
				{
					ShowLoginController("Service session has expired, please login.");
				}
				catch (Exception)
				{
					// if there is a problem during creation or if an exception is thrown it will end up here
					// in the business layer method for creation an exception is thrown should the service return false.
					NetworkActivityView.HideProgress();
					UIAlertView alert = new UIAlertView();
					alert.Message = "There was a problem creating the appointment";
					alert.AddButton("Ok");
					alert.Show();
				}
			}
		}

		// async task to call the business layer to create an appointment
		async Task CreateAppointment()
		{
			await Task.Run(() => BusinessLayer.AppointmentCreate(Client, commentText, startDate, endDate, houseNo, address1, address2, town, county, postCode, risk.Id)).ConfigureAwait(true);
		}

		// checks if client is in demo mode or logged in offline first
		// then checks if required fields are empty and if not returns true
		bool CheckForConnectivityOnSave()
		{
			UIAlertView alert = new UIAlertView();
			bool retVal = false;

			if (Client == null || string.IsNullOrWhiteSpace(address1) || startDate < DateTime.Now && endDate < DateTime.Now)
			{
				alert.Title = @"Not all neccessary information is entered.";
				alert.AddButton("Ok");
				alert.Message = @"Check that you have selected a client, ensured that the address line 1 of the site details is not empty and selected appointment start and end dates.";
				alert.Show();
			}
			else if (BL.Globals.DemoData || BL.Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOffline)
			{
				if (SaveOfflineAppt())
				{
					this.NavigationController.PopViewController(true);
				}
				else
				{
					alert.Title = @"Error saving appointment offline.";
					alert.AddButton("Ok");
					alert.Message = @"There was a problem saving the appointment offline.";
					alert.Show();
				}
			}
			else if (BL.Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOffline)
			{
				alert.Title = @"Offline saving not available.";
				alert.AddButton("Ok");
				alert.Message = @"You need to be logged in online in order to save the appointment presently.";
				alert.Show();
			}
			else
			{
				if (risk == null || risk.Id < 1)
				{
					risk = new RiskTemplate();
					risk.Id = 1;
				}
				retVal = true;
			}

			return retVal;
		}

		bool SaveOfflineAppt()
		{
			BL.Appointment appt = new BL.Appointment();
			appt.Status = AppointmentStatus.Pending;
			appt.Title = Client.Title;
			appt.FirstName = Client.Forename;
			appt.Surname = Client.Surname;
			appt.ClientName = clientName;
			appt.ClientStreetNo = houseNo;
			appt.ClientAddress1 = address1;
			appt.ClientAddress2 = address2;
			appt.ClientAddressCounty = county;
			appt.ClientAddressTown = town;
			appt.ClientAddressPostCode = postCode;
			appt.ClientEmail = Client.Email;
			appt.ClientMobile = Client.Mobile;
			appt.ClientTelephone = Client.Telephone;
			appt.Comments = commentText;
			appt.StartDateTime = startDate;
			appt.Duration = (float)(startDate - endDate).TotalMinutes;

			appt.Subject = string.Empty;
			appt.RiskTemplateId = 1;
			appt.SurveyorId = 1;
			appt.ClientAddressLocation = new GeoLocation();
			appt.ClientAddressLocation.Latitude = 51.179531;
			appt.ClientAddressLocation.Longitude = -1.826355;
			appt.UserCR = "1";
			appt.MasterCalendarId = DAL.Repository.Instance().AppointmentList().Count + 1;
			appt.Id = appt.MasterCalendarId;
			appt.QuoteNo = appt.MasterCalendarId;

			var RecsSaved =	DAL.Repository.Instance().AppointmentSave(appt);
			if (RecsSaved == 0)
			{
				return false;
			}

			// Create a Quote for the appointment
			var quote = new Quote()
			{
				AppointmentRecId = appt.RecId,
				Comments = commentText,
				QuoteId = appt.Id,
				StaffNumber = 1
			};

			quote.Assessment = new RiskAssessment(true);

			RecsSaved = DAL.Repository.Instance().QuoteSave(quote);

			if (RecsSaved == 0)
			{
				return false;
			}

			return true;
		}

		// support method to store information from the notes view controller
		void CommentTextInputSaved(int index, string savedText)
		{
			commentText = savedText;
		}

		// async method to download calendar appointments
		// makes call to business layer to download appointments
		// note that within the business layer a lock is implemented to avoid more than 1 calls to the SQLite db which raises an ecception
		async Task DownloadCalendarAppointmentsAsync()
		{
			if (!BL.Globals.DemoData && BL.Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOnline)
			{
				await Task.Run(() => BusinessLayer.AppointmentItemsListDownload()).ConfigureAwait(false);
			}
		}

		async Task GetRiskTemplatesShortenedListAsync()
		{
			if (!BL.Globals.DemoData && BL.Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOnline)
			{
				await Task.Run(() => BusinessLayer.RiskTemplatesShortenedListDownload()).ConfigureAwait(false);
			}
		}

		#endregion

		class NewAppointmentTableSource : UITableViewSource
		{

			#region Fields

			const string CELL_ID = "SubtitleCell";
			AppointmentNewViewController _Controller;

			#endregion

			#region Constructors

			public NewAppointmentTableSource()
				: base()
			{
			}

			public NewAppointmentTableSource(AppointmentNewViewController controller)
				: base()
			{
				_Controller = controller;
			}

			#endregion

			#region Actions

			public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
			{
				int section = indexPath.Section;

				if (section == SECTION_CLIENT)
				{
					_Controller.SelectOrAddNewClient();
				}
				else if (section == SECTION_JOBSITE)
				{
					_Controller.UpdateJobSite();
				}
				else if (section == SECTION_TIME)
				{
					_Controller.SelectDateAndTime();

				}
				else if (section == SECTION_COMMENTS)
				{
					_Controller.AddOrEditComments(indexPath.Row);

				}
				else if (section == SECTION_RISK)
				{
					_Controller.SelectRiskTemplate();

				}
			}

			#endregion

			#region Overrides

			public override nint NumberOfSections(UITableView tableView)
			{
				return 5;
			}

			public override string TitleForHeader(UITableView tableView, nint section)
			{
				if (section == SECTION_CLIENT)
				{
					return "Client";
				}
				else if (section == SECTION_JOBSITE)
				{
					return "Job Site";
				}
				else if (section == SECTION_TIME)
				{
					return "Date & Time";
				}
				else if (section == SECTION_COMMENTS)
				{
					return "Comments";
				}
				else if (section == SECTION_RISK)
				{
					return "Risk Template";
				}

				return string.Format("bad section{0}", section);
			}

			public override nint RowsInSection(UITableView tableview, nint section)
			{
				return 1;
			}

			public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
			{
				UITableViewCell Cell;
				Cell = tableView.DequeueReusableCell(CELL_ID);
				if (Cell == null)
				{
					Cell = new UITableViewCell(UITableViewCellStyle.Subtitle, CELL_ID);
					Cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
					Cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				}

				int section = indexPath.Section;

				if (section == SECTION_CLIENT)
				{
					Cell.TextLabel.Text = "Select Client";
					Cell.DetailTextLabel.Text = this._Controller.clientName;
				}
				else if (section == SECTION_JOBSITE)
				{
					Cell.TextLabel.Text = "Site Details";
					Cell.DetailTextLabel.Text = this._Controller.houseNo + " " + this._Controller.address1;
				}
				else if (section == SECTION_TIME)
				{
					Cell.TextLabel.Text = "Select Date and Time";
					if (_Controller.startDate == DateTime.MinValue || _Controller.endDate == DateTime.MinValue)
					{
						Cell.DetailTextLabel.Text = "View Calendar and Select Appointment Date";
					}
					else
					{
						Cell.DetailTextLabel.Text = "Start: " + _Controller.startDate.ToString("dd/MM/yyyy HH:mm") + " End: " + _Controller.endDate.ToString("dd/MM/yyyy HH:mm");
					}
				}
				else if (section == SECTION_COMMENTS)
				{
					Cell.TextLabel.Text = "Comments";
					Cell.DetailTextLabel.Text = "Add or Edit Comments for Appointment";
				}
				else if (section == SECTION_RISK)
				{
					Cell.TextLabel.Text = "Select Risk Assessment";

					if (_Controller.risk == null || string.IsNullOrWhiteSpace(_Controller.risk.Name))
					{
						Cell.DetailTextLabel.Text = "Choose risk assessment template for appointment.";
					}
					else {
						Cell.DetailTextLabel.Text = _Controller.risk.Name;
					}
				}

				return Cell;
			}

			#endregion
		}
	}
}

