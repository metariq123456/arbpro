// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/MapViewController/MapViewController.cs#10 $
// $Author: James $
// $Date: 2015/05/31 $
// $Change: 24700 $
// *********************************************************************
//
//
using System;
using CoreGraphics;
using System.Threading.Tasks;
using Foundation;
using UIKit;
using DGS.ArbPro.Mobile.BL;
using CoreLocation;
using MapKit;
using System.Drawing;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class MapViewController : UIViewController
	{
		#region Members

		protected class MapDelegate : MKMapViewDelegate
		{
			protected string annotationIdentifier = "ArbAnnotation";
			UIButton detailButton;
			// avoid GC
			public override MKAnnotationView GetViewForAnnotation (MKMapView mapView, IMKAnnotation annotation)
			{
				Console.WriteLine (string.Format ("annotation {0} {1}", annotation.GetType (), annotation.GetTitle ()));

				// try and dequeue the annotation view
				MKAnnotationView annotationView = mapView.DequeueReusableAnnotation (annotationIdentifier);   
				// if we couldn't dequeue one, create a new one
				if (annotationView == null)
					annotationView = new MKPinAnnotationView (annotation, annotationIdentifier);
				else // if we did dequeue one for reuse, assign the annotation to it
					annotationView.Annotation = annotation;

				// configure our annotation view properties
				annotationView.CanShowCallout = true;
				(annotationView as MKPinAnnotationView).AnimatesDrop = false;
				annotationView.Selected = true;


				if (annotation.GetType () == typeof(MKPointAnnotation)) {
					(annotationView as MKPinAnnotationView).PinColor = MKPinAnnotationColor.Red;
					// you can add an accessory view; in this case, a button on the right and an image on the left
					detailButton = UIButton.FromType (UIButtonType.DetailDisclosure);
					detailButton.TouchUpInside += (s, e) => {
						new UIAlertView ("Location", 
							(annotation as MKPointAnnotation).Title, null, "Close", null).Show ();
					};
					annotationView.RightCalloutAccessoryView = detailButton;
				} else {
					(annotationView as MKPinAnnotationView).PinColor = MKPinAnnotationColor.Purple;
				}
					
				annotationView.LeftCalloutAccessoryView = new UIImageView (UIImage.FromBundle ("Icon-Small.png"));
				return annotationView;
			}
		}

		private bool userLocationObtained;

		private class LocationManagerService
		{
			MKMapView mapView;
			CLLocationManager _LocationManager = null;

			public LocationManagerService (MKMapView mapViewSupplied)
			{
				mapView = mapViewSupplied;

				if (UIDevice.CurrentDevice.CheckSystemVersion (8, 0)) {
					_LocationManager = new CLLocationManager ();
					_LocationManager.AuthorizationChanged += LocationManagerOnAuthorizationChanged;
					_LocationManager.RequestWhenInUseAuthorization ();
				} else {
					mapView.ShowsUserLocation = true;
				}
			}

			private void LocationManagerOnAuthorizationChanged (object sender, CLAuthorizationChangedEventArgs args)
			{
				if (args.Status == CLAuthorizationStatus.AuthorizedWhenInUse ||
				    args.Status == CLAuthorizationStatus.AuthorizedAlways)
					mapView.ShowsUserLocation = true;
				else
					mapView.ShowsUserLocation = false;
			}

			private void GetCurrentLocation ()
			{
				_LocationManager.StartUpdatingLocation ();
				while (_LocationManager.Location == null)
					;
				_LocationManager.StopUpdatingLocation ();
				
			}
		}

		LocationManagerService _LocationService = null;

		#endregion

		#region public Properties

		public GeoLocation[] Locations { get; set; }

		public GeoLocation InitialCentre{ get; set; }

		public bool CaptureMode { get; set; }

		public bool CenterOnInitialLocation { get; set; }

		public bool ShowAddress { get; set; }

		public delegate void LocationCaptured (GeoLocation location);

		public event LocationCaptured LocationCapturedEvent;

		#endregion

		#region Constructor

		public MapViewController () : base ("MapViewController", null)
		{

		}

		#endregion

		#region Private Methods

		private void AddAnnotations ()
		{
			//Iterate through the list of locations passed to the view controller
			//and add an annotation for each.
			if (Locations != null) {
				foreach (GeoLocation location in Locations) {
					if (location != null) {
						AddAnnotation (location.Longitude, location.Latitude, location.Title);
					}
				}
			}
		}

		private void AddAnnotation (double longitude, double latitude, string title)
		{
			//Add a single annotation for a given long, lat and title

			var mapCenter = new CLLocationCoordinate2D (latitude, longitude);
			var mapRegion = MKCoordinateRegion.FromDistance (mapCenter, 200, 200);
			mapView.CenterCoordinate = mapCenter;
			mapView.Region = mapRegion;
			mapView.AddAnnotation (new MKPointAnnotation () { Title = title, Coordinate = mapCenter });
		}

		private void DisplayAddressLabel ()
		{
			if (ShowAddress && Locations != null && Locations.Length == 1 && Locations [0].Title != null) {
				labelAddress.Hidden = false;
				labelAddress.Text = Locations [0].Title;
			}
		}

		private void ShowCaptureButton ()
		{
			if (CaptureMode) {
				markerLabel.Hidden = false;
				UIBarButtonItem buttonItem = new UIBarButtonItem (UIBarButtonSystemItem.Save, Capture);
				NavigationItem.RightBarButtonItem = buttonItem;
			}
		}

		#endregion

		#region Actions

		partial void MapTypeChanged (Foundation.NSObject sender)
		{
			UISegmentedControl seg = (UISegmentedControl)sender;
			switch (seg.SelectedSegment) {
			case 0:
				mapView.MapType = MKMapType.Standard;
				break;
			case 1:
				mapView.MapType = MKMapType.Satellite;
				break;
			case 2:
				mapView.MapType = MKMapType.Hybrid;
				break;
			default:
				break;
			}
		}

		protected void Capture (object sender, EventArgs e)
		{
			//When the save button is pressed capture the location at the centre of the map
			//and pop the view controller, calling the delegate if set.

			GeoLocation location = new GeoLocation ();
			location.Latitude = mapView.CenterCoordinate.Latitude;
			location.Longitude = mapView.CenterCoordinate.Longitude;

			if (LocationCapturedEvent != null) {
				LocationCapturedEvent (location);
			}

			NavigationController.PopViewController (true);
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Set the navigation bar to opaque, so that the controls don't slide under it on iOS7
			this.NavigationController.NavigationBar.Translucent = false;


			_LocationService = new LocationManagerService (mapView);

			markerLabel.Hidden = true;
			labelAddress.Hidden = true;

			labelAddress.Text = "";

			var labelFrame = labelAddress.Frame;
			labelFrame.Size = new SizeF((float)labelFrame.Width,0);
			labelAddress.Frame = labelFrame;
		
			this.Title = "Location";
			mapView.MapType = MKMapType.Standard;

			if (CaptureMode) {
				mapView.DidUpdateUserLocation += (sender, e) => {
					if (mapView.UserLocation != null) {
						if (userLocationObtained == false) {

							CLLocationCoordinate2D coords = mapView.UserLocation.Coordinate;
							MKCoordinateSpan span = new MKCoordinateSpan (MilesToLatitudeDegrees (0.25), MilesToLongitudeDegrees (0.25, coords.Latitude));
							mapView.Region = new MKCoordinateRegion (coords, span);
							userLocationObtained = true;
						}
					}
				};
			}

			ShowCaptureButton ();

			AddAnnotations ();

			DisplayAddressLabel ();
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (BL.Globals.DemoData) {
				NavigationItem.Prompt = "Demonstration Mode";
			} else {
				NavigationItem.Prompt = null;
			}

		}

		#endregion

		/// <summary>Converts miles to latitude degrees</summary>
		public double MilesToLatitudeDegrees (double miles)
		{
			double earthRadius = 3960.0; // in miles
			double radiansToDegrees = 180.0 / Math.PI;
			return (miles / earthRadius) * radiansToDegrees;
		}

		/// <summary>Converts miles to longitudinal degrees at a specified latitude</summary>
		public double MilesToLongitudeDegrees (double miles, double atLatitude)
		{
			double earthRadius = 3960.0; // in miles
			double degreesToRadians = Math.PI / 180.0;
			double radiansToDegrees = 180.0 / Math.PI;
			// derive the earth's radius at that point in latitude
			double radiusAtLatitude = earthRadius * Math.Cos (atLatitude * degreesToRadians);
			return (miles / radiusAtLatitude) * radiansToDegrees;
		}
	}
}

