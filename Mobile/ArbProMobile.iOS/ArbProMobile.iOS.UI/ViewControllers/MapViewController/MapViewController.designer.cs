// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("MapViewController")]
    partial class MapViewController
    {
        [Outlet]
        UIKit.UILabel labelAddress { get; set; }


        [Outlet]
        MapKit.MKMapView mapView { get; set; }


        [Outlet]
        UIKit.UILabel markerLabel { get; set; }


        [Action ("MapTypeChanged:")]
        partial void MapTypeChanged (Foundation.NSObject sender);

        void ReleaseDesignerOutlets ()
        {
            if (labelAddress != null) {
                labelAddress.Dispose ();
                labelAddress = null;
            }

            if (mapView != null) {
                mapView.Dispose ();
                mapView = null;
            }

            if (markerLabel != null) {
                markerLabel.Dispose ();
                markerLabel = null;
            }
        }
    }
}