﻿// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev_ivan/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/CalendarViewController.cs#2 $
// $Author: ivan $
// $Date: 2016/08/30 $
// $Change: 26845 $
// *********************************************************************
using System;
using System.Collections.Generic;
using Foundation;
using Syncfusion.SfSchedule.iOS;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class AppointmentCalendarViewController : UIViewController
    {
		UIBarButtonItem _BtnDone;
        private SFSchedule Schedule;
		DateTime startDate = DateTime.Now;
		DateTime endDate = DateTime.Now;
		DateTime minDate = DateTime.Now;

		#region Constructors
		/// <summary>
		/// Takes in a start and end date for appointments - if they are not min values it displays those dates and times, else it defaults to current date and time.
		/// Initializes the SyncFusion Schedule and calendar and populates it with appointments.
		/// </summary>
		/// <param name="dateStart">Date start.</param>
		/// <param name="dateEnd">Date end.</param>
        public AppointmentCalendarViewController (BL.CalendarItem item) : base ("AppointmentCalendarViewController", null)
        {
			this.Title = @"Calendar";

			TimeSpan m = TimeSpan.FromMinutes(30);
			if (item != null)
			{
				startDate = item.StartTime;
				endDate = item.EndTime;

				DAL.Repository.Instance().CalendarItemSave(item);
			}
			else
			{
				// Date start and end are not set thereforre round up the current date and time to the nearest 30 minutes
				// because that is the interval we are operating on. 
				// Since both dates are datetime.now do the round up for only one and set the other one to be equal to it
				startDate = new DateTime(((startDate.Ticks + m.Ticks - 1) / m.Ticks) * m.Ticks);
				endDate = startDate.AddMinutes(30);
			}

			minDate = new DateTime(((minDate.Ticks + m.Ticks - 1) / m.Ticks) * m.Ticks);

			InitializeSchedule();

			Schedule.Appointments = ScheduleAppointmentFactory.ScheduleAppointmentsFromItems (DAL.Repository.Instance ().CalendarItemList ());

			_BtnDone = new UIBarButtonItem("Done", UIBarButtonItemStyle.Plain, this.BtnDoneClicked);
			this.NavigationItem.RightBarButtonItem = _BtnDone;
        }

		#endregion

		#region overrides

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear (animated);

			if (BL.Globals.DemoData)
			{
				NavigationItem.Prompt = "Demonstration Mode";
			}
			else
			{
				NavigationItem.Prompt = null;
			}

            this.NavigationController.NavigationBar.Translucent = false;
            segView.ValueChanged += SegView_ValueChanged;
			segView.SelectedSegment = 2;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear (animated);

			StartAndEndDateTimeInitialize();
            Schedule.Frame = this.ContentView.Frame;
			this.View.AddSubview (Schedule);
			this.View.AddSubview(lblApptSummary);
        }

        public override void DidReceiveMemoryWarning ()
        {
            base.DidReceiveMemoryWarning ();
            // Release any cached data, images, etc that aren't in use.
        }

		// needed as schedule's constraints do not update along with the content view
		public override void DidRotate(UIKit.UIInterfaceOrientation fromInterfaceOrientation)
		{
			Schedule.Frame = this.ContentView.Frame;
			base.DidRotate(fromInterfaceOrientation);
		}

		#endregion

		#region private methods

		// changes calendar view based on selected segment
        void SegView_ValueChanged (object sender, EventArgs e)
        {
            switch (segView.SelectedSegment) {
            case 0:
                Schedule.ScheduleView = SFScheduleView.SFScheduleViewDay;
					this.lblApptSummary.Hidden = true;
                break;
            case 1:
                Schedule.ScheduleView = SFScheduleView.SFScheduleViewWeek;
					this.lblApptSummary.Hidden = true;
                break;
            default:
                Schedule.ScheduleView = SFScheduleView.SFScheduleViewMonth;
					this.lblApptSummary.Hidden = true;
                break;
            }
        }

		// updates calendar's currently selected date, week or month (depends on calendar view)
		// checks if start date is higher than or equal to end date and if it is it updates end date
		void StartDate_ValueChanged(object sender, EventArgs e)
		{
			Schedule.MoveToDate(StartDate.Date);
			if (StartDate.Date.Compare(EndDate.Date.AddSeconds(-1800)) == NSComparisonResult.Descending)
			{
				EndDate.Date = StartDate.Date.AddSeconds(1800);
			}
		}

		// updates calendar's currently selected date, week or month (depends on calendar view)
		// checks if end date is lower than or equal to start date and if it is it updates start date
		void EndDate_ValueChanged(object sender, EventArgs e)
		{
			Schedule.MoveToDate(EndDate.Date);
			if (EndDate.Date.Compare(StartDate.Date.AddSeconds(1800)) == NSComparisonResult.Ascending)
			{
				StartDate.Date = EndDate.Date.AddSeconds(-1800);
			}
		}

		// saves currently selected start and end dates from date time picker and passes them back to appointmentnewviewcontroller
		void BtnDoneClicked(object Sender, EventArgs e)
		{
			BL.CalendarItem itm = new BL.CalendarItem();
			itm.StartTime = NSDateToDateTime(StartDate.Date);
			itm.EndTime = NSDateToDateTime(EndDate.Date);
			itm.Summary = "Pending Mobile Quote Appointment";
			itm.AllDay = false;
			itm.ItemType = Service.CalendarItemType.Pending;


			this.NavigationController.PopViewController(true);
			AppointmentNewViewController viewCon = (AppointmentNewViewController)NavigationController.TopViewController;
			viewCon.GetDateAndTimeForAppt(itm);
		}

		// converts date time object to ns date
		static NSDate DateTimeToNSDate(DateTime date)
		{
			DateTime reference = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(2001, 1, 1, 0, 0, 0));

			return NSDate.FromTimeIntervalSinceReferenceDate((date - reference).TotalSeconds);
		}

		// converts ns date object to date time and sets the converted time to local time
		static DateTime NSDateToDateTime(NSDate date)
		{
			DateTime reference = TimeZone.CurrentTimeZone.ToUniversalTime(new DateTime(2001, 1, 1, 0, 0, 0));

			return (reference.AddSeconds(date.SecondsSinceReferenceDate)).ToLocalTime();
		}

		// Initializes the schedule
		void InitializeSchedule()
		{
			Schedule = new SFSchedule();
			var scheduleDelegate = new ScheduleDelegate();
			scheduleDelegate.Parent = this;
			Schedule.Delegate = scheduleDelegate;
			Schedule.Calendar.FirstWeekDay = 2;
			Schedule.ScheduleView = SFScheduleView.SFScheduleViewMonth;

			WeekViewSettings weekSettings = new WeekViewSettings();
			weekSettings.WorkStartHour = 8;
			weekSettings.WorkEndHour = 18;
			weekSettings.ShowAllDay = true;

			DayViewSettings daySettings = new DayViewSettings();
			daySettings.WorkStartHour = 8;
			daySettings.WorkEndHour = 18;
			daySettings.ShowAllDay = true;

			DayLabelSettings labelSettings = new DayLabelSettings();
			labelSettings.TimeLabelSize = 8;
			labelSettings.DateLabelSize = 15;
			labelSettings.DayLabelSize = 15;
			labelSettings.DateLabelFormat = (NSString)"dd/MM/yy";
			labelSettings.DayLabelFormat = (NSString)" - EEEE";
			labelSettings.TimeLabelFormat = (NSString)"hh a";

			daySettings.LabelSettings = labelSettings;
			Schedule.DayViewSettings = daySettings;
			Schedule.WeekViewSettings = weekSettings;
		}

		// Sets up start and end date pickers
		void StartAndEndDateTimeInitialize()
		{
			StartDate.Date = DateTimeToNSDate(startDate.ToUniversalTime());
			EndDate.Date = DateTimeToNSDate(endDate.ToUniversalTime());

			StartDate.MinimumDate = DateTimeToNSDate(minDate.ToUniversalTime());
			EndDate.MinimumDate = DateTimeToNSDate(minDate.AddMinutes(30).ToUniversalTime());

			StartDate.ValueChanged += StartDate_ValueChanged;
			EndDate.ValueChanged += EndDate_ValueChanged;
		}

		#endregion

		public class ScheduleDelegate : SFScheduleDelegate
        {
			public AppointmentCalendarViewController Parent
			{
				get;
				set;
			}

			// override in order to go to day view on selected date tap
			public override void didSelectDate(SFSchedule schedule, NSDate selectedDate, NSArray appointments)
			{
				bool isMonthView = schedule.ScheduleView == SFScheduleView.SFScheduleViewMonth;

				this.Parent.lblApptSummary.Hidden = true;

				schedule.ScheduleView = SFScheduleView.SFScheduleViewDay;
				schedule.MoveToDate(selectedDate);
				this.Parent.segView.SelectedSegment = 0;

				if (selectedDate.Compare(NSDate.Now) == NSComparisonResult.Ascending)
				{
					this.Parent.StartDate.Date = this.Parent.StartDate.MinimumDate;
					this.Parent.EndDate.Date = this.Parent.EndDate.MinimumDate;
				}
				else
				{
					this.Parent.StartDate.Date = selectedDate;
					this.Parent.EndDate.Date = selectedDate.AddSeconds(1800);

					if (isMonthView)
					{
						this.Parent.StartDate.Date = this.Parent.StartDate.Date.AddSeconds(28800);
						this.Parent.EndDate.Date = this.Parent.EndDate.Date.AddSeconds(28800);
					}
				}

				if (!isMonthView && appointments != null && appointments.Count > 0)
				{
					foreach (var itm in NSArray.FromArray<ScheduleAppointment>(appointments))
					{
						string a = itm.Subject;

						this.Parent.lblApptSummary.Text = a;
					}

					this.Parent.lblApptSummary.Hidden = false;
				}
			}
        }
    }

}


