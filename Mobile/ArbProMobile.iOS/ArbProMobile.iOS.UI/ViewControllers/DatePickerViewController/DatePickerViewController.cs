// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/DatePickerViewController/DatePickerViewController.cs#9 $
// $Author: Jon $
// $Date: 2015/05/27 $
// $Change: 24685 $
// *********************************************************************
using System;
using Foundation;
using UIKit;
using Microsec.ExtensionMethods;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class DatePickerViewController : UIViewController
    {
		#region Delegates
		public delegate void DidSelectDateDelegate(int cellIndex,NSDate selectedDate);

        public event DidSelectDateDelegate DidSelectDateEvent;
		#endregion
		
		#region Members
        int _CellIndex;
        UIBarButtonItem __NavButtonDone;
		#endregion

        #region Properties
        public string Prompt { get; set; }

        public DateTime? InitialDate { get; set; }
        #endregion
	
		#region Constructors
        public DatePickerViewController(int cellIndex, string prompt) : base ("DatePickerViewController", null)
        {
            _CellIndex = cellIndex;
            Prompt = prompt;

            SetupNavigationBar();
        }
		#endregion
		
		#region Overrides
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
			
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
			
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            if (BL.Globals.DemoData)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            }
            else
            {
                NavigationItem.Prompt = null;
            }

            lblInstructions.Text = Prompt;
            if (InitialDate != null)
            {
				viewDate.Date = InitialDate.DateTimeToNSDate();
            }

			txtSelectedDate.Text = string.Format("{0:dddd dd/MM/yyyy}", viewDate.Date.NSDateToDateTime());



        }
		#endregion
		
		#region Actions
        void ButtonDoneClicked(Object sender, EventArgs e)
        {
            if (DidSelectDateEvent != null)
                DidSelectDateEvent(_CellIndex, viewDate.Date);
			
            if (this.NavigationController != null)
                this.NavigationController.PopViewController(true);
        }

        partial void SelectedDateChanged(NSObject sender)
        {
			NSDate selectedDate = viewDate.Date;
			txtSelectedDate.Text = string.Format("{0:dddd dd/MM/yyyy}", viewDate.Date.NSDateToDateTime());

        }
		#endregion
		
		#region Private Methods
        void SetupNavigationBar()
        {
            __NavButtonDone = new UIBarButtonItem(@"Done", UIBarButtonItemStyle.Done, ButtonDoneClicked);
            this.NavigationItem.RightBarButtonItem = __NavButtonDone;
        }
		#endregion
    }
}

