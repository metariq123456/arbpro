// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("DatePickerViewController")]
    partial class DatePickerViewController
    {
        [Outlet]
        UIKit.UIDatePicker viewDate { get; set; }


        [Outlet]
        UIKit.UILabel lblInstructions { get; set; }


        [Outlet]
        UIKit.UITextField txtSelectedDate { get; set; }


        [Action ("SelectedDateChanged:")]
        partial void SelectedDateChanged (Foundation.NSObject sender);

        void ReleaseDesignerOutlets ()
        {
            if (lblInstructions != null) {
                lblInstructions.Dispose ();
                lblInstructions = null;
            }

            if (txtSelectedDate != null) {
                txtSelectedDate.Dispose ();
                txtSelectedDate = null;
            }

            if (viewDate != null) {
                viewDate.Dispose ();
                viewDate = null;
            }
        }
    }
}