// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	[Register ("TextEntryViewController")]
	partial class TextEntryViewController
	{
		[Outlet]
		MonoTouch.UIKit.UITableView viewTable { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (viewTable != null) {
				viewTable.Dispose ();
				viewTable = null;
			}
		}
	}
}
