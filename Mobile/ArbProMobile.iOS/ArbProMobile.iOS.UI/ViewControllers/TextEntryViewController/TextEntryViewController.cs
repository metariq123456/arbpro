using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
//    public enum TextLookupSource
//    {
//        None
//,
//        ItemType
//,
//        WorkDescription
//,
//        Tool
//,
//    }

	public partial class TextEntryViewController : UIViewController
	{
//		public TextEntryViewController () : base ("TextEntryViewController", null)
//		{
//            KeyboardType = UIKeyboardType.Default;
//        }
//		#endregion
//
//		#region Delegates
//        public delegate void DidInputTextDelegate(int Index,string Text);
//
//        public event DidInputTextDelegate DidInputTextEvent;
//		#endregion
//
//		#region Private Properties
//        UIBarButtonItem __NavButtonDone;
//		#endregion
//
//        #region Private Methods
//        private void CreateLookupTable()
//        {
//
//            // Note that the location here must match the xib layout
//            LookupValues = new UITableView(new RectangleF(0,60,320,120), UITableViewStyle.Plain);
//            LookupValues.Source = new LookupTableViewSource(this, LookupSource);
//
//            LookupValues.ScrollEnabled = true;
//            LookupValues.Hidden = true;
//            this.Add(LookupValues);
//
//        }
//        #endregion
//
//		#region Public Properties
//        public int Index { get; set; }
//
//        public string CurrentText { get; set; }
//
//        public UIKeyboardType KeyboardType { get; set; }
//
//        public string ViewTitle { get; set; }
//
//        public string InputLabel { get; set; }
//
//        public TextLookupSource LookupSource { get; set; }
//
//        public UITableView LookupValues;
//		#endregion
//
//		#region Actions
//
//        partial void textChanged(NSObject sender)
//        {
//            (LookupValues.Source as LookupTableViewSource).FilterText = textInput.Text;
//        }
//
//        public override void ViewDidLoad()
//        {
//            base.ViewDidLoad();
//            Title = ViewTitle;
//            labelInput.Text = InputLabel;
//            textInput.Text = CurrentText;
//            textInput.KeyboardType = KeyboardType;
//            textInput.BecomeFirstResponder();
//
//            if (LookupSource !=TextLookupSource.None)
//            {
//                textInput.Delegate = new MyTextViewDelegate(this);
//            }
//
//            __NavButtonDone = new UIBarButtonItem(@"Done", UIBarButtonItemStyle.Bordered, ButtonDoneClicked);
//            this.NavigationItem.RightBarButtonItem = __NavButtonDone;
//
//        }
//
//        public override void ViewWillAppear(bool animated)
//        {
//            base.ViewWillAppear(animated);
//
//            if (!BL.Globals.LoggedIn)
//            {
//                NavigationItem.Prompt = "Demonstration Mode";
//            }
//            else
//            {
//                NavigationItem.Prompt = null;
//            }
//
//            if (LookupSource != null)
//            {
//                CreateLookupTable();
//            }
//        }
//
//        void ButtonDoneClicked(Object sender, EventArgs e)
//        {
//            if (DidInputTextEvent != null)
//                DidInputTextEvent(Index, textInput.Text);
//
//            if (this.NavigationController != null)
//                this.NavigationController.PopViewControllerAnimated(true);
//        }
//		#endregion
//
//        // A delegate for the textView, which allows us to detect - via the Should ChangeCharacters 
//        // event - when to pop up the lookup TableView.
//        class MyTextViewDelegate : UITextFieldDelegate
//        {
//
//            TextInputViewController _Controller;
//
//            public MyTextViewDelegate(
//
//                TextInputViewController controller) : base()
//            {
//                _Controller = controller;
//            }
//            // This event is raised when the OS is asking whether characters should be changed while typing.
//            // We use it to determine whether the textbox is being typed into, and therefore whether to show the lookup tableview.
//            public override bool ShouldChangeCharacters(UITextField textField, NSRange range, string replacementString)
//            {
//                _Controller.LookupValues.Hidden = false;
//                return true;
//            }
//        }
//        // The source for the lookup tableview.
//        class LookupTableViewSource: UITableViewSource
//        {
//            TextInputViewController _Controller;
//            // All the items in the list
//            List<string> _FullItems;
//            // The items once a filter has been applied
//            List<string> _FilteredItems;
//            // The text to which to filter the list
//            public string FilterText
//            {
//                set
//                {
//                    // Clear out the filtered list, and then only add those items which have the filter in their strings, comparing 
//                    // case-insensitively
//                    _FilteredItems.Clear();
//                    foreach (var s in _FullItems)
//                    {
//                        if (value == "" || s.ToUpper().Contains(value.ToUpper()))
//                            _FilteredItems.Add(s);
//                    }
//                    // Refresh the lookup tableview
//                    _Controller.LookupValues.ReloadData();
//
//                }
//            }
//            #region Constructors
//            public LookupTableViewSource(TextInputViewController controller, TextLookupSource sourceType)
//            {
//                _Controller = controller;
//                _FullItems = new List<string>();
//                _FilteredItems = new List<string>();
//
//                // Initialise the item lists with the data from the database.
//                switch (sourceType)
//                {
//                    case TextLookupSource.ItemType:
//                        var ItemTypes = DAL.Repository.Instance().ItemTypeList();
//                        foreach (var Item in ItemTypes)
//                        {
//                            _FullItems.Add(Item.CommonName);
//                            _FilteredItems.Add(Item.CommonName);
//                        }
//                        break;
//                    case TextLookupSource.Tool:
//                        var Tools = DAL.Repository.Instance().ToolList();
//                        foreach (var Tool in Tools)
//                        {
//                            _FullItems.Add(Tool.Name);
//                            _FilteredItems.Add(Tool.Name);
//                        }
//                        break;
//                    case TextLookupSource.WorkDescription:
//                        var Descriptions = DAL.Repository.Instance().ItemDescriptionList();
//                        foreach (var Desc in Descriptions)
//                        {
//                            _FullItems.Add(Desc.Description);
//                            _FilteredItems.Add(Desc.Description);
//                        }
//                        break;
//
//                    default:
//                        break;
//                }
//
//            }
//            #endregion
//
//
//            #region Overrides
//            public override int NumberOfSections(UITableView tableView)
//            {
//                return 1;
//            }
//
//            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
//            {
//                _Controller.textInput.Text = _FilteredItems[indexPath.Row];
//            }
//
//            public override string TitleForHeader(UITableView tableView, int section)
//            {
//                return "";
//            }
//
//            public override int RowsInSection(UITableView tableview, int section)
//            {
//                return _FilteredItems.Count;
//            }
//
//            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
//            {
//                string CELL_REF = "LookupListCell";
//                UITableViewCell cell = tableView.DequeueReusableCell(CELL_REF);
//                if (cell == null)
//                {
//                    cell = new UITableViewCell(UITableViewCellStyle.Default, CELL_REF);
//                }
//                cell.TextLabel.Text = _FilteredItems[indexPath.Row];
//
//                cell.SelectionStyle = UITableViewCellSelectionStyle.None;
//                return cell;
//            }
//            #endregion
//
//        }
    }
}

