using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class RiskCriterionTableCell : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("RiskCriterionTableCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("RiskCriterionTableCell");

		public RiskCriterionTableCell (IntPtr handle) : base (handle)
		{
		}

		public static RiskCriterionTableCell Create ()
		{
			return (RiskCriterionTableCell)Nib.Instantiate (null, null) [0];
		}
	}
}

