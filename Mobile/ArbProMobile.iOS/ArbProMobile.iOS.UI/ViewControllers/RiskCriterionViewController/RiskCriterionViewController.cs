using System;
using CoreGraphics;
using Foundation;
using UIKit;
using BL = DGS.ArbPro.Mobile;


namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class RiskCriterionViewController : UIViewController
    {
        #region Delegates

        public delegate void DoneButtonDelegate (int IndexOutOfRangeException, BL.AssessmentSection Criterion);

        #endregion

        #region Members

        private BL.AssessmentSection _Criterion = null;

        #endregion

        #region TableViewSource

        class CriterionTableSource:UITableViewSource
        {
            #region Fields

            const int SECTION_CRITERIA = 0;
            const int SECTION_NOTES = 1;
            const string CELL_ID = "tblCell";
            RiskCriterionViewController _Controller;
	      
            UIImage imgYes;
            UIImage imgNo;
            UIImage imgQuestion;

            #endregion

            #region Constructors

            public CriterionTableSource() : base()
            {
                imgYes = UIImage.FromBundle("yes.png");
                imgNo = UIImage.FromBundle("no.png");
                imgQuestion = UIImage.FromBundle("question.png");
            }

            public CriterionTableSource(RiskCriterionViewController controller)
            {
                _Controller = controller;
                imgYes = UIImage.FromBundle("yes.png");
                imgNo = UIImage.FromBundle("no.png");
                imgQuestion = UIImage.FromBundle("question.png");
            }

            #endregion

            #region Overrides

            public override nint NumberOfSections(UITableView tableView)
            {
                return 2;
            }

            public override string TitleForHeader(UITableView tableView, nint section)
            {
                switch (section)
                {
                case SECTION_CRITERIA:
                    return _Controller.AssessmentCriterion.Info;
                case SECTION_NOTES:
                    return "Additional Details";
                default:
                    return string.Format("bad section{0}", section);
                }
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                switch (section)
                {
                case SECTION_CRITERIA:
                    return _Controller.AssessmentCriterion.AssessmentItems.Count;
                case SECTION_NOTES:
                    return 1;
                default:
                    return 0;
                }
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                UITableViewCell Cell;
                Cell = tableView.DequeueReusableCell(CELL_ID);
		                  
                if (Cell == null)
                {
                    Cell = new UITableViewCell(UITableViewCellStyle.Subtitle, CELL_ID);
                    Cell.SelectionStyle = UITableViewCellSelectionStyle.None;
                }
		                  
                switch (indexPath.Section)
                {
                case SECTION_CRITERIA:
                    var itm = _Controller.AssessmentCriterion.AssessmentItems [indexPath.Row];
                    Cell.TextLabel.Text = itm.Title;
                    string label = itm.RefCode != null && itm.RefCode.Length > 0 ? string.Format("Ref: {0}", itm.RefCode):"Reference"; 
                    Cell.DetailTextLabel.Text = label;

                    UIImage img = imgQuestion;
                    if (itm.ControlRequired != BL.AssessmentItem.ControlRequiredType.Unknown)
                    {
                        img = itm.ControlRequired == BL.AssessmentItem.ControlRequiredType.Yes ? imgYes:imgNo;
                    }
                    Cell.AccessoryView = new UIImageView(img);
                    break;
                case SECTION_NOTES:
                    Cell.SelectionStyle = UITableViewCellSelectionStyle.None;
                    Cell.AccessoryView = null;
                    Cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
					
                    Cell.TextLabel.Text = "Hazards / Actions";
                    Cell.DetailTextLabel.Text = _Controller.AssessmentCriterion.AdditionalDetail != null && _Controller.AssessmentCriterion.AdditionalDetail.Length > 0 ? _Controller.AssessmentCriterion.AdditionalDetail:"Add";
                    break;
                default:
                    break;
                }
                return Cell;
            }

            #endregion

            #region Private Methods

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                if (indexPath.Section == SECTION_CRITERIA)
                {
                    if (_Controller.Enabled)
                    {
                        _Controller.AssessmentCriterion.AssessmentItems [indexPath.Row].toggleControlRequired();
                        _Controller.Assessment.IsDirty = true;
                        tableView.ReloadData();
                    }
                } else if (indexPath.Section == SECTION_NOTES)
                {
                    if (_Controller.Enabled)
                    {
                        _Controller.DisplayNotesField(indexPath.Row);
                    }
                }
            }

            #endregion
        }

        #endregion

        #region Properties

        public BL.RiskAssessment Assessment { get; set; }

        public int CriterionIndex { get; set; }

        public BL.AssessmentSection AssessmentCriterion
        {
            get
            {
                if (_Criterion == null)
                {
                    _Criterion = Assessment.Criterion [CriterionIndex];
                }
                return _Criterion;
            }
        }

        public bool Enabled
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public RiskCriterionViewController(BL.RiskAssessment assessment, int criterionIndex) : base("RiskCriterionViewController", null)
        {
            this.Assessment = assessment;
            this.CriterionIndex = criterionIndex;
        }

        #endregion

        #region Overrides

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Set the navigation bar to opaque, so that the controls don't slide under it on iOS7
            this.NavigationController.NavigationBar.Translucent = false;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
			
            //Set the title of the view
            this.Title = this.AssessmentCriterion.Title;
			
            if (BL.Globals.DemoData)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            } else
            {
                NavigationItem.Prompt = null;
            }
            viewTable.Source = new CriterionTableSource(this);
        }

        #endregion

        private void DisplayNotesField(int index)
        {
            NotesInputViewController controller = new NotesInputViewController();
            controller.Index = index;
            controller.Title = this.AssessmentCriterion.Title;
            controller.ViewTitle = "Additional Hazards and Actions Required";
            controller.CurrentText = this.AssessmentCriterion.AdditionalDetail;
            controller.KeyboardType = UIKeyboardType.Default;
            controller.DidInputTextEvent += NotesEntered;
            this.NavigationController.PushViewController(controller, true);
        }

        private void NotesEntered(int index, string text)
        {
            this.AssessmentCriterion.AdditionalDetail = text;
            this.Assessment.IsDirty = true;
            this.viewTable.ReloadData();
        }
    }
}

