using System;
using CoreGraphics;
using Foundation;
using UIKit;
using System.Collections.Generic;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class TextPickerViewController : UIViewController
	{
		#region private properties
		private List<string> FilteredItems { get; set; }

		private UIBarButtonItem NavButtonDone { get; set; }
		#endregion
		#region view delegates
		public delegate void DidPickTextDelegate (int Index, string Text);

		public event DidPickTextDelegate DidPickTextEvent;
		#endregion
		#region constructor
		public TextPickerViewController () : base ("TextPickerViewController", null)
		{
			FilteredItems = new List<string> ();
		}
		#endregion
		#region public properties
		public List<string> LookupItems { get; set; }

		public string DefaultValue { get; set; }

		public int Index { get; set; }

		public string ViewTitle { get; set; }

		public string InputLabel { get; set; }
		// The maximum length of user input into the search text view.
		public int MaxLength { get; set; }

		public string SearchText {
			get { 
				string text = searchBar.Text;
				if (text == null)
					text = "";

				return text;
			}
		}
		#endregion
		#region public methods
		public void FilterItems ()
		{
			string lookupText = SearchText.ToUpper ();

			FilteredItems.Clear ();
			foreach (var item in LookupItems) {
				if (lookupText == "" || item.ToUpper ().Contains (lookupText))
					FilteredItems.Add (item);
			}

			NavButtonDone.Enabled = SearchText.Length > 0;

			table.ReloadData ();
		}

		public void ItemSelected (string text)
		{
			searchBar.Text = text;
			NavButtonDone.Enabled = true;
		}
		#endregion
		#region Actions
		public void ButtonDoneClicked (Object sender, EventArgs e)
		{
			if (DidPickTextEvent != null)
				DidPickTextEvent (Index, SearchText);
			NavigationController.PopViewController (true);
		}
		#endregion
		#region View Overrides
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			// Set the navigation bar to opaque, so that the controls don't slide under it on iOS7
			this.NavigationController.NavigationBar.Translucent = false;

			NavButtonDone = new UIBarButtonItem (@"Done", UIBarButtonItemStyle.Done, ButtonDoneClicked);
			NavigationItem.RightBarButtonItem = NavButtonDone;

			Title = ViewTitle;

			searchBar.Delegate = new SearchBarDelegate (this);
	  		//searchBar.TextChanged = searchBar.Delegate.TextChanged;
			//searchBar.SearchButtonClicked = searchBar.Delegate.SearchButtonClicked;
			table.Source = new LookupTableViewSource (this);
			FilterItems ();

			foreach (UIView view in searchBar.Subviews) {
				if (view.GetType () == typeof(UITextField)) {
					UITextField field = (UITextField)view; 
					field.ClearButtonMode = UITextFieldViewMode.Never;
					field.ReturnKeyType = UIReturnKeyType.Done;
				}
			}

			// Limit the amount of text the user can type in to the search bar text. This is because the user can
			// type into here and press 'Done' with the result that the text entered here is stored in the server database; if it's too
			// long, it fails with a truncation error on the server.
			/*searchBar.ShouldChangeTextInRange += (sBar, range, replacement) =>
			{
				// Note we need to cater for an empty replacement - i.e. the delete key being pressed.
				if (replacement != "" && MaxLength != null && MaxLength != 0) {
					return sBar.Text.Length < MaxLength;
				} else {
					return true;
				}
			};*/
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (BL.Globals.DemoData)
				NavigationItem.Prompt = "Demonstration Mode";
			else
				NavigationItem.Prompt = null;
		}
		#endregion
		#region control delegates
		class SearchBarDelegate : UISearchBarDelegate
		{

			TextPickerViewController _controller;

			public SearchBarDelegate (TextPickerViewController controller) : base()
			{
				_controller = controller;
			}

			public override void CancelButtonClicked (UISearchBar searchBar)
			{
				searchBar.Text = "";
				_controller.FilterItems ();
			}

			public override void TextChanged (UISearchBar searchBar, string searchText)
			{
				_controller.FilterItems ();
			}

			public override void ListButtonClicked (UISearchBar searchBar)
			{
				searchBar.ResignFirstResponder ();
			}

			public override void SearchButtonClicked (UISearchBar searchBar)
			{
				_controller.ButtonDoneClicked (null, null);
			}

			public override bool ShouldChangeTextInRange(UISearchBar searchBar, NSRange range, string text)
			{
				// NOTE: Don't call the base implementation on a Model class
				// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events 
				// Note we need to cater for an empty replacement - i.e. the delete key being pressed.
				if (text != "" && _controller.MaxLength != 0) {
					return searchBar.Text.Length < _controller.MaxLength;
				} else {
					return true;
				}
			}
		}

		class LookupTableViewSource: UITableViewSource
		{
			private TextPickerViewController _controller;
			#region Constructors
			public LookupTableViewSource (TextPickerViewController controller)
			{
				_controller = controller;
			}
			#endregion
			#region Overrides
			public override nint NumberOfSections (UITableView tableView)
			{
				return 1;
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				int count = _controller.FilteredItems.Count; 
				if (count == 0 && _controller.SearchText.Length > 0)
					count = 1;
				return count;
			}

			public override string TitleForHeader (UITableView tableView, nint section)
			{
				return _controller.InputLabel;
			}

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				if (_controller.FilteredItems.Count > 0)
					_controller.ItemSelected (_controller.FilteredItems [indexPath.Row]);
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				string CELL_REF = "LookupListCell";
				UITableViewCell cell = tableView.DequeueReusableCell (CELL_REF);
				if (cell == null) {
					cell = new UITableViewCell (UITableViewCellStyle.Subtitle, CELL_REF);
				}

				cell.Accessory = UITableViewCellAccessory.None;
				cell.TextLabel.TextColor = UIColor.Black;

				string text = "";
				string detailText = "";
				if (_controller.FilteredItems.Count == 0) {
					cell.TextLabel.TextColor = UIColor.Gray;
					text = "No results found";
				} else {
					cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
					text = _controller.FilteredItems [indexPath.Row];
					int maxLength = 28;
					if (_controller.View.Frame.Size.Width > 320)
						maxLength = 50;
					if (text.Length > maxLength) {
						string[] textParts = text.Split (null);
						text = "";
						bool carryOn = true;
						int index = 0;
						while (carryOn == true) {
							if (index < textParts.Length) {
								if ((text.Length + textParts [index].Length < maxLength) && detailText.Length == 0) {
									text += " " + textParts [index];
								} else {
									detailText += " " + textParts [index];
								}
								index ++;
							} else {
								carryOn = false;
							}
						}
					}
					cell.Accessory = UITableViewCellAccessory.None;
				}

				cell.SelectionStyle = UITableViewCellSelectionStyle.Default;
				var i = text.IndexOf(":") + 1;
				cell.TextLabel.Text = text.Substring(i, text.Length - i);
				cell.DetailTextLabel.Text = detailText;
				return cell;
			}
			#endregion
		}
		#endregion
	}
}

