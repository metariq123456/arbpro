// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/AboutViewController/AboutViewController.cs#11 $
// $Author: ivan $
// $Date: 2016/07/13 $
// $Change: 26657 $
// *********************************************************************
//
//
using System;
using CoreGraphics;
using Foundation;
using UIKit;
using MessageUI;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class AboutViewController : UIViewController
	{
		#region Members
		MFMailComposeViewController _mailController;
		#endregion
		
		#region Constructors
		public AboutViewController () : base ("AboutViewController", null)
		{
		}
		#endregion
		
		#region Overrides
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			if (BL.Globals.DemoData)
			{
				NavigationItem.Prompt = "Demonstration Mode";
			}
			else
			{
				NavigationItem.Prompt = null;
			}

		}
		
		public override void ViewWillLayoutSubviews()
        {
			string title;
            base.ViewWillLayoutSubviews();
            title = string.Format("ArbPro V{0}",NSBundle.MainBundle.InfoDictionary["CFBundleVersion"].ToString());
			if (ArbPro.Mobile.BL.Settings.BuildType == DGS.ArbPro.Mobile.BL.BuildTypes.Debug) {
				title += "(D)";
			}

			Title = title;
        }
		#endregion

		#region Actions

		partial void ContactViaEmail(Foundation.NSObject sender)
		{

			if (MFMailComposeViewController.CanSendMail)
			{
				_mailController = new MFMailComposeViewController();
				_mailController.SetToRecipients(new string[] { "mark@arbprosoftware.com" });
				_mailController.SetSubject("ArbPro Mobile for iOS Contact");
				_mailController.Finished += (object s, MFComposeResultEventArgs e) =>
				{
					e.Controller.DismissViewController(true, null);
				};

				this.PresentViewController(_mailController, true, null);
			}
			else 
			{
				UIAlertView alert = new UIAlertView();
				alert.Title = @"Email not set up";
				alert.AddButton("Ok");
				alert.Message = "It appears you do not have email set up or you cannot send emails presently.";
				alert.Show();
			}
		}

		#endregion
		
		#region Private Methods
		#endregion
	}
}

