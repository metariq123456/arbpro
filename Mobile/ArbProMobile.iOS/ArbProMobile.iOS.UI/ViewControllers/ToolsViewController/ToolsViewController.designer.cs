// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	[Register ("ToolsViewController")]
	partial class ToolsViewController
	{
		[Outlet]
		UIKit.UITableView tblTools { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (tblTools != null) {
				tblTools.Dispose ();
				tblTools = null;
			}
		}
	}
}
