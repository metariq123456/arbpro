﻿// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev_ivan/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/ToolsViewController.cs#3 $
// $Author: ivan $
// $Date: 2016/09/07 $
// $Change: 26899 $
// *********************************************************************
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGS.ArbPro.Mobile.BL;
using DGS.ArbPro.Mobile.Service;
using Foundation;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class ToolsViewController : UIViewController
    {
		#region properties

		#endregion

		#region Members

		const int SECTION_TITLE = 0;
		const int SECTION_CALENDAR = 1;
		const int ROW_CALENDAR = 0;
		const int SECTION_FILTERS = 2;
		const int ROW_FILTERS = 0;

		#endregion


		public ToolsViewController () : base ("ToolsViewController", null)
        {
			this.Title = @"Tools";
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning ()
        {
            base.DidReceiveMemoryWarning ();
            // Release any cached data, images, etc that aren't in use.
        }

		// checking if it is demonstration mode and making calls to load the data
		public override void ViewWillAppear(bool animated)
		{
			ToolsTableSource MySource;
			base.ViewWillAppear(animated);

			if (BL.Globals.DemoData)
			{
				NavigationItem.Prompt = "Demonstration Mode";
			}
			else
			{
				NavigationItem.Prompt = null;
			}

			SyncTemplatesAndAppointments();

			MySource = new ToolsTableSource(this);
			tblTools.Source = MySource;
			tblTools.ReloadData();
		}

		#region public  methods

		// Navigation method to go to the calendar view controller
		public void OpenCalendar()
		{
			NavigationItem.Title = "Tools";
			this.NavigationController.PushViewController(new CalendarViewController(), true);
		}

		// Navigation method to go to the filter view controller
		public void OpenFilters()
		{
			NavigationItem.Title = "Tools";
			this.NavigationController.PushViewController(new FilterViewController(), true);
		}

		#endregion

		#region private async

		void SyncTemplatesAndAppointments()
		{
			DownloadCalendarAppointmentsAsync();
		}

		// async method to download calendar appointments
		// makes call to business layer to download appointments
		// note that within the business layer a lock is implemented to avoid more than 1 calls to the SQLite db which raises an ecception
		async void DownloadCalendarAppointmentsAsync()
		{
			if (!BL.Globals.DemoData && BL.Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOnline)
			{
				try
				{
					await Task.Run(() => BusinessLayer.AppointmentItemsListDownload()).ConfigureAwait(false);
				}
				catch (DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException)
				{
					// do nothing here as background sync of appointments and templates is not vital to operation of application
				}
			}
		}

		#endregion

		class ToolsTableSource : UITableViewSource
		{
			#region Fields

			const string CELL_ID = "SubtitleCell";
			ToolsViewController _Controller;

			#endregion

			#region Constructors

			public ToolsTableSource()
				: base()
			{
			}

			public ToolsTableSource(ToolsViewController controller)
				: base()
			{
				_Controller = controller;
			}

			#endregion

			#region Actions

			public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
			{
				int section = indexPath.Section;

				if (section == SECTION_CALENDAR)
				{
					_Controller.OpenCalendar();
				}
				else if (section == SECTION_FILTERS)
				{
					_Controller.OpenFilters();
				}
			}

			#endregion

			#region Overrides

			public override nint NumberOfSections(UITableView tableView)
			{
				return 3;
			}

			public override string TitleForHeader(UITableView tableView, nint section)
			{
				if (section == SECTION_TITLE)
				{
					return string.Empty;
				}
				else if (section == SECTION_CALENDAR)
				{
					return "Calendar";
				}
				else if (section == SECTION_FILTERS)
				{
					return "Filters";
				}

				return string.Format("bad section{0}", section);
			}

			public override nint RowsInSection(UITableView tableview, nint section)
			{
				if (section == SECTION_TITLE)
				{
					return 0;

				}

				return 1;
			}

			public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
			{
				UITableViewCell Cell;
				Cell = tableView.DequeueReusableCell(CELL_ID);
				if (Cell == null)
				{
					Cell = new UITableViewCell(UITableViewCellStyle.Subtitle, CELL_ID);
					Cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
					Cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				}

				int section = indexPath.Section;

				if (section == SECTION_CALENDAR)
				{
					Cell.TextLabel.Text = "Open Calendar";
				}
				else if (section == SECTION_FILTERS)
				{
					Cell.TextLabel.Text = "View Filters";
				}

				return Cell;
			}

			#endregion
		}
    }
}


