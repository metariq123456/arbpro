// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
//
//
using System;
using CoreGraphics;
using System.IO;
using Foundation;
using UIKit;
using DGS.ArbPro.Mobile.BL;
using DGS.ArbPro.Mobile;
using Xamarin.Media;
using System.Collections.Generic;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class QuoteItemViewController : UIViewController
    {
        public QuoteItemViewController() : base("QuoteItemViewController", null)
        {
        }

        #region Private Properties

        private QuoteItem _QuoteItem = null;

        #endregion

        #region Public Properties

        public Quote Quote{ get; set; }

        public Appointment Appointment { get; set; }

        /// <summary>
        /// The index of the item in the Quote's Items list.
        /// </summary>
        /// <value>The index of the item.</value>
        public int ItemIndex{ get; set; }

        public QuoteItem QuoteItem
        {
            get
            {
                if (_QuoteItem == null)
                {
                    _QuoteItem = Quote.Items [ItemIndex];
                }
                return _QuoteItem;
            }
        }

        public UISegmentedControl segTPO;

        #endregion

        #region DataSource

        class QuoteItemSource :UITableViewSource
        {
            #region Members

            const int SECTION_QUOTEITEM = 0;
            const int SECTION_QUOTEITEM_ROWCOUNT = 7;
            const int ROW_QUOTEITEM_ITEMNUMBER = 0;
            const int ROW_QUOTEITEM_ITEMTYPE = 1;
            const int ROW_QUOTEITEM_WORKDESCRIPTION = 2;
            const int ROW_QUOTEITEM_ITEMVALUE = 3;
            const int ROW_QUOTEITEM_PICTURE = 4;
            const int ROW_QUOTEITEM_LOCATION = 5;
            const int ROW_QUOTEITEM_TPO = 6;
            QuoteItemViewController Controller;

            #endregion

            #region Constructors

            public QuoteItemSource(QuoteItemViewController controller)
            {
                Controller = controller;
            }

            #endregion

            #region Overrides

            public override nint NumberOfSections(UITableView tableView)
            {
                return 1;
            }

            public override string TitleForHeader(UITableView tableView, nint section)
            {
                return "";
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                if (section == SECTION_QUOTEITEM)
                {
                    return SECTION_QUOTEITEM_ROWCOUNT;
                }
                return 0;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                string CELL_REF = "quoteItemCell";
                UITableViewCell cell = tableView.DequeueReusableCell(CELL_REF);
                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Value1, CELL_REF);
                }
                cell.AccessoryView = null;
                cell.ImageView.Image = null;

                switch (indexPath.Row)
                {
                case ROW_QUOTEITEM_ITEMNUMBER:
                    cell.TextLabel.Text = "Item Number";
                    cell.DetailTextLabel.Text = Controller.QuoteItem.ItemNumber;
                    if (Controller.Appointment.Editable)
                    {
                        cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    }
                    break;
                case ROW_QUOTEITEM_ITEMTYPE:
                    cell.TextLabel.Text = "Item Type";
                    cell.DetailTextLabel.Text = Controller.QuoteItem.ItemType;
                    if (Controller.Appointment.Editable)
                    {
                        cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    }
                    break;
                case ROW_QUOTEITEM_WORKDESCRIPTION:
                    cell = new UITableViewCell(UITableViewCellStyle.Subtitle, CELL_REF);
                    cell.TextLabel.Text = "Work Description";
                    cell.DetailTextLabel.Text = Controller.QuoteItem.WorkDescription == null || Controller.QuoteItem.WorkDescription.Length == 0 ? "":Controller.QuoteItem.WorkDescription;
                    if (Controller.Appointment.Editable)
                    {
                        cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    }
                    break;
                case ROW_QUOTEITEM_ITEMVALUE:
                    cell.TextLabel.Text = "Item Value";
                    cell.DetailTextLabel.Text = Controller.QuoteItem.ItemValue.ToString("C");
                    if (Controller.Appointment.Editable)
                    {
                        cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    }
                    break;
                case ROW_QUOTEITEM_PICTURE:
                    cell.TextLabel.Text = "Picture";
                    if (Controller.ImageExists())
                    {
                        cell.DetailTextLabel.Text = "Show";
                    } else
                    {
                        var picker = new MediaPicker();
                        if (picker.IsCameraAvailable)
                        {
                            cell.DetailTextLabel.Text = "Take";
                        } else
                        {
                            cell.DetailTextLabel.Text = "No Camera";
                            cell.Accessory = UITableViewCellAccessory.None;
                        }
                    }
                        
                    cell.ImageView.Image = Controller.GetImage();
                    break;
                case ROW_QUOTEITEM_LOCATION:
                    cell.TextLabel.Text = "Location";
                    if (Controller.QuoteItem.Location != null)
                    {
                        cell.DetailTextLabel.Text = "Mapped";
                    } else
                    {
                        cell.DetailTextLabel.Text = "Not Set";
                    }
                    break;
                case ROW_QUOTEITEM_TPO:
                    cell.TextLabel.Text = "TPO";
                    cell.AccessoryView = Controller.segTPO;
                    break;
                default:
                    break;
                }

                cell.SelectionStyle = UITableViewCellSelectionStyle.None;
                return cell;
            }

            #endregion

            #region Private Methods

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                if (indexPath.Section == SECTION_QUOTEITEM)
                {
                    switch (indexPath.Row)
                    {
                    case ROW_QUOTEITEM_ITEMNUMBER:
                        if (Controller.Appointment.Editable)
                        {
                            Controller.DisplayTextInput(ROW_QUOTEITEM_ITEMNUMBER, Controller.QuoteItem.ItemNumber, "Item Number", UIKeyboardType.Default, TextLookupSource.None);
                        }
                        break;
                    case ROW_QUOTEITEM_ITEMTYPE:
                        if (Controller.Appointment.Editable)
                        {
                            Controller.DisplayTextPicker(ROW_QUOTEITEM_ITEMTYPE, Controller.QuoteItem.ItemType, "Item Type", UIKeyboardType.Default, TextLookupSource.ItemType);
                        }
                        break;
                    case ROW_QUOTEITEM_WORKDESCRIPTION:
                        if (Controller.Appointment.Editable)
                        {
                            if (Controller.QuoteItem.WorkDescription == null || Controller.QuoteItem.WorkDescription.Length == 0)
                            {
                                Controller.DisplayTextPicker(ROW_QUOTEITEM_WORKDESCRIPTION, Controller.QuoteItem.WorkDescription, "Work Description", UIKeyboardType.Default, TextLookupSource.WorkDescription);
                            } else
                            {
                                Controller.DisplayNotesInput(ROW_QUOTEITEM_WORKDESCRIPTION, Controller.QuoteItem.WorkDescription, "Work Description", "", UIKeyboardType.Default, true);
                            }
                        }
                        break;
                    case ROW_QUOTEITEM_ITEMVALUE:
                        if (Controller.Appointment.Editable)
                        {
                            Controller.DisplayTextInput(ROW_QUOTEITEM_ITEMVALUE, Controller.QuoteItem.ItemValue.ToString(), "Item Value", UIKeyboardType.DecimalPad, TextLookupSource.None);
                        }
                            break;
                    case ROW_QUOTEITEM_LOCATION:
                        if (Controller.Appointment.Editable)
                        {
                            Controller.CaptureLocation();
                        }
                        break;
                    case ROW_QUOTEITEM_PICTURE:
                        if (Controller.ImageExists())
                        {
                            Controller.DisplayPicture();
                        } else
                        {
                            if (Controller.Appointment.Editable)
                            {
                                Controller.TakePicture();
                            }
                        }
                        break;
                    default:
                        break;
                    }
                }

            }

            #endregion
        }

        #endregion

        #region Actions

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            CreateTPOSegmentControl();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            SetupNavigationBar();
            table.Source = new QuoteItemSource(this);
            table.ReloadData();

            if (BL.Globals.DemoData)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            } else
            {
                NavigationItem.Prompt = null;
            }

        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
            this.Title = @"Back";
        }

        #endregion

        #region Public Methods

        public Boolean ImageExists()
        {
            return File.Exists(PictureFileName());
        }

        public UIImage GetImage()
        {
            if (ImageExists())
            {
                return UIImage.FromFile(PictureFileName());
            } else
            {
                return null;//UIImage.FromBundle("Icon.png");
            }
        }

        public void TakePicture()
        {
            var picker = new MediaPicker();
            if (picker.IsCameraAvailable)
            {
                picker.TakePhotoAsync(new StoreCameraMediaOptions {
                    Directory = "QuoteItemImages",
                    Name = PictureFileName()
                }).ContinueWith(t =>
                {
                    if (t.IsCanceled || t.IsFaulted)
                    {
                        DeleteImage();
                    } else
                    {
                        InvokeOnMainThread(() => table.ReloadData());
                    }					
                });
            }
        }

        public void DisplayPicture()
        {
            if (ImageExists())
            {
                ImageDisplayViewController controller = new ImageDisplayViewController(ImageEditTypeEnum.Picture);
                controller.displayImage = GetImage();
                controller.ImageDeletedEvent += ImageDeleted;
                controller.ImageSavedEvent += ImageSaved;
                NavigationController.PushViewController(controller, true);
            }
        }

        public void DisplayNotesInput(int Index, string CurrentText, string ViewTitle, string InputLabel, UIKeyboardType KeyboardType)
        {
            NotesInputViewController controller = new NotesInputViewController(false);
            controller.MaxLength = DGS.ArbPro.Service.Model.Types.QuoteItem.WorkDescription_MaxLength;
            controller.ViewTitle = ViewTitle;//"Logistics";
            controller.Index = Index;
            controller.CurrentText = CurrentText;
            controller.KeyboardType = KeyboardType;
            controller.DidInputTextEvent += TextInputSaved;
            controller.HidesBottomBarWhenPushed = true;
            NavigationController.PushViewController(controller, true);
        }

        public void DisplayNotesInput(int Index, string CurrentText, string ViewTitle, string InputLabel, UIKeyboardType KeyboardType, bool isPassThrough)
        {
            NotesInputViewController controller = new NotesInputViewController(isPassThrough);
            controller.MaxLength = DGS.ArbPro.Service.Model.Types.QuoteItem.WorkDescription_MaxLength;
            controller.ViewTitle = ViewTitle;//"Logistics";
            controller.Index = Index;
            controller.CurrentText = CurrentText;
            controller.KeyboardType = KeyboardType;
            controller.DidInputTextEvent += TextInputSaved;
            controller.HidesBottomBarWhenPushed = false;
            NavigationController.PushViewController(controller, true);
        }

        public void DisplayTextInput(int RowNumber, string CurrentText, string InputLabel, UIKeyboardType KeyboardType, TextLookupSource lookupSource, bool isPassThrough = false)
        {
            TextInputViewController controller = new TextInputViewController(isPassThrough);
            controller.LookupSource = lookupSource;
            controller.ViewTitle = "Item Details";
            controller.Index = RowNumber;
            controller.CurrentText = CurrentText;
            controller.InputLabel = InputLabel;
            controller.KeyboardType = KeyboardType;
            controller.DidInputTextEvent += TextInputSaved;


            NavigationController.PushViewController(controller, true);
        }

        public void DisplayTextPicker(int Index, string CurrentText, string InputLabel, UIKeyboardType KeyboardType, TextLookupSource lookupSource)
        {
            TextPickerViewController controller = new TextPickerViewController();


            List<string> lookupValues = new List<string>();

            if (lookupSource == TextLookupSource.ItemType)
            {
                controller.MaxLength = DGS.ArbPro.Service.Model.Types.QuoteItem.ItemType_MaxLength;
                var Items = DAL.Repository.Instance().ItemTypeList();
                foreach (var item in Items)
                {
                    lookupValues.Add(item.CommonName);
                }
            } else if (lookupSource == TextLookupSource.WorkDescription)
            {
                controller.MaxLength = DGS.ArbPro.Service.Model.Types.QuoteItem.WorkDescription_MaxLength;
                var Descriptions = DAL.Repository.Instance().ItemDescriptionList();
                foreach (var description in Descriptions)
                {
                    lookupValues.Add(description.Description);
                }
            }

            controller.LookupItems = lookupValues;
            controller.DefaultValue = CurrentText;
            controller.Index = Index;
            controller.ViewTitle = "Item Details";
            controller.InputLabel = InputLabel;
            controller.DidPickTextEvent += TextInputSaved;

            NavigationController.PushViewController(controller, true);
        }

        public void CaptureLocation()
        {
            MapViewController controller = new MapViewController();
            if (Appointment.ClientAddressLocation != null)
            {
                controller.InitialCentre = Appointment.ClientAddressLocation;
            }


            GeoLocation location = QuoteItem.Location;

            if (location != null)
            {
                location.Title = QuoteItem.ItemType;
                controller.Locations = new GeoLocation[] { location };
            }

            controller.CaptureMode = true;
            controller.LocationCapturedEvent += LocationCaptured;
            controller.ShowAddress = false;

            NavigationController.PushViewController(controller, true);
        }

        #endregion

        #region Private Methods

        private string PictureFileName()
        {
            return Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/QuoteItemImages/" + QuoteItem.Guid.ToString() + ".png";
        }

        void DeleteImage()
        {
            Quote.Dirty = true;
            File.Delete(PictureFileName());
        }

        void ImageDeleted()
        {
            DeleteImage();
            table.ReloadData();
        }

        void ImageSaved(UIImage newImage)
        {
            _QuoteItem.HasEditedPicture = true;
            DeleteImage();
            NSError e = new NSError();
            newImage.AsPNG().Save(PictureFileName(), false, out e);
            Quote.Dirty = true;
            table.ReloadData();
        }

        void CreateTPOSegmentControl()
        {
            segTPO = new UISegmentedControl(new string[] { "Unknown", "No", "Yes" });
            segTPO.Enabled = Appointment.Editable;
            segTPO.TintColor = NavigationController.NavigationBar.BarTintColor;
            segTPO.BackgroundColor = UIColor.White;
            segTPO.SetWidth(70, 0);
            segTPO.SetWidth(70, 1);
            segTPO.SetWidth(70, 2);
            segTPO.SelectedSegment = (int)QuoteItem.PreservationOrderStatus;
            segTPO.ValueChanged += delegate
            {
                switch (segTPO.SelectedSegment)
                {
                case 0:
                    QuoteItem.PreservationOrderStatus = EnumPreservationOrderStatus.Unknown;
                    break;
                case 1:
                    QuoteItem.PreservationOrderStatus = EnumPreservationOrderStatus.No;
                    break;
                case 2:
                    QuoteItem.PreservationOrderStatus = EnumPreservationOrderStatus.Yes;
                    break;
                default:
                    break;
                }
                Quote.Dirty = true;
            };
        }

        void SetupNavigationBar()
        {
            this.Title = @"Item";
        }

        void TextInputSaved(int rowNumber, string SavedText)
        {
            switch (rowNumber)
            {
            case 0:
                QuoteItem.ItemNumber = SavedText;
                break;
            case 1:
                QuoteItem.ItemType = SavedText;
                break;
            case 2:
                QuoteItem.WorkDescription = SavedText;
                break;
            case 3:
					decimal output;
					if (!decimal.TryParse(SavedText, out output))
					{
						UIAlertView alert = new UIAlertView();
						alert.Title = "Incorrect value entered";
						alert.Message = "The value you have entered is in an invalid format.\nPlease try again.";
						alert.AddButton("Ok");
						alert.Show();
					}
					else 
					{
						QuoteItem.ItemValue = output;
					}
                break;
            default:
                break;
            }
            Quote.Dirty = true;

            table.ReloadData();
        }

        void LocationCaptured(GeoLocation location)
        {
            QuoteItem.Location = location;
            Quote.Dirty = true;
            table.ReloadData();
        }

        #endregion
    }
}


