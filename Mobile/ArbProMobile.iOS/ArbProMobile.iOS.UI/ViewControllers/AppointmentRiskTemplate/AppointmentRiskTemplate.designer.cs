// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	[Register ("AppointmentRiskTemplate")]
	partial class AppointmentRiskTemplate
	{
		[Outlet]
		UIKit.UISearchBar riskSearchBar { get; set; }

		[Outlet]
		UIKit.UITableView tblRiskTemplate { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (tblRiskTemplate != null) {
				tblRiskTemplate.Dispose ();
				tblRiskTemplate = null;
			}

			if (riskSearchBar != null) {
				riskSearchBar.Dispose ();
				riskSearchBar = null;
			}
		}
	}
}
