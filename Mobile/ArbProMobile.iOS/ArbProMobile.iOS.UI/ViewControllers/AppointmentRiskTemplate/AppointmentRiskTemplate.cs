﻿// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Foundation;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class AppointmentRiskTemplate : UIViewController
	{
		#region public properties

		public List<BL.RiskTemplate> LookupItems { get; set; }

		public string DefaultValue { get; set; }

		// The maximum length of user input into the search text view.
		public int MaxLength { get; set; }

		public string SearchText
		{
			get
			{
				string text = riskSearchBar.Text;
				if (text == null)
					text = string.Empty;

				return text;
			}
		}

		#endregion

		#region private properties

		private List<BL.RiskTemplate> FilteredItems { get; set; }

		#endregion

		#region Members

		private RiskListSource _DataSource;

		class SearchBarDelegate : UISearchBarDelegate
		{
			AppointmentRiskTemplate _controller;

			public SearchBarDelegate(AppointmentRiskTemplate controller)
			{
				_controller = controller;
			}

			public override void CancelButtonClicked(UISearchBar searchBar)
			{
				searchBar.Text = "";
				_controller.FilterItems();
			}

			public override void TextChanged(UISearchBar searchBar, string searchText)
			{
				_controller.FilterItems();
			}

			public override void ListButtonClicked(UISearchBar searchBar)
			{
				searchBar.ResignFirstResponder();
			}

			public override bool ShouldChangeTextInRange(UISearchBar searchBar, NSRange range, string text)
			{
				// NOTE: Don't call the base implementation on a Model class
				// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events 
				// Note we need to cater for an empty replacement - i.e. the delete key being pressed.
				if (text != "" && _controller.MaxLength != 0)
				{
					return searchBar.Text.Length < _controller.MaxLength;
				}
				else {
					return true;
				}
			}
		}

		class RiskListSource : UITableViewSource
		{

			#region Members

			const string CONST_CELL_NAME = "RiskTemplateCell";
			AppointmentRiskTemplate Controller;

			#endregion

			#region Properties

			public List<BL.RiskTemplate> Templates { get; set; }

			#endregion

			#region Constructors

			public RiskListSource(AppointmentRiskTemplate controller)
			{
				Controller = controller;
				Templates = Controller.FilteredItems;
			}

			#endregion

			#region Overrides

			public override nint NumberOfSections(UITableView tableView)
			{
				return 1;
			}

			public override string TitleForHeader(UITableView tableView, nint section)
			{
				if (Templates == null || Templates.Count - 1 == 0)
				{
					return "No Templates Found";
				}
				else if (Templates.Count - 1 == 1)
				{
					return string.Format("{0} Template Found", Templates.Count - 1);
				}
				else
				{
					return string.Format("{0} Templates Found", Templates.Count - 1);
				}
			}

			public override nint RowsInSection(UITableView tableview, nint section)
			{
				if (Templates != null)
				{
					return Templates.Count;
				}
				else
				{
					return 0;
				}
			}

			public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
			{
				return 50;
			}

			public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
			{
				//Get a table cell from the factory
				UITableViewCell cell = tableView.DequeueReusableCell(CONST_CELL_NAME);

				if (cell == null)
				{
					cell = new UITableViewCell(UITableViewCellStyle.Default, CONST_CELL_NAME);
				}
				BL.RiskTemplate r = Templates[indexPath.Row];
				if (r.Id > 0)
				{
					cell.TextLabel.Text = r.Name;
				}

				return cell;
			}

			public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
			{
				BL.RiskTemplate r = GetItem(indexPath.Row);
				if (r.Id > 0)
				{
					Controller.NavigationController.PopViewController(true);
					var viewCon = (AppointmentNewViewController)Controller.NavigationController.TopViewController;
					viewCon.GetRiskTemplateInfo(r);
				}
			}

			#endregion

			#region Public Methods

			public BL.RiskTemplate GetItem(int row)
			{
				return Templates[row];
			}

			#endregion

		}
		//A reference to the buttons is kept to prevent GC from disposing
		public UIButton AddButton;

		#endregion

		public AppointmentRiskTemplate() : base("AppointmentRiskTemplate", null)
		{
			FilteredItems = new List<BL.RiskTemplate>();
		}

		#region Overrides

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			NavigationController.NavigationBar.Translucent = false;
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			Title = "Risk Templates";

			if (BL.Globals.DemoData)
			{
				NavigationItem.Prompt = "Demonstration Mode";
			}
			else
			{
				NavigationItem.Prompt = null;
			}
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			riskSearchBar.Delegate = new SearchBarDelegate(this);

			RefreshData();

			foreach (UIView view in riskSearchBar.Subviews)
			{
				if (view.GetType() == typeof(UITextField))
				{
					var field = (UITextField)view;
					field.ClearButtonMode = UITextFieldViewMode.Never;
					field.ReturnKeyType = UIReturnKeyType.Done;
				}
			}
		}

		#endregion

		#region Private Methods


		void RefreshData()
		{
			PopulateTableSource();
		}

		async void PopulateTableSource()
		{
			try
			{
				_DataSource = await PopulateTableSourceTask();
				tblRiskTemplate.Source = _DataSource;
				FilterItems();
			}
			catch (Exception)
			{
				var alert = new UIAlertView("Error", "Error populating Risk Templates table. Please navigate to the prieviouse page and try again", null, "Ok");
				alert.Show();
			}
		}

		private Task<RiskListSource> PopulateTableSourceTask()
		{
			var taskResult = new TaskCompletionSource<RiskListSource>();
			List<BL.RiskTemplate> allTemplates = BL.BusinessLayer.TemplateList();

			LookupItems = allTemplates;

			taskResult.SetResult(new RiskListSource(this));
			return taskResult.Task;
		}

		#endregion

		#region public methods

		public void FilterItems()
		{
			string lookupText = SearchText.ToUpper();

			FilteredItems.Clear();
			foreach (var item in LookupItems)
			{
				if (lookupText == string.Empty || item.Name.ToUpper().Contains(lookupText))
					FilteredItems.Add(item);
			}

			tblRiskTemplate.ReloadData();
		}

		public void ItemSelected(string text)
		{
			riskSearchBar.Text = text;
		}

		#endregion
	}
}

