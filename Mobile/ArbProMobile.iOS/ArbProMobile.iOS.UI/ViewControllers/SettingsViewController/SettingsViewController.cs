// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
//
//
using System;
using System.Threading.Tasks;
using CoreGraphics;
using DGS.ArbPro.Mobile.BL;
using DGS.ArbPro.Mobile.Core.Enums;
using Foundation;
using MessageUI;
using UIKit;
using Xamarin;

using Microsec.Mobile.Core.Logging;
using System.Threading;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class SettingsViewController : UIViewController
	{
		#region Members

		const int ROW_SERVER = 0;
		const int SECTION_LOGIN = 0;
		const int ROW_LOGGED_IN = 0;
		int SECTION_SYNCHRONISE = 1;
		const int ROW_APPOINMENTS = 0;
		const int ROW_QUOTES = 1;
		const int ROW_REFERENCEDATA = 2;
		int SECTION_APPLICATION = 3;
		const int ROW_DATAMANAGEMENT = 0;
		const int ROW_ABOUT = 1;
		int SECTION_FINANCES = 2;
		const int ROW_VAT = 0;
		int SECTION_LOGGING = 4;
		const int ROW_VIEWLOG = 0;
		const int ROW_EMAILLOG = 1;
		const int ROW_CLEARLOG = 2;
		int SECTION_MANAGEMENT = 5;
		const int ROW_NEWCLIENT = 0;
		const int ROW_EDITCLIENT = 1;
		ILogger log = TinyIoC.TinyIoCContainer.Current.Resolve<ILogger>();

		NetworkStatus _NetworkStatus = NetworkStatus.NotReachable;

		static SemaphoreSlim rowSelectedLock = new SemaphoreSlim(1);

		#endregion

		#region Constructors

		public SettingsViewController ()
			: base ("SettingsViewController", null)
		{
			NSNotificationCenter.DefaultCenter.AddObserver (UIApplication.WillEnterForegroundNotification, WillEnterForeGround);
			this.Title = @"Settings";
		}

		#endregion

		#region Overrides

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		}

		public override void ViewWillAppear (bool animated)
		{
			SettingsTableSource MySource;
			base.ViewWillAppear (animated);

			if (BL.Globals.DemoData) {
				NavigationItem.Prompt = "Demonstration Mode";
			} else {
				if (BL.Globals.LoginStatus != Globals.LoggedInStatus.NotLoggedIn) {
					CheckConnectivity ();
				}
				NavigationItem.Prompt = null;
				ActivityView.ProgressView.DialogButtonClickedEvent += DialogButtonClicked;
			}

			MySource = new SettingsTableSource (this);
			viewTable.Source = MySource;
			viewTable.ReloadData ();
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			//Add BuisnessLayer event handlers to progress bar
			BusinessLayer.ActivityProcessStageEvent += ActivityView.SetProgressTitle;
			BusinessLayer.ItemListCountActivityEvent += ActivityView.SetMaxValue;
			BusinessLayer.ItemSavedEvent += ActivityView.IncreaseProgress;

		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			//Remove BuisnessLayer event handlers to progress bar to prevent conflicts
			BusinessLayer.ActivityProcessStageEvent -= ActivityView.SetProgressTitle;
			BusinessLayer.ItemListCountActivityEvent -= ActivityView.SetMaxValue;
			BusinessLayer.ItemSavedEvent -= ActivityView.IncreaseProgress;
		}

		#endregion

		#region Actions

		void DidChangeVat (int index, string text)
		{
			try {
				if (text != "0" && text != "Not Registered") {
					decimal output;
					if (!decimal.TryParse(text, out output))
					{
						UIAlertView alert = new UIAlertView();
						alert.Title = "Incorrect value entered";
						alert.Message = "The value you have entered is in an invalid format.\nPlease try again.";
						alert.AddButton("Ok");
						alert.Show();
					}
					else
					{
						Settings.VatValue = output;
					}
				} else {
					Settings.VatValue = 0;
				}
			} catch {
			}
		}

		#endregion

		#region Private Methods

		void DisplayTextEntryController (int section)
		{
			TextInputViewController controller = new TextInputViewController ();
			controller.ViewTitle = "V.A.T.";
			controller.KeyboardType = UIKeyboardType.DecimalPad;
			controller.CurrentText = Settings.VatValue > 0 ? Settings.VatValue.ToString () : "Not Registered";
			controller.DidInputTextEvent += DidChangeVat;
			controller.Index = section;
			this.NavigationController.PushViewController (controller, true);
		}

		void ShowLoginController (string message = "")
		{
			if (!string.IsNullOrWhiteSpace(message))
			{
				UIAlertView alert = new UIAlertView();
				alert.Title = "Authentication Error";
				alert.Message = "Your login on this device is no longer valid. \n (Have you logged in from another device?) \n Please login again.";
				alert.AddButton("Ok");
				alert.Show();

				alert.Clicked += (object senders, UIButtonEventArgs es) =>
				{
					LoginViewController controller = new LoginViewController(message, false);
					this.NavigationController.PushViewController(controller, true);
				};
			}
			else
			{
				LoginViewController controller = new LoginViewController(message, false);
				this.NavigationController.PushViewController(controller, true);
			}
		}

		async void SynchroniseAppointments ()
		{
			if (_NetworkStatus == NetworkStatus.NotReachable) {
				DisplayNoConnectionStatus ();
			} else {
				if (Globals.LoginStatus == Globals.LoggedInStatus.NotLoggedIn) {
					ShowLoginController ();
				}

				try
				{
					await log.Debug("Beginning appointments download.");
					ActivityView.ShowProgress("Downloading");
					await rowSelectedLock.WaitAsync();
					await SyncronsieAppointmentsAsync().ConfigureAwait(true);
				}
				catch (Exception ex)
				{
					ActivityView.HideProgress();
					if (ex is DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException)
					{
						ShowLoginController("Service session has expired, please login.");
					}
					else {
						Core.ArbProInsights.LogErrorError(ex);
					}

					await log.Debug(string.Format("Error downloading appointments: {0}.", ex.Message));
				}
				finally
				{
					rowSelectedLock.Release();
					ActivityView.DisplayCompletedStatus("Completed");
					await log.Debug("Completed appointments download.");
				}
			}
		}

		async void CheckConnectivity ()
		{
			ActivityView.DisplayNetworkIndicator (false);
			await AsyncCheckConnectivity ().ConfigureAwait (true);
			ActivityView.DisplayNetworkIndicator (true);
		}

		async Task AsyncCheckConnectivity ()
		{
			await Task.Run (() => _NetworkStatus = ConnectionStatus.CheckReachability ()).ConfigureAwait (false);// BusinessLayer.AppointmentListDownload (Globals.CurrentUser.Id)).ConfigureAwait (false);
		}

		void StartLongRunningTask (int rowIndex)
		{
			SelectAndRunTask (rowIndex);
		}

		void SelectAndRunTask(int rowSelected)
		{
			if (Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOnline)
			{
				if (Settings.HasDownloadedRefData)
				{
					switch (rowSelected)
					{
						case ROW_APPOINMENTS:
							SynchroniseAppointments();
							break;
						case ROW_QUOTES:
							SynchroniseQuotes();
							break;
						case ROW_REFERENCEDATA:
							SynchroniseRefernceData();
							break;
					}
				}
				else {
					SynchroniseRefernceData();
				}
			}
		}

		async Task SyncronsieAppointmentsAsync ()
		{
			await Task.Run (() => BusinessLayer.AppointmentListDownload (Globals.CurrentUser.Id)).ConfigureAwait (false);
		}

		async void SynchroniseQuotes ()
		{
			if (_NetworkStatus == NetworkStatus.NotReachable) {
				DisplayNoConnectionStatus ();
			} else {
				try
				{
					await log.Debug("Beginning upload of quotes.");
					ActivityView.ShowProgress("Uploading");
					await rowSelectedLock.WaitAsync();
					await SynchroniseQuotesAsync().ConfigureAwait(true);
				}
				catch (DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException)
				{
					ActivityView.HideProgress();
					ShowLoginController("Service session has expired, please login.");
				}
				catch (Exception)
				{
					ActivityView.HideProgress();
					UIAlertView alert = new UIAlertView();
					alert.Title = "Quote upload complete but error during contact details update.";
					alert.Message = "Contact details can still be updated from the web application.";
					alert.AddButton("Ok");
					alert.Show();
				}
				finally
				{
					rowSelectedLock.Release();
					ActivityView.DisplayCompletedStatus("Completed");
					await log.Debug("Completed upload of quotes.");
				}
			}
		}

		async Task SynchroniseQuotesAsync()
		{
			//	Create a new task to be run on the background - This task is configured to not be awaited
			await Task.Run(async () =>
			{
				//	Register this task with iOS as a back ground task
				var taskId = UIApplication.SharedApplication.BeginBackgroundTask(() =>
				{
				//	This will be invoked when the alloted time for back ground tasks expires
				Console.WriteLine("Background task time expired!");
				});
				//	Upload the quote items
				await Task.Run(() => BusinessLayer.QuoteItemsUpload()).ConfigureAwait(true);
				//	Delete uploaded and cancelled quotes
				await Task.Run(() => BusinessLayer.DeleteUploadedAndCancelled()).ConfigureAwait(true);
				// Update client contact details information
				await Task.Run(() => BusinessLayer.AppointmentClientContactUploadNotPending()).ConfigureAwait(true);
				//	Notify iOS that the background task has ended
				UIApplication.SharedApplication.EndBackgroundTask(taskId);
			}).ConfigureAwait(false);
		}

		async void SynchroniseRefernceData ()
		{
			if (_NetworkStatus == NetworkStatus.NotReachable) {
				DisplayNoConnectionStatus ();
			} else {

				try
				{
					await log.Debug("Beginning download of reference data.");
					ActivityView.ShowProgress("Downloading");
					await rowSelectedLock.WaitAsync();
					await DownloadRefenceDataAsync().ConfigureAwait(true);
				}
				catch (DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnauthorisedException)
				{
					ActivityView.HideProgress();
					ShowLoginController("Service session has expired, please login and re-download reference data.");
					await log.Debug("Service session has expired.");
				}
				catch (Exception ex)
				{
					ActivityView.DisplayCompletedStatus(string.Format("Error during download: {0}", ex.Message));
					await log.Debug(string.Format("Error during download: {0}", ex.Message));
				}
				finally
				{
					rowSelectedLock.Release();

					//Reload the table view as the rows to be displayed may have changed.
					await log.Debug("Completed download of reference data.");
					this.viewTable.ReloadData();

					ActivityView.DisplayCompletedStatus("Completed");
				}
			}
		}

		async Task DownloadRefenceDataAsync ()
		{
			await Task.Run (() => BusinessLayer.ReferenceDataDownload ()).ConfigureAwait (true);
		}

		// TODO check whether we needed async on this
		void DialogButtonClicked (string buttonCaption)
		{
			if (buttonCaption == "Cancel") {
				BusinessLayer.CancelActivity = true;
				ActivityView.DisplayCompletedStatus ("Cancelled");
			} else {
				ActivityView.HideProgress ();
			}
		}

		void DisplayManagementController ()
		{
			ManagementViewController controller = new ManagementViewController ();
			this.NavigationController.PushViewController (controller, true);
		}

		void DisplayAboutController ()
		{
			AboutViewController controller = new AboutViewController ();
			this.NavigationController.PushViewController (controller, true);
		}

		void DisplayNoConnectionStatus ()
		{
			UIAlertView alert = new UIAlertView();
			alert.Title = @"No network connection available";
			alert.AddButton("Ok");
			alert.Message = "This feature requires internet connectivity.\nPlease try again later.";
			alert.Show();
		}

		void CreateTestItems ()
		{
			bool create = false;
			#if DEBUG
			create = true;
			#endif

			if (!create)
				return;

			//This method will create a default number of 20 items for the first available appointment
			BL.BusinessLayer.AppointmentSetupTestItems ();
		}

		void WillEnterForeGround (NSNotification message)
		{
			CheckConnectivity ();
		}

		void DisplayLogViewController()
		{
			DisplayLogViewController controller = new UI.DisplayLogViewController();
			this.NavigationController.PushViewController(controller, true);
		}

		void EmailLog()
		{
			MFMailComposeViewController mailController;

			if (MFMailComposeViewController.CanSendMail) {
				mailController = new MFMailComposeViewController ();

				mailController.SetToRecipients (new string[]{ "support@arbprosoftware.com" });
				mailController.SetSubject ("ArbPro mobile log");
				mailController.SetMessageBody ("Application log file from user " + Settings.Username +
				". Application version is " + NSBundle.MainBundle.InfoDictionary ["CFBundleVersion"].ToString () +
				". Sent on " + DateTime.Now, false);
				var attachment = NSData.FromFile (log.FullFilename);
				if (attachment != null) {
					mailController.AddAttachmentData (attachment, @"application/txt", "Log.txt");

					mailController.Finished += ( object s, MFComposeResultEventArgs args) => {					
						Console.WriteLine (args.Result.ToString ());
						args.Controller.DismissViewController (true, null);
					};

					this.PresentViewController (mailController, true, null);
				} else {
					UIAlertView alert = new UIAlertView ();
					alert.Title = @"Log file not found";
					alert.AddButton ("Ok");
					alert.Message = "There is no log file to attach.";
					alert.Show ();
				}
			} else {
				UIAlertView alert = new UIAlertView ();
				alert.Title = @"Email not set up";
				alert.AddButton ("Ok");
				alert.Message = "It appears you do not have email set up or you cannot send emails presently.";
				alert.Show ();
			}
		}

		void ClearLog()
		{
			var attachment = NSData.FromFile (log.FullFilename);

			if (attachment != null) {

				UIAlertView alert = new UIAlertView ();
				alert.Title = @"Clear log file";
				alert.AddButton ("Yes");
				alert.AddButton ("No");
				alert.Message = "Are you sure you want to delete the log file? You might want to first email it via the Email Log File button.";
				alert.Show ();

				alert.Clicked += (object senders, UIButtonEventArgs es) => {
					if (es.ButtonIndex == 0) {
						log.DeleteLog ();

						UIAlertView alert2 = new UIAlertView ();
						alert2.Title = @"Log file has been deleted.";
						alert2.AddButton ("Ok");
						alert2.Message = "";
						alert2.Show ();
					}
				};
			} else {
				UIAlertView alert2 = new UIAlertView ();
				alert2.Title = @"Log file not found";
				alert2.AddButton ("Ok");
				alert2.Message = "There is no log file to delete.";
				alert2.Show ();
			}
		}

		public void AddOrEditClient(int clientId)
		{
			if (_NetworkStatus == NetworkStatus.NotReachable)
			{
				DisplayNoConnectionStatus();
			}
			else {
				NavigationItem.Title = "Settings";
				this.NavigationController.PushViewController(new EditClientViewController(clientId, false), true);
			}
		}

		public void DisplayClientList()
		{
			if (_NetworkStatus == NetworkStatus.NotReachable)
			{
				DisplayNoConnectionStatus();
			}
			else {
				NavigationItem.Title = "Settings";
				this.NavigationController.PushViewController(new ClientListViewController(false), true);
			}
		}

		#endregion

		class SettingsTableSource : UITableViewSource
		{

			#region Fields

			const string CELL_ID = "SubtitleCell";
			SettingsViewController _Controller;

			#endregion

			#region Constructors

			public SettingsTableSource ()
				: base ()
			{
			}

			public SettingsTableSource (SettingsViewController controller)
				: base ()
			{
				_Controller = controller;
				if (BL.Globals.DemoData)
				{
					_Controller.SECTION_SYNCHRONISE = -1;
					_Controller.SECTION_APPLICATION = 3;
					_Controller.SECTION_FINANCES = 2;
					_Controller.SECTION_LOGGING = 4;
					_Controller.SECTION_MANAGEMENT = 1;
				}
				else if (Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOffline)
				{
					_Controller.SECTION_SYNCHRONISE = 1;
					_Controller.SECTION_APPLICATION = 5;
					_Controller.SECTION_FINANCES = 3;
					_Controller.SECTION_LOGGING = 4;
					_Controller.SECTION_MANAGEMENT = 2;
				}
				else 
				{
					_Controller.SECTION_SYNCHRONISE = 1;
					_Controller.SECTION_APPLICATION = 4;
					_Controller.SECTION_FINANCES = 3;
					_Controller.SECTION_LOGGING = 5;
					_Controller.SECTION_MANAGEMENT = 2;
				}
			}

			#endregion

			#region Actions

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				int section = indexPath.Section;
				_Controller.CheckConnectivity();

				if (section == SECTION_LOGIN)
				{
					_Controller.ShowLoginController();
				}
				else if (section == _Controller.SECTION_SYNCHRONISE)
				{
					_Controller.StartLongRunningTask(indexPath.Row);
				}
				else if (section == _Controller.SECTION_FINANCES)
				{
					_Controller.DisplayTextEntryController(section);

				}
				else if (section == _Controller.SECTION_APPLICATION)
				{
					switch (indexPath.Row)
					{
						case ROW_DATAMANAGEMENT:
							_Controller.DisplayManagementController();
							break;
						case ROW_ABOUT:
							_Controller.DisplayAboutController();
							break;
						case ROW_ABOUT + 1:
							_Controller.CreateTestItems();
							break;
					}
				}
				else if (section == _Controller.SECTION_LOGGING)
				{
					switch (indexPath.Row)
					{
						case ROW_VIEWLOG:
							_Controller.DisplayLogViewController();
							break;
						case ROW_EMAILLOG:
							_Controller.EmailLog();
							break;
						case ROW_CLEARLOG:
							_Controller.ClearLog();
							break;
					}
				}
				else if (section == _Controller.SECTION_MANAGEMENT)
				{
					switch (indexPath.Row)
					{
						case ROW_NEWCLIENT:
							_Controller.AddOrEditClient(-1);
							break;
						case ROW_EDITCLIENT:
							_Controller.DisplayClientList();
							break;
					}
				}
#if DEBUG


#endif
			}


			#endregion

			#region Overrides

			public override nint NumberOfSections (UITableView tableView)
			{
				if (BL.Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOnline)
					return 6;
				else
					return 5;
			}

			public override string TitleForHeader (UITableView tableView, nint section)
			{
				if (section == SECTION_LOGIN) {
					return "Login";
				} 
				else if (section == _Controller.SECTION_SYNCHRONISE) {
					return "Synchronisation";
				} 
				else if (section == _Controller.SECTION_FINANCES) {
					return "VAT";
				} 
				else if (section == _Controller.SECTION_APPLICATION) {
					return "Application";
				}
				else if (section == _Controller.SECTION_LOGGING)
				{
					return "Logging";
				}
				else if (section == _Controller.SECTION_MANAGEMENT)
				{
					return "Management";
				}

				return string.Format ("bad section{0}", section);
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				if (section == SECTION_LOGIN) {
					return 1;

				} else if (section == _Controller.SECTION_SYNCHRONISE) {
					if (Settings.HasDownloadedRefData)
						return 3;
					else
						return 1;

				} else if (section == _Controller.SECTION_APPLICATION) {
					int returnValue;
					returnValue = 2;
#if DEBUG
					returnValue = 3;
#endif
					return returnValue;

				} else if (section == _Controller.SECTION_FINANCES) {
					return 1;

				} else if (section == _Controller.SECTION_LOGGING) {
					return 3;
				} else if (section == _Controller.SECTION_MANAGEMENT)
				{
					return 2;
				}

				return 0;
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				UITableViewCell Cell;
				Cell = tableView.DequeueReusableCell (CELL_ID);
				if (Cell == null) {
					Cell = new UITableViewCell (UITableViewCellStyle.Subtitle, CELL_ID);
					Cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
					Cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				}

				int section = indexPath.Section;

				if (section == SECTION_LOGIN)
				{
					Cell.TextLabel.Text = "Log in/connect";

					switch (Globals.LoginStatus)
					{
						case Globals.LoggedInStatus.LoggedInOffline:
							//						Cell.DetailTextLabel.Text = string.Format ("Logged in offline as {0}.", Globals.CurrentUser.DisplayName);
							Cell.DetailTextLabel.Text = string.Format("Logged in offline as {0}.", Settings.Username);
							break;
						case Globals.LoggedInStatus.LoggedInOnline:
							Cell.DetailTextLabel.Text = string.Format("Logged in online as {0}.", Settings.Username);
							break;
						default:
							Cell.DetailTextLabel.Text = "Not logged in";
							break;
					}
				}
				else if (section == _Controller.SECTION_SYNCHRONISE)
				{
					var NotAvailableMessage = "Not available - offline";
					if (Settings.HasDownloadedRefData)
					{
						switch (indexPath.Row)
						{
							case ROW_APPOINMENTS:
								{
									Cell.TextLabel.Text = "Download Appointments";
									if (Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOnline)
									{
										Cell.DetailTextLabel.Text = "";
									}
									else {
										Cell.DetailTextLabel.Text = NotAvailableMessage;
									}
								}
								break;
							case ROW_QUOTES:
								{
									Cell.TextLabel.Text = "Upload Completed";
									if (Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOnline)
									{
										if (Settings.DataRetainDays == 1)
										{
											Cell.DetailTextLabel.Text = "Data retention set to 1 day";
										}
										else
										{
											Cell.DetailTextLabel.Text = "Data retention set to " + Settings.DataRetainDays + " days";
										}
									}
									else {
										Cell.DetailTextLabel.Text = NotAvailableMessage;
									}
								}
								break;
							case ROW_REFERENCEDATA:
								{
									Cell.TextLabel.Text = "Download Reference Data";
									if (Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOnline)
									{
										Cell.DetailTextLabel.Text = "";
									}
									else {
										Cell.DetailTextLabel.Text = NotAvailableMessage;
									}
								}
								break;
						}
					}
					else {
						Cell.TextLabel.Text = "Download Reference Data";
						if (Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOnline)
						{
							Cell.DetailTextLabel.Text = "";
						}
						else {
							Cell.DetailTextLabel.Text = NotAvailableMessage;
						}
					}

				}
				else if (section == _Controller.SECTION_APPLICATION)
				{
					switch (indexPath.Row)
					{
						case ROW_DATAMANAGEMENT:
							Cell.TextLabel.Text = "Data Management";
							Cell.DetailTextLabel.Text = "";
							break;
						case ROW_ABOUT:
							Cell.TextLabel.Text = "About";
							Cell.DetailTextLabel.Text = "";
							break;
						case ROW_ABOUT + 1:
							Cell.TextLabel.Text = "Create Items For 1st Appointment";
							Cell.DetailTextLabel.Text = "";
							break;


					}
				}
				else if (section == _Controller.SECTION_FINANCES)
				{
					Cell.TextLabel.Text = "Rate";
					Cell.DetailTextLabel.Text = Settings.VatValue > 0 ? string.Format("{0}%", Settings.VatValue.ToString()) : "Not Registered";

				}
				else if (section == _Controller.SECTION_LOGGING)
				{
					switch (indexPath.Row)
					{
						case ROW_VIEWLOG:
							Cell.TextLabel.Text = "View Log File";
							Cell.DetailTextLabel.Text = "";
							break;
						case ROW_EMAILLOG:
							Cell.TextLabel.Text = "Email Log File";
							Cell.DetailTextLabel.Text = "";
							break;
						case ROW_CLEARLOG:
							Cell.TextLabel.Text = "Clear Log File";
							Cell.DetailTextLabel.Text = "";
							break;
					}
				}
				else if (section == _Controller.SECTION_MANAGEMENT)
				{
					switch (indexPath.Row)
					{
						case ROW_NEWCLIENT:
							Cell.TextLabel.Text = "Add New Client";
							Cell.DetailTextLabel.Text = "";
							break;
						case ROW_EMAILLOG:
							Cell.TextLabel.Text = "Edit Clients";
							Cell.DetailTextLabel.Text = "";
							break;
					}
				}

				return Cell;
			}

			#endregion
		}
	}
}

