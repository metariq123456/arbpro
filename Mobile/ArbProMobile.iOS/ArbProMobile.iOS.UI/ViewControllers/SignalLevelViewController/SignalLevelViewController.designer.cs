// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("SignalLevelViewController")]
    partial class SignalLevelViewController
    {
        [Outlet]
        UIKit.UISegmentedControl SegSignalLevel { get; set; }


        [Outlet]
        UIKit.UITextField TxtProvider { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (SegSignalLevel != null) {
                SegSignalLevel.Dispose ();
                SegSignalLevel = null;
            }

            if (TxtProvider != null) {
                TxtProvider.Dispose ();
                TxtProvider = null;
            }
        }
    }
}