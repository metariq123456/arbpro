using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public enum SignalLevelEnum: int
    {
        None = 0,
        //No Signal
        Poor = 1,
        //1 to 2 bars
        Average = 2,
        // 3 to 4
        Good = 3
        // 5
    }

    public partial class SignalLevelViewController : UIViewController
    {

		
        #region Events

        public delegate void SignalLevelSetDelegate (string Provider, SignalLevelEnum SignalLevel);

        public event SignalLevelSetDelegate SignalLevelSetEvent;

        #endregion

        #region Properties

        public bool Enabled
        {
            get;
            set;
        }

        #endregion

        public string Provider
        {
            get;
            set;
        }

        public SignalLevelEnum Level
        {
            get;
            set;
        }

        #region Members

        //		string _Provider = "";
        //		SignalLevelEnum _MobileSignal;

        UIBarButtonItem _DoneButton = null;

        #endregion

	
        #region Constructors

        public SignalLevelViewController() : base("SignalLevelViewController", null)
        {
            this.Level = SignalLevelEnum.None;
        }

        public SignalLevelViewController(string provider, SignalLevelEnum signalLevel) : base("SignalLevelViewController", null)
        {
            this.Provider = provider;
            this.Level = signalLevel;
        }

        #endregion

        #region Overrides

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
			
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Set the navigation bar to opaque, so that the controls don't slide under it on iOS7
            this.NavigationController.NavigationBar.Translucent = false;
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            base.ViewWillAppear(animated);
            Title = "Mobile Signal";

            if (BL.Globals.DemoData)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            } else
            {
                NavigationItem.Prompt = null;
            }
            
            _DoneButton = new UIBarButtonItem(@"Done", UIBarButtonItemStyle.Done, ButtonDoneClicked);
            NavigationItem.RightBarButtonItem = _DoneButton;
            
            SegSignalLevel.SelectedSegment = (int)this.Level;
            SegSignalLevel.Enabled = this.Enabled;

            TxtProvider.Placeholder = this.Provider;
            TxtProvider.Enabled = this.Enabled;
        }

        #endregion

        void ButtonDoneClicked(object sender, EventArgs e)
        {
            if (this.Enabled)
            {
                if (SignalLevelSetEvent != null)
                {
                    string provider = TxtProvider.Text.Length > 0 ? TxtProvider.Text:TxtProvider.Placeholder;
                    nint selectedSegment = SegSignalLevel.SelectedSegment;
                    // Pass the nint to an int so we can cast to the enum. There's probably a better way
                    // to do this
                    var selectedSegmentAsInt = (int)selectedSegment;
                    SignalLevelSetEvent(provider, (SignalLevelEnum)selectedSegmentAsInt);
                }
            }
            
            if (this.NavigationController != null)
                this.NavigationController.PopViewController(true);
        }
    }
}

