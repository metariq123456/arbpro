// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	[Register ("CalendarViewController")]
	partial class CalendarViewController
	{
		[Outlet]
		UIKit.UIView ContentView { get; set; }

		[Outlet]
		UIKit.UILabel lblApptSummary { get; set; }

		[Outlet]
		UIKit.UISegmentedControl segView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ContentView != null) {
				ContentView.Dispose ();
				ContentView = null;
			}

			if (segView != null) {
				segView.Dispose ();
				segView = null;
			}

			if (lblApptSummary != null) {
				lblApptSummary.Dispose ();
				lblApptSummary = null;
			}
		}
	}
}
