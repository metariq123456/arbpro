﻿// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev_ivan/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/CalendarViewController.cs#3 $
// $Author: ivan $
// $Date: 2016/09/07 $
// $Change: 26899 $
// *********************************************************************
using System;
using Foundation;
using Syncfusion.SfSchedule.iOS;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class CalendarViewController : UIViewController
    {
        private SFSchedule Schedule;

		#region constructor 

        public CalendarViewController () : base ("CalendarViewController", null)
        {
			this.Title = @"Calendar";

			InitializeSchedule();

			Schedule.Appointments = ScheduleAppointmentFactory.ScheduleAppointmentsFromItems (DAL.Repository.Instance ().CalendarItemList ());
        }

		#endregion

		#region overrides

        public override void ViewWillAppear (bool animated)
        {
            base.ViewWillAppear (animated);

			if (BL.Globals.DemoData)
			{
				NavigationItem.Prompt = "Demonstration Mode";
			}
			else
			{
				NavigationItem.Prompt = null;
			}

            this.NavigationController.NavigationBar.Translucent = false;
            segView.ValueChanged += SegView_ValueChanged;
			segView.SelectedSegment = 2;
        }

        public override void ViewDidLoad ()
        {
            base.ViewDidLoad ();
        }

        public override void ViewDidAppear (bool animated)
        {
            base.ViewDidAppear (animated);
			Schedule.Frame = this.ContentView.Frame;
			this.View.AddSubview(Schedule);
			this.View.AddSubview(lblApptSummary);

        }

        public override void DidReceiveMemoryWarning ()
        {
            base.DidReceiveMemoryWarning ();
            // Release any cached data, images, etc that aren't in use.
        }

		// needed as schedule's constraints do not update along with the content view
		public override void DidRotate(UIKit.UIInterfaceOrientation fromInterfaceOrientation)
		{
			Schedule.Frame = this.ContentView.Frame;
			base.DidRotate(fromInterfaceOrientation);
		}

		#endregion

		#region private methods

        void SegView_ValueChanged (object sender, EventArgs e)
        {
            switch (segView.SelectedSegment) {
            case 0:
                Schedule.ScheduleView = SFScheduleView.SFScheduleViewDay;
					this.lblApptSummary.Hidden = true;
                break;
            case 1:
                Schedule.ScheduleView = SFScheduleView.SFScheduleViewWeek;
					this.lblApptSummary.Hidden = true;
                break;
            default:
                Schedule.ScheduleView = SFScheduleView.SFScheduleViewMonth;
					this.lblApptSummary.Hidden = true;
                break;
            }
        }

		// Initializes the schedule
		void InitializeSchedule()
		{
			Schedule = new SFSchedule();
			var scheduleDelegate = new ScheduleDelegate();
			scheduleDelegate.Parent = this;
			Schedule.Delegate = scheduleDelegate;
			Schedule.Calendar.FirstWeekDay = 2;
			Schedule.ScheduleView = SFScheduleView.SFScheduleViewMonth;

			WeekViewSettings weekSettings = new WeekViewSettings();
			weekSettings.WorkStartHour = 8;
			weekSettings.WorkEndHour = 18;
			weekSettings.ShowAllDay = true;

			DayViewSettings daySettings = new DayViewSettings();
			daySettings.WorkStartHour = 8;
			daySettings.WorkEndHour = 18;
			daySettings.ShowAllDay = true;

			DayLabelSettings labelSettings = new DayLabelSettings();
			labelSettings.TimeLabelSize = 8;
			labelSettings.DateLabelSize = 15;
			labelSettings.DayLabelSize = 15;
			labelSettings.DateLabelFormat = (NSString)"dd/MM/yy";
			labelSettings.DayLabelFormat = (NSString)" - EEEE";
			labelSettings.TimeLabelFormat = (NSString)"hh a";

			daySettings.LabelSettings = labelSettings;
			Schedule.DayViewSettings = daySettings;
			Schedule.WeekViewSettings = weekSettings;
		}

		#endregion

        public class ScheduleDelegate : SFScheduleDelegate
        {
			public CalendarViewController Parent
			{
				get;
				set;
			}

			public override void didSelectDate(SFSchedule schedule, NSDate selectedDate, NSArray appointments)
			{
				bool isMonthView = schedule.ScheduleView == SFScheduleView.SFScheduleViewMonth;

				this.Parent.lblApptSummary.Hidden = true;

				schedule.ScheduleView = SFScheduleView.SFScheduleViewDay;
				schedule.MoveToDate(selectedDate);
				this.Parent.segView.SelectedSegment = 0;

				if (!isMonthView && appointments != null && appointments.Count > 0)
				{
					foreach (var itm in NSArray.FromArray<ScheduleAppointment>(appointments))
					{
						string a = itm.Subject;

						this.Parent.lblApptSummary.Text = a;
					}

					this.Parent.lblApptSummary.Hidden = false;
				}
			}
		}
    }

}


