// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("TextInputViewController")]
    partial class TextInputViewController
    {
        [Outlet]
        UIKit.UITextField textInput { get; set; }


        [Outlet]
        UIKit.UILabel labelInput { get; set; }


        [Action ("textChanged:")]
        partial void textChanged (Foundation.NSObject sender);

        void ReleaseDesignerOutlets ()
        {
            if (labelInput != null) {
                labelInput.Dispose ();
                labelInput = null;
            }

            if (textInput != null) {
                textInput.Dispose ();
                textInput = null;
            }
        }
    }
}