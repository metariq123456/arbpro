// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/TextInputViewController/TextInputViewController.cs#10 $
// $Author: ivan $
// $Date: 2016/10/13 $
// $Change: 27237 $
// *********************************************************************
//
//
using System;
using CoreGraphics;
using System.Collections.Generic;
using Foundation;
using UIKit;
using System.Text.RegularExpressions;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public enum TextLookupSource
	{
		None,
		ItemType,
		WorkDescription,
		Tool,
		Hopsitals,
		RescuePlans
	}

	public partial class TextInputViewController : UIViewController
	{
		#region Constructor
		public TextInputViewController () : base ("TextInputViewController", null)
		{
			KeyboardType = UIKeyboardType.Default;
//            LookupSource = TextLookupSource.ItemType;
		}

		public TextInputViewController (bool isPassThrough) : base ("TextInputViewController", null)
		{
			KeyboardType = UIKeyboardType.Default;
			//The following flag is used to tell if the user should be able to pass through the current controller
			//and ont the Text picker view.
			_ISPassThrough = isPassThrough;
		}
		#endregion
		#region Delegates
		public delegate void DidInputTextDelegate (int Index, string Text);

		public event DidInputTextDelegate DidInputTextEvent;
		#endregion
		#region Private Properties
		bool _ISPassThrough;
		UIBarButtonItem __NavButtonDone;
		// TODO - remove this once we've determined we don't need it
//		UIBarButtonItem _PassThroughButton;
		#endregion
		#region Private Methods
		#endregion
		#region Public Properties
		public int Index { get; set; }

		public string CurrentText { get; set; }

		public UIKeyboardType KeyboardType { get; set; }

		public string ViewTitle { get; set; }

		public string InputLabel { get; set; }

		public TextLookupSource LookupSource { get; set; }

		public UITableView LookupValues;
		#endregion
		#region Actions
		partial void textChanged (NSObject sender)
		{
			//(LookupValues.Source as LookupTableViewSource).FilterText = textInput.Text;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Set the navigation bar to opaque, so that the controls don't slide under it on iOS7
			this.NavigationController.NavigationBar.Translucent = false;

			Title = ViewTitle;
			labelInput.Text = InputLabel;
			textInput.Placeholder = CurrentText;
			textInput.KeyboardType = KeyboardType;
			textInput.BecomeFirstResponder ();
			textInput.EditingChanged += TextInput_ValueChanged;

			if (LookupSource != TextLookupSource.None) {
				textInput.Delegate = new MyTextViewDelegate (this);
			}

			__NavButtonDone = new UIBarButtonItem (@"Done", UIBarButtonItemStyle.Bordered, ButtonDoneClicked);
			this.NavigationItem.RightBarButtonItem = __NavButtonDone;
			// TODO - remove this once we've determined we don't need it
			if (_ISPassThrough) {
//				_PassThroughButton = new UIBarButtonItem (@"Select", UIBarButtonItemStyle.Bordered, ButtonSelectClicked);
			}

			// The ShouldReturn event is called when the TextView has finished editing
			textInput.ShouldReturn += (textField) => {
				SaveAndPop ();
				return true;
			};


		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (BL.Globals.DemoData) {
				NavigationItem.Prompt = "Demonstration Mode";
			} else {
				NavigationItem.Prompt = null;
			}
			
			
//            if (LookupSource != null)
//            {
//                CreateLookupTable();
//            }
		}

		public override void ViewWillDisappear(bool animated)
		{
			textInput.ValueChanged -= TextInput_ValueChanged;
			base.ViewWillDisappear(animated);
		}

		void TextInput_ValueChanged(object sender, EventArgs e)
		{
			if (textInput.KeyboardType == UIKeyboardType.DecimalPad && !string.IsNullOrWhiteSpace(textInput.Text))
			{
				textInput.Text = Regex.Replace(textInput.Text, @"[^\d.,]" ,string.Empty);
				textInput.Text = Regex.Replace(textInput.Text, @"[.][.]", ".");
				textInput.Text = Regex.Replace(textInput.Text, @"[,][,]", ",");
			}
		}

		void ButtonSelectClicked (Object sender, EventArgs e)
		{
			if (DidInputTextEvent != null && textInput.Text.Length > 0)
				DidInputTextEvent (Index, textInput.Text);

			if (this.NavigationController != null)
				this.NavigationController.PopViewController (true);
		}

		void ButtonDoneClicked (Object sender, EventArgs e)
		{
			SaveAndPop ();
		}
		// Saves the edited text and pops the view controller
		void SaveAndPop ()
		{
			if (DidInputTextEvent != null && textInput.Text.Length > 0)
				DidInputTextEvent (Index, textInput.Text);

			if (this.NavigationController != null)
				this.NavigationController.PopViewController (true);
		}
		#endregion
		// A delegate for the textView, which allows us to detect - via the Should ChangeCharacters 
		// event - when to pop up the lookup TableView.
		class MyTextViewDelegate : UITextFieldDelegate
		{

			TextInputViewController _Controller;

			public MyTextViewDelegate (

				TextInputViewController controller) : base()
			{
				_Controller = controller;
			}

			// This event is raised when the OS is asking whether characters should be changed while typing.
			// We use it to determine whether the textbox is being typed into, and therefore whether to show the lookup tableview.
			public override bool ShouldChangeCharacters (UITextField textField, NSRange range, string replacementString)
			{
				_Controller.LookupValues.Hidden = false;
				return true;
			}

			public override bool ShouldReturn (UITextField textField)
			{
				_Controller.ButtonDoneClicked (null, null);
				return true;
			}
		}
		// The source for the lookup tableview.
		class LookupTableViewSource: UITableViewSource
		{
			TextInputViewController _Controller;
			// All the items in the list
			List<string> _FullItems;
			// The items once a filter has been applied
			List<string> _FilteredItems;
			// The text to which to filter the list
			public string FilterText {
				set {
					// Clear out the filtered list, and then only add those items which have the filter in their strings, comparing 
					// case-insensitively
					_FilteredItems.Clear ();
					foreach (var s in _FullItems) {
						if (value == "" || s.ToUpper ().Contains (value.ToUpper ()))
							_FilteredItems.Add (s);
					}
					// Refresh the lookup tableview
					_Controller.LookupValues.ReloadData ();

				}
			}
			#region Constructors
			public LookupTableViewSource (TextInputViewController controller, TextLookupSource sourceType)
			{
				_Controller = controller;
				_FullItems = new List<string> ();
				_FilteredItems = new List<string> ();

				// Initialise the item lists with the data from the database.
				switch (sourceType) {
				case TextLookupSource.ItemType:
					var ItemTypes = DAL.Repository.Instance ().ItemTypeList ();
					foreach (var Item in ItemTypes) {
						_FullItems.Add (Item.CommonName);
						_FilteredItems.Add (Item.CommonName);
					}
					break;
				case TextLookupSource.Tool:
					var Tools = DAL.Repository.Instance ().ToolList ();
					foreach (var Tool in Tools) {
						_FullItems.Add (Tool.Name);
						_FilteredItems.Add (Tool.Name);
					}
					break;
				case TextLookupSource.WorkDescription:
					var Descriptions = DAL.Repository.Instance ().ItemDescriptionList ();
					foreach (var Desc in Descriptions) {
						_FullItems.Add (Desc.Description);
						_FilteredItems.Add (Desc.Description);
					}
					break;
				case TextLookupSource.Hopsitals:
					var Hosptals = DAL.Repository.Instance ().HospitalList ();
					foreach (var h in Hosptals) {
						_FullItems.Add (h.Name);
						_FilteredItems.Add (h.Name);
					}
					break;
				case TextLookupSource.RescuePlans:
					var Plans = DAL.Repository.Instance ().AerialRescuePlanList ();
					foreach (var p in Plans) {
						_FullItems.Add (p.Plan);
						_FilteredItems.Add (p.Plan);
					}
					break;

				default:
					break;
				}

			}
			#endregion
			#region Overrides
			public override nint NumberOfSections (UITableView tableView)
			{
				return 1;
			}

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				_Controller.textInput.Text = _FilteredItems [indexPath.Row];
			}

			public override string TitleForHeader (UITableView tableView, nint section)
			{
				return "";
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				return _FilteredItems.Count;
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				string CELL_REF = "LookupListCell";
				UITableViewCell cell = tableView.DequeueReusableCell (CELL_REF);
				if (cell == null) {
					cell = new UITableViewCell (UITableViewCellStyle.Default, CELL_REF);
				}
				cell.TextLabel.Text = _FilteredItems [indexPath.Row];

				cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				return cell;
			}
			#endregion
		}
	}
}

