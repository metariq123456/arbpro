// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/ManagementViewController/ManagementViewController.cs#10 $
// $Author: James $
// $Date: 2015/10/18 $
// $Change: 25439 $
// *********************************************************************
//
//
using System;
using CoreGraphics;
using Foundation;
using UIKit;
using System.Threading.Tasks;
using DGS.ArbPro.Mobile.BL;
using DGS.ArbPro.Mobile.Core.Enums;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class ManagementViewController : UIViewController
	{

		#region Members

		//Define the section indexes for each table view section
		const int SECTION_PICTURES = 0;
		//Define the Row index for a section element
		//SECTION_PICTURES rows
		const int ROW_DELETEPICS = 0;

		#endregion

		#region Constructors

		public ManagementViewController () : base ("ManagementViewController", null)
		{
		}

		#endregion

		#region Overrides

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
				
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
				
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (BL.Globals.DemoData) {
				NavigationItem.Prompt = "Demonstration Mode";
			} else {
				NavigationItem.Prompt = null;
				ActivityView.ProgressView.DialogButtonClickedEvent += DialogButtonClicked;
			}

			this.Title = "Data Management";

			Table.Source = new ManagementTableSource (this);
			
			RefreshData ();
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
			this.Title = "Back";
		}

		#endregion

		#region Private Methods

		async void DeletePictures ()
		{
			if (await GetContinueConformation ("This process will delete all pictures stored by ArbPro not associated with a Quote. Do you want to continue?")) {
				try {
					ActivityView.ShowProgress ("Deleting Pictures");
					await DeletePicturesAsync ().ConfigureAwait (true);
					await ((ManagementTableSource)Table.Source).RefreshAsync ().ConfigureAwait (true);
					ActivityView.DisplayCompletedStatus ("Completed");
					//Reload the table view as the rows to be displayed may have changed.
					this.Table.ReloadData ();
				} catch (Exception) {
					ActivityView.DisplayCompletedStatus ("Error Deleting Pictures.");
				}
				this.NavigationController.PopViewController (true);
			}
		}

		async Task DeletePicturesAsync ()
		{
			int deletedCount = 0;
			deletedCount = await Task.Run (() => BL.BusinessLayer.DeleteOrphanedPictures ()).ConfigureAwait (false);
			if (deletedCount < 0)
				throw new Exception ("Error deleting pictures from device.");
		}

		async Task<bool> GetContinueConformation (string msg)
		{
			MessageBoxResult cont = await MessageBox.Show (msg, "Continue?", MessageBoxButton.YesNo);
			if (cont == MessageBoxResult.Yes)
				return true;
			else
				return false;

		}
		// TODO check whether we needed async on this
		void DialogButtonClicked (string buttonCaption)
		{
			if (buttonCaption == "Cancel") {
				BusinessLayer.CancelActivity = true;
				ActivityView.DisplayCompletedStatus ("Cancelled");
			} else {
				ActivityView.HideProgress ();
			}
		}

		void RefreshData ()
		{
			SetActivityStatus (true);
			RefreshDataAsync ();
		}

		void SetActivityStatus (bool working)
		{
			ActivityView.DisplayNetworkIndicator (!working);
		}

		async void RefreshDataAsync ()
		{
			await ((ManagementTableSource)Table.Source).RefreshAsync ().ConfigureAwait (true);
			Table.ReloadData ();
			SetActivityStatus (false);
		}

		#endregion

		#region TableViewSource

		class ManagementTableSource:UITableViewSource
		{

			#region Fields

			const string CELL_ID = "SubtitleCell";
			int _PictureCount = 0;
			ManagementViewController _Controller;

			#endregion

			#region Constructors

			public ManagementTableSource () :base()
			{
			}

			public ManagementTableSource (ManagementViewController controller):base()
			{
				_Controller = controller;
			}

			#endregion

			#region Actions

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				int section = indexPath.Section;

				switch (indexPath.Section) {
				case SECTION_PICTURES:
					PictureSectionSelected (indexPath.Row);
					break;
				}

			}

			#endregion

			#region Overrides

			public override nint NumberOfSections (UITableView tableView)
			{
				return 1;
			}

			public override string TitleForHeader (UITableView tableView, nint section)
			{
				switch (section) {
				case SECTION_PICTURES:
					return "Pictures";
				default:
					return string.Format ("bad section {0}", section); 
				}
			}

			public override nint RowsInSection (UITableView tableview, nint section)
			{
				switch (section) {
				case SECTION_PICTURES:
					return 1;
				default:
					return 0; 

				}
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				string PictureLabel;

				//Get or create a table cell as required
				UITableViewCell Cell;
				Cell = tableView.DequeueReusableCell (CELL_ID);
				if (Cell == null) {
					Cell = new UITableViewCell (UITableViewCellStyle.Subtitle, CELL_ID);
				}
				//Set default style
				Cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
				Cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				Cell.DetailTextLabel.Text = "";

				switch (indexPath.Section) {
				case SECTION_PICTURES:
					switch (indexPath.Row) {
					case ROW_DELETEPICS:
						Cell.TextLabel.Text = "Delete Files";

						if (_PictureCount < 0) {
							Cell.DetailTextLabel.Text = "Could not check for unassociated pictures.";
						} else if (_PictureCount == 0) {
							Cell.DetailTextLabel.Text = "No unassociated pictures found.";
						} else {
							PictureLabel = _PictureCount > 0 ? "pictures" : "picture";
							Cell.DetailTextLabel.Text = string.Format ("{0} {1} found", _PictureCount, PictureLabel);
						}

						if (_PictureCount <= 0) {
							Cell.Accessory = UITableViewCellAccessory.None;
						}
						break;
					default:
						Cell = SetCellErrorState (Cell, TitleForHeader (tableView, indexPath.Section), indexPath.Row);
						break;
					}
					break;
				default:
					Cell = SetCellErrorState (Cell, "Unknown", indexPath.Row);
					break;
				}



				return Cell;
			}

			#endregion

			#region Private Methods

			UITableViewCell SetCellErrorState (UITableViewCell cell, string sectionName, int rowIndex)
			{
				cell.TextLabel.Text = "Error creating element";
				cell.DetailTextLabel.Text = string.Format ("{0} section element {0} dose not exist", sectionName, rowIndex);
				cell.Accessory = UITableViewCellAccessory.None;
				return cell;
			}

			void PictureSectionSelected (int rowIndex)
			{
				switch (rowIndex) {
				case ROW_DELETEPICS:
					_Controller.DeletePictures ();
					break;
				}
			}

			#endregion

			#region Public Methods

			public async Task RefreshAsync ()
			{
				_PictureCount = await Task.Run (() => BL.BusinessLayer.GetOrphanedPicCount ()).ConfigureAwait (false);
			}

			#endregion

		}

		#endregion

	}
}

