// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("ManagementViewController")]
    partial class ManagementViewController
    {
        [Outlet]
        DGS.ArbPro.Mobile.iOS.UI.NetworkActivityView ActivityView { get; set; }


        [Outlet]
        UIKit.UITableView Table { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ActivityView != null) {
                ActivityView.Dispose ();
                ActivityView = null;
            }

            if (Table != null) {
                Table.Dispose ();
                Table = null;
            }
        }
    }
}