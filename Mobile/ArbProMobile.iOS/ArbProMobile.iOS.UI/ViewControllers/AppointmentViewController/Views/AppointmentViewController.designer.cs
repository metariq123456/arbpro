// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	[Register ("AppointmentViewController")]
	partial class AppointmentViewController
	{
		[Outlet]
		UIKit.UIButton editClientContact { get; set; }

		[Outlet]
		UIKit.UISegmentedControl segView { get; set; }

		[Outlet]
		UIKit.UITableView table { get; set; }

		[Action ("sevViewChanged:")]
		partial void sevViewChanged (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (table != null) {
				table.Dispose ();
				table = null;
			}

			if (segView != null) {
				segView.Dispose ();
				segView = null;
			}

			if (editClientContact != null) {
				editClientContact.Dispose ();
				editClientContact = null;
			}
		}
	}
}
