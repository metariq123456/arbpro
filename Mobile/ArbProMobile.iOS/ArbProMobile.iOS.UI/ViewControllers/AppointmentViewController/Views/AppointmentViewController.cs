// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/AppointmentViewController/Views/AppointmentViewController.cs#19 $
// $Author: ivan $
// $Date: 2016/09/21 $
// $Change: 27019 $
// *********************************************************************
//
//
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using UIKit;
using ObjCRuntime;
using DGS.ArbPro.Mobile.BL;
using DGS.ArbPro.Mobile;
using Microsec.Tools.iOS.UI;
using MessageUI;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class AppointmentViewController : UIViewController
    {
        public enum RiskLookUpSource
        {
            None,
            Hospitals,
            RescuePlans
        }

        #region Constructor

        public AppointmentViewController() : base("AppointmentViewController", null)
        {

        }

        #endregion

        static UIAlertView _Alert = null;


        #region Private Properties

        UIBarButtonItem __NavButtonSave;
        ItemsValueFooterView _ValueView;
        UISegmentedControl segStatus;

        enum RiskAssesmentProperty
        {
            ROW_RISK_SIGNAL = 0,
            ROW_RISK_HOSPITAL = 1,
            ROW_RISK_RESCUEACCESS = 2,
            ROW_RISK_AERIALPLAN = 3,
            ROW_RISK_GRIDREF = 4,
        }

        #endregion

        #region Public Properties

        public int AppointmentRecId{ get; set; }

        public UIButton AddButton;
        private Appointment _Appointment = null;

        public Appointment Appointment
        { 
            get
            {
                if (_Appointment == null)
                {
                    _Appointment = DAL.Repository.Instance().AppointmentGet(AppointmentRecId);
                }
                return _Appointment;
            } 
        }

        private Quote _Quote;

        public Quote Quote
        { 
            get
            {
                if (_Quote == null)
                {
                    _Quote = DAL.Repository.Instance().QuoteForAppoinmentGet(Appointment.QuoteNo);//.QuoteGet(Appointment.QuoteNo);
                }
                return _Quote;
            } 
        }

        #endregion

        #region DataSource

        class AppointmentSource :UITableViewSource
        {

            #region Members

            const int SEGMENT_ALL = -1;
            const int SEGMENT_APPOINTMENT = 0;
            const int SEGMENT_ITEMS = 1;
            const int SEGMENT_LOGISTICS = 2;
            const int SEGMENT_TOOLS = 3;
            const int SEGMENT_RISK = 4;
            const int SECTION_APPOINTMENT = 0;
            const int ROW_APPOINTMENT_DATE = 0;
            const int ROW_APPOINTMENT_TELEPHONE = 1;
            const int ROW_APPOINTMENT_MOBILE = 2;
            const int ROW_APPOINTMENT_ADDRESS = 3;
			const int ROW_APPOINTMENT_EMAIL = 4;
            const int ROW_APPOINTMENT_COMMENTS = 5;
            const int ROW_APPOINTMENT_IDS = 6;
            const int ROW_APPOINTMENT_STATUS = 7;
            const int SECTION_ITEMS = 1;
            const int SECTION_LOGISTICS = 2;
            const int SECTION_TOOLS = 3;
            const int SECTION_RISK_PROCEDURES = 0;
            const int ROW_RISK_SIGNAL = 0;
            const int ROW_RISK_HOSPITAL = 1;
            const int ROW_RISK_RESCUEACCESS = 2;
            const int ROW_RISK_AERIALPLAN = 3;
            const int ROW_RISK_GRIDREF = 4;
            const int SECTION_RISK_CRITERION = 1;
            const int SECTION_RISK_OTHER = 2;
            const int ROW_RISK_NEWOTHER = 0;
            const int SECTION_RISK_COMPLETE = 3;

            AppointmentViewController Controller;
            const string COMMENTS_CELL_NAME = "AppointmentCommentsCell";

            #endregion

            #region Constructors

            public AppointmentSource(AppointmentViewController controller)
            {
                Controller = controller;
            }

            #endregion

            #region Overrides

            public override nfloat GetHeightForHeader(UITableView tableView, nint section)
            {
                if (Controller.segView.SelectedSegment == SEGMENT_RISK)
                    return 30f;
                else
                    return 1f;
            }

            public override nint NumberOfSections(UITableView tableView)
            {
                if (Controller.segView.SelectedSegment == SEGMENT_ALL)
                {
                    return 5;
                } else if (Controller.segView.SelectedSegment == SEGMENT_RISK)
                {
                    return 5;
                }
                return 1;
            }

            public override string TitleForHeader(UITableView tableView, nint section)
            {
                if (Controller.segView.SelectedSegment == SEGMENT_ALL)
                {
                    switch (section)
                    {
                    case SECTION_APPOINTMENT:
                        return "Appointment";
                    case SECTION_ITEMS:
                        return "Items";

                    case SECTION_LOGISTICS:
                        return "Logistics";

                    case SECTION_TOOLS:
                        return "Tools";
                    default:
                        return "Risk Assessment";
                    }
                } else if (Controller.segView.SelectedSegment == SEGMENT_RISK)
                {
                    switch (section)
                    {
                    case SECTION_RISK_PROCEDURES:
                        return "Procedures";
                    case SECTION_RISK_CRITERION:
                        return "General";
                    case SECTION_RISK_OTHER:
                        return "Other Risks";
                    case SECTION_RISK_COMPLETE:
                        return "Completion";
                    default:
                        return "";
                    }
                } else
                {
                    return "";
                }
            }

            public override string TitleForFooter(UITableView tableView, nint section)
            {
                return "";
            }

            public override UIView GetViewForFooter(UITableView tableView, nint section)
            {
                nint seg = Controller.segView.SelectedSegment;

                if (seg == SEGMENT_ITEMS && Controller.Quote.Items.Count > 0)
                {
                    Controller.SetFooterView();
                    return Controller._ValueView;
                } else if (seg == SEGMENT_TOOLS && Controller.Quote.Tools.Count > 0)
                {
                    Controller.SetFooterDeleteMessageOnly();
                    return Controller._ValueView;
                }
                return null;
            }

            public override nfloat GetHeightForFooter(UITableView tableView, nint section)
            {
                nint seg = Controller.segView.SelectedSegment;

                if (seg == SEGMENT_ITEMS && Controller.Quote.Items.Count > 0)
                    return Controller._ValueView.Bounds.Height + 15f;
                else if (Controller.segView.SelectedSegment == SEGMENT_RISK)
                    return 0.1f;
                else
                    return 15f;
            }

			public override nint RowsInSection(UITableView tableview, nint section)
			{
				switch (Controller.segView.SelectedSegment)
				{
					case SEGMENT_ALL:
						return rowCountAll(section);
					case SEGMENT_APPOINTMENT:
						if (this.Controller.Appointment.ClientContactId > 0 || BL.Globals.DemoData)
						{
							this.Controller.editClientContact.Hidden = false;
						}
						return rowCountAppointment();
					case SEGMENT_ITEMS:
						this.Controller.editClientContact.Hidden = true;
						return rowCountItems();
					case SEGMENT_LOGISTICS:
						this.Controller.editClientContact.Hidden = true;
						return rowCountLogistics();
					case SEGMENT_TOOLS:
						this.Controller.editClientContact.Hidden = true;
						return rowCountTools();
					case SEGMENT_RISK:
						this.Controller.editClientContact.Hidden = true;
						return rowCountRisk(section);
					default:
						break;
				}
				return 0;
			}

            private nint rowCountAll(nint section)
            {
                switch (section)
                {
                case SECTION_APPOINTMENT:
                    return rowCountAppointment();
                case SECTION_ITEMS:
                    return rowCountItems();
                case SECTION_LOGISTICS:
                    return rowCountLogistics();
                case SECTION_TOOLS:
                    return rowCountTools();
                default:
                    break;
                }
                return 0;
            }

            private int rowCountAppointment()
            {
                return 8;
            }

            private int rowCountItems()
            {
                if (Controller.Quote != null)
                {
                    if (Controller.Appointment.Editable)
                    {
                        return Controller.Quote.Items.Count + 1;
                    } else
                    {
                        return Controller.Quote.Items.Count;

                    }
                } else
                {
                    return 0;
                }
            }

            private int rowCountLogistics()
            {
                return 3;
            }

            private int rowCountTools()
            {
                if (Controller.Quote != null && Controller.Quote.Tools != null)
                {
                    if (Controller.Appointment.Editable)
                    {
                        return Controller.Quote.Tools.Count + 1;
                    } else
                    {
                        return Controller.Quote.Tools.Count;
                    }
                      
                } else
                {
                    return 1;
                }
            }

            private nint rowCountRisk(nint section)
            {
                switch (section)
                {
                case SECTION_RISK_PROCEDURES:
                    return 5; 
                case SECTION_RISK_CRITERION:
                    return Controller.Quote.Assessment.Criterion.Count;
                case SECTION_RISK_OTHER:
                    if (Controller.Appointment.Editable)
                    {
                        return Controller.Quote.Assessment.FurtherRisks.Count + 1;
                    } else
                    {
                        return Controller.Quote.Assessment.FurtherRisks.Count;
                    }
                       
                case SECTION_RISK_COMPLETE:
                    return 1;
                default:
                    return 0;
                }
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                UITableViewCell cell;
                switch (Controller.segView.SelectedSegment)
                {
                case SEGMENT_ALL:
                    cell = tableCellAll(tableView, indexPath);
                    break;
                case SEGMENT_APPOINTMENT:
                    cell = tableCellAppointment(tableView, indexPath.Row, false);
                    break;
                case SEGMENT_ITEMS:
                    cell = tableCellItem(tableView, indexPath.Row, false);
                    break;
                case SEGMENT_LOGISTICS:
                    cell = tableCellLogistic(tableView, indexPath.Row, false);
                    break;
                case SEGMENT_TOOLS:
                    cell = tableCellTool(tableView, indexPath.Row, false);
                    break;
                case SEGMENT_RISK:
                    cell = tableCellRisk(tableView, indexPath);
                    break;
                default:
                    cell = new UITableViewCell();
                    break;
                }

                cell.SelectionStyle = UITableViewCellSelectionStyle.None;
                return cell;
            }

            public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
            {
                Console.WriteLine(string.Format("section {0} row {1}", indexPath.Section, indexPath.Row));
                if (Controller.segView.SelectedSegment == SEGMENT_ITEMS && indexPath.Row > 0)
                {
                    return true;
                } else if (Controller.segView.SelectedSegment == SEGMENT_TOOLS && indexPath.Row > 0)
                {
                    return true;
                } else
                {
                    return false;
                }
            }

            public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
            {
                switch (editingStyle)
                {
                case UITableViewCellEditingStyle.Delete:
                    switch (Controller.segView.SelectedSegment)
                    {
                    case SEGMENT_ITEMS:
                        if (Controller.Appointment.Editable)
                        {
                            Controller.Quote.DeleteItem(Controller.Quote.Items [indexPath.Row - 1]);
                            tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);
                            Controller.SetFooterView();
                            Controller.SetDataDirty();
                        }
                        break;
                    case SEGMENT_TOOLS:
                        if (Controller.Appointment.Editable)
                        {
                            Controller.Quote.DeleteTool(Controller.Quote.Tools [indexPath.Row - 1]);
                            tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Fade);
                            Controller.SetDataDirty();
                        }
                        break;
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }

            }

            #endregion

            #region Private Methods

            private UITableViewCell tableCellAll(UITableView tableView, NSIndexPath indexPath)
            {
                UITableViewCell cell;
                switch (indexPath.Section)
                {
                case SECTION_APPOINTMENT:
                    cell = tableCellAppointment(tableView, indexPath.Row, true);
                    break;
                case SECTION_ITEMS:
                    cell = tableCellItem(tableView, indexPath.Row, true);
                    break;
                case SECTION_LOGISTICS:
                    cell = tableCellLogistic(tableView, indexPath.Row, true);
                    break;
                case SECTION_TOOLS:
                    cell = tableCellTool(tableView, indexPath.Row, true);
                    break;
                default:
                    cell = tableCellRisk(tableView, indexPath);
                    break;
                }
                return cell;
            }

            private UITableViewCell tableCellAppointment(UITableView tableView, int row, bool summary)
            {
                string CELL_REF = "appointmentCell";
                UITableViewCell cell;

                cell = tableView.DequeueReusableCell(CELL_REF);
                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Value1, CELL_REF);
                }
                cell.Accessory = UITableViewCellAccessory.None;
                cell.AccessoryView = null;

                switch (row)
                {
                case ROW_APPOINTMENT_DATE:
                    cell.TextLabel.Text = "Date";
                    cell.DetailTextLabel.Text = Controller.Appointment.StartDateTime.ToString("dd/MM/yyyy HH:mm");
                    break;
                case ROW_APPOINTMENT_TELEPHONE:
                    cell.TextLabel.Text = "Telephone";
                    cell.DetailTextLabel.Text = Controller.Appointment.ClientTelephone.ToString();
                    NSUrl telephoneURL = new NSUrl(@"tel://" + Controller.Appointment.ClientTelephone.ToString().Replace(" ", "").Trim());
                    if (UIApplication.SharedApplication.CanOpenUrl(telephoneURL))
                    {
                        cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    }
                    break;
                case ROW_APPOINTMENT_MOBILE:
                    cell.TextLabel.Text = "Mobile";

					// We need to handle the null ClientMobile, for the situation where the server is not supplying this field yet - so it won't 
					// default to an empty string.
                    string clientMobile;

                    if (Controller.Appointment.ClientMobile != null)
                    {
                        clientMobile = Controller.Appointment.ClientMobile.ToString();
                    } else
                    {
                        clientMobile = "";
                    }

                    if (clientMobile != "")
                    {
                        cell.DetailTextLabel.Text = clientMobile;
                        NSUrl mobileURL = new NSUrl(@"tel://" + clientMobile.Replace(" ", "").Trim());
                        if (UIApplication.SharedApplication.CanOpenUrl(mobileURL))
                        {
                            cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                        }
                    }
                    break;
                case ROW_APPOINTMENT_ADDRESS:
                    cell.TextLabel.Text = "Address";
                    cell.DetailTextLabel.Text = Controller.Appointment.ClientNameAndAddress;
                    if (Controller.Appointment.ClientAddressLocation != null)
                    {
                        cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    }
                    break;
					case ROW_APPOINTMENT_EMAIL:
						cell.TextLabel.Text = "Email";
						cell.DetailTextLabel.Text = Controller.Appointment.ClientEmail;
						if (!string.IsNullOrWhiteSpace(Controller.Appointment.ClientEmail))
						{
							cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
						}
						break;
                case ROW_APPOINTMENT_COMMENTS:
                    cell.TextLabel.Text = "Comments";
                    cell.DetailTextLabel.Text = Controller.Appointment.Comments;
                    if (!string.IsNullOrEmpty(Controller.Appointment.Comments))
                    {
                        cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    }
                    break;
                case ROW_APPOINTMENT_IDS:
                    cell.TextLabel.Text = "ID";
                    cell.DetailTextLabel.Text = Controller.Appointment.IdDisplayString();
                    break;
                case ROW_APPOINTMENT_STATUS:
                    cell.TextLabel.Text = "Status";
                    cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    cell.AccessoryView = Controller.segStatus;
                    break;
                default:
                    break;
                }

                return cell;
            }

            private UITableViewCell tableCellItem(UITableView tableView, int row, bool summary)
            {
                string CELL_REF = "itemCell";
                bool AddCell = true;

                UITableViewCell cell = tableView.DequeueReusableCell(CELL_REF);
                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Subtitle, CELL_REF);
                    cell.Accessory = UITableViewCellAccessory.None;
                }

                cell.AccessoryView = null;

                if (row == 0 && Controller.Appointment.Editable)
                {
                    Controller.AddButton = new UIButton(UIButtonType.ContactAdd);
                    Controller.AddButton.AddTarget(Controller.ItemAdd, UIControlEvent.TouchUpInside);
                    cell.AccessoryView = Controller.AddButton;

                    cell.TextLabel.Text = "Add Quote Item";
                } else
                {
                  
                    QuoteItem item = Controller.Quote.Items [Controller.Appointment.Editable ? row - 1:row];
                    if (item.ItemNumber != null && item.ItemNumber.Length > 0)
                        cell.TextLabel.Text = string.Format("{0} - {1}", item.ItemNumber, item.ItemType);
                    else
                        cell.TextLabel.Text = item.ItemType;
						
                    cell.DetailTextLabel.Text = string.Format("({0}) {1:C} - {2}", item.PreservationOrderShortStatus, item.ItemValue, item.WorkDescription);

                    if (!summary)
                    {
                        cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    }
                }

                return cell;
            }

            private UITableViewCell tableCellLogistic(UITableView tableView, int row, bool summary)
            {
                string CELL_REF = "logisticsCell";
                UITableViewCell cell = tableView.DequeueReusableCell(CELL_REF);
                
                
                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Value1, CELL_REF);
                    cell.Accessory = UITableViewCellAccessory.None;
                }

                cell.AccessoryView = null;

                switch (row)
                {
                case 0:
                    cell.TextLabel.Text = "Hours";
                    cell.DetailTextLabel.Text = Controller.Quote.TimeHrs.ToString();
                    if (Controller.Appointment.Editable)
                    {
                        cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    }
                    break;
                case 1:
                    cell.TextLabel.Text = "Staff";
                    cell.DetailTextLabel.Text = Controller.Quote.StaffNumber.ToString();
                    if (Controller.Appointment.Editable)
                    {
                        cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    }
                    break;
                case 2:
                    cell.TextLabel.Text = "Notes";
                    if (Controller.Appointment.Editable)
                    {
                        cell.DetailTextLabel.Text = Controller.Quote.Comments != null && Controller.Quote.Comments.Length > 0 ? "View":"Add";
                    } else
                    {
                        cell.DetailTextLabel.Text = "View";
                    }

                    cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    break;
                default:
                    break;
                }

                return cell;
            }

            private UITableViewCell tableCellTool(UITableView tableView, int row, bool summary)
            {
                string CELL_REF = "toolCell";
                UITableViewCell cell = tableView.DequeueReusableCell(CELL_REF);
                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Subtitle, CELL_REF);
                    cell.Accessory = UITableViewCellAccessory.None;
                }

                cell.AccessoryView = null;

                if (row == 0 && Controller.Appointment.Editable)
                {
                    Controller.AddButton = new UIButton(UIButtonType.ContactAdd);
                    Controller.AddButton.AddTarget(Controller.DisplayToolSelection, UIControlEvent.TouchUpInside);
                    cell.AccessoryView = Controller.AddButton;

                    cell.TextLabel.Text = "Add Tool";
                } else
                {

                    if (Controller.Quote != null && Controller.Quote.Tools != null && Controller.Quote.Tools.Count > 0)
                    {
                        cell.TextLabel.TextColor = UIColor.Black;
                        cell.TextLabel.Text = Controller.Quote.Tools [Controller.Appointment.Editable ? row - 1:row].Name;
                        cell.DetailTextLabel.Text = Controller.Quote.Tools [Controller.Appointment.Editable ? row - 1:row].Number.ToString();

                        if (!summary)
                        {
                            if (Controller.Appointment.Editable)
                            {
                                cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                            }
                        }
                    }

                }
                return cell;
            }

            private UITableViewCell tableCellRisk(UITableView tableView, NSIndexPath indexPath)
            {
                string CELL_REF = "riskCell";
                UITableViewCell cell = tableView.DequeueReusableCell(CELL_REF);
                if (cell == null)
                {
                    cell = new UITableViewCell(UITableViewCellStyle.Subtitle, CELL_REF);
                }
				
                cell.AccessoryView = null;
				
                RiskAssessment risk = Controller.Quote.Assessment;
                string Text = "";
                switch (indexPath.Section)
                {
                case SECTION_RISK_PROCEDURES:
                    switch (indexPath.Row)
                    {
                    case ROW_RISK_SIGNAL:
                        cell.TextLabel.Text = "Mobile Signal Level";
                        cell.DetailTextLabel.Text = risk.MobileSignal;
                        break;
                    case ROW_RISK_HOSPITAL:
                        cell.TextLabel.Text = "Nearest A&E Hospital";
                        Text = risk.HospitalName != null ? risk.HospitalName:"";
                        cell.DetailTextLabel.Text = Text;
                        break;
                    case ROW_RISK_RESCUEACCESS:
                        cell.TextLabel.Text = "Rescue Access";
                        cell.DetailTextLabel.Text = risk.RescueAccess;
                        break;
                    case ROW_RISK_AERIALPLAN:
                        cell.TextLabel.Text = "Aerial Rescue Plan";
                        Text = risk.AerialRescueDetails != null ? risk.AerialRescueDetails:"";
                        cell.DetailTextLabel.Text = Text;
                        break;
                    case ROW_RISK_GRIDREF:
                        cell.TextLabel.Text = "Grid Reference";
                        cell.DetailTextLabel.Text = risk.GridReference;
                        break;
                    default:
                        break;
                        if (Controller.Appointment.Editable)
                        {
                            cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                        }
                    }
                    break;
                case SECTION_RISK_CRITERION:
                    cell.TextLabel.Text = risk.Criterion [indexPath.Row].Title;
                    cell.DetailTextLabel.Text = string.Format("Outstanding: {0} Identified: {1}", (risk.Criterion [indexPath.Row].AssessmentItems.FindAll(itm => itm.ControlRequired == BL.AssessmentItem.ControlRequiredType.Unknown).Count.ToString()), (risk.Criterion [indexPath.Row].AssessmentItems.FindAll(itm => itm.ControlRequired == BL.AssessmentItem.ControlRequiredType.Yes)).Count.ToString());
                    cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    break;
                case SECTION_RISK_OTHER:
                    if (indexPath.Row == 0)
                    {
                        if (Controller.Appointment.Editable)
                        {
                            Controller.AddButton = new UIButton(UIButtonType.ContactAdd);
                            Controller.AddButton.AddTarget(Controller.RiskAdd, UIControlEvent.TouchUpInside);
                            cell.AccessoryView = Controller.AddButton;

                            cell.TextLabel.Text = "Add";
                            cell.DetailTextLabel.Text = "";
                        }
                    } else
                    {
                        cell.TextLabel.Text = string.Format("Additional Risk {0}", indexPath.Row.ToString());
                        string label = risk.FurtherRisks [indexPath.Row - 1].Detail == null || risk.FurtherRisks [indexPath.Row - 1].Detail.Length == 0 ? "New Risk":risk.FurtherRisks [indexPath.Row - 1].Detail;
                        cell.DetailTextLabel.Text = label;
                        if (Controller.Appointment.Editable)
                        {
                            cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                        }
                    }
                    break;
                case SECTION_RISK_COMPLETE:
                    switch (indexPath.Row)
                    {
                    case 0:
                        cell.TextLabel.Text = "Signature";
                        cell.DetailTextLabel.Text = risk.CompletionDate != DateTime.MinValue ? string.Format("Signed", risk.CompletionDate.ToShortDateString()):"Awaiting Signature";
                        if (Controller.Appointment.Editable)
                        {
                            cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                        }
                        break;
                    default:
                        break;
                    }
                    break;
                }
				
                return cell;
            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                //Address row selected
                if (Controller.segView.SelectedSegment == SEGMENT_APPOINTMENT && indexPath.Row == ROW_APPOINTMENT_ADDRESS)
                {
                    Controller.DisplayAddressMap();
                }

                if (Controller.segView.SelectedSegment == SEGMENT_APPOINTMENT && indexPath.Row == ROW_APPOINTMENT_COMMENTS)
                {
                    Controller.DisplayAppointmentComments();
                }

                if (Controller.segView.SelectedSegment == SEGMENT_APPOINTMENT && indexPath.Row == ROW_APPOINTMENT_TELEPHONE)
                {
                    Controller.MakeCall(Controller.Appointment.ClientTelephone.ToString());
                }

                if (Controller.segView.SelectedSegment == SEGMENT_APPOINTMENT && indexPath.Row == ROW_APPOINTMENT_MOBILE)
                {
                    if (Controller.Appointment.ClientMobile != null)
                    {
                        Controller.MakeCall(Controller.Appointment.ClientMobile.ToString());
                    }
                }

				if (Controller.segView.SelectedSegment == SEGMENT_APPOINTMENT && indexPath.Row == ROW_APPOINTMENT_EMAIL)
				{
					Controller.SendEmail(Controller.Appointment.ClientEmail);
				}

                //Item row selected - note the first row is the "Add Item" row when the appointment is editable.
                if (Controller.segView.SelectedSegment == SEGMENT_ITEMS && (indexPath.Row > 0 || Controller.Appointment.Editable == false))
                {
                    Controller.DisplayItem(Controller.Appointment.Editable ? indexPath.Row - 1:indexPath.Row);
                }

                //Tool row selected - note that the first row is the "Add Tool" row when the appointment is editable.
                if (Controller.segView.SelectedSegment == SEGMENT_TOOLS && (indexPath.Row > 0 && Controller.Appointment.Editable == true))
                {
                    Controller.DisplayTool(indexPath.Row - 1);
                }
                
				
                //Risk Assesment Selected
                if (Controller.segView.SelectedSegment == SEGMENT_RISK)
                {
                    switch (indexPath.Section)
                    {
                    case SECTION_RISK_PROCEDURES:
                        if (Controller.Appointment.Editable)
                        {
                            Controller.DisplayRiskEntry(indexPath.Section, indexPath.Row);
                        }
                        break;
                    case SECTION_RISK_CRITERION:
                        Controller.DisplayRiskCriterion(indexPath.Row);
                        break;
                    case SECTION_RISK_OTHER:
                        Controller.DisplayRiskOther(indexPath.Row);
                        break;
							
                    case SECTION_RISK_COMPLETE:
                        if (Controller.Appointment.Editable)
                        {
                            Controller.DisplayRiskSignature();
                        }
                        break;
                    default:
                        break;
                    }
                }
				
                //Logistics row selected
                if (Controller.segView.SelectedSegment == SEGMENT_LOGISTICS)
                {
 
                    switch (indexPath.Row)
                    {
                    case 0:
                        if (Controller.Appointment.Editable)
                        {
                            Controller.DisplayTextInput(indexPath.Row, Controller.Quote.TimeHrs.ToString(), "Hours", UIKeyboardType.NumberPad);
                        }
                        break;
                    case 1:
                        if (Controller.Appointment.Editable)
                        {
                            Controller.DisplayTextInput(indexPath.Row, Controller.Quote.StaffNumber.ToString(), "Staff", UIKeyboardType.NumberPad);
                        }
                        break;
                    case 2:
                        Controller.DisplayNotesInput(indexPath.Row, Controller.Quote.Comments, UIKeyboardType.Default);
                        break;
                    default:
                        break;
                    }

                }

            }

            #endregion

        }

        #endregion

        #region Callbacks

        protected void HandleStatusChanged(object sender, StatusEventArgs e)
        {
            switch (e.SelectedSegment)
            {
            case 0:
                Appointment.Status = AppointmentStatus.Pending;
                break;
            case 1:
                Appointment.Status = AppointmentStatus.Completed;
                break;
            default:
                break;
            }

            SetDataDirty();
        }

        #endregion

        #region Actions

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            CreateStatusSegmentControl();
            
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            SetupNavigationBar();
            table.Source = new AppointmentSource(this);
            
            if (_ValueView == null)
            {
                IntPtr intptr_1 = NSBundle.MainBundle.LoadNib("ItemsValueFooterView", null, null).ValueAt(0);
                _ValueView = new ItemsValueFooterView(intptr_1);
            }

            table.ReloadData();

            if (BL.Globals.DemoData)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            } else
            {
                NavigationItem.Prompt = null;
            }

			if (this.Appointment.ClientContactId > 0 || BL.Globals.DemoData)
			{
				this.editClientContact.TouchUpInside += EditClientContact_TouchUpInside;
			}
			else
			{
				this.editClientContact.Hidden = true;
			}

            // We need to test for quote being dirty after QuoteItem or QuoteTool editing.
            if (Quote.Dirty || Quote.Assessment.IsDirty)
            {
                NavigationItem.RightBarButtonItem = __NavButtonSave;
            }
        }

        public override void ViewWillDisappear(bool animated)
        {
            bool found = false;

            foreach (UIViewController vc in NavigationController.ViewControllers)
            {
                if (vc == this)
                {
                    found = true;
                    break;
                }
            }

            if (!found && (Quote.Dirty || Appointment.Dirty))
            {

                _Alert = new UIAlertView("Appointment", string.Format("Appointment for Quote {0} has been edited do you want to save the changes?", Quote.QuoteId), new SaveQuoteAlertDelegate(Appointment, Quote), "No", new string[1]{ "Yes" });
                _Alert.Show();
            }

			this.editClientContact.TouchUpInside -= EditClientContact_TouchUpInside;

            base.ViewWillDisappear(animated);
            //this.Title = "Back";
        }

        public void ButtonSaveClicked(Object sender, EventArgs e)
        {
            int AppointmentReturnValue;
            int QuoteReturnValue;

            AppointmentReturnValue = DAL.Repository.Instance().AppointmentSave(Appointment);
            QuoteReturnValue = DAL.Repository.Instance().QuoteSave(Quote);

            if (AppointmentReturnValue > 0 && QuoteReturnValue > 0)
            {
                //__NavButtonSave.Style = UIBarButtonItemStyle.Plain;
                NavigationItem.RightBarButtonItem = null;
            }

        }

        public void ButtonBackClicked(Object sender, EventArgs e)
        {
            int AppointmentReturnValue;
            int QuoteReturnValue;

            AppointmentReturnValue = DAL.Repository.Instance().AppointmentSave(Appointment);
            QuoteReturnValue = DAL.Repository.Instance().QuoteSave(Quote);

            if (AppointmentReturnValue > 0 && QuoteReturnValue > 0)
            {
                //__NavButtonSave.Style = UIBarButtonItemStyle.Plain;
                NavigationItem.RightBarButtonItem = null;
            }

        }

        public void ItemAdd(Object sender, EventArgs e)
        {
            QuoteItem item = new QuoteItem();
            item.ItemType = "New Quote Item";
            Quote.Items.Add(item);
            SetDataDirty();
            DisplayItem(Quote.Items.Count - 1);
        }

        public void ToolsAdd(List<QuoteTool> selectedTools)
        {
            try
            {
                Quote.Tools.AddRange(selectedTools);
            } catch (Exception ex)
            {
                Console.WriteLine("ToolsAdd Error: {0}", ex.ToString());
            }
            SetDataDirty();
        }

        public void RiskAdd(Object sender, EventArgs e)
        {
            AssessmentAdditionalItem item = new AssessmentAdditionalItem();
            item.Detail = "";
            Quote.Assessment.FurtherRisks.Add(item);
            SetDataDirty();

            DisplayRiskOther(Quote.Assessment.FurtherRisks.Count);
        }

        partial void sevViewChanged(Foundation.NSObject sender)
        {
            SetupNavigationBar();
            table.ReloadData();
        }

        #endregion

        #region Public Methods

        public void SetFooterView()
        {
            _ValueView.DisplayDeleteOnly(false);
            _ValueView.SetItemValues(this.Quote.GetQuoteValue(), Settings.VatValue);
        }

        public void SetFooterDeleteMessageOnly()
        {
            if (_ValueView != null)
                _ValueView.DisplayDeleteOnly(true);
        }

        public void MakeCall(string phoneNumber)
        {
            MakeCallAlertDelegate alertDelegate = new MakeCallAlertDelegate(phoneNumber);
            UIAlertView alert = new UIAlertView("Confirm", "Are you sure you want to call\r" + phoneNumber, alertDelegate, "No", "Yes");
            alert.Show();
        }

		public void SendEmail(string emailAddress)
		{
			MFMailComposeViewController mailController;

			if (MFMailComposeViewController.CanSendMail)
			{
				mailController = new MFMailComposeViewController();

				mailController.SetToRecipients(new string[] { emailAddress });

				mailController.Finished += (object s, MFComposeResultEventArgs args) =>
				{
					Console.WriteLine(args.Result.ToString());
					args.Controller.DismissViewController(true, null);
				};

				this.PresentViewController(mailController, true, null);
			}
			else
			{
				UIAlertView alert = new UIAlertView();
				alert.Title = @"Email not set up";
				alert.AddButton("Ok");
				alert.Message = "It appears you do not have email set up or you cannot send emails presently.";
				alert.Show();
			}
		}

        public void DisplayAddressMap()
        {
            if (Appointment.ClientAddressLocation != null)
            {
                MapViewController controller = new MapViewController();
                GeoLocation addressLocation = Appointment.ClientAddressLocation;
                addressLocation.Title = Appointment.ClientNameAndAddress;
                controller.Locations = new GeoLocation[] { Appointment.ClientAddressLocation };
                controller.CaptureMode = false;
                controller.ShowAddress = true;

                NavigationController.PushViewController(controller, true);
            }
        }

        public void DisplayAppointmentComments()
        {
            if (!string.IsNullOrEmpty(Appointment.Comments))
            {
                TextViewViewController controller = new TextViewViewController();
                controller.Text = Appointment.Comments;
                controller.Title = "Appointment Comments";
                NavigationController.PushViewController(controller, true);
            }
        }

        /// <summary>
        /// Displays the item indicated by the index passed in
        /// </summary>
        /// <param name="index">Index.</param>
        /// <remarks>
        /// The index is the index of the item in the Items list for the quote.</remarks>
        public void DisplayItem(int index)
        {
            NavigationItem.Title = "Items";
            QuoteItemViewController controller = new QuoteItemViewController();
            controller.Appointment = Appointment;
            controller.Quote = Quote;
            controller.Appointment = Appointment;

            controller.ItemIndex = index;

            NavigationController.PushViewController(controller, true);
        }

        public void DisplayToolSelection(object sender, EventArgs e)
        {
            NavigationItem.Title = "Tools";

            ToolPickerViewController controller = new ToolPickerViewController(Quote.Tools);
            controller.ToolsSelectedEvent += ToolsAdd;
            NavigationController.PushViewController(controller, true);
        }

        public void DisplayTool(int toolIndex)
        {
            NavigationItem.Title = "Tools";
            QuoteToolViewController controller = new QuoteToolViewController();
            controller.Quote = Quote;
            controller.ToolIndex = toolIndex;
            NavigationController.PushViewController(controller, true);
        }

        public void DisplayTextPicker(string CurrentText, string InputLabel, UIKeyboardType KeyboardType, TextLookupSource lookupSource)
        {
            TextPickerViewController controller = new TextPickerViewController();

            List<string> entries = new List<string>();
            string title = "";
            switch (lookupSource)
            {
            case TextLookupSource.Hopsitals:
                var hospitals = DAL.Repository.Instance().HospitalList();
                foreach (RiskHospital h in hospitals)
                {
                    entries.Add(h.Id + ": " + h.Name);
                }
                title = "A&E Hosptials";
                break;
            case TextLookupSource.RescuePlans:
                var plans = DAL.Repository.Instance().AerialRescuePlanList();
                foreach (RiskAerialRescuePlan p in plans)
                {
                    entries.Add(p.Id + ": " + p.Plan);
                }
                title = "Plans";
                break;
            }

            controller.LookupItems = entries;
            controller.DefaultValue = CurrentText;
            controller.Index = (int)lookupSource;
            controller.ViewTitle = title;
            controller.InputLabel = InputLabel;
            controller.DidPickTextEvent += TextSelectedSaved;

            NavigationController.PushViewController(controller, true);
        }

        public void DisplayRiskEntry(int section, int row)
        {
            if (section > 0)
                return;
				
            var property = (RiskAssesmentProperty)row;//((section * 2) + row);
            switch (property)
            {
            case RiskAssesmentProperty.ROW_RISK_AERIALPLAN:
                string plan = Quote.Assessment.AerialRescueDetails != null ? Quote.Assessment.AerialRescueDetails:"";
                DisplayTextPicker(plan, "", UIKeyboardType.Default, TextLookupSource.RescuePlans);
                break;
            case RiskAssesmentProperty.ROW_RISK_GRIDREF:
				//TODO: Modify to be map view controller
                DisplayRiskLocationService();
				//DisplayRiskTextInput ((int)property, Quote.Assessment.GridReference, "Grid Ref", "Grid Reference", UIKeyboardType.Default);
                break;
            case RiskAssesmentProperty.ROW_RISK_HOSPITAL:
                string hospital = Quote.Assessment.HospitalName != null ? Quote.Assessment.HospitalName:"";
                DisplayTextPicker(hospital, "", UIKeyboardType.Default, TextLookupSource.Hopsitals);
                break;
            case RiskAssesmentProperty.ROW_RISK_RESCUEACCESS:
                DisplayNotesInput((int)property, Quote.Assessment.RescueAccess, "Rescue Access", "Site Access for Rescue", UIKeyboardType.Default);
                break;
            case RiskAssesmentProperty.ROW_RISK_SIGNAL:
                string provider = "";
                int level = 0;
                if (Quote.Assessment.MobileSignal != null && Quote.Assessment.MobileSignal.IndexOf(':') > 0)
                {
                    string[] signalText = Quote.Assessment.MobileSignal.Split(':');
                    provider = signalText [0].Trim();
                    if (signalText.Length > 1)
                    {
                        if (signalText [1].IndexOf("Poor") > -1)
                            level = 1;
                        else if (signalText [1].IndexOf("Average") > -1)
                            level = 2;
                        else if (signalText [1].IndexOf("Good") > -1)
                            level = 3;
                    }
                }
                DisplayMobileSignalLevelInput(provider, level);
                break;
            default:
                break;
            }
        }

        public void DisplayRiskCriterion(int row)
        {
            RiskCriterionViewController controller = new RiskCriterionViewController(this.Quote.Assessment, row);
            controller.Enabled = Appointment.Editable;
            this.NavigationController.PushViewController(controller, true);
        }

        public void DisplayRiskOther(int row)
        {
            int index = row - 1;
            BL.AssessmentAdditionalItem otherItem = null;
            if (row == 0 && Appointment.Editable)
            {
                otherItem = new AssessmentAdditionalItem();
                this.Quote.Assessment.FurtherRisks.Add(otherItem);
                index = this.Quote.Assessment.FurtherRisks.Count - 1;
            }
			
            RiskOtherViewController controller = new RiskOtherViewController(this.Quote.Assessment, index);
            this.NavigationController.PushViewController(controller, true);
			
        }

        public void DisplayRiskSignature()
        {
            ImageDisplayViewController controller = new ImageDisplayViewController(ImageEditTypeEnum.Signature);
            controller.ImageDeletedEvent += DeleteSignatureAndMarkIncomplete;
            controller.ImageSavedEvent += SaveImageAndCompleteAssessment;
			
            if (File.Exists(SignatureFileName()))
            {
                controller.displayImage = UIImage.FromFile(SignatureFileName());
            }
			
            this.NavigationController.PushViewController(controller, true);
        }

        public void DisplayRiskTextInput(int Index, string CurrentText, string ViewTitle, string InputLabel, UIKeyboardType KeyboardType)
        {
            TextInputViewController controller = new TextInputViewController();
            controller.ViewTitle = ViewTitle;//"Logistics";
            controller.InputLabel = InputLabel;
            controller.Index = Index;
            controller.CurrentText = CurrentText;
            controller.KeyboardType = KeyboardType;
            controller.DidInputTextEvent += RiskTextInputSaved;

            NavigationController.PushViewController(controller, true);
        }

        public void DisplayTextInput(int Index, string CurrentText, string InputLabel, UIKeyboardType KeyboardType)
        {
            TextInputViewController controller = new TextInputViewController();
            controller.ViewTitle = InputLabel;//"Logistics";
            controller.Index = Index;
            controller.CurrentText = CurrentText;
            controller.InputLabel = "";//InputLabel;
            controller.KeyboardType = KeyboardType;
            controller.DidInputTextEvent += TextInputSaved;

            NavigationController.PushViewController(controller, true);
        }

        public void DisplayNotesInput(int Index, string CurrentText, UIKeyboardType KeyboardType)
        {
            NotesInputViewController controller = new NotesInputViewController();
            controller.Enabled = Appointment.Editable;
            controller.ViewTitle = "Notes";
            controller.Index = Index;
            controller.CurrentText = CurrentText;
            controller.KeyboardType = KeyboardType;
            controller.DidInputTextEvent += TextInputSaved;


            NavigationController.PushViewController(controller, true);
        }

        public void DisplayNotesInput(int Index, string CurrentText, string ViewTitle, string InputLabel, UIKeyboardType KeyboardType)
        {
            NotesInputViewController controller = new NotesInputViewController();
            controller.Enabled = Appointment.Editable;
            controller.ViewTitle = ViewTitle;
            controller.Index = Index;
            controller.CurrentText = CurrentText;
            controller.KeyboardType = KeyboardType;
            controller.DidInputTextEvent += RiskTextInputSaved;

            NavigationController.PushViewController(controller, true);
        }

        public void DisplayMobileSignalLevelInput(string provider, int level)
        {
            SignalLevelViewController controller = new SignalLevelViewController(provider, (SignalLevelEnum)level);
            controller.Enabled = Appointment.Editable;

            controller.SignalLevelSetEvent += RiskMobileSignalSet;
            NavigationController.PushViewController(controller, true);
        }

        public void DisplayRiskLocationService()
        {
			
            MapViewController controller = new MapViewController();
            
            GeoLocation location = Quote.Assessment.GetLocation();
            if (location != null)
            {
                controller.InitialCentre = location;
                controller.Locations = new GeoLocation[] { location };
            }
			
            controller.CenterOnInitialLocation = true;
            controller.CaptureMode = true;
            controller.LocationCapturedEvent += LocationCaptured;
            controller.ShowAddress = true;

            NavigationController.PushViewController(controller, true);
        }

        #endregion

        #region Private Methods

        string SignatureFileName()
        {
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/SignatureImages/"))
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/SignatureImages/");
            }
            return Environment.GetFolderPath(Environment.SpecialFolder.Personal) + "/SignatureImages/" + Quote.Assessment.Guid.ToString() + ".png";
        }

        void CreateStatusSegmentControl()
        {
            segStatus = new UISegmentedControl(new string[] { "Pending", "Completed", "Cancelled" });
            segStatus.TintColor = NavigationController.NavigationBar.BarTintColor;
            segStatus.BackgroundColor = UIColor.White;
            segStatus.SetWidth(70, 0);
            segStatus.SetWidth(70, 1);
            segStatus.SetWidth(70, 2);

            segStatus.Enabled = Appointment.Editable;

            segStatus.SelectedSegment = (int)Appointment.Status;
            segStatus.ValueChanged += delegate
            {
                switch (segStatus.SelectedSegment)
                {
                case 0:
                    Appointment.Status = AppointmentStatus.Pending;
                    break;
                case 1:
                    Appointment.Status = AppointmentStatus.Completed;
                    break;
                case 2:
                    Appointment.Status = AppointmentStatus.Cancelled;
                    break;
                default:
                    break;
                }
                SetDataDirty();
            };
        }

        void SetupNavigationBar()
        {
            __NavButtonSave = new UIBarButtonItem(@"Save", UIBarButtonItemStyle.Done, ButtonSaveClicked);

            switch (segView.SelectedSegment)
            {
            case -1:
                this.Title = @"All";
                break;
            case 0:
                this.Title = @"Appointment";
                break;
            case 1:
                this.Title = @"Items";
                break;
            case 2:
                this.Title = @"Logistics";
                break;
            case 3:
                this.Title = @"Tools";
                break;
            case 4:
                this.Title = @"Risk Assessment";
                break;
            default:
                break;
            }
        }

        void RiskMobileSignalSet(string provider, SignalLevelEnum level)
        {
            string strLevel = "None (0 bars)";
            switch (level)
            {
            case SignalLevelEnum.Poor:
                strLevel = "Poor (1-2 bars)";
                break;
            case SignalLevelEnum.Average:
                strLevel = "Average (3-4 bars)";
                break;
            case SignalLevelEnum.Good:
                strLevel = "Good (5 bars)";
                break;
            default:
                strLevel = "None (0 bars)";
                break;
            }
			
            Quote.Assessment.MobileSignal = string.Format("{0} : {1}", provider, strLevel);
            SetDataDirty();
        }

        void RiskTextInputSaved(int Index, string SavedText)
        {
            var property = (RiskAssesmentProperty)Index;
            switch (property)
            {
            case RiskAssesmentProperty.ROW_RISK_AERIALPLAN:
                Quote.Assessment.AerialRescuePlan = 1;//SavedText;
                break;
//                case RiskAssesmentProperty.ROW_RISK_GRIDREF:
//                    Quote.Assessment.GridReference = SavedText;
//					break;
            case RiskAssesmentProperty.ROW_RISK_HOSPITAL:
                Quote.Assessment.NearestAnE = 1;//SavedText;
                break;
            case RiskAssesmentProperty.ROW_RISK_RESCUEACCESS:
                Quote.Assessment.RescueAccess = SavedText;
                break;
            case RiskAssesmentProperty.ROW_RISK_SIGNAL:
                Quote.Assessment.MobileSignal = SavedText;
                break;
            default:
                break;
            }
            SetDataDirty();
        }

        void TextInputSaved(int Index, string SavedText)
        {
            switch (Index)
            {
            case 0:
                Quote.TimeHrs = int.Parse(SavedText);
                break;
            case 1:
                Quote.StaffNumber = int.Parse(SavedText);
                break;
            case 2:
                Quote.Comments = SavedText;
                break;
            default:
                break;
            }
            SetDataDirty();
        }

		void TextSelectedSaved(int Index, string SavedText)
		{
			TextLookupSource source = (TextLookupSource)Index;
			var idFromSavedText = SavedText.Substring(0, SavedText.IndexOf(":"));
			switch (source)
			{
				case TextLookupSource.Hopsitals:
					var hList = DAL.Repository.Instance().HospitalList();
					var h = hList.Find(hos => hos.Id.ToString().Contains(idFromSavedText));
					if (h != null)
					{
						Quote.Assessment.NearestAnE = h.Id;
						Quote.Assessment.HospitalName = h.Name.Replace("\n", " ");
					}
					break;
				case TextLookupSource.RescuePlans:
					var pList = DAL.Repository.Instance().AerialRescuePlanList();
					var p = pList.Find(plan => plan.Id.ToString().Contains(idFromSavedText));
					if (p != null)
					{
						Quote.Assessment.AerialRescuePlan = p.Id;
						Quote.Assessment.AerialRescueDetails = p.Plan;
					}
					break;
				default:
					break;
			}
			SetDataDirty();
		}

        void SaveImageAndCompleteAssessment(UIImage signature)
        {
            DeleteSignatureAndMarkIncomplete();
            NSError e = new NSError();
            signature.AsPNG().Save(SignatureFileName(), false, out e);
            Quote.Assessment.CompletionDate = DateTime.Now;
            Quote.Dirty = true;
            table.ReloadData();
        }

        void DeleteSignatureAndMarkIncomplete()
        {
            if (File.Exists(SignatureFileName()))
            {
                File.Delete(SignatureFileName());
			
                Quote.Assessment.CompletionDate = DateTime.MinValue;
                Quote.Dirty = true;
            }
        }

        void SetDataDirty()
        {
            Quote.Dirty = true;
            Appointment.Dirty = true;

            NavigationItem.RightBarButtonItem = __NavButtonSave;
            table.ReloadData();
        }

        void LocationCaptured(GeoLocation location)
        {
            Quote.Assessment.SetAssessmentLocation(location);
            Quote.Dirty = true;
        }

		void EditClientContact_TouchUpInside(object sender, EventArgs e)
		{
			this.NavigationController.PushViewController(new AppointmentEditContactViewController(this.Appointment), true);
		}

		#endregion

		#region Delegates

		public class MakeCallAlertDelegate : UIAlertViewDelegate
        {

            private string _telephoneNumber;

            public MakeCallAlertDelegate(string telephoneNumber)
            {
                _telephoneNumber = telephoneNumber;
            }

            public override void Clicked(UIAlertView alertView, nint buttonIndex)
            {
                if (buttonIndex == 1)
                {
                    NSUrl telephoneURL = new NSUrl(@"tel:" + _telephoneNumber.Replace(" ", "").Trim());
                    UIApplication.SharedApplication.OpenUrl(telephoneURL);

                }
            }
        }

        #endregion

        protected class StatusEventArgs : EventArgs
        {
            public int SelectedSegment { get; set; }

            public StatusEventArgs() : base()
            {
            }
        }
    }

    public  class SaveQuoteAlertDelegate : UIAlertViewDelegate
    {

        public delegate void AppointmentSavedElegate ();

        public static event AppointmentSavedElegate AppointmentSavedEvent;

        private Appointment _Appointment;
        private Quote _Quote;

        public SaveQuoteAlertDelegate(Appointment appointment, Quote quote)
        {
            _Appointment = appointment;
            _Quote = quote;


            if (AppointmentSavedEvent != null)
                AppointmentSavedEvent();
        }

        public override void Clicked(UIAlertView alertView, nint buttonIndex)
        {
            if (buttonIndex == 1)
            {
                DAL.Repository.Instance().AppointmentSave(_Appointment);
                DAL.Repository.Instance().QuoteSave(_Quote);


                if (AppointmentSavedEvent != null)
                    AppointmentSavedEvent();
            }
        }
    }
}

