using System;
using CoreGraphics;
using System.Text;
using Foundation;
using UIKit;
using CoreAnimation;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class ItemsValueFooterView : UIView
	{
	
		UILabel LblNetValue;
		UILabel LblTotalValue;
		UILabel LblVatValue;
		UILabel LblNetTitle;
		UILabel LblTotalTitle;
		UILabel LblVatTitle;
		
		UILabel LblNoVat;
		
		decimal _Vat = 0;
		
		public float ViewHeight
		{
			get{return 100f;}
		}
		
		public ItemsValueFooterView () : base ()
		{
			Intialise();
		}
		
		public ItemsValueFooterView (decimal vat) : base (new CGRect(10,50,300,100))
		{
			_Vat = vat;
			Intialise();
		}
		
		public ItemsValueFooterView (IntPtr handle) : base (handle)
		{
			Intialise();
		}
		
		void Intialise()
		{
			
			CGPoint center = GetScreenCenter ();
			
			this.BackgroundColor = UIColor.Clear;
			
			LblNetValue = CreateValueLabel((float)center.X,23f,UITextAlignment.Right);
			LblVatValue = CreateValueLabel((float)center.X,47f,UITextAlignment.Right);
			LblTotalValue = CreateValueLabel((float)center.X,71f,UITextAlignment.Right);
			LblNetTitle = CreateValueLabel((float)center.X,23f,UITextAlignment.Left);
			LblVatTitle = CreateValueLabel((float)center.X,47f,UITextAlignment.Left);
			LblTotalTitle = CreateValueLabel((float)center.X,71f,UITextAlignment.Left);
			
			LblNoVat = new UILabel(new CGRect(center.X - 110, 47f,220,20));
			LblNoVat.BackgroundColor = UIColor.Clear;
			LblNoVat.AutoresizingMask = UIViewAutoresizing.None;
			LblNoVat.TextAlignment = UITextAlignment.Center;
			LblNoVat.Text = "Not VAT Registered.";
			
			if(_Vat == 0)
			{
				LblTotalValue.Hidden = true;
			}
			
			this.AddSubviews (new UIView[]{LblNetTitle,LblVatTitle,LblTotalTitle,LblNetValue,LblVatValue,LblTotalValue,LblNoVat});
			this.Hidden = false;
		}
		
		UILabel CreateValueLabel(float xCenter,float yPos, UITextAlignment alignment)
		{
			float width = 110;
			float xPos;
			if(alignment == UITextAlignment.Right)
			{
				xPos = xCenter;
			}
			else
			{
				xPos = xCenter - width;
			}
			UILabel label = new UILabel(new CGRect(xPos,yPos,width,15));
			label.TextAlignment = alignment;
			label.AdjustsFontSizeToFitWidth = true;
			label.BackgroundColor = UIColor.Clear;
			label.Font.WithSize(13f);
			label.AutoresizingMask = UIViewAutoresizing.None;
			return label;
		}

		public void SetItemValues (decimal net,decimal vat)
		{			
			_Vat = vat;
			net = decimal.Round (net, 2, MidpointRounding.AwayFromZero);
			
			if (_Vat > 0) 
			{
				decimal vatValue = decimal.Round ((net * (_Vat / 100)), 2, MidpointRounding.AwayFromZero);
				LblNetTitle.Text = "Net";
				LblVatTitle.Text = string.Format("VAT at {0}%", vat.ToString());
				LblTotalTitle.Text = "Total";
				LblNetValue.Text = net.ToString ("C");
				LblVatValue.Text = vatValue.ToString ("C");
				LblTotalValue.Text = (net + vatValue).ToString ("C");
				DisplayVatLabels(false);
			}
			else
			{
				DisplayVatLabels(true);
				LblNetValue.Text = net.ToString("C");
				LblNetTitle.Text = "Total Value";
			}
		}
		
		void DisplayVatLabels(bool hidden)
		{
			LblNoVat.Hidden = !hidden;
			LblTotalTitle.Hidden = hidden;
			LblTotalValue.Hidden = hidden;
			LblVatTitle.Hidden = hidden;
			LblVatValue.Hidden = hidden;
		}
		
		public void DisplayDeleteOnly(bool value)
		{
			LblNoVat.Hidden = value;
			LblTotalTitle.Hidden = value;
			LblTotalValue.Hidden = value;
			LblVatTitle.Hidden = value;
			LblVatValue.Hidden = value;
	  		LblNetValue.Hidden = value;
			LblNetTitle.Hidden = value;
		}
		
		private CGPoint GetScreenCenter()
		{
			nfloat x = UIScreen.MainScreen.Bounds.Width / 2;
			nfloat y = UIScreen.MainScreen.Bounds.Height / 2;
			
			if(UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft ||
			 UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight )
			 {
				return new CGPoint(y,x);
			 }
			 else
			 {
				return new CGPoint(x,y);
			 }
		}
	}
}

