// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	[Register ("ImageEditViewController")]
	partial class ImageEditViewController
	{
		[Outlet]
		DGS.ArbPro.Mobile.iOS.UI.ImageEditingView EditingView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (EditingView != null) {
				EditingView.Dispose ();
				EditingView = null;
			}
		}
	}
}
