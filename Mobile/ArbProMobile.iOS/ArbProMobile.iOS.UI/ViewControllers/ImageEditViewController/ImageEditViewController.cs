using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class ImageEditViewController : UIViewController
	{
	
		public UIImage Picture {get;set;}
		
		public ImageEditViewController () : base ("ImageEditViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
		}
		
		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			
			if(Picture != null)
				this.Ima.Image = Picture;
		}
	}
}

