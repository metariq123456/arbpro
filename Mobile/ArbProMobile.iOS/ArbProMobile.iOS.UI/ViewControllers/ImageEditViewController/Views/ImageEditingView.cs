using System;
using System.Drawing;
using System.Collections.Generic;
using MonoTouch.CoreGraphics;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class ImageEditingView : UIImageView
	{
		List<PointF> _TouchPoints;
		SizeF _PointSize = new SizeF(15,15);
		
		//public UIImage Picture {get;set;}
		
		public ImageEditingView () : base ()
		{
		}		
		
		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			GetTouchPoint(touches);
		}
		
		public override void TouchesMoved (NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);
			GetTouchPoint(touches);
		}

		public override void Draw (RectangleF rect)
		{
			base.Draw (rect);
				
			using(CGContext con = UIGraphics.GetCurrentContext())
			{
				con.SetAllowsAntialiasing (true);
				foreach(PointF p in _TouchPoints)
				{
					con.SetFillColor(0f,0f,0f,1f);
					con.FillEllipseInRect(new RectangleF(p, _PointSize));
				}
			}
		}

		void GetTouchPoint(NSSet touches)
		{
			UITouch touch = touches.AnyObject as UITouch;
			if(touch != null)
			{
				Console.WriteLine ("Touch point received.");
				_TouchPoints.Add (touch.LocationInView(this));
			}
		}
	}
}

