using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class TextViewViewController : UIViewController
	{
		public TextViewViewController () : base ("TextViewViewController", null)
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Set the navigation bar to opaque, so that the controls don't slide under it on iOS7
			this.NavigationController.NavigationBar.Translucent = false;
			txtText.Text = _Text;

			lblTitle.Text = Title;		
		}

		public override void ViewWillAppear (bool animated)
		{
			if (BL.Globals.DemoData) {
				NavigationItem.Prompt = "Demonstration Mode";
			} else {
				NavigationItem.Prompt = null;
			}
		}

		private string _Text;

		public string Text {
			set
				{ _Text = value; }
		}
// TODO - remove this once we've determined that we can use the inherited Title which this hides.
//		private string _Title;
//
//		public string Title {
//			set { _Title = value; }
//		}
	}
}

