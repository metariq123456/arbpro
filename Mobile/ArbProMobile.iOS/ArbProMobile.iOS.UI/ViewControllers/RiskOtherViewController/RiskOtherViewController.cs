using System;
using CoreGraphics;
using Foundation;
using UIKit;

using DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class RiskOtherViewController : UIViewController
	{
		// TODO - remove this once we've determined we don't need it
//		UIBarButtonItem _BtnDone = null;
		RiskAssessment _Assessment = null;
		int _ItemIndex = -1;
		
		AssessmentAdditionalItem _RiskItem =null;
		public AssessmentAdditionalItem RiskItem 
		{ 
			get
			{
				if(_RiskItem == null)
				{
					_RiskItem = _Assessment.FurtherRisks[_ItemIndex];
				}
				return _RiskItem;
			} 
		}
		
		#region TableViewSource
		class OtherRiskTableSource:UITableViewSource
		  {
		      #region Fields
		      const int SECTION_DETAIL = 0;
		      const int SECTION_CONTROL = 1;
		      const string CELL_ID = "tblCell";
		      RiskOtherViewController _Controller;
			#endregion

		      #region Constructors
		      public OtherRiskTableSource() :base()
		      {

		      }

		      public OtherRiskTableSource(RiskOtherViewController controller) :base()
		      {
		          _Controller = controller;
		      }
		      #endregion

		      #region Overrides
		      public override nint NumberOfSections(UITableView tableView)
		      {
		          return 2;
		      }

		      public override string TitleForHeader(UITableView tableView, nint section)
		      {
		          switch (section)
		          {
		              case SECTION_DETAIL:
		                  return "Details of an additional risk, and control, not already evaluated.";
	                  case SECTION_CONTROL:
		                  return "";
		              default:
		                  return string.Format("bad section{0}", section);
		          }
		      }

		      public override nint RowsInSection(UITableView tableview, nint section)
		      {
		          return 1;
		      }

		      public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				UITableViewCell Cell;
				Cell = tableView.DequeueReusableCell (CELL_ID);
		                  
				if (Cell == null) {
					Cell = new UITableViewCell (UITableViewCellStyle.Subtitle, CELL_ID);
					Cell.SelectionStyle = UITableViewCellSelectionStyle.None;
					Cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
				}
		                  
				switch (indexPath.Section) {
				case SECTION_DETAIL:
					Cell.TextLabel.Text = "Risk";
					if(_Controller.RiskItem.Detail == null || _Controller.RiskItem.Detail.Length == 0)
					{
						Cell.DetailTextLabel.Text = "Not Entered";
					}
					else
					{
						Cell.DetailTextLabel.Text = _Controller.RiskItem.Detail;
					}
					break;
				case SECTION_CONTROL:
					Cell.TextLabel.Text = "Control Measures";
					if(_Controller.RiskItem.Control == null || _Controller.RiskItem.Control.Length == 0)
					{
						Cell.DetailTextLabel.Text = "None";
					}
					else
					{
						Cell.DetailTextLabel.Text = _Controller.RiskItem.Control;
					}
					break;
				default:
					break;
				}
				return Cell;
			}
		      #endregion

		      #region Private Methods
		      public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
		      {
					_Controller.DisplayNotesField(indexPath.Section);
		      }
		      #endregion
		  }
		#endregion
	
		public RiskOtherViewController (RiskAssessment assessment, int itemIndex) : base ("RiskOtherViewController", null)
		{
			_Assessment = assessment;
			_ItemIndex = itemIndex;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
		}
		
		
		public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
			
			//Set the title of the view
			this.Title = "Additional Risk";
			
            if (BL.Globals.DemoData)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            }
            else
            {
                NavigationItem.Prompt = null;
            }
            viewTable.Source = new OtherRiskTableSource(this);
        }
        
        private void DisplayNotesField (int index)
		{
			NotesInputViewController controller = new NotesInputViewController ();
			controller.Index = index;
			if (index == 0) 
			{
				controller.ViewTitle = "Risk Detail";
				controller.CurrentText = this.RiskItem.Detail;
			}
			else
			{
				controller.ViewTitle = "Control Measures Required";
				controller.CurrentText = this.RiskItem.Control;
			
			}
			controller.KeyboardType = UIKeyboardType.Default;
			controller.DidInputTextEvent += NotesEntered;
			this.NavigationController.PushViewController (controller,true);
		}
		
		private void NotesEntered (int index, string text)
		{
			switch (index) 
			{
				case 0:
					this.RiskItem.Detail = text;
					break;
				case 1:
					this.RiskItem.Control = text;
					break;
				default:
					break;
			}
			this.viewTable.ReloadData ();
		}
	}
}

