// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("RiskOtherViewController")]
    partial class RiskOtherViewController
    {
        [Outlet]
        UIKit.UITableView viewTable { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (viewTable != null) {
                viewTable.Dispose ();
                viewTable = null;
            }
        }
    }
}