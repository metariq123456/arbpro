// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/SynchroniseViewController/SynchroniseViewController.cs#8 $
// $Author: Jon $
// $Date: 2015/05/20 $
// $Change: 24664 $
// *********************************************************************
//
using System;
using System.Threading.Tasks;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class SynchroniseViewController : UIViewController
    {
		#region Members

        DateTime? FromDate = null;
        DateTime? ToDate = null;
        DateTime? UpdatedDate = null;
        UIBarButtonItem _NavButtonDownload = null;
		#endregion
	
		#region Constructors
        public SynchroniseViewController() : base ("SynchroniseViewController", null)
        {
            Title = @"Synchronise";

            _NavButtonDownload = new UIBarButtonItem(@"Download", UIBarButtonItemStyle.Done, DownloadAppintments);
            this.NavigationItem.RightBarButtonItem = _NavButtonDownload;
        }
        #endregion

        #region Overrides
        public override void ViewWillAppear(bool animated)
        {
            SynchroniseTableSource MySource;
            base.ViewWillAppear(animated);

            if (!BL.Globals.LoggedIn)
            {
                NavigationItem.Prompt = "Demonstration Mode";
                _NavButtonDownload.Enabled = false;
            }
            else
            {
                NavigationItem.Prompt = null;
                _NavButtonDownload.Enabled = true;
                ActivityView.ProgressView.DialogButtonClickedEvent += DialogButtonClicked;
            }

            MySource = new SynchroniseTableSource(this);
            Table.Source = MySource;

        }
        #endregion

		#region Actions
        void DisplayDatePicker(int cellIndex, string text)
        {
            string Label;
            DateTime? InitialDate;

            switch (cellIndex)
            {
                case 0:
                    Label = "From Date";
                    InitialDate = FromDate;
                    break;
                case 1:
                    Label = "To Date";
                    InitialDate = ToDate;
                    break;
                case 2:
                    Label = "Changed Since Date";
                    InitialDate = UpdatedDate;
                    break;
                default:
                    break;
            }

            DatePickerViewController controller = new DatePickerViewController(cellIndex, string.Format("Select {0}", Label));
            if (InitialDate != null)
            {
                controller.InitialDate = (DateTime)InitialDate;
            }

            controller.DidSelectDateEvent += DatePicked;
            this.NavigationController.PushViewController(controller, true);
        }
        
        async void DialogButtonClicked(string buttonCaption)
        {
			if(buttonCaption == "Cancel")
			{
				BusinessLayer.CancelActivity = true;
				ActivityView.DisplayCompletedStatus("Cancelled");
			}
			else
			{
				ActivityView.HideProgress();
			}
        }

        void DatePicked(int cellIndex, DateTime selectedDate)
        {
            switch (cellIndex)
            {
                case 0:
                    FromDate = selectedDate;
                    break;
                case 1:
                    ToDate = selectedDate;
                    break;
                case 2:
                    UpdatedDate = selectedDate;
                    break;
                default:
                    break;
            }

            if (BL.Globals.LoggedIn && FromDate != null && ToDate != null && UpdatedDate != null)
            {
                _NavButtonDownload.Enabled = true;
            }

        }

        void DownloadAppintments(Object sender, EventArgs e)
        {
            //DateTime MyFromDate;
            //DateTime MyToDate;
            //DateTime MyUpdatedDate;


            if (FromDate != null && ToDate != null && UpdatedDate != null)
            {

                if (FromDate != null && ToDate != null && ToDate < FromDate)
                {
                    UIAlertView Alert = new UIAlertView();
                    Alert.Title = "Error";
                    Alert.Message = "To Date cannot be before From Date";
                    Alert.AddButton("Ok");
                    Alert.Show();

                }

                try
                {
					ActivityView.ShowProgress("Downloading");
					BusinessLayer.ItemListCountActivityEvent += ActivityView.SetMaxValue;
					BusinessLayer.ItemSavedEvent += ActivityView.IncreaseProgress;
                    DownLoadAppoinments((DateTime)FromDate,(DateTime)ToDate,(DateTime)UpdatedDate);
                }
                catch(DGS.ArbPro.Mobile.Core.ServiceLayer.Exceptions.UnatherisedException ex)
                {
					DisplayLoginControl();
                }
                catch (Exception ex)
                {
//                    error = ex.Message;
                }
            }
            else
            {
                UIAlertView Alert = new UIAlertView();
                Alert.Title = "Error";
                Alert.Message = "You must select dates before downloading";
                Alert.AddButton("Ok");
                Alert.Show();
            }
        }
        
        void DisplayLoginControl()
        {
			ActivityView.HideProgress ();
			LoginViewController controller = new LoginViewController(false);
			NavigationController.PushViewController (controller,true);
        }
        
        async void DownLoadAppoinments(DateTime FromDate,DateTime ToDate,DateTime UpdatedDate)
        {
			await DownLoadAppoinmentsAsync(FromDate,ToDate,UpdatedDate).ConfigureAwait (true);
			ActivityView.DisplayCompletedStatus("Completed");
        }
        
        async Task DownLoadAppoinmentsAsync(DateTime FromDate,DateTime ToDate,DateTime UpdatedDate)
        {
			await Task.Run (() => BusinessLayer.AppointmentListDownload(Globals.CurrentUser.Id)).ConfigureAwait (false);
        }
		#endregion
		
		#region Methods

		#endregion

        class SynchroniseTableSource:UITableViewSource
        {

            #region Fields
            const string CELL_ID = "DateCell";
            SynchroniseViewController _Controller;
            #endregion

            #region Constructors
            public SynchroniseTableSource() :base()
            {
            }

            public SynchroniseTableSource(SynchroniseViewController controller)
            {
                _Controller = controller;
            }
            #endregion

            #region Actions

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                UITableViewCell cell = GetCell(tableView, indexPath);
                _Controller.DisplayDatePicker(indexPath.Section, cell.TextLabel.Text);
            }
            #endregion

            #region Overrides
            public override int NumberOfSections(UITableView tableView)
            {
                return 3;
            }

            public override string TitleForHeader(UITableView tableView, int section)
            {
                return "";
            }

            public override int RowsInSection(UITableView tableview, int section)
            {
                return 1;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                UITableViewCell Cell;
                Cell = tableView.DequeueReusableCell(CELL_ID);
                if (Cell == null)
                {
                    Cell = new UITableViewCell(UITableViewCellStyle.Value1, CELL_ID);
                    Cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
                    Cell.SelectionStyle = UITableViewCellSelectionStyle.None;
                }

                switch (indexPath.Section)
                {
                    case 0:
                        Cell.TextLabel.Text = "From date";
                        Cell.DetailTextLabel.Text = _Controller.FromDate == null ? "Select" : String.Format("{0:dd/MM/yyyy}", _Controller.FromDate);
                        break;
                    case 1:
                        Cell.TextLabel.Text = "To date";
                        Cell.DetailTextLabel.Text = _Controller.ToDate == null ? "Select" : String.Format("{0:dd/MM/yyyy}", _Controller.ToDate);
                        break;
                    case 2:
                        Cell.TextLabel.Text = "Changed since date";
                        Cell.DetailTextLabel.Text = _Controller.UpdatedDate == null ? "Select" : String.Format("{0:dd/MM/yyyy}", _Controller.UpdatedDate);
                        break;

                    default:
                        break;
                }
                return Cell;
            }
            #endregion
        }
    }
}
