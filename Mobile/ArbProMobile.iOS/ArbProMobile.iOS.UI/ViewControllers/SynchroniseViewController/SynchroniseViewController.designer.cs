// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	[Register ("SynchroniseViewController")]
	partial class SynchroniseViewController
	{
		[Outlet]
		MonoTouch.UIKit.UITableView Table { get; set; }

		[Outlet]
		DGS.ArbPro.Mobile.iOS.UI.NetworkActivityView ActivityView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (Table != null) {
				Table.Dispose ();
				Table = null;
			}

			if (ActivityView != null) {
				ActivityView.Dispose ();
				ActivityView = null;
			}
		}
	}
}
