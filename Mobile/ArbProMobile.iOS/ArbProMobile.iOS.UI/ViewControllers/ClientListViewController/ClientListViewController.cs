﻿// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DGS.ArbPro.Mobile.BL;
using Foundation;
using Microsec.Tools.iOS.UI;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class ClientListViewController : UIViewController
	{

		#region public properties
		public List<BL.Client> LookupItems { get; set; }

		public string DefaultValue { get; set; }

		// The maximum length of user input into the search text view.
		public int MaxLength { get; set; }

		public string SearchText
		{
			get
			{
				string text = ClientSearchBar.Text;
				if (text == null)
					text = "";

				return text;
			}
		}
		#endregion

		#region private properties
		private List<BL.Client> FilteredItems { get; set; }
		#endregion

		#region Members
		private ClientListSource _DataSource = null;
		UIBarButtonItem _BtnRefresh;
		bool IsForAppointments;

		class SearchBarDelegate : UISearchBarDelegate
		{
			ClientListViewController _controller;

			public SearchBarDelegate(ClientListViewController controller) : base()
			{
				_controller = controller;
			}

			public override void CancelButtonClicked(UISearchBar searchBar)
			{
				searchBar.Text = "";
				_controller.FilterItems();
			}

			public override void TextChanged(UISearchBar searchBar, string searchText)
			{
				_controller.FilterItems();
			}

			public override void ListButtonClicked(UISearchBar searchBar)
			{
				searchBar.ResignFirstResponder();
			}

			public override bool ShouldChangeTextInRange(UISearchBar searchBar, NSRange range, string text)
			{
				// NOTE: Don't call the base implementation on a Model class
				// see http://docs.xamarin.com/guides/ios/application_fundamentals/delegates,_protocols,_and_events 
				// Note we need to cater for an empty replacement - i.e. the delete key being pressed.
				if (text != "" && _controller.MaxLength != 0)
				{
					return searchBar.Text.Length < _controller.MaxLength;
				}
				else {
					return true;
				}
			}
		}

		class ClientListSource : UITableViewSource
		{

			#region Members

			const string CONST_CELL_NAME = "ClientCell";
			ClientListViewController Controller;

			#endregion

			#region Properties

			public List<BL.Client> Clients { get; set; }

			#endregion

			#region Constructors

			public ClientListSource(ClientListViewController controller)
			{
				Controller = controller;
				Clients = Controller.FilteredItems;
			}

			#endregion

			#region Overrides

			public override nint NumberOfSections(UITableView tableView)
			{
				return 1;
			}

			public override string TitleForHeader(UITableView tableView, nint section)
			{
				if (Clients == null || Clients.Count -1 == 0)
				{
					return "No Clients Found";
				}
				else if (Clients.Count -1 == 1)
				{
					return string.Format("{0} Client Found", Clients.Count -1);
				}
				else
				{
					return string.Format("{0} Clients Found", Clients.Count -1);
				}
			}

			public override nint RowsInSection(UITableView tableview, nint section)
			{
				if (Clients != null)
				{
					return Clients.Count;
				}
				else
				{
					return 0;
				}
			}

			public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
			{
				return 50;
			}

			public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
			{
				//Get a table cell from the factory
				UITableViewCell cell = tableView.DequeueReusableCell(CONST_CELL_NAME);

				if (cell == null)
				{
					cell = new UITableViewCell(UITableViewCellStyle.Default, CONST_CELL_NAME);
				}
				BL.Client c = Clients[indexPath.Row];
				if (c.Id > 0)
				{
					if (c.AccountType == 0)
					{
						cell.TextLabel.Text = c.Forename + " " + c.Surname;
					}
					else
					{
						cell.TextLabel.Text = c.BusinessName;
					}
				}
				else 
				{
                    if (Controller.AddButton == null)
					{
						Controller.AddButton = new UIButton(UIButtonType.ContactAdd);
						Controller.AddButton.AddTarget(Controller.AddClient, UIControlEvent.TouchUpInside);
					}

					cell = tableView.DequeueReusableCell("NewClientCell");
					if (cell == null)
					{
						cell = new UITableViewCell(UITableViewCellStyle.Subtitle, "NewClientCell");
						cell.Accessory = UITableViewCellAccessory.None;
					}
					cell.AccessoryView = Controller.AddButton;
					cell.TextLabel.Text = "Add New Client";
				}

				return cell;
			}

			public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
			{
				BL.Client c = GetItem(indexPath.Row);
				if (Controller.IsForAppointments && c.Id > 0)
				{
					this.Controller.NavigationController.PopViewController(true);
					AppointmentNewViewController viewCon = (AppointmentNewViewController)Controller.NavigationController.TopViewController;
					viewCon.GetClientForAppt(c);
				}
				else
				{
					this.Controller.NavigationItem.Title = "Clients";
					this.Controller.NavigationController.PushViewController(new EditClientViewController(c.Id, Controller.IsForAppointments), true);
				}
			}

			#endregion

			#region Public Methods

			public BL.Client GetItem(int row)
			{
				return Clients[row];
			}

			#endregion

		}
		//A reference to the buttons is kept to prevent GC from disposing
		public UIButton AddButton = null;

		#endregion

		#region Constructors

		public ClientListViewController(bool isForAppointments) : base("ClientListViewController", null)
		{
			//Setup navigation bar
			this.Title = @"Clients";

			IsForAppointments = isForAppointments;

			FilteredItems = new List<BL.Client>();

			_BtnRefresh = new UIBarButtonItem("Refresh List", UIBarButtonItemStyle.Plain, this.BtnRefreshClicked);
			this.NavigationItem.RightBarButtonItem = _BtnRefresh;
		}

		#endregion

		#region Overrides

		public override void DidReceiveMemoryWarning()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning();

			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			this.NavigationController.NavigationBar.Translucent = false;
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);
			Title = "Clients";

			if (BL.Globals.DemoData)
			{
				NavigationItem.Prompt = "Demonstration Mode";
			}
			else
			{
				NavigationItem.Prompt = null;
			}
			NetworkView.ProgressView.DialogButtonClickedEvent += DialogButtonClicked;
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			ClientSearchBar.Delegate = new SearchBarDelegate(this);

			this.RefreshData();

			foreach (UIView view in ClientSearchBar.Subviews)
			{
				if (view.GetType() == typeof(UITextField))
				{
					UITextField field = (UITextField)view;
					field.ClearButtonMode = UITextFieldViewMode.Never;
					field.ReturnKeyType = UIReturnKeyType.Done;
				}
			}

			//Add BuisnessLayer event handlers to progress bar
			BusinessLayer.ActivityProcessStageEvent += NetworkView.SetProgressTitle;
			BusinessLayer.ItemListCountActivityEvent += NetworkView.SetMaxValue;
			BusinessLayer.ItemSavedEvent += NetworkView.IncreaseProgress;
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
			//Remove BuisnessLayer event handlers to progress bar to prevent conflicts
			BusinessLayer.ActivityProcessStageEvent -= NetworkView.SetProgressTitle;
			BusinessLayer.ItemListCountActivityEvent -= NetworkView.SetMaxValue;
			BusinessLayer.ItemSavedEvent -= NetworkView.IncreaseProgress;
		}

		#endregion

		#region Private Methods


		void RefreshData()
		{
			PopulateTableSource();
		}

		private async void PopulateTableSource()
		{
			try
			{
				_DataSource = await PopulateTableSourceTask();
				DataTable.Source = _DataSource;
				FilterItems();
			}
			catch (Exception)
			{
				UIAlertView alert = new UIAlertView("Error", "Error populating Clients table. Please navigate to the prieviouse page and try again", null, "Ok");
				alert.Show();
			}
		}

		private Task<ClientListSource> PopulateTableSourceTask()
		{
			var taskResult = new TaskCompletionSource<ClientListSource>();
			List<BL.Client> allClients = BL.BusinessLayer.ClientList();

			BL.Client dummyClient = new Client() { Id = -1 };

			allClients.Insert(0, dummyClient);

			LookupItems = allClients;

			taskResult.SetResult(new ClientListSource(this));
			return taskResult.Task;
		}

		private async void BtnRefreshClicked(object Sender, EventArgs e)
		{
			if (!BL.Globals.DemoData)
			{
				NetworkView.ShowProgress("Downloading Clients List");
				await BtnRefreshClickedAsync().ConfigureAwait(true);
				NetworkView.DisplayCompletedStatus("Completed");
			}
			RefreshData();
		}


		private async Task BtnRefreshClickedAsync()
		{
			// Get the latest clients list
			await Task.Run(() => BusinessLayer.ClientListDownload()).ConfigureAwait(false);
		}

		void DialogButtonClicked(string buttonCaption)
		{
			if (buttonCaption == "Cancel")
			{
				BusinessLayer.CancelActivity = true;
				NetworkView.DisplayCompletedStatus("Cancelled");
			}
			else {
				NetworkView.HideProgress();
			}
		}

		#endregion

		#region public methods

		public void AddClient(object Sender, EventArgs e)
		{
			NavigationItem.Title = "Clients";
			this.NavigationController.PushViewController(new EditClientViewController(-1, IsForAppointments), true);
		}

		public void FilterItems()
		{
			string lookupText = SearchText.ToUpper();

			FilteredItems.Clear();
			foreach (var item in LookupItems)
			{
				if (lookupText == "" || item.Id == -1 || item.Forename.ToUpper().Contains(lookupText) || item.Surname.ToUpper().Contains(lookupText) || (item.BusinessName.ToUpper().Contains(lookupText) && item.AccountType == 1))
					FilteredItems.Add(item);
			}

			DataTable.ReloadData();
		}

		public void ItemSelected(string text)
		{
			ClientSearchBar.Text = text;
		}

		#endregion
	}
}


