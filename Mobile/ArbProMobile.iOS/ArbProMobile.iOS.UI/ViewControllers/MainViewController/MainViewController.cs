// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
//
//
using System;
using CoreGraphics;
using Foundation;
using UIKit;

using DGS.ArbPro.Mobile.BL;
using Microsec.Mobile.Core;


namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class MainViewController : UIViewController
    {
        #region Constructors
        public MainViewController() : base ("MainViewController", null)
        {
			
        }
        #endregion

        #region Overrides
        /// <Docs>Called when the system is running low on memory.</Docs>
        /// <summary>
        /// Dids the receive memory warning.
        /// </summary>
        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        /// <summary>
        /// Views the did load.
        /// </summary>
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Perform any additional setup after loading the view, typically from a nib.
        }

        /// <summary>
        /// Views the did appear.
        /// </summary>
        /// <param name="animated">If set to <c>true</c> animated.</param>
        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

			GetAndSetUserDefaults();
			TinyIoC.TinyIoCContainer.Current.Register<Microsec.Mobile.Core.Logging.ILogger> (new Microsec.Mobile.Core.iOS.Logging.Logger ());
       
			// Default to maximum logging for now
			Globals.LoggingLevel = DGS.ArbPro.Mobile.Core.Enums.LoggingLevel.All;

			BL.BusinessLayer.AppVersion = NSBundle.MainBundle.InfoDictionary ["CFBundleVersion"].ToString ();

			if(Globals.LoginStatus != Globals.LoggedInStatus.NotLoggedIn)//(Globals.CurrentUser != null)
				this.NavigationController.PushViewController(new AppointmentListViewController(), true);
        }
        
        public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			
			this.Title = @"Arb Pro";
			//this.NavigationController.
			           
		}
		#endregion
		
		#region Actions
        /// <summary>
        /// Run the application in demo mode.
        /// </summary>
        /// <param name="sender">Sender.</param>
        partial void BtnDemoModeClicked(NSObject sender)
        {
			Globals.DemoData = true;
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();
			log.Debug("Entering Demonstration Mode.");
            this.NavigationController.PushViewController(new AppointmentListViewController(),true);
        }

        /// <summary>
        /// Button login clicked event handler.
        /// Display the Login screen.
        /// </summary>
        /// <param name="sender">Sender.</param>
        partial void BtnLoginClicked(NSObject sender)
        {
			//NavigationItem.BackBarButtonItem = new UIBarButtonItem("Cancel",UIBarButtonItemStyle.Bordered,null);
            this.NavigationController.PushViewController(new LoginViewController(),true);
        }
        #endregion

        #region Methods
        #region Private
        /// <summary>
        /// Push the view controller.
        /// </summary>
        /// <param name="controller">Controller.</param>
        void PushViewController(UIViewController controller)
        {
            this.NavigationController.PushViewController(controller, true);
        }

		void GetAndSetUserDefaults()
		{
			var defaults = NSUserDefaults.StandardUserDefaults;
			defaults.Synchronize();

			LoadDefaultValues();

			BL.Settings.ServiceURI = defaults.StringForKey("server_preference").ToString();
			BL.Settings.InsightsEnabled = defaults.BoolForKey("insights_enabled");
			BL.Settings.DataRetainDays = (int)defaults.IntForKey("data_retain_days");
		}

		void LoadDefaultValues()
		{
			var settingsDict = new NSDictionary(NSBundle.MainBundle.PathForResource("Settings.bundle/Root.plist", null));

			if (settingsDict != null)
			{
				var prefSpecifierArray = settingsDict[(NSString)"PreferenceSpecifiers"] as NSArray;

				if (prefSpecifierArray != null)
				{
					foreach (var prefItem in NSArray.FromArray<NSDictionary>(prefSpecifierArray))
					{
						var key = prefItem[(NSString)"Key"] as NSString;

						if (key == null)
							continue;

						var value = prefItem[(NSString)"DefaultValue"];

						if (value == null)
							continue;

						NSDictionary defaultsToRegister = new NSDictionary(key, value);
						NSUserDefaults.StandardUserDefaults.RegisterDefaults(defaultsToRegister);
						NSUserDefaults.StandardUserDefaults.Synchronize();
					}
				}
			}
		}

        #endregion
        #region Public

        #endregion
        #endregion
    }
}

