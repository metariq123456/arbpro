// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/LogisticsViewController/LogisticsViewController.cs#8 $
// $Author: Jon $
// $Date: 2015/05/20 $
// $Change: 24664 $
// *********************************************************************
//
//
using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class LogisticsViewController : UIViewController
	{
		public LogisticsViewController () : base ("LogisticsViewController", null)
		{
		this.Title = @"Logistics";
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
		}
	}
}

