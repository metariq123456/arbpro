// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("NotesInputViewController")]
    partial class NotesInputViewController
    {
        [Outlet]
        UIKit.UILabel LblHeader { get; set; }


        [Outlet]
        UIKit.UIToolbar ToolBar { get; set; }


        [Outlet]
        UIKit.UITextView TxtNotes { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (ToolBar != null) {
                ToolBar.Dispose ();
                ToolBar = null;
            }

            if (TxtNotes != null) {
                TxtNotes.Dispose ();
                TxtNotes = null;
            }
        }
    }
}