using System;
using CoreGraphics;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class NotesInputViewController : UIViewController
    {
        #region Delegates

        public delegate void DidInputTextDelegate (int Index, string Text);

        public event DidInputTextDelegate DidInputTextEvent;

        #endregion

        #region Private Properties

        bool _IsPassThrough = false;
        UIBarButtonItem __NavButtonDone;
        UIBarButtonItem _NavButtonSelect;

        #endregion

        #region Public Properties

        public int Index { get; set; }

        public string CurrentText { get; set; }

        public int MaxLength { get; set; }

        public UIKeyboardType KeyboardType { get; set; }

        public string ViewTitle { get; set; }

        public bool Enabled
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        public NotesInputViewController(bool isPassThrough = false) : base("NotesInputViewController", null)
        {
            // Default MaxLength to 0
            MaxLength = 0;
            _IsPassThrough = isPassThrough;

            this.Enabled = true;
        }

        #endregion

        #region Overrides

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
			
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // Set the navigation bar to opaque, so that the controls don't slide under it on iOS7
            this.NavigationController.NavigationBar.Translucent = false;

            Title = ViewTitle;
         
            TxtNotes.Text = CurrentText != null ? CurrentText:"";
            TxtNotes.KeyboardType = KeyboardType;
            if (TxtNotes.Text.Length == 0)
                TxtNotes.BecomeFirstResponder();


            // Limit the amount of text the user can type in to the text. We have a limit set on the mobile client, although
            // the database column is varchar(max). This is because we still want to limit the text to a reasonable value.
            TxtNotes.ShouldChangeText += (sBar, range, replacement) =>
            {
                // Note we need to cater for an empty replacement - i.e. the delete key being pressed.
                if (replacement != "" && MaxLength != 0)
                {
                    return (sBar.Text.Length + replacement.Length - range.Length) <= MaxLength;
                } else
                {
                    return true;
                }
            };

            // Disable the entry field if necessary
            TxtNotes.ShouldBeginEditing += (UITextView textView) =>
            {
                return this.Enabled;
            };
                
            __NavButtonDone = new UIBarButtonItem(@"Done", UIBarButtonItemStyle.Bordered, ButtonDoneClicked);
            this.NavigationItem.RightBarButtonItem = __NavButtonDone;
            
           
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            Title = ViewTitle;
            if (BL.Globals.DemoData)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            } else
            {
                NavigationItem.Prompt = null;
            }
            this.ToolBar.Hidden = !_IsPassThrough;
            if (_IsPassThrough)
            {
				
                _NavButtonSelect = new UIBarButtonItem(@"Select", UIBarButtonItemStyle.Bordered, ButtonSelectClicked);
                UIBarButtonItem trash = new UIBarButtonItem(UIBarButtonSystemItem.Trash, ClearText);
                UIBarButtonItem flexiSpace = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace);
                this.ToolBar.Items = new UIBarButtonItem[] { trash, flexiSpace, _NavButtonSelect };
				
            }
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
            this.Title = @"Back";
        }

        #endregion

        #region Actions

        void ButtonSelectClicked(Object sender, EventArgs e)
        {
            TextPickerViewController controller = new TextPickerViewController();
		
		
            List<string> lookupValues = new List<string>();
		
            var Descriptions = DAL.Repository.Instance().ItemDescriptionList();
            foreach (var description in Descriptions)
            {
                lookupValues.Add(description.Description);
            }
            controller.LookupItems = lookupValues;
            controller.DefaultValue = CurrentText;
            controller.Index = Index;
            controller.ViewTitle = "Item Details";
            controller.InputLabel = "";
            controller.DidPickTextEvent += TextInputSaved;
			
            this.NavigationController.PushViewController(controller, true);
        }

        void ClearText(Object sender, EventArgs e)
        {
            TxtNotes.Text = string.Empty;
        }

        void ButtonDoneClicked(Object sender, EventArgs e)
        {
            if (this.Enabled)
            {
                if (DidInputTextEvent != null)
                    DidInputTextEvent(Index, TxtNotes.Text);
            }

            if (this.NavigationController != null)
                this.NavigationController.PopViewController(true);
        }

        private void TextInputSaved(int rowNumber, string SavedText)
        {
            TxtNotes.Text = SavedText;        
        }

        #endregion
    }
}

