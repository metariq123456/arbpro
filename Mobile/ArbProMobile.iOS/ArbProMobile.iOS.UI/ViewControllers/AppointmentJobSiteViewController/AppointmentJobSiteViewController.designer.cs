// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	[Register ("AppointmentJobSiteViewController")]
	partial class AppointmentJobSiteViewController
	{
		[Outlet]
		UIKit.UITextField txtAddress1 { get; set; }

		[Outlet]
		UIKit.UITextField txtAddress2 { get; set; }

		[Outlet]
		UIKit.UITextField txtClientOrBusinessName { get; set; }

		[Outlet]
		UIKit.UITextField txtCounty { get; set; }

		[Outlet]
		UIKit.UITextField txtHouseNo { get; set; }

		[Outlet]
		UIKit.UITextField txtPostCode { get; set; }

		[Outlet]
		UIKit.UITextField txtTown { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (txtClientOrBusinessName != null) {
				txtClientOrBusinessName.Dispose ();
				txtClientOrBusinessName = null;
			}

			if (txtHouseNo != null) {
				txtHouseNo.Dispose ();
				txtHouseNo = null;
			}

			if (txtAddress1 != null) {
				txtAddress1.Dispose ();
				txtAddress1 = null;
			}

			if (txtAddress2 != null) {
				txtAddress2.Dispose ();
				txtAddress2 = null;
			}

			if (txtTown != null) {
				txtTown.Dispose ();
				txtTown = null;
			}

			if (txtCounty != null) {
				txtCounty.Dispose ();
				txtCounty = null;
			}

			if (txtPostCode != null) {
				txtPostCode.Dispose ();
				txtPostCode = null;
			}
		}
	}
}
