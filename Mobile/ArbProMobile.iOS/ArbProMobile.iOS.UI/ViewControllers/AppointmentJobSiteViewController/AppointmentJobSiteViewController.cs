﻿// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
using System;
using System.Drawing;
using Foundation;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class AppointmentJobSiteViewController : UIViewController
	{
		#region private properties

		UIBarButtonItem _BtnDone;
		string clientName;
		string houseNo;
		string address1;
		string address2;
		string town;
		string county;
		string postCode;

		#endregion

		#region constructors

		/// <summary>
		/// Accepts the following parameters in order to display them for the user
		/// </summary>
		/// <param name="clName">Client name.</param>
		/// <param name="hNo">House no.</param>
		/// <param name="addr1">Address 1.</param>
		/// <param name="addr2">Address 2.</param>
		/// <param name="twn">Town.</param>
		/// <param name="cnty">County.</param>
		/// <param name="pCode">Post code.</param>
		public AppointmentJobSiteViewController(string clName, string hNo, string addr1, string addr2, string twn, string cnty, string pCode) : base("AppointmentJobSiteViewController", null)
		{
			this.Title = @"Site";
			clientName = clName;
			houseNo = hNo;
			address1 = addr1;
			address2 = addr2;
			town = twn;
			county = cnty;
			postCode = pCode;
			_BtnDone = new UIBarButtonItem("Done", UIBarButtonItemStyle.Plain, this.BtnDoneClicked);
			this.NavigationItem.RightBarButtonItem = _BtnDone;
		}

		#endregion

		#region overrides

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			DismissKeyboardOnBackgroundTap();
			RegisterForKeyboardNotifications();
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			this.NavigationController.NavigationBar.Translucent = false;
			RegisterKeyboardDelegate();
			LoadClientInfo();

			if (BL.Globals.DemoData)
			{
				NavigationItem.Prompt = "Demonstration Mode";
			}
			else
			{
				NavigationItem.Prompt = null;
			}
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
			DeregisterKeyboardDelegate();
		}

		#endregion

		#region Private Methods

		// saves values and passes them on to the appointment new view controller
		private void BtnDoneClicked(object Sender, EventArgs e)
		{
			NavigationController.PopViewController(true);
			AppointmentNewViewController viewCon = (AppointmentNewViewController)NavigationController.TopViewController;
			viewCon.StoreJobSiteDetails(txtHouseNo.Text, txtAddress1.Text, txtAddress2.Text, txtTown.Text, txtCounty.Text, txtPostCode.Text);
		}

		// registers keyboard delegate - keyboard on next button press goes to next text field
		private void RegisterKeyboardDelegate()
		{
			txtHouseNo.Tag = 1;
			txtAddress1.Tag = 2;
			txtAddress2.Tag = 3;
			txtTown.Tag = 4;
			txtCounty.Tag = 5;
			txtPostCode.Tag = 6;

			txtHouseNo.ShouldReturn += TextFieldShouldReturn;
			txtAddress1.ShouldReturn += TextFieldShouldReturn;
			txtAddress2.ShouldReturn += TextFieldShouldReturn;
			txtTown.ShouldReturn += TextFieldShouldReturn;
			txtCounty.ShouldReturn += TextFieldShouldReturn;
			txtPostCode.ShouldReturn += TextFieldShouldReturn;
		}

		// de-registers keyboard delegates
		private void DeregisterKeyboardDelegate()
		{
			txtHouseNo.ShouldReturn -= TextFieldShouldReturn;
			txtAddress1.ShouldReturn -= TextFieldShouldReturn;
			txtAddress2.ShouldReturn -= TextFieldShouldReturn;
			txtTown.ShouldReturn -= TextFieldShouldReturn;
			txtCounty.ShouldReturn -= TextFieldShouldReturn;
			txtPostCode.ShouldReturn -= TextFieldShouldReturn;
		}

		// loads passed through information to be displayed on the view
		private void LoadClientInfo()
		{
			txtClientOrBusinessName.Text = clientName;

			txtHouseNo.Text = houseNo;
			txtAddress1.Text = address1;
			txtAddress2.Text = address2;
			txtTown.Text = town;
			txtCounty.Text = county;
			txtPostCode.Text = postCode;

		}

		#endregion // Private 

		#region Keyboard adjust

		/// <summary>
		/// Set this field to any view inside the scroll view to center this view instead of the current responder
		/// </summary>
		protected UIView ViewToCenterOnKeyboardShown;
		public virtual bool HandlesKeyboardNotifications()
		{
			return false;
		}

		protected virtual void RegisterForKeyboardNotifications()
		{
			NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, OnKeyboardNotification);
			NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, OnKeyboardNotification);
		}

		/// <summary>
		/// Gets the UIView that represents the "active" user input control (e.g. textfield, or button under a text field)
		/// </summary>
		/// <returns>
		/// A <see cref="UIView"/>
		/// </returns>
		protected virtual UIView KeyboardGetActiveView()
		{
			return View.FindFirstResponder();
		}

		private void OnKeyboardNotification(NSNotification notification)
		{
			if (!IsViewLoaded) return;

			//Check if the keyboard is becoming visible
			var visible = notification.Name == UIKeyboard.WillShowNotification;

			//Start an animation, using values from the keyboard
			UIView.BeginAnimations("AnimateForKeyboard");
			UIView.SetAnimationBeginsFromCurrentState(true);
			UIView.SetAnimationDuration(UIKeyboard.AnimationDurationFromNotification(notification));
			UIView.SetAnimationCurve((UIViewAnimationCurve)UIKeyboard.AnimationCurveFromNotification(notification));

			//Pass the notification, calculating keyboard height, etc.
			bool landscape = InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || InterfaceOrientation == UIInterfaceOrientation.LandscapeRight;
			var keyboardFrame = visible
									? UIKeyboard.FrameEndFromNotification(notification)
									: UIKeyboard.FrameBeginFromNotification(notification);

			OnKeyboardChanged(visible, (float)(landscape ? keyboardFrame.Width : keyboardFrame.Height));

			//Commit the animation
			UIView.CommitAnimations();
		}

		/// <summary>
		/// Override this method to apply custom logic when the keyboard is shown/hidden
		/// </summary>
		/// <param name='visible'>
		/// If the keyboard is visible
		/// </param>
		/// <param name='keyboardHeight'>
		/// Calculated height of the keyboard (width not generally needed here)
		/// </param>
		protected virtual void OnKeyboardChanged(bool visible, float keyboardHeight)
		{
			var activeView = ViewToCenterOnKeyboardShown ?? KeyboardGetActiveView();
			if (activeView == null)
				return;

			var scrollView = activeView.FindSuperviewOfType(View, typeof(UIScrollView)) as UIScrollView;
			if (scrollView == null)
				return;

			if (!visible)
				RestoreScrollPosition(scrollView);
			else
				CenterViewInScroll(activeView, scrollView, keyboardHeight);
		}

		protected virtual void CenterViewInScroll(UIView viewToCenter, UIScrollView scrollView, float keyboardHeight)
		{
			var contentInsets = new UIEdgeInsets(0.0f, 0.0f, keyboardHeight, 0.0f);
			scrollView.ContentInset = contentInsets;
			scrollView.ScrollIndicatorInsets = contentInsets;

			// Position of the active field relative isnside the scroll view
			var relativeFrame = viewToCenter.Superview.ConvertRectToView(viewToCenter.Frame, scrollView);

			bool landscape = InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || InterfaceOrientation == UIInterfaceOrientation.LandscapeRight;
			var spaceAboveKeyboard = (landscape ? scrollView.Frame.Width : scrollView.Frame.Height) - keyboardHeight;

			// Move the active field to the center of the available space
			var offset = relativeFrame.Y - (spaceAboveKeyboard - viewToCenter.Frame.Height) / 2;
			scrollView.ContentOffset = new PointF(0, (float)offset);
		}

		protected virtual void RestoreScrollPosition(UIScrollView scrollView)
		{
			scrollView.ContentInset = UIEdgeInsets.Zero;
			scrollView.ScrollIndicatorInsets = UIEdgeInsets.Zero;
		}

		/// <summary>
		/// Call it to force dismiss keyboard when background is tapped
		/// </summary>
		protected void DismissKeyboardOnBackgroundTap()
		{
			// Add gesture recognizer to hide keyboard
			var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
			tap.AddTarget(() => View.EndEditing(true));
			View.AddGestureRecognizer(tap);
		}

		protected bool TextFieldShouldReturn(UITextField textField)
		{
			switch (textField.Tag)
			{
				case 1:
					return txtAddress1.BecomeFirstResponder();
				case 2:
					return txtAddress2.BecomeFirstResponder();
				case 3:
					return txtTown.BecomeFirstResponder();
				case 4:
					return txtCounty.BecomeFirstResponder();
				case 5:
					return txtPostCode.BecomeFirstResponder();
				default:
					return textField.ResignFirstResponder();
			}
		}

		#endregion
	}
}


