// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("FilterViewController")]
    partial class FilterViewController
    {
        [Outlet]
        public UIKit.UISwitch switchOpen { get; set; }


        [Outlet]
        UIKit.UISwitch switchCompleted { get; set; }


        [Outlet]
        UIKit.UISwitch switchCarded { get; set; }


        [Outlet]
        UIKit.UITableView viewFilters { get; set; }


        [Action ("StatusChanged:")]
        partial void StatusChanged (Foundation.NSObject sender);

        void ReleaseDesignerOutlets ()
        {
            if (viewFilters != null) {
                viewFilters.Dispose ();
                viewFilters = null;
            }
        }
    }
}