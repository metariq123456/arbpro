// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
//
//
using System;
using CoreGraphics;
using Foundation;
using UIKit;
using System.Collections.Generic;
using Microsec.Tools.iOS.UI;
using DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class FilterViewController : UIViewController
    {
        #region Constructors

        public FilterViewController() : base("FilterViewController", null)
        {
            Title = @"Filters";
        }

        #endregion

        #region Overrides

        public override void ViewWillAppear(bool animated)
        {
            FiltersTableSource MySource;
            base.ViewWillAppear(animated);

            if (BL.Globals.DemoData)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            } else
            {
                NavigationItem.Prompt = null;
            }

            MySource = new FiltersTableSource(this);
//            MySource.ShowSwitch += HandleShowSwitch;
            viewFilters.Source = MySource;

        }

        #endregion

        #region actions

        partial void StatusChanged(NSObject sender)
        {
            Globals.ListFilter.AppointmentStatusPending = switchOpen.On;
            Globals.ListFilter.AppointmentStatusCompleted = switchCompleted.On;
//			Globals.ListFilter.AppointmentStatusCancelled = switchCancelled.On;
        }

        #endregion

        class FiltersTableSource:UITableViewSource
        {
            #region Fields

            const int SECTION_STATUS = 0;
            const int ROW_STATUS_PENDING = 0;
            const int ROW_STATUS_COMPLETED = 1;
            const int ROW_STATUS_CANCELLED = 2;
            const int ROW_STATUS_PARTIALLYUPLOADED = 3;
            const int ROW_STATUS_UPLOADED = 4;
            const int SECTION_DATES = 1;
            const int ROW_DATES_TODAY = 0;
            const string CELL_TODAY = "TodayCell";
            const string CELL_STATUS = "StatusCell";
            /// <summary>
            /// We need to keep a reference to the switches created in GetCell to stop them being garbage collected.
            /// This doesn't seem to affect memory...
            /// </summary>
            List<UISwitch> Switches = new List<UISwitch>();

            // TODO - remove this once we've determined we don't need it
            //FilterViewController _Controller;

            public delegate void mySwitchHandler (object sender, MyEventArgs e);
            // TODO - remove this once we've determined we don't need it
            //			public event mySwitchHandler ShowSwitch = delegate{};

            #endregion

            #region Constructors

            public FiltersTableSource() : base()
            {

            }


            public FiltersTableSource(FilterViewController controller)
            {
                // TODO - remove this once we've determined we don't need it
//				_Controller = controller;
            }

            #endregion

            #region Handlers

            /// <summary>
            /// Handles the switches for the status filter settings.
            /// </summary>
            protected void HandleStatusSwitchChanged(object sender, MyEventArgs e)
            {
                switch (e.indexPath.Row)
                {
                case ROW_STATUS_PENDING:
                    Globals.ListFilter.AppointmentStatusPending = e.SwitchState;
                    break;
                case ROW_STATUS_COMPLETED:
                    Globals.ListFilter.AppointmentStatusCompleted = e.SwitchState;
                    break;
                case ROW_STATUS_CANCELLED:
                    Globals.ListFilter.AppointmentStatusCancelled = e.SwitchState;
                    break;
                case ROW_STATUS_PARTIALLYUPLOADED:
                    Globals.ListFilter.AppointmentStatusPartiallyUploaded = e.SwitchState;
                    break;
                case ROW_STATUS_UPLOADED:
                    Globals.ListFilter.AppointmentStatusUploaded = e.SwitchState;
                    break;
                default:
                    break;
                }
            }

            /// <summary>
            /// Handles the switches for the today filter settings.
            /// </summary>
            protected void HandleTodaySwitchChanged(object sender, MyEventArgs e)
            {
                Globals.ListFilter.TodaysAppointments = e.SwitchState;
    
            }

            #endregion

            #region Overrides

            public override nint NumberOfSections(UITableView tableView)
            {
                return 2;
            }

            public override string TitleForHeader(UITableView tableView, nint section)
            {
                switch (section)
                {
                case SECTION_STATUS:
                    return "Status";
                case SECTION_DATES:
                    return "Dates";
                default:
                    return string.Format("bad section{0}", section);
                }
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                switch (section)
                {
                case SECTION_STATUS:
                    return 5;
                case SECTION_DATES:
                    return 1;
                default:
                    return 0;
                }
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                UITableViewCell Cell = null;

                switch (indexPath.Section)
                {
                case SECTION_STATUS:
                    Cell = tableView.DequeueReusableCell(CELL_STATUS);
                    if (Cell == null)
                    {
                        Cell = new UITableViewCell(UITableViewCellStyle.Default, CELL_STATUS);
                        UISwitch MySwitch = new UISwitch(new CGRect(0, 0, 100, 25));
                        MySwitch.ValueChanged += delegate(object sender, EventArgs e)
                        {
                            MyEventArgs myE = new MyEventArgs();
                            myE.SwitchState = MySwitch.On;
                            myE.indexPath = indexPath;
                            HandleStatusSwitchChanged(this, myE);
                        };

                        Cell.AccessoryView = MySwitch;
                        Switches.Add(MySwitch);

                    }

                    switch (indexPath.Row)
                    {
                    case ROW_STATUS_PENDING:
                        Cell.TextLabel.Text = "Pending";
                        (Cell.AccessoryView as UISwitch).On = Globals.ListFilter.AppointmentStatusPending;
                        break;
                    case ROW_STATUS_COMPLETED:
                        Cell.TextLabel.Text = "Completed";
                        (Cell.AccessoryView as UISwitch).On = Globals.ListFilter.AppointmentStatusCompleted;
                        break;
                    case ROW_STATUS_CANCELLED:
                        Cell.TextLabel.Text = "Cancelled";
                        (Cell.AccessoryView as UISwitch).On = Globals.ListFilter.AppointmentStatusCancelled;
                        break;
                    case ROW_STATUS_PARTIALLYUPLOADED:
                        Cell.TextLabel.Text = "Partially Uploaded";
                        (Cell.AccessoryView as UISwitch).On = Globals.ListFilter.AppointmentStatusPartiallyUploaded;
                        break;
                    case ROW_STATUS_UPLOADED:
                        Cell.TextLabel.Text = "Uploaded";
                        (Cell.AccessoryView as UISwitch).On = Globals.ListFilter.AppointmentStatusUploaded;
                        break;
                    default:
                        break;
                    }
                    break;
                case SECTION_DATES:
                    Cell = tableView.DequeueReusableCell(CELL_TODAY);
                    if (Cell == null)
                    {
                        Cell = new UITableViewCell(UITableViewCellStyle.Default, CELL_TODAY);
                        UISwitch MySwitch = new UISwitch(new CGRect(0, 0, 100, 25));
                        MySwitch.ValueChanged += delegate(object sender, EventArgs e)
                        {
                            MyEventArgs myE = new MyEventArgs();
                            myE.SwitchState = MySwitch.On;
                            myE.indexPath = indexPath;
                            HandleTodaySwitchChanged(this, myE);
                        };

                        Cell.AccessoryView = MySwitch;
                        Switches.Add(MySwitch);

                    }

                    switch (indexPath.Row)
                    {
                    case ROW_DATES_TODAY:
                        Cell.TextLabel.Text = "Only today";
                        (Cell.AccessoryView as UISwitch).On = Globals.ListFilter.TodaysAppointments;
                        break;
                    default:
                        break;
                    }
                    break;
                default:
                    break;
                }
                return Cell;
            }
        }
    }

    public class MyEventArgs : EventArgs
    {
        public NSIndexPath indexPath { get; set; }

        public bool SwitchState { get; set; }

        public MyEventArgs() : base()
        {
        }
    }
}
#endregion

