// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/ImageDisplayViewController/ImageDisplayViewController.cs#8 $
// $Author: Jon $
// $Date: 2015/05/27 $
// $Change: 24685 $
// *********************************************************************
//
//
using System;
using CoreGraphics;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public enum ImageEditTypeEnum
	{
		Picture,
		Signature
	}
	
	public partial class ImageDisplayViewController : UIViewController
	{
		public ImageDisplayViewController (ImageEditTypeEnum mode) : base ("ImageDisplayViewController", null)
		{
			_EditMode = mode;
		}

		public UIImage displayImage { get; set; }
		public delegate void ImageDeleted();
		public event ImageDeleted ImageDeletedEvent;
		public delegate void ImageSaved(UIImage newImage);
		public event ImageSaved ImageSavedEvent;

		UIBarButtonItem _NavButtonSave;
		ImageEditTypeEnum _EditMode;

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			
			if (_EditMode == ImageEditTypeEnum.Picture) {
				if (displayImage != null) {
					EditingView.UserInteractionEnabled = true;
				
					imageView.Image = displayImage;
					_NavButtonSave = new UIBarButtonItem (@"Save", UIBarButtonItemStyle.Done, ButtonSaveClicked);
					this.NavigationItem.RightBarButtonItem = _NavButtonSave;
				}
				ColourView.ColourPickedEvent += SetLineColour;
			} else {
				this.Title = "Signature";
				this.EditingView.SetLineWidth (2f);
				this.EditingView.SetLineColour (UIColor.Black);
				
				_NavButtonSave = new UIBarButtonItem (@"Complete", UIBarButtonItemStyle.Done, ButtonSaveClicked);
				
				if (this.displayImage != null) {
					imageView.Image = displayImage;
					ToolBar.SetItems (new UIBarButtonItem[] { SpacerRight, BtnDelete }, false);
				} else {
					ToolBar.SetItems (new UIBarButtonItem[] { BtnClear, SpacerRight, BtnDelete }, false);
					this.NavigationItem.RightBarButtonItem = _NavButtonSave;
				}
				_NavButtonSave.Enabled = false;
			}
			ColourView.Hidden = true;
			
			this.EditingView.PathCountChangedEvent += ImageContextChanged;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			//Trick the nib into instantiating the outlets.
			ColourView.Hidden = false;
			
			if (BL.Globals.DemoData)
			{
				NavigationItem.Prompt = "Demonstration Mode";
			}
			else
			{
				NavigationItem.Prompt = null;
			}
		}
		
		
		
		#region Actions
		partial void ChangeLineColour (Foundation.NSObject sender)
		{
			ColourView.SetCurrentColour (EditingView.LineColour);
			ColourView.Hidden = !ColourView.Hidden;
			if(!ColourView.Hidden)
				sizeView.Hidden = true;
		}

		partial void ChangeLineSize (Foundation.NSObject sender)
		{
			processView.Value = (EditingView.LineWidth - 1f)/4f;
			sizeView.Hidden = !sizeView.Hidden;
			if(!sizeView.Hidden)
				ColourView.Hidden = true;
		}

		partial void ClearEdits (Foundation.NSObject sender)
		{
			EditingView.ResetImage();
		}
		
		partial void UndoLastEdit (NSObject sender)
		{
			EditingView.UndoLastEdit ();
		}
		
		partial void SetPointSize (NSObject sender)
		{
			EditingView.SetLineWidth((processView.Value * 4) + 1f);
			sizeView.Hidden = true;
		}

		partial void DeletePicture (Foundation.NSObject sender)
		{
			if (ImageDeletedEvent != null)
			{
				ImageDeletedEvent();
			}

			NavigationController.PopViewController (true);
		}
		#endregion
		
		void SetLineColour(UIColor value)
		{
			EditingView.SetLineColour(value);
		}
		
		void CombineImageContexts()
		{
			displayImage = EditingView.CombineImageContext(displayImage);
		}
		
		void GetImageContext()
		{
			displayImage = EditingView.GetImageContext ();
		}
		
		void ButtonSaveClicked(Object sender, EventArgs e)
		{
			if (ImageSavedEvent != null)
			{
				if(_EditMode == ImageEditTypeEnum.Picture)
					CombineImageContexts();
				else
					GetImageContext();
					
				ImageSavedEvent(displayImage);
			}

			NavigationController.PopViewController (true);
		}
		
		void ImageContextChanged(int pathCount)
		{
			_NavButtonSave.Enabled = pathCount > 0;
		}
	}
}

