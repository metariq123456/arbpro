using System;
using CoreGraphics;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class ImageEditingView : UIView
	{	
		public delegate void PathCountChangedDelegate(int pathCount);
		public event PathCountChangedDelegate PathCountChangedEvent;
	
		#region Members
		CGPath _CurrentPath;
		List<CGPath> _EditPaths;
		bool _IsEdit = false;
		#endregion
		
		public float LineWidth {get;set;}
		public UIColor LineColour { get; set; }
		#region Constructors
		public ImageEditingView () : base ()
		{
			Initialise ();
		}

		public ImageEditingView (IntPtr handle) : base (handle)
		{
			Initialise ();
		}
		#endregion
		
		#region Overrides
		public override void TouchesBegan (NSSet touches, UIEvent evt)
		{
			base.TouchesBegan (touches, evt);
			UITouch touch = touches.AnyObject as UITouch;
			if (touch != null) {
				_IsEdit = true;
				_CurrentPath = new CGPath ();
				_CurrentPath.MoveToPoint (touch.LocationInView (this));
			}
		}

		public override void TouchesMoved (NSSet touches, UIEvent evt)
		{
			base.TouchesMoved (touches, evt);
			UITouch touch = touches.AnyObject as UITouch;
			if (touch != null) {
				_CurrentPath.AddLineToPoint (touch.LocationInView (this));
				_CurrentPath.MoveToPoint (touch.LocationInView (this));
			}
			SetNeedsDisplay ();
		}

		public override void TouchesEnded (NSSet touches, UIEvent evt)
		{
			base.TouchesEnded (touches, evt);
			UITouch touch = touches.AnyObject as UITouch;
			if (touch != null) {
				_EditPaths.Add (_CurrentPath);
				_CurrentPath = new CGPath ();
			}
			SetNeedsDisplay ();
			
			if(PathCountChangedEvent != null)
				PathCountChangedEvent(_EditPaths.Count);
		}

		public override void Draw (CGRect rect)
		{
			base.Draw (rect);
			DrawInContext (UIGraphics.GetCurrentContext ());
		}
		#endregion
		
		#region Private Methods
		void Initialise ()
		{
			_EditPaths = new List<CGPath> ();
			this.BackgroundColor = UIColor.Clear;
			LineWidth = 5; // default the line width
			LineColour = UIColor.Red; // default the line colour
		}
		
		void DrawInContext (CGContext context)
		{
			//context.DrawImage (this.Bounds, _ContextImage);
			if (_IsEdit) {
				context.SetAllowsAntialiasing(true);
				context.SetStrokeColor (LineColour.CGColor);
				context.SetLineWidth (LineWidth);
				context.SetLineJoin (CGLineJoin.Round);
				context.SetLineCap (CGLineCap.Round);
				
				context.AddPath (_CurrentPath);
				context.DrawPath (CGPathDrawingMode.Stroke);
				foreach (CGPath p in _EditPaths) {
					context.AddPath (p);
					context.DrawPath (CGPathDrawingMode.Stroke);
				}
			}
		}
		#endregion
		
		#region Public Methods
		public void UndoLastEdit ()
		{
			if (_EditPaths.Count > 0) 
			{
				_EditPaths.RemoveAt (_EditPaths.Count - 1);
				SetNeedsDisplay ();
			}
			
			if(PathCountChangedEvent != null)
				PathCountChangedEvent(_EditPaths.Count);
		}
		
		public void SetLineWidth(float value)
		{
			LineWidth = value;
			SetNeedsDisplay ();
		}
		
		public void SetLineColour(UIColor value)
		{
			LineColour = value;
			SetNeedsDisplay ();
		}
		
		public UIImage CombineImageContext (UIImage image)
		{
			UIImage returnImage = null;
			
			UIGraphics.BeginImageContextWithOptions (this.Bounds.Size, true, 1.5f);
			//UIGraphics.BeginImageContext (image.Size);
			
			CGContext context = UIGraphics.GetCurrentContext ();

			context.InterpolationQuality = CGInterpolationQuality.High;
			context.SetAllowsAntialiasing(true);
			context.SetShouldAntialias(true);
			context.SetAllowsSubpixelPositioning(true);
			image.Draw(this.Bounds);
			//context.DrawImage (this.Bounds, image.CGImage);
			context.SetStrokeColor (LineColour.CGColor);
			context.SetLineWidth (LineWidth);
			context.SetLineJoin (CGLineJoin.Round);
			context.SetLineCap (CGLineCap.Round);
			foreach (CGPath p in _EditPaths) {
				context.AddPath (p);
				context.DrawPath (CGPathDrawingMode.Stroke);
			}
			
			
			returnImage = UIGraphics.GetImageFromCurrentImageContext ();
			
			UIGraphics.EndImageContext ();
			
			return returnImage;
		}
		
		public UIImage GetImageContext ()
		{
			UIImage returnImage = null;
			
			UIGraphics.BeginImageContextWithOptions (this.Bounds.Size, true, 1.5f);
			//UIGraphics.BeginImageContext (image.Size);
			
			CGContext context = UIGraphics.GetCurrentContext ();
			context.InterpolationQuality = CGInterpolationQuality.High;
			context.SetAllowsAntialiasing(true);
			context.SetShouldAntialias(true);
			context.SetAllowsSubpixelPositioning(true);
			context.SetStrokeColor (LineColour.CGColor);
			context.SetLineWidth (LineWidth);
			context.SetLineJoin (CGLineJoin.Round);
			context.SetLineCap (CGLineCap.Round);
			context.SetAlpha (1f);
			
			UIColor.White.SetFill ();
			context.FillRect(Bounds);
			
			foreach (CGPath p in _EditPaths) {
				context.AddPath (p);
				context.DrawPath (CGPathDrawingMode.Stroke);
			}
			
			
			returnImage = UIGraphics.GetImageFromCurrentImageContext ();
			
			UIGraphics.EndImageContext ();
			
			return returnImage;
		}
		
		public void ResetImage ()
		{
			_EditPaths.Clear ();
			SetNeedsDisplay ();
			
			if(PathCountChangedEvent != null)
				PathCountChangedEvent(_EditPaths.Count);
		}
		#endregion
	}
}

