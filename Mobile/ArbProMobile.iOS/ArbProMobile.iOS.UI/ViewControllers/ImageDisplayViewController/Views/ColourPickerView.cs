using System;
using CoreGraphics;

using Foundation;
using UIKit;
using CoreAnimation;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class ColourPickerView : UIView
	{
		public delegate void ColourPickedDelegate(UIColor picked);
		public event ColourPickedDelegate ColourPickedEvent;
		
		UIColor _CurrentColour = UIColor.White;
		// TODO - remove these once we've determined we don't need it
//		UILabel LblRedValue = null;
//		UILabel LblGreenValue = null;
//		UILabel LblBlueValue = null;
		UILabel LblCurrentColour = null;
		
		UISlider SliderRed = null;
		UISlider SliderGreen = null;
		UISlider SliderBlue = null;
		
		UIButton BtnOk = null;
		UIButton BtnCancel = null;
		
		public ColourPickerView () : base ()
		{
			Initialise();
		}
		
		public ColourPickerView (IntPtr handle) : base (handle)
		{
			Initialise();
		}
		
		
		void Initialise()
		{
			UIColor ArbPro = new UIColor(157.0f/255.0f,192.0f/255.0f,75.0f/255.0f,1f);
			this.BackgroundColor = UIColor.Clear;
			//Construct the value display labels
			//LblRedValue = CreateColourValueLabel(25f);//new UILabel(new RectangleF(258,25,42,21));
			//LblGreenValue = CreateColourValueLabel(55f);//new UILabel(new RectangleF(258,55,42,21));
			//LblBlueValue = CreateColourValueLabel(85f);//new UILabel(new RectangleF(258,85,42,21));
			//Construct the colour display label
			LblCurrentColour = new UILabel(new CGRect(18,119,118,28));
			//Set the look of the colour display label
			UIBezierPath corner_mask = UIBezierPath.FromRoundedRect (LblCurrentColour.Bounds,UIRectCorner.AllCorners,new CGSize(25f,25f));
			CAShapeLayer layer = new CAShapeLayer();
			layer.Frame = LblCurrentColour.Bounds;
			layer.Path = corner_mask.CGPath;
			LblCurrentColour.Layer.Mask = layer;
			//Construct the sliders
			SliderBlue = CreateSlider(85, UIColor.Blue);//new UISlider(new RectangleF(134,92,237,23));
			SliderGreen = CreateSlider(55, ArbPro);//new UISlider(new RectangleF(134,62,237,23));
			SliderRed = CreateSlider(25, UIColor.Red);//new UISlider(new RectangleF(134,32,237,23));
			//Construct Ok button
			BtnOk = UIButton.FromType(UIButtonType.RoundedRect);
			BtnOk.Frame = new CGRect(227,119,73,28);
			BtnOk.SetTitle("Ok", UIControlState.Normal);
			BtnOk.SetTitleColor (UIColor.White, UIControlState.Normal);
			BtnOk.SetTitleColor (UIColor.Black,UIControlState.Selected);
			BtnOk.TintColor = ArbPro;
			BtnOk.BackgroundColor = ArbPro;
			BtnOk.TouchUpInside += ConfirmSetColour;
			//Construct Cancel button
			BtnCancel = UIButton.FromType(UIButtonType.RoundedRect);
			BtnCancel.Frame = new CGRect(146,119,73,28);
			BtnCancel.SetTitle("Cancel", UIControlState.Normal);
			BtnCancel.SetTitleColor (UIColor.White, UIControlState.Normal);
			BtnCancel.SetTitleColor (UIColor.Black,UIControlState.Selected);
			BtnCancel.TintColor = ArbPro;
			BtnCancel.BackgroundColor = ArbPro;
			BtnCancel.TouchUpInside += CancelSetColour;
			//Add the views.
			this.AddSubviews (new UIView[]{LblCurrentColour,SliderRed,SliderGreen,SliderBlue,BtnOk,BtnCancel});//,LblRedValue,LblGreenValue,LblBlueValue});
		}
		
		UISlider CreateSlider( float yPos, UIColor colour)
		{
			UISlider slider = new UISlider(new CGRect(18,yPos,284,23));
			slider.MinimumTrackTintColor = colour;
			slider.ThumbTintColor = colour;
			slider.MaxValue = 255;
			slider.ValueChanged += SliderValueChanged;
			
			return slider;
		}
		
		UILabel CreateColourValueLabel(float yPos)
		{
			UILabel label = new UILabel(new CGRect(258,yPos,42,21));
			label.TextAlignment = UITextAlignment.Center;
			label.AdjustsFontSizeToFitWidth = true;
			//Set the look of the display label
			UIBezierPath corner_mask = UIBezierPath.FromRoundedRect (label.Bounds,UIRectCorner.AllCorners,new CGSize(25f,25f));
			CAShapeLayer layer = new CAShapeLayer();
			layer.Frame = label.Bounds;
			layer.Path = corner_mask.CGPath;
			label.Layer.Mask = layer;
			
			return label;
		}
		
		void SliderValueChanged (object sender, EventArgs e)
		{
			int rVal = (int)SliderRed.Value;
			int gVal = (int)SliderGreen.Value;
			int bVal = (int)SliderBlue.Value;
		
			float red = rVal / 255f;
			float green = gVal / 255f;
			float blue = bVal / 255f;
			
			//LblRedValue.Text = rVal.ToString ();
			//LblGreenValue.Text = gVal.ToString ();
			//LblBlueValue.Text = bVal.ToString ();
			
			_CurrentColour = new UIColor(red,green ,blue ,1f);
			LblCurrentColour.BackgroundColor = _CurrentColour;			
		}
		
		void CancelSetColour (object sender, EventArgs e)
		{
			this.Hidden = true;
		}
		
		void ConfirmSetColour (object sender, EventArgs e)
		{
			if(ColourPickedEvent != null)
				ColourPickedEvent(_CurrentColour);
				
			this.Hidden = true;
		}
		
		
		public void SetCurrentColour(UIColor value)
		{
			_CurrentColour = value;
			LblCurrentColour.BackgroundColor = _CurrentColour;
			nfloat red = 0;
			nfloat green = 0;
			nfloat blue = 0;
			nfloat alpha = 0f;
			_CurrentColour.GetRGBA(out red, out green, out blue, out alpha);

			SliderRed.SetValue ((float)red * 255,true);
			SliderGreen.SetValue ((float)green * 255,true);
			SliderBlue.SetValue ((float)blue * 255,true);
			
			//LblRedValue.Text = ((int)SliderRed.Value).ToString ();
			//LblGreenValue.Text = ((int)SliderGreen.Value).ToString ();
			//LblBlueValue.Text = ((int)SliderBlue.Value).ToString ();
		}
	}
}

