// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("ImageDisplayViewController")]
    partial class ImageDisplayViewController
    {
        [Outlet]
        UIKit.UIBarButtonItem BtnClear { get; set; }


        [Outlet]
        UIKit.UIBarButtonItem BtnColour { get; set; }


        [Outlet]
        UIKit.UIBarButtonItem BtnDelete { get; set; }


        [Outlet]
        UIKit.UIBarButtonItem BtnSize { get; set; }


        [Outlet]
        UIKit.UIBarButtonItem BtnUndo { get; set; }


        [Outlet]
        DGS.ArbPro.Mobile.iOS.UI.ColourPickerView ColourView { get; set; }


        [Outlet]
        DGS.ArbPro.Mobile.iOS.UI.ImageEditingView EditingView { get; set; }


        [Outlet]
        UIKit.UIImageView imageView { get; set; }


        [Outlet]
        UIKit.UISlider processView { get; set; }


        [Outlet]
        UIKit.UIView sizeView { get; set; }


        [Outlet]
        UIKit.UIBarButtonItem SpacerLeft { get; set; }


        [Outlet]
        UIKit.UIBarButtonItem SpacerRight { get; set; }


        [Outlet]
        UIKit.UIToolbar ToolBar { get; set; }


        [Action ("ChangeLineColour:")]
        partial void ChangeLineColour (Foundation.NSObject sender);


        [Action ("ChangeLineSize:")]
        partial void ChangeLineSize (Foundation.NSObject sender);


        [Action ("ClearEdits:")]
        partial void ClearEdits (Foundation.NSObject sender);


        [Action ("UndoLastEdit:")]
        partial void UndoLastEdit (Foundation.NSObject sender);


        [Action ("SetPointSize:")]
        partial void SetPointSize (Foundation.NSObject sender);


        [Action ("DeletePicture:")]
        partial void DeletePicture (Foundation.NSObject sender);

        void ReleaseDesignerOutlets ()
        {
            if (BtnClear != null) {
                BtnClear.Dispose ();
                BtnClear = null;
            }

            if (BtnColour != null) {
                BtnColour.Dispose ();
                BtnColour = null;
            }

            if (BtnDelete != null) {
                BtnDelete.Dispose ();
                BtnDelete = null;
            }

            if (BtnSize != null) {
                BtnSize.Dispose ();
                BtnSize = null;
            }

            if (BtnUndo != null) {
                BtnUndo.Dispose ();
                BtnUndo = null;
            }

            if (ColourView != null) {
                ColourView.Dispose ();
                ColourView = null;
            }

            if (EditingView != null) {
                EditingView.Dispose ();
                EditingView = null;
            }

            if (imageView != null) {
                imageView.Dispose ();
                imageView = null;
            }

            if (processView != null) {
                processView.Dispose ();
                processView = null;
            }

            if (sizeView != null) {
                sizeView.Dispose ();
                sizeView = null;
            }

            if (SpacerLeft != null) {
                SpacerLeft.Dispose ();
                SpacerLeft = null;
            }

            if (SpacerRight != null) {
                SpacerRight.Dispose ();
                SpacerRight = null;
            }

            if (ToolBar != null) {
                ToolBar.Dispose ();
                ToolBar = null;
            }
        }
    }
}