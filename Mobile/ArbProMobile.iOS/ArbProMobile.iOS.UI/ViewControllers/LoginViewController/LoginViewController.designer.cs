// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("LoginViewController")]
    partial class LoginViewController
    {
        [Outlet]
        DGS.ArbPro.Mobile.iOS.UI.NetworkActivityView ActivityView { get; set; }


        [Outlet]
        UIKit.UIButton btnLogout { get; set; }


        [Outlet]
        UIKit.UIButton btnOfflineLogin { get; set; }


        [Outlet]
        UIKit.UIButton btnOnlineLogin { get; set; }


        [Outlet]
        UIKit.UILabel lblAuthenticationStatus { get; set; }


        [Outlet]
        UIKit.UILabel lblLoginStatus { get; set; }


        [Outlet]
        UIKit.UILabel LblServiceStatus { get; set; }


        [Outlet]
        UIKit.UITextField TxtPassword { get; set; }


        [Outlet]
        UIKit.UITextField TxtUserName { get; set; }


        [Action ("UserEntryCompleted:")]
        partial void UserEntryCompleted (Foundation.NSObject sender);

        void ReleaseDesignerOutlets ()
        {
            if (ActivityView != null) {
                ActivityView.Dispose ();
                ActivityView = null;
            }

            if (btnLogout != null) {
                btnLogout.Dispose ();
                btnLogout = null;
            }

            if (btnOfflineLogin != null) {
                btnOfflineLogin.Dispose ();
                btnOfflineLogin = null;
            }

            if (btnOnlineLogin != null) {
                btnOnlineLogin.Dispose ();
                btnOnlineLogin = null;
            }

            if (lblAuthenticationStatus != null) {
                lblAuthenticationStatus.Dispose ();
                lblAuthenticationStatus = null;
            }

            if (lblLoginStatus != null) {
                lblLoginStatus.Dispose ();
                lblLoginStatus = null;
            }

            if (LblServiceStatus != null) {
                LblServiceStatus.Dispose ();
                LblServiceStatus = null;
            }

            if (TxtPassword != null) {
                TxtPassword.Dispose ();
                TxtPassword = null;
            }

            if (TxtUserName != null) {
                TxtUserName.Dispose ();
                TxtUserName = null;
            }
        }
    }
}