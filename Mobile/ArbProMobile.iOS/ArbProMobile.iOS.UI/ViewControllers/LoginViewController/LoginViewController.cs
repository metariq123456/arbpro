// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
//
//
using System;
using CoreGraphics;
using System.Threading.Tasks;
using Foundation;
using UIKit;
using DGS.ArbPro.Mobile.Core.Enums;
using DGS.ArbPro.Mobile.BL;
using DGS.ArbPro.Mobile.Interfaces;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class LoginViewController : UIViewController
	{

		#region Members

		// A reference to the buttons is kept to prevent GC from disposing
		UIBarButtonItem _NavButtonCancel = null;
		string _Error = "";
		bool _ReturnToRoot = true;
		NetworkStatus _NetworkStatus = NetworkStatus.NotReachable;
		Microsec.Mobile.Core.Logging.ILogger log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();

		#endregion

		#region Constructors

		public LoginViewController (bool returnToRoot = true)
		{
			NSNotificationCenter.DefaultCenter.AddObserver (UIApplication.WillEnterForegroundNotification, WillEnterForeGround);
		
			_ReturnToRoot = returnToRoot;
			SetupNavigationBar ();
		}

		public LoginViewController (string loginCaption, bool returnToRoot = true) : base ("LoginViewController", null)
		{ 
			NSNotificationCenter.DefaultCenter.AddObserver (UIApplication.WillEnterForegroundNotification, WillEnterForeGround);

			_ReturnToRoot = returnToRoot;
			SetupNavigationBar ();
			_Error = loginCaption;
		}

		#endregion

		#region Overrides

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			TxtUserName.Text = Settings.Username;

			btnOfflineLogin.TouchUpInside += (sender, e) => {
				if (DGS.ArbPro.Mobile.Core.ArbProInsights.LevelOn (LoggingLevel.Info)) {
					Xamarin.Insights.Track ("Offline login");
				}

				Connect (true);
			};

			btnOnlineLogin.TouchUpInside += (sender, e) => {
				if (DGS.ArbPro.Mobile.Core.ArbProInsights.LevelOn (LoggingLevel.Info)) {
					Xamarin.Insights.Track ("Online login");
				}

				Connect (false);
			};

			btnLogout.TouchUpInside += (sender, e) => {
				if (DGS.ArbPro.Mobile.Core.ArbProInsights.LevelOn (LoggingLevel.Info)) {
					Xamarin.Insights.Track ("Logout");
				}

				Globals.Logout ();
				BusinessLayer.LogoutOfService ();
				SetMessagesAndButtons ();
				btnLogout.Enabled = false;
			};

			this.TxtPassword.ShouldReturn += (textField) => { 
				textField.ResignFirstResponder ();
				return true; 
			};

			this.TxtUserName.ShouldReturn += (textField) => { 
				textField.ResignFirstResponder ();
				return true; 
			};
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			lblAuthenticationStatus.Text = "";
			CheckConnectivity ();
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		#endregion

		#region Actions

		void WillEnterForeGround (NSNotification message)
		{
			try {
				CheckConnectivity ();
			} catch (Exception ex) {
				Console.WriteLine (ex.ToString ());
			}
		}

		partial void UserEntryCompleted (NSObject sender)
		{
			TxtPassword.ResignFirstResponder ();
		}

		void BtnCancelClicked (Object sender, EventArgs e)
		{
			if (this.NavigationController != null) {
				if (_ReturnToRoot) {
					Globals.Logout ();
					this.NavigationController.PopToRootViewController (true);
				} else {
					this.NavigationController.PopViewController (true);
				}
			}
		}

		void Connect (bool Offline)
		{
			_Error = "";
			TxtUserName.ResignFirstResponder ();
			TxtPassword.ResignFirstResponder ();
			ActivityView.DisplayNetworkIndicator (false);
			LoginToService (TxtUserName.Text, TxtPassword.Text, Offline);
		}

		#endregion

		#region Private Methods

		void SetMessagesAndButtons ()
		{
			string msg = "";
			// Set the connection message and login status message

			switch (_NetworkStatus) {
            case NetworkStatus.NotReachable:
                BL.BusinessLayer.ConnectionType = "None";
				msg = "Cannot connect - offline login only";
				break;
            case NetworkStatus.ReachableViaCarrierDataNetwork:
                BL.BusinessLayer.ConnectionType = "Mobile";
				msg = "Connected via mobile network";
				break;
            case NetworkStatus.ReachableViaWiFiNetwork:
                BL.BusinessLayer.ConnectionType = "WiFi";
				msg = "Connected via WiFi";
				break;
			case NetworkStatus.Unknown:
				msg = "Unknown";
				_Error = "Error during service status check.";
				break;
			}

			//The application sometimes reaches this point before the outlets have been created.
			bool instantiated = false;
			while (!instantiated) {
				if (LblServiceStatus != null && lblAuthenticationStatus != null) {
					instantiated = true;
					LblServiceStatus.Text = msg;
					this.lblAuthenticationStatus.Text = _Error;
				}
			}	

			switch (Globals.LoginStatus) {
			case Globals.LoggedInStatus.LoggedInOffline:
				lblLoginStatus.Text = string.Format ("Logged in offline as {0}", Settings.Username);	
				break;
			case Globals.LoggedInStatus.LoggedInOnline:
				lblLoginStatus.Text = string.Format ("Logged in online as {0}", Settings.Username);
				break;
			default:
				lblLoginStatus.Text = "Not logged in";
				break;
			}

			// Now set the buttons

			// Offline login - only enabled if not already logged in, and we have a username (i.e. the user has 
			// previously logged in online
			if (Globals.LoginStatus == Globals.LoggedInStatus.NotLoggedIn &&
			    string.IsNullOrEmpty (Settings.Username) == false) {
				btnOfflineLogin.Enabled = true;
			} else {
				btnOfflineLogin.Enabled = false;
			}

			// Online login - only enabled if not already logged in and we have a network connection
			if (Globals.LoginStatus == Globals.LoggedInStatus.NotLoggedIn &&
			    (_NetworkStatus == NetworkStatus.ReachableViaWiFiNetwork || _NetworkStatus == NetworkStatus.ReachableViaCarrierDataNetwork)) {
				btnOnlineLogin.Enabled = true;
				
			} else {
				btnOnlineLogin.Enabled = false;
			}

			// Logout - only enabled if logged in
			if (Globals.LoginStatus != Globals.LoggedInStatus.NotLoggedIn) {
				btnLogout.Enabled = true;
			} else {
				btnLogout.Enabled = false;
			}


		}

		void SetupNavigationBar ()
		{
			this.Title = @"Login";
			_NavButtonCancel = new UIBarButtonItem (@"Back", UIBarButtonItemStyle.Bordered, BtnCancelClicked);
			this.NavigationItem.LeftBarButtonItem = _NavButtonCancel;
		}

		async void CheckConnectivity ()
		{
			bool isError = false;
			try {
				ActivityView.DisplayNetworkIndicator (false);
				await AsyncCheckConnectivity ().ConfigureAwait (true);

				ActivityView.DisplayNetworkIndicator (true);
			} catch (Exception ex) {
				await log.Debug(string.Format("Check connectivity error - {0}.", ex.Message));
				Console.WriteLine ("Check connectivity error - {0}", ex.ToString ());
				isError = true;
			}
			if (isError) {
				try {
					await MessageBox.Show ("Could not check connectivity status. Please click ok to continue.");
				} catch {
				}
			}
			SetMessagesAndButtons ();
		}

		async Task AsyncCheckConnectivity ()
		{
			try {
				await Task.Run (() => _NetworkStatus = ConnectionStatus.CheckReachability ()).ConfigureAwait (false);// BusinessLayer.AppointmentListDownload (Globals.CurrentUser.Id)).ConfigureAwait (false);
			} catch (Exception) {
				_NetworkStatus = NetworkStatus.Unknown;
			}
		}

		async void LoginToService (string username, string password, bool local)
		{
			if (!local) {
				await LoginToServiceAsync (username, password).ConfigureAwait (true);
				if (Globals.CurrentUser != null && this.NavigationController != null) {
					Globals.LoginStatus = Globals.LoggedInStatus.LoggedInOnline;
					Settings.CurrentUser = Globals.CurrentUser;
					Settings.SavePassword (password);
					Settings.RefreshCachedSettings ();
					this.NavigationController.PopViewController (true);
					await log.Debug("Login status - Logged In Online.");
				} else {
					Globals.LoginStatus = Globals.LoggedInStatus.NotLoggedIn;
					lblAuthenticationStatus.Text = _Error;
					ActivityView.DisplayNetworkIndicator (true);
					await log.Debug("Login status - Not Logged In.");
				}
			} else {
				//Attempt offline
				if (Settings.Username == username && Settings.ValidatePassword (password)) {
					Core.ArbProInsights.SetIdentity (Settings.Username, "");
					Globals.CurrentUser = Settings.CurrentUser;
					Globals.LoginStatus = Globals.LoggedInStatus.LoggedInOffline;	
					this.NavigationController.PopViewController (true);
					await log.Debug("Login status - Logged In Offline.");
				} else {
					Globals.LoginStatus = Globals.LoggedInStatus.NotLoggedIn;
					lblAuthenticationStatus.Text = string.Format ("Offline login failed for {0}", username);
					ActivityView.DisplayNetworkIndicator (true);
					await log.Debug("Login status - Not Logged In.");
				}
			}
		}

		async Task LoginToServiceAsync (string username, string password)
		{
			try {
				await Task.Run (() => BusinessLayer.LoginToService (username, password)).ConfigureAwait (false);
				if (Globals.CurrentUser == null) {
					_Error = string.Format ("Online login failed for {0}", username);
				}
			} catch (Exception) {
				_Error = string.Format ("Online login failed for {0}", username);
			}
		}

		#endregion

	}
}

