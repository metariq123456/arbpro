﻿// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
using System;
using System.Drawing;
using System.Threading.Tasks;
using DGS.ArbPro.Mobile.BL;
using Foundation;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class AppointmentEditContactViewController : UIViewController
	{
		BL.Appointment appointment;
		UIBarButtonItem _BtnDone;
		
		public AppointmentEditContactViewController(BL.Appointment appt) : base("AppointmentEditContactViewController", null)
		{
			appointment = appt;
			_BtnDone = new UIBarButtonItem("Done", UIBarButtonItemStyle.Plain, this.BtnDoneClicked);
			this.NavigationItem.RightBarButtonItem = _BtnDone;
		}

		#region overrides

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			DismissKeyboardOnBackgroundTap();
			RegisterForKeyboardNotifications();
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			this.NavigationController.NavigationBar.Translucent = false;
			RegisterKeyboardDelegate();
			LoadClientInfo();

			if (BL.Globals.DemoData)
			{
				NavigationItem.Prompt = "Demonstration Mode";
			}
			else
			{
				NavigationItem.Prompt = null;
			}
		}

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
			DeregisterKeyboardDelegate();
		}

		#endregion

		#region Private Methods

		// saves values and passes them on to the appointment new view controller
		private void BtnDoneClicked(object Sender, EventArgs e)
		{
			UIAlertView alert = new UIAlertView();
			if (!string.IsNullOrWhiteSpace(txtAddress1.Text))
			{
				this.UpdateAppointmentObject();

				this.SaveClientContactToDb();

				NavigationController.PopViewController(true);
			}
			else
			{
				alert.Message = "Ensure that address line 1 is not empty";
				alert.AddButton("Ok");
				alert.Show();
			}
		}

		// registers keyboard delegate - keyboard on next button press goes to next text field
		private void RegisterKeyboardDelegate()
		{
			txtTitle.Tag = 1;
			txtFirstName.Tag = 2;
			txtSurname.Tag = 3;
			txtHouseNo.Tag = 4;
			txtAddress1.Tag = 5;
			txtAddress2.Tag = 6;
			txtTown.Tag = 7;
			txtCounty.Tag = 8;
			txtPostCode.Tag = 9;
			txtTelephone.Tag = 10;
			txtMobile.Tag = 11;
			txtEmail.Tag = 12;

			txtTitle.ShouldReturn += TextFieldShouldReturn;
			txtFirstName.ShouldReturn += TextFieldShouldReturn;
			txtSurname.ShouldReturn += TextFieldShouldReturn;
			txtHouseNo.ShouldReturn += TextFieldShouldReturn;
			txtAddress1.ShouldReturn += TextFieldShouldReturn;
			txtAddress2.ShouldReturn += TextFieldShouldReturn;
			txtTown.ShouldReturn += TextFieldShouldReturn;
			txtCounty.ShouldReturn += TextFieldShouldReturn;
			txtPostCode.ShouldReturn += TextFieldShouldReturn;
			txtTelephone.ShouldReturn += TextFieldShouldReturn;
			txtMobile.ShouldReturn += TextFieldShouldReturn;
			txtEmail.ShouldReturn += TextFieldShouldReturn;
		}

		// de-registers keyboard delegates
		private void DeregisterKeyboardDelegate()
		{
			txtTitle.ShouldReturn -= TextFieldShouldReturn;
			txtFirstName.ShouldReturn -= TextFieldShouldReturn;
			txtSurname.ShouldReturn -= TextFieldShouldReturn;
			txtHouseNo.ShouldReturn -= TextFieldShouldReturn;
			txtAddress1.ShouldReturn -= TextFieldShouldReturn;
			txtAddress2.ShouldReturn -= TextFieldShouldReturn;
			txtTown.ShouldReturn -= TextFieldShouldReturn;
			txtCounty.ShouldReturn -= TextFieldShouldReturn;
			txtPostCode.ShouldReturn -= TextFieldShouldReturn;
			txtTelephone.ShouldReturn -= TextFieldShouldReturn;
			txtMobile.ShouldReturn -= TextFieldShouldReturn;
			txtEmail.ShouldReturn -= TextFieldShouldReturn;
		}

		// loads passed through information to be displayed on the view
		private void LoadClientInfo()
		{
			txtTitle.Text = appointment.Title;
			txtFirstName.Text = appointment.FirstName;
			txtSurname.Text = appointment.Surname;
			txtHouseNo.Text = appointment.ClientStreetNo;
			txtAddress1.Text = appointment.ClientAddress1;
			txtAddress2.Text = appointment.ClientAddress2;
			txtTown.Text = appointment.ClientAddressTown;
			txtCounty.Text = appointment.ClientAddressCounty;
			txtPostCode.Text = appointment.ClientAddressPostCode;
			txtTelephone.Text = appointment.ClientTelephone;
			txtMobile.Text = appointment.ClientMobile;
			txtEmail.Text = appointment.ClientEmail;
		}

		void UpdateAppointmentObject()
		{
			appointment.Title = txtTitle.Text;
			appointment.FirstName = txtFirstName.Text;
			appointment.Surname = txtSurname.Text;
			appointment.ClientName = string.Format("{0} {1} {2}", txtTitle.Text, txtFirstName.Text, txtSurname.Text);
			appointment.ClientStreetNo = txtHouseNo.Text;
			appointment.ClientAddress1 = txtAddress1.Text;
			appointment.ClientAddress2 = txtAddress2.Text;
			appointment.ClientAddressTown = txtTown.Text;
			appointment.ClientAddressCounty = txtCounty.Text;
			appointment.ClientAddressPostCode = txtPostCode.Text;
			appointment.ClientTelephone = txtTelephone.Text;
			appointment.ClientMobile = txtMobile.Text;
			appointment.ClientEmail = txtEmail.Text;
		}

		void SaveClientContactToDb()
		{
			DAL.Repository.Instance().AppointmentSave(appointment);
		}

		#endregion // Private 

		#region Keyboard adjust

		/// <summary>
		/// Set this field to any view inside the scroll view to center this view instead of the current responder
		/// </summary>
		protected UIView ViewToCenterOnKeyboardShown;
		public virtual bool HandlesKeyboardNotifications()
		{
			return false;
		}

		protected virtual void RegisterForKeyboardNotifications()
		{
			NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, OnKeyboardNotification);
			NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, OnKeyboardNotification);
		}

		/// <summary>
		/// Gets the UIView that represents the "active" user input control (e.g. textfield, or button under a text field)
		/// </summary>
		/// <returns>
		/// A <see cref="UIView"/>
		/// </returns>
		protected virtual UIView KeyboardGetActiveView()
		{
			return View.FindFirstResponder();
		}

		private void OnKeyboardNotification(NSNotification notification)
		{
			if (!IsViewLoaded) return;

			//Check if the keyboard is becoming visible
			var visible = notification.Name == UIKeyboard.WillShowNotification;

			//Start an animation, using values from the keyboard
			UIView.BeginAnimations("AnimateForKeyboard");
			UIView.SetAnimationBeginsFromCurrentState(true);
			UIView.SetAnimationDuration(UIKeyboard.AnimationDurationFromNotification(notification));
			UIView.SetAnimationCurve((UIViewAnimationCurve)UIKeyboard.AnimationCurveFromNotification(notification));

			//Pass the notification, calculating keyboard height, etc.
			bool landscape = InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || InterfaceOrientation == UIInterfaceOrientation.LandscapeRight;
			var keyboardFrame = visible
									? UIKeyboard.FrameEndFromNotification(notification)
									: UIKeyboard.FrameBeginFromNotification(notification);

			OnKeyboardChanged(visible, (float)(landscape ? keyboardFrame.Width : keyboardFrame.Height));

			//Commit the animation
			UIView.CommitAnimations();
		}

		/// <summary>
		/// Override this method to apply custom logic when the keyboard is shown/hidden
		/// </summary>
		/// <param name='visible'>
		/// If the keyboard is visible
		/// </param>
		/// <param name='keyboardHeight'>
		/// Calculated height of the keyboard (width not generally needed here)
		/// </param>
		protected virtual void OnKeyboardChanged(bool visible, float keyboardHeight)
		{
			var activeView = ViewToCenterOnKeyboardShown ?? KeyboardGetActiveView();
			if (activeView == null)
				return;

			var scrollView = activeView.FindSuperviewOfType(View, typeof(UIScrollView)) as UIScrollView;
			if (scrollView == null)
				return;

			if (!visible)
				RestoreScrollPosition(scrollView);
			else
				CenterViewInScroll(activeView, scrollView, keyboardHeight);
		}

		protected virtual void CenterViewInScroll(UIView viewToCenter, UIScrollView scrollView, float keyboardHeight)
		{
			var contentInsets = new UIEdgeInsets(0.0f, 0.0f, keyboardHeight, 0.0f);
			scrollView.ContentInset = contentInsets;
			scrollView.ScrollIndicatorInsets = contentInsets;

			// Position of the active field relative isnside the scroll view
			var relativeFrame = viewToCenter.Superview.ConvertRectToView(viewToCenter.Frame, scrollView);

			bool landscape = InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || InterfaceOrientation == UIInterfaceOrientation.LandscapeRight;
			var spaceAboveKeyboard = (landscape ? scrollView.Frame.Width : scrollView.Frame.Height) - keyboardHeight;

			// Move the active field to the center of the available space
			var offset = relativeFrame.Y - (spaceAboveKeyboard - viewToCenter.Frame.Height) / 2;
			scrollView.ContentOffset = new PointF(0, (float)offset);
		}

		protected virtual void RestoreScrollPosition(UIScrollView scrollView)
		{
			scrollView.ContentInset = UIEdgeInsets.Zero;
			scrollView.ScrollIndicatorInsets = UIEdgeInsets.Zero;
		}

		/// <summary>
		/// Call it to force dismiss keyboard when background is tapped
		/// </summary>
		protected void DismissKeyboardOnBackgroundTap()
		{
			// Add gesture recognizer to hide keyboard
			var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
			tap.AddTarget(() => View.EndEditing(true));
			View.AddGestureRecognizer(tap);
		}

		protected bool TextFieldShouldReturn(UITextField textField)
		{
			switch (textField.Tag)
			{
				case 1:
					return txtFirstName.BecomeFirstResponder();
				case 2:
					return txtSurname.BecomeFirstResponder();
				case 3:
					return txtHouseNo.BecomeFirstResponder();
				case 4:
					return txtAddress1.BecomeFirstResponder();
				case 5:
					return txtAddress2.BecomeFirstResponder();
				case 6:
					return txtTown.BecomeFirstResponder();
				case 7:
					return txtCounty.BecomeFirstResponder();
				case 8:
					return txtPostCode.BecomeFirstResponder();
				case 9:
					return txtTelephone.BecomeFirstResponder();
				case 10:
					return txtMobile.BecomeFirstResponder();
				case 11:
					return txtEmail.BecomeFirstResponder();
				default:
					return textField.ResignFirstResponder();
			}
		}

		#endregion
	}
}


