// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	[Register ("AppointmentEditContactViewController")]
	partial class AppointmentEditContactViewController
	{
		[Outlet]
		UIKit.UILabel lblSiteAddress { get; set; }

		[Outlet]
		UIKit.UITextField txtAddress1 { get; set; }

		[Outlet]
		UIKit.UITextField txtAddress2 { get; set; }

		[Outlet]
		UIKit.UITextField txtCounty { get; set; }

		[Outlet]
		UIKit.UITextField txtEmail { get; set; }

		[Outlet]
		UIKit.UITextField txtFirstName { get; set; }

		[Outlet]
		UIKit.UITextField txtHouseNo { get; set; }

		[Outlet]
		UIKit.UITextField txtMobile { get; set; }

		[Outlet]
		UIKit.UITextField txtPostCode { get; set; }

		[Outlet]
		UIKit.UITextField txtSurname { get; set; }

		[Outlet]
		UIKit.UITextField txtTelephone { get; set; }

		[Outlet]
		UIKit.UITextField txtTitle { get; set; }

		[Outlet]
		UIKit.UITextField txtTown { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (txtAddress1 != null) {
				txtAddress1.Dispose ();
				txtAddress1 = null;
			}

			if (txtAddress2 != null) {
				txtAddress2.Dispose ();
				txtAddress2 = null;
			}

			if (txtCounty != null) {
				txtCounty.Dispose ();
				txtCounty = null;
			}

			if (txtEmail != null) {
				txtEmail.Dispose ();
				txtEmail = null;
			}

			if (txtFirstName != null) {
				txtFirstName.Dispose ();
				txtFirstName = null;
			}

			if (txtHouseNo != null) {
				txtHouseNo.Dispose ();
				txtHouseNo = null;
			}

			if (txtMobile != null) {
				txtMobile.Dispose ();
				txtMobile = null;
			}

			if (txtPostCode != null) {
				txtPostCode.Dispose ();
				txtPostCode = null;
			}

			if (txtSurname != null) {
				txtSurname.Dispose ();
				txtSurname = null;
			}

			if (txtTelephone != null) {
				txtTelephone.Dispose ();
				txtTelephone = null;
			}

			if (txtTitle != null) {
				txtTitle.Dispose ();
				txtTitle = null;
			}

			if (txtTown != null) {
				txtTown.Dispose ();
				txtTown = null;
			}

			if (lblSiteAddress != null) {
				lblSiteAddress.Dispose ();
				lblSiteAddress = null;
			}
		}
	}
}
