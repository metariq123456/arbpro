// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
//
//
using System;
using Foundation;
using UIKit;
using DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class AppointmentListCell : UITableViewCell
    {
        public static readonly UINib Nib = UINib.FromName("AppointmentListCell", NSBundle.MainBundle);
        public static readonly NSString Key = new NSString("AppointmentListCell");

        #region Constructors

        public AppointmentListCell() : base()
        {

        }

        public AppointmentListCell(IntPtr handle) : base(handle)
        {

        }

        public static AppointmentListCell Create()
        {
            return (AppointmentListCell)Nib.Instantiate(null, null) [0];
        }

        #endregion

        #region Properties

        public AppointmentStatus Status
        {
			
            set
            {
                switch (value)
                {
                case AppointmentStatus.Pending:
                    CurrentStatus.Text = "Pending";
                    Indicator.BackgroundColor = UIColor.Red;
                    break;
                case AppointmentStatus.Completed:
                    CurrentStatus.Text = "Completed";
                    Indicator.BackgroundColor = UIColor.Green;
                    break;
                case AppointmentStatus.PartiallyUploaded:
                    CurrentStatus.Text = "Partially Uploaded";
                    Indicator.BackgroundColor = UIColor.Cyan;
                    break;
                case AppointmentStatus.Uploaded:
                    CurrentStatus.Text = "Uploaded";
                    Indicator.BackgroundColor = UIColor.Black;
                    break;
                default:
                    CurrentStatus.Text = "Cancelled";
                    Indicator.BackgroundColor = UIColor.Blue;
                    break;
                }
            }
        }

        public string NameAndAddress
        {

            set
            { 
                NameAndLocation.Lines = 2;
                NameAndLocation.MinimumScaleFactor = 0.5f;
                NameAndLocation.AdjustsFontSizeToFitWidth = true;
                NameAndLocation.LineBreakMode = UILineBreakMode.Clip;
                NameAndLocation.Text = value; 
            }
        }

        public string QuoteNo
        {

            set{ QuoteId.Text = value; }
        }

        public DateTime AppointmentTime
        {

            set{ DateAndTime.Text = value.ToString("dd/MM/yy HH:mm"); }
        }

        #endregion

    }
}

