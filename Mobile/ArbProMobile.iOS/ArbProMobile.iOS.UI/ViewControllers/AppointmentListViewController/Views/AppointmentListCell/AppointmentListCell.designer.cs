// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("AppointmentListCell")]
    partial class AppointmentListCell
    {
        [Outlet]
        UIKit.UILabel CurrentStatus { get; set; }


        [Outlet]
        UIKit.UILabel DateAndTime { get; set; }


        [Outlet]
        UIKit.UIView Indicator { get; set; }


        [Outlet]
        UIKit.UILabel NameAndLocation { get; set; }


        [Outlet]
        UIKit.UILabel QuoteId { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CurrentStatus != null) {
                CurrentStatus.Dispose ();
                CurrentStatus = null;
            }

            if (DateAndTime != null) {
                DateAndTime.Dispose ();
                DateAndTime = null;
            }

            if (Indicator != null) {
                Indicator.Dispose ();
                Indicator = null;
            }

            if (NameAndLocation != null) {
                NameAndLocation.Dispose ();
                NameAndLocation = null;
            }

            if (QuoteId != null) {
                QuoteId.Dispose ();
                QuoteId = null;
            }
        }
    }
}