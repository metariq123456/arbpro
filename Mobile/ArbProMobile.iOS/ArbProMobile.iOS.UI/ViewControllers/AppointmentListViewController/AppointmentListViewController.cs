// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
//
//
using System;
using CoreGraphics;
using System.Threading.Tasks;
using System.Collections.Generic;
using Foundation;
using UIKit;
using Microsec.Tools.iOS.UI;
using DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class AppointmentListViewController : UIViewController
    {

        #region Members

        class AppointmentListSource :UITableViewSource
        {

            #region Members

            const string CONST_CELL_NAME = "AppointmentCell";
            TableCellFactory<AppointmentListCell> _CellFactory = new TableCellFactory<AppointmentListCell>(CONST_CELL_NAME, @"AppointmentListCell");
            AppointmentListViewController Controller;

            #endregion

            #region Properties

            public List<BL.Appointment> Appointments { get; set; }

            #endregion

            #region Constructors

            public AppointmentListSource(AppointmentListViewController controller)
            {
                Controller = controller;
            }

            #endregion

            #region Overrides

            public override nint NumberOfSections(UITableView tableView)
            {
                return 1;
            }

            public override string TitleForHeader(UITableView tableView, nint section)
            {
                if (Appointments == null || Appointments.Count -1 == 0)
                {
                    return "No Appointments Found";
                } else if (Appointments.Count -1 == 1)
                {
                    return string.Format("{0} Appointment Found", Appointments.Count -1);
                } else
                {
                    return string.Format("{0} Appointments Found", Appointments.Count -1);
                }
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                if (Appointments != null)
                {
                    return Appointments.Count;
                } else
                {
                    return 0;
                }
            }

            public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
            {
                return 60;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                UITableViewCell Cell;
                //Get a table cell from the factory
            
                Appointment a = Appointments [indexPath.Row];
                if (a.Id > 0)
                {
                    AppointmentListCell ApptCell;
                    ApptCell = (AppointmentListCell)_CellFactory.GetCell(tableView);
                    ApptCell.AppointmentTime = a.StartDateTime;
                    ApptCell.Status = a.Status;
                    ApptCell.QuoteNo = "Q" + a.QuoteNo.ToString();
                    ApptCell.NameAndAddress = a.ClientNameAndAddress;
                    if (a.Status == AppointmentStatus.PartiallyUploaded)
                    {
                        ApptCell.Accessory = UITableViewCellAccessory.None;
                    }

                    Cell = ApptCell;
                } 
				else
                {
                    if (Controller.AddButton == null)
                    {
                        Controller.AddButton = new UIButton(UIButtonType.ContactAdd);
                        Controller.AddButton.AddTarget(Controller.AddAppointment, UIControlEvent.TouchUpInside);
                    }

                    Cell = tableView.DequeueReusableCell("NewApptCell");
                    if (Cell == null)
                    {
                        Cell = new UITableViewCell(UITableViewCellStyle.Subtitle, "NewApptCell");
                        Cell.Accessory = UITableViewCellAccessory.None;
                    }
                    Cell.AccessoryView = Controller.AddButton;
                    Cell.TextLabel.Text = "Add Appointment";
                }

                return Cell;
            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                Appointment appointment = GetItem(indexPath.Row);
                // If this is not the dummy appointment at the bottom of the list
                if (appointment.Id > 0)
                {
                    //Only display the appointment if it is not pending completion or uploading
                    if (appointment.Status != AppointmentStatus.PartiallyUploaded)
                        Controller.DisplayAppointment(appointment.RecId);
                } else
                {
                    Controller.AddAppointment(null,null);
                }
            }

            #endregion

            #region Public Methods

            public BL.Appointment GetItem(int row)
            {
                return Appointments [row];
            }

            public async Task RefreshAsync()
            {
				Appointments = await Task.Run<List<BL.Appointment>>(() => BL.BusinessLayer.AppointmentListFiltered()).ConfigureAwait(false);

				//Add an extra Appointment with a negative Id, to trigger the 'Add' cell
				Appointment DummyAppt = new Appointment() { Id = -1 };
				Appointments.Insert(0, DummyAppt);
            }

            #endregion

        }
        //A reference to the buttons is kept to prevent GC from disposing
        public UIButton AddButton = null;
        UIBarButtonItem _NavButtonTools = null;
        UIBarButtonItem _NavButtonSettings = null;

        #endregion

        #region Constructors

        public AppointmentListViewController() : base("AppointmentListViewController", null)
        {
            //Setup navigation bar
            this.Title = @"About";
			_NavButtonTools = new UIBarButtonItem(@"Tools", UIBarButtonItemStyle.Bordered, BtnToolsClicked);
            _NavButtonSettings = new UIBarButtonItem(@"Settings", UIBarButtonItemStyle.Bordered, BtnSettingsClicked);
            this.NavigationItem.LeftBarButtonItem = _NavButtonTools;
            this.NavigationItem.RightBarButtonItem = _NavButtonSettings;
            SaveQuoteAlertDelegate.AppointmentSavedEvent += this.RefreshDataAsync;
        }

        #endregion

        #region Overrides

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
			
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            Title = "Appointments";
            DataTable.Source = new AppointmentListSource(this);
            
            if (BL.Globals.DemoData)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            } else
            {
                NavigationItem.Prompt = null;
            }
			
            RefreshData();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        #endregion

        #region Actions

		void BtnToolsClicked(Object sender, EventArgs e)
        {
			this.NavigationController.PushViewController(new ToolsViewController(), true);
        }

        void BtnSettingsClicked(Object sender, EventArgs e)
        {
            this.NavigationController.PushViewController(new SettingsViewController(), true);
        }

        public void DisplayAppointment(int AppointmentRecId)
        {
            AppointmentViewController controller = new AppointmentViewController();
            controller.AppointmentRecId = AppointmentRecId;
            this.Title = "Back";
            NavigationController.PushViewController(controller, true);
        }

        #region Async Methods

        async void RefreshDataAsync()
        {
            await ((AppointmentListSource)DataTable.Source).RefreshAsync().ConfigureAwait(true);
            DataTable.ReloadData();
            SetActivityStatus(false);
        }

        #endregion

        #endregion

        #region Private Methods

        void RefreshData()
        {
            SetActivityStatus(true);
            RefreshDataAsync();
        }

        void SetActivityStatus(bool working)
        {
            ProcessView.DisplayNetworkIndicator(!working);
            _NavButtonTools.Enabled = !working;
            _NavButtonSettings.Enabled = !working;
        }

        #endregion

        #region Public Methods

        public void AddAppointment(object Sender, EventArgs e)
        {
            NavigationItem.Title = "Back";
			this.NavigationController.PushViewController(new AppointmentNewViewController(), true);
        }

        #endregion // Public Methods

    }
}

