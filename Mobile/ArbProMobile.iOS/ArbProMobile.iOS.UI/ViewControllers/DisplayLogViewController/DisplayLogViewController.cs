﻿// *********************************************************************
// $Id$
// $Author$
// $Date$
// $Change$
// *********************************************************************
using System;
using System.Threading.Tasks;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class DisplayLogViewController : UIViewController
	{
		NetworkActivityView ProcessingView = null;

		public DisplayLogViewController() : base("DisplayLogViewController", null)
		{
		}

		#region Override

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.

			var view = NetworkActivityView.LoadFromNib(this);
			view.Alpha = (nfloat)0.7;

			var frame = this.View.Frame;
			view.Frame = frame;

			this.View.AddSubview(view);

			ProcessingView = view;
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		public override async void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			SetActivityStatus(true);
			this.TextView.Text = await ReadLogFileAsync();
			SetActivityStatus(false);
		}

		public override void ViewWillDisappear(bool animated)
		{
			ProcessingView.Dispose();
		}

		#endregion

		#region Private

		async Task<string> ReadLogFileAsync()
		{
			var log = TinyIoC.TinyIoCContainer.Current.Resolve<Microsec.Mobile.Core.Logging.ILogger>();

			return await Task.Run(() => log.ReadLogFile()).ConfigureAwait(true);
		}

		void SetActivityStatus(bool shouldShow)
		{
			this.TextView.Hidden = shouldShow;
			ProcessingView.DisplayNetworkIndicator(!shouldShow);
			this.View.BringSubviewToFront(this.ProcessingView);
		}

		#endregion
	}
}


