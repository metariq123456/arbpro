﻿// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/EditClientViewController/EditClientViewController.cs#3 $
// $Author: ivan $
// $Date: 2016/09/21 $
// $Change: 27019 $
// *********************************************************************
using System;
using System.Drawing;
using DGS.ArbPro.Mobile.BL;
using Foundation;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class EditClientViewController : UIViewController
    {
        UIBarButtonItem _BtnDone;
		nint selectedSegmentId;
		int clientId;
		int recId = 0;
		bool IsForAppointments;

        #region Constructors
        public EditClientViewController(int cId, bool isForAppointments) : base("EditClientViewController", null)
        {
			clientId = cId;
			IsForAppointments = isForAppointments;
            _BtnDone = new UIBarButtonItem ("Save", UIBarButtonItemStyle.Plain, this.BtnDoneClicked);
            this.NavigationItem.RightBarButtonItem = _BtnDone;
        }
        #endregion // Constructors

        #region Overrides
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
			DismissKeyboardOnBackgroundTap();
			RegisterForKeyboardNotifications();

            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            this.NavigationController.NavigationBar.Translucent = false;
			RegisterKeyboardDelegate();
			LoadClientInfo();

            if (BL.Globals.DemoData)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            } else
            {
                NavigationItem.Prompt = null;
            }
        }

		public override void ViewWillDisappear(bool animated)
		{
			base.ViewWillDisappear(animated);
			DeregisterKeyboardDelegate();
		}

        #endregion // Overrides

        #region Private Methods

        private void BtnDoneClicked(object Sender, EventArgs e)
        {
			if (BL.Globals.LoginStatus == Globals.LoggedInStatus.LoggedInOnline || BL.Globals.DemoData)
			{
				UIAlertView alert = new UIAlertView();
				alert.Title = @"Save Client";
				alert.AddButton("Yes");
				alert.AddButton("No");
				alert.Message = "Are you sure you want to save this client? This will update any existing information for that client.";
				alert.Show();

				alert.Clicked += (object senders, UIButtonEventArgs es) =>
				{
					if (es.ButtonIndex == 0)
					{
						UIAlertView alert2 = new UIAlertView();
						if (this.SaveClient())
						{
							if (!IsForAppointments)
							{
								alert2.Title = @"Client saved.";
								alert2.AddButton("Ok");
								alert2.Message = string.Empty;
								alert2.Show();
								NavigationController.PopViewController(true);
							}
						}
						else
						{
							alert2.Title = @"Client update error";
							alert2.AddButton("Ok");
							alert2.Message = "A minimum of Surname or Company and Address 1 is needed.";
							alert2.Show();
						}
					}
				};
			}
			else 
			{
				UIAlertView alert2 = new UIAlertView();
				alert2.Title = @"You are not connected to the internet or are not in demonstration mode.";
				alert2.AddButton("Ok");
				alert2.Message = string.Empty;
				alert2.Show();
			}
        }

		private bool SaveClient()
		{
			var retVal = false;
			BL.Client client = new BL.Client();

			client.Id = clientId;
			client.RecId = recId;
			client.AccountType = (int)selectedSegmentId;
			client.Address1 = txtAddress1.Text;
			client.Address2 = txtAddress2.Text;
			client.AltAccountNo = txtAccountNo.Text;
			client.County = txtCounty.Text;
			client.Town = txtTown.Text;
			client.DateOpened = DateTime.Now;
			client.HouseNameNumber = txtHouseNo.Text;
			client.PostCode = txtPostCode.Text;
			client.Mobile = txtMobile.Text;
			client.Telephone = txtTelephone.Text;
			client.Email = txtEmail.Text;
			client.SendSmsMessage = swSms.On;
			client.IncludeInMarketing = swMarketing.On;

			if (selectedSegmentId == 0)
			{
				client.Title = txtTitle.Text;
				client.Forename = txtFirstname.Text;
				client.Surname = txtSurname.Text;
				client.WebAddress = string.Empty;
				client.Sector = string.Empty;
				client.BusinessName = string.Empty;

			}
			else
			{
				client.WebAddress = txtWebsite.Text;
				client.Sector = txtFirstname.Text;
				client.BusinessName = txtSurname.Text;
				client.Title = string.Empty;
				client.Forename = string.Empty;
				client.Surname = string.Empty;
			}

			if (!string.IsNullOrWhiteSpace(txtSurname.Text) && !string.IsNullOrWhiteSpace(client.Address1))
			{
				if (IsForAppointments)
				{
					retVal = true;
					int count = NavigationController.ViewControllers.Length;
					var controller = this.NavigationController;
					this.NavigationController.PopToViewController(NavigationController.ViewControllers[count - 3], true);
					AppointmentNewViewController viewCon = (AppointmentNewViewController)controller.TopViewController;
					viewCon.GetClientForAppt(client);
				}
				else
				{
					retVal = BusinessLayer.ClientUpload(client);
				}
			}

			return retVal;
		}

		private void RegisterKeyboardDelegate()
		{
			txtTitle.Tag = 1;
			txtWebsite.Tag = 1;
			txtFirstname.Tag = 2;
			txtSurname.Tag = 3;
			txtAccountNo.Tag = 4;
			txtHouseNo.Tag = 5;
			txtAddress1.Tag = 6;
			txtAddress2.Tag = 7;
			txtTown.Tag = 8;
			txtCounty.Tag = 9;
			txtPostCode.Tag = 10;
			txtEmail.Tag = 11;
			txtTelephone.Tag = 12;
			txtMobile.Tag = 13;

			txtTitle.ShouldReturn += TextFieldShouldReturn;
			txtWebsite.ShouldReturn += TextFieldShouldReturn;
			txtFirstname.ShouldReturn += TextFieldShouldReturn;
			txtSurname.ShouldReturn += TextFieldShouldReturn;
			txtAccountNo.ShouldReturn += TextFieldShouldReturn;
			txtHouseNo.ShouldReturn += TextFieldShouldReturn;
			txtAddress1.ShouldReturn += TextFieldShouldReturn;
			txtAddress2.ShouldReturn += TextFieldShouldReturn;
			txtTown.ShouldReturn += TextFieldShouldReturn;
			txtCounty.ShouldReturn += TextFieldShouldReturn;
			txtPostCode.ShouldReturn += TextFieldShouldReturn;
			txtEmail.ShouldReturn += TextFieldShouldReturn;
			txtTelephone.ShouldReturn += TextFieldShouldReturn;
			txtMobile.ShouldReturn += TextFieldShouldReturn;
   		}

		private void DeregisterKeyboardDelegate()
		{
			txtTitle.ShouldReturn -= TextFieldShouldReturn;
			txtWebsite.ShouldReturn -= TextFieldShouldReturn;
			txtFirstname.ShouldReturn -= TextFieldShouldReturn;
			txtSurname.ShouldReturn -= TextFieldShouldReturn;
			txtAccountNo.ShouldReturn -= TextFieldShouldReturn;
			txtHouseNo.ShouldReturn -= TextFieldShouldReturn;
			txtAddress1.ShouldReturn -= TextFieldShouldReturn;
			txtAddress2.ShouldReturn -= TextFieldShouldReturn;
			txtTown.ShouldReturn -= TextFieldShouldReturn;
			txtCounty.ShouldReturn -= TextFieldShouldReturn;
			txtPostCode.ShouldReturn -= TextFieldShouldReturn;
			txtEmail.ShouldReturn -= TextFieldShouldReturn;
			txtTelephone.ShouldReturn -= TextFieldShouldReturn;
			txtMobile.ShouldReturn -= TextFieldShouldReturn;
		}

		private void LoadClientInfo()
		{
			BL.Client client = DAL.Repository.Instance().ClientGetForId(clientId);

			client = client ?? new BL.Client();
			recId = client.RecId;
			txtTitle.Text = client.Title;
			txtWebsite.Text = client.WebAddress;
			if (client.AccountType == 0)
			{
				accountTypeControl.SelectedSegment = 0;
				txtFirstname.Text = client.Forename;
				txtSurname.Text = client.Surname;
			}
			else {
				accountTypeControl.SelectedSegment = 1;
				txtFirstname.Text = client.Sector;
				txtSurname.Text = client.BusinessName;
			}
			txtAccountNo.Text = client.AltAccountNo;
			txtHouseNo.Text = client.HouseNameNumber;
			txtAddress1.Text = client.Address1;
			txtAddress2.Text = client.Address2;
			txtTown.Text = client.Town;
			txtCounty.Text = client.County;
			txtPostCode.Text = client.PostCode;
			txtEmail.Text = client.Email;
			txtTelephone.Text = client.Telephone;
			txtMobile.Text = client.Mobile;

			swSms.On = client.RecId > 0 ? client.SendSmsMessage : true;
			swMarketing.On = client.RecId > 0 ? client.IncludeInMarketing : true;

			ClientTypeSwitchChanged(accountTypeControl);
		}

		#endregion // Private 

		#region Partial Methods

		partial void ClientTypeSwitchChanged(UIKit.UISegmentedControl sender)
		{
			selectedSegmentId = sender.SelectedSegment;

			if (selectedSegmentId == 1)
			{
				this.txtTitle.Hidden = true;
				this.lblTitle.Text = "Website";

				this.txtWebsite.Hidden = false;
				this.txtWebsite.SetNeedsLayout();
				this.txtWebsite.SetNeedsDisplay();

				this.lblFirstName.Text = "Sector";
				this.lblSurname.Text = "Company Name";
			}
			else
			{
				this.txtTitle.Hidden = false;
				this.txtTitle.SetNeedsLayout();
				this.txtTitle.SetNeedsDisplay();

				this.txtWebsite.Hidden = true;

				this.lblTitle.Text = "Title";
				this.lblFirstName.Text = "First Name";
				this.lblSurname.Text = "Surname";
			}
		}

		#endregion

		#region Keyboard adjust

		/// <summary>
		/// Set this field to any view inside the scroll view to center this view instead of the current responder
		/// </summary>
		protected UIView ViewToCenterOnKeyboardShown;
		public virtual bool HandlesKeyboardNotifications()
		{
			return false;
		}

		protected virtual void RegisterForKeyboardNotifications()
		{
			NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillHideNotification, OnKeyboardNotification);
			NSNotificationCenter.DefaultCenter.AddObserver(UIKeyboard.WillShowNotification, OnKeyboardNotification);
		}

		/// <summary>
		/// Gets the UIView that represents the "active" user input control (e.g. textfield, or button under a text field)
		/// </summary>
		/// <returns>
		/// A <see cref="UIView"/>
		/// </returns>
		protected virtual UIView KeyboardGetActiveView()
		{
			return View.FindFirstResponder();
		}

		private void OnKeyboardNotification(NSNotification notification)
		{
			if (!IsViewLoaded) return;

			//Check if the keyboard is becoming visible
			var visible = notification.Name == UIKeyboard.WillShowNotification;

			//Start an animation, using values from the keyboard
			UIView.BeginAnimations("AnimateForKeyboard");
			UIView.SetAnimationBeginsFromCurrentState(true);
			UIView.SetAnimationDuration(UIKeyboard.AnimationDurationFromNotification(notification));
			UIView.SetAnimationCurve((UIViewAnimationCurve)UIKeyboard.AnimationCurveFromNotification(notification));

			//Pass the notification, calculating keyboard height, etc.
			bool landscape = InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || InterfaceOrientation == UIInterfaceOrientation.LandscapeRight;
			var keyboardFrame = visible
									? UIKeyboard.FrameEndFromNotification(notification)
									: UIKeyboard.FrameBeginFromNotification(notification);

			OnKeyboardChanged(visible, (float)(landscape ? keyboardFrame.Width : keyboardFrame.Height));

			//Commit the animation
			UIView.CommitAnimations();
		}

		/// <summary>
		/// Override this method to apply custom logic when the keyboard is shown/hidden
		/// </summary>
		/// <param name='visible'>
		/// If the keyboard is visible
		/// </param>
		/// <param name='keyboardHeight'>
		/// Calculated height of the keyboard (width not generally needed here)
		/// </param>
		protected virtual void OnKeyboardChanged(bool visible, float keyboardHeight)
		{
			var activeView = ViewToCenterOnKeyboardShown ?? KeyboardGetActiveView();
			if (activeView == null)
				return;

			var scrollView = activeView.FindSuperviewOfType(View, typeof(UIScrollView)) as UIScrollView;
			if (scrollView == null)
				return;

			if (!visible)
				RestoreScrollPosition(scrollView);
			else
				CenterViewInScroll(activeView, scrollView, keyboardHeight);
		}

		protected virtual void CenterViewInScroll(UIView viewToCenter, UIScrollView scrollView, float keyboardHeight)
		{
			var contentInsets = new UIEdgeInsets(0.0f, 0.0f, keyboardHeight, 0.0f);
			scrollView.ContentInset = contentInsets;
			scrollView.ScrollIndicatorInsets = contentInsets;

			// Position of the active field relative isnside the scroll view
			var relativeFrame = viewToCenter.Superview.ConvertRectToView(viewToCenter.Frame, scrollView);

			bool landscape = InterfaceOrientation == UIInterfaceOrientation.LandscapeLeft || InterfaceOrientation == UIInterfaceOrientation.LandscapeRight;
			var spaceAboveKeyboard = (landscape ? scrollView.Frame.Width : scrollView.Frame.Height) - keyboardHeight;

			// Move the active field to the center of the available space
			var offset = relativeFrame.Y - (spaceAboveKeyboard - viewToCenter.Frame.Height) / 2;
			scrollView.ContentOffset = new PointF(0, (float)offset);
		}

		protected virtual void RestoreScrollPosition(UIScrollView scrollView)
		{
			scrollView.ContentInset = UIEdgeInsets.Zero;
			scrollView.ScrollIndicatorInsets = UIEdgeInsets.Zero;
		}

		/// <summary>
		/// Call it to force dismiss keyboard when background is tapped
		/// </summary>
		protected void DismissKeyboardOnBackgroundTap()
		{
			// Add gesture recognizer to hide keyboard
			var tap = new UITapGestureRecognizer { CancelsTouchesInView = false };
			tap.AddTarget(() => View.EndEditing(true));
			View.AddGestureRecognizer(tap);
		}

		protected bool TextFieldShouldReturn(UITextField textField)
		{
			switch (textField.Tag)
			{
				case 1:
					return txtFirstname.BecomeFirstResponder();
				case 2:
					return txtSurname.BecomeFirstResponder();
				case 3:
					return txtAccountNo.BecomeFirstResponder();
				case 4:
					return txtHouseNo.BecomeFirstResponder();
				case 5:
					return txtAddress1.BecomeFirstResponder();
				case 6:
					return txtAddress2.BecomeFirstResponder();
				case 7:
					return txtTown.BecomeFirstResponder();
				case 8:
					return txtCounty.BecomeFirstResponder();
				case 9:
					return txtPostCode.BecomeFirstResponder();
				case 10:
					return txtEmail.BecomeFirstResponder();
				case 11:
					return txtTelephone.BecomeFirstResponder();
				case 12:
					return txtMobile.BecomeFirstResponder();
				case 13:
					return txtMobile.ResignFirstResponder();
				default:
					return textField.ResignFirstResponder();
			}
		}

		#endregion
	}
}


