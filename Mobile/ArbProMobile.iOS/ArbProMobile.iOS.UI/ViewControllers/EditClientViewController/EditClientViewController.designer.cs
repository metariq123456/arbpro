// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    [Register ("EditClientViewController")]
    partial class EditClientViewController
    {
        [Outlet]
        UIKit.UISegmentedControl accountTypeControl { get; set; }


        [Outlet]
        UIKit.UILabel lblFirstName { get; set; }


        [Outlet]
        UIKit.UILabel lblSurname { get; set; }


        [Outlet]
        UIKit.UILabel lblTitle { get; set; }


        [Outlet]
        UIKit.UISwitch swMarketing { get; set; }


        [Outlet]
        UIKit.UISwitch swSms { get; set; }


        [Outlet]
        UIKit.UITextField txtAccountNo { get; set; }


        [Outlet]
        UIKit.UITextField txtAddress1 { get; set; }


        [Outlet]
        UIKit.UITextField txtAddress2 { get; set; }


        [Outlet]
        UIKit.UITextField txtCounty { get; set; }


        [Outlet]
        UIKit.UITextField txtEmail { get; set; }


        [Outlet]
        UIKit.UITextField txtFirstname { get; set; }


        [Outlet]
        UIKit.UITextField txtHouseNo { get; set; }


        [Outlet]
        UIKit.UITextField txtMobile { get; set; }


        [Outlet]
        UIKit.UITextField txtPostCode { get; set; }


        [Outlet]
        UIKit.UITextField txtSurname { get; set; }


        [Outlet]
        UIKit.UITextField txtTelephone { get; set; }


        [Outlet]
        UIKit.UITextField txtTitle { get; set; }


        [Outlet]
        UIKit.UITextField txtTown { get; set; }


        [Outlet]
        UIKit.UITextField txtWebsite { get; set; }


        [Action ("ClientTypeSwitchChanged:")]
        partial void ClientTypeSwitchChanged (UIKit.UISegmentedControl sender);

        void ReleaseDesignerOutlets ()
        {
            if (accountTypeControl != null) {
                accountTypeControl.Dispose ();
                accountTypeControl = null;
            }

            if (lblFirstName != null) {
                lblFirstName.Dispose ();
                lblFirstName = null;
            }

            if (lblSurname != null) {
                lblSurname.Dispose ();
                lblSurname = null;
            }

            if (lblTitle != null) {
                lblTitle.Dispose ();
                lblTitle = null;
            }

            if (swMarketing != null) {
                swMarketing.Dispose ();
                swMarketing = null;
            }

            if (swSms != null) {
                swSms.Dispose ();
                swSms = null;
            }

            if (txtAccountNo != null) {
                txtAccountNo.Dispose ();
                txtAccountNo = null;
            }

            if (txtAddress1 != null) {
                txtAddress1.Dispose ();
                txtAddress1 = null;
            }

            if (txtAddress2 != null) {
                txtAddress2.Dispose ();
                txtAddress2 = null;
            }

            if (txtCounty != null) {
                txtCounty.Dispose ();
                txtCounty = null;
            }

            if (txtEmail != null) {
                txtEmail.Dispose ();
                txtEmail = null;
            }

            if (txtFirstname != null) {
                txtFirstname.Dispose ();
                txtFirstname = null;
            }

            if (txtHouseNo != null) {
                txtHouseNo.Dispose ();
                txtHouseNo = null;
            }

            if (txtMobile != null) {
                txtMobile.Dispose ();
                txtMobile = null;
            }

            if (txtPostCode != null) {
                txtPostCode.Dispose ();
                txtPostCode = null;
            }

            if (txtSurname != null) {
                txtSurname.Dispose ();
                txtSurname = null;
            }

            if (txtTelephone != null) {
                txtTelephone.Dispose ();
                txtTelephone = null;
            }

            if (txtTitle != null) {
                txtTitle.Dispose ();
                txtTitle = null;
            }

            if (txtTown != null) {
                txtTown.Dispose ();
                txtTown = null;
            }

            if (txtWebsite != null) {
                txtWebsite.Dispose ();
                txtWebsite = null;
            }
        }
    }
}