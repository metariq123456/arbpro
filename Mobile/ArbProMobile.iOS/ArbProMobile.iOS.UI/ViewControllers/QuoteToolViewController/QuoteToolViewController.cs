using UIKit;
using Foundation;
using DGS.ArbPro.Mobile.BL;
using System.Collections.Generic;
using System;

namespace DGS.ArbPro.Mobile.iOS.UI
{
	public partial class QuoteToolViewController : UIViewController
	{
		#region Properties
		public Quote Quote { get; set; }

		/// <summary>
		/// The index of the tool in the Quote's list of tools
		/// </summary>
		/// <value>The index of the tool.</value>
		public int ToolIndex { get; set; }
		#endregion
		#region Constructors
		public QuoteToolViewController () : base ("QuoteToolViewController", null)
		{
			this.Title = "Tool";
		}
		#endregion
		#region Overrides
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);

			if (BL.Globals.DemoData) {
				NavigationItem.Prompt = "Demonstration Mode";
			} else {
				NavigationItem.Prompt = null;
			}
			lstDetails.Source = new ToolTableSource (this);
		}
		#endregion
		#region Public Methods
		public void DisplayTextInput (int Index, string CurrentText, string InputLabel, UIKeyboardType KeyboardType, TextLookupSource lookupSource)
		{
			TextInputViewController controller = new TextInputViewController ();
			controller.ViewTitle = "Tool Details";
			controller.LookupSource = lookupSource;
			controller.Index = Index;
			controller.CurrentText = CurrentText;
			controller.InputLabel = InputLabel;
			controller.KeyboardType = KeyboardType;
			controller.DidInputTextEvent += TextInputSaved;

			NavigationController.PushViewController (controller, true);
		}

		public void DisplayTextPicker (int Index, string CurrentText, string InputLabel, UIKeyboardType KeyboardType, TextLookupSource lookupSource)
		{
			TextPickerViewController controller = new TextPickerViewController ();

			// Limit the length of the tool name
			controller.MaxLength = QuoteTool.Name_MaxLength;
			var Tools = DAL.Repository.Instance ().ToolList ();
			List<string> toolNames = new List<string> ();
			foreach (var Tool in Tools) {
				toolNames.Add (Tool.Name);
			}

			controller.LookupItems = toolNames;
			controller.DefaultValue = CurrentText;
			controller.Index = Index;
			controller.ViewTitle = "Tool Details";
			controller.InputLabel = InputLabel;
			controller.DidPickTextEvent += TextInputSaved;

			NavigationController.PushViewController (controller, true);
		}
		#endregion
		#region Private Methods
		void TextInputSaved (int Index, string SavedText)
		{
			int NewNumber;
			switch (Index) {
			case 0:
				Quote.Tools [ToolIndex].Name = SavedText;
				break;
			case 1:

				int.TryParse (SavedText, out NewNumber);
				Quote.Tools [ToolIndex].Number = NewNumber;
				break;
			default:
				break;
			}
			Quote.Dirty = true;

			lstDetails.ReloadData ();
		}
		#endregion
	}

	class ToolTableSource:UITableViewSource
	{
		#region Fields
		const int SECTION_MAIN = 0;
		const int ROW_MAIN_NAME = 0;
		const int ROW_MAIN_NUMBER = 1;
		const string CELL_ID = "ToolCell";
		QuoteToolViewController _Controller;
		#endregion
		#region Constructors
		public ToolTableSource () :base()
		{

		}

		public ToolTableSource (QuoteToolViewController controller)
		{
			_Controller = controller;
		}
		#endregion
		#region Overrides
		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override string TitleForHeader (UITableView tableView, nint section)
		{
			switch (section) {
			case SECTION_MAIN:
				return "";
			default:
				return string.Format ("bad section{0}", section);
			}
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			switch (section) {
			case SECTION_MAIN:
				return 2;
			default:
				return 0;
			}
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell Cell = null;

			switch (indexPath.Section) {
			case SECTION_MAIN:
				Cell = tableView.DequeueReusableCell (CELL_ID);
				if (Cell == null) {
					Cell = new UITableViewCell (UITableViewCellStyle.Value1, CELL_ID);
					Cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;
					Cell.SelectionStyle = UITableViewCellSelectionStyle.None;
				}

				switch (indexPath.Row) {
				case ROW_MAIN_NAME:
					Cell.TextLabel.Text = "Tool";
					Cell.DetailTextLabel.Text = _Controller.Quote.Tools [_Controller.ToolIndex].Name;
					break;
				case ROW_MAIN_NUMBER:
					Cell.TextLabel.Text = "Number";
					Cell.DetailTextLabel.Text = _Controller.Quote.Tools [_Controller.ToolIndex].Number.ToString ();
					break;
				default:
					break;
				}
				break;
			default:
				break;
			}
			return Cell;
		}
		#endregion
		#region Private Methods
		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			if (indexPath.Section == SECTION_MAIN) {
				switch (indexPath.Row) {
				case ROW_MAIN_NAME:
                    //_Controller.DisplayTextInput(ROW_MAIN_NAME, _Controller.Quote.Tools[_Controller.ToolIndex].Name, "Name", UIKeyboardType.Default,TextLookupSource.Tool);
					_Controller.DisplayTextPicker (ROW_MAIN_NAME, _Controller.Quote.Tools [_Controller.ToolIndex].Name, "Name", UIKeyboardType.Default, TextLookupSource.Tool);
					break;
				case ROW_MAIN_NUMBER:
					_Controller.DisplayTextInput (ROW_MAIN_NUMBER, _Controller.Quote.Tools [_Controller.ToolIndex].Number.ToString (), "Quantity", UIKeyboardType.NumberPad, TextLookupSource.None);
					break;
				default:
					break;
				}
			}

		}
		#endregion
	}
}

