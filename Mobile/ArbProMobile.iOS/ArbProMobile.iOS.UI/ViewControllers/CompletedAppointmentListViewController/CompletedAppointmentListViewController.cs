// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/ViewControllers/CompletedAppointmentListViewController/CompletedAppointmentListViewController.cs#1 $
// $Author: James $
// $Date: 2016/03/27 $
// $Change: 26039 $
// *********************************************************************
//
//
using System;
using CoreGraphics;
using System.Threading.Tasks;
using System.Collections.Generic;
using Foundation;
using UIKit;
using Microsec.Tools.iOS.UI;
using DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.iOS.UI
{
    public partial class CompletedAppointmentListViewController : UIViewController
    {

        #region Members

        class CompletedAppointmentListSource :UITableViewSource
        {

            #region Members

            const string CONST_CELL_NAME = "AppointmentCell";
            TableCellFactory<CompletedAppointmentListCell> _CellFactory = new TableCellFactory<CompletedAppointmentListCell>(CONST_CELL_NAME, @"AppointmentListCell");
            CompletedAppointmentListViewController Controller;

            #endregion

            #region Properties

            public List<BL.Appointment> Appointments { get; set; }

            #endregion

            #region Constructors

            public CompletedAppointmentListSource(CompletedAppointmentListViewController controller)
            {
                Controller = controller;
                //RefreshAsync();
                //Refresh();
            }

            #endregion

            #region Overrides

            public override nint NumberOfSections(UITableView tableView)
            {
                return 1;
            }

            public override string TitleForHeader(UITableView tableView, nint section)
            {
                if (Appointments == null || Appointments.Count == 0)
                {
                    return "No Appointments Found";
                } else if (Appointments.Count == 1)
                {
                    return string.Format("{0} Appointment Found", Appointments.Count);
                } else
                {
                    return string.Format("{0} Appointments Found", Appointments.Count);
                }
            }

            public override nint RowsInSection(UITableView tableview, nint section)
            {
                if (Appointments != null)
                {
                    return Appointments.Count;
                } else
                {
                    return 0;
                }
            }

            public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
            {
                return 60;
            }

            public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
            {
                //Get a table cell from the factory
                CompletedAppointmentListCell Cell = (CompletedAppointmentListCell)_CellFactory.GetCell(tableView);
            
                Appointment a = Appointments [indexPath.Row];
                
                Cell.AppointmentTime = a.StartDateTime;
                Cell.Status = a.Status;
                Cell.QuoteNo = "Q" + a.QuoteNo.ToString();
                Cell.NameAndAddress = a.ClientNameAndAddress;
                Cell.UploadTime = a.UploadTime;

                return Cell;
            }

            public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
            {
                Appointment appointment = GetItem(indexPath.Row);
                //Only display the appointment if it is not pending completion of uploading
                if (appointment.Status != AppointmentStatus.PartiallyUploaded)
                    Controller.DisplayAppointment(appointment.RecId);
            }

            #endregion

            #region Public Methods

            public BL.Appointment GetItem(int row)
            {
                return Appointments [row];
            }

            public void Refresh()
            {
                Appointments = BL.BusinessLayer.AppointmentListFiltered();
            }

            public async Task RefreshAsync()
            {
                Appointments = await Task.Run<List<BL.Appointment>>(() => BL.BusinessLayer.AppointmentListFiltered()).ConfigureAwait(false);
            }

            #endregion

        }
        //A refernce to the buttons is kept to prevent GC from disposing
        UIBarButtonItem _NavButtonFilter = null;
        UIBarButtonItem _NavButtonSettings = null;
        // TODO - remove this once we've determined we don't need it
        //		string _Title = "Appointments";

        #endregion

        #region Constructors

        public CompletedAppointmentListViewController() : base("AppointmentListViewController", null)
        {
            //Setup navigation bar
            this.Title = @"About";
            _NavButtonFilter = new UIBarButtonItem(@"Filter", UIBarButtonItemStyle.Bordered, BtnFilterClicked);
            _NavButtonSettings = new UIBarButtonItem(@"Settings", UIBarButtonItemStyle.Bordered, BtnSettingsClicked);
            this.NavigationItem.LeftBarButtonItem = _NavButtonFilter;
            this.NavigationItem.RightBarButtonItem = _NavButtonSettings;
            SaveQuoteAlertDelegate.AppointmentSavedEvent += this.RefreshDataAsync;
        }

        #endregion

        #region Overrides

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();
			
            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            Title = "Appointments";
            DataTable.Source = new CompletedAppointmentListSource(this);
            
            // GetDataSource();

            // Check whether we have rows in the table
//            if (TableView.NumberOfRowsInSection(0) == 0)
//            {
//                if (EmptyTableView == null)
//                {
//                    EmptyTableView = new UITextView(TableView.Frame);
//                    EmptyTableView.Text = "No appointments to display";
//                    EmptyTableView.TextAlignment = UITextAlignment.Center;
//                }
//                TableView.AddSubview(EmptyTableView);
//                TableView.BackgroundView = EmptyTableView;
//            }
//            else
//            {
//                foreach (UIView SubView in TableView.Subviews)
//                {
//                    if (SubView == EmptyTableView)
//                    {
//                        SubView.RemoveFromSuperview();
//                        TableView.BackgroundView = null;
//                        break;
//                    }
//                }
//           }


            if (BL.Globals.DemoData)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            } else
            {
                NavigationItem.Prompt = null;
            }
			
            RefreshData();
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        #endregion

        #region Actions

        void BtnFilterClicked(Object sender, EventArgs e)
        {
            this.NavigationController.PushViewController(new FilterViewController(), true);
        }

        void BtnSettingsClicked(Object sender, EventArgs e)
        {
            this.NavigationController.PushViewController(new SettingsViewController(), true);
        }

        public void DisplayAppointment(int AppointmentRecId)
        {
            AppointmentViewController controller = new AppointmentViewController();
            controller.AppointmentRecId = AppointmentRecId;
            this.Title = "Back";
            NavigationController.PushViewController(controller, true);
        }

        #region Async Methods

        async void RefreshDataAsync()
        {
            await ((CompletedAppointmentListSource)DataTable.Source).RefreshAsync().ConfigureAwait(true);
            DataTable.ReloadData();
            SetActivityStatus(false);
        }

        #endregion

        #endregion

        #region Private Methods

        void RefreshData()
        {
            SetActivityStatus(true);
            RefreshDataAsync();
        }

        void SetActivityStatus(bool working)
        {
            ProcessView.DisplayNetworkIndicator(!working);
            _NavButtonFilter.Enabled = !working;
            _NavButtonSettings.Enabled = !working;
        }

        #endregion

    }
}

