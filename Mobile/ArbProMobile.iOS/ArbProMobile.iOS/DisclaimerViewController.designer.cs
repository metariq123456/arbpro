// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace DGS.ArbPro.Mobile.iOS
{
	[Register ("DisclaimerViewController")]
	partial class DisclaimerViewController
	{
		[Outlet]
		MonoTouch.UIKit.UILabel lblDisclaimer { get; set; }

		[Action ("btnDemoModeClick:")]
		partial void btnDemoModeClick (MonoTouch.Foundation.NSObject sender);

		[Action ("btnSettingsClick:")]
		partial void btnSettingsClick (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (lblDisclaimer != null) {
				lblDisclaimer.Dispose ();
				lblDisclaimer = null;
			}
		}
	}
}
