// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.iOS/Objects/Globals.cs#7 $
// $Author: Jon $
// $Date: 2015/05/20 $
// $Change: 24664 $
// *********************************************************************
//
//
using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using Interfaces = DGS.ArbPro.Mobile.Interfaces;
using BL = DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.BL
{
    [Preserve(AllMembers = true)]
	static class Globals //: Interfaces.IGlobals
    {
//        #region Constructors
//        public Globals()
//        {
//            DemoMode = true;
//            LoggedIn = false;
//            
//        }
//        #endregion

        #region Properties
        /// <summary>
        /// The authentication keys for the last valid login
        /// </summary>
        /// <value>The authentication keys.</value>
        private static Dictionary<string, string> _AuthenticationKeys = null;

        public static Dictionary<string,string> AuthenticationKeys
        {
            get
            {
                if (_AuthenticationKeys == null)
                    _AuthenticationKeys = new Dictionary<string, string>();
                    
                return _AuthenticationKeys;
            }
            set
            {
                _AuthenticationKeys = value;
            }
        }
        /// <summary>
        /// Gets or sets the last application login date and time.
        /// </summary>
        /// <value>The last app login.</value>
        private static DateTime _LastAppLogin;

        public static DateTime LastAppLogin
        {
            get{ return _LastAppLogin;}
            set{ _LastAppLogin = value;}
        }

        public static bool LoggedIn { get; set; }

        public static bool ShowSettings { get; set; }
#endregion
    }
}

