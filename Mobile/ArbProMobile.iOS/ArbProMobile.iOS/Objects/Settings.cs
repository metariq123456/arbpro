using System;
using System.Collections.Generic;
using MonoTouch.Foundation;
using Interfaces = DGS.ArbPro.Mobile.Interfaces;
using Enums = DGS.ArbPro.Mobile.Core.Enums;
using BL = DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.BL
{
    [Preserve(AllMembers = true)]
	public static class Settings //: Interfaces.ISettings
    {
        private  const string KEY_USERNAME = @"Settings_Username";
        private  const string KEY_PASSWORDHASH = @"Settings_PasswordHash";
        private  const string KEY_SERVICE = @"Settings_URL";
        private const string KEY_DEMOMODE = @"Settings_DemoMode";
		//Filter settings keys
        private  const string KEY_FILTER_STATUS = @"Settings_FilterStatus";
        private  const string KEY_FILTER_DATE = @"Settings_FilterDate";
        private  const string KEY_FILTER_DATETYPE = @"Settings_FilterDateType";
//		#region Constructors
//		public static Settings ()
//		{
//			try
//			{
//				using(NSUserDefaults defaults = NSUserDefaults.StandardUserDefaults)
//				{
//					_ServiceURI = defaults.StringForKey (KEY_SERVICE);
//					_UserName = defaults.StringForKey (KEY_USERNAME);
//					_PasswordHash = defaults.StringForKey(KEY_PASSWORDHASH);
//					
//					//Filter settings
//					_StatusFilter = (Enums.StatusFilterEnum)defaults.IntForKey (KEY_FILTER_STATUS);
//					_DateFilterType = (Enums.DateFilterTypeEnum)defaults.IntForKey (KEY_FILTER_DATETYPE);
//					try
//					{
//						_DateFilter = DateTime.Parse (defaults.StringForKey(KEY_FILTER_DATE));
//					}
//					catch
//					{
//						_DateFilter = DateTime.Now;
//					}
//				}
//			}
//			catch(Exception ex)
//			{
//			
//			}
//		}
//		#endregion
		
		#region Methods
		/// <summary>
		/// Synchronises a string to the iOS Userdefaults.
		/// </summary>
		/// <param name="setting_value">Setting_value.</param>
		/// <param name="key">Key.</param>
        private static void SynchroniseSetting(string setting_value, string key)
        {
            try
            {
                NSUserDefaults.StandardUserDefaults.SetString(setting_value, key);
                NSUserDefaults.StandardUserDefaults.Synchronize();
            }
            catch (Exception)
            {
			// TODO remove this if we don't want to soak up the exception
            }
        }
		/// <summary>
		/// Synchronises an integer to the iOS Userdefaults.
		/// </summary>
		/// <param name="setting_value">Setting_value.</param>
		/// <param name="key">Key.</param>
        private static void SynchroniseSetting(int setting_value, string key)
        {
            try
            {
                NSUserDefaults.StandardUserDefaults.SetInt(setting_value, key);
                NSUserDefaults.StandardUserDefaults.Synchronize();
            }
            catch (Exception)
            {
				// TODO remove this if we don't want to soak up the exception
            }
        }

        /// <summary>
        /// Synchronises a bool to the iOS Userdefaults.
        /// </summary>
        /// <param name="setting_value">Setting_value.</param>
        /// <param name="key">Key.</param>
        private static void SynchroniseSetting(bool setting_value, string key)
        {
            try
            {
                NSUserDefaults.StandardUserDefaults.SetBool(setting_value, key);
                NSUserDefaults.StandardUserDefaults.Synchronize();
            }
            catch (Exception)
            {
				// TODO remove this if we don't want to soak up the exception
            }
        }
		#endregion
		
		#region User-editable settings
		/// <summary>
		/// The URI of the service
		/// </summary>
        private static string _ServiceURI;

        static bool _DemoMode = false;
        public static bool DemoMode {
            get {
                return _DemoMode;
            }
            set {
                _DemoMode = value;
                SynchroniseSetting(value, KEY_DEMOMODE);         
            }
        }
        public static string ServiceURI
        { 
            get
            { 
                #if DEBUG
//					return @"http://10.0.0.49//DGS.ArbPro.Service.Interface.Dev/json/reply/";
                #endif
                return _ServiceURI;
            } 
            set
            {
                if (!value.EndsWith(@"/json/syncReply/"))
                {
                    _ServiceURI = value + @"/json/syncReply/";
                    _ServiceURI.Replace(@"//", @"/");
                }
                else
                {
                    _ServiceURI = value;
                }	
                SynchroniseSetting(value, KEY_SERVICE);				
            } 
        }

		/// <summary>
		/// The filter that is to be applied to the displayed appoinments list.
		/// </summary>
        private static Enums.StatusFilterEnum _StatusFilter;

        public static Enums.StatusFilterEnum StatusFilter
        { 
            get
            { 
                return _StatusFilter;
            } 
            set
            {
                _StatusFilter = value;
                SynchroniseSetting((int)value, KEY_FILTER_STATUS);				
            } 
        }
		/// <summary>
		/// Representation of how the date filter is to be applied to displayed appoinments list.
		/// </summary>
        private static Enums.DateFilterTypeEnum _DateFilterType;

        public static Enums.DateFilterTypeEnum DateFilterType
        { 
            get
            { 
                return _DateFilterType;
            } 
            set
            {
                _DateFilterType = value;
                SynchroniseSetting((int)value, KEY_FILTER_DATETYPE);				
            } 
        }
		/// <summary>
		/// The date filter that is to be applied to the list of appointments.
		/// </summary>
        private static DateTime _DateFilter;

        public static DateTime DateFilter
        { 
            get
            { 
                return _DateFilter;
            } 
            set
            {
                _DateFilter = value;
                SynchroniseSetting(value.ToString (@"dd/MM/yyyy hh:mm"), KEY_FILTER_DATE);				
            } 
        }
		#endregion
		
		#region Silently persisted settings
		/// <summary>
		/// The username used for the last login. This should be presented on the Login screen.
		/// </summary>
        private static string _UserName;

        public static string Username
        {
            get
            {
				string username;
				username = _UserName;
                
				#if DEBUG
                username = @"mobile_test@microsec.co.uk";
                #endif

                return username;
            }
            set
            {
                _UserName = value;
                SynchroniseSetting(value, KEY_USERNAME);
            } 
        }
		/// <summary>
		/// The hash value for the last logged in user. This should not be presented on the any screen, but 
		/// should be persisted to enable offline user validation.
		/// </summary>
        private static string _PasswordHash;

        public static string PasswordHash
        {
            get
            {
				string password;
				password = _PasswordHash;
                
				#if DEBUG
                password = @"password1";
                #endif

                return password;
            }
            set
            {
                _PasswordHash = value;
                SynchroniseSetting(value, KEY_USERNAME);
            } 
        }
		#endregion
    }
}

