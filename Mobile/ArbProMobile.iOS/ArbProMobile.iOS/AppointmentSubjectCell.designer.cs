// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace DGS.ArbPro.Mobile.iOS
{
	[Register ("AppointmentSubjectCell")]
	partial class AppointmentSubjectCell
	{
		[Outlet]
		MonoTouch.UIKit.UITextView txtSubject { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (txtSubject != null) {
				txtSubject.Dispose ();
				txtSubject = null;
			}
		}
	}
}
