// This file has been autogenerated from a class added in the UI designer.
//
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using DGS.ArbPro.Mobile.BL;

namespace DGS.ArbPro.Mobile.iOS
{
    public partial class SettingsViewController2 : UITableViewController
    {
        public SettingsViewController2(IntPtr handle) : base (handle)
        {
			
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            if (!BL.Globals.LoggedIn)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            }
            else
            {
                NavigationItem.Prompt = null;
            }

            if (Globals.LoggedIn)
                cellLoginOut.TextLabel.Text = "Log out";
            else
                cellLoginOut.TextLabel.Text = "Log in";

 
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell Cell;

            Cell = GetCell(tableView, indexPath);
            if (Cell == cellLoginOut)
            {
                if (Globals.LoggedIn)
                {
                    BusinessLayer.LocalLogout();
                    Cell.TextLabel.Text = "Log in";
                }
                else
                {
                    PerformSegue("Login", this);
                }
            }
            else if(Cell == cellSyncAppoinments)
            {
				PerformSegue ("Synchronise", this);
            }
            
            else if(Cell == cellSyncReferenceData)
            {
				BusinessLayer.ReferenceDataDownload();
            }
        }
    }
}
