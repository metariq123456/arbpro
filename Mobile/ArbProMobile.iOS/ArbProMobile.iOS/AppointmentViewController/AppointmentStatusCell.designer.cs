// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace DGS.ArbPro.Mobile.iOS
{
	[Register ("AppointmentStatusCell")]
	partial class AppointmentStatusCell
	{
		[Outlet]
		MonoTouch.UIKit.UISegmentedControl segStatus { get; set; }

		[Action ("StatusChanged:")]
		partial void StatusChanged (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (segStatus != null) {
				segStatus.Dispose ();
				segStatus = null;
			}
		}
	}
}
