// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace DGS.ArbPro.Mobile.iOS
{
	[Register ("AppointmentViewController")]
	partial class AppointmentViewController
	{
		[Outlet]
		MonoTouch.UIKit.UITableView tvAppointment { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISegmentedControl segChoice { get; set; }

		[Outlet]
		MonoTouch.UIKit.UISegmentedControl segStatus { get; set; }

		[Action ("StatusChanged:")]
		partial void StatusChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("segChoiceChanged:")]
		partial void segChoiceChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("Done:")]
		partial void Done (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (tvAppointment != null) {
				tvAppointment.Dispose ();
				tvAppointment = null;
			}

			if (segChoice != null) {
				segChoice.Dispose ();
				segChoice = null;
			}

			if (segStatus != null) {
				segStatus.Dispose ();
				segStatus = null;
			}
		}
	}
}
