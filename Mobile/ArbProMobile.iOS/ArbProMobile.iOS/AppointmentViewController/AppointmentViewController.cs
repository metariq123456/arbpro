// This file has been autogenerated from a class added in the UI designer.
//
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using DGS.ArbPro.Mobile.BL;
using DGS.ArbPro.Mobile.DAL;

namespace DGS.ArbPro.Mobile.iOS
{
    public partial class AppointmentViewController : UIViewController
    {
        #region Properties
        Appointment _Appointment;

        public Appointment Appointment
        {
            get { return _Appointment; }

        }

        Quote _Quote;

        public Quote Quote
        {
            get
            { return _Quote;}
        }

        public UISegmentedControl segFilter
        {
            get { return segChoice;}
        }
        #endregion

        #region Private fields
        Action _updateDataCallBack;
        #endregion

        #region Constructors
        public AppointmentViewController(IntPtr handle) : base (handle)
        {

        }
        #endregion

        #region Overridden Methods
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            Title = "Appointment";

            if (!BL.Globals.LoggedIn)
            {
                NavigationItem.Prompt = "Demonstration Mode";
            }
            else
            {
                NavigationItem.Prompt = null;
            }


            tvAppointment.Source = new AppointmentSource(this);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
            Title = "Back";
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);
        }
        #endregion

        #region Actions

        /// <summary>
        /// Handler vor when the choice segment has its selection changed
        /// </summary>
        /// <param name="sender">Sender.</param>
        partial void segChoiceChanged(NSObject sender)
        {
            switch (segChoice.SelectedSegment)
            {
                case 0:
                    Title = "Overview";
                    NavigationItem.RightBarButtonItem.Style = UIBarButtonItemStyle.Done;
                    NavigationItem.RightBarButtonItem.Title = "Done";
                    break;
                case 1:
                    Title = "Appointment";
                    break;
                case 2:
                    Title = "Items";
                    NavigationItem.RightBarButtonItem.Style = UIBarButtonItemStyle.Plain;
                    NavigationItem.RightBarButtonItem.Title = "+";
                    break;
                case 3:
                    Title = "Logistics";
                    break;
                case 4:
                    Title = "Tools";
                    break;
                default:
                    Title = "Appointment";
                    break;
            }

            tvAppointment.ReloadData();
        }

        partial void Done(NSObject sender)
        {
            if (NavigationItem.RightBarButtonItem.Title == "Done")
            {
           
                DAL.Repository.Instance().AppointmentSave(Appointment);
                DAL.Repository.Instance().QuoteSave(Quote);

                _updateDataCallBack();
                NavigationController.PopViewControllerAnimated(true);
            }
        }
        #endregion

        #region Public Methods
        public void Setup(int appointmentRecId, Action updateDataCallBack)
        {
            _Appointment = DAL.Repository.Instance().AppointmentGet(appointmentRecId);
            _Quote = DAL.Repository.Instance().QuoteGet(Appointment.QuoteNo);
            _updateDataCallBack = updateDataCallBack;
        }
        #endregion
        #region Callbacks
        public void UpdateStatus(AppointmentStatus newStatus)
        {
            Appointment.Status = newStatus;
        }
        #endregion

    }

    class AppointmentSource :UITableViewSource
    {

        #region Private fields

        // Names for cell prototypes
        static NSString AppointmentPlain = new NSString("AppointmentPlain");
        static NSString AppointmentAddress = new NSString("AppointmentAddress");
        static NSString AppointmentTelNo = new NSString("AppointmentTelNo");
        static NSString AppointmentStatus = new NSString("AppointmentStatus");
        static NSString AppointmentSubject = new NSString("AppointmentSubject");
        static NSString QuoteItem = new NSString("QuoteItem");
        static NSString QuoteItemTotal = new NSString("QuoteItemTotal");
        AppointmentViewController _Controller;
        // Constants for section numbers
        const int SECTION_APPOINTMENT = 0;
        const int SECTION_ITEMS = 1;
        const int SECTION_LOGISTICS = 2;
        const int SECTION_TOOLS = 3;
        #endregion

        #region Constructors
        public AppointmentSource(AppointmentViewController controller)
        {
            _Controller = controller;

        }
        #endregion

        #region UITableSource Methods
//        public BL.Appointment GetItem(int row)
//        {
//            return Appointments[row];
//        }

        public override int NumberOfSections(UITableView tableView)
        {
            return 4;
        }

        public override int RowsInSection(UITableView tableview, int section)
        {
            int ReturnValue;
            switch (section)
            {
                case SECTION_APPOINTMENT: 
                    if (_Controller.segFilter.SelectedSegment == 0 || _Controller.segFilter.SelectedSegment == SECTION_APPOINTMENT + 1)
                        ReturnValue = 5;
                    else
                        ReturnValue = 0;
                    break;
                case SECTION_ITEMS:
                    if (_Controller.segFilter.SelectedSegment == 0 || _Controller.segFilter.SelectedSegment == SECTION_ITEMS + 1)
                        ReturnValue = _Controller.Quote.Items.Count;
                    else
                        ReturnValue = 0;
                    break;
                case SECTION_LOGISTICS:
                    if (_Controller.segFilter.SelectedSegment == 0 || _Controller.segFilter.SelectedSegment == SECTION_LOGISTICS + 1)
                        ReturnValue = 1;
                    else
                        ReturnValue = 0;
                    break;
                case SECTION_TOOLS:
                    if (_Controller.segFilter.SelectedSegment == 0 || _Controller.segFilter.SelectedSegment == SECTION_TOOLS + 1)
                        ReturnValue = 1;
                    else
                        ReturnValue = 0;
                    break;
                default:
                    return 1;
            }

            return ReturnValue;
        }

        public override string TitleForHeader(UITableView tableView, int section)
        {
            if (_Controller.segFilter.SelectedSegment == 0)
            {
                switch (section)
                {
                    case SECTION_APPOINTMENT:
                        return "Appointment";
                        break;
                    case SECTION_ITEMS:
                        return string.Format("Items ({0})", _Controller.Quote.Items.Count);
                        break;
                    case SECTION_LOGISTICS:
                        return "Logistics";
                        break;
                    case SECTION_TOOLS:
                        return "Tools";
                        break;
                    default:
                        return string.Format("Bad section {0}", section);
                        break;
                }
            }
            else
            {
                return "";
            }
        }

        public override float GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            if (indexPath.Section == SECTION_APPOINTMENT && indexPath.Row == 3)
            {
                return 70f;
            }
            else
            {
                return 40f;
            }
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            UITableViewCell Cell;
            int Row = indexPath.Row;

            switch (indexPath.Section)
            {
                case SECTION_APPOINTMENT: 
                    switch (indexPath.Row)
                    {
                        case 0:
                            Cell = tableView.DequeueReusableCell(AppointmentPlain);
                            Cell.TextLabel.Text = "Time";
                            Cell.DetailTextLabel.Text = _Controller.Appointment.StartDateTime.ToString("dd/MM/yy HH:mm");
                            break;
                        case 1:
                            Cell = tableView.DequeueReusableCell(AppointmentAddress);
                            Cell.TextLabel.Text = _Controller.Appointment.ClientNameAndAddress;
                            break;
                        case 2:
                            Cell = tableView.DequeueReusableCell(AppointmentTelNo);
                            Cell.TextLabel.Text = "Telephone";
                            Cell.DetailTextLabel.Text = _Controller.Appointment.ClientTelephone;
                            break;
                        case 3:
                            Cell = tableView.DequeueReusableCell(AppointmentSubject);
                            if (Cell == null)
                            {
                                Cell = new AppointmentSubjectCell();
                            }
                            (Cell as AppointmentSubjectCell).Subject = _Controller.Appointment.Subject;
                            break;
                        case 4:
                            Cell = tableView.DequeueReusableCell(AppointmentStatus);
                            if (Cell == null)
                            {
                                Cell = new AppointmentStatusCell();
                            }
                            (Cell as AppointmentStatusCell).SetUpdateCallback(_Controller.UpdateStatus);
                            (Cell as AppointmentStatusCell).Status = _Controller.Appointment.Status;
                            break;
                        default:
                            Cell = tableView.DequeueReusableCell(AppointmentPlain);
                            Cell.TextLabel.Text = string.Format("Bad row {0}", indexPath.Row);
                            break;
                    }
                    break;

                case SECTION_ITEMS:
                    Cell = tableView.DequeueReusableCell(QuoteItem);
                    Cell.TextLabel.Text = _Controller.Quote.Items[indexPath.Row].ToString();
                    Cell.DetailTextLabel.Text = _Controller.Quote.Items[indexPath.Row].ItemValue.ToString("£0.00");
                    break;
                case SECTION_LOGISTICS:
                    Cell = tableView.DequeueReusableCell(AppointmentPlain);
                    Cell.TextLabel.Text = "Logistics";
                    break;
                case SECTION_TOOLS:
                    Cell = tableView.DequeueReusableCell(AppointmentPlain);
                    Cell.TextLabel.Text = "Tools";
                    break;
                default:
                    Cell = tableView.DequeueReusableCell(AppointmentPlain);
                    Cell.TextLabel.Text = string.Format("Bad section {0}", indexPath.Section);
                    break;
            }
            return Cell;
        }

        public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
        {
            bool ReturnValue;

            if (_Controller.segFilter.SelectedSegment == 0)
            {
                ReturnValue = false;

            }
            else
            {
                switch (indexPath.Section)
                {
                    case SECTION_APPOINTMENT:
                        ReturnValue = false;
                        break;
                    case SECTION_ITEMS:
                        ReturnValue = true;
                        break;
                    case SECTION_LOGISTICS:
                        ReturnValue = false;
                        break;
                    case SECTION_TOOLS:
                        ReturnValue = true;
                        break;
                    default:
                        ReturnValue = false;
                        break;
                }
            }

            return ReturnValue;
        }

        public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
        {
            switch (editingStyle)
            {
                case UITableViewCellEditingStyle.Delete:
                    var ItemToDelete = _Controller.Quote.Items[indexPath.Row];
                    _Controller.Quote.DeleteItem(ItemToDelete);

                    tableView.DeleteRows(new NSIndexPath[]{indexPath}, UITableViewRowAnimation.Fade);
                    break;
            }
        }
        #endregion
    }
}
