// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace DGS.ArbPro.Mobile.iOS
{
	[Register ("SettingsViewController2")]
	partial class SettingsViewController2
	{
		[Outlet]
		MonoTouch.UIKit.UITableViewCell cellLoginOut { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableViewCell cellSyncAppoinments { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITableViewCell cellSyncReferenceData { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (cellLoginOut != null) {
				cellLoginOut.Dispose ();
				cellLoginOut = null;
			}

			if (cellSyncAppoinments != null) {
				cellSyncAppoinments.Dispose ();
				cellSyncAppoinments = null;
			}

			if (cellSyncReferenceData != null) {
				cellSyncReferenceData.Dispose ();
				cellSyncReferenceData = null;
			}
		}
	}
}
