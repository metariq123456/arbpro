// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace DGS.ArbPro.Mobile.iOS
{
	[Register ("SettingsViewController")]
	partial class SettingsViewController
	{
		[Outlet]
		MonoTouch.UIKit.UISwitch chkDemoMode { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField txtServerURI { get; set; }

		[Action ("chkDemoModeChanged:")]
		partial void chkDemoModeChanged (MonoTouch.Foundation.NSObject sender);

		[Action ("btnDoneClick:")]
		partial void btnDoneClick (MonoTouch.Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (chkDemoMode != null) {
				chkDemoMode.Dispose ();
				chkDemoMode = null;
			}

			if (txtServerURI != null) {
				txtServerURI.Dispose ();
				txtServerURI = null;
			}
		}
	}
}
