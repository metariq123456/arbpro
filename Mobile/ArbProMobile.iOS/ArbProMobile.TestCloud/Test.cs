﻿// *********************************************************************
// $Id: //depot/DGS/ArbPro/v3/dev/Mobile/ArbProMobile.iOS/ArbProMobile.TestCloud/Test.cs#3 $
// $Author: Jon $
// $Date: 2015/05/20 $
// $Change: 24664 $
// *********************************************************************
using Xamarin.UITest;
using Xamarin.UITest.iOS;

using NUnit.Framework;
using System;
using Xamarin.UITest.Queries;
using System.Linq;

namespace ArbProMobile.TestCloud
{
	enum DemoOrLiveData
	{
		Demo,
		Live
	}

	[TestFixture ()]
	public class Test
	{
		iOSApp app;

		//		static readonly Func<AppQuery, AppQuery> firstCustomerView = c => c.Text ("Mr Jones, Dunroamin Beeding Lane, Bitterne, Wilts, PO19 1GG");

		[SetUp]
		public void Setup ()
		{
			app = ConfigureApp.iOS.AppBundle ("/Users/james/P4/DGS/ArbPro/v3/dev_JGL1/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/bin/iPhoneSimulator/Debug/ArbProMobileiOSUI.app").StartApp ();

			// Configure the test to run on PI-179
//			ConfigureApp.iOS.ApiKey ("2156127bb19c0d8fed86c11bda7cf8fd");
//			app = ConfigureApp.iOS.DeviceIdentifier ("8717ca5d7c189a374e96f1d0fd20a6e68a9d450c").InstalledApp ("com.DGS.ArbPro.Mobile.iPhone").StartApp ();

			//app = ConfigureApp.iOS.InstalledApp ("/Users/james/P4/DGS/ArbPro/v3/dev_JGL1/Mobile/ArbProMobile.iOS/ArbProMobile.iOS.UI/bin/iPhone/Debug/ArbProMobileiOSUI.app").StartApp ();

		}

		[Test ()]
		public void TestScreenNavigation ()
		{
			InitialiseDemoOrLive (DemoOrLiveData.Demo);
			
			// We should get the Appointments screen displayed - 5 minute timeout to allow for slower devices
			app.WaitForElement (x => x.Class ("UILabel").Text ("Appointments"), timeout: TimeSpan.FromSeconds (300));
			app.Screenshot ("Appointments Screen shows");

			// With non-random test data, we should have a specific customer as the 1st customer in the appointment list
			app.WaitForElement (x => x.Class ("UILabel").Text ("Mr Jones, Dunroamin Beeding Lane, Bitterne, Wilts, PO19 1GG"));

			TapFirstAppointment ();

			// Wait for the Appointment screen
			app.WaitForElement (x => x.Text ("Appointment"));
			app.Screenshot ("Appointment screen shows");

			// Make sure all tabs show
			// Select Items
			app.Tap (c => c.Class ("UISegmentLabel").Text ("Items"));
			AppResult[] Result = app.Query (c => c.Class ("UINavigationBar").Id ("Items").Child ().Index (1));
			Assert.IsTrue (Result.Any (), "Items tab not displayed");

			app.Screenshot ("Items tab shows");

			app.Tap (c => c.Class ("UISegmentLabel").Text ("Logistics"));
			Result = app.Query (c => c.Class ("UINavigationBar").Id ("Logistics").Child ().Index (1));
			Assert.IsTrue (Result.Any (), "Logistics tab not displayed");
			app.Screenshot ("Logistics tab shows");


			app.Tap (c => c.Class ("UISegmentLabel").Text ("Tools"));
			Result = app.Query (c => c.Class ("UINavigationBar").Id ("Tools").Child ().Index (1));
			Assert.IsTrue (Result.Any (), "Tools tab not displayed");
			app.Screenshot ("Tools tab shows");

			app.Tap (c => c.Class ("UISegmentLabel").Text ("Risk"));
			Result = app.Query (c => c.Class ("UINavigationBar").Id ("Risk Assessment").Child ().Index (1));
			Assert.IsTrue (Result.Any (), "Risk Assessment tab not displayed");
			app.Screenshot ("Risk Assessment tab shows");

			app.Back ();
			//app.Repl ();
		}

		[Test ()]
		public void TestAppointmentSave ()
		{
			InitialiseDemoOrLive (DemoOrLiveData.Demo);

			TapFirstAppointment ();

			// Make sure we don't have the Save button
			AppResult[] Result = app.Query (c => c.Class ("UINavigationButton").Child ().Index (0).Text ("Save"));
			Assert.IsFalse (Result.Any (), "Save button is already visible");

			app.Tap (c => c.Class ("UISegmentLabel").Text ("Completed"));

			Result = app.Query (c => c.Class ("UINavigationButton").Child ().Index (0).Text ("Save"));
			Assert.IsTrue (Result.Any (), "Save button not shown");

			app.Tap (c => c.Class ("UINavigationButton").Child ().Index (0).Text ("Save"));
			Result = app.Query (c => c.Class ("UINavigationButton").Child ().Index (0).Text ("Save"));
			Assert.IsFalse (Result.Any (), "Save button not hidden on successful save");

		}

		private void InitialiseDemoOrLive (DemoOrLiveData setting)
		{
			if (setting == DemoOrLiveData.Demo) {
				app.WaitForElement (x => x.Text ("Demonstration Mode"));
				app.Screenshot ("Initial screen");

				app.Tap (x => x.Text ("Demonstration Mode"));
			}
		}

		private void TapFirstAppointment ()
		{
			// The top item is the 8th in the list - they seem to be ordered from bottom to top
			// Tap the top item to get to the Appoinmtent view
			app.Tap (c => c.Class ("UITableViewWrapperView").Child ().Index (7).Child ().Index (0));
		}
	}
}

