﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsec.Elastic  
{
    public class ActivityLogEntry
    {
        #region Properties

        private string _Sender;
        public string Sender
        {
            get
            {
                return _Sender;
            }
        }

        private string _Recipient;
        public string Recipient
        {
            get
            {
                return _Recipient;
            }
        }

        private DateTime _SentDate;
        public DateTime SentDate
        {
            get
            {
                return _SentDate;
            }
        }

        private string _Status;
        public string Status
        {
            get
            {
                return _Status;
            }
        }

        private string _Message;
        public string Message
        {
            get
            {
                return _Message;
            }
        }

        private string _MsgId;
        public string MsgId
        {
            get
            {
                return _MsgId;
            }
        }

        private string _TransactionId = "";
        /// <summary>
        /// The transaction Id is the Id of the individual job (a single Email)
        /// This is the value that is returned by elastic when the message is sent via the API
        /// </summary>
        public string TransactionId
        {
            get { return _TransactionId; }
            set { _TransactionId = value; }
        }
#endregion

        public static List<ActivityLogEntry> List(string baseURI, string userName, string apiKey,string channel, DateTime fromTime, DateTime toTime)
        {
            Client c = new Client(baseURI, userName, apiKey);
            var logs = c.ActivityLog(channel, fromTime, toTime);
            return logs;
        }

        public ActivityLogEntry(string sender, string recipient, DateTime sentDate, string status, string message, string msgId)
        {
            DateTime UTCTime;
            _Sender = sender;
            _Recipient = recipient;
            _MsgId = msgId;

            // All dates from ElasticEmail are UTC, even though they aren't denoted as such. Therefore we
            // need to create a UTC time and then convert to local
            UTCTime = new DateTime(sentDate.Year, sentDate.Month, sentDate.Day, sentDate.Hour, sentDate.Minute, sentDate.Second, DateTimeKind.Utc);
            _SentDate = UTCTime.ToLocalTime();

            _Status = status;
            _Message = message;
        }

        /// <summary>
        /// Optional constructure to enable the transation and message Id properties to be set
        /// </summary>
        /// <param name="sender">The email address that sent the email</param>
        /// <param name="recipient">The email address of the recipient</param>
        /// <param name="sentDate">The date the email was sent</param>
        /// <param name="status">The current status of the email</param>
        /// <param name="message">The message associated with the activity (This requires a static Ip to be configured)</param>
        /// <param name="msgId">The email header reference, when the elastic server generates the email</param>
        /// <param name="transId">The transaction Id for the email. This is the value that is returned by the send process</param>
        public ActivityLogEntry(string sender, string recipient, DateTime sentDate, string status, string message, string msgId, string transId)
        {
            DateTime UTCTime;
            _Sender = sender;
            _Recipient = recipient;
            _MsgId = msgId;
            _TransactionId = transId;

            // All dates from ElasticEmail are UTC, even though they aren't denoted as such. Therefore we
            // need to create a UTC time and then convert to local
            UTCTime = new DateTime(sentDate.Year, sentDate.Month, sentDate.Day, sentDate.Hour, sentDate.Minute, sentDate.Second, DateTimeKind.Utc);
            _SentDate = UTCTime.ToLocalTime();

            _Status = status;
            _Message = message;
        }

    }
}
