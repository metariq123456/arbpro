﻿namespace Microsec.Elastic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Mail;
    using System.Net.Mime;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Xml;

    using global::Elastic;

    /// <summary>
    /// The method used to send the email
    /// </summary>
    public enum EmailMethod
    {
        /// <summary>
        /// Send via Elastic
        /// </summary>
        Elastic,

        /// <summary>
        /// Send via the SMTP server
        /// </summary>
        Smtp,
    }

    public class Client
    {
        string _Username;
        string _ApiKey;
        string _URI;

        //static ILog Log = LogManager.GetLogger("DGS.ArbPro.Elastic");

        /// <summary>
        /// Gets or sets the Method property - determines the method used to send emails
        /// </summary>
        public EmailMethod Method { get; set; }

        public string SmtpServer { get; set; }

        public bool EnableSsl { get; set; }

        public string SmtpUsername { get; set; }

        public int SmtpPort { get; set; }

        public string SmtpPassword { get; set; }

        private WebClient WebClient { get; set; }

        public Client(string baseURI, string userName, string apiKey, bool UseSmtp = false, bool EnableSsl = true, string smtpServer = null, string smtpPort = "25", string smtpUsername = null, string smtpPassword = null)
        {
            this._URI = baseURI;
            this._Username = userName;
            this._ApiKey = apiKey;
            if (UseSmtp)
            {
                this.Method = EmailMethod.Smtp;
            }
            else
            {
                this.Method = EmailMethod.Elastic;
            }
            this.EnableSsl = EnableSsl;
            this.SmtpServer = smtpServer;
            int i;
            if (!int.TryParse(smtpPort, out i)) i = 25;
            this.SmtpPort = i;
            this.SmtpUsername = smtpUsername;
            this.SmtpPassword = smtpPassword;

            WebClient = new WebClient();
        }

        /// <summary>
        /// Sends sms message to an already converted number to the E.164 standard.
        /// </summary>
        /// <param name="num">
        /// Customer's number.
        /// </param>
        /// <param name="body">
        /// The text message sent.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string SendSms(string num, string body, string countryCode = "44")
        {
            // example for send https://api.elasticemail.com/v2/sms/send?apikey=your-apikey&to=%2b100000000&body=text_body_of_your_message
            // example for callback https://api.elasticemail.com/v2/sms/callback?apikey=your-apikey&messagesid=12345678,1234567890
            byte[] Response;
            string ReturnValue;

            NameValueCollection values = new NameValueCollection();
            try
            {

                //values.Add("username", _Username);
                values.Add("apikey", _ApiKey);
                values.Add("to", this.ConvertNumberToE164(num,countryCode));
                values.Add("body", body);

                string n = string.Format("{0}{1}", _URI, "v2/sms/send");
                Response = WebClient.UploadValues(n, values);

                ReturnValue = Encoding.UTF8.GetString(Response);
            }
            catch (Exception ex)
            {
                throw ex;
            }



            return ReturnValue;
        }

        /// <summary>
        /// Checks if number given by SendSms() is in the correct E.164 format and if not converts it.
        /// </summary>
        /// <remarks>
        /// This converts to the E164 Standard 
        /// for full details see https://en.wikipedia.org/wiki/E.164
        /// </remarks>
        /// <param name="inputNumber">
        /// Customer number from SendSms().
        /// </param>
        /// <returns>
        /// The converted number <see cref="string"/>.
        /// </returns>
        private string ConvertNumberToE164(string inputNumber, string countryCode)
        {

            // remove all non-numeric characters
            String mobileNumber = CleanNumber(inputNumber);
            countryCode = this.CleanCountryCode(countryCode);

            // trim any leading zeros
            mobileNumber = mobileNumber.TrimStart(new char[] { '0' });
            countryCode = countryCode.TrimStart(new char[] { '0' });

            // check for this in case they've entered 44 (0)xxxxxxxxx or similar
            if (mobileNumber.StartsWith(countryCode + "0"))
            {
                mobileNumber = mobileNumber.Remove(2, 1);
            }

            // add country code if they haven't entered it
            if (!mobileNumber.StartsWith(countryCode))
            {
                mobileNumber = "+" + countryCode + mobileNumber;
            }

            else
            {
                mobileNumber = "+" + mobileNumber;
            }

            // check if it's the right length
            if (mobileNumber.Length <= 15 && mobileNumber.Length >= 13)
            {
                return mobileNumber;
            }

            throw new InvalidNumberException();
        }

        private string CleanNumber(string phone)
        {
            Regex digitsOnly = new Regex(@"[^\d]");
            return digitsOnly.Replace(phone, "");
        }

        private string CleanCountryCode(string code)
        {
            Regex digitsOnly = new Regex(@"[^\d]");
            return digitsOnly.Replace(code, "");
        }

        public string SendEmail(
            string to,
            string subject,
            string bodyText,
            string bodyHtml,
            string from,
            string fromName,
            string replyTo,
            string replyToName,
            List<AttachmentInfo> attachments)
        {
            if (this.Method == EmailMethod.Elastic)
            {
                return this.SendEmailViaElastic(
                    to.Replace(",", ";"),
                    subject,
                    bodyText,
                    bodyHtml,
                    from,
                    fromName,
                    replyTo,
                    replyToName,
                    attachments);
            }

            return this.SendEmailViaSmtp(
                to,
                subject,
                bodyText,
                bodyHtml,
                from,
                fromName,
                replyTo,
                replyToName,
                attachments);
        }

        public string EmailStatus(string messageId)
        {
            NameValueCollection Values = new NameValueCollection();
            byte[] response = WebClient.UploadValues(String.Format("{0}/{1}/{2}?showfailed=true&showerrors=true", _URI, "status", messageId), Values);
            return Encoding.UTF8.GetString(response);
        }

        private string UploadAttachment(string filepath, string filename)
        {
            FileStream Stream;
            WebRequest Request;
            Stream OutStream;
            WebResponse Response = null;
            string Result;

            //Log.DebugFormat("UploadAttachment: filepath={0} filename={1}", filepath, filename);
            try

            {
                Stream = File.OpenRead(filepath);
                Request = WebRequest.Create(String.Format("{0}attachments/upload?username={1}&api_key={2}&file={3}", _URI, _Username, _ApiKey, filename));
                Request.Method = "PUT";
                Request.ContentLength = Stream.Length;
                OutStream = Request.GetRequestStream();
                Stream.CopyTo(OutStream, 4096);
                Stream.Close();
                Response = Request.GetResponse();
                if (Response == null)
                {
                    throw new Exception(string.Format("Error uploading attachment Request={0}", Request.RequestUri));
                }
                Result = new StreamReader(Response.GetResponseStream(), Encoding.UTF8).ReadToEnd();
            }
            catch (Exception ex)
            {
                //Log.ErrorFormat("UploadAttachment: Error {0}", ex.Message);
                throw new Exception(String.Format("Error uploading attachment. {0}", ex.Message));
            }
            finally
            {
                if (Response != null)
                {
                    Response.Close();
                }
            }
            return Result;
        }


        public List<ActivityLogEntry> ActivityLog(string channel, DateTime fromTime, DateTime toTime)
        {
            NameValueCollection Values = new NameValueCollection();
            DateTime FromTime;
            DateTime ToTime;
            XmlDocument DocResults;
            List<ActivityLogEntry> ReturnValue = new List<ActivityLogEntry>();
            DateTime SentDate;
            CultureInfo USCultureInfo;
            WebResponse Response;
            string Result = "";


            //Log.DebugFormat("ActivityLog: channel={0} from {1:MM/dd/yyyy hh:mm:ss tt} to {2:MM/dd/yyyy hh:mm:ss tt}", channel, fromTime, toTime);

            string RequestString;
            //FromTime = new DateTime(fromTime.Year, 1, fromTime.Day, fromTime.Hour, fromTime.Minute, fromTime.Second, DateTimeKind.Local);
            FromTime = new DateTime(fromTime.Year, fromTime.Month, fromTime.Day, fromTime.Hour, fromTime.Minute, fromTime.Second, DateTimeKind.Local);
            FromTime = StartOfDay(FromTime);
            FromTime = FromTime.ToUniversalTime();

            ToTime = new DateTime(toTime.Year, toTime.Month, toTime.Day, toTime.Hour, toTime.Minute, toTime.Second, DateTimeKind.Local);
            ToTime = EndOfDay(ToTime);
            ToTime = ToTime.ToUniversalTime();

            //NOTE: When using the Microsec API key, the chanel needs to be HTTP API otherwise no history will be returned
            RequestString = String.Format("{0}{1}?username={2}&api_key={3}&from={4:MM/dd/yyyy hh:mm:ss tt}&to={5:MM/dd/yyyy hh:mm:ss tt}&channel={6}&format=xml", _URI, "mailer/status/log", _Username, _ApiKey, FromTime, ToTime,channel);

            WebRequest Request = WebRequest.Create(RequestString);
            Request.Method = "GET";

            try
            {
                Response = Request.GetResponse();
                Result = new StreamReader(Response.GetResponseStream(), Encoding.UTF8).ReadToEnd();
                Response.Close();
                if (Result != "")
                {
                    // We need a US culture so we can parse the dates correctly
                    USCultureInfo = new CultureInfo("en-US");
                    DocResults = new XmlDocument();
                    DocResults.LoadXml(Result);
                    foreach (XmlNode Node in DocResults.FirstChild.ChildNodes)
                    {
                        SentDate = DateTime.Parse(Node.Attributes["date"].Value, USCultureInfo.DateTimeFormat);
                        ReturnValue.Add(new ActivityLogEntry(channel, Node.Attributes["to"].Value, SentDate, Node.Attributes["status"].Value, Node.Attributes["message"].Value, Node.Attributes["msgid"].Value, Node.Attributes["transactionid"].Value));
                    }
                }

                //Log.DebugFormat("ActivityLog: {0} results returned", ReturnValue.Count);
            }
            catch (Exception ex)
            {
                //Log.ErrorFormat("ActivityLog: Error getting email activity log:{0}", ex.ToString());
                throw new Exception(string.Format("Error getting email activity log:{0}", ex.ToString()));
            }
            return ReturnValue;
        }

        #region Private Methods

        private string SendEmailViaElastic(string to, string subject, string bodyText, string bodyHtml, string from, string fromName, string replyTo, string replyToName, List<AttachmentInfo> attachments)
        {
            List<string> AttachmentIds = new List<string>();
            string AttachmentString;
            byte[] Response;
            string ReturnValue;

            //Log.DebugFormat("SendEmail: channel={0} to={1}, subject = {2}, attachments={3}", from, to, subject, attachments == null ? 0 : attachments.Count);

            NameValueCollection values = new NameValueCollection();
            try
            {
                if (attachments != null)
                {
                    foreach (AttachmentInfo a in attachments)
                    {
                        AttachmentIds.Add(UploadAttachment(a.SourceFilename, a.AttachedFilename));
                    }
                }
                string pattern = @"ArbPro\d{1,10}@microsec.co.uk;?";
                string replacement = "";
                Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                to = rgx.Replace(to, replacement);
                values.Add("username", _Username);
                values.Add("api_key", _ApiKey);
                values.Add("from", from);
                values.Add("from_name", fromName);
                values.Add("to", to);
                values.Add("channel", from);

                //Changes to make the reply to default to the sender if not set
                if (string.IsNullOrEmpty(replyTo))
                {
                    replyTo = from;
                    replyToName = fromName;
                }

                if (replyTo != null)
                    values.Add("reply_to", replyTo);

                if (replyTo != null)
                    values.Add("reply_to_name", replyToName);

                values.Add("subject", subject);

                if (bodyText != null)
                    values.Add("body_text", bodyText);

                if (bodyHtml != null)
                    values.Add("body_html", bodyHtml);

                if (attachments != null)
                {
                    AttachmentString = string.Join(";", AttachmentIds.ToArray());
                    values.Add("attachments", AttachmentString);
                }

                Response = WebClient.UploadValues(string.Format("{0}/{1}", _URI, "mailer/send"), values);
                ReturnValue = Encoding.UTF8.GetString(Response);
            }
            catch (Exception ex)
            {
                //Log.ErrorFormat("SendEmail: Error {0}", ex.Message);
                throw new Exception(string.Format("SendEmail: Error {0}", ex.Message));
            }

            return ReturnValue;
        }

        /// <summary>
        /// Sends an email via stmp server
        /// </summary>
        /// <param name="to">
        /// The to.
        /// </param>
        /// <param name="subject">
        /// The subject.
        /// </param>
        /// <param name="bodyText">
        /// The body text.
        /// </param>
        /// <param name="bodyHtml">
        /// The body html.
        /// </param>
        /// <param name="from">
        /// The from.
        /// </param>
        /// <param name="fromName">
        /// The from name.
        /// </param>
        /// <param name="replyTo">
        /// The reply to.
        /// </param>
        /// <param name="replyToName">
        /// The reply to name.
        /// </param>
        /// <param name="attachments">
        /// The attachments.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// </exception>
        private string SendEmailViaSmtp(string to, string subject, string bodyText, string bodyHtml, string from, string fromName, string replyTo, string replyToName, List<AttachmentInfo> attachments)
        {
            var message = new MailMessage();
            try
            {
                string[] emails = to.Split(',');
                string pattern = @"^ArbPro\d{1,10}@microsec.co.uk$";
                Regex myRegex = new Regex(pattern, RegexOptions.IgnoreCase);
                foreach(string single_address in emails)
                {
                    

                    Match m = myRegex.Match(single_address);   // m is the first match
                    if (!m.Success)
                    {
                        message.To.Add(single_address);
                    }
                    
                    
                }
                message.From = new MailAddress(this.SmtpUsername);

                message.Subject = subject;

                // Add body in either HTML or plan text format
                if (bodyHtml != null)
                {
                    message.Body = bodyHtml;
                    message.IsBodyHtml = true;
                }
                else if (bodyText != null)
                {
                    message.Body = bodyText;
                }
                else
                {
                    throw new Exception("Neither plain text body nor html body specified in email.");
                }

                // Set the reply to if necessary
                if (!string.IsNullOrEmpty(replyTo))
                {
                    message.ReplyToList.Add(new MailAddress(replyTo));
                }

                if (attachments != null)
                {
                    foreach (var attach in attachments)
                    {
                        var ms = new FileStream(attach.SourceFilename, FileMode.Open);
                        var data = new Attachment(ms, attach.AttachedFilename, MediaTypeNames.Application.Octet);  //this.AttachmentStream(ms, attach.AttachedFilename);
                        var disposition = data.ContentDisposition;
                        disposition.CreationDate = File.GetCreationTime(attach.SourceFilename);
                        disposition.ModificationDate = File.GetLastWriteTime(attach.SourceFilename);
                        disposition.ReadDate = File.GetLastAccessTime(attach.SourceFilename);

                        message.Attachments.Add(data);
                    }
                }

                message.CC.Add(this.SmtpUsername);
                var client = new SmtpClient(this.SmtpServer, this.SmtpPort)
                    //{ Credentials = CredentialCache.DefaultNetworkCredentials };
                                 { Credentials = new NetworkCredential(this.SmtpUsername, this.SmtpPassword) };
                client.EnableSsl = this.EnableSsl;
                client.Send(message);

            }
            catch (SmtpFailedRecipientException exception)
            {
                throw new SmtpFailedRecipientException(string.Format("SendEmail: Error {0}", exception));
            }
            catch (Exception ex)
            {
                // Log.ErrorFormat("SendEmail: Error {0}", ex.Message);
                throw new Exception(string.Format("SendEmail: Error {0}", ex));
            }
            finally
            {
                // message must be disposed of or else the stream for attachments 
                // will not be closed thus preventing file attachments from being deleted.
                message.Dispose();
            }

            return "success";
        }

        private DateTime StartOfDay(DateTime date)
        {
            DateTime ReturnValue;
            ReturnValue = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            return ReturnValue;
        }

        private DateTime EndOfDay(DateTime date)
        {
            DateTime ReturnValue;
            ReturnValue = new DateTime(date.Year, date.Month, date.Day, 0, 0, 0);
            ReturnValue = ReturnValue.AddDays(1);
            ReturnValue = ReturnValue.AddSeconds(-1);
            return ReturnValue;
        }
        #endregion
    }
}
