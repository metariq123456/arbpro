﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Elastic
{
    public class SendEmailException: Exception
    {
        public SendEmailException()
            : base()
        {
        }
    }
}
