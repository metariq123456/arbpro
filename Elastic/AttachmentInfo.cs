﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Microsec.Elastic
{
    public class AttachmentInfo
    {
        public string SourceFilename
        {
            get;
            set;
        }

        public string AttachedFilename
        {
            get;
            set;
        }

        public AttachmentInfo(string sourceFilename, string attachedFilename)
        {
            SourceFilename = sourceFilename;
            AttachedFilename = attachedFilename;
        }
    }
}
