﻿using ArbProWCF.Model;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace ArbProWCF
{

    [ServiceContract]
    public interface IArbProService
    {

        [OperationContract]
        [FaultContract(typeof(ServiceUnauthenticatedException))]
        List<JobWork> GetTodayWork(
            string token,
            int userId);

        [OperationContract]
        [FaultContract(typeof(ServiceUnauthenticatedException))]
        List<CalendarItem> GetCalendarItemsForUser(
            string token,
            string userId,
            DateTime fromDateTime,
            DateTime toDateTime);

        [OperationContract]
        [FaultContract(typeof(ServiceUnauthenticatedException))]
        bool CreateSalesEnquiry(
            DGS.ArbPro.Service.Model.Types.Client client,
            int mobileUserId,
            string token,
            string source,
            string comments,
            DateTime apptStart,
            DateTime apptEnd,
            string streetNo,
            string address1,
            string address2,
            string town,
            string county,
            string postCode,
            int templateId);

        [OperationContract]
        [FaultContract(typeof(ServiceUnauthenticatedException))]
        bool SaveClientContact(
            string token,
            string title,
            string firstName,
            string surname,
            int contactId,
            string streetNo,
            string address1,
            string address2,
            string town,
            string county,
            string postCode,
            string tel,
            string mobile,
            string email,
            int siteId,
            string siteStreetNo,
            string siteAddress1,
            string siteAddress2,
            string siteTown,
            string siteCounty,
            string sitePostCode);

        [OperationContract]
        [FaultContract(typeof(ServiceUnauthenticatedException))]
        [FaultContract(typeof(FaultException))]
        List<Model.RiskTemplate> GetRiskTemplates(string token);

        [OperationContract]
        [FaultContract(typeof(ServiceUnauthenticatedException))]
        bool BookWork(string objBookingDetail, string token);

        [OperationContract]
        [FaultContract(typeof(ServiceUnauthenticatedException))]
        bool AcceptQuote(string objQuote, string token);


    }
}
