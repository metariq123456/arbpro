﻿using System;
using System.Collections.Generic;

namespace ArbProWCF
{
    using System.Configuration;
    using System.Data.SqlTypes;
    using System.Linq;
    using System.ServiceModel;
    using System.Web;

    using ArbProWCF.Model;

    using DGS.ArbPro.Types;

    using log4net;

    using CalendarItem = ArbProWCF.Model.CalendarItem;
    using System.Web.Script.Serialization;
    using Newtonsoft.Json.Converters;
    using Newtonsoft.Json;

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class ArbProService : IArbProService
    {
        #region properties

        private static ILog log = LogManager.GetLogger("ArbProWCFService");

        string dbName = ConfigurationManager.AppSettings["MasterDatabase"];

        private string clientDbName = string.Empty;

        #endregion

        #region public methods
        
        /// <summary>
        /// GetTodayWork for Staff's Today Jobs 
        /// </summary>
        /// <param name="token">Unique user token</param>
        /// <param name="userId">Mobile user id</param>
        public List<Model.JobWork> GetTodayWork(
            string token,
            int userId)
        {
            DGS.ArbPro.Types.MobileUser usr = this.GetAndUpdateUserForToken(token);

            this.clientDbName = usr.ClientDatabaseName;

            List<Model.JobWork> retVal = new List<Model.JobWork>();

            DGS.ArbPro.DAL.JobWork dtbJobItem = new DGS.ArbPro.DAL.JobWork(this.clientDbName);
            retVal = JobItemFromDataTable(dtbJobItem.GetTodayWork(userId));
            
            return retVal;
        }


        /// <summary>
        /// Gets calendar appointments of type quote, work and generic for user
        /// </summary>
        /// <param name="token">Unique user token</param>
        /// <param name="userId">Mobile user id</param>
        /// <param name="fromDateTime">Start date to get appointments from</param>
        /// <param name="toDateTime">End date to get appointments until</param>
        /// <returns>List of appointments as calendar items</returns>
        public List<Model.CalendarItem> GetCalendarItemsForUser(
            string token,
            string userId,
            DateTime fromDateTime,
            DateTime toDateTime)
        {
            DGS.ArbPro.Types.MobileUser usr = this.GetAndUpdateUserForToken(token);

            this.clientDbName = usr.ClientDatabaseName;

            List<Model.CalendarItem> retVal = new List<Model.CalendarItem>();

            DGS.ArbPro.DAL.CalendarItem dtbCalendarItem = new DGS.ArbPro.DAL.CalendarItem(this.clientDbName);

            retVal.AddRange(
                this.CalendarItemFromDataTable(
                    dtbCalendarItem.GetGenericAppointmentsForUser(userId, fromDateTime, toDateTime),
                    CalendarItemType.Generic));

            retVal.AddRange(
                this.CalendarItemFromDataTable(
                    dtbCalendarItem.GetQuoteAppointmentsForUser(userId, fromDateTime, toDateTime),
                    CalendarItemType.Quote));

            retVal.AddRange(
                this.CalendarItemFromDataTable(
                    dtbCalendarItem.GetWorkAppointments(fromDateTime, toDateTime),
                    CalendarItemType.Work));

            return retVal;
        }

        /// <summary>
        /// Sets up an enquiry, job and appointment ready for download. Used for mobile app quote creation.
        /// </summary>
        /// <param name="client"> The client. </param>
        /// <param name="mobileUserId"> The mobile user id. </param>
        /// <param name="token"> The user email address. </param>
        /// <param name="source"> The source. </param>
        /// <param name="comments"> The comments. </param>
        /// <param name="apptStart"> The appointment start. </param>
        /// <param name="apptEnd"> The appointment end. </param>
        /// <param name="streetNo"> The street no. </param>
        /// <param name="address1"> The address 1. </param>
        /// <param name="address2"> The address 2. </param>
        /// <param name="town"> The town. </param>
        /// <param name="county"> The county. </param>
        /// <param name="postCode"> The post code. </param>
        /// <param name="templateId"> The risk template id. </param>
        /// <exception cref="Exception"> Exceptions at various stages of the job and appointment creation related to unsuccessful operations. </exception>
        public bool CreateSalesEnquiry(
            DGS.ArbPro.Service.Model.Types.Client client,
            int mobileUserId,
            string token,
            string source,
            string comments,
            DateTime apptStart,
            DateTime apptEnd,
            string streetNo,
            string address1,
            string address2,
            string town,
            string county,
            string postCode,
            int templateId)
        {
            DGS.ArbPro.Types.MobileUser usr = this.GetAndUpdateUserForToken(token);

            this.clientDbName = usr.ClientDatabaseName;

            var dtbClient = new DGS.ArbPro.DAL.Client(this.clientDbName);

            if (client.Id < 1)
            {
                DGS.ArbPro.Types.Client webClient = this.ConvertToWebClient(client);

                client.Id = dtbClient.Save(webClient);
            }

            DGS.ArbPro.Types.Client enqClient = dtbClient.GetForClientId(client.Id);

            var dtbStaff = new DGS.ArbPro.DAL.Staff(this.clientDbName);
            DGS.ArbPro.Types.Staff staff = dtbStaff.GetForMobileId(mobileUserId);

            var dtbjob = new DGS.ArbPro.DAL.Job(this.clientDbName);
            DGS.ArbPro.Types.Job newJob = new DGS.ArbPro.Types.Job()
            {
                ClientId = enqClient.Id,
                ContactId = enqClient.Contacts.Find(ct => ct.ContactType == DGS.ArbPro.Types.Enums.ContactTypeEnum.Default).Id,
                Source = source,
                Comments = comments,
                ReporterID = mobileUserId,
                Reporter = usr.NameStr,
                SurveyorId = staff.Id
            };

            newJob.Id = dtbjob.Save(newJob);

            if (newJob.Id == -1)
            {
                log.ErrorFormat("Could not save Job. Client Id: {0}, Reporter/Mobile User Id: {1}, Surveyor Id: {2}.", enqClient.Id, mobileUserId, staff.Id);
                return false;
            }

            DGS.ArbPro.DAL.ClientSite dtbSite = new DGS.ArbPro.DAL.ClientSite(this.clientDbName);
            DGS.ArbPro.Types.ClientSite clientSite = null;
            int siteId = dtbSite.CheckIfExistsReturnId(enqClient.Id, streetNo, address1, address2, town, county, postCode);
            if (siteId == -1)
            {
                clientSite = new DGS.ArbPro.Types.ClientSite()
                {
                    HouseNameNo = streetNo,
                    Address1 = address1,
                    Address2 = address2,
                    Town = town,
                    County = county,
                    PostCode = postCode,
                    ClientId = enqClient.Id,
                    ContactId = newJob.ContactId,
                    Reference = string.Empty
                };

                clientSite.Id = dtbSite.Save(clientSite);
            }
            else
            {
                clientSite = dtbSite.GetForId(siteId);
            }

            DGS.ArbPro.DAL.JobSite dtnJobSite = new DGS.ArbPro.DAL.JobSite(this.clientDbName);
            DGS.ArbPro.Types.JobSite newJSite = new DGS.ArbPro.Types.JobSite()
            {
                ClientId = enqClient.Id,
                JobId = newJob.Id,
                Site = clientSite,
                ContactId = newJob.ContactId
            };

            newJSite.Id = dtnJobSite.Save(newJSite);
            siteId = newJSite.Id;

            if (siteId == -1)
            {
                log.ErrorFormat("Could not save Job Site. Client Id: {0}, Job Id: {1}, Contact Id: {2}.", enqClient.Id, newJob.Id, newJob.ContactId);
                return false;
            }

            if (apptStart != DateTime.MinValue && apptEnd >= apptStart)
            {
                var dtbCalendarQuote = new DGS.ArbPro.DAL.Calendar(this.clientDbName);
                DGS.ArbPro.Types.CalendarQuote newQuote = new DGS.ArbPro.Types.CalendarQuote()
                {
                    StartDateTime = apptStart,
                    EndDateTime = apptEnd,
                    SurveyorID = staff.Id,
                    JobSiteID = siteId,
                    Completed = false,
                    LastUpdated = DateTime.Now,
                    LastDownloaded = DateTime.MinValue,
                    UploadTime = DateTime.MinValue,
                    TemplateId = templateId
                };

                if (!dtbCalendarQuote.SaveQuote(newQuote))
                {
                    log.ErrorFormat("Could not save Appointment. Client Id: {0}, Job Site Id: {1}, Surveyor Id: {2}.", enqClient.Id, siteId, staff.Id);
                    return false;
                }
            }

            log.DebugFormat("({0}:{1}) New Job {2} Saved", staff.Id, usr.NameStr, newJob.Id);
            return true;
        }

        /// <summary>
        /// Saves client contact and client site data passed by the mobile app - used for updating appointment contact and site info
        /// Note that this is not a client object but a client contact one!
        /// </summary>
        /// <param name="token"> Validation token</param>
        /// <param name="title"> Client contact title</param>
        /// <param name="firstName"> Client contact first name</param>
        /// <param name="surname"> Client contact surname</param>
        /// <param name="contactId"> The client contact Id</param>
        /// <param name="streetNo"> Street number to be updated</param>
        /// <param name="address1">Address line 1 to be updated</param>
        /// <param name="address2">Address line 2 to be updated</param>
        /// <param name="town">Town to be updated</param>
        /// <param name="county">County to be updated</param>
        /// <param name="postCode">Post code to be updated</param>
        /// <param name="tel">Telephone to be updated</param>
        /// <param name="mobile">Mobile to be updated</param>
        /// <param name="email">Email address to be updated</param>
        /// <param name="siteId">Client site id</param>
        /// <param name="siteStreetNo">Client site house no</param>
        /// <param name="siteAddress1">Client site address 1</param>
        /// <param name="siteAddress2">Client site address 2</param>
        /// <param name="siteTown">Client site town</param>
        /// <param name="siteCounty">Client site county</param>
        /// <param name="sitePostCode">Client site post code</param>
        /// <returns>Success or failure</returns>
        public bool SaveClientContact(string token, string title, string firstName, string surname, int contactId, string streetNo, string address1, string address2, string town, string county, string postCode, string tel, string mobile, string email, int siteId, string siteStreetNo, string siteAddress1, string siteAddress2, string siteTown, string siteCounty, string sitePostCode)
        {
            bool retVal = false;
            DGS.ArbPro.Types.MobileUser usr = this.GetAndUpdateUserForToken(token);

            this.clientDbName = usr.ClientDatabaseName;

            var dtbClientContact = new DGS.ArbPro.DAL.ClientContact(this.clientDbName);
            var dtbClientSite = new DGS.ArbPro.DAL.ClientSite(this.clientDbName);

            DGS.ArbPro.Types.ClientContact contact = dtbClientContact.GetForId(contactId);
            DGS.ArbPro.Types.ClientSite site = dtbClientSite.GetForId(siteId);

            if (contact != null && contact.Id > 0)
            {
                contact.Title = title;
                contact.Forename = firstName;
                contact.Surname = surname;
                contact.StreetNumber = streetNo;
                contact.Address1 = address1;
                contact.Address2 = address2;
                contact.Town = town;
                contact.County = county;
                contact.PostCode = postCode;
                contact.Telephone = tel;
                contact.Mobile = mobile;
                contact.Email = email;

                retVal = dtbClientContact.Save(contact) > 0;
            }

            if (retVal && site != null && site.Id > 0)
            {
                site.HouseNameNo = siteStreetNo;
                site.Address1 = siteAddress1;
                site.Address2 = siteAddress2;
                site.Town = siteTown;
                site.County = siteCounty;
                site.PostCode = sitePostCode;

                retVal = dtbClientSite.Save(site) > 0;
            }

            return retVal;
        }


        public bool AcceptQuote(string objQuote, string token)
        {
            try
            {
                DGS.ArbPro.Types.QuoteAccept objquote = JsonConvert.DeserializeObject<QuoteAccept>(objQuote);
                bool retVal = false;
                DGS.ArbPro.Types.MobileUser usr = this.GetAndUpdateUserForToken(token);
                this.clientDbName = usr.ClientDatabaseName;

                var dbcontextWorkBook = new DGS.ArbPro.DAL.BookWork(this.clientDbName);
                retVal = dbcontextWorkBook.AcceptQuote(objquote.QuoteId);
                return retVal;
            }
            catch (Exception ex)
            {
                log.Debug("ArbProService :BookWork" + ex.Message);
                return false;
            }
        }

        public bool BookWork(string objBookingDetail, string token)
        {
            try
            {
                DGS.ArbPro.Types.BookingDtl objBooking = JsonConvert.DeserializeObject<BookingDtl>(objBookingDetail);

                bool retVal = false;
                DGS.ArbPro.Types.MobileUser usr = this.GetAndUpdateUserForToken(token);
                this.clientDbName = usr.ClientDatabaseName;

                var dbcontextWorkBook = new DGS.ArbPro.DAL.BookWork(this.clientDbName);
                retVal = dbcontextWorkBook.SaveWork(objBooking);

                return retVal;
            }
            catch (Exception ex)
            {
                log.Debug("ArbProService :BookWork" + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Gets a list of all risk templates for the user but contains shortened, only neccesary info
        /// </summary>
        /// <param name="token">The user token</param>
        /// <returns>A list of risk templates</returns>
        public List<Model.RiskTemplate> GetRiskTemplates(string token)
        {
            DGS.ArbPro.Types.MobileUser usr = this.GetAndUpdateUserForToken(token);

            this.clientDbName = usr.ClientDatabaseName;

            DGS.ArbPro.DAL.Templates.Risk riskDb = new DGS.ArbPro.DAL.Templates.Risk(this.clientDbName);

            List<DGS.ArbPro.Types.Templates.Risk> risks = riskDb.GetAllTemplates();

            risks = risks ?? new List<DGS.ArbPro.Types.Templates.Risk>();

            return risks.Select(risk => new RiskTemplate(risk)).ToList();
        }

        #endregion

        #region private methods

        private List<Model.JobWork> JobItemFromDataTable(
            List<DGS.ArbPro.Types.JobWork> jobItems)
        {
            List<Model.JobWork> retVal = new List<Model.JobWork>();
            // Tanzeel
            foreach (DGS.ArbPro.Types.JobWork item in jobItems)
            {
                Model.JobWork calItm = new Model.JobWork();
                calItm.Id = item.Id;
                calItm.StartDate = item.StartDate;
                calItm.EndDate = item.EndDate;
                calItm.Status = item.Status;
                calItm.Address = item.Address;

                retVal.Add(calItm);
            }

            return retVal;
        }

        private List<Model.CalendarItem> CalendarItemFromDataTable(
            List<DGS.ArbPro.Types.CalendarItem> calItems,
            CalendarItemType itemType)
        {
            List<Model.CalendarItem> retVal = new List<Model.CalendarItem>();

            foreach (DGS.ArbPro.Types.CalendarItem item in calItems)
            {
                Model.CalendarItem calItm = new CalendarItem();
                calItm.ItemType = itemType;
                calItm.Summary = item.Summary;
                calItm.Description = item.Description;
                calItm.EndTime = item.EndTime;
                calItm.StartTime = item.StartTime;
                calItm.AllDay = item.AllDay;
                calItm.QuoteId = item.QuoteId;
                calItm.WorkId = item.WorkId;
                calItm.JobId = item.JobId;
                calItm.JobSiteId = item.JobSiteId;

                retVal.Add(calItm);
            }

            return retVal;
        }

        /// <summary>
        /// The convert to web client single.
        /// </summary>
        /// <param name="obj">
        /// The mobile client object
        /// </param>
        /// <returns>
        /// The web client object
        /// </returns>
        private DGS.ArbPro.Types.Client ConvertToWebClient(DGS.ArbPro.Service.Model.Types.Client obj)
        {
            DGS.ArbPro.Types.Client retVal = new DGS.ArbPro.Types.Client();

            retVal.Id = obj.Id;
            retVal.AltAccountNo = this.DecodeSafeURL(obj.AltAccountNo);
            retVal.Title = this.DecodeSafeURL(obj.Title);
            retVal.Forename = this.DecodeSafeURL(obj.Forename);
            retVal.Surname = this.DecodeSafeURL(obj.Surname);
            retVal.BusinessName = this.DecodeSafeURL(obj.BusinessName);
            retVal.HouseNameNumber = this.DecodeSafeURL(obj.HouseNameNumber);
            retVal.Address1 = this.DecodeSafeURL(obj.Address1);
            retVal.Address2 = this.DecodeSafeURL(obj.Address2);
            retVal.Town = this.DecodeSafeURL(obj.Town);
            retVal.County = this.DecodeSafeURL(obj.County);
            retVal.PostCode = this.DecodeSafeURL(obj.PostCode);
            retVal.Sector = this.DecodeSafeURL(obj.Sector);
            retVal.Telephone = this.DecodeSafeURL(obj.Telephone);
            retVal.Mobile = this.DecodeSafeURL(obj.Mobile);
            retVal.Email = this.DecodeSafeURL(obj.Email);
            retVal.WebAddress = this.DecodeSafeURL(obj.WebAddress);
            retVal.AccountType = (DGS.ArbPro.Types.Enums.ClientTypeEnum)obj.AccountType;
            retVal.AddressLongitude = obj.AddressLongitude;
            retVal.AddressLatitude = obj.AddressLatitude;
            retVal.TermsAgreed = obj.TermsAgreed;
            retVal.SendSmsMessage = obj.SendSmsMessage;
            retVal.IncludeInMarketing = obj.IncludeInMarketing;
            if (obj.DateOpened == DateTime.MinValue)
            {
                obj.DateOpened = (DateTime)SqlDateTime.MinValue;
            }

            retVal.DateOpened = obj.DateOpened;

            return retVal;
        }

        private string DecodeSafeURL(string value)
        {
            log.DebugFormat("DecodeSafeURL: Parameters: encoded value= {0}", value);
            string tmp = HttpUtility.UrlDecode(value);
            log.DebugFormat("DecodeSafeURL: Decoded value= {0}", tmp);
            if (tmp == null) tmp = string.Empty;
            return tmp;
        }

        /// <summary>
        /// Gets a user object based on a token and updates the current session's expiration time
        /// </summary>
        /// <param name="token">The user guid token</param>
        /// <returns>A mobile user object</returns>
        private MobileUser GetAndUpdateUserForToken(string token)
        {
            int extensionTime = int.Parse(ConfigurationManager.AppSettings["UserSessionLifetime"]);
            DGS.ArbPro.DAL.MobileUser usrContext = new DGS.ArbPro.DAL.MobileUser(this.dbName);
            DGS.ArbPro.Types.MobileUser usr = usrContext.GetForGuid(token);

            if (string.IsNullOrWhiteSpace(usr.ClientDatabaseName))
            {
                throw new FaultException<ServiceUnauthenticatedException>(
                    new ServiceUnauthenticatedException("Session expired. Please log in again."));
            }

            if (usr.GuidExpirationDate < DateTime.Now)
            {
                usr.UserGuid = Guid.NewGuid();
            }

            usr.GuidExpirationDate = DateTime.Now.AddMinutes(extensionTime);
            usrContext.Save(usr);

            return usr ?? new MobileUser();
        }

        #endregion


    }


}
