﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArbProWCF
{
    using System.Runtime.Serialization;

    [DataContract]
    public class ServiceUnauthenticatedException
    {
        public ServiceUnauthenticatedException(string message)
        {
            this.Message = message;
        }

        [DataMember]
        public string Message { get; set; }
    }
}