﻿namespace ArbProWCF.Model
{
  using DGS.ArbPro.Types;
    using System;
    using System.Runtime.Serialization;


    /// <summary>
    /// The type of the calendar item.
    /// </summary>


    /// <summary>
    /// An item which appears on the calendar.
    /// </summary>
    /// <remarks>
    /// This is a simple item to show on the calendar - it doesn't have detailed properties which the web system holds, for example team id, surveyor id etc.
    /// </remarks>

    [DataContract]
    public class CalendarItem
    {

        #region Constructors
        /// <summary>
        /// Creates a new object based on a core CalendarItem object
        /// </summary>
        /// <param name="sourceItem"></param>
        public CalendarItem(DGS.ArbPro.Types.CalendarItem sourceItem)
        {
            this.Description = sourceItem.Description;
            this.StartTime = sourceItem.StartTime;
            this.EndTime = sourceItem.EndTime;
            this.ItemType = sourceItem.ItemType;
            this.AllDay = sourceItem.AllDay;
            this.QuoteId = sourceItem.QuoteId;
            this.JobId = sourceItem.JobId;
            this.JobSiteId = sourceItem.JobSiteId;
            this.WorkId = sourceItem.WorkId;
        }

        public CalendarItem()
        {

        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        [DataMember]
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Gets or sets the end time.
        /// </summary>
        [DataMember]
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Gets or sets the item type.
        /// </summary>
        [DataMember]
        public CalendarItemType ItemType { get; set; }

        /// <summary>
        /// Gets or sets the Summary - for display in the calendar view.
        /// </summary>
        [DataMember]
        public string Summary { get; set; }

        /// <summary>
        /// Gets or sets the description of the appointment, for display in a detailed view.
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the description of the appointment, for display in a detailed view.
        /// </summary>
        [DataMember]
        public bool AllDay{ get; set; }

        [DataMember]
        public int QuoteId { get; set; }

        [DataMember]
        public int WorkId { get; set; }

        [DataMember]
        public int JobId { get; set; }

        [DataMember]
        public int JobSiteId { get; set; }

        #endregion Properties
    }
}
