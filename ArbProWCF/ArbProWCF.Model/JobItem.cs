﻿namespace ArbProWCF.Model
{
    using DGS.ArbPro.Types;
    using System;
    using System.Runtime.Serialization;

    [DataContract]
    public class JobWork
    {
           #region Constructors
        /// <summary>
        /// Creates a new object based on a core CalendarItem object
        /// </summary>
        /// <param name="sourceItem"></param>
        public JobWork(DGS.ArbPro.Types.JobWork job)
        {
            this.Id = job.Id;
            this.StartTime = job.StartTime;
            this.EndTime = job.EndTime;
            this.Status = job.Status;
            this.Address = job.Address;
        }

        public JobWork()
        {

        }

        #endregion Constructors

        #region Properties

        [DataMember]
        public int Id { get; set; }
        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        [DataMember]
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Gets or sets the end time.
        /// </summary>
        [DataMember]
        public DateTime EndTime { get; set; }

       
        [DataMember]
        public int Status { get; set; }

        [DataMember]
        public string Address { get; set; }

        #endregion Properties

    }
}
