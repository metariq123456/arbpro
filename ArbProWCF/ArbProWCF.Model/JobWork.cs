﻿namespace ArbProWCF.Model
{
    using DGS.ArbPro.Types;
    using System;
    using System.Runtime.Serialization;

    // Tanzeel
    [DataContract]
    public class JobWork
    {

        #region Constructors
        /// <summary>
        /// Creates a new object based on a core CalendarItem object
        /// </summary>
        /// <param name="sourceItem"></param>
        public JobWork(DGS.ArbPro.Types.JobWork sourceItem)
        {
            // Tanzeel
            this.Id = sourceItem.Id;
            this.StartDate = sourceItem.StartDate;
            this.EndDate = sourceItem.EndDate;
            this.Status = sourceItem.Status;
            this.Address = sourceItem.Address;
        }

        public JobWork()
        {

        }
        #endregion Constructors

        #region Properties

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        [DataMember]
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end time.
        /// </summary>
        [DataMember]
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Gets or sets the end Status.
        /// </summary>
        [DataMember]
        public int Status { get; set; }
 
        /// <summary>
        /// Gets or sets the description of the appointment, for display in a detailed view.
        /// </summary>
        [DataMember]
        public string Address { get; set; }

    
        #endregion Properties
    }
}
