﻿namespace ArbProWCF.Model
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Risk assessment templates shortened info
    /// </summary>
    [DataContract]
    public class RiskTemplate
    {
        #region Constructor

        public RiskTemplate()
        {
            
        }

        public RiskTemplate(DGS.ArbPro.Types.Templates.Risk template)
        {
            this.Id = template.Id;
            this.Name = template.Name;
            this.Description = template.Description;
            this.RefDoc = template.ReferenceDoc;
        }

        #endregion

        #region Properties

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string RefDoc { get; set; }

        [DataMember]
        public string Description { get; set; }

        #endregion

    }
}
