﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using DGS.ArbPro.DAL;
using DGS.ArbPro.BL;

namespace DGS.ArbPro.DatabaseAdmin.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class DatabaseAdminTests
    {
        public DatabaseAdminTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        //[TestMethod]
        //public void TestCreateNewClientDatabase()
        //{
        //    Manager m;
        //    DatabaseInfo ReturnValue;
        //    m = new Manager(@"PI-134\SQL2008");
        //    ReturnValue =  m.CreateNewClientDatabase("ArbProClientTemplate","ArbProClient{0}_Dev",@"c:\ArbPro\Databases");
        //    Assert.IsNotNull(ReturnValue);
        //    Assert.IsTrue(ReturnValue.Name != "");
        //    Assert.IsTrue(ReturnValue.Number != 0);
        //}

        [TestMethod]
        public void TestClientOrganisationAdd()
        {
            BL.ClientOrganisationHandler Handler;
            DAL.ClientOrganisation org = new DAL.ClientOrganisation()
            {
                OrganisationName = DateTime.Now.ToLongTimeString(),
                //OrganisationName = "New Organisation",
                Address1 = "Address 1",
                Address2 = "Address 2",
                Town = "Town",
                CountyOrState = "County",
                PostcodeOrZip = "Postcode",
                CountryID = 230,
                UserAccountsEnabled = true,
                MobileEditionEnabled = false,
                ID = 0
            };

            Handler = new ClientOrganisationHandler();
            Assert.IsTrue(Handler.ClientOrganisationAdd(org) > 0);

        }

        [TestMethod]
        public void TestReportQuote()
        {
            var DB = new DAL.Reporting.DBItems.ReportQuoteDB("ArbProClient5_Dev", 439);
            Assert.IsNotNull(DB);
            var Result = DB.Schedule();
            Assert.IsNotNull(Result);
            Assert.IsTrue(Result.QuoteItems.Count > 0);
        }

        [TestMethod]
        public void TestRiskReferenceAdd()
        {
            int ReturnValue;
            BL.RiskReferenceHandler Handler = new RiskReferenceHandler("ArbProClient2_Dev");

            DAL.RiskReference Ref = new RiskReference() { AssessmentItem = DateTime.Now.ToLongTimeString(), Reference = "Reference", Description= "Description" };
            ReturnValue = Handler.RiskReferenceAdd(Ref);
            Assert.IsTrue(ReturnValue > 0);

        }

        [TestMethod]
        public void TestRiskReferenceList()
        {
            List<RiskReference> ReturnValue;
            BL.RiskReferenceHandler Handler = new RiskReferenceHandler("ArbProClient2_Dev");
            ReturnValue = Handler.RiskReferenceList();
            Assert.IsTrue(ReturnValue.Count > 0);

        }

        [TestMethod]
        public void TestRiskReferenceGet()
        {
            List<RiskReference> ReturnValue;
            BL.RiskReferenceHandler Handler = new RiskReferenceHandler("ArbProClient2_Dev");
            ReturnValue = Handler.RiskReferenceList();
            Assert.IsTrue(ReturnValue.Count > 0);

        }

    }
}
