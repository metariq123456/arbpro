﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Microsec.Elastic;

namespace ArbPro.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class EmailTests
    {
        public EmailTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestSendEmail()
        {
            string Result;
            Client c = new Client("https://api.elasticemail.com/", "james@microsec.co.uk", "71929ad1-2f3a-488a-9d60-c42590b87832");

            Result = c.SendEmail("james@microsec.co.uk","Test email","Test body",null,"james@microsec.co.uk","James Lavery",null,null,null);
            Assert.IsTrue(Result != null);

        }
        [TestMethod]
        public void TestActivityLog()
        {
            List<ActivityLogEntry> Results;
            Client c = new Client("https://api.elasticemail.com/", "james@microsec.co.uk", "71929ad1-2f3a-488a-9d60-c42590b87832");

            Results = c.ActivityLog("", new DateTime(2013, 01, 01), new DateTime(2013, 12, 31));
            Assert.IsTrue(Results.Count > 0);
        }
    }
}
