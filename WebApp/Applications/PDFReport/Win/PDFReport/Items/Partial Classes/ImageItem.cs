﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using Xfinium.Pdf;
using Xfinium.Pdf.Graphics;
using Xfinium.Pdf.Graphics.Text;

namespace PDFReport
{
    public partial class ImageItem
    {
        partial void SetImageImplementation(Byte[] imageArray)
        {
            // Set Image to a Byte Array using the "default" Enum of ResizePDFImage.KeepRatioWithinBox
            SetImageImplementation(imageArray, ResizePDFImage.KeepRatioWithinBox);
        }

        partial void SetImageImplementation(Byte[] imageArray, ResizePDFImage ePDFImageResizing)
        {
            // Set Image to a Byte Array using the passed in Enum param
            // this is the ultimate call from all overloads
            this._ImageBytes = imageArray;

            Bitmap bmp;
            // Print a small (1 X 1) image if no image array has been passed in
            if (imageArray == null || imageArray.Length == 0)
            {
                bmp = new Bitmap(1, 1);
            }
            else
            {
                bmp = new Bitmap(System.Drawing.Image.FromStream(new MemoryStream(imageArray)));
            }

            // Initialise bmp variables
            decimal nWidth = bmp.Width;
            decimal nHeight = bmp.Height;
            // Platform-free resizing
            SetImageResizing(nWidth, nHeight, ePDFImageResizing);

            // Finally, set the image
            this.Image = new PdfImage(bmp);
        }

        partial void SetImageImplementation(string imagePath)
        {
            // Set Image to the Byte Array Read from the passed in image path,
            //using the "default" Enum of ResizePDFImage.KeepRatioWithinBox
            SetImageImplementation(File.ReadAllBytes(imagePath), ResizePDFImage.KeepRatioWithinBox);
        }

        partial void SetImageImplementation(string imagePath, ResizePDFImage ePDFImageResizing)
        {
            // Set Image to the Byte Array Read from the passed in image path,
            //using the passed in Enum param
            SetImageImplementation(File.ReadAllBytes(imagePath), ePDFImageResizing);
        }
    }
}
