﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PDFReport;

namespace UnitTestPDFReport
{
    [TestClass]
    public class UnitTest1
    {
        string OutputFilePath = @"C:\Users\james\Desktop";
        string LayoutFilePath = @"C:\P4\Jon_PI-150_6378\Microsec\PDFReport\dev\PDFReport\Test\Layouts";
        string ImagesFilePath = @"C:\P4\Jon_PI-150_6378\Microsec\PDFReport\dev\PDFReport\Test\Images";

        // left hand coordinates for the rows test
        double[] ColumnLefts = new double[5] { 10, 45, 80, 115, 150 };

        // column widths for the rows test
        double[] ColumnWidths = new double[5] { 30, 30, 30, 50, 60 };

        [TestMethod]
        public void TestSimpleHeader()
        {
            TextItem i;
            TextItem PageNumber;
            TextItem PageNumberLabel;

            i = new TextItem("This should be 10cm wide", 30D, 50D, 100D);
            i.ShouldPrintRectangle = true;

            PageNumberLabel = new TextItem("Page:", 5, 70, 100, "HelveticaBold", 10);
            PageNumber = new TextItem("", 20, 70, 100, "Helvetica", 10);
            PageNumber.SpecialType = TextItem.ItemType.PageNumber;


            Report r = new Report();
            r.Filename = Path.Combine(OutputFilePath, "TestSimpleHeader.PDF");
            r.AddHeader();
            r.Header.AddItem(new TextItem("Header 1", 0, 0, 100));
            r.Header.AddItem(new TextItem("Header 2", 0, 20, 100));
            r.Header.AddItem(new TextItem("Header 3", 0, 30, 100, "Courier", 20));
            r.Header.AddItem(new TextItem("Header 4", 0, 60, 100, "HelveticaBold", 20));
            r.Header.AddItem(i);
            r.Header.AddItem(PageNumberLabel);
            r.Header.AddItem(PageNumber);
            r.Header.Print();
            r.SaveAndClose();
        }

        [TestMethod]
        public void TestMultiplePages()
        {
            TextItem i;
            TextItem PageNumber;
            TextItem PageNumberLabel;

            i = new TextItem("This should be courier 20", 0, 50, 200);
            i.FontName = "Courier";
            i.PointSize = 20;

            PageNumberLabel = new TextItem("Page:", 5, 70, 100, "HelveticaBold", 10);
            PageNumber = new TextItem("", 20, 70, 100, "Helvetica", 10);
            PageNumber.SpecialType = TextItem.ItemType.PageNumber;


            Report r = new Report();
            r.Filename = Path.Combine(OutputFilePath, "TestMultiPage.PDF");
            r.AddHeader();
            r.Header.AddItem(new TextItem("Header 1", 0, 0, 100));
            r.Header.AddItem(new TextItem("Header 2", 0, 20, 100));
            r.Header.AddItem(new TextItem("Header 3", 0, 30, 100, "Courier", 20));
            r.Header.AddItem(new TextItem("Header 4", 0, 60, 100, "HelveticaBold", 20));
            r.Header.AddItem(i);
            r.Header.AddItem(PageNumberLabel);
            r.Header.AddItem(PageNumber);
            r.Header.Print();

            r.NewPage();
            r.Header.Print();

            r.SaveAndClose();
        }

        [TestMethod]
        public void TestPageThrows()
        {
            ItemGroup g;
            TextItem PageNumber;
            TextItem PageHeading;
            double FooterTop;
            Report r = new Report();
            r.HeaderPrinted += TestPageThrowsHeaderPrinted;

            r.Filename = Path.Combine(OutputFilePath, "TestRows.PDF");

            // Set up the header and footer
            r.AddHeader();
            PageHeading = new TextItem("Header", 5, 10, r.Width - 5);
            PageHeading.HAlign = "Center";

            r.Header.AddItem(PageHeading);

            r.AddFooter();
            FooterTop = r.Height - 40;
            PageNumber = new TextItem("", 10, FooterTop, 20);
            PageNumber.SpecialType = TextItem.ItemType.PageNumber;

            r.Footer.AddItem(new TextItem("Page:", 0, FooterTop, 100, "HelveticaBold", 10));
            r.Footer.AddItem(PageNumber);


            // Print the document
            r.PrintHeader();

            g = new ItemGroup(r, "Detail");
            g.RelativeTo = PrintRelativeTo.CurrentPos;
            for (int i = 0; i < 300; i++)
            {
                g.AddItem(new TextItem(string.Format("Row {0} 1", i), 10, 0, ColumnWidths[0]));
                g.AddItem(new TextItem(string.Format("Row {0} 2", i), 35, 0, ColumnWidths[1]));
                g.AddItem(new TextItem(string.Format("Row {0} 3", i), 60, 0, ColumnWidths[2]));

                r.PrintGroup(g);
                g.ClearItems();
            }

            // Save the document and close it
            r.SaveAndClose();
        }

        // This is an event handler for the HeaderPrinted event as hooked up in the TestRows method
        void TestPageThrowsHeaderPrinted(object sender, EventArgs e)
        {
            Report Rpt;
            Rpt = (Report)sender;

            Rpt.CurrentY += 5;

            ItemGroup g = new ItemGroup(Rpt, "ColumnHeaders");
            g.AddItem(new TextItem("Heading 1", 10, 0, ColumnWidths[0]));
            g.AddItem(new TextItem("Heading 2", 35, 0, ColumnWidths[1]));
            g.AddItem(new TextItem("Heading 2", 60, 0, ColumnWidths[2]));
            g.RelativeTo = PrintRelativeTo.CurrentPos;

            Rpt.PrintGroup(g);
        }

        [TestMethod]
        public void TestTableHeaders()
        {
            ItemGroup g;
            TextItem PageNumber;
            TextItem PageHeading;
            double FooterTop;

            ItemGroup TableHeader;

            Report r = new Report();

            r.Filename = Path.Combine(OutputFilePath, "TestTableHeaders.PDF");

            // Set up the header and footer
            r.AddHeader();
            PageHeading = new TextItem("Page Header", 5, 10, r.Width - 5);
            PageHeading.HAlign = "Center";

            r.Header.AddItem(PageHeading);

            r.AddFooter();
            FooterTop = r.Height - 40;
            PageNumber = new TextItem("", 10, FooterTop, 20);
            PageNumber.SpecialType = TextItem.ItemType.PageNumber;

            r.Footer.AddItem(new TextItem("Page:", 0, FooterTop, 100, "HelveticaBold", 10));
            r.Footer.AddItem(PageNumber);

            // Create a heading group for the table
            TableHeader = new ItemGroup(r, "TableHeader");
            TableHeader.RelativeTo = PrintRelativeTo.CurrentPos;

            TableHeader.AddItem(new TextItem("Column 1 header", ColumnLefts[0], 2, ColumnWidths[0], "HelveticaBold", 10));
            TableHeader.AddItem(new TextItem("Column 2 header", ColumnLefts[1], 2, ColumnWidths[1], "HelveticaBold", 10));
            TableHeader.AddItem(new TextItem("Column 3 header", ColumnLefts[2], 2, ColumnWidths[2], "HelveticaBold", 10));
            TableHeader.AddItem(new TextItem("Column 4 header", ColumnLefts[3], 2, ColumnWidths[3], "HelveticaBold", 10));
            TableHeader.AddItem(new TextItem("Column 5 header", ColumnLefts[4], 2, ColumnWidths[4], "HelveticaBold", 10));
            TableHeader.AddItem(new LineItem(1.5, ColumnLefts[0], 5, r.Width - ColumnLefts[0] - 5));

            // Print the document
            r.PrintHeader();

            g = new ItemGroup(r, "Detail");
            g.RelativeTo = PrintRelativeTo.CurrentPos;
            for (int i = 0; i < 300; i++)
            {
                g.AddItem(new TextItem(string.Format("Row {0} 1", i), ColumnLefts[0], 0, ColumnWidths[0]));
                g.AddItem(new TextItem(string.Format("Row {0} 2", i), ColumnLefts[1], 0, ColumnWidths[1]));
                g.AddItem(new TextItem(string.Format("Row {0} 3", i), ColumnLefts[2], 0, ColumnWidths[2]));
                g.AddItem(new TextItem(string.Format("Row {0} 4", i), ColumnLefts[3], 0, ColumnWidths[2]));
                g.AddItem(new TextItem(string.Format("Row {0} 5", i), ColumnLefts[4], 0, ColumnWidths[2]));

                if (i % 10 == 0)
                {
                    r.CurrentY += 5;
                    r.PrintGroup(TableHeader);
                }
                r.PrintGroup(g);
                g.ClearItems();
            }

            // Save the document and close it
            r.SaveAndClose();
        }

        [TestMethod]
        public void TestLetter()
        {
            ItemGroup g;
            double SenderTab;
            double RecipientTab;
            ItemGroup SenderAddress;
            ItemGroup RecipientAddress;
            ItemGroup Body;
            ItemGroup Signature;

            Report r = new Report();

            r.Filename =Path.Combine(OutputFilePath, "TestLetter.PDF");

            // Create a group for the sender's address
            SenderAddress = new ItemGroup(r, "SenderAddress");
            SenderAddress.RelativeTo = PrintRelativeTo.Page;

            SenderTab = 140;
            SenderAddress.AddItem(new TextItem("Microsec Ltd", SenderTab, 20, 130, "CompanyName"));
            SenderAddress.AddItem(new TextItem("Langstone Technology Park", SenderTab, SenderAddress.FindItem("CompanyName").Y + SenderAddress.FindItem("CompanyName").Height, 130, "Address1"));
            SenderAddress.AddItem(new TextItem("Langstone Road", SenderTab, SenderAddress.FindItem("Address1").Y + SenderAddress.FindItem("Address1").Height, 130, "Address2"));
            SenderAddress.AddItem(new TextItem("Havant", SenderTab, SenderAddress.FindItem("Address2").Y + SenderAddress.FindItem("Address2").Height, 130, "Town"));
            SenderAddress.AddItem(new TextItem("Hants", SenderTab, SenderAddress.FindItem("Town").Y + SenderAddress.FindItem("Town").Height, 130, "County"));
            SenderAddress.AddItem(new TextItem("PO9 1SA", SenderTab, SenderAddress.FindItem("County").Y + SenderAddress.FindItem("County").Height, 130, "Postcode"));


            RecipientAddress = new ItemGroup(r, "RecipientAddress");
            RecipientAddress.RelativeTo = PrintRelativeTo.Page;

            RecipientTab = 30;
            RecipientAddress.AddItem(new TextItem("Customer 1", RecipientTab, 30, 130, "CompanyName"));
            RecipientAddress.AddItem(new TextItem("Address 1", RecipientTab, RecipientAddress.FindItem("CompanyName").Y + RecipientAddress.FindItem("CompanyName").Height, 130, "Address1"));
            RecipientAddress.AddItem(new TextItem("Road", RecipientTab, RecipientAddress.FindItem("Address1").Y + RecipientAddress.FindItem("Address1").Height, 130, "Address2"));
            RecipientAddress.AddItem(new TextItem("Town", RecipientTab, RecipientAddress.FindItem("Address2").Y + RecipientAddress.FindItem("Address2").Height, 130, "Town"));
            RecipientAddress.AddItem(new TextItem("County", RecipientTab, RecipientAddress.FindItem("Town").Y + RecipientAddress.FindItem("Town").Height, 130, "County"));
            RecipientAddress.AddItem(new TextItem("Postcode", RecipientTab, RecipientAddress.FindItem("County").Y + RecipientAddress.FindItem("County").Height, 130, "Postcode"));


            Body = new ItemGroup(r, "Body");
            Body.AddItem(new TextItem("body", 10, 70, 180, "Body"));

            Signature = new ItemGroup(r, "Signature");
            Signature.AddItem(new TextItem("Yours faithfully", 30, r.Height - 70, 200));
            Signature.AddItem(new TextItem("Managing Director", 30, r.Height - 30, 200));

            // Finished setting up the groups....


            Body.FindTextItem("Body").Text = "Dear Customer,\n\nThis is the body of the letter.\n\n";
            for (int i = 0; i < 50; i++)
            {
                Body.FindTextItem("Body").Text += "And it goes on and on for quite a bit.";
            }

            r.PrintGroup(SenderAddress);
            r.PrintGroup(RecipientAddress);
            r.PrintGroup(Body);
            r.PrintGroup(Signature);

            // Move on to a second page
            r.NewPage();

            // now print another page for a different customer
            RecipientAddress.FindTextItem("CompanyName").Text = "Customer 2";

            r.PrintGroup(SenderAddress);
            r.PrintGroup(RecipientAddress);
            r.PrintGroup(Body);
            r.PrintGroup(Signature);

            // Save the document and close it
            r.SaveAndClose();
        }



        [TestMethod]
        public void TestLoadLayout()
        {
            TextItem i;
            TextItem PageNumber;
            TextItem PageNumberLabel;
            int ImageNumber;

            i = new TextItem("This should be 10cm wide", 30D, 50D, 100D);
            i.FontName = "Courier";
            i.PointSize = 10;
            i.ShouldPrintRectangle = true;

            PageNumberLabel = new TextItem("Page:", 5, 70, 100, "HelveticaBold", 10);
            PageNumber = new TextItem("", 20, 70, 100, "Helvetica", 10);
            PageNumber.SpecialType = TextItem.ItemType.PageNumber;


            Report r = new Report();
            r.LoadLayout(Path.Combine(LayoutFilePath, "TestLoadLayout.xml"));
            r.Filename = Path.Combine(OutputFilePath, "TestLayoutTest.PDF");

            r.PrintHeader();

            r.PrintGroup(r.Group("DetailHeader"));

            for (int DetailCount = 1; DetailCount < 11; DetailCount++)
            {
                ImageNumber = (DetailCount % 2) + 1;
                r.Group("Detail").FindTextItem("Date").Text = string.Format("{0}/11/2013", DetailCount);
                r.Group("Detail").FindTextItem("Description").Text = string.Format("Description for item {0}", DetailCount);
                r.Group("Detail").FindImageItem("SmallImage").ImagePath = string.Format(@"{0}\TestImage{1}.png",ImagesFilePath, ImageNumber);
                r.Group("Detail").FindImageItem("BigImage").ImagePath = string.Format(@"{0}\TestImage{1}.png", ImagesFilePath, ImageNumber);
                r.PrintGroup(r.Group("Detail"));
            }


            r.SaveAndClose();
        }

        [TestMethod]
        public void TestXMLCreationSDM_InvoiceSchedule()
        {
            Report r = new Report();

            r.LoadLayout("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\Schedule.xml");
            r.Filename = "C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\InvoiceSchedule.pdf";

            r.PrintHeader();
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\InvoiceAddressDateInfo.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\SubjectSiteAddress.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\ScheduleDetailsHdr.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\ScheduleDetails.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\ScheduleDetailTotals.xml");
            
            string sName = "Mr Brian Wallace";
            string sAddress = "1 Sumb Street" + System.Environment.NewLine;
            sAddress += "Somewhere" + System.Environment.NewLine;
            sAddress += "Somehow" + System.Environment.NewLine;
            sAddress += "Somewhen" + System.Environment.NewLine;
            sAddress += "PO1 1AA";

            string sSubject = "Invoice";
            string sSiteAddress = "99 Else Road, ";
            sSiteAddress += "SomewhereElse, ";
            sSiteAddress += "SomehowElse, ";
            sSiteAddress += "SomewhenElse, ";
            sSiteAddress += "PO9 9ZZ";

            string sQuoteNo = "2269";
            string sInvoiceNo = "6016";
            string sOrderNo = "0002";
            string sClientIDNo = "68";
            string sSiteRefNo = "112";

            r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = string.Format("{0}/{1}/{2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sName;
            r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sAddress;
            r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = sQuoteNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtInvoiceNo").Text = sInvoiceNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtOrderNo").Text = sOrderNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = sClientIDNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = sSiteRefNo;

            r.Group("grpSubjectSiteAddress").FindTextItem("txtReportSubject").Text = sSubject;
            r.Group("grpSubjectSiteAddress").FindTextItem("txtSiteAddress").Text = sSiteAddress;

            r.PrintGroup(r.Group("grpAddressDateInfo"));
            r.PrintGroup(r.Group("grpSubjectSiteAddress"));

            r.PrintGroup(r.Group("grpDetailHdr"));

            decimal nTotal = 0;

            for (int i = 0; i < 10; i++)
            {
                r.Group("grpDetail").FindTextItem("txtItemNo").Text = "T" + i.ToString();
                r.Group("grpDetail").FindTextItem("txtItem").Text = "Item_" + i.ToString();
                r.Group("grpDetail").FindTextItem("txtDesc").Text = "This is a description for item in loop with number = " + i.ToString() +
                                                                    " Gonna make it wrap, Gonna make it wrap, Gonna make it wrap, Gonna make it wrap, ";
                r.Group("grpDetail").FindTextItem("txtValue").Text = "£" + (89 * i).ToString();
                nTotal += (89 * i);
                r.PrintGroup(r.Group("grpDetail"));
            }

            decimal nVAT = 20;
            decimal nVATAmount = (nTotal * nVAT / 100);

            r.Group("grpDetailTotals").FindTextItem("lblVatDesc").Text = "VAT (" + nVAT.ToString() + "%): ";
            r.Group("grpDetailTotals").FindTextItem("txtTotValue").Text = "£" + nTotal.ToString();
            r.Group("grpDetailTotals").FindTextItem("txtVatValue").Text = "£" + nVATAmount.ToString();
            r.Group("grpDetailTotals").FindTextItem("txtTotVatValue").Text = "£" + (nTotal + nVATAmount).ToString();

            r.PrintGroup(r.Group("grpDetailTotals"));

            r.SaveAndClose();
        }

        [TestMethod]
        public void TestXMLCreationSDM_InvoiceLetter()
        {
            Report r = new Report();

            r.LoadLayout("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\InvoiceLetter.xml");
            r.Filename = "C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\InvoiceLetter.pdf";

            string sSigPath = "C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\sig.gif";

            r.PrintHeader();
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\InvoiceAddressDateInfo.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\SubjectSiteAddress.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\LetterMainText.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\InvoiceLetterSigSummary.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\InvoiceLetterPaymentDetails.xml");

            string sName = "Mr Brian Wallace";
            string sAddress = "1 Sumb Street" + System.Environment.NewLine;
            sAddress += "Somewhere" + System.Environment.NewLine;
            sAddress += "Somehow" + System.Environment.NewLine;
            sAddress += "Somewhen" + System.Environment.NewLine;
            sAddress += "PO1 1AA";

            string sSubject = "Invoice";
            string sSiteAddress = "99 Else Road, ";
            sSiteAddress += "SomewhereElse, ";
            sSiteAddress += "SomehowElse, ";
            sSiteAddress += "SomewhenElse, ";
            sSiteAddress += "PO9 9ZZ";

            string sQuoteNo = "2269";
            string sInvoiceNo = "6016";
            string sOrderNo = "0002";
            string sClientIDNo = "68";
            string sSiteRefNo = "112";

            r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = string.Format("{0}/{1}/{2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sName;
            r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sAddress;
            r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = sQuoteNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtInvoiceNo").Text = sInvoiceNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtOrderNo").Text = sOrderNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = sClientIDNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = sSiteRefNo;

            r.Group("grpSubjectSiteAddress").FindTextItem("txtReportSubject").Text = sSubject;
            r.Group("grpSubjectSiteAddress").FindTextItem("txtSiteAddress").Text = sSiteAddress;

            r.PrintGroup(r.Group("grpAddressDateInfo"));
            r.PrintGroup(r.Group("grpSubjectSiteAddress"));

            string sMainText = "";
            string sSignOff = "DGS TREES";

            for (int j = 0; j < 3; j++)
            {
                sMainText += "Lots and lots of words to fill quite a lot of the page. ";

                for (int k = 0; k < 5; k++)
                {
                    sMainText += "Rinse and repeat. ";
                }
                sMainText += "Simples. " + System.Environment.NewLine + System.Environment.NewLine;
            }
            sMainText += System.Environment.NewLine + sSignOff;

            r.Group("grpMainText").FindTextItem("txtMainText").Text = sMainText;

            r.PrintGroup(r.Group("grpMainText"));

            string sSignatory = "Some Signatory, MD";

            decimal nTotal = 495;
            decimal nVAT = 20;
            decimal nVATAmount = (nTotal * nVAT / 100);

            byte[] bytImage = File.ReadAllBytes(sSigPath);

            r.Group("grpSigSummary").FindImageItem("imgSig").SetImage(bytImage);

            r.Group("grpSigSummary").FindTextItem("txtSigName").Text = sSignatory;

            r.Group("grpSigSummary").FindTextItem("txtVatRate").Text = "(Rate " + nVAT.ToString() + "%)";
            r.Group("grpSigSummary").FindTextItem("txtTotal").Text = "£" + nTotal.ToString();
            r.Group("grpSigSummary").FindTextItem("txtVat").Text = "£" + nVATAmount.ToString();
            r.Group("grpSigSummary").FindTextItem("txtTotalIncVat").Text = "£" + (nTotal + nVATAmount).ToString();

            r.PrintGroup(r.Group("grpSigSummary"));

            string sPaymentDetailsText = "Payment Methods: ";
            sPaymentDetailsText += System.Environment.NewLine;
            sPaymentDetailsText += "Cheque: Please make cheques payable to \"CashMyCheque\"";
            sPaymentDetailsText += System.Environment.NewLine;
            sPaymentDetailsText += "BACS: Acct: 012345 Sort: 112233 Name:CashIt";
            sPaymentDetailsText += System.Environment.NewLine;
            sPaymentDetailsText += System.Environment.NewLine;
            sPaymentDetailsText += "Please quote invoice number, etc. Blah, blah, blah.";
            sPaymentDetailsText += System.Environment.NewLine;
            sPaymentDetailsText += "And one final line to make 6.";

            r.Group("grpPaymentDetails").FindTextItem("txtPaymentDetailsText").Text = sPaymentDetailsText;

            r.PrintGroup(r.Group("grpPaymentDetails"));

            r.SaveAndClose();
        }

        [TestMethod]
        public void TestXMLCreationSDM_QuoteSchedule()
        {
            Report r = new Report();

            r.LoadLayout("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\Schedule.xml");
            r.Filename = "C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\QuoteSchedule.pdf";

            r.PrintHeader();
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\QuoteAddressDateInfoGreen.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\SubjectSiteAddress.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\ScheduleDetailsHdr.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\ScheduleDetails.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\ScheduleDetailTotals.xml");

            string sName = "Mr Brian Wallace";
            string sAddress = "1 Sumb Street" + System.Environment.NewLine;
            sAddress += "Somewhere" + System.Environment.NewLine;
            sAddress += "Somehow" + System.Environment.NewLine;
            sAddress += "Somewhen" + System.Environment.NewLine;
            sAddress += "PO1 1AA";

            string sSubject = "Schedule of Work";
            string sSiteAddress = "99 Else Road, ";
            sSiteAddress += "SomewhereElse, ";
            sSiteAddress += "SomehowElse, ";
            sSiteAddress += "SomewhenElse, ";
            sSiteAddress += "PO9 9ZZ";
            
            string sQuoteNo = "2269";
            string sClientIDNo = "68";
            string sSurveyor = "M B Stocks";
            string sSiteRefNo = "112";

            r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = string.Format("{0}/{1}/{2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sName;
            r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sAddress;
            r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = sQuoteNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = sClientIDNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtSurveyor").Text = sSurveyor;
            r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = sSiteRefNo;

            r.Group("grpSubjectSiteAddress").FindTextItem("txtReportSubject").Text = sSubject;
            r.Group("grpSubjectSiteAddress").FindTextItem("txtSiteAddress").Text = sSiteAddress;

            r.PrintGroup(r.Group("grpAddressDateInfo"));
            r.PrintGroup(r.Group("grpSubjectSiteAddress"));

            r.PrintGroup(r.Group("grpDetailHdr"));

            decimal nTotal = 0;

            for (int i = 0; i < 10; i++)
            {
                r.Group("grpDetail").FindTextItem("txtItemNo").Text = "T" + i.ToString();
                r.Group("grpDetail").FindTextItem("txtItem").Text = "Item_" + i.ToString();
                r.Group("grpDetail").FindTextItem("txtDesc").Text = "This is a description for item in loop with number = " + i.ToString() +
                                                                    " Gonna make it wrap, Gonna make it wrap, Gonna make it wrap, Gonna make it wrap, ";
                r.Group("grpDetail").FindTextItem("txtValue").Text = "£" + (89 * i).ToString();
                nTotal += (89 * i);
                r.PrintGroup(r.Group("grpDetail"));
            }

            decimal nVAT = 20;
            decimal nVATAmount = (nTotal * nVAT / 100);

            r.Group("grpDetailTotals").FindTextItem("lblVatDesc").Text = "VAT (" + nVAT.ToString() + "%): ";
            r.Group("grpDetailTotals").FindTextItem("txtTotValue").Text = "£" + nTotal.ToString();
            r.Group("grpDetailTotals").FindTextItem("txtVatValue").Text = "£" + nVATAmount.ToString();
            r.Group("grpDetailTotals").FindTextItem("txtTotVatValue").Text = "£" + (nTotal + nVATAmount).ToString();

            r.PrintGroup(r.Group("grpDetailTotals"));

            r.SaveAndClose();
        }
        
        [TestMethod]
        public void TestXMLCreationSDM_QuoteLetter()
        {
            Report r = new Report();

            r.LoadLayout("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\QuoteLetter.xml");
            r.Filename = "C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\QuoteLetter.pdf";

            string sSigPath = "C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\sig.gif";

            r.PrintHeader();
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\QuoteAddressDateInfo.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\LetterSalutationSiteAddress.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\LetterMainText.xml");
            r.AddCustomGroup("C:\\Scratch\\ReportTesting\\ArbPro_FINAL\\LetterSignOffSig.xml");
            
            string sName = "Mr Brian Wallace";
            string sAddress = "1 Sumb Street" + System.Environment.NewLine;
            sAddress += "Somewhere" + System.Environment.NewLine;
            sAddress += "Somehow" + System.Environment.NewLine;
            sAddress += "Somewhen" + System.Environment.NewLine;
            sAddress += "PO1 1AA";

            string sSalutation = "Dear " + sName;
            string sSiteAddress = "99 Else Road, ";
            sSiteAddress += "SomewhereElse, ";
            sSiteAddress += "SomehowElse, ";
            sSiteAddress += "SomewhenElse, ";
            sSiteAddress += "PO9 9ZZ";

            string sQuoteNo = "2269";
            string sClientIDNo = "68";
            string sSurveyor = "M B Stocks";
            string sSiteRefNo = "112";

            r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = string.Format("{0}/{1}/{2}", DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sName;
            r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sAddress;
            r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = sQuoteNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = sClientIDNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtSurveyor").Text = sSurveyor;
            r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = sSiteRefNo;

            r.Group("grpSalutationSiteAddress").FindTextItem("txtSalutation").Text = sSalutation;
            r.Group("grpSalutationSiteAddress").FindTextItem("txtSiteAddress").Text = sSiteAddress;

            r.PrintGroup(r.Group("grpAddressDateInfo"));
            r.PrintGroup(r.Group("grpSalutationSiteAddress"));

            string sMainText = "";

            for (int j = 0; j < 3; j++)
            {
                sMainText += "Lots and lots of words to fill quite a lot of the page. ";

                for (int k = 0; k < 5; k++)
                {
                    sMainText += "Rinse and repeat. ";
                }
                sMainText += "Simples. " + System.Environment.NewLine + System.Environment.NewLine;
            }
            sMainText += System.Environment.NewLine;

            r.Group("grpMainText").FindTextItem("txtMainText").Text = sMainText;

            r.PrintGroup(r.Group("grpMainText"));

            string sSignOff = "Regards M B Stocks";
            string sSignatory = "M B Stocks, MD";
            
            r.Group("grpSignOffSig").FindTextItem("txtSignOff").Text = sSignOff;

            byte[] bytImage = File.ReadAllBytes(sSigPath);

            r.Group("grpSignOffSig").FindImageItem("imgSig").SetImage(bytImage);

            r.Group("grpSignOffSig").FindTextItem("txtSigName").Text = sSignatory;

            r.PrintGroup(r.Group("grpSignOffSig"));
            
            r.SaveAndClose();
        }


        private bool _PrintHeader = false;
        [TestMethod]
        public void TestLoadLayoutAlternate()
        {


            Report r = new Report();
            r.LoadLayout(Path.Combine(LayoutFilePath, "AlternateLayoutTest.xml"));
            r.Filename = Path.Combine(OutputFilePath, string.Format("AlternateLayoutTest{0}.PDF",DateTime.Now.Minute.ToString()));
            r.HeaderPrinted += TestPageThrowsPrintTableHeaders;

            //Set header value
            //r.Header.FindTextItem("HeadingDate").Text = DateTime.Now.ToShortDateString();
            r.PrintHeader();
            //Print the intro 
            r.PrintGroup(r.Group("IntroGroup"));

            //Print the first group header
            r.Group("OutOfDateIntroGroup").FindTextItem("OutOfDateIntro").Text = "This text has been replaced at run time and would represent the email introduction for warnning items.";
            r.PrintGroup(r.Group("OutOfDateIntroGroup"));
            //Print the corisponding detail header

            r.Group("TableHeaderPrt1").FindTextItem("BuildingValue").Text = "Test Building 1";
            r.PrintGroup(r.Group("TableHeaderPrt1"));
            r.PrintGroup(r.Group("TableHeaderPrt2"));
            r.PrintGroup(r.Group("TableHeaderPrt3"));
            _PrintHeader = true;
            for (int DetailCount = 1; DetailCount < 50; DetailCount++)
            {
                r.Group("OutOfDateDetail").FindTextItem("PointPath").Text = string.Format("Location A{0}: Location B{0}: Location C{0}: Location D{0}: Point name {0}",DetailCount);
                r.Group("OutOfDateDetail").FindTextItem("FilterBarcode").Text = string.Format("PALL01{0}", (1000000+DetailCount).ToString());
                r.Group("OutOfDateDetail").FindTextItem("FilterType").Text = "AQF4";
                r.Group("OutOfDateDetail").FindTextItem("FilterDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.Group("OutOfDateDetail").FindTextItem("DockBarcode").Text = string.Format("{0}", (1000 + DetailCount).ToString());
                r.Group("OutOfDateDetail").FindTextItem("DockType").Text = "AQF4";
                r.Group("OutOfDateDetail").FindTextItem("DockDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.PrintGroup(r.Group("OutOfDateDetail"));
            }
            _PrintHeader = false;
            r.Group("WeeklyIntroGroup").FindTextItem("WeeklyIntro").Text = @"This section represents a dynamic value that has been set at run time via application code. AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            r.PrintGroup(r.Group("WeeklyIntroGroup"));
            r.Group("TableHeaderPrt1").FindTextItem("BuildingValue").Text = "Warning Test Building 1";
            r.PrintGroup(r.Group("TableHeaderPrt1"));
            r.PrintGroup(r.Group("TableHeaderPrt2"));
            r.PrintGroup(r.Group("TableHeaderPrt3"));
            _PrintHeader = true;
            for (int DetailCount = 1; DetailCount < 50; DetailCount++)
            {
                r.Group("WeeklyDetail").FindTextItem("PointPath").Text = string.Format("Location A{0}: Location B{0}: Location C{0}: Location D{0}: Point name {0}", DetailCount);
                r.Group("WeeklyDetail").FindTextItem("FilterBarcode").Text = string.Format("PALL01{0}", (1000000 + DetailCount).ToString());
                r.Group("WeeklyDetail").FindTextItem("FilterType").Text = "AQF4";
                r.Group("WeeklyDetail").FindTextItem("FilterDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.Group("WeeklyDetail").FindTextItem("DockBarcode").Text = string.Format("{0}", (1000 + DetailCount).ToString());
                r.Group("WeeklyDetail").FindTextItem("DockType").Text = "AQF4";
                r.Group("WeeklyDetail").FindTextItem("DockDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.PrintGroup(r.Group("WeeklyDetail"));
            }
            _PrintHeader = false;


            r.SaveAndClose();
        }

        public void TestPageThrowsPrintTableHeaders(object sender, EventArgs e)
        {
            Report r = (Report)sender;
            if (_PrintHeader)
            {
                r.PrintGroup(r.Group("TableHeaderPrt1"));
                r.PrintGroup(r.Group("TableHeaderPrt2"));
                r.PrintGroup(r.Group("TableHeaderPrt3"));
            }
        }

        [TestMethod]
        public void TestCustomGroupLayout()
        {
            string reportLayout = Path.Combine(LayoutFilePath, "DynamicGroupLayout.xml");
            string headerLayout = Path.Combine(LayoutFilePath, "DynamicHeaderLayout.xml");
            string footerLayout = Path.Combine(LayoutFilePath, "DynamicFooterLayout.xml");

            Report r = new Report();
            r.LoadLayout(reportLayout);
            r.Filename = System.IO.Path.Combine(OutputFilePath, string.Format("DynamicGroupLoad_{0}.pdf",(DateTime.Now.Second + DateTime.Now.Millisecond).ToString()));
            r.AddCustomGroup(headerLayout);
            //r.AddCustomGroup(footerLayout);

            r.HeaderPrinted += TestPageThrowsPrintTableHeaders;

            //Set header value
            //r.Header.FindTextItem("HeadingDate").Text = DateTime.Now.ToShortDateString();
            r.PrintHeader();
            //Print the intro 
            r.PrintGroup(r.Group("IntroGroup"));

            //Print the first group header
            r.Group("OutOfDateIntroGroup").FindTextItem("OutOfDateIntro").Text = "This text has been replaced at run time and would represent the email introduction for warnning items.";
            r.PrintGroup(r.Group("OutOfDateIntroGroup"));
            //Print the corisponding detail header

            r.Group("TableHeaderPrt1").FindTextItem("BuildingValue").Text = "Test Building 1";
            r.PrintGroup(r.Group("TableHeaderPrt1"));
            r.PrintGroup(r.Group("TableHeaderPrt2"));
            r.PrintGroup(r.Group("TableHeaderPrt3"));
            _PrintHeader = true;
            for (int DetailCount = 1; DetailCount < 50; DetailCount++)
            {
                r.Group("OutOfDateDetail").FindTextItem("PointPath").Text = string.Format("Location A{0}: Location B{0}: Location C{0}: Location D{0}: Point name {0}", DetailCount);
                r.Group("OutOfDateDetail").FindTextItem("FilterBarcode").Text = string.Format("PALL01{0}", (1000000 + DetailCount).ToString());
                r.Group("OutOfDateDetail").FindTextItem("FilterType").Text = "AQF4";
                r.Group("OutOfDateDetail").FindTextItem("FilterDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.Group("OutOfDateDetail").FindTextItem("DockBarcode").Text = string.Format("{0}", (1000 + DetailCount).ToString());
                r.Group("OutOfDateDetail").FindTextItem("DockType").Text = "AQF4";
                r.Group("OutOfDateDetail").FindTextItem("DockDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.PrintGroup(r.Group("OutOfDateDetail"));
            }
            _PrintHeader = false;
            r.Group("WeeklyIntroGroup").FindTextItem("WeeklyIntro").Text = @"This section represents a dynamic value that has been set at run time via application code. AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            r.PrintGroup(r.Group("WeeklyIntroGroup"));
            r.Group("TableHeaderPrt1").FindTextItem("BuildingValue").Text = "Warning Test Building 1";
            r.PrintGroup(r.Group("TableHeaderPrt1"));
            r.PrintGroup(r.Group("TableHeaderPrt2"));
            r.PrintGroup(r.Group("TableHeaderPrt3"));
            _PrintHeader = true;
            for (int DetailCount = 1; DetailCount < 50; DetailCount++)
            {
                r.Group("WeeklyDetail").FindTextItem("PointPath").Text = string.Format("Location A{0}: Location B{0}: Location C{0}: Location D{0}: Point name {0}", DetailCount);
                r.Group("WeeklyDetail").FindTextItem("FilterBarcode").Text = string.Format("PALL01{0}", (1000000 + DetailCount).ToString());
                r.Group("WeeklyDetail").FindTextItem("FilterType").Text = "AQF4";
                r.Group("WeeklyDetail").FindTextItem("FilterDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.Group("WeeklyDetail").FindTextItem("DockBarcode").Text = string.Format("{0}", (1000 + DetailCount).ToString());
                r.Group("WeeklyDetail").FindTextItem("DockType").Text = "AQF4";
                r.Group("WeeklyDetail").FindTextItem("DockDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.PrintGroup(r.Group("WeeklyDetail"));
            }
            _PrintHeader = false;


            r.SaveAndClose();
        }

        [TestMethod]
        public void TestCustomHeaderAndFooter()
        {
            TestBody("DynamicGroupLayout.xml","DynamicHeaderLayout.xml","DynamicFooterLayout.xml",string.Format("DynamicGroupLoad_{0}.pdf", (DateTime.Now.Second + DateTime.Now.Millisecond).ToString()));
        }

        [TestMethod]
        public void TestGrid()
        {
            TestBody("GridLayoutTest.xml", "DynamicHeaderLayout.xml", "DynamicFooterLayout.xml", string.Format("GridLayout_{0}.pdf", (DateTime.Now.Second + DateTime.Now.Millisecond).ToString()));
        }

        [TestMethod]
        public void VerticalLineGrid()
        {
            TestBody("LineLayoutTest.xml", "DynamicHeaderLayout.xml", "DynamicFooterLayout.xml", string.Format("LineLayout_{0}.pdf", (DateTime.Now.Second + DateTime.Now.Millisecond).ToString()));
        }

        private void TestBody(string reportFilename, string headerFileName, string footerFileName, string outputFileName)
        {
            string reportLayout = Path.Combine(LayoutFilePath, reportFilename);
            string headerLayout = Path.Combine(LayoutFilePath, headerFileName);
            string footerLayout = Path.Combine(LayoutFilePath, footerFileName);

            Report r = new Report();
            r.LoadLayout(reportLayout);
            r.Filename = Path.Combine(OutputFilePath, outputFileName);
            r.AddCustomGroup(headerLayout);
            r.AddCustomGroup(footerLayout);

            r.HeaderPrinted += TestPageThrowsPrintTableHeaders;

            //Set header value
            //r.Header.FindTextItem("HeadingDate").Text = DateTime.Now.ToShortDateString();
            r.PrintHeader();
            //Print the intro 
            r.PrintGroup(r.Group("IntroGroup"));

            //Print the first group header
            r.Group("OutOfDateIntroGroup").FindTextItem("OutOfDateIntro").Text = "This text has been replaced at run time and would represent the email introduction for warnning items.";
            r.PrintGroup(r.Group("OutOfDateIntroGroup"));
            //Print the corisponding detail header

            r.Group("TableHeaderPrt1").FindTextItem("BuildingValue").Text = "Test Building 1";
            r.PrintGroup(r.Group("TableHeaderPrt1"));
            r.PrintGroup(r.Group("TableHeaderPrt2"));
            r.PrintGroup(r.Group("TableHeaderPrt3"));
            _PrintHeader = true;
            for (int DetailCount = 1; DetailCount < 50; DetailCount++)
            {
                r.Group("OutOfDateDetail").FindTextItem("PointPath").Text = string.Format("Location A{0}: Location B{0}: Location C{0}: Location D{0}: Point name {0}", DetailCount);
                r.Group("OutOfDateDetail").FindTextItem("FilterBarcode").Text = string.Format("PALL01{0}", (1000000 + DetailCount).ToString());
                r.Group("OutOfDateDetail").FindTextItem("FilterType").Text = "AQF4";
                r.Group("OutOfDateDetail").FindTextItem("FilterDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.Group("OutOfDateDetail").FindTextItem("DockBarcode").Text = string.Format("{0}", (1000 + DetailCount).ToString());
                r.Group("OutOfDateDetail").FindTextItem("DockType").Text = "AQF4";
                r.Group("OutOfDateDetail").FindTextItem("DockDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.PrintGroup(r.Group("OutOfDateDetail"));
            }
            _PrintHeader = false;
            r.Group("WeeklyIntroGroup").FindTextItem("WeeklyIntro").Text = @"This section represents a dynamic value that has been set at run time via application code. AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            r.PrintGroup(r.Group("WeeklyIntroGroup"));
            r.Group("TableHeaderPrt1").FindTextItem("BuildingValue").Text = "Warning Test Building 1";
            r.PrintGroup(r.Group("TableHeaderPrt1"));
            r.PrintGroup(r.Group("TableHeaderPrt2"));
            r.PrintGroup(r.Group("TableHeaderPrt3"));
            _PrintHeader = true;
            for (int DetailCount = 1; DetailCount < 50; DetailCount++)
            {
                r.Group("WeeklyDetail").FindTextItem("PointPath").Text = string.Format("Location A{0}: Location B{0}: Location C{0}: Location D{0}: Point name {0}", DetailCount);
                r.Group("WeeklyDetail").FindTextItem("FilterBarcode").Text = string.Format("PALL01{0}", (1000000 + DetailCount).ToString());
                r.Group("WeeklyDetail").FindTextItem("FilterType").Text = "AQF4";
                r.Group("WeeklyDetail").FindTextItem("FilterDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.Group("WeeklyDetail").FindTextItem("DockBarcode").Text = string.Format("{0}", (1000 + DetailCount).ToString());
                r.Group("WeeklyDetail").FindTextItem("DockType").Text = "AQF4";
                r.Group("WeeklyDetail").FindTextItem("DockDate").Text = DateTime.Now.AddDays(-DetailCount).ToShortDateString();
                r.PrintGroup(r.Group("WeeklyDetail"));
            }
            _PrintHeader = false;


            r.SaveAndClose();
        }

        [TestMethod]
        public void LineDrawTest()
        {
            Report r = new Report();
            r.Filename = Path.Combine(OutputFilePath, string.Format("Lines_{0}.pdf", (DateTime.Now.Second + DateTime.Now.Millisecond).ToString()));

            ItemGroup grp = new ItemGroup(r, "lines");
            grp.AddItem(new LineItem(1, 10, 10, 190, "horizontal"));
            grp.AddItem(new LineItem(1, 20, 20, 1, 190, "vertical"));

            r.AddGroup(grp);
            r.PrintGroup(grp);

            r.SaveAndClose();
        }

        [TestMethod]
        public void DGSScheduledWorkTest()
        {
            Report r = new Report();
            r.Filename = Path.Combine(OutputFilePath, string.Format("DGSScheduledWorkTest_{0}.pdf", (DateTime.Now.Second + DateTime.Now.Millisecond).ToString()));

            //Load the layout
            r.LoadLayout(Path.Combine(LayoutFilePath,"ScheduledWork.xml"));

            r.PrintHeader();
            r.PrintGroup(r.Group("QuoteAddress"));

            r.SaveAndClose();
        }


        [TestMethod]
        public void DGSDynamicHeaderTest()
        {
            Report r = new Report();
            r.Filename = Path.Combine(OutputFilePath, string.Format("DGSScheduledWorkTest_{0}.pdf", (DateTime.Now.Second + DateTime.Now.Millisecond).ToString()));

            //Load the layout
            r.LoadLayout(Path.Combine(LayoutFilePath, "ScheduledWork.xml"));
            r.AddCustomGroup(Path.Combine(LayoutFilePath, "DGSClientHeader.xml"));
            r.Header.FindImageItem("Logo").ImagePath = string.Format(@"{0}\Tree.png", ImagesFilePath);

            r.PrintHeader();
            r.PrintGroup(r.Group("QuoteDetails"));
            r.PrintGroup(r.Group("ScheduledDetails"));
            r.PrintGroup(r.Group("DetailHeader"));
            double total = 0;
            double runningTotal = 0;
            ItemGroup g = r.Group("Detail");
            for (int index = 1; index < 4; index++)
            {
                total = 100.00 + (1.75 * index);
                g.FindTextItem("ItemNo").Text = string.Format("T{0}", index);
                g.FindTextItem("Item").Text = "A Tree";
                g.FindTextItem("Work").Text = "Some work to be done";
                g.FindTextItem("Value").Text = string.Format("£{0}", total);
                r.PrintGroup(g);
                runningTotal += total;
            }


            g = r.Group("DetailTotals");
            g.FindTextItem("TotalValue").Text = string.Format("£{0}", runningTotal);
            g.FindTextItem("VatValue").Text = string.Format("£{0}", runningTotal * 0.2); ;
            g.FindTextItem("TotalVatValue").Text = string.Format("£{0}", runningTotal + runningTotal * 0.2); ;
            r.PrintGroup(g);

            r.SaveAndClose();
        }
    }
}
