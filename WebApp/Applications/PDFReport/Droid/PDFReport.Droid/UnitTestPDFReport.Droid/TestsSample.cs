using NUnit.Framework;
using PDFReport;
using Android.OS;
using System.IO;
using System;

namespace UnitTestPDFReport.Droid
{
	[TestFixture]
	public class TestsSample
	{
		private string _DestinationFolder;

		[SetUp]
		public void Setup ()
		{
			_DestinationFolder = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
			CopyAssetToFile ("Layouts/TestLoadLayout.xml", Path.Combine (_DestinationFolder, "TestLoadLayout.xml"));
			CopyAssetToFile ("Layouts/AlternateLayoutTest.xml", Path.Combine (_DestinationFolder, "AlternateLayoutTest.xml"));
		}

		[TearDown]
		public void Tear ()
		{
		}

		[Test]
		public void TestLoadLayout ()
		{
			TextItem i;
			TextItem PageNumber;
			int ImageNumber;

			i = new TextItem ("This should be 10cm wide", 30D, 50D, 100D);
			i.FontName = "Courier";
			i.PointSize = 10;
			i.ShouldPrintRectangle = true;

			PageNumber = new TextItem ("", 20, 70, 100, "Helvetica", 10);
			PageNumber.SpecialType = TextItem.ItemType.PageNumber;


			Report r = new Report ();
			r.LoadLayout (Path.Combine (_DestinationFolder, "TestLoadLayout.xml"));
			r.Filename = System.IO.Path.Combine (Android.OS.Environment.ExternalStorageDirectory.ToString (), "TestLayoutTest.PDF");

			r.PrintHeader ();

			r.PrintGroup (r.Group ("DetailHeader"));

			for (int DetailCount = 1; DetailCount < 11; DetailCount++) {
				ImageNumber = (DetailCount % 2) + 1;
				r.Group ("Detail").FindTextItem ("Date").Text = string.Format ("{0}/11/2013", DetailCount);
				r.Group ("Detail").FindTextItem ("Description").Text = string.Format ("Description for item {0}", DetailCount);
				r.Group ("Detail").FindImageItem ("SmallImage").ImagePath = string.Format (@"				C:\P4\Jon_PI-150_6378\Microsec\PDFReport\dev\PDFReport\Test\Images\TestImage{0}.png", ImageNumber);
				r.Group ("Detail").FindImageItem ("BigImage").ImagePath = string.Format (@"				C:\P4\Jon_PI-150_6378\Microsec\PDFReport\dev\PDFReport\Test\Images\TestImage{0}.png", ImageNumber);
				r.PrintGroup (r.Group ("Detail"));
			}


			r.SaveAndClose ();
		}

		private bool _PrintHeader = false;

		[Test]
		public void TestLoadLayoutAlternate ()
		{


			Report r = new Report ();
			r.LoadLayout (Path.Combine (_DestinationFolder, "AlternateLayoutTest.xml"));
			r.Filename = System.IO.Path.Combine (Android.OS.Environment.ExternalStorageDirectory.ToString (), "AlternateLayoutTest.PDF");

			r.HeaderPrinted += TestPageThrowsPrintTableHeaders;

			//Set header value
			r.Header.FindTextItem ("HeadingDate").Text = DateTime.Now.ToShortDateString ();
			r.PrintHeader ();
			//Print the intro 
			r.PrintGroup (r.Group ("IntroGroup"));

			//Print the first group header
			r.Group ("OutOfDateIntroGroup").FindTextItem ("OutOfDateIntro").Text = "This text has been replaced at run time and would represent the email introduction for warnning items.";
			r.PrintGroup (r.Group ("OutOfDateIntroGroup"));
			//Print the corisponding detail header

			r.Group ("TableHeaderPrt1").FindTextItem ("BuildingValue").Text = "Test Building 1";
			r.PrintGroup (r.Group ("TableHeaderPrt1"));
			r.PrintGroup (r.Group ("TableHeaderPrt2"));
			r.PrintGroup (r.Group ("TableHeaderPrt3"));
			_PrintHeader = true;
			for (int DetailCount = 1; DetailCount < 50; DetailCount++) {
				r.Group ("OutOfDateDetail").FindTextItem ("PointPath").Text = string.Format ("Location A{0}: Location B{0}: Location C{0}: Location D{0}: Point name {0}", DetailCount);
				r.Group ("OutOfDateDetail").FindTextItem ("FilterBarcode").Text = string.Format ("PALL01{0}", (1000000 + DetailCount).ToString ());
				r.Group ("OutOfDateDetail").FindTextItem ("FilterType").Text = "AQF4";
				r.Group ("OutOfDateDetail").FindTextItem ("FilterDate").Text = DateTime.Now.AddDays (-DetailCount).ToShortDateString ();
				r.Group ("OutOfDateDetail").FindTextItem ("DockBarcode").Text = string.Format ("{0}", (1000 + DetailCount).ToString ());
				r.Group ("OutOfDateDetail").FindTextItem ("DockType").Text = "AQF4";
				r.Group ("OutOfDateDetail").FindTextItem ("DockDate").Text = DateTime.Now.AddDays (-DetailCount).ToShortDateString ();
				r.PrintGroup (r.Group ("OutOfDateDetail"));
			}
			_PrintHeader = false;
			r.Group ("WeeklyIntroGroup").FindTextItem ("WeeklyIntro").Text = @"			This section represents a dynamic value that has been set at run time via application code. AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
			r.PrintGroup (r.Group ("WeeklyIntroGroup"));
			r.Group ("TableHeaderPrt1").FindTextItem ("BuildingValue").Text = "Warning Test Building 1";
			r.PrintGroup (r.Group ("TableHeaderPrt1"));
			r.PrintGroup (r.Group ("TableHeaderPrt2"));
			r.PrintGroup (r.Group ("TableHeaderPrt3"));
			_PrintHeader = true;
			for (int DetailCount = 1; DetailCount < 50; DetailCount++) {
				r.Group ("WeeklyDetail").FindTextItem ("PointPath").Text = string.Format ("Location A{0}: Location B{0}: Location C{0}: Location D{0}: Point name {0}", DetailCount);
				r.Group ("WeeklyDetail").FindTextItem ("FilterBarcode").Text = string.Format ("PALL01{0}", (1000000 + DetailCount).ToString ());
				r.Group ("WeeklyDetail").FindTextItem ("FilterType").Text = "AQF4";
				r.Group ("WeeklyDetail").FindTextItem ("FilterDate").Text = DateTime.Now.AddDays (-DetailCount).ToShortDateString ();
				r.Group ("WeeklyDetail").FindTextItem ("DockBarcode").Text = string.Format ("{0}", (1000 + DetailCount).ToString ());
				r.Group ("WeeklyDetail").FindTextItem ("DockType").Text = "AQF4";
				r.Group ("WeeklyDetail").FindTextItem ("DockDate").Text = DateTime.Now.AddDays (-DetailCount).ToShortDateString ();
				r.PrintGroup (r.Group ("WeeklyDetail"));
			}
			_PrintHeader = false;


			r.SaveAndClose ();
		}

		public void TestPageThrowsPrintTableHeaders (object sender, EventArgs e)
		{
			Report r = (Report)sender;
			if (_PrintHeader) {
				r.PrintGroup (r.Group ("TableHeaderPrt1"));
				r.PrintGroup (r.Group ("TableHeaderPrt2"));
				r.PrintGroup (r.Group ("TableHeaderPrt3"));
			}
		}

		#region Utility Methods

		private void CopyAssetToFile (string assetName, string destinationPath)
		{
			if (!File.Exists (destinationPath)) {
				using (var readStream = Android.App.Application.Context.Assets.Open (assetName))
				using (var writeStream = File.OpenWrite (destinationPath))
				using (var reader = new BinaryReader (readStream))
				using (var writer = new BinaryWriter (writeStream)) {
					const int chunkSize = 1024;
					byte[] buffer = reader.ReadBytes (chunkSize);
					while (buffer != null && buffer.Length != 0) {
						writer.Write (buffer);
						buffer = reader.ReadBytes (chunkSize);
					}
				}
			}
		}

		#endregion

	}
}
