﻿using System;
using System.Collections.Generic;
using System.Collections;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.IO;
using System.Xml;

using Xfinium.Pdf;

namespace PDFReport
{
    public class Report : Contracts.IReportLayout
    {
        public delegate void HeaderPrintedHandler(object sender, EventArgs e);
        public event HeaderPrintedHandler HeaderPrinted;

        #region Constructors
        public Report()
        {
            Doc = new PdfFixedDocument();

            Groups = new Hashtable();
            Variables = new Dictionary<string, double>();

            //Items = new List<Item>();
            Doc.Pages.Add();
            Size = PageSize.A4_Portrait;

        }

        #endregion

        #region Properties
        public Dictionary<string, double> Variables { get; set; }

        public double SpaceRemaining
        {
            get
            {
                if (Footer != null)
                {
                    return (this.Height - Footer.Height) - CurrentY;
                }
                else
                {
                    return Height - CurrentY;
                }

            }
        }
        public string Filename { get; set; }
        public ItemGroup Header { get; set; }
        public ItemGroup Footer { get; set; }
        private PageSize _Size;
        public PageSize Size
        {
            get { return _Size; }
            set
            {
                switch (value)
                {
                    case PDFReport.PageSize.A3_Portrait:
                        Width = 297;
                        Height = 420;
                        break;
                    case PDFReport.PageSize.A3_Landscape:
                        Width = 420;
                        Height = 297;
                        break;

                    case PDFReport.PageSize.A4_Portrait:
                        Width = 210;
                        Height = 297;
                        break;
                    case PDFReport.PageSize.A4_Landscape:
                        Width = 297;
                        Height = 210;
                        break;
                    case PDFReport.PageSize.A5_Portrait:
                        Width = 148;
                        Height = 210;
                        break;
                    case PDFReport.PageSize.A5_Landscape:
                        Width = 210;
                        Height = 148;
                        break;
                    default:
                        Width = 210;
                        Height = 297;
                        break;
                }

                _Size = value;

                Doc.Pages[Doc.Pages.Count - 1].Height = Support.mmToPt(Height);
                Doc.Pages[Doc.Pages.Count - 1].Width = Support.mmToPt(Width);
            }
        }
        public double Width { get; set; }
        public double Height { get; set; }
        public double CurrentY { get; set; }

        public PdfFixedDocument Doc { get; set; }
        //private List<Item> Items { get; set; }
        public Hashtable Groups;

        #endregion

        #region Public Methods
        #region General

        public void AddGroup(ItemGroup g)
        {
            Groups[g.Name] = g;
        }

        public void AddCustomGroup(CustomGroup g)
        {
            g.ParentReport = this;

            switch (g.GroupType)
            {
                case CustomeGroupTypeEnum.Header:
                    this.Header = null;
                    this.Header = g;
                    break;
                case CustomeGroupTypeEnum.Footer:
                    this.Footer = null;
                    this.Footer = g;
                    break;
                default:
                    if (Groups.ContainsKey(g.Name))
                    {
                        Groups[g.Name] = g;
                    }
                    else
                    {
                        Groups.Add(g.Name, g);
                    }
                    break;
            }
        }

        public void AddCustomGroup(string layoutFile)
        {
            CustomGroup g = new CustomGroup(this, layoutFile);
            this.AddCustomGroup(g);
        }

        public ItemGroup Group(string name)
        {
            if (Groups[name] != null)
            {
                return (ItemGroup)Groups[name];
            }
            else
            {
                return null;
            }
        }

        public void AddHeader()
        {
            Header = new ItemGroup(this, "Header");
        }

        public void AddFooter()
        {
            Footer = new ItemGroup(this, "Footer");
        }
        public void NewPage()
        {

            if (Footer != null)
            {
                PrintFooter();
            }

            Doc.Pages[Doc.Pages.Count - 1].Graphics.CompressAndClose();

            Doc.Pages.Add();
            Doc.Pages[Doc.Pages.Count - 1].Height = Support.mmToPt(Height);
            Doc.Pages[Doc.Pages.Count - 1].Width = Support.mmToPt(Width);

            CurrentY = 0;
            PrintHeader();
        }

        public void SaveAndClose()
        {

            PrintFooter();
            Doc.Pages[Doc.Pages.Count - 1].Graphics.CompressAndClose();
            Doc.Save(Filename);
        }

        public void SaveAndClose(ref MemoryStream ms)
        {

            PrintFooter();

            Doc.Pages[Doc.Pages.Count - 1].Graphics.CompressAndClose();
            Doc.Save(ms);
        }

        public void PrintHeader()
        {
            if (Header != null)
            {
                PrintGroup(Header);
            }
            if (HeaderPrinted != null)
            {
                HeaderPrinted(this, EventArgs.Empty);
            }
        }

        public void PrintFooter()
        {
            if (Footer != null)
            {
                PrintGroup(Footer);
            }
        }

        public void PrintGroup(ItemGroup g)
        {
            if (g.RelativeTo == PrintRelativeTo.CurrentPos)
            {
                if (SpaceRemaining < g.Height)
                {
                    NewPage();
                }
            }
            g.Top = CurrentY;
            g.Print();
            CurrentY = g.Bottom;
        }
        #endregion
        #region Layout files
        public void LoadLayout(Stream xmlFileStream)
        {
            XmlDocument Doc = new XmlDocument(); ;
            if (xmlFileStream != null)
            {
                try
                {
                    Doc.Load(xmlFileStream);
                    LoadLayout(Doc);
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void LoadLayout(string layoutFile)
        {
            XmlDocument Doc = new XmlDocument();

            if (!File.Exists(layoutFile))
            {
                throw new Exception(string.Format("File '{0}' could not be found", layoutFile));
            }

            try
            {
                Doc.Load(layoutFile);
                LoadLayout(Doc);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("File '{0}' could not be loaded: {1}", layoutFile, ex.Message));
            }
        }

        private void LoadLayout(XmlDocument Doc)
        {
            XmlNode Root;
            XmlAttribute Attr;
            XmlNode CurrentGroup;


            // Set up report properties
            Root = Doc.DocumentElement;
            #region Pagesize
            Attr = Root.Attributes["PageSize"];
            if (Attr != null)
            {
                switch (Attr.Value.ToString())
                {
                    case "A4_Landscape":
                        Size = PageSize.A4_Landscape;
                        break;
                    default:
                        Size = PageSize.A4_Portrait;
                        break;
                }
            }
            #endregion

            // get variables
            foreach (XmlNode Variable in Doc.SelectNodes("//Variables/*"))
            {
                string Name = "";
                double Value = 0;

                Attr = Variable.Attributes["Name"];
                if (Attr != null)
                {
                    Name = Attr.Value;
                }

                Attr = Variable.Attributes["Value"];
                if (Attr != null)
                {
                    if (!double.TryParse(Attr.Value,out Value))
                    {
                        throw new Exception(string.Format("Invalid value {0} for variable{1}", Attr.Value, Name));
                    }
                }

                Variables.Add(Name, Value);
            }

            CurrentGroup = Doc.SelectSingleNode("//Header");
            if (CurrentGroup != null)
            {
                //AddHeader();
                AddGroup(CurrentGroup);
            }

            CurrentGroup = Doc.SelectSingleNode("//Footer");
            if (CurrentGroup != null)
            {
                //AddFooter();
                AddGroup(CurrentGroup);
            }

            foreach (XmlNode g in Doc.SelectNodes("//Groups/*"))
            {
                AddGroup(g);
            }

        }

        private void AddGroup(XmlNode groupNode)
        {
            ItemGroup Group;
            switch (groupNode.Name)
            {
                case "Header":
                    AddHeader();
                    Group = Header;
                    break;
                case "Footer":
                    AddFooter();
                    Group = Footer;

                    break;
                default:
                    Group = ItemGroup.FromXml(this, groupNode);
                    AddGroup(Group);
                    break;
            }

            foreach (XmlNode CurrentItem in groupNode.ChildNodes)
            {

                switch (CurrentItem.Name)
                {
                    case "LineItem":
                        Group.AddItem(LineItem.FromXml(this,CurrentItem));
                        break;
                    case "ImageItem":
                        Group.AddItem(ImageItem.FromXml(this, CurrentItem));
                        break;
                    default:
                        Group.AddItem(TextItem.FromXml(this,CurrentItem));
                        break;
                }
            }
        }
        #endregion
        #endregion

        #region Static Methods
        public void MergeDocuments(MemoryStream[] docs, bool removePages)
        {
            if(this.Doc == null)
                this.Doc = new PdfFixedDocument();

            if (removePages)
                this.Doc.Pages.Clear();
            //TODO: Investigate possibility of memory leak.
            foreach(MemoryStream ms in docs)
            {
                this.Doc.AppendFile(ms);
            }

        }
        #endregion
    }
}
