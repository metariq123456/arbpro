﻿using System;
using System.Drawing;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Xfinium.Pdf;
using Xfinium.Pdf.Graphics;
using Xfinium.Pdf.Graphics.Text;

namespace PDFReport
{
    public partial class ImageItem : Item
    {
        #region properties
        private string _ImagePath;
        public string ImagePath
        {
            get { return _ImagePath; }
            set
            {
                Image = null;
                _ImagePath = value;
            }
        }
        public override double Height { get; set; }
        public override double Width { get; set; }

        protected PdfImage Image { get; set; }
        #endregion

        #region Constructors

        public ImageItem(string imagePath, double x, double y, double width, double height, string name = null)
            : base(x, y, width, name)
        {
            Height = height;
            Width = width;
            ImagePath = imagePath;
        }

        public ImageItem(double x, double y, double width, double height, string name = null)
            : base(x, y, width, name)
        {
            Height = height;
            Width = width;
            ImagePath = "";
        }

        #endregion

        #region Public Methods
        public void SetImage(Byte[] imageArray)
        {
            SetImageImplementation(imageArray);
        }

        public void SetImage(Byte[] imageArray, ResizePDFImage ePDFImageResizing)
        {
            SetImageImplementation(imageArray, ePDFImageResizing);
        }

        public void SetImage(string imagePath)
        {
            SetImageImplementation(imagePath);
        }

        public void SetImage(string imagePath, ResizePDFImage ePDFImageResizing)
        {
            SetImageImplementation(imagePath, ePDFImageResizing);
        }

        private Byte[] _ImageBytes;
        public Byte[] GetImageBytes()
        {
            return _ImageBytes;
        }
        #endregion

        #region Partial method implementation
        /// <summary>
        /// Partial method to enable cross platform image manipulation
        /// </summary>
        /// <param name="imageArray">Byte array of the image data</param>
        /// <param name="ePDFImageResizing">Resizing enum</param>
        partial void SetImageImplementation(Byte[] imageArray, ResizePDFImage ePDFImageResizing);
        partial void SetImageImplementation(Byte[] imageArray);
        partial void SetImageImplementation(string imagePath);
        partial void SetImageImplementation(string imagePath, ResizePDFImage ePDFImageResizing);
        #endregion

        #region Abstract method implementation
        public override void Print(PrintRelativeTo relativeTo)
        {
            switch (relativeTo)
            {
                case PrintRelativeTo.CurrentPos:
                    Print(Support.mmToPt(X), Support.mmToPt(Y + ParentReport.CurrentY),Support.mmToPt(Width), Support.mmToPt(Height));
                    break;
                default:
                    Print(Support.mmToPt(X), Support.mmToPt(Y),Support.mmToPt(Width), Support.mmToPt(Height));
                    break;
            }
        }

        public override void CalculateRectangle()
        {
            Rectangle.BottomRight.Y = Rectangle.TopLeft.Y + Math.Abs(Height);
        }
        #endregion

        #region Private Methods
        private void Print(double x, double y, double width, double height)
        {
            try
            {
                if (x + width > 2450)
                    width = 2450 - x;

                if (y + height > 3480)
                    height = 3480 - y;

                if (Image == null)
                {
                    SetImageImplementation(ImagePath);
                    if (Image == null)
                        Image = new PdfImage(ImagePath);
                }

                ParentReport.Doc.Pages[ParentReport.Doc.Pages.Count - 1].Graphics.DrawImage(Image, x, y, width, height);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error printing image: {0}",ex.ToString());
            }
        }

        private void SetImageResizing(decimal imageWidth, decimal imageHeight, ResizePDFImage ePDFImageResizing)
        {
            // Platform-free resizing
            // Get current imgBox size
            decimal nOrigBoxWidth = (Decimal)this.Width;
            decimal nOrigBoxHeight = (Decimal)this.Height;

            // Check Resize enum
            switch (ePDFImageResizing)
            {
                case ResizePDFImage.KeepImage:
                    //// Resize imgBox to image so as to keep image sizing (and aspect ratio)
                    // Only need to resize the imgBox 
                    // to match the Image size
                    // Image is set after this switch
                    this.Width = (Double)imageWidth;
                    this.Height = (Double)imageHeight;

                    break;

                case ResizePDFImage.LoseRatioWithinBox:
                    //// Stretch image, losing aspect ration, to fit into imgBox
                    // Do nothing here as just setting the image
                    // (which is done after this switch)
                    // will perform this functionality
                    break;

                case ResizePDFImage.KeepRatioWithinBox:
                    //// Resize the image to fit into the box
                    //// whilst keeping the aspect ratio of the image
                    if (nOrigBoxWidth != imageWidth || nOrigBoxHeight != imageHeight)
                    {
                        decimal nOrigRatioWByH = imageWidth / imageHeight;
                        decimal nOrigRatioHByW = imageHeight / imageWidth;
                        decimal nNewWidth = nOrigBoxWidth;
                        decimal nNewHeight = nOrigBoxHeight;

                        // Set the ratio as per original box height
                        // This may leave the width wider than the original box width
                        if (nOrigBoxHeight < nOrigBoxWidth)
                        {
                            nNewWidth = nOrigBoxHeight * nOrigRatioWByH;
                            this.Width = (Double)nNewWidth;
                        }
                        else if (nOrigBoxHeight > nOrigBoxWidth)
                        {
                            nNewHeight = nOrigBoxWidth * nOrigRatioHByW;
                            this.Height = (Double)nNewHeight;
                        }

                        // Set the ratio as per original box width, if necessary,
                        //  to fit the image entirely within the box
                        decimal nCurrBoxWidth = nNewWidth;
                        decimal nCurrBoxHeight = nNewHeight;

                        if (nCurrBoxWidth > nOrigBoxWidth)
                        {
                            decimal nNewOrigWidthRatio = nOrigBoxWidth / nCurrBoxWidth;
                            nNewWidth = nCurrBoxWidth * nNewOrigWidthRatio;
                            this.Width = (Double)nNewWidth;
                            nNewHeight = nNewWidth * nOrigRatioHByW;
                            this.Height = (Double)nNewHeight;
                        }
                    }
                    break;
            }


        }

        #endregion

        #region Static Methods
        public static ImageItem FromXml(Dictionary<string, double> Variables, XmlNode node)
        {

            XmlAttribute Attr;
            ImageItem ReturnValue;

            // Note that all properties are given default values.
            double X = 0;
            double Y = 0; ;
            double Width = 0;
            double Height = 0;
            string Name = Guid.NewGuid().ToString();
            string ImagePath = string.Empty;

            Attr = node.Attributes["X"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "X coordinate", Attr.Value.ToString(), out X);
            }

            Attr = node.Attributes["Y"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "Y coordinate", Attr.Value.ToString(), out Y);
            }

            Attr = node.Attributes["Width"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "Width", Attr.Value.ToString(), out Width);
            }

            Attr = node.Attributes["Height"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "Height", Attr.Value.ToString(), out Height);
            }

            Attr = node.Attributes["Name"];
            if (Attr != null)
            {
                Name = Attr.Value.ToString();
            }

            Attr = node.Attributes["URL"];
            if (Attr != null)
            {
                ImagePath = Attr.Value.ToString();
            }

            if (ImagePath.Length > 0)
                ReturnValue = new ImageItem(ImagePath,X, Y, Width, Height, Name);
            else
                ReturnValue = new ImageItem(X, Y, Width, Height, Name);

            return ReturnValue;
        }

        public static ImageItem FromXml(Report ParentReport, XmlNode node)
        {
            return FromXml(ParentReport.Variables, node);
            
        }
        #endregion
    }
}
