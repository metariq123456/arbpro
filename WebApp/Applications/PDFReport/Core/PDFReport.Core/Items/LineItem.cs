﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

using Xfinium.Pdf;
using Xfinium.Pdf.Graphics;
using Xfinium.Pdf.Graphics.Text;

namespace PDFReport
{
    public class LineItem : Item
    {
        #region Members
        bool _HasVerticalComponent = false;
        #endregion

        #region properties
        private PdfRgbColor PDFColor { get; set; }

        //**************************************
        // Colour
        //**************************************

        private string _StrColour = "Black";
        public string Colour
        {
            get
            {
                return _StrColour;
            }
            set
            {
                PropertyInfo Info;
                try
                {
                    _StrColour = value;
                    Info = typeof(PdfRgbColor).GetProperty(value);
                    if (Info != null)
                    {
                        PDFColor = (PdfRgbColor)Info.GetValue(null, null);
                    }
                    //PdfRgbColor colorValue = (PdfRgbColor)Enum.Parse(typeof(PdfRgbColor), value);
                    //if (Enum.IsDefined(typeof(PdfRgbColor), value) | value.ToString().Contains(","))
                    //    PDFColor = colorValue;
                    //else
                    //    PDFColor = PdfRgbColor.Black;
                }
                catch (ArgumentException ex)
                {
                    PDFColor = PdfRgbColor.Black;
                }
            }
        }

        public double PenSize { get; set; }
        #endregion

        #region Constructors

        public LineItem(double penSize, double x, double y, double width, string name = null)
            : base(x, y, width, name)
        {
            PenSize = penSize;
        }

        public LineItem(double penSize, double x, double y, double width,double height, string name = null)
            : base(x, y, width,height, name)
        {
            _HasVerticalComponent = true;
            PenSize = penSize;
        }
        
        #endregion

        #region Public Methods
        #endregion

        #region Abstract method implementation
        public override void Print(PrintRelativeTo relativeTo)
        {
            PdfPen Pen = new PdfPen(PDFColor, PenSize);
            switch (relativeTo)
            {
                case PrintRelativeTo.CurrentPos:
                    ParentReport.Doc.Pages[ParentReport.Doc.Pages.Count - 1].Graphics.DrawLine(Pen, Support.mmToPt(X), Support.mmToPt(Y + ParentReport.CurrentY),
                                                                                                   Support.mmToPt(X + Width), Support.mmToPt(Y + ParentReport.CurrentY + Height));
                    break;
                default:
                    ParentReport.Doc.Pages[ParentReport.Doc.Pages.Count - 1].Graphics.DrawLine(Pen, Support.mmToPt(X), Support.mmToPt(Y),
                                                                                                   Support.mmToPt(X + Width), Support.mmToPt(Y + Height));

                    break;
            }
        }

        public override void CalculateRectangle()
        {
            if (_HasVerticalComponent)
            {
                Rectangle = new Rect(new Point(X, Y), Width, Height);
            }
            else
                Rectangle.BottomRight.Y = Rectangle.TopLeft.Y;
        }
        #endregion

        #region Static Methods
        public static LineItem FromXml(Dictionary<string, double> Variables, XmlNode node)
        {

            XmlAttribute Attr;
            LineItem ReturnValue;

            // Note that all properties are given default values.
            double X = 0;
            double Y = 0; ;
            double Width = 0;
            double Height = 0;
            double PenSize = 1.0;
            GroupPositionHorizontal RelativeHorizontalPos = GroupPositionHorizontal.Adsolute;
            GroupPositionVertical RelativeVerticalPos = GroupPositionVertical.Adsolute;
            string Name = Guid.NewGuid().ToString();
            string Colour = "Black";

            Attr = node.Attributes["Name"];
            if (Attr != null)
            {
                Name = Attr.Value.ToString();
            }

            Attr = node.Attributes["X"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "X coordinate", Attr.Value.ToString(), out X);
            }

            Attr = node.Attributes["Y"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "Y coordinate", Attr.Value.ToString(), out Y);
            }

            Attr = node.Attributes["Width"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "Width", Attr.Value.ToString(), out Width);
            }

            Attr = node.Attributes["Height"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "Height", Attr.Value.ToString(), out Height);
            }

            Attr = node.Attributes["Name"];
            if (Attr != null)
            {
                Name = Attr.Value.ToString();
            }

            Attr = node.Attributes["Colour"];
            if (Attr != null)
            {
                Colour = Attr.Value.ToString();
            }
            Attr = node.Attributes["PenSize"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "PenSize", Attr.Value.ToString(), out PenSize);
            }
            Attr = node.Attributes["GroupPositionVertical"];
            if (Attr != null)
            {
                switch (Attr.Value.ToString())
                {
                    case "Float":
                        RelativeVerticalPos = GroupPositionVertical.Float;
                        break;
                    default:
                        RelativeVerticalPos = GroupPositionVertical.Adsolute;
                        break;
                }
            }
            Attr = node.Attributes["GroupPositionHorizontal"];
            if (Attr != null)
            {
                switch (Attr.Value.ToString())
                {
                    case "Float":
                        RelativeHorizontalPos = GroupPositionHorizontal.Float;
                        break;
                    default:
                        RelativeHorizontalPos = GroupPositionHorizontal.Adsolute;
                        break;
                }
            }

            ReturnValue = new LineItem(PenSize, X, Y, Width,Height, Name);
            ReturnValue.Colour = Colour;
            ReturnValue.RelativeHorizontalPos = RelativeHorizontalPos;
            ReturnValue.RelativeVerticalPos = RelativeVerticalPos;
            return ReturnValue;
        }


        public static LineItem FromXml(Report ParentReport,XmlNode node)
        {
            return FromXml(ParentReport.Variables, node);
        }
        #endregion
    }
}
