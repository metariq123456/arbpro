﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Xfinium.Pdf;
using Xfinium.Pdf.Graphics;
using Xfinium.Pdf.Graphics.Text;

namespace PDFReport
{
    public class TextItem : Item
    {
        #region Enums
        public enum ItemType
        {
            Text,
            PageNumber
        }
        #endregion

        #region properties
        private PdfRgbColor PDFColor { get; set; }

        //**************************************
        // Colour
        //**************************************
        private string _StrColour = "Black";
        public string Colour
        {
            get
            {
                return _StrColour;
            }
            set
            {
                PropertyInfo Info;
                try
                {
                    _StrColour = value;
                    Info = typeof(PdfRgbColor).GetProperty(value);
                    if (Info != null)
                    {
                        PDFColor = (PdfRgbColor)Info.GetValue(null, null);
                    }
                }
                catch (ArgumentException ex)
                {
                    _StrColour = "Black";
                    PDFColor = PdfRgbColor.Black;
                }
            }
        }

        public override double Height
        {
            get
            {
                double tmp = Support.PtTomm(PdfTextEngine.GetStringHeight(Text, Font, Support.mmToPt(Width)));
                if(base.Height < tmp)
                    return tmp;
                else
                    return base.Height;
            }
            set
            {
                base.Height = value;
            }
        }

        private PdfStringHorizontalAlign HorizontalAlign { get; set; }
        public string HAlign
        {
            get { return HorizontalAlign.ToString(); }
            set
            {
                try
                {
                    PdfStringHorizontalAlign alignValue = (PdfStringHorizontalAlign)Enum.Parse(typeof(PdfStringHorizontalAlign), value);
                    if (Enum.IsDefined(typeof(PdfStringHorizontalAlign), value) | value.ToString().Contains(","))
                        HorizontalAlign = alignValue;
                    else
                        HorizontalAlign = PdfStringHorizontalAlign.Left;
                }
                catch (ArgumentException)
                {
                    HorizontalAlign = PdfStringHorizontalAlign.Left;
                }
            }
        }

        private PdfStringVerticalAlign VerticalAlign { get; set; }
        public string VAlign
        {
            get { return VerticalAlign.ToString(); }
            set
            {
                try
                {
                    PdfStringVerticalAlign alignValue = (PdfStringVerticalAlign)Enum.Parse(typeof(PdfStringVerticalAlign), value);
                    if (Enum.IsDefined(typeof(PdfStringVerticalAlign), value) | value.ToString().Contains(","))
                        VerticalAlign = alignValue;
                    else
                        VerticalAlign = PdfStringVerticalAlign.Middle;
                }
                catch (ArgumentException)
                {
                    VerticalAlign = PdfStringVerticalAlign.Middle;
                }
            }
        }


        public PdfStandardFont Font { get; set; }
        public string Text { get; set; }
        public ItemType SpecialType { get; set; }

        private string _FontName;
        public string FontName
        {
            get { return _FontName; }
            set
            {
                _FontName = value;
                SetFont();
            }
        }


        //**************************************
        // PointSize
        //**************************************
        private double _PointSize;
        public double PointSize
        {
            get { return _PointSize; }
            set
            {
                _PointSize = value;
                SetFont();
            }
        }


        #endregion

        #region Constructors

        public TextItem(string text, double x, double y, double width, string fontName, double pointSize, string name = null)
            : base(x, y, width, name)
        {
            HAlign = "Left";
            VAlign = "Middle";
            Text = text;
            FontName = fontName;
            PointSize = pointSize;
            SetFont();
        }

        public TextItem(string text, double x, double y, double width, string name = null)
            : base(x, y, width, name)
        {
            Rectangle.BottomRight.X = x + width;
            Text = text;
            FontName = "Helvetica";
            PointSize = 10;
            SetFont();
        }


        #endregion

        #region Public Methods
        private void SetFont()
        {
            try
            {
                if (FontName == null)
                {
                    FontName = "Helvetica";
                }
                if (PointSize == 0.0)
                {
                    PointSize = 10;
                }

                PdfStandardFontFace fontValue = (PdfStandardFontFace)Enum.Parse(typeof(PdfStandardFontFace), FontName);
                if (Enum.IsDefined(typeof(PdfStandardFontFace), FontName) | FontName.ToString().Contains(","))
                    Font = new PdfStandardFont(fontValue, PointSize);
                else
                    Font = new PdfStandardFont(PdfStandardFontFace.Helvetica, PointSize);
            }
            catch (ArgumentException)
            {
                Font = new PdfStandardFont(PdfStandardFontFace.Helvetica, PointSize);
            }
        }
        #endregion

        #region Abstract method implementation
        public override void Print(PrintRelativeTo relativeTo)
        {
            PdfStringLayoutOptions slo = new PdfStringLayoutOptions();
            switch (relativeTo)
            {
                case PrintRelativeTo.CurrentPos:
                    slo.Y = Support.mmToPt(Rectangle.TopLeft.Y + ParentReport.CurrentY);
                    break;
                default:
                    slo.Y = Support.mmToPt(Rectangle.TopLeft.Y);
                    break;
            }

            Print(slo);

            if (ShouldPrintRectangle)
            {
                Rectangle.Print(ParentReport, relativeTo);
            }
        }

        public override void CalculateRectangle()
        {
            Rectangle.BottomRight.Y = Rectangle.TopLeft.Y + Support.PtTomm(PdfTextEngine.GetStringHeight(Text, Font, Support.mmToPt(Width)));
        }
        #endregion

        #region Private Methods
        private double Print(PdfStringLayoutOptions slo)
        {
            slo.X = Support.mmToPt(X);
            slo.Width = Support.mmToPt(Width);

            slo.HorizontalAlign = HorizontalAlign;
            slo.VerticalAlign = VerticalAlign;
            PdfStringAppearanceOptions sao = new PdfStringAppearanceOptions();
            sao.Brush = new PdfBrush(PDFColor);
            sao.Font = Font;

            switch (SpecialType)
            {
                case ItemType.Text:
                    if (Text == null)
                        Text = "";

                    ParentReport.Doc.Pages[ParentReport.Doc.Pages.Count - 1].Graphics.DrawString(Text, sao, slo);
                    break;
                case ItemType.PageNumber:
                    ParentReport.Doc.Pages[ParentReport.Doc.Pages.Count - 1].Graphics.DrawString(ParentReport.Doc.Pages.Count.ToString(), sao, slo);
                    break;
                default:
                    break;
            }
            return this.Height;
        }
        #endregion

        #region Static Methods
        public static TextItem FromXml(Dictionary<string, double> Variables, XmlNode node)
        {
            XmlAttribute Attr;
            TextItem ReturnValue;

            // Note that all properties are given default values.
            string Text = "";
            double X = 0;
            double Y = 0; ;
            double Width = 0;
            string Name = Guid.NewGuid().ToString();
            ItemType Type = ItemType.Text;
            GroupPositionHorizontal RelativeHorizontalPos = GroupPositionHorizontal.Adsolute;
            GroupPositionVertical RelativeVerticalPos = GroupPositionVertical.Adsolute;
            string FontName = "Helvetica";
            double PointSize = 10.0D;
            string HAlign = "Left";
            string VAlign = "Middle";
            string Colour = "Black";
            bool PrintRect = false;

            Attr = node.Attributes["Text"];
            if (Attr != null)
            {
                Text = Attr.Value.ToString();
            }

            Attr = node.Attributes["X"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "X", Attr.Value.ToString(), out X);
            }

            Attr = node.Attributes["Y"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "Y", Attr.Value.ToString(), out Y);
            }

            Attr = node.Attributes["Width"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "Width", Attr.Value.ToString(), out Width);
            }

            Attr = node.Attributes["Name"];
            if (Attr != null)
            {
                Name = Attr.Value.ToString();
            }

            Attr = node.Attributes["Colour"];
            if (Attr != null)
            {
                Colour = Attr.Value.ToString();
            }

            Attr = node.Attributes["Type"];
            if (Attr != null)
            {
                switch (Attr.Value.ToString())
                {
                    case "PageNumber":
                        Type = ItemType.PageNumber;
                        break;
                    default:
                        Type = ItemType.Text;
                        break;
                }
            }

            Attr = node.Attributes["Font"];
            if (Attr != null)
            {
                FontName = Attr.Value.ToString();
            }

            Attr = node.Attributes["PointSize"];
            if (Attr != null)
            {
                EvaluateVariable(Variables, "PointSize", Attr.Value.ToString(), out PointSize);
            }

            Attr = node.Attributes["Type"];
            if (Attr != null)
            {
                if (Attr.Value == "PageNumber")
                {
                    Type = ItemType.PageNumber;
                }
            }

            Attr = node.Attributes["HAlign"];
            if (Attr != null)
            {
                HAlign = Attr.Value.ToString();
            }

            Attr = node.Attributes["VAlign"];
            if (Attr != null)
            {
                VAlign = Attr.Value.ToString();
            }

            Attr = node.Attributes["GroupPositionVertical"];
            if (Attr != null)
            {
                switch (Attr.Value.ToString())
                {
                    case "Float":
                        RelativeVerticalPos = GroupPositionVertical.Float;
                        break;
                    default:
                        RelativeVerticalPos = GroupPositionVertical.Adsolute;
                        break;
                }
            }
            Attr = node.Attributes["GroupPositionHorizontal"];
            if (Attr != null)
            {
                switch (Attr.Value.ToString())
                {
                    case "Float":
                        RelativeHorizontalPos = GroupPositionHorizontal.Float;
                        break;
                    default:
                        RelativeHorizontalPos = GroupPositionHorizontal.Adsolute;
                        break;
                }
            }

            Attr = node.Attributes["PrintRectangle"];
            if(Attr != null)
            {
                PrintRect = Attr.Value.ToString().ToLower() == "true";
            }

            ReturnValue = new TextItem(Text, X, Y, Width, FontName, PointSize, Name);
            ReturnValue.HAlign = HAlign;
            ReturnValue.VAlign = VAlign;
            ReturnValue.SpecialType = Type;
            ReturnValue.Colour = Colour;
            ReturnValue.RelativeHorizontalPos = RelativeHorizontalPos;
            ReturnValue.RelativeVerticalPos = RelativeVerticalPos;
            ReturnValue.ShouldPrintRectangle = PrintRect;
            return ReturnValue;
        }

        public static TextItem FromXml(Report ParentReport, XmlNode node)
        {
            return FromXml(ParentReport.Variables, node);
        }
        #endregion
    }
}
