﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

using Xfinium.Pdf;
using Xfinium.Pdf.Graphics;
namespace PDFReport
{
    public abstract class Item
    {
        #region Members
        int _Id = -1;
        int _GroupId = -1;
        double _Height = 0;
        double _Width = 0;
        #endregion

        #region Properties
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public int GroupId
        {
            get { return _GroupId; }
            set { _GroupId = value; }
        }

        public string Name { get; set; }
        public Report ParentReport { get; set; }
        public Rect Rectangle { get; set; }
        public GroupPositionHorizontal RelativeHorizontalPos { get; set; }
        public GroupPositionVertical RelativeVerticalPos { get; set; }

        public bool ShouldPrintRectangle { get; set; }
        public double X { get { return Rectangle.TopLeft.X; } }
        public double Y { get { return Rectangle.TopLeft.Y; } }
        
        public virtual double Width 
        { 
            get 
            { 
                if(_Width <= 0)
                    return Rectangle.BottomRight.X - Rectangle.TopLeft.X; 
                else
                    return _Width; 
            } 
            set 
            { 
                _Width = value; 
            } 
        }

        public virtual double Height 
        { 
            get 
            { 
                if(_Height <= 0)
                    return Rectangle.BottomRight.Y - Rectangle.TopLeft.Y;
                else
                    return _Height; 
            } 
            set 
            { 
                _Height = value;
 
            } 
        }
        #endregion

        #region Constructors
        public Item(double x, double y, double width, double height, string name=null)
        {
            if (name != null)
            {
                Name = name;
            }
            else
            {
                Name = Guid.NewGuid().ToString();
            }
            InitialiseRectangle(x, y, width, height);
        }

        public Item(double x, double y, double width, string name = null)
        {
            if (name != null)
            {
                Name = name;
            }
            else
            {
                Name = Guid.NewGuid().ToString();
            }
            InitialiseRectangle(x, y, width);
        }
        #endregion

        #region Public Methods
        public static void EvaluateVariable(Dictionary<string, double> Variables, string description, string valueToEvaluate, out double value)
        {
            if (Variables.ContainsKey(valueToEvaluate))
            {
                value = Variables[valueToEvaluate];
            }
            else
            {
                if (!double.TryParse(valueToEvaluate, out value))
                {
                    throw new Exception(string.Format("Invalid value {0} for {1}", valueToEvaluate, description));
                }
            }
        }

        public static void EvaluateVariable(Report parentReport, string description, string valueToEvaluate,out double value)
        {
            if (parentReport.Variables.ContainsKey(valueToEvaluate))
            {
                value = parentReport.Variables[valueToEvaluate];
            }
            else
            {
                if (!double.TryParse(valueToEvaluate, out value))
                {
                    throw new Exception(string.Format("Invalid value {0} for {1}",valueToEvaluate,description));
                }
            }
        }
        public void PrintRectangle(PrintRelativeTo relativeTo)
        {
            Rectangle.Print(ParentReport, relativeTo);

            PdfPen Pen;
            Pen = new PdfPen(PdfRgbColor.Black, 0.5);
            //ParentReport.Doc.Pages[ParentReport.Doc.Pages.Count - 1].Graphics.DrawLine(Pen, new PdfPoint(Support.mmToPt(X), Support.mmToPt(Y)), new PdfPoint(Support.mmToPt(X) + Support.mmToPt(Width), Support.mmToPt(Y)));
            //ParentReport.Doc.Pages[ParentReport.Doc.Pages.Count - 1].Graphics.DrawLine(Pen, new PdfPoint(Support.mmToPt(X) + Support.mmToPt(Width), Support.mmToPt(Y)), new PdfPoint(X + Width, Y + Height));
            //ParentReport.Doc.Pages[ParentReport.Doc.Pages.Count - 1].Graphics.DrawLine(Pen, new PdfPoint(Support.mmToPt(X) + Support.mmToPt(Width), Support.mmToPt(Y) + Support.mmToPt(Height)), new PdfPoint(X, Y + Height));
            //ParentReport.Doc.Pages[ParentReport.Doc.Pages.Count - 1].Graphics.DrawLine(Pen, new PdfPoint(Support.mmToPt(X), Support.mmToPt(Y) + Support.mmToPt(Height)), new PdfPoint(Support.mmToPt(X), Support.mmToPt(Y)));
        }
        #endregion

        #region Private Methods
        private void InitialiseRectangle(double x, double y, double width, double height = 0)
        {
            if (height > 0)
                Rectangle = new Rect(new Point(x, y), width, height);
            else
                Rectangle = new Rect(x, y, width);

            _Width = width;
            _Height = height;

            ShouldPrintRectangle = false;
        }
        #endregion

        #region Abstract methods
        public abstract void CalculateRectangle();
        public abstract void Print(PrintRelativeTo r);
        //public abstract double PrintInGroup(double currentGroupY, double currentGroupX = 0);
        #endregion
    }
}
