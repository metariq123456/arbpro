﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Threading.Tasks;

using Xfinium.Pdf;

namespace PDFReport
{

    public class ItemGroup
    {

        #region Properties
        public int Id { get; set; }
        public string Name { get; set; }
        public double BottomPadding { get; set; }

        private double _Top;
        private double _Height;
        public virtual double Height
        {
            get
            {
                return _Height;
            }
            set 
            {
                _Height = value;
            }
        }

        public virtual double Bottom
        {
            get
            {
                double Origin;
                double ReturnValue = 0;
                switch (RelativeTo)
                {
                    case    PrintRelativeTo.Page:
                        Origin = 0;
                        break;
                    default:
                        if(ParentReport == null)
                            Origin = 0;
                        else
                            Origin = ParentReport.CurrentY;
                        break;
                }

                foreach (object i in Items.Values)
                {
                    (i as Item).CalculateRectangle();
                    if ((i as Item).Rectangle.BottomRight.Y + Origin > ReturnValue)
                    {
                        ReturnValue = (i as Item).Rectangle.BottomRight.Y + Origin;
                    }
                }

                Height = ReturnValue - Origin;
                //double calculatedHeight = ReturnValue - Origin;
                //if (Height < calculatedHeight)
                //{
                //    Height = calculatedHeight;
                //}

                return ReturnValue + BottomPadding;
            }
        }

        public double Top
        {
            get
            {
                
                //double Origin;
                //double ReturnValue = ParentReport.Height;
                //switch (RelativeTo)
                //{
                //    case PrintRelativeTo.Page:
                //        Origin = 0;
                //        break;
                //    default:
                //        Origin = ParentReport.CurrentY;
                //        break;
                //}

                //foreach (object i in Items.Values)
                //{
                //    if ((i as Item).Rectangle.TopLeft.Y + Origin < ReturnValue)
                //    {
                //        ReturnValue = (i as Item).Rectangle.TopLeft.Y + Origin;
                //    }
                //}

                //return ReturnValue;
                
                return _Top;
            }
            set
            {
                _Top = value;
            }
        }

        public PrintRelativeTo RelativeTo { get; set; }

        private Report _ParentReport;
        public Report ParentReport 
        {
            get
            {
                return _ParentReport;
            }
            set
            {
                _ParentReport = value;
                if (Items != null && Items.Count > 0)
                {
                    foreach (string k in Items.Keys)
                    {
                        ((Item)Items[k]).ParentReport = _ParentReport;

                    }
                }
            } 
        }
        protected  Hashtable Items { get; set; }
        //public List<Item> Items { get; set; }
        #endregion

        #region Constructors
        public ItemGroup(Report parentReport, string name)
        {
            Id = -1;
            Name = name;
            ParentReport = parentReport;
            RelativeTo = PrintRelativeTo.Page;

            //Items = new List<Item>();
            Items = new Hashtable();
        }
        #endregion

        #region Public methods

        public void ClearItems()
        {
            Items.Clear();
        }

        public void AddItem(Item f)
        {
            Items.Add(f.Name, f);
            //Items.Add(f);
            f.ParentReport = ParentReport;
            f.CalculateRectangle();
            if ((this.RelativeTo == PrintRelativeTo.Page || this.RelativeTo == PrintRelativeTo.PageBottom)&&(this.Height < f.Height))
            {
                this.Height = f.Height;
            }
        }

        public List<Item> GetAllItems()
        {
            List<Item> retVal = new List<Item>();
            foreach (Item itm in Items.Values)
            {
                retVal.Add(itm);
            }
            return retVal;
        }

        public TextItem FindTextItem(string name)
        {
            if (Items[name] != null)
                return (TextItem)Items[name];
            else return null;
        }

        public ImageItem FindImageItem(string name)
        {
            if (Items[name] != null)
                return (ImageItem)Items[name];
            else return null;
        }

        public Item FindItem(string name)
        {
            if (Items[name] != null)
                return (Item)Items[name];
            else return null;
        }

        public virtual void Print()
        {
           foreach (Item f in Items.Values)
           {
               f.Print(RelativeTo);
           }
        }
        #endregion

        #region Static Methods
        public static ItemGroup FromXml(Report parent,XmlNode node)
        {
            XmlAttribute Attr;
            ItemGroup ReturnValue;
            double BottomPadding = 0.0;

            // Note that all properties are given default values.
            string Name = "";
            PrintRelativeTo RelativeTo = PrintRelativeTo.CurrentPos;

            Attr = node.Attributes["Name"];
            if (Attr != null)
            {
                Name= Attr.Value.ToString();
            }

            Attr = node.Attributes["RelativeTo"];
            if (Attr != null)
            {
                switch (Attr.Value.ToString())
                {
                    case "Page":
                        RelativeTo = PrintRelativeTo.Page;
                        break;
                    default:
                        break;
                }
            }

            Attr = node.Attributes["BottomPadding"];
            if (Attr != null)
            {
                if (!double.TryParse(Attr.Value.ToString(), out BottomPadding))
                {
                    throw new Exception(string.Format("Invalid BottomPadding: {0}", Attr.Value.ToString()));
                }
            }

            ReturnValue = new ItemGroup(parent, Name);
            ReturnValue.RelativeTo = RelativeTo;
            ReturnValue.BottomPadding = BottomPadding;


            return ReturnValue;
        }
        #endregion

    }
}
