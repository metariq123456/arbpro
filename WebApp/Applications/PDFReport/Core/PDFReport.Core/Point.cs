﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFReport
{
    /// <summary>
    /// Holds an X and Y coordinate to define a point.
    /// </summary>
    /// <remarks>
    /// We use our own definition so that there is no dependency on System.Drawing which may not be cross-platform.
    /// </remarks>
    public class Point
    {
        #region Properties
        public double X { get; set; }
        public double Y { get; set; }
        #endregion

        #region Constructors
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }
        #endregion
    }
}
