﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xfinium.Pdf;
using Xfinium.Pdf.Graphics;

namespace PDFReport
{
    public class Rect
    {
        #region Properties
        public Point TopLeft { get; set; }
        public Point BottomRight { get; set; }

        public double Height
        {
            get
            {
                return BottomRight.Y - TopLeft.Y;
            }
        }

        public double Width
        {
            get
            {
                return BottomRight.X - TopLeft.X;
            }
        }
        #endregion

        #region Constructors
        public Rect(double x, double y, double width)
        {
            TopLeft = new Point(x, y);
            BottomRight = new Point(x + width, y);
        }

        public Rect(Point topLeft, double width, double height)
        {
            TopLeft = topLeft;
            BottomRight = new Point(topLeft.X + width, topLeft.Y + height);
        }

        public Rect(double x1, double y1,double x2, double y2)
        {
            TopLeft = new Point(x1, y1);
            BottomRight = new Point(x2, y2);
        }

        public void Print(Report rpt, PrintRelativeTo relativeTo)
        {
            //Carry out conversions now to save processing
            double tlX = Support.mmToPt(TopLeft.X);
            double tlY = Support.mmToPt(TopLeft.Y);
            double brX = Support.mmToPt(BottomRight.X);
            double brY = Support.mmToPt(BottomRight.Y);

            if (relativeTo != PrintRelativeTo.Page)
            {
                tlY += Support.mmToPt(rpt.CurrentY);
                brY += Support.mmToPt(rpt.CurrentY);
            }

            PdfPen Pen;
            Pen = new PdfPen(PdfRgbColor.Black,0.5);
            //Draw horizontal lines first
            rpt.Doc.Pages[rpt.Doc.Pages.Count - 1].Graphics.DrawLine(Pen,tlX,tlY,brX,tlY);
            rpt.Doc.Pages[rpt.Doc.Pages.Count - 1].Graphics.DrawLine(Pen,tlX,brY,brX,brY);
            //Draw vertical lines
            rpt.Doc.Pages[rpt.Doc.Pages.Count - 1].Graphics.DrawLine(Pen,tlX,tlY,tlX,brY);
            rpt.Doc.Pages[rpt.Doc.Pages.Count - 1].Graphics.DrawLine(Pen,brX,brY,brX,brY);

            
        }

        #endregion
    }
}
