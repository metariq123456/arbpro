﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;

namespace PDFReport
{
    public class CustomGroup : ItemGroup, Contracts.IReportLayout
    {
        #region Members
        private double _GroupHeight = 0;
        private double _BottomPadding = 0.0;
        private Dictionary<string, double> _Variables = null;
        #endregion

        #region Properties
        /// <summary>
        /// Variables to be applied to the group items
        /// </summary>
        public Dictionary<string, double> Variables 
        {
            get
            {
                 if (_Variables == null)
                    _Variables = new Dictionary<string, double>();

                return _Variables;
            }
            set
            {
                _Variables = value;
            }
        }

        /// <summary>
        /// Overridden property to enable a user to specify a minimum height for the header.
        /// If the value is less than the size of the collection of items the size of the collection is used. 
        /// </summary>
        public override double Height
        {
            get
            {
                return _GroupHeight;
            }
            set
            {
                _GroupHeight = value;
            }
        }

        /// <summary>
        /// Property accessor to evaluate the type of group
        /// </summary>
        public CustomeGroupTypeEnum GroupType
        {
            get
            {
                if (this.Name.ToLower() == "header")
                    return CustomeGroupTypeEnum.Header;
                else if (this.Name.ToLower() == "footer")
                    return CustomeGroupTypeEnum.Footer;
                else
                    return CustomeGroupTypeEnum.Other;
            }
        }

        public override void Print()
        {
            if (ParentReport == null)
                throw new Exception("No report set to print group to");

            if (RelativeTo == PrintRelativeTo.PageBottom || this.GroupType == CustomeGroupTypeEnum.Footer)
            {
                //The group is printed in relation to the bottom of the page
                if (ParentReport != null)
                    ParentReport.CurrentY = ParentReport.Height - this.Height;
            }

            foreach (Item f in base.Items.Values)
            {
                f.Print(PrintRelativeTo.CurrentPos);
            }
        }

        public override double Bottom
        {
            get
            {
                double bottom = base.Bottom;
                double forceBottom = base.Top + Height;
                if (bottom < forceBottom)
                    return (forceBottom);
                else
                    return bottom;
            }
        }

        #endregion

        #region Constructors
        public CustomGroup(string name, double height): base(null, name) 
        {
            this.Height = height;
        }

        public CustomGroup(Report rpt, string layoutFile): base(rpt, "default") 
        {
            this.LoadLayout(layoutFile);
        }

        public CustomGroup(Report rpt, Stream xmlMemoryStream)
            : base(rpt, "default")
        {
            this.LoadLayout(xmlMemoryStream);
        }
        #endregion

        #region Public Methods
        public void LoadLayout(Stream xmlMemoryStream)
        {
            XmlDocument Doc = new XmlDocument(); ;
            if (xmlMemoryStream != null)
            {
                try
                {
                    Doc.Load(xmlMemoryStream);
                    LoadLayout(Doc);
                }
                catch (Exception ex)
                {

                }
            }
        }

        public void LoadLayout(string layoutFile)
        {
            XmlDocument Doc = new XmlDocument();

            if (!File.Exists(layoutFile))
            {
                throw new Exception(string.Format("File '{0}' could not be found", layoutFile));
            }

            try
            {
                Doc.Load(layoutFile);
                LoadLayout(Doc);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error loading layout: {0}", ex.ToString());
            }
        }

        private void LoadLayout(XmlDocument Doc)
        {
            XmlNode Root;
            XmlAttribute Attr;

            // Set up report properties
            Root = Doc.DocumentElement;
            //Set the group name to the root name of the layout file
            this.Name = Root.Name;
            if (this.GroupType == CustomeGroupTypeEnum.Footer)
            {
                RelativeTo = PrintRelativeTo.PageBottom;
            }
            //Set the min height of the group
            Attr = Root.Attributes["Height"];
            if (Attr != null)
            {
                double.TryParse(Attr.Value, out _GroupHeight);
            }

            Attr = Root.Attributes["RelativeTo"];
            if (Attr != null)
            {
                switch (Attr.Value.ToString())
                {
                    case "Page":
                        RelativeTo = PrintRelativeTo.Page;
                        break;
                    case "Group":
                        RelativeTo = PrintRelativeTo.Group;
                        break;
                    case "CurrentPos":
                        RelativeTo = PrintRelativeTo.CurrentPos;
                        break;
                    case "PageBottom":
                        RelativeTo = PrintRelativeTo.PageBottom;
                        break;
                    default:
                        RelativeTo = PrintRelativeTo.CurrentPos;
                        break;
                }
            }

            Attr = Root.Attributes["BottomPadding"];
            if (Attr != null)
            {
                if (!double.TryParse(Attr.Value.ToString(), out _BottomPadding))
                {
                    throw new Exception(string.Format("Invalid BottomPadding: {0}", Attr.Value.ToString()));
                }
            }

            // get variables
            foreach (XmlNode Variable in Doc.SelectNodes("//Variables/*"))
            {
                string Name = "";
                double Value = 0;

                Attr = Variable.Attributes["Name"];
                if (Attr != null)
                {
                    Name = Attr.Value;
                }

                Attr = Variable.Attributes["Value"];
                if (Attr != null)
                {
                    if (!double.TryParse(Attr.Value, out Value))
                    {
                        throw new Exception(string.Format("Invalid value {0} for variable{1}", Attr.Value, Name));
                    }
                }

                Variables.Add(Name, Value);
            }

            // get items
            foreach (XmlNode itmNode in Doc.SelectNodes("//Items/*"))
            {
                switch (itmNode.Name)
                {
                    case "LineItem":
                        this.AddItem(LineItem.FromXml(this.Variables, itmNode));
                        break;
                    case "ImageItem":
                        this.AddItem(ImageItem.FromXml(this.Variables, itmNode));
                        break;
                    default:
                        this.AddItem(TextItem.FromXml(this.Variables, itmNode));
                        break;
                }
            }
        }
        #endregion
    }
}
