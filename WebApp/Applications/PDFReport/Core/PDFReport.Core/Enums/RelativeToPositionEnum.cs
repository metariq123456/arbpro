﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFReport
{
    public enum PrintRelativeTo
    {
        Page,
        CurrentPos,
        PageBottom,
        Group
    }

    public enum GroupPositionVertical
    {
        Adsolute, //The y coordinate will be used to position
        Float     //The current y cordinate within the group will be used
    }

    public enum GroupPositionHorizontal
    {
        Adsolute, //The x coordinate will be used to position
        Float     //The current x cordinate within the group will be used
    }
}
