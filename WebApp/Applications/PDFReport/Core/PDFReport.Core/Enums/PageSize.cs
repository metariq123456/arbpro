﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFReport
{
    public enum PageSize
    {
        A5_Portrait,
        A5_Landscape,
        A4_Portrait,
        A4_Landscape,
        A3_Portrait,
        A3_Landscape
    }
}
