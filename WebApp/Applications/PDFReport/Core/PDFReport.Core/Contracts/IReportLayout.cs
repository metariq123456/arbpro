﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PDFReport.Contracts
{
    public interface IReportLayout
    {
        void LoadLayout(Stream xmlMemoryStream);
        void LoadLayout(string filename);
    }
}
