﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFReport
{
    class Support
    {
        public static double mmToPt(double mm)
        {
            // James got this original value from Xfinium
            //return mm * 2.845275591;
            // Sean got this new value from Google
            return mm * 2.834645669;
        }

        public static double PtTomm(double pt)
        {
            // James got this original value from Xfinium
            //return pt / 2.845275591;
            // Sean got this new value from Google
            return pt / 2.834645669;
        }
    }
}
