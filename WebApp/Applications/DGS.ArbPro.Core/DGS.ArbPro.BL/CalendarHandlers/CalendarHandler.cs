﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;
using DGS.ArbPro.DAL;

namespace DGS.ArbPro.BL
{
    public class CalendarHandler
    {
        /// <summary>
        /// Static method to delete a generic calendar event from the database
        /// </summary>
        /// <param name="databaseName">The name of the database to be accessed</param>
        /// <param name="appointmentId">The Id of the record to be deleted</param>
        /// <returns>Returns success status of operation</returns>
        public static bool DeleteGenericAppointment(string databaseName, int appointmentId)
        {
            DAL.Calendar dbContext = new Calendar(databaseName);
            return dbContext.DeleteGeneric(appointmentId);
        }

        /// <summary>
        /// Static method to delete a quote calendar event from the database
        /// </summary>
        /// <param name="databaseName">The name of the database to be accessed</param>
        /// <param name="appointmentId">The Id of the record to be deleted</param>
        /// <returns>Returns success status of operation</returns>
        public static bool DeleteQuoteAppointment(string databaseName, int appointmentId)
        {
            DAL.Calendar dbContext = new Calendar(databaseName);
            return dbContext.DeleteQuote(appointmentId);
        }
    }
}
