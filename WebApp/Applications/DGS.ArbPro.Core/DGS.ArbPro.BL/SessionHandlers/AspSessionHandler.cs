﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL.SessionHandlers
{
    public class AspSessionHandler
    {
        #region Mebers
        #endregion

        public static Types.User GetForEmail(string email)
        {
            DAL.User dbContext = new DAL.User(System.Configuration.ConfigurationManager.AppSettings["MasterDatabaseName"]);
            return dbContext.GetForEmail(email);
        }
    }
}
