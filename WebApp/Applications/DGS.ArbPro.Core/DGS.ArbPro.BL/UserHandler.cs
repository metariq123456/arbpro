﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UserHandler.cs" company="Microsec Ltd">
//   Copyright 2015 Microsec Ltd
// </copyright>
// <summary>
//   The user handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using log4net;
using System.Web.Configuration;
namespace DGS.ArbPro.BL
{
    using System;
    using System.Collections.Generic;

    using Microsec.Elastic;

    /// <summary>
    /// The user handler.
    /// </summary>
    public static class UserHandler
    {
        /// <summary>
        /// Static log for logging
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger("DGS.ArbPro.BL");

        #region Enums

        /// <summary>
        /// The result from sending a password reset email
        /// </summary>
        public enum SendPasswordResetResult
        {
            /// <summary>
            /// The email was sent OK
            /// </summary>
            Ok, 

            /// <summary>
            /// The user was not found.
            /// </summary>
            UserNotFound, 

            /// <summary>
            /// There was an error generating the Guid
            /// </summary>
            ErrorGeneratingGuid, 

            /// <summary>
            /// There was an error sending the email
            /// </summary>
            ErrorSendingEmail
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// The send password reset email.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static SendPasswordResetResult SendPasswordResetEmail(string userName, string baseUri)
        {
            Log.InfoFormat("Password reset email requested for {0}",userName);

            var userManager = new DAL.User("");
            var user = userManager.GetForEmail(userName);
            if (user == null)
            {
                Log.ErrorFormat("Password reset - user {0} not found",userName);
                return SendPasswordResetResult.UserNotFound;
            }

            var guid = userManager.GenerateResetGuid(user);
            if (guid == null)
            {
                Log.ErrorFormat("Password reset - error generating Guid for user {0}",userName);
                return SendPasswordResetResult.ErrorGeneratingGuid;
            }

            var fromAddress = WebConfigurationManager.AppSettings["PasswordResetFromEmail"]
                                 ?? "noreply@arbprosoftware.com";

            var emailBody = WebConfigurationManager.AppSettings["PasswordResetEmailBody"]
                     ?? "Please visit the following link to reset your password. [PasswordResetLink]";

            var fullUrl = baseUri + "/PasswordReset/PasswordReset.aspx?Id=" + guid;
            emailBody = emailBody.Replace("[PasswordResetLink]", fullUrl);

            var elasticUri = WebConfigurationManager.AppSettings["Elastic_BaseURI"];
            var elasticApiKey = WebConfigurationManager.AppSettings["Elastic_ApiKey"];
            var elasticUsername = WebConfigurationManager.AppSettings["Elastic_Username"];
            var client = new Microsec.Elastic.Client(elasticUri, elasticUsername, elasticApiKey);

            try
            {
                var emailResult = client.SendEmail(userName,
                "Password reset from Arb Pro", emailBody, null,
                fromAddress, "No reply at Arb Pro",
                fromAddress, "No repy at Arb Pro",
                new List<AttachmentInfo>()
                );
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Password reset email requested for {0} failed:{1}", userName,ex.ToString());
                return SendPasswordResetResult.ErrorSendingEmail;
            }

            Log.InfoFormat("Password reset email sent for {0} {1}", userName,fullUrl);
            return SendPasswordResetResult.Ok;
        }

        #endregion
    }
}