﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DGS.ArbPro.Types;
using DGS.ArbPro.DAL;

namespace DGS.ArbPro.BL
{
    public class RiskAssessmentHandler
    {
        public static Types.Risk CreateRiskAssessmentFromTemplate(string databaseName, int templateId, Types.Enums.RiskCreateFromObjectType idType, int id)
        {
            DAL.Risk dbContext = new DAL.Risk(databaseName);
            Types.Risk retVal = null;
            switch (idType)
            {
                case Types.Enums.RiskCreateFromObjectType.JobSite:
                    retVal = dbContext.CreateForJobSiteFromTemplate(templateId, id);
                    break;
                case Types.Enums.RiskCreateFromObjectType.Quote:
                    retVal = dbContext.CreateForQuoteFromTemplate(templateId, id);
                    break;
                case Types.Enums.RiskCreateFromObjectType.Work:
                    retVal = dbContext.CreateForWorkFromTemplate(templateId, id);
                    break;
                default:
                    retVal = dbContext.CreateFromDefaultTemplate(id);
                    break;
            }
            if (retVal == null)
                retVal = new Types.Risk();
            return retVal;
        }

        public static Types.Risk CreateRiskAssessmentFromTemplate(string databaseName, int templateId, int quoteId, int jobSiteId)
        {
            DAL.Risk dbContext = new DAL.Risk(databaseName);
            var r = dbContext.CreateForJobSiteAndQuote(templateId,jobSiteId,quoteId);
            if(r == null)
                r = new Types.Risk();

            return r;
        }

        public static Types.Risk RiskGetForQuoteAndJobSite(string databaseName, int quoteId, int jobSiteId)
        {
            DAL.Risk dbContext = new DAL.Risk(databaseName);
            return dbContext.GetForQuoteAndJobSiteId(quoteId, jobSiteId);
        }

        public static Types.Risk CreateRiskAssessmentFromTemplate(int siteId, int quoteId, int templateId, string databaseName)
        {
            DAL.Risk dbContext = new DAL.Risk(databaseName);
            Types.Risk retVal = null;

            retVal = dbContext.CreateForJobSiteAndQuote(templateId, siteId, quoteId);

            return retVal;
        }


        public static List<Types.RiskTemplateSearchResult> GetTemplateSearchResults(string databaseName)
        {
            DAL.RiskTemplateSearchResult dbContext = new DAL.RiskTemplateSearchResult(databaseName);
            return dbContext.GetRiskTemplateList();
        }

        public static List<Types.RescuePlan> RiskAreialRescuePlans(string databaseName)
        {
            DAL.RescuePlan dbContext = new DAL.RescuePlan(databaseName);
            return dbContext.GetList();
        }

        public static Types.Risk RiskAssessmentForQuoteId(string databaseName, int quoteId)
        {
            DAL.Risk dbContext = new DAL.Risk(databaseName);
            Types.Risk retVal = dbContext.GetForQuoteId(quoteId);
            if (retVal == null)
            {
                //TODO: Return a new riskassessment created from the default template
                //retVal = dbContext.CreateAssessmentForId(appId);
            }
            return retVal;
        }

        public static Types.Risk RiskGetForAppointmentId(string databaseName, int appId)
        {
            DAL.Risk dbContext = new DAL.Risk(databaseName);
            Types.Risk retVal = dbContext.GetForAppointmentId(appId);
            if (retVal == null)
            {
                //TODO: Return a new riskassessment created from the default template
                //retVal = dbContext.CreateAssessmentForId(appId);
            }
            return retVal;
        }

        public static Types.Risk RiskGetForQuoteId(string databaseName, int quoteId)
        {
            DAL.Risk dbContext = new DAL.Risk(databaseName);
            Types.Risk retVal = dbContext.GetForQuoteId(quoteId);
            if (retVal.Id < 1)
            {
                //TODO: Return a new riskassessment created from the default template
                //retVal = dbContext.CreateAssessmentForId(appId);
            }
            return retVal;
        }

        public static Types.Risk RiskGetForId(string databaseName, int riskId)
        {
            DAL.Risk dbContext = new DAL.Risk(databaseName);
            Types.Risk retVal = dbContext.GetForId(riskId);
            if (retVal == null)
            {
                //TODO: Return a new riskassessment created from the default template
                //retVal = dbContext.CreateAssessmentForId(appId);
            }
            return retVal;
        }

        public static Types.ClientSite GetAssessmentSite(string databaseName, int riskId)
        {
            DAL.ClientSite dbContext = new DAL.ClientSite(databaseName);
            Types.ClientSite retVal = dbContext.GetForRiskAssessmentId(riskId);
            return retVal;
        }

        public static List<Types.RiskSection> RiskSectionsGetForRiskId(string databaseName, int riskId)
        {
            DAL.RiskSection dbContext = new DAL.RiskSection(databaseName);
            List<Types.RiskSection> retVal = dbContext.GetForRiskAssessment(riskId);
            return retVal;
        }

        public static Types.RiskSection AssessmentSectionGetForId(string databaseName, int sectionId)
        {
            DAL.RiskSection dbContext = new DAL.RiskSection(databaseName);
            Types.RiskSection retVal = dbContext.GetForId(sectionId);
            return retVal;
        }

        public static List<Types.RiskAdditionalItem> OtherRisksGetForRiskId(string databaseName, int riskId)
        {
            DAL.RiskAdditionalItem dbContext = new DAL.RiskAdditionalItem(databaseName);
            List<Types.RiskAdditionalItem> retVal = dbContext.GetForRiskAssessment(riskId);
            return retVal;
        }

        public static Types.RiskAdditionalItem OtherRiskGetForId(string databaseName, int additionalRiskId)
        {
            if (additionalRiskId < 0)
            {
                return new Types.RiskAdditionalItem();
            }
            else
            {
                DAL.RiskAdditionalItem dbContext = new DAL.RiskAdditionalItem(databaseName);
                return dbContext.GetForId(additionalRiskId);
            }
        }
        public static bool RiskAssessmentUpdate(string databaseName, Types.Risk assessment, string curUsername)
        {
            DAL.Risk dbContext = new DAL.Risk(databaseName);
            int retVal = dbContext.Save(assessment);
            if (retVal > 0)
            {
                int update = dbContext.UpdateLastUpdatedBy(assessment.Id, curUsername);
            }
            return retVal > 0;
        }

        /// <summary>
        /// Handler method to save a risk assessment section and all of its collections
        /// </summary>
        /// <param name="databaseName">string: The name of the database to use</param>
        /// <param name="section">Types.RiskSection: The assessment section to be saved</param>
        /// <returns></returns>
        public static bool RiskAssessmentSectionUpdate(string databaseName, Types.RiskSection section, string curUsername)
        {
            DAL.RiskSection dbContext = new DAL.RiskSection(databaseName);
            int retVal = dbContext.SaveAll(section);
            if (retVal > 0)
            {
                DAL.Risk db = new DAL.Risk(databaseName);
                int update = db.UpdateLastUpdatedBy(section.RiskAssessmentId, curUsername);
            }
            return retVal > 0;
        }


        public static bool RiskOtherRiskUpdate(string databaseName, Types.RiskAdditionalItem item, string curUsername)
        {
            DAL.RiskAdditionalItem dbContext = new DAL.RiskAdditionalItem(databaseName);
            int retVal = dbContext.Save(item);
            if (retVal > 0)
            {
                DAL.Risk db = new DAL.Risk(databaseName);
                int update = db.UpdateLastUpdatedBy(item.RiskAssessmentId, curUsername);
            }
            return retVal > 0;
        }

        /// <summary>
        /// Static method to retreave a risk assessment from the database and check if the Id of the assessment is valid
        /// </summary>
        /// <param name="databaseName">string: The name of the database to be accessed</param>
        /// <param name="jobSiteId">int: The Id of the Job Site to get the assessment for</param>
        /// <returns>bool: True if the Id of the assessment is greater than 0</returns>
        public static bool ValidateAssessmentForJobSite(string databaseName, int jobSiteId)
        {
            DAL.Risk dbContext = new DAL.Risk(databaseName);
            var assessment = dbContext.GetForJobSite(jobSiteId);
            return assessment.Id > 0;
        }

        public static bool ValidateAssessmentForQuoteAndJobSite(string databaseName,int quoteId, int jobSiteId)
        {
            DAL.Risk dbContext = new DAL.Risk(databaseName);
            var assessment = dbContext.GetForQuoteAndJobSiteId(quoteId, jobSiteId);
            return assessment.Id > 0;
        }

        #region Risk Assessment Staff
        public static List<Types.RiskStaff> GetStaffForAssessment(string databaseName, int riskId)
        {
            DAL.RiskStaff dbContext = new DAL.RiskStaff(databaseName);
            return dbContext.GetForAssessment(riskId);
        }

        public static Types.RiskStaff GetRiskStaffMember(string databaseName, int id)
        {
            if (id < 1)
            {
                return new Types.RiskStaff();
            }
            else
            {
                DAL.RiskStaff dbContext = new DAL.RiskStaff(databaseName);
                return dbContext.GetForId(id);
            }
        }

        public static bool SaveRiskStaffMember(string databaseName, Types.RiskStaff obj, string curUsername)
        {
            DAL.RiskStaff dbContext = new DAL.RiskStaff(databaseName);
            int id = dbContext.Save(obj);
            if (id > 0)
            {
                DAL.Risk db = new DAL.Risk(databaseName);
                int update = db.UpdateLastUpdatedBy(obj.AssessmentId, curUsername);
            }
            return id > 0;
        }

        public static bool RemoveRiskStaffMember(string databaseName, int id)
        {
            //TODO: Add update method for removing a risk assessment staff member
            DAL.RiskStaff dbContext = new DAL.RiskStaff(databaseName);
            int removedId = dbContext.SetDeleted(id);
            return removedId > 0;
        }
        #endregion

        public static Types.RiskReferral GetRiskReferalForRiskId(string databaseName, int RiskId)
        {
            DAL.RiskReferral dbContext = new DAL.RiskReferral(databaseName);
            var Referral = dbContext.GetForRiskId(RiskId);
            return Referral;
        }

        public static bool RiskAssessmentReferralSave(string databaseName, Types.RiskReferral Ref, string curUsername)
        {
            DAL.RiskReferral dbContext = new DAL.RiskReferral(databaseName);
            var retVal = dbContext.Save(Ref);
            if (retVal > 0)
            {
                DAL.Risk db = new DAL.Risk(databaseName);
                int update = db.UpdateLastUpdatedBy(Ref.RiskAssessmentID, curUsername);
            }
            return retVal > 0;
        }
    }
}
