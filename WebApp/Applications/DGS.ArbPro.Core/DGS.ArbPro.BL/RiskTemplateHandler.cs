﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RiskTemplateHandler.cs" company="Microsec Ltd">
//   2016 Copyright Microsec Ltd
// </copyright>
// <summary>
//   The risk template handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using DGS.ArbPro.Types.Enums;

    using log4net;

    /// <summary>
    /// The risk template handler.
    /// </summary>
    public class RiskTemplateHandler
    {
        /// <summary>
        /// Static log for logging
        /// </summary>
        private static readonly ILog Log = LogManager.GetLogger("DGS.ArbPro.BL");

        /// <summary>
        /// The db name.
        /// </summary>
        private readonly string dtbName;

        /// <summary>
        /// Initializes a new instance of the <see cref="RiskTemplateHandler"/> class.
        /// </summary>
        /// <param name="databaseName"> The database name. </param>
        public RiskTemplateHandler(string databaseName)
        {
            this.dtbName = databaseName;
        }

        #region update

        /// <summary>
        /// The insert or update risk template and
        /// Map sections to it and map criteria to the sections
        /// </summary>
        /// <param name="risk"> The risk. </param>
        /// <returns> True or false </returns>
        public bool SaveTemplate(Types.Templates.Risk risk)
        {
            try
            {
                int riskId = this.InsertOrUpdateRiskTemplate(risk);

                return riskId > 0 && risk.TemplateSections.All(s => this.SaveSectionTemplate(riskId, s, s.TemplateCriteria));
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error in SaveTemplate - {0}", ex);
                return false;
            }
        }

        /// <summary>
        /// The insert or update risk section template and
        /// Map it to a risk and map criterions to the section
        /// </summary>
        /// <param name="riskId"> The risk id. </param>
        /// <param name="section"> The risk section object. </param>
        /// <param name="criterion"> List of the criterion objects. </param>
        /// <returns> True or false </returns>
        public bool SaveSectionTemplate(int riskId, Types.Templates.RiskSection section, List<Types.Templates.RiskCriterion> criterion)
        {
            try
            {
                int sectionId = this.InsertOrUpdateSectionTemplate(section);

                return sectionId > 0 && criterion.All(c => this.SaveAndMapCriterionTemplate(riskId, sectionId, c));
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error in SaveSectionTemplate - {0}", ex);
                return false;
            }
        }

        /// <summary>
        /// The insert or update risk criterion template and map it with a section.
        /// </summary>
        /// <param name="riskId"> The risk id. </param>
        /// <param name="sectionId"> The risk section id. </param>
        /// <param name="criterion"> The criterion object. </param>
        /// <returns> True or false </returns>
        public bool SaveAndMapCriterionTemplate(int riskId, int sectionId, Types.Templates.RiskCriterion criterion)
        {
            try
            {
                int criterionId = this.InsertOrUpdateCriterionTemplate(criterion);

                return criterionId > 0 && this.InsertTemplateRiskSectionMapping(riskId, sectionId, criterionId) > 0;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error in SaveCriterionTemplate - {0}", ex);
                return false;
            }
        }

        /// <summary>
        /// The insert or update risk section template and map it with a risk template.
        /// </summary>
        /// <param name="riskId"> The risk id. </param>
        /// <param name="section"> The section object. </param>
        /// <returns> True or false </returns>
        public bool SaveSectionAndMapToTemplate(int riskId, Types.Templates.RiskSection section)
        {
            try
            {
                int sectionId = this.InsertOrUpdateSectionTemplate(section);

                return sectionId > 0 && this.InsertTemplateRiskSectionMapping(riskId, sectionId, -1) > 0;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error in SaveSectionAndMapToTemplate - {0}", ex);
                return false;
            }
        }

        /// <summary>
        /// The insert or update risk criterion template and map it with a section.
        /// </summary>
        /// <param name="riskId"> The risk id. </param>
        /// <param name="sectionId"> The section id. </param>
        /// <returns> True or false </returns>
        public bool MapSectionToTemplate(int riskId, int sectionId)
        {
            try
            {
                return this.InsertTemplateRiskSectionMapping(riskId, sectionId, -1) > 0;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error in SaveSectionAndMapToTemplate - {0}", ex);
                return false;
            }
        }

        /// <summary>
        /// The insert or update risk template.
        /// </summary>
        /// <param name="risk"> The risk. </param>
        /// <returns> The Risk ID </returns>
        public int InsertOrUpdateRiskTemplate(Types.Templates.Risk risk)
        {
            DAL.Templates.Risk riskDb = new DAL.Templates.Risk(this.dtbName);

            if (risk.TemplateType == RiskTemplateTypeEnum.Default)
            {
                List<Types.Templates.Risk> riskAll = riskDb.GetAllTemplates();
                foreach (var r in riskAll)
                {
                    if (r.TemplateType == RiskTemplateTypeEnum.Default && r.Id != risk.Id)
                    {
                        r.TemplateType = RiskTemplateTypeEnum.General;
                        riskDb.Save(r);
                    }
                }
            }

            return riskDb.Save(risk);
        }

        /// <summary>
        /// The insert or update criterion template.
        /// </summary>
        /// <param name="criterion"> The criterion object. </param>
        /// <returns> The inserted ID </returns>
        public int InsertOrUpdateCriterionTemplate(Types.Templates.RiskCriterion criterion)
        {
            DAL.Templates.RiskCriterion criterionDb = new DAL.Templates.RiskCriterion(this.dtbName);
            return criterionDb.Save(criterion);
        }

        /// <summary>
        /// The insert or update section template.
        /// </summary>
        /// <param name="section"> The risk section object. </param>
        /// <returns> The inserted ID </returns>
        public int InsertOrUpdateSectionTemplate(Types.Templates.RiskSection section)
        {
            DAL.Templates.RiskSection sectionDb = new DAL.Templates.RiskSection(this.dtbName);
            return sectionDb.Save(section);
        }

        #endregion

        #region get

        /// <summary>
        /// Method to get a list of all risk templates
        /// </summary>
        /// <returns>A list of all risk templates</returns>
        public List<Types.Templates.Risk> GetAllRiskTemplates()
        {
            DAL.Templates.Risk riskDb = new DAL.Templates.Risk(this.dtbName);
            return riskDb.GetAllTemplates();
        }

        /// <summary>
        /// Method to get a risk template from it's id
        /// </summary>
        /// <param name="riskId">Risk template Id</param>
        /// <returns>A risk template</returns>
        public Types.Templates.Risk GetRiskTemplate(int riskId)
        {
            DAL.Templates.Risk riskDb = new DAL.Templates.Risk(this.dtbName);
            return riskDb.GetTemplate(riskId);
        }

        /// <summary>
        /// Method to get a list of all risk templates
        /// </summary>
        /// <returns>A list of all risk templates</returns>
        public List<Types.Templates.RiskSection> GetAllRiskSectionTemplates()
        {
            DAL.Templates.RiskSection riskDb = new DAL.Templates.RiskSection(this.dtbName);
            return riskDb.GetAllSectionTemplates();
        }

        /// <summary>
        /// Method to get a list of all risk sections for a template
        /// </summary>
        /// <param name="templateId">Risk template Id</param>
        /// <returns>A list risk sections</returns>
        public List<Types.Templates.RiskSection> GetSectionsForTemplateId(int templateId)
        {
            DAL.Templates.RiskSection riskDb = new DAL.Templates.RiskSection(this.dtbName);
            return riskDb.GetSectionsForTemplate(templateId);
        }

        /// <summary>
        /// Returns a single section based on it's id.
        /// </summary>
        /// <param name="riskId">Risk Id</param>
        /// <param name="templateId">Risk Section Id</param>
        /// <returns>A risk section</returns>
        public Types.Templates.RiskSection GetSectionForId(int riskId, int templateId)
        {
            DAL.Templates.RiskSection riskDb = new DAL.Templates.RiskSection(this.dtbName);
            return riskDb.GetSectionForId(riskId, templateId);
        }

        /// <summary>
        /// Method to get a list of all risk templates
        /// </summary>
        /// <returns>A list of all risk templates</returns>
        public List<Types.Templates.RiskCriterion> GetAllCriterionTemplates()
        {
            DAL.Templates.RiskCriterion riskDb = new DAL.Templates.RiskCriterion(this.dtbName);
            return riskDb.GetAllCriterionTemplates();
        }

        /// <summary>
        /// Method to get a list of risk criteria for a risk id
        /// </summary>
        /// <param name="riskId">Risk Id</param>
        /// <param name="sectionId">Section Id</param>
        /// <returns>A list of risk criteria</returns>
        public List<Types.Templates.RiskCriterion> GetCriterionForRiskId(int riskId, int sectionId)
        {
            DAL.Templates.RiskCriterion riskDb = new DAL.Templates.RiskCriterion(this.dtbName);
            return riskDb.GetForRiskId(riskId, sectionId);
        }

        /// <summary>
        /// Method to get a list of risk criteria for a section id
        /// </summary>
        /// <param name="sectionId">Risk section Id</param>
        /// <returns>A list of risk criteria</returns>
        public List<Types.Templates.RiskCriterion> GetCriterionForSectionId(int sectionId)
        {
            DAL.Templates.RiskCriterion riskDb = new DAL.Templates.RiskCriterion(this.dtbName);
            return riskDb.GetForSectionId(sectionId);
        }

        /// <summary>
        /// Method to get a risk criterion for criterion id
        /// </summary>
        /// <param name="criterionId">Risk criterion Id</param>
        /// <returns>A list of risk criteria</returns>
        public Types.Templates.RiskCriterion GetCriterionForId(int criterionId)
        {
            DAL.Templates.RiskCriterion riskDb = new DAL.Templates.RiskCriterion(this.dtbName);
            return riskDb.GetForId(criterionId);
        }

        #endregion

        #region mapping

        /// <summary>
        /// The insert template risk mapping.
        /// </summary>
        /// <param name="sectionMappingId"> The section mapping id. </param>
        /// <param name="riskId"> The risk id. </param>
        /// <returns> The mapping ID </returns>
        public int InsertTemplateRiskMapping(int sectionMappingId, int riskId)
        {
            DAL.Templates.Risk riskDb = new DAL.Templates.Risk(this.dtbName);
            return riskDb.InsertTemplateRiskMapping(sectionMappingId, riskId);
        }

        /// <summary>
        /// The remove template risk mapping.
        /// </summary>
        /// <param name="riskId"> The risk id. </param>
        /// <param name="sectionId"> The section id. </param>
        /// <returns> Successful or not </returns>
        public bool RemoveTemplateRiskMapping(int riskId, int sectionId)
        {
            DAL.Templates.Risk riskDb = new DAL.Templates.Risk(this.dtbName);
            return riskDb.RemoveTemplateRiskMapping(sectionId, riskId);
        }

        /// <summary>
        /// The insert template risk section mapping.
        /// </summary>
        /// <param name="riskId"> The risk id. </param>
        /// <param name="sectionId"> The section id. </param>
        /// <param name="criterionId"> The criterion id. </param>
        /// <returns> The mapping ID </returns>
        public int InsertTemplateRiskSectionMapping(int riskId, int sectionId, int criterionId)
        {
            DAL.Templates.Risk riskDb = new DAL.Templates.Risk(this.dtbName);
            return riskDb.InsertTemplateRiskSectionMapping(riskId, sectionId, criterionId);
        }

        /// <summary>
        /// The remove template risk section mapping.
        /// </summary>
        /// <param name="riskId"> The risk id. </param>
        /// <param name="sectionId"> The section id. </param>
        /// <param name="criterionId"> The criterion id. </param>
        /// <returns> Successful or not </returns>
        public bool RemoveTemplateRiskSectionMapping(int riskId, int sectionId, int criterionId)
        {
            DAL.Templates.Risk riskDb = new DAL.Templates.Risk(this.dtbName);
            return riskDb.RemoveTemplateRiskSectionMapping(riskId, sectionId, criterionId);
        }

        #endregion
    }
}
