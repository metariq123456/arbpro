﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL
{
    public class JobHandler
    {
        public static int SaveDocumentsHandler(String DatabaseName, Types.JobDocumentsObject obj)
        {
            int savedDocument = -1;
            var saveImages = new DAL.Document(DatabaseName);
            savedDocument = saveImages.Save(obj);
            obj.Id = savedDocument;
            return savedDocument;
        }

        public static int ToggleDefaultAttachmentForCompany(string databaseName, int DocId, bool DefaultAttachment)
        {
            DAL.Document dbContext = new DAL.Document(databaseName);
            return dbContext.ToggleDefaultAttachmentForJob(DocId, DefaultAttachment);
        }

        public static Types.JobDocumentsObject GetDocumentForID(String DatabaseName, int DocumentID)
        {
            var document_file = new DAL.Document(DatabaseName);
            return document_file.GetForId(DocumentID);
        }

     }
}
