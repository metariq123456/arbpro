﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL.ListItemHandlers
{
    public class WorkDescriptionHandler
    {
        #region Members
        #endregion

        #region Constructors
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public static List<string> GetList(string databaseName)
        {
            DAL.WorkDescription dbContext = new DAL.WorkDescription(databaseName);
            return dbContext.GetList();
        }

        public static List<Types.WorkDescription> GetItems(string databaseName)
        {
            DAL.WorkDescription dbContext = new DAL.WorkDescription(databaseName);
            return dbContext.GetItems();
        }

        public static bool SaveWorkDescription(string databaseName, Types.WorkDescription item)
        {
            DAL.WorkDescription dbContext = new DAL.WorkDescription(databaseName);
            return dbContext.Save(item);
        }

        public static bool Delete(string databaseName, int itemId)
        {
            DAL.WorkDescription dbContext = new DAL.WorkDescription(databaseName);
            return dbContext.Delete(itemId);
        }
        #endregion
        #region Private

        #endregion
        #endregion
    }
}
