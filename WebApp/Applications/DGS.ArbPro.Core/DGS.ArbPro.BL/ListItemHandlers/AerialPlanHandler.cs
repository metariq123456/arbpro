﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL.ListItemHandlers
{
    public class AerialPlanHandler
    {
        #region Static Methods
        /// <summary>
        /// Metod to get a specific Aerial rescue plan
        /// Returns a new object if the Id is less than 1
        /// </summary>
        /// <param name="id">The Id of the object to be returned</param>
        /// <param name="databaseName">The name of the database to be used</param>
        /// <returns>A Types.AerialRescuePlan object</returns>
        public static Types.AerialRescuePlan GetForId(int id, string databaseName)
        {
            if (id < 1)
                return new Types.AerialRescuePlan();
            else
            {
                DAL.AerialRescuePlan dbContext = new DAL.AerialRescuePlan(databaseName);
                return dbContext.Get(id);
            }
        }

        /// <summary>
        /// Method to get a list of all AerialRescuePlan objects from a specified database
        /// </summary>
        /// <param name="databaseName">The name of the database to be used</param>
        /// <returns>An instantiated list of type AerialRescuePlan. The list can be empty if there are no records or an error occured</returns>
        public static List<Types.AerialRescuePlan> GetAll(string databaseName)
        {
            DAL.AerialRescuePlan dbContext = new DAL.AerialRescuePlan(databaseName);
            return dbContext.GetAll();
        }

        /// <summary>
        /// Method to insert or update an entry in a specified database
        /// </summary>
        /// <param name="obj">The object instance to be inserted or updated</param>
        /// <param name="databaseName">The name of the database to be used</param>
        /// <returns>The Id of the affected row</returns>
        public static int Save(Types.AerialRescuePlan obj, string databaseName)
        {
            DAL.AerialRescuePlan dbContext = new DAL.AerialRescuePlan(databaseName);
            return dbContext.Save(obj);
        }
         
        /// <summary>
        /// Method to delete an instance from a specified database
        /// </summary>
        /// <param name="obj">The object to be deleted</param>
        /// <param name="databaseName">The name of the database to be used</param>
        /// <returns>True / False based on success of the operation, NOTE: will return true if the object did not exist</returns>
        public static bool Delete(Types.AerialRescuePlan obj, string databaseName)
        {
            DAL.AerialRescuePlan dbContext = new DAL.AerialRescuePlan(databaseName);
            return dbContext.Delete(obj);
        }

        /// <summary>
        /// Method to delete an instance from a specified database, based on it's Id
        /// </summary>
        /// <param name="id">The Id of te instance to be deleted</param>
        /// <param name="databaseName">The name of the database to be used</param>
        /// <returns>True / False based on success of the operation, NOTE: will return true if the object did not exist</returns>
        public static bool Delete(int id, string databaseName)
        {
            DAL.AerialRescuePlan dbContext = new DAL.AerialRescuePlan(databaseName);
            return dbContext.Delete(id);
        }
        #endregion
    }
}
