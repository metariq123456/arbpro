﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL.ListItemHandlers
{
    public class PickItemHandler
    {
        #region Members
        #endregion

        #region Constructors
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region String Lists
        public static List<string> GetHospitalList(string databaseName)
        {
            return GetList(databaseName, Types.Enums.PickItemTypeEnum.Hospital);
        }

        public static List<string> GetTitleList(string databaseName)
        {
            return GetList(databaseName, Types.Enums.PickItemTypeEnum.Title);
        }

        public static List<string> GetTownList(string databaseName)
        {
            return GetList(databaseName, Types.Enums.PickItemTypeEnum.Town);
        }

        public static List<string> GetCountyList(string databaseName)
        {
            return GetList(databaseName, Types.Enums.PickItemTypeEnum.County);
        }

        public static List<string> GetPermitTypeList(string databaseName)
        {
            return GetList(databaseName, Types.Enums.PickItemTypeEnum.PermitType);
        }
        public static List<string> GetCouncilList(string databaseName)
        {
            return GetList(databaseName, Types.Enums.PickItemTypeEnum.Council);
        }

        public static List<string> GetComplaintTypesList(string databaseName)
        {
            return GetList(databaseName, Types.Enums.PickItemTypeEnum.ComplaintType);
        }

        public static List<string> GetWasteTypesList(string databaseName)
        {
            return GetList(databaseName, Types.Enums.PickItemTypeEnum.WasteTypes);
        }

        public static List<string> GetLeadSourcesList(string databaseName)
        {
            return GetList(databaseName, Types.Enums.PickItemTypeEnum.LeadSource);
        }

        public static List<string> GetStaffTypeList(string databaseName)
        {
            return GetList(databaseName, Types.Enums.PickItemTypeEnum.StaffType);
        }
        #endregion
        #region Object Lists
        public static List<Types.PickItem> GetHospitalItems(string databaseName)
        {
            return GetItems(databaseName, Types.Enums.PickItemTypeEnum.Hospital);
        }

        public static List<Types.PickItem> GetTitleItems(string databaseName)
        {
            return GetItems(databaseName, Types.Enums.PickItemTypeEnum.Title);
        }

        public static List<Types.PickItem> GetTownItems(string databaseName)
        {
            return GetItems(databaseName, Types.Enums.PickItemTypeEnum.Town);
        }

        public static List<Types.PickItem> GetCountyItems(string databaseName)
        {
            return GetItems(databaseName, Types.Enums.PickItemTypeEnum.County);
        }

        public static List<Types.PickItem> GetCouncilItems(string databaseName)
        {
            return GetItems(databaseName, Types.Enums.PickItemTypeEnum.Council);
        }

        public static List<Types.PickItem> GetWasteTypesItems(string databaseName)
        {
            return GetItems(databaseName, Types.Enums.PickItemTypeEnum.WasteTypes);
        }

        public static List<Types.PickItem> GetLeadSourcesItems(string databaseName)
        {
            return GetItems(databaseName, Types.Enums.PickItemTypeEnum.LeadSource);
        }

        public static List<Types.PickItem> GetStaffTypeItems(string databaseName)
        {
            return GetItems(databaseName, Types.Enums.PickItemTypeEnum.StaffType);
        }
        #endregion

        public static bool SavePickItem(string databaseName, Types.PickItem item)
        {
            DAL.PickItem dbContext = new DAL.PickItem(databaseName);
            return dbContext.Save(item);
        }

        public static bool DeletePickItem(string databaseName, int itemId)
        {
            DAL.PickItem dbContext = new DAL.PickItem(databaseName);
            return dbContext.Delete(itemId);
        }

        public static List<Types.PickItem> GetItems(string databaseName, Types.Enums.PickItemTypeEnum itemType)
        {
            DAL.PickItem dbContext = new DAL.PickItem(databaseName);
            return dbContext.GetItemsForType(itemType);
        }
        #endregion
        #region Private
        private static List<string> GetList(string databaseName, Types.Enums.PickItemTypeEnum itemType)
        {
            DAL.PickItem dbContext = new DAL.PickItem(databaseName);
            return dbContext.GetValuesForType(itemType);
        }
        #endregion
        #endregion
    }
}
