﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL.ListItemHandlers
{
    public class ToolHandler
    {
        #region Members
        #endregion

        #region Constructors
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public static List<string> GetList(string databaseName)
        {
            DAL.Tool dbContext = new DAL.Tool(databaseName);
            return dbContext.GetList();
        }

        public static List<Types.Tool> GetItems(string databaseName)
        {
            DAL.Tool dbContext = new DAL.Tool(databaseName);
            return dbContext.GetItems();
        }

        public static bool SaveTool(string databaseName, Types.Tool item)
        {
            DAL.Tool dbContext = new DAL.Tool(databaseName);
            return dbContext.Save(item);
        }

        public static bool DeleteTool(string databaseName, int itemId)
        {
            DAL.Tool dbContext = new DAL.Tool(databaseName);
            return dbContext.Delete(itemId);
        }
        #endregion
        #region Private

        #endregion
        #endregion

    }
}
