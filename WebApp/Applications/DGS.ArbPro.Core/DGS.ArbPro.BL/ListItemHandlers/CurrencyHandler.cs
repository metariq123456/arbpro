﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL.ListItemHandlers
{
    public class CurrencyHandler
    {

                #region Members
        #endregion

        #region Constructors
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region String Lists

        #endregion
        #region Object Lists

        public static List<Types.Currency> GetItems(string databaseName)
        {
            DAL.Currency dbContext = new DAL.Currency(databaseName);
            return dbContext.GetItems();
        }
        public static List<Types.Currency> GetSelectedCurrency(string databaseName)
        {
            DAL.Currency dbContext = new DAL.Currency(databaseName);
            return dbContext.GetSelectedCurrencyItems();
        }
        public static bool SetSelected(string databaseName, int itemId)
        {
            DAL.Currency dbContext = new DAL.Currency(databaseName);
            return dbContext.SetSelected(itemId);
        }
        
        #endregion
        #region Private

        #endregion
        #endregion
        #endregion
    }
    }

