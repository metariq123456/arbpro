﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL.ListItemHandlers
{
    public class TreeNameHandler
    {
        #region Members
        #endregion

        #region Constructors
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public static List<string> GetCommonNames(string databaseName)
        {
            DAL.TreeName dbContext = new DAL.TreeName(databaseName);
            return dbContext.GetCommonList();
        }

        public static List<string> GetLatinNames(string databaseName)
        {
            DAL.TreeName dbContext = new DAL.TreeName(databaseName);
            return dbContext.GetLatinList();
        }

        public static List<string> GetDefaultList(string databaseName)
        {
            DAL.TreeName dbContext = new DAL.TreeName(databaseName);
            return dbContext.GetList();
        }

        public static List<Types.TreeName> GetItems(string databaseName)
        {
            DAL.TreeName dbContext = new DAL.TreeName(databaseName);
            return dbContext.GetItems();
        }

        public static bool SaveItem(string databaseName, Types.TreeName obj)
        {
            DAL.TreeName dbContext = new DAL.TreeName(databaseName);
            return dbContext.Save(obj);
        }

        public static bool DeleteItem(string databaseName, int itemId)
        {
            DAL.TreeName dbContext = new DAL.TreeName(databaseName);
            return dbContext.Delete(itemId);
        }
        #endregion
        #region Private

        #endregion
        #endregion
    }
}
