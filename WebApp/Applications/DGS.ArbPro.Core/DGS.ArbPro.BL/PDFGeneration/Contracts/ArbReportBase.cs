﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using log4net;
using PDFReport;
using DGS.ArbPro.DAL;
using DGS.ArbPro.BL.Enums;

namespace DGS.ArbPro.BL.PDFGeneration.Contracts
{
    public abstract class ArbReportBase
    {
        private ILog _Logger = LogManager.GetLogger("ArbProReports");

        #region Properties
        protected Report ArbReport { get; set; }
        protected string DatabaseName {get;set;}
        protected ILog Logger { get { return _Logger; } }
        #endregion

        #region Constructors
        public ArbReportBase(string dbName)
        {
            DatabaseName = dbName;
            ArbReport = new Report();
            ArbReport.HeaderPrinted += PrintTableHeaders;
            Logger.DebugFormat("Class: ArbReportBase, Method: Constructor, Message: Creating report base for client {0}", DatabaseName);
        }
        #endregion

        #region Abstract Methods
        public abstract string GeneratePdf(string rootLayoutPath, string rootOutputPath);
        protected abstract void PrintTableHeaders(object sender, EventArgs e);
        #endregion

        #region Protected Methods
        /// <summary>
        /// Base method to set the output file report name
        /// This method checks if a file of the same name exists before creating the report, and deletes it.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="filename"></param>
        protected void SetOutputFileName(string path, string filename)
        {
            ArbReport.Filename = Path.Combine(path, string.Format("{0}_{1}",DatabaseName.Replace("ArbPro",""),filename));
            if (File.Exists(ArbReport.Filename))
            {
                try
                {
                    File.Delete(ArbReport.Filename);
                    Logger.DebugFormat("Class: ArbReportBase, Method: SetOutputFileName, Message: Old version of {0} deleted", ArbReport.Filename);
                }
                catch (Exception ex)
                {
                    Logger.WarnFormat("Class: ArbReportBase, Method: SetOutputFileName, Message: Unable to delete file {0}, error - {1} ", ArbReport.Filename, ex.ToString());
                }
            }
        }


        /// <summary>
        /// Base method to load a report layout file and any custom goups
        /// </summary>
        /// <param name="filePath">The fully qualified file name of the layout file</param>
        /// <param name="useCustomGroups">Optional paramater to load custom groups from the database</param>
        protected virtual void LoadReportLayout(string filePath, bool useCustomGroups = false)
        {
            if (File.Exists(filePath))
            {
                try
                {
                    ArbReport.LoadLayout(filePath);
                    Logger.InfoFormat("Class: ArbReportBase, Method: LoadReportLayout, Message: Layout file {0} loaded", filePath);
                    if (useCustomGroups)
                        SetCustomGroups();
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("Class: ArbReportBase, Method: LoadReportLayout, Error: {0}", ex.ToString()); 
                    throw new Exception(string.Format("Report error: The report file {0} or Custom Groups cannot be loaded", filePath),ex);
                }
            }
            else
            {
                Logger.InfoFormat("Class: ArbReportBase, Method: LoadReportLayout, Message: Layout file {0} cannot be found.", filePath);
                throw new Exception(string.Format("File not found: The report file {0} cannot be found", filePath));
            }
        }

        /// <summary>
        /// Base method to enable loading of a reusable group layout file.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        protected virtual bool LoadGroupLayout(string filePath)
        {
            if (File.Exists(filePath))
            {
                try
                {
                    ArbReport.AddCustomGroup(filePath);
                    Logger.InfoFormat("Class: ArbReportBase, Method: LoadGroupLayout, Message: Layout file {0} loaded", filePath);
                    return true;
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("Class: ArbReportBase, Method: LoadGroupLayout, Error: {0}", ex.ToString());
                    return false;
                }
            }
            else
            {
                Logger.InfoFormat("Class: ArbReportBase, Method: LoadGroupLayout, Message: Layout file {0} cannot be found.", filePath);
                return false;
            }
        }
        #endregion
        #region Private Methods
        /// <summary>
        /// Method to load custom groups from the database and add them to the report
        /// </summary>
        protected void SetCustomGroups()
        {
            Logger.DebugFormat("Class: ArbReportBase, Method: SetCustomGroups, Message: Loading custom groups from database {0}", DatabaseName);
            List<CustomGroup> groups = CustomGroupDb.GetCustomGroupsForClient(DatabaseName);
            foreach (CustomGroup g in groups)
            {
                ArbReport.AddCustomGroup(g);
            }
        }
        #endregion
    }
}
