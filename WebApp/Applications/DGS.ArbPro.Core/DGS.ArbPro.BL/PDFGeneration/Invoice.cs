﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using log4net;
using DGS.ArbPro.BL.Enums;

namespace DGS.ArbPro.BL.PDFGeneration
{
    public class Invoice : Contracts.ArbReportBase
    {
        #region Members
        private InvoiceTypeEnum _InvoiceType = InvoiceTypeEnum.InvoiceLetter;
        private int _CustomerId = 0;
        private int _QuoteNo = 0;
        #endregion

        #region Constructors
        public Invoice(string databaseName, int customerId, int quoteNo,InvoiceTypeEnum type) : base(databaseName)
        {
            _InvoiceType = type;
            _CustomerId = customerId;
            _QuoteNo = quoteNo;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method to generate a specific type of invoice letter with dynamic header and footers
        /// for the client.
        /// </summary>
        /// <param name="type">Enum for the type of Invoice to be generated</param>
        /// <param name="databaseName">The client database name</param>
        /// <param name="rootLayoutFilePath">The directory containing the layout files for Invoice generation</param>
        /// <param name="rootOutputPath">The directory for Invoice generation to be output to</param>
        /// <returns>The fully qualified filename for the output</returns>
        public override string GeneratePdf(string rootLayoutPath, string rootOutputPath)
        {
            Logger.DebugFormat("Class: Invoice, Method: GeneratePdf, Message: Generate process started for report file {0}", rootLayoutPath);
            try
            {
                string pdfFilePath = "";
                switch (_InvoiceType)
                {
                    case InvoiceTypeEnum.InvoiceLetter:
                        pdfFilePath = GernerateInvoiceLetter(rootLayoutPath, rootOutputPath);
                        break;
                    case InvoiceTypeEnum.FirstReminder:
                        pdfFilePath = GernerateFirstReminder(rootLayoutPath, rootOutputPath);
                        break;
                    case InvoiceTypeEnum.SecondReminder:
                        pdfFilePath = GernerateSecondReminder(rootLayoutPath, rootOutputPath);
                        break;
                    case InvoiceTypeEnum.FinalReminder:
                        pdfFilePath = GernerateFinalReminder(rootLayoutPath, rootOutputPath);
                        break;
                    case InvoiceTypeEnum.FirstLegalAction:
                        pdfFilePath = GernerateFirstLegalAction(rootLayoutPath, rootOutputPath);
                        break;
                    case InvoiceTypeEnum.SecondLegalAction:
                        pdfFilePath = GernerateFirstLegalAction(rootLayoutPath, rootOutputPath);
                        break;
                }
                return pdfFilePath;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Class: Invoice, Method: GeneratePdf, Message: error - {0}",ex.ToString());
                throw new Exception("Unable to generate Invoice at this time.", ex);
            }
        }

        protected override void PrintTableHeaders(object sender, EventArgs e)
        {

        }
        #endregion

        #region Private Methods
        private string GernerateInvoiceLetter(string rootLayoutPath, string rootOutputPath)
        {
            //Setup the rport structure and output
            SetOutputFileName(rootOutputPath, "InvoiceLetter.pdf");
            LoadReportLayout(Path.Combine(rootLayoutPath, "InvoiceLetter.xml"), true);
            //Get the data from the data base


            return ArbReport.Filename;
        }
        
        private string GernerateFirstReminder(string rootLayoutPath, string rootOutputPath)
        {
            //Setup the report structure and output
            SetOutputFileName(rootOutputPath, "InvoiceFirstReminder.pdf");
            LoadReportLayout(Path.Combine(rootLayoutPath, "InvoiceFirstReminder.xml"), true);




            return ArbReport.Filename;
        }
        
        private string GernerateSecondReminder(string rootLayoutPath, string rootOutputPath)
        {
            //Setup the report structure and output
            SetOutputFileName(rootOutputPath, "InvoiceSecondReminder.pdf");
            LoadReportLayout(Path.Combine(rootLayoutPath, "InvoiceSecondReminder.xml"), true);




            return ArbReport.Filename;
        }
        
        private string GernerateFinalReminder(string rootLayoutPath, string rootOutputPath)
        {
            //Setup the report structure and output
            SetOutputFileName(rootOutputPath, "InvoiceFinalReminder.pdf");
            LoadReportLayout(Path.Combine(rootLayoutPath, "InvoiceFinalReminder.xml"), true);




            return ArbReport.Filename;
        }
        
        private string GernerateFirstLegalAction(string rootLayoutPath, string rootOutputPath)
        {
            //Setup the report structure and output
            SetOutputFileName(rootOutputPath, "InvoiceFirstLegalAction.pdf");
            LoadReportLayout(Path.Combine(rootLayoutPath, "InvoiceFirstLegalAction.xml"), true);




            return ArbReport.Filename;
        }
        
        private string GernerateSecondLegalAction(string databaseName, string rootLayoutPath, string rootOutputPath)
        {
            //Setup the report structure and output
            SetOutputFileName(rootOutputPath, "InvoiceSecondLegalAction.pdf");
            LoadReportLayout(Path.Combine(rootLayoutPath, "InvoiceSecondLegalAction.xml"), true);




            return ArbReport.Filename;
        }
        #endregion
    }
}
