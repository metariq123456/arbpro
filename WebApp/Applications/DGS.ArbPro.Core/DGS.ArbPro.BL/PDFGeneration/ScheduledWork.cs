﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using log4net;
using PDFReport;
using DGS.ArbPro.DAL;
using DGS.ArbPro.BL.Enums;

namespace DGS.ArbPro.BL.PDFGeneration
{
    public class ScheduledWork : Contracts.ArbReportBase
    {
        #region Members
        private int _CustomerId = 0;
        private int _QuoteNo = 0;
        #endregion

        #region Constructors
        public ScheduledWork(string databaseName, int customerId, int quoteNo)
            : base(databaseName)
        {
            _CustomerId = customerId;
            _QuoteNo = quoteNo;
        }
        #endregion

        #region Overrides
        public override string GeneratePdf(string rootLayoutPath, string rootOutputPath)
        {
            Logger.DebugFormat("Class: ScheduledWork, Method: GeneratePdf, Message: Generate process started for layout file {0}", rootLayoutPath);
            try
            {
                //Get the Data for the Report
                DAL.ScheduledWorkReport reportData = DAL.Reporting.ScheduledWorkReportDB.GetScheduledWorkReportForQuote(this.DatabaseName, _QuoteNo);

                //Setup the report structure and output
                SetOutputFileName(rootOutputPath, "ScheduledWork.pdf");
                LoadReportLayout(Path.Combine(rootLayoutPath, "ScheduledWork.xml"),true);

                //Apply the data to the report object
                ArbReport.PrintHeader();
                ItemGroup g = null;
                //Set the Quote Details section
                g = ArbReport.Group("QuoteDetails");
                //Set the item properties
                /*
                 * g.FindTextItem("ClientName").Text = 
                 * g.FindTextItem("QuoteDate").Text = 
                 * g.FindTextItem("BusinessName").Text = 
                 * g.FindTextItem("HouseNo").Text = 
                 * g.FindTextItem("ClientId").Text = 
                 * g.FindTextItem("Road").Text = 
                 * g.FindTextItem("QuoteNo").Text = 
                 * g.FindTextItem("City").Text = 
                 * g.FindTextItem("Surveyor").Text = 
                 * g.FindTextItem("County").Text = 
                 * g.FindTextItem("SiteRef").Text = 
                 * g.FindTextItem("PostCode").Text =    
                */
                //Print the Quotedetails section
                ArbReport.PrintGroup(g);

                //Set the Quote Details section
                g = ArbReport.Group("ScheduledDetails");
                //Set the item properties
                //g.FindTextItem("SiteAddress").Text = 

                //Print the Quotedetails section
                ArbReport.PrintGroup(g);

                //Print the Quote Item details header section
                ArbReport.PrintGroup(ArbReport.Group("DetailHeader"));
                double total = 0;
                double runningTotal = 0;

                //Set the quote item details as required
                g = ArbReport.Group("Detail");
                for (int index = 1; index < 4; index++)
                {
                    total = 100.00 + (1.75 * index);
                    g.FindTextItem("ItemNo").Text = string.Format("T{0}", index);
                    g.FindTextItem("Item").Text = "A Tree";
                    g.FindTextItem("Work").Text = "Some work to be done";
                    g.FindTextItem("Value").Text = string.Format("£{0}", total);
                    ArbReport.PrintGroup(g);
                    runningTotal += total;
                }
                g = ArbReport.Group("DetailTotals");
                g.FindTextItem("TotalValue").Text = string.Format("£{0}", runningTotal);
                g.FindTextItem("VatValue").Text = string.Format("£{0}", runningTotal * 0.2); ;
                g.FindTextItem("TotalVatValue").Text = string.Format("£{0}", runningTotal + runningTotal * 0.2); ;
                ArbReport.PrintGroup(g);

                //Create the report
                ArbReport.SaveAndClose();

                //Return the filename of the new report
                return ArbReport.Filename;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Class: ScheduledWork, Method: GeneratePdf, Message: error - {0}", ex.ToString());
                throw new Exception("Unable to generate Invoice at this time.", ex);
            }
        }

        protected override void PrintTableHeaders(object sender, EventArgs e)
        {

        }
        #endregion
    }
}
