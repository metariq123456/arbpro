﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL
{
    public static class WorkProgrammingHandler
    {
        public static List<Types.Team> GetTeamList(string database)
        {
            var getTeam = new DAL.Team(database);
            return getTeam.GetAllWithStaff();
        }

        public static bool TeamSaveAll(string database, Types.Team ToSave, string strColour)
        {
            ToSave.Colour = System.Drawing.Color.FromName(strColour);
            var saveTeam = new DAL.Team(database);
            return saveTeam.SaveAll(ToSave);
        }

        public static bool TeamDelete(string database, int teamId)
        {

            var saveTeam = new DAL.Team(database);
            return saveTeam.Delete(teamId);
        }
        public static List<Types.TeamStaff> TeamStaffGetForRisk(string databaseName, int selectedId, int riskId)
        {
            DAL.TeamStaff dbContext = new DAL.TeamStaff(databaseName);
            return dbContext.GetForRiskId(selectedId, riskId);
        }

        public static bool SaveTeam(string database, string name, string number, int id, string colour, int driver, int leader, List<int> teamMembers)
        {
            Types.Team toSave = new Types.Team();
            toSave.Id = id;
            toSave.Colour = System.Drawing.Color.FromName(colour);
            toSave.Name = name;
            toSave.DriverId = driver;
            toSave.LeaderId = leader;
            //toSave.Personnel = null;
            int parsed = id;
            int.TryParse(number, out parsed);
            toSave.Number = parsed;
            Types.TeamStaff ts = null;
            foreach (int staffId in teamMembers)
            {
                ts = new Types.TeamStaff();
                ts.StaffId = staffId;
                //ts.
                //toSave.Personnel

            }
            DAL.Team teamContext = new DAL.Team(database);
            return false; //teamContext.SaveAll
        }

        public static Types.Team GetForID(string database, int Id)
        {
            if (Id < 1)
                return new Types.Team();
            else
            {
                var Team = new DAL.Team(database);
                return Team.GetForId(Id, false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="database">string: The name of the database that is to be used</param>
        /// <param name="quoteId">int: The id of the quote</param>
        /// <returns>List of Types.WorkPlan</returns>
        public static List<Types.WorkPlan> GetWorkPlansForQuote(string database, int quoteId)
        {
            DAL.WorkPlan dbContext = new DAL.WorkPlan(database);
            List<Types.WorkPlan> retVal = dbContext.GetListForQuote(quoteId, true);
            //Each quote must have at least one work plan.
            //If the work has not yet been booked, this may need to be created.
            if (retVal.Count == 0)
            {
                Types.WorkPlan wp = new Types.WorkPlan();
                wp.QuoteId = quoteId;
                retVal.Add(wp);
            }
            return retVal;
        }

        /// <summary>
        /// Static method to get a work summary for a quote Id
        /// The summary includes all sites, work items and work plans accoiciated with a work record
        /// </summary>
        /// <param name="database">string: The database to connect to</param>
        /// <param name="quoteId">int: The id of the quote that is to be used</param>
        /// <returns>Types.Worksummary</returns>
        public static Types.WorkSummary WorkSummaryForQuote(string database, int id)
        {
            DAL.WorkSummary dbContext = new DAL.WorkSummary(database);
            return dbContext.GetForQuote(id);
        }

        /// <summary>
        /// Static method to get a work summary for a work Id
        /// The summary includes all sites, work items and work plans accoiciated with a work record
        /// </summary>
        /// <param name="database">string: The database to connect to</param>
        /// <param name="quoteId">int: The id of the quote that is to be used</param>
        /// <returns>Types.Worksummary</returns>
        public static Types.WorkSummary WorkSummaryForWork(string database, int id)
        {
            DAL.WorkSummary dbContext = new DAL.WorkSummary(database);
            return dbContext.GetForWork(id);
        }

        /// <summary>
        /// Static method to get a work summary for a work Id
        /// The summary includes all sites, work items and work plans accoiciated with a work record
        /// </summary>
        /// <param name="database">string: The database to connect to</param>
        /// <param name="quoteId">int: The id of the quote that is to be used</param>
        /// <returns>Types.Worksummary</returns>
        public static Types.WorkSummary WorkSummaryForWorkForQuote(string database, int id)
        {
            DAL.WorkSummary dbContext = new DAL.WorkSummary(database);
            return dbContext.GetForWorkOnQuote(id);
        }
        public static Types.WorkSummary WorkSummaryForJobWork(string database, int id)
        {
            DAL.WorkSummary dbContext = new DAL.WorkSummary(database);
            return dbContext.GetForJobWork(id);
        }
        public static Types.WorkSummary WorkSummaryForWorkPlan(string database, int workPlanId)
        {
            DAL.WorkSummary dbContext = new DAL.WorkSummary(database);
            return dbContext.GetForWorkPlan(workPlanId);
        }

        public static Types.WorkPlan WorkPlanForId(string database, int id, bool includeTools)
        {
            Types.WorkPlan retVal;
            if (id < 1)
            {
                retVal = new Types.WorkPlan();
                return retVal;
            }
            else
            {
                DAL.WorkPlan dbContext = new DAL.WorkPlan(database);
                retVal = dbContext.GetForId(id, includeTools);
                return retVal;
            }
        }

        public static Types.WorkPlan WorkPlanForAppointment(string database, int appointmentId, bool includeTools)
        {
            DAL.WorkPlan dbContext = new DAL.WorkPlan(database);
            return dbContext.GetForAppointment(appointmentId, includeTools);
        }


        /// <summary>
        /// Static method to get a work plan for a quote
        /// The work plan record links all required objects
        /// </summary>
        /// <param name="database">String: The database to use</param>
        /// <param name="quoteId">Int: The Id of the quote</param>
        /// <param name="workId">Int: The Id of the work record</param>
        /// <param name="jobSiteId">Int: The Id of the Job Site</param>
        /// <returns>Type.WorkPlan record</returns>
        public static Types.WorkPlan GetNewWorkPlan(string database, int quoteId, int workId, int jobSiteId)
        {
            //Create a new work plan
            Types.WorkPlan wp = new Types.WorkPlan();
            wp.QuoteId = quoteId;
            wp.WorkId = workId;
            wp.JobSiteId = jobSiteId;
            //Save and update the object with the new id
            DAL.WorkPlan dbContext = new DAL.WorkPlan(database);
            wp.Id = dbContext.Save(wp);
            return wp;
        }

        /// <summary>
        /// Static method to save all staff that are assigned to a work plan.
        /// </summary>
        /// <param name="database">String: The database to use</param>
        /// <param name="wpId">Int: The work plan Id</param>
        /// <param name="staffList">List of Types.Staff that are to be assigned to the work plan</param>
        /// <returns>True or False depending on the success of the DAL method</returns>
        public static bool SaveStaffForWorkPlan(string database, int wpId, List<Types.TeamStaff> staffList)
        {
            DAL.WorkPlanStaff dbContext = new DAL.WorkPlanStaff(database);
            int rowCount = dbContext.Save(wpId, staffList);
            return rowCount > 0;
        }

        /// <summary>
        /// Static method to get all staff currently associated with a work plan
        /// </summary>
        /// <param name="database">String: The database to use</param>
        /// <param name="wpId">Int: The Id of the work plan</param>
        /// <returns>List of Types.Staff</returns>
        public static List<Types.TeamStaff> GetStaffForWorkPlan(string database, int wpId)
        {
            DAL.WorkPlanStaff dbContext = new DAL.WorkPlanStaff(database);
            return dbContext.GetForWorkPlan(wpId);
        }

        /// <summary>
        /// Static method to get all booked work
        /// </summary>
        /// <param name="database">String: The Database to use</param>
        /// <param name="startDate">DateTime: The start date for the work</param>
        /// <param name="endDate">DateTime: The end date for the period</param>
        /// <returns>List of Types.CalendarWork for the specified date range</returns>
        public static List<Types.CalendarWork> GetBookedWork(string database, DateTime startDate, DateTime endDate)
        {
            DAL.Calendar dbContext = new DAL.Calendar(database);
            return dbContext.GetWorkList(startDate, endDate);
        }

        public static List<Types.CalendarWork> GetBookedWork(string database, DateTime startDate, DateTime endDate, int workPlanId)
        {
            DAL.Calendar dbContext = new DAL.Calendar(database);
            return dbContext.GetWorkList(startDate, endDate, workPlanId);
        }

        public static Types.CalendarWork GeAppointmentForWorkPlan(string database, int workPlanId)
        {
            DAL.Calendar dbContext = new DAL.Calendar(database);
            return dbContext.GetForWorkPlan(workPlanId);
        }

        public static Types.CalendarWork GeAppointmentForId(string database, int appointmentId)
        {
            DAL.Calendar dbContext = new DAL.Calendar(database);
            return dbContext.GetWork(appointmentId);
        }

        /// <summary>
        /// Saves a work plan and appointment. If there are skip days these are taken into consideration and then
        /// the appointment is split into multiple appointments.
        /// </summary>
        /// <param name="database">
        /// Database for which changes shall be applied.
        /// </param>
        /// <param name="workPlan">
        /// The work plan for the appointment.
        /// </param>
        /// <param name="appointment">
        /// The appointment details.
        /// </param>
        /// <param name="saveAll">
        /// Whether or not all details in the work plan shall be saved.
        /// </param>
        /// <returns>
        /// Returns a value indicating whether or not the function was successful.
        /// </returns>
        public static bool SaveWorkPlanAndAppointment(string database, Types.WorkPlan workPlan, Types.CalendarWork appointment, bool saveAll)
        {
            int updatedId = -1;
            DAL.WorkPlan wpContext = new DAL.WorkPlan(database);
            DAL.Calendar appContext = new DAL.Calendar(database);
            
            //Check if the work plan collection should be saved
            if (saveAll)
                updatedId = wpContext.SaveAll(workPlan);
            else
                updatedId = wpContext.Save(workPlan);

            //checked if the workPlan plan was saved
            //Only save the appointment if the update succeded
            if (updatedId > 0)
            {
                appointment.WorkPlanId = updatedId;
                appointment.JobSiteId = workPlan.JobSiteId;
                appointment.TeamId = workPlan.TeamId;

                var appointments = GetAppointmentsThroughSplitDays(appointment).ToArray();
                if (appointments.Count() <= 0)
                {
                    return appContext.SaveWork(appointment);
                }
                else
                {
                    bool ret = true;
                    int origId = -1;
                    foreach (var appt in appointments)
                    {
                        if (origId > 0)
                        {
                            origId = appt.Id;
                            appt.Id = 0;
                        }
                        ret &= appContext.SaveWork(appt);
                        if (origId > 0)
                        {
                            appt.Id = origId;
                        }
                        origId = 1;
                    }
                    return ret;
                }
            }
            //Should not get here if all save opertaion succeded
            return false;
        }

        public static bool SaveWorkPlanAndAppointmentForBookJob(string database, Types.WorkPlan workPlan, Types.CalendarWork appointment, bool saveAll)
        {
            int updatedId = -1;
            DAL.WorkPlan wpContext = new DAL.WorkPlan(database);
            DAL.Calendar appContext = new DAL.Calendar(database);

            //Check if the work plan collection should be saved
            if (saveAll)
            {
                updatedId = wpContext.SaveAllForJobWork(workPlan);
                if (updatedId == 1)
                {
                   return appContext.SaveJobWorkAppointmentForEdit(appointment);
                }
               
                
            }
            /*else
                updatedId = wpContext.Save(workPlan);
            
            //checked if the workPlan plan was saved
            //Only save the appointment if the update succeded
            if (updatedId > 0)
            {
                appointment.WorkPlanId = updatedId;
                appointment.JobSiteId = workPlan.JobSiteId;
                appointment.TeamId = workPlan.TeamId;
                
            }*/
            //Should not get here if all save opertaion succeded
            return false;
        }
        public static bool SaveWorkPlanAndAppointmentNew(string database, Types.WorkPlan workPlan, Types.CalendarWork appointment, bool saveAll)
        {
            int updatedId = -1;
            DAL.WorkPlan wpContext = new DAL.WorkPlan(database);
            DAL.Calendar appContext = new DAL.Calendar(database);

            //Check if the work plan collection should be saved
            if (saveAll)
                updatedId = wpContext.SaveAll(workPlan);

            //checked if the workPlan plan was saved
            //Only save the appointment if the update succeded
            if (updatedId > 0)
            {
                appointment.WorkPlanId = updatedId;
                appointment.JobSiteId = workPlan.JobSiteId;
                appointment.TeamId = workPlan.TeamId;
                    return appContext.SaveWork(appointment);
            }
            //Should not get here if all save opertaion succeded
            return false;
        }
        public static int SaveJobWorkPlanAndAppointmentNew(string database, Types.WorkPlan workPlan, Types.CalendarWork appointment, bool saveAll)
        {
            int updatedId = -1;
            DAL.WorkPlan wpContext = new DAL.WorkPlan(database);
            DAL.Calendar appContext = new DAL.Calendar(database);

            //Check if the work plan collection should be saved
            if (saveAll)
                updatedId = wpContext.SaveAll(workPlan);

            //checked if the workPlan plan was saved
            //Only save the appointment if the update succeded
            if (updatedId > 0)
            {
                appointment.WorkPlanId = updatedId;
                appointment.JobSiteId = workPlan.JobSiteId;
                appointment.TeamId = workPlan.TeamId;
                appContext.SaveJobWork(appointment);
            }
            if (workPlan.Comments.Length > 0)
            {
                Types.Note NoteObj = new Types.Note();
                NoteObj.ParentId = workPlan.QuoteId;
                NoteObj.NoteType = 0;
                NoteObj.Text = workPlan.Comments;
                NoteObj.EnteredBy = workPlan.StaffName;
                NoteObj.Id = -1;
                DAL.Note NoteContext = new DAL.Note(database);
                NoteContext.Save(NoteObj);
            }
            //Should not get here if all save opertaion succeded
            return updatedId;
        }
        public static bool SaveOnlyAppointmentForWorkPlan(string database,Types.CalendarWork appointment)
        {
            DAL.Calendar appContext = new DAL.Calendar(database);
            bool ret = true;
            ret &= appContext.SaveWork(appointment);
            return ret;
        }
        /// <summary>
        /// Takes a single appointment and returns 1 or more appointments taking into consideration the skip days
        /// enumeration.
        /// </summary>
        /// <param name="appointment">
        /// The appointment to examine and if necessary break into several smaller appointments.
        /// </param>
        /// <returns>
        /// The enumeration of <see cref="CalendarWork"/> objects.
        /// </returns>
        private static IEnumerable<Types.CalendarWork> GetAppointmentsThroughSplitDays(Types.CalendarWork appointment)
        {
            DateTime? blockStart = appointment.StartDateTime;
            DateTime? blockEnd = null;

            for (var current = appointment.StartDateTime; current <= appointment.EndDateTime; current = current.AddDays(1))
            {
                if ((current.DayOfWeek == DayOfWeek.Saturday &&
                    (appointment.SkipDays == Types.Enums.CalendarSkipDaysEnum.Both || appointment.SkipDays == Types.Enums.CalendarSkipDaysEnum.Saturday)) ||
                    (current.DayOfWeek == DayOfWeek.Sunday &&
                    (appointment.SkipDays == Types.Enums.CalendarSkipDaysEnum.Both || appointment.SkipDays == Types.Enums.CalendarSkipDaysEnum.Sunday)))
                {
                    if (blockEnd != null)
                    {
                        yield return BuildBlock(appointment, ref blockStart, ref blockEnd);
                    }

                    continue;
                }
                else
                {
                    blockEnd = new DateTime(current.Year, current.Month, current.Day,
                        appointment.EndDateTime.Hour, appointment.EndDateTime.Minute, appointment.EndDateTime.Second);

                    if (blockStart == null)
                    {
                        blockStart = current;
                    }
                }
            }

            if (blockStart != blockEnd && blockStart != null && blockEnd != null)
            {
                yield return BuildBlock(appointment, ref blockStart, ref blockEnd);
            }
        }

        private static Types.CalendarWork BuildBlock(Types.CalendarWork appointment, ref DateTime? blockStart, ref DateTime? blockEnd)
        {
            var ret = new Types.CalendarWork
            {
                CalendarString = appointment.CalendarString,
                Color = appointment.Color,
                Comments = appointment.Comments,
                Id = appointment.Id,
                JobSiteId = appointment.JobSiteId,
                JobSiteName = appointment.JobSiteName,
                SkipDays = appointment.SkipDays,
                TeamId = appointment.TeamId,
                WorkPlanId = appointment.WorkPlanId,
                StartDateTime = (DateTime)blockStart,
                EndDateTime = (DateTime)blockEnd
            };

            blockStart = blockEnd = null;
            return ret;
        }

        public static bool DeleteWorkAppointment(string database, int appointmentId)
        {
            DAL.Calendar appContext = new DAL.Calendar(database);
            return appContext.DeleteWork(appointmentId);
        }

        public static int DeleteWorkTool(string database, int Id)
        {
            DAL.WorkTool DBContext = new DAL.WorkTool(database);
            return DBContext.Delete(Id);
        }

        public static int UpdateWorkPlan(string database, Types.WorkPlan workPlan)
        {
            DAL.WorkPlan DBContext = new DAL.WorkPlan(database);
            return DBContext.Save(workPlan);
        }

        public static int UpdateWorkTool(string database,int workId, List<Types.WorkTool> tools)
        {
            DAL.WorkTool DBContext = new DAL.WorkTool(database);
            return DBContext.Save(workId,tools);
        }

        public static List<Types.Work> GetWorkForQuote(string database, int quoteId)
        {
            DAL.Work DBContext = new DAL.Work(database);
            return DBContext.GetForQuote(quoteId);
        }

        public static List<Types.TeamStaff> GetStaffListAsTeamStaff(string database)
        {
            DAL.TeamStaff dbContext = new DAL.TeamStaff(database);
            return dbContext.GetFromStaffList();
        }

        public static List<Types.WorkTool> GetToolListAsWorkTools(string database)
        {
            DAL.WorkTool dbContext = new DAL.WorkTool(database);
            return dbContext.GetFromToolList();
        }

        public static bool DeleteWorkPlan(string database, int workPlanId)
        {
            var dbContext = new DAL.WorkPlan(database);
            return dbContext.Delete(workPlanId);
        }
    }
}
