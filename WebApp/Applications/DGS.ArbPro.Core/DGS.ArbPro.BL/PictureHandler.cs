﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace DGS.ArbPro.BL
{
    public class PictureHandler
    {
        //const string CONST_RISK_PREFIX = "risk_sig_id_";
        //string _DataBaseName;

        //public PictureHandler() { }

        //public PictureHandler(string dataBaseName)
        //{
        //    _DataBaseName = dataBaseName;
        //}

        //public string LocalUrlForSignatureGetorCreate(int riskId, string localPath, Byte[] imageData)
        //{
        //    if(imageData == null || imageData.Length == 0)
        //    {
        //        return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images\\blank.png");
        //    }

        //    string fileName = string.Format("{0}{1}_{2}.Jpeg",CONST_RISK_PREFIX, riskId,DateTime.Now.ToShortDateString()).Replace("/","_");
        //    //string filePath = Path.Combine(localPath, fileName);

        //    return GetOrCreateImageLocation(fileName, localPath, imageData, true);
        //}

        //public List<DAL.QuoteItemPicture> LocalUrlForQuoteItemPicsGetOrCreate(string databaseName, int quoteNo, string localPath)
        //{
        //    List<DAL.QuoteItemPicture> retVals = new List<DAL.QuoteItemPicture>();
        //    DAL.QuoteItemPictureDb DB = new DAL.QuoteItemPictureDb(databaseName);
        //    retVals = DB.QuoteItemPicturesForQuoteNoGet(quoteNo);

        //    foreach (DAL.QuoteItemPicture pic in retVals)
        //    {
        //        if (pic.Guid.Length == 0)
        //            pic.Guid = Guid.NewGuid().ToString();

        //        pic.ImageFilePath = GetOrCreateImageLocation(string.Format("{0}{1}.Jpeg", pic.Guid.Replace("-","_"), quoteNo), localPath, pic.ImageData);
        //    }

        //    return retVals;
        //}

        //private string GetOrCreateImageLocation(string fileName, string filePath, Byte[] imageData, bool UseBlank = false)
        //{
        //    string fullPath = Path.Combine(filePath, fileName);
        //    if (File.Exists(fullPath))
        //    {
        //        return fullPath;
        //    }

        //    if (imageData == null || imageData.Length == 0)
        //    {
        //        return GetDefaultImagePath(UseBlank);
        //    }

        //    try
        //    {
        //        MemoryStream ms = new MemoryStream(imageData);
        //        Bitmap img = new Bitmap(ms);
        //        img.Save(fullPath, System.Drawing.Imaging.ImageFormat.Jpeg);

        //        return fullPath;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error saving image to disk: {0}", ex.Message);
        //        return GetDefaultImagePath(UseBlank);
        //    }
        //}

        //private string GetDefaultImagePath(bool UseBlank)
        //{
        //    if (UseBlank)
        //        return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images\\blank.png");
        //    else
        //        return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Images\\NoImageAvailable.png");
        //}
    }
}

