﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL
{
    public class ReportingDataHandler
    {
        /// <summary>
        /// The db name.
        /// </summary>
        private readonly string dtbName;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportingDataHandler"/> class.
        /// </summary>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        public ReportingDataHandler(string databaseName)
        {
            this.dtbName = databaseName;
        }

        /// <summary>
        /// The get all enquiry sources for dates.
        /// </summary>
        /// <returns> List of ReportingData for enquiry sources. </returns>
        public List<Types.ReportingData> GetEnquiryForDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForReceivedDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForReceivedDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForSentDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForSentDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForAcceptedDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForAcceptedDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForWorkBookedDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForWorkBookedDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForWorkInProgressDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForWorkInProgressDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForWorkCompletedDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForWorkCompletedDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForVatTotal(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForVatTotal(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForVatValueCollected(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForVatValueCollected(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForSalesHistoricallyDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForSalesHistoricallyDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForSalesOutstandingDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForSalesOutstandingDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForIndividualRateDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForIndividualRateDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForIndividualSalesDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForIndividualSalesDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForViewDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.Reporting_GetEnquiryForCategoriesDates(startDate, endDate);
        }
        public List<Types.ReportingData> GetEnquiryForLetterDates(DateTime startDate, DateTime endDate)
        {
            DAL.ReportingData marketingDb = new DAL.ReportingData(this.dtbName);
            return marketingDb.GetEnquiryForLetterDates(startDate, endDate);
        }
    }
}
