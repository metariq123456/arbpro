﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL
{
    /// <summary>
    /// Helper methods for the report handler types.
    /// </summary>
    public static class ReportHandlerExtensions
    {
        /// <summary>
        /// Ensures that if an item already exists, it is replaced, to ensure there are no
        /// duplicate items added to the resulting email.
        /// </summary>
        /// <param name="list">
        /// List to check
        /// </param>
        /// <param name="ordinal">
        /// Ordinal to do the final insert
        /// </param>
        /// <param name="document">
        /// The document to insert after ensuring it is
        /// not already attached
        /// </param>
        public static void InsertWhenMissing(this List<Types.UserDocument> list, int ordinal, Types.UserDocument document)
        {
            var remove = list.SingleOrDefault(x => x.Name.Equals(document.Name));
            if (remove != default(Types.UserDocument))
            {
                list.Remove(remove);
            }
            list.Insert(ordinal, document);
        }
    }
}
