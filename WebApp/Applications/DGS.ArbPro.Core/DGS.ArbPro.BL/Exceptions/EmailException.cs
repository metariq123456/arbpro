﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL.Exceptions
{
    public class EmailException: Exception
    {
        private string _EmailGuid = "";

        public string EmailGuid
        {
            get { return _EmailGuid; }
            set {_EmailGuid = value; }
            
        }

        public EmailException(string EmailGuid, Exception ex)
            : base("Error during email processing", ex)
        {
            _EmailGuid = EmailGuid;
        }
    }
}
