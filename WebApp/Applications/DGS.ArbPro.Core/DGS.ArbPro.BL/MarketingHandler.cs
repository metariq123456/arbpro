﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MarketingHandler.cs" company="Microsec Ltd">
//   2016 Copyright Microsec Ltd
// </copyright>
// <summary>
//   The marketing handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.BL
{
    using System.Collections.Generic;

    /// <summary>
    /// The marketing handler.
    /// </summary>
    public class MarketingHandler
    {
        /// <summary>
        /// The db name.
        /// </summary>
        private readonly string dtbName;

        /// <summary>
        /// Initializes a new instance of the <see cref="MarketingHandler"/> class.
        /// </summary>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        public MarketingHandler(string databaseName)
        {
            this.dtbName = databaseName;
        }

        #region Get

        #region WorkCategory

        /// <summary>
        /// The get all work categories.
        /// </summary>
        /// <returns>
        /// List of work categories
        /// </returns>
        public List<Types.Marketing> GetAllWorkCategories()
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetAllWorkCategories();
        }

        /// <summary>
        /// The get work category for Id.
        /// </summary>
        /// <param name="id">
        /// The work category Id
        /// </param>
        /// <returns>
        /// The work category
        /// </returns>
        public Types.Marketing GetWorkCategoryForId(int id)
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetWorkCategoryForId(id);
        }

        #endregion

        #region ClientWorkCategory

        /// <summary>
        /// The get all client work categories.
        /// </summary>
        /// <returns> List of client work categories. </returns>
        public List<Types.Marketing> GetAllClientWorkCategories()
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetAllClientWorkCategories();
        }

        /// <summary>
        /// The get all distinct for seasonal marketing.
        /// </summary>
        /// <returns> List of client work categories. </returns>
        public List<Types.Marketing> GetAllDistinctForSeasonalMarketing()
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetAllDistinctForSeasonalMarketing();
        }

        /// <summary>
        /// The get all distinct accepted for seasonal marketing.
        /// </summary>
        /// <returns> List of client work categories. </returns>
        public List<Types.Marketing> GetAllDistinctAcceptedForSeasonalMarketing()
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetAllDistinctAcceptedForSeasonalMarketing();
        }

        /// <summary>
        /// The get all distinct not accepted for seasonal marketing.
        /// </summary>
        /// <returns> List of client work categories. </returns>
        public List<Types.Marketing> GetAllDistinctNotAcceptedForSeasonalMarketing()
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetAllDistinctNotAcceptedForSeasonalMarketing();
        }

        /// <summary>
        /// Get all clients for marketing.
        /// </summary>
        /// <returns> A list of all clients for marketing. </returns>
        public List<Types.Marketing> GetAllClientMarketing()
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetAllClientMarketing();
        }

        /// <summary>
        /// The get all for return marketing.
        /// </summary>
        /// <returns> List of client work categories. </returns>
        public List<Types.Marketing> GetAllForReturnMarketing()
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetAllForReturnMarketing();
        }

        /// <summary>
        /// The get sent for return marketing.
        /// </summary>
        /// <returns> List of client work categories. </returns>
        public List<Types.Marketing> GetSentForReturnMarketing()
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetSentForReturnMarketing();
        }

        /// <summary>
        /// The get all return time categories.
        /// </summary>
        /// <returns> List of return time categories. </returns>
        public List<Types.Marketing> GetAllReturnTimeCategories()
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetAllReturnTimeCategories();
        }

        /// <summary>
        /// The get client work categories for client id.
        /// </summary>
        /// <param name="clientId">
        /// The client id.
        /// </param>
        /// <returns>
        /// List of client work categories
        /// </returns>
        public List<Types.Marketing> GetClientWorkCategoriesForClientId(int clientId)
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetWorkClientCategoriesForClientId(clientId);
        }

        /// <summary>
        /// The get client work categories for job id.
        /// </summary>
        /// <param name="jobId"> The job id. </param>
        /// <returns> List of client work categories. </returns>
        public List<Types.Marketing> GetClientWorkCategoriesForJobId(int jobId)
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetWorkClientCategoriesForJobId(jobId);
        }

        /// <summary>
        /// The get client work category for Id.
        /// </summary>
        /// <param name="id">
        /// The client work category Id
        /// </param>
        /// <returns>
        /// The client work category
        /// </returns>
        public Types.Marketing GetClientWorkCategoryForId(int id)
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.GetClientWorkCategoryForId(id);
        }

        #endregion

        #endregion

        #region Update

        #region WorkCategory

        /// <summary>
        /// The save work category.
        /// </summary>
        /// <param name="obj">
        /// The work category <see langword="object"/>.
        /// </param>
        /// <returns>
        /// The inserted or updated id.
        /// </returns>
        public int SaveWorkCategory(Types.Marketing obj)
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);

            return marketingDb.SaveWorkCategory(obj);
        }

        #endregion

        #region ClientWorkCategory

        /// <summary>
        /// The save client work category.
        /// </summary>
        /// <param name="obj">
        /// The client work category <see langword="object"/>.
        /// </param>
        /// <returns>
        /// The inserted or updated id.
        /// </returns>
        public int SaveClientWorkCategory(Types.Marketing obj)
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);

            return marketingDb.SaveClientWorkCategory(obj);
        }

        /// <summary>
        /// Updates client work categories' quote accepted status to 0 for job id - useful for cancellation of quote/ work.
        /// </summary>
        /// <param name="jobId"> The job id. </param>
        /// <returns> Success or failure. </returns>
        public bool UpdateAcceptedWorkClientCategoriesForJobId(int jobId)
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.UpdateAcceptedWorkClientCategoriesForJobId(jobId);
        }

        #endregion

        #endregion

        #region Remove

        #region WorkCategory

        /// <summary>
        /// The delete work category for Id.
        /// </summary>
        /// <param name="id">
        /// The work category Id.
        /// </param>
        /// <returns>
        /// Success or failure
        /// </returns>
        public bool DeleteWorkCategoryForId(int id)
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.DeleteWorkCategoriesForId(id);
        }

        #endregion

        #region ClientWorkCategory

        /// <summary>
        /// Deletes a client work category for an id - used when a category is unselected at job stage.
        /// </summary>
        /// <param name="id"> The client work category id. </param>
        /// <returns> Success or failure. </returns>
        public bool DeleteClientWorkCategoryForId(int id)
        {
            DAL.Marketing marketingDb = new DAL.Marketing(this.dtbName);
            return marketingDb.DeleteClientWorkCategoryForId(id);
        }

        #endregion

        #endregion
    }
}
