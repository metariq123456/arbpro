﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using DGS.ArbPro.DAL;
using DGS.ArbPro.DAL.Reporting.DBItems;
using log4net;

namespace DGS.ArbPro.BL
{
    using DGS.ArbPro.BL.Exceptions;

    public abstract class ReportHandlerBase
    {
        #region Members
        static ILog _Logger = LogManager.GetLogger("BL.ReportHandler");
        string _DatabaseName = "";
        string _OutputPath = "";
        string _LayoutPath = "";
        #endregion

        #region Constructors
        public ReportHandlerBase(string databaseName, string outputPath, string layoutPath)
        {
            _DatabaseName = databaseName;
            _OutputPath = outputPath;
            _LayoutPath = layoutPath;
        }
        #endregion

        #region Properties
        #region Protected
        protected static ILog Logger
        {
            get { return _Logger; }
        }

        protected string DatabaseName
        {
            get { return _DatabaseName; }
            set { _DatabaseName = value; }
        }

        protected string OutputPath
        {
            get { return _OutputPath; }
            set { _OutputPath = value; }
        }

        protected string LayoutPath
        {
            get { return _LayoutPath; }
            set { _LayoutPath = value; }
        }
        #endregion
        #endregion

        #region Methods
        #region Protected
        #region ***** EMAIL *****
        protected string SendDocumentsToEmailHandler(Types.User usr, int recipientContactID, string subject, string emailText, List<Types.UserDocument> attachments)
        {
            string guid = ReportItemHandlers.ReportEmailHandler.SendPDFEmail(
                _DatabaseName,
                recipientContactID,
                usr,
                subject,
                emailText,
                attachments);
            if(guid.Length == 0)
                throw new Exception("Email generation failed to return a valid Guid.");

            return guid;
        }

        protected string SendDocumentsToEmailHandler(Types.User usr, List<string> recipients, string subject, string emailText, List<Types.UserDocument> attachments)
        {
            string guid = ReportItemHandlers.ReportEmailHandler.SendPDFEmail(
                _DatabaseName,
                recipients,
                usr,
                subject,
                emailText,
                attachments);


            if (guid.Length == 0)
                throw new Exception("Email generation failed to return a valid Guid.");

            return guid;
        }

        protected string SendDocumentsToEmailHandler(Types.User usr, Types.EmailSettings settings, List<Types.UserDocument> attachments)
        {
            string guid = "";
            try
            {
                foreach(string ccEmail in settings.CCRecipients)
                {
                    Utility.Logger.Log.DebugFormat("CC Emails:{0}", ccEmail);
                }
                
                 guid = ReportItemHandlers.ReportEmailHandler.SendPDFEmailAndSms(
                    _DatabaseName,
                    settings.Recipients,
                    settings.CCRecipients,
                    usr,
                    settings.Subject,
                    settings.BodyText,
                    attachments,
                    settings.SmsText);
                 
            }
            catch (EmailException ex)
            {
                // Exception needs to be logged
                guid = ex.EmailGuid;
            }

            if (guid.Length == 0)
                throw new Exception("Email generation failed to return a valid Guid.");

            return guid;
        }
        #endregion
        #endregion
        #endregion
    }
}
