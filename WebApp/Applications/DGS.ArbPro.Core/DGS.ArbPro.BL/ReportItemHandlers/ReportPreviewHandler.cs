﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DGS.ArbPro.DAL;
using DGS.ArbPro.DAL.Reporting.DBItems;
using PDFReport;
using System.Web;

namespace DGS.ArbPro.BL
{
    class ReportPreviewHandler
    {
        //CustomGroupHandler cgh = null;
        //string sDatabaseName = "";
        //string sFullLayoutPath = "";
        //string sFullOutputPath = "";

        //public ReportPreviewHandler(string sXMLReportLayoutsPATH, string sPDFReportPATH, string databaseName)
        //{
        //    sDatabaseName = databaseName;
        //    cgh = new CustomGroupHandler(databaseName);
        //    sFullLayoutPath = sXMLReportLayoutsPATH;
        //    sFullOutputPath = sPDFReportPATH;
        //}

        //private string GetOutputFilenameFullPath(string sPreviewFilename)
        //{
        //    return Path.Combine(sFullOutputPath, sPreviewFilename + ".pdf");
        //}

        //public MemoryStream CreatePreviewLetter(string sPreviewType)
        //{

        //    MemoryStream ms = new MemoryStream();

        //    try
        //    {
        //        Report r = new Report();
        //        r.Filename = GetOutputFilenameFullPath(sPreviewType);

        //        switch (sPreviewType.ToLower())
        //        {
        //            case "headerfooterquoteletter":
        //                CreatePreview_HeaderFooterQuoteLetter(ref r);
        //                break;
        //        }

        //        r.SaveAndClose(ref ms);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return ms;
        //}

        //private void CreatePreview_HeaderFooterQuoteLetter(ref Report r)
        //{

        //    r.LoadLayout(Path.Combine(sFullLayoutPath, "QuoteLetter.xml"));

        //    foreach (CustomGroup cg in cgh.GetCustomGroupsForClient())
        //    {
        //        r.AddCustomGroup(cg);
        //    }

        //    r.PrintHeader();
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "QuoteAddressDateInfo.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "LetterSalutationSiteAddress.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "LetterMainText.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "LetterSignOffSig.xml"));

        //    string sCustomerName = "Preview Ltd";
        //    string sCustomerAddress = "1 Sumb Street" + System.Environment.NewLine;
        //    sCustomerAddress += "Somewhere" + System.Environment.NewLine;
        //    sCustomerAddress += "Somehow" + System.Environment.NewLine;
        //    sCustomerAddress += "Somewhen" + System.Environment.NewLine;
        //    sCustomerAddress += "PO1 1AA";

        //    string sContactName = "Mr P Review";
        //    string sSiteAddress = "99 Else Road, ";
        //    sSiteAddress += "SomewhereElse, ";
        //    sSiteAddress += "SomehowElse, ";
        //    sSiteAddress += "SomewhenElse, ";
        //    sSiteAddress += "PO9 9ZZ";

        //    string sQuoteNo = "9999";
        //    string sClientIDNo = "99";
        //    string sQuoteSurveyor = "P R Eview";
        //    string sSiteRefNo = "999";

        //    r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = DateTime.Now.ToShortDateString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sCustomerName;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sCustomerAddress;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = sQuoteNo;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = sClientIDNo;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtSurveyor").Text = sQuoteSurveyor;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = sSiteRefNo;

        //    r.Group("grpSalutationSiteAddress").FindTextItem("txtSalutation").Text = "Dear " + sContactName;
        //    r.Group("grpSalutationSiteAddress").FindTextItem("txtSiteAddress").Text = sSiteAddress;

        //    r.PrintGroup(r.Group("grpAddressDateInfo"));
        //    r.PrintGroup(r.Group("grpSalutationSiteAddress"));

        //    string sLetterText = "Thank you for contacting us to provide a quotation. I attach a detailed schedule of the works required. All tree work undertaken by us conforms to British Standard BS3998 \"Recommendations for Tree Work 2010\".";
        //    sLetterText += Environment.NewLine + Environment.NewLine;
        //    sLetterText += "This quotation is valid for 60 days (pre VAT value) and is based on the site conditions observed on the day of inspection. Please inform us before work commences of any site alterations that may affect operational logistics.";
        //    sLetterText += Environment.NewLine + Environment.NewLine;
        //    sLetterText += "If you have any queries or require further information please do contact us.";
        //    sLetterText += Environment.NewLine + Environment.NewLine;
        //    sLetterText += "P R Eview";
            
        //    r.Group("grpMainText").FindTextItem("txtMainText").Text = sLetterText;

        //    r.PrintGroup(r.Group("grpMainText"));
            
        //    Company cmpy = DAL.Company.GetCompany(sDatabaseName);
        //    Byte[] imgBytes = cmpy.CompanyDirectorSignatureImage;
        //    r.Group("grpSignOffSig").FindImageItem("imgSig").SetImage(imgBytes);

        //    string sDirectorName = cmpy.CompanyDirectorName;
        //    r.Group("grpSignOffSig").FindTextItem("txtSigName").Text = sDirectorName;

        //    r.PrintGroup(r.Group("grpSignOffSig"));
        //}

    }
}
