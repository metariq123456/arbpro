﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using DGS.ArbPro.DAL;
using DGS.ArbPro.DAL.Reporting.DBItems;

namespace DGS.ArbPro.BL
{
    public class QuoteReportHandler_2 //: ReportHandlerBase
    {
        /*
        #region Members
        #endregion

        #region Constructors
        public QuoteReportHandler_2(string databaseName, string outputPath, string layoutPath) : base(databaseName, outputPath, layoutPath) { }
        #endregion

        #region Properties

        #endregion

        #region Methods
        #region ****** Public ******
        #region Creation with Settings
        public MemoryStream GenerateDocumentDownload(Types.QuotePdfSettings settings, Types.User usr)
        {
            if (settings.IsPreview)
                return DownloadPreview(settings, usr);
            else
                return DownloadDocuments(settings, usr);
        }

        public MemoryStream DownloadDocuments(Types.QuotePdfSettings settings, Types.User usr)
        {
            MemoryStream ms = null;
            try
            {
                DAL.QuoteHistoryItem dbContext = new DAL.QuoteHistoryItem(this.DatabaseName);
                switch (settings.GenerationEnum)
                {
                    case Types.Enums.DocGenerationEnum.Both:
                        //Both the letter and schedule are to be generated
                        ms = OpenCombinedQuoteDocuments(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter);
                        break;
                    case Types.Enums.InvoiceDocTypeEnum.Letter:
                    case Types.Enums.InvoiceDocTypeEnum.First_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Second_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Final_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Legal_Action:
                    case Types.Enums.InvoiceDocTypeEnum.Legal_Action_2:
                        Types.InvoiceHistoryItem letter = GetInvoiceLetter(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter);
                        ms = letter.Doc;
                        break;
                    case Types.Enums.InvoiceDocTypeEnum.Schedule:
                        Types.InvoiceHistoryItem schedule = GetInvoiceSchedule(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter);
                        ms = schedule.Doc;
                        break;
                }
            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "InvoiceReportHandler.DownloadDocuments", settings, ex);
                Debug.WriteLine("Error in InvoiceReportHandler.DownloadDocuments: {0}", ex.ToString());
                Logger.ErrorFormat("Error in InvoiceReportHandler.DownloadDocuments: {0}", ex.ToString());
                ms = null;
            }
            return ms;
        }

        public MemoryStream DownloadPreview(Types.QuotePdfSettings settings, Types.User usr)
        {
            MemoryStream ms = null;
            try
            {
                switch (settings.GenerationEnum)
                {
                    case Types.Enums.InvoiceDocTypeEnum.Letter_And_Schedule:
                        //Both the letter and schedule are to be generated
                        ms = OpenCombinedQuoteDocuments(settings);
                        break;
                    case Types.Enums.InvoiceDocTypeEnum.Letter:
                    case Types.Enums.InvoiceDocTypeEnum.First_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Second_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Final_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Legal_Action:
                    case Types.Enums.InvoiceDocTypeEnum.Legal_Action_2:
                        ms = this.InvoiceCreateLetter(settings);
                        break;
                    case Types.Enums.InvoiceDocTypeEnum.Schedule:
                        ms = this.InvoiceCreateSchedule(settings);
                        break;
                }
            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "InvoiceReportHandler.DownloadDocuments", settings, ex);
                Debug.WriteLine("Error in InvoiceReportHandler.DownloadDocuments: {0}", ex.ToString());
                Logger.ErrorFormat("Error in InvoiceReportHandler.DownloadDocuments: {0}", ex.ToString());
                ms = null;
            }
            return ms;
        }

        /// <summary>
        /// Method to generate a preview of the quote documents
        /// This method will always generate a letter and a schedule and return the joined documents.
        /// No History items are created durring this method.
        /// </summary>
        /// <param name="usr">The details ot the user performing the action</param>
        /// <param name="pdfSettings">The settings to be applied to the PDF</param>
        /// <returns>A Memory stream that contains the PDF documents</returns>
        public MemoryStream CreatePreview(Types.User usr, Types.PdfSettings pdfSettings)
        {
            MemoryStream ms = null;
            try
            {

            }
            catch (Exception ex)
            {

            }
            return ms;
        }

        /// <summary>
        /// Method to email, and generate if required, quote documents
        /// </summary>
        /// <param name="pdfSettings">The settings that are to be used for the generation of the PDF</param>
        /// <param name="emailSettings">The settings that are to be used for sending the email</param>
        /// <param name="docsToAttach">A List of user documents that are to be attached to the email</param>
        /// <returns>A Memory stream that contains the PDF documents</returns>
        public MemoryStream EmailQuoteDocuments(Types.User usr, Types.PdfSettings pdfSettings, Types.EmailSettings emailSettings, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {

            }
            catch (Exception ex)
            {

            }
            return ms;
        }

        /// <summary>
        /// Method to generate, or recall PDF documents and create document history items as required
        /// </summary>
        /// <param name="settings">The settings to be applied to PDF generation</param>
        /// <returns>A Memory stream that contains the PDF documents</returns>
        public MemoryStream CreateQuoteDocuments(Types.User usr, Types.PdfSettings settings)
        {
            MemoryStream ms = null;
            try
            {

            }
            catch (Exception ex)
            {

            }
            return ms;
        }
        #endregion
        #region Email
        public MemoryStream SendQuoteLetterByEmail(Types.User usr, int quoteId, int recipientContactId, bool isCopy, string letterText, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                DAL.QuoteHistoryItem dbContext = new QuoteHistoryItem(this.DatabaseName);
                Types.QuoteHistoryItem hstItem = GetQuoteLetter(dbContext, quoteId, Types.Enums.DocDispatchMethodEnum.Email, false, letterText);
                docsToAttach.Insert(0, hstItem.ToEmailAttachment("QuoteLetter.pdf"));
                hstItem.EmailReference = SendDocumentsToEmailHandler(usr, recipientContactId, subject, emailText, docsToAttach);
                ms = hstItem.Doc;
                dbContext.Save(hstItem);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendQuoteLetterByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "SendQuoteLetterByEmail", String.Format("Quote Id : {0}", quoteId), ex);
                throw (ex);
            }
            return ms;
        }

        public MemoryStream SendQuoteScheduleByEmail(Types.User usr, int quoteId, int recipientContactId, bool isCopy, string letterText, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                DAL.QuoteHistoryItem dbContext = new QuoteHistoryItem(this.DatabaseName);
                Types.QuoteHistoryItem hstItem = GetQuoteSchedule(dbContext, quoteId, Types.Enums.DocDispatchMethodEnum.Email, false);
                docsToAttach.Insert(0, hstItem.ToEmailAttachment("QuoteSchedule.pdf"));
                hstItem.EmailReference = SendDocumentsToEmailHandler(usr, recipientContactId, subject, emailText, docsToAttach);
                ms = hstItem.Doc;
                dbContext.Save(hstItem);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendQuoteScheduleByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "SendQuoteScheduleByEmail", String.Format("Quote Id : {0}", quoteId), ex);
                throw (ex);
            }
            return ms;
        }


        public MemoryStream SendQuoteDocumentsByEmail(Types.User usr, int quoteId, int recipientContactId, bool isCopy, string letterText, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                DAL.QuoteHistoryItem dbContext = new QuoteHistoryItem(this.DatabaseName);
                Types.QuoteHistoryItem ltrtItem = GetQuoteSchedule(dbContext, quoteId, Types.Enums.DocDispatchMethodEnum.Email, false);
                Types.QuoteHistoryItem hstItem = GetQuoteLetter(dbContext, quoteId, Types.Enums.DocDispatchMethodEnum.Email, false, letterText);
                docsToAttach.Insert(0, ltrtItem.ToEmailAttachment("QuoteSchedule.pdf"));
                docsToAttach.Insert(0, hstItem.ToEmailAttachment("QuoteLetter.pdf"));
                ltrtItem.EmailReference = SendDocumentsToEmailHandler(usr, recipientContactId, subject, emailText, docsToAttach);
                hstItem.EmailReference = ltrtItem.EmailReference;
                Types.QuoteCombinedReport cmbQuoteReport = new Types.QuoteCombinedReport(this.OutputPath, hstItem, ltrtItem);
                ms = cmbQuoteReport.CreateDocument(this.LayoutPath, false);
                dbContext.Save(ltrtItem);
                dbContext.Save(hstItem);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendQuoteDocumentsByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "SendQuoteDocumentsByEmail", String.Format("Quote Id : {0}", quoteId), ex);
                throw (ex);
            }
            return ms;
        }
        #endregion
        #region Letter
        public MemoryStream OpenQuoteLetter(int recordId, bool createCopy, Types.Enums.DocDispatchMethodEnum dispatchMethod)
        {
            return OpenQuoteLetter(recordId, createCopy, dispatchMethod, "");
        }

        public MemoryStream OpenQuoteLetter(int recordId, bool createCopy, Types.Enums.DocDispatchMethodEnum dispatchMethod, string letterText)
        {
            //Get or Create the History item for the document
            DAL.QuoteHistoryItem historyContext = new QuoteHistoryItem(this.DatabaseName);
            Types.QuoteHistoryItem history = GetQuoteLetter(historyContext, recordId, dispatchMethod, createCopy, letterText);
            return history.Doc;
        }

        public Types.QuoteHistoryItem GetQuoteLetter(DAL.QuoteHistoryItem historyContext, int recordId, Types.Enums.DocDispatchMethodEnum dispatchMethod, bool enableCopy, string letterText = "")
        {
            try
            {
                Types.QuoteHistoryItem history = historyContext.GetDocument(Types.Enums.DocContentTypeEnum.Letter, recordId);
                bool isUpdated = false;

                //Check if there is already a document or if one has to be created
                if (history.Doc == null)
                {
                    //No document for this record exists so create it
                    if (letterText.Length == 0)
                        history.Doc = QuoteCreateDocument(recordId, false);
                    else
                        history.Doc = QuoteCreateDocument(recordId, letterText, false);

                    history.CreationDate = DateTime.Now;
                    isUpdated = true;
                }
                //Check if a copy of the document is to be created
                else if (enableCopy)
                {
                    history = QuoteCreateDuplicate(recordId, Types.Enums.DocContentTypeEnum.Letter, history, historyContext);
                    isUpdated = true;
                }
                //Check if the item needs to be saved
                if (isUpdated)
                {
                    history.DispatchMethod = dispatchMethod;

                    if (dispatchMethod == Types.Enums.DocDispatchMethodEnum.Email)
                        history.SentDate = DateTime.Today;

                    historyContext.Save(history);
                }
                return history;

            }
            catch (Exception ex)
            {
                //TODO: Log to log file
                Console.WriteLine("GetQuoteLetter error: {0}", ex.ToString());
                throw new Exception("BL Error in GetQuoteLetter. Unable to create history item.", ex);
            }
        }

        public Types.QuoteHistoryItem GetQuoteLetter(DAL.QuoteHistoryItem historyContext,Types.PdfSettings settings, Types.Enums.DocDispatchMethodEnum dispatchMethod)
        {
            try
            {
                Types.QuoteHistoryItem history = historyContext.GetDocument(Types.Enums.DocContentTypeEnum.Letter, settings.Id);
                bool isUpdated = false;

                //Check if there is already a document or if one has to be created
                if (history.Doc == null)
                {
                    //No document for this record exists so create it
                    history.Doc = QuoteCreateDocument(settings);
                    
                    history.CreationDate = DateTime.Now;
                    isUpdated = true;
                }
                //Check if a copy of the document is to be created
                else if (history.DispatchMethod != dispatchMethod)
                {
                    history = QuoteCreateDuplicate(settings.Id, Types.Enums.DocContentTypeEnum.Letter, history, historyContext);
                    isUpdated = true;
                }
                //Check if the item needs to be saved
                if (isUpdated)
                {
                    history.DispatchMethod = dispatchMethod;

                    if (dispatchMethod == Types.Enums.DocDispatchMethodEnum.Email)
                        history.SentDate = DateTime.Today;

                    historyContext.Save(history);
                }
                return history;

            }
            catch (Exception ex)
            {
                //TODO: Log to log file
                Console.WriteLine("GetQuoteLetter error: {0}", ex.ToString());
                throw new Exception("BL Error in GetQuoteLetter. Unable to create history item.", ex);
            }
        }
        #endregion
        #region Schedule
        /// <summary>
        /// Method to get or create a Quote schedule PDF document from a quote record to be opened in a browser window
        /// This method can throw an exception that must be handled
        /// </summary>
        /// <param name="recordId">int: The Id of the Quote record</param>
        /// <param name="outputPath">string: Where a PDF file should be saved</param>
        /// <param name="layoutPath">string: The path to the xml layout files</param>
        /// <param name="enableCopy">bool: flag to denote if a copy should be made if a document already exists</param>
        /// <param name="isEmail">bool: flag to denote if the delivery method is email</param>
        /// <returns>MemoryStream: The memory stream containing the PDF document</returns>
        public MemoryStream OpenQuoteSchedule(int recordId, bool createCopy, Types.Enums.DocDispatchMethodEnum dispatchMethod)
        {
            //Get or Create the History item for the document
            DAL.QuoteHistoryItem historyContext = new QuoteHistoryItem(this.DatabaseName);
            Types.QuoteHistoryItem history = GetQuoteSchedule(historyContext, recordId, dispatchMethod, createCopy);
            return history.Doc;
        }

        /// <summary>
        /// Method to get or create a Quote schedule from a quote record
        /// This method can throw an exception that must be handled
        /// </summary>
        /// <param name="recordId">int: The Id of the Quote record</param>
        /// <param name="outputPath">string: Where a PDF file should be saved</param>
        /// <param name="layoutPath">string: The path to the xml layout files</param>
        /// <param name="enableCopy">bool: flag to denote if a copy should be made if a document already exists</param>
        /// <returns>Types.QuoteHistoryItem that contains a memory stream of the PDF report</returns>
        public Types.QuoteHistoryItem GetQuoteSchedule(DAL.QuoteHistoryItem historyContext, int recordId, Types.Enums.DocDispatchMethodEnum dispatchMethod, bool enableCopy)
        {
            try
            {
                Types.QuoteHistoryItem history = historyContext.GetDocument(Types.Enums.DocContentTypeEnum.Schedule, recordId);
                bool isUpdated = false;

                //Check if there is already a document or if one has to be created
                if (history.Doc == null)
                {
                    //No document for this record exists so create it
                    history.Doc = QuoteCreateSchedule(recordId, false);
                    history.CreationDate = DateTime.Now;
                    isUpdated = true;
                }
                //Check if a copy of the document is to be created
                else if (enableCopy)
                {
                    history = QuoteCreateDuplicate(recordId, Types.Enums.DocContentTypeEnum.Schedule, history, historyContext);
                    isUpdated = true;
                }
                //Check if the item needs to be saved
                if (isUpdated)
                {
                    history.DispatchMethod = dispatchMethod;

                    if (dispatchMethod == Types.Enums.DocDispatchMethodEnum.Email)
                        history.SentDate = DateTime.Today;

                    historyContext.Save(history);
                }
                return history;

            }
            catch (Exception ex)
            {
                //TODO: Log to log file
                Console.WriteLine("CreateQuoteSchedule error: {0}", ex.ToString());
                throw new Exception("BL Error in GetQuoteSchedule. Unable to create history item.", ex);
            }
        }
        #endregion
        #region Combined
        public MemoryStream OpenCombinedQuoteDocuments(int recordId, bool createCopy, Types.Enums.DocDispatchMethodEnum dispatchMethod, bool enableCopy, string letterText = "")
        {
            DAL.QuoteHistoryItem dbContext = new QuoteHistoryItem(this.DatabaseName);

            Types.QuoteHistoryItem hstLetter = GetQuoteLetter(dbContext, recordId, dispatchMethod, enableCopy, letterText);
            Types.QuoteHistoryItem hstSchedule = GetQuoteSchedule(dbContext, recordId, dispatchMethod, enableCopy);

            Types.QuoteCombinedReport cmbReport = new Types.QuoteCombinedReport(this.OutputPath, hstLetter, hstSchedule);

            return cmbReport.CreateDocument(this.LayoutPath, false);
        }
        #endregion
        #endregion
        #region ***** Private *****
        private MemoryStream QuoteCreateSchedule(Types.PdfSettings settings)
        {
            Types.QuoteSchedule document = QuoteGetSchedule(settings); // rptContext.GetForQuoteRecId(recordId, this.OutputPath);
            return document.CreateDocument(this.LayoutPath, settings);
        }

        private Types.QuoteSchedule QuoteGetSchedule(Types.PdfSettings settings)
        {
            //Create the document
            DAL.QuoteSchedule rptContext = new QuoteSchedule(this.DatabaseName);
            Types.QuoteSchedule document = rptContext.GetForQuoteRecId(settings.Id, this.OutputPath);
            return document;
        }

        private Types.QuoteLetter QuoteCreateLetter(Types.PdfSettings settings)
        {
            //Create the document
            DAL.QuoteLetter rptContext = new QuoteLetter(this.DatabaseName);
            Types.QuoteLetter document = rptContext.GetForQuoteRecId(settings.Id, this.OutputPath);
            return document;
        }

        private MemoryStream QuoteCreateDocument(Types.PdfSettings settings)
        {
            ////Create the document
            Types.QuoteLetter document = QuoteCreateLetter(settings.Id);
            return document.CreateDocument(this.LayoutPath, settings);
        }

        /// <summary>
        /// Create a Quote schedule. The document will be created as a copy based on the flag
        /// </summary>
        /// <param name="recordId">int: The Quote record Id</param>
        /// <param name="isCopy">bool: Flag to say if the report is to be a copy</param>
        /// <returns></returns>
        private MemoryStream QuoteCreateSchedule(int recordId, bool isCopy)
        {
            //Create the document
            Types.QuoteSchedule document = QuoteCreateSchedule(recordId); // rptContext.GetForQuoteRecId(recordId, this.OutputPath);
            return document.CreateDocument(this.LayoutPath, isCopy);
        }

        private Types.QuoteSchedule QuoteCreateSchedule(int recordId)
        {
            //Create the document
            DAL.QuoteSchedule rptContext = new QuoteSchedule(this.DatabaseName);
            Types.QuoteSchedule document = rptContext.GetForQuoteRecId(recordId, this.OutputPath);
            return document;
        }

        private Types.QuoteLetter QuoteCreateLetter(int recordId)
        {
            //Create the document
            DAL.QuoteLetter rptContext = new QuoteLetter(this.DatabaseName);
            Types.QuoteLetter document = rptContext.GetForQuoteRecId(recordId, this.OutputPath);
            return document;
        }

        private MemoryStream QuoteCreateDocument(int recordId, bool isCopy)
        {
            ////Create the document
            Types.QuoteLetter document = QuoteCreateLetter(recordId);
            return document.CreateDocument(this.LayoutPath, isCopy);
        }

        private MemoryStream QuoteCreateDocument(int recordId, string letterText, bool isCopy)
        {
            //Create the document
            Types.QuoteLetter document = QuoteCreateLetter(recordId);
            return document.CreateDocument(letterText, this.LayoutPath, isCopy);
        }

        private Types.QuoteHistoryItem QuoteCreateDuplicate(int recId, Types.Enums.DocContentTypeEnum rptType, Types.QuoteHistoryItem objToDuplicate, DAL.QuoteHistoryItem historyContext)
        {
            Types.QuoteHistoryItem history = null;
            if (objToDuplicate.Doc != null)
                history = historyContext.CreateDupliacteDocument(objToDuplicate.Id, objToDuplicate.Doc.ToArray());
            else
            {
                MemoryStream ms = null;
                if (rptType == Types.Enums.DocContentTypeEnum.Schedule)
                    ms = QuoteCreateSchedule(recId, true);
                else
                    ms = QuoteCreateDocument(recId, true);

                history = historyContext.CreateDupliacteDocument(objToDuplicate.Id, ms.ToArray());
                try
                {
                    ms.Close();
                    ms.Dispose();
                }
                catch
                { }
            }
            return history;
        }
        #endregion
        #endregion
         * */
    }
}
