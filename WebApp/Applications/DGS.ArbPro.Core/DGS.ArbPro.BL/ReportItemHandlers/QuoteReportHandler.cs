﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

using DGS.ArbPro.DAL.Reporting.DBItems;

namespace DGS.ArbPro.BL
{
    using System.Net.Mail;
    using System.Web.Profile;

    using DGS.ArbPro.Types;
    using DGS.ArbPro.Types.Enums;

    using QuoteHistoryItem = DGS.ArbPro.DAL.QuoteHistoryItem;

    public class QuoteReportHandler: ReportHandlerBase
    {
        #region Members
        #endregion

        #region Constructors
        public QuoteReportHandler(string databaseName, string outputPath, string layoutPath) : base(databaseName, outputPath, layoutPath) { }
        #endregion

        #region Methods
        #region Static

        public static int SendEmailForAppointmentId(
            string database,
            string outputPath,
            string layoutPath,
            int appointmentId,
            int userId,
            bool sendSms)
        {
            //  Get the user for the user Id as this will be used by the emailing process
            var usrDbContext = new DAL.User("");
            var usr = usrDbContext.GetForMobileUserId(userId);

            //  Check if we have a valid user instance. 
            if (usr == null)
            {
                throw new Exception(string.Format("Unable to send email user Id {0} did not return a valid instance",userId));
            }

            //  Get the settings that are to be applied
            var settingContext = new DAL.MobileEmailSettings(database);
            var settings = settingContext.GetForAppointMentId(appointmentId);

            //  Check if we have a valid MobileEmailSettings instance. 
            if (settings == null)
            {
                throw new Exception(string.Format("Unable to send email MobileEmailSettings for database {0} and appointment {1} did not return a valid instance", database,appointmentId));
            }

            //  Create the PDF settings from the mobile settings
            var pdfSettings = new QuotePdfSettings();
            pdfSettings.ContactId = settings.ContactId;
            pdfSettings.GenerationEnum = DocGenerationEnum.Both;
            pdfSettings.IncludeAllSites = false;
            pdfSettings.ShowTotals = true;
            pdfSettings.ShownItemValues = true;
            pdfSettings.IsCopy = false;
            pdfSettings.RptType = ReportTypeEnum.Quote;
            pdfSettings.IsPreview = false;
            pdfSettings.TextBody = settings.DefaultLetterContent;
            pdfSettings.TemplateId = settings.QuoteLetterTemplateId;
            pdfSettings.Id = settings.QuoteId;
            pdfSettings.QuoteId = settings.QuoteId;

            //  Create the Email settings from the mobile settings
            var emailSettings = new EmailSettings();
            emailSettings.BodyText = settings.DefaultEmailBody;
            emailSettings.Subject = settings.DefaultEmailSubject;
            emailSettings.Recipients.Add(settings.ContactId);
            emailSettings.FromAddress = settings.FromAddress;
            if (sendSms)
            {
                emailSettings.SmsText = settings.DefaultSmsText;
            }

            //  Create an instance of the report handler
            var handler = new QuoteReportHandler(database, outputPath, layoutPath);

            //  Invoke the generation and send process
            var ms = handler.EmailDocuments(pdfSettings, emailSettings, usr, new List<UserDocument>());
            //  NOTE:   The previous call generates a memory stream. We do not require this but need to ensure that it is cleaned up
            ms.Dispose();

            //  Return the Id of the Quote record that has been sent
            return settings.QuoteId;

        }
        #endregion Static

        #region Public
        public MemoryStream GenerateDocumentDownload(Types.QuotePdfSettings settings, Types.User usr)
        {
            if (settings.IsPreview)
                return DownloadPreview(settings, usr);
            else
                return DownloadDocuments(settings, usr);
        }


        /// <summary>
        /// Method to load or create a letter and/or schedule and email them to the client
        /// </summary>
        /// <param name="pdfSettings">The settings to be applied to the creation of the document</param>
        /// <param name="emailSettings">The settings to be applied to the creation of the email</param>
        /// <param name="usr">The details of the user creating the documents</param>
        /// <param name="docsToAttach">The list of additional attachments to be added to the email</param>
        /// <returns>A MemoryStream containing the combined documents</returns>
        public MemoryStream EmailDocuments(Types.QuotePdfSettings pdfSettings, Types.EmailSettings emailSettings, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                switch (pdfSettings.GenerationEnum)
                {
                    case Types.Enums.DocGenerationEnum.Both:
                    //Both the letter and schedule are to be generated
                        ms = SendDocumentsByEmail(pdfSettings, emailSettings, usr, docsToAttach);
                        break;
                    case Types.Enums.DocGenerationEnum.Letter:
                        ms = SendLetterByEmail(pdfSettings, emailSettings, usr, docsToAttach);
                        break;
                    case Types.Enums.DocGenerationEnum.Schedule:
                        ms = SendScheduleByEmail(pdfSettings, emailSettings, usr, docsToAttach);
                        break;
                }
            }
            catch (Exception ex)
            {
                if (ex.GetBaseException() is SmtpFailedRecipientException)
                {
                    DAL.ErrorLogger.LogToDb(this.DatabaseName, "QuoteReportHandler.EmailDocuments", pdfSettings, ex);
                    Logger.ErrorFormat("Error in QuoteReportHandler.EmailDocuments: {0}", ex.ToString());
                    Debug.WriteLine("Error in QuoteReportHandler.EmailDocuments: {0}", ex.ToString());
                    //ms = null;
                }
                else
                {
                    DAL.ErrorLogger.LogToDb(this.DatabaseName, "QuoteReportHandler.EmailDocuments", pdfSettings, ex);
                    Logger.ErrorFormat("Error in QuoteReportHandler.EmailDocuments: {0}", ex.ToString());
                    Debug.WriteLine("Error in QuoteReportHandler.EmailDocuments: {0}", ex.ToString());
                    ms = null;
                }
            }
            return ms;
        }


        /// <summary>
        /// Method to load or create a letter and/or schedule and display them to the user for download
        /// and dispatch as letters
        /// </summary>
        /// <param name="settings">The settings to be applied to the creation of the document</param>
        /// <param name="usr">The details of the user creating the documents</param>
        /// <returns>A MemoryStream containing the combined documents</returns>
        public MemoryStream DownloadDocuments(Types.QuotePdfSettings settings, Types.User usr)
        {
            MemoryStream ms = null;
            try
            {
                DAL.QuoteHistoryItem dbContext = new DAL.QuoteHistoryItem(this.DatabaseName);
                switch (settings.GenerationEnum)
                {
                    case Types.Enums.DocGenerationEnum.Both:
                        //Both the letter and schedule are to be generated
                        ms = QuoteCombinedDownload(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter);
                        break;
                    case Types.Enums.DocGenerationEnum.Letter:
                        Types.QuoteHistoryItem letter = GetQuoteLetter(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter);
                        ms = letter.Doc;
                        break;
                    case Types.Enums.DocGenerationEnum.Schedule:
                        Types.QuoteHistoryItem schedule = GetQuoteSchedule(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter);
                        ms = schedule.Doc;
                        break;
                }
            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "QuoteReportHandler.DownloadDocuments", settings, ex);
                Debug.WriteLine("Error in QuoteReportHandler.DownloadDocuments: {0}", ex.ToString());
                Logger.ErrorFormat("Error in QuoteReportHandler.DownloadDocuments: {0}", ex.ToString());
                ms = null;
            }
            return ms;
        }

        public MemoryStream DownloadPreview(Types.QuotePdfSettings settings, Types.User usr)
        {
            MemoryStream ms = null;
            try
            {
                switch (settings.GenerationEnum)
                {
                    case Types.Enums.DocGenerationEnum.Both:
                        //Both the letter and schedule are to be generated
                        ms = QuoteCombinedPreview(settings);
                        break;
                    case Types.Enums.DocGenerationEnum.Letter:
                        ms = this.QuoteCreateLetter(settings);
                        break;
                    case Types.Enums.DocGenerationEnum.Schedule:
                        ms = this.QuoteCreateSchedule(settings);
                        break;
                    case Types.Enums.DocGenerationEnum.Example:
                        ms = QuoteCombinedPreviewExample(settings);
                        break;
                }
            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "QuoteReportHandler.DownloadDocuments", settings, ex);
                Debug.WriteLine("Error in QuoteReportHandler.DownloadDocuments: {0}", ex.ToString());
                Logger.ErrorFormat("Error in QuoteReportHandler.DownloadDocuments: {0}", ex.ToString());
                ms = null;
            }
            return ms;
        }
        #endregion
        #region Private
        #region Return types of MemoryStream
        private MemoryStream SendDocumentsByEmail(Types.QuotePdfSettings pdfSettings, Types.EmailSettings emailSettings, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            DAL.QuoteHistoryItem dbContext = new QuoteHistoryItem(this.DatabaseName);
            Types.QuoteHistoryItem ltrtItem = GetQuoteSchedule(dbContext, pdfSettings, Types.Enums.DocDispatchMethodEnum.Email);
            Types.QuoteHistoryItem hstItem = GetQuoteLetter(dbContext, pdfSettings, Types.Enums.DocDispatchMethodEnum.Email);
            docsToAttach.InsertWhenMissing(0, ltrtItem.ToEmailAttachment("QuoteSchedule.pdf"));
            docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("QuoteLetter.pdf"));
            ltrtItem.EmailReference = SendDocumentsToEmailHandler(usr, emailSettings, docsToAttach);
            hstItem.EmailReference = ltrtItem.EmailReference;
            Types.QuoteCombinedReport cmbQuoteReport = new Types.QuoteCombinedReport(this.OutputPath, hstItem, ltrtItem);
            MemoryStream ms = cmbQuoteReport.CreateDocument(this.LayoutPath, false);
            dbContext.Save(ltrtItem);
            dbContext.Save(hstItem);

            return ms;
        }

        private MemoryStream SendLetterByEmail(Types.QuotePdfSettings pdfSettings, Types.EmailSettings emailSettings, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            DAL.QuoteHistoryItem dbContext = new QuoteHistoryItem(this.DatabaseName);
            Types.QuoteHistoryItem hstItem = GetQuoteLetter(dbContext, pdfSettings, Types.Enums.DocDispatchMethodEnum.Email);
            docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("QuoteLetter.pdf"));
            hstItem.EmailReference = SendDocumentsToEmailHandler(usr, emailSettings, docsToAttach);
            dbContext.Save(hstItem);
            return hstItem.Doc;
        }

        private MemoryStream SendScheduleByEmail(Types.QuotePdfSettings pdfSettings, Types.EmailSettings emailSettings, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            DAL.QuoteHistoryItem dbContext = new QuoteHistoryItem(this.DatabaseName);
            Types.QuoteHistoryItem hstItem = GetQuoteSchedule(dbContext, pdfSettings, Types.Enums.DocDispatchMethodEnum.Email);
            docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("QuoteSchedule.pdf"));
            hstItem.EmailReference = SendDocumentsToEmailHandler(usr, emailSettings, docsToAttach);
            dbContext.Save(hstItem);
            return hstItem.Doc;
        }
        #endregion
        #region History Item return types
        private Types.QuoteHistoryItem GetQuoteLetter(DAL.QuoteHistoryItem dbContext, Types.QuotePdfSettings settings, Types.Enums.DocDispatchMethodEnum dispatchEnum)
        {
            try
            {
                Types.QuoteHistoryItem history = dbContext.GetDocument(Types.Enums.DocContentTypeEnum.Letter, settings.Id);
                bool isUpdated = false;

                //Check if there is already a document or if one has to be created
                if (history.Doc == null)
                {
                    //No document for this record exists so create it
                    history.Doc = QuoteCreateLetter(settings);
                    history.CreationDate = DateTime.Now;
                    isUpdated = true;
                }
                //Check if a copy of the document is to be created
                else if (history.DispatchMethod != dispatchEnum)
                {
                    history = QuoteCreateDuplicate(settings, settings.GenerationEnum, history, dbContext);
                    isUpdated = true;
                }
                //Check if the item needs to be saved
                if (isUpdated)
                {
                    history.DispatchMethod = dispatchEnum;

                    if (dispatchEnum == Types.Enums.DocDispatchMethodEnum.Email)
                        history.SentDate = DateTime.Today;

                    dbContext.Save(history);
                }
                return history;

            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "QuoteReportHandler.GetQuoteLetter", settings, ex);
                Debug.WriteLine("Error in QuoteReportHandler.GetQuoteLetter: {0}", ex.ToString());
                throw new Exception("BL Error in QuoteReportHandler.GetQuoteLetter. Unable to create history item.", ex);
            }
        }

        private Types.QuoteHistoryItem GetQuoteSchedule(DAL.QuoteHistoryItem dbContext, Types.QuotePdfSettings settings, Types.Enums.DocDispatchMethodEnum dispatchEnum)
        {
            try
            {
                Types.QuoteHistoryItem history = dbContext.GetDocument(Types.Enums.DocContentTypeEnum.Schedule, settings.Id);
                bool isUpdated = false;

                //Check if there is already a document or if one has to be created
                if (history.Doc == null)
                {
                    //No document for this record exists so create it
                    history.Doc = QuoteCreateSchedule(settings);
                    history.CreationDate = DateTime.Now;
                    isUpdated = true;
                }
                //Check if a copy of the document is to be created
                else if (history.DispatchMethod != dispatchEnum)
                {
                    history = QuoteCreateDuplicate(settings, Types.Enums.DocGenerationEnum.Schedule, history, dbContext);
                    isUpdated = true;
                }
                //Check if the item needs to be saved
                if (isUpdated)
                {
                    history.DispatchMethod = dispatchEnum;

                    if (dispatchEnum == Types.Enums.DocDispatchMethodEnum.Email)
                        history.SentDate = DateTime.Today;

                    dbContext.Save(history);
                }
                return history;

            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "QuoteReportHandler.GetQuoteSchedule", settings, ex);
                Debug.WriteLine("Error in QuoteReportHandler.GetQuoteSchedule: {0}", ex.ToString());
                throw new Exception("BL Error in QuoteReportHandler.GetQuoteSchedule. Unable to create history item.", ex);
            }
        }
        #endregion
        #region Document Creation
        private MemoryStream QuoteCombinedDownload(DAL.QuoteHistoryItem dbContext, Types.QuotePdfSettings settings, Types.Enums.DocDispatchMethodEnum dispatchEnum)
        {
            Types.QuoteCombinedReport cmbReport = new Types.QuoteCombinedReport(this.OutputPath, 
                GetQuoteLetter(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter),
                GetQuoteSchedule(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter));

            return cmbReport.CreateDocument(this.LayoutPath, settings);
        }
        /// <summary>
        /// Method to generate a combined letter and schedule PDF
        /// This Method should only be accessed if the generated PDF is to be a preview.
        /// No history is stored for preview items
        /// </summary>
        /// <param name="settings">The settings to be applied to the PDF generation</param>
        /// <returns>A memory stream containing a PDF generated from 2 seperate PDFs</returns>
        private MemoryStream QuoteCombinedPreview(Types.QuotePdfSettings settings)
        {
            //Get the report info from the database
            Types.QuoteLetter ltr = QuoteGetLetter(settings);
            Types.QuoteSchedule sch = QuoteGetSchedule(settings);
            //Create the documents
            Types.QuoteHistoryItem hstLtr = new Types.QuoteHistoryItem();
            hstLtr.Doc = ltr.CreateDocument(this.LayoutPath,settings);
            Types.QuoteHistoryItem hstSch = new Types.QuoteHistoryItem();
            hstSch.Doc = sch.CreateDocument(this.LayoutPath,settings);
            //Intialise the combined document
            Types.QuoteCombinedReport rpt = new Types.QuoteCombinedReport(this.OutputPath, hstLtr, hstSch);
            //Generate and return the combined report
            return rpt.CreateDocument(this.LayoutPath, settings);
        }

        /// <summary>
        /// Method to generate an example of a combined letter and schedule PDF
        /// This Method should only be accessed if the generated PDF is to be a preview AND example from the company screen.
        /// No history is stored for preview items
        /// </summary>
        /// <param name="settings">The settings to be applied to the PDF generation</param>
        /// <returns>A memory stream containing a PDF generated from 2 seperate PDFs</returns>
        private MemoryStream QuoteCombinedPreviewExample(Types.QuotePdfSettings settings)
        {
            //Get the report info from the database
            Types.QuoteLetter ltr = GetExampleQuote();
            Types.QuoteSchedule sch = GetExampleSchedule();
            //Create the documents
            Types.QuoteHistoryItem hstLtr = new Types.QuoteHistoryItem();
            hstLtr.Doc = ltr.CreateDocument(this.LayoutPath, settings);
            Types.QuoteHistoryItem hstSch = new Types.QuoteHistoryItem();
            hstSch.Doc = sch.CreateDocument(this.LayoutPath, settings);
            //Intialise the combined document
            Types.QuoteCombinedReport rpt = new Types.QuoteCombinedReport(this.OutputPath, hstLtr, hstSch);
            //Generate and return the combined report
            return rpt.CreateDocument(this.LayoutPath, settings);
        }

        /// <summary>
        /// Creates an example schedule to populate a report
        /// </summary>
        /// <returns></returns>
        private Types.QuoteSchedule GetExampleSchedule()
        {
            Types.QuoteSchedule schedule = new Types.QuoteSchedule(this.DatabaseName);
            schedule.AccountNo = "1234";
            schedule.ClientAddress1 = "Address Line 1";
            schedule.ClientAddress2 = "Address Line 2";
            schedule.ClientCounty = "County";
            schedule.ClientName = "Forename";
            schedule.ClientPostCode = "PO1 1AB";
            schedule.ClientStreetNo = "1";
            schedule.ClientTown = "Town";
            schedule.ContactFirstName = "Forename";
            schedule.ContactMobile = "07123456789";
            schedule.ContactSurname = "Surname";
            schedule.ContactTelephone = "0123456789";
            schedule.ContactTitle = "Mr";
            schedule.ClientID = 12345;
            schedule.QuoteSurveyor = "12345";
            schedule.SiteRef = "12345";
            schedule.QuoteSurveyor = "Surveyor";
            schedule.QuoteTotal = 123;
            schedule.QuoteTotalIncVat = 148;

            //set client site
            Types.ClientSite cs = new Types.ClientSite();
            cs.Address1 = "Address Line 1";
            cs.Address2 = "Address Line 2";
            cs.County = "Coutny";
            cs.HouseNameNo = "1";
            cs.Town = "Town";

            //set job site
            Types.JobSite js = new Types.JobSite();
            js.ClientName = "Example Client Name";
            js.Site = cs;
            schedule.JobSites.Add(js);

            //Set items 
            Types.QuoteItem item = new Types.QuoteItem();

            item.Name = "Example Item";
            item.Description = "Example description";
            item.Value = 123;
            item.VatRate = 20;
            schedule.QuoteItems.Add(item);

            //retrieve header and footer
            DAL.CustomGroup grp = new DAL.CustomGroup(this.DatabaseName);
            schedule.ReportCustomeGroups = grp.GetList();

            return schedule;
        }

        /// <summary>
        /// Creates an example quote to populate a report
        /// </summary>
        /// <returns></returns>
        private Types.QuoteLetter GetExampleQuote()
        {
            Types.QuoteLetter letter = new Types.QuoteLetter(this.DatabaseName);
            letter.AccountNo = "1234";
            letter.ClientAddress1 = "Address Line 1";
            letter.ClientAddress2 = "Address Line 2";
            letter.ClientCounty = "County";
            letter.ClientName = "Example Name";
            letter.ClientPostCode = "PO1 1AB";
            letter.ClientStreetNo = "1";
            letter.ClientTown = "Town";
            letter.ContactFirstName = "Forename";
            letter.ContactMobile = "07123456789";
            letter.ContactSurname = "Surname";
            letter.ContactTelephone = "0123456789";
            letter.ContactTitle = "Mr";
            letter.DirectorName = "Director Name";
            letter.LetterText = "This is an example quote letter and schedule";
            letter.ClientID = 12345;
            letter.QuoteSurveyor = "12345";
            letter.RecordId = 12345;
            letter.ReferenceNo = "12345";
            letter.SiteRef = "12345";

            //Retrieve the current company and set the letters signature
            DAL.Company DBContext = new DAL.Company(this.DatabaseName);
            Types.Company company = DBContext.Get();
            letter.LetterSignature = company.DirectorsSignature;

            //retrieve header and footer
            DAL.CustomGroup grp = new DAL.CustomGroup(this.DatabaseName);
            letter.ReportCustomeGroups = grp.GetList();

            return letter;
        }

        private MemoryStream QuoteCreateLetter(Types.QuotePdfSettings settings)
        {
            Types.QuoteLetter document = QuoteGetLetter(settings); // rptContext.GetForQuoteRecId(recordId, this.OutputPath);
            return document.CreateDocument(this.LayoutPath, settings);
        }

        private Types.QuoteLetter QuoteGetLetter(Types.QuotePdfSettings settings)
        {
            //Create the document
            DAL.QuoteLetter rptContext = new DAL.QuoteLetter(this.DatabaseName);
            Types.QuoteLetter document = rptContext.GetForQuoteRecId(settings.Id, this.OutputPath);
            return document;
        }

        private MemoryStream QuoteCreateSchedule(Types.QuotePdfSettings settings)
        {
            Types.QuoteSchedule document = QuoteGetSchedule(settings); // rptContext.GetForQuoteRecId(recordId, this.OutputPath);
            return document.CreateDocument(this.LayoutPath, settings);
        }

        private Types.QuoteSchedule QuoteGetSchedule(Types.QuotePdfSettings settings)
        {
            //Create the document
            DAL.QuoteSchedule rptContext = new DAL.QuoteSchedule(this.DatabaseName);
            Types.QuoteSchedule document = rptContext.GetForQuoteRecId(settings.Id, this.OutputPath);
            return document;
        }

        private Types.QuoteHistoryItem QuoteCreateDuplicate(Types.QuotePdfSettings settings, Types.Enums.DocGenerationEnum docType, Types.QuoteHistoryItem objToDuplicate, DAL.QuoteHistoryItem dbContext)
        {
            MemoryStream ms = null;
            settings.IsCopy = true;
            if (docType == Types.Enums.DocGenerationEnum.Schedule)
                ms = QuoteCreateSchedule(settings);
            else
                ms = QuoteCreateLetter(settings);

            Types.QuoteHistoryItem history = dbContext.CreateDupliacteDocument(objToDuplicate.Id, ms.ToArray());
            try
            {
                ms.Close();
                ms.Dispose();
            }
            catch
            { }
            return history;
        }
        #endregion
        #endregion
        #endregion
    }
}
