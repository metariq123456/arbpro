﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.DAL;

namespace DGS.ArbPro.BL
{
    public class CustomGroupHandler
    {
        public static Boolean UploadPDFGroupImage(String DatabaseName, String GroupName, Byte[] ImageBytes)
        {
            MemoryStream ms = new MemoryStream(ImageBytes);
            System.Drawing.Image Img = System.Drawing.Image.FromStream(ms);
            ms.Close();

            //MAX WIDTH: 794px / 210mm
            //MAX HEIGHT: 150px / 40mm

            var hPPI = Img.HorizontalResolution;
            var vPPI = Img.VerticalResolution;

            var iWidth = Img.Width / hPPI;
            var iHeight = Img.Height / vPPI;

            var delta = 210f / iWidth;

            var nWidth = iWidth * delta;
            var nHeight = iHeight * delta;

            if (nHeight > 40)
            {
                delta = 40 / iHeight;

                nWidth = iWidth * delta;
                nHeight = iHeight * delta;
            }

            PDFReport.ImageItem PDFImage = new PDFReport.ImageItem(0, 0, nWidth, nHeight);
            PDFImage.SetImage(ImageBytes);

            var cgDB = new DAL.CustomGroup(DatabaseName);
            var Group = cgDB.GetforName(GroupName);

            if (Group.Name == "Error")
            {
                Group = new PDFReport.CustomGroup(GroupName, nHeight);
                Group.Id = cgDB.Save(Group);
            }

            // NOTE: This actually caused an update to occur, but by the time the update happens, there are
            // no items to update. The update of the actual group itself deletes all of its associated images
            // so the subsequent update will always fail. Therefore, this code has been commented out with
            // the aim of being removed in a subsequent release after a test.
            /*foreach (var Item in Group.GetAllItems())
            {
                if (Item.GetType() == typeof(PDFReport.ImageItem))
                    PDFImage.Id = Item.Id;
            }*/

            PDFImage.GroupId = Group.Id;
            //NOTE: This will need to be modified when support for custom design is included
            //Clear all current items
            Group.ClearItems();
            Group.AddItem(PDFImage);


            //var iiDB = new DAL.ImageItem(DatabaseName);
            //iiDB.Save(PDFImage);

            ////now save the custom group incase the size has changed
            //cgDB.Save(Group);

            int retval = cgDB.SaveAll(Group);

            if (retval > 0)
            {
                return true;
            }
            return false;
        }

        public static Boolean ClearPDFGroupImage(String DatabaseName, String GroupName)
        {
            var retVal = false;
            var cgDB = new DAL.CustomGroup(DatabaseName);
            var Group = cgDB.GetforName(GroupName);

            if (Group.Name == "Error")
            {
                return retVal;
            }

            retVal = cgDB.ClearImage(Group.Id);

            return retVal;
        }

        //string _DbName = null;

        //public CustomGroupHandler(string databaseName)
        //{
        //    _DbName = databaseName;
        //}

        //public List<PDFReport.CustomGroup> GetCustomGroupsForClient()
        //{
        //    try
        //    {
        //        return CustomGroupDb.GetCustomGroupsForClient(_DbName);
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("CustomGroupHandler error: {0}", ex.ToString());
        //        return new List<PDFReport.CustomGroup>();
        //    }
        //}

        //public int UpdateCustomGroupsForClient(PDFReport.CustomGroup group)
        //{
        //    try
        //    {
        //        return CustomGroupDb.InsertOrUpdateCustomGroup(_DbName, group);
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("CustomGroupHandler error: {0}", ex.ToString());
        //        return -1;
        //    } 
        //}
    }
}
