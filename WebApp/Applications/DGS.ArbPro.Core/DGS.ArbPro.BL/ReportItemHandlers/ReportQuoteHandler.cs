﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DGS.ArbPro.DAL;
using DGS.ArbPro.DAL.Reporting.DBItems;
using PDFReport;
using System.Web;

namespace DGS.ArbPro.BL
{
    public class ReportQuoteHandler
    {
        #region members
        string _DatabaseName = "";
        string _OutputPath = "";
        string _layoutPath = "";
        #endregion

        #region properties
        #endregion

        #region Constructors
        public ReportQuoteHandler(string databseName, string outputPath, string layoutPath)
        {
            _DatabaseName = databseName;
            _OutputPath = outputPath;
            _layoutPath = layoutPath;
        }
        #endregion

        #region Methods
        #region Public
        public MemoryStream GenerateCombinedManualReport(int calendarId)
        {
            Report r = new Report();
            Types.QuoteManualEntry quoteManual = GetManualEntryQuote(calendarId);
            Types.QuoteManualEquipment quoteEntry = GetManualEquipmentQuote(calendarId);
            //Check if the objects have been created correctly
            if (quoteManual.Id < 1 || quoteEntry.Id < 1)
                return null;

            Types.QuoteCombinedManualReport cmbManualReprt = new Types.QuoteCombinedManualReport(_OutputPath);
            cmbManualReprt.ManualQuotePack.Add("quoteManualEntry", quoteManual.CreateDocument(_layoutPath, false));
            cmbManualReprt.ManualQuotePack.Add("quoteMapTools", quoteEntry.CreateDocument(_layoutPath, false));
            return cmbManualReprt.CreateDocument(_layoutPath, false);
        }
        #endregion
        #region Private
        private Types.QuoteManualEquipment GetManualEquipmentQuote(int id)
        {
            DAL.QuoteManualEquipment dbContext = new DAL.QuoteManualEquipment(_DatabaseName);
            Types.QuoteManualEquipment retVal = dbContext.GetForId(id, _OutputPath);

            return retVal;
        }

        private Types.QuoteManualEntry GetManualEntryQuote(int id)
        {
            DAL.QuoteManualEntry dbContext = new DAL.QuoteManualEntry(_DatabaseName);

            Types.QuoteManualEntry retVal = dbContext.GetForId(id, _OutputPath);
            return retVal;
        }
        #endregion
        #endregion


        //ReportQuoteDB Db = null;
        //CustomGroupHandler cgh = null;
        //string sFullLayoutPath = "";
        //string sFullOutputPath = "";
        //string sOrgID = "";

        //public ReportQuoteHandler(string sXMLReportLayoutsPATH, string sPDFReportPATH, string databaseName, string sReference)
        //{
        //    Db = new ReportQuoteDB(databaseName, Convert.ToInt32(sReference));
        //    cgh = new CustomGroupHandler(databaseName);
        //    sFullLayoutPath = sXMLReportLayoutsPATH;
        //    sFullOutputPath = sPDFReportPATH;

        //}

        //private string GetOutputFilenameFullPath(string sClientID, string sQuoteNo, string sType)
        //{
        //    return Path.Combine(sFullOutputPath, "C" + sClientID + "_Q" + sQuoteNo + "_" + sType + "_" + System.Guid.NewGuid() + ".pdf");
        //}

        //public MemoryStream CreateQuoteLetter(String BodyText, String Address)
        //{

        //    MemoryStream ms = new MemoryStream();

        //    ReportQuoteCoveringLetter rqcl = Db.CoveringLetter();
        //    rqcl.LetterText = BodyText;

        //    var Addrs = ReportHandler.ListAddressesForQuote(Db.DatabaseName ,Db.QuoteNo);
        //    var Addr = Addrs.First(a => a.ID == Convert.ToInt32(Address));

        //    rqcl.ContactTitle = Addr.ClientTitle;
        //    rqcl.ContactFirstName = "";
        //    rqcl.ContactSurname = Addr.BusinessName.Length == 0 ? Addr.ClientName : Addr.BusinessName;
        //    rqcl.CustomerAddress1 = Addr.Address1;
        //    rqcl.CustomerAddress2 = Addr.Address2;
        //    rqcl.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //    rqcl.CustomerAddressTown = Addr.Town;
        //    rqcl.CustomerAddressCounty = Addr.County;
        //    rqcl.CustomerAddressPostCode = Addr.PostCode;

        //    Report r = new Report();
        //    r.Filename = GetOutputFilenameFullPath(rqcl.ClientID.ToString(), rqcl.QuoteNo.ToString(), "QuoteLetter");

        //    CreateMyQuoteLetter(ref r, rqcl);

        //    r.SaveAndClose(ref ms);

        //    return ms;
        //}

        //public string CreateQuoteLetterAttachment(String BodyText, String Address)
        //{
        //    string sAttachmentName = "";

        //    ReportQuoteCoveringLetter rqcl = Db.CoveringLetter();
        //    rqcl.LetterText = BodyText;

        //    var Addrs = ReportHandler.ListAddressesForQuote(Db.DatabaseName, Db.QuoteNo);
        //    var Addr = Addrs.First(a => a.ID == Convert.ToInt32(Address));

        //    rqcl.ContactTitle = Addr.ClientTitle;
        //    rqcl.ContactFirstName = Addr.ClientName;
        //    rqcl.ContactSurname = "";
        //    rqcl.CustomerAddress1 = Addr.Address1;
        //    rqcl.CustomerAddress2 = Addr.Address2;
        //    rqcl.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //    rqcl.CustomerAddressTown = Addr.Town;
        //    rqcl.CustomerAddressCounty = Addr.County;
        //    rqcl.CustomerAddressPostCode = Addr.PostCode;

        //    Report r = new Report();
        //    r.Filename = GetOutputFilenameFullPath(rqcl.ClientID.ToString(), rqcl.QuoteNo.ToString(), "QuoteLetter");
        //    sAttachmentName = r.Filename;

        //    CreateMyQuoteLetter(ref r, rqcl);

        //    r.SaveAndClose();

        //    return sAttachmentName;
        //}

        //private void CreateMyQuoteLetter(ref Report r, ReportQuoteCoveringLetter rqcl)
        //{

        //    r.LoadLayout(Path.Combine(sFullLayoutPath, "QuoteLetter.xml"));

        //    foreach (CustomGroup cg in cgh.GetCustomGroupsForClient())
        //    {
        //        r.AddCustomGroup(cg);
        //    }

        //    r.PrintHeader();
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "QuoteAddressDateInfo.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "LetterSalutationSiteAddress.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "LetterMainText.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "LetterSignOffSig.xml"));

        //    string sCustAddr = "";

        //    if (rqcl.IsCommercial)
        //    {
        //        if (!string.IsNullOrWhiteSpace(rqcl.CustomerName))
        //        {
        //            sCustAddr += rqcl.CustomerName + Environment.NewLine;
        //        }
        //    }

        //    if (!string.IsNullOrWhiteSpace(rqcl.CustomerAddressHouseNameOrNumber))
        //    {
        //        sCustAddr += rqcl.CustomerAddressHouseNameOrNumber + ", ";
        //    }

        //    if (!string.IsNullOrWhiteSpace(rqcl.CustomerAddress1))
        //    {
        //        sCustAddr += rqcl.CustomerAddress1 + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(rqcl.CustomerAddress2))
        //    {
        //        sCustAddr += rqcl.CustomerAddress2 + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(rqcl.CustomerAddressTown))
        //    {
        //        sCustAddr += rqcl.CustomerAddressTown + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(rqcl.CustomerAddressCounty))
        //    {
        //        sCustAddr += rqcl.CustomerAddressCounty + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(rqcl.CustomerAddressPostCode))
        //    {
        //        sCustAddr += rqcl.CustomerAddressPostCode + Environment.NewLine;
        //    }

        //    string sCustName = "";

        //    if (!string.IsNullOrWhiteSpace(rqcl.ContactSurname))
        //    {
        //        sCustName += rqcl.ContactSurname;
        //        if (!string.IsNullOrWhiteSpace(rqcl.ContactFirstName))
        //        {
        //            sCustName = rqcl.ContactFirstName + " " + sCustName;
        //        }
        //        if (!string.IsNullOrWhiteSpace(rqcl.ContactTitle))
        //        {
        //            sCustName = rqcl.ContactTitle + " " + sCustName;
        //        }
        //    }

        //    r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = rqcl.QuoteDateSent.ToShortDateString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sCustName;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sCustAddr;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = rqcl.QuoteNo.ToString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = rqcl.ClientID.ToString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtSurveyor").Text = rqcl.QuoteSurveyor;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = rqcl.SiteRefNo;

        //    string sSiteAddr = rqcl.SiteAddress.Replace("\n", ", ");
        //    sSiteAddr = rqcl.SiteAddress.Replace(",,", ",");

        //    string sSalutation = "Dear ";
        //    if (string.IsNullOrWhiteSpace(rqcl.ContactTitle) || string.IsNullOrWhiteSpace(rqcl.ContactSurname))
        //    {
        //        sSalutation += "Sir/Madam";
        //    }
        //    else
        //    {
        //        sSalutation += rqcl.ContactTitle + " " + rqcl.ContactSurname;
        //    }

        //    r.Group("grpSalutationSiteAddress").FindTextItem("txtSalutation").Text = sSalutation;
        //    r.Group("grpSalutationSiteAddress").FindTextItem("txtSiteAddress").Text = sSiteAddr;

        //    r.PrintGroup(r.Group("grpAddressDateInfo"));
        //    r.PrintGroup(r.Group("grpSalutationSiteAddress"));

        //    r.Group("grpMainText").FindTextItem("txtMainText").Text = rqcl.LetterText.Trim(Environment.NewLine.ToCharArray());

        //    r.PrintGroup(r.Group("grpMainText"));

        //    r.Group("grpSignOffSig").FindImageItem("imgSig").SetImage(rqcl.LetterSignature, PDFReport.ResizePDFImage.KeepRatioWithinBox);

        //    r.Group("grpSignOffSig").FindTextItem("txtSigName").Text = rqcl.DirectorName;

        //    r.PrintGroup(r.Group("grpSignOffSig"));
        //}

        //public MemoryStream CreateQuoteSchedule(String Address)
        //{

        //    MemoryStream ms = new MemoryStream();

        //    ReportQuoteSchedule rqs = Db.Schedule();

        //    var Addrs = ReportHandler.ListAddressesForQuote(Db.DatabaseName, Db.QuoteNo);
        //    var Addr = Addrs.First(a => a.ID == Convert.ToInt32(Address));

        //    rqs.ContactTitle = Addr.ClientTitle;
        //    rqs.ContactFirstName = "";
        //    rqs.ContactSurname = Addr.BusinessName.Length == 0 ? Addr.ClientName : Addr.BusinessName;
        //    rqs.CustomerAddress1 = Addr.Address1;
        //    rqs.CustomerAddress2 = Addr.Address2;
        //    rqs.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //    rqs.CustomerAddressTown = Addr.Town;
        //    rqs.CustomerAddressCounty = Addr.County;
        //    rqs.CustomerAddressPostCode = Addr.PostCode;

        //    Report r = new Report();
        //    r.Filename = GetOutputFilenameFullPath(rqs.ClientID.ToString(), rqs.QuoteNo.ToString(), "QuoteSchedule");

        //    CreateMyQuoteSchedule(ref r, rqs);

        //    r.SaveAndClose(ref ms);

        //    return ms;
        //}

        //public string CreateQuoteScheduleAttachment(String Address)
        //{

        //    string sAttachmentName = "";

        //    ReportQuoteSchedule rqs = Db.Schedule();

        //    var Addrs = ReportHandler.ListAddressesForQuote(Db.DatabaseName, Db.QuoteNo);
        //    var Addr = Addrs.First(a => a.ID == Convert.ToInt32(Address));

        //    rqs.ContactTitle = Addr.ClientTitle;
        //    rqs.ContactFirstName = Addr.ClientName;
        //    rqs.ContactSurname = "";
        //    rqs.CustomerAddress1 = Addr.Address1;
        //    rqs.CustomerAddress2 = Addr.Address2;
        //    rqs.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //    rqs.CustomerAddressTown = Addr.Town;
        //    rqs.CustomerAddressCounty = Addr.County;
        //    rqs.CustomerAddressPostCode = Addr.PostCode;

        //    Report r = new Report();
        //    r.Filename = GetOutputFilenameFullPath(rqs.ClientID.ToString(), rqs.QuoteNo.ToString(), "QuoteSchedule");
        //    sAttachmentName = r.Filename;

        //    CreateMyQuoteSchedule(ref r, rqs);

        //    r.SaveAndClose();

        //    return sAttachmentName;
        //}

        //private void CreateMyQuoteSchedule(ref Report r, ReportQuoteSchedule rqs, Boolean AddHeader = true)
        //{

        //    r.LoadLayout(Path.Combine(sFullLayoutPath, "Schedule.xml"));

        //    foreach (CustomGroup cg in cgh.GetCustomGroupsForClient())
        //    {
        //        r.AddCustomGroup(cg);
        //    }

        //    if (AddHeader)
        //        r.PrintHeader();

        //    r.AddCustomGroup(HttpRuntime.AppDomainAppPath + "\\XML_Report_Layouts\\QuoteAddressDateInfoGreen.xml");
        //    r.AddCustomGroup(HttpRuntime.AppDomainAppPath + "\\XML_Report_Layouts\\SubjectSiteAddress.xml");
        //    r.AddCustomGroup(HttpRuntime.AppDomainAppPath + "\\XML_Report_Layouts\\ScheduleDetailsHdr.xml");
        //    r.AddCustomGroup(HttpRuntime.AppDomainAppPath + "\\XML_Report_Layouts\\ScheduleDetails.xml");
        //    r.AddCustomGroup(HttpRuntime.AppDomainAppPath + "\\XML_Report_Layouts\\ScheduleDetailTotals.xml");

        //    string sCustAddr = "";

        //    if (rqs.IsCommercial)
        //    {
        //        if (!string.IsNullOrWhiteSpace(rqs.CustomerName))
        //        {
        //            sCustAddr += rqs.CustomerName + Environment.NewLine;
        //        }
        //    }

        //    if (!string.IsNullOrWhiteSpace(rqs.CustomerAddressHouseNameOrNumber))
        //    {
        //        sCustAddr += rqs.CustomerAddressHouseNameOrNumber + ", ";
        //    }

        //    if (!string.IsNullOrWhiteSpace(rqs.CustomerAddress1))
        //    {
        //        sCustAddr += rqs.CustomerAddress1 + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(rqs.CustomerAddress2))
        //    {
        //        sCustAddr += rqs.CustomerAddress2 + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(rqs.CustomerAddressTown))
        //    {
        //        sCustAddr += rqs.CustomerAddressTown + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(rqs.CustomerAddressCounty))
        //    {
        //        sCustAddr += rqs.CustomerAddressCounty + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(rqs.CustomerAddressPostCode))
        //    {
        //        sCustAddr += rqs.CustomerAddressPostCode + Environment.NewLine;
        //    }

        //    string sCustName = "";

        //    if (!string.IsNullOrWhiteSpace(rqs.ContactSurname))
        //    {
        //        sCustName += rqs.ContactSurname;
        //        if (!string.IsNullOrWhiteSpace(rqs.ContactFirstName))
        //        {
        //            sCustName = rqs.ContactFirstName + " " + sCustName;
        //        }
        //        if (!string.IsNullOrWhiteSpace(rqs.ContactTitle))
        //        {
        //            sCustName = rqs.ContactTitle + " " + sCustName;
        //        }
        //    }

        //    r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = rqs.QuoteDateSent.ToShortDateString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sCustName;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sCustAddr;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = rqs.QuoteNo.ToString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = rqs.ClientID.ToString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtSurveyor").Text = rqs.QuoteSurveyor;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = rqs.SiteRefNo;

        //    string sSiteAddr = rqs.SiteAddress.Replace("\n", ", ");
        //    sSiteAddr = rqs.SiteAddress.Replace(",,", ",");

        //    r.Group("grpSubjectSiteAddress").FindTextItem("txtReportSubject").Text = "Schedule of Work";
        //    r.Group("grpSubjectSiteAddress").FindTextItem("txtSiteAddress").Text = sSiteAddr;

        //    r.PrintGroup(r.Group("grpAddressDateInfo"));
        //    r.PrintGroup(r.Group("grpSubjectSiteAddress"));

        //    bool bShowTotals = rqs.ShowValues;

        //    r.PrintGroup(r.Group("grpDetailHdr"));

        //    foreach (ReportQuoteItem rqi in rqs.QuoteItems)
        //    {
        //        r.Group("grpDetail").FindTextItem("txtItemNo").Text = rqi.TreeNo;
        //        r.Group("grpDetail").FindTextItem("txtItem").Text = rqi.TreeName;
        //        if (bShowTotals)
        //        {
        //        }
        //        else
        //        {
        //        }
        //        r.PrintGroup(r.Group("grpDetail"));
        //    }

        //    if (bShowTotals)
        //    {
        //        // Only display totals if e should
        //        r.Group("grpDetailTotals").FindTextItem("lblVatDesc").Text = "VAT (" + string.Format("{0:P0}", rqs.QuoteVatRate) + "): ";
        //        r.Group("grpDetailTotals").FindTextItem("txtTotValue").Text = string.Format("{0:c}", rqs.QuoteTotal);
        //        r.Group("grpDetailTotals").FindTextItem("txtVatValue").Text = string.Format("{0:c}", rqs.QuoteVatAmount);
        //        r.Group("grpDetailTotals").FindTextItem("txtTotVatValue").Text = string.Format("{0:c}", rqs.QuoteTotalIncVat);

        //        r.PrintGroup(r.Group("grpDetailTotals"));
        //    }
        //}

        //public MemoryStream CreateQuoteLetterSchedule(String BodyText, String Address)
        //{
        //    MemoryStream ms = new MemoryStream();

        //    ReportQuoteCoveringLetter rqcl = Db.CoveringLetter();
        //    ReportQuoteSchedule rqs = Db.Schedule();

        //    rqcl.LetterText = BodyText;

        //    var Addrs = ReportHandler.ListAddressesForQuote(Db.DatabaseName, Db.QuoteNo);
        //    var Addr = Addrs.First(a => a.ID == Convert.ToInt32(Address));

        //    rqcl.ContactTitle = Addr.ClientTitle;
        //    rqcl.ContactFirstName = "";
        //    rqcl.ContactSurname = Addr.BusinessName.Length == 0 ? Addr.ClientName : Addr.BusinessName;
        //    rqcl.CustomerAddress1 = Addr.Address1;
        //    rqcl.CustomerAddress2 = Addr.Address2;
        //    rqcl.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //    rqcl.CustomerAddressTown = Addr.Town;
        //    rqcl.CustomerAddressCounty = Addr.County;
        //    rqcl.CustomerAddressPostCode = Addr.PostCode;

        //    rqs.ContactTitle = Addr.ClientTitle;
        //    rqs.ContactFirstName = "";
        //    rqs.ContactSurname = Addr.BusinessName.Length == 0 ? Addr.ClientName : Addr.BusinessName;
        //    rqs.CustomerAddress1 = Addr.Address1;
        //    rqs.CustomerAddress2 = Addr.Address2;
        //    rqs.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //    rqs.CustomerAddressTown = Addr.Town;
        //    rqs.CustomerAddressCounty = Addr.County;
        //    rqs.CustomerAddressPostCode = Addr.PostCode;

        //    Report r = new Report();
        //    r.Filename = GetOutputFilenameFullPath(rqcl.ClientID.ToString(), rqcl.QuoteNo.ToString(), "QuoteLetterSchedule");

        //    CreateMyQuoteLetter(ref r, rqcl);
        //    r.NewPage();
        //    CreateMyQuoteSchedule(ref r, rqs, false);

        //    r.SaveAndClose(ref ms);

        //    return ms;
        //}
    }
}
