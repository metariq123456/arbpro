﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web.Configuration;
using DGS.ArbPro.DAL;
using Microsec.Elastic;

namespace DGS.ArbPro.BL.ReportItemHandlers
{
    using System.Runtime.InteropServices.WindowsRuntime;

    using DGS.ArbPro.BL.Exceptions;

    public class ReportEmailHandler
    {
        public struct EmailAttachment
        {
            public String AttachmentName;
            public Byte[] AttachmentBytes;
        }

        #region Static Members
        //static string AttachmentFolder = WebConfigurationManager.AppSettings["RootDocumentsPath"];
        #endregion

        #region Public
        #region Email History
        public static List<Types.EmailHistorySearchResult> GetElasticEmailHistory(string database, DateTime startDate, DateTime endDate)
        {
            List<Types.EmailHistorySearchResult> retVal = new List<Types.EmailHistorySearchResult>();
            try
            {
                //Get the company details
                DAL.Company cmpContext = new DAL.Company(database);
                Types.Company cmp = cmpContext.Get(false);
                //Get the elastic settings
                var baseURI = WebConfigurationManager.AppSettings["Elastic_BaseURI"];
                var apiKey = WebConfigurationManager.AppSettings["Elastic_ApiKey"];
                var username = WebConfigurationManager.AppSettings["Elastic_Username"];

                //Request the email history via the Elastic library
                List<ActivityLogEntry> emailEntries = ActivityLogEntry.List(baseURI,username,apiKey,cmp.Email, startDate, endDate);
                if (emailEntries == null || emailEntries.Count == 0)
                    return retVal;

                //The Activity log returned from the elastic wrapper must be converted to a ArbPro type to reduce coupling
                List<Types.EmailActivityLog> activity = new List<Types.EmailActivityLog>();

                foreach (ActivityLogEntry email in emailEntries)
                {
                    activity.Add(new Types.EmailActivityLog(email.TransactionId, email.Recipient, email.Sender, email.Status, email.SentDate));
                }

                //Now call the DAL to lint the activity with the corrisponding items
                DAL.EmailHistorySearchResult activityContext = new DAL.EmailHistorySearchResult(database);
                retVal = activityContext.GetForActivityLog(activity);
            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(database, "GetEmailHistory", string.Format("startDate: {0}, endDate: {1}", startDate.ToString(), endDate.ToString()), ex);
            }
            return retVal;
        }
        #endregion

        #region Send Email
        /// <summary>
        /// Static method to send  an email to a client contact.
        /// </summary>
        /// <param name="databaseName">string: The database name to connect to</param>
        /// <param name="recipientContactID">int: The id of the client contact the email is to be sent to</param>
        /// <param name="currentUser">Types.User: The user that initiated the request</param>
        /// <param name="csbject">string: The subject of the email</param>
        /// <param name="messageText">string: The text body of the email</param>
        /// <param name="docsToAttach">List of Types.UserDocument: Documents that are to be converted into an attachemnt and attached to the email</param>
        /// <returns>GUID of the relating elastic email</returns>
        public static string SendPDFEmail(String databaseName, Int32 recipientContactID, Types.User currentUser, String csbject, String messageText, List<Types.UserDocument> docsToAttach)
        {
            List<EmailAttachment> attachments = new List<EmailAttachment>();
            foreach (Types.UserDocument doc in docsToAttach)
            {
                if (doc.Name.Length > 0 && doc.Bytes != null && doc.Bytes.Length > 0)
                {
                    attachments.Add(new ReportEmailHandler.EmailAttachment()
                    {
                        AttachmentName = doc.Name,
                        AttachmentBytes = doc.Bytes
                    });
                }
            }

            return SendPDFEmail(databaseName, recipientContactID, currentUser, csbject, messageText, attachments);
        }

        /// <summary>
        /// Static method to send  an email to a list of individuals
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="recipients">List of string: The vlid list of email address that the emial is to be sent to</param>
        /// <param name="currentUser">Types.User: The user that initiated the request</param>
        /// <param name="csbject">string: The subject of the email</param>
        /// <param name="messageText">string: The text body of the email</param>
        /// <param name="docsToAttach">List of Types.UserDocument: Documents that are to be converted into an attachemnt and attached to the email</param>
        /// <returns>GUID of the relating elastic email</returns>
        public static string SendPDFEmail(String databaseName, List<string> recipients, Types.User currentUser, String csbject, String messageText, List<Types.UserDocument> docsToAttach)
        {
            List<EmailAttachment> attachments = new List<EmailAttachment>();
            foreach (Types.UserDocument doc in docsToAttach)
            {
                if (doc.Name.Length > 0 && doc.Bytes != null && doc.Bytes.Length > 0)
                {
                    attachments.Add(new ReportEmailHandler.EmailAttachment()
                    {
                        AttachmentName = doc.Name,
                        AttachmentBytes = doc.Bytes
                    });
                }
            }

            return SendPDFEmail(databaseName, recipients, currentUser, csbject, messageText, attachments);
        }

        /// <summary>
        /// Static method to send  an email to a client contact.
        /// </summary>
        /// <param name="databaseName">string: The database name to connect to</param>
        /// <param name="recipientContactID">int: The id of the client contact the email is to be sent to</param>
        /// <param name="currentUser">Types.User: The user that initiated the request</param>
        /// <param name="csbject">string: The subject of the email</param>
        /// <param name="messageText">string: The text body of the email</param>
        /// <returns>GUID of the relating elastic email</returns>
        public static String SendPDFEmail(String DatabaseName, Int32 RecipientContactID, Types.User CurrentUser, String Subject, String MessageText)
        {
            return SendPDFEmail(DatabaseName, RecipientContactID, CurrentUser, Subject, MessageText, new List<EmailAttachment>());
        }


        /// <summary>
        /// Static method to send  an email to a client contact.
        /// </summary>
        /// <param name="databaseName">string: The database name to connect to</param>
        /// <param name="recipientContactID">int: The id of the client contact the email is to be sent to</param>
        /// <param name="currentUser">Types.User: The user that initiated the request</param>
        /// <param name="csbject">string: The subject of the email</param>
        /// <param name="messageText">string: The text body of the email</param>
        /// <param name="Attachments">List of EmailAttachment: Documents that are to be attached to the email</param>
        /// <returns>GUID of the relating elastic email</returns>
        public static String SendPDFEmail(String DatabaseName, Int32 RecipientContactID, Types.User CurrentUser, String Subject, String MessageText, List<EmailAttachment> Attachments)
        {
            var ContactDB = new DAL.ClientContact(DatabaseName);
            var Recipient = ContactDB.GetForId(RecipientContactID);


            List<String> Recipients = new List<String>();
            Recipients.Add(String.Format("{0}<{1}>", Recipient.Forename + " " + Recipient.Surname, Recipient.Email));
            Recipients.Add(String.Format("{0}<{1}>", CurrentUser.FirstName + " " + CurrentUser.LastName, CurrentUser.EmailAddress));

            return SendPDFEmail(DatabaseName, Recipients, CurrentUser, Subject, MessageText, Attachments);
        }

        public static String SendPDFEmail(String DatabaseName, List<Int32> RecipientContactIDs, Types.User CurrentUser, String Subject, String MessageText, List<Types.UserDocument> docsToAttach)
        {
            var ContactDB = new DAL.ClientContact(DatabaseName);
            List<String> Recipients = new List<String>();
            foreach (int recipientId in RecipientContactIDs)
            {
                var recipient = ContactDB.GetForId(recipientId);
                if (recipient.Id > 0)
                {
                    Recipients.Add(String.Format("{0}<{1}>", recipient.Forename + " " + recipient.Surname, recipient.Email));
                }
            }
            Recipients.Add(String.Format("{0}<{1}>", CurrentUser.FirstName + " " + CurrentUser.LastName, CurrentUser.EmailAddress));

            List<EmailAttachment> attachments = new List<EmailAttachment>();
            foreach (Types.UserDocument doc in docsToAttach)
            {
                if (doc.Name.Length > 0 && doc.Bytes != null && doc.Bytes.Length > 0)
                {
                    attachments.Add(new ReportEmailHandler.EmailAttachment()
                    {
                        AttachmentName = doc.Name,
                        AttachmentBytes = doc.Bytes
                    });
                }
            }

            return SendPDFEmail(DatabaseName, Recipients, CurrentUser, Subject, MessageText, attachments);
        }

        /// <summary>
        /// Static method to send  an email
        /// </summary>
        /// <param name="databaseName">string: The database name to connect to</param>
        /// <param name="Recipients">List of string: The email addressess where the emails are to be sent</param>
        /// <param name="currentUser">Types.User: The user that initiated the request</param>
        /// <param name="csbject">string: The subject of the email</param>
        /// <param name="messageText">string: The text body of the email</param>
        /// <param name="Attachments">List of EmailAttachment: Documents that are to be attached to the email</param>
        /// <returns>GUID of the relating elastic email</returns>
        public static String SendPDFEmail(String DatabaseName, List<String> Recipients, Types.User CurrentUser, String Subject, String MessageText, List<EmailAttachment> Attachments)
        {

            var CompanyDB = new DAL.Company(DatabaseName);
            var Company = CompanyDB.Get();
            string AttachmentFolder = WebConfigurationManager.AppSettings["RootDocumentsPath"];
            //Get the company specific attachment folder
            string companyAttachments = Path.Combine(AttachmentFolder, Company.Name);
            string htmlReplacement = MessageText.Replace("\n", "<br/>");
            // TODO: DGSAP-843: Appending the message to the message html will cause line breaks and tabs to be ignored by html renderers.
            String MessageHTML = "<div><p>" + htmlReplacement + "</p><p>" + Company.DirectorsName + "</p></div>";

            var AttachmentInfo = new List<AttachmentInfo>();

            DAL.SmtpSettings dl = new DAL.SmtpSettings(DatabaseName);
            var SmtpEmail = dl.GetEmailSettings();
            var SmtpUser = dl.GetUserSettings(CurrentUser.Id);

            //Create the attachment directory as needed

            if (!Directory.Exists(companyAttachments))
                Directory.CreateDirectory(companyAttachments);

            switch (Company.FooterType)
            {
                case Types.Enums.EmailItemTypeEnum.None:
                    break;
                case Types.Enums.EmailItemTypeEnum.Text:
                    //TODO
                    break;
                case Types.Enums.EmailItemTypeEnum.Image:
                    if (Company.Footer != null && Company.Footer.Length > 0)
                    {
                        var ms = new MemoryStream(Company.Footer);
                        var Img = System.Drawing.Image.FromStream(ms); // TODO: Get Image Extension

                        MessageHTML += "<img src=\"Company.png\" />";
                        var Footer = File.OpenWrite(companyAttachments + "/" + Company.Name + "_Footer.png");
                        Footer.Write(Company.Footer, 0, Company.Footer.Length);
                        Footer.Close();
                        AttachmentInfo.Add(
                            new AttachmentInfo(
                                companyAttachments + "/" + Company.Name + "_Footer.png",
                                "Company.png"));
                    }
                    break;
            } 

            foreach (var Attachment in Attachments)
            {
                try
                {
                    var Doc = File.OpenWrite(companyAttachments + "/" + Attachment.AttachmentName);
                    Doc.Write(Attachment.AttachmentBytes, 0, Attachment.AttachmentBytes.Length);
                    Doc.Close();
                    AttachmentInfo.Add(
                        new AttachmentInfo(
                            companyAttachments + "/" + Attachment.AttachmentName,
                            Attachment.AttachmentName));
                }
                catch (Exception ex)
                {
                    ErrorLogger.LogToDb(DatabaseName, "SendPDFEmail", "File.Create('" + companyAttachments + "/" + Attachment.AttachmentName + "');", ex);
                }
            }

            var retVal = "";
            try
            {
                var MessageID = SendMessage(
                    Recipients,
                    Company.Email,
                    Company.DirectorsName,
                    Company.AccountsEmail,
                    Company.Name,
                    Subject,
                    MessageText,
                    MessageHTML,
                    AttachmentInfo,
                    SmtpEmail,
                    SmtpUser);
                retVal = MessageID;
            }
            catch (Exception ex)
            {
                ErrorLogger.LogToDb(DatabaseName, "SendMessage", "SendEmail", ex);
            }
            finally
            {
                foreach (var Attachment in AttachmentInfo)
                {
                    try
                    {
                        File.Delete(Attachment.SourceFilename);
                    }
                    catch (Exception ex)
                    {
                        ErrorLogger.LogToDb(
                            "DatabaseName",
                            "SendPDFEmail",
                            "File.Delete('" + companyAttachments + "/" + Attachment.SourceFilename + "');",
                            ex);
                    }
                }
            }


            return retVal;
        }

        public static String SendMessage(List<String> Recipients, String FromEmail, String FromName, String ReplyEmail, String ReplyName, String Subject, String MessageText, String MessageHTML, List<AttachmentInfo> Attachments, Types.SmtpSettings SmtpEmail, Types.SmtpSettings SmtpUser)
        {
            var BaseURI = WebConfigurationManager.AppSettings["Elastic_BaseURI"];
            var ApiKey = WebConfigurationManager.AppSettings["Elastic_ApiKey"];
            var Username = WebConfigurationManager.AppSettings["Elastic_Username"];
            var Client = new Microsec.Elastic.Client(BaseURI, Username, ApiKey, SmtpEmail.UseSmtp, SmtpEmail.EnableSSL, SmtpEmail.HostAddress, SmtpEmail.Port, SmtpUser.Username, SmtpUser.Password);
            return Client.SendEmail(String.Join(",", Recipients),
                Subject, MessageText, MessageHTML,
                FromEmail, FromName,
                ReplyEmail, ReplyName,
                Attachments);
        }

        /// <summary>
        /// DGSAP-855 - Static method to send  an SMS to a client contact.
        /// </summary>
        /// <param name="databaseName">string: The database name to connect to</param>
        /// <param name="recipientContactID">int: The id of the client contact the email is to be sent to</param>
        /// <param name="messageText">string: The text body of the email</param>
        /// <returns>GUID of the relating elastic email</returns>
        public static String SendPDFEmailAndSms(String DatabaseName, List<Int32> RecipientContactIDs, Types.User CurrentUser, String Subject, String MessageText, List<Types.UserDocument> docsToAttach, String SmsMessage)
        {
            var ContactDB = new DAL.ClientContact(DatabaseName);
            List<String> Recipients = new List<String>();
            List<String> MobileNumkbers = new List<String>();

            foreach (int recipientId in RecipientContactIDs)
            {
                var recipient = ContactDB.GetForId(recipientId);
                if (recipient.Id > 0)
                {
                    Recipients.Add(recipient.Email);
                }
                if (recipient.SendSmsMessage)
                {
                    MobileNumkbers.Add(recipient.Mobile);
                }
            }

            Recipients.Add(CurrentUser.EmailAddress);

            List<EmailAttachment> attachments = new List<EmailAttachment>();
            foreach (Types.UserDocument doc in docsToAttach)
            {
                if (doc.Name.Length > 0 && doc.Bytes != null && doc.Bytes.Length > 0)
                {
                    attachments.Add(new ReportEmailHandler.EmailAttachment()
                    {
                        AttachmentName = doc.Name,
                        AttachmentBytes = doc.Bytes
                    });
                }
            }
            var EmailGuid = SendPDFEmail(DatabaseName, Recipients, CurrentUser, Subject, MessageText, attachments);

            if (String.IsNullOrEmpty(EmailGuid))
            {
                throw new Exception("Email generation failed to return a valid Guid.");
            }

            if (!String.IsNullOrEmpty(SmsMessage) && SmsMessage.Length <= 160)
            {
                try
                {
                    SendText(MobileNumkbers, SmsMessage);
                }
                catch (Exception ex)
                {
                    throw new EmailException(EmailGuid, ex);
                }
            }
                return EmailGuid;
        }

        /// <summary>
        /// DGSAP-855 - Static method to send  an SMS to a client contact.
        /// </summary>
        /// <param name="databaseName">string: The database name to connect to</param>
        /// <param name="recipientContactID">int: The id of the client contact the email is to be sent to</param>
        /// <param name="CCEmailList">string: The cc email is to be sent to</param>
        /// <param name="messageText">string: The text body of the email</param>
        /// <returns>GUID of the relating elastic email</returns>
        public static String SendPDFEmailAndSms(String DatabaseName, List<Int32> RecipientContactIDs,List<string> CCEmailList ,Types.User CurrentUser, String Subject, String MessageText, List<Types.UserDocument> docsToAttach, String SmsMessage)
        {
            var ContactDB = new DAL.ClientContact(DatabaseName);
            List<String> Recipients = new List<String>();
            List<String> MobileNumkbers = new List<String>();

            foreach (int recipientId in RecipientContactIDs)
            {
                var recipient = ContactDB.GetForId(recipientId);
                if (recipient.Id > 0 && recipient.Email.Length >0)
                {
                    Recipients.Add(recipient.Email);
                }
                if (recipient.SendSmsMessage)
                {
                    MobileNumkbers.Add(recipient.Mobile);
                }
            }

            //Recipients.Add(CurrentUser.EmailAddress);
            if (CCEmailList.Count > 0)
            {
                foreach (string ccEmail in CCEmailList)
                {
                    if (ccEmail.Length > 0)
                    {
                        Recipients.Add(ccEmail);
                    }
                }
                
            }
            List<EmailAttachment> attachments = new List<EmailAttachment>();
            foreach (Types.UserDocument doc in docsToAttach)
            {
                if (doc.Name.Length > 0 && doc.Bytes != null && doc.Bytes.Length > 0)
                {
                    attachments.Add(new ReportEmailHandler.EmailAttachment()
                    {
                        AttachmentName = doc.Name,
                        AttachmentBytes = doc.Bytes
                    });
                }
            }
            if (Recipients.Count == 0)
            {
                throw new Exception("No Recipients email exist to send email.");
            }
            var EmailGuid = SendPDFEmail(DatabaseName, Recipients, CurrentUser, Subject, MessageText, attachments);
            
            if (String.IsNullOrEmpty(EmailGuid))
            {
                throw new Exception("Email generation failed to return a valid Guid.");
            }

            if (!String.IsNullOrEmpty(SmsMessage) && SmsMessage.Length <= 160)
            {
                try
                {
                    SendText(MobileNumkbers, SmsMessage);
                }
                catch (Exception ex)
                {
                    throw new EmailException(EmailGuid, ex);
                }
            }
            return EmailGuid;
        }
        public static String SendText(List<String> MobileNumbers, String SmsMessage)
        {
            string retval = "";
            foreach (string n in MobileNumbers)
            {
                retval = SendText(n, SmsMessage);
            }

            return retval;
        }

        public static String SendText(String MobileNumber, String SmsMessage)
        {
            var BaseURI = WebConfigurationManager.AppSettings["Elastic_BaseURI"];
            var ApiKey = WebConfigurationManager.AppSettings["Elastic_ApiKey"];
            var Username = WebConfigurationManager.AppSettings["Elastic_Username"];
            var Client = new Microsec.Elastic.Client(BaseURI, Username, ApiKey);
            return Client.SendSms(MobileNumber, SmsMessage);
        }

        #endregion
        #endregion

        #region Private

        #endregion
    }
}
