﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using DGS.ArbPro.DAL;
using DGS.ArbPro.DAL.Reporting.DBItems;
using PDFReport;
using System.Web;

namespace DGS.ArbPro.BL
{
    class ReportInvoiceHandler
    {
        //ReportInvoiceDB Db = null;
        //CustomGroupHandler cgh = null;
        //string sFullLayoutPath = "";
        //string sFullOutputPath = "";

        //public ReportInvoiceHandler(string sXMLReportLayoutsPATH, string sPDFReportPATH, string databaseName, string sReference)
        //{
        //    Db = new ReportInvoiceDB(databaseName, Convert.ToInt32(sReference));
        //    cgh = new CustomGroupHandler(databaseName);
        //    sFullLayoutPath = sXMLReportLayoutsPATH;
        //    sFullOutputPath = sPDFReportPATH;

        //}

        //private string GetOutputFilenameFullPath(string sClientID, string sQuoteNo, string sInvoiceNo, string sType)
        //{
        //    return Path.Combine(sFullOutputPath, "C" + sClientID + "_Q" + sQuoteNo + "_I" + sInvoiceNo + "_" + sType + "_" + System.Guid.NewGuid() + ".pdf");
        //}

        //public MemoryStream CreateInvoiceLetter(String BodyText, String AdditionalText, String Address)
        //{

        //    MemoryStream ms = new MemoryStream();

        //    try
        //    {
        //        ReportInvoiceLetter ril = Db.FirstLetter();
        //        string sReportSubject = "Invoice";
        //        string sLetterType = "";
        //        ril.LetterText = BodyText;
        //        ril.PaymentDetails = AdditionalText;

        //        var Addrs = ReportHandler.ListAddressesForInvoice(Db.DatabaseName, Db.InvoiceNo);
        //        var Addr = Addrs.First(a => a.ID == Convert.ToInt32(Address));

        //        ril.ContactTitle = Addr.ClientTitle;
        //        ril.ContactFirstName = "";
        //        ril.ContactSurname = Addr.BusinessName.Length == 0 ? Addr.ClientName : Addr.BusinessName;
        //        ril.CustomerAddress1 = Addr.Address1;
        //        ril.CustomerAddress2 = Addr.Address2;
        //        ril.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //        ril.CustomerAddressTown = Addr.Town;
        //        ril.CustomerAddressCounty = Addr.County;
        //        ril.CustomerAddressPostCode = Addr.PostCode;

        //        Report r = new Report();

        //        r.LoadLayout(Path.Combine(sFullLayoutPath, "InvoiceLetter.xml"));
        //        CreateMyInvoiceLetter(ref r, ref ril, ref sReportSubject, ref sLetterType);

        //        r.Filename = GetOutputFilenameFullPath(ril.ClientID.ToString(), ril.QuoteNo.ToString(), ril.InvoiceNo.ToString(), sLetterType);

        //        r.SaveAndClose(ref ms);
                
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return ms;
        //}

        //public string CreateInvoiceLetterAttachment(String BodyText, String AdditionalText, String Address)
        //{

        //    string sAttachmentName = "";

        //    MemoryStream ms = new MemoryStream();

        //    try
        //    {
        //        ReportInvoiceLetter ril = Db.FirstLetter();
        //        string sReportSubject = "Invoice";
        //        string sLetterType = "";
        //        ril.LetterText = BodyText;
        //        ril.PaymentDetails = AdditionalText;

        //        var Addrs = ReportHandler.ListAddressesForInvoice(Db.DatabaseName, Db.InvoiceNo);
        //        var Addr = Addrs.First(a => a.ID == Convert.ToInt32(Address));

        //        ril.ContactTitle = Addr.ClientTitle;
        //        ril.ContactFirstName = "";
        //        ril.ContactSurname = Addr.BusinessName.Length == 0 ? Addr.ClientName : Addr.BusinessName;
        //        ril.CustomerAddress1 = Addr.Address1;
        //        ril.CustomerAddress2 = Addr.Address2;
        //        ril.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //        ril.CustomerAddressTown = Addr.Town;
        //        ril.CustomerAddressCounty = Addr.County;
        //        ril.CustomerAddressPostCode = Addr.PostCode;

        //        Report r = new Report();

        //        r.LoadLayout(Path.Combine(sFullLayoutPath, "InvoiceLetter.xml"));
        //        CreateMyInvoiceLetter(ref r, ref ril, ref sReportSubject, ref sLetterType);

        //        r.Filename = GetOutputFilenameFullPath(ril.ClientID.ToString(), ril.QuoteNo.ToString(), ril.InvoiceNo.ToString(), sLetterType);
        //        sAttachmentName = r.Filename;

        //        r.SaveAndClose();

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return sAttachmentName;
        //}

        //private void CreateMyInvoiceLetter(ref Report r, ref ReportInvoiceLetter ril, ref string sReportSubject, ref string sLetterType)
        //{
        //    #region Groups
        //    foreach (CustomGroup cg in cgh.GetCustomGroupsForClient())
        //    {
        //        r.AddCustomGroup(cg);
        //    }

        //    r.PrintHeader();
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "InvoiceAddressDateInfo.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "SubjectSiteAddress.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "LetterMainText.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "InvoiceLetterSigSummary.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "InvoiceLetterPaymentDetails.xml"));
        //    #endregion

        //    #region CustAddr
        //    string sCustAddr = "";

        //    if (ril.IsCommercial)
        //    {
        //        if (!string.IsNullOrWhiteSpace(ril.CustomerName))
        //        {
        //            sCustAddr += ril.CustomerName + Environment.NewLine;
        //        }
        //    }

        //    if (!string.IsNullOrWhiteSpace(ril.CustomerAddressHouseNameOrNumber))
        //    {
        //        sCustAddr += ril.CustomerAddressHouseNameOrNumber + ", ";
        //    }

        //    if (!string.IsNullOrWhiteSpace(ril.CustomerAddress1))
        //    {
        //        sCustAddr += ril.CustomerAddress1 + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(ril.CustomerAddress2))
        //    {
        //        sCustAddr += ril.CustomerAddress2 + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(ril.CustomerAddressTown))
        //    {
        //        sCustAddr += ril.CustomerAddressTown + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(ril.CustomerAddressCounty))
        //    {
        //        sCustAddr += ril.CustomerAddressCounty + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(ril.CustomerAddressPostCode))
        //    {
        //        sCustAddr += ril.CustomerAddressPostCode + Environment.NewLine;
        //    }
        //    #endregion

        //    #region CustName
        //    string sCustName = "";

        //    if (!string.IsNullOrWhiteSpace(ril.ContactSurname))
        //    {
        //        sCustName += ril.ContactSurname;
        //        if (!string.IsNullOrWhiteSpace(ril.ContactFirstName))
        //        {
        //            sCustName = ril.ContactFirstName + " " + sCustName;
        //        }
        //        if (!string.IsNullOrWhiteSpace(ril.ContactTitle))
        //        {
        //            sCustName = ril.ContactTitle + " " + sCustName;
        //        }
        //    }
        //    #endregion

        //    #region DateInfo
        //    r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = ril.InvoiceDateSent.ToShortDateString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sCustName; 
        //    r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sCustAddr;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = ril.QuoteNo.ToString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtInvoiceNo").Text = ril.InvoiceNo.ToString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtOrderNo").Text = ril.OrderNo;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = ril.ClientID.ToString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = ril.SiteRefNo;
        //    #endregion

        //    string sSiteAddr = ril.SiteAddress.Replace("\n", ", ");
        //    sSiteAddr = ril.SiteAddress.Replace(",,", ",");

        //    r.Group("grpSubjectSiteAddress").FindTextItem("txtReportSubject").Text = sReportSubject;
        //    r.Group("grpSubjectSiteAddress").FindTextItem("txtSiteAddress").Text = sSiteAddr;

        //    r.PrintGroup(r.Group("grpAddressDateInfo"));
        //    r.PrintGroup(r.Group("grpSubjectSiteAddress"));

        //    r.Group("grpMainText").FindTextItem("txtMainText").Text = ril.LetterText.Trim(Environment.NewLine.ToCharArray());

        //    r.PrintGroup(r.Group("grpMainText"));

        //    r.Group("grpSigSummary").FindImageItem("imgSig").SetImage(ril.LetterSignature, PDFReport.ResizePDFImage.KeepRatioWithinBox);

        //    r.Group("grpSigSummary").FindTextItem("txtSigName").Text = ril.DirectorName;

        //    r.Group("grpSigSummary").FindTextItem("txtVatRate").Text = "(Rate " + string.Format("{0:P0}", ril.InvoiceVatRate) + ")";
        //    r.Group("grpSigSummary").FindTextItem("txtTotal").Text = string.Format("{0:c}", ril.InvoiceValue);
        //    r.Group("grpSigSummary").FindTextItem("txtVat").Text = string.Format("{0:c}", ril.InvoiceVatValue);
        //    r.Group("grpSigSummary").FindTextItem("txtTotalIncVat").Text = string.Format("{0:c}", ril.InvoiceTotalIncVat);
                        
        //    r.PrintGroup(r.Group("grpSigSummary"));

        //    r.Group("grpPaymentDetails").FindTextItem("txtPaymentDetailsText").Text = ril.PaymentDetails;

        //    r.PrintGroup(r.Group("grpPaymentDetails"));

        //}

        //public MemoryStream CreateInvoiceSchedule(String Address)
        //{

        //    MemoryStream ms = new MemoryStream();

        //    try
        //    {
        //        Report r = new Report();
        //        ReportInvoiceSchedule ris = Db.Schedule();

        //        var Addrs = ReportHandler.ListAddressesForInvoice(Db.DatabaseName, Db.InvoiceNo);
        //        var Addr = Addrs.First(a => a.ID == Convert.ToInt32(Address));

        //        ris.ContactTitle = Addr.ClientTitle;
        //        ris.ContactFirstName = "";
        //        ris.ContactSurname = Addr.BusinessName.Length == 0 ? Addr.ClientName : Addr.BusinessName;
        //        ris.CustomerAddress1 = Addr.Address1;
        //        ris.CustomerAddress2 = Addr.Address2;
        //        ris.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //        ris.CustomerAddressTown = Addr.Town;
        //        ris.CustomerAddressCounty = Addr.County;
        //        ris.CustomerAddressPostCode = Addr.PostCode;

        //        r.LoadLayout(Path.Combine(sFullLayoutPath, "Schedule.xml"));
        //        r.Filename = GetOutputFilenameFullPath(ris.ClientID.ToString(), ris.QuoteNo.ToString(), ris.InvoiceNo.ToString(), "InvoiceSchedule");

        //        CreateMyInvoiceSchedule(ref r, ref ris);

        //        r.SaveAndClose(ref ms);


        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return ms;
        //}

        //public string CreateInvoiceScheduleAttachment(String Address)
        //{

        //    string sAttachmentName = "";

        //    MemoryStream ms = new MemoryStream();

        //    try
        //    {
        //        Report r = new Report();
        //        ReportInvoiceSchedule ris = Db.Schedule();

        //        var Addrs = ReportHandler.ListAddressesForInvoice(Db.DatabaseName, Db.InvoiceNo);
        //        var Addr = Addrs.First(a => a.ID == Convert.ToInt32(Address));

        //        ris.ContactTitle = Addr.ClientTitle;
        //        ris.ContactFirstName = "";
        //        ris.ContactSurname = Addr.BusinessName.Length == 0 ? Addr.ClientName : Addr.BusinessName;
        //        ris.CustomerAddress1 = Addr.Address1;
        //        ris.CustomerAddress2 = Addr.Address2;
        //        ris.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //        ris.CustomerAddressTown = Addr.Town;
        //        ris.CustomerAddressCounty = Addr.County;
        //        ris.CustomerAddressPostCode = Addr.PostCode;

        //        r.LoadLayout(Path.Combine(sFullLayoutPath, "Schedule.xml"));
        //        r.Filename = GetOutputFilenameFullPath(ris.ClientID.ToString(), ris.QuoteNo.ToString(), ris.InvoiceNo.ToString(), "InvoiceSchedule");
        //        sAttachmentName = r.Filename;

        //        CreateMyInvoiceSchedule(ref r, ref ris);

        //        r.SaveAndClose();

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return sAttachmentName;
        //}

        //private void CreateMyInvoiceSchedule(ref Report r, ref ReportInvoiceSchedule ris, Boolean AddHeader = true)
        //{
        //    foreach (CustomGroup cg in cgh.GetCustomGroupsForClient())
        //    {
        //        r.AddCustomGroup(cg);
        //    }

        //    if (AddHeader)
        //        r.PrintHeader();

        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "InvoiceAddressDateInfo.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "SubjectSiteAddress.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "ScheduleDetailsHdr.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "ScheduleDetails.xml"));
        //    r.AddCustomGroup(Path.Combine(sFullLayoutPath, "ScheduleDetailTotals.xml"));

        //    string sCustAddr = "";

        //    if (ris.IsCommercial)
        //    {
        //        if (!string.IsNullOrWhiteSpace(ris.CustomerName))
        //        {
        //            sCustAddr += ris.CustomerName + Environment.NewLine;
        //        }
        //    }

        //    if (!string.IsNullOrWhiteSpace(ris.CustomerAddressHouseNameOrNumber))
        //    {
        //        sCustAddr += ris.CustomerAddressHouseNameOrNumber + ", ";
        //    }

        //    if (!string.IsNullOrWhiteSpace(ris.CustomerAddress1))
        //    {
        //        sCustAddr += ris.CustomerAddress1 + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(ris.CustomerAddress2))
        //    {
        //        sCustAddr += ris.CustomerAddress2 + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(ris.CustomerAddressTown))
        //    {
        //        sCustAddr += ris.CustomerAddressTown + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(ris.CustomerAddressCounty))
        //    {
        //        sCustAddr += ris.CustomerAddressCounty + Environment.NewLine;
        //    }
        //    if (!string.IsNullOrWhiteSpace(ris.CustomerAddressPostCode))
        //    {
        //        sCustAddr += ris.CustomerAddressPostCode + Environment.NewLine;
        //    }

        //    string sCustName = "";

        //    if (!string.IsNullOrWhiteSpace(ris.ContactSurname))
        //    {
        //        sCustName += ris.ContactSurname;
        //        if (!string.IsNullOrWhiteSpace(ris.ContactFirstName))
        //        {
        //            sCustName = ris.ContactFirstName + " " + sCustName;
        //        }
        //        if (!string.IsNullOrWhiteSpace(ris.ContactTitle))
        //        {
        //            sCustName = ris.ContactTitle + " " + sCustName;
        //        }
        //    }

        //    r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = ris.InvoiceDateSent.ToShortDateString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sCustName;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sCustAddr;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = ris.QuoteNo.ToString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtInvoiceNo").Text = ris.InvoiceNo.ToString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtOrderNo").Text = ris.OrderNo;
        //    r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = ris.ClientID.ToString();
        //    r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = ris.SiteRefNo;

        //    string sSiteAddr = ris.SiteAddress.Replace("\n", ", ");
        //    sSiteAddr = ris.SiteAddress.Replace(",,", ",");

        //    r.Group("grpSubjectSiteAddress").FindTextItem("txtReportSubject").Text = "Invoice";
        //    r.Group("grpSubjectSiteAddress").FindTextItem("txtSiteAddress").Text = sSiteAddr;

        //    r.PrintGroup(r.Group("grpAddressDateInfo"));
        //    r.PrintGroup(r.Group("grpSubjectSiteAddress"));

        //    r.PrintGroup(r.Group("grpDetailHdr"));

        //    foreach (ReportInvoiceItem rii in ris.InvoiceItems)
        //    {
        //        r.Group("grpDetail").FindTextItem("txtItemNo").Text = rii.TreeNo;
        //        r.Group("grpDetail").FindTextItem("txtItem").Text = rii.TreeName;
        //        r.Group("grpDetail").FindTextItem("txtDesc").Text = rii.Description;

        //        if (rii.Value == 0)
        //        {
        //            r.Group("grpDetail").FindTextItem("txtValue").Text = "";
        //        }
        //        else
        //        {
        //            r.Group("grpDetail").FindTextItem("txtValue").Text = string.Format("{0:c}", rii.Value);
        //        }
        //        r.PrintGroup(r.Group("grpDetail"));

        //    }

        //    r.Group("grpDetailTotals").FindTextItem("lblVatDesc").Text = "VAT (" + string.Format("{0:P0}", ris.QuoteVatRate) + "): ";
        //    r.Group("grpDetailTotals").FindTextItem("txtTotValue").Text = string.Format("{0:c}", ris.QuoteTotal);
        //    r.Group("grpDetailTotals").FindTextItem("txtVatValue").Text = string.Format("{0:c}", ris.QuoteVatAmount);
        //    r.Group("grpDetailTotals").FindTextItem("txtTotVatValue").Text = string.Format("{0:c}", ris.QuoteTotalIncVat);

        //    r.PrintGroup(r.Group("grpDetailTotals"));
        //}

        //public MemoryStream CreateInvoiceLetterSchedule(String BodyText, String AdditionalText, String Address)
        //{
        //    MemoryStream ms = new MemoryStream();

        //    try
        //    {
        //        ReportInvoiceLetter ril = Db.FirstLetter();
        //        string sReportSubject = "Invoice";
        //        string sLetterType = "";
        //        ril.LetterText = BodyText;
        //        ril.PaymentDetails = AdditionalText;
        //        ReportInvoiceSchedule ris = Db.Schedule();

        //        var Addrs = ReportHandler.ListAddressesForInvoice(Db.DatabaseName, Db.InvoiceNo);
        //        var Addr = Addrs.First(a => a.ID == Convert.ToInt32(Address));

        //        ril.ContactTitle = Addr.ClientTitle;
        //        ril.ContactFirstName = "";
        //        ril.ContactSurname = Addr.BusinessName.Length == 0 ? Addr.ClientName : Addr.BusinessName;
        //        ril.CustomerAddress1 = Addr.Address1;
        //        ril.CustomerAddress2 = Addr.Address2;
        //        ril.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //        ril.CustomerAddressTown = Addr.Town;
        //        ril.CustomerAddressCounty = Addr.County;
        //        ril.CustomerAddressPostCode = Addr.PostCode;

        //        ris.ContactTitle = Addr.ClientTitle;
        //        ris.ContactFirstName = "";
        //        ris.ContactSurname = Addr.BusinessName.Length == 0 ? Addr.ClientName : Addr.BusinessName;
        //        ris.CustomerAddress1 = Addr.Address1;
        //        ris.CustomerAddress2 = Addr.Address2;
        //        ris.CustomerAddressHouseNameOrNumber = Addr.HouseNameOrNo;
        //        ris.CustomerAddressTown = Addr.Town;
        //        ris.CustomerAddressCounty = Addr.County;
        //        ris.CustomerAddressPostCode = Addr.PostCode;

        //        Report r = new Report();

        //        CreateMyInvoiceLetter(ref r, ref ril, ref sReportSubject, ref sLetterType);
        //        r.NewPage();
        //        CreateMyInvoiceSchedule(ref r, ref ris, false);

        //        r.Filename = GetOutputFilenameFullPath(ril.ClientID.ToString(), ril.QuoteNo.ToString(), ril.InvoiceNo.ToString(), sLetterType);

        //        r.SaveAndClose(ref ms);

        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return ms;
        //}
    }
}
