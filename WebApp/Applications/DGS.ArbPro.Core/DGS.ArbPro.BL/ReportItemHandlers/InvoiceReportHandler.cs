﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using DGS.ArbPro.DAL;
using DGS.ArbPro.DAL.Reporting.DBItems;

namespace DGS.ArbPro.BL
{
    public class InvoiceReportHandler: ReportHandlerBase
    {
        #region Members
        #endregion

        #region Constructors
        public InvoiceReportHandler(string databaseName, string outputPath, string layoutPath) : base(databaseName, outputPath, layoutPath) { }
        #endregion

        #region Methods
        #region Public
        public MemoryStream GenerateDocumentDownload(Types.InvoicePdfSettings settings, Types.User usr)
        {
            if (settings.IsPreview)
                return DownloadPreview(settings, usr);
            else
                return DownloadDocuments(settings, usr);
        }

        /// <summary>
        /// Method to load or create a letter and/or schedule and email them to the client
        /// </summary>
        /// <param name="pdfSettings">The settings to be applied to the creation of the document</param>
        /// <param name="emailSettings">The settings to be applied to the creation of the email</param>
        /// <param name="usr">The details of the user creating the documents</param>
        /// <param name="docsToAttach">The list of additional attachments to be added to the email</param>
        /// <returns>A MemoryStream containing the combined documents</returns>
        public MemoryStream EmailDocuments(Types.InvoicePdfSettings pdfSettings, Types.EmailSettings emailSettings, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                switch (pdfSettings.GenerationEnum)
                {
                    case Types.Enums.InvoiceDocTypeEnum.Letter_And_Schedule:
                    //Both the letter and schedule are to be generated
                        ms = SendDocumentsByEmail(pdfSettings, emailSettings, usr, docsToAttach);
                        break;
                    case Types.Enums.InvoiceDocTypeEnum.Letter:
                    case Types.Enums.InvoiceDocTypeEnum.First_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Second_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Final_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Legal_Action:
                    case Types.Enums.InvoiceDocTypeEnum.Legal_Action_2:
                        ms = SendLetterByEmail(pdfSettings, emailSettings, usr, docsToAttach);
                        break;
                    case Types.Enums.InvoiceDocTypeEnum.Schedule:
                        ms = SendScheduleByEmail(pdfSettings, emailSettings, usr, docsToAttach);
                        break;
                }
            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "InvoiceReportHandler.EmailDocuments", pdfSettings, ex);
                Logger.ErrorFormat("Error in InvoiceReportHandler.EmailDocuments: {0}", ex.ToString());
                Debug.WriteLine("Error in InvoiceReportHandler.EmailDocuments: {0}", ex.ToString());
                ms = null;
            }
            return ms;
        }

        /// <summary>
        /// Method to load or create a letter and/or schedule and display them to the user for download
        /// and dispatch as letters
        /// </summary>
        /// <param name="settings">The settings to be applied to the creation of the document</param>
        /// <param name="usr">The details of the user creating the documents</param>
        /// <returns>A MemoryStream containing the combined documents</returns>
        public MemoryStream DownloadDocuments(Types.InvoicePdfSettings settings, Types.User usr)
        {
            MemoryStream ms = null;
            try
            {
                DAL.InvoiceHistoryItem dbContext = new DAL.InvoiceHistoryItem(this.DatabaseName);
                switch (settings.GenerationEnum)
                {
                    case Types.Enums.InvoiceDocTypeEnum.Letter_And_Schedule:
                        //Both the letter and schedule are to be generated
                        ms = InvoiceCombinedDownload(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter);
                        break;
                    case Types.Enums.InvoiceDocTypeEnum.Letter:
                    case Types.Enums.InvoiceDocTypeEnum.First_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Second_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Final_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Legal_Action:
                    case Types.Enums.InvoiceDocTypeEnum.Legal_Action_2:
                        Types.InvoiceHistoryItem letter = GetInvoiceLetter(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter);
                        ms = letter.Doc;
                        break;
                    case Types.Enums.InvoiceDocTypeEnum.Schedule:
                        Types.InvoiceHistoryItem schedule = GetInvoiceSchedule(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter);
                        ms = schedule.Doc;
                        break;
                }
            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "InvoiceReportHandler.DownloadDocuments", settings, ex);
                Debug.WriteLine("Error in InvoiceReportHandler.DownloadDocuments: {0}", ex.ToString());
                Logger.ErrorFormat("Error in InvoiceReportHandler.DownloadDocuments: {0}", ex.ToString());
                ms = null;
            }
            return ms;
        }

        public MemoryStream DownloadPreview(Types.InvoicePdfSettings settings, Types.User usr)
        {
            MemoryStream ms = null;
            try
            {
                switch (settings.GenerationEnum)
                {
                    case Types.Enums.InvoiceDocTypeEnum.Letter_And_Schedule:
                        //Both the letter and schedule are to be generated
                        ms = InvoiceCombinedPreview(settings);
                        break;
                    case Types.Enums.InvoiceDocTypeEnum.Letter:
                    case Types.Enums.InvoiceDocTypeEnum.First_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Second_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Final_Reminder:
                    case Types.Enums.InvoiceDocTypeEnum.Legal_Action:
                    case Types.Enums.InvoiceDocTypeEnum.Legal_Action_2:
                        ms = this.InvoiceCreateLetter(settings);
                        break;
                    case Types.Enums.InvoiceDocTypeEnum.Schedule:
                        ms = this.InvoiceCreateSchedule(settings);
                        break;
                }
            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "InvoiceReportHandler.DownloadDocuments", settings, ex);
                Debug.WriteLine("Error in InvoiceReportHandler.DownloadDocuments: {0}", ex.ToString());
                Logger.ErrorFormat("Error in InvoiceReportHandler.DownloadDocuments: {0}", ex.ToString());
                ms = null;
            }
            return ms;
        }
        #endregion
        #region Private
        #region Return types of MemoryStream
        private MemoryStream SendDocumentsByEmail(Types.InvoicePdfSettings pdfSettings, Types.EmailSettings emailSettings, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            DAL.InvoiceHistoryItem dbContext = new InvoiceHistoryItem(this.DatabaseName);
            Types.InvoiceHistoryItem ltrtItem = GetInvoiceSchedule(dbContext, pdfSettings, Types.Enums.DocDispatchMethodEnum.Email);
            Types.InvoiceHistoryItem hstItem = GetInvoiceLetter(dbContext, pdfSettings, Types.Enums.DocDispatchMethodEnum.Email);
            docsToAttach.InsertWhenMissing(0, ltrtItem.ToEmailAttachment("InvoiceSchedule.pdf"));
            docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("InvoiceLetter.pdf"));
            ltrtItem.EmailReference = SendDocumentsToEmailHandler(usr, emailSettings, docsToAttach);
            hstItem.EmailReference = ltrtItem.EmailReference;
            Types.InvoiceCombinedReport cmbQuoteReport = new Types.InvoiceCombinedReport(this.OutputPath, hstItem, ltrtItem);
            MemoryStream ms = cmbQuoteReport.CreateDocument(this.LayoutPath, false);
            dbContext.Save(ltrtItem);
            dbContext.Save(hstItem);

            return ms;
        }

        private MemoryStream SendLetterByEmail(Types.InvoicePdfSettings pdfSettings, Types.EmailSettings emailSettings, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            DAL.InvoiceHistoryItem dbContext = new InvoiceHistoryItem(this.DatabaseName);
            Types.InvoiceHistoryItem hstItem = GetInvoiceLetter(dbContext, pdfSettings, Types.Enums.DocDispatchMethodEnum.Email);
            docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("InvoiceLetter.pdf"));
            hstItem.EmailReference = SendDocumentsToEmailHandler(usr, emailSettings, docsToAttach);
            dbContext.Save(hstItem);
            return hstItem.Doc;
        }

        private MemoryStream SendScheduleByEmail(Types.InvoicePdfSettings pdfSettings, Types.EmailSettings emailSettings, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            DAL.InvoiceHistoryItem dbContext = new InvoiceHistoryItem(this.DatabaseName);
            Types.InvoiceHistoryItem hstItem = GetInvoiceSchedule(dbContext, pdfSettings, Types.Enums.DocDispatchMethodEnum.Email);
            docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("InvoiceLetter.pdf"));
            hstItem.EmailReference = SendDocumentsToEmailHandler(usr, emailSettings, docsToAttach);
            dbContext.Save(hstItem);
            return hstItem.Doc;
        }
        #endregion
        #region History Item return types
        private Types.InvoiceHistoryItem GetInvoiceLetter(DAL.InvoiceHistoryItem dbContext, Types.InvoicePdfSettings settings, Types.Enums.DocDispatchMethodEnum dispatchEnum)
        {
            try
            {
                Types.InvoiceHistoryItem history = dbContext.GetDocument(settings.GenerationEnum, settings.Id);
                bool isUpdated = false;

                //Check if there is already a document or if one has to be created
                if (history.Doc == null)
                {
                    //No document for this record exists so create it
                    history.Doc = InvoiceCreateLetter(settings);
                    history.CreationDate = DateTime.Now;
                    isUpdated = true;
                }
                //Check if a copy of the document is to be created
                else if (history.DispatchMethod != dispatchEnum)
                {
                    history = InvoiceCreateDuplicate(
                        settings,
                        Types.Enums.InvoiceDocTypeEnum.Letter,
                        history,
                        dbContext);
                    isUpdated = true;
                }
                else
                {
                    // update document for re-send purposes but do not save date or document itself
                    history.Doc = InvoiceCreateLetter(settings);
                }

                //Check if the item needs to be saved
                if (isUpdated)
                {
                    history.DispatchMethod = dispatchEnum;

                    if (dispatchEnum == Types.Enums.DocDispatchMethodEnum.Email)
                        history.SentDate = DateTime.Today;

                    dbContext.Save(history);
                }
                return history;

            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "InvoiceReportHandler.GetInvoiceLetter", settings, ex);
                Debug.WriteLine("Error in InvoiceReportHandler.GetInvoiceLetter: {0}", ex.ToString());
                throw new Exception("BL Error in InvoiceReportHandler.GetInvoiceLetter. Unable to create history item.", ex);
            }
        }

        private Types.InvoiceHistoryItem GetInvoiceSchedule(DAL.InvoiceHistoryItem dbContext, Types.InvoicePdfSettings settings, Types.Enums.DocDispatchMethodEnum dispatchEnum)
        {
            try
            {
                Types.InvoiceHistoryItem history = dbContext.GetDocument(Types.Enums.InvoiceDocTypeEnum.Schedule, settings.Id);
                bool isUpdated = false;

                //Check if there is already a document or if one has to be created
                if (history.Doc == null)
                {
                    //No document for this record exists so create it
                    history.Doc = InvoiceCreateSchedule(settings);
                    history.CreationDate = DateTime.Now;
                    isUpdated = true;
                }
                //Check if a copy of the document is to be created
                else if (history.DispatchMethod != dispatchEnum)
                {
                    history = InvoiceCreateDuplicate(
                        settings,
                        Types.Enums.InvoiceDocTypeEnum.Schedule,
                        history,
                        dbContext);
                    isUpdated = true;
                }
                else
                {
                    // update document for re-send purposes but do not save date or document itself
                    history.Doc = InvoiceCreateSchedule(settings);
                }
                //Check if the item needs to be saved
                if (isUpdated)
                {
                    history.DispatchMethod = dispatchEnum;

                    if (dispatchEnum == Types.Enums.DocDispatchMethodEnum.Email)
                        history.SentDate = DateTime.Today;

                    dbContext.Save(history);
                }
                return history;

            }
            catch (Exception ex)
            {
                DAL.ErrorLogger.LogToDb(this.DatabaseName, "InvoiceReportHandler.GetInvoiceSchedule", settings, ex);
                Debug.WriteLine("Error in InvoiceReportHandler.GetInvoiceSchedule: {0}", ex.ToString());
                throw new Exception("BL Error in InvoiceReportHandler.GetInvoiceSchedule. Unable to create history item.", ex);
            }
        }
        #endregion
        #region Document Creation
        private MemoryStream InvoiceCombinedDownload(DAL.InvoiceHistoryItem dbContext, Types.InvoicePdfSettings settings, Types.Enums.DocDispatchMethodEnum dispatchEnum)
        {
            Types.InvoiceCombinedReport cmbReport = new Types.InvoiceCombinedReport(this.OutputPath, 
                GetInvoiceLetter(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter),
                GetInvoiceSchedule(dbContext, settings, Types.Enums.DocDispatchMethodEnum.Letter));

            return cmbReport.CreateDocument(this.LayoutPath, settings);
        }
        /// <summary>
        /// Method to generate a combined letter and schedule PDF
        /// This Method should only be accessed if the generated PDF is to be a preview.
        /// No history is stored for preview items
        /// </summary>
        /// <param name="settings">The settings to be applied to the PDF generation</param>
        /// <returns>A memory stream containing a PDF generated from 2 seperate PDFs</returns>
        private MemoryStream InvoiceCombinedPreview(Types.InvoicePdfSettings settings)
        {
            //Get the report info from the database
            Types.InvoiceLetter ltr = InvoiceGetLetter(settings);
            Types.InvoiceSchedule sch = InvoiceGetSchedule(settings);
            //Create the documents
            Types.InvoiceHistoryItem hstLtr = new Types.InvoiceHistoryItem();
            hstLtr.Doc = ltr.CreateDocument(this.LayoutPath,settings);
            Types.InvoiceHistoryItem hstSch = new Types.InvoiceHistoryItem();
            hstSch.Doc = sch.CreateDocument(this.LayoutPath,settings);
            //Intialise the combined document
            Types.InvoiceCombinedReport rpt = new Types.InvoiceCombinedReport(this.OutputPath, hstLtr, hstSch);
            //Generate and return the combined report
            return rpt.CreateDocument(this.LayoutPath, settings);
        }

        private MemoryStream InvoiceCreateLetter(Types.InvoicePdfSettings settings)
        {
            Types.InvoiceLetter document = InvoiceGetLetter(settings); // rptContext.GetForQuoteRecId(recordId, this.OutputPath);
            return document.CreateDocument(this.LayoutPath, settings);
        }

        private Types.InvoiceLetter InvoiceGetLetter(Types.InvoicePdfSettings settings)
        {
            //Create the document
            DAL.InvoiceLetter rptContext = new InvoiceLetter(this.DatabaseName);
            Types.InvoiceLetter document = rptContext.GetForInvoiceRecId(settings.Id, this.OutputPath, settings.GenerationEnum);
            return document;
        }

        private MemoryStream InvoiceCreateSchedule(Types.InvoicePdfSettings settings)
        {
            Types.InvoiceSchedule document = InvoiceGetSchedule(settings); // rptContext.GetForQuoteRecId(recordId, this.OutputPath);
            return document.CreateDocument(this.LayoutPath, settings);
        }

        private Types.InvoiceSchedule InvoiceGetSchedule(Types.InvoicePdfSettings settings)
        {
            //Create the document
            DAL.InvoiceSchedule rptContext = new InvoiceSchedule(this.DatabaseName);
            Types.InvoiceSchedule document = rptContext.GetForInvoiceRecId(settings.Id, this.OutputPath);
            return document;
        }

        private Types.InvoiceHistoryItem InvoiceCreateDuplicate(Types.InvoicePdfSettings settings, Types.Enums.InvoiceDocTypeEnum docType, Types.InvoiceHistoryItem objToDuplicate, DAL.InvoiceHistoryItem dbContext)
        {
            MemoryStream ms = null;
            settings.IsCopy = true;
            if (docType == Types.Enums.InvoiceDocTypeEnum.Schedule)
                ms = InvoiceCreateSchedule(settings);
            else
                ms = InvoiceCreateLetter(settings);

            Types.InvoiceHistoryItem history = dbContext.CreateDupliacteDocument(objToDuplicate.Id, ms.ToArray());
            try
            {
                ms.Close();
                ms.Dispose();
            }
            catch
            { }
            return history;
        }
        #endregion
        #endregion
        #endregion
    }
}
