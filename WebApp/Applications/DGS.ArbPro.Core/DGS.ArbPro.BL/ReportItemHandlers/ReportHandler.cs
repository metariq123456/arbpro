﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using DGS.ArbPro.DAL;
using DGS.ArbPro.DAL.Reporting.DBItems;

namespace DGS.ArbPro.BL
{
    public class ReportHandler
    {
        #region Members
        string _DatabaseName = "";
        string _OutputPath = "";
        string _LayoutPath = "";
        #endregion

        #region Constructors
        public ReportHandler(string databaseName, string outputPath, string layoutPath)
        {
            _DatabaseName = databaseName;
            _OutputPath = outputPath;
            _LayoutPath = layoutPath;
        }
        #endregion
        #region Methods
        #region Public
        #region ****** QUOTE ******
        #region Email
        public MemoryStream SendQuoteLetterByEmail(Types.User usr, int quoteId, int recipientContactId, bool isCopy, string letterText, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                DAL.QuoteHistoryItem dbContext = new QuoteHistoryItem(_DatabaseName);
                Types.QuoteHistoryItem hstItem = GetQuoteLetter(dbContext, quoteId, Types.Enums.DocDispatchMethodEnum.Email, false, letterText);
                docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("QuoteLetter.pdf"));
                hstItem.EmailReference = SendDocumentsToEmailHandler(usr, recipientContactId, subject, emailText, docsToAttach);
                ms = hstItem.Doc;
                dbContext.Save(hstItem);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendQuoteLetterByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(_DatabaseName, "SendQuoteLetterByEmail", String.Format("Quote Id : {0}", quoteId), ex);
                throw (ex);
            }
            return ms;
        }

        public MemoryStream SendQuoteScheduleByEmail(Types.User usr, int quoteId, int recipientContactId, bool isCopy, string letterText, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                DAL.QuoteHistoryItem dbContext = new QuoteHistoryItem(_DatabaseName);
                Types.QuoteHistoryItem hstItem = GetQuoteSchedule(dbContext, quoteId, Types.Enums.DocDispatchMethodEnum.Email, false);
                docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("QuoteSchedule.pdf"));
                hstItem.EmailReference = SendDocumentsToEmailHandler(usr, recipientContactId, subject, emailText, docsToAttach);
                ms = hstItem.Doc;
                dbContext.Save(hstItem);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendQuoteScheduleByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(_DatabaseName, "SendQuoteScheduleByEmail", String.Format("Quote Id : {0}", quoteId), ex);
                throw (ex);
            }
            return ms;
        }


        public MemoryStream SendQuoteDocumentsByEmail(Types.User usr, int quoteId, int recipientContactId, bool isCopy, string letterText, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                DAL.QuoteHistoryItem dbContext = new QuoteHistoryItem(_DatabaseName);
                Types.QuoteHistoryItem ltrtItem = GetQuoteSchedule(dbContext, quoteId, Types.Enums.DocDispatchMethodEnum.Email, false);
                Types.QuoteHistoryItem hstItem = GetQuoteLetter(dbContext, quoteId, Types.Enums.DocDispatchMethodEnum.Email, false, letterText);
                docsToAttach.InsertWhenMissing(0, ltrtItem.ToEmailAttachment("QuoteSchedule.pdf"));
                docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("QuoteLetter.pdf"));
                ltrtItem.EmailReference = SendDocumentsToEmailHandler(usr, recipientContactId, subject, emailText, docsToAttach);
                hstItem.EmailReference = ltrtItem.EmailReference;
                Types.QuoteCombinedReport cmbQuoteReport = new Types.QuoteCombinedReport(_OutputPath, hstItem, ltrtItem);
                ms = cmbQuoteReport.CreateDocument(_LayoutPath, false);
                dbContext.Save(ltrtItem);
                dbContext.Save(hstItem);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendQuoteDocumentsByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(_DatabaseName, "SendQuoteDocumentsByEmail", String.Format("Quote Id : {0}", quoteId), ex);
                throw (ex);
            }
            return ms;
        }
        #endregion
        #region Letter
        public MemoryStream OpenQuoteLetter(int recordId, bool createCopy, Types.Enums.DocDispatchMethodEnum dispatchMethod)
        {
            return OpenQuoteLetter(recordId, createCopy, dispatchMethod, "");
        }

        public MemoryStream OpenQuoteLetter(int recordId, bool createCopy, Types.Enums.DocDispatchMethodEnum dispatchMethod, string letterText)
        {
            //Get or Create the History item for the document
            DAL.QuoteHistoryItem historyContext = new QuoteHistoryItem(_DatabaseName);
            Types.QuoteHistoryItem history = GetQuoteLetter(historyContext, recordId, dispatchMethod, createCopy, letterText);
            return history.Doc;
        }

        public Types.QuoteHistoryItem GetQuoteLetter(DAL.QuoteHistoryItem historyContext, int recordId, Types.Enums.DocDispatchMethodEnum dispatchMethod, bool enableCopy, string letterText = "")
        {
            try
            {
                Types.QuoteHistoryItem history = historyContext.GetDocument(Types.Enums.DocContentTypeEnum.Letter, recordId);
                bool isUpdated = false;

                //Check if there is already a document or if one has to be created
                if (history.Doc == null)
                {
                    //No document for this record exists so create it
                    if(letterText.Length == 0)
                        history.Doc = QuoteCreateDocument(recordId, false);
                    else
                        history.Doc = QuoteCreateDocument(recordId,letterText, false);

                    history.CreationDate = DateTime.Now;
                    isUpdated = true;
                }
                //Check if a copy of the document is to be created
                else if (enableCopy)
                {
                    history = QuoteCreateDuplicate(recordId,Types.Enums.DocContentTypeEnum.Letter, history, historyContext);
                    isUpdated = true;
                }
                //Check if the item needs to be saved
                if (isUpdated)
                {
                    history.DispatchMethod = dispatchMethod;

                    if (dispatchMethod == Types.Enums.DocDispatchMethodEnum.Email)
                        history.SentDate = DateTime.Today;

                    historyContext.Save(history);
                }
                return history;

            }
            catch (Exception ex)
            {
                //TODO: Log to log file
                Console.WriteLine("GetQuoteLetter error: {0}", ex.ToString());
                throw new Exception("BL Error in GetQuoteLetter. Unable to create history item.", ex);
            }
        }
        #endregion
        #region Schedule
        /// <summary>
        /// Method to get or create a Quote schedule PDF document from a quote record to be opened in a browser window
        /// This method can throw an exception that must be handled
        /// </summary>
        /// <param name="recordId">int: The Id of the Quote record</param>
        /// <param name="outputPath">string: Where a PDF file should be saved</param>
        /// <param name="layoutPath">string: The path to the xml layout files</param>
        /// <param name="enableCopy">bool: flag to denote if a copy should be made if a document already exists</param>
        /// <param name="isEmail">bool: flag to denote if the delivery method is email</param>
        /// <returns>MemoryStream: The memory stream containing the PDF document</returns>
        public MemoryStream OpenQuoteSchedule(int recordId, bool createCopy, Types.Enums.DocDispatchMethodEnum dispatchMethod)
        {
            //Get or Create the History item for the document
            DAL.QuoteHistoryItem historyContext = new QuoteHistoryItem(_DatabaseName);
            Types.QuoteHistoryItem history = GetQuoteSchedule(historyContext, recordId, dispatchMethod, createCopy);
            return history.Doc;
        }

        /// <summary>
        /// Method to get or create a Quote schedule from a quote record
        /// This method can throw an exception that must be handled
        /// </summary>
        /// <param name="recordId">int: The Id of the Quote record</param>
        /// <param name="outputPath">string: Where a PDF file should be saved</param>
        /// <param name="layoutPath">string: The path to the xml layout files</param>
        /// <param name="enableCopy">bool: flag to denote if a copy should be made if a document already exists</param>
        /// <returns>Types.QuoteHistoryItem that contains a memory stream of the PDF report</returns>
        public Types.QuoteHistoryItem GetQuoteSchedule(DAL.QuoteHistoryItem historyContext, int recordId,Types.Enums.DocDispatchMethodEnum dispatchMethod, bool enableCopy)
        {
            try
            {
                Types.QuoteHistoryItem history = historyContext.GetDocument(Types.Enums.DocContentTypeEnum.Schedule, recordId);
                bool isUpdated = false;

                //Check if there is already a document or if one has to be created
                if (history.Doc == null)
                {
                    //No document for this record exists so create it
                    history.Doc = QuoteCreateSchedule(recordId, false);
                    history.CreationDate = DateTime.Now;
                    isUpdated = true;
                }
                //Check if a copy of the document is to be created
                else if (enableCopy)
                {
                    history = QuoteCreateDuplicate(recordId,Types.Enums.DocContentTypeEnum.Schedule, history, historyContext);
                    isUpdated = true;
                }
                //Check if the item needs to be saved
                if (isUpdated)
                {
                    history.DispatchMethod = dispatchMethod;

                    if (dispatchMethod == Types.Enums.DocDispatchMethodEnum.Email)
                        history.SentDate = DateTime.Today;

                    historyContext.Save(history);
                }
                return history;
                    
            }
            catch(Exception ex)
            {
                //TODO: Log to log file
                Console.WriteLine("CreateQuoteSchedule error: {0}",ex.ToString());
                throw new Exception("BL Error in GetQuoteSchedule. Unable to create history item.", ex);
            }
        }
        #endregion
        #region Combined
        public MemoryStream OpenCombinedQuoteDocuments(int recordId, bool createCopy, Types.Enums.DocDispatchMethodEnum dispatchMethod, bool enableCopy, string letterText = "")
        {
            DAL.QuoteHistoryItem dbContext = new QuoteHistoryItem(_DatabaseName);

            Types.QuoteHistoryItem hstLetter = GetQuoteLetter(dbContext, recordId, dispatchMethod, enableCopy, letterText);
            Types.QuoteHistoryItem hstSchedule = GetQuoteSchedule(dbContext, recordId, dispatchMethod, enableCopy);

            Types.QuoteCombinedReport cmbReport = new Types.QuoteCombinedReport(_OutputPath,hstLetter,hstSchedule);

            return cmbReport.CreateDocument(_LayoutPath, false);
        }
        #endregion
        #endregion

        #region ****** INVOICE ******
        #region Email
        public MemoryStream SendInvoiceLetterByEmail(Types.User usr, int invoiceId, Types.Enums.InvoiceDocTypeEnum  docType, int recipientContactId, bool isCopy, string letterText, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                DAL.InvoiceHistoryItem dbContext = new InvoiceHistoryItem(_DatabaseName);
                Types.InvoiceHistoryItem hstItem = GetInvoiceLetter(dbContext, invoiceId,docType, Types.Enums.DocDispatchMethodEnum.Email, false, letterText);
                docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("InvoiceLetter.pdf"));
                hstItem.EmailReference = SendDocumentsToEmailHandler(usr, recipientContactId, subject, emailText, docsToAttach);
                ms = hstItem.Doc;
                dbContext.Save(hstItem);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendInvoiceLetterByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(_DatabaseName, "SendInvoiceDocumentsByEmail", String.Format("Invoice Id : {0}", invoiceId), ex);
                throw (ex);
            }
            return ms;
        }

        public MemoryStream SendInvoiceScheduleByEmail(Types.User usr, int invoiceId, int recipientContactId, bool isCopy, string letterText, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                DAL.InvoiceHistoryItem dbContext = new InvoiceHistoryItem(_DatabaseName);
                Types.InvoiceHistoryItem hstItem = GetInvoiceSchedule(dbContext, invoiceId, Types.Enums.DocDispatchMethodEnum.Email, false);
                docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("InvoiceSchedule.pdf"));
                hstItem.EmailReference = SendDocumentsToEmailHandler(usr, recipientContactId, subject, emailText, docsToAttach);
                ms = hstItem.Doc;
                dbContext.Save(hstItem);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendInvoiceScheduleByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(_DatabaseName, "SendInvoiceDocumentsByEmail", String.Format("Invoice Id : {0}", invoiceId), ex);
                throw (ex);
            }
            return ms;
        }


        public MemoryStream SendInvoiceDocumentsByEmail(Types.User usr, int invoiceId, Types.Enums.InvoiceDocTypeEnum docType, int recipientContactId, bool isCopy, string letterText, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                DAL.InvoiceHistoryItem dbContext = new InvoiceHistoryItem(_DatabaseName);
                Types.InvoiceHistoryItem ltrtItem = GetInvoiceSchedule(dbContext, invoiceId, Types.Enums.DocDispatchMethodEnum.Email, false);
                Types.InvoiceHistoryItem hstItem = GetInvoiceLetter(dbContext, invoiceId, docType, Types.Enums.DocDispatchMethodEnum.Email, false, letterText);
                docsToAttach.InsertWhenMissing(0, ltrtItem.ToEmailAttachment("QuoteSchedule.pdf"));
                docsToAttach.InsertWhenMissing(0, hstItem.ToEmailAttachment("QuoteLetter.pdf"));
                ltrtItem.EmailReference = SendDocumentsToEmailHandler(usr, recipientContactId, subject, emailText, docsToAttach);
                hstItem.EmailReference = ltrtItem.EmailReference;
                Types.InvoiceCombinedReport cmbQuoteReport = new Types.InvoiceCombinedReport(_OutputPath, hstItem, ltrtItem);
                ms = cmbQuoteReport.CreateDocument(_LayoutPath, false);
                dbContext.Save(ltrtItem);
                dbContext.Save(hstItem);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendInvoiceDocumentsByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(_DatabaseName, "SendInvoiceDocumentsByEmail", String.Format("Invoice Id : {0}", invoiceId), ex);
                throw (ex);
            }
            return ms;
        }
        #endregion
        #region Letters
        public MemoryStream OpenInvoiceLetter(int recordId, bool createCopy, Types.Enums.InvoiceDocTypeEnum docType, Types.Enums.DocDispatchMethodEnum dispatchMethod, string letterText)
        {
            //Get or Create the History item for the document
            DAL.InvoiceHistoryItem historyContext = new InvoiceHistoryItem(_DatabaseName);
            Types.InvoiceHistoryItem history = GetInvoiceLetter(historyContext, recordId, docType, dispatchMethod, createCopy, letterText);
            return history.Doc;
        }

        public Types.InvoiceHistoryItem GetInvoiceLetter(DAL.InvoiceHistoryItem historyContext, int recordId, Types.Enums.InvoiceDocTypeEnum docType, Types.Enums.DocDispatchMethodEnum dispatchMethod, bool enableCopy, string letterText = "")
        {
            try
            {
                Types.InvoiceHistoryItem history = historyContext.GetDocument(docType, recordId);
                bool isUpdated = false;

                //Check if there is already a document or if one has to be created
                if (history.Doc == null)
                {
                    //No document for this record exists so create it
                    if (letterText.Length == 0)
                        history.Doc = InvoiceCreateDocument(recordId,docType, false);
                    else
                        history.Doc = InvoiceCreateDocument(recordId,docType, letterText, false);

                    history.CreationDate = DateTime.Now;
                    isUpdated = true;
                }
                //Check if a copy of the document is to be created
                else if (enableCopy)
                {
                    history = InvoiceCreateDuplicate(recordId, Types.Enums.InvoiceDocTypeEnum.Letter, history, historyContext);
                    isUpdated = true;
                }
                //Check if the item needs to be saved
                if (isUpdated)
                {
                    history.DispatchMethod = dispatchMethod;

                    if (dispatchMethod == Types.Enums.DocDispatchMethodEnum.Email)
                        history.SentDate = DateTime.Today;

                    historyContext.Save(history);
                }
                return history;

            }
            catch (Exception ex)
            {
                Console.WriteLine("GetInvoiceLetter error: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(_DatabaseName, "GetInvoiceLetter",
                    string.Format("int recordId: {0}, Types.Enums.DocDispatchMethodEnum dispatchMethod: {1}, bool enableCopy: {2}, string letterText: {3}",
                    recordId, dispatchMethod.ToString(), enableCopy.ToString(), letterText),
                    ex);
                throw new Exception("BL Error in GetInvoiceLetter. Unable to create history item.", ex);
            }
        }
        #endregion
        #region Schedule
        public MemoryStream OpenInvoiceSchedule(int recordId, bool createCopy, Types.Enums.DocDispatchMethodEnum dispatchMethod)
        {
            //Get or Create the History item for the document
            DAL.InvoiceHistoryItem historyContext = new InvoiceHistoryItem(_DatabaseName);
            Types.InvoiceHistoryItem history = GetInvoiceSchedule(historyContext, recordId, dispatchMethod, createCopy);
            return history.Doc;
        }

        /// <summary>
        /// Method to get or create a Quote schedule from a quote record
        /// This method can throw an exception that must be handled
        /// </summary>
        /// <param name="recordId">int: The Id of the Quote record</param>
        /// <param name="outputPath">string: Where a PDF file should be saved</param>
        /// <param name="layoutPath">string: The path to the xml layout files</param>
        /// <param name="enableCopy">bool: flag to denote if a copy should be made if a document already exists</param>
        /// <returns>Types.QuoteHistoryItem that contains a memory stream of the PDF report</returns>
        public Types.InvoiceHistoryItem GetInvoiceSchedule(DAL.InvoiceHistoryItem historyContext, int recordId, Types.Enums.DocDispatchMethodEnum dispatchMethod, bool enableCopy)
        {
            try
            {
                Types.InvoiceHistoryItem history = historyContext.GetDocument(Types.Enums.InvoiceDocTypeEnum.Schedule, recordId);
                bool isUpdated = false;

                //Check if there is already a document or if one has to be created
                if (history.Doc == null)
                {
                    //No document for this record exists so create it
                    history.Doc = InvoiceCreateSchedule(recordId, false);
                    history.CreationDate = DateTime.Now;
                    isUpdated = true;
                }
                //Check if a copy of the document is to be created
                else if (enableCopy)
                {
                    history = InvoiceCreateDuplicate(recordId, Types.Enums.InvoiceDocTypeEnum.Schedule, history, historyContext);
                    isUpdated = true;
                }
                //Check if the item needs to be saved
                if (isUpdated)
                {
                    history.DispatchMethod = dispatchMethod;

                    if (dispatchMethod == Types.Enums.DocDispatchMethodEnum.Email)
                        history.SentDate = DateTime.Today;

                    historyContext.Save(history);
                }
                return history;

            }
            catch (Exception ex)
            {
                //TODO: Log to log file
                Console.WriteLine("GetInvoiceSchedule error: {0}", ex.ToString());
                throw new Exception("BL Error in GetInvoiceSchedule. Unable to create history item.", ex);
            }
        }
        #endregion

        #region Combined
        public MemoryStream OpenCombinedInvoiceDocuments(int recordId, bool createCopy, Types.Enums.InvoiceDocTypeEnum docType, Types.Enums.DocDispatchMethodEnum dispatchMethod, bool enableCopy, string letterText = "")
        {
            Types.InvoiceCombinedReport cmbReport = CreateCombinedInvoiceDocuments(recordId, createCopy, docType, dispatchMethod, enableCopy, letterText);

            return cmbReport.CreateDocument(_LayoutPath, false);
        }

        public Types.InvoiceCombinedReport CreateCombinedInvoiceDocuments(int recordId, bool createCopy, Types.Enums.InvoiceDocTypeEnum docType, Types.Enums.DocDispatchMethodEnum dispatchMethod, bool enableCopy, string letterText = "")
        {
            DAL.InvoiceHistoryItem dbContext = new InvoiceHistoryItem(_DatabaseName);

            Types.InvoiceHistoryItem hstLetter = GetInvoiceLetter(dbContext, recordId, docType, dispatchMethod, enableCopy, letterText);
            Types.InvoiceHistoryItem hstSchedule = GetInvoiceSchedule(dbContext, recordId, dispatchMethod, enableCopy);

            Types.InvoiceCombinedReport cmbReport = new Types.InvoiceCombinedReport(_OutputPath, hstLetter, hstSchedule);

            return cmbReport;
        }
        #endregion
        #endregion

        #region ****** Work Packs ******
        #region ****** Email ******
        public MemoryStream SendWorkOrderByEmail(Types.User usr, int quoteId, string recipient, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            return SendWorkOrderByEmail(usr, quoteId, new List<string>() { recipient }, emailText, subject, docsToAttach);
        }

        public MemoryStream SendWorkOrderByEmail(Types.User usr, int quoteId, List<string> recipients, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                ms = OpenWorkScheduleForQuote(quoteId);
                Types.UserDocument pdfAttachment = new Types.UserDocument() { Name = "WorkOrder.pdf", Bytes = ms.ToArray() };
                docsToAttach.Insert(0, pdfAttachment);
                SendDocumentsToEmailHandler(usr, recipients, subject, emailText, docsToAttach);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendWorkOrderByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(_DatabaseName, "SendWorkOrderByEmail", String.Format("Quote Id : {0}", quoteId), ex);
                throw (ex);
            }
            return ms;
        }

        public MemoryStream SendRiskAssessmentByEmail(Types.User usr, int quoteId, string recipient, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            return SendRiskAssessmentByEmail(usr, quoteId, new List<string>() { recipient }, emailText, subject, docsToAttach);
        }

        public MemoryStream SendRiskAssessmentByEmail(Types.User usr, int quoteId, List<string> recipients, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                ms = OpenRiskAssessmentForQuote(quoteId);
                Types.UserDocument pdfAttachment = new Types.UserDocument() { Name = "RiskAssessment.pdf", Bytes = ms.ToArray() };
                docsToAttach.Insert(0, pdfAttachment);
                SendDocumentsToEmailHandler(usr, recipients , subject, emailText, docsToAttach);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendRiskAssessmentByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(_DatabaseName, "SendRiskAssessmentByEmail", String.Format("Quote Id : {0}", quoteId), ex);
                throw (ex);
            }
            return ms;
        }

        public MemoryStream SendWorkPackByEmail(Types.User usr, int quoteId, string recipient, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            return SendWorkPackByEmail(usr, quoteId, new List<string>() { recipient }, emailText, subject, docsToAttach);
        }

        public MemoryStream SendWorkPackByEmail(Types.User usr, int quoteId, List<string> recipients, string emailText, string subject, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            try
            {
                Types.WorkPackDocuments wp = WorkPackForQuote(quoteId);
                List<Types.UserDocument> pdfAttachments = new List<Types.UserDocument>();
                foreach (KeyValuePair<string, MemoryStream> pdf in wp.DocumentsPack)
                {
                    if (pdf.Value != null && pdf.Value.Length > 0)
                        pdfAttachments.Add(new Types.UserDocument() { Name = pdf.Key, Bytes = pdf.Value.ToArray() });
                }
                docsToAttach.InsertRange(0, pdfAttachments);
                SendDocumentsToEmailHandler(usr, recipients, subject, emailText, docsToAttach);
                ms = wp.CreateDocument(_LayoutPath,false);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Report handler - SendWorkPackByEmail: {0}", ex.ToString());
                DAL.ErrorLogger.LogToDb(_DatabaseName, "SendWorkPackByEmail", String.Format("Quote Id : {0}", quoteId), ex);
                throw (ex);
            }
            return ms;
        }
        #endregion
        #region ****** Combined Documnets ******
        public MemoryStream OpenWorkPackDocuments(int quoteId)
        {
            //The following may raise an exception
            return WorkPackForQuote(quoteId).CreateDocument(_LayoutPath, false);
        }
        #endregion

        #region ****** Risk Assessment ******
        public MemoryStream OpenRiskAssessmentForQuote(int quoteId)
        {
            Types.RiskDocument assessment = RiskDocumentForQuote(quoteId);
            return assessment.CreateDocument(_LayoutPath, false);
        }
        #endregion

        #region ****** Work Schedule ******
        public MemoryStream OpenWorkScheduleForQuote(int quoteId)
        {
            Types.WorkOrder assessment = WorkOrderDocForQuote(quoteId);
            return assessment.CreateDocument(_LayoutPath, false);
        }
        #endregion
        #endregion

        #endregion
        #region Private
        #region ***** EMAIL *****
        private string SendDocumentsToEmailHandler(Types.User usr, int recipientContactID, string subject, string emailText, List<Types.UserDocument> attachments)
        {
            return ReportItemHandlers.ReportEmailHandler.SendPDFEmail(
                _DatabaseName,
                recipientContactID,
                usr,
                subject,
                emailText,
                attachments);
        }

        private string SendDocumentsToEmailHandler(Types.User usr, List<string> recipients, string subject, string emailText, List<Types.UserDocument> attachments)
        {
            return ReportItemHandlers.ReportEmailHandler.SendPDFEmail(
                _DatabaseName,
                recipients,
                usr,
                subject,
                emailText,
                attachments);
        }
        #endregion
        #region ***** QUOTE *****
        /// <summary>
        /// Create a Quote schedule. The document will be created as a copy based on the flag
        /// </summary>
        /// <param name="recordId">int: The Quote record Id</param>
        /// <param name="isCopy">bool: Flag to say if the report is to be a copy</param>
        /// <returns></returns>
        private MemoryStream QuoteCreateSchedule(int recordId, bool isCopy)
        {
            //Create the document
            Types.QuoteSchedule document = QuoteCreateSchedule(recordId); // rptContext.GetForQuoteRecId(recordId, _OutputPath);
            return document.CreateDocument(_LayoutPath, isCopy);
        }

        private Types.QuoteSchedule QuoteCreateSchedule(int recordId)
        {
            //Create the document
            DAL.QuoteSchedule rptContext = new QuoteSchedule(_DatabaseName);
            Types.QuoteSchedule document = rptContext.GetForQuoteRecId(recordId, _OutputPath);
            return document;
        }

        private Types.QuoteLetter QuoteCreateLetter(int recordId)
        {
            //Create the document
            DAL.QuoteLetter rptContext = new QuoteLetter(_DatabaseName);
            Types.QuoteLetter document = rptContext.GetForQuoteRecId(recordId, _OutputPath);
            return document;
        }

        private MemoryStream QuoteCreateDocument(int recordId, bool isCopy)
        {
            ////Create the document
            Types.QuoteLetter document = QuoteCreateLetter(recordId);
            return document.CreateDocument(_LayoutPath, isCopy);
        }

        private MemoryStream QuoteCreateDocument(int recordId,string letterText, bool isCopy)
        {
            //Create the document
            Types.QuoteLetter document = QuoteCreateLetter(recordId);
            return document.CreateDocument(letterText, _LayoutPath, isCopy);
        }

        private Types.QuoteHistoryItem QuoteCreateDuplicate(int recId,Types.Enums.DocContentTypeEnum rptType, Types.QuoteHistoryItem objToDuplicate, DAL.QuoteHistoryItem historyContext)
        {
            MemoryStream ms = null;
            if(rptType == Types.Enums.DocContentTypeEnum.Schedule)
                ms = QuoteCreateSchedule(recId, true);
            else
                ms = QuoteCreateDocument(recId, true);

            Types.QuoteHistoryItem history = historyContext.CreateDupliacteDocument(objToDuplicate.Id, ms.ToArray());
            try
            {
                ms.Close();
                ms.Dispose();
            }
            catch
            { }
            return history;
        }
        #endregion

        #region ***** INVOICE *****
        /// <summary>
        /// Create a Quote schedule. The document will be created as a copy based on the flag
        /// </summary>
        /// <param name="recordId">int: The Quote record Id</param>
        /// <param name="isCopy">bool: Flag to say if the report is to be a copy</param>
        /// <returns></returns>
        private MemoryStream InvoiceCreateSchedule(int recordId, bool isCopy)
        {
            //Create the document
            DAL.InvoiceSchedule rptContext = new InvoiceSchedule(_DatabaseName);
            Types.InvoiceSchedule document = rptContext.GetForInvoiceRecId(recordId, _OutputPath);
            return document.CreateDocument(_LayoutPath, isCopy);
        }

        private MemoryStream InvoiceCreateDocument(int recordId, Types.Enums.InvoiceDocTypeEnum letterType, bool isCopy)
        {
            //Create the document
            DAL.InvoiceLetter rptContext = new InvoiceLetter(_DatabaseName);
            Types.InvoiceLetter document = rptContext.GetForInvoiceRecId(recordId, _OutputPath, letterType);
            return document.CreateDocument(_LayoutPath, isCopy);
        }

        private MemoryStream InvoiceCreateDocument(int recordId, Types.Enums.InvoiceDocTypeEnum letterType, string letterText, bool isCopy)
        {
            //Create the document
            DAL.InvoiceLetter rptContext = new InvoiceLetter(_DatabaseName);
            Types.InvoiceLetter document = rptContext.GetForInvoiceRecId(recordId, _OutputPath,letterType);
            return document.CreateDocument(letterText, _LayoutPath, isCopy);
        }

        private Types.InvoiceHistoryItem InvoiceCreateDuplicate(int recId, Types.Enums.InvoiceDocTypeEnum rptType, Types.InvoiceHistoryItem objToDuplicate, DAL.InvoiceHistoryItem historyContext)
        {
            MemoryStream ms = null;
            if (rptType == Types.Enums.InvoiceDocTypeEnum.Schedule)
                ms = QuoteCreateSchedule(recId, true);
            else
                ms = QuoteCreateDocument(recId, true);

            Types.InvoiceHistoryItem history = historyContext.CreateDupliacteDocument(objToDuplicate.Id, ms.ToArray());
            try
            {
                ms.Close();
                ms.Dispose();
            }
            catch
            { }
            return history;
        }
        #endregion

        #region ***** Work Packs *****
        private Types.WorkPackDocuments WorkPackForQuote(int quoteId)
        {
            Types.WorkPackDocuments docs = new Types.WorkPackDocuments(_OutputPath);
            //The order the docs are added to the list will impact how the combined doc is created
            docs.DocumentsPack.Add("WorkOrder.pdf",WorkOrderDocForQuote(quoteId).CreateDocument(_LayoutPath,false));
            docs.DocumentsPack.Add("RiskAssessment.pdf", RiskDocumentForQuote(quoteId).CreateDocument(_LayoutPath, false));

            return docs;
        }

        private Types.RiskDocument RiskDocumentForQuote(int quoteId)
        {
            DAL.RiskDocument dbContext = new RiskDocument(_DatabaseName);
            return dbContext.GetForQuoteId(quoteId, _OutputPath);
        }

        private Types.WorkOrder WorkOrderDocForQuote(int quoteId)
        {
            DAL.WorkOrder dbContext = new WorkOrder(_DatabaseName);
            return dbContext.GetForQuoteId(quoteId, _OutputPath);
        }

        #endregion
        #endregion
        #endregion
    }
}
