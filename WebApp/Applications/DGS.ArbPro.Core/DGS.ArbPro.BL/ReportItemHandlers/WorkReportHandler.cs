﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;
using DGS.ArbPro.DAL;
using DGS.ArbPro.DAL.Reporting.DBItems;
using PDFReport;

namespace DGS.ArbPro.BL
{
    public class WorkReportHandler : ReportHandlerBase
    {
        #region Members
        #endregion

        #region Constructors
        public WorkReportHandler(string databaseName, string outputPath, string layoutPath) : base(databaseName, outputPath, layoutPath) { }
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Public method to generate a work pack document based on a PDF settings object
        /// </summary>
        /// <param name="settings">Types.WorkPdfSettings: The settings to be used to create the document</param>
        /// <returns></returns>
        public MemoryStream OpenWorkPack(Types.WorkPdfSettings settings)
        {
            MemoryStream ms = null;
            switch (settings.GenerationEnum)
            {
                case Types.Enums.DocGenerationEnum.Letter:
                    //This is the works oreder
                    ms = GenerateWorksOrderDoc(settings);
                    break;
                case Types.Enums.DocGenerationEnum.Schedule:
                    //This is the risk assessment
                    ms = GenerateRiskAssessmentDoc(settings);
                    break;
                default:
                    ms = GenerateAllDocs(settings);
                    break;
            }
            return ms;
        }

        public MemoryStream OpenTpoWorkSchedule(Types.WorkPdfSettings settings)
        {
            MemoryStream ms = null;
            switch (settings.GenerationEnum)
            {
                case Types.Enums.DocGenerationEnum.Letter:
                    //This is the works oreder
                    ms = GenerateTpoWorksScheduleDoc(settings);
                    break;
            }

            return ms;
        }

        public MemoryStream PrintImage(int PictureId,Byte[] picByte)
        {
            Types.WorkPdfSettings settings = new Types.WorkPdfSettings();
            settings.DocNo = 1;
            settings.Id = 1;
            settings.QuoteId = 1;

            MemoryStream ms = null;
            ms = GenerateImageDoc(settings,picByte);
            return ms;
        }

        public MemoryStream EmailWorkPack(Types.WorkPdfSettings settings, string toAddress, string subject, string content, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            switch (settings.GenerationEnum)
            {
                case Types.Enums.DocGenerationEnum.Letter:
                    //This is the works oreder
                    ms = EmailWorksOrderDoc(settings, toAddress, subject, content, usr, docsToAttach);
                    break;
                case Types.Enums.DocGenerationEnum.Schedule:
                    //This is the risk assessment
                    ms = EmailRiskAssessmentDoc(settings, toAddress, subject, content, usr, docsToAttach);
                    break;
                default:
                    ms = EmailAllDocs(settings, toAddress, subject, content, usr, docsToAttach);
                    break;
            }
            return ms;
        }
        public MemoryStream AttachWorkPackLetter(Types.WorkPdfSettings settings, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            ms = GenerateTpoWorksScheduleDoc(settings);
            Types.UserDocument Doc = new Types.UserDocument();
            Doc = Types.UserDocument.ToUserDocument("Work Schedule-" + settings.DocNo + ".pdf", ms);
            Doc.FileName = "Work Schedule-" + settings.DocNo;
            Doc.DocumentType = Types.Enums.UserDocumentTypeEnum.Temporary;
            Doc.ContentType = "pdf";
            Doc.Id = docsToAttach.Count;
            Doc.ClientID = -5;
            docsToAttach.InsertWhenMissing(0, Doc);
            return ms;
        }

        public MemoryStream AttachRiskAssessmentDoc(Types.WorkPdfSettings settings, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = null;
            ms = GenerateRiskAssessmentDoc(settings);
            Types.UserDocument Doc = new Types.UserDocument();
            Doc = Types.UserDocument.ToUserDocument("RiskAssessment-" + settings.DocNo + ".pdf", ms);
            Doc.FileName = "Risk Assessment-" + settings.DocNo;
            Doc.DocumentType = Types.Enums.UserDocumentTypeEnum.Temporary;
            Doc.ContentType = "pdf";
            Doc.Id = docsToAttach.Count;
            Doc.ClientID = -6;
            docsToAttach.InsertWhenMissing(0, Doc);
            return ms;
        }
        #endregion
        #region Private
        #region Email Generation
        private MemoryStream EmailWorksOrderDoc(Types.WorkPdfSettings settings, string toAddress, string subject, string content, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = GenerateWorksOrderDoc(settings);
            docsToAttach.Insert(0, Types.UserDocument.ToUserDocument("WorkOrder.pdf", ms));
            SendDocumentsToEmailHandler(usr, new List<string>() { toAddress, usr.EmailAddress }, subject, content, docsToAttach);
            return ms;
        }

        private MemoryStream EmailRiskAssessmentDoc(Types.WorkPdfSettings settings, string toAddress, string subject, string content, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            MemoryStream ms = GenerateRiskAssessmentDoc(settings);
            docsToAttach.Insert(0, Types.UserDocument.ToUserDocument("RiskAssessment.pdf", ms));
            SendDocumentsToEmailHandler(usr, new List<string>() { toAddress, usr.EmailAddress }, subject, content, docsToAttach);
            return ms;
        }



        private MemoryStream EmailAllDocs(Types.WorkPdfSettings settings, string toAddress, string subject, string content, Types.User usr, List<Types.UserDocument> docsToAttach)
        {
            Types.WorkPackDocuments wp = GenerateCombinedObject(settings);
            //Iterate through each of the documents in the work packs document pack and add them to the attachment list
            foreach (KeyValuePair<string, MemoryStream> doc in wp.DocumentsPack)
            {
                docsToAttach.Insert(0, Types.UserDocument.ToUserDocument(string.Format("{0}.pdf", doc.Key), doc.Value));
            }
            SendDocumentsToEmailHandler(usr, new List<string>() { toAddress, usr.EmailAddress }, subject, content, docsToAttach);
            return wp.CreateDocument(this.LayoutPath, settings.IsCopy);
        }
        #endregion
        #region Document generation methods
        private MemoryStream GenerateWorksOrderDoc(Types.WorkPdfSettings settings)
        {
            Types.WorkOrder doc = GenerateWorksOrder(settings);
            return doc.CreateDocument(this.LayoutPath, settings);
        }

        private MemoryStream GenerateTpoWorksScheduleDoc(Types.WorkPdfSettings settings)
        {
            Types.TpoWorkSchedule doc = GenerateTpoWorksSchedule(settings);
            return doc.CreateDocument(this.LayoutPath, settings);
        }

        private MemoryStream GenerateImageDoc(Types.WorkPdfSettings settings,Byte[] workPic)
        {
            Types.TpoWorkSchedule doc = GenerateTpoWorksSchedule(settings);
            return doc.CreateImageDocument(this.LayoutPath, settings,workPic);
        }


        private MemoryStream GenerateRiskAssessmentDoc(Types.WorkPdfSettings settings)
        {
            Types.RiskDocument doc = GenerateRiskAssessment(settings);
            return doc.CreateDocument(this.LayoutPath, settings.IsCopy);
        }

        private MemoryStream GenerateAllDocs(Types.WorkPdfSettings settings)
        {
            Types.WorkPackDocuments doc = GenerateCombinedObject(settings);
            return doc.CreateDocument(this.LayoutPath, settings.IsCopy);
        }
        #endregion
        #region Object Creation Methods
        private Types.TpoWorkSchedule GenerateTpoWorksSchedule(Types.WorkPdfSettings settings)
        {
            DAL.TpoWorkSchedule ordContext = new DAL.TpoWorkSchedule(this.DatabaseName);
            Types.TpoWorkSchedule retVal = null;
            //Check how the report object is going to be created
            switch (settings.ReportParentType)
            {
                case Types.Enums.WorkReportParentEnum.JobAndQuoteId:
                case Types.Enums.WorkReportParentEnum.JobSite:
                    //The report is being created from a Work reference
                    retVal = ordContext.GetForJobSiteId(settings.Id, this.OutputPath, settings.QuoteId);
                    break;
            }
            return retVal;
        }
        private Types.WorkOrder GenerateWorksOrder(Types.WorkPdfSettings settings)
        {
            DAL.WorkOrder ordContext = new DAL.WorkOrder(this.DatabaseName);
            Types.WorkOrder retVal = null;
            //Check how the report object is going to be created
            switch (settings.ReportParentType)
            {
                case Types.Enums.WorkReportParentEnum.QuoteRecord:
                    //The report is being created from a Quote reference
                    retVal = ordContext.GetForQuoteId(settings.Id, this.OutputPath);
                    break;
                case Types.Enums.WorkReportParentEnum.WorkRecord:
                    //The report is being created from a Work reference
                    retVal = ordContext.GetForWorkId(settings.Id, this.OutputPath);
                    break;
                case Types.Enums.WorkReportParentEnum.ClientSite:
                    //The report is being created from a client site reference
                    retVal = ordContext.GetForClientSiteId(settings.Id, this.OutputPath);
                    break;
                //Temporary until the work plan can be retrieved via job and quote
                case Types.Enums.WorkReportParentEnum.JobAndQuoteId:
                case Types.Enums.WorkReportParentEnum.JobSite:
                    //The report is being created from a Work reference
                    retVal = ordContext.GetForJobSiteId(settings.Id, this.OutputPath);
                    break;
                case Types.Enums.WorkReportParentEnum.WorkPlan:
                    retVal = ordContext.GetForWorkPlanId(settings.QuoteId, this.OutputPath);
                    break;
            }
            return retVal;
        }

        private Types.RiskDocument GenerateRiskAssessment(Types.WorkPdfSettings settings)
        {
            DAL.RiskDocument riskContext = new DAL.RiskDocument(this.DatabaseName);
            Types.RiskDocument retVal = null;
            //Check how the report object is going to be created
            switch (settings.ReportParentType)
            {
                case Types.Enums.WorkReportParentEnum.QuoteRecord:
                    //The report is being created from a Quote reference
                    retVal = riskContext.GetForQuoteId(settings.QuoteId, this.OutputPath);
                    break;
                case Types.Enums.WorkReportParentEnum.WorkRecord:
                    //The report is being created from a Work reference
                    retVal = riskContext.GetForWorkId(settings.Id, this.OutputPath);
                    break;
                case Types.Enums.WorkReportParentEnum.ClientSite:
                    //The report is being created from a client site reference
                    retVal = riskContext.GetForClientSiteId(settings.Id, this.OutputPath);
                    break;
                case Types.Enums.WorkReportParentEnum.JobSite:
                    //The report is being created from a Work reference
                    retVal = riskContext.GetForJobSiteId(settings.Id, this.OutputPath);
                    break;
                case Types.Enums.WorkReportParentEnum.JobAndQuoteId:
                    //The report is being created from both a jobsite Id and quote Id
                    retVal = riskContext.GetForJobSiteAndQuoteId(settings.Id, settings.QuoteId, this.OutputPath);
                    break;
                case Types.Enums.WorkReportParentEnum.WorkPlan:
                    retVal = riskContext.GetForWorkPlanId(settings.QuoteId, this.OutputPath);
                    break;
            }
            return retVal;
        }

        private Types.WorkPackDocuments GenerateCombinedObject(Types.WorkPdfSettings settings)
        {
            Types.WorkPackDocuments retVal = new Types.WorkPackDocuments(this.DatabaseName);
            retVal.DocumentsPack.Add("WorkOrder", GenerateWorksOrderDoc(settings));
            retVal.DocumentsPack.Add("RiskAssessment", GenerateRiskAssessmentDoc(settings));
            return retVal;
        }
        #endregion

        #endregion
        #endregion
    }
}
