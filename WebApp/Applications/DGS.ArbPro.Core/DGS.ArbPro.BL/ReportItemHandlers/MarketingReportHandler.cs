﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MarketingReportHandler.cs" company="Microsec Ltd">
//   2016 Copyright Microsec Ltd
// </copyright>
// <summary>
//   The marketing report handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.BL
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Net.Mail;

    using DGS.ArbPro.BL.ReportItemHandlers;
    using DGS.ArbPro.DAL;
    using DGS.ArbPro.Types;

    using MarketingLetter = DGS.ArbPro.Types.MarketingLetter;
    using User = DGS.ArbPro.Types.User;

    /// <summary>
    /// The marketing report handler.
    /// </summary>
    public class MarketingReportHandler : ReportHandlerBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MarketingReportHandler"/> class.
        /// </summary>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="outputPath">
        /// The output path.
        /// </param>
        /// <param name="layoutPath">
        /// The layout path.
        /// </param>
        public MarketingReportHandler(string databaseName, string outputPath, string layoutPath)
            : base(databaseName, outputPath, layoutPath)
        {
        }

        #endregion

        #region Public

        /// <summary>
        /// The generate document download.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="usr">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="MemoryStream"/>.
        /// </returns>
        public MemoryStream GenerateDocumentDownload(MarketingPdfSettings settings, User usr)
        {
            return this.DownloadPreview(settings, usr);
        }

        /// <summary>
        /// Method to load or create a letter and/or schedule and email them to the client
        /// </summary>
        /// <param name="pdfSettings">
        /// The settings to be applied to the creation of the document
        /// </param>
        /// <param name="emailSettings">
        /// The settings to be applied to the creation of the email
        /// </param>
        /// <param name="usr">
        /// The details of the user creating the documents
        /// </param>
        /// <param name="recipients">
        /// The recipients.
        /// </param>
        /// <returns>
        /// A MemoryStream containing the combined documents
        /// </returns>
        public string EmailDocuments(MarketingPdfSettings pdfSettings, EmailSettings emailSettings, User usr, List<string> recipients)
        {
            string guid = string.Empty;
            try
            {
                var ms = this.CreateMarketingLetter(pdfSettings);
                byte[] document = null;
                if (ms != null)
                {
                    document = ms.ToArray();
                }

                List<ReportEmailHandler.EmailAttachment> attachments = new List<ReportEmailHandler.EmailAttachment>();

                if (document != null && document.Length > 0)
                {
                    attachments.Add(new ReportEmailHandler.EmailAttachment { AttachmentName = "MarketingLetter.pdf", AttachmentBytes = document });
                }

                guid = ReportEmailHandler.SendPDFEmail(usr.ClientDatabaseName, recipients, usr, emailSettings.Subject, emailSettings.BodyText, attachments);
            }
            catch (Exception ex)
            {
                if (ex.GetBaseException() is SmtpFailedRecipientException)
                {
                    ErrorLogger.LogToDb(this.DatabaseName, "MarketingReportHandler.EmailDocuments", pdfSettings, ex);
                    Logger.ErrorFormat("Error in MarketingReportHandler.EmailDocuments: {0}", ex);
                    Debug.WriteLine("Error in MarketingReportHandler.EmailDocuments: {0}", ex.ToString());
                }
                else
                {
                    ErrorLogger.LogToDb(this.DatabaseName, "MarketingReportHandler.EmailDocuments", pdfSettings, ex);
                    Logger.ErrorFormat("Error in MarketingReportHandler.EmailDocuments: {0}", ex);
                    Debug.WriteLine("Error in MarketingReportHandler.EmailDocuments: {0}", ex.ToString());
                    guid = string.Empty;
                }
            }

            return guid;
        }

        /// <summary>
        /// The download preview.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <param name="usr">
        /// The user.
        /// </param>
        /// <returns>
        /// The <see cref="MemoryStream"/>.
        /// </returns>
        public MemoryStream DownloadPreview(MarketingPdfSettings settings, User usr)
        {
            MemoryStream ms;
            try
            {
                ms = this.CreateMarketingLetter(settings);
            }
            catch (Exception ex)
            {
                ErrorLogger.LogToDb(this.DatabaseName, "MarketingReportHandler.DownloadPreview", settings, ex);
                Debug.WriteLine("Error in MarketingReportHandler.DownloadPreview: {0}", ex.ToString());
                Logger.ErrorFormat("Error in MarketingReportHandler.DownloadPreview: {0}", ex);
                ms = null;
            }

            return ms;
        }
        
        #endregion

        #region Private

        /// <summary>
        /// The create marketing letter.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// The <see cref="MemoryStream"/>.
        /// </returns>
        private MemoryStream CreateMarketingLetter(MarketingPdfSettings settings)
        {
            MarketingLetter document = this.MarketingGetLetter(settings);
            return document.CreateDocument(this.LayoutPath, settings);
        }

        /// <summary>
        /// The marketing get letter.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        /// <returns>
        /// The <see cref="Types.MarketingLetter"/>.
        /// </returns>
        private MarketingLetter MarketingGetLetter(MarketingPdfSettings settings)
        {
            // Create the document
            DAL.MarketingLetter rptContext = new DAL.MarketingLetter(this.DatabaseName);
            MarketingLetter document = rptContext.GetForMarketingRecId(settings.Id, this.OutputPath);
            return document;
        }
        
        #endregion
    }
}
