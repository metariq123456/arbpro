﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DGS.ArbPro.DAL;

namespace DGS.ArbPro.BL
{
    public class TemplateHandler
    {
        String DBName;

        public TemplateHandler(String DatabaseName)
        {
            DBName = DatabaseName;
        }

        public List<Types.TextTemplate> ListAll()
        {
            var DB = new DAL.TextTemplate(DBName);
            return DB.GetList();
        }

        public List<Types.TextTemplate> ListFiltered(Types.Enums.TextTemplateTypeEnum Type, Types.Enums.MessageTypeEnum Format)
        {
            var DB = new DAL.TextTemplate(DBName);
            return DB.GetList((Int32)Type, (Int32)Format);
        }

        public static List<Types.TextTemplate> ListFilteredByMessageType(Types.Enums.MessageTypeEnum Format, string databaseName)
        {
            var DB = new DAL.TextTemplate(databaseName);
            return DB.GetList(-1, (Int32)Format, -1);
        }

        public static List<Types.TextTemplate> ListFilteredByMessageType(string databaseName,Types.Enums.MessageTypeEnum Format, Types.Enums.TextTemplateTypeEnum tmplType)
        {
            DAL.TextTemplate dbContext = new DAL.TextTemplate(databaseName);
            return dbContext.GetList((int)tmplType, (int)Format, -1);
        }

        /// <summary>
        /// The marketing list filtered by message type.
        /// </summary>
        /// <param name="databaseName"> The database name. </param>
        /// <param name="format"> The format. </param>
        /// <param name="tmplType"> The template type. </param>
        /// <returns> List of text templates. </returns>
        public static List<Types.TextTemplate> MarketingListFilteredByMessageType(string databaseName, Types.Enums.MessageTypeEnum format, Types.Enums.TextTemplateTypeEnum tmplType)
        {
            DAL.TextTemplate dbContext = new DAL.TextTemplate(databaseName);
            return dbContext.GetMarketingList((int)tmplType, (int)format);
        }

        // DGSAP-873
        public static List<Types.SmsTextTemplate> ListFilteredSmsByMessageType(string databaseName, int quoteId, Types.Enums.TextTemplateTypeEnum tmplType)
        {
            DAL.SmsTextTemplate dbContext = new DAL.SmsTextTemplate(databaseName);
            return dbContext.GetList((int)tmplType, quoteId);
        }

        public Types.TextTemplate GetForID(Int32 ID)
        {
            var DB = new DAL.TextTemplate(DBName);
            var List = DB.GetList(-1, -1, ID);

            if (List.Count == 0)
                return null;
            else
                return List[0];
        }

        public static void Save(Types.TextTemplate obj, string databaseName)
        {
            var DB = new DAL.TextTemplate(databaseName);
            obj.Id = DB.Save(obj);
        }

        public static bool Delete(Types.TextTemplate obj, string databaseName)
        {
            var DB = new DAL.TextTemplate(databaseName);
            return DB.Delete(obj);
        }

        #region TextTemplates
        public static List<Types.TextTemplate> GetTextTemplatesList(String DatabaseName)
        {
            var DB = new DAL.TextTemplate(DatabaseName);
            return DB.GetList();
        }
        #endregion
    }
}
