﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using DGS.ArbPro.DAL;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.BL
{
    public class AdminHandler
    {
        /// <summary>
        /// Static method to get a list of all Admin users
        /// from the database
        /// </summary>
        /// <returns>A List of Types.AdminUser</returns>
        public static List<Types.AdminUser> ListUsers()
        {
            DAL.AdminUser dbContext = new DAL.AdminUser();
            return dbContext.GetAll();
        }

        /// <summary>
        /// Static method to get an Admin user from the database
        /// </summary>
        /// <param name="userName">The username of the admin user to be returned</param>
        /// <returns>A populated AdminUser. The id will -1 if there is an error</returns>
        public static Types.AdminUser GetUserForUsername(string userName)
        {
            var dbContext = new DAL.AdminUser();
            return dbContext.GetForUserName(userName);
        }

        /// <summary>
        ///  Static method to get an Admin user from the database
        /// </summary>
        /// <param name="email">The email address of the admin user to be returned</param>
        /// <returns>A populated AdminUser. The id will -1 if there is an error</returns>
        public static Types.AdminUser GetUserForEmail(string email)
        {
            var dbContext = new DAL.AdminUser();
            return dbContext.GetForEmail(email);
        }

        /// <summary>
        /// Static method to get an Admin user from the database
        /// </summary>
        /// <param name="UserId">The string representation of the Id of the admin user to be returned</param>
        /// <returns>A populated AdminUser. The id will -1 if there is an error</returns>
        public static Types.AdminUser GetUserForId(string UserId)
        {
            int id = -1;
            int.TryParse(UserId, out id);
            return GetUserForId(id);
        }

        /// <summary>
        /// Static method to get an Admin user from the database
        /// </summary>
        /// <param name="UserId">The Id of the admin user to be returned</param>
        /// <returns>A populated AdminUser. The id will -1 if there is an error</returns>
        public static Types.AdminUser GetUserForId(int UserId)
        {
            var dbContext = new DAL.AdminUser();
            return dbContext.GetForId(UserId);
        }

        /// <summary>
        /// Static method to unlock a locked Admin user
        /// </summary>
        /// <param name="User">The admin user to be unlocked</param>
        public static void UnlockAdmin(Types.AdminUser User)
        {
            if (User.Locked)
            {
                var dbContext = new DAL.AdminUser();
                User.AuthenticationFailures = 0;
                dbContext.Save(User);
            }
        }

        /// <summary>
        /// Static method to lock an admin user
        /// </summary>
        /// <param name="User">The admin user object that is to be locked</param>
        public static void LockAdmin(Types.AdminUser User)
        {
            if (!User.Locked)
            {
                var dbContext = new DAL.AdminUser();
                User.LockUser();
                dbContext.Save(User);
            }
        }

        /// <summary>
        /// Static method to insert of update an Admin user inthe database
        /// </summary>
        /// <param name="User">The admin user object that is to be saved or updated</param>
        /// <returns>The Id of the updated database row, -1 if there is an error</returns>
        public static int SaveAdmin(Types.AdminUser User)
        {
            var dbContext = new DAL.AdminUser();
            return dbContext.Save(User);
        }
    }
}

