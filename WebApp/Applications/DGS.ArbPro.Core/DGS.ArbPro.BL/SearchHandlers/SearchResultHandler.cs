﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL.SearchHandlers
{
    public class SearchResultHandler
    {
        public static List<Types.RiskSearchResult> GetRiskAssessmentListForQuote(string databaseName, int quoteId)
        {
            var dbContext = new DAL.RiskSearchResult(databaseName);
            return dbContext.GetListForQuote(quoteId);
        }

        public static List<Types.RiskSearchResult> GetClientRiskAssessmentList(string databaseName, int clientId)
        {
            var dbContext = new DAL.RiskSearchResult(databaseName);
            return dbContext.GetForClientId(clientId);
        }

        public static List<Types.AppointmentSearchResult> GetClientAppointmentList(string databaseName, int clientId, bool incompleteOnly)
        {
            var dbContext = new DAL.AppointmentSearchResult(databaseName);
            return dbContext.GetForClientId(clientId, incompleteOnly);
        }

        public static List<Types.SearchResult> GetClientJobList(string databaseName, int clientId)
        {
            var dbContext = new DAL.JobSearchResult(databaseName);
            return dbContext.GetForClientId(clientId);
        }

        public static List<Types.ClientSite> GetClientSiteList(string databaseName, int clientId)
        {
            var dbContext = new DAL.ClientSite(databaseName);
            return dbContext.GetForClient(clientId);
        }

        public static List<Types.ContactSearchResult> GetClientContactList(string databaseName, int clientId)
        {
            var dbContext = new DAL.ContactSearchResult(databaseName);
            return dbContext.GetForClientModel(clientId);
        }

        // DGSAP-873
        public static List<Types.QuoteContactSearchResult> GetQuoteClientContactList(string databaseName, int clientId)
        {
            var dbContext = new DAL.QuoteContactSearchResult(databaseName);
            return dbContext.GetForClientModel(clientId);
        }

        public static List<Types.SearchResult> GetClientForNameLike(string databaseName, string filterText)
        {
            var dbContext = new DAL.ClientSearchResult(databaseName);
            return dbContext.GetForNameLike(filterText.Trim());
        }
        public static List<Types.SearchResult> GetClientForNameFull(string databaseName, string filterText)
        {
            var dbContext = new DAL.ClientSearchResult(databaseName);
            return dbContext.GetForNameFull(filterText.Trim());
        }

        public static List<Types.SearchResult> GetForClientIdOrAccounts(string databaseName, string filterText)
        {
            var dbContext = new DAL.ClientSearchResult(databaseName);
            return dbContext.GetForIdOrAccount(filterText.Trim());
        }


        public static List<Types.SearchResult> GetClientForAddressLike(string databaseName, string filterText)
        {
            var dbContext = new DAL.ClientSearchResult(databaseName);
            return dbContext.GetForAddressLike(filterText.Trim());
        }

        public static List<Types.SearchResult> GetClientForEmailLike(string databaseName, string filterText)
        {
            var dbContext = new DAL.ClientSearchResult(databaseName);
            return dbContext.GetForEmailLike(filterText.Trim());
        }

        public static List<Types.SearchResult> GetClientForPostCodeLike(string databaseName, string filterText)
        {
            var dbContext = new DAL.ClientSearchResult(databaseName);
            return dbContext.GetForPostCodeLike(filterText.Trim());
        }

        public static List<Types.SearchResult> GetClientForTelephoneLike(string databaseName, string filterText)
        {
            var dbContext = new DAL.ClientSearchResult(databaseName);
            return dbContext.GetForTelephoneLike(filterText.Trim());
        }

        public static List<Types.SearchResult> GetContactDetailsLike(string databaseName, string filterText)
        {
            var dbContext = new DAL.ContactSearchResult(databaseName);
            return dbContext.GetForContactDetailsLike(filterText.Trim());
        }

        public static List<Types.SearchResult> GetContactNameLike(string databaseName, string filterText)
        {
            var dbContext = new DAL.ContactSearchResult(databaseName);
            return dbContext.GetForNameLike(filterText.Trim());
        }

        public static List<Types.SearchResult> GetQuoteClientNumberLike(string databaseName, string clientId)
        {
            // DGSAP-822 - Changed stored procedure call for this search function
            var dbContext = new DAL.QuoteSearchResult(databaseName);
            return dbContext.GetForClientAccount(clientId);
        }
        public static List<Types.SearchResult> GetQuoteIdLike(string databaseName, string filterText)
        {
            var dbContext = new DAL.QuoteSearchResult(databaseName);
            return dbContext.GetForQuoteId(filterText);
        }

        public static List<Types.SearchResult> GetQuoteStatusLike(string databaseName, string clientId, Types.Enums.QuoteStatusEnum Status)
        {
            var dbContext = new DAL.QuoteSearchResult(databaseName);
            return dbContext.GetForStatus(Status);
        }

        public static List<Types.SearchResult> GetQuoteNameLike(string databaseName, string filterText)
        {
            var dbContext = new DAL.QuoteSearchResult(databaseName);
            return dbContext.GetForNameLike(filterText);
        }

        public static List<Types.SearchResult> GetQuoteSiteAddressLike(string databaseName, string filterText)
        {
            var dbContext = new DAL.QuoteSearchResult(databaseName);
            return dbContext.GetForSiteAddressLike(filterText);
        }

        public static Types.ClientSearchResult GetClientForId(string databaseName, int clientId)
        {
            var dbContext = new DAL.ClientSearchResult(databaseName);
            return dbContext.GetForClientId(clientId);
        }

        public static List<Types.SearchResult>GetDocumentList(string databaseName, string filterText)
        {
            var dbContext = new DAL.DocumentHistorySearchResult(databaseName);
            return dbContext.GetAllForDocumentRefLike(filterText);
        }

        public static List<Types.SearchResult> GetQuoteDocumentList(string databaseName, string filterText)
        {
            var dbContext = new DAL.QuoteSearchResult(databaseName);
            return dbContext.GetForReferenceLike(filterText);
        }

        public static List<Types.SearchResult> GetInvoiceDocumentList(string databaseName, string filterText)
        {
            var dbContext = new DAL.InvoiceSearchResult(databaseName);
            return dbContext.GetForReferenceLike(filterText);
        }

        public static Types.MapDetailsSearchResult GetMapDetailsFroQuoteSite(int quoteId, int siteId, string database)
        {
            DAL.MapDetailsSearchResult dbContext = new DAL.MapDetailsSearchResult(database);
            return dbContext.GetForQuoteSite(quoteId, siteId);
        }

        //Invoice
        public static List<Types.SearchResult> GetInvoiceForId(string databaseName, string filterText)
        {
            var dbContext = new DAL.InvoiceSearchResult(databaseName);
            return dbContext.GetForId(filterText);
        }

        public static List<Types.SearchResult> GetForInvClientId(string databaseName, string filterText)
        {
            int clientId;
            bool res = int.TryParse(filterText, out clientId);
            clientId = res ? clientId : -1;

            var dbContext = new DAL.InvoiceSearchResult(databaseName);
            return dbContext.GetForInvClientId(clientId);
        }

        public static List<Types.SearchResult> GetForInvNameLike(string databaseName, string filterText)
        {
            var dbContext = new DAL.InvoiceSearchResult(databaseName);
            return dbContext.GetForClientNameLike(filterText);
        }

        public static List<Types.SearchResult> GetForInvSiteAddress(string databaseName, string filterText)
        {
            var dbContext = new DAL.InvoiceSearchResult(databaseName);
            return dbContext.GetForInvSiteAddressLike(filterText);
        }

        public static List<Types.SearchResult> GetForJobId(string databaseName, string filterText)
        {
            int jobId;
            bool res = int.TryParse(filterText, out jobId);
            jobId = res ? jobId : -1;

            var dbContext = new DAL.JobSearchResult(databaseName);
            return dbContext.GetForId(jobId);
        }
    }
}
