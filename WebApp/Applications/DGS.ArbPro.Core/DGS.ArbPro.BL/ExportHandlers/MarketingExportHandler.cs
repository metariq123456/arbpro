﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MarketingExportHandler.cs" company="Microsec Ltd">
//   2016 Copyright Microsec Ltd
// </copyright>
// <summary>
//   The marketing export handler.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.BL.ExportHandlers
{
    using System.Collections.Generic;

    using DGS.ArbPro.Types.ReportObjects;

    /// <summary>
    /// The marketing export handler.
    /// </summary>
    public class MarketingExportHandler
    {
        /// <summary>
        /// The export.
        /// </summary>
        /// <param name="obj"> List of marketing objects. </param>
        /// <returns> Marketing export object </returns>
        public static MarketingExport Export(List<Types.Marketing> obj)
        {
            MarketingExport exportObj = new MarketingExport();

            MarketingExportLine firstLine = new MarketingExportLine();

            if (exportObj.ExportLines.Count == 0)
            {
                firstLine.Column1 = "Email Address";
                firstLine.Column2 = "Name";
                firstLine.Column3 = "Address";
                firstLine.Column4 = "Telephone";
                firstLine.Column5 = "Mobile";
                firstLine.Column6 = "Work Category";

                exportObj.ExportLines.Add(firstLine);
            }

            foreach (Types.Marketing o in obj)
            {
                MarketingExportLine line = new MarketingExportLine();
                
                line.Column1 = o.Email;
                line.Column2 = o.ClientName;
                line.Column3 = o.Address;
                line.Column4 = o.Telephone;
                line.Column5 = o.Mobile;
                line.Column6 = o.Name;

                exportObj.ExportLines.Add(line);
            }

            return exportObj;
        }
    }
}
