﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DGS.ArbPro.BL.ExportHandlers
{
    public class GraphExportHandler 
    {
        public GraphExportHandler()
        {

        }
        public static Types.ReportObjects.GraphExport GraphExportLeadSource(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportLeadDource(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforReceived(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforReceived(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforQuotationSent(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforQuotationSent(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforQuotationAccepted(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforQuotationAccepted(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforTreeWorkBooked(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforTreeWorkBooked(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforTreeProgress(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforTreeProgress(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforTreeCompleted(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforTreeCompleted(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforSalesHistorically(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforSalesHistorically(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforSalesOutstanding(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforSalesOutstanding(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforSuccessRate(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforSuccessRate(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforSales(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforSales(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforInvoicesPaid(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforInvoicesPaid(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforInvoicesVatPaid(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforInvoicesVatPaid(FromDate, ToDate);
        }
        public static Types.ReportObjects.GraphExport GraphExportforInvoicesTotalSaleIncVat(String FromDate, String ToDate, String DatabaseName)
        {
            var GExport = new DAL.GraphExport(DatabaseName);
            return GExport.GraphExportforInvoicesTotalSaleIncVat(FromDate, ToDate);
        }

        
        

        
    }
}