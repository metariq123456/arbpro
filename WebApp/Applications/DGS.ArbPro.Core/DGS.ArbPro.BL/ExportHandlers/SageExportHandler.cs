﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums.SageEnums;

namespace DGS.ArbPro.BL.ExportHandlers
{
    public class SageExportHandler
    {
        public static Types.ReportObjects.SageExport Export(SageTypeEnum OutputType, ExportTypeEnum ObjectType, DateTime FromDate, DateTime ToDate, String DatabaseName)
        {
            switch (OutputType)
            {
                case SageTypeEnum.KASHFLOW:
                    return ExportKashFlow(DatabaseName, ObjectType, FromDate, ToDate);
                case SageTypeEnum.QBKS:
                    return ExportQuickbooks(DatabaseName, ObjectType, FromDate, ToDate);
                case SageTypeEnum.SAGE09:
                    return ExportSage09(DatabaseName, ObjectType,FromDate,ToDate);
                case SageTypeEnum.SAGE13:
                    return ExportSage13(DatabaseName, ObjectType, FromDate, ToDate);
                case SageTypeEnum.XERO:
                    return ExportXero(DatabaseName, ObjectType, FromDate, ToDate);
                case SageTypeEnum.CSV:
                    return ExportInvoice(DatabaseName, ObjectType, FromDate, ToDate);
            }
            return new Types.ReportObjects.SageExport();
        }

        private static Types.ReportObjects.SageExport ExportSage13(String DatabaseName, ExportTypeEnum ObjectType, DateTime FromDate, DateTime ToDate)
        {
            var SExport = new DAL.SageExport(DatabaseName);
            return SExport.Export(SageTypeEnum.SAGE13, ObjectType, FromDate, ToDate);
        }

        private static Types.ReportObjects.SageExport ExportSage09(String DatabaseName, ExportTypeEnum ObjectType, DateTime FromDate, DateTime ToDate)
        {
            var SExport = new DAL.SageExport(DatabaseName);
            return SExport.Export(SageTypeEnum.SAGE09, ObjectType, FromDate, ToDate);
        }

        private static Types.ReportObjects.SageExport ExportQuickbooks(String DatabaseName, ExportTypeEnum ObjectType, DateTime FromDate, DateTime ToDate)
        {
            var SExport = new DAL.SageExport(DatabaseName);
            return SExport.Export(SageTypeEnum.QBKS, ObjectType, FromDate, ToDate);
        }

        private static Types.ReportObjects.SageExport ExportKashFlow(String DatabaseName, ExportTypeEnum ObjectType, DateTime FromDate, DateTime ToDate)
        {
            var SExport = new DAL.SageExport(DatabaseName);
            return SExport.Export(SageTypeEnum.KASHFLOW, ObjectType, FromDate, ToDate);
        }
        private static Types.ReportObjects.SageExport ExportXero(String DatabaseName, ExportTypeEnum ObjectType, DateTime FromDate, DateTime ToDate)
        {
            var SExport = new DAL.SageExport(DatabaseName);
            return SExport.Export(SageTypeEnum.XERO, ObjectType, FromDate, ToDate);
        }

        private static Types.ReportObjects.SageExport ExportInvoice(String DatabaseName, ExportTypeEnum ObjectType, DateTime FromDate, DateTime ToDate)
        {
            var SExport = new DAL.SageExport(DatabaseName);
            return SExport.ExportToCSVInvoice(SageTypeEnum.CSV, ObjectType, FromDate, ToDate);
        }
    }
}
