﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.BL.Enums
{
    public enum InvoiceTypeEnum
    {
        InvoiceLetter,
        FirstReminder,
        SecondReminder,
        FinalReminder,
        FirstLegalAction,
        SecondLegalAction
    }
}
