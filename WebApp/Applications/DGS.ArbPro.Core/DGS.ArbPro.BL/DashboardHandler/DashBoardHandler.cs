﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;
using DGS.ArbPro.DAL;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.BL
{
    public class DashBoardHandler
    {
        #region Members
        #endregion

        #region Constructors
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Invoices
        public static List<SearchResult> GetOverdueInvoices(string databaseName)
        {
            var dbContext = new DAL.InvoiceSearchResult(databaseName);
            return dbContext.GetAllOverdue();
            //return GetInvoices(databaseName, DateTime.Now, true);
        }

        public static Types.SearchSummary GetOverdueInvoicesSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResultsWithCultureCodeForInvoice(GetOverdueInvoices(databaseName));
            return retVal;
        }

        public static List<SearchResult> GetInvoicesDueInWeek(string databaseName)
        {
            return GetInvoices(databaseName, DateTime.Now.AddDays(7), false);
        }

        public static List<SearchResult> GetInvoicesDue(string databaseName)
        {
            return GetAllInvoices(databaseName);
        }

        public static List<SearchResult> GetInvoicesSent(string databaseName)
        {
            return GetAllInvoicesSent(databaseName);
        }
        public static List<SearchResult> GetInvoicesPaid(string databaseName)
        {
            return GetAllInvoicesPaid(databaseName);
        }
        public static Types.SearchSummary GetInvoicesDueInWeekSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResults(GetInvoicesDueInWeek(databaseName));
            return retVal;
        }

        public static Types.SearchSummary GetInvoicesDue(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResultsWithCultureCodeForInvoice(GetInvoicesDue(databaseName));
            return retVal;
        }
        public static Types.SearchSummary GetInvoicesSent(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResultsWithCultureCodeForInvoice(GetInvoicesSent(databaseName));
            return retVal;
        }
        public static Types.SearchSummary GetInvoicesPaid(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResultsWithCultureCodeForInvoice(GetInvoicesPaid(databaseName));
            return retVal;
        }

        public static List<SearchResult> GetPendingInvoices(string databaseName)
        {
            //NOTE: This function should display all search results that require an invoice to be sent
            //This includes work that has been completed but not yet invoiced
            //Get all invoices that have not been sent
            DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(databaseName);
            List<SearchResult> retVal = dbContext.GetInvoiceableWork();
            return retVal;
        }

        public static Types.SearchSummary GetPendingInvoicesSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResultsWithCultureCodeForInvoice(GetPendingInvoices(databaseName));
            return retVal;
        }
        #endregion

        #region Quotes

        public static Types.SearchSummary GetPendingQuotesSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResultsWithCultureCodeForQuote(GetQuotesForStatus(databaseName, QuoteStatusEnum.Pending));
            return retVal;
        }

        public static Types.SearchSummary GetSentQuotesSummary(string databaseName, string title, int days)
        {
            Types.SearchSummary retVal = new SearchSummary(title);

            DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(databaseName);
            var results = dbContext.GetSentWithinDays(days);

            retVal.SetSearchResultsWithCultureCodeForQuote(results);
            
            return retVal;
        }
        public static Types.SearchSummary GetAllSentQuotesSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);

            DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(databaseName);
            var results = dbContext.GetAllSentQuotes();

            retVal.SetSearchResultsWithCultureCodeForQuote(results);

            return retVal;
        }
        #endregion

        #region Work
        public static List<SearchResult> GetPendingWork(string databaseName)
        {
            DAL.WorkSearchResult dbContext = new DAL.WorkSearchResult(databaseName);
            return dbContext.GetWorkNotBooked();
        }
        public static List<SearchResult> GetPendingJobWork(string databaseName,int Month)
        {
            DAL.WorkSearchResult dbContext = new DAL.WorkSearchResult(databaseName);
            return dbContext.GetJobWorkNotBooked(Month);
        }

        public static List<SearchResult> GetJobNotAcceptedWork(string databaseName, int Month)
        {
            DAL.WorkSearchResult dbContext = new DAL.WorkSearchResult(databaseName);
            return dbContext.GetJobNotAcceptedWork(Month);
        }

        public static List<SearchResult> GetAllJobWork(string databaseName, int Month)
        {
            DAL.WorkSearchResult dbContext = new DAL.WorkSearchResult(databaseName);
            return dbContext.GetAllJobWork(Month);
        }
        public static Types.SearchSummary GetPendingWorkSummary(string databaseName, string title)
        {

            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResultsWithCultureCode(GetPendingWork(databaseName));
            return retVal;
        }
        public static Types.SearchSummary GetJobPendingWorkSummary(string databaseName, string title,int Month)
        {

            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResultsWithCultureCode(GetPendingJobWork(databaseName, Month));
            return retVal;
        }

        public static Types.SearchSummary GetJobNotAcceptedWorkSummary(string databaseName, string title, int Month)
        {

            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResultsWithCultureCode(GetJobNotAcceptedWork(databaseName, Month));
            return retVal;
        }

        public static Types.SearchSummary GetAllJobWorkSummary(string databaseName, string title, int Month)
        {

            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResultsWithCultureCode(GetAllJobWork(databaseName, Month));
            return retVal;
        }
        #endregion

        #region Notes

        public static List<SearchResult> GetRecentQuoteNotes(string databaseName)
        {
            DAL.NoteSearchResult dbContext = new DAL.NoteSearchResult(databaseName);
            return dbContext.GetSelected(); // Changed from GetAll, to only get items that have been selected for display
        }


        public static List<SearchResult> GetSelectedQuoteNotes(string databaseName)
        {
            DAL.NoteSearchResult dbContext = new DAL.NoteSearchResult(databaseName);
            return dbContext.GetSelected();
        }

        public static Types.SearchSummary GetRecentQuoteNotesSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResults(GetSelectedQuoteNotes(databaseName));
            return retVal;
        }

        #endregion

        #region Jobs
        public static List<SearchResult> GetPendingJobs(string databaseName)
        {
            DAL.JobSearchResult dbContext = new DAL.JobSearchResult(databaseName);
            return dbContext.GetPendingForDashboard(); // DGSAP-726 .GetForStatus(Types.Enums.JobStatusEnum.Pending);
        }

        public static Types.SearchSummary GetPendingJobsSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResults(GetPendingJobs(databaseName));
            return retVal;
        }
        #endregion

        #region Appointments
        public static List<Types.AppointmentSearchResult> GetTodaysAppointments(string databaseName)
        {
            DAL.AppointmentSearchResult dbContext = new DAL.AppointmentSearchResult(databaseName);
            return dbContext.GetForDate(DateTime.Now);
        }

        public static Types.SearchSummary GetTodaysAppointmentSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResults(GetTodaysAppointments(databaseName));
            return retVal;
        }

        public static List<Types.AppointmentSearchResult> GetQuoteAppsForWeek(string databaseName)
        {
            DAL.QuoteAppointmentSearch dbContext = new DAL.QuoteAppointmentSearch(databaseName);
            return dbContext.GetForDateRange(DateTime.Today, DateTime.Now.AddYears(1));
        }

        public static Types.SearchSummary GetQuoteAppsForWeekSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResults(GetQuoteAppsForWeek(databaseName));
            return retVal;
        }

        public static List<Types.AppointmentSearchResult> GetWorkAppsForWeek(string databaseName)
        {
            DAL.WorkAppointmentSearch dbContext = new DAL.WorkAppointmentSearch(databaseName);
            // DGSAP-726 Modification to get all work appointments for the next 30 days. 331 days are added to take into account midnight.
            return dbContext.GetForDateRange(DateTime.Today, DateTime.Today.AddDays(31));
        }

        public static Types.SearchSummary GetWorkAppsSummaryForMonth(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResults(GetWorkAppsForWeek(databaseName), true);
            return retVal;
        }

        public static List<Types.AppointmentSearchResult> GetCompanyAppsForWeek(string databaseName)
        {
            return null;//return GetAppointmentsForWeek(databaseName, Types.Enums.AppointmentTypeEnum.Company);
        }

        public static Types.SearchSummary GetCompanyAppsForWeekSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary("Coming Soon","Coming Soon: Currently in development");
            return retVal;
        }

        public static List<Types.AppointmentSearchResult> GetGenericAppsDateRange(string databaseName, int daySpan)
        {

            DAL.GenericAppointmentSearch dbContext = new DAL.GenericAppointmentSearch(databaseName);
            return dbContext.GetBetweenDates(DateTime.Today, DateTime.Today.AddDays(daySpan));
        }

        public static Types.SearchSummary GetGenericAppSummaryForDateRange(string databaseName, string title, int daySpan)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResults(GetGenericAppsDateRange(databaseName,31));
            return retVal;
        }

        #endregion


        #region TPO
        public static Types.SearchSummary TposGetEightWeekExpiredSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResults(TposGetForTypeStageAndStatus(databaseName, TreeProtectionOrderTypeEnum.TPO, TreeProtectionProgreesEnum.Application_Made, TreeProtectionStatusEnum.Protected));
            return retVal;
        }

        public static Types.SearchSummary TposGetSixWeekExpiredSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResults(TposGetForTypeStageAndStatus(databaseName, TreeProtectionOrderTypeEnum.Conservation_Area, TreeProtectionProgreesEnum.Application_Made, TreeProtectionStatusEnum.Protected));
            return retVal;
        }

        public static List<Types.SearchResult> TposGetAppeals(string databaseName)
        {
            DAL.TpoSearchResult dbContext = new DAL.TpoSearchResult(databaseName);
            return dbContext.GetAppealedNoDescisionDate();
        }

        public static Types.SearchSummary TposGetAppealsSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResults(TposGetAppeals(databaseName));
            return retVal;
        }

        public static List<Types.SearchResult> TposGetGranted(string databaseName)
        {
            DAL.TpoSearchResult dbContext = new DAL.TpoSearchResult(databaseName);
            return dbContext.GetGenericGrantedNotBooked();
        }

        public static Types.SearchSummary TposGetGrantedSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title);
            retVal.SetSearchResults(TposGetGranted(databaseName));
            return retVal;
        }
        #endregion

        #region Finances

        public static Types.SearchSummary CompanyFinanceSummary(string databaseName, string title)
        {
            Types.SearchSummary retVal = new SearchSummary(title, false);

            var dbContext = new DAL.FinanceSearchResult(databaseName);
            var summary = dbContext.DashboardSummary();
            retVal.SetSearchResults(summary,false);
            return retVal;
        }
        #endregion

        #region Private
        private static List<SearchResult> GetQuotesForStatus(string databaseName, Types.Enums.QuoteStatusEnum status)
        {
            DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(databaseName);
            return dbContext.GetForStatus(status);
        }


        private static List<SearchResult> GetInvoices(string databaseName, DateTime forDueBy, bool includeOverdue)
        {
            DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(databaseName);
            return dbContext.GetForDueDate(forDueBy, includeOverdue);
        }

        private static List<SearchResult> GetAllInvoices(string databaseName)
        {
            DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(databaseName);
            return dbContext.GetAllDue();
        }

        private static List<SearchResult> GetAllInvoicesPaid(string databaseName)
        {
            DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(databaseName);
            return dbContext.GetAllPaid();
        }
        private static List<SearchResult> GetAllInvoicesSent(string databaseName)
        {
            DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(databaseName);
            return dbContext.GetAllSent();
        }
        private static List<Types.AppointmentSearchResult> GetAppointmentsForWeek(string databaseName, Types.Enums.AppointmentTypeEnum itemType)
        {
            DAL.AppointmentSearchResult dbContext = new DAL.AppointmentSearchResult(databaseName);
            return dbContext.GetForTypeAndDate(itemType, DateTime.Now.AddDays(7));
        }

        private static List<Types.SearchResult> TposGetForTypeStageAndStatus(string database,
            TreeProtectionOrderTypeEnum orderType, TreeProtectionProgreesEnum stage, TreeProtectionStatusEnum status)
        {
            DAL.TpoSearchResult dbContext = new DAL.TpoSearchResult(database);
            return dbContext.GetForDashBoard(orderType, stage, status);
        }
        #endregion
        #endregion
        #endregion
    }
}
