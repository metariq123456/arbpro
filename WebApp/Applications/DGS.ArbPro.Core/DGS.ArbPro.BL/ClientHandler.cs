﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL
{
    using DGS.ArbPro.Types.Enums;

    public class ClientHandler
    {
        /// <summary>
        /// Static method to get all the client contacts for a client Id
        /// </summary>
        /// <param name="databaseName">The database to attach to</param>
        /// <param name="clientId">the Id of the client</param>
        public static List<Types.ClientContact> GetContactsList(string databaseName, int clientId)
        {
            DAL.ClientContact dbContext = new DAL.ClientContact(databaseName);
            return dbContext.GetForClient(clientId);
        }

        public static bool SetClientDeleted(string databaseName, int clientId)
        {
            DAL.Client dbContext = new DAL.Client(databaseName);
            return dbContext.Delete(clientId);
        }

        public static int SaveClientContact(string database, Types.ClientContact obj)
        {
            var dbContext = new DAL.ClientContact(database);
            return dbContext.Save(obj);
        }
        public static int SaveDocumentsFile(string database, Types.ClientContact obj)
        {
            var dbContext = new DAL.ClientContact(database);
            return dbContext.Save(obj);
        }


        /// <summary>
        /// Static method to set the contact status and to ensure a default contact is set
        /// </summary>
        /// <param name="database">The database to attach to</param>
        /// <param name="contactId"> The Id of the client contact</param>
        /// <param name="contactType">The type of the contact</param>
        /// <returns></returns>
        public static bool SetContactStatus(string database, int contactId, int contactType)
        {
            var contactDb = new DAL.ClientContact(database);
            var contact = contactDb.GetForId(contactId);
            var contactList = contactDb.GetForClient(contact.ClientId);
            var newContactType = (Types.Enums.ContactTypeEnum)contactType;
            var contactSave = false;

            if (contactList.Count < 2)
            {
                // if we have only 1 contact then he will always be a default one
                newContactType = ContactTypeEnum.Default;
                contactSave = false;
            } 
            else if (contact.ContactType == ContactTypeEnum.Default)
            {
                // if the current contact is the default one then we need to find a new one to set to default
                var newDefault = contactList.FindAll((c) => { return c.Id != contact.Id; }).FirstOrDefault();
                newDefault.ContactType = ContactTypeEnum.Default;
                contactDb.Save(newDefault);
                contactSave = true;
            }
            else if (newContactType == ContactTypeEnum.Default)
            {
                // if we are changing the current contact to default then we need to find the existing default contact and change him to general
                var curDefault = contactList.Find((c) => { return c.ContactType == ContactTypeEnum.Default; });
                if (curDefault != null)
                {
                    curDefault.ContactType = ContactTypeEnum.General;
                    contactDb.Save(curDefault);
                }
                contactSave = true;
            }
            else
            {
                // checking if we have a default contact at all
                var curDefault = contactList.Find((c) => { return c.ContactType == ContactTypeEnum.Default; });
                if (curDefault == null)
                {
                    // if we do not have a default contact we set the first unchanged contact as a default one
                    var newDefault = contactList.FindAll((c) => { return c.Id != contact.Id; }).FirstOrDefault();
                    newDefault.ContactType = ContactTypeEnum.Default;
                    contactDb.Save(newDefault);
                }
                contactSave = true;
            }
            contact.ContactType = newContactType;
            contactDb.Save(contact);
            return contactSave;
        }
    }
}
