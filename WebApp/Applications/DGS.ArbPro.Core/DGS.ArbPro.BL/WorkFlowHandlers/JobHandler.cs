﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.BL.WorkFlowHandlers
{
    public class JobHandler
    {
        #region Accessors
        #region Job
        public static Types.Job GetJob(string database, int jobId)
        {
            var dbContext = new DAL.Job(database);
            return dbContext.GetForId(jobId);
        }

        public static int CreateJob(string database, Types.User usr, int clientId)
        {
            var dataContext = new DAL.Job(database);
            var obj = new Types.Job
            {
                ClientId = clientId,
                ContactId = -1,
                Source = "Arb Pro",
                Comments = "Job added within Arb Pro",
                ReporterID = usr.Id,
                Reporter = usr.NameStr
            };
            return dataContext.Save(obj);
        }

        public static int CreateJob(Types.User currentUser, Types.Job job, List<Types.ClientSite> selectedSites)
        {
            var dataContext = new DAL.Job(currentUser.ClientDatabaseName);
            job.ReporterID = currentUser.Id;
            job.Reporter = currentUser.NameStr;
            var newJobId = dataContext.Save(job);

            if (newJobId > 0)
            {
                // The job has been saved so create a job site for each of the selected sites
                Types.JobSite js = null;
                var siteContext = new DAL.JobSite(currentUser.ClientDatabaseName);
                int siteId = -1;
                foreach (var selectedSite in selectedSites)
                {
                    js = new JobSite
                    {
                        JobId = newJobId,
                        ContactId = job.ContactId,
                        ClientId = job.ClientId,
                        Site = selectedSite
                    };
                    siteId = siteContext.Save(js);
                    if (siteId < 1)
                    {
                        // The job site was not created correctly
                        newJobId = -1;
                        break;
                    }
                }
            }

            return newJobId;
        }
        #endregion

        #region JobSite
        public static List<Types.JobSite> GetJobSitesForQuote(string database, int quoteId, bool includeItems)
        {
            DAL.JobSite dbContext = new DAL.JobSite(database);
            return dbContext.GetForQuote(quoteId,includeItems);
        }
        #endregion
        #region Lists
        public static List<Types.JobItem> GetItemsForClientSite(string databaseName, int clientSiteId)
        {
            DAL.JobItem dbContext = new DAL.JobItem(databaseName);
            return dbContext.GetAllForClientSite(clientSiteId);
        }

        public static List<Types.DocumentHistory> GetHistoryForJob(string databaseName, int jobId)
        {
            var retVal = new List<Types.DocumentHistory>(); 
            var qDbContext = new DAL.QuoteHistoryItem(databaseName);
            var iDbContext = new DAL.InvoiceHistoryItem(databaseName);
            //  Get the quote documents for the job and add them to the return value
            retVal.AddRange(qDbContext.GetQuoteDocumentsForJob(jobId));
            //  Get the invoice documents for the job and add them to the return value;
            retVal.AddRange(iDbContext.GetInvoiceDocumentsForJob(jobId));
            //  Iterate through all of the documents and ensure the document has not been set
            foreach (var d in retVal)
            {
                d.Doc = null;
            }
            // return the list
            return retVal;
        }

        public static int UpdateJobDetails(Types.Job obj, string database)
        {
            var dbContext = new DAL.Job(database);
            return dbContext.Save(obj);
        }
        #endregion
        #endregion
    }
}
