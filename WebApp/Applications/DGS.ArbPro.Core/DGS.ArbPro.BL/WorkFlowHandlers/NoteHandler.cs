﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.BL
{
    public class NoteHandler
    {
        public static Types.Note GetNewNote(int parentId, NoteTypeEnum parentType, string user)
        {
            Types.Note obj = new Types.Note(parentId, parentType);
            obj.EnteredBy = user;
            return obj;
        }

        public static Types.Note GetNote(Types.User usr, int parentId, int id, NoteTypeEnum parentType)
        {
            if (id < 1)
            {
                //  A new note has been requested
                var note = new Types.Note(parentId, parentType);
                note.EnteredBy = string.Format("{0} {1}", usr.FirstName, usr.LastName).Trim();
                return note;
            }
            else
            {
                DAL.Note dbContext = new DAL.Note(usr.ClientDatabaseName);
                return dbContext.GetForId(id);
            }
        }


        public static Types.Note GetForId(string databaseName, int id)
        {
            DAL.Note dbContext = new DAL.Note(databaseName);
            return dbContext.GetForId(id);
        }

        public static List<Types.Note> GetNotesForParent(string databaseName, int parentId, NoteTypeEnum parentType)
        {
                DAL.Note dbContext = new DAL.Note(databaseName);
                return dbContext.GetForParent(parentId, parentType);
        }

        public static List<Types.Note> GetInvoiceNotesForQuote(string databaseName, int quoteId)
        {
            DAL.Note dbContext = new DAL.Note(databaseName);
            return dbContext.GetForInvoiceNotesForQuote(quoteId);
        }

        public static int Save(string databaseName, Types.Note obj)
        {
            DAL.Note dbContext = new DAL.Note(databaseName);
            return dbContext.Save(obj);
        }
    }
}
