﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL
{
    public class PermitsNecessaryHandler
    {
        public static Types.PermitsNecessary GetPermitsNecessaryForQuoteId(string databaseName, int quoteId)
        {
            DAL.PermitsNecessary dbContext = new DAL.PermitsNecessary(databaseName);
            return dbContext.GetForQuote(quoteId);
        }

        public static int SavePermitsNecessary(string databaseName, Types.PermitsNecessary PermtisNecessary)
        {
            DAL.PermitsNecessary dbContext = new DAL.PermitsNecessary(databaseName);
            return dbContext.Save(PermtisNecessary);
        }
    }
}
