﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.DAL;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.BL.WorkFlowHandlers
{
    public class QuoteHandler
    {
        public static Types.Quote GetForID(String DatabaseName, Int32 Id, Boolean IncludeHistoryDoc)
        {
            var DB = new DAL.Quote(DatabaseName);

            var ReturnValue = DB.GetForId(Id);

            if (!IncludeHistoryDoc)
            {
                foreach (var History in ReturnValue.History)
                {
                    History.Doc = null;
                }
            }

            return ReturnValue;
        }

        public static Types.Quote GetOnlyQuoteForID(String DatabaseName, Int32 Id)
        {
            var DB = new DAL.Quote(DatabaseName);

            var ReturnValue = DB.GetQuoteOnlyById(Id);
            
            return ReturnValue;
        }

        public static int DeletePONoFromQuote(String DatabaseName, Int32 Id)
        {
            var DB = new DAL.Quote(DatabaseName);
            int ReturnValue = DB.DeletePONoFromQuote(Id);
            return ReturnValue;
        }

        public static int SavePONoOnQuote(String DatabaseName, Int32 Id,string PONo)
        {
            var DB = new DAL.Quote(DatabaseName);
            int ReturnValue = DB.SavePONoOnQuote(Id, PONo);
            return ReturnValue;
        }

        public static Types.Quote DuplicateQuote(string databaseName, int id, bool keepRisk)
        {
            DAL.Quote dbContext = new DAL.Quote(databaseName);
            var newQuote = dbContext.Duplicate(id);

            DAL.Risk riskDB = new DAL.Risk(databaseName);
            DAL.TreeProtectionOrder tpoDB = new DAL.TreeProtectionOrder(databaseName);

            // Get list of risk assessment search result for QuoteId
            // Iterate through results calling Risk assessment.Duplicate(riskId,newQuoteId)

            if (keepRisk)
            {
                DAL.RiskSearchResult riskSearch = new DAL.RiskSearchResult(databaseName);
                List<Types.RiskSearchResult> listRisk = riskSearch.GetListForQuote(id);
                foreach (Types.RiskSearchResult rs in listRisk)
                {
                    riskDB.DuplicateRiskAssessmentForQuote(rs.Id, newQuote.Id);
                }
            }

            //Run the duplication of TPOs stored procedure
            tpoDB.DuplicateTPOForQuote(id, newQuote.Id);

            // DGSAP-813 - Duplication of work plan tools for a quote
            DAL.WorkPlan wpContext = new DAL.WorkPlan(databaseName);
            wpContext.DuplicateToolsListForQuote(id, newQuote.Id);

            return newQuote;
        }

        public static List<Types.Quote> GetForJob(String DatabaseName, Int32 JobId)
        {
            var DB = new DAL.Quote(DatabaseName);

            var ReturnValue = DB.GetForJob(JobId);

            return ReturnValue;
        }

        public static bool DeletePendingForId(String DatabaseName, Int32 QuoteId)
        {
            var DB = new DAL.Quote(DatabaseName);

            var ReturnValue = DB.DeletePendingForId(QuoteId);

            return ReturnValue;
        }

        public static List<Types.QuoteHistoryItem> GetHistory(String DatabaseName, Int32 JobId, Boolean IncludeHistoryDoc)
        {
            var DB = new DAL.QuoteHistoryItem(DatabaseName);

            var ReturnValue = DB.GetQuoteDocumentsForJob(JobId);

            if (!IncludeHistoryDoc)
            {
                foreach (var History in ReturnValue)
                {
                    History.Doc = null;
                }
            }

            return ReturnValue;
        }

        public static Types.QuoteHistoryItem GetHistoryRec(String DatabaseName, Int32 id, Boolean IncludeHistoryDoc = true)
        {
            var DB = new DAL.QuoteHistoryItem(DatabaseName);

            return DB.GetQuoteDocument(id);
        }

        public static bool SaveQuote(String DatabaseName, Types.Quote obj)
        {
            DAL.Quote dbContext = new DAL.Quote(DatabaseName);
            var retVal = dbContext.Save(obj);
            return retVal > 0;
        }

        /// <summary>
        /// Static method to add or update quote items for a quote.
        /// The method will generate a new quote record if the quoteId is -1.
        /// New quote items that do not have a job item Id will automatically generate one.
        /// </summary>
        /// <param name="database">The name of the database to connect to</param>
        /// <param name="jobId">The Id of the job the quote is for</param>
        /// <param name="quoteId">The Id of the Quote -1 if new</param>
        /// <param name="jobSiteId">The Id of the job site the items are being added to</param>
        /// <param name="items">The list of new / edited items</param>
        /// <returns>boolean: true if completed without error</returns>
        public static bool SaveQuoteItems(string database, int jobId, int quoteId, int jobSiteId, List<Types.QuoteItem> items)
        {
            //Check if the items are being added to a new quote
            if (quoteId == -1)
            {
                //The items are being add to a new quote
                var quoteDbContext = new DAL.Quote(database);
                var newQuote = quoteDbContext.CreateForJob(jobId,jobSiteId);
                if (newQuote.Id < 1)
                {
                    //An error has occured if we get here 
                    return false;
                }
                else
                {
                    //Update the parameter with the newly created quote Id
                    quoteId = newQuote.Id;
                }
            }

            bool didComplete = true;
            int insertedId = -1;

            var itemDbContext = new DAL.QuoteItem(database);

            //The quoteId parameter should now have the correct Id for the items
            foreach (Types.QuoteItem qItm in items)
            {
                //Check if the quote item is a new item
                if (qItm.Id < 1)
                {
                    //Set the Id to -1 as it may be set to zero
                    qItm.Id = -1;
                    qItm.QuoteId = quoteId;
                    qItm.JobId = jobId;
                    qItm.JobSiteId = jobSiteId;
                }

                insertedId = itemDbContext.Save(qItm);
                if (insertedId < 1)
                    didComplete = false;
            }

            return didComplete;
        }

        public static bool SetQuoteStatus(string database, int quoteId, int status)
        {
            var hasUpdated = false;
            try
            {
                //ArbPro.Utility.Logger.Log.DebugFormat("{0}: Setting Quote {1} to status {2}", database, quoteId, status);
                var dataContext = new DAL.Quote(database);
                var updatedId = dataContext.UpdateStatus(quoteId, status);
                hasUpdated = updatedId > 0;

                //ArbPro.Utility.Logger.Log.DebugFormat("{0}: Setting Quote {1} completed with success of: {1}", database, quoteId, hasUpdated);

                #region Legacy Code
                //var DB = new DAL.Quote(database);
                //var Quote = DB.GetForId(quoteId);
                //Quote.Status = (Types.Enums.QuoteStatusEnum)status;
                //DB.Save(Quote);
                //ArbPro.Utility.Logger.Log.DebugFormat("{0}: Setting Quote {1} to status {2}",database,quoteId,status);
                //if (Quote.Status == Types.Enums.QuoteStatusEnum.Accepted)
                //{
                //    var QDB = new DAL.QuoteItem(database);
                //    var Items = QDB.GetAllForQuote(quoteId);

                //    var WDB = new DAL.Work(database);
                //    var WorkList = WDB.GetForQuote(Quote.Id);
                //    // Create the work record
                //    ArbPro.Utility.Logger.Log.DebugFormat("{0}: Setting Quote {1} to status {2}", database, quoteId, status);
                //    Types.Work Work = null;
                //    if (WorkList == null || WorkList.Count == 0)
                //    {
                //       // DGSAP-868 Removed unrequired database call and changed the object accessed to set the work record Job Id 
                //        Work = new Types.Work()
                //        {
                //            QuoteId = Quote.Id,
                //            ClientId = Quote.ClientId,
                //            ContactId = Quote.ContactId,
                //            JobSiteId = Quote.JobSiteId,
                //            Status = Types.Enums.WorkStatusEnum.Pending,
                //            JobId = Quote.JobId
                //        };
                //        Work.Id = WDB.Save(Work);
                //    }
                //    else
                //    {
                //        Work = WorkList[0];
                //    }

                //    var WIDB = new DAL.WorkItem(database);

                //    foreach (var Item in Items)
                //    {
                //        var WorkItem = new Types.WorkItem();
                //        WorkItem.WorkId = Work.Id;
                //        WorkItem.JobId = Item.JobId;
                //        WorkItem.JobItemId = Item.JobItemId;
                //        WorkItem.JobSiteId = Item.JobSiteId;
                //        WorkItem.QuoteItemId = Item.Id;
                //        WorkItem.Code = Item.Code;
                //        WorkItem.Name = Item.Name;
                //        WorkItem.Description = Item.Description;
                //        WorkItem.Value = Item.Value;
                //        WorkItem.VatRate = Item.VatRate;
                //        WorkItem.Status = Types.Enums.ItemStatusEnum.Scheduled;


                //        if (!WIDB.GetAllForJobSite(Item.JobSiteId).Exists(i => i.QuoteItemId == Item.Id))
                //            WIDB.Save(WorkItem);
                //    }
                //}

                //return true;
                #endregion
            }
            catch(Exception ex)
            {
                DAL.ErrorLogger.LogToDb(database, "BL.SetQuoteStatus", string.Format("quoteId: {0}, status: {1}", quoteId, status), ex);
                hasUpdated = false;
            }
            return hasUpdated;
        }
    }
}
