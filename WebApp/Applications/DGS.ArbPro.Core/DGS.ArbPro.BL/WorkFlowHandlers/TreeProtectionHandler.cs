﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL
{
    public class TreeProtectionHandler
    {
        #region TPO Search result Methods

        #endregion



        public static int SaveTpo(string databaseName, Types.TreeProtectionOrder tpo)
        {
            DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(databaseName);
            return dbContext.Save(tpo);
        }

        public static Types.TreeProtectionOrder GetTpoForId(string databaseName, int id)
        {
            DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(databaseName);
            return dbContext.GetForId(id);
        }

        public static Types.TreeProtectionOrder GetTpoForQuoteId(string databaseName, int quoteId)
        {
            DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(databaseName);
            //TODO: Check if the TPO is populated, check if there should be a tpo Create a new TPO if required and set the Quote number
            Types.TreeProtectionOrder retVal = dbContext.GetForQuote(quoteId);
            if (retVal.Id < 1)
            {
                //There is no TPO in the DB for this quote number.
                //Assosiate this new object with the Quote
                retVal.QuoteId = quoteId;
            }

            return retVal;
        }

        public static Types.TreeProtectionOrder GetTpoForQuoteIdAndJobSiteId(string databaseName, int quoteId, int jobSiteId)
        {
            DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(databaseName);
            //TODO: Check if the TPO is populated, check if there should be a tpo Create a new TPO if required and set the Quote number
            Types.TreeProtectionOrder retVal = dbContext.GetForQuoteAndJobSite(quoteId, jobSiteId);
            if (retVal.Id < 1)
            {
                //There is no TPO in the DB for this quote number.
                //Assosiate this new object with the Quote
                retVal.QuoteId = quoteId;
                retVal.JobSiteId = jobSiteId;
            }

            return retVal;
        }

        /// <summary>
        /// Get all TPOs for a given quote ID.
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="quoteId"></param>
        /// <returns>
        /// List of TPO objects.
        /// </returns>
        public static List<Types.TreeProtectionOrder> GetTposForQuoteId(string databaseName, int quoteId)
        {
            DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(databaseName);
            return dbContext.GetAllForQuote(quoteId);
        }

        public static List<Types.TreeProtectionOrder> GetTpoForStatus(string databaseName, Types.Enums.TreeProtectionStatusEnum status)
        {
            DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(databaseName);
            return dbContext.GetForStatus(status);
        }

        public static List<Types.TreeProtectionOrder> GetTpoForStatusNot(string databaseName, Types.Enums.TreeProtectionStatusEnum status)
        {
            DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(databaseName);
            return dbContext.GetForStatusNot(status);
        }

        public static List<Types.TreeProtectionOrder> GetTpoForProgressStage(string databaseName, Types.Enums.TreeProtectionProgreesEnum stage)
        {
            DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(databaseName);
            return dbContext.GetForProcessStage(stage);
        }

        public static List<Types.TreeProtectionOrder> GetTpoForProgressStageBelow(string databaseName, Types.Enums.TreeProtectionProgreesEnum stage)
        {
            DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(databaseName);
            return dbContext.GetForProcessStageBelow(stage);
        }

        public static List<Types.TreeProtectionOrder> GetTpoForProgressStageAbove(string databaseName, Types.Enums.TreeProtectionProgreesEnum stage)
        {
            DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(databaseName);
            return dbContext.GetForProcessStageAbove(stage);
        }

        public static List<Types.TreeProtectionOrder> GetTpoForRefernceLike(string databaseName, Types.Enums.TpoReferenceTypeEnum refType, string refValue)
        {
            DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(databaseName);
            return dbContext.GetForReferenceLike(refType, refValue);
        }
    }
}
