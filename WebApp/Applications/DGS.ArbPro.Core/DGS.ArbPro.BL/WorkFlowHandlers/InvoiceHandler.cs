﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL
{
    public class InvoiceHandler
    {
        public static Types.Invoice GetInvoice(string databaseName, int invoiceId)
        {
            if (invoiceId < 1)
            {
                return new Types.Invoice();
            }
            else
            {
                DAL.Invoice dbContext = new DAL.Invoice(databaseName);
                return dbContext.GetForId(invoiceId);
            }
        }

        public static Types.InvoiceSummary GetSummaryForQuote(string databaseName, int quoteId)
        {
            DAL.InvoiceSummary dbContext = new DAL.InvoiceSummary(databaseName);
            return dbContext.GetForQuote(quoteId);
        }

        public static Types.Invoice GetCurrentInvoice(string databaseName, int quoteId)
        {
            DAL.Invoice dbContext = new DAL.Invoice(databaseName);
            return dbContext.GetForQuoteAndStatus(quoteId, Types.Enums.InvoiceStatusEnum.Pending);
        }
        public static Types.Invoice GetCurrentInvoiceForPaid(string databaseName, int invoiceId)
        {
            DAL.Invoice dbContext = new DAL.Invoice(databaseName);
            return dbContext.GetForQuoteAndStatusForInvoice(invoiceId);
        }

        public static List<Types.InvoiceItem> GetInvoicableItems(string databaseName, int quoteId)
        {
            DAL.InvoiceItem dbContext = new DAL.InvoiceItem(databaseName);
            return dbContext.GetAllInvoicableForQuote(quoteId);
        }

        public static List<Types.InvoiceHistoryItem> GetInvoiceHistoryForQuote(string database, int quoteId, bool includeDocuments)
        {
            DAL.InvoiceHistoryItem dbContext = new DAL.InvoiceHistoryItem(database);
             List<Types.InvoiceHistoryItem> retVal =  dbContext.GetDocumentsForQuote(quoteId);
             if (!includeDocuments)
             {
                 //Set all of the document stream 
                 foreach (Types.InvoiceHistoryItem doc in retVal)
                 {
                     doc.Doc = null;
                 }
             }
             return retVal;
        }


        public static bool SetStatus(string database, int invoiceId, Types.Enums.InvoiceStatusEnum status)
        {
            DAL.Invoice dbContext = new DAL.Invoice(database);
            int retVal = dbContext.SetStatus(invoiceId, status);
            //  Check if the status is cancelled - this requires the items to be released
            if (status == Types.Enums.InvoiceStatusEnum.Cancelled && retVal > 0)
            {
                var itmContext = new DAL.InvoiceItem(database);
                retVal = itmContext.CancelAllForInvoice(invoiceId);

            }
            return retVal > 0;
        }

        public static List<Types.Invoice> GetSentInvoicesForQuote(string database, int quoteId)
        {
            DAL.Invoice dbContext = new DAL.Invoice(database);
            List<Types.Invoice> retVal = dbContext.GetSentForQuote(quoteId);
            foreach (Types.Invoice i in retVal)
            {
                i.History.Clear();
            }
            return retVal;
        }

        public static int SaveInvoice(string database, Types.Invoice obj, List<Types.InvoiceItem> selectedItems, List<Types.InvoiceItem> deletedItems)
        {
            if (selectedItems != null && selectedItems.Count > 0)
            {
                foreach (Types.InvoiceItem itm in selectedItems)
                {
                    obj.InvoicedItems.Add(itm);
                }
            }
            DAL.InvoiceItem itmContext = new DAL.InvoiceItem(database);
            bool failed = false;
            if (deletedItems != null && deletedItems.Count > 0)
            {
                foreach (Types.InvoiceItem d in deletedItems)
                {
                    if (d.Id > 0)
                    {
                        var id = itmContext.DeleteItem(d.Id);
                        if (id < 1)
                        {
                            failed = true;
                            break;
                        }
                    }
                }
            }
            if (failed)
            {
                return -1;
            }
            else
            {
                DAL.Invoice dbContext = new DAL.Invoice(database);
                return dbContext.SaveAll(obj);
            }
        }
        public static int SaveInvoicePaidDate(string database, Types.Invoice obj)
        {
            DAL.Invoice dbContext = new DAL.Invoice(database);
            return dbContext.SaveInvoicePaidDate(obj);
        }
        public static int SaveInvoiceSentDate(string database, Types.Invoice obj)
        {
            DAL.Invoice dbContext = new DAL.Invoice(database);
            return dbContext.SaveInvoiceSentDate(obj);
        }
        

        public static Types.InvoiceItem GetInvoiceItem(string database, int id)
        {
            DAL.InvoiceItem dbContext = new DAL.InvoiceItem(database);
            return dbContext.GetForId(id);
        }

        public static bool SaveInvoiceItem(string database, Types.InvoiceItem obj)
        {
            DAL.InvoiceItem dbContext = new DAL.InvoiceItem(database);
            var retVal = dbContext.Save(obj);
            return retVal > 0;
        }
    }
}
