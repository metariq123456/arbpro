﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.BL.WorkFlowHandlers
{
    public class WorkHandler
    {
        public static bool SetWorkItemsCompleted(string database, List<Types.WorkItem> itemsToComplete)
        {
            bool retVal = true;
            var dbContext = new DAL.WorkItem(database);
            foreach (Types.WorkItem itm in itemsToComplete)
            {
                itm.Status = Types.Enums.ItemStatusEnum.WorkCompleted;
                var recId = dbContext.Save(itm);
                retVal = retVal && recId > 0;
            }
            return retVal;
        }

        /// <summary>
        /// Static method to get or create a work item for a specific client organisation
        /// </summary>
        /// <param name="database">string: The database name to be accessed</param>
        /// <param name="itemId">int: The id of the workitem to be retreaved, -1 if a new item is to be created</param>
        /// <param name="jobsiteId">optional int: The job site Id for a new item</param>
        /// <returns>Returns an instantiated WorkItem object</returns>
        public static Types.WorkItem WorkItemGet(string database, int itemId, int jobsiteId = -1)
        {
            Types.WorkItem retVal = null;
            if (itemId > 0)
            {
                var dataContext = new DAL.WorkItem(database);
                retVal = dataContext.GetForId(itemId);
            }
            else
            {
                retVal = new WorkItem();
                retVal.JobSiteId = jobsiteId;
            }

            return retVal;
        }

        /// <summary>
        /// Static method to save a work item to a specified database
        /// </summary>
        /// <param name="database">string: The database name to be accessed</param>
        /// <param name="item">WorkItem object that is to be persisted to the database</param>
        /// <returns>int: The Id of the inserted or updated row in the database</returns>
        public static int WorkItemSave(string database, Types.WorkItem item)
        {
            var dataContext = new DAL.WorkItem(database);
            return dataContext.Save(item);
        }
    }
}
