﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.BL
{
    public class CompanyHandler
    {
        public static List<Types.DocumentReference> GetDocumentReferences(string databaseName)
        {
            DAL.DocumentReference dbContext = new DAL.DocumentReference(databaseName);
            return dbContext.GetValues();
        }

        public static int SaveContact(string databaseName, Types.CompanyContact obj)
        {
            DAL.CompanyContact dbContext = new DAL.CompanyContact(databaseName);
            return dbContext.Save(obj);
        }
        //public static int SaveDocuments(string databaseName, Types.CompanyDocumentsObject obj)
        //{
        //    DAL.CompanyDocuments dbContext = new DAL.CompanyDocuments(databaseName);
        //    return dbContext.DocumentSave(obj);
        //}

        public static List<Types.CompanyContact> GetCompanyContacts(string databaseName)
        {
            DAL.CompanyContact dbContext = new DAL.CompanyContact(databaseName);
            return dbContext.GetList();
        }

        public static Types.CompanyContact GetContact(string databaseName, int id)
        {
            if (id < 1)
            {
                return new Types.CompanyContact();
            }
            else
            {
                DAL.CompanyContact dbContext = new DAL.CompanyContact(databaseName);
                return dbContext.GetForId(id);
            }
        }

        public static bool UpdateQuoteReferences(string databaseName, Types.DocumentReference obj)
        {
            DAL.DocumentReference dbContext = new DAL.DocumentReference(databaseName);
            return dbContext.SetNextQuoteNumber(obj);
        }

        public static bool UpdateInvoiceReferences(string databaseName, Types.DocumentReference obj)
        {
            DAL.DocumentReference dbContext = new DAL.DocumentReference(databaseName);
            return dbContext.SetNextInvoiceNumber(obj);
        }

        public static bool UpdateCreditRReferences(string databaseName, Types.DocumentReference obj)
        {
            DAL.DocumentReference dbContext = new DAL.DocumentReference(databaseName);
            return dbContext.SetNextCreditNoteNumber(obj);
        }

        public static List<Types.Staff> ListAllStaff(String DatabaseName)
        {
            var staff = new DAL.Staff(DatabaseName);
            return staff.GetAll();
        }
        public static List<Types.CompanyDocumentsObject> ListAllDocuments(String DatabaseName)
        {
            var document = new DAL.CompanyDocuments(DatabaseName);
            return document.GetDocumentsAll();
        }

        public static List<Types.CompanyDocumentsObject> SetDefaultAttachmentForCompanyDoument(String DatabaseName,int id)
        {
            var document = new DAL.CompanyDocuments(DatabaseName);
            return document.GetDocumentsAll();
        }
        public static Types.Staff GetStaffForID(String DatabaseName, int StaffID)
        {
            var staffMember = new DAL.Staff(DatabaseName);
            return staffMember.GetForId(StaffID);
        }
        public static Types.CompanyDocumentsObject GetDocumentForID(String DatabaseName, int DocumentID)
        {
            var document_file = new DAL.CompanyDocuments(DatabaseName);
            return document_file.GetForId(DocumentID);
        }

        /// <summary>
        /// Method to update a staff member and an associated user.
        /// If the IsMobileUser flag is set and a password has been set the method will insert or update the
        /// Master database with a user. If the flag is set to false and a MobileUserId is present in the object
        /// The method will update the MasterDatabase to disable mobile login for the user
        /// </summary>
        /// <param name="DatabaseName">The name of the client database to use</param>
        /// <param name="staff">The staff object to update or insert into the database</param>
        /// <param name="companyId">The Id of the company that the logged in user belongs to</param>
        /// <returns>Int: The Id of the inserted / updated staff record, -1 if an error in updated the staff record or -9999 if an error updating the user record</returns>
        public static int SaveStaffMember(String DatabaseName, Types.Staff staff, int companyId)
        {
            int savedMember = -1;
            var saveStaff = new DAL.Staff(DatabaseName);
            
            savedMember = saveStaff.Save(staff);
            staff.Id = savedMember;

            try
            {
                if (savedMember > 0 && staff.IsMobileUser)
                {
                    //The user is a mobile user
                    int mobileUserId = staff.MobileUserId;
                    //NOTE: An empty string is passed through to force the DAL to use the master database
                    DAL.MobileUser userContext = new DAL.MobileUser("");
                    int updatedId = userContext.Save(staff.ToMobileUser(companyId));
                    if (mobileUserId < 1 && updatedId > 0 && staff.Password.Length > 0)
                    {
                        //The user is a new mobile user so the User Id needs to be inserted into the staff record
                        staff.MobileUserId = updatedId;
                        savedMember = saveStaff.Save(staff);
                    }
                    else if (updatedId < 0)
                    {
                        //An error occured when updating the user record
                        savedMember = -9999;
                    }
                }
                else if (savedMember > 0 && !staff.IsMobileUser && staff.MobileUserId > 0)
                {
                    //The mobile user account has been disabled by the user, update the record
                    //NOTE: An empty string is passed through to force the DAL to use the master database
                    DAL.MobileUser userContext = new DAL.MobileUser("");
                    int updatedId = userContext.Save(staff.ToMobileUser(companyId));
                    if (updatedId < 0)
                    {
                        //An error occured when updating the user record
                        savedMember = -9999;
                    }
                }
            }
            catch (Exception ex)
            {
                //Any error that has been thrown will be due to the update or creation of the user object.
                return -9998;
            }
            return savedMember;
        }
        public static int SaveDocumentsHandler(String DatabaseName, Types.CompanyDocumentsObject obj, int companyId)
        {
            int savedDocument = -1;
            var saveImages = new DAL.CompanyDocuments(DatabaseName);
            savedDocument = saveImages.Save(obj);
            obj.Id = savedDocument;
            return savedDocument;
        }

        public static Types.Company GetCompany(string databaseName)
        {
            var Company = new DAL.Company(databaseName);
            return Company.Get(false);
        }

        public static bool SaveCompany(string DatabaseName, Types.Company CompanyObj)
        {
            var Company = new DAL.Company(DatabaseName);
            return Company.Save(CompanyObj);
        }

        public static bool SaveEmailFooterImage(string databaseName, byte[] imageBytes)
        {
            DAL.Company dbContext = new DAL.Company(databaseName);
            return dbContext.SaveEmailFooter(1, Types.Enums.EmailItemTypeEnum.Image, imageBytes);
        }

        public static bool SaveDirectorsSignature(string databaseName, byte[] imageBytes)
        {
            DAL.Company dbContext = new DAL.Company(databaseName);
            return dbContext.SaveDirectorsSignature(1, imageBytes);
        }

        public static int ToggleDefaultAttachmentForCompany(string databaseName, int DocId, bool DefaultAttachment)
        {
            DAL.Company dbContext = new DAL.Company(databaseName);
            return dbContext.ToggleDefaultAttachmentForCompany(DocId,DefaultAttachment);
        }
        public static List<Types.CompanyDocumentsObject> GetDefaultAttachmentForCompany(string databaseName)
        {
            DAL.Company dbContext = new DAL.Company(databaseName);
            return dbContext.GetDefaultAttachmentForCompany();
        }
        /// <summary>
        /// Method to clear the email footer from the company table (sets it to null)
        /// </summary>
        /// <param name="databaseName">the client database</param>
        /// <returns> true/false</returns>
        public static bool ClearEmailFooterImage(string databaseName)
        {
            DAL.Company dbContext = new DAL.Company(databaseName);
            return dbContext.ClearCompanyImage(1, 1);
        }

        /// <summary>
        /// Method to clear the director's signature from the company table (sets it to null)
        /// </summary>
        /// <param name="databaseName">the client database</param>
        /// <returns> true/false</returns>
        public static bool ClearDirectorsSignature(string databaseName)
        {
            DAL.Company dbContext = new DAL.Company(databaseName);
            return dbContext.ClearCompanyImage(1, 2);
        }

        #region Complaints
        public static List<Types.ComplaintSearchResult> SearchComplaintsForStatus(string databaseName, bool closed)
        {
            DAL.ComplaintSearchResult dbContext = new DAL.ComplaintSearchResult(databaseName);
            return dbContext.GetForType(closed);
        }

        public static Types.Complaint GetComplaint(string databaseName, int id)
        {
            //Check if the calling method is requesting a new object;
            if (id < 1)
            {
                return new Types.Complaint();
            }
            else
            {
                DAL.Complaint dbContext = new DAL.Complaint(databaseName);
                return dbContext.GetForId(id);
            }
        }

        public static int SaveComplaint(string databaseName, Types.Complaint obj)
        {
            DAL.Complaint dbContext = new DAL.Complaint(databaseName);
            return dbContext.Save(obj);
        }
        #endregion

        
    }
}
