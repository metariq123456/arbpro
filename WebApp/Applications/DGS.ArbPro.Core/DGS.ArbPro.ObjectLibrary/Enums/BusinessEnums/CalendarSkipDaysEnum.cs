﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum CalendarSkipDaysEnum
    {
        None = 0,
        Saturday = 1,
        Sunday = 2,
        Both = 3,
        Other = 4
    }

    public enum RepeatType
    {
        Daily = 1,
        Monthly = 2,
        Weekly = 3
    }
    public enum WeekDays
    {
        Monday = 1,
        Tuesday = 2,
        Wednesday = 3,
        Thursday = 4,
        Friday = 5,
        Saturday = 6,
        Sunday = 7
    }
}
