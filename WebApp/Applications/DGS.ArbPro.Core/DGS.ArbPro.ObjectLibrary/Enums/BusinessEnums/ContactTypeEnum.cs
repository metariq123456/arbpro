﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum ContactTypeEnum
    {
        Default,
        General,
        AlternateInvoice,
        Supplier,
        Contractor
    }
}
