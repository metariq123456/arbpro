﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum NoteTypeEnum
    {
        Quote = 0,
        Work = 1,
        Invoice = 2,
        Credit = 3
    }
}
