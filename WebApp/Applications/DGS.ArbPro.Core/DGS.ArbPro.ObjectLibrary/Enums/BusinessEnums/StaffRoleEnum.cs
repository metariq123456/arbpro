﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum StaffRoleEnum
    {
        None = 0,
	    General = 1,
	    TeamMember = 2,
	    Driver = 3,
	    TeamLeader = 4,
	    Surveyor = 5,
	    Director = 10
    }
}
