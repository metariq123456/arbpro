﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum EmailItemTypeEnum
    {
        None = 0,
		Image = 1,
		Text = 2
    }
}
