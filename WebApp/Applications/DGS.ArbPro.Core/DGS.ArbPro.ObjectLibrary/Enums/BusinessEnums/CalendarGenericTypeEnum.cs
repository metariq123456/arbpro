﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum CalendarGenericType
    {
        BankHoliday,
        CompanyHoliday,
        StaffHoliday,
        Event,
        StaffMeeting,
        Meeting,
        Interview,
        Other
    }
}
