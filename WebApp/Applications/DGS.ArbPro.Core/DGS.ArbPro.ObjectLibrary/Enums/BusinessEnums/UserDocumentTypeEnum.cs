﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum UserDocumentTypeEnum
    {
        Global,
        Global_NonEmail,
        Client,
        Client_NonEmail,
        Temporary
    }
}
