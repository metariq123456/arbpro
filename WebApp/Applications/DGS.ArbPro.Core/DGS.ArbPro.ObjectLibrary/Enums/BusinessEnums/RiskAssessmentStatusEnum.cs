﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum RiskAssessmentStatusEnum
    {
        Pending = 0,
        Referral_Required = 1,
        Referral_Completed = 2,
        Completed = 3
    }
}
