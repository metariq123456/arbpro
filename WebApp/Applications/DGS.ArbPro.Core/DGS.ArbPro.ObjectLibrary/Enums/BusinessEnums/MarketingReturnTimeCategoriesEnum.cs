﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MarketingReturnTimeCategoriesEnum.cs" company="Microsec Ltd">
//   2016 Copyright Microsec Ltd
// </copyright>
// <summary>
//   The marketing return time categories enum.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.Types.Enums
{
    /// <summary>
    /// The marketing return time categories <see langword="enum"/>.
    /// </summary>
    public enum MarketingReturnTimeCategoriesEnum
    {
        OneMonth = 1,
        TwoMonths = 2,
        ThreeMonths = 3,
        FourMonths = 4,
        FiveMonths = 5,
        SixMonths = 6,
        SevenMonths = 7,
        EightMonths = 8,
        NineMonths = 9,
        TenMonths = 10,
        ElevenMonths = 11,
        TwelveMonths = 12,
        EighteenMonths = 18,
        TwentyFourMonths = 24,
        ThirtyMonths = 30,
        ThirtuSixMonths = 36,
        FourtyTwoMonths = 42,
        FourtyEightMonths = 48,
        FiftyFourMonths = 54,
        SixtyMonths = 60,
        NotApplicable = 99
    }
}
