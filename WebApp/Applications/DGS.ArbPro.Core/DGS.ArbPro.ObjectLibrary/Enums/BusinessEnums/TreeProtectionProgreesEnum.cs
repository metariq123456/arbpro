﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum TreeProtectionProgreesEnum
    {
        No_Application = 0, // = No Application Made
        Application_Made = 1, // = Application Made
        Consent_Granted = 2, // = Consent Granted
        Application_Refused = 3, // = Application Refused
        Appealed = 4, // = Appealed
        Appeal_Rejected = 5, // = Appeal Refused
        Appeal_Granted = 6 // = Appeal Succeded
    }

    // DGSAP-726 Dashboard Tree Protection Orders Modifications
    public enum TreeProtectionOrderTypeEnum
    {
        TPO = 0,
        Conservation_Area = 1
    }
}
