﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum TpoReferenceTypeEnum
    {
        Internal = 0,
        LocalAuthority = 1,
        Appeal = 2,
        Quote = 3
    }
}
