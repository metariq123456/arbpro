﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum JobStatusEnum
    {
        Pending = 0,
        Quoted = 1,
        Scheduled = 2,
        WorkCompleted = 3,
        Invoiced = 4,
        Paid = 5,
        Credited = 6,
        Closed = 98,
        Canceled = 99,
    }
}
