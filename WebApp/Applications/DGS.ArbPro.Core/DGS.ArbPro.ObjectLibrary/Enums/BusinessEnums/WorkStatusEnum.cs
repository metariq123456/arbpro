﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum WorkStatusEnum
    {
        /*
        Status -
        0 - Pending
        1 - Scheduled
        2 - Inprogress
        3 - Completed
        */
        Pending = 0,
        Scheduled = 1,
        InProgress = 2,
        Completed = 3,
        Cancelled = 99
    }
}
