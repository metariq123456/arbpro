﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum ContactMethodEnum
    {
        None = 0,
        Telephone = 1,
        Mobile = 2,
        Email = 3,
        Letter = 4
    }
}
