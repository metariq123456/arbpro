﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum TreeProtectionStatusEnum
    {
        Unknown = 0,
        None = 1,
        Protected = 2
    }
}
