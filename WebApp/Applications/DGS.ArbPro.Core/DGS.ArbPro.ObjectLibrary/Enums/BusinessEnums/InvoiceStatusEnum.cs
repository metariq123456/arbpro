﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum InvoiceStatusEnum
    {
        Pending = 0,
        Sent = 1,
        Paid = 2,
        Overdue = 3,
        FurtherAction = 4,
        Credited = 5,
        Cancelled = 99
    }
}
