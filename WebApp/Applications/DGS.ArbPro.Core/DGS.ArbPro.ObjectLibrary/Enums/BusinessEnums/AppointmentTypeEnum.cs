﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    /// <summary>
    /// Enum to represent the type of an appointment of an instance
    /// </summary>
    public enum AppointmentTypeEnum
    {
        Generic = 0,
        Company = 1,
        Sales = 2,
        Work = 3
    }
}
