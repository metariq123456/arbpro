﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum QuoteStatusEnum
    {
        Pending = 0,
        Sent = 1,
        Cancelled = 2,
        Accepted = 3,
        Rejected = 4
    }
}
