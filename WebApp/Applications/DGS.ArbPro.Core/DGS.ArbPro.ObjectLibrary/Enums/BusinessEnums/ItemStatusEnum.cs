﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum ItemStatusEnum
    {
        None = 0,
        Quoted = 1,
        Scheduled = 2,
        WorkCompleted = 3,
        Invoiced = 4,
        Credited = 5,
        Cancelled = 99
    }
}
