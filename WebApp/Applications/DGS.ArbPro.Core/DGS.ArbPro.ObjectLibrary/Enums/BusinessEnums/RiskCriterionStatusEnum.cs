﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum RiskCriterionStatusEnum
    {
        Unknown = 0,
        No = 1,
        Yes = 2
    }
}
