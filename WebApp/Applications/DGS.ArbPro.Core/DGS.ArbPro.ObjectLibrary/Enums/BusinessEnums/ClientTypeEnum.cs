﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum ClientTypeEnum
    {
        Residential = 0,
        Commercial = 1,
        Other = 10
    }
}
