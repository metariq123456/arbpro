﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum PickItemTypeEnum
    {
        Title = 0,
        Town = 1,
		County = 2,
		Council = 3,
		WasteTypes = 4,
        StaffType = 5,
        LeadSource = 6,
        Hospital = 7,
        ComplaintType = 8,
        PermitType = 9
    }
}
