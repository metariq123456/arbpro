﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum StaffTypeEnum
    {
        Director = 0,
        Surveyour = 1,
        Driver = 2,
        Other = 10
    }
}
