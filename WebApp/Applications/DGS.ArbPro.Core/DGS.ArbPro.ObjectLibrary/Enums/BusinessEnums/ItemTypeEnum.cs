﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum ItemTypeEnum
    {
        JobItem = 0,
        QuoteItem = 1,
        WorkItem = 2,
        InvoiceItem = 3,
        CreditItem = 8
    }
}
