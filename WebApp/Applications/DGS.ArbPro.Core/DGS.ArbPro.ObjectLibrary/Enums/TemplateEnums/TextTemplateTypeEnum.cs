﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum TextTemplateTypeEnum
    {
        Quote = 0,
        Invoice = 1,
        Credit = 2,
        Marketing =3,
        Return =4,
    }
}
