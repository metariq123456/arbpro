﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum RiskCreateFromObjectType
    {
        NotSpecified = -1,
        JobSite = 0,
        Quote = 1,
        Work = 2,
        WorkPlan =3,
    }
}
