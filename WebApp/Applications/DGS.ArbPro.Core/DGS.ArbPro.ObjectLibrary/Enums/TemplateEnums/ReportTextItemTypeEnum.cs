﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    /// <summary>
    /// Enum to represent the type of item represented by the text template
    /// </summary>
    public enum ReportTextTemplateTypeEnum
    {
        Quote,
        Invoice,
        Marketing,
        Return
    }
}
