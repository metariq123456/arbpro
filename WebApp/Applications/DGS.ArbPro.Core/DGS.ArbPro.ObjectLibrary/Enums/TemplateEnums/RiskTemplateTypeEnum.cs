﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum RiskTemplateTypeEnum
    {
        Default = 0,
        General = 1,
        Specific = 2
    }
}
