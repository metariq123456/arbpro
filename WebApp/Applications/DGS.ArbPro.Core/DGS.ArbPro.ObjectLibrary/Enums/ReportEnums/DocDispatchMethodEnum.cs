﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum DocDispatchMethodEnum
    {
        None = 0,
        Letter = 1,
        Email = 2
    }
}
