﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    /// <summary>
    /// Enum used to determin the parent object for all work reports
    /// </summary>
    public enum WorkReportParentEnum
    {
        QuoteRecord = 0,
        WorkRecord = 1,
        ClientSite = 2,
        JobSite = 3,
        JobAndQuoteId = 4,
        WorkPlan=5
    }
}
