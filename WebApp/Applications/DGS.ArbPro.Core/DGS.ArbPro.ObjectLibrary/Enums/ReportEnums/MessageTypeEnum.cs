﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum MessageTypeEnum
    {
        Letter = 0,
        Email = 1,
        Marketing = 2,
        //V3.7.0
        Sms = 3
    }
}
