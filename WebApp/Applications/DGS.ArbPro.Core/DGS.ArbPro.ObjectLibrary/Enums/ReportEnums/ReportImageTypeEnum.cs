﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum ReportImageTypeEnum
    {
        PDFHeader = 0,
        PDFFooter = 1,
        PDFSignature = 2,
        EmailHeader = 3,
        EmailFooter = 4,
        EmailSignatur = 5
    }
}
