﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum DocContentTypeEnum
    {
        Schedule = 0,
        Letter = 1
    }

    //NOTE: For work settings  the schedule refers to the risk assessment and the letter the works oreder and schedule
    public enum DocGenerationEnum
    {
        Letter = 0,
        Schedule = 1,
        Both = 3,
        Example = 4
    }
}
