﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum InvoiceDocTypeEnum
    {
        Schedule,
        Letter,
        First_Reminder,
        Second_Reminder,
        Final_Reminder,
        Legal_Action,
        Legal_Action_2,
        Letter_And_Schedule
    }
}
