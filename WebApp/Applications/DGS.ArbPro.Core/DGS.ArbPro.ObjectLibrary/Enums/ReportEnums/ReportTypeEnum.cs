﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    public enum ReportTypeEnum
    {
        Quote = 0,
        Invoice = 1,
        CreditNote = 2,
        RiskAssessment = 3,
        WorkProgramming = 4,
        WorkSchedule = 5,
        MarketingMerge = 6,
        TpoWorkSchedule = 7,
        Marketing = 8
    }
}
