﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    /// <summary>
    /// Enum to represent the type of template an instance represents
    /// </summary>
    public enum ReportTextTemplateMessageEnum
    {
        Letter = 0,
        Email = 1,
        Other = 2
    }
}
