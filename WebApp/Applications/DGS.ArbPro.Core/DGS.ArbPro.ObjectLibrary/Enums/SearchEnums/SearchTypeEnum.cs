﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums
{
    /// <summary>
    /// Enum that represents the type of search result an instance represents
    /// </summary>
    public enum SearchTypeEnum
    {
        Client = 0,
        Quote = 1,
        Invoice = 2,
        Appointment = 3,
        Contact = 4,
        Work = 5,
        Job = 6,
        Risk = 7,
        RiskTemplate = 8,
        Complaint = 9,
        TPO = 10,
        EmailHistory = 11,
        Document = 12,
        Note = 13,
        Map = 14,
        Finance = 15
    }
}
