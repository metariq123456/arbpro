﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Enums.SageEnums
{
    public enum SageTypeEnum
    {
        SAGE13 = 0,
        SAGE09 = 1,
        QBKS = 4,
        KASHFLOW = 3,
        XERO = 2,
        CSV = 5
    }

    public enum ExportTypeEnum
    {
        CUSTOMER = 0,
        INVOICE = 1,
        RECEIPT = 2,    //DGSAP-549 Modification to enable export of invoice receipts
        CREDIT_NOTE = 3
    }
}
