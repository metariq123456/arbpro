using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class DatabaseUser : BusinessBase
    {
        #region Members
        public User UserInfo;
        public ClientOrganisation ClientOrg;
        #endregion
        #region Constructors
        public DatabaseUser() : base() 
        {
            UserInfo = new User();
            ClientOrg = new ClientOrganisation();
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
