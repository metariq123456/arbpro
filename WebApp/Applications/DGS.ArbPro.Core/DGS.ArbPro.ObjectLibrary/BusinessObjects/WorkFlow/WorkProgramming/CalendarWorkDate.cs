﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class CalendarWorkDate : BusinessBase
    {

        #region Members

        int _CalendarWorkId = -1;
        DateTime _BookDate = DateTime.MinValue;

        #endregion

        #region Constructors
        public CalendarWorkDate() : base() { }

        #endregion

        #region Properties

        public int CalendarWorkId
        {
            get { return _CalendarWorkId; }
            set { _CalendarWorkId = value; }
        }

        public DateTime BookDate
        {
            get { return _BookDate; }
            set { _BookDate = value; }
        }

        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }

}
