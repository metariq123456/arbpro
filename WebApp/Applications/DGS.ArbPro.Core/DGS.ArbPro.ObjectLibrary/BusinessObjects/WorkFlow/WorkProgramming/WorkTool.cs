﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class WorkTool : BusinessBase
    {
        #region Members
	    int _WorkPlanId = -1;
	    int _ToolId = -1;
	    string _Item = "";
	    int _Number = 0;
        #endregion

        #region Constructors
        public WorkTool() { }
        #endregion

        #region Properties
        public int WorkPlanId
        {
            get { return _WorkPlanId; }
            set { _WorkPlanId = value; }
        }

        public int ToolId
        {
            get { return _ToolId; }
            set { _ToolId = value; }
        }

        public string Item
        {
            get { return _Item; }
            set { _Item = value; }
        }

        public int Number
        {
            get { return _Number; }
            set { _Number = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
