﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class WorkSummary : BusinessBase
    {
        //NOTE: The Id is the Work record Id that this summary is linked to
        #region Members
        decimal _Value = 0.00M;
        decimal _VatValue = 0.00M;
        int _QuoteId = -1;
        int _JobId = -1;
        string _QuoteNo = "Pending";
        string _Accountno = "";
        string _ClientName = ""; //Client Name
        string _StreetNumber = ""; //StreetNumber,
	    string _Address1 = ""; //AddressLn1,
	    string _Address2 = ""; //AddressLn2,
	    string _Town = ""; //Town,
	    string _County = ""; //County,
	    string _PostCode = ""; //PostCode,
	    string _Telephone = ""; //Telephone,
	    string _Mobile = ""; //Mobile,
	    string _Email = ""; //Email
        List<JobSite> _JobSites = null;
        List<WorkItem> _WorkItems = null;
        List<WorkPlan> _BookedWork = null;
        #endregion

        #region Constructors
        public WorkSummary() : base() { }
        #endregion

        #region Properties
        /// <summary>
        /// The Id of the quote the work record is linked to
        /// </summary>
        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }
        /// <summary>
        /// The Quote reference for the work
        /// </summary>
        public string QuoteNo
        {
            get { return _QuoteNo; }
            set { _QuoteNo = value; }
        }


        public int JobId
        {
            get { return _JobId; }
            set { _JobId = value; }
        }
        /// <summary>
        /// The Account No or Id for the client 
        /// </summary>
        public string AccountNo
        {
            get { return _Accountno; }
            set { _Accountno = value; }
        }
        /// <summary>
        /// The gross value of the work
        /// </summary>
        public decimal Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        /// <summary>
        /// The vat value of the work items
        /// </summary>
        public decimal VatVal
        {
            get { return _VatValue; }
            set { _VatValue = value; }
        }
        /// <summary>
        /// The name of the client
        /// </summary>
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }
        //The street number or house name of the client
        public string StreetNumber
        {
            get { return _StreetNumber; }
            set { _StreetNumber = value; }
        }
        /// <summary>
        /// The first address line of the clients main address
        /// </summary>
        public string AddressLn1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }
        /// <summary>
        /// The clients main address second line
        /// </summary>
        public string AddressLn2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }
        /// <summary>
        /// The town of the clients main address is located in
        /// </summary>
        public string Town
        {
            get { return _Town; }
            set { _Town = value; }
        }
        /// <summary>
        /// The county of the clients main address is located in
        /// </summary>
        public string County
        {
            get { return _County; }
            set { _County = value; }
        }
        /// <summary>
        /// The Post code of the clients main address is located in
        /// </summary>
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }
        /// <summary>
        /// The telephone number of the clients primary contact
        /// </summary>
        public string Telephone
        {
            get { return _Telephone; }
            set { _Telephone = value; }
        }
        /// <summary>
        /// The mobile telephone number of the clients primary contact
        /// </summary>
        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        /// <summary>
        /// The email address for the client
        /// </summary>
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        /// <summary>
        /// The list of job sites that are linked to the work items
        /// </summary>
        public List<JobSite> JobSites
        {
            get
            {
                if (_JobSites == null)
                    _JobSites = new List<JobSite>();

                return _JobSites;
            }
            set { _JobSites = value; }
        }
        /// <summary>
        /// The list of workitems for all sites
        /// </summary>
        public List<WorkItem> WorkItems
        {
            get
            {
                if (_WorkItems  == null)
                    _WorkItems = new List<WorkItem>();

                return _WorkItems;
            }
            set 
            { 
                _WorkItems = value;
                CalulateValues();
            }
        }
        /// <summary>
        /// List of any work plans that have already been created for the quote or work records
        /// </summary>
        public List<WorkPlan> BookedWork
        {
            get
            {
                if (_BookedWork == null)
                    _BookedWork = new List<WorkPlan>();

                return _BookedWork;
            }
            set { _BookedWork = value; }
        }

        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Method to calculate the value and vat value of all the work items
        /// </summary>
        public void CalulateValues()
        {
            _Value = 0.00M;
            _VatValue = 0.00M;
            foreach (WorkItem itm in this.WorkItems)
            {
                _Value += itm.Value;
                _VatValue += (itm.Value * (itm.VatRate / 100));
            }
        }
        #endregion
        #region Overrides
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion

    }
}
