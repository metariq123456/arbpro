﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class Team : BusinessBase
    {
        #region Members
        private int _Number = 0;
        private Color _Colour = Color.Snow;
        private string _Name = "";
        private int _LeaderId = -1;
        private int _DriverId = -1;
        private List<TeamStaff> _Personnel = null;
        #endregion

        #region Constructors
        public Team() { }
        #endregion

        #region Properties
        /// <summary>
        /// The number assigned o the team
        /// </summary>
        public int Number
        {
            get{return _Number;}
            set{_Number = value;}
        }

        /// <summary>
        /// The colour assigned to the team
        /// </summary>
        public Color Colour
        {
            get{return _Colour;}
            set{_Colour = value;}
        }

        public string RGBHexValue 
        {
            get 
            {
                if (_Colour.Name.Length > 3)
                    return _Colour.Name.Substring(2, _Colour.Name.Length - 2);
                else
                    return "000000";
            }
        }

        public String ColourName { get { return Colour.Name; } }
        /// <summary>
        /// The name assigned to the team
        /// </summary>
        public string Name
        {
            get{return _Name;}
            set{_Name = value;}
        }

        /// <summary>
        /// The Id of the Team Leaders Staff record
        /// </summary>
        public int LeaderId
        {
            get { return _LeaderId; }
            set { _LeaderId = value; }
        }

        /// <summary>
        /// The Id of the designated drivers Staff record
        /// </summary>
        public int DriverId
        {
            get { return _DriverId; }
            set { _DriverId = value; }
        }

        /// <summary>
        /// The staff assigned to the team including there role
        /// </summary>
        public List<TeamStaff> Personnel 
        {
            get 
            {
                if (_Personnel == null)
                    _Personnel = new List<TeamStaff>();

                return _Personnel; 
            }
            set { _Personnel = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
