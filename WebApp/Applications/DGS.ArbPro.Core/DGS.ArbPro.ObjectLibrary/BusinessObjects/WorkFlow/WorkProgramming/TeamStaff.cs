﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class TeamStaff : BusinessBase
    {
        #region Members
        int _ParentId = -1;
        int _StaffId = -1;

        string _StaffName = "";
        string _Task = "Not Set";

        bool _Deleted = false;
        #endregion

        #region Constructor
        public TeamStaff() { }
        #endregion

        #region Properties
        /// <summary>
        /// The Id of the parent record the staff is associated with
        /// This could be a team of a work plan id
        /// </summary>
        public int ParentId
        {
            get { return _ParentId; }
            set { _ParentId = value; }
        }

        /// <summary>
        /// The Id of the associated staff record
        /// </summary>
        public int StaffId
        {
            get { return _StaffId; }
            set { _StaffId = value; }
        }

        /// <summary>
        /// The name of the member os staff
        /// </summary>
        public string StaffName
        {
            get { return _StaffName; }
            set { _StaffName = value; }
        }

        /// <summary>
        /// The Role the member of staff is assigned within the current team.
        /// </summary>
        public string Task
        {
            get { return _Task; }
            set { _Task = value; }
        }

        public bool Deleted
        {
            get { return _Deleted; }
            set { _Deleted = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
