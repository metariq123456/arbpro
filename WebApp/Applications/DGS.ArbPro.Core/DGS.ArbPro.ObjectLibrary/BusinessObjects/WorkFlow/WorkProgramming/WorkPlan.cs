﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class WorkPlan :BusinessBase
    {
        #region Members
        int _JobSiteId = -1;
        int _TeamId = -1;
        int _QuoteId = -1;
        string _QuoteRef = "Pending";
        decimal _Value = 0.00M;
        int _WorkId = -1;
        int _RequiredStaff = 0;
        int _TeamLdrId = -1;
        int _TeamDrvrId = -1;
        string _TeamName = "";
        string _SiteName = "";
        List<TeamStaff> _Personnel = null;
        List<WorkTool> _Tools = null;
        List<WorkItem> _Items = null;
        DateTime? _StartDate = null;
        decimal _TimeRequired = 0;
        Boolean _IsRecurring = false;
        int _CalendarWorkId = -1;
        string _Comments = "";
        string _StaffName = "";
        Types.CalendarWork _Appointment = null;
        #endregion

        #region Constructors
        public WorkPlan() : base() { }
        #endregion

        #region Properties
        public int JobSiteId
        {
            get { return _JobSiteId; }
            set { _JobSiteId = value; }
        }

        public int TeamId
        {
            get { return _TeamId; }
            set { _TeamId = value; }
        }

        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }

        public int WorkId
        {
            get { return _WorkId; }
            set { _WorkId = value; }
        }

        public int StaffRequired
        {
            get { return _RequiredStaff; }
            set { _RequiredStaff = value; }
        }
        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        public string StaffName
        {
            get { return _StaffName; }
            set { _StaffName = value; }
        }
        //public int TeamLeaderId
        //{
        //    get { return _TeamLdrId; }
        //    set { _TeamLdrId = value; }
        //}

        //public int TeamDriverId
        //{
        //    get { return _TeamDrvrId; }
        //    set { _TeamDrvrId = value; }
        //}

        public string TeamName
        {
            get { return _TeamName; }
            set { _TeamName = value; }
        }

        public string SiteAddress
        {
            get { return _SiteName; }
            set { _SiteName = value; }
        }

        public string QuoteReference
        {
            get { return _QuoteRef; }
            set { _QuoteRef = value; }
        }

        public List<TeamStaff> Personnel
        {
            get 
            {
                if (_Personnel == null)
                    _Personnel = new List<TeamStaff>();

                return _Personnel; 
            }
            set { _Personnel = value; }
        }

        public List<WorkTool> Tools
        {
            get 
            { 
                if(_Tools == null)
                    _Tools = new List<WorkTool>();

                return _Tools;
            }
            set { _Tools = value; }
        }

        public Decimal QuoteValue
        {
            get { return ScheduledItems.Sum(wi => wi.Value); }
            set { _Value = value; }
        }

        public DateTime? StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }

        public List<WorkItem> ScheduledItems
        {
            get
            {
                if (_Items == null)
                    _Items = new List<WorkItem>();

                return _Items;
            }
            set { _Items = value; }
        }

        /// <summary>
        /// Gets or sets The estimated time for completion of the work plan
        /// </summary>
        public decimal TimeRequired
        {
            get { return _TimeRequired; }
            set { _TimeRequired = value; }
        }

        public Boolean IsRecurring
        {
            get { return _IsRecurring; }
            set { _IsRecurring = value; }
        }

        public int CalendarWorkId
        {
            get { return _CalendarWorkId; }
            set { _CalendarWorkId = value; }
        }
        public Types.CalendarWork Appointment
        {
            get { return _Appointment; }
            set { _Appointment = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
