﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class InvoiceSummary : BusinessBase
    {
        #region Members
        //  New members added to simplify UI modifications
        //int _JobId = -1;
        int _ClientId = -1;
        int _WorkId = -1;
        string _QuoteRef = "Pending";
        decimal _QuoteValue = 0.00M;
        decimal _QuoteVatValue = 0.00M;
        decimal _TotalWorkValue = 0.00M;
        decimal _TotalWorkVatValue = 0.00M;
        decimal _CompletedWorkValue = 0.00M;
        decimal _CompletedWorkVatValue = 0.00M;
        decimal _PendingValue = 0.00M;
        decimal _PendingVat = 0.00M;
        decimal _InvoicedValue = 0.00M;
        decimal _InvoicedVatValue = 0.00M;
        decimal _RecieptsValue = 0.00M;
        decimal _ReceiptVat = 0.00M;
        decimal _OverdueValue = 0.00M;
        decimal _OverdueVat = 0.00M;

        List<Types.Invoice> _Invoices = null;
        List<Types.WorkItem> _InvoicableItems = null;
        #endregion

        #region Constructors
        public InvoiceSummary() : base() { }
        #endregion

        #region Properties

        /// <summary>
        /// The Id of the client the invoice is for
        /// </summary>
        public int ClientId
        {
            get { return _ClientId; }
            set { _ClientId = value; }
        }

        /// <summary>
        /// The Quote Id for the invoice
        /// </summary>
        public int QuoteId
        {
            get { return base.Id; }
            set { base.Id = value; }
        }

        /// <summary>
        /// The Work Id for the Quote
        /// </summary>
        public int WorkId
        {
            get { return _WorkId; }
            set { _WorkId = value; }
        }

        public string QuoteNo
        {
            get { return _QuoteRef; }
            set { _QuoteRef = value; }
        }

        /// <summary>
        /// The total value of the Quote excluding VAT
        /// </summary>
        public decimal QuoteValue
        {
            get { return _QuoteValue; }
            set { _QuoteValue = value; }
        }
        /// <summary>
        /// The total VAT value for the Quote
        /// </summary>
        public decimal QuoteVatValue
        {
            get { return _QuoteVatValue; }
            set { _QuoteVatValue = value; }
        }
        /// <summary>
        /// The total value of all completed work excluding VAT
        /// </summary>
        public decimal TotalWorkValue
        {
            get { return _TotalWorkValue; }
            set { _TotalWorkValue = value; }
        }
        /// <summary>
        /// The total VAT value of all completed work
        /// </summary>
        public decimal TotalWorkVatValue
        {
            get { return _TotalWorkVatValue; }
            set { _TotalWorkVatValue = value; }
        }
        /// <summary>
        /// The total value of all completed work excluding VAT
        /// </summary>
        public decimal CompletedWorkValue
        {
            get { return _CompletedWorkValue; }
            set { _CompletedWorkValue = value; }
        }
        /// <summary>
        /// The total VAT value of all completed work
        /// </summary>
        public decimal CompletedWorkVatValue
        {
            get { return _CompletedWorkVatValue; }
            set { _CompletedWorkVatValue = value; }
        }
        /// <summary>
        /// The value of all invoices that have not been sent
        /// </summary>
        public decimal PendingValue
        {
            get { return _PendingValue; }
            set { _PendingValue = value; }
        }
        /// <summary>
        /// The VAT value of all invoices that have not been sent
        /// </summary>
        public decimal PendingVat
        {
            get { return _PendingVat; }
            set { _PendingVat = value; }
        }
        /// <summary>
        /// The total value of all invoices that have been dispatch (includ paid) excluding VAT
        /// </summary>
        public decimal InvoicedValue
        {
            get { return _InvoicedValue; }
            set { _InvoicedValue = value; }
        }
        /// <summary>
        /// The total VAT value for all invoices
        /// </summary>
        public decimal InvoicedVatValue
        {
            get { return _InvoicedVatValue; }
            set { _InvoicedVatValue = value; }
        }
        /// <summary>
        /// The total value of all received payments Excluding VAT
        /// </summary>
        public decimal InvoiceReceipts
        {
            get { return _RecieptsValue; }
            set { _RecieptsValue = value; }
        }
        /// <summary>
        /// The total VAT for all received payments
        /// </summary>
        public decimal InvoiceReceiptVat
        {
            get { return _ReceiptVat; }
            set { _ReceiptVat = value; }
        }

        public decimal OverdueValue
        {
            get { return _OverdueValue; }
            set { _OverdueValue = value; }
        }

        public decimal OverdueVat
        {
            get { return _OverdueVat; }
            set { _OverdueVat = value; }
        }

        /// <summary>
        /// The list of all invoices for the quote
        /// </summary>
        public List<Types.Invoice> Invoices
        {
            get
            {
                if (_Invoices == null)
                    _Invoices = new List<Types.Invoice>();

                return _Invoices;
            }
            set { _Invoices = value; }
        }

        /// <summary>
        /// List of all completed work items that have not yet been invoiced
        /// </summary>
        public List<Types.WorkItem> InvoicableItems
        {
            get
            {
                if (_InvoicableItems == null)
                    _InvoicableItems = new List<Types.WorkItem>();

                return _InvoicableItems;
            }
            set { _InvoicableItems = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
