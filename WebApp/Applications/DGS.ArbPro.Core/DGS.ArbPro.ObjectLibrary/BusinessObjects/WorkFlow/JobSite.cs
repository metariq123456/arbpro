﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class JobSite : WorkFlowBase
    {
        #region Members
        int _BookedWorkItemCount = -1;
        DateTime? _BookedWorkStartDate = null;
        int _JobId = -1;
        ClientSite _Site = null;
        List<JobItem> _SiteItems = null;
        List<QuoteSearchResult> _Quotes = null;
        Boolean _Disabled = false;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public JobSite() : base() { }

        /// <summary>
        /// Constructer to enable a JobSite to be created from a client site
        /// </summary>
        /// <param name="site">ClientSite</param>
        public JobSite(ClientSite site)
            : base()
        {
            _Site = site;
            ClientId = site.Id;
        }

        /// <summary>
        /// Constructor to enable a JobSite to be created from job and client Ids
        /// </summary>
        /// <param name="jobId">Int</param>
        /// <param name="clientId">Int</param>
        public JobSite(int jobId, int clientId)
            : base()
        {
            base.ClientId = clientId;
            _JobId = jobId;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Overriden property to prevent a Job site from becoming locked
        /// </summary>
        public override bool Locked
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// The number of booked work items against the Job for this Job Site.
        /// </summary>
        public int BookedWorkItemCount
        {
            get { return _BookedWorkItemCount; }
            set { _BookedWorkItemCount = value; }
        }

        /// <summary>
        /// The date of the earlier appointment booked against the job for this Job Site.
        /// </summary>
        public DateTime? BookedWorkStartDate
        {
            get { return _BookedWorkStartDate; }
            set { _BookedWorkStartDate = value; }
        }

        /// <summary>
        /// The Id for the Job this Job Site belongs to
        /// </summary>
        public int JobId
        {
            get { return _JobId; }
            set { _JobId = value; }
        }

        /// <summary>
        /// Site String for Listing
        /// </summary>
        public String SiteString
        {
            get
            {
                if (Site == null) return "";
                
                return Site.GetSiteAddress();
            }
            set { }
        }

        /// <summary>
        /// All items add to the Job for this site.
        /// Items add at Work stage will be displayed her as well
        /// </summary>
        public List<JobItem> SiteItems
        {
            get { return _SiteItems; }
            set { _SiteItems = value; }
        }

        /// <summary>
        /// Brief details of each quote for the Job Site
        /// </summary>
        public List<QuoteSearchResult> QuoteDetails
        {
            get { return _Quotes; }
            set { _Quotes = value; }
        }

        public Int32 SiteId
        {
            get
            {
                return Site.Id;
            }
        }

        public Boolean Disabled
        {
            get
            {
                return _Disabled;
            }
            set
            {
                _Disabled = value;
            }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
