﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class Quote : WorkFlowBase
    {
        #region Members
        int _JobSiteId = -1;
        int _AppointmentId = -1;
        int _StaffId = -1;
        string _StaffName = "";
        decimal _VAT = 0.00M;
        string _PurchaseOrderNumber;
        DateTime dateCreated = DateTime.MinValue;
        DateTime dateSent = DateTime.MinValue;
        List<JobItem> _JobItems = null;
        List<QuoteItem> _QuotedItems = null;
        List<QuoteHistoryItem> _History = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructer to be used to create a new Quote object
        /// with a status of Pending and a Locked flag set to false
        /// </summary>
        public Quote() : base() { }

        /// <summary>
        /// Optional constructor to populate the status enum
        /// </summary>
        /// <param name="status">int - The numeric value of the typed enum</param>
        /// <param name="locked">bool - Status to show if the Quote can be edited</param>
        public Quote(int status, bool locked)
            : base(status, locked) 
        {
        }

        /// <summary>
        /// Optional constructor to populate the status
        /// </summary>
        /// <param name="statusEnum">The QuoteStatusEnum value</param>
        /// <param name="locked">bool - Status to show if the Quote can be edited</param>
        public Quote(QuoteStatusEnum statusEnum, bool locked)
            : base((int)statusEnum, locked)
        {
        }
        #endregion

        #region Properties

        /// <summary>
        /// The Id of the parent Job record
        /// </summary>
        public int JobSiteId
        {
            get { return _JobSiteId; }
            set { _JobSiteId = value; }
        }

        /// <summary>
        /// The Id of the appointment record
        /// returns -1 if there is no appoinment
        /// </summary>
        public int AppointmentId
        {
            get { return _AppointmentId; }
            set { _AppointmentId = value; }
        }

        /// <summary>
        /// The VAT rate attributed to the Quote
        /// </summary>
        public decimal VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }

        /// <summary>
        /// The Staff Id for the assigned surveyor
        /// Will return -1 if not set
        /// </summary>
        public int SurveyorId
        {
            get { return _StaffId; }
            set { _StaffId = value; }
        }

        /// <summary>
        /// The name of the assigned surveyor
        /// Will return "" if not set
        /// </summary>
        public string SurveyorName
        {
            get { return _StaffName; }
            set { _StaffName = value; }
        }

        /// <summary>
        /// The current status of the Quote
        /// </summary>
        public QuoteStatusEnum Status
        {
            get { return (QuoteStatusEnum)base.Status; }
            set { base.Status = (int)value; }
        }

        /// <summary>
        /// All Site Job items that have not been attributed to the quote
        /// </summary>
        public List<JobItem> JobItems
        {
            get
            {
                if (_JobItems == null)
                    _JobItems = new List<JobItem>();

                return _JobItems; 
            }
            set { _JobItems = value; }
        }

        /// <summary>
        /// All items already attributed to the Quote
        /// </summary>
        public List<QuoteItem> QuotedItems
        {
            get 
            {
                if (_QuotedItems == null)
                    _QuotedItems = new List<QuoteItem>();

                return _QuotedItems; 
            }
            set { _QuotedItems = value; }
        }

        /// <summary>
        /// A List of all the document history for the Quote
        /// </summary>
        public List<QuoteHistoryItem> History
        {
            get 
            {
                if (_History == null)
                    _History = new List<QuoteHistoryItem>();

                return _History; 
            }
            set { _History = value; }
        }

        public DateTime DateCreated
        {
            get { return this.dateCreated; }
            set { this.dateCreated = value; }
        }

        public DateTime DateSent
        {
            get { return this.dateSent; }
            set { this.dateSent = value; }
        }

        /// <summary>
        /// The name of the assigned surveyor
        /// Will return "" if not set
        /// </summary>
        public string PurchaseOrderNumber
        {
            get { return _PurchaseOrderNumber; }
            set { _PurchaseOrderNumber = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
