﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class Invoice  : WorkFlowBase
    {
        #region Members
        int _JobId = -1;
        int _WorkId = -1;
        int _QuoteId = -1;
        decimal _VAT = 0.00M;
        string _HouseNameNo = "";
        string _Address1 = "";
        string _Address2 = "";
        string _Town = "";
        string _County = "";
        string _PostCode = "";
        decimal _Value = 0.00M;
        DateTime _SentDate = DateTime.MinValue;
        DateTime _DueDate = DateTime.MinValue;
        DateTime _PaidDate = DateTime.MinValue;
        DateTime dateCreated = DateTime.MinValue;
        //Calculated values
        decimal _VatValue = 0.00M;
        decimal _GrossValue = 0.00M;

        //Cllections
        List<InvoiceItem> _InvoicedItems = null;
        List<InvoiceHistoryItem> _History = null;
        #endregion

        #region Constructors
        public Invoice() : base() { }
        #endregion

        #region Properties
        /// <summary>
        /// The Job Id for the Invoice
        /// </summary>
        public int JobId
        {
            get { return _JobId; }
            set { _JobId = value; }
        }

        /// <summary>
        /// The Work Id for the Invoice
        /// </summary>
        public int WorkId
        {
            get { return _WorkId; }
            set { _WorkId = value; }
        }

        /// <summary>
        /// The QuoteId for the Invoice
        /// </summary>
        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the ClientContact may change
        /// </summary>
        public string HouseNameNo
        {
            get { return _HouseNameNo; }
            set { _HouseNameNo = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the ClientContact may change
        /// </summary>
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the ClientContact may change
        /// </summary>
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the ClientContact may change
        /// </summary>
        public string Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the ClientContact may change
        /// </summary>
        public string County
        {
            get { return _County; }
            set { _County = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the ClientContact may change
        /// </summary>
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        /// <summary>
        /// The total value of all invoice items
        /// Does not include VAT
        /// </summary>
        public decimal Value
        {
            get { return _Value; }
            set 
            { 
                _Value = value;
                CalculateMemberValues();
            }
        }

        /// <summary>
        /// The VAT value to be attributed to the Invoice value
        /// </summary>
        public decimal VAT
        {
            get { return _VAT; }
            set
            {
                _VAT = value;
                CalculateItemValues();
            }
        }

        /// <summary>
        /// The total amount of VAT due on the invoice
        /// Accessor method only
        /// </summary>
        public decimal VatValue
        {
            get { return _VatValue; }
        }

        /// <summary>
        /// The toatl value, including VAT due on the invoice
        /// Accessor method only
        /// </summary>
        public decimal GrossValue
        {
            get { return _GrossValue; }
        }

        /// <summary>
        /// The status of the invoice
        /// </summary>
        public Enums.InvoiceStatusEnum Status
        {
            get { return (Enums.InvoiceStatusEnum)base.Status; }
            set { base.Status = (int)value; }
        }

        public string StatusString
        {
            get { return this.Status.ToString(); }
        }

        public DateTime DateSent
        {
            get { return _SentDate; }
            set { _SentDate = value; }
        }

        public DateTime DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }
        }

        public DateTime PaidDate
        {
            get { return _PaidDate; }
            set { _PaidDate = value; }
        }

        ///// <summary>
        ///// The list of completed work items that can be invoiced
        ///// </summary>
        //public List<WorkItem> CompletedItems
        //{
        //    get { return _CompletedItems; }
        //    set { _CompletedItems = value; }
        //}

        /// <summary>
        /// The Items included on the invoice
        /// </summary>
        public List<InvoiceItem> InvoicedItems
        {
            get 
            {
                if (_InvoicedItems == null)
                    _InvoicedItems = new List<InvoiceItem>();

                return _InvoicedItems; 
            }
            set { _InvoicedItems = value; }
        }

        /// <summary>
        /// The Document history for the Invoice
        /// </summary>
        public List<InvoiceHistoryItem> History
        {
            get 
            {
                if (_History == null)
                    _History = new List<InvoiceHistoryItem>();

                return _History;
            }
            set { _History = value; }
        }

        public DateTime DateCreated
        {
            get { return this.dateCreated; }
            set { this.dateCreated = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Method to force the recalculation of the Invoice values
        /// </summary>
        public void CalculateItemValues()
        {
            decimal val = 0.00M;
            if (this.InvoicedItems != null)
            {
                foreach (InvoiceItem itm in this.InvoicedItems)
                {
                    val += itm.Value;
                }
            }
            //Set the corrisponding member
            _Value = val;
            CalculateMemberValues();
        }

        private void CalculateMemberValues()
        {
            _VatValue = _Value * _VAT;
            _GrossValue = _Value + _VatValue;
        }


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
