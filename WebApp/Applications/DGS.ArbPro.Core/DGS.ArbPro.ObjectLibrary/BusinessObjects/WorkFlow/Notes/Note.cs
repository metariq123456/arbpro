﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class Note : BusinessBase
    {
        #region members
        int _ParentId = -1;
        Enums.NoteTypeEnum _NoteType = 0;
        string _Text = "";
        string _EnteredBy = "";
        DateTime _EnteredDate = DateTime.MinValue;
        DateTime _LastUpdatedDate = DateTime.MinValue;
        private bool _ShowOnDashboard = false;
        #endregion

        #region Constructors
        public Note() : base() { }

        public Note(int parentId, Enums.NoteTypeEnum noteType)
            : base()
        {
            _ParentId = parentId;
            _NoteType = noteType;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The Id of the parent record
        /// </summary>
        public int ParentId
        {
            get { return _ParentId; }
            set { _ParentId = value; }
        }

        /// <summary>
        /// The type of parent the note is for
        /// </summary>
        public Enums.NoteTypeEnum NoteType
        {
            get { return _NoteType; }
            set { _NoteType = value; }
        }

        /// <summary>
        /// The text for the note
        /// </summary>
        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }

        /// <summary>
        /// Who the note was entered by
        /// </summary>
        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        /// <summary>
        /// The date the note was entered
        /// </summary>
        public DateTime EnteredDate
        {
            get { return _EnteredDate; }
            set { _EnteredDate = value; }
        }

        /// <summary>
        /// The date the note was last updated
        /// </summary>
        public DateTime LastUpdated
        {
            get { return _LastUpdatedDate; }
            set { _LastUpdatedDate = value; }
        }

        /// <summary>
        /// Flag to denote if the note is to be displayed on the dashboard
        /// DGSAP-712
        /// </summary>
        public bool ShowOnDashboard
        {
            get { return _ShowOnDashboard; }
            set { _ShowOnDashboard = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
