﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class Work : WorkFlowBase
    {
        #region Members
        int _QuoteId = -1;
        int _JobId = -1;
        int _JobSiteId = -1;
        int _AppointmentId = -1;
        int _TeamId = -1;
        DateTime dateCreated = DateTime.MinValue;
        List<WorkItem> _WorkItems = null;
        #endregion

        #region Constructors
        public Work() : base() { }

        /// <summary>
        /// Optional constructer to facilitate setting the status and locked flag
        /// </summary>
        /// <param name="status">Int - The numeric value of the status enum value</param>
        /// <param name="locked">Bool - Denotes if the record can be modified</param>
        public Work(int status, bool locked) : base(status, locked) { }

        /// <summary>
        /// Optional constructer to facilitate setting the status and locked flag
        /// </summary>
        /// <param name="status">WorkStatusEnum - The enum value for the status</param>
        /// <param name="locked">Bool - Denotes if the record can be modified</param>
        public Work(WorkStatusEnum status, bool locked) : base((int)status, locked) { }
        #endregion

        #region Properties
        /// <summary>
        /// The JobId for the work record
        /// </summary>
        public int JobId
        {
            get{return _JobId;}
            set{_JobId = value;}
        }

        /// <summary>
        /// The Job Site Id for the work record
        /// </summary>
        public int JobSiteId
        {
            get { return _JobSiteId; }
            set { _JobSiteId = value; }
        }

        /// <summary>
        /// The Id for the relaited Quote
        /// </summary>
        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }

        /// <summary>
        /// The Id of the Appointment.
        /// Returns -1 if no appointment has been made
        /// </summary>
        public int AppointmentId
        {
            get { return _AppointmentId; }
            set { _AppointmentId = value; }
        }

        /// <summary>
        /// The Id of the Team record in the database.
        /// This is included for a future improvement and 
        /// is not relevant to current processes
        /// </summary>
        public int TeamId
        {
            get { return _TeamId; }
            set { _TeamId = value; }
        }

        /// <summary>
        /// The List of work items for the work record
        /// </summary>
        public List<WorkItem> WorkItems
        {
            get { return _WorkItems; }
            set { _WorkItems = value; }
        }

        /// <summary>
        /// The typed enum status for the current Work record.
        /// </summary>
        public WorkStatusEnum Status
        {
            get { return (WorkStatusEnum)base.Status; }
            set { base.Status = (int)value; }
        }

        public DateTime DateCreated
        {
            get { return this.dateCreated; }
            set { this.dateCreated = value; }
        }

        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion    
    }
}
