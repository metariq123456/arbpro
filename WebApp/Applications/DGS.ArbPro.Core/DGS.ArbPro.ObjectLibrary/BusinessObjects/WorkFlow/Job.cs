﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class Job : WorkFlowBase
    {
        #region Members
        List<QuoteSearchResult> _JobQuotes = null;
        List<WorkSearchResult> _JobWork = null;
        List<InvoiceSearchResult> _JobInvoices = null;
        List<JobSite> _WorkSites = null;
        string _Source = "";
        string _Comments = "";
        string _Reporter = "";
        int _ReporterID = -1;
        DateTime dateCreated = DateTime.MinValue;
        Enums.JobStatusEnum _Status = JobStatusEnum.Pending;

        private int _SurveyorId = -1;
        private bool _ShowInDashboard = true;
        private string _DefaultSurveyor = "Not Set";
        #endregion

        #region Constructors
        public Job() : base() { }
        #endregion

        #region Properties

        /// <summary>
        /// The Quote history of the Job for all sites
        /// </summary>
        public List<QuoteSearchResult> Quotes
        {
            get { return _JobQuotes; }
            set { _JobQuotes = value; }
        }

        /// <summary>
        /// The Quote history of the Job for all sites
        /// </summary>
        public List<WorkSearchResult> Work
        {
            get { return _JobWork; }
            set { _JobWork = value; }
        }

        /// <summary>
        /// The Invoice History of the Job for all Sites
        /// </summary>
        public List<InvoiceSearchResult> Invoices
        {
            get { return _JobInvoices; }
            set { _JobInvoices = value; }
        }

        /// <summary>
        /// The Sites allocated to the Job
        /// </summary>
        public List<JobSite> WorkSites
        {
            get { return _WorkSites; }
            set { _WorkSites = value; }
        }

        public string Source
        {
            get { return _Source; }
            set { _Source = value; }
        }

        public string Reporter
        {
            get { return _Reporter; }
            set { _Reporter = value; }
        }

        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        public int ReporterID
        {
            get { return _ReporterID; }
            set { _ReporterID = value; }
        }
        #endregion

        public new JobStatusEnum Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        public int SurveyorId
        {
            get { return _SurveyorId; }
            set { _SurveyorId = value; }
        }

        public bool ShowInDashboard
        {
            get { return _ShowInDashboard; }
            set { _ShowInDashboard = value; }
        }

        public string DefaultSurveyor
        {
            get { return _DefaultSurveyor; }
            set { _DefaultSurveyor = value; }
        }

        public string StatusString
        {
            get
            {
                var statString = string.Empty;
                switch (this.Status)
                {
                    case JobStatusEnum.WorkCompleted:
                        statString = "Work Completed";
                        break;
                    default:
                        statString = Status.ToString();
                        break;
                }
                return statString;
            }
            set{ /* Do nothing */ }
        }

        public DateTime DateCreated
        {
            get { return this.dateCreated; }
            set { this.dateCreated = value; }
        }


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}