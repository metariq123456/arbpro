﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class CreditNote : WorkFlowBase
    {
        #region Members
        /*
            Id int PRIMARY KEY IDENTITY(1,1) NOT NULL,
	        InvoiceId INT NOT NULL,
	        Value MONEY NOT NULL DEFAULT 0,
	        Document VARBINARY(MAX) NULL,
	        DateSent DATETIME NOT NULL,
	        UserId INT NOT NULL,
	        Status INT NOT NULL DEFAULT 0,
	        VAT decimal (3,3)
         * */
        int _InvoiceID = -1;
        int _UserId = -1;
        string _HouseNameNo = "";
        string _Address1 = "";
        string _Address2 = "";
        string _Town = "";
        string _County = "";
        string _PostCode = "";
        decimal _VAT = 0.00M;
        ClientSite _Site = null;
        List<CreditItem> _CreditedItems = null;
        List<CreditHistory> _History = null;
        #endregion

        #region Constructors
        public CreditNote() : base() { }
        #endregion

        #region Properties
        /// <summary>
        /// The invoice of Id that the credit note has been raised against
        /// </summary>
        public int InvoiceId
        {
            get { return _InvoiceID; }
            set { _InvoiceID = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the Contact may change
        /// </summary>
        public string HouseNameNo
        {
            get { return _HouseNameNo; }
            set { _HouseNameNo = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the Contact may change
        /// </summary>
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the Contact may change
        /// </summary>
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the Contact may change
        /// </summary>
        public string Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the Contact may change
        /// </summary>
        public string County
        {
            get { return _County; }
            set { _County = value; }
        }

        /// <summary>
        /// The House name or number for the Invoice address
        /// This is stored in the Invoice as the Contact may change
        /// </summary>
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        public Enums.CreditStatusEnum Status
        {
            get { return (CreditStatusEnum)base.Status; }
            set { base.Status = (int)value; }
        }

        public decimal VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }

        public ClientSite Site
        {
            get { return _Site; }
            set { _Site = value; }
        }

        /// <summary>
        /// A List of all the items attributed to the credit note.
        /// These may be work invoiced items.
        /// </summary>
        public List<CreditItem> CreditedItems
        {
            get { return _CreditedItems; }
            set { _CreditedItems = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
