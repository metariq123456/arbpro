﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class CreditHistory : DocumentHistory
    {
        #region Member

        #endregion

        #region Constructors
        public CreditHistory()
            : base(ReportTypeEnum.CreditNote)
        { }
        #endregion

        #region Properties


        public override string DocTypeString
        {
            get
            {
                return "Credit Note";
            }
            set
            {
                //Do nothing
            }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
