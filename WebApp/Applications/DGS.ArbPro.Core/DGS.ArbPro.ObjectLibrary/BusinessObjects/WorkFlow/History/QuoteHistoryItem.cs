﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class QuoteHistoryItem : DocumentHistory
    {
        #region Member
        Enums.DocContentTypeEnum _DocumentType = Enums.DocContentTypeEnum.Schedule;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public QuoteHistoryItem():base(ReportTypeEnum.Quote) { }

        /// <summary>
        /// Optional constructor with dates
        /// </summary>
        /// <param name="creationDate">DateTime: The date the document was created</param>
        /// <param name="sentDate">DateTime: The date the document was dispatched</param>
        public QuoteHistoryItem(DateTime creationDate, DateTime sentDate)
            : base(ReportTypeEnum.Quote)
        {
            this.CreationDate = creationDate;
            this.SentDate = sentDate;
        }

        /// <summary>
        /// Optional constructor to set the document type and memory stream
        /// </summary>
        /// <param name="docType">Enums.DocContentTypeEnum: The type of document</param>
        /// <param name="stream">MemoryStream: The memory stream containing the PDF document</param>
        public QuoteHistoryItem(Enums.DocContentTypeEnum docType, MemoryStream stream)
            : base(ReportTypeEnum.Quote)
        {
            this.DocumentType = docType;
            this.Doc = stream;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The type of document (Schedule / Letter)
        /// </summary>
        public Enums.DocContentTypeEnum DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }

        public override string DocTypeString
        {
            get
            {
                return this.DocumentType.ToString().Replace("_"," ");
            }
            set
            {
                //Do nothing
            }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
