﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class InvoiceHistoryItem : DocumentHistory
    {
        #region Member
        Enums.InvoiceDocTypeEnum _DocumentType = Enums.InvoiceDocTypeEnum.Letter;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public InvoiceHistoryItem():base(ReportTypeEnum.Invoice) { }

        /// <summary>
        /// Optional constructor with dates
        /// </summary>
        /// <param name="creationDate">DateTime: The date the document was created</param>
        /// <param name="sentDate">DateTime: The date the document was dispatched</param>
        public InvoiceHistoryItem(DateTime creationDate, DateTime sentDate)
            : base(ReportTypeEnum.Invoice)
        {
            this.CreationDate = creationDate;
            this.SentDate = sentDate;
        }

        /// <summary>
        /// Optional constructor to set the document type and memory stream
        /// </summary>
        /// <param name="docType">Enums.DocContentTypeEnum: The type of document</param>
        /// <param name="stream">MemoryStream: The memory stream containing the PDF document</param>
        public InvoiceHistoryItem(Enums.InvoiceDocTypeEnum docType, MemoryStream stream)
            : base(ReportTypeEnum.Invoice)
        {
            this.DocumentType = docType;
            this.Doc = stream;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The type of document (Schedule / Letter)
        /// </summary>
        public Enums.InvoiceDocTypeEnum DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }

        public override string DocTypeString
        {
            get
            {
                if (_DocumentType == InvoiceDocTypeEnum.Letter_And_Schedule)
                    return InvoiceDocTypeEnum.Letter.ToString();
                else
                    return (_DocumentType.ToString()).Replace("_", " ") ;
            }
            set
            {
                //Do nothing
            }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}