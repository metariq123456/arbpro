﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class QuoteItem : Item
    {
        #region Members
        int _QuoteId = -1;
        bool _Quoted = false;
        #endregion

        #region Constructors
        public QuoteItem() : base() { }

        public QuoteItem(int quoteID, JobItem item) : base((Item)item) 
        {
            _QuoteId = quoteID;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The Id of the quote record the item belongs to
        /// </summary>
        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }

        /// <summary>
        /// Status flag to show if the item was sent on the last Quote
        /// </summary>
        public bool Quoted
        {
            get { return _Quoted; }
            set { _Quoted = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
