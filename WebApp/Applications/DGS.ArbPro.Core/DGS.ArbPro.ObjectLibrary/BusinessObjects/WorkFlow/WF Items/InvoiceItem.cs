﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class InvoiceItem : Item
    {
        #region Members
        int _InvoiceId = -1;
        int _WorkItemId = -1;
        #endregion

        #region Constructors
        public InvoiceItem() : base() { }

        public InvoiceItem(int invId, WorkItem item) : base(item) 
        {
            _InvoiceId = invId;
            _WorkItemId = item.Id;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The Id of the invoice record the item belongs to
        /// </summary>
        public int InvoiceId
        {
            get { return _InvoiceId; }
            set { _InvoiceId = value; }
        }

        /// <summary>
        /// The Id of the relaited Work Item
        /// </summary>
        public int WorkItemId
        {
            get { return _WorkItemId; }
            set { _WorkItemId = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}