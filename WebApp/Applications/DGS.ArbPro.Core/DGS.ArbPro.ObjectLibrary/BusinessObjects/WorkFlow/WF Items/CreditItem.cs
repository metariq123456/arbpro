﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class CreditItem : Item
    {
        #region Members
        int _CreditNoteId = -1;
        int _InvoiceItemId = -1;
        string _Description = "";
        string _Reason = "";
        decimal _Value = 0.00M;
        #endregion

        #region Constructors
        public CreditItem() : base() { }

        public CreditItem(int creditId, InvoiceItem item) : base((Item)item) 
        {
            _CreditNoteId = creditId;
            _InvoiceItemId = item.Id;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The record Id that the item belongs to
        /// </summary>
        public int CreditNoteId
        {
            get { return _CreditNoteId; }
            set { _CreditNoteId = value; }
        }

        /// <summary>
        /// The invoice item that is being credited.
        /// This may be -1 as it may not be an actual item
        /// </summary>
        public int InvoiceItemId
        {
            get { return _InvoiceItemId; }
            set { _InvoiceItemId = value; }
        }

        /// <summary>
        /// The description of the item being credited
        /// </summary>
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        /// <summary>
        /// The reason the item is being credited
        /// </summary>
        public string Reason
        {
            get { return _Reason; }
            set { _Reason = value; }
        }

        /// <summary>
        /// The credit value
        /// </summary>
        public decimal Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
