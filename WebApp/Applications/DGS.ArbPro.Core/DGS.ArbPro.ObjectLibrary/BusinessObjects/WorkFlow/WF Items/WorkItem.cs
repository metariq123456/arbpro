﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class    WorkItem : Item
    {
        #region Members
        int _WorkId = -1;
        int _QuoteItemId = -1;
        bool _IsScheduled = false;   //Items are scheduled by default
        bool _Completed = false;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public WorkItem():base(){}


        public WorkItem(int workId, QuoteItem item) : base((Item)item)
        {
            _WorkId = workId;
            _QuoteItemId = item.Id;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The Id of the work record the item belongs to
        /// </summary>
        public int WorkId
        {
            get { return _WorkId; }
            set { _WorkId = value; }
        }

        /// <summary>
        /// The Id of the relaited QuoteItem
        /// value is -1 if added at Work stage
        /// </summary>
        public int QuoteItemId
        {
            get { return _QuoteItemId; }
            set { _QuoteItemId = value; }
        }

        /// <summary>
        /// Scheduled flag to denote if the items has been booked for work
        /// </summary>
        public bool Booked
        {
            get { return _IsScheduled; }
            set { _IsScheduled = value; }
        }

        /// <summary>
        /// Completion status flag
        /// </summary>
        public bool Completed
        {
            get { return _Completed; }
            set { _Completed = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
