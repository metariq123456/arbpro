﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class ItemPicture: BusinessBase
    {
        #region Members
        string _ImageGuid = "";
        int _JobItemId = -1;
        int _ParentId = -1;
        Enums.ItemTypeEnum _ParentItemType = Enums.ItemTypeEnum.JobItem;
        byte[] _ImageBytes = null;
        #endregion

        #region Constructors
        public ItemPicture() : base() { }

        public ItemPicture(Guid imageGuid)
            : base()
        {
            _ImageGuid = imageGuid.ToString();
        }

        #endregion

        #region Properties
        /// <summary>
        /// The Guid for an image primarily used for upload
        /// </summary>
        public string ImageGuid
        {
            get { return _ImageGuid; }
            set { _ImageGuid = value; }
        }

        /// <summary>
        /// The Job Item Id of the base item
        /// </summary>
        public int JobItemId
        {
            get { return _JobItemId; }
            set { _JobItemId = value; }
        }

        /// <summary>
        /// The record Item of the relevcant item.
        /// This will be where the item was added, as a Quote Item, Work Item etc
        /// </summary>
        public int ParentId
        {
            get { return _ParentId; }
            set { _ParentId = value; }
        }

        /// <summary>
        /// The type of item the intial upload was attribted to
        /// </summary>
        public Enums.ItemTypeEnum ParentItemType
        {
            get { return _ParentItemType; }
            set { _ParentItemType = value; }
        }

        /// <summary>
        /// The byte array of the image
        /// </summary>
        public byte[] ImageBytes
        {
            get { return _ImageBytes; }
            set { _ImageBytes = value; }
        }

        public bool HasImage
        {
            get { return ImageBytes != null && ImageBytes.Length > 0; }
        }

        public string Base64Image { get; set; }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
