﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class ClientSite : BusinessBase
    {
        #region Members
        String _Reference = "";
        int _ClientId = -1;
        int _ContactId = -1;
        string _HouseNameNo = "";
        string _Address1 = "";
        string _Address2 = "";
        string _Town = "";
        string _County = "";
        string _PostCode = "";
        double _Latitude = 0;
        double _Longitude = 0;
        #endregion

        #region Constructors
        public ClientSite() : base() { }
        #endregion

        #region Properties

        /// <summary>
        /// The Reference of the client site for this record.
        /// </summary>
        public String Reference
        {
            get { return _Reference; }
            set { _Reference = value; }
        }

        /// <summary>
        /// The Id of the client record the site is for.
        /// </summary>
        public int ClientId
        {
            get { return _ClientId; }
            set { _ClientId = value; }
        }

        /// <summary>
        /// The Id of the client contact for the site.
        /// This can return -1
        /// </summary>
        public int ContactId
        {
            get { return _ContactId; }
            set { _ContactId = value; }
        }

        /// <summary>
        /// The house name or number of the address
        /// </summary>
        public string HouseNameNo
        {
            get { return _HouseNameNo; }
            set { _HouseNameNo = value; }
        }

        /// <summary>
        /// The first line of the address
        /// </summary>
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        /// <summary>
        /// The second line of the address
        /// </summary>
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        /// <summary>
        /// The Town or City the address is in
        /// </summary>
        public string Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        /// <summary>
        /// The county the address is in
        /// </summary>
        public string County
        {
            get { return _County; }
            set { _County = value; }
        }

        /// <summary>
        /// The Post code for the address
        /// </summary>
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        /// <summary>
        /// The Geo location latitude of the address
        /// </summary>
        public double Latitude
        {
            get { return _Latitude; }
            set { _Latitude = value; }
        }

        /// <summary>
        /// The Geo location latitude of the address
        /// </summary>
        public double Longitude
        {
            get { return _Longitude; }
            set { _Longitude = value; }
        }

        public String Line1
        {
            get { return HouseNameNo + " " + Address1; }
        }

        /// <summary>
        /// Gets the site details as a single line, without the postcode
        /// DGSAP-792: This property is required for access by JavaScript
        /// </summary>
        public string SiteDetails
        {
            get { return this.GetSiteAddress(false); }
        }
        #endregion


        #region Methods
        /// <summary>
        /// Public method to generate site address details
        /// </summary>
        /// <param name="includePostCode">Optional parameter to denote if the postcode should be appended to the returned value</param>
        /// <returns>Comma delimited string for the site address details</returns>
        public string GetSiteAddress(bool includePostCode = true)
        {
            StringBuilder sb = new StringBuilder();
            this.AppendAddressValue(ref sb, this.HouseNameNo);
            this.AppendAddressValue(ref sb, this.Address1);
            this.AppendAddressValue(ref sb, this.Town);

            // Check to see if the poscode is to be added
            if (includePostCode)
            {
                this.AppendAddressValue(ref sb, this.PostCode);
            }

            // DGSAP-792 Check if there are any details for the site address 
            if (sb.Length == 0)
            {
                sb.Append("No Site Address.");
            }

            return sb.ToString();
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }

        private void AppendAddressValue(ref StringBuilder sb, string value)
        {
            // Check if the value to be appended is valid
            if (!string.IsNullOrWhiteSpace(value))
            {
                // Check the length of the string build to check if a comma needs to be appended
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }

                sb.Append(value);
            }
        }
        #endregion
    }
}
