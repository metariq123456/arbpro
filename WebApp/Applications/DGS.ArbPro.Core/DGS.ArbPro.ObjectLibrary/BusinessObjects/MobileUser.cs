﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class MobileUser : UserBase
    {
        #region Members

        public int CompanyID;
        public string ClientDatabaseName;

        // Not stored in Database - used for concurrent user check only
        public String IPAddress;
        #endregion

        #region Constructors
        public MobileUser() : base() { }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion

    }
}
