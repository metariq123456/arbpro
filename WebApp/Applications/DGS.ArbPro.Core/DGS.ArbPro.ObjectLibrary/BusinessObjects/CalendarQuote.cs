﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class CalendarQuote : CalendarBase
    {
        #region Members
        private Int32 _SurvayorID = -1;
        private Int32 _JobSiteID = -1;
        private String _JobSiteName;
        private Boolean _Completed;
        private DateTime _LastUpdated = DateTime.MinValue;
        private DateTime _LastDownloaded = DateTime.MinValue;
        private DateTime _UploadTime = DateTime.MinValue;
        private Int32 _TemplateId = 1;
        #endregion

        #region Constructors
        public CalendarQuote()
        {
        }
        #endregion

        #region Properties
        public Int32 SurveyorID
        {
            get { return _SurvayorID; }
            set { _SurvayorID = value; }
        }

        public Int32 JobSiteID
        {
            get { return _JobSiteID; }
            set { _JobSiteID = value; }
        }

        public String JobSiteName
        {
            get { return _JobSiteName; }
            set { _JobSiteName = value; }
        }

        public Boolean Completed
        {
            get { return _Completed; }
            set { _Completed = value; }
        }

        public DateTime LastUpdated
        {
            get { return _LastUpdated; }
            set { _LastUpdated = value; }
        }

        public DateTime LastDownloaded
        {
            get { return _LastDownloaded; }
            set { _LastDownloaded = value; }
        }

        public DateTime UploadTime
        {
            get { return _UploadTime; }
            set { _UploadTime = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }

        public Int32 TemplateId
        {
            get { return _TemplateId; }
            set { _TemplateId = value; }
        }
        #endregion
    }
}
