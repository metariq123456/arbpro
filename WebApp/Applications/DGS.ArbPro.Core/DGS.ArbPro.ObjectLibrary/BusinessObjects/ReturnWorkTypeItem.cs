﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.Types
{
    public class ReturnWorkTypeItem : BusinessBase
    {
        #region Members
        #endregion

        #region Constructors
        public ReturnWorkTypeItem() : base() { }

        #endregion

        #region Properties
        public string Type { get; set; }
        public string UserCR { get; set; }
        public string DefaultLetterQA { get; set; }
        public string DefaultLetterNA { get; set; }
        public string DefaultLetterReturn { get; set; }
        public string ReturnDate { get; set; }
        public string RetReqd { get; set; } //HTML component

        public int ClientID {get;set;}
        public int ValueQuoteNo { get; set; }

        public bool ReturnCompleted { get; set; }

        public decimal ReturnDateID { get; set; }
        public decimal WorkTypeID { get; set; }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion

    }
}

