﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class Client : BusinessBase
    {
        #region Members
        private string _AltAccountNo = "";
        private string _Forename = "";
        private string _Surname = "";
        private string _Title = "";
        private string _HouseNameOrNumber = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private string _Town = "";
        private string _County = "";
        private string _PostCode = "";
        private string _TelephoneNumber = "";
        private string _Mobile = "";
        private string _Email = "";
        private string _BusinessName = "";

        private Enums.ClientTypeEnum _AccountType = ClientTypeEnum.Other;
        private DateTime _DateOpened = DateTime.MinValue;

        private int _MultiRefLink = 0;
        private string _CoCode = "";
        private string _Sector = "";
        private string _WebAddress = "";
        private string _PaymentDays = "";
        private decimal _CreditLimit = 0.00M;
        private bool _TermsAgreed = false;
        private string _VATNo = "";
        private double _AddressLatitude = 0.00f;
        private double _AddressLongitude = 0.00f;

        // DGSAP-855
        private bool _SendSmsMessage = true;

        private bool _IncludeInMarketing = true;

        private List<ContactSearchResult> _Contacts = null;
        private List<QuoteSearchResult> _Quotes = null;
        private List<InvoiceSearchResult> _Invoices = null;
        #endregion

        #region Constructors
        public Client() : base() { }

        #endregion

        #region Properties
        public string AltAccountNo
        {
            get { return _AltAccountNo; }
            set { _AltAccountNo = value; }
        }

        public string Forename
        {
            get { return _Forename; }
            set { _Forename = value; }
        }

        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string HouseNameNumber
        {
            get { return _HouseNameOrNumber; }
            set { _HouseNameOrNumber = value; }
        }

        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        public string Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        public string County
        {
            get { return _County; }
            set { _County = value; }
        }

        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        public string Telephone
        {
            get { return _TelephoneNumber; }
            set { _TelephoneNumber = value; }
        }

        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public string BusinessName
        {
            get { return _BusinessName; }
            set { _BusinessName = value; }
        }

        public Enums.ClientTypeEnum AccountType
        {
            get { return _AccountType; }
            set { _AccountType = value; }
        }

        public DateTime DateOpened
        {
            get { return _DateOpened; }
            set { _DateOpened = value; }
        }

        public double AddressLatitude
        {
            get { return _AddressLatitude; }
            set { _AddressLatitude = value; }
        }

        public double AddressLongitude
        {
            get { return _AddressLongitude; }
            set { _AddressLongitude = value; }
        }

        //V3.7.0
        public bool SendSmsMessage
        {
            get { return _SendSmsMessage; }
            set { _SendSmsMessage = value; }
        }

        public bool IncludeInMarketing
        {
            get { return this._IncludeInMarketing; }
            set { this._IncludeInMarketing = value; }
        }

        public List<ContactSearchResult> Contacts
        {
            get { return _Contacts; }
            set { _Contacts = value; }
        }

        public List<QuoteSearchResult> Quotes
        {
            get { return _Quotes; }
            set { _Quotes = value; }
        }

        public List<InvoiceSearchResult> Invoices
        {
            get { return _Invoices; }
            set { _Invoices = value; }
        }


        public int MultiRefLink
        {
            get { return _MultiRefLink; }
            set { _MultiRefLink = value; }
        }

        public string CoCode
        {
            get { return _CoCode; }
            set { _CoCode = value; }
        }

        public string Sector
        {
            get { return _Sector; }
            set { _Sector = value; }
        }

        public string WebAddress
        {
            get { return _WebAddress; }
            set { _WebAddress = value; }
        }

        public string PaymentDays
        {
            get { return _PaymentDays; }
            set { _PaymentDays = value; }
        }

        public decimal CreditLimit
        {
            get { return _CreditLimit; }
            set { _CreditLimit = value; }
        }

        public bool TermsAgreed
        {
            get { return _TermsAgreed; }
            set { _TermsAgreed = value; }
        }

        public string VATNo
        {
            get { return _VATNo; }
            set { _VATNo = value; }
        }
        
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
