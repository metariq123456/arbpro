﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class User : UserBase
    {
        #region Members

        public Int32 CompanyID;
        public String Title;
        public String Telephone;
        public Boolean UserAccountEnabled;
        public Boolean MobileEditionEnabled;
        public String CompanyPosition;
        public Boolean TsAndCsAccepted;
        public String Comment;
        public DateTime CreationDate;
        public string SessionId;
        public DateTime LastActivityDate;
        public String ClientDatabaseName;

        /// <summary>
        /// Flag to denote if the user has access to the web application
        /// </summary>
        public Boolean IsWebEnabled;

        // Not stored in Database - used for concurrent user check only
        public String IPAddress;
        #endregion

        #region Constructors
        public User() : base() { }
        #endregion

        #region Properties
        /// <summary>
        /// The fully formatted name of the user, with Title, Forename and Surname - with spaces
        /// in between.
        /// </summary>
        public override String NameStr
        {
            get
            {
                StringBuilder str = new StringBuilder();

                str.Append(Title == null ? "" : Title);
                str.Append(" ");
                str.Append(FirstName == null ? "" : FirstName);
                str.Append(" ");
                str.Append(LastName == null ? "" : LastName);
                return str.Replace("  ", " ").ToString().Trim();
            }
        }

        public string SessionID { get; set; }

        #endregion

        #region Overrides
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion

    }
}
