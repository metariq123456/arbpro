﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class JobDocumentsObject : BusinessBase
    {
        #region Members
        private string _DocumentName = "";
        private string _DocumentType = "";
        private DateTime _DocumentDate = DateTime.MinValue;
        private string _Staff = "";
        private byte[] _DocumentImage = null;
        private string _DocumentFileName = "";
        private bool _DefaultAttachment = false;
        private int _JobId = 0;
        #endregion

        #region Constructors
        public JobDocumentsObject() { }

        #endregion

        #region Properties
        public string DocumentName
        {
            get { return _DocumentName; }
            set { _DocumentName = value; }
        }
        public string DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        public DateTime DocumentDate
        {
            get { return _DocumentDate; }
            set { _DocumentDate = value; }
        }
        public string Staff
        {
            get { return _Staff; }
            set { _Staff = value; }
        }

        public byte[] DocumentImage 
        {
            get { return _DocumentImage; }
            set { _DocumentImage = value; }

        }
        public string DocumentFileName
        {
            get { return _DocumentFileName; }
            set { _DocumentFileName = value; }
        }
        public bool DefaultAttachment
        {
            get { return _DefaultAttachment; }
            set { _DefaultAttachment = value; }
        }
        public int JobId
        {
            get { return _JobId; }
            set { _JobId = value; }
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
