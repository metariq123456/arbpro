﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Marketing.cs" company="Microsec Ltd">
//   Copyright 2016 Microsec Ltd
// </copyright>
// <summary>
//   The marketing.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.Types
{
    using System;

    using DGS.ArbPro.Types.Enums;

    /// <summary>
    /// The marketing.
    /// </summary>
    public class Marketing : BusinessBase
    {
        #region Members

        #region WorkCategoriesAndClientWorkCategoriesCommon

        /// <summary>
        /// The name.
        /// </summary>
        private string name = string.Empty;

        /// <summary>
        /// The description.
        /// </summary>
        private string description = string.Empty;

        /// <summary>
        /// The return time category.
        /// </summary>
        private MarketingReturnTimeCategoriesEnum returnTimeCategory = (MarketingReturnTimeCategoriesEnum)99;

        #endregion

        #region ClientWorkCategory

        /// <summary>
        /// The client id.
        /// </summary>
        private int jobId = -1;

        /// <summary>
        /// The comments.
        /// </summary>
        private string comments = string.Empty;

        /// <summary>
        /// The return date.
        /// </summary>
        private DateTime returnDate = DateTime.MinValue;

        /// <summary>
        /// The email sent date.
        /// </summary>
        private DateTime emailSentDate = DateTime.MinValue;
        
        #endregion

        #region Client

        /// <summary>
        /// The client name.
        /// </summary>
        private string clientName = string.Empty;

        /// <summary>
        /// The address.
        /// </summary>
        private string address = string.Empty;

        /// <summary>
        /// The telephone.
        /// </summary>
        private string telephone = string.Empty;

        /// <summary>
        /// The mobile.
        /// </summary>
        private string mobile = string.Empty;

        /// <summary>
        /// The email.
        /// </summary>
        private string email = string.Empty;

        #endregion

        #region Return Time Categories

        /// <summary>
        /// The time category id.
        /// </summary>
        private int timeCategoryId = -1;

        /// <summary>
        /// The time category name.
        /// </summary>
        private string timeCategoryName = string.Empty;

        #endregion

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Marketing"/> class.
        /// </summary>
        public Marketing()
        {
            this.EmailSent = false;
            this.QuoteAccepted = false;
        }

        #endregion

        #region Properties

        #region WorkCategoriesAndClientWorkCategoriesCommon
        
        /// <summary>
        /// Gets or sets the work category name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        /// <summary>
        /// Gets or sets the work category description.
        /// </summary>
        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        /// <summary>
        /// Gets or sets the return time category.
        /// </summary>
        public MarketingReturnTimeCategoriesEnum ReturnTimeCategory
        {
            get { return this.returnTimeCategory; }
            set { this.returnTimeCategory = value; }
        }

        #endregion

        #region ClientWorkCategories

        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        public int JobId
        {
            get { return this.jobId; }
            set { this.jobId = value; }
        }

        /// <summary>
        /// Gets or sets the comments.
        /// </summary>
        public string Comments
        {
            get { return this.comments; }
            set { this.comments = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether quote accepted.
        /// </summary>
        public bool QuoteAccepted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether email sent.
        /// </summary>
        public bool EmailSent { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is for return marketing.
        /// </summary>
        public bool IsForReturnMarketing { get; set; }

        /// <summary>
        /// Gets or sets the return date.
        /// </summary>
        public DateTime ReturnDate
        {
            get { return this.returnDate; }
            set { this.returnDate = value; }
        }

        /// <summary>
        /// Gets or sets the email sent date.
        /// </summary>
        public DateTime EmailSentDate
        {
            get { return this.emailSentDate; }
            set { this.emailSentDate = value; }
        }
        
        #endregion
        
        #region Return Time Categories

        /// <summary>
        /// Gets or sets the time category id.
        /// </summary>
        public int TimeCategoryId
        {
            get { return this.timeCategoryId; }
            set { this.timeCategoryId = value; }
        }

        /// <summary>
        /// Gets or sets the time category name.
        /// </summary>
        public string TimeCategoryName
        {
            get { return this.timeCategoryName; }
            set { this.timeCategoryName = value; }
        }

        #endregion

        #region Client

        /// <summary>
        /// Gets or sets the client name.
        /// </summary>
        public string ClientName
        {
            get { return this.clientName; }
            set { this.clientName = value; }
        }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        public string Address
        {
            get { return this.address; }
            set { this.address = value; }
        }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email
        {
            get { return this.email; }
            set { this.email = value; }
        }

        /// <summary>
        /// Gets or sets the telephone.
        /// </summary>
        public string Telephone
        {
            get { return this.telephone; }
            set { this.telephone = value; }
        }

        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        public string Mobile
        {
            get { return this.mobile; }
            set { this.mobile = value; }
        }
        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.GetObjectState(this);
        }

        #endregion
    }
}
