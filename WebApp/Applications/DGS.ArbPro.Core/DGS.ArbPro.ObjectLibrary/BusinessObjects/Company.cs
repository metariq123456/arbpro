﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class Company : BusinessBase
    {
        //TODO: Put in Master DB
        public const int MAX_CONCURRENT = 1;

        #region Members
        private string _RegistrationNo = "";
        private string _VatRegNo = "";
        private string _Name = "";
        private string _StreetNumber = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private string _Town = "";
        private string _County = "";
        private string _PostCode = "";
        private string _Telephone = "";
        private string _Mobile = "";
        private string _WebSite = "";
        private string _Email = "";
        private string _AccountsEmail = "";
        private int _DirectorId = -1;
        private string _DirectorsName = "";
        private Enums.EmailItemTypeEnum _HeaderType = Enums.EmailItemTypeEnum.None;
        private byte[] _Header = null;
        private Enums.EmailItemTypeEnum _FooterType = Enums.EmailItemTypeEnum.None;
        private byte[] _Footer = null;
        private byte[] _DirectorsSignature = null;
        private string _DirectorsQualifications = "";
        private decimal _VatRate = 0.20M;
        private string _SageVatCode = "";
        private string _SageSalesCode = "";
        private string _SageReceiptCode = "";
        private bool _CommonNames = true;
        private bool displayMarketing = true;
        #endregion

        #region Constructors
        public Company() : base() { }

        #endregion

        #region Properties
        /// <summary>
        /// The Companies House registration number
        /// </summary>
        public string RegistrationNo
        {
            get { return _RegistrationNo; }
            set { _RegistrationNo = value; }
        }

        /// <summary>
        /// The VAT registration number
        /// </summary>
        public string VatRegNo
        {
            get { return _VatRegNo; }
            set { _VatRegNo = value; }
        }

        /// <summary>
        /// The Company name
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// The house name or number on the street
        /// </summary>
        public string StreetNumber
        {
            get { return _StreetNumber; }
            set { _StreetNumber = value; }
        }

        /// <summary>
        /// The first line of the company's address
        /// </summary>
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        /// <summary>
        /// The second line of the company's address
        /// </summary>
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        /// <summary>
        /// The town of the company's address
        /// </summary>
        public string Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        /// <summary>
        /// The county of the company's address
        /// </summary>
        public string County
        {
            get { return _County; }
            set { _County = value; }
        }

        /// <summary>
        /// The post code of the company's address
        /// </summary>
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        /// <summary>
        /// The company's switchboard
        /// </summary>
        public string Telephone
        {
            get { return _Telephone; }
            set { _Telephone = value; }
        }

        /// <summary>
        /// The company's mobile number
        /// </summary>
        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }

        /// <summary>
        /// The company's web address
        /// </summary>
        public string WebSite
        {
            get { return _WebSite; }
            set { _WebSite = value; }
        }

        /// <summary>
        /// The email address of the accounts department
        /// </summary>
        public string AccountsEmail
        {
            get { return _AccountsEmail; }
            set { _AccountsEmail = value; }
        }

        /// <summary>
        /// The main email address of the company
        /// </summary>
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        /// <summary>
        /// The record Id of the staff record for the director
        /// </summary>
        public int DirectorId
        {
            get { return _DirectorId; }
            set { _DirectorId = value; }
        }

        /// <summary>
        /// The name of the director
        /// </summary>
        public string DirectorsName
        {
            get { return _DirectorsName; }
            set { _DirectorsName = value; }
        }

        /// <summary>
        /// The type of email header options are
        /// None
        /// String
        /// Image
        /// </summary>
        public Enums.EmailItemTypeEnum HeaderType
        {
            get { return _HeaderType; }
            set { _HeaderType = value; }
        }

        /// <summary>
        /// The data array of the email header
        /// Can return null
        /// </summary>
        public byte[] Header
        {
            get { return _Header; }
            set { _Header = value; }
        }

        /// <summary>
        /// The type of email footer. Options are
        /// None
        /// String
        /// Image
        /// </summary>
        public Enums.EmailItemTypeEnum FooterType
        {
            get { return _FooterType; }
            set { _FooterType = value; }
        }

        /// <summary>
        /// The data array of the email footer
        /// Can return null
        /// </summary>
        public byte[] Footer
        {
            get { return _Footer; }
            set { _Footer = value; }
        }

        /// <summary>
        /// The bytes of the image of the directors signature
        /// Can be null
        /// </summary>
        public byte[] DirectorsSignature
        {
            get { return _DirectorsSignature; }
            set { _DirectorsSignature = value; }
        }

        /// <summary>
        /// The Qualifications of the director
        /// </summary>
        public string DirectorsQualifications
        {
            get { return _DirectorsQualifications; }
            set { _DirectorsQualifications = value; }
        }

        /// <summary>
        /// The default VAT rate for the company
        /// </summary>
        public decimal VatRate
        {
            get { return _VatRate; }
            set { _VatRate = value; }
        }

        /// <summary>
        /// The sage code for VAT export
        /// </summary>
        public string SageVatCode
        {
            get { return _SageVatCode; }
            set { _SageVatCode = value; }
        }

        /// <summary>
        /// The Sage nominal sales code for export
        /// </summary>
        public string SageSalesCode
        {
            get { return _SageSalesCode; }
            set { _SageSalesCode = value; }
        }

        /// <summary>
        /// The Sage nominal reciept code for export
        /// </summary>
        public string SageReceiptCode
        {
            get { return _SageReceiptCode; }
            set { _SageReceiptCode = value; }
        }

        /// <summary>
        /// Boolean to set the default type of tree names to use
        /// True if normal, false if Latin
        /// </summary>
        public bool CommonNames
        {
            get { return _CommonNames; }
            set { _CommonNames = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Marketing Management window 
        /// will automatically appear after Quote has been produced and accepted
        /// </summary>
        public bool DisplayMarketing
        {
            get { return this.displayMarketing; }
            set { this.displayMarketing = value; }
        }

        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
