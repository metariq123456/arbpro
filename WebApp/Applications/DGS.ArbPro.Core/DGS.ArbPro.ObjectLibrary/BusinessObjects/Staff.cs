﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class Staff : BusinessBase
    {
        #region Members
        private string _Name = "";
	    private string _Email = "";
	    private string _Mobile = "";
	    private string _Telephone = "";
	    private Enums.StaffRoleEnum _Position = Enums.StaffRoleEnum.None;
	    private string _StreetNumber  = "";
	    private string _Address1 = "";
	    private string _Address2 = "";
	    private string _Town = "";
	    private string _County = "";
	    private string _PostCode = "";
	    private string _StaffRole = "";
	    private byte[] _Signature = null;
        private bool _IsMobileUser = false;
        private int _MobileUserId = -1;
        private string _Password = "";
        #endregion

        #region Constructors
        public Staff() : base() { }

        #endregion

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }

        public string Telephone
        {
            get { return _Telephone; }
            set { _Telephone = value; }
        }

        public Enums.StaffRoleEnum Position
        {
            get { return _Position; }
            set { _Position = value; }
        }

        public string StreetNumber
        {
            get { return _StreetNumber; }
            set { _StreetNumber = value; }
        }

        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        public string Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        public string County
        {
            get { return _County; }
            set { _County = value; }
        }

        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        public string StaffRole
        {
            get { return _StaffRole; }
            set { _StaffRole = value; }
        }

        public byte[] Signature
        {
            get { return _Signature; }
            set { _Signature = value; }
        }


        /// <summary>
        /// Flag to denote if the user is mobile enabled. Used in conjunction with the property
        /// MobileUserId to determin insert, update or enabled status.
        /// </summary>
        public bool IsMobileUser
        {
            get { return _IsMobileUser; }
            set { _IsMobileUser = value; }
        }

        /// <summary>
        /// The mobile user Id value is the Id of the user record in the database the
        /// staff member is associated.
        /// This value is used for associated mobile logins
        /// and ensures that no 2 users will have the same user Id.
        /// This will aid investigation and logging of behaviour.
        /// </summary>
        public int MobileUserId
        {
            get { return _MobileUserId; }
            set { _MobileUserId = value; }
        }

        /// <summary>
        /// The password to be set for the Mobile user.
        /// This will never be returned from the database for a staff record
        /// </summary>
        public string Password
        {
            get { return _Password; }
            set
            {
                if (value.ToLower() != "password")
                    _Password = value;
            }
        }

        #endregion

        #region Methods
        /// <summary>
        /// Method to generate a user record that is preconfigured with the required values for a
        /// </summary>
        /// <param name="companyId">Int: The Id of the company the user belongs to</param>
        /// <returns>A Master Database User object for the staff member; used for mobile authentication</returns>
        public MobileUser ToMobileUser(int companyId)
        {
            MobileUser retVal = new MobileUser();
            retVal.Id = this.MobileUserId;
            retVal.CompanyID = companyId;
            retVal.EmailAddress = this.Email;
            if (this.Password.Length > 0)
            {
                if (MobileUser.PassesRegex(this.Password))
                    retVal.PasswordHashed = retVal.HashPassword(this.Password, true);
                else
                    throw new Exception("Invalid Password");
            }

            return retVal;
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
