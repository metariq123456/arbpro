﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DGS.ArbPro.Types
{
    public class RiskReferral : BusinessBase
    {
        #region Constructors
        public RiskReferral() : base() { }
        #endregion

        #region Members
        private int _RiskAssessmentId = -1;
        private DateTime _ReferralDate;
        private Boolean _TeamLeaderSpokenWith;
        private String _ReferralComments;
        private String _AdditionalActions;
        #endregion
        
        #region Properties
        public int RiskAssessmentID
        {
            get { return _RiskAssessmentId; }
            set { _RiskAssessmentId = value; }
        }

        public DateTime ReferralDate
        {
            get { return _ReferralDate; }
            set { _ReferralDate = value; }
        }

        public Boolean TeamLeaderSpokenWith
        {
            get { return _TeamLeaderSpokenWith; }
            set { _TeamLeaderSpokenWith = value; }
        }

        public String ReferralComments
        {
            get { return _ReferralComments; }
            set { _ReferralComments = value; }
        }

        public String AdditionalActions
        {
            get { return _AdditionalActions; }
            set { _AdditionalActions = value; }
        }
        #endregion
        
        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
