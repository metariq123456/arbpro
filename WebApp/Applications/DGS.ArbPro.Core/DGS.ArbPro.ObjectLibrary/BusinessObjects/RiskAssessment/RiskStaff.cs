﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class RiskStaff : BusinessBase
    {
        #region Members
        int _AssessmentId = -1;
        int _StaffId = -1;
        string _StaffName = "";
        string _Task = "";
        string _Comments = "";
        #endregion

        #region Constructors
        public RiskStaff():base(){}
        #endregion

        #region Properties
        public int AssessmentId
        {
            get { return _AssessmentId; }
            set {_AssessmentId = value;}
        }

        public int StaffId
        {
            get { return _StaffId; }
            set { _StaffId = value; }
        }

        public string StaffName
        {
            get { return _StaffName; }
            set { _StaffName = value; }
        }

        public string Task
        {
            get { return _Task; }
            set { _Task = value; }
        }

        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        #endregion

        #region Methods
        public override string ToString()
        {
           return this.GetObjectState(this);
        }
        #endregion
    }
}
