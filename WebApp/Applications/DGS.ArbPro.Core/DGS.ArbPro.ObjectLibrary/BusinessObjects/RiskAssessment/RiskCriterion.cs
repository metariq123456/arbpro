﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DGS.ArbPro.Types
{
    public class RiskCriterion: BusinessBase
    {
        #region Members
	    private int _RiskSectionId = -1;
	    private string _Criterion = "";
	    private string _ChapterRef = "";
	    private int _AssessmentPhase = -1;
	    private Enums.RiskCriterionStatusEnum _ControlRequired = Enums.RiskCriterionStatusEnum.Unknown;
        private Enums.RiskCriterionStatusEnum _OnDayControlRequired = Enums.RiskCriterionStatusEnum.Unknown;
        #endregion

        #region Constructors
        public RiskCriterion() : base() { }
        #endregion

        #region Properties
        public int RiskSectionId
        {
            get { return _RiskSectionId; }
            set { _RiskSectionId = value; }
        }

        public string Criterion
        {
            get { return _Criterion; }
            set { _Criterion = value; }
        }

        public string ChapterRef
        {
            get { return _ChapterRef; }
            set { _ChapterRef = value; }
        }

        public int AssessmentPhase
        {
            get { return _AssessmentPhase; }
            set { _AssessmentPhase = value; }
        }

        public Enums.RiskCriterionStatusEnum ControlRequired
        {
            get { return _ControlRequired; }
            set { _ControlRequired = value; }
        }

        public string ControlRequiredString
        {
            get { return _ControlRequired.ToString(); }
        }

        public Enums.RiskCriterionStatusEnum OnDayControlRequired
        {
            get { return _OnDayControlRequired; }
            set { _OnDayControlRequired = value; }
        }

        public string OnDayControlRequiredString
        {
            get { return _OnDayControlRequired.ToString(); }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
