﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DGS.ArbPro.Types
{
    public class Risk : BusinessBase
    {
        #region Members
        private int _ClientId = 0;//NOT NULL,
        private string _ClientName = "";
        private string _ClientTelephone = "";
        private string _ClientEmail = "";
        private string _ClientAddress = "";

        private int _QuoteId = 0;
        private int _AppointmentId = 0;
        private int _JobSiteId = 0;//NOT NULL,
        private int _AssessingStaffId = 0;//NOT NULL,
        private int _AerialPlanId = 0;//NOT NULL,
        private int _TeamLeaderStaffId = 0;//NOT NULL,
        private int _AssignedAerialStaffId = 0;//NOT NULL,
        private bool _CS38Certifed = false;
        private DateTime _AssessmentDate = DateTime.MinValue;
        private DateTime _WorkDate = DateTime.MinValue;
        private DateTime _CompletionDate = DateTime.MinValue;
        private DateTime _ReviewDate = DateTime.MinValue;
        private string _NearestHospital = "";
        private string _AerialPlan = "";
        private string _ReferenceDoc = "";
        private string _MobileSignal = "";
        private string _SiteAccess = "";
        private string _GridReference = "";
        private byte[] _AssessorSignatureImage = null;
        private bool _SatisfactoryCompletion = false;
        private bool _ReferralRequired = false;
        private bool _ReferralCompleted = false;
        private string _ReferralComments = "";
        private string _ManagementComments = "";
        private int _AuditId = 0;//NOT NULL,
        private bool _Completed = false;

        private string _RescueNomaniee = "Not Set";
        private string _RescuePlan = "Not Set";

        private List<string> _StaffPresent = null;
        private List<RiskAdditionalItem> _AdditionRisks = null;
        private List<RiskSection> _Sections = null;

        private string _LastUpdatedBy = "";
        private DateTime _LastUpdatedDate = DateTime.MinValue;
        private DateTime _ReferralDate = DateTime.MinValue;
        private string _AdditionalActions = "Not implemented yet";
        #endregion

        #region Constructors
        public Risk() : base() { }

        #endregion

        #region Properties
        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }

        public int ClientId
        {
            get { return _ClientId; }
            set { _ClientId = value; }
        }

        public int AppointmentId
        {
            get { return _AppointmentId; }
            set { _AppointmentId = value; }
        }

        public int JobSiteId
        {
            get { return _JobSiteId; }
            set { _JobSiteId = value; }
        }

        public int AssessingStaffId
        {
            get { return _AssessingStaffId; }
            set { _AssessingStaffId = value; }
        }

        public int AerialPlanId
        {
            get { return _AerialPlanId; }
            set { _AerialPlanId = value; }
        }

        public string AerialPlan
        {
            get { return _AerialPlan; }
            set { _AerialPlan = value; }
        }

        public int TeamLeaderStaffId
        {
            get { return _TeamLeaderStaffId; }
            set { _TeamLeaderStaffId = value; }
        }

        public int AssignedAerialStaffId
        {
            get { return _AssignedAerialStaffId; }
            set { _AssignedAerialStaffId = value; }
        }

        public bool CS38Certifed
        {
            get { return _CS38Certifed; }
            set { _CS38Certifed = value; }
        }

        public DateTime AssessmentDate
        {
            get { return _AssessmentDate; }
            set { _AssessmentDate = value; }
        }

        public DateTime WorkDate
        {
            get { return _WorkDate; }
            set { _WorkDate = value; }
        }

        public DateTime CompletionDate
        {
            get { return _CompletionDate; }
            set { _CompletionDate = value; }
        }

        public DateTime ReviewDate
        {
            get { return _ReviewDate; }
            set { _ReviewDate = value; }
        }

        public string NearestHospital
        {
            get { return _NearestHospital; }
            set { _NearestHospital = value; }
        }

        public string ReferenceDoc
        {
            get { return _ReferenceDoc; }
            set { _ReferenceDoc = value; }
        }

        public string MobileSignal
        {
            get { return _MobileSignal; }
            set { _MobileSignal = value; }
        }

        public string SiteAccess
        {
            get { return _SiteAccess; }
            set { _SiteAccess = value; }
        }

        public string GridReference
        {
            get { return _GridReference; }
            set { _GridReference = value; }
        }

        public byte[] AssessorSignatureImage
        {
            get { return _AssessorSignatureImage; }
            set { _AssessorSignatureImage = value; }
        }

        public bool SatisfactoryCompletion
        {
            get { return _SatisfactoryCompletion; }
            set { _SatisfactoryCompletion = value; }
        }

        public bool ReferralRequired
        {
            get { return _ReferralRequired; }
            set { _ReferralRequired = value; }
        }

        public bool ReferralCompleted
        {
            get { return _ReferralCompleted; }
            set { _ReferralCompleted = value; }
        }

        public string ReferralComments
        {
            get { return _ReferralComments; }
            set { _ReferralComments = value; }
        }

        public string ManagementComments
        {
            get { return _ManagementComments; }
            set { _ManagementComments = value; }
        }

        public int AuditId
        {
            get { return _AuditId; }
            set { _AuditId = value; }
        }

        public bool Completed
        {
            get { return _Completed; }
            set { _Completed = value; }
        }

        //Display properties

        public string RescueNomaniee
        {
            get { return _RescueNomaniee; }
            set { _RescueNomaniee = value; }
        }

        public string RescuePlan
        {
            get { return _RescuePlan; }
            set { _RescuePlan = value; }
        }

        public List<RiskAdditionalItem> AdditionalRisks
        {
            get
            {
                if (_AdditionRisks == null)
                    _AdditionRisks = new List<RiskAdditionalItem>();
                return _AdditionRisks;
            }
            set { _AdditionRisks = value; }
        }

        public List<RiskSection> AssessmentSections
        {
            get
            {
                if (_Sections == null)
                    _Sections = new List<RiskSection>();
                return _Sections;
            }
            set { _Sections = value; }
        }

        public List<string> StaffPresent
        {
            get
            {
                if (_StaffPresent == null)
                    _StaffPresent = new List<string>();

                return _StaffPresent;
            }
            set { _StaffPresent = value; }
        }

        //Just added
        public string LastUpdatedBy
        {
            get { return _LastUpdatedBy; }
            set { _LastUpdatedBy = value; }
        }

        public DateTime LastUpdatedDate
        {
            get { return _LastUpdatedDate; }
            set { _LastUpdatedDate = value; }
        }

        public DateTime ReferralDate
        {
            get { return _ReferralDate; }
            set { _ReferralDate = value; }
        }

        public string AdditionalActions
        {
            get { return _AdditionalActions; }
            set { _AdditionalActions = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}

