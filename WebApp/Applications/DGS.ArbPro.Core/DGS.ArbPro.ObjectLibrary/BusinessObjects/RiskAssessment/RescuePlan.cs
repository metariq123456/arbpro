﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DGS.ArbPro.Types
{
    public class RescuePlan : BusinessBase
    {
        #region Members
        string _Details = "";
        #endregion

        #region
        public string Details
        {
            get { return _Details; }
            set { _Details = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
