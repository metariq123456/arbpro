﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class RiskAdditionalItem : BusinessBase
    {
        #region Members
	    private int _RiskAssessmentId = -1;
	    private string _AssessedRisk = "";
        private string _ControlRequired = "";
        #endregion

        #region Constructors
        public RiskAdditionalItem() : base() { }

        #endregion

        #region Properties
        public int RiskAssessmentId
        {
            get { return _RiskAssessmentId; }
            set { _RiskAssessmentId = value; }
        }

        public string AssessedRisk
        {
            get { return _AssessedRisk; }
            set { _AssessedRisk = value; }
        }

        public string ControlRequired
        {
            get { return _ControlRequired; }
            set { _ControlRequired = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
