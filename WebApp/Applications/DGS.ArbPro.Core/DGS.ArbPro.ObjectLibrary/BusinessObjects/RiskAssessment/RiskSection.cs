﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DGS.ArbPro.Types
{
    public class RiskSection : BusinessBase
    {
        #region Members
	    private int _RiskAssessmentId = -1;
	    private string _Title = "";
        private string _Description = "";
        private string _ReferenceDoc = "";
        private string _Chapter = "";
        private string _Comments = "";

        private List<RiskCriterion> _SectionCriteria = null;
        #endregion

        #region Constructors
        public RiskSection() : base() { }
        #endregion

        #region Properties
        public int RiskAssessmentId
        {
            get { return _RiskAssessmentId; }
            set { _RiskAssessmentId = value; }
        }

        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public string ReferenceDoc
        {
            get { return _ReferenceDoc; }
            set { _ReferenceDoc = value; }
        }

        public string Chapter
        {
            get { return _Chapter; }
            set { _Chapter = value; }
        }

        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        public List<RiskCriterion> Criteria
        {
            get { return _SectionCriteria; }
            set { _SectionCriteria = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
