﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public enum CalendarItemType
    {
        /// <summary>
        /// An appointment for a quote.
        /// </summary>
        Quote,

        /// <summary>
        /// An appointment for work.
        /// </summary>
        Work,

        /// <summary>
        /// A generic appointment
        /// </summary>
        Generic,

        /// <summary>
        /// A temporary appointment (used for mobile appointment creation)
        /// </summary>
        Pending
    }
    public class CalendarItem
    {
        #region Properties
        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// Gets or sets the end time.
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Gets or sets the item type.
        /// </summary>
        public CalendarItemType ItemType { get; set; }

        /// <summary>
        /// Gets or sets the Summary - for display in the calendar view.
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// Gets or sets the description of the appointment, for display in a detailed view.
        /// </summary>
        public string Description { get; set; }

        public bool AllDay { get; set; }

        public int QuoteId { get; set; }

        public int WorkId { get; set; }

        public int JobId { get; set; }

        public int JobSiteId { get; set; }

        #endregion Properties
    }
}
