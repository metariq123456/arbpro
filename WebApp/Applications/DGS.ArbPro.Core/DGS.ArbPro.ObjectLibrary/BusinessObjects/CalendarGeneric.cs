﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class CalendarGeneric : CalendarBase
    {
        #region Members
        private Boolean _AllDay = false;
        private String _Name = "";
        private String _Comment = "";
        private CalendarGenericType _Type = CalendarGenericType.Other;
        private Int32 _StaffID = -1;
        #endregion

        #region Constructors
        public CalendarGeneric() : base() { }
        #endregion

        #region Properties
        public Boolean AllDay
        {
            get { return _AllDay; }
            set { _AllDay = value; }
        }

        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public String Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }

        public CalendarGenericType Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        public Int32 StaffID
        {
            get { return _StaffID; }
            set { _StaffID = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
