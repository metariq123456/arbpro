﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class PermitsNecessary : BusinessBase
    {
        #region Members
        int _QuoteId = -1;
        string _QuoteReference = "";
        int _PermitNecessary = 0;
        int _PermitObtained = 0;
        string _PermitType = "";
        string _PermitComments = "";
        #endregion

        #region Constructors
        public PermitsNecessary() : base() { }
        #endregion

        #region Properties
        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }

        public string QuoteReference
        {
            get { return _QuoteReference; }
            set { _QuoteReference = value; }
        }

        public int PermitNecessary
        {
            get { return _PermitNecessary; }
            set { _PermitNecessary = value; }
        }

        public int PermitObtained
        {
            get { return _PermitObtained; }
            set { _PermitObtained = value; }
        }

        public string PermitType
        {
            get { return _PermitType; }
            set { _PermitType = value; }
        }

        public string PermitComments
        {
            get { return _PermitComments; }
            set { _PermitComments = value; }
        }
        #endregion
        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
