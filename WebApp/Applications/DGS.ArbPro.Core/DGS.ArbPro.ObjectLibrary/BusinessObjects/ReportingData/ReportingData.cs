﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class ReportingData : BusinessBase
    {
        #region Members

        private string name = "";

        #endregion

        #region Constructors

        public ReportingData() : base()
        {
            this.PercentageAmount = 0;
            this.Amount = 0;
            this.Value = 0;
        }

        #endregion

        #region Properties
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public int Amount { get; set; }

        public decimal Value { get; set; }

        public double PercentageAmount { get; set; }

        #endregion

        #region Methods

        public override string ToString()
        {
            return base.GetObjectState(this);
        }

        #endregion
    }
}
