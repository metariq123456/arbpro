﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.Types
{
    public class QuoteItemPicture
    {
        public string Guid { get; set; }
        public string QuoteTreeNo { get; set; }
        public string QuoteTree { get; set; }
        public string Description { get; set; }
        public string ImageFilePath { get; set; }
        public Byte[] ImageData { get; set; }
    }
}

