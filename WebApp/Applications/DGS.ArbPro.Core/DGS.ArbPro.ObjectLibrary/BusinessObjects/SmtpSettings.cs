﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class SmtpSettings : BusinessBase
    {
        #region Members
        private string _HostAddress = "";
        private string _Port = "";
        private int _UserId = -1;
        private string _Username = "";
        private string _Password = "";
        private bool _EnableSSL = true;
        private bool _UseSmtp = false;
        #endregion
        
        #region Constructors
        public SmtpSettings() : base() { }
        #endregion


        #region Properties
        public string HostAddress
        {
            get { return _HostAddress; }
            set { _HostAddress = value; }
        }

        public string Port
        {
            get { return _Port; }
            set { _Port = value; }
        }


        public int UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }


        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }


        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        public bool EnableSSL
        {
            get { return _EnableSSL; }
            set { _EnableSSL = value; }
        }

        public bool UseSmtp
        {
            get { return _UseSmtp; }
            set { _UseSmtp = value; }
        }

        #endregion

        #region Methods

        public override string ToString()
        {
            return base.GetObjectState(this);
        }

        #endregion
    }
}
