﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class EmailSettings
    {
        string _ToAddress = "";
        string _FromAddress = "";
        string _Subject = "";
        Dictionary<string, string> _AttachementDetails = null;
    }
}
