﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class DocumentReference: BusinessBase
    {
        #region Members
        string _Prefix = "";
        int _Reference = -1;
        string _Suffix = "";
        #endregion

        #region Constructors
        public DocumentReference() { }
        #endregion

        #region Properties
        public string Prefix
        {
            get { return _Prefix; }
            set { _Prefix = value; }
        }

        public int Reference
        {
            get { return _Reference; }
            set { _Reference = value; }
        }

        public string Suffix
        {
            get { return _Suffix; }
            set { _Suffix = value; }
        
        }

        public string AsReferenceString
        {
            get { return this.ToString(); }
        }
        #endregion

        #region Methods
        #region Oversides
        public override string ToString()
        {
            StringBuilder retVal = new StringBuilder();
            if (this.Prefix.Length > 0)
                retVal.AppendFormat("{0}-", this.Prefix);

            retVal.Append(this.Reference);

            if (this.Suffix.Length > 0)
                retVal.AppendFormat("-{0}", this.Suffix);

            return retVal.ToString();
        }
        #endregion
        #endregion
    }
}
