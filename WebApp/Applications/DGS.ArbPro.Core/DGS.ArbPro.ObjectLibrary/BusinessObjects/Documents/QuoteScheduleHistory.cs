﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class QuoteScheduleHistory : DocumentBase
    {
        #region Method


        #endregion

        #region Constructors
        public QuoteScheduleHistory()
        {
            _Quotes = new List<Quote>();
        }
        #endregion

        #region Properties
        public Client ClientDetails
        {
            get { return _Client; }
            set { _Client = value; }
        }

        /// <summary>
        /// A list of all Quote records to be included on the quote schedule
        /// The quote object includes the client site object that it is located at. 
        /// </summary>
        public List<Quote> QuoteRecords
        {
            get { return _Quotes; }
            set { _Quotes = value; }
        }
        #endregion


    }
}
