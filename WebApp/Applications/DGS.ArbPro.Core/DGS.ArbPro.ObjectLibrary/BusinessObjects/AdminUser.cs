﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class AdminUser : UserBase
    {
        #region Members
        string _Company = "";
        string _Username = "";
        #endregion

        #region Constructors
        public AdminUser() : base() { }
        #endregion

        #region Properties
        public string Company
        {
            get{return _Company;}
            set {_Company = value;}
        }

        public string Username
        {
            get { return _Username; }
            set { _Username = value; }
        }
        #endregion

        #region Methods
        public void LockUser()
        {
            this.AuthenticationFailures = MaxAuthFailures;
        }


        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion

    }
}
