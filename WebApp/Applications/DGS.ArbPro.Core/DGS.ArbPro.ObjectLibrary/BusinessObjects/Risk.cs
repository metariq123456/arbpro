﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace DGS.ArbPro.Types
{
    public class Risk : BusinessBase
    {
        #region Members
        #endregion

        #region Constructors
        public Risk() : base() { }

        #endregion

        #region Properties
        /* Data base fields / class properties:
            These fields are set by using reflection in some methods.
            The names are representative of the declaired return names within the stored procedure
        */
        public int RiskIDNo {get; set;}
	    public string RiskCompanyName {get; set;}
	    public int QuoteValueNo {get; set;}
	    public int ClientIDNo {get; set;}
	    public DateTime RiskDateQuote {get; set;}
	    public DateTime RiskDateWork {get; set;}
	    public string RiskSiteAddress {get; set;}
	    public string RiskEmergencyProceduresGenericRisker {get; set;}
	    public string RiskEmergencyNearestAandE {get; set;}
	    public string RiskEmergencyMobilePhoneSignal {get; set;}
	    public string RiskEmergencySiteAccessForRescue {get; set;}
	    public string RiskEmergencyRescuePlan {get; set;}
	    public string RiskEmergencyArialRescuePlan {get; set;}
	    public bool RiskEmergencyAdequateFirstAidProvisions {get; set;}
        public string RiskSiteGenericRisker { get; set; }
	    public bool RiskSiteToPublic {get; set;}
	    public bool RiskSiteTrafficManagement {get; set;}
	    public bool RiskSiteOverheadLinesUndergroundServices {get; set;}
	    public bool RiskSiteBuildingOrObstructions {get; set;}
	    public bool RiskSiteSensitiveEmotionallyCharged {get; set;}
	    public string RiskSiteControlMeasuresNecessary {get; set;}
	    public string RiskEnvironmentalGenreicRisker {get; set;}
	    public bool RiskEnvironmentalWaterCourse {get; set;}
	    public bool RiskEnvironmentalProtectedSpeciesHabitat {get; set;}
	    public bool RiskEnvironmentalNoiseDust {get; set;}
	    public string RiskEnvironmentalControlMeasuresNecessary {get; set;}
        public string RiskOperationsGenericRisker { get; set; }
	    public bool RiskOperationsChainSaw {get; set;}
	    public bool RiskOperationsTreeClimbing {get; set;}
	    public bool RiskOperationsChainsawInTree {get; set;}
	    public bool RiskOperationsBranchRemoval {get; set;}
	    public bool RiskOperationsLoweringTechniquesNecessary {get; set;}
	    public bool RiskOperationsTreeFelling {get; set;}
	    public bool RiskOperationsDeLimbingAndCrossCuting {get; set;}
	    public bool RiskOperationsStormDamage {get; set;}
	    public bool RiskOperationsChemicalApplication {get; set;}
	    public string RiskOperationsControlMeasuresNecessary {get; set;}
        public string RiskMachineryGenericRisker { get; set; }
	    public bool RiskMachineryBrushWoodChipper {get; set;}
	    public bool RiskMachineryStumpGrinder {get; set;}
	    public bool RiskMachineryWinch {get; set;}
	    public bool RiskMachineryBrushCutterClearingSaw {get; set;}
	    public bool RiskMachineryHedgeTrimmers {get; set;}
	    public bool RiskMachineryMechanisedPolePrunner {get; set;}
	    public bool RiskMachineryMobileElevatedWorkPlatform {get; set;}
	    public bool RiskMachineryCrane {get; set;}
	    public bool RiskMachineryMountedHydraulicLoadingArm {get; set;}
	    public bool RiskMachineryOther {get; set;}
	    public string RiskMachinerControlMeasuresNecessary {get; set;}
	    public string RiskAssessmentCompletedBy {get; set;}
	    public string RiskAssessmentSignedByNamedPerson {get; set;}
	    public string RiskAssessmentSignedByOtherStaffPressent {get; set;}
	    public string RiskOnSiteTrainingInspection {get; set;}
	    public string RiskOnSiteTrainingInspectionDetails {get; set;}
	    public string RiskOnSiteTrainingInspectionAction {get; set;}
	    public string RiskOnSiteTrainingInspectionSignAndDate {get; set;}
	    public string RiskWorkingAtHeightGeneric {get; set;}
	    public bool RiskWorkingAtHeightMewp {get; set;}
	    public bool RiskWorkingAtHeightIsItCostEffective {get; set;}
	    public bool RiskWorkingAtHeightLadder {get; set;}
	    public bool RiskWorkingAtHeightFallingObjects {get; set;}
	    public bool RiskOperationsOther {get; set;}
	    public bool RiskWorkingAtHeightRopeandHarness {get; set;}
	    public string RiskWorkingAtHeightControlMeasures {get; set;}
	    public bool RiskWorkingAtHeightCompletedFromGround {get; set;}
	    public bool RiskWorkingAtHeightSafeToClimb {get; set;}
	    public bool RiskWorkingAtHeightWeatherFavourable {get; set;}
	    public bool RiskWorkingAtHeightAerialRescueArranged {get; set;}
	    public bool RiskWorkingAtHeightStaffHoldQualifications {get; set;}
	    public bool RiskWorkingAtHeightUsePoleSaw {get; set;}
	    public bool RiskWorkingAtHeightStraightFell {get; set;}
	    public bool RiskOpperationsOther {get; set;}
	    public bool CS38 {get; set;}
	    public string RiskEmergencyAdequateFirstAidProvisionsRef {get; set;}
	    public string RiskSiteToPublicRef {get; set;}
	    public string RiskSiteTrafficManagementRef {get; set;}
	    public string RiskSiteOverheadLinesUndergroundServicesRef {get; set;}
	    public string RiskSiteBuildingOrObstructionsRef {get; set;}
        public string RiskSiteSensitiveEmotionallyChargedRef { get; set; }
        public string RiskSiteOtherRef { get; set; }
	    public string RiskEnvironmentalWaterCourseRef {get; set;}
	    public string RiskEnvironmentalProtectedSpeciesHabitatRef {get; set;}
	    public string RiskEnvironmentalNoiseDustRef {get; set;}
	    public string RiskOperationsChainSawRef {get; set;}
	    public string RiskOperationsTreeClimbingRef {get; set;}
	    public string RiskOperationsChainsawInTreeRef {get; set;}
	    public string RiskOperationsBranchRemovalRef {get; set;}
	    public string RiskOperationsLoweringTechniquesNecessaryRef {get; set;}
	    public string RiskOperationsTreeFellingRef {get; set;}
	    public string RiskOperationsDeLimbingAndCrossCutingRef {get; set;}
	    public string RiskOperationsStormDamageRef {get; set;}
	    public string RiskOperationsChemicalApplicationRef {get; set;}
	    public string RiskMachineryBrushWoodChipperRef {get; set;}
	    public string RiskMachineryStumpGrinderRef {get; set;}
	    public string RiskMachineryWinchRef {get; set;}
	    public string RiskMachineryBrushCutterClearingSawRef {get; set;}
	    public string RiskMachineryHedgeTrimmersRef {get; set;}
	    public string RiskMachineryMechanisedPolePrunnerRef {get; set;}
	    public string RiskMachineryMobileElevatedWorkPlatformRef {get; set;}
	    public string RiskMachineryCraneRef {get; set;}
	    public string RiskMachineryMountedHydraulicLoadingArmRef {get; set;}
	    public string RiskMachineryOtherRef {get; set;}
	    public string RiskWorkingAtHeightMewpRef {get; set;}
	    public string RiskWorkingAtHeightIsItCostEffectiveRef {get; set;}
	    public string RiskWorkingAtHeightLadderRef {get; set;}
	    public string RiskWorkingAtHeightFallingObjectsRef {get; set;}
	    public string RiskOperationsOtherRef {get; set;}
	    public string RiskWorkingAtHeightRopeandHarnessRef {get; set;}
	    public string RiskWorkingAtHeightCompletedFromGroundRef {get; set;}
	    public string RiskWorkingAtHeightSafeToClimbRef {get; set;}
	    public string RiskWorkingAtHeightWeatherFavourableRef {get; set;}
	    public string RiskWorkingAtHeightAerialRescueArrangedRef {get; set;}
	    public string RiskWorkingAtHeightStaffHoldQualificationsRef {get; set;}
	    public string RiskWorkingAtHeightUsePoleSawRef {get; set;}
	    public string RiskWorkingAtHeightStraightFellRef {get; set;}
	    public bool RiskSiteRoadsFootpathsPublicAreas {get; set;}
	    public string RiskSiteRoadsFootpathsPublicAreasRef {get; set;}
        public string GridReference { get; set; }
        public string RiskSiteReference { get; set; }
        public string RiskSiteForman { get; set; }
        public string RiskWorkDate { get; set; }
        public Byte[] Signature { get; set; }

        public List<RiskAdditionalItem> AdditionalRisks { get; set; }
        #endregion

        #region Methods
        #endregion
    }
}

