﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class ClientOrganisation : BusinessBase
    {
        #region Members
        private const string PasswordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).{8,100}$";

        string _OrganisationName = "";
        string _Address1 = "";
        string _Address2 = "";
        string _Town = "";
        string _CountyOrState = "";
        string _PostcodeOrZip = "";
        int _CountryID = 230; //Defualted to UK
        bool _UserAccountsEnabled = false;
        bool _MobileEditionEnabled = false;
        int _NumLicenses = 0;
        float _Value = 0;
        #endregion

        #region Constructors
        public ClientOrganisation() : base() { }
        #endregion

        #region Properties
        public string OrganisationName
        {
            get { return _OrganisationName; }
            set { _OrganisationName = value; }
        }
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }
        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }
        public string Town
        {
            get { return _Town; }
            set { _Town = value; }
        }
        public string CountyOrState
        {
            get { return _CountyOrState; }
            set { _CountyOrState = value; }
        }
        public string PostcodeOrZip
        {
            get { return _PostcodeOrZip; }
            set { _PostcodeOrZip = value; }
        }
        public int CountryID
        {
            get { return _CountryID; }
            set { _CountryID = value; }
        }
        public bool UserAccountsEnabled
        {
            get { return _UserAccountsEnabled; }
            set { _UserAccountsEnabled = value; }
        }
        public bool MobileEditionEnabled
        {
            get { return _MobileEditionEnabled; }
            set { _MobileEditionEnabled = value; }
        }
        public int NumLicenses
        {
            get { return _NumLicenses; }
            set { _NumLicenses = value; }
        }
        public float Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
