﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Templates
{
    public class Risk: BusinessBase
    {
        #region Members
        string _Name = "New Template";
        string _ReferenceDoc = "Not Set";
        string _Description = "";
        bool _IsDeleted = false;
        bool _IsEditable = true;
        Enums.RiskTemplateTypeEnum _TemplateType = Enums.RiskTemplateTypeEnum.General;
        List<RiskSection> _TemplateSections = null;
        #endregion

        #region Constructors
        public Risk():base() { }
        #endregion

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string ReferenceDoc
        {
            get { return _ReferenceDoc; }
            set { _ReferenceDoc = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public Enums.RiskTemplateTypeEnum TemplateType
        {
            get { return _TemplateType; }
            set { _TemplateType = value; }
        }

        public List<RiskSection> TemplateSections
        {
            get
            {
                if (_TemplateSections == null)
                    _TemplateSections = new List<RiskSection>();

                return _TemplateSections;
            }
            set { _TemplateSections = value; }
        }

        public bool IsDeleted
        {
            get { return _IsDeleted; }
            set { _IsDeleted = value; }
        }

        public bool IsEditable
        {
            get { return _IsEditable; }
            set { _IsEditable = value; }
        }

        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
