﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Templates
{
    public class RiskCriterion : BusinessBase
    {
        private int assessmentPhase = 1;

	    public string Criterion {get;set;}
	    public string ChapterRef{get;set;}
        public bool IsDeleted { get; set; }

        public int AssessmentPhase
        {
            get { return this.assessmentPhase; }
            set { this.assessmentPhase = value; }
        }

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
