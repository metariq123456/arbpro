﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class SmsTextTemplate : BusinessBase
    {
        #region Members
        private string _TemplateName = "";
        private string _BodyText = "";
        private string _AdditionalText = "";
        private Enums.MessageTypeEnum _MessageType = Enums.MessageTypeEnum.Letter;
        private Enums.ReportTextTemplateTypeEnum _ObjectType = Enums.ReportTextTemplateTypeEnum.Quote;
        private string _Subject = "";
        private string _Salutation = "";
        #endregion

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public SmsTextTemplate() : base() { }

        /// <summary>
        /// Optional constructor to create an instance with a name and type'
        /// </summary>
        /// <param name="name">string: The name of the template</param>
        /// <param name="obj">The type of report the template can be used with </param>
        /// <param name="msg">The delivery method for the template</param>
        public SmsTextTemplate(string name, Enums.ReportTextTemplateTypeEnum obj, Enums.MessageTypeEnum msg)
            : base()
        {
            _TemplateName = name;
            _ObjectType = obj;
            _MessageType = msg;
        }
        #endregion

        #region Properties

        /// <summary>
        /// DGSAP-864 The salutation field in the invoice templates.
        /// </summary>
        public string Salutation
        {
            get { return _Salutation; }
            set { _Salutation = value; }
        }

        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; }
        }

        /// <summary>
        /// Additional text to be applied to the template.
        /// </summary>
        public string AdditionalText
        {
            get { return _AdditionalText; }
            set { _AdditionalText = value; }
        }

        /// <summary>
        /// The name of the template
        /// </summary>
        public string TemplateName
        {
            get { return _TemplateName; }
            set { _TemplateName = value; }
        }

        /// <summary>
        /// The type of report that the template can be used with.
        /// </summary>
        public Enums.ReportTextTemplateTypeEnum ObjectType
        {
            get { return _ObjectType; }
            set { _ObjectType = value; }
        }

        /// <summary>
        /// The delivery method to be associated with the template
        /// </summary>
        public Enums.MessageTypeEnum MessageType
        {
            get { return _MessageType; }
            set { _MessageType = value; }
        }

        /// <summary>
        /// The text body of the template
        /// </summary>
        public string BodyText
        {
            get { return _BodyText; }
            set { _BodyText = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion

    }
}
