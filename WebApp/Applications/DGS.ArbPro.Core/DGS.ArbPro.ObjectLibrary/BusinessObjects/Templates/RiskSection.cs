﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.Templates
{
    public class RiskSection: BusinessBase
    {
        #region Members
        string _Title = "";
        string _ReferenceDoc = "";
        string _ChapterRef = "";
        string _Description = "";
        bool _IsDeleted = false;
        List<RiskCriterion> _TemplateCriteria = null;
        #endregion

        #region Constructors
        public RiskSection() : base() { }

        public RiskSection(string referenceDoc)
            : base()
        {
            _ReferenceDoc = referenceDoc;
        }
        #endregion

        #region Properties
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        public string ReferenceDoc
        {
            get { return _ReferenceDoc; }
            set { _ReferenceDoc = value; }
        }

        public string ChapterRef
        {
            get { return _ChapterRef; }
            set { _ChapterRef = value; }
        }

        public bool IsDeleted
        {
            get { return _IsDeleted; }
            set { _IsDeleted = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        public List<RiskCriterion> TemplateCriteria
        {
            get
            {
                if (_TemplateCriteria == null)
                    _TemplateCriteria = new List<RiskCriterion>();

                return _TemplateCriteria;
            }
            set { _TemplateCriteria = value; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
