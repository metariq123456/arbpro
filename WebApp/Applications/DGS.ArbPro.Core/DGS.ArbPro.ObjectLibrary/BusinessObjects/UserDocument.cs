﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class UserDocument : BusinessBase
    {
        #region Members
        private int _ClientID;
        private int _UploadedByID;
        private string _Description;
        private byte[] _Bytes;
        private string _ContentType;
        private DateTime _UploadedTime;
        private string _Name;
        private string _Extension;
        private string _FileName;
        private Enums.UserDocumentTypeEnum _DocumentType;
        #endregion

        #region Constructors
        public UserDocument() : base() { }

        public UserDocument(string name, byte[] document) : base() 
        { }
        #endregion

        #region Properties
        public int ClientID
        {
            get { return _ClientID; }
            set { _ClientID = value; }
        }
        public int UploadedByID
        {
            get { return _UploadedByID; }
            set { _UploadedByID = value; }
        }
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        public byte[] Bytes
        {
            get { return _Bytes; }
            set { _Bytes = value; }
        }
        public string ContentType
        {
            get { return _ContentType; }
            set { _ContentType = value; }
        }
        public DateTime UploadedTime
        {
            get { return _UploadedTime; }
            set { _UploadedTime = value; }
        }
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public string Extension
        {
            get { return _Extension; }
            set { _Extension = value; }
        }
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
        public Enums.UserDocumentTypeEnum DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        public int DocumentTypeID
        {
            get { return (int)DocumentType; }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }

        /// <summary>
        /// Static helper method to create a User document object from a name and a memory stream
        /// </summary>
        /// <param name="name">String: The name to be attributed to the user document</param>
        /// <param name="report">MemoryStream: the report</param>
        /// <returns>Types.UserDocument object</returns>
        public static Types.UserDocument ToUserDocument(string name, MemoryStream report)
        {
            byte[] document = null;
            if(report != null)
                document = report.ToArray();

            Types.UserDocument retVal = new UserDocument();
            retVal.Name = name;
            retVal.Bytes = document;

            return retVal;
        }
        #endregion
    }
}
