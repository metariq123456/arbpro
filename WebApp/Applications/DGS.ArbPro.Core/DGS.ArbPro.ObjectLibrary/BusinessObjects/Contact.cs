﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class ClientContact : ContactBase
    {
        #region Members
        private int _ClientId = -1;
        private Enums.ContactTypeEnum _Type = Enums.ContactTypeEnum.General;
        #endregion

        #region Constructors
        public ClientContact() : base() { }

        #endregion

        #region Properties
        public int ClientId
        {
            get { return _ClientId; }
            set { _ClientId = value; }
        }

        public Enums.ContactTypeEnum ContactType
        {
            get { return _Type; }
            set { _Type = value; }
        }

        public string ContactTypeString
        {
            get { return _Type.ToString(); }
        }
        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
