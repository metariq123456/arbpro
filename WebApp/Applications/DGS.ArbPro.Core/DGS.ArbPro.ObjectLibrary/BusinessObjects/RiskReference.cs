﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.Types
{
    public class RiskReference : BusinessBase
    {
        #region Members
        #endregion

        #region Constructors
        public RiskReference() : base() { }

        #endregion

        #region Properties
        /// <summary>
        /// The name of the section of the risk assessment the assessment item belongs to
        /// </summary>
        public string AssessmentSection { get; set; }

        /// <summary>
        /// The name of the risk assessment item.
        /// </summary>
        /// <remarks>
        /// This should match the name of the column in the Risks table, and also the label IDs on the Risks.aspx page.
        /// </remarks>
        public string AssessmentItem { get; set; }

        /// <summary>
        /// The description displayed on the Risk Reference editing screeen.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The reference displayed on the Risk Assessment forms.
        /// </summary>
        public string Reference { get; set; }

        #endregion


        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion

    }
}
