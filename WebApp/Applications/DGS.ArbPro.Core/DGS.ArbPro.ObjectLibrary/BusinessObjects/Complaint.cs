﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class Complaint : BusinessBase
    {
        #region Members
        string _EnteredBy = "";
        string _AllocattedTo = "";
        string _SiteVisitedBy = "";
        string _ComplaintType = "";
        string _Title = "";
        string _Firstname = "";
        string _Surname = "";
        string _AddressLn1 = "";
        string _AddressLn2 = "";
        string _Town = "";
        string _County = "";
        string _PostCode = "";
        string _Telephone = "";
        string _Mobile = "";
        string _Email = "";
        string _Details = "";
        string _ActionTaken = "";
        string _AssociatedQuoteRef = "";
        bool _Closed = false;
        Enums.ContactMethodEnum _ContactMethod = Enums.ContactMethodEnum.Telephone;
        DateTime _DateOfComplaint = DateTime.MinValue;
        DateTime _DateEntered = DateTime.Today;
        DateTime _DateRectified = DateTime.MinValue;
        DateTime _DateClosed = DateTime.MinValue;
        #endregion

        #region Constructors
        public Complaint() : base() { }
        #endregion

        #region Properties
        /// <summary>
        /// The name of the member of staff that entered the complaint into the system
        /// </summary>
        public string EnteredBy
        {
            get { return _EnteredBy; }
            set { _EnteredBy = value; }
        }

        /// <summary>
        /// The catagory of the complaint
        /// </summary>
        public string ComplaintType
        {
            get { return _ComplaintType; }
            set { _ComplaintType = value; }
        }

        /// <summary>
        /// The name of the member of staff the complaint is allocated to
        /// </summary>
        public string AllocattedTo
        {
            get { return _AllocattedTo; }
            set { _AllocattedTo = value; }
        }

        /// <summary>
        /// The name of the member of staff that performed any type of site visit
        /// </summary>
        public string SiteVisitedBy
        {
            get { return _SiteVisitedBy; }
            set { _SiteVisitedBy = value; }
        }

        /// <summary>
        /// The complainants title
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        /// <summary>
        /// The complainants first name
        /// </summary>
        public string Firstname
        {
            get { return _Firstname; }
            set { _Firstname = value; }
        }

        /// <summary>
        /// The complainants surname
        /// </summary>
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        /// <summary>
        /// The first address line of the complainant
        /// </summary>
        public string AddressLn1
        {
            get { return _AddressLn1; }
            set { _AddressLn1 = value; }
        }

        /// <summary>
        /// The second address line of the complainant
        /// </summary>
        public string AddressLn2
        {
            get { return _AddressLn2; }
            set { _AddressLn2 = value; }
        }

        /// <summary>
        /// The town of the complainant
        /// </summary>
        public string Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        /// <summary>
        /// The county of the complainant
        /// </summary>
        public string County
        {
            get { return _County; }
            set { _County = value; }
        }

        /// <summary>
        /// The postcode of the complainant
        /// </summary>
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        /// <summary>
        /// The telephone number of the complainant
        /// </summary>
        public string Telephone
        {
            get { return _Telephone; }
            set { _Telephone = value; }
        }

        /// <summary>
        /// The mobile phone number of the complainant
        /// </summary>
        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }

        /// <summary>
        /// The email address of the complainant
        /// </summary>
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        /// <summary>
        /// The details ofg the complaint
        /// </summary>
        public string Details
        {
            get { return _Details; }
            set { _Details = value; }
        }

        /// <summary>
        /// The action taken to resolve the complaint
        /// </summary>
        public string ActionTaken
        {
            get { return _ActionTaken; }
            set { _ActionTaken = value; }
        }

        /// <summary>
        /// The quote reference of any associated quote
        /// </summary>
        public string AssociatedQuoteRef
        {
            get { return _AssociatedQuoteRef; }
            set { _AssociatedQuoteRef = value; }
        }

        /// <summary>
        /// Flag to denote if the complaint is closed
        /// </summary>
        public bool Closed
        {
            get { return _Closed; }
            set { _Closed = value; }
        }

        /// <summary>
        /// The complainants prefered method of contact
        /// </summary>
        public Enums.ContactMethodEnum ContactMethod
        {
            get { return _ContactMethod; }
            set { _ContactMethod = value; }
        }

        /// <summary>
        /// The date the complaint was received
        /// </summary>
        public DateTime DateOfComplaint
        {
            get { return _DateOfComplaint; }
            set { _DateOfComplaint = value; }
        }

        /// <summary>
        /// The date the complaint was entered into the system
        /// </summary>
        public DateTime DateEntered
        {
            get { return _DateEntered; }
            set { _DateEntered = value; }
        }

        /// <summary>
        /// The date that action was taken to resolve the complaint
        /// </summary>
        public DateTime DateRectified
        {
            get { return _DateRectified; }
            set { _DateRectified = value; }
        }

        /// <summary>
        /// The date that the complaint was marked as closed to be used for audit processes
        /// </summary>
        public DateTime DateClosed
        {
            get { return _DateClosed; }
            set { _DateClosed = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion

        
    }
}
