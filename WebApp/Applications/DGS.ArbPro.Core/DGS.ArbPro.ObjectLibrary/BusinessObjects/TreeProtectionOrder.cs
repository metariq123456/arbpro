﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class TreeProtectionOrder : BusinessBase
    {
        #region Members
        int _QuoteId = -1;
        int _JobSiteId = -1;
        string _QuoteReference = "";
		Enums.TreeProtectionStatusEnum _Status = Enums.TreeProtectionStatusEnum.Unknown;
		Enums.TreeProtectionProgreesEnum _Progress = Enums.TreeProtectionProgreesEnum.No_Application;
        Enums.TreeProtectionOrderTypeEnum _orderTypeEnum = TreeProtectionOrderTypeEnum.TPO;
        string _LocalAuthorityName = "";
        string _CouncilReference = "Not Set";
        string _InternalReference = "Not Set";
        string _AppealReference = "N/A";
		DateTime _ApplicationDate = DateTime.MinValue;
        DateTime _RegistrationDate = DateTime.MinValue;
		DateTime _InitialDecisionDate = DateTime.MinValue;
		DateTime _AppealDate = DateTime.MinValue;
		DateTime _AppealDecisionDate = DateTime.MinValue;
		DateTime _SixWeekStatutory = DateTime.MinValue;
        DateTime _EightWeekStatutory = DateTime.MinValue;
		string _Notes = "";
        string _TpoOfficer = "";
        string _ContactNumber = "Not Available";
		string _OrderType = "";
		string _OrderNumber = "";
		bool _ConservationArea = false;
		bool _Conformationreceived = false;
        #endregion

        #region Constructors
        public TreeProtectionOrder() : base() { }

        #endregion

        #region Properties
        
        /// <summary>
        /// The Quote record Id of the associated Quote
        /// </summary>
        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }

        /// <summary>
        /// The JobSite record Id of the associated Quote
        /// </summary>
        public int JobSiteId
        {
            get { return _JobSiteId; }
            set { _JobSiteId = value; }
        }

        /// <summary>
        /// The reference number of the associated quote
        /// </summary>
        public string QuoteReference
        {
            get { return _QuoteReference; }
            set { _QuoteReference = value; }
        }

        /// <summary>
        /// The status of the Existance of a TPO
        /// </summary>
        public Enums.TreeProtectionStatusEnum Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        /// <summary>
        /// The current progress stage of the TPO
        /// </summary>
        public Enums.TreeProtectionProgreesEnum ProgressStage
        {
            get { return _Progress; }
            set { _Progress = value; }
        }

        /// <summary>
        /// The local authority to which the TPO must be submitted
        /// </summary>
        public string LocalAuthorityName
        {
            get { return _LocalAuthorityName; }
            set { _LocalAuthorityName = value; }
        }

        /// <summary>
        /// The reference number given to the submitted TPO by the local authority
        /// </summary>
        public string CouncilReference
        {
            get { return _CouncilReference; }
            set { _CouncilReference = value; }
        }

        /// <summary>
        /// The internal reference number given to the TPO by the company
        /// </summary>
        public string InternalReference
        {
            get { return _InternalReference; }
            set { _InternalReference = value; }
        }

        /// <summary>
        /// The reference number for an associated appeal
        /// </summary>
        public string AppealReference
        {
            get { return _AppealReference; }
            set { _AppealReference = value; }
        }

        /// <summary>
        /// The date the application was submitted to the local authority
        /// </summary>
        public DateTime ApplicationDate
        {
            get { return _ApplicationDate; }
            set { _ApplicationDate = value; }
        }

        /// <summary>
        /// The date the application was submitted to the local authority
        /// </summary>
        public DateTime RegistrationDate
        {
            get { return _RegistrationDate; }
            set { _RegistrationDate = value; }
        }

        /// <summary>
        /// The date the decision of the initial submission was received
        /// </summary>
        public DateTime InitialDecisionDate
        {
            get { return _InitialDecisionDate; }
            set { _InitialDecisionDate = value; }
        }

        /// <summary>
        /// The date of an Appeal. This will be min date if there is no appeal
        /// </summary>
        public DateTime AppealDate
        {
            get { return _AppealDate; }
            set { _AppealDate = value; }
        }

        /// <summary>
        /// The date a decision was received for an appeal. This will be min date if there is no appeal
        /// </summary>
        public DateTime AppealDecisionDate
        {
            get { return _AppealDecisionDate; }
            set { _AppealDecisionDate = value; }
        }

        /// <summary>
        /// Manditory date range
        /// </summary>
        public DateTime SixWeekStatutory
        {
            get { return _SixWeekStatutory; }
            set { _SixWeekStatutory = value; }
        }

        /// <summary>
        /// Manditory date range
        /// </summary>
        public DateTime EightWeekStatutory
        {
            get { return _EightWeekStatutory; }
            set { _EightWeekStatutory = value; }
        }

        /// <summary>
        /// Additional information for the TPO
        /// </summary>
        public string Notes
        {
            get { return _Notes; }
            set { _Notes = value; }
        }

        /// <summary>
        /// The local authority official responsible for the TPO
        /// </summary>
        public string TpoOfficer
        {
            get { return _TpoOfficer; }
            set { _TpoOfficer = value; }
        }

        /// <summary>
        /// The direct dial telephone number for the TPO Officer
        /// </summary>
        public string ContactNumber
        {
            get { return _ContactNumber; }
            set { _ContactNumber = value; }
        }

        /// <summary>
        /// The type of TPO, if any.
        /// </summary>
        public string OrderType
        {
            get { return _orderTypeEnum.ToString().Replace("_"," "); }
            set { /* Do nothing */ }
        }

        /// <summary>
        /// The TPO order number
        /// </summary>
        public string OrderNumber
        {
            get { return _OrderNumber; }
            set { _OrderNumber = value; }
        }

        /// <summary>
        /// The TPO order type: Defaults to TPO
        /// </summary>
        public Enums.TreeProtectionOrderTypeEnum OrderTypeEnum
        {
            get { return _orderTypeEnum; }
            set { _orderTypeEnum = value; }
        }

        /// <summary>
        /// Flag to denote if the TPO is for a conservation area
        /// </summary>
        public bool ConservationArea
        {
            get { return _ConservationArea; }
            set { _ConservationArea = value; }
        }

        /// <summary>
        /// Flag to denote that conformation has been received
        /// </summary>
        public bool Conformationreceived
        {
            get { return _Conformationreceived; }
            set { _Conformationreceived = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
