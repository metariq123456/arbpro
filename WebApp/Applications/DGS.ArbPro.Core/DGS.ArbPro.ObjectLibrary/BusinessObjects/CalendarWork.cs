﻿using DGS.ArbPro.Types;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class CalendarWork : CalendarBase
    {
        #region Members
        Enums.CalendarSkipDaysEnum _SkipDays = 0;
        int _JobSiteId = -1;
        int _WorkPlanId = -1;
        int _TeamId = 0;
        string _Comments = "";

        string _JobSiteName = "";
        Color _Color = Color.White;

        bool _IsRecurring = false;
        bool _IsMultiDates = false;
        List<CalendarWorkDate> _CalendarWorkDate = null;
        string _WeekDays = null;

        int _RepeatTypeId = -1;
        int _EveryFor = -1;
        bool _EveryDay = false;
        int _OnMonthDay = -1;
        TimeSpan _StartTimeDay1 = TimeSpan.MinValue;
        TimeSpan _ConnectiveDayEndTime = TimeSpan.MinValue;
        TimeSpan _LastDayEndTime = TimeSpan.MinValue;
        DateTime _StartTimeDay1New = DateTime.MinValue;
        DateTime _ConnectiveDayEndTimeNew = DateTime.MinValue;
        DateTime _LastDayEndTimeNew = DateTime.MinValue;
        #endregion

        #region Constructors
        public CalendarWork() : base() { }
        #endregion

        #region Properties
        /// <summary>
        /// Enum to show if any days are to be missed
        /// </summary>
        public Enums.CalendarSkipDaysEnum SkipDays
        {
            get { return _SkipDays; }
            set { _SkipDays = value; }
        }

        /// <summary>
        /// The Id of the Job site for the appointment
        /// </summary>
        public int JobSiteId
        {
            get { return _JobSiteId; }
            set { _JobSiteId = value; }
        }

        /// <summary>
        /// The Id of the work plan record
        /// </summary>
        public int WorkPlanId
        {
            get { return _WorkPlanId; }
            set { _WorkPlanId = value; }
        }

        /// <summary>
        /// The number of staff required for the work
        /// </summary>
        public int TeamId
        {
            get { return _TeamId; }
            set { _TeamId = value; }
        }

        /// <summary>
        /// Comments that may be recorded against the appointment
        /// </summary>
        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }

        public string JobSiteName
        {
            get { return _JobSiteName; }
            set { _JobSiteName = value; }
        }

        public Color Color
        {
            get { return _Color; }
            set { _Color = value; }
        }

        public int RepeatTypeId
        {
            get { return _RepeatTypeId; }
            set { _RepeatTypeId = value; }
        }

        public int EveryFor
        {
            get { return _EveryFor; }
            set { _EveryFor = value; }
        }

        public bool EveryDay
        {
            get { return _EveryDay; }
            set { _EveryDay = value; }
        }

        public int OnMonthDay
        {
            get { return _OnMonthDay; }
            set { _OnMonthDay = value; }
        }

        public bool IsRecurring
        {
            get { return _IsRecurring; }
            set { _IsRecurring = value; }
        }
        public bool IsMultiDates
        {
            get { return _IsMultiDates; }
            set { _IsMultiDates = value; }
        }
        public List<CalendarWorkDate> CalendarWorkDate
        {
            get
            {
                if (_CalendarWorkDate == null)
                    _CalendarWorkDate = new List<CalendarWorkDate>();

                return _CalendarWorkDate;
            }
            set { _CalendarWorkDate = value; }
        }

        public TimeSpan StartTimeDay1
        {
            get { return _StartTimeDay1; }
            set { _StartTimeDay1 = value; }
        }

        public TimeSpan ConnectiveDayEndTime
        {
            get { return _ConnectiveDayEndTime; }
            set { _ConnectiveDayEndTime = value; }
        }
        public TimeSpan LastDayEndTime
        {
            get { return _LastDayEndTime; }
            set { _LastDayEndTime = value; }
        }

        public DateTime StartTimeDay1New
        {
            get { return _StartTimeDay1New; }
            set { _StartTimeDay1New = value; }
        }

        public DateTime ConnectiveDayEndTimeNew
        {
            get { return _ConnectiveDayEndTimeNew; }
            set { _ConnectiveDayEndTimeNew = value; }
        }
        public DateTime LastDayEndTimeNew
        {
            get { return _LastDayEndTimeNew; }
            set { _LastDayEndTimeNew = value; }
        }
        public string WeekDay
        {
            get { return _WeekDays; }
            set { _WeekDays = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
