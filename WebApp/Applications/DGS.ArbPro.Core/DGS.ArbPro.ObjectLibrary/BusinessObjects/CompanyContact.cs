﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class CompanyContact: ContactBase
    {
        #region Members
        string _Comments = "";
        #endregion

        #region Constructors
        public CompanyContact() : base() { }
        #endregion

        #region Properties
        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
