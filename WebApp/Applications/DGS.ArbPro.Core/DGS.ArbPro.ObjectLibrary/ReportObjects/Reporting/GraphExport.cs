﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.ReportObjects
{
    public class GraphExport
    {
        List<GraphExportLine> _ExportLines = null;
        public GraphExport() { }

        public List<GraphExportLine> ExportLines
        {
            get
            {
                if (_ExportLines == null)
                {
                    _ExportLines = new List<GraphExportLine>();

                }
                return _ExportLines;
            }
            set
            {
                _ExportLines = value;
            }
        }
        public String StringValue
        {
            get
            {
                StringBuilder SB = new StringBuilder();
                foreach (var Line in ExportLines)
                {
                    SB.Append(Line.ToString());
                }
                return SB.ToString();
            }
        }
        public void GenerateFile(ref StreamWriter sw)
        {
            try
            {
                foreach (GraphExportLine value in ExportLines)
                {
                    sw.WriteLine(value.ToString());
                }
            }
            catch (Exception err)
            {

            }
        }
    }
}
