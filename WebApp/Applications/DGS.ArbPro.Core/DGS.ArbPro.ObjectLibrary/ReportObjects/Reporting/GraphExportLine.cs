﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.ReportObjects 
{
    public class GraphExportLine
    {

        #region Members
        string _Column1 = string.Empty;
        string _Column2 = string.Empty;
        string _Column3 = string.Empty;
        string _Column4 = string.Empty;
        string _Column5 = string.Empty;
        string _Column6 = string.Empty;
        string _Column7 = string.Empty;
        string _Column8 = string.Empty;
        string _Column9 = string.Empty;
        string _Column10 = string.Empty;
        string _Column11 = string.Empty;
        string _Column12 = string.Empty;
        string _Column13 = string.Empty;
        #endregion

        #region Properties
        public string Column1
        {
            get { return _Column1; }
            set { _Column1 = value; }
        }
        public string Column2
        {
            get { return _Column2; }
            set { _Column2 = value; }
        }
        public string Column3
        {
            get { return _Column3; }
            set { _Column3 = value; }
        }
        public string Column4
        {
            get { return _Column4; }
            set { _Column4 = value; }
        }
        public string Column5
        {
            get { return _Column5; }
            set { _Column5 = value; }
        }
        public string Column6
        {
            get { return _Column6; }
            set { _Column6 = value; }
        }
        public string Column7
        {
            get { return _Column7; }
            set { _Column7 = value; }
        }
        public string Column8
        {
            get { return _Column8; }
            set { _Column8 = value; }
        }
        public string Column9
        {
            get { return _Column9; }
            set { _Column9 = value; }
        }
        public string Column10
        {
            get { return _Column10; }
            set { _Column10 = value; }
        }
        public string Column11
        {
            get { return _Column11; }
            set { _Column11 = value; }
        }
        public string Column12
        {
            get { return _Column10; }
            set { _Column10 = value; }
        }
        public string Column13
        {
            get { return _Column11; }
            set { _Column11 = value; }
        }
        #endregion

        #region methods
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Column1);
            sb.Append(",");
            if (Column2 != "")
            {
                sb.Append(Column2);
            }
            sb.Append(",");
            if (Column3 != "")
            {
                sb.Append(Column3);
            }
            sb.Append(",");
            if (Column4 != "")
            {
                sb.Append(Column4);
            }
            sb.Append(",");
            if (Column5 != "")
            {
                sb.Append(Column5);
            }
            sb.Append(",");
            if (Column6 != "")
            {
                sb.Append(Column6);
            }
            sb.Append(",");
            if (Column7 != "")
            {
                sb.Append(Column7);
            }
            sb.Append(",");
            if (Column8 != "")
            {
                sb.Append(Column8);
            }
            sb.Append(",");
            if (Column9 != "")
            {
                sb.Append(Column9);
            }
            sb.Append(",");
            if (Column10 != "")
            {
                sb.Append(Column10);
            }
            sb.Append(",");
            if (Column11 != "")
            {
                sb.Append(Column11);
            }
            if (Column12 != "")
            {
                sb.Append(Column12);
            }
            sb.Append(",");
            if (Column13 != "")
            {
                sb.Append(Column13);
            }
            sb.Append(",");
            sb.Append(Environment.NewLine);
            return sb.ToString();
        }
        #endregion
    }
}
