﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.ReportObjects
{
    public partial class ReportQuoteItem
    {
        #region Properties
        public String TreeNo { get; set; }
        public String TreeName { get; set; }
        public String Description { get; set; }
        public Decimal Value { get; set; }
        #endregion

        #region Constructors
        public ReportQuoteItem() { }
        #endregion
    }
}
