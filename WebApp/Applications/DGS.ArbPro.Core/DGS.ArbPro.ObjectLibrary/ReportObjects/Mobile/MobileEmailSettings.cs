﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class MobileEmailSettings : BusinessBase
    {
        public int AppointmentId { get; set; }
        public int JobSiteId { get; set; }
        public int SurveyorId { get; set; }
        public int ContactId { get; set; }
        public int QuoteId { get; set; }
        public int QuoteLetterTemplateId { get; set; }
        public string DefaultLetterContent { get; set; }
        public string DefaultSalutation { get; set; }
        public string DefaultEmailBody { get; set; }
        public string DefaultEmailSubject { get; set; }
        public string FromAddress { get; set; }
        public string DefaultSmsText { get; set; }

        public override string ToString()
        {
            return base.GetObjectState(this); ;
        }
    }
}
