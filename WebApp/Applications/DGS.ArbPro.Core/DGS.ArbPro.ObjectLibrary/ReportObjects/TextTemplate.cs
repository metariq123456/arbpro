﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.Types
{
    public partial class TextTemplate
    {
        #region Properties
        public Int32 ID { get; set; }
        public String TemplateName { get; set; }
        public String BodyText { get; set; }
        public String AdditionalText { get; set; } // Email Subject or Payment Info etc
        public Enums.ReportTextTemplateTypeEnum ObjectType { get; set; }
        public Enums.ReportTextTemplateMessageEnum MessageType { get; set; }
        #endregion

        #region Constructors
        public TextTemplate(String Name, Enums.ReportTextTemplateTypeEnum Obj, Enums.ReportTextTemplateMessageEnum Msg)
        {
            TemplateName = Name;
            ObjectType = Obj;
            MessageType = Msg;
        }
        #endregion
    }
}
