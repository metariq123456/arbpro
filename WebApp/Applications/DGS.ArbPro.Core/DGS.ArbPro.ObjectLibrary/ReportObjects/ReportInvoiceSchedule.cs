﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types.ReportObjects
{
    public class ReportInvoiceSchedule
    {
        #region Properties
        public Int32 ClientID { get; set; }
        public String CustomerName { get; set; }
        public String CustomerAddressHouseNameOrNumber { get; set; }
        public String CustomerAddress1 { get; set; }
        public String CustomerAddress2 { get; set; }
        public String CustomerAddressTown { get; set; }
        public String CustomerAddressCounty { get; set; }
        public String CustomerAddressPostCode { get; set; }
        public String ContactTitle { get; set; }
        public String ContactFirstName { get; set; }
        public String ContactSurname { get; set; }
        public Int32 QuoteNo { get; set; }
        public Int32 InvoiceNo { get; set; }
        public String OrderNo { get; set; }
        public DateTime InvoiceDateSent { get; set; }
        public String SiteRefNo { get; set; }
        public String SiteAddress { get; set; }
        public List<ReportInvoiceItem> InvoiceItems { get; set; }
        public Decimal QuoteTotal { get; set; }
        public Decimal QuoteVatRate { get; set; }
        public Decimal QuoteVatAmount { get; set; }
        public Decimal QuoteTotalIncVat { get; set; }
        public Boolean IsCommercial { get; set; }
        #endregion

        #region Constructors
        public ReportInvoiceSchedule(){ }
        #endregion
    }
}
