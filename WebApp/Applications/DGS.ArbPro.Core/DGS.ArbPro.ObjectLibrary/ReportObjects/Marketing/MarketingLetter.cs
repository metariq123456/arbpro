﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MarketingLetter.cs" company="Microsec Ltd">
//   Copyright 2016 Microsec Ltd
// </copyright>
// <summary>
//   The marketing letter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.Types
{
    using System;
    using System.IO;

    using PDFReport;

    /// <summary>
    /// The marketing letter.
    /// </summary>
    public class MarketingLetter : Contracts.ReportBase
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MarketingLetter"/> class.
        /// </summary>
        /// <param name="savePath">
        /// The save path.
        /// </param>
        public MarketingLetter(string savePath)
            : base(Enums.ReportTypeEnum.Marketing, savePath, "Marketing Letter")
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the letter text.
        /// </summary>
        public string LetterText { get; set; }

        /// <summary>
        /// Gets or sets the letter signature.
        /// </summary>
        public byte[] LetterSignature { get; set; }

        /// <summary>
        /// Gets or sets the director name.
        /// </summary>
        public string DirectorName { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is commercial.
        /// </summary>
        public bool IsCommercial { get; set; }

        #endregion

        #region Methods

        #region Public

        #endregion

        #region Overrides

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            return this.GetObjectState(this);
        }

        /// <summary>
        /// The generate pdf document.
        /// </summary>
        /// <param name="r">
        /// The r.
        /// </param>
        /// <param name="layoutPath">
        /// The layout path.
        /// </param>
        /// <param name="settings">
        /// The settings.
        /// </param>
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            this.LetterText = settings.TextBody;
            this.GeneratePDFDocument(ref r, layoutPath, settings.IsCopy);
        }

        /// <summary>
        /// Method to populate a PDF document from the properties
        /// </summary>
        /// <param name="r">
        /// PDF Report object
        /// </param>
        /// <param name="layoutPath">
        /// string: path to the layout files
        /// </param>
        /// <param name="isCopy">
        /// The is Copy.
        /// </param>
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
            r.LoadLayout(Path.Combine(layoutPath, "MarketingLetter.xml"));

            if (this.ReportCustomeGroups != null)
            {
                foreach (CustomGroup cg in this.ReportCustomeGroups)
                {
                    r.AddCustomGroup(cg);
                }
            }

            r.PrintHeader();
            r.AddCustomGroup(Path.Combine(layoutPath, "MarketingAddressDateInfo.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "LetterMainText.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "LetterSignOffSig.xml"));
            
            string addressString = string.Empty;

            if (this.IsCommercial)
            {
                if (!string.IsNullOrWhiteSpace(this.ClientName))
                {
                    addressString += this.ClientName + Environment.NewLine;
                }
            }

            if (!string.IsNullOrWhiteSpace(this.ClientStreetNo))
            {
                addressString += this.ClientStreetNo + ", ";
            }

            if (!string.IsNullOrWhiteSpace(this.ClientAddress1))
            {
                addressString += this.ClientAddress1 + Environment.NewLine;
            }

            if (!string.IsNullOrWhiteSpace(this.ClientAddress2))
            {
                addressString += this.ClientAddress2 + Environment.NewLine;
            }

            if (!string.IsNullOrWhiteSpace(this.ClientTown))
            {
                addressString += this.ClientTown + Environment.NewLine;
            }

            if (!string.IsNullOrWhiteSpace(this.ClientCounty))
            {
                addressString += this.ClientCounty + Environment.NewLine;
            }

            if (!string.IsNullOrWhiteSpace(this.ClientPostCode))
            {
                addressString += this.ClientPostCode + Environment.NewLine + Environment.NewLine + Environment.NewLine;
            }

            DateTime curDate = DateTime.Now;

            r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = addressString;
            r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = this.ClientName;
            r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = curDate.ToLongDateString();

            r.PrintGroup(r.Group("grpAddressDateInfo"));

            r.Group("grpMainText").FindTextItem("txtMainText").Text = this.LetterText.Trim(Environment.NewLine.ToCharArray());

            r.PrintGroup(r.Group("grpMainText"));

            r.Group("grpSignOffSig").FindImageItem("imgSig").SetImage(this.LetterSignature, ResizePDFImage.KeepRatioWithinBox);

            r.Group("grpSignOffSig").FindTextItem("txtSigName").Text = this.DirectorName;

            r.PrintGroup(r.Group("grpSignOffSig"));
        }

        #endregion

        #endregion
    }
}
