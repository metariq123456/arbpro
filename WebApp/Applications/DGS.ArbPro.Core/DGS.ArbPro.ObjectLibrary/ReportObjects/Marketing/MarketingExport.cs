﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MarketingExport.cs" company="Microsec Ltd">
//   Copyright 2016 Microsec Ltd
// </copyright>
// <summary>
//   The marketing export.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.Types.ReportObjects
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;

    /// <summary>
    /// The marketing export.
    /// </summary>
    public class MarketingExport
    {
        #region Members

        /// <summary>
        /// The export lines.
        /// </summary>
        private List<MarketingExportLine> exportLines;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the export lines.
        /// </summary>
        public List<MarketingExportLine> ExportLines
        {
            get
            {
                return this.exportLines ?? (this.exportLines = new List<MarketingExportLine>());
            }

            set
            {
                this.exportLines = value;
            }
        }

        /// <summary>
        /// Gets the string value.
        /// </summary>
        public string StringValue
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                foreach (var line in this.ExportLines)
                {
                    sb.Append(line);
                }

                return sb.ToString();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The generate file.
        /// </summary>
        /// <param name="sw">
        /// The stream writer.
        /// </param>
        public void GenerateFile(ref StreamWriter sw)
        {
            try
            {
                foreach (MarketingExportLine value in this.ExportLines)
                {
                    sw.WriteLine(value.ToString());
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        #endregion
    }
}
