﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MarketingExportLine.cs" company="Microsec Ltd">
//   Copyright 2016 Microsec Ltd
// </copyright>
// <summary>
//   Defines the MarketingExportLine type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.Types.ReportObjects
{
    using System;
    using System.Text;

    /// <summary>
    /// The marketing export line.
    /// </summary>
    public class MarketingExportLine
    {
        #region Members

        private string column1 = string.Empty;
        private string column2 = string.Empty;
        private string column3 = string.Empty;
        private string column4 = string.Empty;
        private string column5 = string.Empty;
        private string column6 = string.Empty;

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the column 1.
        /// </summary>
        public string Column1
        {
            get { return this.column1; }
            set { this.column1 = value; }
        }

        /// <summary>
        /// Gets or sets the column 2.
        /// </summary>
        public string Column2
        {
            get { return this.column2; }
            set { this.column2 = value; }
        }

        /// <summary>
        /// Gets or sets the column 3.
        /// </summary>
        public string Column3
        {
            get { return this.column3; }
            set { this.column3 = value; }
        }

        /// <summary>
        /// Gets or sets the column 4.
        /// </summary>
        public string Column4
        {
            get { return this.column4; }
            set { this.column4 = value; }
        }

        /// <summary>
        /// Gets or sets the column 5.
        /// </summary>
        public string Column5
        {
            get { return this.column5; }
            set { this.column5 = value; }
        }

        /// <summary>
        /// Gets or sets the column 6.
        /// </summary>
        public string Column6
        {
            get { return this.column6; }
            set { this.column6 = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// The to string.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(this.Column1);
            sb.Append("\t");
            if (this.Column2 != string.Empty)
            {
                sb.Append(this.Column2);
            }

            sb.Append("\t");
            if (this.Column3 != string.Empty)
            {
                sb.Append(this.Column3);
            }

            sb.Append("\t");
            if (this.Column4 != string.Empty)
            {
                sb.Append(this.Column4);
            }

            sb.Append("\t");
            if (this.Column5 != string.Empty)
            {
                sb.Append(this.Column5);
            }

            sb.Append("\t");
            if (this.Column6 != string.Empty)
            {
                sb.Append(this.Column6);
            }

            sb.Append(Environment.NewLine);
            return sb.ToString();
        }

        #endregion
    }
}
