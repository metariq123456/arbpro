﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    /// <summary>
    /// POCO class for setting properties of a PDF
    /// </summary>
    public class WorkPdfSettings : PdfSettings
    {
        #region Memebers
        Types.Enums.DocGenerationEnum _GenerationEnum = Enums.DocGenerationEnum.Both;
        Types.Enums.WorkReportParentEnum _RiskParentType = Enums.WorkReportParentEnum.JobSite;
        bool _ShowPictures = true;
        int _DocNo;
        #endregion

        #region Constructors
        public WorkPdfSettings() : base(Enums.ReportTypeEnum.WorkProgramming) { }
        #endregion

        #region Properties

        public Types.Enums.DocGenerationEnum GenerationEnum
        {
            get { return _GenerationEnum; }
            set { _GenerationEnum = value; }
        }
        /// <summary>
        /// Enum to denote the type of object the base Id is for.
        /// </summary>
        public Types.Enums.WorkReportParentEnum ReportParentType
        {
            get { return _RiskParentType; }
            set { _RiskParentType = value; }
        }
        /// <summary>
        /// Flag to denote if the individual item values are to be shown
        /// </summary>
        public bool ShowPictures
        {
            get { return _ShowPictures; }
            set { _ShowPictures = value; }
        }
        public int DocNo
        {
            get { return _DocNo; }
            set { _DocNo = value; }
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return base.GetObjectState(this); ;
        }
        #endregion
    }
}
