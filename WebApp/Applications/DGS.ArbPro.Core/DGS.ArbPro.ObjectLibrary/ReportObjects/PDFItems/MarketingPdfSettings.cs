﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MarketingPdfSettings.cs" company="Microsec Ltd">
//   Copyright 2016 Microsec Ltd
// </copyright>
// <summary>
//   The marketing pdf settings.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.Types
{
    /// <summary>
    /// The marketing pdf settings.
    /// </summary>
    public class MarketingPdfSettings : PdfSettings
    {
        #region Memebers

        /// <summary>
        /// The generation enumerable.
        /// </summary>
        private Enums.DocGenerationEnum generationEnum = Enums.DocGenerationEnum.Letter;

        /// <summary>
        /// The client id.
        /// </summary>
        private int clientId = -1;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MarketingPdfSettings"/> class.
        /// </summary>
        public MarketingPdfSettings()
            : base(Enums.ReportTypeEnum.Marketing)
        {
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the generation enumerable.
        /// </summary>
        public Enums.DocGenerationEnum GenerationEnum
        {
            get { return this.generationEnum; }
            set { this.generationEnum = value; }
        }

        /// <summary>
        /// Gets or sets the client id.
        /// </summary>
        public int ClientId
        {
            get { return this.clientId; }
            set { this.clientId = value; }
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return this.GetObjectState(this);
        }

        #endregion
    }
}
