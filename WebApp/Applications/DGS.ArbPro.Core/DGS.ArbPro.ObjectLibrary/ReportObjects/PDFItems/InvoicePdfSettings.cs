﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    /// <summary>
    /// POCO class for setting properties of a PDF
    /// </summary>
    public class InvoicePdfSettings : PdfSettings
    {
        #region Memebers
        Types.Enums.InvoiceDocTypeEnum _GenerationEnum = Enums.InvoiceDocTypeEnum.Letter_And_Schedule;
        string _PaymentDetails = "";
        #endregion

        #region Constructors
        public InvoicePdfSettings() : base() 
        {
            base.RptType = Enums.ReportTypeEnum.Invoice;
        }
        //public InvoicePdfSettings():base(Enums.ReportTypeEnum.Invoice){}
        #endregion

        #region Properties

        public Types.Enums.InvoiceDocTypeEnum GenerationEnum
        {
            get { return _GenerationEnum; }
            set { _GenerationEnum = value; }
        }
        /// <summary>
        /// The payment details for the reoprt. This may be based on the template or free text
        /// </summary>
        public string PaymentDetails
        {
            get { return _PaymentDetails; }
            set { _PaymentDetails = value; }
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return base.GetObjectState(this); ;
        }
        #endregion
    }
}
