﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class EmailSettings : BusinessBase
    {
        #region Members
        List<int> _Recipients = null;
        string _Subject = "";
        string _BodyText = "";
        string _FromAddress = "";
        // DGSAP-852
        string _SmsText = "";
        List<String> _CCRecipients = null;
        #endregion

        #region Constructors

        #endregion

        #region Properties
        /// <summary>
        /// The list of recipient Ids the email is to be sent to
        /// </summary>
        public List<int> Recipients
        {
            get
            {
                if (_Recipients == null)
                    _Recipients = new List<int>();

                return _Recipients;
            }
            set { _Recipients = value; }
        }

        /// <summary>
        /// The list of CC recipient the email is to be sent to
        /// </summary>
        public List<string> CCRecipients
        {
            get
            {
                if (_CCRecipients == null)
                    _CCRecipients = new List<string>();

                return _CCRecipients;
            }
            set { _CCRecipients = value; }
        }
        /// <summary>
        /// The subject of the email
        /// </summary>
        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; }
        }

        /// <summary>
        /// The body text of the email
        /// </summary>
        public string BodyText
        {
            get { return _BodyText; }
            set { _BodyText = value; }
        }

        /// <summary>
        /// The sender of the email
        /// </summary>
        public string FromAddress
        {
            get { return _FromAddress; }
            set { _FromAddress = value; }
        }

        // DGSAP-852 - The body text of the sms
        public string SmsText
        {
            get { return _SmsText; }
            set { _SmsText = value; }
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return base.GetObjectState(this); ;
        }
        #endregion
    }
}
