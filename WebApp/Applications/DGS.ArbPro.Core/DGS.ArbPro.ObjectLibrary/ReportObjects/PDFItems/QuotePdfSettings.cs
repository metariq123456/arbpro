﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    /// <summary>
    /// POCO class for setting properties of a Quote PDF
    /// </summary>
    public class QuotePdfSettings : PdfSettings
    {
        #region Memebers
        Types.Enums.DocGenerationEnum _GenerationEnum = Enums.DocGenerationEnum.Both;
        bool _ShownItemValues = true;
        bool _ShowTotals = true;
        bool _IncludeAllSites = true;
        int _ContactId = -1;
        #endregion

        #region Constructors
        public QuotePdfSettings():base(Enums.ReportTypeEnum.Quote) { }
        #endregion

        #region Properties
        public Types.Enums.DocGenerationEnum GenerationEnum
        {
            get { return _GenerationEnum; }
            set { _GenerationEnum = value; }
        }
        /// <summary>
        /// Flag to denote if the individual item values are to be shown
        /// </summary>
        public bool ShownItemValues
        {
            get { return _ShownItemValues; }
            set { _ShownItemValues = value; }
        }
        /// <summary>
        /// Flag to denote if the item totals are to be shown
        /// </summary>
        public bool ShowTotals
        {
            get { return _ShowTotals; }
            set { _ShowTotals = value; }
        }
        /// <summary>
        /// Flag to denote if all sites for a report should be included in one report
        /// </summary>
        public bool IncludeAllSites
        {
            get { return _IncludeAllSites; }
            set { _IncludeAllSites = value; }
        }

        public int ContactId
        {
            get { return _ContactId; }
            set { _ContactId = value; }
        }
        #endregion

        #region Overrides
        public override string ToString()
        {
            return base.GetObjectState(this); ;
        }
        #endregion
    }
}
