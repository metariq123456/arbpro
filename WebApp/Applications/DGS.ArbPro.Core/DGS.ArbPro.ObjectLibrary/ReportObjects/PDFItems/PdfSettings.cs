﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    /// <summary>
    /// BASE POCO class for setting properties of a PDF
    /// </summary>
    public abstract class PdfSettings : BusinessBase
    {
        #region Memebers
        Types.Enums.ReportTypeEnum _RptType = Enums.ReportTypeEnum.Quote;
        bool _IsPreview = false;
        bool _IsCopy = false;
        int _TemplateId = -1;
        string _TextBody = "";
        int _QuoteId = 1;
        #endregion

        #region Constructors
        public PdfSettings() { }

        public PdfSettings(Enums.ReportTypeEnum rptType)
        {
            _RptType = rptType;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Enum for the type of report the settings are for
        /// </summary>
        public Types.Enums.ReportTypeEnum RptType
        {
            get { return _RptType; }
            set { _RptType = value; }
        }
        /// <summary>
        /// Flag to denote if the report is a preview to test layout
        /// </summary>
        public bool IsPreview
        {
            get { return _IsPreview; }
            set { _IsPreview = value; }
        }
        /// <summary>
        /// Flag to denote if the report is a copy of an existing report
        /// </summary>
        public bool IsCopy
        {
            get { return _IsCopy; }
            set { _IsCopy = value; }
        }
        /// <summary>
        /// The Id of the template that the report is based on
        /// </summary>
        public int TemplateId
        {
            get { return _TemplateId; }
            set { _TemplateId = value; }
        }
        /// <summary>
        /// The text body for the report, this may be the template text or free text
        /// </summary>
        public string TextBody
        {
            get { return _TextBody; }
            set { _TextBody = value; }
        }

        /// <summary>
        /// the quote id of the current report
        /// </summary>
        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }
        #endregion
    }
}
