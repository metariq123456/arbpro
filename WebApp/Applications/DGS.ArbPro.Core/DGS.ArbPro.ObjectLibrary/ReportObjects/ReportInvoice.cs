﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.Types
{
    public partial class ReportInvoiceLetter
    {
        #region Properties
        public Int32 ClientID {get;set;}
        public String CustomerName {get;set;}
        public String CustomerAddressHouseNameOrNumber {get;set;}
        public String CustomerAddress1 {get;set;}
        public String CustomerAddress2 {get;set;}
        public String CustomerAddressTown {get;set;}
        public String CustomerAddressCounty {get;set;}
        public String CustomerAddressPostCode {get;set;}
        public String ContactTitle {get;set;}
        public String ContactFirstName {get;set;}
        public String ContactSurname {get;set;}
        public Int32 QuoteNo {get;set;}
        public String OrderNo {get;set;}
        public DateTime InvoiceDateSent {get;set;}
        public Int32 InvoiceNo {get;set;}
        public String SiteRefNo {get;set;}
        public String SiteAddress {get;set;}
        public String LetterText {get;set;}
        public Byte[] LetterSignature {get;set;}
        public String DirectorName {get;set;}
        public Decimal InvoiceValue {get;set;}
        public Decimal InvoiceVatRate {get;set;}
        public Decimal InvoiceVatValue {get;set;}
        public Decimal InvoiceTotalIncVat {get;set;}
        public String PaymentDetails {get;set;}
        public Boolean IsCommercial {get;set;}
        #endregion

        #region Constructors
        public ReportInvoiceLetter()
        {
        }
        #endregion
    }
}
