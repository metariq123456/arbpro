﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    public partial class QuoteLetter : Contracts.ReportBase
    {
        #region Properties
        public DateTime QuoteDateSent { get; set; }
        public String QuoteSurveyor { get; set; }
        public String LetterText { get; set; }
        public Byte[] LetterSignature { get; set; }
        public String DirectorName { get; set; }
        public Boolean IsCommercial { get; set; }
        #endregion

        #region Constructors
        public QuoteLetter(string savePath):base(Enums.ReportTypeEnum.Quote,  savePath, "Quote Letter") 
        {
            this.QuoteDateSent = DateTime.Today;
        }
        public QuoteLetter(string savePath, string cultureCode): base(Enums.ReportTypeEnum.Quote, savePath, "Quote Letter")
        {
            this.QuoteDateSent = DateTime.Today;
        }
        #endregion

        #region Methods
        #region Public
        #endregion
        #region Overrides

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            this.LetterText = settings.TextBody;
            GeneratePDFDocument(ref r, layoutPath, settings.IsCopy);
        }

        /// <summary>
        /// Method to populate a PDF document from the properties
        /// </summary>
        /// <param name="r">PDF Report object</param>
        /// <param name="layoutPath">string: path to the layout files</param>
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {

            r.LoadLayout(Path.Combine(layoutPath, "QuoteLetter.xml"));

            if (ReportCustomeGroups != null)
            {
                foreach (CustomGroup cg in base.ReportCustomeGroups)
                {
                    r.AddCustomGroup(cg);
                }
            }

            r.PrintHeader();
            r.AddCustomGroup(Path.Combine(layoutPath, "QuoteAddressDateInfo.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "LetterSalutationSiteAddress.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "LetterMainText.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "LetterSignOffSig.xml"));

            //TODO: LoadAdditionalLayouts(ref r, layoutPath, settings);

            string sCustAddr = "";

            if (this.IsCommercial)
            {
                if (!string.IsNullOrWhiteSpace(this.ClientName))
                {
                    sCustAddr += this.ClientName + Environment.NewLine;
                }
            }

            if (!string.IsNullOrWhiteSpace(this.ClientStreetNo))
            {
                sCustAddr += this.ClientStreetNo + ", ";
            }

            if (!string.IsNullOrWhiteSpace(this.ClientAddress1))
            {
                sCustAddr += this.ClientAddress1 + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientAddress2))
            {
                sCustAddr += this.ClientAddress2 + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientTown))
            {
                sCustAddr += this.ClientTown + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientCounty))
            {
                sCustAddr += this.ClientCounty + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientPostCode))
            {
                sCustAddr += this.ClientPostCode + Environment.NewLine;
            }

            string sCustName = "";

            if (!string.IsNullOrWhiteSpace(this.ContactSurname))
            {
                sCustName += this.ContactSurname;
                if (!string.IsNullOrWhiteSpace(this.ContactFirstName))
                {
                    sCustName = this.ContactFirstName + " " + sCustName;
                }
                if (!string.IsNullOrWhiteSpace(this.ContactTitle))
                {
                    sCustName = this.ContactTitle + " " + sCustName;
                }
            }

            r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = this.QuoteDateSent.ToShortDateString();
            r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sCustName;
            r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sCustAddr;
            r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = this.ReferenceNo;
            //r.Group("grpAddressDateInfo").FindTextItem("txtQuoteRef").Text = this.ReferenceNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = this.ClientID.ToString();
            r.Group("grpAddressDateInfo").FindTextItem("txtSurveyor").Text = this.QuoteSurveyor;
            r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = this.SiteRef;
            r.Group("grpAddressDateInfo").FindTextItem("txtOrderNo").Text = this.PurchaseOrderNo;
            string sSalutation = "Dear ";
            if (string.IsNullOrWhiteSpace(this.ContactTitle) || string.IsNullOrWhiteSpace(this.ContactSurname))
            {
                sSalutation += "Sir/Madam";
            }
            else
            {
                sSalutation += this.ContactTitle + " " + this.ContactSurname;
            }

            r.Group("grpSalutationSiteAddress").FindTextItem("txtSalutation").Text = sSalutation;
            r.Group("grpSalutationSiteAddress").FindTextItem("txtSiteAddress").Text = this.GetSingleLineSiteAddress();

            r.PrintGroup(r.Group("grpAddressDateInfo"));
            r.PrintGroup(r.Group("grpSalutationSiteAddress"));

            r.Group("grpMainText").FindTextItem("txtMainText").Text = this.LetterText.Trim(Environment.NewLine.ToCharArray());

            r.PrintGroup(r.Group("grpMainText"));

            r.Group("grpSignOffSig").FindImageItem("imgSig").SetImage(this.LetterSignature, PDFReport.ResizePDFImage.KeepRatioWithinBox);

            r.Group("grpSignOffSig").FindTextItem("txtSigName").Text = this.DirectorName;

            r.PrintGroup(r.Group("grpSignOffSig"));

            if (isCopy)
            {
                //TODO: POPULATE COPY FIELD
            }
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
