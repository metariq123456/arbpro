﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    public class QuoteCombinedReport : Contracts.ReportBase
    {
        #region Members
        QuoteHistoryItem _Letter = null;
        QuoteHistoryItem _Schedule = null;
        #endregion

        #region Constructors
        public QuoteCombinedReport(string savePath) : base(Enums.ReportTypeEnum.Quote, savePath, "Quote Letter and Schedule") { }

        public QuoteCombinedReport(string savePath, QuoteHistoryItem letter, QuoteHistoryItem schedule)
            : base(Enums.ReportTypeEnum.Quote, savePath, "Quote Letter and Schedule") 
        {
            _Letter = letter;
            _Schedule = schedule;
            this.ClientID = -999;
        }
        #endregion

        #region Properties
        public QuoteHistoryItem Letter
        {
            get { return _Letter; }
            set { _Letter = value; }
        }

        public QuoteHistoryItem Schedule
        {
            get { return _Schedule; }
            set { _Schedule = value; }
        }
        #endregion

        #region Methods
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
            r.MergeDocuments(new MemoryStream[]{ _Letter.Doc, _Schedule.Doc }, true); 
        }

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            GeneratePDFDocument(ref r, layoutPath, settings.IsCopy);
        }

        public override string ToString()
        {
            return string.Format("Letter : {0}{1}Schedule : {2}", base.GetObjectState(_Letter),Environment.NewLine,base.GetObjectState(_Schedule));
        }
        #endregion
    }
}
