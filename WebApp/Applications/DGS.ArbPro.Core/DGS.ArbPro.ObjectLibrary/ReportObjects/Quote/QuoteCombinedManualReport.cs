﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    public class QuoteCombinedManualReport : Contracts.ReportBase
    {
        #region Members
        private Dictionary<string, MemoryStream> _ManualQuotePack = null;
        #endregion
                     
        #region Constructors
        public QuoteCombinedManualReport(string savePath) : base(Enums.ReportTypeEnum.Quote, savePath, "Combined Manual Quote Report") { }
        #endregion

        #region Properties
        public Dictionary<string, MemoryStream> ManualQuotePack
        {
            get
            {
                if (_ManualQuotePack == null)
                     _ManualQuotePack = new Dictionary<string, MemoryStream>();
                    
                return _ManualQuotePack;
            }
            set
            {
                _ManualQuotePack = value;
            }
        }
        #endregion

        #region Methods
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            GeneratePDFDocument(ref r, layoutPath, settings.IsCopy);
        }
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
            List<MemoryStream> pdfs = new List<MemoryStream>();
            foreach (KeyValuePair<string, MemoryStream> doc in this.ManualQuotePack)
            {
                try
                {
                    if (doc.Value != null)
                        pdfs.Add(doc.Value);
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Error in Combined Manual Report: {0}", ex.ToString());
                }
            }
            r.MergeDocuments(pdfs.ToArray(), true);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
