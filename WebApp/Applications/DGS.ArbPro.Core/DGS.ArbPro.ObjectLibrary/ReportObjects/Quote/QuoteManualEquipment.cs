﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    public class QuoteManualEquipment : Contracts.ReportBase
    {
        #region Members
        private String _SiteSpecificRefNo = "";
        private String _SiteAddress = "";
        private int _JobNo = 0;
        //private String _IdNo = "";
        //private String _EnquiryNo = "";
        #endregion

        #region Constructors
        public QuoteManualEquipment(string savePath) : base(Enums.ReportTypeEnum.Quote, savePath, "Quote Letter and Schedule"/*?*/) { }
        #endregion

        #region Properties
        public String SiteSpecificRefNo
        {
            get { return _SiteSpecificRefNo; }
            set { _SiteSpecificRefNo = value; }
        }
        public String SiteAddress
        {
            get { return _SiteAddress; }
            set { _SiteAddress = value; }
        }
        public int JobNo
        {
            get { return _JobNo;}
            set { _JobNo = value;}
        }
        //public String IdNo
        //{
        //    get { return _IdNo; }
        //    set {_IdNo = value;}
        //}
        //public String EnquiryNo
        //{
        //    get { return _EnquiryNo; }
        //    set { _EnquiryNo = value; }
        //}
        #endregion

        #region Methods
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            GeneratePDFDocument(ref r, layoutPath, settings.IsCopy);
        }

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
            r.LoadLayout(Path.Combine(layoutPath, "MapToolsAndMachineryTemplate.xml"));

            r.Group("grpSiteInfo").FindTextItem("txtSiteSpecificRefNo").Text = SiteSpecificRefNo;
            r.Group("grpSiteInfo").FindTextItem("txtSiteAddress").Text = SiteAddress;
            r.Group("grpSiteInfo").FindTextItem("txtJobNum").Text = JobNo.ToString();

            r.PrintGroup(r.Group("grpSiteInfo"));
            r.PrintGroup(r.Group("grpDrawing"));
            r.PrintGroup(r.Group("grpRecordComments"));
            r.PrintGroup(r.Group("grpItemsRowOne"));
            r.PrintGroup(r.Group("grpItemsRowTwo"));
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion

    }
}
