﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    public class QuoteManualEntry : Contracts.ReportBase
    {
        #region Members
        private String _Name = "";
        private String _Company = "";
        private String _AddressLine1 = "";
        private String _AddressLine2 = "";
        private String _City = "";
        private String _County = "";
        private String _PostCode = "";
        private String _Mobile = "";
        private String _Telephone = "";
        private String _ContactName = "";
        //Trees protected, bool?
        private String _CouncilName = "";
        private DateTime _DateQuoteBooked = new DateTime();
        private DateTime _QuoteTime = new DateTime();
        private String _SurveyorName = "";
        private String _SiteAddress = "";
        private String _SiteSpecificRefNo = "";
        private String _SiteContactName = "";
        private int _JobNo = 0;
        private String _AccountNo = "";
        #endregion

        #region Constructors
        public QuoteManualEntry(string savePath) : base(Enums.ReportTypeEnum.Quote, savePath, "Quote Manual Entry"/*?*/) { }
        #endregion

        #region Properties
        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        public String Company
        {
            get { return _Company; }
            set { _Company = value; }
        }
        public String AddressLine1
        {
            get { return _AddressLine1; }
            set { _AddressLine1 = value; }
        }
        public String AddressLine2
        {
            get { return _AddressLine2; }
            set { _AddressLine2 = value; }
        }
        public String City
        {
            get { return _City; }
            set { _City = value; }
        }
        public String County
        {
            get { return _County; }
            set {_County = value; }
        }
        public String PostCode
        {
            get { return _PostCode; }
            set {_PostCode = value; }
        }
        public String Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        public String Telephone
        {
            get { return _Telephone; }
            set { _Telephone = value; }
        }
        public String ContactName
        {
            get { return _ContactName; }
            set { _ContactName = value; }
        }
        public String CouncilName
        {
            get {return _CouncilName; }
            set { _CouncilName = value; }
        }
        public DateTime DateQuoteBooked
        {
            get { return _DateQuoteBooked; }
            set { _DateQuoteBooked = value; }
        }
        public DateTime QuoteTime
        {
            get { return _QuoteTime; }
            set { _QuoteTime = value; }
        }
        public String SurveyorName
        {
            get { return _SurveyorName; }
            set { _SurveyorName = value; }
        }
        public String SiteAddress
        {
            get { return _SiteAddress; }
            set { _SiteAddress = value; }
        }
        public String SiteSpecificRefNo
        {
            get { return _SiteSpecificRefNo; }
            set { _SiteSpecificRefNo = value; }
        }
        public String SiteContactName
        {
            get { return _SiteContactName; }
            set { _SiteContactName = value; }
        }
        public int JobNo
        {
            get { return _JobNo; }
            set { _JobNo = value; }
        }
        public String AccountNo
        {
            get { return _AccountNo;}
            set { _AccountNo = value;}
        }
        #endregion

        #region Methods
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            GeneratePDFDocument(ref r, layoutPath, settings.IsCopy);
        }
        /// <summary>
        /// Method to populate a PDF document from the properties
        /// </summary>
        /// <param name="r">PDF Report object</param>
        /// <param name="layoutPath">string: path to the layout files</param>
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
            r.LoadLayout(Path.Combine(layoutPath, "QuoteFormTemplate.xml"));
            
            r.Group("grpAddressAndQuote").FindTextItem("txtAddressName").Text = ClientName;
            r.Group("grpAddressAndQuote").FindTextItem("txtCompanyName").Text = Company;
            r.Group("grpAddressAndQuote").FindTextItem("txtAddressLineOne").Text = AddressLine1;
            r.Group("grpAddressAndQuote").FindTextItem("txtAddressLineTwo").Text = AddressLine2;
            r.Group("grpAddressAndQuote").FindTextItem("txtCity").Text = City;
            r.Group("grpAddressAndQuote").FindTextItem("txtCounty").Text = County;
            r.Group("grpAddressAndQuote").FindTextItem("txtPostCode").Text = PostCode;
            r.Group("grpAddressAndQuote").FindTextItem("txtJobNum").Text = JobNo.ToString();
            r.Group("grpAddressAndQuote").FindTextItem("txtAccNum").Text = AccountNo;
            r.PrintGroup(r.Group("grpAddressAndQuote"));

            r.Group("grpDetails").FindTextItem("txtClientMobile").Text = Mobile;
            r.Group("grpDetails").FindTextItem("txtTelNum").Text = Telephone;
            r.Group("grpDetails").FindTextItem("txtContactName").Text = ClientName;
            r.Group("grpDetails").FindTextItem("txtQuoteBooked").Text = DateQuoteBooked.ToShortDateString().ToString();
            r.Group("grpDetails").FindTextItem("txtQuoteTime").Text = QuoteTime.TimeOfDay.ToString();
            r.Group("grpDetails").FindTextItem("txtSurveyorName").Text = SurveyorName;
            r.Group("grpDetails").FindTextItem("txtCouncilName").Text = County;
            r.PrintGroup(r.Group("grpDetails"));

            r.Group("grpSite").FindTextItem("txtSiteAddress").Text = SiteAddress;
            r.Group("grpSite").FindTextItem("txtSpecificRefNo").Text = SiteSpecificRefNo;
            r.Group("grpSite").FindTextItem("txtContactName").Text = ContactName;
            r.Group("grpSite").FindTextItem("txtContact").Text = ContactName;
            r.PrintGroup(r.Group("grpSite"));
           
            r.PrintGroup(r.Group("grpTable"));
            r.NewPage();
            r.Group("grpPageTable").FindTextItem("txtJobNo").Text = JobNo.ToString();
            r.PrintGroup(r.Group("grpPageTable"));
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
