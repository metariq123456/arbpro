﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PDFReport;
using System.Threading;
using System.Globalization;

namespace DGS.ArbPro.Types
{
    public class QuoteSchedule : Contracts.ReportBase
    {
        #region Members
        DateTime _QuoteDateSent = DateTime.Today;
        String _QuoteSurveyor = "";
        Decimal _QuoteTotal = 0.0M;
        Decimal _QuoteVatRate = 0.0M;
        Decimal _QuoteVatAmount = 0.0M;
        Decimal _QuoteTotalIncVat = 0.0M;
        Boolean _IsCommercial = false;
        Boolean _ShowValues = false;
        List<QuoteItem> _QuoteItems = null;
        List<JobSite> _JobSites = null;
        string _CultureCode = "en-GB";
        string _currencySymbol = " ";
        #endregion

        #region Constructors
        public QuoteSchedule(string savePath) : base(Enums.ReportTypeEnum.Quote, savePath, "Quote Schedule") { }
        public QuoteSchedule(string savePath, string cultureCode)
            : base(Enums.ReportTypeEnum.Quote, savePath, "Quote Schedule")
        {
            _CultureCode = cultureCode;
        }
        #endregion

        #region Properties
        public DateTime QuoteDateSent
        {
            get { return _QuoteDateSent; }
            set { _QuoteDateSent = value; }
        }

        public String QuoteSurveyor
        {
            get { return _QuoteSurveyor; }
            set { _QuoteSurveyor = value; }
        }

        public Decimal QuoteTotal
        {
            get { return _QuoteTotal; }
            set { _QuoteTotal = value; }
        }

        public Decimal QuoteVatRate
        {
            get { return _QuoteVatRate; }
            set { _QuoteVatRate = value; }
        }

        public Decimal QuoteVatAmount
        {
            get { return _QuoteVatAmount; }
            set { _QuoteVatAmount = value; }
        }

        public Decimal QuoteTotalIncVat
        {
            get { return _QuoteTotalIncVat; }
            set { _QuoteTotalIncVat = value; }
        }

        public Boolean IsCommercial
        {
            get { return _IsCommercial; }
            set { _IsCommercial = value; }
        }

        public Boolean ShowValues
        {
            get { return _ShowValues; }
            set { _ShowValues = value; }
        }

        public List<QuoteItem> QuoteItems
        {
            get
            {
                if (_QuoteItems == null)
                    _QuoteItems = new List<QuoteItem>();

                return _QuoteItems;
            }
            set { _QuoteItems = value; }
        }

        public List<JobSite> JobSites
        {
            get
            {
                if (_JobSites == null)
                    _JobSites = new List<JobSite>();

                return _JobSites;
            }
            set { _JobSites = value; }
        }
        #endregion

        #region Methods
        #region Public

        #endregion
        #region Protected
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {

            r.LoadLayout(Path.Combine(layoutPath, "Schedule.xml"));
            //TODO: REMOVE FOLLOWING LINE AND REPLACE WITH THE load of custom groups


            if (ReportCustomeGroups != null)
            {
                foreach (CustomGroup cg in base.ReportCustomeGroups)
                {
                    r.AddCustomGroup(cg);
                }
            }

            r.PrintHeader();
            r.AddCustomGroup(Path.Combine(layoutPath, "QuoteAddressDateInfoGreen.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "SubjectSiteAddress.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "ScheduleDetailsHdr.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "ScheduleDetails.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "ScheduleDetailTotals.xml"));

            //TODO: LoadAdditionalLayouts(ref r, layoutPath, settings);

            string sCustAddr = "";

            if (this.IsCommercial)
            {
                if (!string.IsNullOrWhiteSpace(this.ClientName))
                {
                    sCustAddr += this.ClientName + Environment.NewLine;
                }
            }

            if (!string.IsNullOrWhiteSpace(this.ClientStreetNo))
            {
                sCustAddr += this.ClientStreetNo + ", ";
            }

            if (!string.IsNullOrWhiteSpace(this.ClientAddress1))
            {
                sCustAddr += this.ClientAddress1 + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientAddress2))
            {
                sCustAddr += this.ClientAddress2 + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientTown))
            {
                sCustAddr += this.ClientTown + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientCounty))
            {
                sCustAddr += this.ClientCounty + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientPostCode))
            {
                sCustAddr += this.ClientPostCode + Environment.NewLine;
            }

            string sCustName = "";

            if (!string.IsNullOrWhiteSpace(this.ContactSurname))
            {
                sCustName += this.ContactSurname;
                if (!string.IsNullOrWhiteSpace(this.ContactFirstName))
                {
                    sCustName = this.ContactFirstName + " " + sCustName;
                }
                if (!string.IsNullOrWhiteSpace(this.ContactTitle))
                {
                    sCustName = this.ContactTitle + " " + sCustName;
                }
            }

            r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = this.QuoteDateSent.ToShortDateString();
            r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sCustName;
            r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sCustAddr;
            r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = this.ReferenceNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = this.ClientID.ToString();
            r.Group("grpAddressDateInfo").FindTextItem("txtSurveyor").Text = this.QuoteSurveyor;
            r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = this.SiteRef;
            r.Group("grpAddressDateInfo").FindTextItem("txtOrderNo").Text = this.PurchaseOrderNo;
            r.Group("grpSubjectSiteAddress").FindTextItem("txtReportSubject").Text = "Schedule of Work";
            r.Group("grpSubjectSiteAddress").FindTextItem("txtSiteAddress").Text = this.GetSingleLineSiteAddress();

            r.PrintGroup(r.Group("grpAddressDateInfo"));
            r.PrintGroup(r.Group("grpSubjectSiteAddress"));

            bool bShowTotals = this.ShowValues;

            r.PrintGroup(r.Group("grpDetailHdr"));

            decimal subtotal = 0;

            foreach (QuoteItem rqi in this.QuoteItems)
            {
                r.Group("grpDetail").FindTextItem("txtItemNo").Text = rqi.Code;
                r.Group("grpDetail").FindTextItem("txtItem").Text = rqi.Name;
                // Only show Value if we should
                if (rqi.Value == 0)
                {
                    r.Group("grpDetail").FindTextItem("txtValue").Text = "";
                }
                else
                {
                    r.Group("grpDetail").FindTextItem("txtValue").Text = string.Format("{0:c}", rqi.Value);
                }
                r.Group("grpDetail").FindTextItem("txtDesc").Text = rqi.Description;
                r.PrintGroup(r.Group("grpDetail"));

                if (r.Height - r.Footer.Height - r.CurrentY < 25)
                {
                    r.NewPage();
                }

                //Calculate the values
                subtotal += rqi.Value;
            }

            if (bShowTotals)
            {
                decimal vatTotal = subtotal * this.QuoteVatRate;
                // Only display totals if e should
                r.Group("grpDetailTotals").FindTextItem("lblVatDesc").Text = "VAT (" + string.Format("{0:P0}", this.QuoteVatRate) + "): ";
                r.Group("grpDetailTotals").FindTextItem("txtTotValue").Text = string.Format("{0:c}", subtotal);
                r.Group("grpDetailTotals").FindTextItem("txtVatValue").Text = string.Format("{0:c}", vatTotal);
                r.Group("grpDetailTotals").FindTextItem("txtTotVatValue").Text = string.Format("{0:c}", (vatTotal + subtotal));

                r.PrintGroup(r.Group("grpDetailTotals"));
            }

            if (isCopy)
            {
                //TODO: POPULATE COPY FIELD
            }
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            var OldCulture = Thread.CurrentThread.CurrentCulture;
            try
            {
                //Cast the base settings class back to its required concreat type
                QuotePdfSettings qSettings = (QuotePdfSettings)settings;

                r.LoadLayout(Path.Combine(layoutPath, "Schedule.xml"));
                //TODO: REMOVE FOLLOWING LINE AND REPLACE WITH THE load of custom groups


                if (ReportCustomeGroups != null)
                {
                    foreach (CustomGroup cg in base.ReportCustomeGroups)
                    {
                        r.AddCustomGroup(cg);
                    }
                }

     
                r.AddCustomGroup(Path.Combine(layoutPath, "QuoteAddressDateInfoGreen.xml"));
                r.AddCustomGroup(Path.Combine(layoutPath, "SubjectSiteAddress.xml"));
                r.AddCustomGroup(Path.Combine(layoutPath, "ScheduleDetailsHdr.xml"));
                r.AddCustomGroup(Path.Combine(layoutPath, "ScheduleDetails.xml"));
                r.AddCustomGroup(Path.Combine(layoutPath, "ScheduleDetailTotals.xml"));
                r.AddCustomGroup(Path.Combine(layoutPath, "VatDetails.xml"));
                r.AddCustomGroup(Path.Combine(layoutPath, "ScheduleGrossTotal.xml"));

                //TODO: LoadAdditionalLayouts(ref r, layoutPath, settings);

                // We cannot change the culture before adding custom groups, as some cultures cause the 
                // group loading to fail.
                Thread.CurrentThread.CurrentCulture = new CultureInfo(_CultureCode, false);

                r.PrintHeader(); 

                string sCustAddr = "";

                if (this.IsCommercial)
                {
                    if (!string.IsNullOrWhiteSpace(this.ClientName))
                    {
                        sCustAddr += this.ClientName + Environment.NewLine;
                    }
                }

                if (!string.IsNullOrWhiteSpace(this.ClientStreetNo))
                {
                    sCustAddr += this.ClientStreetNo + ", ";
                }

                if (!string.IsNullOrWhiteSpace(this.ClientAddress1))
                {
                    sCustAddr += this.ClientAddress1 + Environment.NewLine;
                }
                if (!string.IsNullOrWhiteSpace(this.ClientAddress2))
                {
                    sCustAddr += this.ClientAddress2 + Environment.NewLine;
                }
                if (!string.IsNullOrWhiteSpace(this.ClientTown))
                {
                    sCustAddr += this.ClientTown + Environment.NewLine;
                }
                if (!string.IsNullOrWhiteSpace(this.ClientCounty))
                {
                    sCustAddr += this.ClientCounty + Environment.NewLine;
                }
                if (!string.IsNullOrWhiteSpace(this.ClientPostCode))
                {
                    sCustAddr += this.ClientPostCode + Environment.NewLine;
                }

                string sCustName = "";

                if (!string.IsNullOrWhiteSpace(this.ContactSurname))
                {
                    sCustName += this.ContactSurname;
                    if (!string.IsNullOrWhiteSpace(this.ContactFirstName))
                    {
                        sCustName = this.ContactFirstName + " " + sCustName;
                    }
                    if (!string.IsNullOrWhiteSpace(this.ContactTitle))
                    {
                        sCustName = this.ContactTitle + " " + sCustName;
                    }
                }

                r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = this.QuoteDateSent.ToShortDateString();
                r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sCustName;
                r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sCustAddr;
                r.Group("grpAddressDateInfo").FindTextItem("txtQuoteNo").Text = this.ReferenceNo;
                //r.Group("grpAddressDateInfo").FindTextItem("txtQuoteRef").Text = this.ReferenceNo;
                r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = this.ClientID.ToString();
                r.Group("grpAddressDateInfo").FindTextItem("txtSurveyor").Text = this.QuoteSurveyor;
                r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = this.SiteRef;
                r.Group("grpAddressDateInfo").FindTextItem("txtOrderNo").Text = this.PurchaseOrderNo;
                r.Group("grpSubjectSiteAddress").FindTextItem("txtReportSubject").Text = "Schedule of Work";
                r.Group("grpSubjectSiteAddress").FindTextItem("txtSiteAddress").Text = this.GetSingleLineSiteAddress();

                r.PrintGroup(r.Group("grpAddressDateInfo"));
                //r.PrintGroup(r.Group("grpSubjectSiteAddress"));

                //r.PrintGroup(r.Group("grpDetailHdr"));

                decimal subtotal = 0;
                Dictionary<decimal, decimal> invVatValues = new Dictionary<decimal, decimal>();

                foreach (JobSite js in this.JobSites)
                {

                    //Get a list of all the items for the job site
                    List<QuoteItem> jsItems = this.QuoteItems.FindAll(itm => itm.JobSiteId == js.Id);
                    //if there are items for the jobsite print the site and item details
                    if (jsItems.Count > 0)
                    {
                        r.Group("grpSubjectSiteAddress").FindTextItem("txtReportSubject").Text = "Quote";
                        r.Group("grpSubjectSiteAddress").FindTextItem("txtSiteAddress").Text = this.GetSingleLineSiteAddress(js);

                        r.PrintGroup(r.Group("grpSubjectSiteAddress"));

                        r.PrintGroup(r.Group("grpDetailHdr"));

                        //Print each item for the job site
                        decimal jsTotal = 0;
                        Dictionary<decimal, decimal> jsVatValues = new Dictionary<decimal, decimal>();
                        foreach (QuoteItem rii in jsItems)
                        {
                            r.Group("grpDetail").FindTextItem("txtItemNo").Text = rii.Code;
                            r.Group("grpDetail").FindTextItem("txtItem").Text = rii.Name;
                            r.Group("grpDetail").FindTextItem("txtDesc").Text = rii.Description;
                            if (rii.Value > 0 && qSettings.ShownItemValues)
                            {
                                r.Group("grpDetail").FindTextItem("txtValue").Text = string.Format("{0:C}", rii.Value);
                            }
                            else
                            {
                                r.Group("grpDetail").FindTextItem("txtValue").Text = "";
                            }
                            jsTotal += rii.Value;
                            if (jsVatValues.ContainsKey(rii.VatRate))
                                jsVatValues[rii.VatRate] += (rii.Value * (rii.VatRate / 100));
                            else
                                jsVatValues.Add(rii.VatRate, (rii.Value * (rii.VatRate / 100)));


                            if (invVatValues.ContainsKey(rii.VatRate))
                                invVatValues[rii.VatRate] += (rii.Value * (rii.VatRate / 100));
                            else
                                invVatValues.Add(rii.VatRate, (rii.Value * (rii.VatRate / 100)));

                            r.PrintGroup(r.Group("grpDetail"));

                            if (r.Height - r.Footer.Height - r.CurrentY < 25)
                            {
                                r.NewPage();
                            }
                        }
                        //Print the totals for the job site
                        if (qSettings.ShowTotals)
                        {
                            r.Group("grpDetailTotals").FindTextItem("txtTotValue").Text = string.Format("{0:C}", jsTotal);
                            r.PrintGroup(r.Group("grpDetailTotals"));
                            decimal vatValue = 0;
                            foreach (KeyValuePair<decimal, decimal> pair in jsVatValues)
                            {
                                //r.Group("grpVatTotals").FindTextItem("lblVatDesc").Text = "VAT (" + pair.Key.ToString() + "%): ";
                                r.Group("grpVatTotals").FindTextItem("lblVatDesc").Text = "VAT (" + string.Format("{0:P0}", pair.Key / 100) + "): ";
                                r.Group("grpVatTotals").FindTextItem("txtVatValue").Text = string.Format("{0:C}", pair.Value);
                                r.PrintGroup(r.Group("grpVatTotals"));
                                vatValue += pair.Value;
                            }

                            r.Group("grpGrossTotals").FindTextItem("txtTotVatValue").Text = string.Format("{0:C}", (vatValue + jsTotal));
                            r.PrintGroup(r.Group("grpGrossTotals"));
                            subtotal += jsTotal;
                        }
                    }
                }
                //Print the schedule totals if there is more than one job site
                if (JobSites.Count > 1 && qSettings.ShowTotals)
                {
                    r.Group("grpDetailTotals").FindTextItem("txtTotValue").Text = string.Format("{0:C}", subtotal);
                    r.PrintGroup(r.Group("grpDetailTotals"));
                    decimal vatValue = 0;
                    foreach (KeyValuePair<decimal, decimal> pair in invVatValues)
                    {
                        r.Group("grpVatTotals").FindTextItem("lblVatDesc").Text = "VAT (" + string.Format("{0:P0}", pair.Key / 100) + "): ";
                        r.Group("grpVatTotals").FindTextItem("txtVatValue").Text = string.Format("{0:C}", pair.Value);
                        r.PrintGroup(r.Group("grpVatTotals"));
                        vatValue += pair.Value;
                    }

                    r.Group("grpGrossTotals").FindTextItem("txtTotVatValue").Text = string.Format("{0:C}", (vatValue + subtotal));
                    r.PrintGroup(r.Group("grpGrossTotals"));
                }

                if (qSettings.IsCopy)
                {
                    //TODO: POPULATE COPY FIELD
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = OldCulture;

            }
        }

    }

}
