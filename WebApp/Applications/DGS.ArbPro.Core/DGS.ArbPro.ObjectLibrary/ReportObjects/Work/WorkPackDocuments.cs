﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    public class WorkPackDocuments: Contracts.ReportBase
    {
        #region Members
        Dictionary<string, MemoryStream> _WorkPackDocuments = null;
        #endregion

        #region Constructors
        public WorkPackDocuments(string savePath) : base(Enums.ReportTypeEnum.WorkProgramming, savePath, "Work Pack") { }
        #endregion

        #region Properties
        public Dictionary<string, MemoryStream> DocumentsPack
        {
            get
            {
                if(_WorkPackDocuments == null)
                    _WorkPackDocuments = new Dictionary<string, MemoryStream>();

                return _WorkPackDocuments;
            }
            set{ _WorkPackDocuments = value;}
        }
        #endregion

        #region Methods

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
            List<MemoryStream> pdfs = new List<MemoryStream>();
            foreach (KeyValuePair<string, MemoryStream> doc in this.DocumentsPack)
            {
                try
                {
                    if(doc.Value != null)
                        pdfs.Add(doc.Value);
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Error in WorkPackDocuments GeneratePDFDocument: {0}", ex.ToString());
                }
            }
            r.MergeDocuments(pdfs.ToArray(), true);
        }

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            GeneratePDFDocument(ref r, layoutPath,settings.IsCopy);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }

        #endregion

    }
}
