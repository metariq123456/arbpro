﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using PDFReport;
using System.Diagnostics;

namespace DGS.ArbPro.Types
{
    public partial class WorkOrder : Contracts.ReportBase
    {
        #region Members
        private int _WorkPlanId = 0;
        private int _QuoteId = 0;
        private string _SurveyorName = "";
        private string _PONumber = "";
        private DateTime _StartDate = new DateTime();
        private DateTime _EndDate = new DateTime();
        private DateTime _StartTime = new DateTime();
        //TPO information
        private TreeProtectionOrder _TPOForWork = new TreeProtectionOrder();
        private Team _Team = new Team();
        private List<WorkTool> _Tools = new List<WorkTool>();
        private List<WorkItem> _WorkItems = new List<WorkItem>();
        private string _ClientName = "";
        private string _ContactTelephone = "";
        private string _ContactFirstName = "";
        private string _ContactSurname = "";
        private string _SiteAdd1 = "";
        private string _SiteAdd2 = "";
        private string _SiteStreet = "";
        private string _SiteTown = "";
        private string _SiteCounty = "";
        private string _SitePostCode = "";
        private string _SiteRef = "";
        private bool _IsAm;
        private Byte[] _WorkItemImage;

        private Enums.TreeProtectionStatusEnum _TpoStatusEnum = 0;
        private Enums.TreeProtectionProgreesEnum _ApplicationProgessEnum = 0;

        private bool _ConservationArea = false;
        private string _TpoOfficer = "";
        private string _LocalAuthorityName = "";
        private string _AppealReference = "";
        private DateTime _AppealDecisionDate;
        private int _StaffRequired = 0;
        private string _BusinessName = "";
        private Types.ItemPicture _WorkPicture;
        private Dictionary<int, ItemPicture> _ItemPictures = new Dictionary<int, ItemPicture>();

        private decimal _TimeRequired = 0;

        private List<Types.Note> _Comments;
        #endregion

        #region Properties
        public int WorkPlanId
        {
            get { return _WorkPlanId; }
            set { _WorkPlanId = value; }
        }
        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }
        public string SurveyorName
        {
            get { return _SurveyorName; }
            set { _SurveyorName = value; }
        }
        public string PONumber
        {
            get { return _PONumber; }
            set { _PONumber = value; }
        }
        public DateTime StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
        public DateTime EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }

        public DateTime StartTime
        {
            get { return _StartTime; }
            set { _StartTime = value; }
        }
        //TPO information
        public TreeProtectionOrder TPOForWork
        {
            get { return _TPOForWork; }
            set { _TPOForWork = value; }
        }
        public Team Team
        {
            get { return _Team; }
            set { _Team = value; }
        }
        public List<WorkTool> Tools
        {
            get { return _Tools; }
            set { _Tools = value; }
        }
        public List<WorkItem> WorkItems
        {
            get { return _WorkItems; }
            set { _WorkItems = value; }
        }
        public Dictionary<int, ItemPicture> ItemPictures
        {
            get { return _ItemPictures; }
            set { _ItemPictures = value; }
        }
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }
        public string ContactTelephone
        {
            get { return _ContactTelephone; }
            set { _ContactTelephone = value; }
        }
        public string ContactFirstName
        {
            get { return _ContactFirstName; }
            set { _ContactFirstName = value; }
        }
        public string ContactSurname
        {
            get { return _ContactSurname; }
            set { _ContactSurname = value; }
        }
        public string SiteStreet
        {
            get { return _SiteStreet; }
            set { _SiteStreet = value; }
        }
        public string SiteAdd1
        {
            get { return _SiteAdd1; }
            set { _SiteAdd1 = value; }
        }
        public string SiteAdd2
        {
            get { return _SiteAdd2; }
            set { _SiteAdd2 = value; }
        }
        public string SiteTown
        {
            get { return _SiteTown; }
            set { _SiteTown = value; }
        }
        public string SiteCounty
        {
            get { return _SiteCounty; }
            set { _SiteCounty = value; }
        }
        public string SitePostCode
        {
            get { return _SitePostCode; }
            set { _SitePostCode = value; }
        }
        public string SiteRef
        {
            get { return _SiteRef; }
            set { _SiteRef = value; }
        }
        public bool IsAm
        {
            get { return _IsAm; }
            set { _IsAm = value; }
        }
        public Byte[] WorkItemImage
        {
            get { return _WorkItemImage; }
            set { _WorkItemImage = value; }
        }

        public Enums.TreeProtectionStatusEnum TpoStatusEnum
        {
            get { return _TpoStatusEnum; }
            set { _TpoStatusEnum = value; }
        }
        public Enums.TreeProtectionProgreesEnum ApplicationProgessEnum
        {
            get { return _ApplicationProgessEnum; }
            set { _ApplicationProgessEnum = value; }
        }

        public bool ConservationArea
        {
            get { return _ConservationArea; }
            set { _ConservationArea = value; }
        }
        public string TpoOfficer
        {
            get { return _TpoOfficer; }
            set { _TpoOfficer = value; }
        }
        public string LocalAuthorityName
        {
            get { return _LocalAuthorityName; }
            set { _LocalAuthorityName = value; }
        }
        public string AppealReference
        {
            get { return _AppealReference; }
            set { _AppealReference = value; }
        }
        public DateTime AppealDecisionDate
        {
            get { return _AppealDecisionDate; }
            set { _AppealDecisionDate = value; }
        }
        public int StaffRequired
        {
            get { return _StaffRequired; }
            set { _StaffRequired = value; }
        }
        public string BusinessName
        {
            get { return _BusinessName; }
            set { _BusinessName = value; }
        }
        public Types.ItemPicture WorkPicture
        {
            get { return _WorkPicture; }
            set { _WorkPicture = value; }
        }

        public decimal TimeRequired
        {
            get { return _TimeRequired; }
            set { _TimeRequired = value; }
        }

        public List<Types.Note> Comments
        {
            get
            {
                if (_Comments == null)
                    _Comments = new List<Types.Note>();
                return _Comments;
            }
            set { _Comments = value; }
        }
        #endregion

        #region Constructors
        public WorkOrder(string savePath)
            : base(Enums.ReportTypeEnum.WorkProgramming, savePath, "Work Programming")
        {
            WorkItems = new List<WorkItem>();
            Tools = new List<WorkTool>();
            Team = new Team();
        }
        #endregion

        #region Methods
        #region Public
        #endregion
        #region Overrides
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            WorkPdfSettings workSettings = (WorkPdfSettings)settings;
            GeneratePDFDocument(ref r, layoutPath, workSettings.IsCopy, workSettings.ShowPictures);
        }

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
            GeneratePDFDocument(ref r, layoutPath, isCopy, true);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #region Private
        private void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy, bool includePics)
        {
            r.LoadLayout(Path.Combine(layoutPath, "WorkOrderTemplate.xml"));
            Byte[] tickBytes = File.ReadAllBytes(Path.Combine(layoutPath, "Images\\Tick.png"));

            r.AddCustomGroup(Path.Combine(layoutPath, "ClientInfo.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "WorkItemDetails.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "WorkItemDetailsNoImage.xml"));

            r.Group("Title").FindTextItem("txtSurveyorName").Text = SurveyorName;
            r.Group("Title").FindTextItem("txtQuoteNumber").Text = this.ReferenceNo;
            r.PrintGroup(r.Group("Title"));

            r.Group("ClientInfo").FindTextItem("txtClientName").Text = ClientName.ToString();
            r.Group("ClientInfo").FindTextItem("txtSiteContactName").Text = ContactFirstName + " " + ContactSurname;
            r.Group("ClientInfo").FindTextItem("txtContactNo").Text = ContactTelephone;
            r.Group("ClientInfo").FindTextItem("txtSiteAddress").Text = String.Format("{0} {1} {2} {3} {4} {5}", SiteStreet, SiteAdd1, SiteAdd2, SiteTown, SiteCounty, SitePostCode);
            r.Group("ClientInfo").FindTextItem("txtContactNumbers").Text = String.Format("Telephone: {0} Mobile: {1}", ContactTelephone, ContactMobile);
            r.Group("ClientInfo").FindTextItem("txtSpecificRefNo").Text = SiteRef;
            r.Group("ClientInfo").FindTextItem("txtBusiness").Text = BusinessName;
            r.PrintGroup(r.Group("ClientInfo"));



            r.Group("AppointmentDateTime").FindTextItem("txtStartDate").Text = StartDate.ToShortDateString().ToString();
            //r.Group("AppointmentDateTime").FindTextItem("txtTimeHrs").Text = this.TimeRequired.ToString();
            r.Group("AppointmentDateTime").FindTextItem("txtEndDate").Text = EndDate.ToShortDateString().ToString();
            if (StartTime.ToString("tt") == "AM") { IsAm = true; }
            if (IsAm)
            {
                r.Group("AppointmentDateTime").FindImageItem("imgIsAmTick").SetImage(tickBytes, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                r.Group("AppointmentDateTime").FindTextItem("txtAmStartTime").Text = StartTime.ToShortTimeString();
            }
            else
            {
                r.Group("AppointmentDateTime").FindImageItem("imgIsPmTick").SetImage(tickBytes, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                r.Group("AppointmentDateTime").FindTextItem("txtPmStartTime").Text = StartTime.ToShortTimeString();
            }
            r.PrintGroup(r.Group("AppointmentDateTime"));

            r.Group("StaffTableHeader").FindTextItem("txtNoStaffRequired").Text = StaffRequired.ToString();
            r.PrintGroup(r.Group("StaffTableHeader"));

            for (int i = 0; i < Team.Personnel.Count; i++)
            {
                r.Group("StaffRows").FindTextItem("txtStaffName").Text = Team.Personnel[i].StaffName;
                r.Group("StaffRows").FindTextItem("txtRole").Text = Team.Personnel[i].Task;
                

                i++;
                if (i < Team.Personnel.Count)
                {
                    r.Group("StaffRows").FindTextItem("txtStaffName2").Text = Team.Personnel[i].StaffName;
                    r.Group("StaffRows").FindTextItem("txtRole2").Text = Team.Personnel[i].Task;
                    
                }
                else
                {
                    r.Group("StaffRows").FindTextItem("txtStaffName2").Text = "";
                    r.Group("StaffRows").FindTextItem("txtRole2").Text = "";
                    
                }

                r.PrintGroup(r.Group("StaffRows"));
            }

            r.PrintGroup(r.Group("ContainerBottomLine"));

            r.PrintGroup(r.Group("ToolsTableHeader"));

            for (int i = 0; i < Tools.Count; i++)
            {
                r.Group("ToolRows").FindTextItem("txtItem").Text = Tools[i].Item;
                r.Group("ToolRows").FindTextItem("txtNumberOf").Text = Tools[i].Number.ToString();

                i++;
                if (i < Tools.Count)
                {
                    r.Group("ToolRows").FindTextItem("txtItem2").Text = Tools[i].Item;
                    r.Group("ToolRows").FindTextItem("txtNumberOf2").Text = Tools[i].Number.ToString();
                }
                else
                {
                    r.Group("ToolRows").FindTextItem("txtItem2").Text = "";
                    r.Group("ToolRows").FindTextItem("txtNumberOf2").Text = "";
                }

                r.PrintGroup(r.Group("ToolRows"));
            }
            r.PrintGroup(r.Group("ContainerBottomLine"));

            r.Group("Management").FindTextItem("txtQuoteNo").Text = QuoteId.ToString();
            r.Group("Management").FindTextItem("txtCouncil").Text = LocalAuthorityName;
            r.Group("Management").FindTextItem("txtAppRefNo").Text = AppealReference;
            if (this.AppealDecisionDate.Equals(DateTime.MinValue))
            {
                r.Group("Management").FindTextItem("txtDateReceived").Text = "Not yet received.";
            }
            else
            {
                r.Group("Management").FindTextItem("txtDateReceived").Text = this.AppealDecisionDate.ToString();
            }

            r.Group("Management").FindTextItem("txtTreeOfficer").Text = TpoOfficer;
            r.Group("Management").FindTextItem("txtDirectDial").Text = TPOForWork.ContactNumber.ToString();

            if (ConservationArea == true) { r.Group("Management").FindImageItem("imgConservationTick").SetImage(tickBytes, PDFReport.ResizePDFImage.KeepRatioWithinBox); }
            switch (TpoStatusEnum)
            {
                case Enums.TreeProtectionStatusEnum.Unknown:
                    break;
                case Enums.TreeProtectionStatusEnum.None:
                    r.Group("Management").FindImageItem("imgTreesNotProtectedTick").SetImage(tickBytes, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                    break;
                case Enums.TreeProtectionStatusEnum.Protected:
                    r.Group("Management").FindImageItem("imgTpoExistsTick").SetImage(tickBytes, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                    break;
            }
            switch (ApplicationProgessEnum)
            {
                case Enums.TreeProtectionProgreesEnum.Application_Refused:
                    r.Group("Management").FindImageItem("imgApplicationRefusedTick").SetImage(tickBytes, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                    r.Group("Management").FindImageItem("imgDecisionReceivedTick").SetImage(tickBytes, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                    break;
                case Enums.TreeProtectionProgreesEnum.Consent_Granted:
                    r.Group("Management").FindImageItem("imgConsentTick").SetImage(tickBytes, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                    r.Group("Management").FindImageItem("imgDecisionReceivedTick").SetImage(tickBytes, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                    break;
            }

            if (Tools.Count > 20)
            {
                r.NewPage();
            }
            r.PrintGroup(r.Group("Management"));

            if (Comments != null && Comments.Count > 0)
            {
                r.NewPage();
                r.Group("CommentsContainer").FindTextItem("txtQuoteNumber").Text = QuoteId.ToString();
                r.PrintGroup(r.Group("CommentsContainer"));

                foreach (Note c in Comments)
                {
                    string noteType = "";
                    if (c.NoteType == Enums.NoteTypeEnum.Quote)
                        noteType = "Quote";
                    else
                        noteType = "Work";

                    r.Group("Comments").FindTextItem("txtCommentTitle").Text = "Type: " + noteType + " Entered By: " +
                                                                               c.EnteredBy;
                    r.Group("Comments").FindTextItem("txtComment").Text = c.EnteredDate + " - " + c.Text;
                    r.PrintGroup(r.Group("Comments"));
                }
            }

            r.NewPage();
            r.Group("WorkItemHeader").FindTextItem("txtQuoteNumber").Text = this.ReferenceNo;
            r.PrintGroup(r.Group("WorkItemHeader"));
            for (int i = 0; i < WorkItems.Count; i++)
            {
                if (includePics)
                {
                    Byte[] workPic = null;
                    if (ItemPictures.ContainsKey(WorkItems[i].Id))
                    {
                        workPic = ItemPictures[WorkItems[i].Id].ImageBytes;
                        r.Group("WorkItemDetails")
                            .FindImageItem("ItemImage")
                            .SetImage(workPic, PDFReport.ResizePDFImage.KeepRatioWithinBox);

                        r.Group("WorkItemDetails").FindTextItem("txtNo").Text = WorkItems[i].Code.ToString();
                        r.Group("WorkItemDetails").FindTextItem("txtType").Text = WorkItems[i].Name; // 
                        r.Group("WorkItemDetails").FindTextItem("txtDetails").Text = WorkItems[i].Description;

                        r.PrintGroup(r.Group("WorkItemDetails"));
                    }
                    else
                    {
                        r.Group("WorkItemDetailsNoImage").FindTextItem("txtNo2").Text = WorkItems[i].Code.ToString();
                        r.Group("WorkItemDetailsNoImage").FindTextItem("txtType2").Text = WorkItems[i].Name; 
                        r.Group("WorkItemDetailsNoImage").FindTextItem("txtDetails2").Text = WorkItems[i].Description;

                        r.PrintGroup(r.Group("WorkItemDetailsNoImage"));
                    }
                }
                else
                {
                    r.Group("WorkItemDetailsNoImage").FindTextItem("txtNo2").Text = WorkItems[i].Code.ToString();
                    r.Group("WorkItemDetailsNoImage").FindTextItem("txtType2").Text = WorkItems[i].Name;
                    r.Group("WorkItemDetailsNoImage").FindTextItem("txtDetails2").Text = WorkItems[i].Description;

                    r.PrintGroup(r.Group("WorkItemDetailsNoImage"));
                }
            }
        }
        #endregion
        #endregion

    }
}
