﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    using System.Dynamic;

    /// <summary>
    /// Object class that corresponds with the DAL work schedule class 
    /// in order to create the work schedule without work being booked
    /// </summary>
    public partial class TpoWorkSchedule : Contracts.ReportBase
    {
        #region Members
        private int _JobSiteId = 0;
        private int _QuoteId = 0;
        //TPO information
        private List<QuoteItem> _QuoteItems = new List<QuoteItem>();
        private string _ClientName = "";
        private string _ContactTelephone = "";
        private string _ContactFirstName = "";
        private string _ContactSurname = "";
        private string _SiteAdd1 = "";
        private string _SiteAdd2 = "";
        private string _SiteStreet = "";
        private string _SiteTown = "";
        private string _SiteCounty = "";
        private string _SitePostCode = "";
        private string _SiteRef = "";
        private Byte[] _WorkItemImage;
        private string _BusinessName = "";
        private Types.ItemPicture _WorkPicture;
        private Dictionary<int, ItemPicture> _ItemPictures = new Dictionary<int, ItemPicture>();
        #endregion

        #region Properties
        /// <summary>
        /// The job site id
        /// </summary>
        public int JobSiteId
        {
            get { return _JobSiteId; }
            set { _JobSiteId = value; }
        }

        /// <summary>
        /// The quote reference id
        /// </summary>
        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }

        /// <summary>
        /// Quote items
        /// </summary>
        public List<QuoteItem> QuoteItems
        {
            get { return _QuoteItems; }
            set { _QuoteItems = value; }
        }

        /// <summary>
        /// Item pictures
        /// </summary>
        public Dictionary<int, ItemPicture> ItemPictures
        {
            get { return _ItemPictures; }
            set { _ItemPictures = value; }
        }

        /// <summary>
        /// Client names
        /// </summary>
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        /// <summary>
        /// Client contact telephone
        /// </summary>
        public string ContactTelephone
        {
            get { return _ContactTelephone; }
            set { _ContactTelephone = value; }
        }

        /// <summary>
        /// Client contact first name
        /// </summary>
        public string ContactFirstName
        {
            get { return _ContactFirstName; }
            set { _ContactFirstName = value; }
        }

        /// <summary>
        /// Client contact surname
        /// </summary>
        public string ContactSurname
        {
            get { return _ContactSurname; }
            set { _ContactSurname = value; }
        }

        /// <summary>
        /// Site street nunber
        /// </summary>
        public string SiteStreet
        {
            get { return _SiteStreet; }
            set { _SiteStreet = value; }
        }

        /// <summary>
        /// 1st line of address
        /// </summary>
        public string SiteAdd1
        {
            get { return _SiteAdd1; }
            set { _SiteAdd1 = value; }
        }

        /// <summary>
        /// 2nd line of address
        /// </summary>
        public string SiteAdd2
        {
            get { return _SiteAdd2; }
            set { _SiteAdd2 = value; }
        }

        /// <summary>
        /// Town
        /// </summary>
        public string SiteTown
        {
            get { return _SiteTown; }
            set { _SiteTown = value; }
        }

        /// <summary>
        /// County
        /// </summary>
        public string SiteCounty
        {
            get { return _SiteCounty; }
            set { _SiteCounty = value; }
        }

        /// <summary>
        /// Post code
        /// </summary>
        public string SitePostCode
        {
            get { return _SitePostCode; }
            set { _SitePostCode = value; }
        }

        /// <summary>
        /// Site reference number
        /// </summary>
        public string SiteRef
        {
            get { return _SiteRef; }
            set { _SiteRef = value; }
        }

        /// <summary>
        /// The bytes of the image
        /// </summary>
        public Byte[] WorkItemImage
        {
            get { return _WorkItemImage; }
            set { _WorkItemImage = value; }
        }

        /// <summary>
        /// Business' name
        /// </summary>
        public string BusinessName
        {
            get { return _BusinessName; }
            set { _BusinessName = value; }
        }

        /// <summary>
        /// The picture
        /// </summary>
        public Types.ItemPicture WorkPicture
        {
            get { return _WorkPicture; }
            set { _WorkPicture = value; }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes the class
        /// </summary>
        /// <param name="savePath">The path of the saved document</param>
        public TpoWorkSchedule(string savePath)
            : base(Enums.ReportTypeEnum.WorkProgramming, savePath, "Work Programming")
        {
            QuoteItems = new List<QuoteItem>();
        }
        #endregion

        #region Methods
        #region Public
        #endregion
        #region Overrides

        /// <summary>
        /// Generation of the pdf document
        /// </summary>
        /// <param name="r">Report</param>
        /// <param name="layoutPath">Path to get the template from</param>
        /// <param name="settings">The pdf settings</param>
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            WorkPdfSettings workSettings = (WorkPdfSettings)settings;
            GeneratePDFDocument(ref r, layoutPath, workSettings.IsCopy, workSettings.ShowPictures);
        }

        /// <summary>
        /// Generates a copy of the pdf
        /// </summary>
        /// <param name="r">Report</param>
        /// <param name="layoutPath">Path to get the template from</param>
        /// <param name="isCopy">If it is a copy</param>
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
            GeneratePDFDocument(ref r, layoutPath, isCopy, true);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #region Private

        /// <summary>
        /// Populates the pdf template document with the results from the stored procedure executed in TpoWorkSchedule.cs
        /// </summary>
        /// <param name="r">Report</param>
        /// <param name="layoutPath">Path of template</param>
        /// <param name="isCopy">If it is a copy or not</param>
        /// <param name="includePics">Should it include pictures</param>
        private void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy, bool includePics)
        {
            r.LoadLayout(Path.Combine(layoutPath, "TpoWorkScheduleTemplate.xml"));
            Byte[] tickBytes = File.ReadAllBytes(Path.Combine(layoutPath, "Images\\Tick.png"));

            r.AddCustomGroup(Path.Combine(layoutPath, "ClientInfo.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "WorkItemDetails.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "WorkItemImages.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "WorkItemDetailsNoImage.xml"));

            r.Group("Title").FindTextItem("txtQuoteNumber").Text = this.ReferenceNo;
            r.PrintGroup(r.Group("Title"));

            r.Group("ClientInfo").FindTextItem("txtClientName").Text = this.ClientName;
            r.Group("ClientInfo").FindTextItem("txtSiteContactName").Text = this.ContactFirstName + " " + this.ContactSurname;
            r.Group("ClientInfo").FindTextItem("txtContactNo").Text = this.ContactTelephone;
            r.Group("ClientInfo").FindTextItem("txtSiteAddress").Text = String.Format(
                "{0} {1} {2} {3} {4} {5}",
                SiteStreet,
                SiteAdd1,
                SiteAdd2,
                SiteTown,
                SiteCounty,
                SitePostCode);
            r.Group("ClientInfo").FindTextItem("txtContactNumbers").Text = String.Format(
                "Telephone: {0} Mobile: {1}",
                ContactTelephone,
                ContactMobile);
            r.Group("ClientInfo").FindTextItem("txtSpecificRefNo").Text = SiteRef;
            r.Group("ClientInfo").FindTextItem("txtBusiness").Text = BusinessName;
            r.PrintGroup(r.Group("ClientInfo"));

            r.PrintGroup(r.Group("ContainerBottomLine"));

            r.Group("QuoteItemHeader").FindTextItem("txtQuoteNumber").Text = this.ReferenceNo;
            r.PrintGroup(r.Group("QuoteItemHeader"));
            for (int i = 0; i < QuoteItems.Count; i++)
            {
                r.AddCustomGroup(Path.Combine(layoutPath, "WorkItemDetails.xml"));
                r.AddCustomGroup(Path.Combine(layoutPath, "WorkItemImages.xml"));

                int TotalImageforQuoteItem = 1;
                bool isImage = false;
                if (includePics)
                {
                    Byte[] workPic = null;


                    foreach (var dictPic in ItemPictures)
                    {
                        var picItem = dictPic.Value;
                        if (picItem.ParentId.Equals(QuoteItems[i].Id))
                        {
                            isImage = true;
                            if (TotalImageforQuoteItem <= 5)
                            {
                                workPic = picItem.ImageBytes;
                                r.Group("WorkItemImages")
                                .FindImageItem("ItemImage" + TotalImageforQuoteItem)
                                .SetImage(workPic, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                                TotalImageforQuoteItem++;
                            }
                        }
                    }
                    if (isImage)
                    {
                        if ((TotalImageforQuoteItem - 1) > 1)
                        {
                            r.Group("WorkItemDetails").FindTextItem("txtNo").Text = QuoteItems[i].Code.ToString();
                            r.Group("WorkItemDetails").FindTextItem("txtType").Text = QuoteItems[i].Name;
                            r.Group("WorkItemDetails").FindTextItem("txtDetails").Text = QuoteItems[i].Description;
                            r.PrintGroup(r.Group("WorkItemDetails"));
                            r.PrintGroup(r.Group("WorkItemImages"));
                        }
                        else
                        {
                            
                            r.Group("WorkItemDetails").FindTextItem("txtNo").Text = QuoteItems[i].Code.ToString();
                            r.Group("WorkItemDetails").FindTextItem("txtType").Text = QuoteItems[i].Name;
                            r.Group("WorkItemDetails").FindTextItem("txtDetails").Text = QuoteItems[i].Description;
                            r.Group("WorkItemDetails").FindImageItem("ItemImage")
                           .SetImage(workPic, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                            r.PrintGroup(r.Group("WorkItemDetails"));

                        }
                    }
                    else
                    {
                        r.Group("WorkItemDetailsNoImage").FindTextItem("txtNo2").Text = QuoteItems[i].Code.ToString();
                        r.Group("WorkItemDetailsNoImage").FindTextItem("txtType2").Text = QuoteItems[i].Name;
                        r.Group("WorkItemDetailsNoImage").FindTextItem("txtDetails2").Text = QuoteItems[i].Description;

                        r.PrintGroup(r.Group("WorkItemDetailsNoImage"));
                    }
                }
                else
                {
                    r.Group("WorkItemDetailsNoImage").FindTextItem("txtNo2").Text = QuoteItems[i].Code.ToString();
                    r.Group("WorkItemDetailsNoImage").FindTextItem("txtType2").Text = QuoteItems[i].Name;
                    r.Group("WorkItemDetailsNoImage").FindTextItem("txtDetails2").Text = QuoteItems[i].Description;

                    r.PrintGroup(r.Group("WorkItemDetailsNoImage"));
                }
            }
        }



        #endregion
        #endregion

        //private void GeneratePDFImageDocument(ref Report r, string layoutPath)
        //{
        //    r.LoadLayout(Path.Combine(layoutPath, "TpoWorkScheduleTemplate.xml"));
        //    Byte[] tickBytes = File.ReadAllBytes(Path.Combine(layoutPath, "Images\\Tick.png"));

        //    r.AddCustomGroup(Path.Combine(layoutPath, "ClientInfo.xml"));

        //    Byte[] workPic = null;

        //    workPic = null;
        //    r.Group("WorkItemImages")
        //    .FindImageItem("ItemImage")
        //    .SetImage(workPic, PDFReport.ResizePDFImage.KeepRatioWithinBox);
        //}


    }
}
