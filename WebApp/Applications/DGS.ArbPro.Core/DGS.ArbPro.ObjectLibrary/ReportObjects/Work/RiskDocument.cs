﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    using System.Text.RegularExpressions;

    public partial class RiskDocument : Contracts.ReportBase
    {
        #region Members
	    bool _CS38Certifed = false;
	    DateTime _DateOfAssessment = DateTime.MinValue;
        DateTime _WorkDate = DateTime.MinValue;
        DateTime _SalesAppointmentDate = DateTime.MinValue;
        DateTime _WorkAppoinmentDate = DateTime.MinValue;
	    string _NearestHospital = "";
        string _ReferenceDoc = "";
        string _MobileSignal = "";
        string _SiteAccess = "";
        string _GridReference = "";
        string _RescuePlan = "";
        string _RescueNomaniee = "";
        string _Assessor = "";
        string _TeamLeader = "";
        Byte[] _AssessorSignatureImage = null;

        List<RiskSection> _AssessmentSections = null;
        List<RiskAdditionalItem> _AdditionalItems = null;
        #endregion

        #region Constructors
        public RiskDocument(string savePath)
            : base(Enums.ReportTypeEnum.RiskAssessment, savePath, "Risk Assessment")
        {

        }
        #endregion

        #region Properties
        public bool CS38Certifed
        {
            get { return _CS38Certifed; }
            set { _CS38Certifed = value; }
        }

        public DateTime DateOfAssessment
        {
            get { return _DateOfAssessment; }
            set { _DateOfAssessment = value; }
        }

        public DateTime WorkDate
        {
            get { return _WorkDate; }
            set { _WorkDate = value; }
        }

        public DateTime SalesAppointmentDate
        {
            get { return _SalesAppointmentDate; }
            set { _SalesAppointmentDate = value; }
        }

        public DateTime WorkAppoinmentDate
        {
            get { return _WorkAppoinmentDate; }
            set { _WorkAppoinmentDate = value; }
        }

        public string NearestHospital
        {
            get { return _NearestHospital; }
            set { _NearestHospital = value; }
        }

        public string ReferenceDoc
        {
            get { return _ReferenceDoc; }
            set { _ReferenceDoc = value; }
        }

        public string MobileSignal
        {
            get { return _MobileSignal; }
            set { _MobileSignal = value; }
        }

        public string SiteAccess
        {
            get { return _SiteAccess; }
            set { _SiteAccess = value; }
        }

        public string GridReference
        {
            get { return _GridReference; }
            set { _GridReference = value; }
        }

        public string RescuePlan
        {
            get { return _RescuePlan; }
            set { _RescuePlan = value; }
        }

        public string RescueNomaniee
        {
            get { return _RescueNomaniee; }
            set { _RescueNomaniee = value; }
        }

        public string Assessor
        {
            get { return _Assessor; }
            set { _Assessor = value; }
        }

        public string TeamLeader
        {
            get { return _TeamLeader; }
            set { _TeamLeader = value; }
        }

        public Byte[] AssessorSignatureImage
        {
            get { return _AssessorSignatureImage; }
            set { _AssessorSignatureImage = value; }
        }

        public List<RiskSection> AssessmentSections
        {
            get
            {
                if (_AssessmentSections == null)
                    _AssessmentSections = new List<RiskSection>();

                return _AssessmentSections;
            }
            set { _AssessmentSections = value; }
        }

        public List<RiskAdditionalItem> AdditionalItems
        {
            get
            {
                if (_AdditionalItems == null)
                    _AdditionalItems = new List<RiskAdditionalItem>();

                return _AdditionalItems;
            }
            set { _AdditionalItems = value; }
        }
        #endregion

        #region Methods
        #region Public
        #endregion
        #region Overrides
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
            r.LoadLayout(Path.Combine(layoutPath, "Arb Pro Risk Assesment Template.xml"));
            Byte[] tickBytes = File.ReadAllBytes(Path.Combine(layoutPath, "Images\\Tick.png"));
            if (ReportCustomeGroups != null)
            {
                foreach (CustomGroup cg in base.ReportCustomeGroups)
                {
                    r.AddCustomGroup(cg);
                }
            }
            //Get a reference to the group to be populated
            ItemGroup group = r.Group("Information");
            //Populate the items as required
            group.FindTextItem("ClientIDText").Text = this.ClientID.ToString();
            group.FindTextItem("DateRiskAssesmentText").Text = 
                this.SalesAppointmentDate > DateTime.MinValue && this.SalesAppointmentDate >= this.DateOfAssessment ? this.SalesAppointmentDate.ToShortDateString() : 
                this.DateOfAssessment > DateTime.MinValue ? this.DateOfAssessment.ToShortDateString() : "";
            group.FindTextItem("QuoteNoText").Text = this.ReferenceNo;
            group.FindTextItem("DateWorkText").Text = 
                this.WorkAppoinmentDate > DateTime.MinValue && this.WorkAppoinmentDate >= this.WorkDate ? this.SalesAppointmentDate.ToShortDateString() : 
                this.WorkDate > DateTime.MinValue ? this.WorkDate.ToShortDateString() : "";
            group.FindTextItem("SurveyorNameText").Text = this.Assessor;
            group.FindTextItem("SiteRefText").Text = this.SiteRef;
            group.FindTextItem("ForemanNameText").Text = this.TeamLeader;
            group.FindTextItem("SiteAddressText").Text = base.GetMultiLineSiteAddress();
            //Print the current group
            r.PrintGroup(group);
            //Get a reference to the emergency section
            group = r.Group("EmergencyProcedures");
            //Populate the items as required
            group.FindTextItem("ReferGenericRiskDocLbl").Text = this.ReferenceDoc;
            group.FindTextItem("AEHosptialText").Text = this.NearestHospital;
            group.FindTextItem("MobilePhoneSigText").Text = this.MobileSignal;
            group.FindTextItem("SiteAccessRescueText").Text = this.SiteAccess;
            group.FindTextItem("AerialRescuePlanText").Text = this.RescuePlan;
            group.FindTextItem("SiteGridRefText").Text = this.GridReference;
            //Print the current group
            r.PrintGroup(group);

            //Iterate through each assessment section and print the contents
            ItemGroup tblHeader = r.Group("TableHeader");
            group = r.Group("TableGridInfo");
            ItemGroup tblFooter = r.Group("PrintBottomGridLine");
            
            foreach (Types.RiskSection s in this.AssessmentSections)
            {
                if (s.Comments.Length > 55)
                {
                    var output = Regex.Split(s.Comments, @"(.{1,47})(?:\s|$)").Where(x => x.Length > 0).ToList();
                    s.Comments = string.Empty;
                    foreach (var o in output)
                    {
                        s.Comments += o + System.Environment.NewLine;
                    }
                }

                tblHeader.FindTextItem("HeaderTitleText").Text = s.Title;
                tblHeader.FindTextItem("HeaderDescriptionText").Text = s.Description;
                tblHeader.FindTextItem("DocumentRefText").Text = "(Ref: " + s.ReferenceDoc + ")";

                r.PrintGroup(tblHeader);
                //Iterate through each criteria
                bool printAdditionalHazards = true;
                foreach (RiskCriterion c in s.Criteria)
                {
                    group.FindTextItem("AdditionalHazardsText").Text = printAdditionalHazards ? s.Comments : "";
                    group.FindTextItem("DescriptionText").Text = c.Criterion;
                    group.FindTextItem("GenericChapterText").Text = c.ChapterRef;

                    if (c.ControlRequired == Enums.RiskCriterionStatusEnum.Yes)
                        group.FindImageItem("ChkBoxImagePreWork").SetImage(tickBytes, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                    else
                        group.FindImageItem("ChkBoxImagePreWork").SetImage(new Byte[0]); //Reset

                    if (c.OnDayControlRequired == Enums.RiskCriterionStatusEnum.Yes)
                        group.FindImageItem("ChkBoxImageDayOfWork").SetImage(tickBytes, PDFReport.ResizePDFImage.KeepRatioWithinBox);
                    else
                        group.FindImageItem("ChkBoxImageDayOfWork").SetImage(new Byte[0]);
                    r.PrintGroup(group);
                    printAdditionalHazards = false;
                }

                r.PrintGroup(tblFooter);
            }

            //Print the additional risk items
            tblHeader = r.Group("AdditionalRiskTableHeader");
            r.PrintGroup(tblHeader);
            if (this.AdditionalItems.Count > 0)
            {
                group = r.Group("AdditionalRisksInformation");
                foreach (RiskAdditionalItem itm in this.AdditionalItems)
                {
                    if (r.Height - r.CurrentY < 25)
                    {
                        r.NewPage();
                    }

                    if (itm.ControlRequired.Length > 100)
                    {
                        var output = Regex.Split(itm.ControlRequired, @"(.{1,60})(?:\s|$)").Where(x => x.Length > 0).ToList();
                        itm.ControlRequired = string.Empty;
                        foreach (var o in output)
                        {
                            itm.ControlRequired += o + System.Environment.NewLine;
                        }
                    }

                    group.FindTextItem("AssessedRiskText").Text = itm.AssessedRisk;
                    group.FindTextItem("ControlMeasureText").Text = itm.ControlRequired;
                }
            }
            else
            {
                group = r.Group("NoAdditionalRisks");
            }
            r.PrintGroup(group);

            r.NewPage();
            //Print the section that should be filled in on the day of work (ONLY)
            //TODO: Check to see if this section should be printed by assessing the current status of Work completed
            r.PrintGroup(r.Group("DayOfWorkRisksTable"));

            r.PrintGroup(r.Group("AerialRescueTable"));

            r.PrintGroup(r.Group("OtherStaffPresentTable"));

            group = r.Group("RiskAssesmentCompletedByTables");
            group.FindImageItem("SigImage").SetImage(this.AssessorSignatureImage, ResizePDFImage.LoseRatioWithinBox);
            group.FindTextItem("DateText").Text = this.DateOfAssessment == DateTime.MinValue ? string.Empty : this.DateOfAssessment.ToShortDateString();
            r.PrintGroup(group);
        }

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            GeneratePDFDocument(ref r,layoutPath,settings.IsCopy);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion

    }
}
