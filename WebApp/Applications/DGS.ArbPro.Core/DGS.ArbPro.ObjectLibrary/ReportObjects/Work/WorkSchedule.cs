﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    public partial class WorkSchedule : Contracts.ReportBase
    {
        #region Properties
        public Int32 QuoteNo { get; set; }
        public DateTime QuoteDateSent { get; set; }
        public DateTime DateOfWork { get; set; }
        public String QuoteSurveyor { get; set; }
        public Byte[] LetterSignature { get; set; }
        public String DirectorName { get; set; }
        public Boolean IsCommercial { get; set; }
        #endregion
        
        #region Properties

        #endregion

        #region Constructors
        public WorkSchedule(string savePath)
            : base(Enums.ReportTypeEnum.WorkSchedule, savePath, "Work Schedule")
        {

        }
        #endregion

        #region Methods
        #region Public
        #endregion
        #region Overrides

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            throw new NotImplementedException();
        }

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion

    }
}
