﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    /// <summary>
    /// This class is to be used as a wrapper for the emailing system log objects
    /// </summary>
    public class EmailActivityLog
    {
        #region Members
        string _MsgId = "";
        string _Recipient = "";
        string _Sender = "";
        DateTime _SentDate = DateTime.MinValue;
        string _Status = "";
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        public EmailActivityLog() { }

        /// <summary>
        /// Optional constructor
        /// </summary>
        /// <param name="guid">The message Id for the log item</param>
        /// <param name="recipient">The email address for the recipient of the message</param>
        /// <param name="Sender">The sender or chanel for the log entry</param>
        /// <param name="status">The status of the email</param>
        /// <param name="sentDate">The date the email was sent</param>
        public EmailActivityLog(string guid, string recipient, string Sender, string status, DateTime sentDate)
        {
            _MsgId = guid;
            _Recipient = recipient;
            _Sender = Sender;
            _SentDate = sentDate;
            _Status = status;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The Id of the email the log entry represents
        /// </summary>
        public string MsgId
        {
            get { return _MsgId; }
            set { _MsgId = value; }
        }

        /// <summary>
        /// The email address for the recipient of the message
        /// </summary>
        public string Recipient
        {
            get { return _Recipient; }
            set { _Recipient = value; }
        }

        /// <summary>
        /// The sender or chanel for the log entry
        /// </summary>
        public string Sender
        {
            get { return _Sender; }
            set { _Sender = value; }
        }

        /// <summary>
        /// The date the email was sent
        /// </summary>
        public DateTime SentDate
        {
            get { return _SentDate; }
            set { _SentDate = value; }
        }

        /// <summary>
        /// The status if the email sent, clicked, bounced etc.
        /// </summary>
        public string Status
        {
            get { return _Status; }
            set { _Status = value; }
        }
        #endregion

    }
}
