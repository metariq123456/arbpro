﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.Types
{
    public partial class ReportQuoteCoveringLetter
    {
        #region Properties
        public Int32 ClientID { get; set; }
        public String CustomerName { get; set; }
        public String CustomerAddressHouseNameOrNumber { get; set; }
        public String CustomerAddress1 { get; set; }
        public String CustomerAddress2 { get; set; }
        public String CustomerAddressTown { get; set; }
        public String CustomerAddressCounty { get; set; }
        public String CustomerAddressPostCode { get; set; }
        public String ContactTitle { get; set; }
        public String ContactFirstName { get; set; }
        public String ContactSurname { get; set; }
        public Int32 QuoteNo { get; set; }
        public DateTime QuoteDateSent { get; set; }
        public String QuoteSurveyor { get; set; }
        public String SiteRefNo { get; set; }
        public String SiteAddress { get; set; }
        public String LetterText { get; set; }
        public Byte[] LetterSignature { get; set; }
        public String DirectorName { get; set; }
        public Boolean IsCommercial { get; set; }
        #endregion

        #region Constructors
        public ReportQuoteCoveringLetter() { }
        #endregion
    }
}
