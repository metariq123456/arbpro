﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DGS.ArbPro.Types.ReportObjects
{
    public class SageExport
    {
        #region Members
        List<SageExportLine> _ExportLines = null;
        #endregion

        #region Constructors
        public SageExport()
        {

        }
        #endregion

        #region Properties
        public List<SageExportLine> ExportLines
        {
            get
            {
                if (_ExportLines == null)
                {
                    _ExportLines = new List<SageExportLine>();

                }
                return _ExportLines;
            }
            set
            {
                _ExportLines = value;
            }
        }

        public String StringValue
        {
            get
            {
                StringBuilder SB = new StringBuilder();
                foreach (var Line in ExportLines)
                {
                    SB.Append(Line.ToString());
                }
                return SB.ToString();
            }
        }
        #endregion

        #region Methods
        public void GenerateFile(ref StreamWriter sw)
        {
            try
            {
                foreach (SageExportLine value in ExportLines)
                {
                    sw.WriteLine(value.ToString());
                }
            }
            catch (Exception err)
            {
               
            }
        }


        private static bool ExportSage13(/* ByVal T As Type */)
        {
            //Sub ExportSage13(ByVal T As Type)
            //        If T = Type.Invoice Then
            //            Response.Write("Type," _
            //                & "Account Reference," _
            //                & "Nominal A/C Ref," _
            //                & "Date," _
            //                & "Reference," _
            //                & "Net Amount," _
            //                & "Tax Code," _
            //                & "Tax Amount," _
            //                & vbCrLf)

            //            While rstGlobal.Read()
            //                Response.Write(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}", _
            //                    "SI", _
            //                    rstGlobal("txtAccountNo").ToString(), _
            //                    Dlookup("SalesNominalCode", "Company"), _
            //                    DateTime.Parse(rstGlobal("InvoiceDate").ToString()).ToString("dd/MM/yyyy"), _
            //                    rstGlobal("InvoiceNo").ToString(), _
            //                    rstGlobal("InvoiceTotalValue").ToString(), _
            //                    Dlookup("SalesVATCode", "Company"), _
            //                    rstGlobal("InvoiceVATValue").ToString(), _
            //                    vbCrLf))
            //            End While
            //        ElseIf T = Type.Customer Then
            //            Response.Write("Account Reference," _
            //                & "Account Name," _
            //                & "Street 1," _
            //                & "Street 2," _
            //                & "Town," _
            //                & "County," _
            //                & "Postcode," _
            //                & "Contact Name," _
            //                & "Telephone Number" _
            //                & vbCrLf)

            //            While rstGlobal.Read()
            //                Response.Write(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
            //                    rstGlobal("txtAccountNo").ToString(), _
            //                    rstGlobal("AccountName").ToString(), _
            //                    rstGlobal("ClientAddress1").ToString(), _
            //                    rstGlobal("ClientAddress2").ToString(), _
            //                    rstGlobal("ClientTown").ToString(), _
            //                    rstGlobal("ClientCounty").ToString(), _
            //                    rstGlobal("ClientPostCode").ToString(), _
            //                    rstGlobal("ClientForename").ToString() + " " + rstGlobal("ClientSurname").ToString(), _
            //                    rstGlobal("ClientTelephoneNumber").ToString(), _
            //                    vbCrLf))
            //            End While
            //        Else
            //            Response.Write("Type," _
            //                & "Account Reference," _
            //                & "Nominal A/C Ref," _
            //                & "Date," _
            //                & "Reference," _
            //                & "Net Amount," _
            //                & "Tax Code," _
            //                & "Tax Amount," _
            //                & vbCrLf)

            //            While rstGlobal.Read()
            //                Response.Write(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}",
            //                    "SA", _
            //                    rstGlobal("txtAccountNo").ToString(), _
            //                    Dlookup("ReceiptNominalCode", "Company"), _
            //                    DateTime.Parse(rstGlobal("InvoiceDatePaymentReceived").ToString()).ToString("dd/MM/yyyy"), _
            //                    rstGlobal("InvoiceNo").ToString(), _
            //                    rstGlobal("InvoiceTotalValue").ToString(), _
            //                    Dlookup("SalesVATCode", "Company"), _
            //                    rstGlobal("InvoiceVATValue").ToString(), _
            //                    vbCrLf))
            //            End While
            //        End If
            //    End Sub
            return false; //for now
        }

        private static bool ExportSage09(/*ByVal T As Type*/)
        {

            //    Sub ExportSage09(ByVal T As Type)
            //        If T = Type.Invoice Then
            //            While rstGlobal.Read()
            //                Response.Write(String.Format("{0},{1},{2},,{3},{4},{5},{6},{7},{8},{9}", _
            //                    "SI", _
            //                    rstGlobal("txtAccountNo").ToString(), _
            //                    Dlookup("SalesNominalCode", "Company"), _
            //                    DateTime.Parse(rstGlobal("InvoiceDate").ToString()).ToString("dd/MM/yyyy"), _
            //                    rstGlobal("InvoiceNo").ToString(), _
            //                    "ArbPro Invoice", _
            //                    rstGlobal("InvoiceTotalValue").ToString(), _
            //                    Dlookup("SalesVATCode", "Company"), _
            //                    rstGlobal("InvoiceVATValue").ToString(), _
            //                    vbCrLf))
            //            End While
            //        ElseIf T = Type.Customer Then
            //            While rstGlobal.Read()
            //                Response.Write(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
            //                    rstGlobal("txtAccountNo").ToString(), _
            //                    rstGlobal("AccountName").ToString(), _
            //                    rstGlobal("ClientAddress1").ToString(), _
            //                    rstGlobal("ClientAddress2").ToString(), _
            //                    rstGlobal("ClientTown").ToString(), _
            //                    rstGlobal("ClientCounty").ToString(), _
            //                    rstGlobal("ClientPostCode").ToString(), _
            //                    rstGlobal("ClientForename").ToString() + " " + rstGlobal("ClientSurname").ToString(), _
            //                    rstGlobal("ClientTelephoneNumber").ToString(), _
            //                    vbCrLf))
            //            End While
            //        Else
            //            While rstGlobal.Read()
            //                Response.Write(String.Format("{0},{1},{2},,{3},{4},{5},{6},{7},{8},{9}",
            //                    "SA", _
            //                    rstGlobal("txtAccountNo").ToString(), _
            //                    Dlookup("ReceiptNominalCode", "Company"), _
            //                    DateTime.Parse(rstGlobal("InvoiceDatePaymentReceived").ToString()).ToString("dd/MM/yyyy"), _
            //                    rstGlobal("InvoiceNo").ToString(), _
            //                    "ArbPro Sales Receipt", _
            //                    rstGlobal("InvoiceTotalValue").ToString(), _
            //                    Dlookup("SalesVATCode", "Company"), _
            //                    rstGlobal("InvoiceVATValue").ToString(), _
            //                    vbCrLf))
            //            End While
            //        End If
            //    End Sub
            return false; //for now
        }

        private static bool ExportQuickbooks(/*ByVal T As Type*/)
        {
            //    Sub ExportQuickbooks(ByVal T As Type)
            //        If T = Type.Invoice Then

            //        ElseIf T = Type.Customer Then
            //            Response.Write(
            //                "Account Number," _
            //                & "Customer Name," _
            //                & "Company Name," _
            //                & "Email," _
            //                & "Phone," _
            //                & "Mobile," _
            //                & "Street," _
            //                & "City/Town," _
            //                & "County," _
            //                & "Postcode," _
            //                & vbCrLf)
            //            While rstGlobal.Read()
            //                Dim ClientNo As String = rstGlobal("ClientIDNo").ToString()
            //                Dim ClientName As String = rstGlobal("ClientForename").ToString() & " " & rstGlobal("ClientSurname").ToString()
            //                Dim BusinessName As String = rstGlobal("ClientBusinessName").ToString()
            //                Dim ClientEmail As String = rstGlobal("ClientEmail").ToString()
            //                Dim ClientPhone As String = rstGlobal("ClientTelephoneNumber").ToString()
            //                Dim ClientMobile As String = rstGlobal("ClientMobile").ToString()
            //                Dim ClientAddress1 As String = rstGlobal("ClientHouseNameOrNumber").ToString() + ", " + rstGlobal("ClientAddress1").ToString()
            //                Dim ClientTown As String = rstGlobal("ClientTown").ToString()
            //                Dim ClientCounty As String = rstGlobal("ClientCounty").ToString()
            //                Dim ClientPostCode As String = rstGlobal("ClientPostCode").ToString()
            //                Response.Write(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", _
            //                    ClientNo, _
            //                    ClientName, _
            //                    BusinessName, _
            //                    ClientEmail, _
            //                    ClientPhone, _
            //                    ClientMobile, _
            //                    ClientAddress1, _
            //                    ClientTown, _
            //                    ClientCounty, _
            //                    ClientPostCode, _
            //                    vbCrLf))
            //            End While
            //        Else

            //        End If
            //    End Sub
            return false; //for now
        }

        private static bool ExportKashFlow(/*ByVal T As Type*/)
        {
            //    Sub ExportKashFlow(ByVal T As Type)
            //        If T = Type.Invoice Then
            //            Response.Write("Customer Id,Invoice No,Invoice Date,Due Date,Quantity,Description,Charge Type,Rate,Vat Amount,Vat Rate," + vbCrLf)

            //            While rstGlobal.Read()
            //                Response.Write(
            //                    String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", _
            //                                  rstGlobal("txtAccountNo").ToString(), _
            //                                  rstGlobal("InvoiceNo").ToString(), _
            //                                  rstGlobal("InvoiceDate").ToString(), _
            //                                  rstGlobal("InvoiceDuePaymentDate").ToString(), _
            //                                  "1", _
            //                                  "Arb Pro Invoice", _
            //                                  "Work", _
            //                                  rstGlobal("InvoiceTotalValue").ToString(), _
            //                                  rstGlobal("InvoiceVatValue").ToString(), _
            //                                  (Double.Parse(rstGlobal("InvoiceVAT")) * 100).ToString(), _
            //                                  vbCrLf))
            //            End While
            //        ElseIf T = Type.Customer Then
            //            Response.Write("Customer Id,Customer Name," + vbCrLf)
            //            While rstGlobal.Read()
            //                Response.Write(
            //                    String.Format("{0},{1},{2}", _
            //                                  rstGlobal("txtAccountNo").ToString(), _
            //                                  rstGlobal("AccountName").ToString(), _
            //                                  vbCrLf))
            //            End While
            //        Else
            //            Response.Write("Invoice No,Paid Date,Paid Amount," + vbCrLf)
            //            While rstGlobal.Read()
            //                Response.Write(
            //                    String.Format("{0},{1},{2},{3}",
            //                                  rstGlobal("InvoiceNo").ToString(), _
            //                                  rstGlobal("InvoiceDatePaymentReceived").ToString(), _
            //                                  rstGlobal("InvoiceValue").ToString(), _
            //                                  vbCrLf))
            //            End While
            //        End If
            //    End Sub

            //End Class
            return false; //for now
        }
        #endregion
    }
}
