﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    using System.Data.SqlTypes;
    using System.Threading;
    using System.Globalization;

    public class InvoiceLetter:Contracts.ReportBase
    {
        #region Members
        public String QuoteNo;
        public String OrderNo;
        public String InvoiceNo;
        public DateTime InvoiceDateSent;
        public String LetterText;
        public Byte[] LetterSignature;
        public String DirectorName;
        public Decimal InvoiceValue;
        public Decimal InvoiceVatRate;
        public Decimal InvoiceVatValue;
        public Decimal InvoiceTotalIncVat;
        public String PaymentDetails;
        public Boolean IsCommercial;
        private Invoice invoice = null;
        string _CultureCode = "en-GB";

        private Enums.InvoiceDocTypeEnum InvoiceRptType { get; set; }
        #endregion

        #region Constructors
        public InvoiceLetter(string savePath, Enums.InvoiceDocTypeEnum invType) : base(Enums.ReportTypeEnum.Invoice, savePath, invType.ToString()) 
        {
            this.InvoiceRptType = invType;
        }
        public InvoiceLetter(string savePath, Enums.InvoiceDocTypeEnum invType, string cultureCode) : base(Enums.ReportTypeEnum.Invoice, savePath, invType.ToString())
        {
            this.InvoiceRptType = invType;
            _CultureCode = cultureCode;
        }
        #endregion

        #region Public Properties


        public Invoice Invoice
        {
            get
            {
                if (this.invoice == null)
                    this.invoice = new Invoice();

                return this.invoice;
            }
            set { this.invoice = value; }
        }

        #endregion

        #region Methods
        #region Protected

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)//ref string sReportSubject, ref string sLetterType, string sInvoiceLetterType)
        {
        }

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            var OldCulture = Thread.CurrentThread.CurrentCulture;
            try
            {


            //Cast the base settings class back to its required concreat type
            InvoicePdfSettings invSettings = (InvoicePdfSettings)settings;

            string sReportSubject = "Invoice";
            switch (invSettings.GenerationEnum)
            {
                case Enums.InvoiceDocTypeEnum.Letter:
                    sReportSubject = "Invoice";
                    break;
                case Enums.InvoiceDocTypeEnum.First_Reminder:
                    sReportSubject = "Reminder";
                    break;
                case Enums.InvoiceDocTypeEnum.Second_Reminder:
                    sReportSubject = "Second Reminder";
                    break;
                case Enums.InvoiceDocTypeEnum.Final_Reminder:
                    sReportSubject = "Final Reminder";
                    break;
                case Enums.InvoiceDocTypeEnum.Legal_Action_2:
                case Enums.InvoiceDocTypeEnum.Legal_Action:
                    sReportSubject = "Legal Action";
                    break;
            }

            foreach (CustomGroup cg in base.ReportCustomeGroups)
            {
                r.AddCustomGroup(cg);
            }

            r.PrintHeader();
            r.AddCustomGroup(Path.Combine(layoutPath, "InvoiceAddressDateInfo.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "SubjectSiteAddress.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "LetterMainText.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "InvoiceLetterSigSummary.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "InvoiceLetterPaymentDetails.xml"));

            LoadAdditionalLayouts(ref r, layoutPath, settings);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(_CultureCode, false);

            string sCustAddr = "";

            if (this.IsCommercial)
            {
                if (!string.IsNullOrWhiteSpace(this.ClientName))
                {
                    sCustAddr += this.ClientName + Environment.NewLine;
                }
            }

            if (!string.IsNullOrWhiteSpace(this.ClientStreetNo))
            {
                sCustAddr += this.ClientStreetNo + ", ";
            }

            if (!string.IsNullOrWhiteSpace(this.ClientAddress1))
            {
                sCustAddr += this.ClientAddress1 + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientAddress2))
            {
                sCustAddr += this.ClientAddress2 + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientTown))
            {
                sCustAddr += this.ClientTown + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientCounty))
            {
                sCustAddr += this.ClientCounty + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientPostCode))
            {
                sCustAddr += this.ClientPostCode + Environment.NewLine;
            }

            string sCustName = "";

            if (!string.IsNullOrWhiteSpace(this.ContactSurname))
            {
                sCustName += this.ContactSurname;
                if (!string.IsNullOrWhiteSpace(this.ContactFirstName))
                {
                    sCustName = this.ContactFirstName + " " + sCustName;
                }
                if (!string.IsNullOrWhiteSpace(this.ContactTitle))
                {
                    sCustName = this.ContactTitle + " " + sCustName;
                }
            }

            r.Group("grpAddressDateInfo").FindTextItem("txtCurDate").Text = DateTime.Now.ToShortDateString();
            if (this.Invoice.DateCreated == DateTime.MinValue || this.Invoice.DateCreated == SqlDateTime.MinValue)
            {
                r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = string.Empty;
            }
            else
            {
                r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = this.Invoice.DateCreated.ToShortDateString();
            }
            r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sCustName;
            r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sCustAddr;
            r.Group("grpAddressDateInfo").FindTextItem("txtQuoteRef").Text = this.QuoteNo.ToString();
            //r.Group("grpAddressDateInfo").FindTextItem("txtInvoiceRef").Text = this.ReferenceNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtInvoiceNo").Text = this.ReferenceNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = this.ClientID.ToString();
            r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = this.SiteRef;
            r.Group("grpAddressDateInfo").FindTextItem("txtOrderNo").Text = this.PurchaseOrderNo;
               
            r.Group("grpSubjectSiteAddress").FindTextItem("txtReportSubject").Text = sReportSubject;
            r.Group("grpSubjectSiteAddress").FindTextItem("txtSiteAddress").Text = this.GetSingleLineSiteAddress();

            r.PrintGroup(r.Group("grpAddressDateInfo"));
            r.PrintGroup(r.Group("grpSubjectSiteAddress"));

            r.Group("grpMainText").FindTextItem("txtMainText").Text = invSettings.TextBody.Trim(Environment.NewLine.ToCharArray());

            r.PrintGroup(r.Group("grpMainText"));

            r.Group("grpSigSummary").FindImageItem("imgSig").SetImage(this.LetterSignature, PDFReport.ResizePDFImage.KeepRatioWithinBox);

            r.Group("grpSigSummary").FindTextItem("txtSigName").Text = this.DirectorName;

            r.Group("grpSigSummary").FindTextItem("txtVatRate").Text = "";
            r.Group("grpSigSummary").FindTextItem("txtTotal").Text = string.Format("{0:c}", this.InvoiceValue);
            r.Group("grpSigSummary").FindTextItem("txtVat").Text = string.Format("{0:c}", this.InvoiceVatValue);
            r.Group("grpSigSummary").FindTextItem("txtTotalIncVat").Text = string.Format("{0:c}", this.InvoiceTotalIncVat);

            r.PrintGroup(r.Group("grpSigSummary"));

            r.Group("grpPaymentDetails").FindTextItem("txtPaymentDetailsText").Text = invSettings.PaymentDetails;

            r.PrintGroup(r.Group("grpPaymentDetails"));

            if (invSettings.IsCopy)
            {
                //TODO: DISPLAY COPY LABAL
            }

            if (invSettings.IsPreview)
            {
                //TODO: DISPLAY PREVIEW IMAGE
            }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = OldCulture;
            }
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
