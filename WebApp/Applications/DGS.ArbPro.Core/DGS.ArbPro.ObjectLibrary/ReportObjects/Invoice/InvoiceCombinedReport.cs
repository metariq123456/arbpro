﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    public class InvoiceCombinedReport: Contracts.ReportBase
    {
        #region Members
        InvoiceHistoryItem _Letter = null;
        InvoiceHistoryItem _Schedule = null;
        #endregion

        #region Constructors
        public InvoiceCombinedReport(string savePath) : base(Enums.ReportTypeEnum.Invoice, savePath, "Quote Letter and Schedule") { }

        public InvoiceCombinedReport(string savePath, InvoiceHistoryItem letter, InvoiceHistoryItem schedule)
            : base(Enums.ReportTypeEnum.Invoice, savePath, "Quote Letter and Schedule") 
        {
            _Letter = letter;
            _Schedule = schedule;
            this.ClientID = -999;
        }
        #endregion

        #region Properties
        public InvoiceHistoryItem Letter
        {
            get { return _Letter; }
            set { _Letter = value; }
        }

        public InvoiceHistoryItem Schedule
        {
            get { return _Schedule; }
            set { _Schedule = value; }
        }
        #endregion

        #region Methods
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
            r.MergeDocuments(new MemoryStream[]{ _Letter.Doc, _Schedule.Doc }, true); 
        }

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            r.MergeDocuments(new MemoryStream[] { _Letter.Doc, _Schedule.Doc }, true); 
        }

        public override string ToString()
        {
            return string.Format("Letter : {0}{1}Schedule : {2}", base.GetObjectState(_Letter),Environment.NewLine,base.GetObjectState(_Schedule));
        }
        #endregion

    }
}