﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types
{
    using System.Data.SqlTypes;
    using System.Threading;
    using System.Globalization;

    public class InvoiceSchedule : Contracts.ReportBase
    {
        #region Members
        string _QuoteNo = "";
        string _InvoiceNo = "";
        string _OrderNo = "";
        DateTime _InvoiceDateSent = DateTime.Today;
        string _SiteRefNo = "";
        string _SiteAddress = "";
        Decimal _QuoteTotal = 0.0M;
        Decimal _QuoteVatRate = 0.0M;
        Decimal _QuoteVatAmount = 0.0M;
        Decimal _QuoteTotalIncVat = 0.0M;
        Boolean _IsCommercial = false;
         private Invoice invoice = null;

        List<InvoiceItem> _InvoiceItems = null;
        List<JobSite> _JobSites = null;
        string _CultureCode = "en-GB";
        #endregion

        #region Constructors
        public InvoiceSchedule(string savePath) : base( Enums.ReportTypeEnum.Invoice, savePath, "Invoice Schedule") { }
        public InvoiceSchedule(string savePath, string cultureCode) : base(Enums.ReportTypeEnum.Invoice, savePath, "Invoice Schedule") 
        {
            _CultureCode = cultureCode;
        
        }
        #endregion

        #region Properties
        public string QuoteNo
        {
            get { return _QuoteNo; }
            set { _QuoteNo = value; }
        }

        public string InvoiceNo
        {
            get { return _InvoiceNo; }
            set { _InvoiceNo = value; }
        }

        public string OrderNo
        {
            get { return _OrderNo; }
            set { _OrderNo = value; }
        }
   

        public DateTime InvoiceDateSent
        {
            get { return _InvoiceDateSent; }
            set { _InvoiceDateSent = value; }
        }

        public string SiteRefNo
        {
            get { return _SiteRefNo; }
            set { _SiteRefNo = value; }
        }

        public string SiteAddress
        {
            get { return _SiteAddress; }
            set { _SiteAddress = value; }
        }

        public Decimal QuoteTotal
        {
            get { return _QuoteTotal; }
            set { _QuoteTotal = value; }
        }

        public Decimal QuoteVatRate
        {
            get { return _QuoteVatRate; }
            set { _QuoteVatRate = value; }
        }

        public Decimal QuoteVatAmount
        {
            get { return _QuoteVatAmount; }
            set { _QuoteVatAmount = value; }
        }

        public Decimal QuoteTotalIncVat
        {
            get { return _QuoteTotalIncVat; }
            set { _QuoteTotalIncVat = value; }
        }

        public Boolean IsCommercial
        {
            get { return _IsCommercial; }
            set { _IsCommercial = value; }
        }

        public Invoice Invoice
        {
            get
            {
                if (this.invoice == null)
                    this.invoice = new Invoice();

                return this.invoice;
            }
            set { this.invoice = value; }
        }

        public List<InvoiceItem> InvoiceItems
        {
            get
            {
                if (_InvoiceItems == null)
                    _InvoiceItems = new List<InvoiceItem>();

                return _InvoiceItems;
            }
            set { _InvoiceItems = value; }
        }

        public List<JobSite> JobSites
        {
            get
            {
                if (_JobSites == null)
                    _JobSites = new List<JobSite>();

                return _JobSites;
            }
            set { _JobSites = value; }
        }
        #endregion

        #region Methods
        #region Public
        #endregion
        #region Protected
        protected override void GeneratePDFDocument(ref Report r, string layoutPath, PdfSettings settings)
        {
            var OldCulture = Thread.CurrentThread.CurrentCulture;
            try
            {


            //Cast the base settings class back to its required concreat type
            InvoicePdfSettings invSettings = (InvoicePdfSettings)settings;
         
            if (ReportCustomeGroups != null)
            {
                foreach (CustomGroup cg in base.ReportCustomeGroups)
                {
                    r.AddCustomGroup(cg);
                }
            }

            r.PrintHeader();
            r.AddCustomGroup(Path.Combine(layoutPath, "InvoiceAddressDateInfo.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "SubjectSiteAddress.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "ScheduleDetailsHdr.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "ScheduleDetails.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "ScheduleDetailTotals.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "VatDetails.xml"));
            r.AddCustomGroup(Path.Combine(layoutPath, "ScheduleGrossTotal.xml"));

            LoadAdditionalLayouts(ref r, layoutPath, settings);
            Thread.CurrentThread.CurrentCulture = new CultureInfo(_CultureCode, false);

            string sCustAddr = "";

            if (this.IsCommercial)
            {
                if (!string.IsNullOrWhiteSpace(base.ClientName))
                {
                    sCustAddr += this.ClientName + Environment.NewLine;
                }
            }

            if (!string.IsNullOrWhiteSpace(this.ClientStreetNo))
            {
                sCustAddr += this.ClientStreetNo + ", ";
            }

            if (!string.IsNullOrWhiteSpace(this.ClientAddress1))
            {
                sCustAddr += this.ClientAddress1 + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientAddress2))
            {
                sCustAddr += this.ClientAddress2 + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientTown))
            {
                sCustAddr += this.ClientTown + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientCounty))
            {
                sCustAddr += this.ClientCounty + Environment.NewLine;
            }
            if (!string.IsNullOrWhiteSpace(this.ClientPostCode))
            {
                sCustAddr += this.ClientPostCode + Environment.NewLine;
            }

            string sCustName = "";

            if (!string.IsNullOrWhiteSpace(this.ContactSurname))
            {
                sCustName += this.ContactSurname;
                if (!string.IsNullOrWhiteSpace(this.ContactFirstName))
                {
                    sCustName = this.ContactFirstName + " " + sCustName;
                }
                if (!string.IsNullOrWhiteSpace(this.ContactTitle))
                {
                    sCustName = this.ContactTitle + " " + sCustName;
                }
            }


            r.Group("grpAddressDateInfo").FindTextItem("txtCurDate").Text = DateTime.Now.ToShortDateString();
            if (this.Invoice.DateCreated == DateTime.MinValue || this.Invoice.DateCreated == SqlDateTime.MinValue)
            {
                r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = string.Empty;
            }
            else
            {
                r.Group("grpAddressDateInfo").FindTextItem("txtDate").Text = this.Invoice.DateCreated.ToShortDateString();
            }
            r.Group("grpAddressDateInfo").FindTextItem("txtCustomerName").Text = sCustName;
            r.Group("grpAddressDateInfo").FindTextItem("txtAddress").Text = sCustAddr;
            r.Group("grpAddressDateInfo").FindTextItem("txtQuoteRef").Text = this.QuoteNo;
            //r.Group("grpAddressDateInfo").FindTextItem("txtInvoiceRef").Text = this.InvoiceNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtInvoiceNo").Text = this.InvoiceNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtClientIDNo").Text = this.ClientID.ToString();
            r.Group("grpAddressDateInfo").FindTextItem("txtSiteRefNo").Text = this.SiteRefNo;
            r.Group("grpAddressDateInfo").FindTextItem("txtOrderNo").Text = this.PurchaseOrderNo;
            r.PrintGroup(r.Group("grpAddressDateInfo"));
            decimal invTotal = 0;
            Dictionary<decimal,decimal> invVatValues = new Dictionary<decimal,decimal>();

            foreach(JobSite js in this.JobSites)
            {
                //Get a list of all the items for the job site
                List<InvoiceItem> jsItems = this.InvoiceItems.FindAll(itm => itm.JobSiteId == js.Id);
                //if there are items for the jobsite print the site and item details
                if(jsItems.Count > 0)
                {
                    r.Group("grpSubjectSiteAddress").FindTextItem("txtReportSubject").Text = "Invoice";
                    r.Group("grpSubjectSiteAddress").FindTextItem("txtSiteAddress").Text = this.GetSingleLineSiteAddress(js);

                    r.PrintGroup(r.Group("grpSubjectSiteAddress"));

                    r.PrintGroup(r.Group("grpDetailHdr"));

                    //Print each item for the job site
                    decimal jsTotal = 0;
                    Dictionary<decimal,decimal> jsVatValues = new Dictionary<decimal,decimal>();
                    foreach (InvoiceItem rii in jsItems)
                    {
                        r.Group("grpDetail").FindTextItem("txtItemNo").Text = rii.Code;
                        r.Group("grpDetail").FindTextItem("txtItem").Text = rii.Name;
                        r.Group("grpDetail").FindTextItem("txtDesc").Text = rii.Description;
                        r.Group("grpDetail").FindTextItem("txtValue").Text = string.Format("{0:c}", rii.Value);
                        jsTotal += rii.Value;
                        if(jsVatValues.ContainsKey(rii.VatRate))
                           jsVatValues[rii.VatRate] += (rii.Value * (rii.VatRate / 100));
                        else
                            jsVatValues.Add(rii.VatRate,(rii.Value * (rii.VatRate / 100)));

                
                        if(invVatValues.ContainsKey(rii.VatRate))
                            invVatValues[rii.VatRate] += (rii.Value * (rii.VatRate / 100));
                        else
                            invVatValues.Add(rii.VatRate,(rii.Value * (rii.VatRate / 100)));

                        r.PrintGroup(r.Group("grpDetail"));

                        if (r.Height - r.Footer.Height - r.CurrentY < 25)
                        {
                            r.NewPage();
                        }
                    }
                    //Print the totals for the job site
                    r.Group("grpDetailTotals").FindTextItem("txtTotValue").Text = string.Format("{0:c}", jsTotal);
                    r.PrintGroup(r.Group("grpDetailTotals"));
                    decimal vatValue = 0;
                    foreach(KeyValuePair<decimal,decimal> pair in jsVatValues)
                    {
                        //r.Group("grpVatTotals").FindTextItem("lblVatDesc").Text = "VAT (" + pair.Key.ToString() + "%): ";
                        r.Group("grpVatTotals").FindTextItem("lblVatDesc").Text = "VAT (" + string.Format("{0:P0}", pair.Key/100) + "): ";
                        r.Group("grpVatTotals").FindTextItem("txtVatValue").Text = string.Format("{0:c}", pair.Value);
                        r.PrintGroup(r.Group("grpVatTotals"));
                        vatValue += pair.Value;
                    }

                    r.Group("grpGrossTotals").FindTextItem("txtTotVatValue").Text = string.Format("{0:c}", (vatValue + jsTotal));
                    r.PrintGroup(r.Group("grpGrossTotals"));
                    invTotal += jsTotal;
                }
            }
            //Print the schedule totals if there is more than one job site
            if(JobSites.Count > 1)
            {
                r.Group("grpDetailTotals").FindTextItem("txtTotValue").Text = string.Format("{0:c}", invTotal);
                r.PrintGroup(r.Group("grpDetailTotals"));
                decimal vatValue = 0;
                foreach(KeyValuePair<decimal,decimal> pair in invVatValues)
                {
                    r.Group("grpVatTotals").FindTextItem("lblVatDesc").Text = "VAT (" + string.Format("{0:P0}", pair.Key/100) + "): ";
                    r.Group("grpVatTotals").FindTextItem("txtVatValue").Text = string.Format("{0:c}", pair.Value);
                    r.PrintGroup(r.Group("grpVatTotals"));
                    vatValue += pair.Value;
                }

                r.Group("grpGrossTotals").FindTextItem("txtTotVatValue").Text = string.Format("{0:c}", (vatValue + invTotal));
                r.PrintGroup(r.Group("grpGrossTotals"));
            }

            if (settings.IsCopy)
            {
                //TODO: PRINT COPY LABEL
            }
            
            if (invSettings.IsPreview)
            {
                //TODO: DISPLAY PREVIEW IMAGE
            }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = OldCulture;
            }
        }

        protected override void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy)
        {
           throw new NotImplementedException();
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
