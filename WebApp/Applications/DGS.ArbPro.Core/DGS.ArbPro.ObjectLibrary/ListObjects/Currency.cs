﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class Currency : BusinessBase
    {
        #region Members
        int _id = 0;
        string _name = "";
        string  _cultureCode = "";
        string  _character = "";
        bool _selected;
        #endregion

        #region Constructors
        public Currency() : base() { }
        #endregion

        #region Properties

        /// <summary>
        /// The string value of the item
        /// </summary>
        /// 
        public int Id 
        {
            get { return _id; }
            set { _id = value; }
        
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// The total stock of the item held be the company
        /// </summary>
        public string CultureCode
        {
            get { return _cultureCode; }
            set { _cultureCode = value; }
        }
        public string Character
        {
            get { return _character; }
            set { _character = value; }
        }

        public bool Selected
        {
            get { return _selected; }
            set { _selected = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion

    }
}
