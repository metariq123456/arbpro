﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class WorkDescription : BusinessBase
    {
        #region Members
        string _Item = "";
        decimal _AverageHrsRequired = 0.00M;
        #endregion

        #region Constructors
        public WorkDescription() : base() { }
        #endregion

        #region Properties

        /// <summary>
        /// The string value of the item
        /// </summary>
        public string Item
        {
            get { return _Item; }
            set { _Item = value; }
        }

        /// <summary>
        /// The average number of hours required to complete the task
        /// </summary>
        public decimal AverageHrsRequired
        {
            get { return _AverageHrsRequired; }
            set { _AverageHrsRequired = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}