﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class TreeName : BusinessBase
    {
        #region Members
        string _Common = "";
        string _Latin = "";
        #endregion

        #region Constructors
        public TreeName() : base() { }
        #endregion

        #region Properties
        public string Common
        {
            get { return _Common; }
            set { _Common = value; }
        }

        public string Latin
        {
            get { return _Latin; }
            set { _Latin = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
