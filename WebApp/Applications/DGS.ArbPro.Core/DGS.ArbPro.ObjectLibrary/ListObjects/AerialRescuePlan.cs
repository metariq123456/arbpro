﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class AerialRescuePlan : BusinessBase
    {
        #region Members
        string _RescuePlan = "";
        #endregion

        #region Constructors
        public AerialRescuePlan() { }
        #endregion

        #region Properties
        /// <summary>
        /// The description of the Aerial rescue plan
        /// Must be 254 characters or less
        /// </summary>
        public string RescuePlan
        {
            get { return _RescuePlan; }
            set { _RescuePlan = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return string.Format("Id: {0}, RescuePlan: {1}", this.Id, this.RescuePlan);
        }
        #endregion
    }
}
