﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class Tool : BusinessBase
    {
        #region Members
        string _Item = "";
        int _Stock = 0;
        bool _Deleted;
        #endregion

        #region Constructors
        public Tool() : base() { }
        #endregion

        #region Properties

        /// <summary>
        /// The string value of the item
        /// </summary>
        public string Item
        {
            get { return _Item; }
            set { _Item = value; }
        }

        /// <summary>
        /// The total stock of the item held be the company
        /// </summary>
        public int Stock
        {
            get { return _Stock; }
            set { _Stock = value; }
        }

        public bool Deleted
        {
            get { return _Deleted; }
            set { _Deleted = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
