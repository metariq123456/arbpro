﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Types
{
    public class PickItem : BusinessBase
    {
        #region Members
        PickItemTypeEnum _ItemType = PickItemTypeEnum.Title;
        string _Value = "";
        #endregion

        #region Constructors
        public PickItem() : base() { }
        #endregion

        #region Properties
        /// <summary>
        /// The type of pick list this item belongs to
        /// </summary>
        public PickItemTypeEnum ItemType
        {
            get { return _ItemType; }
            set { _ItemType = value; }
        }

        /// <summary>
        /// The string value of the item
        /// </summary>
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
