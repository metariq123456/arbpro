﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using PDFReport;

namespace DGS.ArbPro.Types.Contracts
{
    public abstract class ReportBase : BusinessBase
    {
        #region Members
        //Record information
        int _RecordId = -1;

        //Template Specific
        string _TemplateText = "";

        //Client details
        Int32 _ClientID = -1;
        String _AccountNo = "";
        String _ClientName = "";
        String _ClientStreetNo = "";
        String _ClientAddress1 = "";
        String _ClientAddress2 = "";
        String _ClientTown = "";
        String _ClientCounty = "";
        String _ClientPostCode = "";
        string _SavePath = "";
        string _SaveFileName = "";
        string _ReferenceNo = "";
        //Site details
        string _SiteRef = "";
        string _SiteStreet = "";
        string _SiteAdd1 = "";
        string _SiteAdd2 = "";
        string _SiteTown = "";
        string _SiteCounty = "";
        string _SitePostCode = "";
        //Contact details
        String _ContactTitle = "";
        String _ContactFirstName = "";
        String _ContactSurname = "";
        String _ContactMobile = "";
        String _ContactTelephone = "";
        //Purchase order details
        string _PurchaseOrderNo = "";

        List<PDFReport.CustomGroup> _ReportCustomeGroups = null;

        private string DocumentName { get; set; }
        private Enums.ReportTypeEnum RptType { get; set; }
        #endregion

        #region Constructors
        public ReportBase(Enums.ReportTypeEnum rptType, string savePath, string documentName)
        {
            ReportCustomeGroups = new List<CustomGroup>();
            this.RptType = rptType;
            this.SavePath = savePath;
            this.DocumentName = documentName;
            this.TemplateText = "";
            this.SaveFileName = "";
        }
        #endregion

        #region Properties
        //Record information
        public int RecordId
        {
            get { return _RecordId; }
            set { _RecordId = value; }
        }

        //Template Specific
        public string TemplateText
        {
            get { return _TemplateText; }
            set { _TemplateText = value; }
        }

        //Client details
        public Int32 ClientID
        {
            get { return _ClientID; }
            set { _ClientID = value; }
        }
        public String AccountNo
        {
            get { return _AccountNo; }
            set { _AccountNo = value; }
        }
        public String ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }
        public String ClientStreetNo
        {
            get { return _ClientStreetNo; }
            set { _ClientStreetNo = value; }
        }
        public String ClientAddress1
        {
            get { return _ClientAddress1; }
            set { _ClientAddress1 = value; }
        }

        public String ClientAddress2
        {
            get { return _ClientAddress2; }
            set { _ClientAddress2 = value; }
        }

        public String ClientTown
        {
            get { return _ClientTown; }
            set { _ClientTown = value; }
        }

        public String ClientCounty
        {
            get { return _ClientCounty; }
            set { _ClientCounty = value; }
        }

        public String ClientPostCode
        {
            get { return _ClientPostCode; }
            set { _ClientPostCode = value; }
        }

        public string SavePath
        {
            get { return _SavePath; }
            set { _SavePath = value; }
        }

        public string SaveFileName
        {
            get { return _SaveFileName; }
            set { _SaveFileName = value; }
        }

        public string ReferenceNo
        {
            get { return _ReferenceNo; }
            set { _ReferenceNo = value; }
        }

        //Site details
        public string SiteRef
        {
            get { return _SiteRef; }
            set { _SiteRef = value; }
        }

        public string SiteStreet
        {
            get { return _SiteStreet; }
            set { _SiteStreet = value; }
        }

        public string SiteAdd1
        {
            get { return _SiteAdd1; }
            set { _SiteAdd1 = value; }
        }

        public string SiteAdd2
        {
            get { return _SiteAdd2; }
            set { _SiteAdd2 = value; }
        }

        public string SiteTown
        {
            get { return _SiteTown; }
            set { _SiteTown = value; }
        }

        public string SiteCounty
        {
            get { return _SiteCounty; }
            set { _SiteCounty = value; }
        }

        public string SitePostCode
        {
            get { return _SitePostCode; }
            set { _SitePostCode = value; }
        }

        //Contact details
        public String ContactTitle
        {
            get { return _ContactTitle; }
            set { _ContactTitle = value; }
        }

        public String ContactFirstName
        {
            get { return _ContactFirstName; }
            set { _ContactFirstName = value; }
        }

        public String ContactSurname
        {
            get { return _ContactSurname; }
            set { _ContactSurname = value; }
        }

        public String ContactMobile
        {
            get { return _ContactMobile; }
            set { _ContactMobile = value; }
        }

        public String ContactTelephone
        {
            get { return _ContactTelephone; }
            set { _ContactTelephone = value; }
        }
        public String PurchaseOrderNo
        {
            get { return _PurchaseOrderNo; }
            set { _PurchaseOrderNo = value; }
        }

        public List<PDFReport.CustomGroup> ReportCustomeGroups
        {
            get
            {
                if (_ReportCustomeGroups == null)
                    _ReportCustomeGroups = new List<CustomGroup>();

                return _ReportCustomeGroups;
            }
            set { _ReportCustomeGroups = value; }
        }

        #endregion

        #region Methods
        #region Public Static
        public static bool SaveStreamToFile(string outPutFilename, MemoryStream ms)
        {
            try
            {
                System.IO.File.WriteAllBytes(outPutFilename, ms.ToArray());
                ms.Close();
                return true;
            }
            catch (Exception ex)
            {
                //TODO: Log the exception to the log file
                Console.WriteLine("Failed to save memory stream to file: {0}", ex.ToString());
                return false;
            }
        }

        public static MemoryStream ReadStreamFromFile(string outPutFilename)
        {
            MemoryStream ms = new MemoryStream();
            try
            {
                using (FileStream file = new FileStream("file.bin", FileMode.Open, FileAccess.Read))
                {
                    byte[] bytes = new byte[file.Length];
                    file.Read(bytes, 0, (int)file.Length);
                    ms.Write(bytes, 0, (int)file.Length);
                }
            }
            catch (Exception ex)
            {
                //TODO: Log the exception to the log file
                Console.WriteLine("Failed to read memory stream from file: {0}", ex.ToString());
            }
            return ms;
        }

        #endregion
        #region Public
        /// <summary>
        /// Method to create a PDF document based on the settings passed in
        /// This method can raise an exception
        /// </summary>
        /// <param name="layoutPath">string: The path to the layout files to be used in the generation process</param>
        /// <param name="settings">Types.PdfSettings: The settings to be applied to the document</param>
        /// <returns>MemoryStream containing the PDF document</returns>
        public MemoryStream CreateDocument(string layoutPath, Types.PdfSettings settings)
        {
            try
            {
                this.SaveFileName = GetOutputFilenameFullPath(this.ClientID.ToString(), this.RptType, this.ReferenceNo, this.DocumentName);
                MemoryStream ms = new MemoryStream();

                Report r = new Report();
                r.Filename = this.SaveFileName;

                GeneratePDFDocument(ref r, layoutPath, settings);
                //r.SaveAndClose();
                r.SaveAndClose(ref ms);

                return ms;
            }
            catch (Exception ex)
            {
                throw new Exception(this.DocumentName + " Generation Failed", ex);
            }
        }

        public MemoryStream CreateImageDocument(string layoutPath, Types.PdfSettings settings, Byte[] workPic)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                Report r = new Report();
                r.Filename = "ItemImage_.pdf";

                r.LoadLayout(Path.Combine(layoutPath, "TpoWorkScheduleTemplate.xml"));
                Byte[] tickBytes = File.ReadAllBytes(Path.Combine(layoutPath, "Images\\Tick.png"));
                r.AddCustomGroup(Path.Combine(layoutPath, "ImageView.xml"));

                r.Group("ImageView")
                .FindImageItem("ItemImage")
                .SetImage(workPic, PDFReport.ResizePDFImage.KeepRatioWithinBox);

                r.PrintGroup(r.Group("ImageView"));


                r.SaveAndClose(ref ms);

                return ms;
            }
            catch (Exception ex)
            {
                throw new Exception(this.DocumentName + " Generation Failed", ex);
            }
        }



        /// <summary>
        /// Method to create a PDF document.
        /// This method can throw an exception that requires handling.
        /// </summary>
        /// <param name="layoutPath">string: The path to the layout files to be used in the generation process</param>
        /// <returns>MemoryStream containing the PDF document</returns>
        public MemoryStream CreateDocument(string layoutPath, bool isCopy)
        {
            try
            {
                this.SaveFileName = GetOutputFilenameFullPath(this.ClientID.ToString(), this.RptType, this.ReferenceNo, this.DocumentName);
                MemoryStream ms = new MemoryStream();

                Report r = new Report();
                r.Filename = this.SaveFileName;

                GeneratePDFDocument(ref r, layoutPath, isCopy);
                //r.SaveAndClose();
                r.SaveAndClose(ref ms);

                return ms;
            }
            catch (Exception ex)
            {
                throw new Exception(this.DocumentName + " Generation Failed", ex);
            }
        }

        /// <summary>
        /// Method to create a PDF document.
        /// This method can throw an exception that requires handling.
        /// </summary>
        /// <param name="templateText">string: The text that will be displayed in the letter body</param>
        /// <param name="layoutPath">string: The path to the layout files to be used in the generation process</param>
        /// <returns>MemoryStream containing the PDF document</returns>
        public MemoryStream CreateDocument(string templateText, string layoutPath, bool isCopy)
        {
            try
            {
                this.SaveFileName = GetOutputFilenameFullPath(this.ClientID.ToString(), this.RptType, this.RecordId.ToString(), this.DocumentName);
                MemoryStream ms = new MemoryStream();

                Report r = new Report();
                r.Filename = this.SaveFileName;

                this.TemplateText = templateText;
                GeneratePDFDocument(ref r, layoutPath, isCopy);

                r.SaveAndClose(ref ms);

                return ms;
            }
            catch (Exception ex)
            {
                throw new Exception(this.DocumentName + " Generation Failed", ex);
            }
        }

        /// <summary>
        /// Method to create a PDF document and Save it to the filesystem
        /// This method can throw an exception that requires handling
        /// </summary>
        /// <param name="layoutPath">string: The path to the layout files to be used in the generation process</param>
        /// <returns>string: The fully qulified path to the create document, returns an empty string if failed without an exception</returns>
        public string CreateAttachment(string layoutPath, bool isCopy)
        {
            string retVal = "";
            try
            {
                MemoryStream ms = CreateDocument(layoutPath, isCopy);
                if (Contracts.ReportBase.SaveStreamToFile(this.SaveFileName, ms))
                {
                    retVal = this.SaveFileName;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retVal;
        }

        /// <summary>
        /// Method to create a PDF document and Save it to the filesystem
        /// This method can throw an exception that requires handling
        /// </summary>
        /// <param name="templateText">string: The text that will be displayed in the letter body</param>
        /// <param name="layoutPath">string: The path to the layout files to be used in the generation process</param>
        /// <returns>string: The fully qulified path to the create document, returns an empty string if failed without an exception</returns>
        public string CreateAttachment(string templateText, string layoutPath, bool isCopy)
        {
            string retVal = "";
            try
            {
                MemoryStream ms = CreateDocument(templateText, layoutPath, isCopy);
                if (Contracts.ReportBase.SaveStreamToFile(this.SaveFileName, ms))
                {
                    retVal = this.SaveFileName;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return retVal;
        }
        #endregion
        #region Internal
        internal void LoadAdditionalLayouts(ref Report r, string layoutPath, PdfSettings settings)
        {

            if (settings.IsCopy)
            {
                r.AddCustomGroup(Path.Combine(layoutPath, "CopyOverlayLayout.xml"));
                //Todo: Load copy image
            }

            if (settings.IsPreview)
            {
                r.AddCustomGroup(Path.Combine(layoutPath, "PreviewOverlayLayout.xml"));
                //Todo: Load preview image
            }
        }
        internal void GenerateReportSection(ref Report r, string layoutPath, bool isCopy)
        {
            GeneratePDFDocument(ref r, layoutPath, isCopy);
        }

        internal void GenerateReportSection(ref Report r, string layoutPath, Types.PdfSettings settings)
        {
            GeneratePDFDocument(ref r, layoutPath, settings);
        }
        #endregion
        #region Protected
        #region Abstract
        protected abstract void GeneratePDFDocument(ref Report r, string layoutPath, Types.PdfSettings settings);
        protected abstract void GeneratePDFDocument(ref Report r, string layoutPath, bool isCopy);
        #endregion
        protected string GetOutputFilenameFullPath(string sClientID, Enums.ReportTypeEnum rptType, string id, string sType)
        {
            string filename = "";
            switch (rptType)
            {
                case Enums.ReportTypeEnum.Quote:
                    filename = Path.Combine(SavePath, "C" + sClientID + "_Quote" + id + "_" + sType + "_" + System.Guid.NewGuid() + ".pdf");
                    break;
                case Enums.ReportTypeEnum.Invoice:
                    filename = Path.Combine(SavePath, "C" + sClientID + "_Inv" + id + "_" + sType + "_" + System.Guid.NewGuid() + ".pdf");
                    break;
                case Enums.ReportTypeEnum.CreditNote:
                    filename = Path.Combine(SavePath, "C" + sClientID + "_CrdNote" + id + "_" + sType + "_" + System.Guid.NewGuid() + ".pdf");
                    break;
                case Enums.ReportTypeEnum.RiskAssessment:
                    filename = Path.Combine(SavePath, "C" + sClientID + "_Risk" + id + "_" + sType + "_" + System.Guid.NewGuid() + ".pdf");
                    break;
                case Enums.ReportTypeEnum.WorkProgramming:
                    filename = Path.Combine(SavePath, "C" + sClientID + "_WP" + id + "_" + sType + "_" + System.Guid.NewGuid() + ".pdf");
                    break;
                case Enums.ReportTypeEnum.WorkSchedule:
                    filename = Path.Combine(SavePath, "C" + sClientID + "_WSch" + id + "_" + sType + "_" + System.Guid.NewGuid() + ".pdf");
                    break;
                case Enums.ReportTypeEnum.MarketingMerge:
                    filename = Path.Combine(SavePath, "C" + sClientID + "_MailMerge" + id + "_" + sType + "_" + System.Guid.NewGuid() + ".pdf");
                    break;
            }
            return filename;
        }

        protected string GetSingleLineSiteAddress()
        {
            StringBuilder sb = new StringBuilder();
            bool hasPreviousField = false;
            if (SiteStreet != null && SiteStreet.Length > 0)
            {
                sb.Append(SiteStreet);
                hasPreviousField = true;
            }
            hasPreviousField = AppendField(sb, SiteAdd1, ", ", hasPreviousField);
            hasPreviousField = AppendField(sb, SiteAdd2, ", ", hasPreviousField);
            hasPreviousField = AppendField(sb, SiteTown, ", ", hasPreviousField);
            hasPreviousField = AppendField(sb, SiteCounty, ", ", hasPreviousField);
            hasPreviousField = AppendField(sb, SitePostCode, ", ", hasPreviousField);
            //The return item needs to be striped of possible legacy items
            return (sb.Replace("\n", ", ").Replace(",,", ", ").Replace(", , ", ", ").Replace("  ", " ")).ToString();
        }

        protected string GetSingleLineSiteAddress(JobSite js)
        {
            StringBuilder sb = new StringBuilder();
            bool hasPreviousField = false;
            hasPreviousField = AppendField(sb, js.Site.HouseNameNo, ", ", hasPreviousField);
            hasPreviousField = AppendField(sb, js.Site.Address1, ", ", hasPreviousField);
            hasPreviousField = AppendField(sb, js.Site.Address2, ", ", hasPreviousField);
            hasPreviousField = AppendField(sb, js.Site.Town, ", ", hasPreviousField);
            hasPreviousField = AppendField(sb, js.Site.County, ", ", hasPreviousField);
            hasPreviousField = AppendField(sb, js.Site.PostCode, ", ", hasPreviousField);
            //The return item needs to be striped of possible legacy items
            return (sb.Replace("\n", ", ").Replace(",,", ", ").Replace(", , ", ", ").Replace("  ", " ")).ToString();
        }

        protected string GetMultiLineSiteAddress()
        {
            StringBuilder sb = new StringBuilder();
            bool hasPreviousField = false;
            if (SiteStreet != null && SiteStreet.Length > 0)
            {
                sb.Append(SiteStreet);
                hasPreviousField = true;
            }
            hasPreviousField = AppendField(sb, SiteAdd1, ",\n", hasPreviousField);
            hasPreviousField = AppendField(sb, SiteAdd2, ",\n", hasPreviousField);
            hasPreviousField = AppendField(sb, SiteTown, ",\n", hasPreviousField);
            hasPreviousField = AppendField(sb, SiteCounty, ",\n", hasPreviousField);
            hasPreviousField = AppendField(sb, SitePostCode, ",\n", hasPreviousField);
            //The return item needs to be striped of possible legacy items
            return (sb.Replace(",,", ", ").Replace(",\n,", ",\n").Replace("  ", " ")).ToString();
        }

        protected string GetMultiLineSiteAddress(JobSite js)
        {
            StringBuilder sb = new StringBuilder();
            bool hasPreviousField = false;
            if (SiteStreet.Length > 3)
            {
                hasPreviousField = AppendField(sb, SiteAdd1, ",\n", hasPreviousField);
                hasPreviousField = AppendField(sb, SiteAdd1, ",\n", hasPreviousField);
            }
            else
            {
                hasPreviousField = AppendField(sb, SiteAdd1, " ", hasPreviousField);
                hasPreviousField = AppendField(sb, SiteAdd1, " ", hasPreviousField);
            }
            hasPreviousField = AppendField(sb, SiteAdd1, ",\n", hasPreviousField);
            hasPreviousField = AppendField(sb, SiteAdd2, ",\n", hasPreviousField);
            hasPreviousField = AppendField(sb, SiteTown, ",\n", hasPreviousField);
            hasPreviousField = AppendField(sb, SiteCounty, ",\n", hasPreviousField);
            hasPreviousField = AppendField(sb, SitePostCode, ",\n", hasPreviousField);
            //The return item needs to be striped of possible legacy items
            return (sb.Replace(",,", ", ").Replace(",\n,", ",\n").Replace("  ", " ")).ToString();
        }
        #endregion
        #region Private
        private bool AppendField(StringBuilder sb, string field, string seperator, bool useSeperator)
        {
            if (field != null && field.Length > 0)
            {
                if (useSeperator)
                    sb.Append(seperator);

                sb.Append(field);
                return true;
            }
            else
                return useSeperator;
        }
        #endregion
        #endregion
    }
}
