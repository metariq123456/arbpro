﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public abstract class WorkFlowBase : BusinessBase
    {
        /*
         * The Work-Flow is
         *      Create a Job
         *      Select a Site
         *      Create a Quotation for the site
         *      Send the Quote  
         *      Accept Quote
         *      Quote Items are available for completion
         *      Completed Items become available for invoicing
         */

        #region Members
        int _ClientId = -1;
        int _JobId = -1;
        int _ContactId = -1;
        ContactSearchResult _Contact = null;
        ClientSite _Site = null;
        string _ClientName = "";
        int _Status = 0;
        bool _Locked = false;
        string _Reference = "";
        #endregion

        #region Constructors
        //Default Constructor
        public WorkFlowBase() : base() { }

        /// <summary>
        /// Optional paramater to set the status of the object
        /// </summary>
        /// <param name="status">int - The int value of the typed status enum</param>
        public WorkFlowBase(int status,bool locked) : base()
        {
            _Status = status;
            _Locked = locked;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The Id of the client the work flow is for
        /// </summary>
        public int ClientId
        {
            get { return _ClientId; }
            set { _ClientId = value; }
        }

        /// <summary>
        /// The Id of the Job for the client
        /// </summary>
        public int JobId
        {
            get { return _JobId; }
            set { _JobId = value; }
        }

        /// <summary>
        /// The Id the of the contact
        /// </summary>
        public int ContactId
        {
            get
            {
                if (this.PointOfContact != null && this.PointOfContact.Id > 0 )
                    return this.PointOfContact.Id;
                else
                    return _ContactId;
            }
            set { _ContactId = value; }
        }

        /// <summary>
        /// Flag to show if the WorkFlow item can be modified
        /// Returns true if a item has been, or all items completed sent
        /// </summary>
        public virtual bool Locked
        {
            get { return _Locked; }
        }

        /// <summary>
        /// The name or business name of the Client
        /// </summary>
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        /// <summary>
        /// The contact for the WF item
        /// </summary>
        public ContactSearchResult PointOfContact
        {
            get 
            {
                if(_Contact == null)
                    _Contact = new ContactSearchResult();
                
                return _Contact;
            }
            set 
            { 
                _Contact = value; 
                //Ensure the contact Id is updated
                //Legacy - The contact Id is being referenced in places it should not be, and may not have been correctly set
                if (_Contact != null)
                    this.ContactId = _Contact.Id;
            }
        }

        /// <summary>
        /// Protected value that is cast to and from the corisponding status
        /// </summary>
        protected int Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        /// <summary>
        /// The Client site the Job is for
        /// </summary>
        public ClientSite Site
        {
            get { return _Site; }
            set
            {
                _Site = value;
            }
        }

        public string Reference
        {
            get { return _Reference; }
            set { _Reference = value; }
        }
        #endregion

        #region Methods
        #endregion
    }
}
