﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{


    /// <summary>
    /// Base type of all documents that can be stored to, or retreaved from the database
    /// </summary>
    public abstract class DocumentBase : BusinessBase
    {
        #region Members
        DateTime _LastSentDate = DateTime.MinValue;
        DateTime _CreatedDate = DateTime.MinValue;
        byte[] _Document = null;
        Enums.ReportTextTemplateMessageEnum _Method = Enums.ReportTextTemplateMessageEnum.Letter;
        #endregion

        #region Constructors
        public DocumentBase() : base() { }
        #endregion

        #region Properties
        public DateTime LastSentDate
        {
            get { return _LastSentDate; }
            set { _LastSentDate = value; }
        }

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        public byte[] Document
        {
            get { return _Document; }
            set { _Document = value; }
        }

        public Enums.ReportTextTemplateMessageEnum Method
        {
            get { return _Method; }
            set { _Method = value; }
        }
        #endregion

        #region Methods

        #endregion
    }
}
