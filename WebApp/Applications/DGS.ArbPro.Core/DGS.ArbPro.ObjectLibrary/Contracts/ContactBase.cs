﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{


    /// <summary>
    /// Base type of all contacts that can be stored to, or retreaved from the database
    /// </summary>
    public abstract class ContactBase : BusinessBase
    {
        #region Members
        private string _Title = "";
        private string _ForeName = "";
        private string _Surname = "";
        private string _BusinessName = "";
        private string _TelephoneNumber = "";
        private string _MobileNumber = "";
        private string _EmailAddress = "";
        private string _StreetNumber = "";
        private string _Address1 = "";
        private string _Address2 = "";
        private string _Town = "";
        private string _County = "";
        private string _PostCode = "";
        private string _JobTitle = "";
        private string _Department = "";
        //V3.7.0
        private bool _SendSmsMessage = true;
        #endregion

        #region Constructors
        public ContactBase() : base() { }
        #endregion

        #region Properties


        /// <summary>
        /// The contacts title (Mr, Mrs, Miss etc)
        /// </summary>
        public string Title
        {
            get { return _Title; }
            set { _Title = value; }
        }

        /// <summary>
        ///The contacts first name
        /// </summary>
        public string Forename
        {
            get { return _ForeName; }
            set { _ForeName = value; }
        }

        /// <summary>
        /// The contacts surname
        /// </summary>
        public string Surname
        {
            get { return _Surname; }
            set { _Surname = value; }
        }

        /// <summary>
        /// The contacts buseness name
        /// </summary>
        public string BusinessName
        {
            get { return _BusinessName; }
            set { _BusinessName = value; }
        }

        /// <summary>
        /// The contacts telephone number
        /// </summary>
        public string Telephone
        {
            get { return _TelephoneNumber; }
            set { _TelephoneNumber = value; }
        }

        /// <summary>
        /// The contacts mobile telephone number
        /// </summary>
        public string Mobile
        {
            get { return _MobileNumber; }
            set { _MobileNumber = value; }
        }

        /// <summary>
        /// The contacts email address
        /// </summary>
        public string Email
        {
            get { return _EmailAddress; }
            set { _EmailAddress = value; }
        }


        public string StreetNumber
        {
            get { return _StreetNumber; }
            set { _StreetNumber = value; }
        }

        /// <summary>
        /// The contacts address
        /// </summary>
        public string Address1
        {
            get { return _Address1; }
            set { _Address1 = value; }
        }

        public string Address2
        {
            get { return _Address2; }
            set { _Address2 = value; }
        }

        public string Town
        {
            get { return _Town; }
            set { _Town = value; }
        }

        public string County
        {
            get { return _County; }
            set { _County = value; }
        }
        /// <summary>
        /// The contacts postcode
        /// </summary>
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        /// <summary>
        /// The job title of the contact
        /// </summary>
        public string JobTitle
        {
            get { return _JobTitle; }
            set { _JobTitle = value; }
        }

        /// <summary>
        /// The contacts deparment
        /// </summary>
        public string Department
        {
            get { return _Department; }
            set { _Department = value; }
        }

        //V3.7.0
        public bool SendSmsMessage
        {
            get { return _SendSmsMessage; }
            set { _SendSmsMessage = value; }
        }

        public string FormatedName
        {
            get { return FormatName(); }
        }
        #endregion

        #region Methods
        protected virtual string FormatName()
        {
            StringBuilder sb = new StringBuilder();
            //Check if there is a business name to be returned
            if (BusinessName.Length > 0)
            {
                sb.Append(BusinessName);
                //Check if there is a department - else use the title and last name
                if (Department.Length > 0)
                {
                    sb.Append(" ");
                    sb.Append(Department);
                }
                else
                {
                    sb.Append(" ");
                    sb.Append(Title);
                    sb.Append(" ");
                    sb.Append(Surname);
                }
            }
            else
            {
                sb.Append(" ");
                sb.Append(Title);
                sb.Append(" ");
                sb.Append(Forename);
                sb.Append(" ");
                sb.Append(Surname);
            }
            //Replace multiple white space with a single white space
            return sb.Replace("   ", " ").Replace("  ", " ").ToString();
        }
        #endregion
    }
}
