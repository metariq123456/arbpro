﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public abstract class CalendarBase : BusinessBase
    {
        #region Members
        private DateTime _StartDateTime = DateTime.MinValue;
        private DateTime _EndDateTime = DateTime.MinValue;
        private String _CalendarString = "";
        #endregion

        #region Constructors
        #endregion

        #region Properties
        public DateTime StartDateTime
        {
            get { return _StartDateTime; }
            set { _StartDateTime = value; }
        }

        public DateTime EndDateTime
        {
            get { return _EndDateTime; }
            set { _EndDateTime = value; }
        }

        public String CalendarString
        {
            get { return _CalendarString; }
            set { _CalendarString = value; }
        }
        #endregion
    }
}
