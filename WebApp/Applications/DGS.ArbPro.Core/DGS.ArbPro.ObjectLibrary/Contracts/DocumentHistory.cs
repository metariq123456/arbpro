﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    /// <summary>
    /// Base type of all History objects that can be created from Quotes, Invoices of Credit Notes
    /// </summary>
    public abstract class DocumentHistory : BusinessBase
    {
        #region Members
        int _RecordId = -1;
        string _ReferenceNo = "";
        string _EmailReference = "";
        Enums.ReportTypeEnum _ItemType = Enums.ReportTypeEnum.Quote;
        Enums.DocDispatchMethodEnum _DispatchMethod = Enums.DocDispatchMethodEnum.None;
        DateTime _CreationDate = DateTime.Today;
        DateTime _SentDate = DateTime.MinValue;
        MemoryStream _Doc = null;
        #endregion

        #region Constructors
        public DocumentHistory(Enums.ReportTypeEnum type) : base() 
        {
            _ItemType = type;
        }
        #endregion

        #region Properties
        public abstract string DocTypeString
        {
            get;
            set;
        }

        /// <summary>
        /// The Identity of the record the doument is for
        /// </summary>
        public int RecordId
        {
            get { return _RecordId; }
            set { _RecordId = value; }
        }

        /// <summary>
        /// The reference number of the document.
        /// Can be an empty string
        /// </summary>
        public string ReferenceNo
        {
            get { return _ReferenceNo; }
            set { _ReferenceNo = value; }
        }

        /// <summary>
        /// The reference received from the email service provider when the document is sent via email.
        /// This value can be used to obtain the status of the email.
        /// Can be an empty string
        /// </summary>
        public string EmailReference
        {
            get { return _EmailReference; }
            set { _EmailReference = value; }
        }

        /// <summary>
        /// Enum for how the document was sent
        /// </summary>
        public Enums.DocDispatchMethodEnum DispatchMethod
        {
            get { return _DispatchMethod; }
            set { _DispatchMethod = value; }
        }

        public string DispatchMethodString
        {
            get
            {
                switch (DispatchMethod)
                {
                    case Enums.DocDispatchMethodEnum.None:
                        return "None";
                    case Enums.DocDispatchMethodEnum.Email:
                        return "Email";
                    case Enums.DocDispatchMethodEnum.Letter:
                        return "Letter";
                    default:
                        return "None";
                }
            }
        }

        /// <summary>
        /// The type of item the report is based on (Quote, Invoice etc)
        /// </summary>
        public Enums.ReportTypeEnum ItemType
        {
            get { return _ItemType; }
        }

        public string ItemTypeString
        {
            get
            {
                switch (ItemType)
                {
                    case Enums.ReportTypeEnum.Quote:
                        return "Quote";
                    case Enums.ReportTypeEnum.Invoice:
                        return "Invoice";
                    case Enums.ReportTypeEnum.MarketingMerge:
                        return "Marketing Merge";
                    case Enums.ReportTypeEnum.CreditNote:
                        return "Credit Note";
                    case Enums.ReportTypeEnum.RiskAssessment:
                        return "Risk Assessment";
                    case Enums.ReportTypeEnum.WorkProgramming:
                        return "Work Programming";
                    case Enums.ReportTypeEnum.WorkSchedule:
                        return "Work Schedule";
                    default:
                        return "";
                }
            }
        }

        /// <summary>
        /// The date the document was created
        /// </summary>
        public DateTime CreationDate
        {
            get { return _CreationDate; }
            set { _CreationDate = value; }
        }

        /// <summary>
        /// The date the document was sent
        /// </summary>
        public DateTime SentDate
        {
            get { return _SentDate; }
            set { _SentDate = value; }
        }

        /// <summary>
        /// The memory stream containing the PDF document
        /// </summary>
        public MemoryStream Doc
        {
            get { return _Doc; }
            set { _Doc = value; }
        }
        #endregion

        #region Methods
        public Types.UserDocument ToEmailAttachment(string name)
        {
            return Types.UserDocument.ToUserDocument(name, this.Doc);
        }

        #endregion

        #region Comparer
        public class Comparer : IComparer<DocumentHistory>
        {
            public Comparer()
                : base()
            {

            }

            public int Compare(DocumentHistory x, DocumentHistory y)
            {
                return -x.CreationDate.CompareTo(y.CreationDate);
            }
        }
        #endregion
    }
}
