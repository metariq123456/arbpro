﻿using System;
using System.Reflection; 
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Data;

using System.Data.SqlTypes;

namespace DGS.ArbPro.Types
{
    /// <summary>
    /// Base type of all objects that can be stored to, or retreaved from the database
    /// </summary>
    public abstract class BusinessBase
    {
        /// <summary>
        /// Static log for logging
        /// </summary>
        protected static readonly ILog Log = LogManager.GetLogger("DGS.ArbPro.BL");

        #region Members
        int _Id = -1;
        #endregion

        #region Constructors
        //Default constructor
        public BusinessBase() { }
        #endregion

        #region Properties
        /// <summary>
        /// The Id of the record in the Database
        /// </summary>
        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }
        #endregion

        #region Methods
        #region Abstract
        public abstract override string ToString(); 
        #endregion
        #region Proptected
        /// <summary>
        /// Protected method to get the type, property names and values from an object as a string
        /// </summary>
        /// <param name="obj">object</param>
        /// <returns>string</returns>
        protected string GetObjectState(object obj)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(obj.GetType().ToString());
                sb.Append("-");
                var properties = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                foreach (var p in properties)
                {
                    sb.Append(p.Name);
                    sb.Append(":");
                    var value = p.GetValue(obj, null);
                    sb.Append(value == null? "Null" : value.ToString());
                    sb.Append(Environment.NewLine);
                }

                return sb.ToString();
            }
            catch
            {
                return "Error: Failed to evaluate object.";
            }
        }
        #endregion
        #region Private
        /// <summary>
        /// Static method to get a list of properties for an object using reflection
        /// </summary>
        /// <param name="obj">object</param>
        /// <returns>PropertyInfo[]</returns>
        private static PropertyInfo[] GetProperties(object obj)
        {
            return obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
        }



        #endregion
        #endregion
    }
}
