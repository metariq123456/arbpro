﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace DGS.ArbPro.Types
{
    public abstract class UserBase : BusinessBase
    {

        protected const String PasswordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).{8,100}$";
        protected const int MaxAuthFailures = 3;

        public string Salt;
        public String PasswordHashed;
        public String EmailAddress;
        public String FirstName;
        public String LastName;
        public Int32 AuthenticationFailures;
        public Boolean IsOnline;
        public DateTime LastLoginDate;
        public Guid UserGuid;
        public DateTime GuidExpirationDate;

        public virtual String NameStr
        {
            get
            {
                StringBuilder str = new StringBuilder();
                str.Append(FirstName == null ? "" : FirstName);
                str.Append(" ");
                str.Append(LastName == null ? "" : LastName);
                return str.Replace("  ", " ").ToString().Trim();
            }
        }

        #region Public Methods
        /// <summary>
        /// Returns true if the password, when hashed with the salt, matches the
        /// hashed value stored for the user.
        /// <para>
        /// If the password does not match, the number of authentication failures is incremented,
        /// otherwise it is set to 0.
        /// </para>
        /// </summary>
        /// <param name="Password">The password to test</param>
        /// <returns>True on success, false on failure</returns>
        public Boolean PasswordMatch(String Password)
        {
            bool ReturnValue;

            ReturnValue = (PasswordHashed == HashPassword(Password));

            if (ReturnValue)
            {
                AuthenticationFailures = 0;
            }
            else
            {
                AuthenticationFailures++;
            }
            return ReturnValue;
        }

        /// <summary>
        /// Tests the password against the regex of allowed password formats.
        /// </summary>
        /// <param name="Password">The password to test</param>
        /// <returns>True if the password matches the regex, otherwise false.</returns>
        public static Boolean PassesRegex(String Password)
        {
            return Regex.IsMatch(Password, PasswordRegex);
        }

        /// <summary>
        /// Hashes the password (using the salt stored in the object).
        /// </summary>
        /// <param name="password">The password to hash.</param>
        /// <param name="regenerateSalt">If set to true, generates a new salt before hashing.</param>
        /// <returns>The hashed password.</returns>
        public string HashPassword(string password, bool regenerateSalt = false)
        {
            try
            {
                if (regenerateSalt)
                {
                    this.Salt = CreateSalt();
                }

                // Create a new instance of the hash crypto service provider.
                HashAlgorithm hashAlg = new SHA256CryptoServiceProvider();

                // Convert the data to hash to an array of Bytes - note we're using the salt here too
                byte[] bytValue = System.Text.Encoding.UTF8.GetBytes(password + this.Salt);

                // Compute the Hash. This returns an array of Bytes.
                byte[] bytHash = hashAlg.ComputeHash(bytValue);

                // Convert the hash value to a base64-encoded string, 
                string base64 = Convert.ToBase64String(bytHash);

                return base64;
            }
            catch (Exception ex)
            {
                var message = string.Format("Could not hash password - exception {0}", ex.ToString());
                Log.ErrorFormat(message);
                throw new Exception(message);
            }
        }

        public void Unlock()
        {
            AuthenticationFailures = 0;
        }

        public bool Locked
        {
            get
            {
                return AuthenticationFailures >= MaxAuthFailures;
            }
        }
        #endregion

        #region Private methods

        /// <summary>
        /// Creates a cryptographically random string to use as a salt.
        /// </summary>
        /// <returns></returns>
        private static string CreateSalt()
        {
            // Generate a cryptographic random number using the cryptographic 
            // service provider
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[30];
            rng.GetBytes(buff);

            // Return a Base64 string representation of the random number
            return Convert.ToBase64String(buff);
        }
        #endregion

    }
}
