﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public abstract class Item : BusinessBase
    {
        #region  members
        int _JobId = -1;
        int _JobSiteId = -1;
        int _JobItemId = -1;
        int _TPOId = -1;
        int _OrderId = -1;

        decimal _Value = 0.00M;
        decimal _VatRate = 0.00M;
        string _Code = "";
        string _Name = "";
        string _Description = "";
        string _ImageGuid = "";
        double _Latitude = 0;
        double _Longitude = 0;
        Enums.ItemStatusEnum _Status = Enums.ItemStatusEnum.None;
        Enums.TreeProtectionStatusEnum _PreservationStatus = Enums.TreeProtectionStatusEnum.Unknown;
        #endregion

        #region Constructors
        public Item() : base() { }

        public Item(Item itm): base() 
        {
            _JobId = itm.JobId;
            _JobSiteId = itm.JobSiteId;
            _JobItemId = itm.JobItemId;
            _TPOId = itm.TPOId;
            _Value = itm.Value;
            _VatRate = itm.VatRate;
            _Code = itm.Code;
            _Name = itm.Name;
            _Description = itm.Description;
            _Latitude = itm.Latitude;
            _Longitude = itm.Longitude;
        }
        #endregion

        #region Properties


        /// <summary>
        /// The Job Id of the Item
        /// </summary>
        public int JobId
        {
            get { return _JobId; }
            set { _JobId = value; }
        }

        /// <summary>
        /// The Job site Id of the Item
        /// </summary>
        public int JobSiteId
        {
            get { return _JobSiteId; }
            set { _JobSiteId = value; }
        }

        /// <summary>
        /// The Id of the relaited Job Item
        /// </summary>
        public int JobItemId
        {
            get { return _JobItemId; }
            set { _JobItemId = value; }
        }

        /// <summary>
        /// The Id for the Tree Protection Order
        /// </summary>
        public int TPOId 
        {
            get { return _TPOId; }
            set { _TPOId = value; }
        }

        /// <summary>
        /// The value of the Item Exculding VAT
        /// </summary>
        public decimal Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        /// <summary>
        /// The VAT to be applied to the item, as a decimal
        /// </summary>
        public decimal VatRate
        {
            get { return _VatRate; }
            set { _VatRate = value; }
        }

        /// <summary>
        /// The code of the item eg T1
        /// </summary>
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        /// <summary>
        /// The name of the type of item
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// The description of the work to be carried out
        /// </summary>
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        /// <summary>
        /// The Geo potional Latitude value
        /// </summary>
        public double Latitude
        {
            get { return _Latitude; }
            set { _Latitude = value; }
        }

        /// <summary>
        /// The guid for any picture associated to the item.
        /// Primarily used for upload.
        /// </summary>
        public string ImageGuid
        {
            get { return _ImageGuid; }
            set { _ImageGuid = value; }
        }

        /// <summary>
        /// The Geo potional Longitude value
        /// </summary>
        public double Longitude
        {
            get { return _Longitude; }
            set { _Longitude = value; }
        }

        /// <summary>
        /// The current status of the Item
        /// </summary>
        public Enums.ItemStatusEnum Status
        {
            get { return _Status; }
            set { _Status = value; }
        }

        /// <summary>
        /// The Id for the Quote Item Order
        /// </summary>
        public int OrderId
        {
            get { return _OrderId; }
            set { _OrderId = value; }
        }

        /// <summary>
        /// Preservation status of the Item
        /// </summary>
        public Enums.TreeProtectionStatusEnum PreservationStatus
        {
            get { return _PreservationStatus; }
            set { _PreservationStatus = value; }
        }
        #endregion

        #region Methods
        public Types.JobItem ToJobItem()
        {
            JobItem jb = new JobItem();
            jb.Id = this.Id;
            jb.JobId = this.JobId;
            jb.JobSiteId = this.JobSiteId;
            jb.Code = this.Code;
            jb.Name = this.Name;
            jb.Description = this.Description;
            jb.Value = this.Value;
            jb.VatRate = this.VatRate;
            jb.Status = this.Status;
            jb.PreservationStatus = this.PreservationStatus;
            jb.Latitude = this.Latitude;
            jb.Longitude = this.Longitude;
            jb.OrderId = this.OrderId;
            return jb;
        }
        #endregion
    }
}
