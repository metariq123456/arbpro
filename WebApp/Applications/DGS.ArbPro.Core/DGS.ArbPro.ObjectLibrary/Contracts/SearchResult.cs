﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public abstract class SearchResult : BusinessBase
    {
        #region Members
        private int _ParentId = -1;
        private decimal _Value = 0.00M;
        private string _Reference = "Pending";
        #endregion

        #region Properties
        public int ParentId
        {
            get { return _ParentId; }
            set { _ParentId = value; }
        }

        public decimal Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        /// <summary>
        /// The document reference for the object type
        /// This is to be used everywhere the item is to be identified
        /// </summary>
        public string Reference
        {
            get { return _Reference; }
            set { _Reference = value; }
        }

        public String Title
        {
            get { return TitleGet(); }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        public String Detail
        {
            get { return DetailGet(); }
            set { /*NOTE: JSON serializer requires setter*/ }
        }


        public String SideBarDetail
        {
            get { return SideBarDetailsGet(); }
            set { /*NOTE: JSON serializer requires setter*/ }
        }
        #endregion

        #region Abstract Properties
        public abstract Enums.SearchTypeEnum SearchType { get; set; }
        #endregion

        #region Abstract Methods
        public abstract string TitleGet();
        public abstract string DetailGet();
        public abstract string SideBarDetailsGet();
        #endregion
    }
}