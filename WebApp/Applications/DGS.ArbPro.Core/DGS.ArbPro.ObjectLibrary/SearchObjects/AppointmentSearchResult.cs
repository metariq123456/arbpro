﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public partial class AppointmentSearchResult : SearchResult
    {
        #region Members
        DateTime _AppointmentDateTime = DateTime.MinValue;
        Enums.AppointmentTypeEnum _AppointmentType = Enums.AppointmentTypeEnum.Generic;
        string _AppointmentFor = "";
        string _AppointmentWith = "";
        string _Details = "";
        int _JobID = -1;
        #endregion

        #region Constructors
        public AppointmentSearchResult() { }
        #endregion

        #region Properties
        /// <summary>
        /// The type of search result this object represents
        /// </summary>
        public override Enums.SearchTypeEnum SearchType
        {
            get{ return Enums.SearchTypeEnum.Appointment;}
        }

        /// <summary>
        /// The date and time of the appointment
        /// </summary>
        public DateTime AppointmentDateTime
        {
            get { return _AppointmentDateTime; }
            set { _AppointmentDateTime = value; }
        }

        /// <summary>
        /// Who the appoinment is for. This may be a staff member of work crew
        /// </summary>
        public string AppointmentFor
        {
            get { return _AppointmentFor; }
            set { _AppointmentFor = value; }
        }

        /// <summary>
        /// The person who the appoinment is with. This would be a name for a Sales appointment,
        /// or an address for a work appointment
        /// </summary>
        public string AppointmentWith
        {
            get { return _AppointmentWith; }
            set { _AppointmentWith = value; }
        }

        /// <summary>
        /// Any pertinant details for the appoinment.
        /// ie enquiry details or quote/work details.
        /// </summary>
        public string Details
        {
            get { return _Details; }
            set { _Details = value; }
        }

        public int JobID
        {
            get { return _JobID; }
            set { _JobID = value; }
        }
        #endregion

        #region Methods
        #region Public Overrides
        /// <summary>
        /// A method that concatinates the appoinment date, time, type and for properties into a single value that
        /// can be used for the title of a search result item.
        /// </summary>
        /// <returns>String</returns>
        public override string TitleGet()
        {
            return string.Format("{0}: {1} Appointment at {2} for {3}", _AppointmentDateTime.ToShortDateString(), _AppointmentType.ToString(), _AppointmentDateTime.ToShortTimeString(), _AppointmentFor);
        }

        /// <summary>
        /// A method that concatinates who the appointment is with and the appoinment details into a single string that can be used as additional information for a search result.
        /// </summary>
        /// <returns>String</returns>
        public override string DetailGet()
        {
            return string.Format("{0} - {1}",_AppointmentWith, _Details);
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("");
        }
        #endregion
        #endregion
    }
}
