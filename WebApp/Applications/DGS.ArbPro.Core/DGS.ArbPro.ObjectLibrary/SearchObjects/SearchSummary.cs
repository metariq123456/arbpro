﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    /// <summary>
    /// Summary class for search result items.
    /// Calculates value based properties for the seach results
    /// Primarily used for items such as dashboards
    /// </summary>
    public class SearchSummary
    {
        #region Members
        string _Title = "";
        string _DefaultString = "No results to display.";
        string _AdditionalText = "";
        List<SearchResult> _Results = null;
        private bool _ShowResultCount = true;
        string _CultureCode = "en-GB";
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor required for serialization
        /// </summary>
        public SearchSummary()
        {
        }

        /// <summary>
        /// Construct that takes a title for the summary result set;
        /// </summary>
        /// <param name="title">The title to be applied to the results, will be appended with (n) where n is the number of results</param>
        public SearchSummary(string title) 
        {
            _Title = title;
        }
        /// <summary>
        /// Optional constructor to set the summary title and the default text that is returned when there are no results
        /// </summary>
        /// <param name="title">The title to be applied to the results, will be appended with (n) where n is the number of results</param>
        /// <param name="strDefault">The string that will replace the title if there are no results</param>
        public SearchSummary(string title, string strDefault)
        {
            _Title = title;
            _DefaultString = strDefault;
        }


        /// <summary>
        /// Optional constructor to set the title and denote if the number of results should also be displayed
        /// </summary>
        /// <param name="title">The string to be displayed</param>
        /// <param name="showResultCount">flag to denote if the number of results should be displayed in (): true for yes</param>
        public SearchSummary(string title, bool showResultCount)
        {
            _Title = title;
            _ShowResultCount = showResultCount;
        }


        #endregion

        #region Properties
        /// <summary>
        /// Read only property to get the title for the summary result set
        /// </summary>
        public string Title
        {
            get
            {
                if (!_ShowResultCount)
                    return _Title;
                else if (this.Results.Count > 0)
                    return string.Format("{0} ({1})",_Title, _Results.Count);
                else
                    return _DefaultString;
            }
        }
        /// <summary>
        /// Additional text that can be added to the summary
        /// This value defaults to the calculated value of the result set
        /// </summary>
        public string AdditionalText
        {
            get { return _AdditionalText; }
            set { _AdditionalText = value; }
        }

        /// <summary>
        /// Read only property to access the result set of the summary
        /// </summary>
        public List<SearchResult> Results
        {
            get
            {
                if (_Results == null)
                    _Results = new List<SearchResult>();

                return _Results;
            }
        }
        #endregion

        #region Methods
        public void SetSearchResults(List<SearchResult> results)
        {
            _Results = results;      
            CalculateSummaryValue();
        }
        public void SetSearchResultsWithCultureCode(List<SearchResult> results)
        {
            _Results = results;
            var cultureCode = ((WorkSearchResult)_Results.LastOrDefault()).CultureCode;
            _CultureCode = cultureCode;
            //List<SearchResult> NewResults = (from t in _Results select t).OfType<SearchResult>().ToList();
            //List<WorkSearchResult> NewWorkResults = (from t in NewResults from p in t select p).OfType<WorkSearchResult>().ToList();
            CalculateSummaryValue();
        }
        public void SetSearchResultsWithCultureCodeForInvoice(List<SearchResult> results)
        {
            _Results = results;
            var cultureCode = ((InvoiceSearchResult)_Results.LastOrDefault()).CultureCode;
            _CultureCode = cultureCode;
            //List<SearchResult> NewResults = (from t in _Results select t).OfType<SearchResult>().ToList();
            //List<WorkSearchResult> NewWorkResults = (from t in NewResults from p in t select p).OfType<WorkSearchResult>().ToList();
            CalculateSummaryValue();
        }
        public void SetSearchResultsWithCultureCodeForQuote(List<SearchResult> results)
        {
            _Results = results;
            var cultureCode = ((QuoteSearchResult)_Results.LastOrDefault()).CultureCode;
            _CultureCode = cultureCode;
            //List<SearchResult> NewResults = (from t in _Results select t).OfType<SearchResult>().ToList();
            //List<WorkSearchResult> NewWorkResults = (from t in NewResults from p in t select p).OfType<WorkSearchResult>().ToList();
            CalculateSummaryValue();
        }




        public void SetSearchResults(List<AppointmentSearchResult> results)
        {
            this.Results.AddRange(results);
            _AdditionalText = "";
        }

        public void SetSearchResults(List<SearchResult> results, bool calculateValue)
        {
            this.Results.AddRange(results);
            if (calculateValue)
            {
                CalculateSummaryValue();
            }
            else
            {
                _AdditionalText = "";
            }
        }

        public void SetSearchResults(List<AppointmentSearchResult> results, bool calculateValue)
        {
            this.Results.AddRange(results);
            if (calculateValue)
            {
                CalculateSummaryValue();
            }
            else
            {
                _AdditionalText = "";
            }
        }

        private void CalculateSummaryValue()
        {
            var OldCulture = Thread.CurrentThread.CurrentCulture;
            try
            {

                Thread.CurrentThread.CurrentCulture = new CultureInfo(_CultureCode, false);

            decimal value = 0;
            foreach (var r in this.Results)
            {
                value += r.Value;
            }
            if (value > 0)
                _AdditionalText = string.Format("Total Value: {0:C}", value);
            else
                _AdditionalText = "";
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = OldCulture;
            }
        }
        #endregion
    }
}
