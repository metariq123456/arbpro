﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public partial class ClientSearchResult : SearchResult
    {
        #region Members
        string _Name = "";
        string _Address = "";
        string _Telephone = "";
        string _Email = "";
        int _QuoteCount = 0;
        int _jobId = 0;
        Enums.ClientTypeEnum _ClientType = Enums.ClientTypeEnum.Residential;
        #endregion

        #region Constructors
        //Default Constructor
        public ClientSearchResult() { }
        #endregion

        #region Properties
        /// <summary>
        /// The type of search result this object represents
        /// </summary>
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Client; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        /// <summary>
        /// The full title firstname and surname or company name of the client
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// The main address of the client
        /// </summary>
        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }

        /// <summary>
        /// The main address of the client
        /// </summary>
        public string Telephone
        {
            get { return _Telephone; }
            set { _Telephone = value; }
        }

        /// <summary>
        /// The main address of the client
        /// </summary>
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        /// <summary>
        /// The Total number of Quotes for the client
        /// </summary>
        public int QuoteCount
        {
            get { return _QuoteCount; }
            set { _QuoteCount = value; }
        }

        /// <summary>
        /// The type for the client
        /// </summary>
        public Enums.ClientTypeEnum ClientType
        {
            get { return _ClientType; }
            set { _ClientType = value; }
        }

        public string ClientTypeString
        {
            get
            {
                return _ClientType.ToString();
            }
        }
        #endregion

        #region Methods
        #region Public Overrides
        /// <summary>
        /// Method to format the client no and name to be used as the title for a search result
        /// </summary>
        /// <returns>String</returns>
        public override string TitleGet()
        {
            return string.Format("Client No:{0} - {1}",this.Id,_Name);
        }

        /// <summary>
        /// Method to format the client type and address to be used as the detail for a search result
        /// </summary>
        /// <returns></returns>
        public override string DetailGet()
        {
            return string.Format("Client type: {0} Address: {1}",_ClientType.ToString(),_Address);
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("");
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
