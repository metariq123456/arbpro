﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace DGS.ArbPro.Types
{
    public class JobSearchResult: SearchResult
    {
        #region Members
        string _CultureCode = "en-GB";
        string _ClientName = "";
        string _SiteAddress= "";
        string _ContactName = "";
        string _Comments = "";
        DateTime dateCreated = DateTime.MinValue;
        Enums.JobStatusEnum _CurrentStatus = Enums.JobStatusEnum.Pending;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor, results in decimal values displayed in en-GB currency format
        /// </summary>
        public JobSearchResult():base() { }

        /// <summary>
        /// Constructor to set the culture code for decimal to currency display
        /// </summary>
        /// <param name="cultureCode">Country code for currency format, as a string</param>
        public JobSearchResult(string cultureCode)
            : base()
        {
            _CultureCode = cultureCode;
        }
        #endregion

        #region Properties
        
        /// <summary>
        /// The date created for the quote (if exists)
        /// Note that it can be empty
        /// </summary>
        public DateTime DateCreated
        {
            get { return this.dateCreated; }
            set { this.dateCreated = value; }
        }

        /// <summary>
        /// The type of search result this object represents
        /// </summary>
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Job; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        /// <summary>
        /// The name of the Client the Quote is for
        /// </summary>
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        /// <summary>
        /// The name of the Client the Quote is for
        /// </summary>
        public string SiteAddress
        {
            get { return _SiteAddress; }
            set { _SiteAddress = value; }
        }

        /// <summary>
        /// The contact name for the job
        /// </summary>
        public string ContactName
        {
            get { return _ContactName; }
            set { _ContactName = value; }
        }

        public string Comments
        {
            get 
            {
                if (_Comments.Length == 0)
                    _Comments = "No additional details.";

                return _Comments; 
            }
            set { _Comments = value; }
        }

        public Enums.JobStatusEnum Status
        {
            get { return _CurrentStatus; }
            set { _CurrentStatus = value; }
        }
        #endregion

        #region Methods
        #region Public Overrides
        /// <summary>
        /// Method to format the job number, and client name into a search result title
        /// </summary>
        /// <returns>String</returns>
        public override string TitleGet()
        {
            return string.Format("Job No: {0}, Client Name: {1}",this.Id,_ClientName);
        }

        /// <summary>
        /// Method to format the status, value, number of items and site address into the details for a search result
        /// </summary>
        /// <returns></returns>
        public override string DetailGet()
        {
            string dateCreatedStr = this.dateCreated == DateTime.MinValue ? "N/A" : this.dateCreated.ToShortDateString() + " (<font color=\"red\">" + (DateTime.Now - this.dateCreated).Days + " Day(s) Ago</font>)";

            return string.Format("Status: {0}, Job Date Created: {1}, Comments: {2}", _CurrentStatus, dateCreatedStr, _Comments);
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("<b>Job No:</b> {0}<br><b>Client:</b> {1}<br><b>Site Address:</b> {2}<br><b>Status:</b> {3}<br><b>Comments:</b> {4}", this.Id, _ClientName, _SiteAddress, _CurrentStatus, _Comments);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
