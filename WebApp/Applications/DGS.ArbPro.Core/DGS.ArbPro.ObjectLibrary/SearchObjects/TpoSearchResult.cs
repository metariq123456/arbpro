﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class TpoSearchResult : SearchResult
    {
        #region Members
        int _JobId = -1;
        Enums.TreeProtectionProgreesEnum _PorgressStage = Enums.TreeProtectionProgreesEnum.No_Application;
        DateTime _SixWeekDate = DateTime.MinValue;
        DateTime _EightWeekDate = DateTime.MinValue;
        DateTime _ApplicationDate = DateTime.MinValue;
        string _Reference = "";
        string _ClientName = "";
        #endregion

        #region Construtors

        #endregion

        #region Properties
        public int JobId
        {
            get { return _JobId; }
            set { _JobId = value; }
        }

        public Enums.TreeProtectionProgreesEnum PorgressStage
        {
            get { return _PorgressStage; }
            set { _PorgressStage = value; }
        }

        public DateTime SixWeekDate
        {
            get { return _SixWeekDate; }
            set { _SixWeekDate = value; }
        }

        public DateTime EightWeekDate
        {
            get { return _EightWeekDate; }
            set { _EightWeekDate = value; }
        }

        public DateTime ApplicationDate
        {
            get { return _ApplicationDate; }
            set { _ApplicationDate = value; }
        }

        public string Reference
        {
            get { return _Reference; }
            set { _Reference = value; }
        }

        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        #endregion

        #region Methods
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.TPO; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        public override string TitleGet()
        {
            if (this.ApplicationDate == DateTime.MinValue)
                return string.Format("Quote No: {0}, N/A - {1}", this.Reference, this.ClientName);
            return string.Format("Quote No: {0}, {1} - {2}",this.Reference, this.ApplicationDate.ToShortDateString(), this.ClientName);
        }

        public override string DetailGet()
        {
            if (this.SixWeekDate == DateTime.MinValue && this.EightWeekDate == DateTime.MinValue)
                return string.Format("Six week date: {0}\n, Eight week date: {1}\n, Application stage: {2}", "N/A", "N/A", this.PorgressStage.ToString().Replace("_", " "));
            else if (this.SixWeekDate == DateTime.MinValue)
                return string.Format("Six week date: {0}\n, Eight week date: {1}\n, Application stage: {2}", "N/A", this.EightWeekDate.ToShortDateString(), this.PorgressStage.ToString().Replace("_", " "));
            else if (this.EightWeekDate == DateTime.MinValue)
                return string.Format("Six week date: {0}\n, Eight week date: {1}\n, Application stage: {2}", this.SixWeekDate.ToShortDateString(), "N/A", this.PorgressStage.ToString().Replace("_", " "));

            return string.Format("Six week date: {0}\n, Eight week date: {1}\n, Application stage: {2}", this.SixWeekDate.ToShortDateString(), this.EightWeekDate.ToShortDateString(), this.PorgressStage.ToString().Replace("_", " "));
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("{0}<br>Six week date: {1}<br>Eight week date: {2}<br>Application stage: {3}", this.Title, this.SixWeekDate.ToShortDateString(), this.EightWeekDate.ToShortDateString(), this.PorgressStage.ToString().Replace("_", " "));
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
