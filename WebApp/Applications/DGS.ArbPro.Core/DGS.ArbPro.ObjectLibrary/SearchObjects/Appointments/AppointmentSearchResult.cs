﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public abstract class AppointmentSearchResult : SearchResult
    {
        #region Members
        DateTime _StartDateTime = DateTime.MinValue;
        DateTime _EndDateTime = DateTime.MinValue;
        Enums.AppointmentTypeEnum _AppointmentType = Enums.AppointmentTypeEnum.Generic;
        string _StaffName = "";
        #endregion

        #region Constructors
        public AppointmentSearchResult(Enums.AppointmentTypeEnum appType)
        {
            _AppointmentType = appType;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The type of search result this object represents
        /// </summary>
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Appointment; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        /// <summary>
        /// The date and time of the appointment
        /// </summary>
        public DateTime StartDateTime
        {
            get { return _StartDateTime; }
            set { _StartDateTime = value; }
        }

        /// <summary>
        /// The end date and time of the appointment
        /// </summary>
        public DateTime EndDateTime
        {
            get { return _EndDateTime; }
            set { _EndDateTime = value; }
        }

        public string StaffName
        {
            get { return _StaffName; }
            set { _StaffName = value; }
        }
        #endregion

        #region Methods
        public override string TitleGet()
        {
            throw new NotImplementedException();
        }

        public override string DetailGet()
        {
            throw new NotImplementedException();
        }

        public override string SideBarDetailsGet()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
