﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class QuoteAppointmentSearch : AppointmentSearchResult
    {
        #region Mebers
        string _ClientName = "";
        string _SiteAddress = "";
        #endregion

        #region Constructors
        public QuoteAppointmentSearch() : base(Enums.AppointmentTypeEnum.Sales) { }
        #endregion

        #region Properties
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        public string SiteAddress
        {
            get { return _SiteAddress; }
            set { _SiteAddress = value; }
        }
        #endregion

        #region methods
        #region Overrides
        public override string TitleGet()
        {
            return string.Format("Sales - {0} at {1}: {2}", this.StartDateTime.ToShortDateString(), this.StartDateTime.ToShortTimeString(), this.ClientName);
        }

        public override string DetailGet()
        {
            return string.Format("Surveyor:{0}\nSite:{1}", this.StaffName, this.SiteAddress);
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("{0} at {1}: {2}<br>Surveyor: {3}<br>Site:{4}",
                this.StartDateTime.ToShortDateString(),
                this.StartDateTime.ToShortTimeString(),
                this.ClientName,
                this.StaffName,
                this.SiteAddress);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
