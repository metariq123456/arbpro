﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class GenericAppointmentSearch : AppointmentSearchResult
    {
        #region Members
        int _EventType = -1;
        bool _AllDayEvent = false;
        #endregion

        #region Constructors
        public GenericAppointmentSearch() : base(Enums.AppointmentTypeEnum.Generic) { }
        #endregion

        #region Properties
        /// <summary>
        /// The type of generic appointment event
        /// </summary>
        public int EventType
        {
            get { return _EventType; }
            set { _EventType = value; }
        }

        /// <summary>
        /// Flag to state if the appointment is all day
        /// </summary>
        public bool IsAllDayEvent
        {
            get { return _AllDayEvent; }
            set { _AllDayEvent = value; }
        }
        #endregion

        #region methods
        #region Overrides
        public override string TitleGet()
        {
            if (this.IsAllDayEvent)
                return string.Format("{0}{1}", this.StartDateTime.ToShortDateString(), string.IsNullOrWhiteSpace(this.StaffName) ? "" : " for " + this.StaffName);
            else
                return string.Format("{0} at {1}{2}", this.StartDateTime.ToShortDateString(), this.StartDateTime.ToShortTimeString(), string.IsNullOrWhiteSpace(this.StaffName) ? "" : " for " + this.StaffName);
        }

        public override string DetailGet()
        {
            if (this.IsAllDayEvent)
                return string.Format("{0}: All day.",this.EventType.ToString());
            else
                return string.Format("{0} until {1} on {2}.", this.EventType.ToString(), this.EndDateTime.ToShortTimeString(),  this.EndDateTime.ToShortDateString());
        }

        public override string SideBarDetailsGet()
        {
            if (this.IsAllDayEvent)
                return string.Format("{0}:{1}<br> On {2} All day.", this.EventType.ToString(), this.StaffName, this.StartDateTime.ToShortDateString());
            else
                return string.Format("{0}:{1}<br> On {2} at {3}<br> Until {4} on {5}", 
                    this.EventType.ToString(), 
                    this.StaffName, 
                    this.StartDateTime.ToShortDateString(), 
                    this.StartDateTime.ToShortTimeString(), 
                    this.EndDateTime.ToShortTimeString(), 
                    this.EndDateTime.ToShortDateString());
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
