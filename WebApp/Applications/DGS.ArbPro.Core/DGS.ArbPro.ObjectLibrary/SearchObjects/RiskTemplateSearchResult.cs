﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class RiskTemplateSearchResult : SearchResult
    {
        #region Members
        string _Name = "";
        string _Reference = "";
        string _Description = "";
        #endregion

        #region Constructors

        #endregion

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        public string Reference
        {
            get { return _Reference; }
            set { _Reference = value; }
        }

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }


        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.RiskTemplate; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Method to format the client no and name to be used as the title for a search result
        /// </summary>
        /// <returns>String</returns>
        public override string TitleGet()
        {
            return string.Format("Template Name: {0}",_Name);
        }

        /// <summary>
        /// Method to format the client type and address to be used as the detail for a search result
        /// </summary>
        /// <returns></returns>
        public override string DetailGet()
        {
            return string.Format("Description: {0}",_Description);
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("Template Name: {0}<br>{1}", _Name, _Description);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion

    }
}
