﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace DGS.ArbPro.Types
{
    public partial class InvoiceSearchResult : SearchResult
    {
        #region Members
        string _CultureCode = "en-GB";
        int _QuoteNumber = 0;
        string _QuoteReference = "Pending";
        string _InvoiceDate = "";
        string _PaymentDate = "";
        string _InvoiceAddress = "";
        DateTime dateSent = DateTime.MinValue;
        DateTime dateCreated = DateTime.MinValue;
        private string tpoStatus = string.Empty;
        Enums.InvoiceStatusEnum _CurrentStatus = Enums.InvoiceStatusEnum.Sent;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor, results in decimal values displayed in en-GB currency format
        /// </summary>
        public InvoiceSearchResult():base() { }

        /// <summary>
        /// Constructor to set the culture code for decimal to currency display
        /// </summary>
        /// <param name="cultureCode">Country code for currency format, as a string</param>
        public InvoiceSearchResult(string cultureCode):base()
        {
            _CultureCode = cultureCode;
        }
        #endregion

        #region Properties

        /// <summary>
        /// The date sent for the quote (if exists)
        /// Note that it can be empty
        /// </summary>
        public DateTime DateSentDate
        {
            get { return this.dateSent; }
            set { this.dateSent = value; }
        }
        public string CultureCode
        {
        get
        {
            return this._CultureCode;
        }
        }


        /// <summary>
        /// The date created for the quote (if exists)
        /// Note that it can be empty
        /// </summary>
        public DateTime DateCreated
        {
            get { return this.dateCreated; }
            set { this.dateCreated = value; }
        }

        /// <summary>
        /// TPO status string
        /// </summary>
        public string TpoStatus
        {
            get { return this.tpoStatus; }
            set { this.tpoStatus = value; }
        }

        /// <summary>
        /// The type of search result this object represents
        /// </summary>
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Invoice; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        /// <summary>
        /// The Quote Id that the invoice is for
        /// </summary>
        public int QuoteNumber
        {
            get { return _QuoteNumber; }
            set { _QuoteNumber = value; }
        }

        /// <summary>
        /// The total value of the Invoice
        /// </summary>
        public decimal TotalValue
        {
            get { return base.Value; }
            set { base.Value = value; }
        }

        /// <summary>
        /// The alpha numeric invoice identifier
        /// </summary>
        public string InvoiceReference
        {
            get { return base.Reference; }
            set { base.Reference = value; }
        }

        /// <summary>
        /// The date the invoice was sent
        /// </summary>
        public string DateSent
        {
            get { return _InvoiceDate; }
            set { _InvoiceDate = value; }
        }

        /// <summary>
        /// The date payment was due
        /// </summary>
        public string DateDue
        {
            get { return _PaymentDate; }
            set { _PaymentDate = value; }
        }

        /// <summary>
        /// The address the invoice was addressed to
        /// </summary>
        public string QuoteReference
        {
            get { return _QuoteReference; }
            set { _QuoteReference = value; }
        }

        /// <summary>
        /// The address the invoice was addressed to
        /// </summary>
        public string InvoiceAddress
        {
            get { return _InvoiceAddress; }
            set { _InvoiceAddress = value; }
        }

        /// <summary>
        /// The current status of the invoice
        /// </summary>
        public Enums.InvoiceStatusEnum Status
        {
            get { return _CurrentStatus; }
            set { _CurrentStatus = value; }
        }
        #endregion

        #region Methods
        #region Public Overrides
        /// <summary>
        /// Method to format the invoice number, value and date into a search result title
        /// </summary>
        /// <returns>String</returns>
        public override string TitleGet()
        {
            if (this.Status == Enums.InvoiceStatusEnum.Pending)
            {
                return string.Format("Quote No: {0}, Completed work to be invoiced: {1}", this.QuoteReference ,this.TotalValue.ToString("C", CultureInfo.CreateSpecificCulture(_CultureCode)));
            }
            else
            {
                return string.Format("Invoice No: {0}, Value: {1}, Due Date: {2}", this.InvoiceReference, this.TotalValue.ToString("C", CultureInfo.CreateSpecificCulture(_CultureCode)), _PaymentDate);
            }
        }

        /// <summary>
        /// Method to format the quote number, current status and invoice address into the detail of a search result
        /// </summary>
        /// <returns>String</returns>
        public override string DetailGet()
        {
            if (this.Status == Enums.InvoiceStatusEnum.Pending)
            {
                string dateCreatedStr = this.dateCreated == DateTime.MinValue ? "N/A" : this.dateCreated.ToShortDateString() + " (<font color=\"red\">" + (DateTime.Now - this.dateCreated).Days + " Day(s) Ago</font>)";

                return string.Format("Status: Invoice Pending, Send To: {0}, Invoice Date Created: {1}", _InvoiceAddress, dateCreatedStr);
            }
            else
            {
                string dateCreatedStr = this.dateCreated == DateTime.MinValue ? "N/A" : this.dateCreated.ToShortDateString() + " (<font color=\"red\">" + (DateTime.Now - this.dateCreated).Days + " Day(s) Ago</font>)";

                return string.Format("Invoice for Quote No: {0}, Invoice Date Created: {1}, Status: {2}, Sent To: {3}", this.QuoteReference, dateCreatedStr, _CurrentStatus, _InvoiceAddress);
            }
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("");
        }


        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
