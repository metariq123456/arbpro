﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class ComplaintSearchResult: SearchResult
    {
        #region Members
        DateTime _DateOfComplaint = DateTime.MinValue;
        string _Complainant = "";
        string _AllocatedTo = "";
        string _ComplaintType = "";
        string _Detail = "";
        bool _Closed = false;
        #endregion

        #region Constructors
        public ComplaintSearchResult() : base() { }
        #endregion

        #region Properties
        #region Overrides
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Complaint; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }
        #endregion

        public DateTime DateOfComplaint
        {
            get { return _DateOfComplaint; }
            set { _DateOfComplaint = value; }
        }

        public string Complainant
        {
            get { return _Complainant; }
            set { _Complainant = value; }
        }

        public string AllocatedTo
        {
            get { return _AllocatedTo; }
            set { _AllocatedTo = value; }
        }

        public string ComplaintType
        {
            get { return _ComplaintType; }
            set { _ComplaintType = value; }
        }

        public string ComplaintDetail
        {
            get { return _Detail; }
            set 
            {
                if (value.Length > 30)
                    _Detail = string.Format("{0}...", value.Substring(0, 27));
                else
                    _Detail = value;
            }
        }

        public bool Closed
        {
            get { return _Closed; }
            set { _Closed = value; }
        }
        #endregion

        #region Methods
        #region Overrides

        public override string TitleGet()
        {
            return string.Format("{0}-{1}: {2}", this.DateOfComplaint.ToShortDateString(), this.ComplaintType, this.Complainant);
        }

        public override string DetailGet()
        {
            return string.Format("Allocated To: {0}{1}{2}", this.AllocatedTo, Environment.NewLine, this.ComplaintDetail);
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("{0}-{1}<br>Complainant: {2}<br>Allocated To: {3}<br>{4}",this.DateOfComplaint.ToShortDateString(), this.ComplaintType,this.Complainant, this.AllocatedTo,this.ComplaintDetail);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
