﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class EmailHistorySearchResult : SearchResult
    {
        #region Members
        private string _Sender = "";
        private string _Recipient = "";
        private string _Status = "";
        private string _ClientName = "";
        private string _MsgId = "";
        private string _DocRef = "Not Set";
        private int _DocumentType = 0;
        private DateTime _SentDate = DateTime.MinValue;
        #endregion

        #region Constructors
        #endregion

        #region Properties
        public string Sender
        {
            get {return _Sender;}
            set { _Sender = value; }
        }

        public string Recipient
        {
            get {return _Recipient;}
            set { _Recipient = value; }
        }

        public DateTime SentDate
        {
            get {return _SentDate;}
            set { _SentDate = value; }
        }

        public string Status
        {
            get {return _Status;}
            set { _Status = value; }
        }

        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        public string MsgId
        {
            get {return _MsgId;}
            set { _MsgId = value; }
        }

        public string DocRef
        {
            get { return _DocRef; }
            set { _DocRef = value; }
        }

        public int DocumentType
        {
            get { return _DocumentType; }
            set { _DocumentType = value; }
        }
        #endregion

        #region Methods
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.EmailHistory; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        public override string TitleGet()
        {
            return string.Format("Date: {0}, Recipient: {1}",this.SentDate.ToShortDateString(), this.Recipient);
        }

        public override string DetailGet()
        {
            return string.Format("Status: {0}{1}Message: {2}", this.Status,Environment.NewLine, this.DocRef);
        }

        public override string SideBarDetailsGet()
        {
            //Check the type of email
            string typeString = "";
            switch (this.DocumentType)
            {
                case 0:
                    typeString = "Quote";
                    break;
                case 1:
                    typeString = "Invoice";
                    break;
                case 2:
                    typeString = "Work Pack";
                    break;
            }
            return string.Format("<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td>", this.SentDate.ToShortDateString(), this.ClientName, typeString, this.DocRef, this.Recipient, this.Value.ToString("c"), this.Status);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
