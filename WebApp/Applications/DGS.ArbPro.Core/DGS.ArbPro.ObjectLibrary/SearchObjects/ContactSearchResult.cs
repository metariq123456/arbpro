﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public partial class ContactSearchResult : SearchResult
    {
        #region Members
        string _Name = "";
        string _Email = "";
        string _Telephone = "";
        string _Mobile = "";
        string _ClientName = "";
        int _jobID = 0;
        int _ClientId = 0;
        Enums.ClientTypeEnum _ClientType = Enums.ClientTypeEnum.Residential;
        Enums.ContactTypeEnum _ContactType = Enums.ContactTypeEnum.General;
        int _ContactTypeId = -1;
        private bool _SendSmsMessage = true;
        private bool _IncludeInMarketing = true;
        #endregion

        #region Constructors
        //Default Constructor
        public ContactSearchResult() { }
        #endregion

        #region Properties
        /// <summary>
        /// The type of search result this object represents
        /// </summary>
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Contact; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        /// <summary>
        /// The full name of the contact
        /// </summary>
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// The full name of the client to which the contact is associated
        /// </summary>
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        /// <summary>
        /// The email details of the contact
        /// </summary>
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        /// <summary>
        /// The Telephone details of the contact
        /// </summary>
        public string Telephone
        {
            get { return _Telephone; }
            set { _Telephone = value; }
        }

        /// <summary>
        /// The Telephone details of the contact
        /// </summary>
        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }

        /// <summary>
        /// The record Id for the associated client
        /// </summary>
        public int ClientId
        {
            get { return _ClientId; }
            set { _ClientId = value; }
        }

        /// <summary>
        /// The sms message preference for the associated client
        /// </summary>
        public bool SendSmsMessage
        {
            get { return _SendSmsMessage; }
            set { _SendSmsMessage = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether include in marketing.
        /// </summary>
        public bool IncludeInMarketing
        {
            get { return this._IncludeInMarketing; }
            set { this._IncludeInMarketing = value; }
        }

        /// <summary>
        /// The type of client the contact is associated with
        /// </summary>
        public Enums.ClientTypeEnum ClientType
        {
            get { return _ClientType; }
            set { _ClientType = value; }
        }

        /// <summary>
        /// The description of the contact type entry int the ContactType table
        /// </summary>
        public Enums.ContactTypeEnum ContactType
        {
            get { return _ContactType; }
            set { _ContactType = value; }
        }

        public String ContactTypeString
        {
            get
            {
                switch (ContactType)
                {
                    case Enums.ContactTypeEnum.Default:
                        return "Default";
                    case Enums.ContactTypeEnum.General:
                        return "General";
                    case Enums.ContactTypeEnum.Supplier:
                        return "Supplier";
                    case Enums.ContactTypeEnum.Contractor:
                        return "Contractor";
                    case Enums.ContactTypeEnum.AlternateInvoice:
                        return "Alternative Invoice";
                }
                return "General";
            }
            set { }
        }

        /// <summary>
        /// The ID of the contact type entry int the ContactType table
        /// </summary>
        public int ContactTypeId
        {
            get { return _ContactTypeId; }
            set { _ContactTypeId = value; }
        }
        
        /// <summary>
        /// A simple representation of the contact with just name and email. Useful
        /// for contact selection lists etc.
        /// </summary>
        public string NameAndEmail
        {
            get { return _Name + " - " + _Email; }
        }
        #endregion

        #region Methods
        #region Public Overrides
        /// <summary>
        /// Method to format the client no and name to be used as the title for a search result
        /// </summary>
        /// <returns>String</returns>
        public override string TitleGet()
        {
            return string.Format("Client No:{0} Contact Name: {1}", _ClientId, _Name);
        }

        /// <summary>
        /// Method to format the client type and address to be used as the detail for a search result
        /// </summary>
        /// <returns></returns>
        public override string DetailGet()
        {
            return string.Format("Client type: {0}, Client Name: {1}, Email: {2}, Telephone: {3}, Mobile: {4}", _ClientType.ToString(), _ClientName, _Email, _Telephone, _Mobile);
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("Name: {0}, Email Address: {1}, Type: {2}", _Name, _Email, this.ContactTypeString);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
