﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class DocumentHistorySearchResult : SearchResult
    {
        #region members
        String _RefNo;
        DateTime _DateCreated;
        DateTime _DateSent;
        Types.Enums.DocDispatchMethodEnum _DispatchMethod;
        #endregion

        #region consturctors
        public DocumentHistorySearchResult() : base() { }
        #endregion

        #region properties
        #region overrides
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Document; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }
        #endregion

        public String RefNo
        {
            get { return _RefNo; }
            set { _RefNo = value; }
        }

        public DateTime DateCreated
        {
            get { return _DateCreated; }
            set { _DateCreated = value; }
        }

        public DateTime DateSent
        {
            get { return _DateSent; }
            set { _DateSent = value; }
        }

        public Enums.DocDispatchMethodEnum DispatchMethod
        {
            get { return _DispatchMethod; }
            set { _DispatchMethod = value; }
        }
        #endregion

        #region methods
        #region overrides
        public override string TitleGet()
        {
            return string.Format("{0}", this.RefNo);
        }

        public override string DetailGet()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Date Created: {0} - Dispatch Method: {1}", 
                DateCreated > new DateTime(1900,1,1) ? DateCreated.ToShortDateString() : "N/A", 
                DispatchMethod.ToString());
            if (DateSent > new DateTime(1753, 01, 01))
            {
                sb.AppendFormat(" - Date Sent: {0}",
                    DateSent> new DateTime(1900, 1, 1) ? DateSent.ToShortDateString() : "N/A");
            }
            return sb.ToString();
        }

        public override string SideBarDetailsGet()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("{0}", this.RefNo);
            sb.AppendFormat("Date Created: {0}<br>Dispatch Method: {1}",
                DateCreated > new DateTime(1900, 1, 1) ? DateCreated.ToShortDateString() : "N/A", 
                DispatchMethod.ToString());
            if (DateSent > new DateTime(1753, 01, 01))
            {
                sb.AppendFormat("<br>Date Sent: {0}",
                    DateSent > new DateTime(1900, 1, 1) ? DateSent.ToShortDateString() : "N/A");
            }
            return sb.ToString();
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}
