﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace DGS.ArbPro.Types
{
    public partial class FinanceSearchResult: SearchResult
    {
        #region Members

        string _CultureCode = "en-GB";
        private string _Heading = "";
        #endregion
        #region Constructors
        public FinanceSearchResult(string cultureCode) 
        {
            _CultureCode = cultureCode;
        }
        public FinanceSearchResult()
        {
            _CultureCode = "en-GB";
        }
        #endregion
        #region Properties

        public string Heading
        {
            get { return _Heading; }
            set { _Heading = value; }
        }

        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Finance; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        #endregion

        #region Methods
        public override string TitleGet()
        {
            return this.Heading;
        }


        public override string DetailGet()
        {
            return this.Value.ToString("C", CultureInfo.CreateSpecificCulture(_CultureCode));
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("<tr><td>{0}</td><td>{1}</td></tr>", this.Heading,
                this.Value.ToString("C", CultureInfo.CreateSpecificCulture(_CultureCode)));
        }

        public override string ToString()
        {
            return this.GetObjectState(this);
        }
        #endregion
    }
}
