﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace DGS.ArbPro.Types
{
    public class WorkSearchResult: SearchResult
    {
        #region Members
        string _CultureCode = "en-GB";
        int _ItemCount = 0;
        string _ClientName = "";
        string _ContactDTL = "";
        int _QuoteId = 0;
        int _SiteId = 0;
        string _SiteAddress = "";
        DateTime dateCreated = DateTime.MinValue;
        private string tpoStatus = string.Empty;
        Enums.WorkStatusEnum _CurrentStatus = Enums.WorkStatusEnum.Pending;
        int notesPresent = 0;
        int _ClientSiteId = -1;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor, results in decimal values displayed in en-GB currency format
        /// </summary>
        public WorkSearchResult():base() { }

        /// <summary>
        /// Constructor to set the culture code for decimal to currency display
        /// </summary>
        /// <param name="cultureCode">Country code for currency format, as a string</param>
        public WorkSearchResult(string cultureCode)
            : base()
        {
            _CultureCode = cultureCode;
        }
        #endregion

        #region Properties
        public string CultureCode 
        {
            get
            {
                return this._CultureCode;
            }
           
        }

        /// <summary>
        /// The date created for the quote (if exists)
        /// Note that it can be empty
        /// </summary>
        public DateTime DateCreated
        {
            get { return this.dateCreated; }
            set { this.dateCreated = value; }
        }

        /// <summary>
        /// TPO status string
        /// </summary>
        public string TpoStatus
        {
            get { return this.tpoStatus; }
            set { this.tpoStatus = value; }
        }

        /// <summary>
        /// Amounts of notes related to work object
        /// </summary>
        public int NotesPresent
        {
            get { return this.notesPresent; }
            set { this.notesPresent = value; }
        }

        /// <summary>
        /// The type of search result this object represents
        /// </summary>
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Work; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        /// <summary>
        /// The number of items included in the Quote
        /// </summary>
        public int WorkItemCount
        {
            get { return _ItemCount; }
            set { _ItemCount = value; }
        }

        /// <summary>
        /// The total value of the Quote inc VAT
        /// </summary>
        public decimal QuoteValue
        {
            get { return base.Value; }
            set { base.Value = value; }
        }

        /// <summary>
        /// The name of the Client the Quote is for
        /// </summary>
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        /// <summary>
        /// The Contact Detail of the Client the Quote is for
        /// </summary>
        public string ContactDTL
        {
            get { return _ContactDTL; }
            set { _ContactDTL = value; }
        }

        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }


        /// <summary>
        /// The Site address for the Quote
        /// </summary>
        public string SiteAddress
        {
            get { return _SiteAddress; }
            set { _SiteAddress = value; }
        }

        public int SiteId
        {
            get { return _SiteId; }
            set { _SiteId = value; }
        }

        public int ClientSiteId
        {
            get { return _ClientSiteId; }
            set { _ClientSiteId = value; }
        }
        /// <summary>
        /// The current status of the quote
        /// </summary>
        public Enums.WorkStatusEnum Status
        {
            get { return _CurrentStatus; }
            set { _CurrentStatus = value; }
        }
        #endregion

        #region Methods
        #region Public Overrides
        /// <summary>
        /// Method to format the quote number, and client name into a search result title
        /// </summary>
        /// <returns>String</returns>
        public override string TitleGet()
        {
            return string.Format("Quote No:{0}, Client Name: {1}",this.Reference,_ClientName);
        }

        /// <summary>
        /// Method to format the status, value, number of items and site address into the details for a search result
        /// </summary>
        /// <returns></returns>
        public override string DetailGet()
        {
            string notes = this.notesPresent > 0 ? "Yes" : "No";

            string dateCreatedStr = this.dateCreated == DateTime.MinValue ? "N/A" : this.dateCreated.ToShortDateString() + " (<font color=\"red\">" + (DateTime.Now - this.dateCreated).Days + " Day(s) Ago</font>)";

            return string.Format(
                "Status: {0}, Value: {1}, Items: {2}, Site Address: {3}, Work Date Created: {4}, Trees Protected: {5}, Notes: {6}",
                _CurrentStatus.ToString(),
                this.QuoteValue.ToString("C", CultureInfo.CreateSpecificCulture(_CultureCode)),
                _ItemCount,
                _SiteAddress,
                dateCreatedStr,
                this.tpoStatus,
                notes);
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("Quote No: {0}<br/>{1}", this.Reference, _SiteAddress);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}