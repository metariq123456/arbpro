﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
    public class MapDetailsSearchResult : SearchResult
    {
        #region Members
        string _QuoteRef = "";
        string _ClientName = "";
        string _ContactName = "";
        string _Telephone = "";
        string _Mobile = "";
        string _SiteRef = "";
        string _SiteAddress = "";
        string _SitePostCode = "";
        List<QuoteItem> _SiteItems = null;
        #endregion


        #region Constructors
        /// <summary>
        /// Default constructor, results in decimal values displayed in en-GB currency format
        /// </summary>
        public MapDetailsSearchResult() : base() { }
        #endregion

        #region Properties
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Map; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        /// <summary>
        /// The Quote reference for the Quote.
        /// This will be the numeric record value if no
        /// quote documents have been generated
        /// </summary>
        public string QuoteRef
        {
            get{return _QuoteRef;}
            set { _QuoteRef = value; }
        }

        /// <summary>
        /// The name of the Client the Quote is for
        /// </summary>
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        /// <summary>
        /// The contact name for the Quote or the Site
        /// </summary>
        public string ContactName
        {
            get { return _ContactName; }
            set { _ContactName = value; }
        }

        /// <summary>
        /// The contacts mobile number
        /// </summary>
        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }

        /// <summary>
        /// The contact telephone
        /// </summary>
        public string Telephone
        {
            get { return _Telephone; }
            set { _Telephone = value; }
        }

        /// <summary>
        /// The Site specific reference
        /// </summary>
        public string SiteRef
        {
            get { return _SiteRef; }
            set { _SiteRef = value; }
        }

        /// <summary>
        /// The Site Address, comma delimited string
        /// </summary>
        public string SiteAddress
        {
            get { return _SiteAddress; }
            set { _SiteAddress = value; }
        }

        /// <summary>
        /// The post code of the site
        /// </summary>
        public string SitePostCode
        {
            get { return _SitePostCode; }
            set { _SitePostCode = value; }
        }

        /// <summary>
        /// The list of Quote items for the Quote and site
        /// Note: This property will always return an instantiated list
        /// </summary>
        public List<QuoteItem> SiteItems
        {
            get
            {
                if (_SiteItems == null)
                    _SiteItems = new List<QuoteItem>();

                return _SiteItems;
            }
            set { _SiteItems = value; }
        }
        #endregion

        #region Methods
        public override string TitleGet()
        {
            return "";
        }

        public override string DetailGet()
        {
            return "";
        }

        public override string SideBarDetailsGet()
        {
            StringBuilder html = new StringBuilder();
            html.Append("");
            html.Append("<strong>Quote No: </strong><label>");
            html.Append(this.QuoteRef);
            html.Append("</label><br><strong>Customer: </strong><label>");
            html.Append(this.ClientName);
            if(this.ClientName != this.ContactName){
            html.Append("</label><br><strong>Contact: </strong><label>");
            html.Append(this.ContactName);
            }
            html.Append("</label><br><strong>Telephone: </strong><label>");
            html.Append(this.Telephone);
            html.Append("</label><br><strong>Mobile:</strong><label>");
            html.Append(this.Mobile);
            html.Append("</label><br><strong>Site Reference:</strong><label>");
            html.Append(this.SiteRef);
            html.Append("</label><br><strong>Site Address:</strong><label>");
            if (string.IsNullOrEmpty(this.SitePostCode))
            {
                html.Append(this.SiteAddress.Trim());
            }
            else
            {
                html.Append(this.SiteAddress.Replace(this.SitePostCode, "").Trim());
            }
            html.Append("</label><br><strong>Post Code:</strong><label>");
            html.Append(this.SitePostCode);
            html.Append("</label>");
            return html.ToString();
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
    }
}
