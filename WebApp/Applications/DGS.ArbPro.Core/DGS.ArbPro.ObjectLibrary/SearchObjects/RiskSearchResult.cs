﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace DGS.ArbPro.Types
{
    public class RiskSearchResult: SearchResult
    {
        #region Members
        Enums.RiskAssessmentStatusEnum _CurrentStatus = Enums.RiskAssessmentStatusEnum.Pending;
        int _QuoteId = -1;
        string _ClientName = "";
        string _SiteAddress = "";
        private int _JobSiteId = -1;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor, results in decimal values displayed in en-GB currency format
        /// </summary>
        public RiskSearchResult():base() { }
        #endregion

        #region Properties
        /// <summary>
        /// The type of search result this object represents
        /// </summary>
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Risk; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        /// <summary>
        /// The name of the Client the Risk Assessment is for
        /// </summary>
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        /// <summary>
        /// The Site address for the Risk Assessment
        /// </summary>
        public string SiteAddress
        {
            get { return _SiteAddress; }
            set { _SiteAddress = value; }
        }

        public Enums.RiskAssessmentStatusEnum Status
        {
            get { return _CurrentStatus; }
            set { _CurrentStatus = value; }
        }

        public int QuoteId
        {
            get { return _QuoteId; }
            set { _QuoteId = value; }
        }

        public int JobSiteId
        {
            get { return _JobSiteId; }
            set { _JobSiteId = value; }
        }
        #endregion

        #region Methods
        #region Public Overrides
        /// <summary>
        /// Method to format the job number, and client name into a search result title
        /// </summary>
        /// <returns>String</returns>
        public override string TitleGet()
        {
            return string.Format("Quote No:{0}, Client name: {1}",this.QuoteId,_ClientName);
        }

        /// <summary>
        /// Method to format the status, value, number of items and site address into the details for a search result
        /// </summary>
        /// <returns></returns>
        public override string DetailGet()
        {
            return string.Format("Site Address: {0}, Status: {1}",_SiteAddress,  _CurrentStatus.ToString().Replace("_"," ") );
        }

        public override string SideBarDetailsGet()
        {
            string pending = "";
            if (this.Id < 0) { pending = "Assessment Required"; }
            return string.Format("Site Address:{0}<br/><b>{1}</b>", _SiteAddress, pending);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}