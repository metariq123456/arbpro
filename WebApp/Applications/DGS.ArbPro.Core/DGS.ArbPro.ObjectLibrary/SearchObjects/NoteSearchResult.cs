﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace DGS.ArbPro.Types
{
    public class NoteSearchResult: SearchResult
    {
        #region Members
        
        string _CultureCode = "en-GB";
        string _Note = "";
        string _ClientName = "";
        DateTime _CreateDate = DateTime.MinValue;
        string _Author = string.Empty;
        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor, results in decimal values displayed in en-GB currency format
        /// </summary>
        public NoteSearchResult() : base() { }

        /// <summary>
        /// Constructor to set the culture code for decimal to currency display
        /// </summary>
        /// <param name="cultureCode">Country code for currency format, as a string</param>
        public NoteSearchResult(string cultureCode)
            : base()
        {
            _CultureCode = cultureCode;
        }
        #endregion

        #region Properties

        /// <summary>
        /// The type of search result this object represents
        /// </summary>
        public override Enums.SearchTypeEnum SearchType
        {
            get { return Enums.SearchTypeEnum.Note; }
            set { /*NOTE: JSON serializer requires setter*/ }
        }

        /// <summary>
        /// The note for the quote
        /// </summary>
        public string Note
        {
            get { return _Note; }
            set { _Note = value; }
        }

        /// <summary>
        /// The client name for the quote
        /// </summary>
        public string ClientName
        {
            get { return _ClientName; }
            set { _ClientName = value; }
        }

        /// <summary>
        /// The author of the note
        /// </summary>
        public string Author
        {
            get { return _Author; }
            set { _Author = value; }
        }

        /// <summary>
        /// The date the note was created
        /// </summary>
        public DateTime CreateDate
        {
            get { return _CreateDate; }
            set { _CreateDate = value; }
        }

        #endregion

        #region Methods
        #region Public Overrides

        /// <summary>
        /// Method to format the quote number, and client name into a search result title
        /// </summary>
        /// <returns>String</returns>
        public override string TitleGet()
        {
            return string.Format("Quote No:{0}, Client Name: {1}", this.Id, _ClientName);
        }

        /// <summary>
        /// Method to format the note into the details for a search result
        /// </summary>
        /// <returns></returns>
        public override string DetailGet()
        {
            return string.Format("Created: {0}, Author: {1}, Note: {2}", _CreateDate.ToString("yyyy-MM-dd"), _Author, _Note);
        }

        public override string SideBarDetailsGet()
        {
            return string.Format("Quote No:{0}", this.Id);
        }

        public override string ToString()
        {
            return base.GetObjectState(this);
        }
        #endregion
        #endregion
    }
}