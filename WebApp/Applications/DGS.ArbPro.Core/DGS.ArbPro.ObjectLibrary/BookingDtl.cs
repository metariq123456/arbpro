﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.Types
{
   public class BookingDtl
    {
        public int WorkId { get; set; }

        public int QuoteId { get; set; }

        public String ClientName { get; set; }

        public String SiteAddress { get; set; }

        public String Telephone { get; set; }

        public String Mobile { get; set; }

        public String Email { get; set; }

        public int SiteId { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }

        public int UserId { get; set; }

        public int QuoteStatus { get; set; }

        public List<BookedWork> BookedWorkItem { get; set; }
    }
    public class BookedWork
    {
        public int Id { get; set; }
        public int WorkId { get; set; }
        public String Code { get; set; }
        public String Name { get; set; }
        public String Descriptioin { get; set; }
        public decimal value { get; set; }
    }
    public class QuoteAccept
    {
        public int QuoteId { get; set; }
    }
}
