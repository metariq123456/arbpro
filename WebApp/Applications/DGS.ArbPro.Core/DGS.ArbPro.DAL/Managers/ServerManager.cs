﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Configuration;
using Microsoft.SqlServer.Management;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;


namespace DGS.ArbPro.DAL
{
    public class ServerManager
    {
        Server _Server;
        public ServerManager()
        {
            _Server = new Server(Properties.Settings.Default.Server);
        }

        public string GetDbNameForOrganisationId(int clientId)
        {
            var dbNamePatern = Properties.Settings.Default.ClientDatabasePattern;
            return string.Format(dbNamePatern, clientId);
        }

        public bool CheckClientDatabaseExists(int ClientId)
        {
            bool retVal = true;
            try
            {
                var clientDbName = GetDbNameForOrganisationId(ClientId);
                retVal = _Server.Databases[clientDbName] != null;
            }
            catch (Exception ex)
            {
                throw new Exception("Error checking for database.", ex);
            }

            return retVal;
        }

        public bool CreateNewClientDatabase(int ClientID)
        {
            bool ReturnValue = true;
            Backup bk;
            string NewDatabaseName = "";
            string BackupFilename;
            Database SourceDatabase;
            Database NewDatabase;
            string NewDatabasePattern = Properties.Settings.Default.ClientDatabasePattern;
            string DatabaseDirectory = Properties.Settings.Default.DatabaseDirectory;
            string TemplateDatabase = Properties.Settings.Default.ClientTemplateDatabase;
            try
            {
                _Server.SetDefaultInitFields(true);
                NewDatabaseName = string.Format(NewDatabasePattern, ClientID);
                if (_Server.Databases[NewDatabaseName] == null)
                {
                    NewDatabase = new Database(_Server, NewDatabaseName);
                    NewDatabase.Create();
                }
                else
                {
                    throw new Exception(string.Format("Database {0} already exists - cannot create",NewDatabaseName));
                }


                SourceDatabase = _Server.Databases[TemplateDatabase];
                bk = new Backup();
                bk.Action = BackupActionType.Database;
                BackupDeviceItem bdi = default(BackupDeviceItem);

                // Back up to a temporary file
                BackupFilename = System.IO.Path.Combine(DatabaseDirectory,string.Format("{0}.bak",Guid.NewGuid()));// System.IO.Path.GetTempFileName();

                bdi = new BackupDeviceItem(BackupFilename, DeviceType.File);

                //Add the device to the Backup object. 
                bk.Devices.Add(bdi);

                //Set the Incremental property to False to specify that this is a full database backup. 
                bk.Incremental = false;
                bk.Database = TemplateDatabase;
                bk.SqlBackup(_Server);
                bk.Devices.Remove(bdi);

                Restore rs = new Restore();
                rs.ReplaceDatabase = true;
                rs.NoRecovery = false;
                rs.Devices.Add(bdi);
                rs.Database = NewDatabaseName;

                // We need to relocate the files so that we're restoring to the new database's files, not the original from which the
                // backup was taken
                rs.RelocateFiles.Add(new RelocateFile("ArbProClient", string.Format(@"{0}\{1}.mdf", DatabaseDirectory, NewDatabaseName)));
                rs.RelocateFiles.Add(new RelocateFile("ArbProClient_log", string.Format(@"{0}\{1}.ldf", DatabaseDirectory, NewDatabaseName)));
                rs.SqlRestore(_Server);
                rs.Devices.Remove(bdi);

                foreach (Microsoft.SqlServer.Management.Smo.User u in SourceDatabase.Users)
                {
                    if (!NewDatabase.Users.Contains(u.Name))
                    {
                        NewDatabase.Users.Add(u);
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("Could not create new database. Error {0}", ex.Message));
            }
            finally
            {
            }

            return ReturnValue;


        }

        protected static void DataTransferEvent_Handler(object sender, DataTransferEventArgs e)
        {
            Console.WriteLine("[" + e.DataTransferEventType + "] " + e.Message);
        }
    }
}
