﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class LogEntry
    {
        public int ID { get; set; }
        public Boolean IsError { get; set; }
        public int AspSessionID { get; set; }
        public DateTime AspSessionStartTime { get; set; }
        public DateTime AspSessionEndTime { get; set; }
        public String UserAgent { get; set; }
        public String AppVersion { get; set; }
        public DateTime EventTime { get; set; }
        public String Event { get; set; }
        public String Info { get; set; }
        public String Details { get; set; }

        public LogEntry(DataRow Row)
        {
            ID = int.Parse(Row["ID"].ToString());
            IsError = Boolean.Parse(Row["IsError"].ToString());
            AspSessionID = int.Parse(Row["AspSessionID"].ToString());
            try { AspSessionStartTime = DateTime.Parse(Row["AspSessionStartTime"].ToString()); } catch { }
            try { AspSessionEndTime = DateTime.Parse(Row["AspSessionEndTime"].ToString()); } catch { }
            UserAgent = Row["UserAgent"].ToString();
            AppVersion = Row["AppVersion"].ToString();
            try { EventTime = DateTime.Parse(Row["EventTime"].ToString()); } catch { }
            if (!IsError)
            {
                Event = Row["Event"].ToString();
                Info = Row["Info"].ToString();
                Details = Row["Details"].ToString();
            }
            else
            {
                Event = String.Format("Error: DB{0}", ID);
                Info = Row["Event"].ToString();
                Details = Row["Details"].ToString();
            }
        }
    }

    public class LogDB
    {
        string _DatabaseName;

        public LogDB(String Database)
        {
            _DatabaseName = Database;
        }

        public List<LogEntry> GetLogs(DateTime StartTime, DateTime EndTime, Boolean ShowEvents, Boolean ShowErrors, int SessionID = -1, int ErrorNo = -1)
        {
            List<LogEntry> ReturnValue = new List<LogEntry>();

            SqlParameter[] Parameters = new SqlParameter[] {
                new SqlParameter("@StartTime", StartTime),
                new SqlParameter("@EndTime", EndTime),
                new SqlParameter("@Errors", ShowErrors),
                new SqlParameter("@Events", ShowEvents),
                new SqlParameter("@AspSessionID", SessionID),
                new SqlParameter("@ErrorNo", ErrorNo)
            };

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("admin_GetLogs", Parameters, _DatabaseName);

            foreach (DataRow Row in DS.Tables[0].Rows)
            {
                ReturnValue.Add(new LogEntry(Row));
            }

            return ReturnValue;
        }
    }
}
