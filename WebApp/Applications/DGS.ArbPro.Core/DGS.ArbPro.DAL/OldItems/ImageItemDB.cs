﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using PDFReport;

namespace DGS.ArbPro.DAL
{
    public class ImageItemDB
    {
        public static List<ImageItem> GetImageItemsForGroup(string databaseName,int id)
        {
            List<ImageItem> retVal = new List<ImageItem>();
            DataSet ds;
            ImageItem item = null;
            ds = DBHelper.ExecuteStoredProcDataSet("ReportImageItemsForGroupGet", new SqlParameter[] { new SqlParameter("@GroupId", id) }, databaseName);
            foreach (DataRow Row in ds.Tables[0].Rows)
            {
                item = new ImageItem(double.Parse(Row["XPos"].ToString()), double.Parse(Row["YPos"].ToString()), double.Parse(Row["Width"].ToString()), double.Parse(Row["Height"].ToString()), Row["Name"].ToString());
                item.SetImage((Byte[])Row["Image"]);
                retVal.Add(item);
            }
            return retVal;
        }

        public static int InsertImageItemForGroup(string databaseName, int groupID, ImageItem item)
        {
            try
            {
                int ReturnValue;
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@GroupId", groupID),
                    new SqlParameter("@Name", item.Name), 
                    new SqlParameter("@ImageArray",item.GetImageBytes()),
                    new SqlParameter("@FilePath",item.ImagePath),
	                new SqlParameter("@XPos", item.X),
	                new SqlParameter("@YPos", item.Y),
	                new SqlParameter("@Width", item.Width),
                    new SqlParameter("@Height",item.Height)
                };
                ReturnValue = DBHelper.ExecuteStoredProcReturnID("ReportImageItemInsert", parameters, databaseName);
                item.Id = ReturnValue;
                return ReturnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error inserting a new line item: {0}", ex.ToString());
                return -1;
            }
        }
    }
}
