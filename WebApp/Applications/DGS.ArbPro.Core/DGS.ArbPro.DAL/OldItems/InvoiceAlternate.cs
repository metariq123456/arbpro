﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class InvoiceAlternate
    {
        public int ID;
        public int InvoiceID;
        public String ClientTitle;
        public String ClientName;
        public String BusinessName;
        public String HouseNameOrNo;
        public String Address1;
        public String Address2;
        public String City;
        public String Town;
        public String County;
        public String PostCode;

        public InvoiceAlternate(SqlDataReader Row)
        {
            ID = Row.IsDBNull(0) ? -1 : Int32.Parse(Row[0].ToString());
            InvoiceID = Row.IsDBNull(1) ? -1 : Int32.Parse(Row[1].ToString());
            ClientTitle = Row.IsDBNull(2) ? "" : Row[2].ToString();
            ClientName = Row.IsDBNull(3) ? "" : Row[3].ToString();
            BusinessName = Row.IsDBNull(4) ? "" : Row[4].ToString();
            HouseNameOrNo = Row.IsDBNull(5) ? "" : Row[5].ToString();
            Address1 = Row.IsDBNull(6) ? "" : Row[6].ToString();
            Address2 = Row.IsDBNull(7) ? "" : Row[7].ToString();
            City = Row.IsDBNull(8) ? "" : Row[8].ToString();
            Town = Row.IsDBNull(9) ? "" : Row[9].ToString();
            County = Row.IsDBNull(10) ? "" : Row[10].ToString();
            PostCode = Row.IsDBNull(11) ? "" : Row[11].ToString();
        }
    }
}
