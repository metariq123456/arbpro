﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class Company
    {
        public Decimal CoCode;
        public String CompanyName;
        public String CompanyAddress;
        public String CompanyPostCode;
        public String CompanyVATNo;
        public String CompanyRegistrationNo;
        public String CompanyWebsiteAddress;
        public String CompanyTelephoneNumber;
        public String CompanyDirectorName;
        public String CompanyDirectorEmailAddress;
        public String CompanyDirectorQualifications;
        public String CompanyHeader;
        public String CompanyFooter;
        public String CompanyDirectorSignature;
        public String SMTPServer;
        public String SMTPUser;
        public String SMTPPass;
        public String SMTPAccountName;
        public String EmailHeader;
        public String EmailFooter;
        public String SalesNominalCode;
        public String SalesVATCode;
        public Decimal DefaultVATRate;
        public String LatinCommon;
        public String AccountsEmailAddress;
        public String ReceiptNominalCode;
        public Byte[] CompanyDirectorSignatureImage;

        public Company(SqlDataReader Row)
        {
            CoCode =                        Row.IsDBNull(0) ? 0 : (Decimal)Row["CoCode"];
            CompanyName =                   Row.IsDBNull(1) ? "" : (String)Row["CompanyName"];
            CompanyAddress =                Row.IsDBNull(2) ? "" : (String)Row["CompanyName"];
            CompanyPostCode =               Row.IsDBNull(3) ? "" : (String)Row["CompanyPostCode"];
            CompanyVATNo =                  Row.IsDBNull(4) ? "" : (String)Row["CompanyVATNo"];
            CompanyRegistrationNo =         Row.IsDBNull(5) ? "" : (String)Row["CompanyRegistrationNo"];
            CompanyWebsiteAddress =         Row.IsDBNull(6) ? "" : (String)Row["CompanyWebsiteAddress"];
            CompanyTelephoneNumber =        Row.IsDBNull(7) ? "" : (String)Row["CompanyTelephoneNumber"];
            CompanyDirectorName =           Row.IsDBNull(8) ? "" : (String)Row["CompanyDirectorName"];
            CompanyDirectorEmailAddress =   Row.IsDBNull(9) ? "" : (String)Row["CompanyDirectorEmailAddress"];
            CompanyDirectorQualifications = Row.IsDBNull(10) ? "" : (String)Row["CompanyDirectorQualifications"];
            CompanyHeader =                 Row.IsDBNull(11) ? "" : (String)Row["CompanyHeader"];
            CompanyFooter =                 Row.IsDBNull(12) ? "" : (String)Row["CompanyFooter"];
            CompanyDirectorSignature =      Row.IsDBNull(13) ? "" : (String)Row["CompanyDirectorSignature"];
            SMTPServer =                    Row.IsDBNull(14) ? "" : (String)Row["SMTPServer"];
            SMTPUser =                      Row.IsDBNull(15) ? "" : (String)Row["SMTPUser"];
            SMTPPass =                      Row.IsDBNull(16) ? "" : (String)Row["SMTPPass"];
            SMTPAccountName =               Row.IsDBNull(17) ? "" : (String)Row["SMTPAccountName"];
            EmailHeader =                   Row.IsDBNull(18) ? "" : (String)Row["EmailHeader"];
            EmailFooter =                   Row.IsDBNull(19) ? "" : (String)Row["EmailFooter"];
            SalesNominalCode =              Row.IsDBNull(20) ? "" : (String)Row["SalesNominalCode"];
            SalesVATCode =                  Row.IsDBNull(21) ? "" : (String)Row["SalesVATCode"];
            DefaultVATRate =                Row.IsDBNull(22) ? 0 : (Decimal)Row["DefaultVATRate"];
            LatinCommon =                   Row.IsDBNull(23) ? "" : (String)Row["LatinCommon"];
            AccountsEmailAddress =          Row.IsDBNull(24) ? "" : (String)Row["AccountsEmailAddress"];
            ReceiptNominalCode =            Row.IsDBNull(25) ? "" : (String)Row["ReceiptNominalCode"];
            CompanyDirectorSignatureImage = Row.IsDBNull(26) ? new Byte[0] : (Byte[])Row["CompanyDirectorSignatureImage"];
        }

        public void Save(String DatabaseName)
        {
            CompanyDB DB = new CompanyDB(DatabaseName);
            DB.SaveCompany(this);
        }

        public static Company GetCompany(String DatabaseName)
        {
            CompanyDB DB = new CompanyDB(DatabaseName);
            return DB.GetCompany();
        }
    }
}
