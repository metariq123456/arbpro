﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class AdminDb
    {
        string _DatabaseName;

        public AdminDb(String Database)
        {
            _DatabaseName = Database;
        }

        public List<AdminUser> GetAdministrators(String ApplicationName, int PageIndex, int PageSize)
        {
            List<AdminUser> Users = new List<AdminUser>();

            SqlParameter[] Parameters = new SqlParameter[] {
                new SqlParameter("@ApplicationName", ApplicationName),
                new SqlParameter("@PageIndex", PageIndex),
                new SqlParameter("@PageSize", PageSize)
            };

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("aspnet_Membership_GetAllUsers", Parameters, _DatabaseName);

            foreach (DataRow Row in DS.Tables[0].Rows)
            {
                Users.Add(AdminUser.CreateFromRow(Row));
            }

            return Users;
        }

        public AdminUser GetAdministratorForUserId(String UserId)
        {
            AdminUser User = null;

            SqlParameter[] Parameters = new SqlParameter[] {
                new SqlParameter("@UserId", UserId),
                new SqlParameter("@CurrentTimeUtc", DateTime.UtcNow.ToString())
            };

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("aspnet_Membership_GetUserByUserId", Parameters, _DatabaseName);

            foreach (DataRow Row in DS.Tables[0].Rows)
            {
                User = AdminUser.CreateFromRow(Row, UserId);
            }

            return User;
        }

        public void UnlockAdmin(String ApplicationName, String UserName)
        {
            SqlParameter[] Parameters = new SqlParameter[] {
                new SqlParameter("@ApplicationName", ApplicationName),
                new SqlParameter("@UserName", UserName)
            };

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("aspnet_Membership_UnlockUser", Parameters, _DatabaseName);
        }

        public void LockAdmin(String ApplicationName, String UserName)
        {
            SqlParameter[] Parameters = new SqlParameter[] {
                new SqlParameter("@ApplicationName", ApplicationName),
                new SqlParameter("@UserName", UserName)
            };

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("aspnet_Membership_LockUser", Parameters, _DatabaseName);
        }

        public void SaveAdmin(String ApplicationName, AdminUser User)
        {
            SqlParameter[] Parameters = new SqlParameter[] {
                new SqlParameter("@ApplicationName", ApplicationName),
                new SqlParameter("@UserName", User.UserName),
                new SqlParameter("@Email", User.EmailAddress),
                new SqlParameter("@Comment", User.Comment),
                new SqlParameter("@IsApproved", true),
                new SqlParameter("@LastLoginDate", User.LastLoginDate),
                new SqlParameter("@LastActivityDate", User.LastActivityDate),
                new SqlParameter("@UniqueEmail", 1),
                new SqlParameter("@CurrentTimeUtc", DateTime.UtcNow)
            };

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("aspnet_Membership_UpdateUser", Parameters, _DatabaseName);
        }
    }
}

