﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DGS.ArbPro.DAL
{
    public class RiskReferenceDB
    {
        string _DatabaseName;

        public RiskReferenceDB(string databaseName)
        {
            _DatabaseName = databaseName;
        }


        /// <summary>
        /// Adds a new risk reference to the database.
        /// </summary>
        /// <param name="riskReference"></param>
        /// <returns></returns>
        public int Add(RiskReference riskReference)
        {
            int NewID;
            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
            IDParameter.Direction = ParameterDirection.Output;
            SqlParameter[] parameters = new SqlParameter[]
            {     
                new SqlParameter("@AssessmentSection", riskReference.AssessmentSection),
	            new SqlParameter("@AssessmentItem",riskReference.AssessmentItem),
                new SqlParameter("@Description",riskReference.Description),
	            new SqlParameter("@Reference", riskReference.Reference),
                IDParameter
            };

            DBHelper.ExecuteStoredProcRowCount("RiskReferenceAdd", parameters, _DatabaseName);
            NewID = (int)IDParameter.Value;
            return NewID;

        }

        public int Update(RiskReference riskReference)
        {
            int ReturnValue;

            SqlParameter[] parameters = new SqlParameter[]
            {                
                new SqlParameter("@AssessmentSection", riskReference.AssessmentSection), 
                new SqlParameter("@ID",riskReference.ID),
                new SqlParameter("@Description",riskReference.Description),
	            new SqlParameter("@Reference", riskReference.Reference),
            };

            //ReturnValue = DBHelper.ExecuteStoredProcRowCount("RiskReferenceChapterRefUpdate", parameters, _DatabaseName);
            ReturnValue = 1;

            return ReturnValue;

        }

        public int UpdateReference(int ItemId, string ChapterReference)
        {
            int ReturnValue;

            SqlParameter[] parameters = new SqlParameter[]
            {                
                new SqlParameter("@ID",ItemId),
	            new SqlParameter("@Reference", ChapterReference),
            };

            ReturnValue = DBHelper.ExecuteStoredProcRowCount("RiskReferenceChapterRefUpdate", parameters, _DatabaseName);
            ReturnValue = 1;

            return ReturnValue;

        }

        /// <summary>
        /// Returns a list of all Risk References in the database.
        /// </summary>
        /// <returns></returns>
        public List<RiskReference> List()
        {
            DataSet ReferenceList;

            List<RiskReference> ReturnValue = new List<RiskReference>();

           ReferenceList = DBHelper.ExecuteStoredProcDataSet("RiskReferenceList", new SqlParameter[] { }, _DatabaseName);
            foreach (DataRow Row in ReferenceList.Tables[0].Rows)
            {
                ReturnValue.Add(new RiskReference()
                {
                  ID = int.Parse(Row["ID"].ToString()),
                    AssessmentSection = Row["AssessmentSection"].ToString().Replace('-', '_').Replace("@", "At").Replace("[", "").Replace("]", ""),
                    AssessmentItem = Row["AssessmentItem"].ToString().Replace('-','_').Replace("@","At").Replace("[","").Replace("]",""),
                    Description = Row["Description"].ToString(),
                    Reference = Row["Reference"].ToString()
                });

          }
            Console.WriteLine("Number of reference items = {0}", ReturnValue.Count.ToString());
            return ReturnValue;
        }
    }
}
