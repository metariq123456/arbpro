﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class TextTemplateDB
    {
        public static List<TextTemplate> GetList(String DBName, Int32 TypeFilter = -1, Int32 MessageFilter = -1, Int32 IDFilter = -1)
        {
            List<TextTemplate> ReturnValue = new List<TextTemplate>();

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@TypeFilter", TypeFilter),
                new SqlParameter("@MessageFilter", MessageFilter),
                new SqlParameter("@IDFilter", IDFilter)
            };

            var Result = DBHelper.ExecuteReader("TextTemplateGetList", Params, DBName);

            while (Result.Read())
                ReturnValue.Add(new TextTemplate(Result));
            Result.Close();

            return ReturnValue;
        }

        public static Int32 Save(String DBName, TextTemplate Object)
        {
            Int32 ReturnValue = -1;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("ID", Object.ID),
                new SqlParameter("TemplateName", Object.TemplateName),
                new SqlParameter("ObjectType", (Int32)Object.ObjectType),
                new SqlParameter("MessageType", (Int32)Object.MessageType),
                new SqlParameter("BodyText", Object.BodyText),
                new SqlParameter("AdditionalText", Object.AdditionalText)
            };

            var Result = DBHelper.ExecuteReader("TextTemplateInsertOrUpdate", Params, DBName);

            while (Result.Read())
                ReturnValue = Result.IntegerValue("NewID");
            Result.Close();

            return ReturnValue;
        }
    }
}
