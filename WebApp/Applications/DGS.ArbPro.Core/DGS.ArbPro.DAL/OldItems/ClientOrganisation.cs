﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class ClientOrganisation :Contracts.DBContextBase
    {
        #region Properties
        public string OrganisationName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Town { get; set; }
        public string CountyOrState { get; set; }
        public string PostcodeOrZip { get; set; }
        public int CountryID { get; set; }
        public string Country { get; set; }
        public bool UserAccountsEnabled { get; set; }
        public bool MobileEditionEnabled { get; set; }
        #endregion

        public String Name
        {
            get
            {
                return String.Format("{0}: {1}", ID, OrganisationName);
            }
        }
    }

    public class ClientCountry
    {
        public int ID { get; set; }
        public String ISOCode { get; set; }
        public String Name { get; set; }
    }

    public class ClientUser
    {
        public int ID { get; set; }
        public int ClientOrganisation { get; set; }
        public String Email { get; set; }
        public int NamePrefixID { get; set; }
        public String NamePrefix { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Telephone { get; set; }
        public Boolean Enabled { get; set; }
        public Boolean Mobile { get; set; }
        public int AuthFailures { get; set; }
        public String Position { get; set; }
        public Boolean TsAndCsAccepted { get; set; }

        public String Name { get { return (ID > -1 ? ID + ": " : "") + NamePrefix + " " + FirstName + " " + LastName; } }
    }

    public class ClientUserPrefix
    {
        public int ID { get; set; }
        public String Prefix { get; set; }
    }
}
