﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class RiskReference: DataObjectBase
    {
        #region Properties
        /// <summary>
        /// The name of the section of the risk assessment the assessment item belongs to
        /// </summary>
        public string AssessmentSection { get; set; }

        /// <summary>
        /// The name of the risk assessment item.
        /// </summary>
        /// <remarks>
        /// This should match the name of the column in the Risks table, and also the label IDs on the Risks.aspx page.
        /// </remarks>
        public string AssessmentItem { get; set; }

        /// <summary>
        /// The description displayed on the Risk Reference editing screeen.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The reference displayed on the Risk Assessment forms.
        /// </summary>
        public string Reference { get; set; }

        #endregion

        #region Data Binding Methods

        public void Save(string DataBaseName, string Reference)
        {
            RiskReferenceDB dbObj = new RiskReferenceDB(DataBaseName);

            dbObj.UpdateReference(ID, Reference);
        }

        public static List<RiskReference> List(string DataBaseName)
        {
            RiskReferenceDB dbObj = new RiskReferenceDB(DataBaseName);
            List<RiskReference> retList = dbObj.List();
            return retList;
        }

        public static void UpdateChapterReference(int ID, string Reference, string DataBaseName)
        {
            RiskReferenceDB dbObj = new RiskReferenceDB(DataBaseName);
            dbObj.UpdateReference(ID, Reference);
        }
        #endregion
    }
}
