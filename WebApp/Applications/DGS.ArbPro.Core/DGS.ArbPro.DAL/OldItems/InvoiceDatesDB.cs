﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class InvoiceDatesDB
    {
        public static List<InvoiceDates> List(string DatabaseName, Int32 InvoiceID)
        {
            List<InvoiceDates> ReturnValue = new List<InvoiceDates>();

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("InvoiceID", InvoiceID)
            };

            var Result = DBHelper.ExecuteReader("InvoiceDatesGetList",
                Params,
                DatabaseName);

            while (Result.Read())
            {
                ReturnValue.Add(new InvoiceDates(Result));
            }
            Result.Close();

            return ReturnValue;
        }

        public static InvoiceDates Get(string DatabaseName, Int32 InvoiceID, Int32 InvoiceDateID = -1)
        {
            InvoiceDates ReturnValue = null;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("InvoiceID", InvoiceID),
                new SqlParameter("InvoiceDateID", InvoiceDateID)
            };

            var Result = DBHelper.ExecuteReader("InvoiceDatesGetList",
                Params,
                DatabaseName);

            while (Result.Read())
            {
                ReturnValue = new InvoiceDates(Result);
            }
            Result.Close();

            return ReturnValue;
        }

        public static Int32 Save(string DatabaseName, InvoiceDates Object)
        {
            Int32 ReturnValue = Object.ID;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@InvoiceDatesID", Object.ID),
	            new SqlParameter("@ClientID", Object.ClientID),
	            new SqlParameter("@QuoteID", Object.QuoteID),
	            new SqlParameter("@InvoiceNo", Object.InvoiceID),
	            new SqlParameter("@InvoiceDatesDateSent", Object.InvoiceSentDate),
	            new SqlParameter("@InvoiceDatesType", Object.InvoiceType),
	            new SqlParameter("@UserCR", Object.UserCR),
	            new SqlParameter("@Success", Object.Success)
            };

            var Result = DBHelper.ExecuteReader("InvoiceDatesInsertOrUpdate",
                Params,
                DatabaseName);

            while (Result.Read())
            {
                ReturnValue = Result.IntegerValue("NewID");
            }
            Result.Close();

            return ReturnValue;

        }
    }
}
