﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using PDFReport;

namespace DGS.ArbPro.DAL
{
    public class LineItemDB
    {
        public static List<LineItem> GetLineItemsForGroup(string databaseName, int groupID)
        {
            List<LineItem> retVal = new List<LineItem>();
            DataSet ds;
            LineItem item = null;
            ds = DBHelper.ExecuteStoredProcDataSet("ReportLineItemsForGroupGet", new SqlParameter[] { new SqlParameter("@GroupId", groupID) }, databaseName);
            foreach (DataRow Row in ds.Tables[0].Rows)
            {
                item = new LineItem(double.Parse(Row["BrushSize"].ToString()), double.Parse(Row["XPos"].ToString()), double.Parse(Row["YPos"].ToString()), double.Parse(Row["Width"].ToString()), double.Parse(Row["Height"].ToString()), Row["Name"].ToString());
                item.Colour = Row["Colour"].ToString();
                retVal.Add(item);
            }
            return retVal;
        }

        public static int InsertLineItemForGroup(string databaseName, int groupID, LineItem item)
        {
            try
            {
                int ReturnValue;

                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@GroupId", groupID),
                    new SqlParameter("@Name", item.Name), 
                    new SqlParameter("@HorizontalPos",(int)item.RelativeHorizontalPos),
                    new SqlParameter("@VerticalPos",(int)item.RelativeVerticalPos),
	                new SqlParameter("@XPos", item.X),
	                new SqlParameter("@YPos", item.Y),
	                new SqlParameter("@Width", item.Width),
                    new SqlParameter("@Height",item.Height),
                    new SqlParameter("@BrushSize",item.PenSize),
                    new SqlParameter("@Colour",item.Colour),
                };
                ReturnValue = DBHelper.ExecuteStoredProcReturnID("ReportLineItemInsert", parameters, databaseName);
                item.Id = ReturnValue;
                return ReturnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error inserting a new line item: {0}", ex.ToString());
                return -1;
            }
        }
    }
}
