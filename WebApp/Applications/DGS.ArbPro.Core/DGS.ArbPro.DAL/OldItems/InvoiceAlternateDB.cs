﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class InvoiceAlternateDB
    {
        String DatabaseName;

        public InvoiceAlternateDB(String DBName)
        {
            DatabaseName = DBName;
        }

        public List<InvoiceAlternate> ListAddressesForInvoice(Int32 InvoiceID)
        {
            List<InvoiceAlternate> ReturnValue = new List<InvoiceAlternate>();

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("InvoiceID", InvoiceID)
            };

            var Result = DBHelper.ExecuteReader(
                "AddressListForInvoice",
                Params,
                DatabaseName);

            while (Result.Read())
                ReturnValue.Add(new InvoiceAlternate(Result));
            Result.Close();

            return ReturnValue;
        }

        public List<InvoiceAlternate> ListAddressesForQuote(Int32 QuoteID)
        {
            List<InvoiceAlternate> ReturnValue = new List<InvoiceAlternate>();

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("QuoteID", QuoteID)
            };

            var Result = DBHelper.ExecuteReader(
                "AddressListForQuote",
                Params,
                DatabaseName);

            while (Result.Read())
                ReturnValue.Add(new InvoiceAlternate(Result));
            Result.Close();

            return ReturnValue;
        }
    }
}
