﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;

namespace DGS.ArbPro.DAL
{
    public class RiskDB
    {
        string _DatabaseName;
        
        public RiskDB(string databaseName)
        {
            _DatabaseName = databaseName;
        }
        
        public Risk GetRiskAssessmentForQuoteNo(int QuoteNo)
        {
            Risk retVal = null ;
            DataSet ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessmentForQuoteNoGet", new SqlParameter[] { new SqlParameter("@QuoteNo", QuoteNo) }, _DatabaseName);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                retVal = new Risk();
                
                retVal.RiskIDNo = row["RiskIDNo"] != DBNull.Value  ? (int)row["RiskIDNo"] : -1;
                retVal.RiskCompanyName = row["RiskCompanyName"] != DBNull.Value  ? row["RiskCompanyName"].ToString().Replace("\n", " ").Replace("  "," ") : string.Empty;
                retVal.QuoteValueNo = row["QuoteValueNo"] != DBNull.Value  ? (int)row["QuoteValueNo"] : -1;
                retVal.ClientIDNo = row["ClientIDNo"] != DBNull.Value  ? (int)row["ClientIDNo"] : -1;
                retVal.RiskDateQuote = row["RiskDateQuote"] != DBNull.Value && row["RiskDateQuote"].ToString() != "" ? DateTime.Parse(row["RiskDateQuote"].ToString().Replace("\n", " ").Replace("  ", " ")) : DateTime.MinValue;
                retVal.RiskDateWork = row["RiskDateWork"] != DBNull.Value && row["RiskDateWork"].ToString() != "" ? DateTime.Parse(row["RiskDateWork"].ToString().Replace("\n", " ").Replace("  ", " ")) : DateTime.MinValue;
                retVal.RiskSiteAddress = row["RiskSiteAddress"] != DBNull.Value  ? row["RiskSiteAddress"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskEmergencyProceduresGenericRisker = row["RiskEmergencyProceduresGenericRisker"] != DBNull.Value ? row["RiskEmergencyProceduresGenericRisker"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskEmergencyNearestAandE = row["RiskEmergencyNearestAandE"] != DBNull.Value ? row["RiskEmergencyNearestAandE"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskEmergencyMobilePhoneSignal = row["RiskEmergencyMobilePhoneSignal"] != DBNull.Value ? row["RiskEmergencyMobilePhoneSignal"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskEmergencySiteAccessForRescue = row["RiskEmergencySiteAccessForRescue"] != DBNull.Value ? row["RiskEmergencySiteAccessForRescue"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskEmergencyRescuePlan = row["RiskEmergencyRescuePlan"] != DBNull.Value ? row["RiskEmergencyRescuePlan"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskEmergencyArialRescuePlan = row["RiskEmergencyArialRescuePlan"] != DBNull.Value ? row["RiskEmergencyArialRescuePlan"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskEmergencyAdequateFirstAidProvisions = row["RiskEmergencyAdequateFirstAidProvisions"] != DBNull.Value  ? (bool)row["RiskEmergencyAdequateFirstAidProvisions"] : false;
                retVal.RiskSiteGenericRisker = row["RiskSiteGenericRisker"] != DBNull.Value ? row["RiskSiteGenericRisker"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskSiteToPublic = row["RiskSiteToPublic"] != DBNull.Value  ? (bool)row["RiskSiteToPublic"] : false;
                retVal.RiskSiteTrafficManagement = row["RiskSiteTrafficManagement"] != DBNull.Value  ? (bool)row["RiskSiteTrafficManagement"] : false;
                retVal.RiskSiteOverheadLinesUndergroundServices = row["RiskSiteOverheadLinesUndergroundServices"] != DBNull.Value  ? (bool)row["RiskSiteOverheadLinesUndergroundServices"] : false;
                retVal.RiskSiteBuildingOrObstructions = row["RiskSiteBuildingOrObstructions"] != DBNull.Value  ? (bool)row["RiskSiteBuildingOrObstructions"] : false;
                retVal.RiskSiteSensitiveEmotionallyCharged = row["RiskSiteSensitiveEmotionallyCharged"] != DBNull.Value  ? (bool)row["RiskSiteSensitiveEmotionallyCharged"] : false;
                retVal.RiskSiteControlMeasuresNecessary = row["RiskSiteControlMeasuresNecessary"] != DBNull.Value ? row["RiskSiteControlMeasuresNecessary"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskEnvironmentalGenreicRisker = row["RiskEnvironmentalGenreicRisker"] != DBNull.Value  ? row["RiskEnvironmentalGenreicRisker"].ToString() : string.Empty;
                retVal.RiskEnvironmentalWaterCourse = row["RiskEnvironmentalWaterCourse"] != DBNull.Value  ? (bool)row["RiskEnvironmentalWaterCourse"] : false;
                retVal.RiskEnvironmentalProtectedSpeciesHabitat = row["RiskEnvironmentalProtectedSpeciesHabitat"] != DBNull.Value  ? (bool)row["RiskEnvironmentalProtectedSpeciesHabitat"] : false;
                retVal.RiskEnvironmentalNoiseDust = row["RiskEnvironmentalNoiseDust"] != DBNull.Value  ? (bool)row["RiskEnvironmentalNoiseDust"] : false;
                retVal.RiskEnvironmentalControlMeasuresNecessary = row["RiskEnvironmentalControlMeasuresNecessary"] != DBNull.Value ? row["RiskEnvironmentalControlMeasuresNecessary"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsGenericRisker = row["RiskOperationsGenericRisker"] != DBNull.Value ? row["RiskOperationsGenericRisker"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsChainSaw = row["RiskOperationsChainSaw"] != DBNull.Value  ? (bool)row["RiskOperationsChainSaw"] : false;
                retVal.RiskOperationsTreeClimbing = row["RiskOperationsTreeClimbing"] != DBNull.Value  ? (bool)row["RiskOperationsTreeClimbing"] : false;
                retVal.RiskOperationsChainsawInTree = row["RiskOperationsChainsawInTree"] != DBNull.Value  ? (bool)row["RiskOperationsChainsawInTree"] : false;
                retVal.RiskOperationsBranchRemoval = row["RiskOperationsBranchRemoval"] != DBNull.Value  ? (bool)row["RiskOperationsBranchRemoval"] : false;
                retVal.RiskOperationsLoweringTechniquesNecessary = row["RiskOperationsLoweringTechniquesNecessary"] != DBNull.Value  ? (bool)row["RiskOperationsLoweringTechniquesNecessary"] : false;
                retVal.RiskOperationsTreeFelling = row["RiskOperationsTreeFelling"] != DBNull.Value  ? (bool)row["RiskOperationsTreeFelling"] : false;
                retVal.RiskOperationsDeLimbingAndCrossCuting = row["RiskOperationsDeLimbingAndCrossCuting"] != DBNull.Value  ? (bool)row["RiskOperationsDeLimbingAndCrossCuting"] : false;
                retVal.RiskOperationsStormDamage = row["RiskOperationsStormDamage"] != DBNull.Value  ? (bool)row["RiskOperationsStormDamage"] : false;
                retVal.RiskOperationsChemicalApplication = row["RiskOperationsChemicalApplication"] != DBNull.Value  ? (bool)row["RiskOperationsChemicalApplication"] : false;
                retVal.RiskOperationsControlMeasuresNecessary = row["RiskOperationsControlMeasuresNecessary"] != DBNull.Value  ? row["RiskOperationsControlMeasuresNecessary"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryGenericRisker = row["RiskMachineryGenericRisker"] != DBNull.Value ? row["RiskMachineryGenericRisker"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryBrushWoodChipper = row["RiskMachineryBrushWoodChipper"] != DBNull.Value  ? (bool)row["RiskMachineryBrushWoodChipper"] : false;
                retVal.RiskMachineryStumpGrinder = row["RiskMachineryStumpGrinder"] != DBNull.Value  ? (bool)row["RiskMachineryStumpGrinder"] : false;
                retVal.RiskMachineryWinch = row["RiskMachineryWinch"] != DBNull.Value  ? (bool)row["RiskMachineryWinch"] : false;
                retVal.RiskMachineryBrushCutterClearingSaw = row["RiskMachineryBrushCutterClearingSaw"] != DBNull.Value  ? (bool)row["RiskMachineryBrushCutterClearingSaw"] : false;
                retVal.RiskMachineryHedgeTrimmers = row["RiskMachineryHedgeTrimmers"] != DBNull.Value  ? (bool)row["RiskMachineryHedgeTrimmers"] : false;
                retVal.RiskMachineryMechanisedPolePrunner = row["RiskMachineryMechanisedPolePrunner"] != DBNull.Value  ? (bool)row["RiskMachineryMechanisedPolePrunner"] : false;
                retVal.RiskMachineryMobileElevatedWorkPlatform = row["RiskMachineryMobileElevatedWorkPlatform"] != DBNull.Value  ? (bool)row["RiskMachineryMobileElevatedWorkPlatform"] : false;
                retVal.RiskMachineryCrane = row["RiskMachineryCrane"] != DBNull.Value  ? (bool)row["RiskMachineryCrane"] : false;
                retVal.RiskMachineryMountedHydraulicLoadingArm = row["RiskMachineryMountedHydraulicLoadingArm"] != DBNull.Value  ? (bool)row["RiskMachineryMountedHydraulicLoadingArm"] : false;
                retVal.RiskMachineryOther = row["RiskMachineryOther"] != DBNull.Value  ? (bool)row["RiskMachineryOther"] : false;
                retVal.RiskMachinerControlMeasuresNecessary = row["RiskMachinerControlMeasuresNecessary"] != DBNull.Value  ? row["RiskMachinerControlMeasuresNecessary"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskAssessmentCompletedBy = row["CompletedBy"] != DBNull.Value  ? row["CompletedBy"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskAssessmentSignedByNamedPerson = row["RiskAssessmentSignedByNamedPerson"] != DBNull.Value  ? row["RiskAssessmentSignedByNamedPerson"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskAssessmentSignedByOtherStaffPressent = row["RiskAssessmentSignedByOtherStaffPressent"] != DBNull.Value  ? row["RiskAssessmentSignedByOtherStaffPressent"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOnSiteTrainingInspection = row["RiskOnSiteTrainingInspection"] != DBNull.Value  ? row["RiskOnSiteTrainingInspection"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOnSiteTrainingInspectionDetails = row["RiskOnSiteTrainingInspectionDetails"] != DBNull.Value  ? row["RiskOnSiteTrainingInspectionDetails"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOnSiteTrainingInspectionAction = row["RiskOnSiteTrainingInspectionAction"] != DBNull.Value  ? row["RiskOnSiteTrainingInspectionAction"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOnSiteTrainingInspectionSignAndDate = row["RiskOnSiteTrainingInspectionSignAndDate"] != DBNull.Value  ? row["RiskOnSiteTrainingInspectionSignAndDate"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightGeneric = row["RiskWorkingAtHeightGeneric"] != DBNull.Value  ? row["RiskWorkingAtHeightGeneric"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightMewp = row["RiskWorkingAtHeightMewp"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightMewp"] : false;
                retVal.RiskWorkingAtHeightIsItCostEffective = row["RiskWorkingAtHeightIsItCostEffective"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightIsItCostEffective"] : false;
                retVal.RiskWorkingAtHeightLadder = row["RiskWorkingAtHeightLadder"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightLadder"] : false;
                retVal.RiskWorkingAtHeightFallingObjects = row["RiskWorkingAtHeightFallingObjects"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightFallingObjects"] : false;
                retVal.RiskOperationsOther = row["RiskOperationsOther"] != DBNull.Value  ? (bool)row["RiskOperationsOther"] : false;
                retVal.RiskWorkingAtHeightRopeandHarness = row["RiskWorkingAtHeightRopeandHarness"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightRopeandHarness"] : false;
                retVal.RiskWorkingAtHeightControlMeasures = row["RiskWorkingAtHeightControlMeasures"] != DBNull.Value  ? row["RiskWorkingAtHeightControlMeasures"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightCompletedFromGround = row["RiskWorkingAtHeightCompletedFromGround"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightCompletedFromGround"] : false;
                retVal.RiskWorkingAtHeightSafeToClimb = row["RiskWorkingAtHeightSafeToClimb"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightSafeToClimb"] : false;
                retVal.RiskWorkingAtHeightWeatherFavourable = row["RiskWorkingAtHeightWeatherFavourable"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightWeatherFavourable"] : false;
                retVal.RiskWorkingAtHeightAerialRescueArranged = row["RiskWorkingAtHeightAerialRescueArranged"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightAerialRescueArranged"] : false;
                retVal.RiskWorkingAtHeightStaffHoldQualifications = row["RiskWorkingAtHeightStaffHoldQualifications"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightStaffHoldQualifications"] : false;
                retVal.RiskWorkingAtHeightUsePoleSaw = row["RiskWorkingAtHeightUsePoleSaw"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightUsePoleSaw"] : false;
                retVal.RiskWorkingAtHeightStraightFell = row["RiskWorkingAtHeightStraightFell"] != DBNull.Value  ? (bool)row["RiskWorkingAtHeightStraightFell"] : false;
                retVal.RiskOpperationsOther = row["RiskOpperationsOther"] != DBNull.Value  ? (bool)row["RiskOpperationsOther"] : false;
                retVal.CS38 = row["CS38"] != DBNull.Value  ? (bool)row["CS38"] : false;
                retVal.RiskEmergencyAdequateFirstAidProvisionsRef = row["RiskEmergencyAdequateFirstAidProvisionsRef"] != DBNull.Value  ? row["RiskEmergencyAdequateFirstAidProvisionsRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskSiteToPublicRef = row["RiskSiteToPublicRef"] != DBNull.Value  ? row["RiskSiteToPublicRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskSiteTrafficManagementRef = row["RiskSiteTrafficManagementRef"] != DBNull.Value  ? row["RiskSiteTrafficManagementRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskSiteOverheadLinesUndergroundServicesRef = row["RiskSiteOverheadLinesUndergroundServicesRef"] != DBNull.Value  ? row["RiskSiteOverheadLinesUndergroundServicesRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskSiteBuildingOrObstructionsRef = row["RiskSiteBuildingOrObstructionsRef"] != DBNull.Value  ? row["RiskSiteBuildingOrObstructionsRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskSiteSensitiveEmotionallyChargedRef = row["RiskSiteSensitiveEmotionallyChargedRef"] != DBNull.Value  ? row["RiskSiteSensitiveEmotionallyChargedRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskEnvironmentalWaterCourseRef = row["RiskEnvironmentalWaterCourseRef"] != DBNull.Value  ? row["RiskEnvironmentalWaterCourseRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskEnvironmentalProtectedSpeciesHabitatRef = row["RiskEnvironmentalProtectedSpeciesHabitatRef"] != DBNull.Value  ? row["RiskEnvironmentalProtectedSpeciesHabitatRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskEnvironmentalNoiseDustRef = row["RiskEnvironmentalNoiseDustRef"] != DBNull.Value  ? row["RiskEnvironmentalNoiseDustRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsChainSawRef = row["RiskOperationsChainSawRef"] != DBNull.Value  ? row["RiskOperationsChainSawRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsTreeClimbingRef = row["RiskOperationsTreeClimbingRef"] != DBNull.Value  ? row["RiskOperationsTreeClimbingRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsChainsawInTreeRef = row["RiskOperationsChainsawInTreeRef"] != DBNull.Value  ? row["RiskOperationsChainsawInTreeRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsBranchRemovalRef = row["RiskOperationsBranchRemovalRef"] != DBNull.Value  ? row["RiskOperationsBranchRemovalRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsLoweringTechniquesNecessaryRef = row["RiskOperationsLoweringTechniquesNecessaryRef"] != DBNull.Value  ? row["RiskOperationsLoweringTechniquesNecessaryRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsTreeFellingRef = row["RiskOperationsTreeFellingRef"] != DBNull.Value  ? row["RiskOperationsTreeFellingRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsDeLimbingAndCrossCutingRef = row["RiskOperationsDeLimbingAndCrossCutingRef"] != DBNull.Value  ? row["RiskOperationsDeLimbingAndCrossCutingRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsStormDamageRef = row["RiskOperationsStormDamageRef"] != DBNull.Value  ? row["RiskOperationsStormDamageRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsChemicalApplicationRef = row["RiskOperationsChemicalApplicationRef"] != DBNull.Value  ? row["RiskOperationsChemicalApplicationRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryBrushWoodChipperRef = row["RiskMachineryBrushWoodChipperRef"] != DBNull.Value  ? row["RiskMachineryBrushWoodChipperRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryStumpGrinderRef = row["RiskMachineryStumpGrinderRef"] != DBNull.Value  ? row["RiskMachineryStumpGrinderRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryWinchRef = row["RiskMachineryWinchRef"] != DBNull.Value  ? row["RiskMachineryWinchRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryBrushCutterClearingSawRef = row["RiskMachineryBrushCutterClearingSawRef"] != DBNull.Value  ? row["RiskMachineryBrushCutterClearingSawRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryHedgeTrimmersRef = row["RiskMachineryHedgeTrimmersRef"] != DBNull.Value  ? row["RiskMachineryHedgeTrimmersRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryMechanisedPolePrunnerRef = row["RiskMachineryMechanisedPolePrunnerRef"] != DBNull.Value  ? row["RiskMachineryMechanisedPolePrunnerRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryMobileElevatedWorkPlatformRef = row["RiskMachineryMobileElevatedWorkPlatformRef"] != DBNull.Value  ? row["RiskMachineryMobileElevatedWorkPlatformRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryCraneRef = row["RiskMachineryCraneRef"] != DBNull.Value  ? row["RiskMachineryCraneRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryMountedHydraulicLoadingArmRef = row["RiskMachineryMountedHydraulicLoadingArmRef"] != DBNull.Value  ? row["RiskMachineryMountedHydraulicLoadingArmRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskMachineryOtherRef = row["RiskMachineryOtherRef"] != DBNull.Value  ? row["RiskMachineryOtherRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightMewpRef = row["RiskWorkingAtHeightMewpRef"] != DBNull.Value  ? row["RiskWorkingAtHeightMewpRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightIsItCostEffectiveRef = row["RiskWorkingAtHeightIsItCostEffectiveRef"] != DBNull.Value  ? row["RiskWorkingAtHeightIsItCostEffectiveRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightLadderRef = row["RiskWorkingAtHeightLadderRef"] != DBNull.Value  ? row["RiskWorkingAtHeightLadderRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightFallingObjectsRef = row["RiskWorkingAtHeightFallingObjectsRef"] != DBNull.Value  ? row["RiskWorkingAtHeightFallingObjectsRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskOperationsOtherRef = row["RiskOperationsOtherRef"] != DBNull.Value  ? row["RiskOperationsOtherRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightRopeandHarnessRef = row["RiskWorkingAtHeightRopeandHarnessRef"] != DBNull.Value  ? row["RiskWorkingAtHeightRopeandHarnessRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightCompletedFromGroundRef = row["RiskWorkingAtHeightCompletedFromGroundRef"] != DBNull.Value  ? row["RiskWorkingAtHeightCompletedFromGroundRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightSafeToClimbRef = row["RiskWorkingAtHeightSafeToClimbRef"] != DBNull.Value  ? row["RiskWorkingAtHeightSafeToClimbRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightWeatherFavourableRef = row["RiskWorkingAtHeightWeatherFavourableRef"] != DBNull.Value  ? row["RiskWorkingAtHeightWeatherFavourableRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightAerialRescueArrangedRef = row["RiskWorkingAtHeightAerialRescueArrangedRef"] != DBNull.Value  ? row["RiskWorkingAtHeightAerialRescueArrangedRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightStaffHoldQualificationsRef = row["RiskWorkingAtHeightStaffHoldQualificationsRef"] != DBNull.Value  ? row["RiskWorkingAtHeightStaffHoldQualificationsRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightUsePoleSawRef = row["RiskWorkingAtHeightUsePoleSawRef"] != DBNull.Value  ? row["RiskWorkingAtHeightUsePoleSawRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkingAtHeightStraightFellRef = row["RiskWorkingAtHeightStraightFellRef"] != DBNull.Value  ? row["RiskWorkingAtHeightStraightFellRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskSiteRoadsFootpathsPublicAreas = row["RiskSiteRoadsFootpathsPublicAreas"] != DBNull.Value  ? (bool)row["RiskSiteRoadsFootpathsPublicAreas"] : false;
                retVal.RiskSiteRoadsFootpathsPublicAreasRef = row["RiskSiteRoadsFootpathsPublicAreasRef"] != DBNull.Value ? row["RiskSiteRoadsFootpathsPublicAreasRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskSiteOtherRef = row["RiskOpperationsOtherRef"] != DBNull.Value ? row["RiskOpperationsOtherRef"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.GridReference = row["GridReference"] != DBNull.Value  ? row["GridReference"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskSiteReference = row["RiskSiteReference"] != DBNull.Value ? row["RiskSiteReference"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskSiteForman =  row["Forman"] != DBNull.Value ? row["Forman"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                retVal.RiskWorkDate =  row["WorkDate"] != DBNull.Value ?  (DateTime.Parse(row["WorkDate"].ToString())).ToShortDateString() : string.Empty;
                retVal.Signature = row["SignatureImage"] != DBNull.Value ? (Byte[])row["SignatureImage"] : null;
            }
            
            if(retVal != null)
                retVal.AdditionalRisks = AdditionalRisksForAssessment(QuoteNo);
            
            return retVal;
        }
        
        public List<AdditionalRisk> AdditionalRisksForAssessment(int QuoteNo)
        {
            List<AdditionalRisk> retVal = new List<AdditionalRisk>();
            DataSet ds = DBHelper.ExecuteStoredProcDataSet("RiskAdditionalRisksForQuoteNoGet", new SqlParameter[] { new SqlParameter("@QuoteNo", QuoteNo) }, _DatabaseName);
            
            AdditionalRisk risk = null;
            
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                risk = new AdditionalRisk();
                risk.AssessedRisk = row["AssessedRisk"] != DBNull.Value ? row["AssessedRisk"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                risk.ControlMeasures = row["ControlMeasures"] != DBNull.Value ? row["ControlMeasures"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                
                retVal.Add(risk);
            }
            
            return retVal;
        }
        
        /// <summary>
        /// Returns a list of all Risk References in the database.
        /// </summary>
        /// <returns></returns>
        public List<RiskReference> ChapterReferenceList(int QuoteNo)
        {
            DataSet ReferenceList;
            
            List<RiskReference> ReturnValue = new List<RiskReference>();
            
            SqlParameter param = new SqlParameter();
            
            ReferenceList = DBHelper.ExecuteStoredProcDataSet("RiskChapterRefListForQuoteNo", new SqlParameter[] { new SqlParameter("@QuoteNo", QuoteNo) }, _DatabaseName);
            foreach (DataRow Row in ReferenceList.Tables[0].Rows)
            {
                ReturnValue.Add(new RiskReference()
                {
                    AssessmentItem = Row["AssessmentItem"].ToString().Replace('-', '_').Replace("@", "At").Replace("[", "").Replace("]", ""),
                    Reference = Row["Reference"].ToString()
                });
            }
            Console.WriteLine("Number of reference items = {0}", ReturnValue.Count.ToString());
            return ReturnValue;
        }
    }
}

