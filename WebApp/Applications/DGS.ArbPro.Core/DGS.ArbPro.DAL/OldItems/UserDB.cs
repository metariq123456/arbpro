﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace DGS.ArbPro.DAL
{
    public class UserDB
    {
        public void SetStaffId(int userId, int StaffId)
        {
            SqlParameter[] Parameters = new SqlParameter[]
            {
                new SqlParameter("@AccountId", userId),
                new SqlParameter("@AllocatedStaffId", StaffId)
            };
            DBHelper.ExecuteNonQuery("UserAccountStaffNumberUpdate", System.Data.CommandType.StoredProcedure, Parameters, "ArbProMaster");
        }

        public Dictionary<int, String> GetUnassignedUsers(int ClientId, int StaffUserId)
        {
            SqlParameter[] Parameters = new SqlParameter[]
            {
                new SqlParameter("@ClientId", ClientId),
                new SqlParameter("@StaffUserId", StaffUserId)
            };
            DataSet Result = DBHelper.ExecuteStoredProcDataSet("UserAccountsForClientIdGet", Parameters, "ArbProMaster");

            Dictionary<int, String> ReturnValue = new Dictionary<int, String>();

            foreach (DataRow Row in Result.Tables[0].Rows)
            {
                ReturnValue.Add(
                    int.Parse(Row["UserAccountId"].ToString()),
                    Row["EmailAddress"].ToString());
            }

            return ReturnValue;
        }

        public int GetStaffUser(int ClientId, int StaffId)
        {
            SqlParameter[] Parameters = new SqlParameter[]
            {
                new SqlParameter("@ClientId", ClientId),
                new SqlParameter("@StaffId", StaffId)
            };

            DataSet Result = DBHelper.ExecuteStoredProcDataSet("UserAccountForStaffIdGet", Parameters, "ArbProMaster");

            int ReturnValue = -1;

            int.TryParse(Result.Tables[0].Rows[0]["UserAccountId"].ToString(), out ReturnValue);

            return ReturnValue;
        }

        public void SetStaffUser(String ClientDBName, int UserId, int StaffId)
        {
            SqlParameter[] Parameters = new SqlParameter[]
            {
                new SqlParameter("@AccountId", UserId),
                new SqlParameter("@AllocatedStaffId", StaffId)
            };

            DBHelper.ExecuteNonQuery("UserAccountStaffNumberUpdate", CommandType.StoredProcedure, Parameters, "ArbProMaster");
            DBHelper.ExecuteNonQuery("UserAccoutnStaffNumberUpdate", CommandType.StoredProcedure, Parameters, ClientDBName);
        }
   }
}
