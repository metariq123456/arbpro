﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;

namespace DGS.ArbPro.DAL
{
    public class QuoteItemPictureDb
    {
        string _DatabaseName;

        public QuoteItemPictureDb(string dbName)
        {
            _DatabaseName = dbName;
        }

        public List<QuoteItemPicture> QuoteItemPicturesForQuoteNoGet(int quoteNo)
        {
            List<QuoteItemPicture> retVals = new List<QuoteItemPicture>();
            QuoteItemPicture pic = null ;
            DataSet ds = DBHelper.ExecuteStoredProcDataSet("PictureForQuoteNoGet", new SqlParameter[] { new SqlParameter("@QuoteNo", quoteNo) }, _DatabaseName);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                pic = new QuoteItemPicture();
                pic.ImageData = row["Picture"] != DBNull.Value ? (Byte[])row["Picture"] : null;
                pic.QuoteTree = row["QuoteTree"] != DBNull.Value ? row["QuoteTree"].ToString() : string.Empty;
                pic.QuoteTreeNo = row["QuoteTreeNo"] != DBNull.Value ? row["QuoteTreeNo"].ToString() : string.Empty;
                pic.Description = row["QuoteDescription"] != DBNull.Value ? row["QuoteDescription"].ToString() : string.Empty;
                pic.Guid = row["UploadItemGuid"] != DBNull.Value ? row["UploadItemGuid"].ToString() : string.Empty;
                retVals.Add(pic);
            }
            return retVals;
        }
    }
}

