﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class TextTemplate
    {
        public enum Type
        {
            Quote,
            Invoice,
            Marketing,
            Return
        }

        public enum Message
        {
            Letter,
            Email
        }

        public Int32 ID { get; set; }
        public String TemplateName { get; set; }
        public Type ObjectType { get; set; }
        public Message MessageType { get; set; }

        public String BodyText { get; set; }
        public String AdditionalText { get; set; } // Email Subject or Payment Info etc

        public TextTemplate(String Name, Type Obj, Message Msg)
        {
            TemplateName = Name;
            ObjectType = Obj;
            MessageType = Msg;
        }

        public TextTemplate(SqlDataReader Row)
        {
            ID = Row.IntegerValue("ID");
            TemplateName = Row.StringValue("TemplateName");
            ObjectType = (Type)Row.IntegerValue("ObjectType");
            MessageType = (Message)Row.IntegerValue("MessageType");

            BodyText = Row.StringValue("BodyText");
            AdditionalText = Row.StringValue("AdditionalText");
        }
    }
}
