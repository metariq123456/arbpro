﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DatabaseAdmin
{
    public class DatabaseInfo
    {
        public string Name { get; set; }
        public int Number { get; set; }
    }
}
