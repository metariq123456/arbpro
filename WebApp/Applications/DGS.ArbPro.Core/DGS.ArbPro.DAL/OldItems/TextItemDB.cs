﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using PDFReport;

namespace DGS.ArbPro.DAL
{
    public class TextItemDB
    {
        public static List<TextItem> GetTextItemsForGroup(string databaseName, int groupId)
        {
            List<TextItem> retVal = new List<TextItem>();
            DataSet ds;
            TextItem item = null;
            ds = DBHelper.ExecuteStoredProcDataSet("ReportTextItemsForGroupGet", new SqlParameter[] { new SqlParameter("@GroupId", groupId) }, databaseName);
            foreach (DataRow Row in ds.Tables[0].Rows)
            {
                item = new TextItem(Row["Text"].ToString(), double.Parse(Row["XPos"].ToString()), double.Parse(Row["YPos"].ToString()), double.Parse(Row["Width"].ToString()), Row["Name"].ToString());
                item.Colour = Row["Colour"].ToString();
                item.FontName = Row["Font"].ToString();
                item.HAlign = Row["HAlign"].ToString();
                item.VAlign = Row["VAlign"].ToString();
                item.Height = double.Parse(Row["Height"].ToString());
                item.PointSize = double.Parse(Row["PointSize"].ToString());
                retVal.Add(item);
            }
            return retVal;
        }

        public static int InsertTextItemForGroup(string databaseName, int groupID, TextItem item)
        {
            try
            {
                int ReturnValue;
                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@GroupId", groupID),
                    new SqlParameter("@Name", item.Name), 
                    new SqlParameter("@HAlign",item.HAlign),
                    new SqlParameter("@VAlign",item.VAlign),
	                new SqlParameter("@XPos", item.X),
	                new SqlParameter("@YPos", item.Y),
	                new SqlParameter("@Width", item.Width),
                    new SqlParameter("@Height",item.Height),
                    new SqlParameter("@Text",item.Text),
                    new SqlParameter("@ItemType",(int)item.SpecialType),
                    new SqlParameter("@Font",item.FontName),
                    new SqlParameter("@PointSize", item.Font.Size),
                    new SqlParameter("@Colour",item.Colour),
                };
                ReturnValue = DBHelper.ExecuteStoredProcReturnID("ReportTextItemInsert", parameters, databaseName);
                item.Id = ReturnValue;
                return ReturnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error inserting a new line item: {0}", ex.ToString());
                return -1;
            }
        }

    }
}
