﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    internal static class RepeaterExtensions
    {
        public static Int32 IntegerValue(this SqlDataReader Reader, String ColumnName)
        {
            try
            {
                return (Int32)Reader[ColumnName];
            }
            catch 
            {
                try
                {
                    return Int32.Parse(Reader[ColumnName].ToString());
                }
                catch
                {
                    return 0;
                }
            }
        }

        public static String StringValue(this SqlDataReader Reader, String ColumnName)
        {
            try
            {
                return (String)Reader[ColumnName];
            }
            catch 
            {
                return "";
            }
        }

        public static Decimal DecimalValue(this SqlDataReader Reader, String ColumnName)
        {
            try
            {
                return (Decimal)Reader[ColumnName];
            }
            catch (Exception ex)
            {
                try
                {
                    return Decimal.Parse(Reader[ColumnName].ToString());
                }
                catch (Exception ex2)
                {
                    return 0;
                }
            }
        }

        public static Byte[] ByteValue(this SqlDataReader Reader, String ColumnName)
        {
            try
            {
                return (Byte[])Reader[ColumnName];
            }
            catch
            {
                return new Byte[0];
            }
        }

        public static DateTime DateValue(this SqlDataReader Reader, String ColumnName)
        {
            try
            {
                return (DateTime)Reader[ColumnName];
            }
            catch
            {
                return new DateTime();
            }
        }

        public static Boolean BooleanValue(this SqlDataReader Reader, String ColumnName)
        {
            try
            {
                return (Boolean)Reader[ColumnName];
            }
            catch
            {
                return false;
            }
        }
    }
}
