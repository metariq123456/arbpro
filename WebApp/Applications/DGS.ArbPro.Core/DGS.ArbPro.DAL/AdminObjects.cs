﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class AdminUser
    {
        public String UserId { get; set; }
        public String UserName { get; set; }
        public String LoweredUserName { get { return UserName.ToLower(); } }
        public String EmailAddress { get; set; }
        public String LoweredEmailAddress { get { return EmailAddress.ToLower(); } }
        public DateTime CreateDate { get; set; }
        public DateTime LastLoginDate { get; set; }
        public DateTime LastActivityDate { get; set; }
        public DateTime LastPasswordChangedDate { get; set; }
        public Boolean Locked { get; set; }
        public String Comment { get; set; }
        
        public static AdminUser CreateFromRow(DataRow Row, String UserId = "")
        {
            AdminUser ReturnValue = new AdminUser();

            if (String.IsNullOrEmpty(UserId))
                ReturnValue.UserId = Row["UserId"].ToString();
            else
                ReturnValue.UserId = UserId;

            ReturnValue.UserName = Row["UserName"].ToString();
            ReturnValue.EmailAddress = Row["Email"].ToString();
            ReturnValue.CreateDate = DateTime.Parse(Row["CreateDate"].ToString());
            ReturnValue.LastLoginDate = DateTime.Parse(Row["LastLoginDate"].ToString());
            ReturnValue.LastActivityDate = DateTime.Parse(Row["LastActivityDate"].ToString());
            ReturnValue.LastPasswordChangedDate = DateTime.Parse(Row["LastPasswordChangedDate"].ToString());
            ReturnValue.Locked = Boolean.Parse(Row["IsLockedOut"].ToString());
            ReturnValue.Comment = Row["Comment"].ToString();

            return ReturnValue;
        }

        public static String GeneratePassword()
        {
            StringBuilder SB = new StringBuilder();
            Random Gen = new Random();

            char ch;
            for (int i = 0; i < 7; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * Gen.NextDouble() + 65)));
                SB.Append(ch);
            }
            SB.Append("!");
            return SB.ToString();
        }
    }

    public class AdminRole
    {
        public static String[] RoleCategories = new String[] { "Admin Management", "Client Management", "Logging" };
        public static String[][] Roles = new String[][] { new String[] { "Edit Info", "Security", "Roles", "Create Users"}, new String[] { "Create Clients", "Edit Clients", "Create Users", "Edit Users", "Support" }, new String[] { "Application Log", "Mobile Log", "IIS Log" } };

        public static Dictionary<String, String[]> CategorisedRoles = new Dictionary<String, String[]>();

        static AdminRole()
        {
            for (int i = 0; i < RoleCategories.Length; i++)
                CategorisedRoles.Add(RoleCategories[i], Roles[i]);
        }

        public static Dictionary<int, String> ListCategories()
        {
            Dictionary<int, String> ReturnValue = new Dictionary<int, String>();

            for (int i = 0; i < RoleCategories.Length; i++)
            {
                ReturnValue.Add(i, RoleCategories[i]);
            }

            return ReturnValue;
        }

        public static Dictionary<String, Boolean> ListRoles(String Category)
        {
            Dictionary<String, Boolean> ReturnValue = new Dictionary<String, Boolean>();

            foreach (String Role in CategorisedRoles[Category])
            {
                ReturnValue.Add(Role, false);
            }

            return ReturnValue;
        }
    }
}

