﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class User : DataObjectBase
    {
        #region Properties
        public int ClientOrganisationID { get; set; }
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string NamePrefixID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public bool UserAccountEnabled { get; set; }
        public bool MobileEditionEnabled { get; set; }
        public int AuthenticationFailures { get; set; }
        public string ClientOrganisationPosition { get; set; }
        public bool TsAndCsAccepted { get; set; }
        public int StaffId { get; set; }
        #endregion

    }
}
