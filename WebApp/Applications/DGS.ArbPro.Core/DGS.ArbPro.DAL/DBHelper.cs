﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace DGS.ArbPro.DAL
{
    internal class DBHelper
    {
        static string MasterConnectionString = ConfigurationManager.ConnectionStrings["DGS.ArbPro.Core.My.MySettings.MasterDatabase_ConnectionString"].ConnectionString;


        // This function will be used to execute R(CRUD) operation of parameterless commands
        internal static DataTable ExecuteSelectCommand(string CommandName, CommandType cmdType, string databaseName)
        {
            string ConnectionString = "";

            if (string.IsNullOrEmpty(databaseName))
            {
                ConnectionString = MasterConnectionString;
            }
            else
            {
                var Builder = new SqlConnectionStringBuilder(MasterConnectionString);
                Builder.ConnectionString = MasterConnectionString;
                Builder.InitialCatalog = databaseName;
                ConnectionString = Builder.ConnectionString;

            }

            DataTable table = null;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = cmdType;
                    cmd.CommandText = CommandName;

                    try
                    {
                        if (con.State != ConnectionState.Open)
                        {
                            con.Open();
                        }

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            table = new DataTable();
                            da.Fill(table);
                        }
                    }
                    catch
                    {
                        throw;
                    }
                }
            }

            return table;
        }

        // This function will be used to execute R(CRUD) operation of parameterized commands
        internal static DataTable ExecuteParamerizedSelectCommand(string CommandName, CommandType cmdType, SqlParameter[] param, string databaseName)
        {

            string ConnectionString = "";

            if (string.IsNullOrEmpty(databaseName))
            {
                ConnectionString = MasterConnectionString;
            }
            else
            {
                var Builder = new SqlConnectionStringBuilder(MasterConnectionString);
                Builder.ConnectionString = MasterConnectionString;
                Builder.InitialCatalog = databaseName;
                ConnectionString = Builder.ConnectionString;

            }
            DataTable table = new DataTable();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = cmdType;
                    cmd.CommandText = CommandName;
                    cmd.Parameters.AddRange(param);

                    try
                    {
                        if (con.State != ConnectionState.Open)
                        {
                            con.Open();
                        }

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            da.Fill(table);
                        }
                    }
                    catch
                    {
                        throw;
                    }
                }
            }

            return table;
        }

        // This function will be used to execute CUD(CRUD) operation of parameterized commands
        internal static int ExecuteNonQuery(string CommandName, CommandType cmdType, SqlParameter[] pars, string databaseName)
        {
            int result = 0;
            string ConnectionString = "";

            if (string.IsNullOrEmpty(databaseName))
            {
                ConnectionString = MasterConnectionString;
            }
            else
            {
                var Builder = new SqlConnectionStringBuilder(MasterConnectionString);
                Builder.ConnectionString = MasterConnectionString;
                Builder.InitialCatalog = databaseName;
                ConnectionString = Builder.ConnectionString;
            }

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = cmdType;
                    cmd.CommandText = CommandName;
                    cmd.Parameters.AddRange(pars);

                    try
                    {
                        if (con.State != ConnectionState.Open)
                        {
                            con.Open();
                        }

                        result = cmd.ExecuteNonQuery();
                    }
                    catch
                    {
                        throw;
                    }
                }
            }

            return result;
        }

        internal static SqlDataReader ExecuteReader(String CommandName, SqlParameter[] Params, String DatabaseName)
        {
            SqlDataReader ReturnValue = null;
            string ConnectionString = "";

            if (string.IsNullOrEmpty(DatabaseName))
            {
                ConnectionString = MasterConnectionString;
            }
            else
            {
                var Builder = new SqlConnectionStringBuilder(MasterConnectionString);
                Builder.ConnectionString = MasterConnectionString;
                Builder.InitialCatalog = DatabaseName;
                ConnectionString = Builder.ConnectionString;
            }

            SqlConnection con = new SqlConnection(ConnectionString);
            SqlCommand cmd = new SqlCommand();

            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = CommandName;
            cmd.Parameters.AddRange(Params);

            con.Open();
            try
            {
                ReturnValue = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return ReturnValue;
        }

        internal static DataSet ExecuteStoredProcDataSet(string CommandName, SqlParameter[] pars, string databaseName)
        {
            DataSet ReturnValue = null;
            SqlDataAdapter Adapter;
            string ConnectionString = "";

            if (string.IsNullOrEmpty(databaseName))
            {
                ConnectionString = MasterConnectionString;
            }
            else
            {
                var Builder = new SqlConnectionStringBuilder(MasterConnectionString);
                Builder.ConnectionString = MasterConnectionString;
                Builder.InitialCatalog = databaseName;
                ConnectionString = Builder.ConnectionString;
            }

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = CommandName;
                    cmd.Parameters.AddRange(pars);

                    try
                    {
                        if (con.State != ConnectionState.Open)
                        {
                            con.Open();
                        }
                        ReturnValue = new DataSet();
                        Adapter = new SqlDataAdapter();
                        Adapter.SelectCommand = cmd;
                        Adapter.Fill(ReturnValue, "Table");
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return ReturnValue;
        }

        internal static int ExecuteStoredProcRowCount(string CommandName, SqlParameter[] pars, string databaseName)
        {
            int ReturnValue = -1;
            string ConnectionString = "";

            if (string.IsNullOrEmpty(databaseName))
            {
                ConnectionString = MasterConnectionString;
            }
            else
            {
                var Builder = new SqlConnectionStringBuilder(MasterConnectionString);
                Builder.ConnectionString = MasterConnectionString;
                Builder.InitialCatalog = databaseName;
                ConnectionString = Builder.ConnectionString;
            }

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = CommandName;
                    cmd.Parameters.AddRange(pars);

                    try
                    {
                        if (con.State != ConnectionState.Open)
                        {
                            con.Open();
                        }
                        ReturnValue = cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return ReturnValue;
        }

        internal static int ExecuteStoredProcReturnID(string CommandName, SqlParameter[] pars, string databaseName)
        {
            int ReturnValue = -1;
            string ConnectionString = "";

            if (string.IsNullOrEmpty(databaseName))
            {
                ConnectionString = MasterConnectionString;
            }
            else
            {
                var Builder = new SqlConnectionStringBuilder(MasterConnectionString);
                Builder.ConnectionString = MasterConnectionString;
                Builder.InitialCatalog = databaseName;
                ConnectionString = Builder.ConnectionString;
            }

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = CommandName;
                    cmd.Parameters.AddRange(pars);

                    try
                    {
                        if (con.State != ConnectionState.Open)
                        {
                            con.Open();
                        }
                        var Result = cmd.ExecuteReader();

                        while (Result.Read())
                            ReturnValue = Int32.Parse(Result["ID"].ToString());
                        Result.Close();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    finally
                    {
                        con.Close();
                    }
                }
            }

            return ReturnValue;
        }

        // This function will be used to execute CUD(CRUD) operation of parameterized commands
        internal static int ExecuteScalar(string CommandName, CommandType cmdType, SqlParameter[] pars, string databaseName)
        {
            int result = 0;
            string ConnectionString = "";

            if (string.IsNullOrEmpty(databaseName))
            {
                ConnectionString = MasterConnectionString;
            }
            else
            {
                var Builder = new SqlConnectionStringBuilder(MasterConnectionString);
                Builder.ConnectionString = MasterConnectionString;
                Builder.InitialCatalog = databaseName;

            }

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandType = cmdType;
                    cmd.CommandText = CommandName;
                    cmd.Parameters.AddRange(pars);

                    try
                    {
                        if (con.State != ConnectionState.Open)
                        {
                            con.Open();
                        }

                        result = (int)cmd.ExecuteScalar();
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }

            return result;
        }

    }
}
