﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;


namespace DGS.ArbPro.DAL
{
    public class ClientOrganisationDB
    {
        public int Add(ClientOrganisation clientOrganisation)
        {
            int NewID;
            ServerManager Manager;
            SqlParameter IDParameter = new SqlParameter("@ID", SqlDbType.Int);
            IDParameter.Direction = ParameterDirection.Output;
            SqlParameter[] parameters = new SqlParameter[]
            {                
	            new SqlParameter("@OrganisationName",clientOrganisation.OrganisationName),
	            new SqlParameter("@Address1", clientOrganisation.Address1),
	            new SqlParameter("@Address2",clientOrganisation.Address2),
	            new SqlParameter("@Town",clientOrganisation.Town),
	            new SqlParameter("@CountyOrState",clientOrganisation.CountyOrState),
	            new SqlParameter("@PostcodeOrZip",clientOrganisation.PostcodeOrZip),
	            new SqlParameter("@CountryID",clientOrganisation.CountryID),
	            new SqlParameter("@UserAccountsEnabled",clientOrganisation.UserAccountsEnabled ? 1:0),
	            new SqlParameter("@MobileEditionEnabled",clientOrganisation.MobileEditionEnabled ? 1: 0),
                IDParameter
            };

           DBHelper.ExecuteStoredProcRowCount("uspClientOrganisationAdd",  parameters,"ArbProMaster");
            NewID = (int)IDParameter.Value;

           if (NewID > 0)
           {
               Manager = new ServerManager();
               if (Manager.CreateNewClientDatabase(NewID))
               {
                   return NewID;
               }
               else
               {
                   return -1;
               }
           }

           return NewID;

        }

        public int Add(ClientUser User, String Password)
        {
            int NewID = -1;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@ClientOrganisationID", User.ClientOrganisation),
                new SqlParameter("@EmailAddress", User.Email),
                new SqlParameter("@Password", Password),
                new SqlParameter("@NamePrefixID", User.NamePrefixID),
                new SqlParameter("@FirstName", User.FirstName),
                new SqlParameter("@LastName", User.LastName),
                new SqlParameter("@Telephone", User.Telephone),
                new SqlParameter("@ClientOrganisationPosition", User.Position),
                new SqlParameter("@UserAccountEnabled", User.Enabled),
                new SqlParameter("@MobileEditionEnabled", User.Mobile)
            };

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("uspUserAccountAdd", Params, "ArbProMaster");

            foreach (DataRow Row in DS.Tables[0].Rows)
            {
                NewID = int.Parse(Row["ReturnID"].ToString());
            }

            return NewID;
        }

        public void Save(ClientOrganisation clientOrganisation)
        {
            SqlParameter[] Params = new SqlParameter[]
            {
                new SqlParameter("@ClientID", clientOrganisation.ID),
	            new SqlParameter("@OrganisationName",clientOrganisation.OrganisationName),
	            new SqlParameter("@Address1", clientOrganisation.Address1),
	            new SqlParameter("@Address2",clientOrganisation.Address2),
	            new SqlParameter("@Town",clientOrganisation.Town),
	            new SqlParameter("@CountyOrState",clientOrganisation.CountyOrState),
	            new SqlParameter("@PostcodeOrZip",clientOrganisation.PostcodeOrZip),
	            new SqlParameter("@CountryID",clientOrganisation.CountryID),
	            new SqlParameter("@UserAccountsEnabled",clientOrganisation.UserAccountsEnabled ? 1:0),
	            new SqlParameter("@MobileEditionEnabled",clientOrganisation.MobileEditionEnabled ? 1: 0)
            };

            DBHelper.ExecuteStoredProcDataSet("uspClientOrganisationSave", Params, "ArbProMaster");
        }

        public void Save(ClientUser User)
        {
            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@UserID", User.ID),
                new SqlParameter("@NamePrefixID", User.NamePrefixID),
                new SqlParameter("@FirstName", User.FirstName),
                new SqlParameter("@LastName", User.LastName),
                new SqlParameter("@Email", User.Email),
                new SqlParameter("@Telephone", User.Telephone),
                new SqlParameter("@Position", User.Position),
                new SqlParameter("@Enabled", User.Enabled),
                new SqlParameter("@Mobile", User.Mobile)
            };

            DBHelper.ExecuteStoredProcDataSet("uspUserAccountSave", Params, "ArbProMaster");
        }

        public List<ClientOrganisation> List()
        {
            List<ClientOrganisation> ReturnValue = new List<ClientOrganisation>();

            SqlParameter[] Params = new SqlParameter[0];

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("uspClientOrganisationList", Params, "ArbProMaster");

            foreach (DataRow Row in DS.Tables[0].Rows)
            {
                ClientOrganisation Org = new ClientOrganisation();
                Org.ID = int.Parse(Row["ID"].ToString());
                Org.OrganisationName = Row["Name"].ToString();
                Org.Address1 = Row["Address1"].ToString();
                Org.Address2 = Row["Address2"].ToString();
                Org.Town = Row["Town"].ToString();
                Org.CountyOrState = Row["CountyOrState"].ToString();
                Org.PostcodeOrZip = Row["PostcodeOrZip"].ToString();
                Org.CountryID = int.Parse(Row["CountryID"].ToString());
                Org.Country = Row["Country"].ToString();
                Org.UserAccountsEnabled = Boolean.Parse(Row["UserAccountsEnabled"].ToString());
                Org.MobileEditionEnabled = Boolean.Parse(Row["MobileEditionEnabled"].ToString());
                ReturnValue.Add(Org);
            }

            return ReturnValue;
        }

        public ClientOrganisation GetForId(int Id)
        {
            ClientOrganisation Org = new ClientOrganisation();

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@ClientID", Id)
            };

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("uspClientOrganisationGet", Params, "ArbProMaster");

            foreach (DataRow Row in DS.Tables[0].Rows)
            {
                Org.ID = int.Parse(Row["ID"].ToString());
                Org.OrganisationName = Row["Name"].ToString();
                Org.Address1 = Row["Address1"].ToString();
                Org.Address2 = Row["Address2"].ToString();
                Org.Town = Row["Town"].ToString();
                Org.CountyOrState = Row["CountyOrState"].ToString();
                Org.PostcodeOrZip = Row["PostcodeOrZip"].ToString();
                Org.CountryID = int.Parse(Row["CountryID"].ToString());
                Org.Country = Row["Country"].ToString();
                Org.UserAccountsEnabled = Boolean.Parse(Row["UserAccountsEnabled"].ToString());
                Org.MobileEditionEnabled = Boolean.Parse(Row["MobileEditionEnabled"].ToString());
            }

            return Org;
        }

        public List<ClientCountry> ListCountries()
        {
            List<ClientCountry> ReturnValue = new List<ClientCountry>();

            SqlParameter[] Params = new SqlParameter[0];

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("uspCountryList", Params, "ArbProMaster");

            foreach (DataRow Row in DS.Tables[0].Rows)
            {
                ClientCountry C = new ClientCountry();
                C.ID = int.Parse(Row["ID"].ToString());
                C.ISOCode = Row["ISOCode"].ToString();
                C.Name = Row["CountryName"].ToString();
                ReturnValue.Add(C);
            }

            return ReturnValue;
        }

        public List<ClientUser> ListUsers(int ClientID)
        {
            List<ClientUser> ReturnValue = new List<ClientUser>();

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@ClientId", ClientID)
            };

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("uspUserAccountList", Params, "ArbProMaster");

            foreach (DataRow Row in DS.Tables[0].Rows)
            {
                ClientUser User = new ClientUser();
                User.ID = int.Parse(Row["ID"].ToString());
                User.ClientOrganisation = int.Parse(Row["ClientOrganisation"].ToString());
                User.Email = Row["EmailAddress"].ToString();
                User.NamePrefixID = int.Parse(Row["NamePrefixID"].ToString());
                User.NamePrefix = Row["NamePrefix"].ToString();
                User.FirstName = Row["FirstName"].ToString();
                User.LastName = Row["LastName"].ToString();
                User.Telephone = Row["Telephone"].ToString();
                User.Enabled = Boolean.Parse(Row["AccountEnabled"].ToString());
                User.Mobile = Boolean.Parse(Row["MobileEnabled"].ToString());
                User.AuthFailures = int.Parse(Row["AuthFailures"].ToString());
                User.Position = Row["OrganisationPosition"].ToString();
                User.TsAndCsAccepted = Boolean.Parse(Row["TsAndCsAccepted"].ToString());
                ReturnValue.Add(User);
            }

            return ReturnValue;
        }

        public ClientUser GetUser(int UserId)
        {
            ClientUser User = new ClientUser();

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@UserId", UserId)
            };

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("uspUserAccountGet", Params, "ArbProMaster");

            foreach (DataRow Row in DS.Tables[0].Rows)
            {
                User.ID = int.Parse(Row["ID"].ToString());
                User.ClientOrganisation = int.Parse(Row["ClientOrganisation"].ToString());
                User.Email = Row["EmailAddress"].ToString();
                User.NamePrefixID = int.Parse(Row["NamePrefixID"].ToString());
                User.NamePrefix = Row["NamePrefix"].ToString();
                User.FirstName = Row["FirstName"].ToString();
                User.LastName = Row["LastName"].ToString();
                User.Telephone = Row["Telephone"].ToString();
                User.Enabled = Boolean.Parse(Row["AccountEnabled"].ToString());
                User.Mobile = Boolean.Parse(Row["MobileEnabled"].ToString());
                User.AuthFailures = int.Parse(Row["AuthFailures"].ToString());
                User.Position = Row["OrganisationPosition"].ToString();
                User.TsAndCsAccepted = Boolean.Parse(Row["TsAndCsAccepted"].ToString());
            }

            return User;
        }

        public void UnlockAccount(int UserID)
        {
            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@UserAccountID", UserID)
            };

            DBHelper.ExecuteStoredProcDataSet("uspUserAccountUnlock", Params, "ArbProMaster");
        }

        public void ResetPassword(int UserID, String Password)
        {
            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@UserID", UserID),
                new SqlParameter("@PassHash", Password)
            };

            DBHelper.ExecuteStoredProcDataSet("uspUserAccountSetPassword", Params, "ArbProMaster");
        }

        public List<ClientUserPrefix> ListPrefixes()
        {
            List<ClientUserPrefix> ReturnValue = new List<ClientUserPrefix>();

            SqlParameter[] Params = new SqlParameter[0];

            DataSet DS = DBHelper.ExecuteStoredProcDataSet("uspNamePrefixGetList", Params, "ArbProMaster");

            foreach (DataRow Row in DS.Tables[0].Rows)
            {
                ClientUserPrefix C = new ClientUserPrefix();
                C.ID = int.Parse(Row["ID"].ToString());
                C.Prefix = Row["Description"].ToString();
                ReturnValue.Add(C);
            }

            return ReturnValue;
        }
    }
}
