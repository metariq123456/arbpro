﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using PDFReport;

namespace DGS.ArbPro.DAL
{
    public class CustomGroupDb
    {

        public static List<CustomGroup> GetCustomGroupsForClient(string databaseName)
        {
            
            DataSet ds;
            List<CustomGroup> returnValue = new List<CustomGroup>();
            List<Item> items = new List<Item>();

            CustomGroup g = null;
            int id = 0;
            ds = DBHelper.ExecuteStoredProcDataSet("ReportCustomGroupsGet", new SqlParameter[] { }, databaseName);
            foreach (DataRow Row in ds.Tables[0].Rows)
            {
                items = new List<Item>();
                g = new CustomGroup(Row["Name"].ToString(), double.Parse(Row["Height"].ToString()));
                //Set the remaining group properties
                g.Id = (int)Row["ID"];
                g.RelativeTo = (PrintRelativeTo)int.Parse(Row["RelativeTo"].ToString());
                g.BottomPadding = double.Parse(Row["BottomPadding"].ToString());
                //Get the group items from the database
                items.AddRange(TextItemDB.GetTextItemsForGroup(databaseName, g.Id));
                items.AddRange(LineItemDB.GetLineItemsForGroup(databaseName, g.Id));
                items.AddRange(ImageItemDB.GetImageItemsForGroup(databaseName, g.Id));
                foreach (Item itm in items)
                {
                    g.AddItem(itm);
                }
                returnValue.Add(g);
            }
            return returnValue;
        }

        public static int InsertOrUpdateCustomGroup(string databaseName, CustomGroup group)
        {
            string proc = "";
            if (group.Id < 1)
                proc = "ReportCustomGroupInsert";
            else
                proc = "ReportCustomGroupUpdate"; ;

            try
            {
                int ReturnValue;

                SqlParameter[] parameters = new SqlParameter[]
                {
                    new SqlParameter("@ID", group.Id),
                    new SqlParameter("@Name", group.Name), 
                    new SqlParameter("@Height",group.Height),
	                new SqlParameter("@BottomPadding", group.BottomPadding),
	                new SqlParameter("@RelativeTo", (int)group.RelativeTo)
                };
                ReturnValue = DBHelper.ExecuteStoredProcReturnID(proc, parameters, databaseName);

                if (ReturnValue > 0)
                    group.Id = ReturnValue;
                else
                    throw new Exception("Failed to recieve a valid Id for a new custom group.");

                foreach (Item itm in group.GetAllItems())
                {
                    if (itm.GetType() == typeof(LineItem))
                        LineItemDB.InsertLineItemForGroup(databaseName, group.Id, (LineItem)itm);
                    else if (itm.GetType() == typeof(ImageItem))
                        ImageItemDB.InsertImageItemForGroup(databaseName, group.Id, (ImageItem)itm);
                    else
                        TextItemDB.InsertTextItemForGroup(databaseName, group.Id, (TextItem)itm);
                }

                return ReturnValue;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error inserting a new custom group: {0}", ex.ToString());
                return -1;
            }
        }
    }
}
