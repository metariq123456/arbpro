﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class CompanyDB
    {
        String DatabaseName;

        public CompanyDB(String DBName)
        {
            DatabaseName = DBName;
        }

        public Company GetCompany()
        {
            Company ReturnValue = null;

            SqlParameter[] Params = new SqlParameter[0];

            var Result = DBHelper.ExecuteReader(
                "CompanyGet",
                Params,
                DatabaseName);

            while (Result.Read())
                ReturnValue = new Company(Result);
            Result.Close();

            return ReturnValue;
        }

        public void SaveCompany(Company Object)
        {
            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@CoCode", Object.CoCode),
                new SqlParameter("@CompanyName", Object.CompanyName),
                new SqlParameter("@CompanyAddress", Object.CompanyAddress),
                new SqlParameter("@CompanyPostCode", Object.CompanyPostCode),
                new SqlParameter("@CompanyVATNo", Object.CompanyVATNo),
                new SqlParameter("@CompanyRegistrationNo", Object.CompanyRegistrationNo),
                new SqlParameter("@CompanyWebsiteAddress", Object.CompanyWebsiteAddress),
                new SqlParameter("@CompanyTelephoneNumber", Object.CompanyTelephoneNumber),
                new SqlParameter("@CompanyDirectorName", Object.CompanyDirectorName),
                new SqlParameter("@CompanyDirectorEmailAddress", Object.CompanyDirectorEmailAddress),
                new SqlParameter("@CompanyDirectorQualifications", Object.CompanyDirectorQualifications),
                new SqlParameter("@CompanyHeader", Object.CompanyHeader),
                new SqlParameter("@CompanyFooter", Object.CompanyFooter),
                new SqlParameter("@CompanyDirectorSignature", Object.CompanyDirectorSignature),
                new SqlParameter("@SMTPServer", Object.SMTPServer),
                new SqlParameter("@SMTPUser", Object.SMTPUser),
                new SqlParameter("@SMTPPass", Object.SMTPPass),
                new SqlParameter("@SMTPAccountName", Object.SMTPAccountName),
                new SqlParameter("@EmailHeader", Object.EmailHeader),
                new SqlParameter("@EmailFooter", Object.EmailFooter),
                new SqlParameter("@SalesNominalCode", Object.SalesNominalCode),
                new SqlParameter("@SalesVATCode", Object.SalesVATCode),
                new SqlParameter("@DefaultVATRate", Object.DefaultVATRate),
                new SqlParameter("@LatinCommon", Object.LatinCommon),
                new SqlParameter("@AccountsEmailAddress", Object.AccountsEmailAddress),
                new SqlParameter("@ReceiptNominalCode", Object.ReceiptNominalCode),
                new SqlParameter("@CompanyDirectorSignatureImage", Object.CompanyDirectorSignatureImage),
            };

            DBHelper.ExecuteNonQuery("CompanySave", CommandType.StoredProcedure, Params, DatabaseName);
        }
    }
}
