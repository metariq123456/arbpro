﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DGS.ArbPro.DAL.Reporting.Types;
using System.Data.SqlClient;

namespace DGS.ArbPro.DAL
{
    class DocumentDB
    {
        public static List<Document> GetListAll(String DatabaseName)
        {
            List<Document> aDocs = new List<Document>();
            //depot/Microsec/Jon_Projects/Microsec.Logger.Win/
            SqlParameter[] Params = new SqlParameter[] {
            };

            var Result = DBHelper.ExecuteReader("Document_GetListAll", Params, DatabaseName);

            while (Result.Read())
                aDocs.Add(new Document(Result));
            Result.Close();

            return aDocs;
        }

        public static List<Document> GetListFiltered(String DatabaseName, 
            Int32 ClientID = -1, 
            Int32 DocumentTypeID = -1, 
            Int32 DocumentID = -1, 
            String FileExt = "")
        {
            List<Document> aDocs = new List<Document>();

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@ClientID", ClientID),
                new SqlParameter("@DocumentType", DocumentTypeID),
                new SqlParameter("@DocumentID", DocumentID),
                new SqlParameter("@FileExt", FileExt)
            };
            try
            {
                var Result = DBHelper.ExecuteReader("Document_GetListFiltered", Params, DatabaseName);

                while (Result.Read())
                    aDocs.Add(new Document(Result));
                Result.Close();
            }
            catch (Exception ex)
            {

            }
            return aDocs;
        }

        public static Int32 Save(String DatabaseName, Document Object)
        {
            Int32 ReturnValue = -1;
            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@ID", Object.ID),
                new SqlParameter("@ClientID", Object.ClientID),
                new SqlParameter("@UploadedBy", Object.UploadedBy),
                new SqlParameter("@Description", Object.Description),
                new SqlParameter("@DocumentBytes", Object.DocumentBytes),
                new SqlParameter("@DocumentContentType", Object.DocumentContentType),
                new SqlParameter("@UploadDate", Object.UploadDate),
                new SqlParameter("@FileName", Object.FileName),
                new SqlParameter("@FileExt", Object.FileExt),
                new SqlParameter("@DisplayName", Object.DisplayName),
                new SqlParameter("@DocumentTypeID", Object.DocumentTypeValue)
            };

            var Result = DBHelper.ExecuteReader("Document_Save", Params, DatabaseName);

            while (Result.Read())
                ReturnValue = (Int32)Result["NewID"];
            Result.Close();

            return ReturnValue;
        }
    }

    class DocumentClientDB
    {
        public static List<DocumentClient> GetListAll(String DatabaseName)
        {
            List<DocumentClient> aDocClients = new List<DocumentClient>();

            SqlParameter[] Params = new SqlParameter[] {
            };

            var Result = DBHelper.ExecuteReader("DocumentClient_GetListAll", Params, DatabaseName);

            while (Result.Read())
                aDocClients.Add(new DocumentClient(Result));

            Result.Close();

            return aDocClients;
        }
    }
}