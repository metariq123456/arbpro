﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;

namespace DGS.ArbPro.DAL
{
    public class ReturnWorkTypeItemDB
    {
        private string _DatabaseName;
        
        public ReturnWorkTypeItemDB(string databaseName)
        {
            _DatabaseName = databaseName;
        }
        
        public List<ReturnWorkTypeItem> ReturnWorkListForClientQuoteGet(int ClientNo, int QuoteNo)
        {
            List<ReturnWorkTypeItem> retVal = new List<ReturnWorkTypeItem>();
            ReturnWorkTypeItem item = null;
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkCompleteProcessReturnDatesGet", new SqlParameter[] { new SqlParameter("@ClientIdNo",ClientNo), new SqlParameter("@QuoteIdNo", QuoteNo) }, _DatabaseName);
                
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    item = new ReturnWorkTypeItem();
                    item.Type = row["Type"] != DBNull.Value ? row["Type"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                    //The stored procedure should not return any values that have a null value fot type
                    if (item.Type != string.Empty)
                    {
                        item.ClientID = row["ClientID"] != DBNull.Value ? (int)row["ClientID"] : -1;
                        item.DefaultLetterQA = row["DefaultLetterQA"] != DBNull.Value ? row["DefaultLetterQA"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                        item.DefaultLetterNA = row["DefaultLetterNA"] != DBNull.Value ? row["DefaultLetterNA"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                        item.DefaultLetterReturn = row["DefaultLetterReturn"] != DBNull.Value ? row["DefaultLetterReturn"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                        item.ReturnCompleted = row["ReturnCompleted"] != DBNull.Value ? (bool)row["ReturnCompleted"] : false;
                        item.ReturnDateID = row["ReturnDateID"] != DBNull.Value ? (decimal)row["ReturnDateID"] : -1;
                        item.UserCR = row["UserCR"] != DBNull.Value ? row["UserCR"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                        item.ValueQuoteNo = row["ValueQuoteNo"] != DBNull.Value ? (int)row["ValueQuoteNo"] : -1;
                        item.WorkTypeID = row["WorkTypeID"] != DBNull.Value ? (decimal)row["WorkTypeID"] : -1;
                        item.RetReqd = row["RetReqd"] != DBNull.Value ? row["RetReqd"].ToString().Replace("\n", " ").Replace("  ", " ") : string.Empty;
                        item.ReturnDate = row["ReturnDate"] != DBNull.Value ? row["ReturnDate"].ToString() : string.Empty;
                        retVal.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            
            return retVal;
        }
    }
}