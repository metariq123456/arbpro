﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class Document
    {
        public Int32 m_ID = -1;
        public Int32 m_ClientID = -1;
        public string m_ClientName = "";
        public string m_UploadedBy = "";
        public string m_Description = "";
        public Byte[] m_DocumentBytes = new Byte[0];
        public string m_DocumentContentType = "";
        public DateTime m_UploadDate = DateTime.MinValue;
        public string m_FileName = "";
        public string m_FileExt = "";
        public string m_DisplayName = "";
        public DocumentType m_DocumentTypeValue = 0;

        #region Constructor
        public Document()
        {
        }

        public Document(SqlDataReader Row)
        {
            ID = Row.IntegerValue("ID");
            ClientID = Row.IntegerValue("ClientID");
            ClientName = Row.StringValue("ClientName");
            UploadedBy = Row.StringValue("UploadedBy");
            Description = Row.StringValue("Description");
            DocumentBytes = Row.ByteValue("DocumentBytes");
            DocumentContentType = Row.StringValue("DocumentContentType");
            UploadDate = Row.DateValue("UploadDate");
            FileName = Row.StringValue("FileName");
            FileExt = Row.StringValue("FileExt");
            DisplayName = Row.StringValue("DisplayName");
            DocumentTypeValue = (DocumentType)Row.IntegerValue("DocumentType");
        }

        public Document(Int32 ClientID, String UploadedBy, String Description, Byte[] Bytes, String DocumentContentType, DateTime UploadDate, String FileName, String FileExtension, String DisplayName, DocumentType DocumentType)
        {
            this.ID = -1;
            this.ClientID = ClientID;
            this.ClientName = ClientName;
            this.UploadedBy = UploadedBy;
            this.Description = Description;
            this.DocumentBytes = Bytes;
            this.DocumentContentType = DocumentContentType;
            this.UploadDate = UploadDate;
            this.FileName = FileName;
            this.FileExt = FileExtension;
            this.DisplayName = DisplayName;
            this.DocumentTypeValue = DocumentType;
        }
        #endregion

        #region Properties

        public Int32 ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public Int32 ClientID
        {
            get { return m_ClientID; }
            set { m_ClientID = value; }
        }

        public string UploadedBy
        {
            get { return m_UploadedBy; }
            set { m_UploadedBy = value; }
        }

        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }

        public Byte[] DocumentBytes
        {
            get { return m_DocumentBytes; }
            set { m_DocumentBytes = value; }
        }

        public string DocumentContentType
        {
            get { return m_DocumentContentType; }
            set { m_DocumentContentType = value; }
        }

        public DateTime UploadDate
        {
            get { return m_UploadDate; }
            set { m_UploadDate = value; }
        }

        public string FileName
        {
            get { return m_FileName; }
            set { m_FileName = value; }
        }

        public string FileExt
        {
            get { return m_FileExt; }
            set { m_FileExt = value; }
        }

        public string DisplayName
        {
            get { return m_DisplayName; }
            set { m_DisplayName = value; }
        }
               
        public DocumentType DocumentTypeValue
        {
            get { return m_DocumentTypeValue; }
            set { m_DocumentTypeValue = value; }
        }

        public string ClientName
        {
            get { return m_ClientName; }
            set { m_ClientName = value; }
        }

        public string DocumentTypeName
        {
            get
            {
                return m_DocumentTypeValue.ToString();
            }
        }

        public string DescriptionAbbr
        {
            get
            {
                string sRtn = "";

                //Abbreviate the Description if it is more than 60 characters long

                if (m_Description.Length <= 60)
                {
                    sRtn = m_Description;
                }
                else
                {
                    if (m_Description.Substring(60, 1) == " ")
                    {
                        sRtn = m_Description.Substring(0, 60);
                    }
                    else
                    {
                        sRtn = m_Description.Substring(0, 60);
                        sRtn = sRtn.Substring(0, sRtn.LastIndexOf(" "));
                    }

                    sRtn += "...";

                }
                return sRtn;
            }
        }       


        #endregion

        #region Enums
        public enum DocumentType
        {
            Global,
            Global_NonEmail,
            Client,
            Client_NonEmail,
            Temporary
        }

        #endregion

        #region Public Methods
        public void Save(String DBName)
        {
            m_ID = DocumentDB.Save(DBName, this);
        }
        #endregion

        #region Public Static Methods

        public static List<Document> GetListAll(String DatabaseName)
        {
            // Call from Core will need to pass:
            // DGS.ArbPro.Core.Common.ASPSession.GetLoggedInUserClientDatabaseName()
            return DocumentDB.GetListAll(DatabaseName);
        }

        public static List<Document> GetListForClient(String DatabaseName, Int32 ClientID, Int32 DocumentType = -1, String Extension = "")
        {
            return DocumentDB.GetListFiltered(DatabaseName, ClientID, DocumentType, -1, Extension);
        }

        public static Document GetForID(String DatabaseName, Int32 DocumentID)
        {
            var Result = DocumentDB.GetListFiltered(DatabaseName, -1, -1, DocumentID);
            if (Result.Count > 0)
                return Result[0];
            else
                return null;
        }

        #endregion

    }

    public class DocumentClient
    {
        public Int32 m_ID = -1;
        public string m_Name = "";

        #region Properties

        public Int32 ID
        {
            get { return m_ID; }
            set { m_ID = value; }
        }

        public string Name
        {
            get { return m_Name; }
            set { m_Name = value; }
        }

        #endregion

        public DocumentClient(SqlDataReader Row)
        {
            ID = Row.IntegerValue("ID");
            Name = Row.StringValue("Name");
        }

        #region Public Static Methods

        public static List<DocumentClient> GetListAll(String DatabaseName)
        {
            return DocumentClientDB.GetListAll(DatabaseName);
        }

        #endregion
    }
}
