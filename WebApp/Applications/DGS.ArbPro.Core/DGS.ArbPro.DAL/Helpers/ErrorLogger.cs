﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DGS.ArbPro.DAL
{
    public class ErrorLogger : Contracts.DBContextBase
    {
        public ErrorLogger(string databaseName) : base(databaseName,"ErrorLogger") { }

        //protected static async Task LogErrorAsync(string databaseName, string method, string parmsWithValues, Exception ex)
        //{
        //    await Task.Run(() =>
        //        {
        //            ErrorLogger dbLogger = new ErrorLogger(databaseName);
        //            dbLogger.LogError(method, parmsWithValues, ex);
        //        });
        //}

        //public static async void LogToDb(string databaseName, string method, string parmsWithValues, Exception ex)
        //{
        //    await LogErrorAsync(databaseName, method, parmsWithValues, ex).ConfigureAwait(false);
        //}

        /// <summary>
        /// Generic static function to enable logging to a specified database
        /// </summary>
        /// <param name="databaseName">The database to be used</param>
        /// <param name="method">The method the error was raised in</param>
        /// <param name="parmsWithValues">The paramaters sent to the method when error was raised</param>
        /// <param name="ex">The raised exception</param>
        public static void LogToDb(string databaseName, string method, string parmsWithValues, Exception ex)
        {
            ErrorLogger dbLogger = new ErrorLogger(databaseName);
            dbLogger.LogError(method, parmsWithValues, ex);
        }

        /// <summary>
        /// Generic static function to enable logging to a specified database
        /// </summary>
        /// <param name="databaseName">The database to be used</param>
        /// <param name="className">The name of the class that raised the exception</param>
        /// <param name="method">The method the error was raised in</param>
        /// <param name="parmsWithValues">The paramaters sent to the method when error was raised</param>
        /// <param name="ex">The raised exception</param>
        public static void LogToDb(string databaseName, string className, string method, string parmsWithValues, Exception ex)
        {
            ErrorLogger dbLogger = new ErrorLogger(databaseName);
            dbLogger.LogError(className, method, parmsWithValues, ex);
        }

        /// <summary>
        /// Generic function to enable logging of an objects state to the database
        /// This function relies on the overload of the ToString function to retreave the properties and values of the object
        /// paramater
        /// </summary>
        /// <param name="databaseName">The database to be used</param>
        /// <param name="method">The method where the error was raised</param>
        /// <param name="parmsWithValues">Object: the Types namespace object</param>
        /// <param name="ex">The raised exception</param>
        public static void LogToDb(string databaseName, string method, object parmsWithValues, Exception ex)
        {
            ErrorLogger dbLogger = new ErrorLogger(databaseName);
            dbLogger.LogError(method, parmsWithValues.ToString(), ex);
        }
    }
}
