﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL.Reporting.Types
{
    public class ReportInvoiceLetter
    {
        public Int32 ClientID;
        public String CustomerName;
        public String CustomerAddressHouseNameOrNumber;
        public String CustomerAddress1;
        public String CustomerAddress2;
        public String CustomerAddressTown;
        public String CustomerAddressCounty;
        public String CustomerAddressPostCode;
        public String ContactTitle;
        public String ContactFirstName;
        public String ContactSurname;
        public Int32 QuoteNo;
        public String OrderNo;
        public DateTime InvoiceDateSent;
        public Int32 InvoiceNo;
        public String SiteRefNo;
        public String SiteAddress;
        public String LetterText;
        public Byte[] LetterSignature;
        public String DirectorName;
        public Decimal InvoiceValue;
        public Decimal InvoiceVatRate;
        public Decimal InvoiceVatValue;
        public Decimal InvoiceTotalIncVat;
        public String PaymentDetails;
        public Boolean IsCommercial;

        public ReportInvoiceLetter(SqlDataReader Row)
        {
            ClientID = Row.IntegerValue("ClientID");
            CustomerName = Row.StringValue("CustomerName");
            CustomerAddressHouseNameOrNumber = Row.StringValue("CustomerHouseNameOrNumber");
            CustomerAddress1 = Row.StringValue("CustomerAddress1");
            CustomerAddress2 = Row.StringValue("CustomerAddress2");
            CustomerAddressTown = Row.StringValue("CustomerAddressTown");
            CustomerAddressCounty = Row.StringValue("CustomerAddressCounty");
            CustomerAddressPostCode = Row.StringValue("CustomerAddressPostCode");
            ContactTitle = Row.StringValue("ContactTitle");
            ContactFirstName = Row.StringValue("ContactForeName");
            ContactSurname = Row.StringValue("ContactSurname");
            QuoteNo = Row.IntegerValue("QuoteNo");
            OrderNo = Row.StringValue("OrderNo");
            InvoiceDateSent = Row.DateValue("InvoiceDateSent");
            InvoiceNo = Row.IntegerValue("InvoiceNo");
            SiteRefNo = Row.StringValue("SiteRefNo");
            SiteAddress = Row.StringValue("SiteAddress");
            LetterText = Row.StringValue("LetterText");
            LetterSignature = Row.ByteValue("LetterSignature");
            DirectorName = Row.StringValue("CompanyDirectorName");
            InvoiceValue = Row.DecimalValue("InvoiceValue");
            InvoiceVatRate = Row.DecimalValue("InvoiceVatRate");
            InvoiceVatValue = Row.DecimalValue("InvoiceVatValue");
            InvoiceTotalIncVat = Row.DecimalValue("InvoiceTotalIncVat");
            PaymentDetails = Row.StringValue("PaymentDetailsText");
            IsCommercial = Row.BooleanValue("IsCommercial");
        }
    }

    public class ReportInvoiceSchedule
    {
        public Int32 ClientID;
        public String CustomerName;
        public String CustomerAddressHouseNameOrNumber;
        public String CustomerAddress1;
        public String CustomerAddress2;
        public String CustomerAddressTown;
        public String CustomerAddressCounty;
        public String CustomerAddressPostCode;
        public String ContactTitle;
        public String ContactFirstName;
        public String ContactSurname;
        public Int32 QuoteNo;
        public Int32 InvoiceNo;
        public String OrderNo;
        public DateTime InvoiceDateSent;
        public String SiteRefNo;
        public String SiteAddress;
        public List<ReportInvoiceItem> InvoiceItems;
        public Decimal QuoteTotal;
        public Decimal QuoteVatRate;
        public Decimal QuoteVatAmount;
        public Decimal QuoteTotalIncVat;
        public Boolean IsCommercial;

        public ReportInvoiceSchedule(SqlDataReader Row)
        {
            InvoiceItems = new List<ReportInvoiceItem>();

            while (Row.Read())
            {
                ClientID = Row.IntegerValue("ClientID");
                CustomerName = Row.StringValue("CustomerName");
                CustomerAddressHouseNameOrNumber = Row.StringValue("CustomerHouseNameOrNumber");
                CustomerAddress1 = Row.StringValue("CustomerAddress1");
                CustomerAddress2 = Row.StringValue("CustomerAddress2");
                CustomerAddressTown = Row.StringValue("CustomerAddressTown");
                CustomerAddressCounty = Row.StringValue("CustomerAddressCounty");
                CustomerAddressPostCode = Row.StringValue("CustomerAddressPostCode");
                ContactTitle = Row.StringValue("ContactTitle");
                ContactFirstName = Row.StringValue("ContactForeName");
                ContactSurname = Row.StringValue("ContactSurname");
                QuoteNo = Row.IntegerValue("QuoteNo");
                InvoiceDateSent = Row.DateValue("InvoiceDateSent");
                InvoiceNo = Row.IntegerValue("InvoiceNo");
                OrderNo = Row.StringValue("OrderNo");
                SiteRefNo = Row.StringValue("SiteRefNo");
                SiteAddress = Row.StringValue("SiteAddress");
                InvoiceItems.Add(new ReportInvoiceItem(Row));
                QuoteTotal = Row.DecimalValue("QuoteTotalValue");
                QuoteVatRate = Row.DecimalValue("QuoteVatRate");
                QuoteVatAmount = Row.DecimalValue("QuoteVatAmount");
                QuoteTotalIncVat = Row.DecimalValue("QuoteTotal");
                IsCommercial = Row.BooleanValue("IsCommercial");
            }

            Row.Close();
        }
    }

    public class ReportInvoiceItem
    {
        public String TreeNo;
        public String TreeName;
        public String Description;
        public Decimal Value;

        public ReportInvoiceItem(SqlDataReader Row)
        {
            TreeNo = Row.StringValue("QuoteItemNo");
            TreeName = Row.StringValue("QuoteItemName");
            Description = Row.StringValue("QuoteItemDescription");
            Value = Row.DecimalValue("QuoteItemValue");
        }
    }
}
