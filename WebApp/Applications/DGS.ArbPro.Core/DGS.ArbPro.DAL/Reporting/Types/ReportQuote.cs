﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class ReportQuoteCoveringLetter
    {
        public Int32 ClientID;
        public String CustomerName;
        public String CustomerAddressHouseNameOrNumber;
        public String CustomerAddress1;
        public String CustomerAddress2;
        public String CustomerAddressTown;
        public String CustomerAddressCounty;
        public String CustomerAddressPostCode;
        public String ContactTitle;
        public String ContactFirstName;
        public String ContactSurname;
        public Int32 QuoteNo;
        public DateTime QuoteDateSent;
        public String QuoteSurveyor;
        public String SiteRefNo;
        public String SiteAddress;
        public String LetterText;
        public Byte[] LetterSignature;
        public String DirectorName;
        public Boolean IsCommercial;

        public ReportQuoteCoveringLetter(Int32 ID, SqlDataReader Row)
        {
            ClientID = Row.IntegerValue("ClientID");
            CustomerName = Row.StringValue("CustomerName");
            CustomerAddressHouseNameOrNumber = Row.StringValue("CustomerHouseNameOrNumber");
            CustomerAddress1 = Row.StringValue("CustomerAddress1");
            CustomerAddress2 = Row.StringValue("CustomerAddress2");
            CustomerAddressTown = Row.StringValue("CustomerAddressTown");
            CustomerAddressCounty = Row.StringValue("CustomerAddressCounty");
            CustomerAddressPostCode = Row.StringValue("CustomerAddressPostCode");
            ContactTitle = Row.StringValue("ContactTitle");
            ContactFirstName = Row.StringValue("ContactForeName");
            ContactSurname = Row.StringValue("ContactSurname");
            QuoteNo = Row.IntegerValue("QuoteNo");
            QuoteDateSent = Row.DateValue("QuoteDateSent");
            QuoteSurveyor = Row.StringValue("QuoteSurveyor");
            SiteRefNo = Row.StringValue("SiteRefNo");
            SiteAddress = Row.StringValue("SiteAddress");
            LetterText = Row.StringValue("LetterText");
            LetterSignature = Row.ByteValue("LetterSignature");
            DirectorName = Row.StringValue("CompanyDirectorName");
            IsCommercial = Row.BooleanValue("IsCommercial");
        }
    }

    public class ReportQuoteSchedule
    {
        public Int32 ClientID;
        public String CustomerName;
        public String CustomerAddressHouseNameOrNumber;
        public String CustomerAddress1;
        public String CustomerAddress2;
        public String CustomerAddressTown;
        public String CustomerAddressCounty;
        public String CustomerAddressPostCode;
        public String ContactTitle;
        public String ContactFirstName;
        public String ContactSurname;
        public Int32 QuoteNo;
        public DateTime QuoteDateSent;
        public String QuoteSurveyor;
        public String SiteRefNo;
        public String SiteAddress;
        public List<ReportQuoteItem> QuoteItems;
        public Decimal QuoteTotal;
        public Decimal QuoteVatRate;
        public Decimal QuoteVatAmount;
        public Decimal QuoteTotalIncVat;
        public Boolean IsCommercial;
        public Boolean ShowValues;

        public ReportQuoteSchedule(SqlDataReader Row)
        {
            QuoteItems = new List<ReportQuoteItem>();

            while (Row.Read())
            {
                ClientID = Row.IntegerValue("ClientID");
                CustomerName = Row.StringValue("CustomerName");
                CustomerAddressHouseNameOrNumber = Row.StringValue("CustomerHouseNameOrNumber");
                CustomerAddress1 = Row.StringValue("CustomerAddress1");
                CustomerAddress2 = Row.StringValue("CustomerAddress2");
                CustomerAddressTown = Row.StringValue("CustomerAddressTown");
                CustomerAddressCounty = Row.StringValue("CustomerAddressCounty");
                CustomerAddressPostCode = Row.StringValue("CustomerAddressPostCode");
                ContactTitle = Row.StringValue("ContactTitle");
                ContactFirstName = Row.StringValue("ContactForeName");
                ContactSurname = Row.StringValue("ContactSurname");
                QuoteNo = Row.IntegerValue("QuoteNo");
                QuoteDateSent = Row.DateValue("QuoteDateSent");
                QuoteSurveyor = Row.StringValue("QuoteSurveyor");
                SiteRefNo = Row.StringValue("SiteRefNo");
                SiteAddress = Row.StringValue("SiteAddress");
                QuoteItems.Add(new ReportQuoteItem(Row));
                QuoteTotal = Row.DecimalValue("QuoteTotalValue");
                QuoteVatRate = Row.DecimalValue("QuoteVatRate");
                QuoteVatAmount = Row.DecimalValue("QuoteVatAmount");
                QuoteTotalIncVat = Row.DecimalValue("QuoteTotal");
                IsCommercial = Row.BooleanValue("IsCommercial");
                ShowValues = Row.BooleanValue("IncludeValues");                
            }

            Row.Close();
        }
    }

    public class ReportQuoteItem
    {
        public String TreeNo;
        public String TreeName;
        public String Description;
        public Decimal Value;

        public ReportQuoteItem(SqlDataReader Row)
        {
            TreeNo = Row.StringValue("QuoteItemNo");
            TreeName = Row.StringValue("QuoteItemName");
            Description = Row.StringValue("QuoteItemDescription");
            Value = Row.DecimalValue("QuoteItemValue");
        }
    }
}
