﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using DGS.ArbPro.DAL.Reporting.Types;

namespace DGS.ArbPro.DAL.Reporting
{
    public class ScheduledWorkReportDB
    {
        public static ScheduledWorkReport GetScheduledWorkReportForQuote(string databaseName, int QuoteId)
        {
            DataSet ds;
            ScheduledWorkReport returnValue = new ScheduledWorkReport();

            ds = DBHelper.ExecuteStoredProcDataSet("GetScheduledWorkReportForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", QuoteId) }, databaseName);

            QuoteDetailsDB.PopulateReportQuoteDetails(returnValue, ds);

            foreach (DataRow Row in ds.Tables[0].Rows)
            {

            }
            return returnValue;
        }
    }
}
