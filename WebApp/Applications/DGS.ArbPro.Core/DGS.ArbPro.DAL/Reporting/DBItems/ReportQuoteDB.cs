﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DGS.ArbPro.DAL.Reporting.Types;

namespace DGS.ArbPro.DAL.Reporting.DBItems
{
    public class ReportQuoteDB
    {
        public String DatabaseName;
        public Int32 QuoteNo;

        public ReportQuoteDB(String DatabaseName, Int32 QuoteNo)
        {
            this.DatabaseName = DatabaseName;
            this.QuoteNo = QuoteNo;
        }

        public ReportQuoteCoveringLetter CoveringLetter()
        {
            ReportQuoteCoveringLetter ReturnValue = null;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@QuoteNo", QuoteNo)
            };

            var Result = DBHelper.ExecuteReader(
                "ReportQuoteCoveringLetter",
                Params,
                DatabaseName);

            while (Result.Read())
                ReturnValue = new ReportQuoteCoveringLetter(0, Result);
            Result.Close();

            return ReturnValue;
        }

        public ReportQuoteSchedule Schedule()
        {
            ReportQuoteSchedule ReturnValue = null;

            SqlParameter[] Params = new[] {
                new SqlParameter("@QuoteNo", QuoteNo)
            };

            var Result = DBHelper.ExecuteReader(
                "ReportQuoteSchedule",
                Params,
                DatabaseName);

            ReturnValue = new ReportQuoteSchedule(Result);

            return ReturnValue;
        }
    }
}
