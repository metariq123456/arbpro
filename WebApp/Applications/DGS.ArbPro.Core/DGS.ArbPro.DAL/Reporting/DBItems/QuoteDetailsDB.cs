﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using DGS.ArbPro.DAL.Reporting.Types;

namespace DGS.ArbPro.DAL.Reporting
{
    public class QuoteDetailsDB
    {
        /// <summary>
        /// Database access method to get quote details for a given quote number
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="QuoteId"></param>
        /// <returns></returns>
        public static QuoteDetails GetReportQuoteDetails(string databaseName, int QuoteId)
        {
            DataSet ds;
            QuoteDetails returnValue = new QuoteDetails();

            ds = DBHelper.ExecuteStoredProcDataSet("ReportQuoteDetails", new SqlParameter[] { new SqlParameter("@QuoteId", QuoteId) }, databaseName);

            //Populate the data objects
            PopulateReportQuoteDetails(returnValue, ds);

            return returnValue;
        }

        /// <summary>
        /// Static method to enable derived classes to populate values based on a join within a stored procedure
        /// </summary>
        /// <param name="reportDataItem">The Data item that ingerits from QuoteDetails</param>
        /// <param name="ds">The Dataset for the stored procedure</param>
        public static void PopulateReportQuoteDetails(QuoteDetails reportDataItem, DataSet ds)
        {
            foreach (DataRow Row in ds.Tables[0].Rows)
            {
                reportDataItem.QuoteId = int.Parse(Row["QuioteNo"].ToString());
                reportDataItem.ClientId = int.Parse(Row["ClientId"].ToString());
                reportDataItem.SiteRef = int.Parse(Row["SiteRef"].ToString());

                reportDataItem.BusinessName = Row["ClientBusinessName"].ToString();
                reportDataItem.SiteAddress = Row["SiteAddress"].ToString();
                reportDataItem.SiteContact = Row["SiteContact"].ToString();
                reportDataItem.SitePostCode = Row["SitePostCode"].ToString();
                reportDataItem.Surveyor = Row["StaffSurveyorName"].ToString();
                reportDataItem.ClientAddressLn1 = Row["ClientAddress1"].ToString();
                reportDataItem.ClientAddressLn2 = Row["ClientAddress2"].ToString();
                reportDataItem.ClientCounty = Row["ClientAddressCounty"].ToString();
                reportDataItem.ClientFirstName = Row["ClientForename"].ToString();
                reportDataItem.ClientHouseNameNo = Row["HouseNum"].ToString();
                reportDataItem.ClientPostCode = Row["ClientAddressPostCode"].ToString();
                reportDataItem.ClientSurname = Row["ClientSurname"].ToString();
                reportDataItem.ClientTitle = Row["ClientTitle"].ToString();
                reportDataItem.ClientTown = Row["ClientAddressTown"].ToString();

                reportDataItem.WorkDate = DateTime.Parse(Row["SiteRef"].ToString());
                reportDataItem.QuoteSentDate = DateTime.Parse(Row["SiteRef"].ToString());

            }
        }
    }
}
