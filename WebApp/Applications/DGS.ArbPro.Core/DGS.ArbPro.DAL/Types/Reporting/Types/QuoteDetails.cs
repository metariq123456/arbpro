﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class QuoteDetails
    {
        #region Members
        //Client address details
        string m_ClientTitle = "";
        string m_FirstName = "";
        string m_Surname = "";
        string m_BusinessName = "";
        string m_HouseNameNo = "";
        string m_AddressLn1 = "";
        string m_AddressLn2 = "";
        string m_Town = "";
        string m_County = "";
        string m_PostCode = "";
        //Site address details
        string m_SiteAddress = "";
        string m_SitePostCode = "";
        string m_SiteContact = "";

        //Quote details
        string m_Surveyor = "";
        int m_QuoteId = 0;
        int m_ClientId = 0;
        int m_SiteRef = 0;

        DateTime m_QuoteSent = DateTime.MinValue;
        DateTime m_StartDate = DateTime.MinValue;
        #endregion

        #region Properties  
        public int QuoteId
        {
            get { return m_QuoteId; }
            set { m_QuoteId = value; }
        }

        public int ClientId
        {
            get { return m_ClientId; }
            set { m_ClientId = value; }
        }

        public int SiteRef
        {
            get { return m_SiteRef; }
            set { m_SiteRef = value; }
        }

        public string ClientTitle
        {
            get { return m_ClientTitle; }
            set { m_ClientTitle = value; }
        }

        public string ClientFirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }

        public string ClientAddressLn2
        {
            get { return m_AddressLn2; }
            set { m_AddressLn2 = value; }
        }

        public string ClientAddressLn1
        {
            get { return m_AddressLn1; }
            set { m_AddressLn1 = value; }
        }

        public string ClientHouseNameNo
        {
            get { return m_HouseNameNo; }
            set { m_HouseNameNo = value; }
        }

        public string BusinessName
        {
            get { return m_BusinessName; }
            set { m_BusinessName = value; }
        }

        public string ClientSurname
        {
            get { return m_Surname; }
            set { m_Surname = value; }
        }

        public string Surveyor
        {
            get { return m_Surveyor; }
            set { m_Surveyor = value; }
        }

        public string SiteAddress
        {
            get { return m_SiteAddress; }
            set { m_SiteAddress = value; }
        }

        public string SitePostCode
        {
            get { return m_SitePostCode; }
            set { m_SitePostCode = value; }
        }

        public string SiteContact
        {
            get { return m_SiteContact; }
            set { m_SiteContact = value; }
        }

        public string ClientPostCode
        {
            get { return m_PostCode; }
            set { m_PostCode = value; }
        }

        public string ClientCounty
        {
            get { return m_County; }
            set { m_County = value; }
        }

        public string ClientTown
        {
            get { return m_Town; }
            set { m_Town = value; }
        }

        public DateTime QuoteSentDate
        {
            get { return m_QuoteSent; }
            set { m_QuoteSent = value; }
        }

        public DateTime WorkDate
        {
            get { return m_StartDate; }
            set { m_StartDate = value; }
        }
        #endregion


    }
}
