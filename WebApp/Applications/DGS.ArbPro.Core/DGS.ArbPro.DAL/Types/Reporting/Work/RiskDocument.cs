﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class RiskDocument : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public RiskDocument(string databaseName)
            : base(databaseName, "RiskAssessmentDocument", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get
        public Types.RiskDocument GetForRiskId(int riskId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_RiskAssessment_ForRiskId", new SqlParameter[] { new SqlParameter("@RecId", riskId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForRiskId", string.Format("riskId: {0}, savePath: {1}", riskId, savePath), ex);
                Console.WriteLine("Error in RiskDocument.GetForRiskId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }

        public Types.RiskDocument GetForQuoteId(int quoteId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_RiskAssessment_ForQuoteId", new SqlParameter[] { new SqlParameter("@RecId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteId", string.Format("quoteId: {0}, savePath: {1}", quoteId, savePath), ex);
                Console.WriteLine("Error in RiskDocument.GetForQuoteId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }

        public Types.RiskDocument GetForWorkId(int workId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_RiskAssessment_ForWorkId", new SqlParameter[] { new SqlParameter("@RecId", workId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkId", string.Format("workId: {0}, savePath: {1}", workId, savePath), ex);
                Console.WriteLine("Error in RiskDocument.GetForWorkId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }

        public Types.RiskDocument GetForClientSiteId(int clientSiteId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_RiskAssessment_ForClientSiteId", new SqlParameter[] { new SqlParameter("@RecId", clientSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientSiteId", string.Format("clientSiteId: {0}, savePath: {1}", clientSiteId, savePath), ex);
                Console.WriteLine("Error in RiskDocument.GetForClientSiteId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }

        public Types.RiskDocument GetForJobSiteId(int jobSiteId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_RiskAssessment_ForJobSiteId", new SqlParameter[] { new SqlParameter("@RecId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJobSiteId", string.Format("jobSiteId: {0}, savePath: {1}", jobSiteId, savePath), ex);
                Console.WriteLine("Error in RiskDocument.GetForJobSiteId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }


        public Types.RiskDocument GetForWorkPlanId(int workPlanId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_RiskAssessment_ForWorkPlanId", new SqlParameter[] { new SqlParameter("@RecId", workPlanId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkPlanId", string.Format("workPlanId: {0}, savePath: {1}", workPlanId, savePath), ex);
                Console.WriteLine("Error in RiskDocument.GetForWorkPlanId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }

        public Types.RiskDocument GetForJobSiteAndQuoteId(int jobSiteId, int quoteId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_RiskAssessment_ForJobSiteAndQuoteId", (new SqlParameter[] { new SqlParameter("@RecId", jobSiteId), new SqlParameter("@QuoteId", quoteId) }), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJobSiteAndQuoteId", string.Format("jobSiteId: {0} quoteId: {1} savePath: {2}", jobSiteId, quoteId, savePath), ex);
                Console.WriteLine(string.Format("Error in RiskDocument.GetForJobSiteAndQuoteId: {0}", ex.ToString()));
            }

            return ToObject(ds,savePath); 

        }
        #endregion
        #endregion
        #region Private
        private Types.RiskDocument ToObject(DataSet ds, string savePath)
        {
            Types.RiskDocument retVal = new Types.RiskDocument(savePath);

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0], savePath);

                if (retVal.RecordId > 0)
                {
                    //The record has been successfully created, now populate the sections and the additional risk items.
                    RiskSection sectionContext = new RiskSection(this.DatabaseName);
                    retVal.AssessmentSections = sectionContext.GetForRiskAssessment(retVal.RecordId);

                    RiskAdditionalItem additionalItemContext = new RiskAdditionalItem(this.DatabaseName);
                    retVal.AdditionalItems = additionalItemContext.GetForRiskAssessment(retVal.RecordId);

                }

            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("ds: {0}, savePath: {1}", ds.ToString(), savePath), ex);
                Console.WriteLine("Error in RiskDocument.ToObject: {0}", ex.ToString());
            }

            return retVal;
        }

        private Types.RiskDocument CreateInstance(DataRow row, string savePath)
        {
            Types.RiskDocument obj = new Types.RiskDocument(savePath);
            obj.RecordId = (int)row["Id"];
            obj.ReferenceNo = (string)row["QuoteRef"];
            obj.ClientID = (int)row["ClientId"];
            //Assessment Dates
            obj.DateOfAssessment = base.DateTimeValue(row, "DateOfAssessment");
            obj.WorkDate = base.DateTimeValue(row, "WorkDate");
            obj.SalesAppointmentDate = base.DateTimeValue(row, "SalesAppointmentDate");
            obj.WorkAppoinmentDate = base.DateTimeValue(row, "WorkAppoinmentDate");
            //Risk assessment details
            obj.CS38Certifed = (bool)row["CS38Certifed"];
            obj.TeamLeader = (string)row["TeamLeader"]; //Foreman on document
            obj.Assessor = (string)row["Assessor"];
            obj.NearestHospital = (string)row["NearestHospital"];
            obj.ReferenceDoc = (string)row["ReferenceDoc"];
            obj.MobileSignal = (string)row["MobileSignal"];
            obj.SiteAccess = (string)row["SiteAccess"];
            obj.GridReference = (string)row["GridReference"];
            obj.AssessorSignatureImage = ByteValue(row, "AssessorSignatureImage");
            obj.RescuePlan = (string)row["RescuePlan"];
            obj.RescueNomaniee = (string)row["RescueNomaniee"];
            //Site Componenets
            obj.SiteRef = (string)row["SiteRef"];
            obj.SiteStreet = (string)row["StreetNumber"];
            obj.SiteAdd1 = (string)row["AddressLn1"];
            obj.SiteAdd2 = (string)row["AddressLn2"];
            obj.SiteTown = (string)row["Town"];
            obj.SiteCounty = (string)row["County"];
            obj.SitePostCode = (string)row["PostCode"];
            return obj;
        }
        #endregion
        #endregion
    }
}
