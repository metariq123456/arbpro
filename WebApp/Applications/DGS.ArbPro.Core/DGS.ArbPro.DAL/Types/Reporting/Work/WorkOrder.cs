﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class WorkOrder: Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public WorkOrder(string databaseName)
            : base(databaseName, "WorkOrder", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get
        public Types.WorkOrder GetForQuoteId(int quoteId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_WorkOrder_ForQuoteId", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteId", string.Format("quoteId: {0}, savePath: {1}", quoteId, savePath), ex);
                Console.WriteLine("Error in WorkOrder.GetForQuoteId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }

        public Types.WorkOrder GetForAppoinmentId(int appointmentId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_WorkOrder_ForAppointmentId", new SqlParameter[] { new SqlParameter("@AppointmentId", appointmentId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForAppoinmentId", string.Format("appointmentId: {0}, savePath: {1}", appointmentId, savePath), ex);
                Console.WriteLine("Error in WorkOrder.GetForAppoinmentId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }

        public Types.WorkOrder GetForWorkId(int workId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_WorkOrder_ForWorkId", new SqlParameter[] { new SqlParameter("@WorkId", workId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkId", string.Format("workId: {0}, savePath: {1}", workId, savePath), ex);
                Console.WriteLine("Error in WorkOrder.GetForWorkId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }

        public Types.WorkOrder GetForClientSiteId(int clientSiteId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_WorkOrder_ForClientSiteId", new SqlParameter[] { new SqlParameter("@ClientSiteId", clientSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientSiteId", string.Format("clientSiteId: {0}, savePath: {1}", clientSiteId, savePath), ex);
                Console.WriteLine("Error in WorkOrder.GetForClientSiteId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }

        public Types.WorkOrder GetForJobSiteId(int jobSiteId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_WorkOrder_ForJobSiteId", new SqlParameter[] { new SqlParameter("@JobSiteId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJobSiteId", string.Format("jobSiteId: {0}, savePath: {1}", jobSiteId, savePath), ex);
                Console.WriteLine("Error in WorkOrder.GetForJobSiteId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }

        public Types.WorkOrder GetForWorkPlanId(int workPlanId, string savePath)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_WorkOrder_ForWorkPlanId", new SqlParameter[] { new SqlParameter("@WorkPlanId", workPlanId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkPlanId", string.Format("workPlanId: {0}, savePath: {1}", workPlanId, savePath), ex);
                Console.WriteLine("Error in WorkOrder.GetForWorkPlanId: {0}", ex.ToString());
            }
            return ToObject(ds, savePath);
        }
        #endregion
        #endregion
        #region Private
        private Types.WorkOrder ToObject(DataSet ds, string savePath)
        {
            Types.WorkOrder retVal = new Types.WorkOrder(savePath);

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0], savePath);

                if (retVal.WorkPlanId > 0)
                {
                    //Populate the child objects for the work order
                    DAL.TreeProtectionOrder treeContext = new TreeProtectionOrder(this.DatabaseName);
                    DAL.Team staffContext = new Team(this.DatabaseName);
                    DAL.WorkPlan scheduleContext = new WorkPlan(this.DatabaseName);
                    DAL.WorkItem itemContext = new WorkItem(this.DatabaseName);
                    DAL.ItemPicture pictureContext = new ItemPicture(this.DatabaseName);
                    DAL.Note notes = new Note(this.DatabaseName);
                    Types.WorkPlan wp = scheduleContext.GetForId(retVal.WorkPlanId, true);
                    if (wp.Appointment.CalendarWorkDate.Count > 0)
                    {
                        retVal.StartDate = wp.Appointment.CalendarWorkDate.Min(item=>item.BookDate);
                        retVal.EndDate = wp.Appointment.CalendarWorkDate.Max(item => item.BookDate);
                    }
                    retVal.StartTime = wp.Appointment.StartTimeDay1New;
                    retVal.TPOForWork = treeContext.GetForQuote(retVal.RecordId);
                    retVal.Team = staffContext.GetForWorkPlan(retVal.WorkPlanId);
                    retVal.Comments = notes.GetForParent(retVal.QuoteId, Types.Enums.NoteTypeEnum.Quote);
                    retVal.Comments.AddRange(notes.GetForParent(wp.WorkId, Types.Enums.NoteTypeEnum.Work));
                    //retVal.Comments.Add( notes.GetForParent(retVal.QuoteId, Types.Enums.NoteTypeEnum.Work);

                    retVal.Team.Personnel = wp.Personnel;
                    if (retVal.StaffRequired <= 0)
                    {
                        retVal.StaffRequired = retVal.Team.Personnel.Count();
                    }

                    retVal.Tools = wp.Tools;
                    retVal.WorkItems = itemContext.GetForWorkPlan(wp.Id);
                    List<Types.WorkItem> sortedItems = retVal.WorkItems.OrderBy(item => item.JobItemId).ToList();
                    sortedItems = retVal.WorkItems.OrderBy(item => item.OrderId).ToList();
                    retVal.WorkItems = sortedItems;
                    Types.ItemPicture itmPicture = new Types.ItemPicture();

                    foreach (Types.WorkItem itm in retVal.WorkItems)
                    {
                        var piclist = pictureContext.GetForJobItem(itm.JobItemId);
                        if (piclist.Count > 0)
                        {
                            itmPicture = piclist[0];
                            if (retVal.ItemPictures.ContainsKey(itm.Id))
                            {
                                retVal.ItemPictures[itm.Id] = itmPicture;
                            }
                            else
                            {
                                retVal.ItemPictures.Add(itm.Id, itmPicture);
                            }
                        }
                        //retVal.WorkPicture = pictureContext.GetForJobItem(
                    }
                }

            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("ds: {0}, savePath: {1}", ds.ToString(), savePath), ex);
                Console.WriteLine("Error in WorkOrder.ToObject: {0}", ex.ToString());
            }

            return retVal;
        }

        private Types.WorkOrder CreateInstance(DataRow row, string savePath)
        {
            Types.WorkOrder obj = new Types.WorkOrder(savePath);
            obj.SurveyorName = (string)row["SurveyorName"];
            obj.RecordId = (int)row["WorkId"];
            obj.WorkPlanId = (int)row["WorkPlanId"];
            obj.QuoteId = (int)row["QuoteId"];
            obj.ReferenceNo = (string)row["QuoteRef"];
            obj.PONumber = " "; //TODO: Update Sp to enable (string)row["PONumber"];
            obj.StartDate = base.DateTimeValue(row,"StartDate");
            obj.EndDate = base.DateTimeValue(row,"EndDate");
            obj.StaffRequired = (int)row["StaffRequired"];
            obj.TimeRequired = DecimaleValue(row, "TimeRequired");
            //Client Details
            obj.ClientID = (int)row["ClientId"];
            obj.AccountNo = (string)row["AccountNum"];
            obj.ClientName = (string)row["CustomerName"];
            obj.ClientStreetNo = (string)row["StreetNumber"];
			obj.ClientAddress1 = (string)row["Address1"];
            obj.ClientAddress2 = (string)row["Address2"];
            obj.ClientTown = (string)row["Town"];
            obj.ClientCounty = (string)row["County"];
            obj.ClientPostCode = (string)row["PostCode"];
            obj.BusinessName = (string)row["BusinessName"];
            //Site Componenets
            obj.SiteRef = (string)row["SiteRef"];
            obj.SiteStreet = (string)row["SiteStreetNumber"];
            obj.SiteAdd1 = (string)row["SiteAddress1"];
            obj.SiteAdd2 = (string)row["SiteAddress2"];
            obj.SiteTown = (string)row["SiteTown"];
            obj.SiteCounty = (string)row["SiteCounty"];
            obj.SitePostCode = (string)row["SitePostCode"];
            //Contact details
            obj.ContactTitle = (string)row["ContactTitle"];
            obj.ContactFirstName = (string)row["ContactFirstName"];
            obj.ContactSurname = (string)row["ContactSurname"];
            obj.ContactMobile = (string)row["ContactMobile"];
            obj.ContactTelephone = (string)row["ContactTelephone"];
            //TPO
            obj.TpoStatusEnum = (Types.Enums.TreeProtectionStatusEnum)(IntValue(row, "TpoStatus"));
            obj.ApplicationProgessEnum = (Types.Enums.TreeProtectionProgreesEnum)(IntValue(row, "ApplicationProgress"));
            obj.ConservationArea = BoolValue(row,"ConservationArea");
            obj.TpoOfficer = StringValue(row,"TpoOfficer");
            obj.LocalAuthorityName = StringValue(row,"LocalAuthorityName");
            obj.AppealReference = StringValue(row,"AppealReference");
            obj.AppealDecisionDate = DateTimeValue(row,"AppealDecisionDate");
            return obj;
        }
        #endregion
        #endregion
    }
}
