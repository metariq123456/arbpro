﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    /// <summary>
    /// Class for printing work schedules without booking work
    /// </summary>
    public class TpoWorkSchedule : Contracts.DBContextBase
    {
        #region Members

        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="TpoWorkSchedule"/> class.
        /// </summary>
        /// <param name="databaseName">Database name</param>
        public TpoWorkSchedule(string databaseName)
            : base(databaseName, "TpoWorkSchedule", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties

        #endregion

        #region Methods

        #region Public

        #region Get


        /// <summary>
        /// Method to grab and create the document of a work schedule
        /// </summary>
        /// <param name="jobSiteId">The job site id of the quote</param>
        /// <param name="savePath">The save path for the document template</param>
        /// <param name="quoteId">The Quote id</param>
        /// <returns>The created document</returns>
        public Types.TpoWorkSchedule GetForJobSiteId(int jobSiteId, string savePath, int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Report_TpoWorkSchedule_ForJobSiteId", new SqlParameter[] { new SqlParameter("@JobSiteId", jobSiteId), new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJobSiteId", string.Format("jobSiteId: {0}, savePath: {1}", jobSiteId, savePath), ex);
                Console.WriteLine("Error in TpoWorkSchedule.GetForJobSiteId: {0}", ex.ToString());
            }

            return ToObject(ds, savePath);
        }

        #endregion

        #endregion

        #region Private
        /// <summary>
        /// Method to create the document and retrieve the pictures, comments, staff members and tools for it
        /// </summary>
        /// <param name="ds">The return result of the executed stored procedure</param>
        /// <param name="savePath">The path in which the document template is saved</param>
        /// <returns>The combined document</returns>
        private Types.TpoWorkSchedule ToObject(DataSet ds, string savePath)
        {
            Types.TpoWorkSchedule retVal = new Types.TpoWorkSchedule(savePath);

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0], savePath);

                if (retVal.JobSiteId > 0)
                {

                    // Populate the child objects for the work order
                    DAL.QuoteItem itemContext = new QuoteItem(this.DatabaseName);
                    DAL.ItemPicture pictureContext = new ItemPicture(this.DatabaseName);
                    retVal.QuoteItems = itemContext.GetAllForQuote(retVal.QuoteId);
                    List<Types.QuoteItem> sortedItems = retVal.QuoteItems.OrderBy(item => item.JobItemId).ToList();
                    sortedItems = retVal.QuoteItems.OrderBy(item => item.OrderId).ToList();
                    retVal.QuoteItems = sortedItems;
                    Types.ItemPicture itmPicture = new Types.ItemPicture();

                    foreach (Types.QuoteItem itm in retVal.QuoteItems)
                    {
                        var piclist = pictureContext.GetForJobItem(itm.JobItemId);
                        if (piclist.Count > 0)
                        {
                            foreach (var item in piclist)
                            {
                                itmPicture = item;
                                if (retVal.ItemPictures.ContainsKey(item.Id))
                                {
                                    retVal.ItemPictures[item.Id] = itmPicture;
                                }
                                else
                                {
                                    retVal.ItemPictures.Add(item.Id, itmPicture);
                                } 
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("ds: {0}, savePath: {1}", ds.ToString(), savePath), ex);
                Console.WriteLine("Error in TpoWorkSchedule.ToObject: {0}", ex.ToString());
            }

            return retVal;
        }

        /// <summary>
        /// Creates an instance of the document for the current quote item
        /// </summary>
        /// <param name="row">A data row</param>
        /// <param name="savePath">Save path from the To Object method</param>
        /// <returns>An instance of the object</returns>
        private Types.TpoWorkSchedule CreateInstance(DataRow row, string savePath)
        {
            Types.TpoWorkSchedule obj = new Types.TpoWorkSchedule(savePath);
            obj.JobSiteId = (int)row["JobSiteId"];
            obj.QuoteId = (int)row["QuoteId"];
            obj.ReferenceNo = (string)row["QuoteRef"];

            // Client Details
            obj.ClientID = (int)row["ClientId"];
            obj.AccountNo = (string)row["AccountNum"];
            obj.ClientName = (string)row["CustomerName"];
            obj.ClientStreetNo = (string)row["StreetNumber"];
            obj.ClientAddress1 = (string)row["Address1"];
            obj.ClientAddress2 = (string)row["Address2"];
            obj.ClientTown = (string)row["Town"];
            obj.ClientCounty = (string)row["County"];
            obj.ClientPostCode = (string)row["PostCode"];
            obj.BusinessName = (string)row["BusinessName"];

            // Site Componenets
            obj.SiteRef = (string)row["SiteRef"];
            obj.SiteStreet = (string)row["SiteStreetNumber"];
            obj.SiteAdd1 = (string)row["SiteAddress1"];
            obj.SiteAdd2 = (string)row["SiteAddress2"];
            obj.SiteTown = (string)row["SiteTown"];
            obj.SiteCounty = (string)row["SiteCounty"];
            obj.SitePostCode = (string)row["SitePostCode"];

            // Contact details
            obj.ContactTitle = (string)row["ContactTitle"];
            obj.ContactFirstName = (string)row["ContactFirstName"];
            obj.ContactSurname = (string)row["ContactSurname"];
            obj.ContactMobile = (string)row["ContactMobile"];
            obj.ContactTelephone = (string)row["ContactTelephone"];
            return obj;
        }

        #endregion

        #endregion
    }
}