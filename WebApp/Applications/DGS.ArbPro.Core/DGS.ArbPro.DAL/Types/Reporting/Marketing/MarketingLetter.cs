﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MarketingLetter.cs" company="Microsec Ltd">
//   2016 Copyright Microsec Ltd
// </copyright>
// <summary>
//   The marketing letter.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.DAL
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// The marketing letter.
    /// </summary>
    public class MarketingLetter : Contracts.DBContextBase
    {
        #region Members

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MarketingLetter"/> class.
        /// </summary>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        public MarketingLetter(string databaseName)
            : base(databaseName, "MarketingLetter")
        {
        }

        #endregion

        #region Properties

        #endregion

        #region Methods

        #region Public

        #region Get Methods

        /// <summary>
        /// Method to return a single marketing letter from the database
        /// </summary>
        /// <param name="marketingId">
        /// The marketing Id.
        /// </param>
        /// <param name="savePath">
        /// The save Path.
        /// </param>
        /// <returns>
        /// A marketing letter. 
        /// </returns>
        public Types.MarketingLetter GetForMarketingRecId(int marketingId, string savePath)
        {
            Types.MarketingLetter retVal = null;
            try
            {
                // ReSharper disable once RedundantExplicitArrayCreation
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("Report_Marketing_CoveringLetter", new SqlParameter[] { new SqlParameter("@RecId", marketingId) }, this.DatabaseName);
                retVal = this.ToObject(ds, savePath);
            }
            catch (Exception ex)
            {
                this.LogError("GetForQuoteRecId", string.Format("quoteId: {0}, savePath: {1}", marketingId, savePath), ex);
                Console.WriteLine("Error in QuoteLetter.GetForQuoteRecId: {0}", ex);
            }

            return retVal;
        }

        #endregion

        #endregion

        #region Private

        /// <summary>
        /// Returns a single instance of a QuoteLetter from the first row of a table in a dataset
        /// </summary>
        /// <param name="ds">
        /// the Data Set
        /// </param>
        /// <param name="savePath">
        /// The save Path.
        /// </param>
        /// <returns>
        /// A Types.QuoteLetter instance
        /// </returns>
        private Types.MarketingLetter ToObject(DataSet ds, string savePath)
        {
            Types.MarketingLetter retVal = new Types.MarketingLetter(savePath);

            // Check if there is any data to be returned
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                retVal = this.CreateObject(ds.Tables[0].Rows[0], savePath);
                retVal.ReportCustomeGroups = CustomGroup.GetList(this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("ToObject", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in MarketingLetter.ToObject: {0}", ex);
            }

            return retVal;
        }
        
        /// <summary>
        /// Helper method to create an object instance from a data row
        /// </summary>
        /// <param name="row">
        /// the Data Row
        /// </param>
        /// <param name="savePath">
        /// The save Path.
        /// </param>
        /// <returns>
        /// marketing letter instance
        /// </returns>
        private Types.MarketingLetter CreateObject(DataRow row, string savePath)
        {
            Types.MarketingLetter obj = new Types.MarketingLetter(savePath);
            obj.Id = (int)row["RecId"];
            obj.ClientID = (int)row["ClientId"];
            obj.ClientName = (string)row["CustomerName"];
            obj.ClientStreetNo = (string)row["StreetNumber"];
            obj.ClientAddress1 = (string)row["Address1"];
            obj.ClientAddress2 = (string)row["Address2"];
            obj.ClientTown = (string)row["Town"];
            obj.ClientCounty = (string)row["County"];
            obj.ClientPostCode = (string)row["PostCode"];
            obj.ContactTitle = (string)row["ContactTitle"];
            obj.ContactFirstName = (string)row["ContactFirstName"];
            obj.ContactSurname = (string)row["ContactSurname"];
            obj.SiteRef = (string)row["SiteRef"];
            obj.SiteStreet = (string)row["SiteStreetNum"];
            obj.SiteAdd1 = (string)row["SiteAddress1"];
            obj.SiteAdd2 = (string)row["SiteAddress2"];
            obj.SiteTown = (string)row["SiteTown"];
            obj.SiteCounty = (string)row["SiteCounty"];
            obj.SitePostCode = (string)row["SitePostCode"];
            obj.LetterText = (string)row["LetterText"];
            obj.LetterSignature = this.ByteValue(row, "Signature");
            obj.DirectorName = (string)row["CompanyDirectorName"];
            return obj;
        }

        #endregion

        #endregion
    }
}
