﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL.Reporting.DBItems
{
    public class ReportInvoiceDB
    {
        public String DatabaseName;
        public Int32 InvoiceNo;

        public ReportInvoiceDB(String DatabaseName, Int32 InvoiceNo)
        {
            this.DatabaseName = DatabaseName;
            this.InvoiceNo = InvoiceNo;
        }

        public ReportInvoiceSchedule Schedule()
        {
            ReportInvoiceSchedule ReturnValue = null;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@InvoiceNo", InvoiceNo)
            };

            var Result = DBHelper.ExecuteReader(
                "ReportInvoiceSchedule",
                Params,
                DatabaseName);

            ReturnValue = new ReportInvoiceSchedule();// (Result);

            return ReturnValue;
        }

        public ReportInvoiceLetter FirstLetter()
        {
            ReportInvoiceLetter ReturnValue = null;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@InvoiceNo", InvoiceNo),
                new SqlParameter("@LetterTemplate", "1stInvoiceLetter")
            };

            var Result = DBHelper.ExecuteReader(
                "ReportInvoiceLetter",
                Params,
                DatabaseName);

            while (Result.Read())
                ReturnValue = new ReportInvoiceLetter();// (Result);
            Result.Close();

            return ReturnValue;
        }

        public ReportInvoiceLetter FirstReminder()
        {
            ReportInvoiceLetter ReturnValue = null;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@InvoiceNo", InvoiceNo),
                new SqlParameter("@LetterTemplate", "FirstReminder")
            };

            var Result = DBHelper.ExecuteReader(
                "ReportInvoiceLetter",
                Params,
                DatabaseName);

            while (Result.Read())
                ReturnValue = new ReportInvoiceLetter();// (Result);
            Result.Close();

            return ReturnValue;
        }

        public ReportInvoiceLetter SecondReminder()
        {
            ReportInvoiceLetter ReturnValue = null;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@InvoiceNo", InvoiceNo),
                new SqlParameter("@LetterTemplate", "SecondReminder")
            };

            var Result = DBHelper.ExecuteReader(
                "ReportInvoiceLetter",
                Params,
                DatabaseName);

            while (Result.Read())
                ReturnValue = new ReportInvoiceLetter();// (Result);
            Result.Close();

            return ReturnValue;
        }

        public ReportInvoiceLetter FinalReminder()
        {
            ReportInvoiceLetter ReturnValue = null;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@InvoiceNo", InvoiceNo),
                new SqlParameter("@LetterTemplate", "FinalReminder")
            };

            var Result = DBHelper.ExecuteReader(
                "ReportInvoiceLetter",
                Params,
                DatabaseName);

            while (Result.Read())
                ReturnValue = new ReportInvoiceLetter();// (Result);
            Result.Close();

            return ReturnValue;
        }

        public ReportInvoiceLetter LegalAction1()
        {
            ReportInvoiceLetter ReturnValue = null;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@InvoiceNo", InvoiceNo),
                new SqlParameter("@LetterTemplate", "LegalAction1")
            };

            var Result = DBHelper.ExecuteReader(
                "ReportInvoiceLetter",
                Params,
                DatabaseName);

            while (Result.Read())
                ReturnValue = new ReportInvoiceLetter();// (Result);
            Result.Close();

            return ReturnValue;
        }

        public ReportInvoiceLetter LegalAction2()
        {
            ReportInvoiceLetter ReturnValue = null;

            SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("@InvoiceNo", InvoiceNo),
                new SqlParameter("@LetterTemplate", "LegalAction2")
            };

            var Result = DBHelper.ExecuteReader(
                "ReportInvoiceLetter",
                Params,
                DatabaseName);

            while (Result.Read())
                ReturnValue = new ReportInvoiceLetter();// (Result);
            Result.Close();

            return ReturnValue;
        }
    }
}
