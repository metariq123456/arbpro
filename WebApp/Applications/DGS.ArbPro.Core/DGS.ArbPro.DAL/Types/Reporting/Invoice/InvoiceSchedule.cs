﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class InvoiceSchedule: Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public InvoiceSchedule(string databaseName)
            : base(databaseName, "InvoiceSchedule", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        /// <summary>
        /// Method to return a single InvoiceSchedule from the database
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>Types.InvoiceSchedule</returns>
        public Types.InvoiceSchedule GetForInvoiceRecId(int invId, string savePath)
        {
            Types.InvoiceSchedule retVal = null;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Report_InvoiceSchedule_GetForInvoiceRec", new SqlParameter[] { new SqlParameter("@InvId", invId) }, this.DatabaseName);
                retVal = ToObject(ds, savePath);


            }
            catch (Exception ex)
            {
                LogError("GetForInvoiceRecId", string.Format("invId: {0}, savePath: {1}", invId, savePath), ex);
                Console.WriteLine("Error in InvoiceSchedule.GetForInvoiceRecId: {0}", ex.ToString());
            }
            return retVal;

        }
        #endregion
        #endregion

        #region Private

        /// <summary>
        /// Returns a single instance of a InvoiceSchedule from the first row of a table in a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>A Types.InvoiceSchedule instance</returns>
        private Types.InvoiceSchedule ToObject(DataSet ds, string savePath)
        {
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            Types.InvoiceSchedule retVal = new Types.InvoiceSchedule(savePath);
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0],savePath, cultureCode);
                retVal.ReportCustomeGroups = DAL.CustomGroup.GetList(this.DatabaseName);

                DAL.InvoiceItem itmContext = new DAL.InvoiceItem(this.DatabaseName);
                retVal.InvoiceItems = itmContext.GetAllForInvoice(retVal.RecordId);
                List<Types.InvoiceItem> sortedItems = retVal.InvoiceItems.OrderBy(item => item.JobItemId).ToList();
                sortedItems = retVal.InvoiceItems.OrderBy(item => item.OrderId).ToList();
                retVal.InvoiceItems = sortedItems;
                
                DAL.JobSite jsContext = new DAL.JobSite(this.DatabaseName);
                retVal.JobSites = jsContext.GetForInvoice(retVal.RecordId, false, false);

                DAL.Invoice dtbInvoice = new DAL.Invoice(this.DatabaseName);
                retVal.Invoice = dtbInvoice.GetForId(retVal.RecordId);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in InvoiceSchedule.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Populate a list of Clients from a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>List of Types.InvoiceSchedule</returns>
        private List<Types.InvoiceSchedule> ToObjectList(DataSet ds, string savePath)
        {
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            List<Types.InvoiceSchedule> retVal = new List<Types.InvoiceSchedule>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.InvoiceSchedule obj = null;
            try
            {
                DAL.InvoiceItem itmContext = new DAL.InvoiceItem(this.DatabaseName);
                DAL.CustomGroup dbContext = new DAL.CustomGroup(this.DatabaseName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateObject(r,savePath, cultureCode);
                    obj.ReportCustomeGroups = dbContext.GetList();
                    obj.InvoiceItems = itmContext.GetAllForInvoice(obj.RecordId);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in InvoiceSchedule.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>InvoiceSchedule instance</returns>
        private Types.InvoiceSchedule CreateObject(DataRow row, string savePath, string cultureCode)
        {
            Types.InvoiceSchedule obj = new Types.InvoiceSchedule(savePath, cultureCode);
            obj.RecordId = (int)row["InvId"];
            obj.ClientID = (int)row["ClientId"];
            obj.ClientName = (string)row["CustomerName"];
            obj.ClientStreetNo = (string)row["StreetNumber"];
            obj.ClientAddress1 = (string)row["Address1"];
            obj.ClientAddress2 = (string)row["Address2"];
            obj.ClientTown = (string)row["Town"];
            obj.ClientCounty = (string)row["County"];
            obj.ClientPostCode = (string)row["PostCode"];
            obj.ContactTitle = (string)row["ContactTitle"];
            obj.ContactFirstName = (string)row["ContactFirstName"];
            obj.ContactSurname = (string)row["ContactSurname"];
            obj.SiteStreet = (string)row["SiteStreetNum"];
            obj.SiteAdd1 = (string)row["SiteAddress1"];
            obj.SiteAdd2 = (string)row["SiteAddress2"];
            obj.SiteTown = (string)row["SiteTown"];
            obj.SiteCounty = (string)row["SiteCounty"];
            obj.SitePostCode = (string)row["SitePostCode"];
            obj.QuoteNo = (string)row["QuoteRefNo"];
            obj.InvoiceNo = (string)row["RefNo"];
            obj.OrderNo = obj.QuoteNo; // (string)row["OrderNo"];
            obj.IsCommercial = (bool)row["IsCommercial"];
            obj.SiteRefNo = (string)row["SiteRef"];
            obj.PurchaseOrderNo = StringValue(row, "PurchaseOrderNumber");
            return obj;
        }
        #endregion
        #endregion
    }
}
