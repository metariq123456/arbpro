﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class InvoiceLetter: Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public InvoiceLetter(string databaseName)
            : base(databaseName, "InvoiceLetter", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        /// <summary>
        /// Method to return a single letter from the database
        /// </summary>
        /// <param name="invId">int: The invoice record Id</param>
        /// <param name="savePath">string: the path where a file would be saved</param>
        /// <param name="rptType">InvoiceDocTypeEnum: The type of invoice document to be created</param>
        /// <returns></returns>
        public Types.InvoiceLetter GetForInvoiceRecId(int invId, string savePath, InvoiceDocTypeEnum rptType)
        {
            Types.InvoiceLetter retVal = null;
            try
            {
                DataSet ds = null;
                string templateName = "1stInvoiceLetter";
                switch (rptType)
                {
                    case InvoiceDocTypeEnum.Letter:
                        templateName = "1stInvoiceLetter";
                        break;
                    case InvoiceDocTypeEnum.First_Reminder:
                        templateName = "FirstReminder";
                        break;
                    case InvoiceDocTypeEnum.Second_Reminder:
                        templateName = "SecondReminder";
                        break;
                    case InvoiceDocTypeEnum.Final_Reminder:
                        templateName = "FinalReminder";
                        break;
                    case InvoiceDocTypeEnum.Legal_Action:
                        templateName = "LegalAction1";
                        break;
                    case InvoiceDocTypeEnum.Legal_Action_2:
                        templateName = "LegalAction2";
                        break;
                }

                ds = DBHelper.ExecuteStoredProcDataSet("Report_Invoice_Letter", new SqlParameter[] { new SqlParameter("@InvId", invId), new SqlParameter("@TemplateType", templateName) }, this.DatabaseName);
                retVal = ToObject(ds, savePath, rptType);


            }
            catch (Exception ex)
            {
                LogError("GetForQuoteRecId", string.Format("invId: {0}, savePath: {1}", invId, savePath), ex);
                Console.WriteLine("Error in InvoiceLetter.GetForQuoteRecId: {0}", ex.ToString());
            }
            return retVal;

        }
        #endregion
        #endregion

        #region Private

        /// <summary>
        /// Returns a single instance of a InvoiceLetter from the first row of a table in a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>A Types.InvoiceLetter instance</returns>
        private Types.InvoiceLetter ToObject(DataSet ds, string savePath, InvoiceDocTypeEnum rptType)
        {
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            Types.InvoiceLetter retVal = new Types.InvoiceLetter(savePath, rptType);
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0], savePath, rptType, cultureCode);
                retVal.ReportCustomeGroups = DAL.CustomGroup.GetList(this.DatabaseName);

                DAL.Invoice dtbInvoice = new DAL.Invoice(this.DatabaseName);
                retVal.Invoice = dtbInvoice.GetForId(retVal.Id);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in InvoiceLetter.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Populate a list of Clients from a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>List of Types.InvoiceLetter</returns>
        private List<Types.InvoiceLetter> ToObjectList(DataSet ds, string savePath, InvoiceDocTypeEnum rptType)
        {
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            List<Types.InvoiceLetter> retVal = new List<Types.InvoiceLetter>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.InvoiceLetter obj = null;
            try
            {
                DAL.CustomGroup dbContext = new DAL.CustomGroup(this.DatabaseName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateObject(r, savePath, rptType, cultureCode);
                    obj.ReportCustomeGroups = DAL.CustomGroup.GetList(this.DatabaseName);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in InvoiceLetter.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>InvoiceLetter instance</returns>
        private Types.InvoiceLetter CreateObject(DataRow row, string savePath, InvoiceDocTypeEnum rptType, string cultureCode)
        {
            Types.InvoiceLetter obj = new Types.InvoiceLetter(savePath, rptType, cultureCode);
            obj.Id = (int)row["InvId"];
            obj.ClientID = (int)row["ClientId"]; 
            obj.ReferenceNo = (string)row["RefNo"];
            obj.QuoteNo = (string)row["QuoteRefNo"];
            obj.OrderNo = obj.QuoteNo;
            obj.ClientName = (string)row["CustomerName"];
            obj.ClientStreetNo = (string)row["StreetNumber"];
            obj.ClientAddress1 = (string)row["Address1"];
            obj.ClientAddress2 = (string)row["Address2"];
            obj.ClientTown = (string)row["Town"];
            obj.ClientCounty = (string)row["County"];
            obj.ClientPostCode = (string)row["PostCode"];
            obj.ContactTitle = (string)row["ContactTitle"];
            obj.ContactFirstName = (string)row["ContactFirstName"];
            obj.ContactSurname = (string)row["ContactSurename"];
            obj.SiteStreet = (string)row["SiteStreetNum"];
            obj.SiteAdd1 = (string)row["SiteAddress1"];
            obj.SiteAdd2 = (string)row["SiteAddress2"];
            obj.SiteTown = (string)row["SiteTown"];
            obj.SiteCounty = (string)row["SiteCounty"];
            obj.SitePostCode = (string)row["SitePostCode"];
            obj.LetterText = (string)row["BodyText"];
            obj.LetterSignature = base.ByteValue(row, "Signature");
            obj.DirectorName = (string)row["CompanyDirectorName"];
            obj.InvoiceDateSent = DateTime.Today;
            obj.InvoiceValue = (decimal)row["Total"];
            obj.InvoiceVatValue = (decimal)row["VAT"];
            obj.InvoiceTotalIncVat = obj.InvoiceValue + obj.InvoiceVatValue;
            obj.IsCommercial = (bool)row["IsCommercial"];
            obj.SiteRef = (string)row["SiteRef"];
            obj.PurchaseOrderNo = StringValue(row, "PurchaseOrderNumber");
            return obj;
        }
        #endregion
        #endregion
    }
}
