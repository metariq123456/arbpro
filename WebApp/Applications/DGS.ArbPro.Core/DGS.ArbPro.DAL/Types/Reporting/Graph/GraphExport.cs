﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;
using System.Data.SqlClient;

namespace DGS.ArbPro.DAL
{
    public class GraphExport : Contracts.DBContextBase
    {
        public GraphExport(string databaseName)
            : base(databaseName, "GraphExport", DateTime.Parse("1970-01-01"))
        {

        }
        public Types.ReportObjects.GraphExport GraphExportLeadDource(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_LeadSource", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_LeadSource: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforReceived(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_Received", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforQuotationSent(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_QuotationSent", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_QuotationSent: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforQuotationAccepted(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_QuotationAccepted", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforTreeWorkBooked(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_WorkBooked", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforTreeProgress(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_WorkInProgress", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforTreeCompleted(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_WorkCompleted", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforSalesHistorically(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_SalesHistorically", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforSalesOutstanding(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_SalesOutstanding", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforSuccessRate(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_Received", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforSales(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_IndividualSales", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforInvoicesPaid(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_SalesPaid", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforInvoicesVatPaid(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("Graph_Export_SalesVatPaid", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.GraphExport GraphExportforInvoicesTotalSaleIncVat(String FromDate, String ToDate)
        {
            DataSet ds = null;
            try
            {

                ds = DBHelper.ExecuteStoredProcDataSet("GraphExportforInvoicesTotalSaleIncVat", new SqlParameter[] { Parameterize("@FromDate", FromDate), Parameterize("@ToDate", ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in Graph_Export_Received: {0}", ex.ToString());
            }
            return GetExport(ds);
        }

        

        

        private Types.ReportObjects.GraphExport GetExport(DataSet ds)
        {
            Types.ReportObjects.GraphExport retVal = new Types.ReportObjects.GraphExport();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.ExportLines.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("GetFirstFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Staff.GetFirstFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }
        private Types.ReportObjects.GraphExportLine CreateInstance(DataRow r)
        {
            Types.ReportObjects.GraphExportLine obj = new Types.ReportObjects.GraphExportLine();
            obj.Column1 = (string)r["Column1"];
            obj.Column2 = (string)r["Column2"];
            obj.Column3 = (string)r["Column3"];
            obj.Column4 = (string)r["Column4"];
            obj.Column5 = (string)r["Column5"];
            obj.Column6 = (string)r["Column6"];
            obj.Column7 = (string)r["Column7"];
            obj.Column8 = (string)r["Column8"];
            obj.Column9 = (string)r["Column9"];
            obj.Column10 = (string)r["Column10"];
            obj.Column11 = (string)r["Column11"];
            return obj;
        }

    }
}
