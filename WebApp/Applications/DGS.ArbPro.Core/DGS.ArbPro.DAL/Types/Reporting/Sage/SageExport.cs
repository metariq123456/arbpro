﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;
using DGS.ArbPro.Types.Enums.SageEnums;

namespace DGS.ArbPro.DAL
{
    public class SageExport : Contracts.DBContextBase
    {
        #region Constructor
        public SageExport(string databaseName)
            : base(databaseName, "SageExport", DateTime.Parse("1970-01-01"))
        {
        }
        #endregion
        public Types.ReportObjects.SageExport Export(SageTypeEnum OutputType, ExportTypeEnum ObjectType, DateTime FromDate, DateTime ToDate)
        {
            DataSet ds = null;
            try
            {
                var ProcedureName = String.Format("Export_{0}_{1}", OutputType.ToString(), ObjectType.ToString());
                ds = DBHelper.ExecuteStoredProcDataSet(ProcedureName, new SqlParameter[] { Parameterize("@From",FromDate), Parameterize("@To",ToDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in SageExport: {0}", ex.ToString());
            }
            return GetExport(ds);
        }
        public Types.ReportObjects.SageExport ExportToCSVInvoice(SageTypeEnum OutputType, ExportTypeEnum ObjectType, DateTime FromDate, DateTime ToDate)
        {
            DataSet ds = null;
            try
            {
                var ProcedureName = "Export_CSV_INVOICE";
                int Status = 0;
                if ((int)ObjectType == 4)
                    Status = (int)InvoiceStatusEnum.Sent;
                if ((int)ObjectType ==5)
                    Status = (int)InvoiceStatusEnum.Pending;
                if ((int)ObjectType == 6)
                    Status = (int)InvoiceStatusEnum.Paid;


                ds = DBHelper.ExecuteStoredProcDataSet(ProcedureName, new SqlParameter[] { Parameterize("@From", FromDate), Parameterize("@To", ToDate), Parameterize("@Status", Status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Export", "None", ex);
                Console.WriteLine("Error in SageExport: {0}", ex.ToString());
            }
            return GetExport(ds);
        }

        private Types.ReportObjects.SageExport GetExport(DataSet ds)
        {
            Types.ReportObjects.SageExport retVal = new Types.ReportObjects.SageExport();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.ExportLines.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("GetFirstFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Staff.GetFirstFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.ReportObjects.SageExportLine CreateInstance(DataRow r)
        {
            Types.ReportObjects.SageExportLine obj = new Types.ReportObjects.SageExportLine();
            obj.Column1 = (string)r["Column1"];
            obj.Column2 = (string)r["Column2"];
            obj.Column3 = (string)r["Column3"];
            obj.Column4 = (string)r["Column4"];
            obj.Column5 = (string)r["Column5"];
            obj.Column6 = (string)r["Column6"];
            obj.Column7 = (string)r["Column7"];
            obj.Column8 = (string)r["Column8"];
            obj.Column9 = (string)r["Column9"];
            obj.Column10 = (string)r["Column10"];
            obj.Column11 = (string)r["Column11"];
            return obj;
        }
    }
}

    //Types.Staff retVal = new Types.Staff();

    //if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
    //    return retVal;

    //try
    //{
    //    retVal = CreateObject(ds.Tables[0].Rows[0]);
    //}
    //catch (Exception ex)
    //{
    //    LogError("GetFirstFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
    //    Console.WriteLine("Error in Staff.GetFirstFromDataSet: {0}", ex.ToString());
    //}

    //return retVal;
