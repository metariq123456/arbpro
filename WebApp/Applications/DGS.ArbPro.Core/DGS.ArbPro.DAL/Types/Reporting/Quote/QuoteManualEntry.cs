﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class QuoteManualEntry : Contracts.DBContextBase
    {
       #region Constructors
        public QuoteManualEntry(string databaseName)
            : base(databaseName, "QuoteManualEntry")
        {
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        public Types.QuoteManualEntry GetForId(int Id, string savePath)
        {
            Types.QuoteManualEntry retVal = null;
            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("Report_Quote_ManualEntry", new SqlParameter[] { new SqlParameter("Id", Id) }, this.DatabaseName);
                retVal = ToObject(ds, savePath);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteRecId", string.Format("quoteId: {0}, savePath: {1}", Id, savePath), ex);
                Console.WriteLine("Error in QuoteManualEntry.GetForId: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #endregion

        #region Private
        private Types.QuoteManualEntry ToObject(DataSet ds, string savePath)
        {
            Types.QuoteManualEntry retVal = new Types.QuoteManualEntry(savePath);
            //check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0], savePath);
                retVal.ReportCustomeGroups = DAL.CustomGroup.GetList(this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteManualEntry.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.QuoteManualEntry> ToObjectList(DataSet ds, string savePath)
        {
            List<Types.QuoteManualEntry> retVal = new List<Types.QuoteManualEntry>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.QuoteManualEntry obj = null;

            try
            {
                DAL.CustomGroup dbContext = new DAL.CustomGroup(this.DatabaseName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateObject(r, savePath);
                    obj.ReportCustomeGroups = dbContext.GetList();
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteManualEntry.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.QuoteManualEntry CreateObject(DataRow row, string savePath)
        {
            Types.QuoteManualEntry obj = new Types.QuoteManualEntry(savePath);
            try
            {
                obj.Id = (int)row["JobId"];
                obj.DateQuoteBooked = (DateTime)row["DateQuoteBooked"];
                obj.QuoteTime = (DateTime)row["StartDateTime"];
                obj.SurveyorName = (string)row["StaffName"];
                obj.Company = (string)row["ClientStreetNumber"];
                obj.AddressLine1 = (string)row["ClientAddressLine1"];
                obj.AddressLine2 = (string)row["ClientAddressLine2"];
                obj.SiteTown = (string)row["ClientTown"];
                obj.SiteCounty = (string)row["ClientCounty"];
                obj.PostCode = (string)row["ClientPostCode"];
                obj.SiteAddress = (string)row["SiteAddress"];
                obj.SiteSpecificRefNo = (string)row["SiteRef"];
                obj.AccountNo = (string)row["AccountNumber"];
                obj.JobNo = (int)row["JobId"];
                obj.ClientName = (string)row["ClientName"];
                obj.Mobile = (string)row["ClientMob"];
                obj.Telephone = (string)row["ClientTelephone"];
                obj.ContactName = (string)row["ContactName"];
             }
            catch (Exception ex)
            {
                Console.WriteLine("Error in QuoteManualEntry.CreateObject: {0}", ex.ToString());
            }
            return obj;
        }
        #endregion

        #endregion
    }
}

