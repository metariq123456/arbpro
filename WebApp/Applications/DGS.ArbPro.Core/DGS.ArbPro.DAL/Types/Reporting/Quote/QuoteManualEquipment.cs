﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class QuoteManualEquipment : Contracts.DBContextBase
    {
        #region Constructors
        public QuoteManualEquipment(string databaseName)
            : base(databaseName, "QuoteManualEquipment")
        {
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        public Types.QuoteManualEquipment GetForId(int Id, string savePath)
        {
            Types.QuoteManualEquipment retVal = null;
            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("Report_Quote_ManualEquipment", new SqlParameter[] { new SqlParameter("Id", Id) }, this.DatabaseName);
                retVal = ToObject(ds, savePath);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteRecId", string.Format("quoteId: {0}, savePath: {1}", Id, savePath), ex);
                Console.WriteLine("Error in QuoteManualEquipment.GetForId: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #endregion

        #region Private
        private Types.QuoteManualEquipment ToObject(DataSet ds, string savePath)
        {
            Types.QuoteManualEquipment retVal = new Types.QuoteManualEquipment(savePath);
            //check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0], savePath);
                retVal.ReportCustomeGroups = DAL.CustomGroup.GetList(this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteManualEquipment.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.QuoteManualEquipment> ToObjectList(DataSet ds, string savePath)
        {
            List<Types.QuoteManualEquipment> retVal = new List<Types.QuoteManualEquipment>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.QuoteManualEquipment obj = null;

            try
            {
                DAL.CustomGroup dbContext = new DAL.CustomGroup(this.DatabaseName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateObject(r, savePath);
                    obj.ReportCustomeGroups = dbContext.GetList();
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteManualEquipment.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.QuoteManualEquipment CreateObject(DataRow row, string savePath)
        {
            Types.QuoteManualEquipment obj = new Types.QuoteManualEquipment(savePath);
            try
            {
                obj.Id = (int)row["JobId"];
                obj.SiteSpecificRefNo = (string)row["SiteRef"];
                obj.SiteAddress = (string)row["SiteAddress"];
                obj.JobNo = (int)row["JobId"];
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error in QuoteManualEquipment.CreateObject: {0}", ex.ToString());
            }
            return obj;


        }
        #endregion

        #endregion
    }
}
