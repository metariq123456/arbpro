﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class QuoteSchedule: Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public QuoteSchedule(string databaseName)
            : base(databaseName, "QuoteSchedule")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        /// <summary>
        /// Method to return a single QuoteSchedule from the database
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>Types.QuoteSchedule</returns>
        public Types.QuoteSchedule GetForQuoteRecId(int quoteId, string savePath)
        {
            Types.QuoteSchedule retVal = null;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Report_QuoteSchedule_GetForQuoteRec", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
                retVal = PopulateSingleResultFromDataSet(ds, savePath);
                //Get the items for the schedule
                DAL.QuoteItem itemsContext = new QuoteItem(this.DatabaseName);
                retVal.QuoteItems = itemsContext.GetAllForQuote(retVal.Id);
                List<Types.QuoteItem> sortedItems = retVal.QuoteItems.OrderBy(item => item.JobItemId).ToList();
                sortedItems = retVal.QuoteItems.OrderBy(item => item.OrderId).ToList();
                retVal.QuoteItems = sortedItems;
                retVal.ReportCustomeGroups = DAL.CustomGroup.GetList(this.DatabaseName);
                //Get to the jobsites associated with the quote
                DAL.JobSite jsContext = new DAL.JobSite(this.DatabaseName);
                retVal.JobSites = jsContext.GetForQuote(retVal.Id, false, false);

            }
            catch (Exception ex)
            {
                LogError("GetForQuoteRecId", string.Format("quoteId: {0}, savePath: {1}", quoteId, savePath), ex);
                Console.WriteLine("Error in QuoteSchedule.GetForQuoteRecId: {0}", ex.ToString());
            }
            return retVal;

        }
        #endregion
        #endregion

        #region Private

        /// <summary>
        /// Returns a single instance of a QuoteSchedule from the first row of a table in a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>A Types.QuoteSchedule instance</returns>
        private Types.QuoteSchedule PopulateSingleResultFromDataSet(DataSet ds, string savePath)
        {
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            Types.QuoteSchedule retVal = new Types.QuoteSchedule(savePath);
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0],savePath, cultureCode);
                
            }
            catch (Exception ex)
            {
                LogError("PopulateSingleResultFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteSchedule.PopulateSingleResultFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Populate a list of Clients from a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>List of Types.QuoteSchedule</returns>
        private List<Types.QuoteSchedule> PopulateResultsFromDataSet(DataSet ds, string savePath)
        {
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            List<Types.QuoteSchedule> retVal = new List<Types.QuoteSchedule>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.QuoteSchedule obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateObject(r,savePath, cultureCode);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteSchedule.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>QuoteSchedule instance</returns>
        private Types.QuoteSchedule CreateObject(DataRow row, string savePath, string cultureCode)
        {
            Types.QuoteSchedule obj = new Types.QuoteSchedule(savePath, cultureCode);
            obj.Id = (int)row["RecId"];
            obj.ClientID = (int)row["ClientId"];
            obj.ReferenceNo = (string)row["RefNo"];
            obj.ClientName = (string)row["CustomerName"];
            obj.ClientStreetNo = (string)row["StreetNumber"];
            obj.ClientAddress1 = (string)row["Address1"];
            obj.ClientAddress2 = (string)row["Address2"];
            obj.ClientTown = (string)row["Town"];
            obj.ClientCounty = (string)row["County"];
            obj.ClientPostCode = (string)row["PostCode"];
            obj.ContactTitle = (string)row["ContactTitle"];
            obj.ContactFirstName = (string)row["ContactFirstName"];
            obj.ContactSurname = (string)row["ContactSurename"];
            obj.QuoteSurveyor = (string)row["Surveyor"];
            obj.SiteRef = (string)row["SiteRef"];
            obj.SiteStreet = (string)row["SiteStreetNum"];
            obj.SiteAdd1 = (string)row["SiteAddress1"];
            obj.SiteAdd2 = (string)row["SiteAddress2"];
            obj.SiteTown = (string)row["SiteTown"];
            obj.SiteCounty = (string)row["SiteCounty"];
            obj.SitePostCode = (string)row["SitePostCode"];
            obj.IsCommercial = (bool)row["IsCommercial"];
            obj.QuoteVatRate = (decimal)row["VatRate"];
            obj.PurchaseOrderNo = StringValue(row, "PurchaseOrderNumber");
            return obj;
        }
        #endregion
        #endregion
    }
}
