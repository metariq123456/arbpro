﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class QuoteLetter: Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public QuoteLetter(string databaseName)
            : base(databaseName, "QuoteLetter")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        /// <summary>
        /// Method to return a single QuoteLetter from the database
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>Types.QuoteLetter</returns>
        public Types.QuoteLetter GetForQuoteRecId(int quoteId, string savePath)
        {
            Types.QuoteLetter retVal = null;
            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("Report_Quote_CoveringLetter", new SqlParameter[] { new SqlParameter("@RecId", quoteId) }, this.DatabaseName);
                retVal = ToObject(ds, savePath);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteRecId", string.Format("quoteId: {0}, savePath: {1}", quoteId, savePath), ex);
                Console.WriteLine("Error in QuoteLetter.GetForQuoteRecId: {0}", ex.ToString());
            }
            return retVal;

        }
        #endregion
        #endregion

        #region Private

        /// <summary>
        /// Returns a single instance of a QuoteLetter from the first row of a table in a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>A Types.QuoteLetter instance</returns>
        private Types.QuoteLetter ToObject(DataSet ds, string savePath)
        {
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            Types.QuoteLetter retVal = new Types.QuoteLetter(savePath);
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0],savePath, cultureCode);
                retVal.ReportCustomeGroups = DAL.CustomGroup.GetList(this.DatabaseName);
                
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteLetter.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Populate a list of Clients from a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>List of Types.QuoteLetter</returns>
        private List<Types.QuoteLetter> ToObjectList(DataSet ds, string savePath)
        {
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            List<Types.QuoteLetter> retVal = new List<Types.QuoteLetter>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.QuoteLetter obj = null;
            try
            {
                DAL.CustomGroup dbContext = new DAL.CustomGroup(this.DatabaseName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateObject(r,savePath, cultureCode);
                    obj.ReportCustomeGroups = dbContext.GetList();
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteLetter.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>QuoteLetter instance</returns>
        private Types.QuoteLetter CreateObject(DataRow row, string savePath, string cultureCode)
        {
            Types.QuoteLetter obj = new Types.QuoteLetter(savePath, cultureCode);
            obj.Id = (int)row["RecId"];
            obj.ClientID = (int)row["ClientId"];
            obj.ReferenceNo = (string)row["RefNo"];
            obj.ClientName = (string)row["CustomerName"];
            obj.ClientStreetNo = (string)row["StreetNumber"];
            obj.ClientAddress1 = (string)row["Address1"];
            obj.ClientAddress2 = (string)row["Address2"];
            obj.ClientTown = (string)row["Town"];
            obj.ClientCounty = (string)row["County"];
            obj.ClientPostCode = (string)row["PostCode"];
            obj.ContactTitle = (string)row["ContactTitle"];
            obj.ContactFirstName = (string)row["ContactFirstName"];
            obj.ContactSurname = (string)row["ContactSurename"];
            obj.QuoteSurveyor = (string)row["Surveyor"];
            obj.SiteRef = (string)row["SiteRef"];
            obj.SiteStreet = (string)row["SiteStreetNum"];
            obj.SiteAdd1 = (string)row["SiteAddress1"];
            obj.SiteAdd2 = (string)row["SiteAddress2"];
            obj.SiteTown = (string)row["SiteTown"];
            obj.SiteCounty = (string)row["SiteCounty"];
            obj.SitePostCode = (string)row["SitePostCode"];
            obj.LetterText = (string)row["LetterText"];
            obj.LetterSignature = base.ByteValue(row,"Signature");
            obj.DirectorName = (string)row["CompanyDirectorName"];
            obj.IsCommercial = (bool)row["IsCommercial"];
            obj.PurchaseOrderNo = StringValue(row, "PurchaseOrderNumber");
            return obj;
        }
        #endregion
        #endregion
    }
}
