﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    class QuoteMapTools : Contracts.DBContextBase
    {
        #region Constructors
        public QuoteMapTools(string databaseName)  : base(databaseName, "QuoteMapTools")
        {
        }
        #endregion
    }
}
