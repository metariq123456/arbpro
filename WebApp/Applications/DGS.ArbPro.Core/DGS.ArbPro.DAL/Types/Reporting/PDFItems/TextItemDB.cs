﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Drawing;
using PDFReport;

namespace DGS.ArbPro.DAL
{
    public class TextItem: Contracts.DBContextBase
    {
        #region Constructors
        public TextItem(string databaseName) : base(databaseName, "LineItem") { }
        #endregion

        #region Methods
        #region Public
        public int Save(PDFReport.TextItem obj)
        {
            int retVal = -1;
            string spName = "ReportTextItemInsert";
            ParameterMode mode = ParameterMode.Insert;

            if (obj.Id > 0)
            {
                spName = "ReportTextItemUpdate";
                mode = ParameterMode.Update;
            }

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(spName, Parameters(obj, mode), this.DatabaseName);
            }
            catch (Exception ex)
            {

            }
            return retVal;
        }

        public List<PDFReport.TextItem> GetForGroup(int groupId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ReportTextItemsForGroupGet", new SqlParameter[] { new SqlParameter("@GroupId", groupId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {

            }
            return ToObjectList(ds);
        }
        #endregion
        #region Private
        private SqlParameter[] Parameters(PDFReport.TextItem obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
                retVal.Add(new SqlParameter("@Id", obj.Id));

            retVal.Add(new SqlParameter("@GroupId", obj.GroupId));
            retVal.Add(new SqlParameter("@Name", obj.Name));
            retVal.Add(new SqlParameter("@HAlign", obj.HAlign));
            retVal.Add(new SqlParameter("@VAlign", obj.VAlign));
            retVal.Add(new SqlParameter("@XPos", obj.X));
            retVal.Add(new SqlParameter("@YPos", obj.Y));
            retVal.Add(new SqlParameter("@Width", obj.Width));
            retVal.Add(new SqlParameter("@Height", obj.Height));
            retVal.Add(new SqlParameter("@Text", obj.Text));
            retVal.Add(new SqlParameter("@ItemType", (int)obj.SpecialType));
            retVal.Add(new SqlParameter("@Font", obj.FontName));
            retVal.Add(new SqlParameter("@PointSize", obj.Font.Size));
            retVal.Add(new SqlParameter("@Colour", obj.Colour));

            return retVal.ToArray();
        }

        private List<PDFReport.TextItem> ToObjectList(DataSet ds)
        {
            List<PDFReport.TextItem> retVal = new List<PDFReport.TextItem>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {

            }
            return retVal;
        }

        private PDFReport.TextItem CreateInstance(DataRow row)
        {
            PDFReport.TextItem obj = new PDFReport.TextItem(row["Text"].ToString(),
                                                            double.Parse(row["XPos"].ToString()),
                                                            double.Parse(row["YPos"].ToString()),
                                                            double.Parse(row["Width"].ToString()),
                                                            row["Name"].ToString());
                
            obj.Id = (int)row["Id"];
            obj.GroupId = (int)row["GroupId"];
            obj.Colour = row["Colour"].ToString();
            obj.FontName = row["Font"].ToString();
            obj.HAlign = row["HAlign"].ToString();
            obj.VAlign = row["VAlign"].ToString();
            obj.Height = double.Parse(row["Height"].ToString());
            obj.PointSize = double.Parse(row["PointSize"].ToString());
            return obj;
        }
        #endregion
        #endregion
        //public static List<TextItem> GetTextItemsForGroup(string databaseName, int groupId)
        //{
        //    List<TextItem> retVal = new List<TextItem>();
        //    DataSet ds;
        //    TextItem item = null;
        //    ds = DBHelper.ExecuteStoredProcDataSet("ReportTextItemsForGroupGet", new SqlParameter[] { new SqlParameter("@GroupId", groupId) }, databaseName);
        //    foreach (DataRow Row in ds.Tables[0].Rows)
        //    {
        //        item = new TextItem(Row["Text"].ToString(), double.Parse(Row["XPos"].ToString()), double.Parse(Row["YPos"].ToString()), double.Parse(Row["Width"].ToString()), Row["Name"].ToString());
        //        item.Colour = Row["Colour"].ToString();
        //        item.FontName = Row["Font"].ToString();
        //        item.HAlign = Row["HAlign"].ToString();
        //        item.VAlign = Row["VAlign"].ToString();
        //        item.Height = double.Parse(Row["Height"].ToString());
        //        item.PointSize = double.Parse(Row["PointSize"].ToString());
        //        retVal.Add(item);
        //    }
        //    return retVal;
        //}

        //public static int InsertTextItemForGroup(string databaseName, int groupID, TextItem item)
        //{
        //    try
        //    {
        //        int ReturnValue;
        //        SqlParameter[] parameters = new SqlParameter[]
        //        {
        //            new SqlParameter("@GroupId", groupID),
        //            new SqlParameter("@Name", item.Name), 
        //            new SqlParameter("@HAlign",item.HAlign),
        //            new SqlParameter("@VAlign",item.VAlign),
        //            new SqlParameter("@XPos", item.X),
        //            new SqlParameter("@YPos", item.Y),
        //            new SqlParameter("@Width", item.Width),
        //            new SqlParameter("@Height",item.Height),
        //            new SqlParameter("@Text",item.Text),
        //            new SqlParameter("@ItemType",(int)item.SpecialType),
        //            new SqlParameter("@Font",item.FontName),
        //            new SqlParameter("@PointSize", item.Font.Size),
        //            new SqlParameter("@Colour",item.Colour),
        //        };
        //        ReturnValue = DBHelper.ExecuteStoredProcReturnID("ReportTextItemInsert", parameters, databaseName);
        //        item.Id = ReturnValue;
        //        return ReturnValue;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error inserting a new line item: {0}", ex.ToString());
        //        return -1;
        //    }
        //}

    }
}
