﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using PDFReport;

namespace DGS.ArbPro.DAL
{
    public class ImageItem : Contracts.DBContextBase
    {
        #region Constructors
        public ImageItem(string databaseName) : base(databaseName, "ImageItem", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Methods
        #region Public
        public int Save(PDFReport.ImageItem obj)
        {
            int retVal = -1;
            string spName = "ReportImageItemInsert";
            ParameterMode mode = ParameterMode.Insert;

            if (obj.Id > 0)
            {
                spName = "ReportImageItemUpdate";
                mode = ParameterMode.Update;
            }

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(spName, Parameters(obj, mode), this.DatabaseName);
            }
            catch (Exception ex)
            {

            }
            return retVal;
        }

        public List<PDFReport.ImageItem> GetForGroup(int groupId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ReportImageItemsForGroupGet", new SqlParameter[] { new SqlParameter("@GroupId", groupId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {

            }
            return ToObjectList(ds);
        }
        #endregion
        #region Private
        private SqlParameter[] Parameters(PDFReport.ImageItem obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
                retVal.Add(new SqlParameter("@Id", obj.Id));

            retVal.Add(new SqlParameter("@GroupId", obj.GroupId));
            retVal.Add(new SqlParameter("@Name", obj.Name));
            retVal.Add(Parameterize("@ImageArray",obj.GetImageBytes()));
            retVal.Add(new SqlParameter("@FilePath", obj.ImagePath));
            retVal.Add(new SqlParameter("@XPos", obj.X));
            retVal.Add(new SqlParameter("@YPos", obj.Y));
            retVal.Add(new SqlParameter("@Width", obj.Width));
            retVal.Add(new SqlParameter("@Height", obj.Height));

            return retVal.ToArray();
        }

        private List<PDFReport.ImageItem> ToObjectList(DataSet ds)
        {
            List<PDFReport.ImageItem> retVal = new List<PDFReport.ImageItem>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {

            }
            return retVal;
        }

        private PDFReport.ImageItem CreateInstance(DataRow row)
        {
            PDFReport.ImageItem obj = new PDFReport.ImageItem(double.Parse(row["XPos"].ToString()),
                                                                double.Parse(row["YPos"].ToString()),
                                                                double.Parse(row["Width"].ToString()),
                                                                double.Parse(row["Height"].ToString()),
                                                                row["Name"].ToString());
            obj.Id = (int)row["Id"];
            obj.GroupId = (int)row["GroupId"];
            obj.SetImage(ByteValue(row, "Image"));
            return obj;
        }
        #endregion
        #endregion
        //public static List<ImageItem> GetImageItemsForGroup(string databaseName,int id)
        //{
        //    List<ImageItem> retVal = new List<ImageItem>();
        //    DataSet ds;
        //    ImageItem item = null;
        //    ds = DBHelper.ExecuteStoredProcDataSet("ReportImageItemsForGroupGet", new SqlParameter[] { new SqlParameter("@GroupId", id) }, databaseName);
        //    foreach (DataRow Row in ds.Tables[0].Rows)
        //    {
        //        item = new ImageItem(double.Parse(Row["XPos"].ToString()), double.Parse(Row["YPos"].ToString()), double.Parse(Row["Width"].ToString()), double.Parse(Row["Height"].ToString()), Row["Name"].ToString());
        //        item.SetImage((Byte[])Row["Image"]);
        //        retVal.Add(item);
        //    }
        //    return retVal;
        //}

        //public static int InsertImageItemForGroup(string databaseName, int groupID, ImageItem item)
        //{
        //    try
        //    {
        //        int ReturnValue;
        //        SqlParameter[] parameters = new SqlParameter[]
        //        {
                    
        //        };
        //        ReturnValue = DBHelper.ExecuteStoredProcReturnID("ReportImageItemInsert", parameters, databaseName);
        //        item.Id = ReturnValue;
        //        return ReturnValue;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error inserting a new line item: {0}", ex.ToString());
        //        return -1;
        //    }
        //}
    }
}
