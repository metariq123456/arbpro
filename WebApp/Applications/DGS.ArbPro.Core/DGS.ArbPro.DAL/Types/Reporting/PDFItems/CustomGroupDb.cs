﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using PDFReport;

namespace DGS.ArbPro.DAL
{
    public class CustomGroup :Contracts.DBContextBase
    {
        #region Constructors
        public CustomGroup(string databaseName) : base(databaseName, "CustomGroup") { }
        #endregion

        #region Methods
        #region Public
        public int Save(PDFReport.CustomGroup obj)
        {
            int retVal = -1;
            string spName = "ReportCustomGroupInsert";
            ParameterMode mode = ParameterMode.Insert;

            if (obj.Id > 0)
            {
                spName = "ReportCustomGroupUpdate";
                mode = ParameterMode.Update;
            }

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(spName, Parameters(obj, mode), this.DatabaseName);
            }
            catch (Exception ex)
            {

            }
            return retVal;
        }

        public bool ClearImage(int Id)
        {
            DBHelper.ExecuteStoredProcDataSet("ReportImageItemClear", new SqlParameter[] { new SqlParameter("@GroupId", Id) }, this.DatabaseName);

            return true;
        }

        public int SaveAll(PDFReport.CustomGroup obj)
        {
            int retVal = -1;
            string spName = "ReportCustomGroupInsert";
            ParameterMode mode = ParameterMode.Insert;

            if (obj.Id > 0)
            {
                spName = "ReportCustomGroupUpdate";
                mode = ParameterMode.Update;
            }

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(spName, Parameters(obj, mode), this.DatabaseName);
                if (retVal > 0)
                {
                    DAL.LineItem lineContext = new DAL.LineItem(this.DatabaseName);
                    DAL.ImageItem imageContext = new DAL.ImageItem(this.DatabaseName);
                    DAL.TextItem textContext = new DAL.TextItem(this.DatabaseName);
                    //The parent object has been saved correctly now save all of it's items
                    foreach (Item itm in obj.GetAllItems())
                    {
                        itm.GroupId = retVal;
                        if (itm.GetType() == typeof(PDFReport.LineItem))
                        {
                            lineContext.Save((PDFReport.LineItem)itm);
                        }
                        else if (itm.GetType() == typeof(PDFReport.ImageItem))
                        {
                            imageContext.Save((PDFReport.ImageItem)itm);
                        }
                        else if (itm.GetType() == typeof(PDFReport.TextItem))
                        {
                            textContext.Save((PDFReport.TextItem)itm);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return retVal;
        }

        public List<PDFReport.CustomGroup> GetList()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ReportCustomGroupsGet",new SqlParameter[] {},this.DatabaseName);
            }
            catch (Exception ex)
            {

            }
            return ToObjectList(ds);
        }

        public PDFReport.CustomGroup GetforName(string groupName)
        {

            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ReportCustomGroupsGetForName", new SqlParameter[] { new SqlParameter("@Name",groupName)}, this.DatabaseName);
            }
            catch (Exception ex)
            {

            }
            return ToObject(ds);
        }
        #endregion
        #region Internal
        internal static List<PDFReport.CustomGroup> GetList(string dataBaseName)
        {
            DAL.CustomGroup dbContext = new DAL.CustomGroup(dataBaseName);
            return dbContext.GetList();
        }
        #endregion
        #region Private
        private SqlParameter[] Parameters(PDFReport.CustomGroup obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();

            //if (mode == ParameterMode.Update)
                retVal.Add(new SqlParameter("@Id", obj.Id));

            retVal.Add(new SqlParameter("@Name", obj.Name));
            retVal.Add(new SqlParameter("@Height", obj.Height));
            retVal.Add(new SqlParameter("@BottomPadding", obj.BottomPadding));
            retVal.Add(new SqlParameter("@RelativeTo", (int)obj.RelativeTo));

            return retVal.ToArray();
        }

        private PDFReport.CustomGroup ToObject(DataSet ds)
        {
            PDFReport.CustomGroup retVal = new PDFReport.CustomGroup("Error", 0.0);

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            DAL.ImageItem imageContext = new DAL.ImageItem(this.DatabaseName);
            DAL.LineItem lineContext = new DAL.LineItem(this.DatabaseName);
            DAL.TextItem textContext = new DAL.TextItem(this.DatabaseName);
            try
            {
                List<PDFReport.Item> items = new List<Item>();
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
                //Get all the items for the group
                items.AddRange(imageContext.GetForGroup(retVal.Id));
                items.AddRange(lineContext.GetForGroup(retVal.Id));
                items.AddRange(textContext.GetForGroup(retVal.Id));
                //Add all the items to the group
                foreach (PDFReport.Item itm in items)
                {
                    retVal.AddItem(itm);
                }
            }
            catch (Exception ex)
            {

            }
            return retVal;
        }

        private List<PDFReport.CustomGroup> ToObjectList(DataSet ds)
        {
            List<PDFReport.CustomGroup> retVal = new List<PDFReport.CustomGroup>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            DAL.ImageItem imageContext = new DAL.ImageItem(this.DatabaseName);
            DAL.LineItem lineContext = new DAL.LineItem(this.DatabaseName);
            DAL.TextItem textContext = new DAL.TextItem(this.DatabaseName);
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    // ensure the list items are cleared out for each iteration of 'r'
                    List<PDFReport.Item> items = new List<Item>();
                    var g = CreateInstance(r);

                    //Get all the items for the group
                    items.AddRange(imageContext.GetForGroup(g.Id));
                    items.AddRange(lineContext.GetForGroup(g.Id));
                    items.AddRange(textContext.GetForGroup(g.Id));
                    //Add all the items to the group
                    foreach (PDFReport.Item itm in items)
                    {
                        g.AddItem(itm);
                    }

                    retVal.Add(g);
                }
            }
            catch (Exception ex)
            {

            }
            return retVal;
        }

        private PDFReport.CustomGroup CreateInstance(DataRow row)
        {
            PDFReport.CustomGroup obj = new PDFReport.CustomGroup(row["Name"].ToString(), double.Parse(row["Height"].ToString()));
            obj.Id = (int)row["ID"];
            obj.RelativeTo = (PrintRelativeTo)int.Parse(row["RelativeTo"].ToString());
            obj.BottomPadding = double.Parse(row["BottomPadding"].ToString());
            return obj;
        }
        #endregion
        #endregion
        //public static List<CustomGroup> GetCustomGroupsForClient(string databaseName)
        //{
            
        //    DataSet ds;
        //    List<CustomGroup> returnValue = new List<CustomGroup>();
        //    List<Item> items = new List<Item>();

        //    CustomGroup g = null;
        //    ds = DBHelper.ExecuteStoredProcDataSet("ReportCustomGroupsGet", new SqlParameter[] { }, databaseName);
        //    foreach (DataRow Row in ds.Tables[0].Rows)
        //    {
        //        items = new List<Item>();
        //        g = new CustomGroup(Row["Name"].ToString(), double.Parse(Row["Height"].ToString()));
        //        //Set the remaining group properties
        //        g.Id = (int)Row["ID"];
        //        g.RelativeTo = (PrintRelativeTo)int.Parse(Row["RelativeTo"].ToString());
        //        g.BottomPadding = double.Parse(Row["BottomPadding"].ToString());
        //        //Get the group items from the database
        //        items.AddRange(TextItemDB.GetTextItemsForGroup(databaseName, g.Id));
        //        items.AddRange(LineItemDB.GetLineItemsForGroup(databaseName, g.Id));
        //        items.AddRange(ImageItemDB.GetImageItemsForGroup(databaseName, g.Id));
        //        foreach (Item itm in items)
        //        {
        //            g.AddItem(itm);
        //        }
        //        returnValue.Add(g);
        //    }
        //    return returnValue;
        //}

        //public static int InsertOrUpdateCustomGroup(string databaseName, CustomGroup group)
        //{
        //    string proc = "";
        //    if (group.Id < 1)
        //        proc = "ReportCustomGroupInsert";
        //    else
        //        proc = "ReportCustomGroupUpdate"; ;

        //    try
        //    {
        //        int ReturnValue;

        //        SqlParameter[] parameters = new SqlParameter[]
        //        {
        //            new SqlParameter("@ID", group.Id),
        //            new SqlParameter("@Name", group.Name), 
        //            new SqlParameter("@Height",group.Height),
        //            new SqlParameter("@BottomPadding", group.BottomPadding),
        //            new SqlParameter("@RelativeTo", (int)group.RelativeTo)
        //        };
        //        ReturnValue = DBHelper.ExecuteStoredProcReturnID(proc, parameters, databaseName);

        //        if (ReturnValue > 0)
        //            group.Id = ReturnValue;
        //        else
        //            throw new Exception("Failed to recieve a valid Id for a new custom group.");

        //        foreach (Item itm in group.GetAllItems())
        //        {
        //            if (itm.GetType() == typeof(LineItem))
        //                LineItemDB.InsertLineItemForGroup(databaseName, group.Id, (LineItem)itm);
        //            else if (itm.GetType() == typeof(ImageItem))
        //                ImageItemDB.InsertImageItemForGroup(databaseName, group.Id, (ImageItem)itm);
        //            else
        //                TextItemDB.InsertTextItemForGroup(databaseName, group.Id, (TextItem)itm);
        //        }

        //        return ReturnValue;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error inserting a new custom group: {0}", ex.ToString());
        //        return -1;
        //    }
        //}
    }
}
