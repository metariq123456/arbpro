﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using PDFReport;

namespace DGS.ArbPro.DAL
{
    public class LineItem: Contracts.DBContextBase
    {
        #region Constructors
        public LineItem(string databaseName) : base(databaseName, "LineItem") { }
        #endregion

        #region Methods
        #region Public
        public int Save(PDFReport.LineItem obj)
        {
            int retVal = -1;
            string spName = "ReportLineItemInsert";
            ParameterMode mode = ParameterMode.Insert;

            if (obj.Id > 0)
            {
                spName = "ReportLineItemUpdate";
                mode = ParameterMode.Update;
            }

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(spName, Parameters(obj, mode), this.DatabaseName);
            }
            catch (Exception ex)
            {

            }
            return retVal;
        }

        public List<PDFReport.LineItem> GetForGroup(int groupId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ReportLineItemsForGroupGet", new SqlParameter[] { new SqlParameter("@GroupId", groupId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {

            }
            return ToObjectList(ds);
        }
        #endregion
        #region Private
        private SqlParameter[] Parameters(PDFReport.LineItem obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
                retVal.Add(new SqlParameter("@Id", obj.Id));

            retVal.Add(new SqlParameter("@GroupId", obj.GroupId));
            retVal.Add(new SqlParameter("@Name", obj.Name));
            retVal.Add(new SqlParameter("@HorizontalPos", (int)obj.RelativeHorizontalPos));
            retVal.Add(new SqlParameter("@VerticalPos", (int)obj.RelativeVerticalPos));
            retVal.Add(new SqlParameter("@XPos", obj.X));
            retVal.Add(new SqlParameter("@YPos", obj.Y));
            retVal.Add(new SqlParameter("@Width", obj.Width));
            retVal.Add(new SqlParameter("@Height", obj.Height));
            retVal.Add(new SqlParameter("@BrushSize", obj.PenSize));
            retVal.Add(new SqlParameter("@Colour", obj.Colour));

            return retVal.ToArray();
        }

        private List<PDFReport.LineItem> ToObjectList(DataSet ds)
        {
            List<PDFReport.LineItem> retVal = new List<PDFReport.LineItem>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {

            }
            return retVal;
        }

        private PDFReport.LineItem CreateInstance(DataRow row)
        {
            PDFReport.LineItem obj = new PDFReport.LineItem(double.Parse(row["BrushSize"].ToString()),
                                                            double.Parse(row["XPos"].ToString()),
                                                            double.Parse(row["YPos"].ToString()),
                                                            double.Parse(row["Width"].ToString()),
                                                            double.Parse(row["Height"].ToString()),
                                                            row["Name"].ToString());
                
            obj.Id = (int)row["Id"];
            obj.GroupId = (int)row["GroupId"];
            obj.Colour = row["Colour"].ToString();
            return obj;
        }
        #endregion
        #endregion
        //public static List<LineItem> GetLineItemsForGroup(string databaseName, int groupID)
        //{
        //    List<LineItem> retVal = new List<LineItem>();
        //    DataSet ds;
        //    LineItem item = null;
        //    ds = DBHelper.ExecuteStoredProcDataSet("ReportLineItemsForGroupGet", new SqlParameter[] { new SqlParameter("@GroupId", groupID) }, databaseName);
        //    foreach (DataRow Row in ds.Tables[0].Rows)
        //    {
        //        item = new LineItem(double.Parse(Row["BrushSize"].ToString()), double.Parse(Row["XPos"].ToString()), double.Parse(Row["YPos"].ToString()), double.Parse(Row["Width"].ToString()), double.Parse(Row["Height"].ToString()), Row["Name"].ToString());
        //        item.Colour = Row["Colour"].ToString();
        //        retVal.Add(item);
        //    }
        //    return retVal;
        //}

        //public static int InsertLineItemForGroup(string databaseName, int groupID, LineItem item)
        //{
        //    try
        //    {
        //        int ReturnValue;

        //        SqlParameter[] parameters = new SqlParameter[]
        //        {
        //            new SqlParameter("@GroupId", groupID),
        //            new SqlParameter("@Name", item.Name), 
        //            new SqlParameter("@HorizontalPos",(int)item.RelativeHorizontalPos),
        //            new SqlParameter("@VerticalPos",(int)item.RelativeVerticalPos),
        //            new SqlParameter("@XPos", item.X),
        //            new SqlParameter("@YPos", item.Y),
        //            new SqlParameter("@Width", item.Width),
        //            new SqlParameter("@Height",item.Height),
        //            new SqlParameter("@BrushSize",item.PenSize),
        //            new SqlParameter("@Colour",item.Colour),
        //        };
        //        ReturnValue = DBHelper.ExecuteStoredProcReturnID("ReportLineItemInsert", parameters, databaseName);
        //        item.Id = ReturnValue;
        //        return ReturnValue;
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine("Error inserting a new line item: {0}", ex.ToString());
        //        return -1;
        //    }
        //}
    }
}
