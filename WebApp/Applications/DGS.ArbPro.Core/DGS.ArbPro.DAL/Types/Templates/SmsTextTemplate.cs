﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    /// <summary>
    /// Class solely for SMS templates. used in invoice and quote generation
    /// </summary>
    public class SmsTextTemplate : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for the class 
        /// </summary>
        /// <param name="databaseName">The database name</param>
        public SmsTextTemplate(string databaseName)
            : base(databaseName, "SmsTextTemplate", DateTime.Parse("1970-01-01"))
        {
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public

        /// <summary>
        /// Method to get the existing templates for a quote and invoice
        /// </summary>
        /// <param name="typeFilter">0 = Quote or 1 = Invoice</param>
        /// <param name="quoteId">Quotes and invoices have a quote id. we use that to get the correct templates of the correct type</param>
        /// <returns>Passes the stored procedure results onto PopulateResultSet</returns>
        public List<Types.SmsTextTemplate> GetList(int typeFilter, int quoteId)
        {
            DataSet ds = null;
            try
            {
                SqlParameter[] Params = new SqlParameter[] 
                {
                    new SqlParameter("@TypeFilter", typeFilter),
                    new SqlParameter("@QuoteId", quoteId)
                };

                ds = DBHelper.ExecuteStoredProcDataSet("SmsTextTemplate_GetList", Params, base.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("PopulateResultSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in SmsTextTemplate.GetList: {0}", ex.ToString());
            }

            return PopulateResultSet(ds);
        }


        #endregion
        #region Private

        /// <summary>
        /// Gets the results from the stored procedure executed in GetList
        /// and passes them on to create an object
        /// </summary>
        /// <param name="ds">the result from the stored procedure</param>
        /// <returns>Populated objects with existing templates of the correct type</returns>
        private List<Types.SmsTextTemplate> PopulateResultSet(DataSet ds)
        {
            List<Types.SmsTextTemplate> retVal = new List<Types.SmsTextTemplate>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in SmsTextTemplate.PopulateResultSet: {0}", ex.ToString());
            }

            return retVal;
        }

        /// <summary>
        /// Creates a new object and populates it
        /// through the results from PopulateResultSet
        /// </summary>
        /// <param name="row">data rows passed through PopulateResultSet</param>
        /// <returns>A single populated SMS template of the correct type</returns>
        private Types.SmsTextTemplate CreateObject(DataRow row)
        {
            Types.SmsTextTemplate obj = new Types.SmsTextTemplate();
            obj.Id = base.IntValue(row, "Id");
            obj.BodyText = base.StringValue(row, "BodyText");
            obj.TemplateName = base.StringValue(row, "TemplateName");
            obj.MessageType = (MessageTypeEnum)base.IntValue(row, "MessageType");
            obj.ObjectType = (ReportTextTemplateTypeEnum)base.IntValue(row, "ObjectType");
            obj.AdditionalText = base.StringValue(row, "AdditionalText");
            obj.Salutation = base.StringValue(row, "Salutation");
            return obj;
        }
        #endregion
        #endregion
    }
}
