﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class TextTemplate: Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public TextTemplate(string databaseName)
            : base(databaseName, "TextTemplate", DateTime.Parse("1970-01-01"))
        {
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public List<Types.TextTemplate> GetList(int TypeFilter = -1, int MessageFilter = -1, int IDFilter = -1)
        {
            DataSet ds = null;
            try
            {
                SqlParameter[] Params = new SqlParameter[] 
                {
                    new SqlParameter("@TypeFilter", TypeFilter),
                    new SqlParameter("@MessageFilter", MessageFilter),
                    new SqlParameter("@IDFilter", IDFilter)
                };

                ds = DBHelper.ExecuteStoredProcDataSet("TextTemplate_GetList", Params, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("PopulateResultSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in TextTemplate.GetList: {0}", ex.ToString());
            }

            return PopulateResultSet(ds);
        }

        /// <summary>
        /// The get marketing list.
        /// </summary>
        /// <param name="typeFilter"> The type filter. </param>
        /// <param name="messageFilter"> The message filter. </param>
        /// <returns> List of text templates. </returns>
        public List<Types.TextTemplate> GetMarketingList(int typeFilter = -1, int messageFilter = -1)
        {
            DataSet ds = null;
            try
            {
                SqlParameter[] Params = new SqlParameter[] 
                {
                    new SqlParameter("@TypeFilter", typeFilter),
                    new SqlParameter("@MessageFilter", messageFilter)
                };

                ds = DBHelper.ExecuteStoredProcDataSet("MarketingTextTemplate_GetList", Params, this.DatabaseName);
            }
            catch (Exception ex)
            {
                if (ds != null)
                {
                    this.LogError("PopulateResultSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                }

                Console.WriteLine("Error in TextTemplate.GetList: {0}", ex.ToString());
            }

            return this.PopulateResultSet(ds);
        }

        public int Save(Types.TextTemplate Object)
        {
            int ReturnValue = -1;
            try
            {
                SqlParameter[] Params = new SqlParameter[] {
                new SqlParameter("ID", Object.Id),
                new SqlParameter("TemplateName", Object.TemplateName),
                new SqlParameter("ObjectType", (Int32)Object.ObjectType),
                new SqlParameter("MessageType", (Int32)Object.MessageType),
                new SqlParameter("BodyText", Object.BodyText),
                new SqlParameter("AdditionalText", Object.AdditionalText),
                // DGSAP-864
                new SqlParameter("Salutation", Object.Salutation) 
            };

                ReturnValue = DBHelper.ExecuteStoredProcReturnID("TextTemplate_InsertOrUpdate", Params, base.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("PopulateResultSet", string.Format("Types.TextTemplate Object: {0}", Object.ToString()), ex);
                Console.WriteLine("Error in TextTemplate.Save: {0}", ex.ToString());
            }
            return ReturnValue;
        }

        public bool Delete(Types.TextTemplate obj)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("TextTemplate_Delete", CommandType.StoredProcedure, new SqlParameter[] { new SqlParameter("@Id", obj.Id) }, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Delete", string.Format("Int Id: {0}", obj.Id), ex);
                Console.WriteLine("Error in TextTemplate.Delete: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Private
        private List<Types.TextTemplate> PopulateResultSet(DataSet ds)
        {
            List<Types.TextTemplate> retVal = new List<Types.TextTemplate>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in TextTemplate.PopulateResultSet: {0}", ex.ToString());
            }

            return retVal;
        }

        private Types.TextTemplate CreateObject(DataRow row)
        {
            Types.TextTemplate obj = new Types.TextTemplate();
            obj.Id = base.IntValue(row, "Id");
            obj.AdditionalText = base.StringValue(row, "AdditionalText");
            obj.BodyText = base.StringValue(row, "BodyText");
            obj.TemplateName = base.StringValue(row, "TemplateName");
            obj.MessageType = (MessageTypeEnum)base.IntValue(row, "MessageType");
            obj.ObjectType = (ReportTextTemplateTypeEnum)base.IntValue(row, "ObjectType");
            // DGSAP-864
            obj.Salutation = base.StringValue(row, "Salutation");
            return obj;
        }
        #endregion
        #endregion
    }
}
