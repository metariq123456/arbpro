﻿namespace DGS.ArbPro.DAL.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// The risk criterion.
    /// </summary>
    public class RiskCriterion : Contracts.DBContextBase
    {
        #region public methods

        /// <summary>
        /// Initializes a new instance of the <see cref="RiskCriterion"/> class.
        /// </summary>
        /// <param name="database">
        /// The database.
        /// </param>
        public RiskCriterion(string database)
            : base(database, "RiskTemplate", DateTime.Parse("1970-01-01"))
        {
        }

        /// <summary>
        /// Gets all risk criterion template objects
        /// </summary>
        /// <returns>List of risk criterion template objects</returns>
        public List<Types.Templates.RiskCriterion> GetAllCriterionTemplates()
        {
            List<Types.Templates.RiskCriterion> retVal = null;
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("Template_RiskCriterion_GetAll", this.DatabaseName);
                retVal = this.PopulateFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("Get", string.Format("Error in GetAllCriterionTemplates "), ex);
                Console.WriteLine("Error in GetAllCriterionTemplates: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Gets the criteria for a section based on section id.
        /// </summary>
        /// <param name="id">
        /// The section id.
        /// </param>
        /// <returns>
        /// The list of criteria
        /// </returns>
        public List<Types.Templates.RiskCriterion> GetForSectionId(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Template_RiskCriterion_GetForSection", new SqlParameter[] { new SqlParameter("@TemplateId", id) }, this.DatabaseName); 
            }
            catch (Exception ex)
            {
                this.LogError("Get", string.Format("Error in GetForSectionId "), ex);
                Console.WriteLine("Error in GetForSectionId: {0}", ex);
            }

            return this.PopulateFromDataSet(ds);
        }

        /// <summary>
        /// Gets the criteria for a section based on section id.
        /// </summary>
        /// <param name="id"> The risk id. </param>
        /// <param name="sectionId"> The section id. </param>
        /// <returns>
        /// The list of criteria
        /// </returns>
        public List<Types.Templates.RiskCriterion> GetForRiskId(int id, int sectionId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Template_RiskCriterion_GetForRisk", new SqlParameter[] { new SqlParameter("@TemplateId", id), new SqlParameter("@SectionId", sectionId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("Get", string.Format("Error in GetForSectionId "), ex);
                Console.WriteLine("Error in GetForSectionId: {0}", ex);
            }

            return this.PopulateFromDataSet(ds);
        }

        /// <summary>
        /// Gets the criteria for a section based on section id.
        /// </summary>
        /// <param name="id">
        /// The section id.
        /// </param>
        /// <returns>
        /// The list of criteria
        /// </returns>
        public Types.Templates.RiskCriterion GetForId(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Template_RiskCriterion_GetForCriterionID", new SqlParameter[] { new SqlParameter("@TemplateId", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("Get", string.Format("Error in GetForSectionId "), ex);
                Console.WriteLine("Error in GetForSectionId: {0}", ex);
            }

            return this.PopulateSingleFromDataSet(ds);
        }

        /// <summary>
        /// Inserts or updates a risk criterion
        /// </summary>
        /// <param name="obj">RiskCriterion template object</param>
        /// <returns>Inserted or updated ID</returns>
        public int Save(Types.Templates.RiskCriterion obj)
        {
            if (obj.Id <= 0)
            {
                return this.Insert(obj);
            }
            else
            {
                return this.Update(obj);
            }
        }

        #endregion
        #region private methods

        /// <summary>
        /// Method to insert a record into the database
        /// </summary>
        /// <param name="riskCriterion">RiskCriterion template object</param>
        /// <returns>The record Id of the inserted object</returns>
        private int Insert(Types.Templates.RiskCriterion riskCriterion)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Template_RiskCriteria_Insert", this.Parameters(riskCriterion, ParameterMode.Insert), this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("Insert", string.Format("Types.RiskCriterion risk: {0}", riskCriterion), ex);
                Console.WriteLine("Error in RiskCriterion.Insert: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Method to update a record in the database
        /// </summary>
        /// <param name="riskCriterion">RiskCriterion template object</param>
        /// <returns>The record Id of the updated object</returns>
        private int Update(Types.Templates.RiskCriterion riskCriterion)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Template_RiskCriteria_Update", this.Parameters(riskCriterion, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("Update", string.Format("Types.RiskCriterion risk: {0}", riskCriterion), ex);
                Console.WriteLine("Error in RiskCriterion.Update: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Adds the parameters to an array to pass to the stored procedure
        /// </summary>
        /// <param name="obj">RiskCriterion template object</param>
        /// <param name="mode">Insert or update</param>
        /// <returns>Array of parameters</returns>
        private SqlParameter[] Parameters(Types.Templates.RiskCriterion obj, ParameterMode mode)
        {
            var pars = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                pars.Add(Parameterize("@Id", obj.Id));
                pars.Add(Parameterize("@IsDeleted", obj.IsDeleted));
            }

            pars.Add(Parameterize("@Criterion", obj.Criterion));
            pars.Add(Parameterize("@ChapterRef", obj.ChapterRef));
            pars.Add(Parameterize("@AssessmentPhase", obj.AssessmentPhase));

            return pars.ToArray();
        }

        /// <summary>
        /// Passes from data set from the stored procedure to be processed.
        /// </summary>
        /// <param name="ds">
        /// The data set.
        /// </param>
        /// <returns>
        /// List of risk criterion objects
        /// </returns>
        private List<Types.Templates.RiskCriterion> PopulateFromDataSet(DataSet ds)
        {
            List<Types.Templates.RiskCriterion> retVal = new List<Types.Templates.RiskCriterion>();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                retVal.Add(this.CreateObject(r));
            }

            return retVal;
        }

        /// <summary>
        /// Passes from data set from the stored procedure to be processed.
        /// </summary>
        /// <param name="ds">
        /// The data set.
        /// </param>
        /// <returns>
        /// Single risk criterion object
        /// </returns>
        private Types.Templates.RiskCriterion PopulateSingleFromDataSet(DataSet ds)
        {
            Types.Templates.RiskCriterion retVal = new Types.Templates.RiskCriterion();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                retVal = this.CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                this.LogError("PopulateSingleResultFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in Client.PopulateSingleResultFromDataSet: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Adds the results from the stored procedure to an object.
        /// </summary>
        /// <param name="r">
        /// A data row from the data set.
        /// </param>
        /// <returns>
        /// A risk criterion object.
        /// </returns>
        private Types.Templates.RiskCriterion CreateObject(DataRow r)
        {
            Types.Templates.RiskCriterion obj = new Types.Templates.RiskCriterion();
            obj.Id = (int)r["Id"];
            obj.Criterion = (string)r["Criterion"];
            obj.ChapterRef = (string)r["ChapterRef"];
            obj.AssessmentPhase = (int)r["AssessmentPhase"];
            obj.IsDeleted = (bool)r["IsDeleted"];
            return obj;
        }

        #endregion
    }
}