﻿namespace DGS.ArbPro.DAL.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    using DGS.ArbPro.Types.Enums;

    /// <summary>
    /// The risk.
    /// </summary>
    public class Risk : Contracts.DBContextBase
    {
        #region public methods

        /// <summary>
        /// Initializes a new instance of the <see cref="Risk"/> class. 
        /// </summary>
        /// <param name="database">
        /// Database name
        /// </param>
        public Risk(string database)
            : base(database, "RiskTemplate", DateTime.Parse("1970-01-01"))
        {
        }

        /// <summary>
        /// Gets all risk template objects
        /// </summary>
        /// <returns>List of risk template objects</returns>
        public List<Types.Templates.Risk> GetAllTemplates()
        {
            List<Types.Templates.Risk> retVal = null;
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("Template_Risk_GetAll", this.DatabaseName);
                retVal = this.PopulateResultFromDataSet(ds);
                RiskSection sectionContext = new RiskSection(this.DatabaseName);
                foreach (var risk in retVal)
                {
                    risk.TemplateSections = sectionContext.GetSectionsForTemplate(risk.Id);
                }
            }
            catch (Exception ex)
            {
                this.LogError("Get", string.Format("Error in GetAllTemplate "), ex);
                Console.WriteLine("Error in GetAllTemplate: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Gets a risk template based on an ID
        /// </summary>
        /// <param name="id">Risk template ID</param>
        /// <returns>Risk template object</returns>
        public Types.Templates.Risk GetTemplate(int id)
        {
            Types.Templates.Risk retVal = null;
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("Template_Risk_GetForID", new SqlParameter[] { new SqlParameter("@TemplateId", id) }, this.DatabaseName);
                retVal = this.PopulateSingleResultFromDataSet(ds);
                RiskSection sectionContext = new RiskSection(this.DatabaseName);
                retVal.TemplateSections = sectionContext.GetSectionsForTemplate(retVal.Id);
            }
            catch (Exception ex)
            {
                this.LogError("Get", string.Format("Error in GetTemplate "), ex);
                Console.WriteLine("Error in GetTemplate: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Gets a risk template based on an appointment ID
        /// </summary>
        /// <param name="appointmentId">The appointment ID</param>
        /// <returns>Risk template object</returns>
        public Types.Templates.Risk GetForAppointmentId(int appointmentId)
        {
            Types.Templates.Risk retVal = null;
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("TemplateRisk_GetForAppointment", new SqlParameter[] { new SqlParameter("@AppointmentId", appointmentId) }, this.DatabaseName);
                retVal = this.PopulateSingleResultFromDataSet(ds);
                RiskSection sectionContext = new RiskSection(this.DatabaseName);
                retVal.TemplateSections = sectionContext.GetSectionsForTemplate(retVal.Id);
            }
            catch (Exception ex)
            {
                this.LogError("GetForAppointmentId", string.Format("int appointmentId: {0}", appointmentId), ex);
                Console.WriteLine("Error in Risk.GetForAppointmentId: {0}", ex);
            }
            return retVal;
        }

        /// <summary>
        /// Inserts or updates a risk template
        /// </summary>
        /// <param name="obj">Risk template object</param>
        /// <returns>Inserted or updated ID</returns>
        public int Save(Types.Templates.Risk obj)
        {
            if (obj.Id <= 0)
            {
                return this.Insert(obj);
            }
            else
            {
                return this.Update(obj);
            }
        }

        /// <summary>
        /// Inserts an entry in the template risk section mapping table
        /// </summary>
        /// <param name="riskId">The risk ID</param>
        /// <param name="sectionId">The section ID</param>
        /// <param name="criterionId">The criterion ID</param>
        /// <returns>Inserted id or -1 if entry already exists</returns>
        public int InsertTemplateRiskSectionMapping(int riskId, int sectionId, int criterionId)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Template_RiskSectionMapping_Insert", new SqlParameter[] { new SqlParameter("@RiskTemplateId", riskId), new SqlParameter("@SectionTemplateId", sectionId), new SqlParameter("@CriterionTemplateId", criterionId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("InsertTemplateRiskSectionMapping", string.Format("Section Id: {0}, Criterion Id: {1}", sectionId, criterionId), ex);
                Console.WriteLine("Error in InsertTemplateRiskSectionMapping: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Removes an entry in the template risk section mapping table
        /// </summary>
        /// <param name="riskId">The risk ID</param>
        /// <param name="sectionId">The section ID</param>
        /// <param name="criterionId">The criterion ID</param>
        /// <returns>True or false based on success</returns>
        public bool RemoveTemplateRiskSectionMapping(int riskId, int sectionId, int criterionId)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteStoredProcDataSet("Template_RiskSectionMapping_Remove", new SqlParameter[] { new SqlParameter("@RiskTemplateId", riskId), new SqlParameter("@SectionTemplateId", sectionId), new SqlParameter("@CriterionTemplateId", criterionId) }, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                this.LogError("RemoveTemplateRiskSectionMapping", string.Format("Section Id: {0}, Criterion Id: {1}", sectionId, criterionId), ex);
                Console.WriteLine("Error in RemoveTemplateRiskSectionMapping: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Inserts an entry in the template risk mapping table
        /// </summary>
        /// <param name="sectionId">The section ID</param>
        /// <param name="riskId">The risk ID</param>
        /// <returns>Inserted id or -1 if entry already exists</returns>
        public int InsertTemplateRiskMapping(int sectionMappingId, int riskId)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Template_RiskMapping_Insert", new SqlParameter[] { new SqlParameter("@RiskTemplateId", riskId), new SqlParameter("@SectionMappingId", sectionMappingId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("InsertTemplateRiskSectionMapping", string.Format("Section Id: {0}, Risk Id: {1}", sectionMappingId, riskId), ex);
                Console.WriteLine("Error in InsertTemplateRiskSectionMapping: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Removes an entry in the template risk mapping table
        /// </summary>
        /// <param name="sectionId">The section ID</param>
        /// <param name="riskId">The criterion ID</param>
        /// <returns>True or false based on success</returns>
        public bool RemoveTemplateRiskMapping(int sectionId, int riskId)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteStoredProcDataSet("Template_RiskMapping_Remove", new SqlParameter[] { new SqlParameter("@SectionTemplateId", sectionId), new SqlParameter("@RiskTemplateId", riskId) }, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                this.LogError("RemoveTemplateRiskMapping", string.Format("Section Id: {0}, Risk Id: {1}", sectionId, riskId), ex);
                Console.WriteLine("Error in RemoveTemplateRiskMapping: {0}", ex);
            }

            return retVal;
        }

        #endregion
        #region private methods

        /// <summary>
        /// Method to insert a record into the database
        /// </summary>
        /// <param name="risk">Risk template object</param>
        /// <returns>The record Id of the inserted object</returns>
        private int Insert(Types.Templates.Risk risk)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Template_Risk_Insert", this.Parameters(risk, ParameterMode.Insert), this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("Insert", string.Format("Types.Risk risk: {0}", risk), ex);
                Console.WriteLine("Error in Risk.Insert: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Method to update a record in the database
        /// </summary>
        /// <param name="risk">Risk template object</param>
        /// <returns>The record Id of the updated object</returns>
        private int Update(Types.Templates.Risk risk)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Template_Risk_Update", this.Parameters(risk, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("Update", string.Format("Types.Risk risk: {0}", risk), ex);
                Console.WriteLine("Error in Risk.Update: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Adds the parameters to an array to pass to the stored procedure
        /// </summary>
        /// <param name="obj">Risk template object</param>
        /// <param name="mode">Insert or update</param>
        /// <returns>Array of parameters</returns>
        private SqlParameter[] Parameters(Types.Templates.Risk obj, ParameterMode mode)
        {
            var pars = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                pars.Add(Parameterize("@Id", obj.Id));
                pars.Add(Parameterize("@IsDeleted", obj.IsDeleted));
            }

            pars.Add(Parameterize("@Name", obj.Name));
            pars.Add(Parameterize("@ReferenceDoc", obj.ReferenceDoc));
            pars.Add(Parameterize("@Description", obj.Description));
            pars.Add(Parameterize("@TemplateType", (int)obj.TemplateType));

            return pars.ToArray();
        }

        /// <summary>
        /// Checks if the data set is valid and passes it on to create an object
        /// </summary>
        /// <param name="ds">The data set</param>
        /// <returns>Risk template object</returns>
        private Types.Templates.Risk PopulateSingleResultFromDataSet(DataSet ds)
        {
            Types.Templates.Risk retVal = new Types.Templates.Risk();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                retVal = this.CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                this.LogError("PopulateSingleResultFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in Client.PopulateSingleResultFromDataSet: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Checks if the data set is valid and passes it on to create an object
        /// </summary>
        /// <param name="ds">The data set</param>
        /// <returns>Risk template object</returns>
        private List<Types.Templates.Risk> PopulateResultFromDataSet(DataSet ds)
        {
            List<Types.Templates.Risk> retVal = new List<Types.Templates.Risk>();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(this.CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                this.LogError("PopulateSingleResultFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in Client.PopulateSingleResultFromDataSet: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Creates a risk template object
        /// </summary>
        /// <param name="r">Data row from stored procedure</param>
        /// <returns>Risk template object</returns>
        private Types.Templates.Risk CreateObject(DataRow r)
        {
            Types.Templates.Risk obj = new Types.Templates.Risk();
            obj.Id = (int)r["Id"];
            obj.Name = (string)r["Name"];
            obj.ReferenceDoc = (string)r["RefDoc"];
            obj.Description = (string)r["Dec"];
            obj.IsDeleted = (bool)r["IsDeleted"];
            obj.IsEditable = (bool)r["IsEditable"];
            obj.TemplateType = (RiskTemplateTypeEnum)((int)r["Type"]);
            return obj;
        }

        #endregion
    }
}
