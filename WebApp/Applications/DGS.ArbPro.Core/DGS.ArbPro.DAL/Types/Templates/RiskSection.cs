﻿namespace DGS.ArbPro.DAL.Templates
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// The risk section.
    /// </summary>
    public class RiskSection : Contracts.DBContextBase
    {
        #region public methods

        /// <summary>
        /// Initializes a new instance of the <see cref="RiskSection"/> class.
        /// </summary>
        /// <param name="database">
        /// The database.
        /// </param>
        public RiskSection(string database)
            : base(database, "RiskTemplate", DateTime.Parse("1970-01-01"))
        {
        }

        /// <summary>
        /// Gets all risk section template objects
        /// </summary>
        /// <returns>List of risk section template objects</returns>
        public List<Types.Templates.RiskSection> GetAllSectionTemplates()
        {
            List<Types.Templates.RiskSection> retVal = null;
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("Template_RiskSection_GetAll", this.DatabaseName);
                retVal = this.PopulateFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("Get", string.Format("Error in GetAllSectionTemplates "), ex);
                Console.WriteLine("Error in GetAllSectionTemplates: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Returns a list of sections for a given template.
        /// </summary>
        /// <param name="id">
        /// The id of the template to return the sections for.
        /// </param>
        /// <returns>
        /// A list of risk section objects
        /// </returns>
        public List<Types.Templates.RiskSection> GetSectionsForTemplate(int id)
        {
            List<Types.Templates.RiskSection> retVal = null;
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Template_RiskSection_GetForRiskID", new SqlParameter[] { new SqlParameter("@TemplateId", id) }, this.DatabaseName);
                retVal = this.PopulateFromDataSet(ds);

                RiskCriterion criteriaContext = new RiskCriterion(this.DatabaseName);
                foreach (var section in retVal)
                {
                    section.TemplateCriteria = criteriaContext.GetForRiskId(id, section.Id);
                }

            }
            catch (Exception ex)
            {
                this.LogError("Get", string.Format("Error in GetSectionsForTemplate "), ex);
                Console.WriteLine("Error in GetSectionsForTemplate: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Returns a single section based on it's id.
        /// </summary>
        /// <param name="riskId">The risk id. </param>
        /// <param name="sectionId">The section id. </param>
        /// <returns>A Risk Section</returns>
        public Types.Templates.RiskSection GetSectionForId(int riskId, int sectionId)
        {
            Types.Templates.RiskSection retVal = null;
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Template_RiskSection_GetForSectionID", new SqlParameter[] { new SqlParameter("@TemplateId", sectionId) }, this.DatabaseName);
                retVal = this.PopulateSingleFromDataSet(ds);

                if (riskId > 0)
                {
                    RiskCriterion criteriaContext = new RiskCriterion(this.DatabaseName);
                    retVal.TemplateCriteria = criteriaContext.GetForRiskId(riskId, sectionId);
                }
            }
            catch (Exception ex)
            {
                this.LogError("Get", string.Format("Error in GetSectionForId "), ex);
                Console.WriteLine("Error in GetSectionForId: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Update a document.
        /// </summary>
        /// <param name="section">
        /// The RiskSection object.
        /// </param>
        /// <returns>
        /// The updated id
        /// </returns>
        public int Update(Types.Templates.RiskSection section)
        {
            int id = -1;
            try
            {
                // update the risk assessment template section
                id = DBHelper.ExecuteStoredProcReturnID("Template_RiskSection_Update", new SqlParameter[] { new SqlParameter("@Id", section.Id), new SqlParameter("@Title", section.Title), new SqlParameter("@ReferenceDoc", section.ReferenceDoc), new SqlParameter("@ChapterRef", section.ChapterRef), new SqlParameter("@Description", section.Description) }, this.DatabaseName);

                // update the individual risk assessment template criterion for the sections
                foreach (var criteria in section.TemplateCriteria)
                {
                    id = DBHelper.ExecuteStoredProcReturnID("Template_RiskCriteria_Update", new SqlParameter[] { new SqlParameter("@Id", criteria.Id), new SqlParameter("@Criterion", criteria.Criterion), new SqlParameter("@ChapterRef", criteria.ChapterRef), new SqlParameter("@AssessmentPhase", criteria.AssessmentPhase) }, this.DatabaseName);
                }

                Utility.Logger.Log.InfoFormat("UpdateDocumentReferencesForSectionId: {0}", string.Format("section.Id: {0}", section.Id));
                Console.WriteLine("Updated risk section template in RiskSection.UpdateDocumentReferencesForSectionId");
            }
            catch (Exception ex)
            {
                this.LogError("UpdateDocumentReferencesForSectionId", string.Format("section.Id: {0}", section.Id), ex);
                Console.WriteLine("Error in RiskSection.UpdateDocumentReferencesForSectionId: {0}", ex);
            }

            return id;
        }

        /// <summary>
        /// Inserts or updates a risk section template
        /// </summary>
        /// <param name="obj">RiskSection template object</param>
        /// <returns>Inserted or updated ID</returns>
        public int Save(Types.Templates.RiskSection obj)
        {
            if (obj.Id <= 0)
            {
                return this.Insert(obj);
            }
            else
            {
                return this.UpdateSection(obj);
            }
        }

        #endregion
        #region private methods

        /// <summary>
        /// Method to insert a record into the database
        /// </summary>
        /// <param name="risk">RiskSection template object</param>
        /// <returns>The record Id of the inserted object</returns>
        private int Insert(Types.Templates.RiskSection risk)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Template_RiskSection_Insert", this.Parameters(risk, ParameterMode.Insert), this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("Insert", string.Format("Types.RiskSection risk: {0}", risk), ex);
                Console.WriteLine("Error in RiskSection.Insert: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Method to update a record in the database
        /// </summary>
        /// <param name="risk">RiskSection template object</param>
        /// <returns>The record Id of the updated object</returns>
        private int UpdateSection(Types.Templates.RiskSection risk)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Template_RiskSection_Update", this.Parameters(risk, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("Update", string.Format("Types.RiskSection risk: {0}", risk), ex);
                Console.WriteLine("Error in RiskSection.Update: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Adds the parameters to an array to pass to the stored procedure
        /// </summary>
        /// <param name="obj">RiskSection template object</param>
        /// <param name="mode">Insert or update</param>
        /// <returns>Array of parameters</returns>
        private SqlParameter[] Parameters(Types.Templates.RiskSection obj, ParameterMode mode)
        {
            var pars = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                pars.Add(Parameterize("@Id", obj.Id));
                pars.Add(Parameterize("@IsDeleted", obj.IsDeleted));
            }

            pars.Add(Parameterize("@Title", obj.Title));
            pars.Add(Parameterize("@ReferenceDoc", obj.ReferenceDoc));
            pars.Add(Parameterize("@ChapterRef", obj.ChapterRef));
            pars.Add(Parameterize("@Description", obj.Description));

            return pars.ToArray();
        }

        /// <summary>
        /// Processes the data set and returns a list of objects
        /// </summary>
        /// <param name="ds">
        /// The data set
        /// </param>
        /// <returns>
        /// A list of risk section objects
        /// </returns>
        private List<Types.Templates.RiskSection> PopulateFromDataSet(DataSet ds)
        {
            List<Types.Templates.RiskSection> retVal = new List<Types.Templates.RiskSection>();
            
            // Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                Types.Templates.RiskSection obj = this.CreateObject(r);
                retVal.Add(obj);
            }

            return retVal;
        }

        /// <summary>
        /// Processes the data set and returns a single object
        /// </summary>
        /// <param name="ds">
        /// The data set
        /// </param>
        /// <returns>
        /// A risk section object
        /// </returns>
        private Types.Templates.RiskSection PopulateSingleFromDataSet(DataSet ds)
        {
            Types.Templates.RiskSection retVal = new Types.Templates.RiskSection();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }
            
            try
            {
                retVal = this.CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                this.LogError("PopulateSingleResultFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in Client.PopulateSingleResultFromDataSet: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Creates an object from the information passed by the stored procedure
        /// </summary>
        /// <param name="r">
        /// A data row from the data set
        /// </param>
        /// <returns>
        /// A risk section object
        /// </returns>
        private Types.Templates.RiskSection CreateObject(DataRow r)
        {
            Types.Templates.RiskSection obj = new Types.Templates.RiskSection();
            obj.Id = (int)r["Id"];
            obj.Title = (string)r["Title"];
            obj.ReferenceDoc = (string)r["ReferenceDoc"];
            obj.ChapterRef = (string)r["ChapterRef"];
            obj.Description = (string)r["Description"];
            obj.IsDeleted = (bool)r["IsDeleted"];
            return obj;
        }

        #endregion
    }
}
