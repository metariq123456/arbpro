﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using log4net;
using System.Data;
using System.Reflection;
using System.IO;
using System.Data.SqlTypes;

namespace DGS.ArbPro.DAL.Contracts
{
    public class DBContextBase
    {
        #region Enums
        protected enum ParameterMode
        {
            Insert,
            Update
        }
        #endregion

        #region Members
        string _DatabaseName = "";
        string _ClassName = "";
        static DateTime _MinDateValue = DateTime.MinValue;
        static ILog _Log = LogManager.GetLogger("DGS.ArbPro.DAL");
        #endregion

        #region Constructors
        public DBContextBase(string database, string className)
        {
            _DatabaseName = database;
            _ClassName = className;
            _MinDateValue = DateTime.MinValue;
        }

        public DBContextBase(string database, string className, DateTime minDateValue)
        {
            _DatabaseName = database;
            _ClassName = className;
            _MinDateValue = minDateValue;
        }
        #endregion

        #region Properties
        #region Protected
        protected string DatabaseName { get { return _DatabaseName; } }
        protected ILog Log { get { return _Log; } }
        #endregion
        #endregion

        #region Methods
        #region Protected

        /// <summary>
        /// Method to execute an update procedure for an object that inherits this class
        /// </summary>
        /// <param name="procedure">The name of the procedure to be executed</param>
        /// <param name="sqlParams">The array of parameters to be passed to the stored procedure</param>
        /// <returns></returns>
        protected virtual int Save(string procedure, SqlParameter[] sqlParams)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(procedure, sqlParams, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Save", string.Format("string procedure: {0}, SqlParameter[] sqlParams count: {1}", procedure, ParamArrayString(sqlParams)), ex);
                Debug.WriteLine("Error in {0}.Save: {1}",_ClassName, ex.ToString());
            }
            return retVal;

        }

        protected void LogError(string method, string paramsWithvalues, Exception error)
        {
            try
            {
                //NOTE: The Aysnc loggon functionality has been temporarily removed due to an async stack error being raised from calls
                //initiating web pages
                LogErrorToDB(_ClassName, method, paramsWithvalues, error, _DatabaseName);
                //LogErrorAsync(_ClassName, method, paramsWithvalues, error, _DatabaseName);
                Debug.WriteLine("Error logged to database correctly");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error durring error logging.");
                _Log.ErrorFormat("DBContextBase.LogError - {0}", ex.ToString());
            }
        }

        protected void LogError(string className, string method, string paramsWithvalues, Exception error)
        {
            try
            {
                //NOTE: The Aysnc loggon functionality has been temporarily removed due to an async stack error being raised from calls
                //initiating web pages
                LogErrorToDB(className, method, paramsWithvalues, error, _DatabaseName);
                //LogErrorAsync(_ClassName, method, paramsWithvalues, error, _DatabaseName);
                Debug.WriteLine("Error logged to database correctly");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error durring error logging.");
                _Log.ErrorFormat("DBContextBase.LogError - {0}", ex.ToString());
            }
        }

        protected void LogError(MethodBase method, Exception ex, params object[] values)
        {
            ParameterInfo[] parms = method.GetParameters();
            object[] namevalues = new object[2 * parms.Length];

            string msg = "Error in " + method.Name + " ";
            for (int i = 0, j = 0; i < parms.Length; i++, j += 2)
            {
                msg += "{" + j + "}={" + (j + 1) + "}, ";
                namevalues[j] = parms[i].Name;
                if (i < values.Length)
                {
                    if (values[i] == null)
                    {
                        namevalues[j + 1] = "Null Value";
                    }
                    else
                    {
                        namevalues[j + 1] = values[i];
                    }
                }
            }
            LogErrorToDB(_ClassName, method.Name, string.Format(msg, namevalues), ex, _DatabaseName);
             _Log.ErrorFormat(msg + " Error: " + ex.Message, namevalues);
        }

        protected bool HasRows(DataSet ds)
        {
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return false;
            else
                return true;
        }

        protected bool HasRows(DataSet ds, int tableIndex)
        {
            if (ds == null || ds.Tables == null || ds.Tables.Count - 1 < tableIndex || ds.Tables[tableIndex].Rows == null || ds.Tables[tableIndex].Rows.Count == 0)
                return false;
            else
                return true;
        }
        #endregion
        #region private
        private void LogErrorToDB(string className, string method, string paramsWithvalues, Exception ex, string dbName)
        {
            _Log.ErrorFormat("{0} - {1}.{2} - Params: {3} Error: {4}", dbName, className, method, paramsWithvalues, ex.ToString());
            var sqlParams = new SqlParameter[]
            {
                new SqlParameter("@ErrTimeUTC",DateTime.UtcNow),
                new SqlParameter("@ErrTimeLocal",DateTime.UtcNow.ToLocalTime()),
                new SqlParameter("@Class",className),
                new SqlParameter("@Method", method),
                new SqlParameter("@Params", paramsWithvalues),
                new SqlParameter("@ErrMessage", ex.Message),
                new SqlParameter("@Trace",ex.StackTrace != null ? ex.StackTrace : "No Stack"),
                new SqlParameter("@InnerException",ex.InnerException != null ? ex.InnerException.ToString() : "None")
            };
            int errId = DBHelper.ExecuteStoredProcReturnID("ErrorLogInsert", sqlParams, dbName);
        }


        private async void LogErrorAsync(string className, string method, string paramsWithvalues, Exception ex, string dbName)
        {
            bool dbLogUpdated = await LogErrorToDBTask(className, method, paramsWithvalues, ex, dbName).ConfigureAwait(false);
        }

        private Task<bool> LogErrorToDBTask(string className, string method, string paramsWithvalues, Exception ex, string dbName)
        {
            _Log.ErrorFormat("{0} - {1}.{2} - Params: {3} Error: {4} ", dbName, _ClassName, method, paramsWithvalues, ex.ToString(), dbName);
            var taskResult = new TaskCompletionSource<bool>();
            var sqlParams = new SqlParameter[]
            {
                new SqlParameter("@ErrTimeUTC",DateTime.UtcNow),
                new SqlParameter("@ErrTimeLocal",DateTime.UtcNow.ToLocalTime()),
                new SqlParameter("@Class",className),
                new SqlParameter("@Method", method),
                new SqlParameter("@Params", paramsWithvalues),
                new SqlParameter("@ErrMessage", ex.Message),
                new SqlParameter("@Trace",ex.StackTrace),
                new SqlParameter("@InnerException",ex.InnerException != null ? ex.InnerException.ToString() : "None")
            };
            int errId = DBHelper.ExecuteStoredProcReturnID("ErrorLogInsert", sqlParams, dbName);
            taskResult.SetResult(errId > 0);
            return taskResult.Task;
        }

        #endregion
        #endregion

        #region Static Methods
        #region Parameterize

        protected static SqlParameter Parameterize(string Name, DataTable Value)
        {
            SqlParameter ReturnValue = new SqlParameter(Name, Value);
            ReturnValue.SqlDbType = SqlDbType.Structured;

            return ReturnValue;
        }

        protected static SqlParameter Parameterize(String Name, String Value)
        {
            if (Value == null)
            {
                return new SqlParameter(Name, DBNull.Value);
            }
            else
            {
                return new SqlParameter(Name, Value);
            }
        }

        protected static SqlParameter Parameterize(String Name, Int32 Value)
        {
            return new SqlParameter(Name, Value);
        }

        protected static SqlParameter Parameterize(String Name, Double Value)
        {
            return new SqlParameter(Name, Value);
        }

        protected static SqlParameter Parameterize(String Name, float Value)
        {
            return new SqlParameter(Name, Value);
        }

        protected static SqlParameter Parameterize(String Name, DateTime Value)
        {
            if (Value <= _MinDateValue || Value.Ticks == 0)
                return new SqlParameter(Name, DBNull.Value);
            else if ((DateTime)SqlDateTime.MinValue >= Value)
                return new SqlParameter(Name, SqlDateTime.MinValue);
            else if ((DateTime)SqlDateTime.MaxValue <= Value)
                return new SqlParameter(Name, SqlDateTime.MaxValue);
            else
                return new SqlParameter(Name, Value);
        }

        protected static SqlParameter Parameterize(String Name, Boolean Value)
        {
            return new SqlParameter(Name, Value);
        }

        protected static SqlParameter Parameterize(String Name, MemoryStream Value)
        {
            var ReturnValue = new SqlParameter(Name, SqlDbType.VarBinary);
            if (Value == null || Value.Length == 0)
                ReturnValue.Value = DBNull.Value;
            else
                ReturnValue.Value = Value.ToArray();

            return ReturnValue;
        }

        protected static SqlParameter Parameterize(String Name, Byte[] Value)
        {
            // We need to handle null values carefully for VarBinary - and also can't use immediate if 
            // here
            var ReturnValue = new SqlParameter(Name, SqlDbType.VarBinary);
            if (Value == null || Value.Length == 0)
                ReturnValue.Value = DBNull.Value;
            else
                ReturnValue.Value = Value;

            return ReturnValue;
        }

        protected static SqlParameter Parameterize(String Name, Decimal Value, int Precision, int Scale)
        {
            var ReturnValue = new SqlParameter(Name, SqlDbType.Decimal);
            ReturnValue.Precision = (byte)Precision;
            ReturnValue.Scale = (byte)Scale;
            ReturnValue.Value = Value;
            return ReturnValue;
        }

        /// <summary>
        /// Static method to create a SQL data type paramater from a list of objects
        /// This method uses reflection to map the specified property names within the object to the column names
        /// of the data type that is required.
        /// NOTE: Use of this procedure requires the programmer to be aware of the column names and types for the SQL data type being used
        /// NOTE: This function does not currently support datetime correctly
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">The name of the parameter the stored procedure is expecting</param>
        /// <param name="columntoPropertyMapping">Dictionary: Contains the maapings for data structure column name to the property name of T</param>
        /// <param name="objectList">List of type T that is to be added to the data structure parameter</param>
        /// <returns>SQLParameter of type SqlDbType.Structured</returns>
        protected static SqlParameter Parameterize<T>(String name, Dictionary<string, string> columnToPropertyMapping, List<T> objectList)
        {
            DataTable dt = new DataTable();
            SqlParameter parameter = new SqlParameter();
            //Get a list of all the properties of the current object - need to get the properties here so that the data type of the columns can be set
            List<PropertyInfo> pInfoList = (typeof(T).GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)).ToList();
            PropertyInfo pInfo = null;
            //Add a column to the data table for each key
            DataColumn ndc = null;
            foreach (string key in columnToPropertyMapping.Keys)
            {
                pInfo = pInfoList.Find(p => p.Name == columnToPropertyMapping[key]);
                //Check if the property for the column mapping has been found
                ndc = new DataColumn(key);
                if (pInfo != null)
                {
                    ndc.DataType = pInfo.PropertyType;
                }
                dt.Columns.Add(ndc);
            }
            DataRow r = null;
            
            //Iterate through the list to get the required values and add them to the data structure
            foreach (T obj in objectList)
            {
                //Create a new data row
                r = dt.NewRow();
                //Iterate through each column specified and add the property value required
                foreach (DataColumn dc in dt.Columns)
                {
                    //Find the required property info for the current column mapping
                    pInfo = pInfoList.Find(p => p.Name == columnToPropertyMapping[dc.ColumnName]);
                    //Check if the property for the column mapping has been found
                    if (pInfo != null)
                    {
                        //Due to the date time issue with SQL and C# the value should be checked and converted as required.
                        //NOTE - Not support at this time.
                        //if (pInfo.PropertyType == typeof(DateTime))
                        //{
                        //    if (dc.DataType != typeof(DateTime))
                        //    {
                        //        dc.DataType = typeof(DateTime);
                        //    }

                        //    r[dc.ColumnName] = ((DateTime)pInfo.GetValue(obj, null)).ToUniversalTime();
                        //}
                        //else
                        //{
                            //Get the value of the property from the current object and add it to the rows column
                            r[dc.ColumnName] = pInfo.GetValue(obj, null);
                        //}
                    }
                    else
                    {
                        //The property has not been found so add null to the data structure column for this row
                        r[dc.ColumnName] = null;
                    }
                }
                //Once all of the mapped columns have been iterated add the row to the data table
                dt.Rows.Add(r);
            }
            //The parameter for the SP must be of SqlDbType.Structured 
            parameter.ParameterName = name;
            parameter.SqlDbType = System.Data.SqlDbType.Structured;
            parameter.Value = dt;
            return parameter;
        }

        #endregion
        #region GetValue
        protected String StringValue(DataRow Row, String Column)
        {
            try
            {
                if (!Row.Table.Columns.Contains(Column) || DBNull.Value.Equals(Row[Column]))
                    return "";

                return (String)Row[Column];
            }
            catch
            {
                return "";
            }
        }
        protected Int32 IntValue(DataRow Row, String Column)
        {
            try
            {
                if (DBNull.Value.Equals(Row[Column]))
                    return 0;

                return (Int32)Row[Column];
            }
            catch
            {
                return 0;
            }
        }
        protected Double DoubleValue(DataRow Row, String Column)
        {
            try
            {
                if (DBNull.Value.Equals(Row[Column]))
                    return 0d;

                return (Double)Row[Column];
            }
            catch
            {
                return 0d;
            }
        }
        protected decimal DecimaleValue(DataRow Row, String Column)
        {
            try
            {
                if (DBNull.Value.Equals(Row[Column]))
                    return 0.00M;

                return (decimal)Row[Column];
            }
            catch
            {
                return 0M;
            }
        }
        protected float FloatValue(DataRow Row, String Column)
        {
            try
            {
                if (DBNull.Value.Equals(Row[Column]))
                    return 0f;

                return (float)Row[Column];
            }
            catch
            {
                return 0f;
            }
        }
        protected Boolean BoolValue(DataRow Row, String Column)
        {
            try
            {
                if (DBNull.Value.Equals(Row[Column]))
                    return false;

                return (Boolean)Row[Column];
            }
            catch
            {
                return false;
            }
        }
        protected DateTime DateTimeValue(DataRow Row, String Column)
        {
            try
            {
                if (DBNull.Value.Equals(Row[Column]))
                    return DateTime.MinValue;
                else
                {
                    DateTime retVal = (DateTime)Row[Column];
                    if (retVal <= _MinDateValue)
                        retVal = DateTime.MinValue;

                    return retVal;
                }
            }
            catch
            {
                return DateTime.MinValue;
            }
        }
        protected Byte[] ByteValue(DataRow Row, String Column)
        {
            try
            {
                if (DBNull.Value.Equals(Row[Column]))
                    return new Byte[0];

                return (Byte[])Row[Column];
            }
            catch
            {
                return new Byte[0];
            }
        }

        protected MemoryStream StreamValue(DataRow Row, String Column)
        {
            try
            {
                if (DBNull.Value.Equals(Row[Column]))
                    return null;

                return new MemoryStream((Byte[])Row[Column]);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Protected method to report the contents of a parameter array, as a string
        /// </summary>
        /// <param name="sqlParams">The array of SQL Parameters to be reported</param>
        /// <returns>The string representation of the SQL Parameter array contents</returns>
        protected string ParamArrayString(SqlParameter[] sqlParams)
        {
            var sb = new StringBuilder();
            if (sqlParams == null)
            {
                sb.Append("Null Array");
            }
            else if (sqlParams.Length == 0)
            {
                sb.Append("Empty Array");
            }
            else
            {
                foreach (SqlParameter p in sqlParams)
                {
                    sb.AppendFormat("{0}: {1}", p.ParameterName, p.Value);
                    sb.AppendLine();
                }
            }
            return sb.ToString();
        }
        //protected T ToValue<T>(DataRow Row, String Column)
        //{
        //    try
        //    {
        //        if (DBNull.Value.Equals(Row[Column]))
        //            return new T();

        //        return (T)Convert.ChangeType(Row[Column],typeof(T));
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        protected string GetCultureCodeForClient(string databaseName)
        {
            var retVal = "en-GB";
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Currency_GetSelectedCurrency", new SqlParameter[] { }, this.DatabaseName);
                if (this.HasRows(ds))
                {
                    retVal = this.StringValue(ds.Tables[0].Rows[0], "CultureCode");
                }
            }
            catch (Exception ex)
            {
                LogError("Currency_GetSelectedCurrency", "Unable to get selected currency settings", ex);
                Console.WriteLine("Error in JobSearchResult.GetCultureCodeForClient: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion

        #endregion
    }
}
