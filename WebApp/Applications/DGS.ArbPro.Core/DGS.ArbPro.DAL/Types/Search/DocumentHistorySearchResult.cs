﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class DocumentHistorySearchResult : Contracts.DBContextBase
    {
        private string _databaseName = "";

        public DocumentHistorySearchResult(string databaseName) : base(databaseName, "DocumentHistorySearchResult", DateTime.Parse("1970-01-01")) { }

        #region Methods
        #region public
        public List<Types.SearchResult> GetAllForDocumentRefLike(string filterText)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Documents_Reference", new SqlParameter[] { new SqlParameter("@FilterText", filterText), new SqlParameter("@DocType", -1) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForDocumentRefLike", string.Format("filterText: {0}", filterText), ex);
                Console.WriteLine("Error in DocumentHistorySearchResult.GetAllForDocumentRefLike: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }

        public List<Types.SearchResult> GetQuotesForDocumentRefLike(string filterText)
        {
            DataSet ds = null;
            try
            {
                var @params = new SqlParameter[]
                {
                    new SqlParameter("@FilterText", filterText),
                    new SqlParameter("@DocType", (object)0)
                };

                ds = DBHelper.ExecuteStoredProcDataSet("Search_Documents_Reference", @params, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetQuotesForDocumentRefLike", string.Format("filterText: {0}", filterText), ex);
                Console.WriteLine("Error in DocumentHistorySearchResult.GetQuotesForDocumentRefLike: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }

        public List<Types.SearchResult> GetInvoicesForDocumentRefLike(string filterText)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Documents_Reference", new SqlParameter[] { new SqlParameter("@FilterText", filterText), new SqlParameter("@DocType", 1) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetInvoicesForDocumentRefLike", string.Format("filterText: {0}", filterText), ex);
                Console.WriteLine("Error in DocumentHistorySearchResult.GetInvoicesForDocumentRefLike: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        #endregion

        #region private
        private List<Types.SearchResult> PopulateResultsFromDataSet(DataSet ds)
        {
            List<Types.SearchResult> retVal = new List<Types.SearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.DocumentHistorySearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in DocumentHistorySearchResult.PopulateResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.DocumentHistorySearchResult CreateSearchResult(DataRow row)
        {
            Types.DocumentHistorySearchResult obj = new Types.DocumentHistorySearchResult();
            obj.Id = this.IntValue(row, "Id");
            obj.ParentId = this.IntValue(row, "ParentId");
            obj.RefNo = this.StringValue(row, "RefNo");
            obj.DateCreated = this.DateTimeValue(row, "DateCreated");

            try
            {
                obj.DateSent = DateTime.MinValue;
                if (row["DateSent"].GetType() == typeof(System.DateTime))
                {
                    obj.DateSent = (DateTime)row["DateSent"];
                }
            }
            catch {  }

            obj.DispatchMethod = (Types.Enums.DocDispatchMethodEnum)row["DispatchMethod"];
            return obj;
        }
        #endregion
        #endregion
    }
}
