﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public partial class NoteSearchResult : Contracts.DBContextBase
    {
        #region Members
        string _CultureCode = "en-GB";
        #endregion

        #region Constructors
        public NoteSearchResult(string databaseName)
            : base(databaseName, "NoteSearchResult", DateTime.Parse("1970-01-01"))
        {
        }

        public NoteSearchResult(string databaseName, string countryCode)
            : base(databaseName, "NoteSearchResult")
        {
            _CultureCode = countryCode;
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public

        /// <summary>
        /// Search for all quote notes in date created descending order
        /// </summary>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetAll()
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteNote_GetAll", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAll", string.Empty, ex);
                Console.WriteLine("Error in NoteSearchResult.GetAll: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericList(ds);
        }

        public List<SearchResult> GetSelected()
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteNote_GetForDashboard", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetSelected", string.Empty, ex);
                Console.WriteLine("Error in NoteSearchResult.GetSelected: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericList(ds);
        }
        
        #endregion

        #region Private

        /// <summary>
        /// Helper method to validate a dataset and create list of typed search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.NoteSearchResult</returns>
        private List<Types.NoteSearchResult> PopulateTypedResultsFromDataSet(DataSet ds)
        {
            List<Types.NoteSearchResult> retVal = new List<Types.NoteSearchResult>();
            
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            Types.NoteSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r, cultureCode);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateTypedResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in NoteSearchResult.PopulateTypedResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to validate a dataset and create list of base search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.SearchResult</returns>
        private List<Types.SearchResult> ToGenericList(DataSet ds)
        {
            List<Types.SearchResult> retVal = new List<Types.SearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            Types.NoteSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r, cultureCode);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToGenericList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in NoteSearchResult.ToGenericList: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row.
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>Types.NoteSearchResult</returns>
        private Types.NoteSearchResult CreateSearchResult(DataRow row, string cultureCode)
        {
            Types.NoteSearchResult obj = new Types.NoteSearchResult(cultureCode);
            
            obj.Id = this.IntValue(row,"Id");
            obj.ParentId = this.IntValue(row, "ParentId");
            obj.Author = this.StringValue(row, "Author");
            obj.CreateDate = this.DateTimeValue(row,"DateCreated");
            obj.Note = this.StringValue(row, "Note");
            obj.ClientName = this.StringValue(row,"ClientName");
            
            return obj;
        }

        #endregion
        #endregion
    }
}
