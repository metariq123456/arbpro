﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class EmailHistorySearchResult : Contracts.DBContextBase
    {
        #region Members
        Dictionary<string, string> _ActivityMapping = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Required constructor to create an instance connection to a specific database
        /// The constructor also generates the data type mapping for the user defined type
        /// </summary>
        /// <param name="database">The name of the database to connect to</param>
        public EmailHistorySearchResult(string database) : base(database, "EmailHistorySearchResult") 
        {
            //Create the property mapping for the EmailActivity user defined data type
            _ActivityMapping =  new Dictionary<string, string>();
            _ActivityMapping.Add("MsgId", "MsgId");
            _ActivityMapping.Add("Recipient", "Recipient");
            _ActivityMapping.Add("Sender", "Sender");
            _ActivityMapping.Add("Status", "Status");
            _ActivityMapping.Add("SentDate", "SentDate");
        }
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Public method to create a list of email history results based on an activity log
        /// </summary>
        /// <param name="activity">The list of activity log entries to return results for</param>
        /// <returns>List of Types.EmailHistorySearchResult. This can be an empty list if an error occures</returns>
        public List<Types.EmailHistorySearchResult> GetForActivityLog(List<Types.EmailActivityLog> activity)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Email_HistoryForActivity", Parameterize<Types.EmailActivityLog>("@Activity", _ActivityMapping, activity), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForActivityLog", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return ToObjectList(ds);
        }
        #endregion
        #region Private
        /// <summary>
        /// Method to inflate a single object from a dataset
        /// This method takes the first row of the first table and converts it into an object.
        /// If an error occures a newly instantiated object will be returned
        /// </summary>
        /// <param name="ds">A populated dataset</param>
        /// <returns>A single Types.EmailHistorySearchResult object. If an error occures the object id will be -1 </returns>
        private Types.EmailHistorySearchResult ToObject(DataSet ds)
        {
            Types.EmailHistorySearchResult retVal = new Types.EmailHistorySearchResult();
            //Check if the dataset has any rows
            if (!HasRows(ds))
                return retVal;

            //Convert the first row of the first table to an object instance
            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }

            return retVal;
        }

        /// <summary>
        /// Method to generate a list of objects from a dataset.
        /// This method uses only the first table from the dataset
        /// </summary>
        /// <param name="ds">A Populated dataset</param>
        /// <returns>List of Types.EmailHistorySearchResult. This can be an empty list if an error occures</returns>
        private List<Types.EmailHistorySearchResult> ToObjectList(DataSet ds)
        {
            List<Types.EmailHistorySearchResult> retVal = new List<Types.EmailHistorySearchResult>();
            //Check if the dataset has any rows
            if (!HasRows(ds))
                return retVal;

            //Convert the first row of the first table to an object instance
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }

            return retVal;
        }

        /// <summary>
        /// Method to inflate an object from a datarow
        /// </summary>
        /// <param name="row">The datarow to populate the new object from</param>
        /// <returns>A new, populated, Types.EmailHistorySearchResult object</returns>
        private Types.EmailHistorySearchResult CreateInstance(DataRow row)
        {
            Types.EmailHistorySearchResult obj = new Types.EmailHistorySearchResult();
            //TODO: DGSAP-430 Populate the object from the values returned from the database
            obj.Id = base.IntValue(row,"Id"); //The id of the Quote / Work item or invoice
            obj.ParentId = base.IntValue(row, "ParentId"); //The id of the job
	        obj.ClientName = base.StringValue(row,"ClientName"); //The name of the client
            obj.Value = base.DecimaleValue(row, "Value");   //The monetary value for the item
	        obj.DocumentType= base.IntValue(row,"ItemType"); //The type of item This should be an enum
            obj.DocRef = base.StringValue(row, "Reference");    //This is the reference associated with the document
	        obj.Recipient= base.StringValue(row,"Recipient"); //The email address of the recipient
	        obj.Status= base.StringValue(row,"Status"); //The status of the email sent / bounced / clicked etc
	        obj.SentDate= base.DateTimeValue(row,"SentDate"); //The date the email was sent
            return obj;
        }
        #endregion
        #endregion
    }
}
