﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public partial class WorkSearchResult : Contracts.DBContextBase
    {
        #region Members
        string _CultureCode = "en-GB";
        #endregion

        #region Constructors
        public WorkSearchResult(string databaseName)
            : base(databaseName, "WorkSearchResult", DateTime.Parse("1970-01-01"))
        {
        }

        public WorkSearchResult(string databaseName, string countryCode)
            : base(databaseName, "WorkSearchResult")
        {
            _CultureCode = countryCode;
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Search for all quotes for a client Id
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForClientId(int clientId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientId", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in WorkSearchResult.GetForClientId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that are for a client, contact or business name containing the specified string
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForNameLike(string name)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_NameLike", new SqlParameter[] { new SqlParameter("@Name", name) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForNameLike", string.Format("name: {0}", name), ex);
                Console.WriteLine("Error in WorkSearchResult.GetForClientNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that have a site address containing the specified string
        /// </summary>
        /// <param name="address">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForSiteAddressLike(string address)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_SiteAddress", new SqlParameter[] { new SqlParameter("@Address", address) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForSiteAddressLike", string.Format("address: {0}", address), ex);
                Console.WriteLine("Error in WorkSearchResult.GetForSiteAddressLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that are set to a specified status
        /// </summary>
        /// <param name="status">Enums.InvoiceStatusEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForStatus(WorkStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_Status", new SqlParameter[] { new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatus", string.Format("status: {0}", (int)status), ex);
                Console.WriteLine("Error in WorkSearchResult.GetForStatus: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        public List<SearchResult> GetWorkNotBooked()
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_NotBooked", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetWorkNotBooked", "None", ex);
                Console.WriteLine("Error in WorkSearchResult.GetWorkNotBooked: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        public List<SearchResult> GetJobWorkNotBooked(int Month)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_JobWork_NotBooked", new SqlParameter[] { new SqlParameter("@Month", Month) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetJobWorkNotBooked", "None", ex);
                Console.WriteLine("Error in WorkSearchResult.GetJobWorkNotBooked: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        public List<SearchResult> GetJobNotAcceptedWork(int Month)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_JobWork_NotAcceptedAndNotBooked", new SqlParameter[] { new SqlParameter("@Month", Month) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetJobWorkNotBooked", "None", ex);
                Console.WriteLine("Error in WorkSearchResult.GetJobNotAcceptedWork: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        public List<SearchResult> GetAllJobWork(int Month)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_JobWork_QuotesAll", new SqlParameter[] { new SqlParameter("@Month", Month) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllJobWork", "None", ex);
                Console.WriteLine("Error in WorkSearchResult.GetAllJobWork: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }
        /// <summary>
        /// Search for all quotes that are not set to a specified status
        /// </summary>
        /// <param name="status">Enums.InvoiceStatusEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForStatusNot(InvoiceStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_StatusNot", new SqlParameter[] { new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatus", string.Format("status: {0}", (int)status), ex);
                Console.WriteLine("Error in WorkSearchResult.GetForStatusNot: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }
        #endregion

        #region Internal
        /// <summary>
        /// Internal method to be used to populate a Client model Quotes property
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of Types.WorkSearchResult</returns>
        internal List<Types.WorkSearchResult> GetForClientModel(int clientId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientModel", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in WorkSearchResult.GetForClientModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateTypedResultsFromDataSet(ds);

        }

        /// <summary>
        /// Internal method to be used to populate a Job model Quotes property
        /// </summary>
        /// <param name="jobId">int</param>
        /// <returns>List of Types.WorkSearchResult</returns>
        internal List<Types.WorkSearchResult> GetForJobModel(int jobId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_JobId", new SqlParameter[] { new SqlParameter("@jobId", jobId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJobModel", string.Format("jobId: {0}", jobId), ex);
                Console.WriteLine("Error in WorkSearchResult.GetForJobModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateTypedResultsFromDataSet(ds);

        }

        /// <summary>
        /// Internal async method to be used to populate a Client model Quotes property
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>A Task that returns List of Types.WorkSearchResult</returns>
        internal Task<List<Types.WorkSearchResult>> GetForClientModelAsync(int clientId)
        {
            var taskResult = new TaskCompletionSource<List<Types.WorkSearchResult>>();
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                //Return the populated search result list
                taskResult.SetResult(PopulateTypedResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetForClientModelAsync", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in WorkSearchResult.GetForClientModelAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion

        #region Private
        /// <summary>
        /// Helper method to validate a dataset and create list of typed search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.ClientSearchResult</returns>
        private List<Types.WorkSearchResult> PopulateTypedResultsFromDataSet(DataSet ds)
        {
            List<Types.WorkSearchResult> retVal = new List<Types.WorkSearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.WorkSearchResult obj = null;
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r, cultureCode);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateTypedResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in WorkSearchResult.PopulateTypedResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to validate a dataset and create list of base search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.SearchResult</returns>
        private List<Types.SearchResult> ToGenericObjectList(DataSet ds)
        {
            List<Types.SearchResult> retVal = new List<Types.SearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            Types.WorkSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r, cultureCode);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateGenericResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in WorkSearchResult.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row.
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>Types.WorkSearchResult</returns>
        private Types.WorkSearchResult CreateSearchResult(DataRow row, string cultureCode)
        {
            Types.WorkSearchResult obj = new Types.WorkSearchResult(cultureCode);
            obj.Id = this.IntValue(row,"Id");
            obj.ParentId = this.IntValue(row, "ParentId");
            obj.WorkItemCount = this.IntValue(row,"WorkItemCount");
            obj.QuoteValue = this.DecimaleValue(row, "QuoteValue");
            obj.ClientName = this.StringValue(row, "ClientName");
            obj.ContactDTL = this.StringValue(row, "Contactdtl");
            obj.QuoteId = this.IntValue(row, "QuoteId");
            obj.SiteAddress = this.StringValue(row, "SiteAddress");
            obj.SiteId = this.IntValue(row, "SiteId");
            obj.Status = (WorkStatusEnum) this.IntValue(row,"Status");
            obj.Reference = this.StringValue(row, "Reference");
            obj.DateCreated = this.DateTimeValue(row, "DateCreated");
            obj.NotesPresent = this.IntValue(row, "NotesAmount");
            obj.ClientSiteId = this.IntValue(row, "ClientSiteId");
            int tpo = this.IntValue(row, "TpoStatus");
            switch (tpo)
            {
                case 0:
                    obj.TpoStatus = "Unknown";
                    break;
                case 1:
                    obj.TpoStatus = "No";
                    break;
                case 2:
                    obj.TpoStatus = "Yes";
                    break;
            }

            return obj;
        }
        #endregion
        #endregion
    }
}
