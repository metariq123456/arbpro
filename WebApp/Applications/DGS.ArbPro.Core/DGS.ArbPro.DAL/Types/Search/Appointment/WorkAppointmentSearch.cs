﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class WorkAppointmentSearch: Contracts.DBContextBase
    {
        #region Constructors
        public WorkAppointmentSearch(string databaseName)
            : base(databaseName, "WorkAppointmentSearch", DateTime.Parse("1970-01-01"))
        { }
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Method to get a list of all open Work appointments from today
        /// </summary>
        /// <returns> List<Types.WorkAppointmentSearch></returns>
        public List<Types.AppointmentSearchResult> GetList()
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_WorkAppoinment_GetList", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetList", "None", ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetList: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all appointments that are for or with a name like the specified parameter
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForNameLike(string name)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_CalendarWork_GetForNameLike", new SqlParameter[] { new SqlParameter("@Name", name) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForNameLike", string.Format("name: {0}", name), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetForNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }


        /// <summary>
        /// Search for all appointments that are for a client Id
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForClientId(int clientId)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_CalendarWork_GetForClient", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientId", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetForClientId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a satff Id
        /// </summary>
        /// <param name="staffId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForStaffId(int staffId)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_CalendarWork_GetForStaff", new SqlParameter[] { new SqlParameter("@StaffId", staffId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStaffId", string.Format("staffId: {0}", staffId), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetForStaffId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a satff Id on a specific date
        /// </summary>
        /// <param name="staffId">int</param>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForStaffIdAndDate(int staffId, DateTime forDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_CalendarWork_GetForStaffAndDate", new SqlParameter[] { new SqlParameter("@StaffId", staffId), new SqlParameter("@Date", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStaffIdAndDate", string.Format("staffId: {0}, forDate: {1}", staffId.ToString(), forDate.ToString()), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetForStaffIdAndDate: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a specific date
        /// </summary>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForDate(DateTime forDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_CalendarWork_GetForDate", new SqlParameter[] { new SqlParameter("@ForDate", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDate", string.Format("forDate: {0}", forDate.ToString()), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetForDate: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }


        public List<Types.AppointmentSearchResult> GetForDateRange(DateTime startDate, DateTime endDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_CalendarWork_GetForDateRange", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDate", string.Format("forDate: {0}", startDate.ToString()), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetForDate: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }
        /// <summary>
        /// Search for all appointments that are for a specific date and time
        /// </summary>
        /// <param name="forDate">DateTime</param>
        /// <param name="forTime">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForDateAndTime(DateTime forDate, DateTime forTime)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_WorkAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@Date", forDate), new SqlParameter("@Time", forTime) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDateAndTime", string.Format("forDate: {0}, forTime: {1}", forDate.ToString(), forTime.ToString()), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetForDateAndTime: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a specific type
        /// </summary>
        /// <param name="type">Enums.AppointmentTypeEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForType(AppointmentTypeEnum type)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_WorkAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@Type", (int)type) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForType", string.Format("type: {0}", type.ToString()), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetForType: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a specific type on a date
        /// </summary>
        /// <param name="type">Enums.AppointmentTypeEnum</param>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForTypeAndDate(AppointmentTypeEnum type, DateTime forDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_WorkAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@Type", (int)type), new SqlParameter("@Date", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForTypeAndDate", string.Format("type: {0}, forDate: {1}", type.ToString(), forDate.ToString()), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetForTypeAndDate: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Async task to search for all appointments that are for a specific type on a date
        /// </summary>
        /// <param name="type">Enums.AppointmentTypeEnum</param>
        /// <param name="forDate">DateTime</param>
        /// <returns>Task containing List of SearchResult objects</returns>
        public Task<List<Types.WorkAppointmentSearch>> GetForTypeAndDateAsync(AppointmentTypeEnum type, DateTime forDate)
        {
            var taskResult = new TaskCompletionSource<List<Types.WorkAppointmentSearch>>();
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_WorkAppoinment_GetForTypeAndDateAsync", new SqlParameter[] { new SqlParameter("@Type", (int)type), new SqlParameter("@Date", forDate) }, this.DatabaseName);
                //Return the populated search result list
                taskResult.SetResult(ToTypedObjectList(ds));
            }
            catch (Exception ex)
            {
                LogError("GetForTypeAndDateAsync", string.Format("type: {0}, forDate: {1}", type.ToString(), forDate.ToString()), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetForTypeAndDateAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion

        #region Internal Methods
        internal List<Types.WorkAppointmentSearch> GetListForStaffModel(int staffId)
        {
            DataSet ds = null;
            //Populate the data set from the database
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_WorkAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@StaffId", staffId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetListForStaffModel", string.Format("staffId: {0}", staffId), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.GetListForStaffModel: {0}", ex.ToString());
            }
            return ToTypedObjectList(ds);
        }
        #endregion

        #region Private Static
        private List<Types.WorkAppointmentSearch> ToTypedObjectList(DataSet ds)
        {
            List<Types.WorkAppointmentSearch> retVal = new List<Types.WorkAppointmentSearch>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.WorkAppointmentSearch obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToTypedObjectList", string.Format("ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.ToTypedObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.AppointmentSearchResult> ToGenericObjectList(DataSet ds)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.WorkAppointmentSearch obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToGenericObjectList", string.Format("ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in WorkAppointmentSearch.ToGenericObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.WorkAppointmentSearch CreateSearchResult(DataRow row)
        {
            Types.WorkAppointmentSearch obj = new Types.WorkAppointmentSearch();
            obj.Id = (int)row["Id"];
            obj.ParentId = (int)row["ParentId"];
            obj.StartDateTime = (DateTime)row["StartDate"];
            obj.EndDateTime = (DateTime)row["EndDate"];
            obj.ClientName = (string)row["ClientName"];
            obj.StaffName = (string)row["StaffName"];
            obj.SiteAddress = (string)row["SiteAddress"];
            obj.Value = DecimaleValue(row,"Value");
            return obj;
        }
        #endregion
        #endregion
    }
}