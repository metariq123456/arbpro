﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class QuoteAppointmentSearch: Contracts.DBContextBase
    {
        #region Constructors
        public QuoteAppointmentSearch(string databaseName)
            : base(databaseName, "QuoteAppointmentSearch", DateTime.Parse("1970-01-01"))
        { }
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Method to get a list of all open Quote appointments from today
        /// </summary>
        /// <returns> List<Types.QuoteAppointmentSearch></returns>
        public List<Types.AppointmentSearchResult> GetList()
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteAppoinment_GetList", new SqlParameter[]{ }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetList", "None", ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetList: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }


        /// <summary>
        /// Search for all appointments that are for or with a name like the specified parameter
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForNameLike(string name)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteAppoitnment_GetForName", new SqlParameter[] {new SqlParameter("@Name", name) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForNameLike", string.Format("name: {0}", name), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetForNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a client Id
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForClientId(int clientId, bool incompleteOnly)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteAppoitnment_GetForClient", new SqlParameter[] { new SqlParameter("@ClientId", clientId), new SqlParameter("@IncompleteOnly", incompleteOnly) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientId", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetForClientId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a satff Id
        /// </summary>
        /// <param name="staffId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForStaffId(int staffId)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteAppoinment_GetForStaff", new SqlParameter[] { new SqlParameter("@StaffId", staffId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStaffId", string.Format("staffId: {0}", staffId), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetForStaffId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a satff Id on a specific date
        /// </summary>
        /// <param name="staffId">int</param>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForStaffIdAndDate(int staffId, DateTime forDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteAppoinment_GetForStaffAndDate", new SqlParameter[] { new SqlParameter("@StaffId", staffId), new SqlParameter("@Date", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStaffIdAndDate", string.Format("staffId: {0}, forDate: {1}", staffId.ToString(), forDate.ToString()), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetForStaffIdAndDate: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a specific date
        /// </summary>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForDate(DateTime forDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_CalendarQuote_GetForDate", new SqlParameter[] { new SqlParameter("@ForDate", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDate", string.Format("forDate: {0}", forDate.ToString()), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetForDate: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        public List<Types.AppointmentSearchResult> GetForDateRange(DateTime startDate, DateTime endDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_CalendarQuote_GetForDateRange", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDateRange", string.Format("forDate: {0}", startDate.ToString()), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetForDateRange: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a specific date and time
        /// </summary>
        /// <param name="forDate">DateTime</param>
        /// <param name="forTime">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForDateAndTime(DateTime forDate, DateTime forTime)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@Date", forDate), new SqlParameter("@Time", forTime) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDateAndTime", string.Format("forDate: {0}, forTime: {1}", forDate.ToString(), forTime.ToString()), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetForDateAndTime: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a specific type
        /// </summary>
        /// <param name="type">Enums.AppointmentTypeEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForType(AppointmentTypeEnum type)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@Type", (int)type) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForType", string.Format("type: {0}", type.ToString()), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetForType: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a specific type on a date
        /// </summary>
        /// <param name="type">Enums.AppointmentTypeEnum</param>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForTypeAndDate(AppointmentTypeEnum type, DateTime forDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@Type", (int)type), new SqlParameter("@Date", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForTypeAndDate", string.Format("type: {0}, forDate: {1}", type.ToString(), forDate.ToString()), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetForTypeAndDate: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Async task to search for all appointments that are for a specific type on a date
        /// </summary>
        /// <param name="type">Enums.AppointmentTypeEnum</param>
        /// <param name="forDate">DateTime</param>
        /// <returns>Task containing List of SearchResult objects</returns>
        public Task<List<Types.QuoteAppointmentSearch>> GetForTypeAndDateAsync(AppointmentTypeEnum type, DateTime forDate)
        {
            var taskResult = new TaskCompletionSource<List<Types.QuoteAppointmentSearch>>();
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteAppoinment_GetForTypeAndDateAsync", new SqlParameter[] { new SqlParameter("@Type", (int)type), new SqlParameter("@Date", forDate) }, this.DatabaseName);
                //Return the populated search result list
                taskResult.SetResult(PopulateTypedResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetForTypeAndDateAsync", string.Format("type: {0}, forDate: {1}", type.ToString(), forDate.ToString()), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetForTypeAndDateAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion

        #region Internal Methods
        internal List<Types.QuoteAppointmentSearch> GetListForStaffModel(int staffId)
        {
            DataSet ds = null;
            //Populate the data set from the database
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_QuoteAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@StaffId", staffId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetListForStaffModel", string.Format("staffId: {0}", staffId), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.GetListForStaffModel: {0}", ex.ToString());
            }
            return PopulateTypedResultsFromDataSet(ds);
        }
        #endregion

        #region Private Static
        private List<Types.QuoteAppointmentSearch> PopulateTypedResultsFromDataSet(DataSet ds)
        {
            List<Types.QuoteAppointmentSearch> retVal = new List<Types.QuoteAppointmentSearch>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.QuoteAppointmentSearch obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateTypedResultsFromDataSet", string.Format("ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.PopulateTypedResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.AppointmentSearchResult> PopulateGenericResultsFromDataSet(DataSet ds)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.QuoteAppointmentSearch obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateGenericResultsFromDataSet", string.Format("ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteAppointmentSearch.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.QuoteAppointmentSearch CreateSearchResult(DataRow row)
        {
            Types.QuoteAppointmentSearch obj = new Types.QuoteAppointmentSearch();
            obj.Id = (int)row["Id"];
            obj.ParentId = (int)row["ParentId"];
            obj.StartDateTime = (DateTime)row["StartDate"];
            obj.EndDateTime = (DateTime)row["EndDate"];
            obj.ClientName = (string)row["ClientName"];
            obj.StaffName = (string)row["StaffName"];
            obj.SiteAddress = (string)row["SiteAddress"];
            return obj;
        }
        #endregion
        #endregion
    }
}