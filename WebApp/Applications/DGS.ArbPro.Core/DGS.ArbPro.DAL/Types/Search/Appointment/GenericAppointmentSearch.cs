﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class GenericAppointmentSearch: Contracts.DBContextBase
    {
        #region Constructors
        public GenericAppointmentSearch(string databaseName)
            : base(databaseName, "GenericAppointmentSearch", DateTime.Parse("1970-01-01"))
        { }
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Method to get a list of all open Work appointments from today
        /// </summary>
        /// <returns> List<Types.WorkAppointmentSearch></returns>
        public List<Types.AppointmentSearchResult> GetList()
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_GenericAppoinment_GetList", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetList", "None", ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetList: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for or with a name like the specified parameter
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForNameLike(string name)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_GenericAppoinment_GetForName", new SqlParameter[] {new SqlParameter("@Name", name) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForNameLike", string.Format("name: {0}", name), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetForNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a client Id
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForClientId(int clientId)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Appointment_GetForClient", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientId", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetForClientId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a satff Id
        /// </summary>
        /// <param name="staffId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForStaffId(int staffId)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_GenericAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@StaffId", staffId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStaffId", string.Format("staffId: {0}", staffId), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetForStaffId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a satff Id on a specific date
        /// </summary>
        /// <param name="staffId">int</param>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForStaffIdAndDate(int staffId, DateTime forDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_GenericAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@StaffId", staffId), new SqlParameter("@Date", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStaffIdAndDate", string.Format("staffId: {0}, forDate: {1}", staffId.ToString(), forDate.ToString()), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetForStaffIdAndDate: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a specific date
        /// </summary>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForDate(DateTime forDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_CalendarGeneric_GetForDate", new SqlParameter[] { new SqlParameter("@ForDate", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDate", string.Format("forDate: {0}", forDate.ToString()), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetForDate: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        public List<Types.AppointmentSearchResult> GetForDateRange(DateTime startDate, DateTime endDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_CalendarGeneric_GetForDateRange", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDateRange", string.Format("forDate: {0}", startDate.ToString()), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetForDateRange: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }
        /// <summary>
        /// Search for all appointments that are for a specific date and time
        /// </summary>
        /// <param name="forDate">DateTime</param>
        /// <param name="forTime">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForDateAndTime(DateTime forDate, DateTime forTime)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_GenericAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@Date", forDate), new SqlParameter("@Time", forTime) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDateAndTime", string.Format("forDate: {0}, forTime: {1}", forDate.ToString(), forTime.ToString()), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetForDateAndTime: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a specific type
        /// </summary>
        /// <param name="type">Enums.AppointmentTypeEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForType(AppointmentTypeEnum type)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_GenericAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@Type", (int)type) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForType", string.Format("type: {0}", type.ToString()), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetForType: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all appointments that are for a specific type on a date
        /// </summary>
        /// <param name="type">Enums.AppointmentTypeEnum</param>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of AppointmentSearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetBetweenDates(DateTime startDate, DateTime endDate)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_GenericAppointment_ForDate", new SqlParameter[] { 
                    new SqlParameter("@FromDate", startDate), 
                    new SqlParameter("@ToDate", endDate)
                }, 
                this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetBetweenDates", string.Format("DateTime startDate:{0}, DateTime endDate:{1}", startDate.ToString(), endDate.ToString()), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetBetweenDates: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Async task to search for all appointments that are for a specific type on a date
        /// </summary>
        /// <param name="type">Enums.AppointmentTypeEnum</param>
        /// <param name="forDate">DateTime</param>
        /// <returns>Task containing List of SearchResult objects</returns>
        public Task<List<Types.GenericAppointmentSearch>> GetForTypeAndDateAsync(AppointmentTypeEnum type, DateTime forDate)
        {
            var taskResult = new TaskCompletionSource<List<Types.GenericAppointmentSearch>>();
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_GenericAppoinment_GetForTypeAndDateAsync", new SqlParameter[] { new SqlParameter("@Type", (int)type), new SqlParameter("@Date", forDate) }, this.DatabaseName);
                //Return the populated search result list
                taskResult.SetResult(PopulateTypedResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetForTypeAndDateAsync", string.Format("type: {0}, forDate: {1}", type.ToString(), forDate.ToString()), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetForTypeAndDateAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion

        #region Internal Methods
        internal List<Types.GenericAppointmentSearch> GetListForStaffModel(int staffId)
        {
            DataSet ds = null;
            //Populate the data set from the database
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_GenericAppoinment_GetForName", new SqlParameter[] { new SqlParameter("@StaffId", staffId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetListForStaffModel", string.Format("staffId: {0}", staffId), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.GetListForStaffModel: {0}", ex.ToString());
            }
            return PopulateTypedResultsFromDataSet(ds);
        }
        #endregion

        #region Private Static
        private List<Types.GenericAppointmentSearch> PopulateTypedResultsFromDataSet(DataSet ds)
        {
            List<Types.GenericAppointmentSearch> retVal = new List<Types.GenericAppointmentSearch>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.GenericAppointmentSearch obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateTypedResultsFromDataSet", string.Format("ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.PopulateTypedResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.AppointmentSearchResult> PopulateGenericResultsFromDataSet(DataSet ds)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.GenericAppointmentSearch obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateGenericResultsFromDataSet", string.Format("ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in GenericAppointmentSearch.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.GenericAppointmentSearch CreateSearchResult(DataRow row)
        {
            /*
             *  CalendarGeneric.ID AS Id,
	            CalendarGeneric.StaffID AS ParentId,
	            CalendarGeneric.StartDateTime AS StartDate,
	            CalendarGeneric.EndDateTime AS EndDate,
	            CalendarGeneric.EventType AS EventType,
	            CalendarGeneric.AllDay AS AllDay,
	            Staff.Name AS StaffName
             * */
            Types.GenericAppointmentSearch obj = new Types.GenericAppointmentSearch();
            obj.Id = (int)row["Id"];
            obj.ParentId = (int)row["ParentId"];
            obj.StartDateTime = (DateTime)row["StartDate"];
            obj.EndDateTime = (DateTime)row["EndDate"];
            obj.EventType = (int)row["EventType"];
            obj.IsAllDayEvent = (bool)row["AllDay"];
            obj.StaffName = (string)row["StaffName"];
            return obj;
        }
        #endregion
        #endregion
    }
}