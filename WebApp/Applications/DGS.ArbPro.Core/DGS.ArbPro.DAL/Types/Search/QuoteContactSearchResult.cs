﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public partial class QuoteContactSearchResult : Contracts.DBContextBase
    {
        #region Constructors
        public QuoteContactSearchResult(string databaseName) : base(databaseName, "QuoteContactSearchResult", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public

        ///// <summary>
        ///// Search for all contacts for a client Id
        ///// </summary>
        ///// <param name="clientId">int</param>
        ///// <returns>List of Search Result objects</returns>
        //public List<SearchResult> GetForClientId(int clientId)
        //{
        //    //Populate the dataset
        //    DataSet ds = null;
        //    try
        //    {
        //        ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_Contact_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogError("GetForClientId", string.Format("clientId: {0}", clientId), ex);
        //        Console.WriteLine("Error in ContactSearchResult.GetForClientId: {0}", ex.ToString());
        //    }
        //    //Return the populated search result list
        //    return PopulateGenericResultsFromDataSet(ds);
        //}
        #endregion

        #region Internal
        /// <summary>
        /// Method to return a typed set for a Client model property
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of Types.ContactSearchResult</returns>
        public List<Types.QuoteContactSearchResult> GetForClientModel(int clientId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_Contact_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientModel", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in ContactSearchResult.GetForClientModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateTypedResultsFromDataSet(ds);
        }

        ///// <summary>
        ///// Async task to return a typed set for a Client model property
        ///// </summary>
        ///// <param name="clientId">int</param>
        ///// <returns>Task containing List of Types.ContactSearchResult</returns>
        //internal Task<List<Types.QuoteContactSearchResult>> GetForClientModelAsync(int clientId)
        //{
        //    var taskResult = new TaskCompletionSource<List<Types.QuoteContactSearchResult>>();
        //    DataSet ds = null;
        //    try
        //    {
        //        ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_Contact_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
        //        //Return the populated search result list
        //        taskResult.SetResult(PopulateTypedResultsFromDataSet(ds));
        //    }
        //    catch (Exception ex)
        //    {
        //        LogError("GetForClientModelAsync", string.Format("clientId: {0}", clientId), ex);
        //        Console.WriteLine("Error in ContactSearchResult.GetForClientModelAsync: {0}", ex.ToString());
        //        taskResult.SetException(ex);
        //    }
        //    return taskResult.Task;
        //}

        /// <summary>
        /// Helper method to create an object instance for quotes only from a data row.
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>Types.QuoteContactSearchResult</returns>
        internal static Types.QuoteContactSearchResult CreateSearchResult(DataRow row)
        {
            Types.QuoteContactSearchResult obj = new Types.QuoteContactSearchResult();
            obj.Id = (int)row["ContactId"];
            obj.ParentId = (int)row["ClientId"];
            obj.Name = (string)row["ContactName"];
            obj.SurnameAndTitle = (string)row["SurnameAndTitle"];
            obj.Email = (string)row["ContactEmail"];
            obj.Telephone = (string)row["ContactTelephone"];
            obj.Mobile = (string)row["ContactMobile"];
            obj.ClientName = (string)row["ClientName"];
            obj.ClientId = (int)row["ClientId"];
            obj.SendSmsMessage = (bool)row["SendSmsMessage"];
            obj.ClientType = (ClientTypeEnum)(int)row["ClientType"];
            obj.ContactType = (ContactTypeEnum)(int)row["ContactType"];
            return obj;
        }
        #endregion

        #region Private
        /// <summary>
        /// Helper method to validate a dataset and create list of typed search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.ContactSearchResult</returns>
        private List<Types.QuoteContactSearchResult> PopulateTypedResultsFromDataSet(DataSet ds)
        {
            List<Types.QuoteContactSearchResult> retVal = new List<Types.QuoteContactSearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.QuoteContactSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateTypedResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ContactSearchResult.PopulateTypedResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        ///// <summary>
        ///// Helper method to validate a dataset and create list of base search results
        ///// </summary>
        ///// <param name="ds">Dataset</param>
        ///// <returns>A List of Types.SearchResult</returns>
        //private List<Types.SearchResult> PopulateGenericResultsFromDataSet(DataSet ds)
        //{
        //    List<Types.SearchResult> retVal = new List<Types.SearchResult>();
        //    //Check if there is any data to be returned
        //    if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
        //        return retVal;

        //    Types.QuoteContactSearchResult obj = null;
        //    try
        //    {
        //        foreach (DataRow r in ds.Tables[0].Rows)
        //        {
        //            obj = CreateSearchResult(r);
        //            retVal.Add(obj);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogError("PopulateGenericResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
        //        Console.WriteLine("Error in ContactSearchResult.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
        //    }
        //    return retVal;
        //}
        #endregion
        #endregion
    }
}
