﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class AppointmentSearchResult : Contracts.DBContextBase
    {
        QuoteAppointmentSearch QuoteContext = null;
        WorkAppointmentSearch Workcontext = null;
        GenericAppointmentSearch GenericContext = null;

        #region Constructors
        public AppointmentSearchResult(string databaseName)
            : base(databaseName, "AppointmentSearchResult", DateTime.Parse("1970-01-01"))
        {
            QuoteContext = new QuoteAppointmentSearch(databaseName);
            Workcontext = new WorkAppointmentSearch(databaseName);
            GenericContext = new GenericAppointmentSearch(databaseName);
        }
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Search for all appointments that are for or with a name like the specified parameter
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForNameLike(string name)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            retVal.AddRange(QuoteContext.GetForNameLike(name));
            retVal.AddRange(Workcontext.GetForNameLike(name));
            retVal.AddRange(GenericContext.GetForNameLike(name));
            return retVal;
        }

        /// <summary>
        /// Search for all appointments that are for a client Id
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForClientId(int clientId, bool incompleteOnly)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            retVal.AddRange(QuoteContext.GetForClientId(clientId, incompleteOnly));
            return retVal;
        }

        /// <summary>
        /// Search for all appointments that are for a satff Id
        /// </summary>
        /// <param name="staffId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForStaffId(int staffId)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            retVal.AddRange(QuoteContext.GetForStaffId(staffId));
            retVal.AddRange(Workcontext.GetForStaffId(staffId));
            retVal.AddRange(GenericContext.GetForStaffId(staffId));
            return retVal;
        }

        /// <summary>
        /// Search for all appointments that are for a satff Id on a specific date
        /// </summary>
        /// <param name="staffId">int</param>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForStaffIdAndDate(int staffId, DateTime forDate)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            retVal.AddRange(QuoteContext.GetForStaffIdAndDate(staffId,forDate));
            retVal.AddRange(Workcontext.GetForStaffIdAndDate(staffId, forDate));
            retVal.AddRange(GenericContext.GetForStaffIdAndDate(staffId, forDate));
            return retVal;
        }

        /// <summary>
        /// Search for all appointments that are for a specific date
        /// </summary>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForDate(DateTime forDate)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            retVal.AddRange(QuoteContext.GetForDate(forDate));
            retVal.AddRange(Workcontext.GetForDate(forDate));
            retVal.AddRange(GenericContext.GetForDate(forDate));
            return retVal;
        }

        /// <summary>
        /// Search for all appointments that are for a specific date and time
        /// </summary>
        /// <param name="forDate">DateTime</param>
        /// <param name="forTime">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForDateAndTime(DateTime forDate, DateTime forTime)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            retVal.AddRange(QuoteContext.GetForDateAndTime(forDate,forTime));
            retVal.AddRange(Workcontext.GetForDateAndTime(forDate,forTime));
            retVal.AddRange(GenericContext.GetForDateAndTime(forDate, forTime));
            return retVal;
        }

        /// <summary>
        /// Search for all appointments that are for a specific type and not in the past
        /// </summary>
        /// <param name="type">Enums.AppointmentTypeEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForType(AppointmentTypeEnum type)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            switch (type)
            {
                case AppointmentTypeEnum.Sales:
                    retVal.AddRange(QuoteContext.GetList());
                    break;
                case AppointmentTypeEnum.Work:
                    retVal.AddRange(Workcontext.GetList());
                    break;
                case AppointmentTypeEnum.Generic:
                    retVal.AddRange(GenericContext.GetList());
                    break;
                case AppointmentTypeEnum.Company:
                    retVal.AddRange(QuoteContext.GetList());
                    retVal.AddRange(Workcontext.GetList());
                    retVal.AddRange(GenericContext.GetList());
                    break;

            }
            return retVal;
        }

        /// <summary>
        /// Search for all appointments that are for a specific type on a date
        /// </summary>
        /// <param name="type">Enums.AppointmentTypeEnum</param>
        /// <param name="forDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.AppointmentSearchResult> GetForTypeAndDate(AppointmentTypeEnum type, DateTime forDate)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            switch (type)
            {
                case AppointmentTypeEnum.Sales:
                    retVal.AddRange(QuoteContext.GetForDate(forDate));
                    break;
                case AppointmentTypeEnum.Work:
                    retVal.AddRange(Workcontext.GetForDate(forDate));
                    break;
                case AppointmentTypeEnum.Generic:
                    retVal.AddRange(GenericContext.GetForDate(forDate));
                    break;
                case AppointmentTypeEnum.Company:
                    retVal.AddRange(QuoteContext.GetForDate(forDate));
                    retVal.AddRange(Workcontext.GetForDate(forDate));
                    retVal.AddRange(GenericContext.GetForDate(forDate));
                    break;

            }
            return retVal;
        }
        #endregion

        #region Internal Methods
        internal List<Types.AppointmentSearchResult> GetListForStaffModel(int staffId)
        {
            List<Types.AppointmentSearchResult> retVal = new List<Types.AppointmentSearchResult>();
            retVal.AddRange(QuoteContext.GetListForStaffModel(staffId));
            retVal.AddRange(Workcontext.GetListForStaffModel(staffId));
            retVal.AddRange(GenericContext.GetListForStaffModel(staffId));
            return retVal;
        }
        #endregion

        
        #endregion
    }
}
