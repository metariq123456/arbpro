﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class RiskTemplateSearchResult : Contracts.DBContextBase
    {
        #region Constructors
        public RiskTemplateSearchResult(string databaseName) : base(databaseName, "RiskTemplateSearchResult", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Methods
        #region Public
        public List<Types.RiskTemplateSearchResult> GetRiskTemplateList()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_RiskTemplates", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetRiskTemplateList", "None", ex);
                Console.WriteLine("Error in RiskTemplateSearchResult.RiskTemplateSearchResult: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }
        #endregion
        #region Private
        private List<Types.SearchResult> ToGenericObjectList(DataSet ds)
        {
            List<Types.SearchResult> retVal = new List<Types.SearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.RiskTemplateSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateInstance(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToGenericObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in RiskTemplateSearchResult.ToGenericObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.RiskTemplateSearchResult> ToObjectList(DataSet ds)
        {
            List<Types.RiskTemplateSearchResult> retVal = new List<Types.RiskTemplateSearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.RiskTemplateSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateInstance(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in RiskTemplateSearchResult.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.RiskTemplateSearchResult CreateInstance(DataRow row)
        {
            Types.RiskTemplateSearchResult obj = new Types.RiskTemplateSearchResult();
            obj.Id = this.IntValue(row,"Id");
            obj.ParentId = this.IntValue(row, "ParentId");
            obj.Name = this.StringValue(row,"Name");
            obj.Reference = this.StringValue(row,"RefDoc");
            obj.Description = this.StringValue(row,"Description");
            return obj;
        }
        #endregion
        #endregion
    }
}
