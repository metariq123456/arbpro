﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public partial class InvoiceSearchResult : Contracts.DBContextBase
    {
        #region Constructors
        public InvoiceSearchResult(string databaseName) : base(databaseName, "InvoiceSearchResult", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public List<SearchResult> GetForInvSiteAddressLike(string address)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_SiteAddress", new SqlParameter[] { new SqlParameter("Address", address) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForInvSiteAddressLike", string.Format("address: {0}", address), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForInvSiteAddressLike: {0}", ex.ToString());
            }

            return ToGenericObjectList(ds);
        }

        //GetInvoiceableWork
        public List<SearchResult> GetInvoiceableWork()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_WorkNotInvoiced", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetInvoiceableWork", "None", ex);
                Console.WriteLine("Error in InvoiceSearchResult.Search_Invoice_WorkNotInvoiced: {0}", ex.ToString());
            }

            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Returns all invoices based on an invoice id
        /// </summary>
        /// <param name="_invoiceId"></param>
        /// <returns></returns>
        public List<SearchResult> GetForId(string _invoiceId)
        {
            DataSet ds = null;
            int invoiceId;
            bool res = int.TryParse(_invoiceId, out invoiceId);
            invoiceId = res ? invoiceId : -1;

            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_InvoiceId", new SqlParameter[] { new SqlParameter("@InvoiceId", invoiceId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("_invoiceId: {0}", _invoiceId), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForId: {0}", ex.ToString());
            }

            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all invoices for a quote Id
        /// </summary>
        /// <param name="quoteId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForQuoteId(int quoteId)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_QuoteId", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteId", string.Format("quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all invoices for a client Id
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForInvClientId(int clientId)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientId", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all invoices for a client or business name like the supplied paramater.
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForClientNameLike(string name)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_NameLike", new SqlParameter[] { new SqlParameter("@Name", name) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientNameLike", string.Format("name: {0}", name), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all invoices with a version number like the supplied paramater.
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForInoiceVersionLike(string versionNo)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_VersionLike", new SqlParameter[] { new SqlParameter("@Version", versionNo) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForInoiceVersionLike", string.Format("versionNo: {0}", versionNo), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForInoiceVersionLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all invoices that are set to a specified status
        /// </summary>
        /// <param name="status">Enums.InvoiceStatusEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForStatus(InvoiceStatusEnum status)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_Status", new SqlParameter[] { new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatus", string.Format("status: {0}", (int)status), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that are not set to a specified status
        /// </summary>
        /// <param name="status">Enums.InvoiceStatusEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForStatusNot(InvoiceStatusEnum status)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_StatusNot", new SqlParameter[] { new SqlParameter("@status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatusNot", string.Format("status: {0}", (int)status), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all invoices that are due before the specified date but not been paid
        /// </summary>
        /// <param name="dueByDate">DateTime</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForDueDate(DateTime dueByDate, bool includeOverdue)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_ForDueDate", new SqlParameter[] { new SqlParameter("@ForDate", dueByDate), new SqlParameter("@IncludeOverdue", includeOverdue) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDueDate", string.Format("dueByDate: {0}, IncludeOverdue: {1}", dueByDate.ToShortDateString(), includeOverdue.ToString()), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForDueDate: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        public List<SearchResult> GetAllDue()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_AllDue", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllDue", "", ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetAllDue: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        public List<SearchResult> GetAllSent()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_AllSent", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllSent", "", ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetAllSent: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }
        public List<SearchResult> GetAllPaid()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_AllPaid", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllPaid", "", ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetAllPaid: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }
        public List<SearchResult> GetAllOverdue()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_Overdue", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Search_Invoice_Overdue", "None", ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetAllDue: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        public List<SearchResult> GetForReferenceLike(string referenceNo)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_ReferenceLike", new SqlParameter[] { new SqlParameter("@RefNo", referenceNo) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForReferenceLike", string.Format("referenceNo: {0}", referenceNo), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForReferenceLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }
        #endregion

        #region Internal
        /// <summary>
        /// Internal method to be used to populate a Client model Quotes property
        /// </summary>
        /// <param name="quoteId">int</param>
        /// <returns>List of Types.InvoiceSearchResult</returns>
        internal List<Types.InvoiceSearchResult> GetForQuoteModel(int quoteId)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_QuoteId", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientModel", string.Format("quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForQuoteModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToTypedObjectList(ds);
        }

        /// <summary>
        /// Internal method to be used to populate a Job model Invoices property
        /// </summary>
        /// <param name="jobId">int</param>
        /// <returns>List of Types.InvoiceSearchResult</returns>
        internal List<Types.InvoiceSearchResult> GetForJobModel(int jobId)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_JobId", new SqlParameter[] { new SqlParameter("@JobId", jobId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientModel", string.Format("jobId: {0}", jobId), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForQuoteModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToTypedObjectList(ds);
        }

        /// <summary>
        /// Internal async method to be used to populate a Quote model Invoices property
        /// </summary>
        /// <param name="quoteId">int</param>
        /// <returns>List of Types.InvoiceSearchResult</returns>
        internal Task<List<Types.InvoiceSearchResult>> GetForQuoteModelAsync(int quoteId)
        {
            var taskResult = new TaskCompletionSource<List<Types.InvoiceSearchResult>>();
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_QuoteId", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
                //Return the populated search result list
                taskResult.SetResult(ToTypedObjectList(ds));
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteModelAsync", string.Format("quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in InvoiceSearchResult.GetForQuoteModelAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }

        /// <summary>
        /// Internal method to be used to populate a Client model Quotes property
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of Types.InvoiceSearchResult</returns>
        internal List<Types.InvoiceSearchResult> GetForClientModel(int clientId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientModel", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForClientModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToTypedObjectList(ds);

        }

        /// <summary>
        /// Internal async method to be used to populate a Client model Quotes property
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>A Task that returns List of Types.InvoiceSearchResult</returns>
        internal Task<List<Types.InvoiceSearchResult>> GetForClientModelAsync(int clientId)
        {
            var taskResult = new TaskCompletionSource<List<Types.InvoiceSearchResult>>();
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Invoice_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                //Return the populated search result list
                taskResult.SetResult(ToTypedObjectList(ds));
            }
            catch (Exception ex)
            {
                LogError("GetForClientModelAsync", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForClientModelAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion

        #region Private
        /// <summary>
        /// Helper method to validate a dataset and create list of typed search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.InvoiceSearchResult</returns>
        private List<Types.InvoiceSearchResult> ToTypedObjectList(DataSet ds)
        {
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            List<Types.InvoiceSearchResult> retVal = new List<Types.InvoiceSearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.InvoiceSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r, cultureCode);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateTypedResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in InvoiceSearchResult.PopulateTypedResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to validate a dataset and create list of base search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.SearchResult</returns>
        private List<Types.SearchResult> ToGenericObjectList(DataSet ds)
        {
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            List<Types.SearchResult> retVal = new List<Types.SearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.InvoiceSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r, cultureCode);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateGenericResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in InvoiceSearchResult.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row.
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>Types.InvoiceSearchResult</returns>
        private Types.InvoiceSearchResult CreateSearchResult(DataRow row, string cultureCode)
        {
            Types.InvoiceSearchResult obj = new Types.InvoiceSearchResult(cultureCode);
            obj.Id = base.IntValue(row,"Id");
            obj.ParentId = base.IntValue(row,"ParentId");
            obj.QuoteNumber = base.IntValue(row,"QuoteId");
            obj.QuoteReference = this.StringValue(row, "QuoteRef");
            obj.TotalValue = this.DecimaleValue(row, "TotalValue");
            obj.InvoiceReference = this.StringValue(row, "Reference");
            DateTime invDate = base.DateTimeValue(row, "DateSent");
            obj.DateSent = invDate > DateTime.MinValue ? invDate.ToShortDateString() : "Unknown";
            obj.InvoiceAddress = this.StringValue(row, "Address");
            obj.Status = (InvoiceStatusEnum)this.IntValue(row, "Status");
            invDate = base.DateTimeValue(row, "DueDate");
            obj.DateDue = invDate > DateTime.MinValue ? invDate.ToShortDateString() : "Unknown";
            obj.DateCreated = this.DateTimeValue(row, "DateCreated");
            obj.DateSentDate = this.DateTimeValue(row, "DateSentDate");
            int tpo = this.IntValue(row, "TpoStatus");
            switch (tpo)
            {
                case 0:
                    obj.TpoStatus = "Unknown";
                    break;
                case 1:
                    obj.TpoStatus = "No";
                    break;
                case 2:
                    obj.TpoStatus = "Yes";
                    break;
            }

            return obj;
        }
        #endregion
        #endregion
    }
}
