﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class ComplaintSearchResult : Contracts.DBContextBase
    {
        #region Constructors
        public ComplaintSearchResult(string databaseName) : base(databaseName, "ComplaintSearchResult", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Methods
        #region Public
        public List<Types.SearchResult> GetAllGeneric()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Complaint_GetAll", new SqlParameter[] {}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllGeneric", "None", ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.GetAllGeneric{0}{1}{0}", Environment.NewLine, ex.ToString());
            }
            return ToGenericList(ds);
        }

        public List<Types.SearchResult> GetGenericForType(bool closed)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Complaint_GetForType", new SqlParameter[] { new SqlParameter("@Closed", closed) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllOfTypeGeneric", string.Format("closed: {0}", closed.ToString()), ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.GetAllOfTypeGeneric{0}{1}{0}", Environment.NewLine, ex.ToString());
            }

            return ToGenericList(ds);
        }

        public List<Types.SearchResult> GetGenericForTextLike(string text)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Complaint_GetForSearchLike", new SqlParameter[] { new SqlParameter("@Value", text) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllGenericForTextLike", string.Format("text: {0}", text), ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.GetAllGenericForTextLike{0}{1}{0}", Environment.NewLine, ex.ToString());
            }

            return ToGenericList(ds);
        }

        public List<Types.SearchResult> GetGenericForStaffLike(string staffName)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Complaint_GetForStaffLike", new SqlParameter[] { new SqlParameter("@StaffName", staffName) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllGenericForTextLike", string.Format("staffName: {0}", staffName), ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.GetAllGenericForTextLike{0}{1}{0}", Environment.NewLine, ex.ToString());
            }

            return ToGenericList(ds);
        }

        public List<Types.ComplaintSearchResult> GetAll()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Complaint_GetAll", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAll", "None", ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.GetAll{0}{1}{0}", Environment.NewLine, ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.ComplaintSearchResult> GetForType(bool closed)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Complaint_GetForType", new SqlParameter[] { new SqlParameter("@Closed", closed) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllOfType", string.Format("closed: {0}", closed.ToString()), ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.GetAllOfType{0}{1}{0}", Environment.NewLine, ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.ComplaintSearchResult> GetForTextLike(string text)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Complaint_GetForSearchLike", new SqlParameter[] { new SqlParameter("@Value", text) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllGenericForTextLike", string.Format("text: {0}", text), ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.GetAllGenericForTextLike{0}{1}{0}", Environment.NewLine, ex.ToString());
            }

            return ToObjectList(ds);
        }

        public List<Types.ComplaintSearchResult> GetForStaffLike(string staffName)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Complaint_GetForStaffLike", new SqlParameter[] { new SqlParameter("@StaffName", staffName) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllGenericForTextLike", string.Format("staffName: {0}", staffName), ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.GetAllGenericForTextLike{0}{1}{0}", Environment.NewLine, ex.ToString());
            }

            return ToObjectList(ds);
        }
        #endregion
        #region Private
        //private List<T> ToList<T>(DataSet ds) where T : Types.SearchResult
        //{
        //    List<T> retVal = new List<T>();
        //    try
        //    {
        //        foreach (DataRow r in ds.Tables[0].Rows)
        //        {
        //            var obj = CreateInstance(r);
        //            if (obj.GetType() == typeof(T))
        //                retVal.Add(obj as T);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        LogError("ToGenericList", string.Format("DataSet: {0}", ds.ToString()), ex);
        //        Debug.WriteLine("Error in: ComplaintSearchResult.ToGenericList{0}{1}{0}", Environment.NewLine, ex.ToString());
        //    }
        //    return retVal;
        //}

        private List<Types.SearchResult> ToGenericList(DataSet ds)
        {
            List<Types.SearchResult> retVal = new List<Types.SearchResult>();

            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToGenericList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.ToGenericList{0}{1}{0}", Environment.NewLine, ex.ToString());
            }

            return retVal;
        }

        private List<Types.ComplaintSearchResult> ToObjectList(DataSet ds)
        {
            List<Types.ComplaintSearchResult> retVal = new List<Types.ComplaintSearchResult>();

            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.ToObjectList{0}{1}{0}", Environment.NewLine, ex.ToString());
            }

            return retVal;
        }

        //private T CreateInstance<T>(DataRow row) where T : SearchResult 
        //{
        //    Types.ComplaintSearchResult obj = new Types.ComplaintSearchResult();
        //    obj.Id = (int)row["Id"];
        //    obj.ParentId = -1; //Not implemented for complaint at present - May be QuoteId at a later version
        //    obj.DateOfComplaint = DateTimeValue(row, "ComplaintDate");
        //    obj.Complainant = (string)row["Complainant"];
        //    obj.AllocatedTo = (string)row["AllocatedStaffName"];
        //    obj.ComplaintType = (string)row["Type"];
        //    obj.ComplaintDetail = (string)row["Detail"];
        //    obj.Closed = (bool)row["Closed"];
        //    return obj as T;
        //}

        private Types.ComplaintSearchResult CreateInstance(DataRow row)
        {
            Types.ComplaintSearchResult obj = new Types.ComplaintSearchResult();
            obj.Id = this.IntValue(row, "Id");
            obj.ParentId = -1; //Not implemented for complaint at present - May be QuoteId at a later version
            obj.DateOfComplaint = this.DateTimeValue(row,"ComplaintDate");
            obj.Complainant = this.StringValue(row, "Complainant");
            obj.AllocatedTo = this.StringValue(row, "AllocatedStaffName");
            obj.ComplaintType = this.StringValue(row, "Type");
            obj.ComplaintDetail = this.StringValue(row, "Detail");
            obj.Closed = this.BoolValue(row, "Closed");
            return obj;
        }
        #endregion
        #endregion
    }
}
