﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class TpoSearchResult : Contracts.DBContextBase
    {
        #region Constructors
        public TpoSearchResult(string databaseName) : base(databaseName, "TpoSearchResult", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Methods
        #region Public
        #region Generic

        public List<Types.SearchResult> GetForDashBoard(TreeProtectionOrderTypeEnum orderType, TreeProtectionProgreesEnum progressStage, TreeProtectionStatusEnum status)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_TPO_OrderTypeAndProgress", new SqlParameter[] { Parameterize("@OrderTypeEnum", (int)orderType), Parameterize("@ProgressEnum", (int)progressStage), Parameterize("@StatusEnum", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDashBoard", string.Format("TreeProtectionOrderTypeEnum orderType: {0}, TreeProtectionProgreesEnum progressStage: {1}, TreeProtectionStatusEnum status: {2}", orderType, progressStage, status), ex);
            }
            return ToGenericList(ds);
        }

        public List<Types.SearchResult> GetAppealedNoDescisionDate()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_TPO_AppealedNoDescisionDate", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAppealedNoDescisionDate", "None", ex);
            }
            return ToGenericList(ds);
        }

        public List<Types.SearchResult> GetGenericSixWeekExpiredDate(DateTime forDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_TPO_SixWeekExpiration", new SqlParameter[] { new SqlParameter("@ForDate", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetGenericSixWeekExpiredDate", string.Format("DateTime forDate: {0}", forDate.ToString()), ex);
            }
            return ToGenericList(ds);
        }

        public List<Types.SearchResult> GetGenericEightWeekExpiredDate(DateTime forDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_TPO_EightWeekExpiration", new SqlParameter[] { new SqlParameter("@ForDate", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetGenericEightWeekExpiredDate", string.Format("DateTime forDate: {0}", forDate.ToString()), ex);
            }
            return ToGenericList(ds);
        }

        public List<Types.SearchResult> GetGenericForStatus(Types.Enums.TreeProtectionProgreesEnum progressStage)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_TPO_GetForStatusOf", new SqlParameter[] { new SqlParameter("@ProgressStage", (int)progressStage) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetGenericForStatus", string.Format("TreeProtectionProgreesEnum progressStage: {0}", progressStage.ToString()), ex);
            }
            return ToGenericList(ds);
        }

        public List<Types.SearchResult> GetGenericGrantedNotBooked()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("[Search_TPO_GrantedNotBooked]", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetGenericGrantedNotBooked", "None", ex);
            }
            return ToGenericList(ds);
        }

        public List<Types.SearchResult> GetGenericForQuote(int QuoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_TPO_GetForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", QuoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Search_TPO_GetForQuote", string.Format("QuoteId: {0}", QuoteId), ex);
            }
            return ToGenericList(ds);
        }
        #endregion
        #region Typed
        public List<Types.TpoSearchResult> GetTypedSixWeekExpiredDate(DateTime forDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_TPO_SixWeekExpiration", new SqlParameter[] { new SqlParameter("@ForDate", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetTypedSixWeekExpiredDate", string.Format("DateTime forDate: {0}", forDate.ToString()), ex);
            }
            return ToObjectList(ds);
        }

        public List<Types.TpoSearchResult> GetTypedEightWeekExpiredDate(DateTime forDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_TPO_EightWeekExpiration", new SqlParameter[] { new SqlParameter("@ForDate", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetTypedEightWeekExpiredDate", string.Format("DateTime forDate: {0}", forDate.ToString()), ex);
            }
            return ToObjectList(ds);
        }

        public List<Types.TpoSearchResult> GetTypedForStatus(Types.Enums.TreeProtectionProgreesEnum progressStage)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_TPO_GetForStatusOf", new SqlParameter[] { new SqlParameter("@ProgressStage", (int)progressStage) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetTypedForStatus", string.Format("TreeProtectionProgreesEnum progressStage: {0}", progressStage.ToString()), ex);
            }
            return ToObjectList(ds);
        }
        #endregion
        #endregion
        #region Private
        /// <summary>
        /// Method to generate a generic list of search results from a dataset
        /// </summary>
        /// <param name="ds">DataSet: The data returned from the stored procedure</param>
        /// <returns>List of Types.SearchResult </returns>
        private List<Types.SearchResult> ToGenericList(DataSet ds)
        {
            List<Types.SearchResult> retVal = new List<Types.SearchResult>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToGenericList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }

        /// <summary>
        /// Method to generate a typed list of Tpo search results from a dataset
        /// </summary>
        /// <param name="ds">DataSet: The data returned from the stored procedure</param>
        /// <returns>List of Types.TpoSearchResult </returns>
        private List<Types.TpoSearchResult> ToObjectList(DataSet ds)
        {
            List<Types.TpoSearchResult> retVal = new List<Types.TpoSearchResult>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }

        /// <summary>
        /// Method to create a single TpoSearchResult instance from a datarow
        /// </summary>
        /// <param name="row">The row to be used to create the object instance</param>
        /// <returns>Types.TpoSearchResult instance</returns>
        private Types.TpoSearchResult CreateInstance(DataRow row)
        {
            Types.TpoSearchResult obj = new Types.TpoSearchResult();

            obj.Id = this.IntValue(row,"Id");
            obj.ParentId = this.IntValue(row,"ParentId");
            obj.PorgressStage = (Types.Enums.TreeProtectionProgreesEnum)this.IntValue(row,"PorgressStage");
            obj.SixWeekDate = this.DateTimeValue(row, "SixWeekDate");
            obj.EightWeekDate = this.DateTimeValue(row, "EightWeekDate");
            obj.ApplicationDate = this.DateTimeValue(row, "ApplicationDate");
            obj.Reference = this.StringValue(row, "Reference");
            obj.ClientName = this.StringValue(row, "ClientName");

            return obj;
        }
        #endregion
        #endregion
    }
}
