﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public partial class QuoteSearchResult: Contracts.DBContextBase
    {
        #region Members
        string _CultureCode = "en-GB";
        #endregion

        #region Constructors
        public QuoteSearchResult(string databaseName)
            : base(databaseName, "QuoteSearchResult", DateTime.Parse("1970-01-01"))
        {
        }

        public QuoteSearchResult(string databaseName, string countryCode)
            : base(databaseName, "QuoteSearchResult")
        {
            _CultureCode = countryCode;
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Search for all quotes for a client Id
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForClientId(int clientId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientId", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForClientId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Method to
        /// </summary>
        /// <param name="days"></param>
        /// <returns></returns>
        public List<SearchResult> GetSentWithinDays(int days)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_SentWithinDays", new SqlParameter[] { Parameterize("@Days", days) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetSentWithinDays", string.Format("days: {0}", days), ex);
                Debug.WriteLine("Error in QuoteSearchResult.GetSentWithinDays: {0}", ex.ToString());
            }
            return ToGenericObjectList(ds);
        }

        public List<SearchResult> GetAllSentQuotes()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_AllSent", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllSentQuotes", "Error:", ex);
                Debug.WriteLine("Error in QuoteSearchResult.GetSentWithinDays: {0}", ex.ToString());
            }
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search all quotes for client account number
        /// Resolution to DGSAP-822
        /// </summary>
        /// <param name="clientAcc">string: The account number, or part of, to search for</param>
        /// <returns>List of generic search results</returns>
        public List<SearchResult> GetForClientAccount(string clientAcc)
        {
            DataSet ds = null;

            // Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_ClientAccount", new SqlParameter[] { new SqlParameter("@ClientAcc", clientAcc) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientAccount", string.Format("clientAcc: {0}", clientAcc), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForClientAccount: {0}", ex.ToString());
            }

            // Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes for a quote id like..
        /// </summary>
        /// <param name="filterText">Full or partial Quote.ID string LIKE pattern</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForQuoteId(string filterText)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_QuoteId", new SqlParameter[] { new SqlParameter("@FilterText", filterText) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientId", string.Format("filterText: {0}", filterText), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForQuoteId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that are for a client, contact or business name containing the specified string
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForNameLike(string name)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_NameLike", new SqlParameter[] { new SqlParameter("@Name", name) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForNameLike", string.Format("name: {0}", name), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForClientNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that have a site address containing the specified string
        /// </summary>
        /// <param name="address">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForSiteAddressLike(string address)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_SiteAddress", new SqlParameter[] { new SqlParameter("@Address", address) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForSiteAddressLike", string.Format("address: {0}", address), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForSiteAddressLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that are set to a specified status
        /// </summary>
        /// <param name="status">Enums.InvoiceStatusEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForStatus(QuoteStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_Status", new SqlParameter[] { new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatus", string.Format("status: {0}", (int)status), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForStatus: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that are not set to a specified status
        /// </summary>
        /// <param name="status">Enums.InvoiceStatusEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForStatusNot(InvoiceStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_StatusNot", new SqlParameter[] { new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatus", string.Format("status: {0}", (int)status), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForStatusNot: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Method to return document reference based on reference no
        /// </summary>
        /// <param name="referenceNo"></param>
        /// <returns></returns>
        public List<SearchResult> GetForReferenceLike(string referenceNo)
        {
            //Populate the dataset
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_ReferenceLike", new SqlParameter[] { new SqlParameter("@RefNo", referenceNo) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForReferenceLike", string.Format("referenceNo: {0}", referenceNo), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForReferenceLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }
        #endregion

        #region Internal
        /// <summary>
        /// Internal method to be used to populate a Client model Quotes property
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of Types.QuoteSearchResult</returns>
        internal List<Types.QuoteSearchResult> GetForClientModel(int clientId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientModel", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForClientModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToTypedObjectList(ds);

        }

        /// <summary>
        /// Internal method to be used to populate a Job model Quotes property
        /// </summary>
        /// <param name="jobId">int</param>
        /// <returns>List of Types.QuoteSearchResult</returns>
        internal List<Types.QuoteSearchResult> GetForJobModel(int jobId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_JobId", new SqlParameter[] { new SqlParameter("@jobId", jobId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJobModel", string.Format("jobId: {0}", jobId), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForJobModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToTypedObjectList(ds);

        }

        /// <summary>
        /// Internal async method to be used to populate a Client model Quotes property
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>A Task that returns List of Types.QuoteSearchResult</returns>
        internal Task<List<Types.QuoteSearchResult>> GetForClientModelAsync(int clientId)
        {
            var taskResult = new TaskCompletionSource<List<Types.QuoteSearchResult>>();
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Quote_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                //Return the populated search result list
                taskResult.SetResult(ToTypedObjectList(ds));
            }
            catch (Exception ex)
            {
                LogError("GetForClientModelAsync", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in QuoteSearchResult.GetForClientModelAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion

        #region Private
        /// <summary>
        /// Helper method to validate a dataset and create list of typed search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.ClientSearchResult</returns>
        private List<Types.QuoteSearchResult> ToTypedObjectList(DataSet ds)
        {
            List<Types.QuoteSearchResult> retVal = new List<Types.QuoteSearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            Types.QuoteSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r, cultureCode);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToTypedObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteSearchResult.ToTypedObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to validate a dataset and create list of base search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.SearchResult</returns>
        private List<Types.SearchResult> ToGenericObjectList(DataSet ds)
        {
            List<Types.SearchResult> retVal = new List<Types.SearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            Types.QuoteSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r, cultureCode);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToGenericObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteSearchResult.ToGenericObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row.
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>Types.ClientSearchResult</returns>
        private Types.QuoteSearchResult CreateSearchResult(DataRow row, string cultureCode)
        {
            Types.QuoteSearchResult obj = new Types.QuoteSearchResult(cultureCode);
            obj.Id = this.IntValue(row,"Id");
            obj.ParentId = this.IntValue(row, "ParentId");
            obj.QuoteItemCount = this.IntValue(row,"QuoteItemCount");
            obj.QuoteValue = this.DecimaleValue(row, "QuoteValue");
            obj.ClientName = this.StringValue(row, "ClientName");
            obj.SiteAddress = this.StringValue(row,"SiteAddress");
            obj.Reference = this.StringValue(row,"Reference");
            obj.Status = (QuoteStatusEnum)this.IntValue(row,"Status");
            obj.DateCreated = this.DateTimeValue(row, "DateCreated");
            obj.DateSent = this.DateTimeValue(row, "DateSent");
            obj.NotesPresent = base.IntValue(row, "NotesAmount");
            int tpo = this.IntValue(row, "TpoStatus");
            switch (tpo)
            {
                case 0:
                    obj.TpoStatus = "Unknown";
                    break;
                case 1:
                    obj.TpoStatus = "No";
                    break;
                case 2:
                    obj.TpoStatus = "Yes";
                    break;
            }

            return obj;
        }
        #endregion
        #endregion
    }
}
