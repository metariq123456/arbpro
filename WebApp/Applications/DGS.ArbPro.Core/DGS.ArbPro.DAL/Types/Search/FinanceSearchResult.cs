﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class FinanceSearchResult: Contracts.DBContextBase
    {
        #region Constructors

        /// <summary>
        /// Initialises a new instance of the <see cref="FinanceSearchResult"/> class
        /// </summary>
        /// <param name="database">The name of the database to be used</param>
        public FinanceSearchResult(string database) : base(database, "FinanceSearchResult")
        {
        }

        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Gets the current finances for the company to be displayed on the dashboard
        /// </summary>
        /// <returns></returns>
        public List<Types.SearchResult> DashboardSummary()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Finance_Summary", this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("DashboardSummary", "None", ex);
            }

            return this.ToGenericObjectList(ds);
        }
        #endregion
        #region Private

        private List<Types.SearchResult> ToGenericObjectList(DataSet ds)
        {
            var retVal = new List<Types.SearchResult>();
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            if (!this.HasRows(ds))
            {
                return retVal;
            }

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(this.CreateInstance(r, cultureCode));
                }
            }
            catch (Exception ex)
            {
                this.LogError("ToGenericObjectList", "DataSet ds", ex);
            }

            return retVal;
        }


        private List<Types.FinanceSearchResult> ToTypedObjectList(DataSet ds)
        {
            var retVal = new List<Types.FinanceSearchResult>();
            var cultureCode = GetCultureCodeForClient(this.DatabaseName);
            if (!this.HasRows(ds))
            {
                return retVal;
            }

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(this.CreateInstance(r, cultureCode));
                }
            }
            catch (Exception ex)
            {
                this.LogError("ToTypedObjectList", "DataSet ds", ex);
            }

            return retVal;
        }

        private Types.FinanceSearchResult CreateInstance(DataRow row, string cultureCode)
        {
            var obj = new Types.FinanceSearchResult(cultureCode);
            obj.Heading = this.StringValue(row, "Heading");
            obj.Value = this.DecimaleValue(row, "Value");
            return obj;
        }
        #endregion
        #endregion
    }
}
