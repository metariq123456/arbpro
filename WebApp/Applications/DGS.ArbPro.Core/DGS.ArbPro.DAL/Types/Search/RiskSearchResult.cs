﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public partial class RiskSearchResult: Contracts.DBContextBase
    {
        #region Members
        string _CultureCode = "en-GB";
        #endregion

        #region Constructors
        public RiskSearchResult(string databaseName)
            : base(databaseName, "RiskSearchResult", DateTime.Parse("1970-01-01"))
        {
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Search for all risk assessments for a client Id
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<Types.RiskSearchResult> GetForClientId(int clientId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_RiskAssessment_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientId", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in RiskSearchResult.GetForClientId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateTypedResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all quotes that are for a client, contact or business name containing the specified string
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForNameLike(string name)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_NameLike", new SqlParameter[] { new SqlParameter("@Name", name) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForNameLike", string.Format("name: {0}", name), ex);
                Console.WriteLine("Error in RiskSearchResult.GetForClientNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all quotes that have a site address containing the specified string
        /// </summary>
        /// <param name="address">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForSiteAddressLike(string address)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_SiteAddress", new SqlParameter[] { new SqlParameter("@Address", address) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForSiteAddressLike", string.Format("address: {0}", address), ex);
                Console.WriteLine("Error in RiskSearchResult.GetForSiteAddressLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all quotes that are set to a specified status
        /// </summary>
        /// <param name="status">Enums.InvoiceStatusEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForStatus(WorkStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_Status", new SqlParameter[] { new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatus", string.Format("status: {0}", (int)status), ex);
                Console.WriteLine("Error in RiskSearchResult.GetForStatus: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all quotes that are not set to a specified status
        /// </summary>
        /// <param name="status">Enums.InvoiceStatusEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForStatusNot(InvoiceStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_StatusNot", new SqlParameter[] { new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatus", string.Format("status: {0}", (int)status), ex);
                Console.WriteLine("Error in RiskSearchResult.GetForStatusNot: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        public List<Types.RiskSearchResult> GetListForQuote(int quoteId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_RiskAssessment_QuoteId", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetListForQuote", string.Format("quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in RiskSearchResult.GetForQuote: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateTypedResultsFromDataSet(ds);
        }
        #endregion

        #region Internal
        /// <summary>
        /// Internal method to be used to populate a Client model Quotes property
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of Types.RiskSearchResult</returns>
        internal List<Types.RiskSearchResult> GetForClientModel(int clientId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientModel", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in RiskSearchResult.GetForClientModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateTypedResultsFromDataSet(ds);

        }

        /// <summary>
        /// Internal method to be used to populate a Job model Quotes property
        /// </summary>
        /// <param name="jobId">int</param>
        /// <returns>List of Types.RiskSearchResult</returns>
        internal List<Types.RiskSearchResult> GetForJobModel(int jobId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_JobId", new SqlParameter[] { new SqlParameter("@jobId", jobId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJobModel", string.Format("jobId: {0}", jobId), ex);
                Console.WriteLine("Error in RiskSearchResult.GetForJobModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateTypedResultsFromDataSet(ds);

        }

        /// <summary>
        /// Internal async method to be used to populate a Client model Quotes property
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>A Task that returns List of Types.RiskSearchResult</returns>
        internal Task<List<Types.RiskSearchResult>> GetForClientModelAsync(int clientId)
        {
            var taskResult = new TaskCompletionSource<List<Types.RiskSearchResult>>();
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Work_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                //Return the populated search result list
                taskResult.SetResult(PopulateTypedResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetForClientModelAsync", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in RiskSearchResult.GetForClientModelAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion

        #region Private
        /// <summary>
        /// Helper method to validate a dataset and create list of typed search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.ClientSearchResult</returns>
        private List<Types.RiskSearchResult> PopulateTypedResultsFromDataSet(DataSet ds)
        {
            List<Types.RiskSearchResult> retVal = new List<Types.RiskSearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.RiskSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateTypedResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in RiskSearchResult.PopulateTypedResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to validate a dataset and create list of base search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.SearchResult</returns>
        private List<Types.SearchResult> PopulateGenericResultsFromDataSet(DataSet ds)
        {
            List<Types.SearchResult> retVal = new List<Types.SearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.RiskSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateGenericResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in RiskSearchResult.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row.
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>Types.RiskSearchResult</returns>
        private Types.RiskSearchResult CreateSearchResult(DataRow row)
        {
            Types.RiskSearchResult obj = new Types.RiskSearchResult();
            obj.Id = this.IntValue(row,"Id");
            obj.ParentId = this.IntValue(row, "ParentId"); //Job Id
            obj.QuoteId = this.IntValue(row,"QuoteId"); //Quote Id
            obj.ClientName = this.StringValue(row, "ClientName");
            obj.SiteAddress = this.StringValue(row,"SiteAddress");
            obj.Status = (RiskAssessmentStatusEnum)this.IntValue(row,"CurrentStatus");
            obj.JobSiteId = this.IntValue(row, "JobSiteId");
            return obj;
        }
        #endregion
        #endregion
    }
}
