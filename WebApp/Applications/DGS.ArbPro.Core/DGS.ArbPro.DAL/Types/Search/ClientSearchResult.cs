﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class ClientSearchResult : Contracts.DBContextBase
    {

        #region Constructors
        public ClientSearchResult(string databaseName)
            : base(databaseName, "ClientSearchResult", DateTime.Parse("1970-01-01"))
        {
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Search for a single client with a specified Id
        /// Can return a null value.
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>A Single Search Result Object</returns>
        public Types.ClientSearchResult GetForClientId(int clientId)
        {
            Types.ClientSearchResult retVal = null;
            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Client_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                //Populate the dataset

                var results = PopulateTypedResultsFromDataSet(ds);
                //There should only ever be on result
                if (results.Count > 0)
                    retVal = results[0];
            }
            catch (Exception ex)
            {
                LogError("GetForClientId", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in ClientSearchResult.GetForClientId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return retVal;
        }

        /// <summary>
        /// Method to return a typed search result for each client in the database.
        /// This is for use when populating the list on the client page
        /// </summary>
        /// <returns>List of Types.ClientSearchResult</returns>
        public List<Types.ClientSearchResult> GetAll()
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Client_All", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAll", "None", ex);
                Console.WriteLine("Error in ClientSearchResult.GetAll: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateTypedResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all Clients that have a firstname, surname or business name like the specified parameter
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForNameLike(string name)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Client_Name", new SqlParameter[] { new SqlParameter("@Name", name) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForNameLike", string.Format("name: {0}", name), ex);
                Console.WriteLine("Error in ClientSearchResult.GetForNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }
        public List<SearchResult> GetForNameFull(string name)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Client_Full", new SqlParameter[] { new SqlParameter("@Name", name) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForNameFull", string.Format("name: {0}", name), ex);
                Console.WriteLine("Error in ClientSearchResult.GetForNameFull: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }


        public List<SearchResult> GetForIdOrAccount(string filter)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Client_IdOrAccount", new SqlParameter[] { new SqlParameter("@Filter", filter) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForIdOrAccount", string.Format("filter: {0}", filter), ex);
                Console.WriteLine("Error in ClientSearchResult.GetForIdOrAccount: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all Clients that have an address like the specified parameter
        /// </summary>
        /// <param name="address">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForAddressLike(string address)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Client_Address", new SqlParameter[] { new SqlParameter("@Address", address) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForAddressLike", string.Format("address: {0}", address), ex);
                Console.WriteLine("Error in ClientSearchResult.GetForAddressLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all Clients that have a postcode like the specified parameter
        /// </summary>
        /// <param name="postCode">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForPostCodeLike(string postCode)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Client_PostCode", new SqlParameter[] { new SqlParameter("@PostCode", postCode) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForPostCodeLike", string.Format("postCode: {0}", postCode), ex);
                Console.WriteLine("Error in ClientSearchResult.GetForPostCodeLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all Clients that have a telephone number like the specified parameter
        /// </summary>
        /// <param name="telephone">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForTelephoneLike(string telephone)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Client_Telephone", new SqlParameter[] { new SqlParameter("@Telephone", telephone) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForTelephoneLike", string.Format("telephone: {0}", telephone), ex);
                Console.WriteLine("Error in ClientSearchResult.GetForTelephoneLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        /// <summary>
        /// Search for all Clients that have an email like the specified parameter
        /// </summary>
        /// <param name="email">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForEmailLike(string email)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Client_Email", new SqlParameter[] { new SqlParameter("@Email", email) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForEmailLike", string.Format("email: {0}", email), ex);
                Console.WriteLine("Error in ClientSearchResult.GetForEmailLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateGenericResultsFromDataSet(ds);
        }

        public List<SearchResult> GetForJobId(string _jobId)
        {
            DataSet ds = null;

            int jobId = 0;
            bool res = int.TryParse(_jobId, out jobId);
            jobId = res ? jobId : -1;

            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Client_JobId", new SqlParameter[] { new SqlParameter("JobId", jobId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJobId", string.Format("_jobId: {0}", _jobId), ex);
                Console.WriteLine("Error in ClientSearchResult.GetForJobId: {0}", ex.ToString());
            }

            return PopulateGenericResultsFromDataSet(ds);
        }
        #endregion

        #region Private
        /// <summary>
        /// Helper method to validate a dataset and create list of typed search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.ClientSearchResult</returns>
        private List<Types.ClientSearchResult> PopulateTypedResultsFromDataSet(DataSet ds)
        {
            List<Types.ClientSearchResult> retVal = new List<Types.ClientSearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.ClientSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateTypedResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ClientSearchResult.PopulateTypedResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to validate a dataset and create list of base search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.SearchResult</returns>
        private List<Types.SearchResult> PopulateGenericResultsFromDataSet(DataSet ds)
        {
            List<Types.SearchResult> retVal = new List<Types.SearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.ClientSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateGenericResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ClientSearchResult.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row.
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>Types.ClientSearchResult</returns>
        private Types.ClientSearchResult CreateSearchResult(DataRow row)
        {
            Types.ClientSearchResult obj = new Types.ClientSearchResult();
            obj.Id = this.IntValue(row, "Id");
            obj.ParentId = -1; //A client has no parent
            obj.Name = this.StringValue(row, "Name");
            obj.Address = this.StringValue(row, "Address");
            obj.Telephone = this.StringValue(row, "Telephone");
            obj.Email = this.StringValue(row, "Email");
            obj.QuoteCount = this.IntValue(row, "QuoteCount");
            obj.ClientType = (ClientTypeEnum)this.IntValue(row, "ClientType");
            return obj;
        }
        #endregion
        #endregion
    }
}
