﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class JobSearchResult: Contracts.DBContextBase
    {
        #region Members
        string _CultureCode = "en-GB";
        #endregion

        #region Constructors
        public JobSearchResult(string databaseName)
            : base(databaseName, "JobSearchResult", DateTime.Parse("1970-01-01"))
        {
        }

        public JobSearchResult(string databaseName, string countryCode)
            : base(databaseName, "JobSearchResult")
        {
            _CultureCode = countryCode;
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public

        public List<SearchResult> GetPendingForDashboard()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Job_PendingForDashboard", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetPendingForDashboard", "None", ex);
                Console.WriteLine("Error in JobSearchResult.GetPendingForDashboard: {0}", ex.ToString());
            }

            return ToGenericObjectList(ds);
        }

        public List<SearchResult> GetForId(int jobId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Job_Id", new SqlParameter[] { new SqlParameter("@JobId", jobId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("jobId: {0}", jobId), ex);
                Console.WriteLine("Error in JobSearchResult.GetForId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }
        /// <summary>
        /// Search for all quotes for a client Id
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForClientId(int clientId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Job_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientId", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in JobSearchResult.GetForClientId: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that are for a client, contact or business name containing the specified string
        /// </summary>
        /// <param name="name">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForNameLike(string name)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Job_NameLike", new SqlParameter[] { new SqlParameter("@Name", name) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForNameLike", string.Format("name: {0}", name), ex);
                Console.WriteLine("Error in JobSearchResult.GetForClientNameLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that have a site address containing the specified string
        /// </summary>
        /// <param name="address">string</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForSiteAddressLike(string address)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Job_SiteAddress", new SqlParameter[] { new SqlParameter("@Address", address) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForSiteAddressLike", string.Format("address: {0}", address), ex);
                Console.WriteLine("Error in JobSearchResult.GetForSiteAddressLike: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that are set to a specified status
        /// </summary>
        /// <param name="status">Enums.InvoiceStatusEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForStatus(JobStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Job_Status", new SqlParameter[] { new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatus", string.Format("status: {0}", (int)status), ex);
                Console.WriteLine("Error in JobSearchResult.GetForStatus: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }

        /// <summary>
        /// Search for all quotes that are not set to a specified status
        /// </summary>
        /// <param name="status">Enums.InvoiceStatusEnum</param>
        /// <returns>List of SearchResult objects</returns>
        public List<SearchResult> GetForStatusNot(InvoiceStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Job_StatusNot", new SqlParameter[] { new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatus", string.Format("status: {0}", (int)status), ex);
                Console.WriteLine("Error in JobSearchResult.GetForStatusNot: {0}", ex.ToString());
            }
            //Return the populated search result list
            return ToGenericObjectList(ds);
        }
        #endregion

        #region Internal
        /// <summary>
        /// Internal method to be used to populate a Client model Quotes property
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of Types.JobSearchResult</returns>
        internal List<Types.JobSearchResult> GetForClientModel(int clientId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Job_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClientModel", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in JobSearchResult.GetForClientModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateTypedResultsFromDataSet(ds);

        }

        /// <summary>
        /// Internal method to be used to populate a Job model Quotes property
        /// </summary>
        /// <param name="jobId">int</param>
        /// <returns>List of Types.JobSearchResult</returns>
        internal List<Types.JobSearchResult> GetForJobModel(int jobId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Job_JobId", new SqlParameter[] { new SqlParameter("@jobId", jobId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJobModel", string.Format("jobId: {0}", jobId), ex);
                Console.WriteLine("Error in JobSearchResult.GetForJobModel: {0}", ex.ToString());
            }
            //Return the populated search result list
            return PopulateTypedResultsFromDataSet(ds);

        }

        /// <summary>
        /// Internal async method to be used to populate a Client model Quotes property
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>A Task that returns List of Types.JobSearchResult</returns>
        internal Task<List<Types.JobSearchResult>> GetForClientModelAsync(int clientId)
        {
            var taskResult = new TaskCompletionSource<List<Types.JobSearchResult>>();
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_Job_ClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                //Return the populated search result list
                taskResult.SetResult(PopulateTypedResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetForClientModelAsync", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in JobSearchResult.GetForClientModelAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion

        #region Private
        /// <summary>
        /// Helper method to validate a dataset and create list of typed search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.ClientSearchResult</returns>
        private List<Types.JobSearchResult> PopulateTypedResultsFromDataSet(DataSet ds)
        {
            List<Types.JobSearchResult> retVal = new List<Types.JobSearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;


            var cultureCode = GetCultureCodeForClient(this.DatabaseName);

            Types.JobSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r, cultureCode);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateTypedResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in JobSearchResult.PopulateTypedResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to validate a dataset and create list of base search results
        /// </summary>
        /// <param name="ds">Dataset</param>
        /// <returns>A List of Types.SearchResult</returns>
        private List<Types.SearchResult> ToGenericObjectList(DataSet ds)
        {
            List<Types.SearchResult> retVal = new List<Types.SearchResult>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            var cultureCode = GetCultureCodeForClient(this.DatabaseName);

            Types.JobSearchResult obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateSearchResult(r,cultureCode);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToGenericObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in JobSearchResult.ToGenericObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row.
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>Types.JobSearchResult</returns>
        private Types.JobSearchResult CreateSearchResult(DataRow row, string cultureCode)
        {
            Types.JobSearchResult obj = new Types.JobSearchResult(cultureCode);

            obj.Id = this.IntValue(row, "Id");
            obj.ParentId = this.IntValue(row, "ParentId"); //The parent of a job is the client
            obj.ClientName = this.StringValue(row, "ClientName");
            obj.SiteAddress = this.StringValue(row, "SiteAddress");
            obj.ContactName = this.StringValue(row, "ContactName");
            obj.Comments = this.StringValue(row, "Comments");
            obj.Status = (JobStatusEnum)this.IntValue(row, "Status");
            obj.DateCreated = this.DateTimeValue(row, "DateCreated");

            return obj;
        }
        #endregion
        #endregion
    }
}