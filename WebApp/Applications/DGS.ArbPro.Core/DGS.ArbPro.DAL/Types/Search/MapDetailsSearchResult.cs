﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class MapDetailsSearchResult : Contracts.DBContextBase
    {
        #region Constructors
        public MapDetailsSearchResult(string databaseName) : base(databaseName, "MapDetailsSearchResult") { }
        #endregion

        #region Methods
        #region Public
        public Types.MapDetailsSearchResult GetForQuoteSite(int quoteId, int siteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Search_MapDetails_ForQuoteSite", new SqlParameter[] { Parameterize("@QuoteId", quoteId), Parameterize("@SiteId", siteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("int quoteId: {0}, int siteId: {1}", quoteId,siteId), ex);
            }
            return ToObject(GetItemsForQuote(quoteId), ds);
        }
        #endregion
        #region Private
        private List<Types.QuoteItem> GetItemsForQuote(int quoteId)
        {
            QuoteItem itemContext = new QuoteItem(this.DatabaseName);
            return itemContext.GetAllForQuote(quoteId);
        }

        private List<Types.MapDetailsSearchResult> ToObjectList(List<Types.QuoteItem> items, DataSet ds)
        {
            List<Types.MapDetailsSearchResult> retVal = new List<Types.MapDetailsSearchResult>();
            if (!HasRows(ds))
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    Types.MapDetailsSearchResult obj = CreateInstance(r);
                    //Populate the site items
                    obj.SiteItems = items.FindAll(i => i.JobSiteId == obj.Id);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("List<Types.QuoteItem> items: {0}, DataSet ds: {1}", items != null ? items.Count().ToString() : "Null", ds.ToString()), ex);
            }

            return retVal;
        }

        private Types.MapDetailsSearchResult ToObject(List<Types.QuoteItem> items, DataSet ds)
        {
            Types.MapDetailsSearchResult retVal = new Types.MapDetailsSearchResult();
            if (!HasRows(ds))
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
                retVal.SiteItems = items.FindAll(i => i.JobSiteId == retVal.Id);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("List<Types.QuoteItem> items: {0}, DataSet ds: {1}", items != null ? items.Count().ToString() : "Null", ds.ToString()), ex);
                retVal = new Types.MapDetailsSearchResult();
            }

            return retVal;
        }

        private Types.MapDetailsSearchResult CreateInstance(DataRow row)
        {
            Types.MapDetailsSearchResult obj = new Types.MapDetailsSearchResult();
            obj.Id = base.IntValue(row, "SiteId");          //This is the Id of the site being displayed
            obj.ParentId = base.IntValue(row, "QuoteId");   //This is the quote record Id as this is a quote can be for multiple sites
            obj.QuoteRef = base.StringValue(row, "QuoteRef");
            obj.ClientName = base.StringValue(row, "ClientName");
            obj.ContactName = base.StringValue(row, "ContactName");
            obj.Mobile = base.StringValue(row, "Mobile");
            obj.Telephone = base.StringValue(row, "Telephone");
            obj.SiteRef = base.StringValue(row, "SiteRef");
            obj.SiteAddress = base.StringValue(row, "SiteAddress");
            obj.SitePostCode = base.StringValue(row, "SitePostCode");
            return obj;
        }
        #endregion
        #endregion
    }
}
