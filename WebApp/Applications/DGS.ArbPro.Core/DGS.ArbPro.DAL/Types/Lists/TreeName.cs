﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class TreeName: Contracts.DBContextBase
    {
        #region Members

        #endregion

        #region Constructors
        public TreeName(string databaseName)
            : base(databaseName, "Staff", DateTime.Parse("1970-01-01"))
        { }
        #endregion

        #region Properties

        #endregion

        #region Methods
        #region Public
        public List<string> GetCommonList()
        {
            return GetValues("TreeList_GetCommonName");
        }

        public List<string> GetLatinList()
        {
            return GetValues("TreeList_GetLatinName");
        }

        public List<string> GetList()
        {
            return GetValues("TreeList_GetDefaultName");
        }


        public List<Types.TreeName> GetItems()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TreeList_Get", new SqlParameter[] {}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetItems","None", ex);
                Console.WriteLine("Error in TreeName.GetItems: {0}", ex.ToString());
            }
            return PopulateItemsFromDataSet(ds);
        }


        public bool Save(Types.TreeName obj)
        {
            if (obj.Id < 1)
                return Insert(obj);
            else
                return Update(obj);
        }

        public bool Delete(int Id)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("TreeList_Delete", CommandType.StoredProcedure,new SqlParameter[]{new SqlParameter("@Id",Id)}, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Delete", string.Format("Int Id: {0}", Id), ex);
                Console.WriteLine("Error in TreeName.Delete: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Private
        private List<string> GetValues(string procedureName)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet(procedureName, new SqlParameter[] {}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetValues", string.Format("string procedureName: {0}", procedureName), ex);
                Console.WriteLine("Error in TreeName.GetValues: {0}", ex.ToString());
            }
            return PopulateValuesFromDataSet(ds);
        }

        private bool Insert(Types.TreeName obj)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("TreeList_Insert", CommandType.StoredProcedure, GetInsertParameters(obj), this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.TreeName obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in TreeName.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private bool Update(Types.TreeName obj)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("TreeList_Update", CommandType.StoredProcedure, GetUpdateParameters(obj), this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.TreeName obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in TreeName.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetInsertParameters(Types.TreeName obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Common",obj.Common),
                new SqlParameter("@Latin",obj.Latin)
            };
        }

        private SqlParameter[] GetUpdateParameters(Types.TreeName obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",obj.Id),
                new SqlParameter("@Common",obj.Common),
                new SqlParameter("@Latin",obj.Latin)
            };
        }

        private List<string> PopulateValuesFromDataSet(DataSet ds)
        {
            List<string> retVal = new List<string>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add((string)r["Name"]);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateValuesFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in TreeName.PopulateValuesFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.TreeName> PopulateItemsFromDataSet(DataSet ds)
        {
            List<Types.TreeName> retVal = new List<Types.TreeName>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateItemsFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in TreeName.PopulateItemsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.TreeName CreateObject(DataRow row)
        {
            Types.TreeName obj = new Types.TreeName();
            obj.Id = (int)row["Id"];
            obj.Common = (string)row["Common"];
            obj.Latin = (string)row["Latin"];
            return obj;
        }
        #endregion
        #endregion
    }
}

