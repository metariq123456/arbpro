﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class AerialRescuePlan : Contracts.DBContextBase
    {
        #region Constructors
        public AerialRescuePlan(string databaseName) : base(databaseName, "AerialResucePlan") { }
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Method to get a single object based on its Id
        /// </summary>
        /// <param name="Id">The Id of the object to be returned</param>
        /// <returns>An instantiated object: NOTE the object may be a new instance if an error occured or there was no record for the Id</returns>
        public Types.AerialRescuePlan Get(int Id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("AerialRescuePlan_GetForId", new SqlParameter[] {Parameterize("@Id",Id)}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAll", "None", ex);
                ds = null;
            }
            return ToObject(ds);
        }

        /// <summary>
        /// Method to get a list of all entries in the database
        /// </summary>
        /// <returns>List of Aerial rescue plan objects. NOTE the list can be empty if an error occures or there are no records</returns>
        public List<Types.AerialRescuePlan> GetAll()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("AerialRescuePlan_GetAll", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAll", "None", ex);
                ds = null;
            }
            return ToObjectList(ds);
        }

        /// <summary>
        /// Method to update / insert an row in the database
        /// </summary>
        /// <param name="obj">The object to insert or update</param>
        /// <returns>The Id of the affected row</returns>
        public int Save(Types.AerialRescuePlan obj)
        {
            if (obj.Id < 1)
                return Save("AerialRescuePlan_Insert", Parameters(obj, ParameterMode.Insert));
            else
                return Save("AerialRescuePlan_Update", Parameters(obj, ParameterMode.Update));
        }

        /// <summary>
        /// Method to delete an AerialRescuePlan record from the database
        /// </summary>
        /// <param name="obj">The object to be deleted</param>
        /// <returns>True / False based on success of the operation</returns>
        public bool Delete(Types.AerialRescuePlan obj)
        {
            return  Delete(obj.Id);
        }

        /// <summary>
        /// Method to delete an AerialRescuePlan record from the database
        /// </summary>
        /// <param name="Id">The Id of the row to be deleted</param>
        /// <returns>True / False based on success of the operation</returns>
        public bool Delete(int Id)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("AerialRescuePlan_Delete", CommandType.StoredProcedure, new SqlParameter[] { Parameterize("@Id", Id) }, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Delete", string.Format("int Id: {0}", Id), ex);
            }
            return retVal;
        }
        #endregion
        #region Private
        /// <summary>
        /// Method to run a database stored procedure to update or insert a record
        /// </summary>
        /// <param name="procedureName">The name of the stored procedure to be run</param>
        /// <param name="parameters">SQL Parameter array for the values of the object being updated / inserted</param>
        /// <returns>The Id of the affected row</returns>
        private int Save(string procedureName, SqlParameter[] parameters)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(procedureName, parameters, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Save", string.Format("string procedureName: {0}, SqlParameter[] parameters size: {1}",procedureName, parameters.Count()), ex);
            }
            return retVal;
        }

        /// <summary>
        /// Method to gernerate the paramater array to be used by stored procedures
        /// </summary>
        /// <param name="obj">The object instance to create the params for</param>
        /// <param name="mode">The mode of the operation insert / update</param>
        /// <returns>An array of SQL parameters based on the mode of the operation</returns>
        private SqlParameter[] Parameters(Types.AerialRescuePlan obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();

            if(mode == ParameterMode.Update)
                retVal.Add(Parameterize("@Id",obj.Id));

            retVal.Add(Parameterize("@Plan", obj.RescuePlan));

            return retVal.ToArray();
        }

        /// <summary>
        /// Method to create a single object from a dataset
        /// The first row of the first table is used to inflate the object
        /// </summary>
        /// <param name="ds">Dataset returned from the database</param>
        /// <returns></returns>
        private Types.AerialRescuePlan ToObject(DataSet ds)
        {
            Types.AerialRescuePlan retVal = new Types.AerialRescuePlan();

            if (!base.HasRows(ds))
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }

        /// <summary>
        /// Method to create a list of objects from a dataset returned from
        /// the database
        /// </summary>
        /// <param name="ds">Populated dataset</param>
        /// <returns>An instantiated object list</returns>
        private List<Types.AerialRescuePlan> ToObjectList(DataSet ds)
        {
            List<Types.AerialRescuePlan> retVal = new List<Types.AerialRescuePlan>();

            if (!base.HasRows(ds))
                return retVal;
            
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }

        /// <summary>
        /// Method to inflate an object from a dataset row
        /// </summary>
        /// <param name="row">DataRow the row to inflate the object from</param>
        /// <returns>A populated object</returns>
        private Types.AerialRescuePlan CreateInstance(DataRow row)
        {
            Types.AerialRescuePlan obj = new Types.AerialRescuePlan();
            obj.Id = base.IntValue(row, "Id");
            obj.RescuePlan = base.StringValue(row, "AerailPlan");
            return obj;
        }
        #endregion
        #endregion
    }
}
