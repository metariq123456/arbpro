﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class WorkDescription: Contracts.DBContextBase
    {
        #region Members

        #endregion

        #region Constructors
        public WorkDescription(string databaseName)
            : base(databaseName, "WorkDescription", DateTime.Parse("1970-01-01"))
        { }
        #endregion

        #region Properties

        #endregion

        #region Methods
        #region Public

        public List<string> GetList()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkDescription_Get", new SqlParameter[]{}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetList", "None", ex);
                Console.WriteLine("Error in WorkDescription.GetList: {0}", ex.ToString());
            }
            return PopulateValuesFromDataSet(ds);
        }


        public List<Types.WorkDescription> GetItems()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkDescription_Get", new SqlParameter[] {}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetItems", "None", ex);
                Console.WriteLine("Error in WorkDescription.GetItems: {0}", ex.ToString());
            }
            return PopulateItemsFromDataSet(ds);
        }


        public bool Save(Types.WorkDescription obj)
        {
            if (obj.Id < 1)
                return Insert(obj);
            else
                return Update(obj);
        }

        public bool Delete(int Id)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("WorkDescription_Delete", CommandType.StoredProcedure,new SqlParameter[]{new SqlParameter("@Id",Id)}, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Delete", string.Format("Int Id: {0}", Id), ex);
                Console.WriteLine("Error in WorkDescription.Delete: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Private
        private bool Insert(Types.WorkDescription obj)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("WorkDescription_Insert", CommandType.StoredProcedure, GetInsertParameters(obj), this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.WorkDescription obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in WorkDescription.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private bool Update(Types.WorkDescription obj)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("WorkDescription_Update", CommandType.StoredProcedure, GetUpdateParameters(obj), this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.WorkDescription obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in WorkDescription.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetInsertParameters(Types.WorkDescription obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Item",obj.Item),
                new SqlParameter("@TimeRequired",obj.AverageHrsRequired)
            };
        }

        private SqlParameter[] GetUpdateParameters(Types.WorkDescription obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",obj.Id),
                new SqlParameter("@Item",obj.Item),
                new SqlParameter("@TimeRequired",obj.AverageHrsRequired)
            };
        }

        private List<string> PopulateValuesFromDataSet(DataSet ds)
        {
            List<string> retVal = new List<string>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add((string)r["Item"]);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateValuesFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in WorkDescription.PopulateValuesFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.WorkDescription> PopulateItemsFromDataSet(DataSet ds)
        {
            List<Types.WorkDescription> retVal = new List<Types.WorkDescription>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateItemsFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in WorkDescription.PopulateItemsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.WorkDescription CreateObject(DataRow row)
        {
            Types.WorkDescription obj = new Types.WorkDescription();
            obj.Id = (int)row["Id"];
            obj.Item = (string)row["Item"];
            obj.AverageHrsRequired = (decimal)row["TimeRequired"];
            return obj;
        }
        #endregion
        #endregion
    }
}

