﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class PickItem: Contracts.DBContextBase
    {
        #region Members

        #endregion

        #region Constructors
        public PickItem(string databaseName)
            : base(databaseName, "PickItem", DateTime.Parse("1970-01-01"))
        { }
        #endregion

        #region Properties

        #endregion

        #region Methods
        #region Public
        public List<string> GetTitlesList()
        {
            return GetValuesForType(PickItemTypeEnum.Title);
        }

        public List<string> GetTownsList()
        {
            return GetValuesForType(PickItemTypeEnum.Town);
        }

        public List<string> GetCountiesList()
        {
            return GetValuesForType(PickItemTypeEnum.County);
        }

        public List<string> GetStaffTypeList()
        {
            return GetValuesForType(PickItemTypeEnum.StaffType);
        }

        public List<string> GetLeadSourceList()
        {
            return GetValuesForType(PickItemTypeEnum.LeadSource);
        }
        
        public List<string> GetValuesForType(PickItemTypeEnum itemtype)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("PickItem_GetForType", new SqlParameter[] { new SqlParameter("@Type", (int)itemtype) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetValuesForType", string.Format("PickItemTypeEnum itemtype: {0}", itemtype.ToString()), ex);
                Console.WriteLine("Error in PickItem.GetValuesForType: {0}", ex.ToString());
            }
            return PopulateValuesFromDataSet(ds);
        }


        public List<Types.PickItem> GetItemsForType(PickItemTypeEnum itemtype)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("PickItem_GetForType", new SqlParameter[] { new SqlParameter("@Type", (int)itemtype) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetItemsForType", string.Format("PickItemTypeEnum itemtype: {0}", itemtype.ToString()), ex);
                Console.WriteLine("Error in PickItem.GetItemsForType: {0}", ex.ToString());
            }
            return PopulateItemsFromDataSet(ds);
        }


        public bool Save(Types.PickItem obj)
        {
            if (obj.Id < 1)
                return Insert(obj);
            else
                return Update(obj);
        }

        public bool Delete(int Id)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("PickItem_Delete", CommandType.StoredProcedure,new SqlParameter[]{new SqlParameter("@Id",Id)}, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Delete", string.Format("Int Id: {0}", Id), ex);
                Console.WriteLine("Error in PickItem.Delete: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Private
        private bool Insert(Types.PickItem obj)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("PickItem_Insert", CommandType.StoredProcedure, GetInsertParameters(obj), this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.PickItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in PickItem.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private bool Update(Types.PickItem obj)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("PickItem_Update", CommandType.StoredProcedure, GetUpdateParameters(obj), this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.PickItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in PickItem.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetInsertParameters(Types.PickItem obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Type",obj.ItemType),
                new SqlParameter("@Value",obj.Value)
            };
        }

        private SqlParameter[] GetUpdateParameters(Types.PickItem obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",obj.Id),
                new SqlParameter("@Type",obj.ItemType),
                new SqlParameter("@Value",obj.Value)
            };
        }

        private List<string> PopulateValuesFromDataSet(DataSet ds)
        {
            List<string> retVal = new List<string>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add((string)r["Value"]);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateValuesFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in PickItem.PopulateValuesFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.PickItem> PopulateItemsFromDataSet(DataSet ds)
        {
            List<Types.PickItem> retVal = new List<Types.PickItem>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateItemsFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in PickItem.PopulateItemsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.PickItem CreateObject(DataRow row)
        {
            Types.PickItem obj = new Types.PickItem();
            obj.Id = (int)row["Id"];
            obj.ItemType = (PickItemTypeEnum)((int)row["Type"]);
            obj.Value = (string)row["Value"];
            return obj;
        }
        #endregion
        #endregion
    }
}
