﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Tool: Contracts.DBContextBase
    {
        #region Members

        #endregion

        #region Constructors
        public Tool(string databaseName)
            : base(databaseName, "Tool", DateTime.Parse("1970-01-01"))
        { }
        #endregion

        #region Properties

        #endregion

        #region Methods
        #region Public
        public List<string> GetList()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Tool_Get", new SqlParameter[] {}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetValuesForType", "None", ex);
                Console.WriteLine("Error in Tool.GetValuesForType: {0}", ex.ToString());
            }
            return PopulateValuesFromDataSet(ds);
        }

        public List<Types.Tool> GetForWorkOrder(int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Tool_GetForWorkOrder", new SqlParameter[] { new SqlParameter("@QuoteId",quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkOrder", string.Format("quoteId: {0}",quoteId), ex);
                Console.WriteLine("Error in Tool.GetForWorkOrder: {0}", ex.ToString());
            }
            return PopulateItemsFromDataSet(ds);
        }

        public List<Types.Tool> GetItems()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Tool_Get", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetItems", "None", ex);
                Console.WriteLine("Error in Tool.GetItems: {0}", ex.ToString());
            }
            return PopulateItemsFromDataSet(ds);
        }


        public bool Save(Types.Tool obj)
        {
            if (obj.Id < 1)
                return Insert(obj);
            else
                return Update(obj);
        }

        public bool Delete(int Id)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("Tool_Delete", CommandType.StoredProcedure,new SqlParameter[]{new SqlParameter("@Id",Id)}, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Delete", string.Format("Int Id: {0}", Id), ex);
                Console.WriteLine("Error in Tool.Delete: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Private
        private bool Insert(Types.Tool obj)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("Tool_Insert", CommandType.StoredProcedure, GetInsertParameters(obj), this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.Tool obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in Tool.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private bool Update(Types.Tool obj)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("Tool_Update", CommandType.StoredProcedure, GetUpdateParameters(obj), this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.Tool obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in Tool.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetInsertParameters(Types.Tool obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Item",obj.Item),
                new SqlParameter("@Stock",obj.Stock)
            };
        }

        private SqlParameter[] GetUpdateParameters(Types.Tool obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",obj.Id),
                new SqlParameter("@Item",obj.Item),
                new SqlParameter("@Stock",obj.Stock)
            };
        }

        private List<string> PopulateValuesFromDataSet(DataSet ds)
        {
            List<string> retVal = new List<string>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add((string)r["Item"]);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateValuesFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Tool.PopulateValuesFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.Tool> PopulateItemsFromDataSet(DataSet ds)
        {
            List<Types.Tool> retVal = new List<Types.Tool>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateItemsFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Tool.PopulateItemsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.Tool CreateObject(DataRow row)
        {
            Types.Tool obj = new Types.Tool();
            obj.Id = (int)row["Id"];
            obj.Item = (string)row["Item"];
            obj.Stock = (int)row["Stock"];
            obj.Deleted = (bool)row["Deleted"];
            return obj;
        }
        #endregion
        #endregion
    }
}

