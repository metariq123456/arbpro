﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using System.Threading.Tasks;

namespace DGS.ArbPro.DAL
{
    public class Currency : Contracts.DBContextBase
    {
                #region Members

        #endregion

        #region Constructors
        public Currency(string databaseName)
            : base(databaseName, "Currency", DateTime.Parse("1970-01-01"))
        { }
        #endregion

        #region Properties

        #endregion

        #region Methods
        #region Public
        public List<Types.Currency> GetItems()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Currency_Get", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetItems", "None", ex);
                Console.WriteLine("Error in Currency.GetItems: {0}", ex.ToString());
            }
            return PopulateItemsFromDataSet(ds);
        }
        public List<Types.Currency> GetSelectedCurrencyItems()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Currency_GetSelectedCurrency", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetItems", "None", ex);
                Console.WriteLine("Error in Currency.GetSelectedCurrency: {0}", ex.ToString());
            }
            return PopulateItemsFromDataSet(ds);
        }

        public bool SetSelected(int Id)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("Currency_SetSelected", CommandType.StoredProcedure,new SqlParameter[]{new SqlParameter("@Id",Id)}, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("Delete", string.Format("Int Id: {0}", Id), ex);
                Console.WriteLine("Error in Currency.SetSelected: {0}", ex.ToString());
            }
            return retVal;
        }
        
        #endregion
        private List<Types.Currency> PopulateItemsFromDataSet(DataSet ds)
        {
            List<Types.Currency> retVal = new List<Types.Currency>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateItemsFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Tool.PopulateItemsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }
        private Types.Currency CreateObject(DataRow row)
        {
            Types.Currency obj = new Types.Currency();
            obj.Id = (int)row["Id"];
            obj.Name = (string)row["Name"];
            obj.CultureCode = (string)row["CultureCode"];
            obj.Character = (string)row["Character"];
            obj.Selected = (bool)row["Selected"];
            return obj;
        }
        #endregion
    }
}
