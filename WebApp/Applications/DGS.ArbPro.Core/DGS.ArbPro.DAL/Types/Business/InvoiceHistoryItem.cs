﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class InvoiceHistoryItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public InvoiceHistoryItem(string databaseName)
            : base(databaseName, "InvoiceHistoryItem")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #endregion
    }
}
