using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;


namespace DGS.ArbPro.DAL
{
    public class DatabaseUser : Contracts.DBContextBase
    {
        #region Constructors
        public DatabaseUser(string databaseName)
            : base(databaseName, "User", DateTime.Parse("1970-01-01"))
        {
        }
        #endregion
        #region Methods
        #region Public
        public List<Types.DatabaseUser> GetAllDatabaseUser()
        {
            List<Types.DatabaseUser> DatabaseUser;
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("User_Get_All", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in User.ToObject: {0}", ex.ToString());
            }
            DatabaseUser = ToObject(ds);
            return DatabaseUser;
        }
        public Types.DatabaseUser GetCompanyAndAdminUser()
        {
            List<Types.DatabaseUser> DatabaseUserList;
            Types.DatabaseUser DatabaseUser = new Types.DatabaseUser() ;
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Get_Client_And_AdminUser", new SqlParameter[] { }, this.DatabaseName);
                DatabaseUserList = ToObject(ds);
                if(DatabaseUserList.Count > 0)
                {
                    DatabaseUser = DatabaseUserList[0];
                }
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in User.ToObject: {0}", ex.ToString());
            }
           
            return DatabaseUser;
        }
        #endregion
        #region Private
        private List<Types.DatabaseUser> ToObject(DataSet ds)
        {
            List<Types.DatabaseUser> ReturnValue = new List<Types.DatabaseUser>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return ReturnValue;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    ReturnValue.Add(CreateResult(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in DatabaseUser.ToObject: {0}", ex.ToString());
            }

            return ReturnValue;
        }

        private Types.DatabaseUser CreateResult(DataRow row)
        {
            Types.DatabaseUser obj = new Types.DatabaseUser();
            obj.ClientOrg.Id = IntValue(row, "ClientID");
            obj.ClientOrg.OrganisationName = StringValue(row, "OrganisationName");
            obj.ClientOrg.Address1 = StringValue(row, "Address1");
            obj.ClientOrg.Address2 = StringValue(row, "Address2");
            obj.ClientOrg.Town = StringValue(row, "Town");
            obj.ClientOrg.CountyOrState = StringValue(row, "CountyOrState");
            obj.ClientOrg.PostcodeOrZip = StringValue(row, "PostcodeOrZip");
            obj.ClientOrg.CountryID = IntValue(row, "CountryID");
            obj.ClientOrg.UserAccountsEnabled = BoolValue(row, "UserAccountsEnabled");
            obj.ClientOrg.MobileEditionEnabled = BoolValue(row, "MobileEditionEnabled");
            obj.ClientOrg.NumLicenses = IntValue(row, "NumLicenses");
            obj.UserInfo.Id = IntValue(row, "UserID");
            obj.UserInfo.CompanyID = IntValue(row, "CompanyID");
            obj.UserInfo.EmailAddress = StringValue(row, "EmailAddress");
            obj.UserInfo.Salt = StringValue(row, "Salt");
            obj.UserInfo.PasswordHashed = StringValue(row, "PasswordHashed");
            obj.UserInfo.Title = StringValue(row, "Title");
            obj.UserInfo.FirstName = StringValue(row, "FirstName");
            obj.UserInfo.LastName = StringValue(row, "LastName");
            obj.UserInfo.Telephone = StringValue(row, "Telephone");
            obj.UserInfo.UserAccountEnabled = BoolValue(row, "UserAccountEnabled");
            obj.UserInfo.MobileEditionEnabled = BoolValue(row, "MobileEditionEnabled");
            obj.UserInfo.IsWebEnabled = BoolValue(row, "IsWebEnabled");
            obj.UserInfo.AuthenticationFailures = IntValue(row, "AuthenticationFailures");
            obj.UserInfo.CompanyPosition = StringValue(row, "CompanyPosition");
            obj.UserInfo.TsAndCsAccepted = BoolValue(row, "TsAndCsAccepted");
            obj.UserInfo.Comment = StringValue(row, "Comment");
            obj.UserInfo.CreationDate = DateTimeValue(row, "CreationDate");
            obj.UserInfo.IsOnline = BoolValue(row, "IsOnline");
            obj.UserInfo.LastActivityDate = DateTimeValue(row, "LastActivityDate");
            obj.UserInfo.LastLoginDate = DateTimeValue(row, "LastLoginDate");
            obj.UserInfo.SessionId = StringValue(row, "SessionId");
            return obj;
        }
        #endregion
        #endregion
    }
}
