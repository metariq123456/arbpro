﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class RiskAdditionalItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public RiskAdditionalItem(string databaseName)
            : base(databaseName, "RiskAdditionalItem")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #endregion
    }
}
