﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Quote : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Quote(string database) : base(database, "Quote", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        /// <summary>
        /// Method to get a complete Quote record from the database
        /// The returned object will have the following if any records exist
        ///     populated work sites,
        ///     populated quote search results,
        ///     populated invoice search results
        /// </summary>
        /// <param name="quoteId">int</param>
        /// <returns>A Single populated Quote</returns>
        public Types.Quote GetForId(int quoteId)
        {
            Types.Quote retVal = new Types.Quote();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Quote_GetForID", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
                retVal = ToObject(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in Quote.GetForId: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to get a only Quote record from the database
        /// The returned object will have the following if any records exist
        ///     populated quote search results,
        /// </summary>
        /// <param name="quoteId">int</param>
        /// <returns>A Single populated Quote</returns>
        public Types.Quote GetQuoteOnlyById(int quoteId)
        {
            Types.Quote retVal = new Types.Quote();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("QuoteOnly_ById", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
                retVal = new Types.Quote();
                //Check if there is any data to be returned
                if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return retVal;

                DataRow row = ds.Tables[0].Rows[0];
                retVal.PurchaseOrderNumber = this.StringValue(row, "PurchaseOrderNumber");

            }
            catch (Exception ex)
            {
                LogError("GetQuoteOnlyById", string.Format("quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in Quote.GetQuoteOnlyById: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to delete the Purchase order no from the quote
        /// The returned object will have the following if any records exist
        ///    record is successfully deleted or not,
        /// </summary>
        /// <param name="quoteId">int</param>
        /// <returns>Success status deletion</returns>
        public int DeletePONoFromQuote(int quoteId)
        {
            int retVal = -1;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Delete_PurchaseOrderNo", new SqlParameter[] { new SqlParameter("@Id", quoteId) }, this.DatabaseName);
                //Check if there is any data to be returned
                if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return retVal;

                DataRow row = ds.Tables[0].Rows[0];
                retVal = this.IntValue(row, "Status");

            }
            catch (Exception ex)
            {
                LogError("DeletePONoFromQuote", string.Format("quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in Quote.DeletePONoFromQuote: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to save the Purchase order no on a quote
        /// The returned object will have the following if any records exist
        ///    record is successfully saved or not,
        /// </summary>
        /// <param name="quoteId">int</param>
        /// <param name="purchaseOrderNumber">int</param>
        /// <returns>Success status of PO save or not</returns>
        public int SavePONoOnQuote(int quoteId, string purchaseOrderNumber)
        {
            int retVal = -1;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("UpDate_PurchaseOrderNo", new SqlParameter[] { new SqlParameter("@Id", quoteId), new SqlParameter("@PO_No", purchaseOrderNumber) }, this.DatabaseName);
                //Check if there is any data to be returned
                if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                    return retVal;

                DataRow row = ds.Tables[0].Rows[0];
                retVal = this.IntValue(row, "Status");

            }
            catch (Exception ex)
            {
                LogError("DeletePONoFromQuote", string.Format("quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in Quote.DeletePONoFromQuote: {0}", ex.ToString());
            }
            return retVal;
        }
        /// <summary>
        /// Method to get a list of all quotes for an Enquiry
        /// The returned objects will not have any collections populated
        /// </summary>
        /// <param name="enquiryId">int</param>
        /// <returns>List of Types.Quote</returns>
        public List<Types.Quote> GetForEnquiry(int enquiryId)
        {
            List<Types.Quote> retVal = new List<Types.Quote>();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Quote_GetForEnquiry", new SqlParameter[] { new SqlParameter("@EnquiryId", enquiryId) }, this.DatabaseName);
                retVal = ToObjectList(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForEnquiry", string.Format("enquiryId: {0}", enquiryId), ex);
                Console.WriteLine("Error in Quote.GetForEnquiry: {0}", ex.ToString());
            }
            return retVal;
        }


        public List<Types.Quote> GetForJob(int jobId)
        {
            List<Types.Quote> retVal = new List<Types.Quote>();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Quote_GetForJob", new SqlParameter[] { new SqlParameter("@JobId", jobId) }, this.DatabaseName);
                retVal = ToObjectList(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForJob", string.Format("jobId: {0}", jobId), ex);
                Console.WriteLine("Error in Quote.GetForJob: {0}", ex.ToString());
            }
            return retVal;
        }



        public bool DeletePendingForId(int QuoteId)
        {
            bool retVal = false;
            try
            {

                DBHelper.ExecuteStoredProcRowCount("Quote_DeletePendingForID", new SqlParameter[] { new SqlParameter("@QuoteId", QuoteId) }, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("GetForJob", string.Format("jobId: {0}", QuoteId), ex);
                Console.WriteLine("Error in Quote.GetForJob: {0}", ex.ToString());
                retVal = false;
            }
            return retVal;
        }

        public List<Types.Quote> GetForJobSite(int jobSiteId)
        {
            List<Types.Quote> retVal = new List<Types.Quote>();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Quote_GetForJobSite", new SqlParameter[] { new SqlParameter("@JobSiteId", jobSiteId) }, this.DatabaseName);
                retVal = ToObjectList(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForJobSite", string.Format("jobSiteId: {0}", jobSiteId), ex);
                Console.WriteLine("Error in Quote.GetForJobSite: {0}", ex.ToString());
            }
            return retVal;
        }

        public Types.Quote Duplicate(int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Quote_Duplicate", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Duplicate", string.Format("quoteId: {0}", quoteId), ex);
            }
            Types.Quote retVal = ToObject(ds);
            if (retVal.Id > -1)
            {
                DAL.ItemPicture picDuplicate = new ItemPicture(this.DatabaseName);
                picDuplicate.Duplicate(quoteId, retVal.Id);
            }

            return retVal;
        }

        /// <summary>
        /// Method to create and return a new quote for a specific job number
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        public Types.Quote CreateForJob(int jobId, int jobSiteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Quote_Create_ForJob", new SqlParameter[] { new SqlParameter("@JobId", jobId), new SqlParameter("@JobSiteId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("CreateForJob", string.Format("jobId: {0}, jobSiteId: {1}", jobId, jobSiteId), ex);
            }

            return ToObject(ds);
        }
        
        #endregion
        #region Update Methods
        public int Save(Types.Quote quote)
        {
            if (quote.Id < 0)
                return Insert(quote);
            else
                return Update(quote);
        }

        /// <summary>
        /// Method to update a quote record status if the new status is higher in value than the current status
        /// If the status is set to accepted (3) the quote is promoted to work and
        /// all quote items for the quote are upgraded to work items.
        /// All other status values only update the Quote status.
        /// </summary>
        /// <param name="id">Integer Id of the Quote that is to be updated</param>
        /// <param name="status"><Types.Enums.QuoteStatusEnum/> for the new status of the Quote</param>
        /// <returns>The Id of the updated Quote record</returns>
        public int UpdateStatus(int id, Types.Enums.QuoteStatusEnum status)
        {
            return this.UpdateStatus(id, (int)status);
        }

        /// <summary>
        /// Method to update a quote record status if the new status is higher in value than the current status
        /// If the status is set to accepted (3) the quote is promoted to work and
        /// all quote items for the quote are upgraded to work items.
        /// All other status values only update the Quote status.
        /// </summary>
        /// <param name="id">Integer Id of the Quote that is to be updated</param>
        /// <param name="status">Integer value for the new status of the Quote</param>
        /// <returns>The Id of the updated Quote record</returns>
        public int UpdateStatus(int id, int status)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Quote_UpdateStatus", new SqlParameter[] { Parameterize("@Id", id), Parameterize("@Status", status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("UpdateStatus", string.Format("int id: {0}, int status: {1}", id,status), ex);
                Debug.WriteLine("Error in Quote.UpdateStatus: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #endregion

        #region Private
        /// <summary>
        /// Method to insert a record into the database
        /// </summary>
        /// <param name="site">Types.Quote</param>
        /// <returns>int - The record Id of the inserted object</returns>
        private int Insert(Types.Quote quote)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Quote_Insert", Parameters(quote, ParameterMode.Insert), this.DatabaseName);
                //Only try to insert the Jobsites ifthe Quote was insert correctly
                //if (retVal > 0)
                //{
                //    DAL.JobSite siteDB = new JobSite(this.DatabaseName);
                //    foreach(Types.JobSite s in quote.
                //}
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.Quote quote: {0}", quote.ToString()), ex);
                Console.WriteLine("Error in Quote.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to update a record in the database
        /// </summary>
        /// <param name="site">Types.Quote</param>
        /// <returns>int - The record Id of the updated object</returns>
        private int Update(Types.Quote quote)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Quote_Update", Parameters(quote, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.Quote quote: {0}", quote.ToString()), ex);
                Console.WriteLine("Error in Quote.Update: {0}", ex.ToString());
            }
            return retVal;
        }



        /// <summary>
        /// Private helper method to get a parameter array from a quote object to be passed to an insert
        /// or update procedure
        /// </summary>
        /// <param name="obj">Types.Quote: The object to be inserted or updated</param>
        /// <param name="mode">The mode of operation, insert or update</param>
        /// <returns>An array of parameters to be passed to a stored procedure</returns>
        private SqlParameter[] Parameters(Types.Quote obj, ParameterMode mode)
        {
            var retVal = new List<SqlParameter>();
            //  Add the Id if we are in update mode
            if (mode == ParameterMode.Update)
                retVal.Add(Parameterize("@Id", obj.Id));
            //  Add remaining parameters to the list,
            retVal.Add(Parameterize("@JobSiteId", obj.JobSiteId));
            retVal.Add(Parameterize("@ClientId", obj.ClientId));
            retVal.Add(Parameterize("@ContactId", obj.PointOfContact.Id));
            retVal.Add(Parameterize("@AppointmentId", obj.AppointmentId));
            retVal.Add(Parameterize("@StaffId", obj.SurveyorId));
            retVal.Add(Parameterize("@Status", (int)obj.Status));
            retVal.Add(Parameterize("@Vat",obj.VAT,3,3));
            retVal.Add(Parameterize("@JobId", obj.JobId));

            return retVal.ToArray();
        }

        private Types.Quote ToObject(DataSet ds)
        {
            Types.Quote retVal = new Types.Quote();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                DAL.QuoteItem itmDB = new DAL.QuoteItem(this.DatabaseName);
                DAL.QuoteHistoryItem hstDB = new DAL.QuoteHistoryItem(this.DatabaseName);
                retVal = CreateWorkFlow(ds.Tables[0].Rows[0]);
                if (retVal.Id > 0)
                {
                    retVal.QuotedItems = itmDB.GetAllForQuote(retVal.Id);
                    retVal.History = hstDB.GetDocumentsForQuote(retVal.Id);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Quote.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.Quote> ToObjectList(DataSet ds)
        {
            List<Types.Quote> retVal = new List<Types.Quote>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.Quote obj = null;
            try
            {
                DAL.QuoteItem itmDB = new DAL.QuoteItem(this.DatabaseName);
                DAL.QuoteHistoryItem hstDB = new DAL.QuoteHistoryItem(this.DatabaseName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateWorkFlow(r);
                    if (obj.Id > 0)
                    {
                        obj.QuotedItems = itmDB.GetAllForQuote(obj.Id);
                        //obj.History = hstDB.GetDocumentsForQuote(obj.Id); TODO NO PROC
                    }
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Quote.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.Quote CreateWorkFlow(DataRow row)
        {
            Types.Quote obj = new Types.Quote();
            obj.Id = (int)row["Id"];
            obj.JobSiteId = (int)row["JobSiteId"];
            obj.ClientId = (int)row["ClientId"];
            obj.ClientName = (string)row["ClientName"];
            obj.Status = (QuoteStatusEnum)((int)row["Status"]);
            obj.SurveyorName = (string)row["StaffName"];
            obj.JobId = this.IntValue(row, "JobId");
            obj.DateCreated = this.DateTimeValue(row, "DateCreated");
            obj.DateSent = this.DateTimeValue(row, "DateSent");
            obj.PurchaseOrderNumber = this.StringValue(row, "PurchaseOrderNumber");

            //Create the ClientContact
            obj.PointOfContact = DAL.ContactSearchResult.CreateSearchResult(row);
            //  NOTE: DUE TO UI IMPLEMENTATION THIS IS NO LONGER VALID (1 Quote can be for many sites) -- obj.Site = DAL.ClientSite.CreateObject(row);
            return obj;
        }
        #endregion
        #endregion
    }
}
