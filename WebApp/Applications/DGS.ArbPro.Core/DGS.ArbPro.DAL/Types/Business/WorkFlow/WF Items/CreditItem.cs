﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class CreditItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public CreditItem(string database) : base(database, "CreditItem", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        public List<Types.CreditItem> GetAllForCreditNote(int creditNoteId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("CreditItem_GetForInvoice", new SqlParameter[] { new SqlParameter("CreditNoteId", creditNoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForCreditNote", string.Format("int creditNoteId: {0}", creditNoteId), ex);
                Console.WriteLine("Error in CreditItem.GetAllForCreditNote: {0}", ex.ToString());
            }
            //Return the populated CreditItem list
            return PopulateResultsFromDataSet(ds);
        }

        public List<Types.CreditItem> GetAllForJobSite(int jobSiteId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("CreditItem_GetForJobSite", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSite", string.Format("int jobSiteId: {0}", jobSiteId), ex);
                Console.WriteLine("Error in CreditItem.GetAllForJobSite: {0}", ex.ToString());
            }
            //Return the populated CreditItem list
            return PopulateResultsFromDataSet(ds);
        }

        public List<Types.CreditItem> GetAllForJobSite(int jobSiteId, ItemStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("CreditItem_GetForJobSiteAndStatus", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId), new SqlParameter("Status", status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSite", string.Format("int jobSiteId: {0}, ItemStatusEnum status: {1}", jobSiteId, status.ToString()), ex);
                Console.WriteLine("Error in CreditItem.GetAllForJobSite: {0}", ex.ToString());
            }
            //Return the populated CreditItem list
            return PopulateResultsFromDataSet(ds);
        }

        public Task<List<Types.CreditItem>> GetAllForCreditNoteAsync(int creditNoteId)
        {
            var taskResult = new TaskCompletionSource<List<Types.CreditItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("CreditItem_GetForInvoice", new SqlParameter[] { new SqlParameter("CreditNoteId", creditNoteId) }, this.DatabaseName);
                taskResult.SetResult(PopulateResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForCreditNoteAsync", string.Format("int creditNoteId: {0}", creditNoteId), ex);
                Console.WriteLine("Error in CreditItem.GetAllForCreditNoteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }

            return taskResult.Task;
        }

        public Task<List<Types.CreditItem>> GetAllForJobSiteAsync(int jobSiteId)
        {
            var taskResult = new TaskCompletionSource<List<Types.CreditItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("CreditItem_GetForJobSite", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId) }, this.DatabaseName);
                taskResult.SetResult(PopulateResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSiteAsync", string.Format("int jobSiteId: {0}", jobSiteId), ex);
                Console.WriteLine("Error in CreditItem.GetAllForJobSiteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }

        public Task<List<Types.CreditItem>> GetAllForJobSiteAsync(int jobSiteId, ItemStatusEnum status)
        {
            var taskResult = new TaskCompletionSource<List<Types.CreditItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("CreditItem_GetForJobSiteAndStatus", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId), new SqlParameter("Status", status) }, this.DatabaseName);
                taskResult.SetResult(PopulateResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSiteAsync", string.Format("int jobSiteId: {0}, ItemStatusEnum status: {1}", jobSiteId, status.ToString()), ex);
                Console.WriteLine("Error in CreditItem.GetAllForJobSiteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion
        #region Update Methods
        public int Save(Types.CreditItem obj)
        {
            if (obj.Id < 0)
                return InsertCreditItem(obj);
            else
                return UpdateCreditItem(obj);
        }
        #endregion
        #endregion
        #region Private
        private int InsertCreditItem(Types.CreditItem obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("CreditItem_Insert", GetInsertParamaters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("InsertCreditItem", string.Format("Types.CreditItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in CreditItem.InsertCreditItem: {0}", ex.ToString());
            }
            return retVal;
        }

        private int UpdateCreditItem(Types.CreditItem obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("CreditItem_Update", GetUpdateParamaters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("UpdateCreditItem", string.Format("Types.CreditItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in CreditItem.UpdateCreditItem: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetInsertParamaters(Types.CreditItem obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter ("CreditNoteId",obj.CreditNoteId),
                new SqlParameter ("InvoiceItemId",obj.InvoiceItemId),
                new SqlParameter ("Value",obj.Value),
                new SqlParameter ("VatRate",obj.VatRate ),
                new SqlParameter ("Status",obj.Status)
            };

            return parameters;
        }

        private SqlParameter[] GetUpdateParamaters(Types.CreditItem obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter ("Id",obj.Id),
                new SqlParameter ("CreditNoteId",obj.CreditNoteId),
                new SqlParameter ("InvoiceItemId",obj.InvoiceItemId),
                new SqlParameter ("Value",obj.Value),
                new SqlParameter ("VatRate",obj.VatRate ),
                new SqlParameter ("Status",obj.Status)
            };

            return parameters;
        }

        private List<Types.CreditItem> PopulateResultsFromDataSet(DataSet ds)
        {
            List<Types.CreditItem> retVal = new List<Types.CreditItem>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.CreditItem obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateItem(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in CreditItem.PopulateResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        internal static Types.CreditItem CreateItem(DataRow row)
        {
            Types.CreditItem obj = new Types.CreditItem();
            obj.Id = (int)row["Id"];
            obj.CreditNoteId = (int)row["CreditNoteId"];
            obj.JobId = (int)row["JobId"];
            obj.JobSiteId = (int)row["JobSiteId"];
            obj.TPOId = (int)row["TPOId"];
            obj.Value = (decimal)row["Value"];
            obj.VatRate = (decimal)row["VatRate"];
            obj.InvoiceItemId = (int)row["InvoiceItemId"];
            obj.Name = (string)row["Name"];
            obj.Description = (string)row["WorkDescription"];
            obj.Code = (string)row["Code"];
            obj.Latitude = (double)row["Latitude"];
            obj.Longitude = (double)row["Longitude"];
            obj.Status = (ItemStatusEnum)((int)row["Status"]);
            return obj;
        }
        #endregion
        #endregion
    }
}
