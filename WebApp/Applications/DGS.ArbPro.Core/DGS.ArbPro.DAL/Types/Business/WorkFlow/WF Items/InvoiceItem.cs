﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class InvoiceItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public InvoiceItem(string database) : base(database, "InvoiceItem", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        public Types.InvoiceItem GetForId(int itemId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("InvoiceItem_GetForId", new SqlParameter[] { new SqlParameter("ItemId", itemId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int itemId: {0}", itemId), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllForInvoice: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return PopulateResultsFromDataSet(ds).FirstOrDefault();
        }

        public List<Types.InvoiceItem> GetAllForInvoice(int invoiceId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("InvoiceItem_GetForInvoice", new SqlParameter[] { new SqlParameter("InvoiceId", invoiceId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForInvoice", string.Format("int invoiceId: {0}", invoiceId), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllForInvoice: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return PopulateResultsFromDataSet(ds);
        }

        public List<Types.InvoiceItem> GetAllForJobSite(int jobSiteId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("InvoiceItem_GetForJobSite", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSite", string.Format("int jobSiteId: {0}", jobSiteId), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllForJobSite: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return PopulateResultsFromDataSet(ds);
        }

        public List<Types.InvoiceItem> GetAllForJobSite(int jobSiteId, ItemStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("InvoiceItem_GetForJobSiteAndStatus", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId), new SqlParameter("Status", status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSite", string.Format("int jobSiteId: {0}, ItemStatusEnum status: {1}", jobSiteId, status.ToString()), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllForJobSite: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return PopulateResultsFromDataSet(ds);
        }

        public Task<List<Types.InvoiceItem>> GetAllForInvoiceAsync(int invoiceId)
        {
            var taskResult = new TaskCompletionSource<List<Types.InvoiceItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("InvoiceItem_GetForInvoice", new SqlParameter[] { new SqlParameter("InvoiceId", invoiceId) }, this.DatabaseName);
                taskResult.SetResult(PopulateResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForInvoiceAsync", string.Format("int invoiceId: {0}", invoiceId), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllForInvoiceAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }

            return taskResult.Task;
        }

        public Task<List<Types.InvoiceItem>> GetAllForJobSiteAsync(int jobSiteId)
        {
            var taskResult = new TaskCompletionSource<List<Types.InvoiceItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("InvoiceItem_GetForJobSite", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId) }, this.DatabaseName);
                taskResult.SetResult(PopulateResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSiteAsync", string.Format("int jobSiteId: {0}", jobSiteId), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllForJobSiteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }

        public Task<List<Types.InvoiceItem>> GetAllForJobSiteAsync(int jobSiteId, ItemStatusEnum status)
        {
            var taskResult = new TaskCompletionSource<List<Types.InvoiceItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("InvoiceItem_GetForJobSiteAndStatus", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId), new SqlParameter("Status", status) }, this.DatabaseName);
                taskResult.SetResult(PopulateResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSiteAsync", string.Format("int jobSiteId: {0}, ItemStatusEnum status: {1}", jobSiteId, status.ToString()), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllForJobSiteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion
        #region Update Methods
        public int Save(Types.InvoiceItem obj)
        {
            obj.Status = ItemStatusEnum.Invoiced;
            if (obj.Id < 0)
                return InsertInvoiceItem(obj);
            else
                return UpdateInvoiceItem(obj);
        }

        public int DeleteItem(int Id)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("InvoiceItem_Delete", new SqlParameter[] { new SqlParameter("@Id", Id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("DeleteItem", string.Format("int Id: {0}", Id.ToString()), ex);
                Console.WriteLine("Error in InvoiceItem.DeleteItem: {0}", ex.ToString());
            }
            return retVal;
        }

        public int CancelAllForInvoice(int invoiceId)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("InvoiceItem_CancelForInvoice", new SqlParameter[] { new SqlParameter("@InvoiceId", invoiceId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("CancelAllForInvoice", string.Format("int Id: {0}", invoiceId.ToString()), ex);
                Console.WriteLine("Error in InvoiceItem.CancelAllForInvoice: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #endregion
        #region Private
        private int InsertInvoiceItem(Types.InvoiceItem obj)
        {
            int retVal = -1;
            try
            {
                //Check if the Job item needs to be created
                if (obj.JobItemId < 1)
                {
                    DAL.JobItem itmContext = new DAL.JobItem(this.DatabaseName);
                    obj.JobItemId = itmContext.Save(obj.ToJobItem());
                }
                retVal = DBHelper.ExecuteStoredProcReturnID("InvoiceItem_Insert", GetInsertParamaters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("InsertInvoiceItem", string.Format("Types.InvoiceItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in InvoiceItem.InsertInvoiceItem: {0}", ex.ToString());
            }
            return retVal;
        }

        private int UpdateInvoiceItem(Types.InvoiceItem obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("InvoiceItem_Update", GetUpdateParamaters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("UpdateInvoiceItem", string.Format("Types.InvoiceItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in InvoiceItem.UpdateInvoiceItem: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetInsertParamaters(Types.InvoiceItem obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter ("InvoiceId",obj.InvoiceId),
                new SqlParameter ("JobItemId", obj.JobItemId),
                new SqlParameter ("WorkItemId",obj.WorkItemId),
                new SqlParameter ("Value",obj.Value),
                new SqlParameter ("VatRate",obj.VatRate ),
                new SqlParameter ("WorkDescription", obj.Description),
                new SqlParameter ("Status",obj.Status),
                new SqlParameter ("OrderId",obj.OrderId)
            };

            return parameters;
        }

        private SqlParameter[] GetUpdateParamaters(Types.InvoiceItem obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter ("Id",obj.Id),
                new SqlParameter ("InvoiceId",obj.InvoiceId),
                new SqlParameter("JobItemId", obj.JobItemId),
                new SqlParameter ("WorkItemId",obj.WorkItemId),
                new SqlParameter("Code", obj.Code),
                new SqlParameter("Name", obj.Name),
                new SqlParameter("WorkDescription", obj.Description),
                new SqlParameter ("Value",obj.Value),
                new SqlParameter ("VatRate",obj.VatRate ),
                new SqlParameter ("Status",obj.Status),
                new SqlParameter ("OrderId",obj.OrderId)
            };

            return parameters;
        }

        private List<Types.InvoiceItem> PopulateResultsFromDataSet(DataSet ds)
        {
            List<Types.InvoiceItem> retVal = new List<Types.InvoiceItem>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.InvoiceItem obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateItem(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in InvoiceItem.PopulateResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }
        private List<Types.InvoiceItem> PopulateResultsFromDataSetForInvoice(DataSet ds)
        {
            List<Types.InvoiceItem> retVal = new List<Types.InvoiceItem>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.InvoiceItem obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateItem(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in InvoiceItem.PopulateResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }


        public List<Types.InvoiceItem> GetAllInvoicableForQuote(int quoteId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("InvoiceItem_GetInvoiceable", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllInvoicableForQuote", string.Format("int quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllInvoicableForQuote: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return PopulateResultsFromDataSet(ds);
        }

        internal static Types.InvoiceItem CreateItem(DataRow row)
        {
            Types.InvoiceItem obj = new Types.InvoiceItem();
            obj.Id = (int)row["Id"];
            obj.WorkItemId = (int)row["WorkItemId"];
            obj.InvoiceId = (int)row["InvoiceId"];
            obj.JobId = (int)row["JobId"];
            obj.JobItemId = (int)row["JobItemId"];
            obj.JobSiteId = (int)row["JobSiteId"];
            obj.TPOId = (int)row["TPOId"];
            obj.OrderId = (int)row["OrderId"];
            obj.Value = (decimal)row["Value"];
            obj.VatRate = (decimal)row["VatRate"];
            obj.Name = (string)row["Name"];
            obj.Description = (string)row["WorkDescription"];
            obj.Code = (string)row["Code"];
            obj.Latitude = (double)row["Latitude"];
            obj.Longitude = (double)row["Longitude"];
            obj.Status = (ItemStatusEnum)((int)row["Status"]);
            return obj;
        }
        #endregion
        #endregion
    }
}