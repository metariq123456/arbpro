﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class JobItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public JobItem(string database) : base(database, "JobItem", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        public Types.JobItem GetForId(int id)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobItem_GetForId", new SqlParameter[] { new SqlParameter("Id", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int id: {0}", id), ex);
                Console.WriteLine("Error in JobItem.GetForId: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return ToObject(ds);
        }


          public List<Types.JobItem> GetAllForClientSite(int clientSite)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobItem_GetForClientSite", new SqlParameter[] { new SqlParameter("ClientSite", clientSite) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForClientSite", string.Format("int clientSite: {0}", clientSite), ex);
                Console.WriteLine("Error in JobItem.GetAllForClientSite: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return ToObjectList(ds);
        }

        /// <summary>
        /// Method to get all items for a Job Site
        /// </summary>
        /// <param name="jobSiteId">int - The id of the job site</param>
        /// <returns>List of Types.JobItem</returns>
        public List<Types.JobItem> GetAllForJobSite(int jobSiteId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobItem_GetForJobSite", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSite", string.Format("int jobSiteId: {0}", jobSiteId), ex);
                Console.WriteLine("Error in JobItem.GetAllForJobSite: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return ToObjectList(ds);
        }
        

        /// <summary>
        /// Method to get all items for a Job Site that have a status that is less than or equal to a specified status
        /// </summary>
        /// <param name="jobSiteId">int - The Job site Id</param>
        /// <param name="status">ItemStatusEnum - The status to get view</param>
        /// <returns></returns>
        public List<Types.JobItem> GetAllForJobSite(int jobSiteId, ItemStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobItem_GetForJobSiteAndStatus", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId), new SqlParameter("Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSite", string.Format("int jobSiteId: {0}, ItemStatusEnum status: {1}", jobSiteId, status.ToString()), ex);
                Console.WriteLine("Error in JobItem.GetAllForJobSite: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return ToObjectList(ds);
        }

        /// <summary>
        /// Async method to get all items for a Job Site
        /// </summary>
        /// <param name="jobSiteId">int</param>
        /// <returns>Task containing a List of Types.JobItem</returns>
        public Task<List<Types.JobItem>> GetAllForJobSiteAsync(int jobSiteId)
        {
            var taskResult = new TaskCompletionSource<List<Types.JobItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobItem_GetForJobSite", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId) }, this.DatabaseName);
                taskResult.SetResult(ToObjectList(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSiteAsync", string.Format("int jobSiteId: {0}", jobSiteId), ex);
                Console.WriteLine("Error in JobItem.GetAllForJobSiteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }

        /// <summary>
        /// Async method to get all items for a Job Site that have a status that is less than or equal to a specified status
        /// </summary>
        /// <param name="jobSiteId">int</param>
        /// <param name="status">ItemStatusEnum</param>
        /// <returns>Task containing a List of Types.JobItem</returns>
        public Task<List<Types.JobItem>> GetAllForJobSiteAsync(int jobSiteId, ItemStatusEnum status)
        {
            var taskResult = new TaskCompletionSource<List<Types.JobItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobItem_GetForJobSiteAndStatus", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId), new SqlParameter("Status", status) }, this.DatabaseName);
                taskResult.SetResult(ToObjectList(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSiteAsync", string.Format("int jobSiteId: {0}, ItemStatusEnum status: {1}", jobSiteId, status.ToString()), ex);
                Console.WriteLine("Error in JobItem.GetAllForJobSiteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion
        #region Update Methods
        /// <summary>
        /// Method to insert or update a JobItem record
        /// </summary>
        /// <param name="obj">JobItem - The object to be saved</param>
        /// <returns>int - The record Id of the effected row</returns>
        public int Save(Types.JobItem obj)
        {
            if (obj.Id < 0)
                return InsertJobItem(obj);
            else
                return UpdateJobItem(obj);
        }
        #endregion
        #endregion
        #region Private
        /// <summary>
        /// Method to insert a new JobItem record
        /// </summary>
        /// <param name="obj">Types.JobItem - The object to be inserted</param>
        /// <returns>The record Id of the new row</returns>
        private int InsertJobItem(Types.JobItem obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("JobItem_Insert", GetInsertParamaters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("InsertJobItem", string.Format("Types.InvoiceItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in JobItem.InsertJobItem: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to update an existsing JobItem record
        /// </summary>
        /// <param name="obj">Types.JobItem - The object to be updated</param>
        /// <returns>The record Id of the effected row</returns>
        private int UpdateJobItem(Types.JobItem obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("JobItem_Update", GetUpdateParamaters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("UpdateJobItem", string.Format("Types.InvoiceItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in JobItem.UpdateJobItem: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetInsertParamaters(Types.JobItem obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter ("JobId",obj.JobId ),
                new SqlParameter ("JobSiteId",obj.JobSiteId ),
                new SqlParameter ("TPOId",obj.TPOId ),
                new SqlParameter ("PreservationStatus",(int)obj.PreservationStatus),
                new SqlParameter ("Code",obj.Code ),
                new SqlParameter ("Name",obj.Name ),
                new SqlParameter ("WorkDescription",obj.Description ),
                new SqlParameter ("Value",obj.Value ),
                new SqlParameter ("Latitude",obj.Latitude ),
                new SqlParameter ("Longitude",obj.Longitude ),
                new SqlParameter ("Status",(int)obj.Status),
                new SqlParameter ("UploadItemGuid", DBNull.Value),
                new SqlParameter ("VatRate", obj.VatRate)
            };

            return parameters;
        }

        private SqlParameter[] GetUpdateParamaters(Types.JobItem obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter ("Id",obj.Id ),
                new SqlParameter ("JobId",obj.JobId ),
                new SqlParameter ("JobSiteId",obj.JobSiteId ),
                new SqlParameter ("TPOId",obj.TPOId ),
                new SqlParameter ("Code",obj.Code ),
                new SqlParameter ("Name",obj.Name ),
                new SqlParameter ("WorkDescription",obj.Description ),
                new SqlParameter ("Value",obj.Value ),
                new SqlParameter ("Latitude",obj.Latitude ),
                new SqlParameter ("Longitude",obj.Longitude ),
                new SqlParameter ("Status",(int)obj.Status),
                new SqlParameter ("VatRate", obj.VatRate),
                new SqlParameter ("UploadItemGuid", DBNull.Value),
                new SqlParameter("PreservationStatus",(int)obj.PreservationStatus)
            };

            return parameters;
        }

        private Types.JobItem ToObject(DataSet ds)
        {
            Types.JobItem retVal = new Types.JobItem();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateItem(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in JobItem.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.JobItem> ToObjectList(DataSet ds)
        {
            List<Types.JobItem> retVal = new List<Types.JobItem>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.JobItem obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateItem(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in JobItem.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        internal static Types.JobItem CreateItem(DataRow row)
        {
            Types.JobItem obj = new Types.JobItem();
            obj.Id = (int)row["Id"];
            obj.JobId = (int)row["JobId"];
            obj.JobSiteId = (int)row["JobSiteId"];
            obj.TPOId = (int)row["TPOId"];
            obj.Value = (decimal)row["Value"];
            obj.VatRate = (decimal)row["VatRate"];
            obj.Name = (string)row["Name"];
            obj.Description = (string)row["WorkDescription"];
            obj.Code = (string)row["Code"];
            obj.Latitude = (double)row["Latitude"];
            obj.Longitude = (double)row["Longitude"];
            obj.Status = (ItemStatusEnum)((int)row["Status"]);
            obj.PreservationStatus = (TreeProtectionStatusEnum)((int)row["PreservationStatus"]);
            return obj;
        }
        #endregion
        #endregion
    }
}
