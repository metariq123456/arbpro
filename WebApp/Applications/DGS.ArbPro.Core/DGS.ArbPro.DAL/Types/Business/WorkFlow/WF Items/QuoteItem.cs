﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class QuoteItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public QuoteItem(string databaseName)
            : base(databaseName, "QuoteItem", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        public Types.QuoteItem GetForId(int itemId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("QuoteItem_GetForId", new SqlParameter[] { new SqlParameter("ItemId", itemId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int itemId: {0}", itemId), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllForInvoice: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return PopulateResultsFromDataSet(ds).FirstOrDefault();
        }

        /// <summary>
        /// Method to get all quote items for a quote
        /// </summary>
        /// <param name="quoteId">int - The Id of the Quote</param>
        /// <returns>Task containing a List of Types.QuoteItem</returns>
        public List<Types.QuoteItem> GetAllForQuote(int quoteId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("QuoteItem_GetForQuote", new SqlParameter[] { new SqlParameter("QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForInvoice", string.Format("int quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllForInvoice: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return PopulateResultsFromDataSet(ds);
        }


        public List<Types.QuoteItem> GetAllForQuoteAndStatus(int quoteId, ItemStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("QuoteItem_GetForQuoteAndStatus", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId), new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForQuoteAndStatus", string.Format("int quoteId: {0} ItemStatusEnum status: {1}", quoteId, status.ToString()), ex);
                Console.WriteLine("Error in QuoteItem.GetAllForQuoteAndStatus: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return PopulateResultsFromDataSet(ds);
        }

        /// <summary>
        /// Method to get all quote items for a Job Site
        /// </summary>
        /// <param name="jobSiteId">int - The Id of the JobSite</param>
        /// <returns>Task containing a List of Types.QuoteItem</returns>
        public List<Types.QuoteItem> GetAllForJobSite(int jobSiteId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("QuoteItem_GetForJobSite", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSite", string.Format("int jobSiteId: {0}", jobSiteId), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllForJobSite: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return PopulateResultsFromDataSet(ds);
        }

        /// <summary>
        /// Method to get all quote items of a specified status for a Job Site
        /// </summary>
        /// <param name="jobSiteId">int - The Id of the JobSite</param>
        /// <param name="status">ItemStatusEnum</param>
        /// <returns>Task containing a List of Types.QuoteItem</returns>
        public List<Types.QuoteItem> GetAllForJobSite(int jobSiteId, ItemStatusEnum status)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("QuoteItem_GetForJobSiteAndStatus", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId), new SqlParameter("Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSite", string.Format("int jobSiteId: {0}, ItemStatusEnum status: {1}", jobSiteId, status.ToString()), ex);
                Console.WriteLine("Error in InvoiceItem.GetAllForJobSite: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return PopulateResultsFromDataSet(ds);
        }

        /// <summary>
        /// Async method to get all quote items for a quote
        /// </summary>
        /// <param name="quoteId">int - The Id of the Quote</param>
        /// <returns>Task containing a List of Types.QuoteItem</returns>
        public Task<List<Types.QuoteItem>> GetAllForQuoteAsync(int quoteId)
        {
            var taskResult = new TaskCompletionSource<List<Types.QuoteItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("QuoteItem_GetForQuote", new SqlParameter[] { new SqlParameter("QuoteId", quoteId) }, this.DatabaseName);
                taskResult.SetResult(PopulateResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForInvoiceAsync", string.Format("int quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in QuoteItem.GetAllForInvoiceAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }

            return taskResult.Task;
        }

        /// <summary>
        /// Async method to get all quote items for a Job Site
        /// </summary>
        /// <param name="jobSiteId">int - The Id of the JobSite</param>
        /// <returns>Task containing a List of Types.QuoteItem</returns>
        public Task<List<Types.QuoteItem>> GetAllForJobSiteAsync(int jobSiteId)
        {
            var taskResult = new TaskCompletionSource<List<Types.QuoteItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("QuoteItem_GetForJobSite", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId) }, this.DatabaseName);
                taskResult.SetResult(PopulateResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSiteAsync", string.Format("int jobSiteId: {0}", jobSiteId), ex);
                Console.WriteLine("Error in QuoteItem.GetAllForJobSiteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }

        /// <summary>
        /// Async method to get all quote items of a specified status for a Job Site
        /// </summary>
        /// <param name="jobSiteId">int - The Id of the JobSite</param>
        /// <param name="status">ItemStatusEnum</param>
        /// <returns>Task containing a List of Types.QuoteItem</returns>
        public Task<List<Types.QuoteItem>> GetAllForJobSiteAsync(int jobSiteId, ItemStatusEnum status)
        {
            var taskResult = new TaskCompletionSource<List<Types.QuoteItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("QuoteItem_GetForJobSiteAndStatus", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId), new SqlParameter("Status", (int)status) }, this.DatabaseName);
                taskResult.SetResult(PopulateResultsFromDataSet(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSiteAsync", string.Format("int jobSiteId: {0}, ItemStatusEnum status: {1}", jobSiteId, status.ToString()), ex);
                Console.WriteLine("Error in QuoteItem.GetAllForJobSiteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion
        #region Update Methods
        /// <summary>
        /// Deletes the record with the Id supplied
        /// </summary>
        /// <param name="itemId">The id of the item to delete.</param>
        /// <returns>True on succes, false on error.</returns>
        public bool Delete(int itemId)
        {
            bool ReturnValue = true;
            try
            {
                DBHelper.ExecuteStoredProcRowCount("QuoteItem_Delete", new SqlParameter[] { new SqlParameter("@QuoteItemId", itemId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Delete", string.Format("int itemId: {0}", itemId), ex);
                Console.WriteLine("Error in QuoteItem.Delete: {0}", ex.ToString());
                ReturnValue = false;
            }
            return ReturnValue;
        }
        /// <summary>
        /// Method to insert or update a record as required
        /// </summary>
        /// <param name="obj">Types.QuoteItem</param>
        /// <returns>int - the record Id of object</returns>
        public int Save(Types.QuoteItem obj)
        {
            if (obj.Id < 0)
                return InsertQuoteItem(obj);
            else
                return UpdateQuoteItem(obj);
        }
        #endregion
        #endregion
        #region Private
        /// <summary>
        /// Method to insert a row into the database
        /// </summary>
        /// <param name="obj">Types.QuoteItem</param>
        /// <returns>int - the record Id of the inserted row</returns>
        private int InsertQuoteItem(Types.QuoteItem obj)
        {
            int retVal = -1;
            try
            {
                //Check if the Job item needs to be created
                if (obj.JobItemId < 1)
                {
                    DAL.JobItem itmContext = new DAL.JobItem(this.DatabaseName);
                    obj.JobItemId = itmContext.Save(obj.ToJobItem());
                }
                retVal = DBHelper.ExecuteStoredProcReturnID("QuoteItem_Insert", GetInsertparameters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("InsertInvoiceItem", string.Format("Types.InvoiceItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in InvoiceItem.InsertInvoiceItem: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to update a row in the database
        /// </summary>
        /// <param name="obj">Types.QuoteItem</param>
        /// <returns>int - the record Id of the updated row</returns>
        public int UpdateQuoteItem(Types.QuoteItem obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("QuoteItem_Update", GetUpdateparameters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("UpdateQuoteItem", string.Format("Types.QuoteItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in QuoteItem.UpdateQuoteItem: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an array of parameters to be used during inserts
        /// </summary>
        /// <param name="obj">Types.QuoteItem</param>
        /// <returns>SqlParameter[]</returns>
        private SqlParameter[] GetInsertparameters(Types.QuoteItem obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                //Quote specifc
                new SqlParameter ("QuoteId",obj.QuoteId),
                new SqlParameter ("WorkDescription",obj.Description ),
                new SqlParameter ("Value",obj.Value ), 
                new SqlParameter ("VatRate",obj.VatRate ),
                //Generic items
                new SqlParameter ("JobItemId",obj.JobItemId),
                new SqlParameter ("Status",(int)obj.Status),
                new SqlParameter ("OrderId",obj.OrderId)
            };

            return parameters;
        }

        /// <summary>
        /// Helper method to create an array of parameters to be used during updates
        /// </summary>
        /// <param name="obj">Types.QuoteItem</param>
        /// <returns>SqlParameter[]</returns>
        private SqlParameter[] GetUpdateparameters(Types.QuoteItem obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter ("Id",obj.Id),
                new SqlParameter ("QuoteId",obj.QuoteId),
                new SqlParameter ("JobItemId",obj.JobItemId),
                new SqlParameter("Code", obj.Code),
                new SqlParameter ("Name", obj.Name),
                new SqlParameter ("Value",obj.Value ),
                new SqlParameter ("VatRate",obj.VatRate ),
                new SqlParameter ("WorkDescription",obj.Description ),
                new SqlParameter ("Status",(int)obj.Status),
                new SqlParameter ("OrderId",obj.OrderId)
            };

            return parameters;
        }

        /// <summary>
        /// Method to create  a List of Types.QuoteItem from a Dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>Types.QuoteItem</returns>
        private List<Types.QuoteItem> PopulateResultsFromDataSet(DataSet ds)
        {
            List<Types.QuoteItem> retVal = new List<Types.QuoteItem>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.QuoteItem obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateItem(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteItem.PopulateResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>Types.QuoteItem</returns>
        internal static Types.QuoteItem CreateItem(DataRow row)
        {
            Types.QuoteItem obj = new Types.QuoteItem();
            obj.Id = (int)row["Id"];
            obj.QuoteId = (int)row["QuoteId"];
            obj.JobId = (int)row["JobId"];
            obj.JobSiteId = (int)row["JobSiteId"];
            obj.JobItemId = (int)row["JobItemId"];
            obj.TPOId = (int)row["TPOId"];
            obj.Value = (decimal)row["Value"];
            obj.VatRate = (decimal)row["VatRate"];
            obj.Name = (string)row["Name"];
            obj.Description = (string)row["WorkDescription"];
            obj.Code = (string)row["Code"];
            obj.Latitude = (double)row["Latitude"];
            obj.Longitude = (double)row["Longitude"];
            obj.Status = (ItemStatusEnum)((int)row["Status"]);
            obj.PreservationStatus = (TreeProtectionStatusEnum)((int)row["PreservationStatus"]);
            obj.OrderId = (int)row["OrderId"];
            return obj;
        }
        #endregion
        #endregion
    }
}
