﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;


namespace DGS.ArbPro.DAL
{
    public class ItemPicture : Contracts.DBContextBase
    {
        #region Constructors
        public ItemPicture(string databaseName) : base(databaseName, "ItemPicture", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Methods
        #region Public
        public int Save(DGS.ArbPro.Types.ItemPicture obj)
        {
            if (obj.Id > 0)
                return Update(obj);
            else
                return Insert(obj);
        }

        public Types.ItemPicture GetForId(int Id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ItemPicture_GetForId", new SqlParameter[] { new SqlParameter("@Id", Id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int Id: {0}", Id), ex);
                Debug.WriteLine("Error in ItemPicture.GetForId: {0}", ex.ToString());
            }
            return ToFirstObject(ds);
        }

        internal List<Types.ItemPicture> Duplicate(int oldQuoteId, int newQuoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ItemPicture_Duplicate", new SqlParameter[] { new SqlParameter("@OldQuoteId", oldQuoteId), new SqlParameter("@NewQuoteId", newQuoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Duplicate", string.Format("int Id: {0}{1}", oldQuoteId, newQuoteId), ex);
                Debug.WriteLine("Error in ItemPicture.Duplicate: {0}{1}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.ItemPicture> GetForItem(ItemTypeEnum itemType, int recordId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ItemPicture_GetForItem", new SqlParameter[] { new SqlParameter("@ItemType", (int)itemType), new SqlParameter("@RecordId", recordId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForItem", string.Format("ItemTypeEnum itemType: {0}, int recordId: {1}", itemType.ToString(), recordId), ex);
                Debug.WriteLine("Error in ItemPicture.GetForItem: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.ItemPicture> GetForJobItem(int jobItemId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ItemPicture_GetForJobItem", new SqlParameter[] { new SqlParameter("@JobItemId", jobItemId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForItem", string.Format("int jobItemId: {0}", jobItemId), ex);
                Debug.WriteLine("Error in ItemPicture.GetForJobItem: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        /// <summary>
        /// Method to clear an image reference from the ItemPictureRef table
        /// </summary>
        /// <param name="ItemId">The job item's id</param>
        /// <returns>true/false</returns>
        public bool ClearImage(int ItemId, int PictureId = -1)
        {
            var retVal = false;
            try
            {
                DBHelper.ExecuteStoredProcDataSet("ItemPicture_DeleteForJobItem", new SqlParameter[] { new SqlParameter("@JobItemId", ItemId), new SqlParameter("@PictureId", PictureId) }, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                retVal = false;
                LogError("Clear", string.Format("Types.ItemPicture: "), ex);
                throw ex;
            }
            return retVal;
        }

        #endregion
        #region Private
        private int Update(Types.ItemPicture obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("ItemPicture_Update", Parameters(obj, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.ItemPicture obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in ItemPicture.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Insert(Types.ItemPicture obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("ItemPicture_Insert", Parameters(obj, ParameterMode.Insert), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.ItemPicture obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in ItemPicture.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] Parameters(Types.ItemPicture obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
                retVal.Add(new SqlParameter("Id", obj.Id));

            retVal.Add(new SqlParameter("Guid", obj.ImageGuid));
            retVal.Add(new SqlParameter("JobItemId", obj.JobItemId));
            retVal.Add(new SqlParameter("ParentId", obj.ParentId));
            retVal.Add(new SqlParameter("ParentItemType", (int)obj.ParentItemType));
            retVal.Add(Parameterize("Image", obj.ImageBytes));

            return retVal.ToArray();
        }

        private List<Types.ItemPicture> ToObjectList(DataSet ds)
        {
            List<Types.ItemPicture> retVal = new List<Types.ItemPicture>();

            if (ds == null | ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in ItemPicture.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.ItemPicture ToFirstObject(DataSet ds)
        {
            Types.ItemPicture retVal = new Types.ItemPicture();

            if (ds == null | ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToFirstObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in ItemPicture.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.ItemPicture CreateInstance(DataRow row)
        {
            Types.ItemPicture obj = new Types.ItemPicture();
            obj.Id = (int)row["Id"];
            obj.ImageGuid = (string)row["Guid"];
            obj.JobItemId = (int)row["JobItemId"];
            obj.ParentId = (int)row["ParentId"];
            obj.ParentItemType = (ItemTypeEnum)((int)row["ParentItemType"]);
            obj.ImageBytes = ByteValue(row, "Image");
            obj.Base64Image = Convert.ToBase64String(obj.ImageBytes);
            return obj;
        }
        #endregion
        #endregion
    }
}
