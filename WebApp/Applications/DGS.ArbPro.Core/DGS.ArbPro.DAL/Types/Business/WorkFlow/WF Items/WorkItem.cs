﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class WorkItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public WorkItem(string database) : base(database, "WorkItem", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        public Types.WorkItem GetForId(int workItemId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkItem_GetForId", new SqlParameter[] { new SqlParameter("ItemId", workItemId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForId", string.Format("int WorkItemId: {0}", workItemId), ex);
                Console.WriteLine("Error in WorkItem.GetAllForId: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return ToObject(ds);
        }

        public List<Types.WorkItem> GetForWorkPlan(int workPlanId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkItem_GetForWorkPlan", new SqlParameter[] { new SqlParameter("@WorkPlanId", workPlanId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkPlan", string.Format("int workPlanId: {0}", workPlanId), ex);
                Console.WriteLine("Error in WorkItem.GetForWorkPlan: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.WorkItem> GetAllForWork(int workId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkItem_GetForWork", new SqlParameter[] { new SqlParameter("WorkId", workId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForInvoice", string.Format("int WorkId: {0}", workId), ex);
                Console.WriteLine("Error in WorkItem.GetAllForInvoice: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return ToObjectList(ds);
        }

        public List<Types.WorkItem> GetAllCompletedForWork(int workId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkItem_GetCompletedForWork", new SqlParameter[] { new SqlParameter("WorkId", workId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllCompletedForWork", string.Format("int WorkId: {0}", workId), ex);
                Console.WriteLine("Error in WorkItem.GetAllCompletedForWork: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return ToObjectList(ds);
        }

        public List<Types.WorkItem> GetForQuoteJobSite(int quoteId, int jobSiteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkItem_GetForQuoteJobSite",new SqlParameter[] { Parameterize("@QuoteId",quoteId), Parameterize("@JobSiteId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteJobSite", string.Format("int quoteId: {0}, int jobSiteId: {1}",quoteId, jobSiteId), ex);
            }
            return ToObjectList(ds);
        }

        public List<Types.WorkItem> GetInvoiceableForJobSite(int jobSiteId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkItem_GetInvoiceable", new SqlParameter[] { new SqlParameter("@JobSiteId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetInvoiceableForJobSite", string.Format("int jobSiteId: {0}", jobSiteId), ex);
            }
            //Return the populated InvoiceItem list
            return ToObjectList(ds);
        }

        public List<Types.WorkItem> GetInvoiceableForQuote(int quoteId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkItem_GetInvoiceableForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetInvoiceableForJobSite", string.Format("int jobSiteId: {0}", quoteId), ex);
            }
            //Return the populated InvoiceItem list
            return ToObjectList(ds);
        }

        public List<Types.WorkItem> GetAllForJobSite(int jobSiteId)
        {
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkItem_GetForJobSite", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSite", string.Format("int jobSiteId: {0}", jobSiteId), ex);
                Console.WriteLine("Error in WorkItem.GetAllForJobSite: {0}", ex.ToString());
            }
            //Return the populated InvoiceItem list
            return ToObjectList(ds);
        }

        public Task<List<Types.WorkItem>> GetAllForWorkAsync(int workId)
        {
            var taskResult = new TaskCompletionSource<List<Types.WorkItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkItem_GetForWork", new SqlParameter[] { new SqlParameter("WorkId", workId) }, this.DatabaseName);
                taskResult.SetResult(ToObjectList(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForWorkAsync", string.Format("int workId: {0}", workId), ex);
                Console.WriteLine("Error in WorkItem.GetAllForWorkAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }

            return taskResult.Task;
        }

        public Task<List<Types.WorkItem>> GetAllCompletedForWorkAsync(int workId)
        {
            var taskResult = new TaskCompletionSource<List<Types.WorkItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkItem_GetCompletedForWork", new SqlParameter[] { new SqlParameter("WorkId", workId) }, this.DatabaseName);
                taskResult.SetResult(ToObjectList(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForWorkAsync", string.Format("int workId: {0}", workId), ex);
                Console.WriteLine("Error in WorkItem.GetAllCompletedForWorkAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }

            return taskResult.Task;
        }

        public Task<List<Types.WorkItem>> GetAllForJobSiteAsync(int jobSiteId)
        {
            var taskResult = new TaskCompletionSource<List<Types.WorkItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkItem_GetForJobSite", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId) }, this.DatabaseName);
                taskResult.SetResult(ToObjectList(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSiteAsync", string.Format("int jobSiteId: {0}", jobSiteId), ex);
                Console.WriteLine("Error in WorkItem.GetAllForJobSiteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }

        public Task<List<Types.WorkItem>> GetAllForJobSiteAsync(int jobSiteId, ItemStatusEnum status)
        {
            var taskResult = new TaskCompletionSource<List<Types.WorkItem>>();
            DataSet ds = null;
            //Populate the dataset
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("InvoiceItem_GetForJobSiteAndStatus", new SqlParameter[] { new SqlParameter("JobSiteId", jobSiteId), new SqlParameter("Status", status) }, this.DatabaseName);
                taskResult.SetResult(ToObjectList(ds));
            }
            catch (Exception ex)
            {
                LogError("GetAllForJobSiteAsync", string.Format("int jobSiteId: {0}, ItemStatusEnum status: {1}", jobSiteId, status.ToString()), ex);
                Console.WriteLine("Error in WorkItem.GetAllForJobSiteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
            }
            return taskResult.Task;
        }
        #endregion
        #region Update Methods
        public int Save(Types.WorkItem obj)
        {
            if (obj.Id < 0)
                return InsertWorkItem(obj);
            else
                return UpdateWorkItem(obj);
        }

        public bool AssignToWorkPlan(int WorkPlanId, List<Types.WorkItem> items)
        {
            bool retVal = true;
            try
            {
                //Create the property mappings for the data structure parameter
                Dictionary<string, string> propertyMapping = new Dictionary<string, string>();
                propertyMapping.Add("Id", "Id");
                //Create the parameter list and add the required params
                List<SqlParameter> dbParams = new List<SqlParameter>();
                dbParams.Add(Parameterize("@WorkPlanId", WorkPlanId));
                dbParams.Add(Parameterize<Types.WorkItem>("@ItemIds", propertyMapping, items));
                //Exicute the stored procedure
                var id = DBHelper.ExecuteStoredProcReturnID("WorkItem_WorkPlanItemsUpdate", dbParams.ToArray(), this.DatabaseName);
                retVal = id > 0;
            }
            catch (Exception ex)
            {
                LogError("AssignToWorkAppoinment", string.Format("int WorkPlanId: {0}, List<Types.WorkItem> items: {1}", WorkPlanId, items.ToString()), ex);
                retVal = false;
            }
            return retVal;
        }
        #endregion
        #endregion
        #region Private
        private int InsertWorkItem(Types.WorkItem obj)
        {
            int retVal = -1;
            try
            {
                //Check if the Job item needs to be created
                if (obj.JobItemId < 1)
                {
                    DAL.JobItem itmContext = new DAL.JobItem(this.DatabaseName);
                    obj.JobItemId = itmContext.Save(obj.ToJobItem());
                }
                retVal = DBHelper.ExecuteStoredProcReturnID("WorkItem_Insert", GetInsertParamaters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("InsertWorkItem", string.Format("Types.WorkItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in WorkItem.InsertWorkItem: {0}", ex.ToString());
            }
            return retVal;
        }

        private int UpdateWorkItem(Types.WorkItem obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("WorkItem_Update", GetUpdateParamaters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("UpdateWorkItem", string.Format("Types.WorkItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in WorkItem.UpdateWorkItem: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetInsertParamaters(Types.WorkItem obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                //Quote specifc
                new SqlParameter ("WorkId",obj.WorkId),
                new SqlParameter("QuoteItemId",obj.QuoteItemId),
                new SqlParameter ("WorkDescription",obj.Description ),
                new SqlParameter ("Value",obj.Value ),
                new SqlParameter ("VatRate",obj.VatRate ),
                //Generic items
                new SqlParameter ("JobItemId",obj.JobItemId),
                new SqlParameter ("Status",(int)obj.Status),
                new SqlParameter ("OrderId",obj.OrderId)
            };

            return parameters;
        }

        private SqlParameter[] GetUpdateParamaters(Types.WorkItem obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter ("Id",obj.Id),
                new SqlParameter ("WorkId",obj.WorkId),
                new SqlParameter ("JobItemId",obj.JobItemId),
                new SqlParameter ("QuoteItemId",obj.QuoteItemId),
                new SqlParameter ("Value",obj.Value ),
                new SqlParameter("Code", obj.Code), //Previously removed, Add for resolution of DGSAP-819
                new SqlParameter("Name", obj.Name),
                new SqlParameter ("VatRate",obj.VatRate ),
                new SqlParameter ("WorkDescription",obj.Description ),
                new SqlParameter ("Status", (int)obj.Status),
                new SqlParameter ("OrderId",obj.OrderId)
            };

            return parameters;
        }

        private List<Types.WorkItem> ToObjectList(DataSet ds)
        {
            List<Types.WorkItem> retVal = new List<Types.WorkItem>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.WorkItem obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateItem(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in WorkItem.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.WorkItem ToObject(DataSet ds)
        {
            Types.WorkItem retVal = null;
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal = CreateItem(r);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in WorkItem.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        internal static Types.WorkItem CreateItem(DataRow row)
        {
            Types.WorkItem obj = new Types.WorkItem();
            obj.Id = (int)row["Id"];
            obj.WorkId = (int)row["WorkId"];
            obj.QuoteItemId = (int)row["QuoteItemId"];
            obj.JobItemId = (int)row["JobItemId"];
            obj.JobId = (int)row["JobId"];
            obj.JobSiteId = (int)row["JobSiteId"];
            obj.TPOId = (int)row["TPOId"];
            obj.Value = (decimal)row["Value"];
            obj.VatRate = (decimal)row["VatRate"];
            obj.Name = (string)row["Name"];
            obj.Description = (string)row["WorkDescription"];
            obj.Code = (string)row["Code"];
            obj.Latitude = (double)row["Latitude"];
            obj.Longitude = (double)row["Longitude"];
            obj.Status = (ItemStatusEnum)((int)row["Status"]);
            obj.Booked = ((int)row["Booked"]) > 0;
            obj.OrderId = (int)row["OrderId"];
            obj.Completed = obj.Status == ItemStatusEnum.WorkCompleted || obj.Status == ItemStatusEnum.Invoiced || obj.Status == ItemStatusEnum.Credited;
            obj.PreservationStatus = (TreeProtectionStatusEnum)((int)row["PreservationStatus"]);
            return obj;
        }
        #endregion
        #endregion
    }
}
