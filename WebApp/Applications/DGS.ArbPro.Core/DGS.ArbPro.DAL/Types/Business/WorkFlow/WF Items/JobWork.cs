﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class JobWork : Contracts.DBContextBase
    {
        #region Constructors

        public JobWork(string database) : base(database, "JobWork", DateTime.Parse("1970-01-01")) { }

        #endregion


        /// <summary>
        /// Method to get a list of all jobs for a Client
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<Types.JobWork> GetTodayWork(int userId)
        {
            List<Types.JobWork> retVal = new List<Types.JobWork>();

            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Job_GetTodayJobForStaff", new SqlParameter[] { new SqlParameter("@UserId", userId) }, this.DatabaseName);
                retVal = ToObjectList(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForClient", string.Format("userId: {0}", userId), ex);
                Console.WriteLine("Error in Job.GetTodayWork: {0}", ex.ToString());
            }

            return retVal;
        }

        private List<Types.JobWork> ToObjectList(DataSet ds)
        {
            List<Types.JobWork> retVal = new List<Types.JobWork>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.JobWork obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateItem(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in JobWork.ToObjectList: {0}", ex.ToString());
            }

            return retVal;
        }

        internal static Types.JobWork CreateItem(DataRow row)
        {
            Types.JobWork obj = new Types.JobWork();
            obj.Id = (int)row["Id"];
            obj.StartDate = (DateTime)row["StartDate"];
            obj.EndDate = (DateTime)row["EndDate"];
            obj.Status = (int)row["Status"];
            obj.Address = (string)row["Address"];
            // Tanzeel

            return obj;
        }
    }
}
