﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class CreditNote : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public CreditNote(string database) : base(database, "CreditNote", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        /// <summary>
        /// Method to get a complete CreditNote record from the database
        /// </summary>
        /// <param name="creditNoteId">int</param>
        /// <returns>A Single populated CreditNote</returns>
        public Types.CreditNote GetForId(int creditNoteId)
        {
            Types.CreditNote retVal = new Types.CreditNote();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("CreditNote_GetForID", new SqlParameter[] { new SqlParameter("@JobId", creditNoteId) }, this.DatabaseName);
                retVal = PopulateSingleResultFromDataSet(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("creditNoteId: {0}", creditNoteId), ex);
                Console.WriteLine("Error in CreditNote.GetForId: {0}", ex.ToString());
            }
            return retVal;
        }


        /// <summary>
        /// Method to get a list of all creditNotes for an Enquiry
        /// The returned objects will not have any collections populated
        /// </summary>
        /// <param name="enquiryId">int</param>
        /// <returns>List of Types.CreditNote</returns>
        public List<Types.CreditNote> GetForEnquiry(int enquiryId)
        {
            List<Types.CreditNote> retVal = new List<Types.CreditNote>();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("CreditNote_GetForEnquiry", new SqlParameter[] { new SqlParameter("@EnquiryId", enquiryId) }, this.DatabaseName);
                retVal = PopulateResultsFromDataSet(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForEnquiry", string.Format("enquiryId: {0}", enquiryId), ex);
                Console.WriteLine("Error in CreditNote.GetForEnquiry: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Update Methods
        public int Save(Types.CreditNote obj)
        {
            if (obj.Id < 0)
                return Insert(obj);
            else
                return Update(obj);
        }

        public int SaveAll(Types.CreditNote obj)
        {

            if (obj.Id < 0)
                return InsertAll(obj);
            else
                return UpdateAll(obj);
        }
        #endregion
        #endregion

        #region Private
        /// <summary>
        /// Method to insert a record into the database
        /// </summary>
        /// <param name="site">Types.CreditNote</param>
        /// <returns>int - The record Id of the inserted object</returns>
        private int Insert(Types.CreditNote creditNote)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("CreditNote_Insert", GetInsertParams(creditNote), this.DatabaseName);
                //Only try to insert the Jobsites ifthe CreditNote was insert correctly
                //if (retVal > 0)
                //{
                //    DAL.JobSite siteDB = new JobSite(this.DatabaseName);
                //    foreach(Types.JobSite s in creditNote.
                //}
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.CreditNote creditNote: {0}", creditNote.ToString()), ex);
                Console.WriteLine("Error in CreditNote.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private int InsertAll(Types.CreditNote creditNote)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("CreditNote_Insert", GetInsertParams(creditNote), this.DatabaseName);
                //Only try to insert the Jobsites ifthe CreditNote was insert correctly
                if (retVal > 0)
                {
                    DAL.CreditItem itmDB = new CreditItem(this.DatabaseName);
                    int updated = 0;
                    foreach (Types.CreditItem itm in creditNote.CreditedItems)
                    {
                        updated = itmDB.Save(itm);
                        if (updated < 0)
                        {
                            retVal = -1;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.CreditNote creditNote: {0}", creditNote.ToString()), ex);
                Console.WriteLine("Error in CreditNote.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to update a record in the database
        /// </summary>
        /// <param name="site">Types.CreditNote</param>
        /// <returns>int - The record Id of the updated object</returns>
        private int Update(Types.CreditNote creditNote)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("CreditNote_Update", GetUpdateParams(creditNote), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.CreditNote creditNote: {0}", creditNote.ToString()), ex);
                Console.WriteLine("Error in CreditNote.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private int UpdateAll(Types.CreditNote creditNote)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("CreditNote_Update", GetInsertParams(creditNote), this.DatabaseName);
                //Only try to insert the Jobsites ifthe CreditNote was insert correctly
                if (retVal > 0)
                {
                    DAL.CreditItem itmDB = new CreditItem(this.DatabaseName);
                    int updated = 0;
                    foreach (Types.CreditItem itm in creditNote.CreditedItems)
                    {
                        updated = itmDB.Save(itm);
                        if (updated < 0)
                        {
                            retVal = -1;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.CreditNote creditNote: {0}", creditNote.ToString()), ex);
                Console.WriteLine("Error in CreditNote.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create insert parameters array from an object
        /// </summary>
        /// <param name="site">Types.CreditNote</param>
        /// <returns>SqlParameter array</returns>
        private SqlParameter[] GetInsertParams(Types.CreditNote creditNote)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@InvoiceId", creditNote.InvoiceId),
                new SqlParameter("@ContactId", creditNote.PointOfContact.Id),
                new SqlParameter("@StreetNumber", creditNote.HouseNameNo),
                new SqlParameter("@Address1", creditNote.Address1),
                new SqlParameter("@Address2", creditNote.Address2),
                new SqlParameter("@County", creditNote.County),
                new SqlParameter("@PostCode", creditNote.PostCode),
                new SqlParameter("@UserId", creditNote.UserId),
                new SqlParameter("@Status", (int)creditNote.Status),
                new SqlParameter("@VAT", creditNote.VAT)
            };
        }

        /// <summary>
        /// Helper method to create update parameters array from an object
        /// </summary>
        /// <param name="site">Types.CreditNote</param>
        /// <returns>SqlParameter array</returns>
        private SqlParameter[] GetUpdateParams(Types.CreditNote creditNote)
        {
            /*
	            Id int PRIMARY KEY IDENTITY(1,1) NOT NULL,
	            InvoiceId INT NOT NULL,
	            ContactId INT NOT NULL,
	            StreetNumber VARCHAR(100),
	            Address1 VARCHAR(100),
	            Address2 VARCHAR(100),
	            Town VARCHAR(100),
	            County VARCHAR (100),
	            PostCode VARCHAR (100),
	            UserId INT NOT NULL,
	            Status INT NOT NULL DEFAULT 0,
	            VAT decimal (3,3)
             */
            return new SqlParameter[]
            {
                new SqlParameter("@Id",creditNote.Id),
                new SqlParameter("@InvoiceId", creditNote.InvoiceId),
                new SqlParameter("@ContactId", creditNote.PointOfContact.Id),
                new SqlParameter("@StreetNumber", creditNote.HouseNameNo),
                new SqlParameter("@Address1", creditNote.Address1),
                new SqlParameter("@Address2", creditNote.Address2),
                new SqlParameter("@County", creditNote.County),
                new SqlParameter("@PostCode", creditNote.PostCode),
                new SqlParameter("@UserId", creditNote.UserId),
                new SqlParameter("@Status", (int)creditNote.Status),
                new SqlParameter("@VAT", creditNote.VAT)
            };
        }

        private Types.CreditNote PopulateSingleResultFromDataSet(DataSet ds)
        {
            Types.CreditNote retVal = new Types.CreditNote();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                DAL.CreditItem itmDB = new DAL.CreditItem(this.DatabaseName);
                retVal = CreateCreditNoteFlow(ds.Tables[0].Rows[0]);
                if (retVal.Id > 0)
                {
                    retVal.CreditedItems = itmDB.GetAllForCreditNote(retVal.Id);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateSingleResultFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in CreditNote.PopulateSingleResultFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.CreditNote> PopulateResultsFromDataSet(DataSet ds)
        {
            List<Types.CreditNote> retVal = new List<Types.CreditNote>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.CreditNote obj = null;
            try
            {
                DAL.CreditItem itmDB = new DAL.CreditItem(this.DatabaseName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateCreditNoteFlow(r);
                    if (obj.Id > 0)
                    {
                        obj.CreditedItems = itmDB.GetAllForCreditNote(obj.Id);
                    }
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in CreditNote.PopulateResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.CreditNote CreateCreditNoteFlow(DataRow row)
        {
            Types.CreditNote obj = new Types.CreditNote();
            obj.Id = (int)row["Id"];
            obj.InvoiceId = (int)row["InvoiceId"];
            obj.ClientName = (string)row["ClientName"];
            obj.HouseNameNo = (string)row["StreetNumber"];
            obj.Address1 = (string)row["Address1"];
            obj.Address2 = (string)row["Address2"];
            obj.Town = (string)row["Town"];
            obj.County = (string)row["County"];
            obj.PostCode = (string)row["PostCode"];
            obj.Status = (CreditStatusEnum)((int)row["Status"]);
            obj.VAT = (decimal)row["VAT"];
            //Create the Contact
            obj.PointOfContact = DAL.ContactSearchResult.CreateSearchResult(row);
            //Create the Site object
            obj.Site = DAL.ClientSite.CreateObject(row);

            return obj;
        }
        #endregion
        #endregion
    }
}