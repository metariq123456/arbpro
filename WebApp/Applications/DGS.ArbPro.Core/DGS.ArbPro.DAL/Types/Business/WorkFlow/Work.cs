﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Work : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Work(string database) : base(database, "Work", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        /// <summary>
        /// Method to get a complete Work record from the database
        /// </summary>
        /// <param name="workId">int</param>
        /// <returns>A Single populated Work</returns>
        public Types.Work GetForId(int workId)
        {
            Types.Work retVal = new Types.Work();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Work_GetForID", new SqlParameter[] { new SqlParameter("@WorkId", workId) }, this.DatabaseName);
                retVal = PopulateSingleResultFromDataSet(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("workId: {0}", workId), ex);
                Console.WriteLine("Error in Work.GetForId: {0}", ex.ToString());
            }
            return retVal;
        }


        /// <summary>
        /// Method to get a list of all works for an Enquiry
        /// The returned objects will not have any collections populated
        /// </summary>
        /// <param name="enquiryId">int</param>
        /// <returns>List of Types.Work</returns>
        public List<Types.Work> GetForEnquiry(int enquiryId)
        {
            List<Types.Work> retVal = new List<Types.Work>();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Work_GetForEnquiry", new SqlParameter[] { new SqlParameter("@EnquiryId", enquiryId) }, this.DatabaseName);
                retVal = PopulateResultsFromDataSet(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForEnquiry", string.Format("enquiryId: {0}", enquiryId), ex);
                Console.WriteLine("Error in Work.GetForEnquiry: {0}", ex.ToString());
            }
            return retVal;
        }


        public List<Types.Work> GetForQuote(int quoteId)
        {
            List<Types.Work> retVal = new List<Types.Work>();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Work_GetForQuoteID", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
                retVal = PopulateResultsFromDataSet(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForQuote", string.Format("quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in Work.GetForQuote: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Update Methods
        public Int32 Save(Types.Work obj)
        {
            if (obj.Id == -1)
                return Insert(obj);
            else
                return Update(obj);
        }
        #endregion
        #endregion

        #region Private
        /// <summary>
        /// Method to insert a record into the database
        /// </summary>
        /// <param name="site">Types.Work</param>
        /// <returns>int - The record Id of the inserted object</returns>
        private int Insert(Types.Work work)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Work_Insert", GetInsertParams(work), this.DatabaseName);
                //Only try to insert the Jobsites ifthe Work was insert correctly
                //if (retVal > 0)
                //{
                //    DAL.JobSite siteDB = new JobSite(this.DatabaseName);
                //    foreach(Types.JobSite s in work.
                //}
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.Work work: {0}", work.ToString()), ex);
                Console.WriteLine("Error in Work.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to update a record in the database
        /// </summary>
        /// <param name="site">Types.Work</param>
        /// <returns>int - The record Id of the updated object</returns>
        private int Update(Types.Work work)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Work_Update", GetUpdateParams(work), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.Work work: {0}", work.ToString()), ex);
                Console.WriteLine("Error in Work.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create insert parameters array from an object
        /// </summary>
        /// <param name="site">Types.Work</param>
        /// <returns>SqlParameter array</returns>
        private SqlParameter[] GetInsertParams(Types.Work work)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@ClientId", work.ClientId),
                new SqlParameter("@JobId", work.JobId),
                new SqlParameter("@JobSiteId", work.JobSiteId),
                new SqlParameter("@QuoteId", work.QuoteId),
                new SqlParameter("@ContactId", work.ContactId),
                new SqlParameter("@AppointMentId", work.AppointmentId),
                new SqlParameter("@TeamId", work.TeamId),
                new SqlParameter("@Status", (int)work.Status)
            };
        }

        /// <summary>
        /// Helper method to create update parameters array from an object
        /// </summary>
        /// <param name="site">Types.Work</param>
        /// <returns>SqlParameter array</returns>
        private SqlParameter[] GetUpdateParams(Types.Work work)
        {
            /*
             
		        @Id INT
		        ,@ClientId INT
		        ,@JobId INT
		        ,@JobSiteId INT
		        ,@QuoteId INT
		        ,@ContactId INT
		        ,@AppointMentId INT
		        ,@TeamId INT
		        ,@Status INT
             */
            return new SqlParameter[]
            {
                new SqlParameter("@Id",work.Id),
                new SqlParameter("@ClientId", work.ClientId),
                new SqlParameter("@JobId", work.JobId),
                new SqlParameter("@JobSiteId", work.JobSiteId),
                new SqlParameter("@QuoteId", work.AppointmentId),
                new SqlParameter("@ContactId", work.PointOfContact.Id),
                new SqlParameter("@AppointMentId", work.AppointmentId),
                new SqlParameter("@TeamId", work.TeamId),
                new SqlParameter("@Status", (int)work.Status)
            };
        }

        private Types.Work PopulateSingleResultFromDataSet(DataSet ds)
        {
            Types.Work retVal = new Types.Work();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                DAL.WorkItem itmDB = new DAL.WorkItem(this.DatabaseName);
                retVal = CreateWorkFlow(ds.Tables[0].Rows[0]);
                if (retVal.Id > 0)
                {
                    retVal.WorkItems = itmDB.GetAllForWork(retVal.Id);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateSingleResultFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Work.PopulateSingleResultFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.Work> PopulateResultsFromDataSet(DataSet ds)
        {
            List<Types.Work> retVal = new List<Types.Work>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.Work obj = null;
            try
            {
                DAL.WorkItem itmDB = new DAL.WorkItem(this.DatabaseName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateWorkFlow(r);
                    if (obj.Id > 0)
                    {
                        obj.WorkItems = itmDB.GetAllForWork(obj.Id);
                    }
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Work.PopulateResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.Work CreateWorkFlow(DataRow row)
        {
            Types.Work obj = new Types.Work();
            obj.Id = (int)row["Id"];
            obj.QuoteId = (int)row["QuoteId"];
            obj.JobId = (int)row["JobId"];
            obj.JobSiteId = (int)row["JobSiteId"];
            obj.ClientId = (int)row["ClientId"];
            obj.ClientName = (string)row["ClientName"];
            obj.TeamId = (int)row["TeamId"];
            obj.DateCreated = this.DateTimeValue(row, "DateCreated");
            //Create the ClientContact
            obj.PointOfContact = DAL.ContactSearchResult.CreateSearchResult(row);

            obj.Site = DAL.ClientSite.CreateObject(row);
            return obj;
        }
        #endregion
        #endregion
    }
}
