﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class InvoiceSummary:Contracts.DBContextBase
    {
        #region Constructors
        public InvoiceSummary(string database):base(database,"InvoiceSummary"){}
        #endregion

        #region Methods
        #region Public
        public Types.InvoiceSummary GetForQuote(int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Quote_InvoiceSummary", new SqlParameter[] { Parameterize("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("int quoteId: {0}", quoteId.ToString()), ex);
            }
            return ToObject(ds);
        }
        public Types.InvoiceSummary GetForInvoice(int invoiceId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("GetPaidInvoices_For_Edit", new SqlParameter[] { Parameterize("@QuoteId", invoiceId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("int quoteId: {0}", invoiceId.ToString()), ex);
            }
            return ToObject(ds);
        }
        #endregion
        #region Private
        private Types.InvoiceSummary ToObject(DataSet ds)
        {
            Types.InvoiceSummary retVal = new Types.InvoiceSummary();
            if (!base.HasRows(ds))
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
                if (retVal.Id > 0)
                {
                    //  Load the Invoices and Invoicable work items
                    var invContext = new DAL.Invoice(this.DatabaseName);
                    var itmContext = new DAL.WorkItem(this.DatabaseName);
                    retVal.Invoices = invContext.GetForQuote(retVal.Id);
                    retVal.InvoicableItems = itmContext.GetInvoiceableForQuote(retVal.Id);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }

            return retVal;
        }

        private List<Types.InvoiceSummary> ToObjectList(DataSet ds)
        {
            List<Types.InvoiceSummary> retVal = new List<Types.InvoiceSummary>();
            if (!base.HasRows(ds))
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }

            return retVal;
        }

        private Types.InvoiceSummary CreateInstance(DataRow row)
        {
            Types.InvoiceSummary obj = new Types.InvoiceSummary();
            obj.Id = (int)row["Id"];
            obj.WorkId = (int)row["WorkId"];
            obj.QuoteNo = (string)row["QuoteRef"];
            obj.QuoteValue = base.DecimaleValue(row, "QuoteValue");
            obj.QuoteVatValue = base.DecimaleValue(row, "QuoteVat");
            obj.TotalWorkValue = base.DecimaleValue(row, "WorkValue");
            obj.TotalWorkVatValue = base.DecimaleValue(row, "WorkVat");
            obj.CompletedWorkValue = base.DecimaleValue(row, "CompWorkValue");
            obj.CompletedWorkVatValue = base.DecimaleValue(row, "CompWorkVat");
            obj.PendingValue = base.DecimaleValue(row, "PendingValue");
            obj.PendingVat = base.DecimaleValue(row, "PendingVat");
            obj.InvoicedValue = base.DecimaleValue(row, "InvoiceValue");
            obj.InvoicedVatValue = base.DecimaleValue(row, "InvoiceVat");
            obj.InvoiceReceipts = base.DecimaleValue(row, "ReceiptsValue");
            obj.InvoiceReceiptVat = base.DecimaleValue(row, "ReceiptsVat");
            obj.OverdueValue = base.DecimaleValue(row, "OverdueValue");
            obj.OverdueVat = base.DecimaleValue(row, "OverdueVat");
            return obj;
        }
        #endregion
        #endregion
    }
}
