﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Job : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Job(string database) : base(database, "Job", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        /// <summary>
        /// Method to get a complete Job record from the database
        /// The returned object will have the following if any records exist
        ///     populated work sites,
        ///     populated quote search results,
        ///     populated invoice search results
        /// </summary>
        /// <param name="jobId">int</param>
        /// <returns>A Single populated Job</returns>
        public Types.Job GetForId(int jobId)
        {
            Types.Job retVal = new Types.Job();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Job_GetForID", new SqlParameter[] { new SqlParameter("@JobId", jobId) }, this.DatabaseName);
                retVal = ToObject(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("jobId: {0}", jobId), ex);
                Console.WriteLine("Error in Job.GetForId: {0}", ex.ToString());
            }
            return retVal;
        }


        /// <summary>
        /// Method to get a list of all jobs for an Enquiry
        /// </summary>
        /// <param name="enquiryId">int</param>
        /// <returns>List of Types.Job</returns>
        public List<Types.Job> GetForEnquiry(int enquiryId)
        {
            List<Types.Job> retVal = new List<Types.Job>();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Job_GetForEnquiry", new SqlParameter[] { new SqlParameter("@EnquiryId", enquiryId) }, this.DatabaseName);
                retVal = ToObjectList(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForEnquiry", string.Format("enquiryId: {0}", enquiryId), ex);
                Console.WriteLine("Error in Job.GetForEnquiry: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to get a list of all jobs for a Client
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public List<Types.Job> GetForClient(int clientId)
        {
            List<Types.Job> retVal = new List<Types.Job>();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Job_GetForClient", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                retVal = ToObjectList(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForClient", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in Job.GetForClient: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Update Methods
        public int Save(Types.Job obj)
        {
            if (obj.Id <= 0)
                return Insert(obj);
            else
                return Update(obj);
        }
        #endregion
        #endregion

        #region Private
        /// <summary>
        /// Method to insert a record into the database
        /// </summary>
        /// <param name="site">Types.Job</param>
        /// <returns>int - The record Id of the inserted object</returns>
        private int Insert(Types.Job job)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Job_Insert", Parameters(job, ParameterMode.Insert), this.DatabaseName);
                //Only try to insert the Jobsites ifthe Job was insert correctly
                //if (retVal > 0)
                //{
                //    DAL.JobSite siteDB = new JobSite(this.DatabaseName);
                //    foreach(Types.JobSite s in job.
                //}
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.Job job: {0}", job.ToString()), ex);
                Console.WriteLine("Error in Job.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to update a record in the database
        /// </summary>
        /// <param name="site">Types.Job</param>
        /// <returns>int - The record Id of the updated object</returns>
        private int Update(Types.Job job)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Job_Update", Parameters(job, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.Job job: {0}", job.ToString()), ex);
                Console.WriteLine("Error in Job.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] Parameters(Types.Job obj, ParameterMode mode)
        {
            var pars = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                pars.Add(Parameterize("@Id", obj.Id));
            }

            pars.Add(Parameterize("@ClientId", obj.ClientId));
            pars.Add(Parameterize("@ContactId", obj.ContactId));
            pars.Add(Parameterize("@Source", obj.Source));
            pars.Add(Parameterize("@ReporterId", obj.ReporterID));
            pars.Add(Parameterize("@Comments", obj.Comments));
            pars.Add(Parameterize("@Status", (int)obj.Status));
            pars.Add(Parameterize("@DefaultSurveyor", obj.SurveyorId));
            pars.Add(Parameterize("@IncludeOnDashboard", obj.ShowInDashboard));

            if (mode == ParameterMode.Insert)
            {
                pars.Add(Parameterize("@Reporter", obj.Reporter));
            }

            return pars.ToArray();
        }



        /// <summary>
        /// Helper method to create insert parameters array from an object
        /// </summary>
        /// <param name="site">Types.Job</param>
        /// <returns>SqlParameter array</returns>
        private SqlParameter[] GetInsertParams(Types.Job job)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@ClientId", job.ClientId),
                new SqlParameter("@ContactId", job.ContactId),
                new SqlParameter("@Source", job.Source),
                new SqlParameter("@ReporterId", job.ReporterID),
                new SqlParameter("@Comments", job.Comments),
                new SqlParameter("@Status", (int)job.Status),
                new SqlParameter("@Reporter", job.Reporter)
            };
        }

        /// <summary>
        /// Helper method to create update parameters array from an object
        /// </summary>
        /// <param name="site">Types.Job</param>
        /// <returns>SqlParameter array</returns>
        private SqlParameter[] GetUpdateParams(Types.Job job)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",job.Id),
                new SqlParameter("@ClientId", job.ClientId),
                new SqlParameter("@ContactId", job.PointOfContact.Id),
                new SqlParameter("@Source", job.Source),
                new SqlParameter("@ReporterId", job.ReporterID),
                new SqlParameter("@Comments", job.Comments),
                new SqlParameter("@Status", (int)job.Status)
            };
        }

        private Types.Job ToObject(DataSet ds)
        {
            Types.Job retVal = new Types.Job();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateWorkFlow(ds.Tables[0].Rows[0]);
                if (retVal.Id > 0)
                {
                    DAL.Calendar appDB = new Calendar(this.DatabaseName);

                    DAL.JobSite siteDB = new JobSite(this.DatabaseName);
                    retVal.WorkSites = siteDB.GetForJob(retVal.Id, true);

                    DAL.QuoteSearchResult quoteDB = new QuoteSearchResult(this.DatabaseName);
                    retVal.Quotes = quoteDB.GetForJobModel(retVal.Id);

                    DAL.WorkSearchResult workDB = new WorkSearchResult(this.DatabaseName);
                    retVal.Work = workDB.GetForJobModel(retVal.Id);

                    DAL.InvoiceSearchResult invDB = new InvoiceSearchResult(this.DatabaseName);
                    retVal.Invoices = invDB.GetForJobModel(retVal.Id);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Job.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.Job> ToObjectList(DataSet ds)
        {
            List<Types.Job> retVal = new List<Types.Job>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.Job obj = null;
            try
            {
                DAL.Calendar appDB = new Calendar(this.DatabaseName);
                DAL.JobSite siteDB = new JobSite(this.DatabaseName);
                DAL.QuoteSearchResult quoteDB = new QuoteSearchResult(this.DatabaseName);
                DAL.InvoiceSearchResult invDB = new InvoiceSearchResult(this.DatabaseName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateWorkFlow(r);
                    if (obj.Id > 0)
                    {                        
                        obj.WorkSites = siteDB.GetForJob(obj.Id, false);

                        obj.Quotes = quoteDB.GetForJobModel(obj.Id);

                        obj.Invoices = invDB.GetForJobModel(obj.Id);
                    }
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Job.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.Job CreateWorkFlow(DataRow row)
        {
            Types.Job obj = new Types.Job();
            obj.Id = (int)row["Id"];
            obj.ClientId = (int)row["ClientId"];
            obj.ClientName = (string)row["ClientName"];
            obj.Source = (string)row["Source"];
            obj.Comments = (string)row["Comments"];
            obj.ReporterID = (int)row["ReporterId"];
            obj.Status = (JobStatusEnum)row["Status"];
            obj.ReporterID = (int)row["ReporterId"];
            obj.Reporter = base.StringValue(row,"Reporter");
            obj.Comments = (string)row["Comments"];
            obj.DateCreated = this.DateTimeValue(row, "DateCreated");

            obj.DefaultSurveyor = StringValue(row, "DefaultSurveyor");
            obj.SurveyorId = IntValue(row, "SurveyorId");
            obj.ShowInDashboard = BoolValue(row, "IncludeOnDashboard");

            //Create the Contact
            obj.PointOfContact = DAL.ContactSearchResult.CreateSearchResult(row);
            return obj;
        }
        #endregion
        #endregion
    }
}
