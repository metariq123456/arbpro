﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Invoice : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Invoice(string database) : base(database, "Invoice", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        /// <summary>
        /// Method to get a complete Job record from the database
        /// The returned object will have the following if any records exist
        ///     populated work sites,
        ///     populated quote search results,
        ///     populated invoice search results
        /// </summary>
        /// <param name="invoiceId">int</param>
        /// <returns>A Single populated Job</returns>
        public Types.Invoice GetForId(int invoiceId)
        {
            Types.Invoice retVal = new Types.Invoice();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Invoice_GetForID", new SqlParameter[] { new SqlParameter("@InvoiceId", invoiceId) }, this.DatabaseName);
                retVal = ToObject(ds);

            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("invoiceId: {0}", invoiceId), ex);
                Console.WriteLine("Error in Invoice.GetForId: {0}", ex.ToString());
            }
            return retVal;
        }


        /// <summary>
        /// Method to get a list of all invoices for an Enquiry
        /// The returned objects will not have any collections populated
        /// </summary>
        /// <param name="enquiryId">int</param>
        /// <returns>List of Types.Invoice</returns>
        public List<Types.Invoice> GetForQuote(int quoteId, bool includeDocument = false)
        {
            List<Types.Invoice> retVal = new List<Types.Invoice>();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Invoice_GetForQuoteID", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
                retVal = ToObjectList(ds, includeDocument);

            }
            catch (Exception ex)
            {
                LogError("GetForQuote", string.Format("quoteId: {0}", quoteId), ex);
                Console.WriteLine("Error in Invoice.GetForQuote: {0}", ex.ToString());
            }
            return retVal;
        }

        public Types.Invoice GetForQuoteAndStatus(int quoteId, Types.Enums.InvoiceStatusEnum status)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Invoice_GetForQuoteAndStatus", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId), new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteAndStatus", string.Format("quoteId: {0}, status: {1}", quoteId,status.ToString()), ex);
            }
            return ToObject(ds);
        }
        public Types.Invoice GetForQuoteAndStatusForInvoice(int invoiceId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("GetPaidInvoices_For_Edit", new SqlParameter[] { new SqlParameter("@InvoiceId", invoiceId)}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteAndStatus", string.Format("quoteId: {0}, status: {1}", invoiceId), ex);
            }
            return ToInvoiceObject(ds);
        }

        public List<Types.Invoice> GetSentForQuote(int quoteId, bool includeDocuments = false)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Invoice_GetSentForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId)}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetSentForQuote", string.Format("quoteId: {0}", quoteId), ex);
            }
            return ToObjectList(ds, includeDocuments);
        }

        public List<Types.Invoice> GetForWork(int workId, bool includeDocuments = false)
        {
            List<Types.Invoice> retVal = new List<Types.Invoice>();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Invoice_GetForWork", new SqlParameter[] { new SqlParameter("@WorkId", workId) }, this.DatabaseName);
                retVal = ToObjectList(ds, includeDocuments);

            }
            catch (Exception ex)
            {
                LogError("GetForWork", string.Format("workId: {0}", workId), ex);
                Console.WriteLine("Error in Invoice.GetForWork: {0}", ex.ToString());
            }
            return retVal;
        }


        public List<Types.Invoice> GetForEnquiry(int enquiryId, bool includeDocuments = false)
        {
            List<Types.Invoice> retVal = new List<Types.Invoice>();
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Invoice_GetForEnquiry", new SqlParameter[] { new SqlParameter("@EnquiryId", enquiryId) }, this.DatabaseName);
                retVal = ToObjectList(ds, includeDocuments);

            }
            catch (Exception ex)
            {
                LogError("GetForEnquiry", string.Format("enquiryId: {0}", enquiryId), ex);
                Console.WriteLine("Error in Invoice.GetForEnquiry: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Update Methods
        public int SetStatus(int id, Types.Enums.InvoiceStatusEnum status)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Invoice_UpdateStatus", new SqlParameter[] { new SqlParameter("@Id", id), new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch(Exception ex)
            {
                LogError("SetStatus", string.Format("int id: {0}, Types.Enums.InvoiceStatusEnum status: {1}", id, status.ToString()), ex);
            }
            return retVal;
        }

        /// <summary>
        /// Update method to insert or update an invoice record in the database
        /// </summary>
        /// <param name="obj">The invoice object to be updated</param>
        /// <returns>The id of the inserted / updated row, -1 if there is an error</returns>
        public int Save(Types.Invoice obj)
        {
            if (obj.Id < 0)
            {
                return base.Save("Invoice_Insert", this.Parameters(obj, ParameterMode.Insert));
            }
            else
            {
                return base.Save("Invoice_Update", this.Parameters(obj, ParameterMode.Update));
            }
        }

        /// <summary>
        /// Update method to insert or update an invoice record in the database
        /// then update all child items 
        /// </summary>
        /// <param name="obj">The invoice object to be inserted / updated</param>
        /// <returns>The id of the inserted / updated row of the invoice table, -1 if there is an error</returns>
        public int SaveAll(Types.Invoice obj)
        {
            int retVal = this.Save(obj);

            // Update all the items for the invoices
            if (retVal > 0)
            {
                int recId = -1;
                var itemDbContext = new InvoiceItem(this.DatabaseName);
                foreach (Types.InvoiceItem item in obj.InvoicedItems)
                {
                    item.InvoiceId = retVal;
                    item.JobId = obj.JobId;
                    recId = itemDbContext.Save(item);
                    if (recId < 1)
                    {
                        retVal = -1;
                        break;
                    }
                }
            }

            return retVal;
        }

        public int SaveInvoicePaidDate(Types.Invoice obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Invoice_UpdatePaidDate", GetUpdateParamsForPaid(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("SetStatus", string.Format("int id: {0}, Types.Enums.InvoiceStatusEnum status: {1}", obj.ToString()), ex);
            }
            return retVal;
        }
        public int SaveInvoiceSentDate(Types.Invoice obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Invoice_UpdateSentDate", GetUpdateParamsForSent(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("SetStatus", string.Format("int id: {0}, Invoice_UpdateSentDate status: {1}", obj.ToString()), ex);
            }
            return retVal;
        }
        
        #endregion
        #endregion

        #region Private
        private SqlParameter[] GetUpdateParamsForPaid(Types.Invoice obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",obj.Id),
                new SqlParameter("@PaidDate",obj.PaidDate),
                new SqlParameter("@DueDate",obj.DueDate),
                new SqlParameter("@DateSent",obj.DateSent),
            };
        }
        private SqlParameter[] GetUpdateParamsForSent(Types.Invoice obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",obj.Id),
                new SqlParameter("@DueDate",obj.DueDate),
                new SqlParameter("@DateSent",obj.DateSent),
            };
        }

        /// <summary>
        /// Private method to convert an invoice object to a parameter array
        /// </summary>
        /// <param name="obj">The object to generate the parameter array from</param>
        /// <param name="mode">The mode of opperation</param>
        /// <returns>An array of SQL Paramaters</returns>
        private SqlParameter[] Parameters(Types.Invoice obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();
            // Only add the id parameter if the mode is update
            if (mode == ParameterMode.Update)
            {
                retVal.Add(Parameterize("@Id", obj.Id));
            }

            retVal.Add(Parameterize("@QuoteId", obj.QuoteId));
            retVal.Add(Parameterize("@WorkId", obj.WorkId));
            retVal.Add(Parameterize("@ClientId", obj.ClientId));
            retVal.Add(Parameterize("@ContactId", obj.PointOfContact.Id));
            retVal.Add(Parameterize("@StreetName", obj.HouseNameNo));
            retVal.Add(Parameterize("@Address1", obj.Address1));
            retVal.Add(Parameterize("@Address2", obj.Address2));
            retVal.Add(Parameterize("@Town", obj.Town));
            retVal.Add(Parameterize("@County", obj.County));
            retVal.Add(Parameterize("@PostCode", obj.PostCode));
            retVal.Add(Parameterize("@Status", (int)obj.Status));
            retVal.Add(Parameterize("@VAT", obj.VAT,9,3));
            retVal.Add(Parameterize("@DueDate", obj.DueDate));

            return retVal.ToArray();
        }

        private Types.Invoice ToObject(DataSet ds)
        {
            Types.Invoice retVal = new Types.Invoice();
            
            // Check if there is any data to be returned
            if (!HasRows(ds))
                return retVal;

            try
            {
                // Create the invoice item
                DAL.InvoiceItem invDB = new DAL.InvoiceItem(this.DatabaseName);
                retVal = CreateWorkFlow(ds.Tables[0].Rows[0]);
                if (retVal.Id > 0)
                {
                    // If the record has been populated get all the invoice items
                    retVal.InvoicedItems = invDB.GetAllForInvoice(retVal.Id);
                }
                retVal.CalculateItemValues();
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Invoice.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }
        private Types.Invoice ToInvoiceObject(DataSet ds)
        {
            Types.Invoice retVal = new Types.Invoice();

            // Check if there is any data to be returned
            if (!HasRows(ds))
                return retVal;

            try
            {
                // Create the invoice item
                DAL.InvoiceItem invDB = new DAL.InvoiceItem(this.DatabaseName);
                retVal = CreateWorkFlowforInvoice(ds.Tables[0].Rows[0]);
                if (retVal.Id > 0)
                {
                    // If the record has been populated get all the invoice items
                    retVal.InvoicedItems = invDB.GetAllForInvoice(retVal.Id);
                }
                retVal.CalculateItemValues();
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Invoice.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.Invoice> ToObjectList(DataSet ds, bool includeDocument)
        {
            List<Types.Invoice> retVal = new List<Types.Invoice>();
            
            // Check if there is any data to be returned
            if (!HasRows(ds))
                return retVal;

            Types.Invoice obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    DAL.InvoiceItem invDB = new DAL.InvoiceItem(this.DatabaseName);
                    DAL.InvoiceHistoryItem hstDB = new DAL.InvoiceHistoryItem(this.DatabaseName);
                    obj = this.CreateWorkFlow(r);
                    if (obj.Id > 0)
                    {
                        obj.InvoicedItems = invDB.GetAllForInvoice(obj.Id);
                        obj.History = hstDB.GetDocumentsForInvoice(obj.Id);
                        if (!includeDocument)
                        {
                            foreach (Types.InvoiceHistoryItem h in obj.History)
                            {
                                h.Doc = null;
                            }
                        }
                    }
                    obj.CalculateItemValues();
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Invoice.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }
        private Types.Invoice CreateWorkFlowforInvoice(DataRow row)
        {
            Types.Invoice obj = new Types.Invoice();
            obj.Id = (int)row["Id"];
            obj.Value = (decimal)row["GrossValue"];
            obj.Reference = (string)row["Reference"];
            obj.DueDate = DateTimeValue(row, "DueDate");
            obj.PaidDate = DateTimeValue(row, "PaidDate");
            obj.DateSent = DateTimeValue(row, "DateSent");
            obj.Status = (InvoiceStatusEnum)((int)row["Status"]);

            return obj;
        }


        private Types.Invoice CreateWorkFlow(DataRow row)
        {            
            Types.Invoice obj = new Types.Invoice();
            obj.Id = (int)row["Id"];
            obj.QuoteId = (int)row["QuoteId"];
            obj.WorkId = (int) row["WorkId"];   // DGSAP-796
            obj.ClientId = (int)row["ClientId"];
            obj.ClientName = (string)row["ClientName"];
            obj.HouseNameNo = (string)row["HouseNameNo"];
            obj.Address1 = (string)row["Address1"];
            obj.Address2 = (string)row["Address2"];
            obj.Town = (string)row["Town"];
            obj.County = (string)row["County"];
            obj.PostCode = (string)row["PostCode"];
            obj.Value = (decimal)row["Value"];
            obj.VAT = (decimal)row["VAT"];
            obj.Reference = (string)row["InvRefNo"];
            obj.DateSent = DateTimeValue(row, "DateSent");
            obj.DueDate = DateTimeValue(row, "DueDate");
            obj.PaidDate = DateTimeValue(row, "PaidDate");
            obj.Status = (InvoiceStatusEnum)((int)row["Status"]);
            obj.DateCreated = this.DateTimeValue(row, "DateCreated");
            
            // Removed because not required - DGSAP-891
            //obj.Site = DAL.ClientSite.CreateObject(row);
            
            // Create the ClientContact
            obj.PointOfContact = DAL.ContactSearchResult.CreateSearchResult(row);
            obj.ContactId = obj.PointOfContact.Id;

            return obj;
        }
        #endregion
        #endregion
    }
}