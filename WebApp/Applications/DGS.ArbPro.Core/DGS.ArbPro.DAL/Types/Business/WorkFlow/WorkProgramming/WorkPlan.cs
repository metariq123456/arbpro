﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class WorkPlan : Contracts.DBContextBase
    {
        #region Constructors
        public WorkPlan(string databaseName) : base(databaseName, "WorkPlan", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Methods
        #region Public

        /// <summary>
        /// Method to Insert or Update a work plan.
        /// If the Id of the work plan is -1 a new record will be inserted into the database.
        /// </summary>
        /// <param name="obj">The object to be saved</param>
        /// <returns>the Id of the row updated in the database. -1 if there was an error</returns>
        public int Save(Types.WorkPlan obj)
        {
            if (obj.Id < 0)
                return Insert(obj, false);
            else
                return Update(obj, false);
        }

        public bool Delete(int workplanId)
        {

            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcRowCount("WorkPlan_Delete", new SqlParameter[] { Parameterize("@Id", workplanId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Delete", string.Format("Types.WorkPlan Id: {0}, ", workplanId), ex);
                Debug.WriteLine("Error in WorkPlan.Delete: {0}", ex.ToString());
            }
            return retVal > 0;
        }

        /// <summary>
        /// Method to Insert or Update a work plan and all of the collections associated with it.
        /// If the Id of the work plan is -1 a new record will be inserted into the database.
        /// </summary>
        /// <param name="obj">The object to be saved</param>
        /// <returns>the Id of the row updated in the database. -1 if there was an error</returns>
        public int SaveAll(Types.WorkPlan obj)
        {
            if (obj.Id < 0)
                return Insert(obj, true);  // change after adding tool & staff
            else
                return Update(obj, true);
        }

        public int SaveAllForJobWork(Types.WorkPlan obj)
        {
             return UpdateForWorkJob(obj);
        }
        /// <summary>
        /// Get a specifc work plan based on its Id.
        /// </summary>
        /// <param name="id">The Id to search for</param>
        /// <param name="includeTools">Flag to denote if the work tools collection should be populated in the returned object</param>
        /// <returns>a single work plan object, the Id will be -1 if an error occured or there were no results</returns>
        public Types.WorkPlan GetForId(int id, bool includeTools)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkPlan_GetForId", new SqlParameter[] { new SqlParameter("@Id", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int id: {0}, bool includeTools: {1}", id, includeTools), ex);
                Debug.WriteLine("Error in WorkPlan.GetForId: {0}", ex.ToString());
            }
            return this.ToObject(ds, includeTools);
        }

        /// <summary>
        /// Get the work plan for an appointment.
        /// NOTE: 1 WorkPlan may have many appointments.
        /// </summary>
        /// <param name="appointmentId">The appoinment Id to search for</param>
        /// <param name="includeTools">Flag to denote if the work tools collection should be populated in the returned object</param>
        /// <returns>a single work plan object, the Id will be -1 if an error occured or there were no results</returns>
        public Types.WorkPlan GetForAppointment(int appointmentId, bool includeTools)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkPlan_GetForAppointment", new SqlParameter[] { new SqlParameter("@AppointmentId", appointmentId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForAppointment", string.Format("int appointmentId: {0}, bool includeTools: {1}", appointmentId, includeTools), ex);
                Debug.WriteLine("Error in WorkPlan.GetForAppointment: {0}", ex.ToString());
            }
            return this.ToObject(ds, includeTools);
        }

        /// <summary>
        /// Get the latest workplan associated with a quote
        /// </summary>
        /// <param name="quoteId">The Id of the Quote</param>
        /// <param name="includeTools">Flag to denote if the work tools collection should be populated in the returned object</param>
        /// <returns>a single work plan object, the Id will be -1 if an error occured or there were no results</returns>
        public Types.WorkPlan GetForQuote(int quoteId, bool includeTools)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkPlan_GetForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuote", string.Format("int quoteId: {0}, bool includeTools: {1}", quoteId, includeTools), ex);
                Debug.WriteLine("Error in WorkPlan.GetForQuote: {0}", ex.ToString());
            }
            return this.ToObject(ds, includeTools);
        }

        /// <summary>
        /// Get the all workplans associated with a quote
        /// </summary>
        /// <param name="quoteId">The Id of the Quote</param>
        /// <param name="includeTools">Flag to denote if the work tools collection should be populated in the returned object</param>
        /// <returns>a single work plan object, the Id will be -1 if an error occured or there were no results</returns>
        public List<Types.WorkPlan> GetListForQuote(int quoteId, bool includeTools)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkPlan_GetForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuote", string.Format("int quoteId: {0}, bool includeTools: {1}", quoteId, includeTools), ex);
                Debug.WriteLine("Error in WorkPlan.GetForQuote: {0}", ex.ToString());
            }
            return this.ToObjectList(ds, includeTools);
        }

        public List<Types.WorkPlan> GetWorkListForQuote(int quoteId, bool includeTools)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Work_GetForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuote", string.Format("int quoteId: {0}, bool includeTools: {1}", quoteId, includeTools), ex);
                Debug.WriteLine("Error in WorkPlan.GetForQuote: {0}", ex.ToString());
            }
            return this.ToObjectWorkListForQuote(ds, includeTools);
        }

        /// <summary>
        /// Get the Work Plan associated with a work record
        /// </summary>
        /// <param name="workId">Int: The Id of thje work record</param>
        /// <param name="includeTools">bool: Flag to specify if tools are to be loaded</param>
        /// <returns>A single Types.WorkPlan object</returns>
        public Types.WorkPlan GetForWork(int workId, bool includeTools)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkPlan_GetForWork", new SqlParameter[] { new SqlParameter("@WorkId", workId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWork", string.Format("int workId: {0}, bool includeTools: {1}", workId, includeTools), ex);
                Debug.WriteLine("Error in WorkPlan.GetForWork: {0}", ex.ToString());
            }
            return this.ToObject(ds, includeTools);
        }

        /// <summary>
        /// Method to get all Workplans that have been assigned to a specifc team
        /// </summary>
        /// <param name="teamId">The Id of the team that is to be searched for</param>
        /// <param name="includeTools">Flag to denote if the work tools collection should be populated in the returned object</param>
        /// <returns>List of WorkPlan objects, this list will be empty if an error occured</returns>
        public List<Types.WorkPlan> GetForTeamId(int teamId, bool includeTools)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkPlan_GetForTeam", new SqlParameter[] { new SqlParameter("@TeamId", teamId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForTeamId", string.Format("int teamId: {0}, bool includeTools: {1}", teamId, includeTools), ex);
                Debug.WriteLine("Error in WorkPlan.GetForTeamId: {0}", ex.ToString());
            }
            return this.ToObjectList(ds, includeTools);
        }

        /// <summary>
        /// Method to get all workplans for a specific date
        /// </summary>
        /// <param name="forDate">The date to get work plans for</param>
        /// <param name="includeTools">Flag to denote if the work tools collection should be populated in the returned object</param>
        /// <returns>List of WorkPlan objects, this list will be empty if an error occured</returns>
        public List<Types.WorkPlan> GetForDate(DateTime forDate, bool includeTools)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkPlan_GetForDate", new SqlParameter[] { new SqlParameter("@ForDate", forDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDate", string.Format("DateTime forDate: {0}, bool includeTools: {1}", forDate, includeTools), ex);
                Debug.WriteLine("Error in WorkPlan.GetForDate: {0}", ex.ToString());
            }
            return this.ToObjectList(ds, includeTools);
        }

        /// <summary>
        /// Method to get all WorkPlans for a date range (inclusive)
        /// </summary>
        /// <param name="startDate">The first day on which workplans are required range</param>
        /// <param name="endDate">The last day of the range in which work plans are required</param>
        /// <param name="includeTools">Flag to denote if the work tools collection should be populated in the returned object</param>
        /// <returns>List of WorkPlan objects, this list will be empty if an error occured</returns>
        public List<Types.WorkPlan> GetForDateRange(DateTime startDate, DateTime endDate, bool includeTools)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkPlan_GetForDateRange", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForDateRange", string.Format("DateTime startDate: {0}, DateTime endDate: {1}, bool includeTools: {2}", startDate, endDate, includeTools), ex);
                Debug.WriteLine("Error in WorkPlan.GetForDateRange: {0}", ex.ToString());
            }
            return this.ToObjectList(ds, includeTools);
        }

        public bool DuplicateToolsListForQuote(int originalQuoteId, int newQuoteId)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcRowCount("WorkPlan_DuplicateForQuote", new SqlParameter[] { new SqlParameter("@OriginalQuote", originalQuoteId), new SqlParameter("@NewQuote", newQuoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("DuplicateToolsListForQuote", string.Format("int originalQuoteId: {0}, int newQuoteId: {1}", originalQuoteId, newQuoteId), ex);
                Debug.WriteLine("Error in WorkPlan.DuplicateToolsListForQuote: {0}", ex.ToString());
                retVal = 0;
            }
            return retVal > 0;
        }

        #endregion
        #region Private
        /// <summary>
        /// Method to get the SQL parameter collection for an object
        /// </summary>
        /// <param name="obj">The object that is to be parameterised</param>
        /// <param name="mode">Enum value to denote if an Id is to be parameterised</param>
        /// <returns>An SQLParameter array</returns>
        private SqlParameter[] GetParameters(Types.WorkPlan obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();

            //Only add the Id of the object to the parameter collection if it is an update
            if(mode == ParameterMode.Update)
                retVal.Add(Parameterize("@Id", obj.Id));

            retVal.Add(Parameterize("@QuoteId", obj.QuoteId));
            retVal.Add(Parameterize("@WorkId", obj.WorkId));
            retVal.Add(Parameterize("@TeamId", obj.TeamId));
            retVal.Add(Parameterize("@StaffRequired", obj.StaffRequired));
            retVal.Add(Parameterize("@JobSiteId", obj.JobSiteId));
            retVal.Add(Parameterize("@Time",obj.TimeRequired,6,2));
            return retVal.ToArray();
        }

        /// <summary>
        /// Method to insert a new row into the database
        /// </summary>
        /// <param name="obj">The object to be inserted</param>
        /// <param name="saveChildren">Boolean to denote if child collections should be saved</param>
        /// <returns>The id of the row that was inserted or -1 if an error occured</returns>
        private int Insert(Types.WorkPlan obj, bool saveChildren)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("WorkPlan_Insert", GetParameters(obj, ParameterMode.Insert), this.DatabaseName);
                if (saveChildren && retVal > 0)
                {
                    obj.Id = retVal;
                    if (!SaveChildren(obj))
                        retVal = -1;
                }
            }
            catch(Exception ex)
            {
                LogError("Insert", string.Format("Types.WorkPlan obj: {0}, bool saveChildren: {1}", obj.ToString(), saveChildren), ex);
                Debug.WriteLine("Error in WorkPlan.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to update an existing row in the database
        /// </summary>
        /// <param name="obj">The object to be updated</param>
        /// <param name="saveChildren">Boolean to denote if child collections should be saved</param>
        /// <returns>The id of the row that was update or -1 if an error occured</returns>
        private int Update(Types.WorkPlan obj, bool saveChildren)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("WorkPlan_Update", GetParameters(obj, ParameterMode.Update), this.DatabaseName);
                if (saveChildren && retVal > 0)
                {
                    if (!SaveChildren(obj))
                        retVal = -1;
                }
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.WorkPlan obj: {0}, bool saveChildren: {1}", obj.ToString(), saveChildren), ex);
                Debug.WriteLine("Error in WorkPlan.Update: {0}", ex.ToString());
            }
            return retVal;
        }
        private int UpdateForWorkJob(Types.WorkPlan obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("WorkPlan_Update", GetParameters(obj, ParameterMode.Update), this.DatabaseName);
                if (!SaveChildrenForJobWork(obj))
                {
                    retVal = -1;
                }
                else
                {
                    retVal = 1;
                }

            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.WorkPlan obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in WorkPlan.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private DateTime? WorkMaxDate_GetForQuote(int CalendarWorkId)
        {
            DataSet ds = null;
            DateTime? MaxDate = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkMaxDate_GetForQuote", new SqlParameter[] { new SqlParameter("@CalendarWorkId", CalendarWorkId) }, this.DatabaseName);
                if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                {
                    return MaxDate;
                }
                DataRow row = ds.Tables[0].Rows[0];
                MaxDate = (DateTime)row["BookDate"];

                   
            }
            catch (Exception ex)
            {
                LogError("WorkMaxDate_GetForQuote", "Error in WorkMaxDate_GetForQuote", ex);
                Debug.WriteLine("Error in WorkPlan.WorkMaxDate_GetForQuote: {0}", ex.ToString());
            }
            return MaxDate;
        }
        //private int Delete(int workplanId)
        //{
        //    int retVal = -1;
        //    try
        //    {
        //        retVal = DBHelper.ExecuteStoredProcRowCount("WorkPlan_Delete",new SqlParameter[] {Parameterize("@Id",workplanId)}, this.DatabaseName);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogError("_Delete", string.Format("Types.WorkPlan Id: {0}, ", workplanId), ex);
        //        Debug.WriteLine("Error in WorkPlan._Delete: {0}", ex.ToString());
        //    }
        //    return retVal;
        //}

        /// <summary>
        /// Private method to save collections within the object
        /// </summary>
        /// <param name="obj">The updated object</param>
        /// <returns>success state of the combined update</returns>
        private bool SaveChildren(Types.WorkPlan obj)
        {
            bool retVal = true;
            WorkItem itemContext = new WorkItem(this.DatabaseName);
            if (obj.ScheduledItems.Count == 0 || itemContext.AssignToWorkPlan(obj.Id, obj.ScheduledItems))
            {
                WorkPlanStaff staffContext = new WorkPlanStaff(this.DatabaseName);
                int rowCount = staffContext.Save(obj.Id, obj.Personnel);
                retVal = rowCount > 0 && rowCount == obj.Personnel.Count;
                WorkTool toolContext = new WorkTool(this.DatabaseName);
                if (retVal)
                {
                    rowCount = toolContext.Save(obj.Id, obj.Tools);
                    retVal = rowCount > 0;
                    //foreach (Types.WorkTool wt in obj.Tools)
                    //{
                    //    wt.WorkPlanId = obj.Id;
                    if (!retVal)
                    {
                        retVal = false;
                        LogError("SaveChildren", string.Format("Types.WorkPlan obj: {0}", obj.ToString()), new Exception(string.Format("Failed to save Tools for WorkPlan Id {0}", obj.Id)));
                        Debug.WriteLine("Error in WorkPlan.SaveChildren: Aborting save operation for collection");
                    }
                    // }
                }
                else
                {
                    LogError("SaveChildren", string.Format("Types.WorkPlan obj: {0}", obj.ToString()), new Exception(string.Format("Failed to save staff for WorkPlan Id {0}", obj.Id)));
                    Debug.WriteLine("Error in WorkPlan.SaveChildren: Aborting save operation for collections");
                }
                return retVal;
            }
            else
            {
                return false;
            }
        }

        private bool SaveChildrenForJobWork(Types.WorkPlan obj)
        {
            bool retVal = true;
            WorkItem itemContext = new WorkItem(this.DatabaseName);
            if (obj.ScheduledItems.Count == 0 || itemContext.AssignToWorkPlan(obj.Id, obj.ScheduledItems))
            {
                WorkPlanStaff staffContext = new WorkPlanStaff(this.DatabaseName);
                int rowCount = staffContext.Save(obj.Id, obj.Personnel);
                retVal = rowCount > 0 && rowCount == obj.Personnel.Count;
                WorkTool toolContext = new WorkTool(this.DatabaseName);
                if (retVal)
                {
                    rowCount = toolContext.Save(obj.Id, obj.Tools);
                    retVal = rowCount > 0;
                    if (!retVal)
                    {
                        retVal = false;
                        LogError("SaveChildrenForJobWork", string.Format("Types.WorkPlan obj: {0}", obj.ToString()), new Exception(string.Format("Failed to save Tools for WorkPlan Id {0}", obj.Id)));
                        Debug.WriteLine("Error in WorkPlan.SaveChildren: Aborting save operation for collection");
                    }
                }
            }
            else
            {
                retVal = false;
            }


                
                return retVal;
        }
          /// <summary>
        /// Private method to convert all results to a typed list of objects
        /// </summary>
        /// <param name="ds">The dataset containing the result set</param>
        /// <returns></returns>
        private List<Types.WorkPlan> ToObjectList(DataSet ds, bool includeTools)
        {
            List<Types.WorkPlan> retVal = new List<Types.WorkPlan>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;
            try
            {
                WorkPlanStaff teamContext = new WorkPlanStaff(this.DatabaseName);
                WorkTool toolContext = new WorkTool(this.DatabaseName);
                WorkItem itemContext = new WorkItem(this.DatabaseName);
                Types.WorkPlan wp = null;
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    wp = CreateInstance(r);
                    //Get the items for the work plan
                    wp.ScheduledItems = itemContext.GetForWorkPlan(wp.Id);
                    //Get the Team for this work plan
                    wp.Personnel = teamContext.GetForWorkPlan(wp.Id);
                    if (includeTools)
                    {
                        //Get all the tools for this work plan
                        wp.Tools = toolContext.GetForWorkPlan(wp.Id);
                    }
                    retVal.Add(wp);
                }

            }
            catch(Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in WorkPlan.ToObjectList: {0}", ex.ToString());
            }

            return retVal;
 }


        private List<Types.WorkPlan> ToObjectWorkListForQuote(DataSet ds, bool includeTools)
        {
            List<Types.WorkPlan> retVal = new List<Types.WorkPlan>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;
            try
            {
                WorkPlanStaff teamContext = new WorkPlanStaff(this.DatabaseName);
                WorkTool toolContext = new WorkTool(this.DatabaseName);
                WorkItem itemContext = new WorkItem(this.DatabaseName);
                Types.WorkPlan wp = null;
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    wp = CreateWorkInstanceForQuote(r);
                   
                    //Get the items for the work plan
                    wp.ScheduledItems = itemContext.GetForWorkPlan(wp.Id);
                    //Get the Team for this work plan
                    wp.Personnel = teamContext.GetForWorkPlan(wp.Id);
                    if (includeTools)
                    {
                        //Get all the tools for this work plan
                        wp.Tools = toolContext.GetForWorkPlan(wp.Id);
                    }
                    retVal.Add(wp);
                }

            }
            catch (Exception ex)
            {
                LogError("ToObjectWorkListForQuote", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in WorkPlan.ToObjectWorkListForQuote: {0}", ex.ToString());
            }

            return retVal;
        }
        /// <summary>
        /// Method to convert the first result in a result set to an object
        /// </summary>
        /// <param name="ds">Dataset containing the result set</param>
        /// <returns>A Types.WorkPlan object</returns>
        private Types.WorkPlan ToObject(DataSet ds, bool includeTools)
        {
            Types.WorkPlan retVal = new Types.WorkPlan();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                WorkPlanStaff teamContext = new WorkPlanStaff(this.DatabaseName);
                WorkTool toolContext = new WorkTool(this.DatabaseName);
                WorkItem itemContext = new WorkItem(this.DatabaseName);
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
                //Get the items for the instance
                retVal.ScheduledItems = itemContext.GetForWorkPlan(retVal.Id);
                //Get the Team for this work plan
                retVal.Personnel = teamContext.GetForWorkPlan(retVal.Id);
                if (includeTools)
                {
                    //Get all the tools for this work plan
                    retVal.Tools = toolContext.GetForWorkPlan(retVal.Id);
                }
                DAL.Calendar appContext = new DAL.Calendar(this.DatabaseName);
                retVal.Appointment = appContext.GetWorkListForWorkPlanId(retVal.Id);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in WorkPlan.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Protected
        /// <summary>
        /// Method to create an instance of a work plan from a dat row.
        /// This method will throw an exception if the data row does not have the correct column names.
        /// </summary>
        /// <param name="row">DataRow object containing the data for the object</param>
        /// <returns></returns>
        protected static Types.WorkPlan CreateInstance(DataRow row)
        {
            Types.WorkPlan obj = new Types.WorkPlan();
            obj.Id = (int)row["Id"];
            obj.JobSiteId = (int)row["JobSiteId"];
            obj.QuoteId = (int)row["QuoteId"];
            obj.WorkId = (int)row["WorkId"];
            obj.TeamId = (int)row["TeamId"];
            obj.StaffRequired = (int)row["StaffRequired"];
            obj.TeamName = (string)row["TeamName"];
            obj.SiteAddress = (string)row["SiteName"];
            obj.TimeRequired = (decimal)row["TimeRequired"];
            obj.QuoteReference = (string)row["Reference"];
            
            if (row.Table.Columns.IndexOf("StartDate") >= 0 && (row["StartDate"].GetType() != typeof(DBNull)))
            { 
                obj.StartDate =(DateTime)row["StartDate"];
            }
            return obj;
        }

        protected Types.WorkPlan CreateWorkInstanceForQuote(DataRow row)
        {
            Types.WorkPlan obj = new Types.WorkPlan();
            obj.Id = (int)row["Id"];
            obj.JobSiteId = (int)row["JobSiteId"];
            obj.QuoteId = (int)row["QuoteId"];
            obj.WorkId = (int)row["WorkId"];
            obj.TeamId = (int)row["TeamId"];
            obj.StaffRequired = (int)row["StaffRequired"];
            obj.TeamName = (string)row["TeamName"];
            obj.SiteAddress = (string)row["SiteName"];
            obj.TimeRequired = (decimal)row["TimeRequired"];
            obj.QuoteReference = (string)row["Reference"];
            if (row["CalendarWorkId"].GetType() != typeof(DBNull))
            {
                obj.CalendarWorkId = (int)row["CalendarWorkId"];
            }
           
            obj.IsRecurring = BoolValue(row,"isRecurring");
            if (obj.IsRecurring == true)
            {
                
                obj.StartDate = this.DateTimeValue(row,"EndDateTime");
                
            }
            if (obj.IsRecurring == false)
            {
                obj.StartDate = WorkMaxDate_GetForQuote(obj.CalendarWorkId);
                if (!obj.StartDate.HasValue)
                {
                    obj.StartDate = this.DateTimeValue(row, "EndDateTime");
                }
            }
                        
            return obj;
        }
        #endregion
        #endregion
    }
}

