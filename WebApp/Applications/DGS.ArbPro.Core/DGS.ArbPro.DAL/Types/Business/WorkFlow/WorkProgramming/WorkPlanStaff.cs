﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    /// <summary>
    /// This class extends the functionality of TeamStaff for use with the work plan object
    /// </summary>
    public class WorkPlanStaff: DAL.TeamStaff
    {
        #region Constructors
        public WorkPlanStaff(string database) : base(database, "WorkPlanStaff") { }
        #endregion

        #region Methods
        #region Public
        public List<Types.TeamStaff> GetForWorkPlan(int workPlanId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkPlanStaff_GetForPlan", new SqlParameter[] { Parameterize("@WorkPlanId", workPlanId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkPlan", string.Format("int workPlanId: {0}", workPlanId), ex);
            }
            return ToObjectList(ds);
        }

        public int Save(int workPlanId, List<Types.TeamStaff> staffList)
        {
            int retVal = -1;
            try
            {
                //Create the property mappings for the data structure parameter
                Dictionary<string,string> propertyMapping = new Dictionary<string,string>();
                propertyMapping.Add("StaffId","StaffId");
                propertyMapping.Add("Task","Task");
                //Create the parameter list and add the required params
                List<SqlParameter> dbParams = new List<SqlParameter>();
                dbParams.Add(Parameterize("@WorkPlanId", workPlanId));
                dbParams.Add(Parameterize<Types.TeamStaff>("@StaffRoles",propertyMapping,staffList));
                //Exicute the stored procedure
                retVal = DBHelper.ExecuteStoredProcReturnID("WorkPlanStaff_Insert", dbParams.ToArray(), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Save", string.Format("int workPlanId: {0}, staffList: {1}", workPlanId, staffList.ToString()), ex);
            }
            return retVal;
        }


        #endregion
        #region Private

        #endregion
        #endregion
    }
}
