﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class WorkSummary : Contracts.DBContextBase
    {
        #region Constructors
        public WorkSummary(string database) : base(database, "WorkSummary") { }
        #endregion

        #region Methods
        #region Public Methods
        public Types.WorkSummary GetForQuote(int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Quote_WorkSummary", new SqlParameter[] { Parameterize("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuote", string.Format("Int quoteId: {0}", quoteId), ex);
            }
            return ToObject(ds);
        }

        public Types.WorkSummary GetForWork(int workId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Work_WorkSummary", new SqlParameter[] { Parameterize("@WorkId", workId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWork", string.Format("Int workId: {0}", workId), ex);
            }
            return ToObject(ds);
        }

        public Types.WorkSummary GetForWorkOnQuote(int workId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Work_WorkSummary", new SqlParameter[] { Parameterize("@WorkId", workId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWork", string.Format("Int workId: {0}", workId), ex);
            }
            return ToObjectForWorkOnQuote(ds);
        }
        public Types.WorkSummary GetForJobWork(int workId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobWork_WorkSummary", new SqlParameter[] { Parameterize("@WorkId", workId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJobWork", string.Format("Int workId: {0}", workId), ex);
            }
            return ToObject(ds);
        }
        public Types.WorkSummary GetForWorkPlan(int workPlanId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Work_WorkSummary_ForWorkPlan", new SqlParameter[] { Parameterize("@WorkPlanId", workPlanId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkPlan", string.Format("Int workPlanId: {0}", workPlanId), ex);
            }
            return ToObject(ds);
        }
        public Types.WorkSummary GetForBookWorkPlan(int workPlanId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Work_WorkSummary_ForWorkPlan", new SqlParameter[] { Parameterize("@WorkPlanId", workPlanId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkPlan", string.Format("Int workPlanId: {0}", workPlanId), ex);
            }
            return ToObject(ds);
        }
        #endregion
        #region Private
        private Types.WorkSummary ToObject(DataSet ds)
        {
            if (!HasRows(ds))
                return new Types.WorkSummary();

            Types.WorkSummary retVal = null;
            try
            {
                DAL.JobSite jsContext = new JobSite(this.DatabaseName);
                DAL.WorkItem wiContext = new WorkItem(this.DatabaseName);
                DAL.WorkPlan plContext = new WorkPlan(this.DatabaseName);
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
                if (retVal.Id > 0)
                {
                    retVal.WorkItems = wiContext.GetAllForWork(retVal.Id);
                    retVal.JobSites = jsContext.GetForQuote(retVal.QuoteId, false, false);
                    retVal.BookedWork = plContext.GetListForQuote(retVal.QuoteId, true);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }

        private Types.WorkSummary ToObjectForWorkOnQuote(DataSet ds)
        {
            if (!HasRows(ds))
                return new Types.WorkSummary();

            Types.WorkSummary retVal = null;
            try
            {
                DAL.JobSite jsContext = new JobSite(this.DatabaseName);
                DAL.WorkItem wiContext = new WorkItem(this.DatabaseName);
                DAL.WorkPlan plContext = new WorkPlan(this.DatabaseName);
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
                if (retVal.Id > 0)
                {
                    retVal.WorkItems = wiContext.GetAllForWork(retVal.Id);
                    retVal.JobSites = jsContext.GetForQuote(retVal.QuoteId, false, false);
                    retVal.BookedWork = plContext.GetWorkListForQuote(retVal.QuoteId, true);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }
        private List<Types.WorkSummary> ToObjectList(DataSet ds)
        {
            List<Types.WorkSummary> retVal = new List<Types.WorkSummary>();

            if (!HasRows(ds))
                return retVal;

            try
            {
                DAL.JobSite jsContext = new JobSite(this.DatabaseName);
                DAL.WorkItem wiContext = new WorkItem(this.DatabaseName);
                DAL.WorkPlan plContext = new WorkPlan(this.DatabaseName);
                Types.WorkSummary ws = null;
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    ws = CreateInstance(r);
                    if (ws.Id > 0)
                    {
                        ws.WorkItems = wiContext.GetAllForWork(ws.Id);
                        ws.JobSites = jsContext.GetForQuote(ws.QuoteId,false,false);
                        ws.BookedWork = plContext.GetListForQuote(ws.QuoteId, true);
                    }
                    retVal.Add(ws);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }

        private Types.WorkSummary CreateInstance(DataRow row)
        {
            Types.WorkSummary obj = new Types.WorkSummary();
            obj.Id = base.IntValue(row, "Id");
            obj.QuoteId = base.IntValue(row, "QuoteId");
            obj.AccountNo = base.StringValue(row, "AccountNumber");
            obj.ClientName = base.StringValue(row, "ClientName");
            obj.StreetNumber = base.StringValue(row, "StreetNumber");
            obj.AddressLn1 = base.StringValue(row, "AddressLn1");
            obj.AddressLn2 = base.StringValue(row, "AddressLn2");
            obj.Town = base.StringValue(row, "Town");
            obj.County = base.StringValue(row, "County");
            obj.PostCode = base.StringValue(row, "PostCode");
            obj.Telephone = base.StringValue(row, "Telephone");
            obj.Mobile = base.StringValue(row, "Mobile");
            obj.Email = base.StringValue(row, "Email");
            obj.QuoteNo = base.StringValue(row, "QuoteRef");
            obj.JobId = base.IntValue(row,"JobId");
            return obj;
        }
        #endregion
        #endregion
    }
}
