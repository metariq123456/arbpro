﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Team : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Team(string database) : base(database, "Team", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public List<Types.Team> GetAll()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Team_GetAll", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAll", "None", ex);
                Debug.WriteLine("Error in Team.GetAll: {0}", ex.ToString());
            }
            return ToObjectList(ds, false);
        }

        public List<Types.Team> GetAllWithStaff()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Team_GetAll", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAll", "None", ex);
                Debug.WriteLine("Error in Team.GetAll: {0}", ex.ToString());
            }
            return ToObjectList(ds, true);
        }

        public List<Types.Team> GetForWorkAppointmentId(int calendarWorkId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Team_GetForWorkAppointment", new SqlParameter[] { new SqlParameter("@CalendarWorkId", calendarWorkId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkAppointmentId", string.Format("int calendartWorkId: {0}", calendarWorkId), ex);
                Debug.WriteLine("Error in Team.GetForWorkAppointmentId: {0}", ex.ToString());
            }
            return ToObjectList(ds, false);
        }

        public Types.Team GetForWorkPlan(int workPlanId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Team_GetForWorkPlan", new SqlParameter[] { new SqlParameter("@WorkPlanId", workPlanId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkPlan", string.Format("int workPlanId: {0}", workPlanId), ex);
                Debug.WriteLine("Error in Team.GetForWorkPlan: {0}", ex.ToString());
            }
            return PopulateSingleResultFromDataSet(ds, true);
        }

        public Types.Team GetForId(int teamId, bool includeStaff)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Team_GetForId", new SqlParameter[] { new SqlParameter("@TeamId", teamId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int teamId: {0}, bool includeStaff: {1}", teamId, includeStaff), ex);
                Debug.WriteLine("Error in Team.GetForId: {0}", ex.ToString());
            }
            return PopulateSingleResultFromDataSet(ds, includeStaff);
        }

        public bool Save(Types.Team obj)
        {
            int retVal = -1;
            if (obj.Id < 1)
                retVal = Insert(obj);
            else
                retVal = Update(obj);

            return retVal > 0;

        }

        public bool SaveAll(Types.Team obj)
        {
            int retVal = -1;
            if (obj.Id < 1)
                retVal = Insert(obj);
            else
                retVal = Update(obj);

            return retVal > 0;

        }

        public bool Delete(int teamId)
        {
            return _Delete(teamId) > 0;
        }

        #endregion
        #region Private
        private int Insert(Types.Team obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Team_Insert", Parameters(obj, ParameterMode.Insert), this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.Team obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in Team.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Update(Types.Team obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Team_Update", Parameters(obj, ParameterMode.Update), this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.Team obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in Team.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private int _Delete(int teamId)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Team_Delete", Parameterize("@Id",teamId), this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("_Delete", string.Format("Team id:", teamId), ex);
                Debug.WriteLine("Error in Team.Update: {0}", ex.ToString());
            }
            return retVal;
        }


        private SqlParameter[] Parameters(Types.Team obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                retVal.Add(Parameterize("@Id", obj.Id));
            }
            retVal.Add(Parameterize("@ARGBValue", obj.Colour.ToArgb()));
            retVal.Add(Parameterize("@Number", obj.Number));
            retVal.Add(Parameterize("@Name", obj.Name));
            retVal.Add(Parameterize("@LeaderId", obj.LeaderId));
            retVal.Add(Parameterize("@DriverId", obj.DriverId));
            retVal.Add(Parameterize("@Members", MembersParameter(obj)));

            return retVal.ToArray();
        }

        private DataTable MembersParameter(Types.Team obj)
        {
            DataTable ReturnValue = new DataTable();
            ReturnValue.Columns.Add("StaffId", typeof(int));
            ReturnValue.Columns["StaffId"].SetOrdinal(0);

            ReturnValue.Columns.Add("Task", typeof(string));
            ReturnValue.Columns["Task"].SetOrdinal(1);

            foreach (Types.TeamStaff item in obj.Personnel)
            {
                if (!item.Deleted)
                {
                    ReturnValue.Rows.Add(new object[] { item.StaffId, item.Task });
                }

            }
            return ReturnValue;
        }
        private Types.Team PopulateSingleResultFromDataSet(DataSet ds, bool includeStaff)
        {
            Types.Team retVal = new Types.Team();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);

                if (retVal.Id > 0)
                {
                    TeamStaff tsContext = new TeamStaff(this.DatabaseName);
                    retVal.Personnel = tsContext.GetForTeamId(retVal.Id);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateSingleResultFromDataSet", string.Format("DataSet ds: {0}, bool includeStaff: {1}", ds.ToString(), includeStaff), ex);
                Debug.WriteLine("Error in Team.PopulateSingleResultFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.Team> ToObjectList(DataSet ds, bool includeStaff)
        {
            List<Types.Team> retVal = new List<Types.Team>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                if (includeStaff)
                {
                    Types.Team obj = null;
                    DAL.TeamStaff staffContext = new TeamStaff(this.DatabaseName);
                    foreach (DataRow r in ds.Tables[0].Rows)
                    {
                        obj = CreateInstance(r);
                        obj.Personnel = staffContext.GetForTeamId(obj.Id);
                        retVal.Add(obj);
                    }
                }
                else
                {
                    foreach (DataRow r in ds.Tables[0].Rows)
                    {
                        retVal.Add(CreateInstance(r));
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in Team.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.Team CreateInstance(DataRow row)
        {
            Types.Team obj = new Types.Team();
            obj.Id = (int)row["Id"];
            obj.Colour = Color.FromArgb((int)row["ARGBIntValue"]);
            obj.Number = (int)row["Number"];
            obj.Name = (string)row["Name"];
            obj.LeaderId = (int)row["LeaderId"];
            obj.DriverId = (int)row["DriverId"];
            return obj;
        }

        static DataTable TeamMem()
        {
            var dt = new DataTable();
            dt.Columns.Add("SalesPersonID", typeof(int));
            dt.Columns.Add("TerritoryID", typeof(int));
            return dt;
        }
        #endregion
        #endregion
    }
}
