﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class TeamStaff : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public TeamStaff(string database) : base(database, "TeamStaff", DateTime.Parse("1970-01-01")) { }

        public TeamStaff(string database, string className) : base(database, className, DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public List<Types.TeamStaff> GetFromStaffList()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TeamStaff_GetFromStaffList", new SqlParameter[] {}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetFromStaffList", "None", ex);
                Debug.WriteLine("Error in TeamStaff.GetFromStaffList: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.TeamStaff> GetForTeamId(int teamId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TeamStaff_GetForTeamId", new SqlParameter[] { new SqlParameter("@TeamId", teamId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForTeamId", string.Format("int teamId: {0}", teamId), ex);
                Debug.WriteLine("Error in TeamStaff.GetForTeamId: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.TeamStaff> GetForRiskId(int selectedId, int riskId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TeamStaff_GetForRiskId", new SqlParameter[] { new SqlParameter("@SelectedId", selectedId), new SqlParameter("@RiskId", riskId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForRiskId", string.Format("int selectedId: {0}, int riskId: {1}", selectedId,riskId), ex);
                Debug.WriteLine("Error in TeamStaff.GetForRiskId: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public Types.TeamStaff GetForId(int teamStaffId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TeamStaff_GetForId", new SqlParameter[] { new SqlParameter("@TeamStaffId", teamStaffId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int TeamStaffId: {0}", teamStaffId), ex);
                Debug.WriteLine("Error in TeamStaff.GetForId: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public bool Save(Types.TeamStaff obj)
        {
            if (obj.Id < 1 && obj.Deleted) return true; // If the item has no id and is "Deleted" then it doesn't exist
            int retVal = -1;
            if (obj.Id < 1)
                retVal = Insert(obj);
            else if (!obj.Deleted)
                retVal = Update(obj);
            else
                retVal = Delete(obj);
            return retVal > 0;

        }
        #endregion
        #region Private
        private int Delete(Types.TeamStaff obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("TeamStaff_Delete", new SqlParameter[] { Parameterize("@Id", obj.Id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Delete", string.Format("Types.TeamStaff obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in TeamStaff.Delete: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Insert(Types.TeamStaff obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("TeamStaff_Insert", Parameters(obj, ParameterMode.Insert), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.TeamStaff obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in TeamStaff.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Update(Types.TeamStaff obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("TeamStaff_Update", Parameters(obj, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.TeamStaff obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in TeamStaff.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] Parameters(Types.TeamStaff obj, ParameterMode mode)
        {
            List<SqlParameter> ReturnVal = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                ReturnVal.Add(Parameterize("@Id", obj.Id));
            }

            ReturnVal.Add(Parameterize("@TeamId", obj.ParentId));
            ReturnVal.Add(Parameterize("@StaffId", obj.StaffId));
            ReturnVal.Add(Parameterize("@StaffRole", obj.Task));
            return ReturnVal.ToArray();
        }

        private Types.TeamStaff ToObject(DataSet ds)
        {
            Types.TeamStaff retVal = new Types.TeamStaff();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in TeamStaff.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }

        protected List<Types.TeamStaff> ToObjectList(DataSet ds)
        {
            List<Types.TeamStaff> retVal = new List<Types.TeamStaff>();

            if (!HasRows(ds))
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in TeamStaff.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private static Types.TeamStaff CreateInstance(DataRow row)
        {
            Types.TeamStaff obj = new Types.TeamStaff();
            obj.Id = (int)row["Id"];
            obj.StaffId = (int)row["StaffId"];
            obj.ParentId = (int)row["ParentId"];
            obj.Task = (string)row["Task"];
            obj.StaffName = (string)row["StaffName"];
            obj.Deleted = false;
            return obj;
        }
        #endregion
        #endregion
    }
}
