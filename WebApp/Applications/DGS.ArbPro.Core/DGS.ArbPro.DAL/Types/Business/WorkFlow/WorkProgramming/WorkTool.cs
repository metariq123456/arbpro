﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class WorkTool: Contracts.DBContextBase
    {
        #region Constructors
        public WorkTool(string database) : base(database, "WorkTool", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public List<Types.WorkTool> GetFromToolList()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkTool_GetFromTools", new SqlParameter[] {}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetFromToolList", "None", ex);
                Debug.WriteLine("Error in WorkTool.GetFromToolList: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.WorkTool> GetForTeamId(int teamId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkTool_GetForTeamId", new SqlParameter[] { new SqlParameter("@TeamId", teamId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForTeamId", string.Format("int teamId: {0}", teamId), ex);
                Debug.WriteLine("Error in WorkTool.GetForTeamId: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.WorkTool> GetForWorkPlan(int workPlanId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkTool_GetForWorkPlan", new SqlParameter[] { new SqlParameter("@WorkPlanId", workPlanId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkPlan", string.Format("int workPlanId: {0}", workPlanId), ex);
                Debug.WriteLine("Error in WorkTool.GetForWorkPlan: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.WorkTool> GetForQuoteId(int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("WorkTool_GetForQuoteId", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteId", string.Format("int quoteId: {0}", quoteId), ex);
                Debug.WriteLine("Error in WorkTool.GetForQuoteId: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public bool Save(Types.WorkTool obj)
        {
            int retVal = -1;
            if (obj.Id < 1)
                retVal = Insert(obj);
            else
                retVal = Update(obj);

            return retVal > 0;

        }

        public int Save(int workPlanId, List<Types.WorkTool> toolsList)
        {
            int retVal = -1;
            try
            {
                //Create the property mappings for the data structure parameter
                Dictionary<string, string> propertyMapping = new Dictionary<string, string>();
                propertyMapping.Add("Id", "Id");
                propertyMapping.Add("WorkPlanId", "WorkPlanId");
                propertyMapping.Add("ToolId", "ToolId");
                propertyMapping.Add("Number", "Number");
                //Create the parameter list and add the required params
                List<SqlParameter> dbParams = new List<SqlParameter>();
                dbParams.Add(Parameterize("@WorkPlanId", workPlanId));
                dbParams.Add(Parameterize<Types.WorkTool>("@ToolsList", propertyMapping, toolsList));
                //Exicute the stored procedure
                retVal = DBHelper.ExecuteStoredProcReturnID("WorkTool_List_Insert", dbParams.ToArray(), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Save", string.Format("int workPlanId: {0}, toolsList: {1}", workPlanId, toolsList.ToString()), ex);
            }
            return retVal;
        }

        public bool Save(List<Types.WorkTool> tools)
        {
            bool saved = true;
            foreach (Types.WorkTool obj in tools)
            {
                saved = Save(obj) && saved;
            }
            return saved;

        }

        public int Delete(int Id)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("WorkTool_Delete", Parameterize("@Id", Id), this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("_Delete", string.Format("Team id:", Id), ex);
                Debug.WriteLine("Error in Team.Update: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Private
        private int Insert(Types.WorkTool obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("WorkTool_Insert", Parameters(obj, ParameterMode.Insert), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.WorkTool obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in WorkTool.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Update(Types.WorkTool obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("WorkTool_Update", Parameters(obj, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.WorkTool obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in WorkTool.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] Parameters(Types.WorkTool obj, ParameterMode mode)
        {
            List<SqlParameter> ReturnVal = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                ReturnVal.Add(Parameterize("@Id", obj.Id));
            }
            ReturnVal.Add(Parameterize("@WorkPlanId", obj.WorkPlanId));
            ReturnVal.Add(Parameterize("@ToolId", obj.ToolId));
            ReturnVal.Add(Parameterize("@Number", obj.Number));
            return ReturnVal.ToArray();
        }

        private Types.WorkTool PopulateSingleResultFromDataSet(DataSet ds)
        {
            Types.WorkTool retVal = new Types.WorkTool();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("PopulateSingleResultFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in WorkTool.PopulateSingleResultFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.WorkTool> ToObjectList(DataSet ds)
        {
            List<Types.WorkTool> retVal = new List<Types.WorkTool>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in WorkTool.PopulateResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.WorkTool CreateInstance(DataRow row)
        {
            Types.WorkTool obj = new Types.WorkTool();
            obj.Id = (int)row["Id"];
            obj.WorkPlanId = (int)row["WorkPlanId"];
            obj.ToolId = (int)row["ToolId"];
            obj.Item = (string)row["Item"];
            obj.Number = (int)row["Number"];
            return obj;
        }
        #endregion
        #endregion
    }
}
