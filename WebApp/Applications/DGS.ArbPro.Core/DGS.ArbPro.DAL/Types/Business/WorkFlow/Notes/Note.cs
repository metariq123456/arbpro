﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Note : Contracts.DBContextBase
    {
        #region Constructors
        public Note(string databaseName) : base(databaseName, "Note", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Methods
        #region Public
        public int Save(Types.Note obj)
        {
            int retVal = -1;
            ParameterMode mode = ParameterMode.Update;
            string spName = "Note_Update";
            if (obj.Id < 1)
            {
                mode = ParameterMode.Insert;
                spName = "Note_Insert";
            }

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(spName, Parameters(obj, mode), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Save", obj.ToString(), ex);
            }
            return retVal;
        }

        public Types.Note GetForId(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Note_GetForId", new SqlParameter[] { new SqlParameter("@Id", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int id: {0}", id.ToString()), ex);
            }
            return ToObject(ds);
        }

        public List<Types.Note> GetForParent(int parentId, NoteTypeEnum parentType)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Note_GetForParent", new SqlParameter[] { new SqlParameter("@ParentId", parentId), new SqlParameter("@ParentType", (int)parentType) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForParent", string.Format("int parentId: {0}, NoteTypeEnum parentType: {1}", parentId.ToString(), parentType.ToString()), ex);
            }
            return ToObjectList(ds);
        }

        public List<Types.Note> GetForInvoiceNotesForQuote(int quoteId)
        {
            DataSet ds = null;
            NoteTypeEnum parentType = NoteTypeEnum.Invoice;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Note_GetForQuoteInvoices", new SqlParameter[] { new SqlParameter("@ParentId", quoteId), new SqlParameter("@ParentType", (int)parentType) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForParent", string.Format("int parentId: {0}, NoteTypeEnum parentType: {1}", quoteId.ToString(), parentType.ToString()), ex);
            }
            return ToObjectList(ds);
        }
        #endregion
        #region Private
        private SqlParameter[] Parameters(Types.Note obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();
            if (mode == ParameterMode.Update)
            {
                retVal.Add(new SqlParameter("@Id", obj.Id));
            }
            else
            {
                retVal.Add(new SqlParameter("@ParentId", obj.ParentId));
                retVal.Add(new SqlParameter("@ParentType", (int)obj.NoteType));
                retVal.Add(new SqlParameter("@EnteredBy", obj.EnteredBy));
            }
            retVal.Add(new SqlParameter("@Text", obj.Text));
            retVal.Add(new SqlParameter("@ShowOnDashboard", obj.ShowOnDashboard));

            return retVal.ToArray();
        }

        private Types.Note ToObject(DataSet ds)
        {
            Types.Note retVal = new Types.Note();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }

        private List<Types.Note> ToObjectList(DataSet ds)
        {
            List<Types.Note> retVal = new List<Types.Note>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }

        private Types.Note CreateInstance(DataRow row)
        {
            Types.Note obj = new Types.Note();
            obj.Id = (int)row["Id"];
            obj.ParentId = (int)row["ParentId"];
            obj.NoteType = (NoteTypeEnum)((int)row["ParentType"]);
            obj.Text = (string)row["Text"];
            obj.EnteredBy = (string)row["EnteredBy"];
            obj.EnteredDate = DateTimeValue(row,"DateEntered");
            obj.LastUpdated = DateTimeValue(row,"UpdatedDate");
            obj.ShowOnDashboard = BoolValue(row, "ShowOnDashboard");
            return obj;
        }

        #endregion
        #endregion
    }
}
