﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class JobSite : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public JobSite(string database) : base(database, "JobSite", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        public Types.JobSite GetForId(int jobId, bool includeItems)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobSite_GetForID", new SqlParameter[] { new SqlParameter("@JobId", jobId), new SqlParameter("@Disabled", false) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("jobId: {0}", jobId), ex);
                Debug.WriteLine("Error in JobSite.GetForId: {0}", ex.ToString());
            }
            return ToObject(ds, includeItems);
        }

        public bool Remove(int jobId, int siteId)
        {
            int NumRemoved;
            try
            {
                NumRemoved = DBHelper.ExecuteStoredProcRowCount("JobSite_Remove",
                    new SqlParameter[] { Parameterize("@JobId",jobId), 
                        Parameterize("@SiteId", siteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Remove", string.Format("JobId={0} SiteId={1}", jobId, siteId), ex);
                Debug.WriteLine("Error in JobSite.Remove: {0}", ex.ToString());
                return false;
            }
            return true;
        }

        /// <summary>
        /// Method to get all JobSites for a single Job
        /// </summary>
        /// <param name="jobId">Int - The Id of the Job to get the JobSites for</param>
        /// <returns>List of Types.Jobsite</returns>
        public List<Types.JobSite> GetForJob(int jobId, bool includeItems, bool includeDisabled = false)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobSite_GetForJob", new SqlParameter[] { new SqlParameter("@JobId", jobId), new SqlParameter("@Disabled", includeDisabled) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJob", string.Format("jobId: {0}, includeItems: {1}, includeDisabled: {2}", jobId, includeItems, includeDisabled), ex);
                Debug.WriteLine("Error in Client.GetForJob: {0}", ex.ToString());
            }
            return ToObjectList(ds, includeItems);
        }

        public List<Types.JobSite> GetForQuote(int quoteId, bool includeItems, bool includeDisabled = false)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobSite_GetForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId), new SqlParameter("@Disabled", includeDisabled) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJob", string.Format("jobId: {0}, includeItems: {1}, includeDisabled: {2}", quoteId, includeItems, includeDisabled), ex);
            }
            return ToObjectList(ds, includeItems);
        }

        public List<Types.JobSite> GetForInvoice(int invId, bool includeItems, bool includeDisabled = false)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobSite_GetForInvoice", new SqlParameter[] { new SqlParameter("@InvId", invId), new SqlParameter("@Disabled", includeDisabled) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJob", string.Format("jobId: {0}, includeItems: {1}, includeDisabled: {2}", invId, includeItems, includeDisabled), ex);
            }
            return ToObjectList(ds, includeItems);
        }
        #endregion
        #region Update Methods
        /// <summary>
        /// Method used to insert of update a record in the database
        /// If the process fails -1 will be returned
        /// </summary>
        /// <param name="site">Types.JobSite</param>
        /// <returns>int - The record Id of the inserted/updated object</returns>
        public int Save(Types.JobSite site)
        {
            if (site.Id < 0)
                return Insert(site);
            else
                return Update(site);
        }
        #endregion
        #endregion
        #region Private Methods
        /// <summary>
        /// Method to insert a record into the database
        /// </summary>
        /// <param name="site">Types.JobSite</param>
        /// <returns>int - The record Id of the inserted object</returns>
        private int Insert(Types.JobSite site)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("JobSite_Insert", GetInsertParams(site), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.JobSite site: {0}", site.ToString()), ex);
                Debug.WriteLine("Error in JobSite.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to update a record in the database
        /// </summary>
        /// <param name="site">Types.JobSite</param>
        /// <returns>int - The record Id of the updated object</returns>
        private int Update(Types.JobSite site)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("JobSite_Update", GetUpdateParams(site), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.JobSite site: {0}", site.ToString()), ex);
                Debug.WriteLine("Error in JobSite.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create insert parameters array from an object
        /// </summary>
        /// <param name="site">Types.JobSite</param>
        /// <returns>SqlParameter array</returns>
        private SqlParameter[] GetInsertParams(Types.JobSite site)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@JobId",site.JobId),
                new SqlParameter("@ContactId", site.ContactId),
                new SqlParameter("@ClientSiteId",site.Site.Id),
                new SqlParameter("@Disabled",site.Disabled)
            };
        }

        /// <summary>
        /// Helper method to create update parameters array from an object
        /// </summary>
        /// <param name="site">Types.JobSite</param>
        /// <returns>SqlParameter array</returns>
        private SqlParameter[] GetUpdateParams(Types.JobSite site)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",site.Id),
                new SqlParameter("@JobId",site.JobId),
                new SqlParameter("@ContactId", site.PointOfContact.Id),
                new SqlParameter("@ClientSiteId",site.Site.Id),
                new SqlParameter("@Disabled",site.Disabled)
            };
        }

        /// <summary>
        /// Private method to populate a List of objects from a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>List of Types.JobSite</returns>
        private List<Types.JobSite> ToObjectList(DataSet ds, bool includeItems)
        {
            List<Types.JobSite> retVal = new List<Types.JobSite>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.JobSite obj = null;
            try
            {
                DAL.JobItem jobItemDB = new JobItem(this.DatabaseName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateObject(r);
                    if (includeItems)
                    {
                        //Get the Job Items for the Job Site
                        obj.SiteItems = jobItemDB.GetAllForJobSite(obj.Id);
                    }
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in JobSite.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }


        private Types.JobSite ToObject(DataSet ds, bool includeItems)
        {
            Types.JobSite retVal = null;
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.JobSite obj = null;
            try
            {
                DAL.JobItem jobItemDB = new JobItem(this.DatabaseName);
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateObject(r);
                    if (includeItems)
                    {
                        //Get the Job Items for the Job Site
                        obj.SiteItems = jobItemDB.GetAllForJobSite(obj.Id);
                    }
                    retVal = obj;
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in JobSite.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>JobSite instance with populated contact and site objects</returns>
        private Types.JobSite CreateObject(DataRow row)
        {
            /*
             *  The dataset contains the columns required to create the JobSite
             *  the Contact and the Site details for the object.
             * 
             *  This is to try to reduce the number of calls to the database
             * 
             */
            Types.JobSite obj = new Types.JobSite();
            obj.Id = (int)row["Id"];
            if (row.Table.Columns.IndexOf("BookedWorkItemCount") >= 0)
            {
                obj.BookedWorkItemCount = (int)row["BookedWorkItemCount"];
                if (row["BookedWorkStartDate"].GetType() != typeof(DBNull))
                {
                    obj.BookedWorkStartDate = (DateTime)row["BookedWorkStartDate"];
                }
            }
            
            obj.ClientId = (int)row["ClientId"];
            obj.JobId = (int)row["JobId"];
            obj.ContactId = (int)row["ContactId"];
            //Create and populate a Contact object
            obj.PointOfContact = DAL.ContactSearchResult.CreateSearchResult(row);
            //Create and populate a ClientSite object
            obj.Site = DAL.ClientSite.CreateObject(row);
            return obj;
        }
        #endregion
        #endregion
    }
}
