﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class ClientOrganisation : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public ClientOrganisation(string databaseName)
            : base(databaseName, "User", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public Types.ClientOrganisation GetForId(int Id)
        {
            DataSet ds = null;

            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Business_ClientOrganisation_Id", new SqlParameter[] { new SqlParameter("@Id", Id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("Id: {0}", Id), ex);
                Console.WriteLine("Error in ClientOrganisation.GetForrId: {0}", ex.ToString());
            }

            return ToObject(ds);
        }

        public List<Types.ClientOrganisation> GetAll()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Organisation_Get_All", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", "NONE", ex);
                Console.WriteLine("Error in ClientOrganisation.GetAll: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public int Save(Types.ClientOrganisation obj)
        {
            if (obj.Id < 1)
            {
                //The obj is a new organisation so insert it
                return this.Save("Organisation_Insert", this.Parameters(obj));
            }
            else
            {
                //The object is an existing organisation that needs updating
                return this.Save("Organisation_Update", this.Parameters(obj));
            }
        }

        public Types.ClientOrganisation TotalClientsAmount()
        {
            Types.ClientOrganisation ReturnValue = null;

            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("Get_AllClientAmount", new SqlParameter[] { }, this.DatabaseName);

                var Results = ToObjectList(ds);

                if (Results.Count > 0)
                    ReturnValue = Results[0];
            }
            catch (Exception ex)
            {
                LogError("TotalClientsAmount", "Error in TotalClientsAmount", ex);
                Console.WriteLine("Error in User.TotalClientsAmount: {0}", ex.ToString());
            }

            return ReturnValue;
        }
        #endregion

        #region Private
        private int Save(string procedureName, SqlParameter[] parameters)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(procedureName, parameters, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Save", string.Format("procedureName: {0}, parameters: {1}", procedureName, parameters.ToString()), ex);
                retVal = -1;
            }
            return retVal;
        }

        private SqlParameter[] Parameters(Types.ClientOrganisation obj)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();
            //Check if the object exists in the database
            if (obj.Id > 0)
            {
                //The object is not new and the id needs to be added to the collection
                retVal.Add(Parameterize("@Id", obj.Id));
            }
            retVal.Add(Parameterize("@Name", obj.OrganisationName));
            retVal.Add(Parameterize("@Address1", obj.Address1));
            retVal.Add(Parameterize("@Address2", obj.Address2));
            retVal.Add(Parameterize("@Town", obj.Town));
            retVal.Add(Parameterize("@County", obj.CountyOrState));
            retVal.Add(Parameterize("@PostCode", obj.PostcodeOrZip));
            retVal.Add(Parameterize("@CountryID", obj.CountryID));
            retVal.Add(Parameterize("@UserAccountsEnabled", obj.UserAccountsEnabled));
            retVal.Add(Parameterize("@MobileEditionEnabled", obj.MobileEditionEnabled));
            retVal.Add(Parameterize("@NumLicenses", obj.NumLicenses));
            retVal.Add(Parameterize("@Value", obj.Value));
            return retVal.ToArray();
        }

        private Types.ClientOrganisation ToObject(DataSet ds)
        {
            var ReturnValue = new Types.ClientOrganisation();

            if (!base.HasRows(ds))
                return ReturnValue;

            try
            {
                ReturnValue = CreateResult(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in User.ToObject: {0}", ex.ToString());
            }

            return ReturnValue;
        }


        private List<Types.ClientOrganisation> ToObjectList(DataSet ds)
        {
            List<Types.ClientOrganisation> ReturnValue = new List<Types.ClientOrganisation>();

            if (!base.HasRows(ds))
                return ReturnValue;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    ReturnValue.Add(CreateResult(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in User.ToObjectList: {0}", ex.ToString());
            }

            return ReturnValue;
        }

        private Types.ClientOrganisation CreateResult(DataRow row)
        {
            Types.ClientOrganisation obj = new Types.ClientOrganisation();
            obj.Id = IntValue(row, "ID");
            obj.OrganisationName = StringValue(row, "OrganisationName");
            obj.Address1 = StringValue(row, "Address1");
            obj.Address2 = StringValue(row, "Address2");
            obj.Town = StringValue(row, "Town");
            obj.CountyOrState = StringValue(row, "CountyOrState");
            obj.PostcodeOrZip = StringValue(row, "PostcodeOrZip");
            obj.CountryID = IntValue(row, "CountryID");
            obj.UserAccountsEnabled = BoolValue(row, "UserAccountsEnabled");
            obj.MobileEditionEnabled = BoolValue(row, "MobileEditionEnabled");
            obj.NumLicenses = IntValue(row, "NumLicenses");
            obj.Value = (float)DecimaleValue(row, "Value");
            return obj;
        }
        #endregion
        #endregion
    }
}
