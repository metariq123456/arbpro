﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Marketing.cs" company="Microsec Ltd">
//   2016 Copyright Microsec Ltd
// </copyright>
// <summary>
//   The marketing.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;

    using DGS.ArbPro.Types.Enums;

    /// <summary>
    /// The marketing.
    /// </summary>
    [SuppressMessage("ReSharper", "RedundantExplicitArrayCreation", Justification = "Reviewed. Suppression is OK here.")]
    public class Marketing : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Marketing"/> class.
        /// </summary>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        public Marketing(string databaseName)
            : base(databaseName, "Marketing", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion
        
        #region Methods

        #region Public

        #region WorkCategories

        /// <summary>
        /// The get all work categories.
        /// </summary>
        /// <returns>
        /// The <see cref="Marketing"/>.
        /// </returns>
        public List<Types.Marketing> GetAllWorkCategories()
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingWorkCategory_GetAll", this.DatabaseName);
                retVal = this.PopulateWorkCategoriesFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetAllWorkCategories", "Error in GetAllWorkCategories", ex);
                Debug.WriteLine("Error in Marketing.GetAllWorkCategories: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The get work categories for id.
        /// </summary>
        /// <param name="workCategoryId">
        /// The work category id.
        /// </param>
        /// <returns>
        /// The <see cref="Marketing"/>.
        /// </returns>
        public Types.Marketing GetWorkCategoryForId(int workCategoryId)
        {
            Types.Marketing retVal = new Types.Marketing();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingWorkCategory_GetForId", new SqlParameter[] { new SqlParameter("@Id", workCategoryId) }, this.DatabaseName);
                retVal = this.PopulateWorkCategoryFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetWorkCategoryForId", string.Format("Work Category Id: {0}", workCategoryId), ex);
                Debug.WriteLine("Error in Marketing.GetWorkCategoryForId: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Deletes work categories for id.
        /// </summary>
        /// <param name="workCategoryId">
        /// The work category id.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool DeleteWorkCategoriesForId(int workCategoryId)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteStoredProcDataSet("MarketingWorkCategory_Delete", new SqlParameter[] { new SqlParameter("@Id", workCategoryId) }, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                this.LogError("DeleteWorkCategoriesForId", string.Format("Work Category Id: {0}", workCategoryId), ex);
                Debug.WriteLine("Error in Marketing.DeleteWorkCategoriesForId: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Inserts or updates a work category.
        /// </summary>
        /// <param name="obj">
        /// The work category <see langword="object"/>.
        /// </param>
        /// <returns>
        /// The inserted or updated id.
        /// </returns>
        public int SaveWorkCategory(Types.Marketing obj)
        {
            if (obj.Id <= 0)
            {
                return this.InsertWorkCategory(obj);
            }
            else
            {
                return this.UpdateWorkCategory(obj);
            }
        }

        #endregion

        #region ClientWorkCategories

        /// <summary>
        /// The get all return time categories.
        /// </summary>
        /// <returns> List of return time categories. </returns>
        public List<Types.Marketing> GetAllReturnTimeCategories()
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingReturnTimeCategories_GetAll", this.DatabaseName);
                retVal = this.PopulateReturnTimeCategoriesFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetAllClientWorkCategories", "Error in GetAllClientWorkCategories", ex);
                Debug.WriteLine("Error in Marketing.GetAllClientWorkCategories: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The get all client work categories.
        /// </summary>
        /// <returns>
        /// The <see cref="Marketing"/>.
        /// </returns>
        public List<Types.Marketing> GetAllClientWorkCategories()
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingWorkCategoryJob_GetAll", this.DatabaseName);
                retVal = this.PopulateClientWorkCategoriesFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetAllClientWorkCategories", "Error in GetAllClientWorkCategories", ex);
                Debug.WriteLine("Error in Marketing.GetAllClientWorkCategories: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The get all distinct client work categories.
        /// </summary>
        /// <returns> A list of client work categories. </returns>
        public List<Types.Marketing> GetAllDistinctForSeasonalMarketing()
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingSeasonal_GetAllDistinct", this.DatabaseName);
                retVal = this.PopulateClientSeasonalMarketingFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetAllDistinctForSeasonalMarketing", "Error in GetAllDistinctForSeasonalMarketing", ex);
                Debug.WriteLine("Error in Marketing.GetAllDistinctForSeasonalMarketing: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The get all distinct accepted for seasonal marketing.
        /// </summary>
        /// <returns> A list of client work categories. </returns>
        public List<Types.Marketing> GetAllDistinctAcceptedForSeasonalMarketing()
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingSeasonal_GetAllDistinctAccepted", this.DatabaseName);
                retVal = this.PopulateClientSeasonalMarketingFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetAllDistinctAcceptedForSeasonalMarketing", "Error in GetAllDistinctAcceptedForSeasonalMarketing", ex);
                Debug.WriteLine("Error in Marketing.GetAllDistinctAcceptedForSeasonalMarketing: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The get all distinct not accepted for seasonal marketing.
        /// </summary>
        /// <returns> A list of client work categories. </returns>
        public List<Types.Marketing> GetAllDistinctNotAcceptedForSeasonalMarketing()
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingSeasonal_GetAllDistinctNotAccepted", this.DatabaseName);
                retVal = this.PopulateClientSeasonalMarketingFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetAllDistinctNotAcceptedForSeasonalMarketing", "Error in GetAllDistinctNotAcceptedForSeasonalMarketing", ex);
                Debug.WriteLine("Error in Marketing.GetAllDistinctNotAcceptedForSeasonalMarketing: {0}", ex);
            }

            return retVal;
        }
        
        /// <summary>
        /// Get all clients for marketing.
        /// </summary>
        /// <returns> A list of all clients for marketing. </returns>
        public List<Types.Marketing> GetAllClientMarketing()
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingClients_GetAll", this.DatabaseName);
                retVal = this.PopulateAllClientMarketingDetailsFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetAllClientMarketing", "Error in GetAllClientMarketing", ex);
                Debug.WriteLine("Error in Marketing.GetAllClientMarketing: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The get all for return marketing.
        /// </summary>
        /// <returns> A list of client work categories. </returns>
        public List<Types.Marketing> GetAllForReturnMarketing()
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingReturn_GetAll", this.DatabaseName);
                retVal = this.PopulateClientWorkCategoriesFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetAllForReturnMarketing", "Error in GetAllForReturnMarketing", ex);
                Debug.WriteLine("Error in Marketing.GetAllForReturnMarketing: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The get sent for return marketing.
        /// </summary>
        /// <returns> A list of client work categories. </returns>
        public List<Types.Marketing> GetSentForReturnMarketing()
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingReturn_GetSent", this.DatabaseName);
                retVal = this.PopulateClientWorkCategoriesFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetSentForReturnMarketing", "Error in GetSentForReturnMarketing", ex);
                Debug.WriteLine("Error in Marketing.GetSentForReturnMarketing: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The get work client categories for client id.
        /// </summary>
        /// <param name="clientId">
        /// The client id.
        /// </param>
        /// <returns> The client work category. </returns>
        public List<Types.Marketing> GetWorkClientCategoriesForClientId(int clientId)
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingWorkCategoryJob_GetForClientId", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                retVal = this.PopulateClientWorkCategoriesFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetWorkClientCategoriesForClientId", string.Format("Client Id: {0}", clientId), ex);
                Debug.WriteLine("Error in Marketing.GetWorkClientCategoriesForClientId: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The get work client categories for job id.
        /// </summary>
        /// <param name="jobId"> The job id. </param>
        /// <returns> List of client work categories. </returns>
        public List<Types.Marketing> GetWorkClientCategoriesForJobId(int jobId)
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingWorkCategoryJob_GetForJobId", new SqlParameter[] { new SqlParameter("@JobId", jobId) }, this.DatabaseName);
                retVal = this.PopulateClientWorkCategoriesFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetWorkClientCategoriesForJobId", string.Format("Job Id: {0}", jobId), ex);
                Debug.WriteLine("Error in Marketing.GetWorkClientCategoriesForJobId: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The get client work category for id.
        /// </summary>
        /// <param name="clientWorkCategoryId">
        /// The client work category id.
        /// </param>
        /// <returns>
        /// The <see cref="Marketing"/>.
        /// </returns>
        public Types.Marketing GetClientWorkCategoryForId(int clientWorkCategoryId)
        {
            Types.Marketing retVal = new Types.Marketing();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("MarketingWorkCategoryJob_GetForId", new SqlParameter[] { new SqlParameter("@Id", clientWorkCategoryId) }, this.DatabaseName);
                retVal = this.PopulateClientWorkCategoryFromDataSet(ds);
            }
            catch (Exception ex)
            {
                this.LogError("GetWorkClientCategoryForId", string.Format("Client Work Category Id: {0}", clientWorkCategoryId), ex);
                Debug.WriteLine("Error in Marketing.GetWorkClientCategoryForId: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The delete client work category for id.
        /// </summary>
        /// <param name="clientWorkCategoryId"> The client work category id. </param>
        /// <returns> Success or failure. </returns>
        public bool DeleteClientWorkCategoryForId(int clientWorkCategoryId)
        {
            int retVal = 0;

            try
            {
                retVal = DBHelper.ExecuteStoredProcRowCount("MarketingWorkCategoryJob_DeleteForId", new SqlParameter[] { new SqlParameter("@Id", clientWorkCategoryId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("DeleteClientWorkCategoryForId", string.Format("Client Work Category Id: {0}", clientWorkCategoryId), ex);
                Debug.WriteLine("Error in Marketing.DeleteClientWorkCategoryForId: {0}", ex);
            }

            return retVal > 0;
        }

        /// <summary>
        /// Inserts or updates a client work category.
        /// </summary>
        /// <param name="obj">
        /// The client work category <see langword="object"/>.
        /// </param>
        /// <returns>
        /// The inserted or updated id.
        /// </returns>
        public int SaveClientWorkCategory(Types.Marketing obj)
        {
            if (obj.Id <= 0)
            {
                return this.InsertClientWorkCategory(obj);
            }
            else
            {
                return this.UpdateClientWorkCategory(obj);
            }
        }

        /// <summary>
        /// The remove work client categories for job id - useful for cancellation of quote/ work.
        /// </summary>
        /// <param name="jobId"> The job id. </param>
        /// <returns> Success or failure. </returns>
        public bool UpdateAcceptedWorkClientCategoriesForJobId(int jobId)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteStoredProcDataSet("MarketingWorkCategoryJob_UpdateAcceptedForJobId", new SqlParameter[] { new SqlParameter("@JobId", jobId) }, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                this.LogError("UpdateAcceptedWorkClientCategoriesForJobId", string.Format("Job Id: {0}", jobId), ex);
                Debug.WriteLine("Error in Marketing.UpdateAcceptedWorkClientCategoriesForJobId: {0}", ex);
            }

            return retVal;
        }

        #endregion

        #endregion

        #region Private

        #region WorkCategories

        /// <summary>
        /// Inserts a work category.
        /// </summary>
        /// <param name="obj">
        /// The work category <see langword="object"/>.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int InsertWorkCategory(Types.Marketing obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("MarketingWorkCategory_Insert", this.WorkCategoryParameters(obj, ParameterMode.Insert), this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("InsertWorkCategory", string.Format("Types.Marketing obj: {0}", obj), ex);
                Debug.WriteLine("Error in Marketing.InsertWorkCategory: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Updates a work category.
        /// </summary>
        /// <param name="obj">
        /// The work category <see langword="object"/>.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int UpdateWorkCategory(Types.Marketing obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("MarketingWorkCategory_Update", this.WorkCategoryParameters(obj, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("UpdateWorkCategory", string.Format("Types.Marketing obj: {0}", obj), ex);
                Debug.WriteLine("Error in Marketing.UpdateWorkCategory: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Adds the parameters to an array to pass to the stored procedure
        /// </summary>
        /// <param name="obj"><see cref="Marketing"/> object</param>
        /// <param name="mode">Insert or update</param>
        /// <returns>Array of parameters</returns>
        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1126:PrefixCallsCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        private SqlParameter[] WorkCategoryParameters(Types.Marketing obj, ParameterMode mode)
        {
            var pars = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                pars.Add(Parameterize("@Id", obj.Id));
            }

            pars.Add(Parameterize("@Name", obj.Name));
            pars.Add(Parameterize("@Description", obj.Description));
            pars.Add(Parameterize("@ReturnTimeCategoryId", (int)obj.ReturnTimeCategory));

            return pars.ToArray();
        }

        /// <summary>
        /// The populate work categories from data set.
        /// </summary>
        /// <param name="ds">
        /// The dataset.
        /// </param>
        /// <returns>
        /// The list of work category objects.
        /// </returns>
        private List<Types.Marketing> PopulateWorkCategoriesFromDataSet(DataSet ds)
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                retVal.AddRange(from DataRow r in ds.Tables[0].Rows select this.CreateWorkCategory(r));
            }
            catch (Exception ex)
            {
                this.LogError("PopulateWorkCategoriesFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in PopulateWorkCategoriesFromDataSet: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The populate single work category from data set.
        /// </summary>
        /// <param name="ds">
        /// The dataset.
        /// </param>
        /// <returns>
        /// The work category <see langword="object"/>
        /// </returns>
        private Types.Marketing PopulateWorkCategoryFromDataSet(DataSet ds)
        {
            Types.Marketing retVal = new Types.Marketing();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                retVal = this.CreateWorkCategory(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                this.LogError("PopulateWorkCategoryFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in PopulateWorkCategoryFromDataSet: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The create work category.
        /// </summary>
        /// <param name="r">
        /// The data row.
        /// </param>
        /// <returns>
        /// The work category <see langword="object"/>.
        /// </returns>
        private Types.Marketing CreateWorkCategory(DataRow r)
        {
            Types.Marketing retVal = new Types.Marketing();
            retVal.Id = (int)r["Id"];
            retVal.Name = this.StringValue(r, "Name");
            retVal.Description = this.StringValue(r, "Description");
            retVal.ReturnTimeCategory = (MarketingReturnTimeCategoriesEnum)r["ReturnTimeCategoryId"];

            return retVal;
        }

        #endregion

        #region ClientWorkCategories

        /// <summary>
        /// Inserts a client work category.
        /// </summary>
        /// <param name="obj">
        /// The client work category <see langword="object"/>.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int InsertClientWorkCategory(Types.Marketing obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("MarketingWorkCategoryJob_Insert", this.ClientWorkCategoryParameters(obj, ParameterMode.Insert), this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("InsertClientWorkCategory", string.Format("Types.Marketing obj: {0}", obj), ex);
                Debug.WriteLine("Error in Marketing.InsertClientWorkCategory: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Updates a client work category.
        /// </summary>
        /// <param name="obj">
        /// The client work category <see langword="object"/>.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int UpdateClientWorkCategory(Types.Marketing obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("MarketingWorkCategoryJob_Update", this.ClientWorkCategoryParameters(obj, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("UpdateClientWorkCategory", string.Format("Types.Marketing obj: {0}", obj), ex);
                Debug.WriteLine("Error in Marketing.UpdateClientWorkCategory: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Adds the parameters to an array to pass to the stored procedure
        /// </summary>
        /// <param name="obj">The client work category <see langword="object"/></param>
        /// <param name="mode">Insert or update</param>
        /// <returns>Array of parameters</returns>
        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1126:PrefixCallsCorrectly", Justification = "Reviewed. Suppression is OK here.")]
        private SqlParameter[] ClientWorkCategoryParameters(Types.Marketing obj, ParameterMode mode)
        {
            var pars = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                pars.Add(Parameterize("@Id", obj.Id));
            }

            pars.Add(Parameterize("@JobId", obj.JobId));
            pars.Add(Parameterize("@Name", obj.Name));
            pars.Add(Parameterize("@Description", obj.Description));
            pars.Add(Parameterize("@Comments", obj.Comments));
            pars.Add(Parameterize("@ReturnDate", obj.ReturnDate));
            pars.Add(Parameterize("@QuoteAccepted", obj.QuoteAccepted));
            pars.Add(Parameterize("@EmailSent", obj.EmailSent));
            pars.Add(Parameterize("@IsForReturnMarketing", obj.IsForReturnMarketing));
            pars.Add(Parameterize("@EmailSentDate", obj.EmailSentDate));
            pars.Add(Parameterize("@ReturnTimeCategoryId", (int)obj.ReturnTimeCategory));

            return pars.ToArray();
        }

        /// <summary>
        /// The populate client work categories from data set.
        /// </summary>
        /// <param name="ds"> The dataset. </param>
        /// <returns> The list of client work category objects. </returns>
        private List<Types.Marketing> PopulateClientWorkCategoriesFromDataSet(DataSet ds)
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                retVal.AddRange(from DataRow r in ds.Tables[0].Rows select this.CreateClientWorkCategory(r));
            }
            catch (Exception ex)
            {
                this.LogError("PopulateClientWorkCategoriesFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in PopulateClientWorkCategoriesFromDataSet: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The populate return time categories from data set.
        /// </summary>
        /// <param name="ds"> The dataset. </param>
        /// <returns> A list of return time categories. </returns>
        private List<Types.Marketing> PopulateReturnTimeCategoriesFromDataSet(DataSet ds)
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                retVal.AddRange(from DataRow r in ds.Tables[0].Rows select this.CreateReturnTimeCategory(r));
            }
            catch (Exception ex)
            {
                this.LogError("PopulateClientWorkCategoriesFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in PopulateClientWorkCategoriesFromDataSet: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The populate client seasonal marketing from data set.
        /// </summary>
        /// <param name="ds"> The dataset. </param>
        /// <returns> The list of client seasonal marketing objects. </returns>
        private List<Types.Marketing> PopulateClientSeasonalMarketingFromDataSet(DataSet ds)
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                retVal.AddRange(from DataRow r in ds.Tables[0].Rows select this.CreateClientSeasonalMarketing(r));
            }
            catch (Exception ex)
            {
                this.LogError("PopulateClientWorkCategoriesFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in PopulateClientWorkCategoriesFromDataSet: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The populate single client work category from data set.
        /// </summary>
        /// <param name="ds">
        /// The dataset.
        /// </param>
        /// <returns>
        /// The client work category <see langword="object"/>
        /// </returns>
        private Types.Marketing PopulateClientWorkCategoryFromDataSet(DataSet ds)
        {
            Types.Marketing retVal = new Types.Marketing();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                retVal = this.CreateClientWorkCategory(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                this.LogError("PopulateClientWorkCategoryFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in PopulateClientWorkCategoryFromDataSet: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Function to populate a list of all clients' details for marketing
        /// </summary>
        /// <param name="ds">The data set</param>
        /// <returns>A list of all clients' details for marketing</returns>
        private List<Types.Marketing> PopulateAllClientMarketingDetailsFromDataSet(DataSet ds)
        {
            List<Types.Marketing> retVal = new List<Types.Marketing>();

            // Check if there is any data to be returned
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return retVal;
            }

            try
            {
                retVal.AddRange(from DataRow r in ds.Tables[0].Rows select this.CreateAllClientMarketing(r));
            }
            catch (Exception ex)
            {
                this.LogError("PopulateAllClientMarketingDetailsFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in PopulateAllClientMarketingDetailsFromDataSet: {0}", ex);
            }

            return retVal;
        }

        /// <summary>
        /// The create client work category.
        /// </summary>
        /// <param name="r">
        /// The data row.
        /// </param>
        /// <returns>
        /// The client work category <see langword="object"/>.
        /// </returns>
        private Types.Marketing CreateClientWorkCategory(DataRow r)
        {
            Types.Marketing retVal = new Types.Marketing();
            retVal.Id = this.IntValue(r, "Id");
            retVal.JobId = this.IntValue(r, "JobId");
            retVal.ClientName = this.StringValue(r, "ClientName");
            retVal.Address = this.StringValue(r, "ClientAddress");
            retVal.Telephone = this.StringValue(r, "Telephone");
            retVal.Mobile = this.StringValue(r, "Mobile");
            retVal.Email = this.StringValue(r, "EmailAddress");
            retVal.Name = this.StringValue(r, "Name");
            retVal.Description = this.StringValue(r, "Description");
            retVal.Comments = this.StringValue(r, "Comments");
            retVal.ReturnDate = this.DateTimeValue(r, "ReturnDate");
            retVal.QuoteAccepted = this.BoolValue(r, "QuoteAccepted");
            retVal.EmailSent = this.BoolValue(r, "EmailSent");
            retVal.EmailSentDate = this.DateTimeValue(r, "EmailSentDate");
            retVal.IsForReturnMarketing = this.BoolValue(r, "IsForReturnMarketing");
            int timeCatId = this.IntValue(r, "ReturnTimeCategoryId");
            retVal.ReturnTimeCategory = (MarketingReturnTimeCategoriesEnum)timeCatId;

            return retVal;
        }

        /// <summary>
        /// The create return time category.
        /// </summary>
        /// <param name="r"> The data row. </param>
        /// <returns> List of return time categories. </returns>
        private Types.Marketing CreateReturnTimeCategory(DataRow r)
        {
            Types.Marketing retVal = new Types.Marketing();
            retVal.TimeCategoryId = this.IntValue(r, "CategoryId");
            retVal.TimeCategoryName = this.StringValue(r, "Value");

            return retVal;
        }

        /// <summary>
        /// The create client seasonal marketing.
        /// </summary>
        /// <param name="r"> The data row. </param>
        /// <returns> A seasonal marketing object. </returns>
        private Types.Marketing CreateClientSeasonalMarketing(DataRow r)
        {
            Types.Marketing retVal = new Types.Marketing();
            retVal.ClientName = this.StringValue(r, "ClientName");
            retVal.Address = this.StringValue(r, "ClientAddress");
            retVal.Telephone = this.StringValue(r, "Telephone");
            retVal.Mobile = this.StringValue(r, "Mobile");
            retVal.Email = this.StringValue(r, "EmailAddress");
            retVal.Name = this.StringValue(r, "Name");
            retVal.Description = this.StringValue(r, "Description");
            retVal.QuoteAccepted = this.BoolValue(r, "QuoteAccepted");

            return retVal;
        }

        /// <summary>
        /// The create all client marketing.
        /// </summary>
        /// <param name="r"> The data row. </param>
        /// <returns> A client marketing object. </returns>
        private Types.Marketing CreateAllClientMarketing(DataRow r)
        {
            Types.Marketing retVal = new Types.Marketing();
            retVal.ClientName = this.StringValue(r, "ClientName");
            retVal.Address = this.StringValue(r, "ClientAddress");
            retVal.Telephone = this.StringValue(r, "Telephone");
            retVal.Mobile = this.StringValue(r, "Mobile");
            retVal.Email = this.StringValue(r, "EmailAddress");

            return retVal;
        }

        #endregion

        #endregion

        #endregion
    }
}
