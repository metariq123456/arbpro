﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class TreeProtectionOrder : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public TreeProtectionOrder(string databaseName)
            : base(databaseName, "TreeProtectionOrder", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Method to Update or insert a TPO into the datebase
        /// </summary>
        /// <param name="obj">Types.TreeProtectionOrder object to be inserted or updated</param>
        /// <returns>The id of the inserted/updated row or -1 if there was an error</returns>
        public int Save(Types.TreeProtectionOrder obj)
        {
            if (obj.Id > 0)
                return this.Update(obj);
            else
                return this.Insert(obj);
        }

        /// <summary>
        /// Method to get a TPO based on its Id
        /// </summary>
        /// <param name="tpoId">The Id of the record to be retreaved</param>
        /// <returns>Types.TreeProtectionOrder object, the object will have an Id of -1 if an error occured</returns>
        public Types.TreeProtectionOrder GetForId(int tpoId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TPO_GetForId", new SqlParameter[] { new SqlParameter("@TpoId", tpoId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int tpoId: {0}", tpoId), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.GetForId: {0}", ex.ToString());
            }
            return ToFirstObject(ds);
        }

        /// <summary>
        /// Method to get a TPO for an associated Quote.
        /// The method utilises the primary key from the Quote table and not the Quote reference
        /// </summary>
        /// <param name="quoteId">Int - The record Id of the quote the TPO is for</param>
        /// <returns>Types.TreeProtectionOrder object, the object will have an Id of -1 if an error occured</returns>
        public Types.TreeProtectionOrder GetForQuote(int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TPO_GetForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int quoteId: {0}", quoteId), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.GetForQuote: {0}", ex.ToString());
            }
            return ToFirstObject(ds);
        }

        public Types.TreeProtectionOrder GetForQuoteAndJobSite(int quoteId, int jobSiteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TPO_GetForQuoteAndJobSite", new SqlParameter[]
                { 
                    new SqlParameter("@QuoteId", quoteId),
                    new SqlParameter("@JobSiteId", jobSiteId)
                },
                this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteAndJobSite", string.Format("int quoteId: {0} jobSiteId: {1}", quoteId, jobSiteId), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.GetForQuoteAndJobSite: {0}", ex.ToString());
            }
            return ToFirstObject(ds);
        }

        /// <summary>
        /// Method to get all TPOs for an associated Quote.
        /// </summary>
        /// <param name="quoteId">
        /// The quote ID <see cref="int"/> value.
        /// </param>
        /// <returns>
        /// List of TPOs for a given quote id.
        /// </returns>
        public List<Types.TreeProtectionOrder> GetAllForQuote(int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TPO_GetForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int quoteId: {0}", quoteId), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.GetForQuote: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        /// <summary>
        /// Method to get a list of all TPOs that have a reference like the specified value
        /// </summary>
        /// <param name="refenceType">TpoReferenceTypeEnum -The type of reference to search for</param>
        /// <param name="value">string - The part or full reference to search for</param>
        /// <returns>List of Types.TreeProtectionOrder or an empty list if there is an error</returns>
        public List<Types.TreeProtectionOrder> GetForReferenceLike(TpoReferenceTypeEnum refenceType, string value)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TPO_GetForReferenceLike", new SqlParameter[] { new SqlParameter("@RefType", (int)refenceType), new SqlParameter("@Reference", value) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForReferenceLike", string.Format("TpoReferenceTypeEnum refenceType: {0}, string value: {1}", refenceType, value), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.GetForReferenceLike: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        /// <summary>
        /// Method to get all TPOs at a particular stage
        /// </summary>
        /// <param name="stage">Enum value for the stage to search for</param>
        /// <returns>List of Types.TreeProtectionOrder or an empty list if there is an error</returns>
        public List<Types.TreeProtectionOrder> GetForProcessStage(TreeProtectionProgreesEnum stage)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TPO_GetForProcessStage", new SqlParameter[] { new SqlParameter("@Stage", (int)stage) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForProcessStage", string.Format("int stage: {0}", stage), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.GetForProcessStage: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        /// <summary>
        /// Method to get all TPOs that are below a particular stage
        /// </summary>
        /// <param name="stage">Enum value for the stage to get values below</param>
        /// <returns>List of Types.TreeProtectionOrder or an empty list if there is an error</returns>
        public List<Types.TreeProtectionOrder> GetForProcessStageBelow(TreeProtectionProgreesEnum stage)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TPO_GetForProcessStageBelow", new SqlParameter[] { new SqlParameter("@Stage", (int)stage) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForProcessStageBelow", string.Format("int stage: {0}", stage), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.GetForProcessStageBelow: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        /// <summary>
        /// Method to get all TPOs that are above a particular stage
        /// </summary>
        /// <param name="stage">Enum value for the stage to get values above</param>
        /// <returns>List of Types.TreeProtectionOrder or an empty list if there is an error</returns>
        public List<Types.TreeProtectionOrder> GetForProcessStageAbove(TreeProtectionProgreesEnum stage)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TPO_GetForProcessStageAbove", new SqlParameter[] { new SqlParameter("@Stage", (int)stage) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForProcessStageAbove", string.Format("int stage: {0}", stage), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.GetForProcessStageAbove: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        /// <summary>
        /// Method to get all TPOs that are for a particular status
        /// </summary>
        /// <param name="status">TreeProtectionStatusEnum value for the required status</param>
        /// <returns>List of Types.TreeProtectionOrder or an empty list if there is an error</returns>
        public List<Types.TreeProtectionOrder> GetForStatus(TreeProtectionStatusEnum status)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TPO_GetForStatus", new SqlParameter[] { new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatus", string.Format("TreeProtectionStatusEnum status: {0}", status), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.GetForStatus: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        /// <summary>
        /// Method to get all TPOs that are not for a particular status
        /// </summary>
        /// <param name="status">TreeProtectionStatusEnum value for the required status</param>
        /// <returns>List of Types.TreeProtectionOrder or an empty list if there is an error</returns>
        public List<Types.TreeProtectionOrder> GetForStatusNot(TreeProtectionStatusEnum status)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("TPO_GetForStatusNot", new SqlParameter[] { new SqlParameter("@Status", (int)status) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForStatusNot", string.Format("TreeProtectionStatusEnum status: {0}", status), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.GetForStatusNot: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public int DuplicateTPOForQuote(int CurQuoteId, int NewQuoteId)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("TPO_Duplicate", new SqlParameter[] { new SqlParameter("@NewQuoteId", NewQuoteId), new SqlParameter("@CurQuoteId", CurQuoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("DuplicateTPO", string.Format("CurQuoteId: {0}, SiteId {1}, NewQuoteId {2}", CurQuoteId.ToString(), NewQuoteId.ToString()), ex);
            }
            return retVal;
        }
        #endregion
        #region Private
        private int Insert(Types.TreeProtectionOrder obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("TPO_Insert", Parameters(obj, ParameterMode.Insert), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.TreeProtectionOrder obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Update(Types.TreeProtectionOrder obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("TPO_Update", Parameters(obj, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.TreeProtectionOrder obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] Parameters(Types.TreeProtectionOrder obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();
            //Only add the objects Id to the params collection if it is being updated
            if(mode == ParameterMode.Update)
                retVal.Add(new SqlParameter("@Id",obj.Id));

            retVal.Add(new SqlParameter("@QuoteId", obj.QuoteId));
            retVal.Add(new SqlParameter("@JobSiteId", obj.JobSiteId));
            retVal.Add(new SqlParameter("@Status", obj.Status));
            retVal.Add(new SqlParameter("@ProgressStage", obj.ProgressStage));
            retVal.Add(new SqlParameter("@LocalAuthorityName", obj.LocalAuthorityName));
            retVal.Add(new SqlParameter("@CouncilReference", obj.CouncilReference));
            retVal.Add(new SqlParameter("@InternalReference", obj.InternalReference));
            retVal.Add(new SqlParameter("@AppealReference", obj.AppealReference));
            //Dates must use the Parameterize function to enable insert of DBNull
            retVal.Add(Parameterize("@ApplicationDate", obj.ApplicationDate));
            retVal.Add(Parameterize("@RegistrationDate", obj.RegistrationDate));
            retVal.Add(Parameterize("@InitialDecisionDate", obj.InitialDecisionDate));
            retVal.Add(Parameterize("@AppealDate", obj.AppealDate));
            retVal.Add(Parameterize("@AppealDecisionDate", obj.AppealDecisionDate));
            retVal.Add(Parameterize("@SixWeekStatutory", obj.RegistrationDate.Ticks <= 0 ? DateTime.MinValue : obj.SixWeekStatutory));
            retVal.Add(Parameterize("@EightWeekStatutory", obj.RegistrationDate.Ticks <= 0 ? DateTime.MinValue : obj.EightWeekStatutory));
            retVal.Add(new SqlParameter("@Notes", obj.Notes));
            retVal.Add(new SqlParameter("@TpoOfficer", obj.TpoOfficer));
            retVal.Add(new SqlParameter("@ContactNumber", obj.ContactNumber));
            // retVal.Add(new SqlParameter("@OrderType", obj.OrderType)); -- Removed due to DGSAP-726
            retVal.Add(new SqlParameter("@OrderTypeEnum", (int)obj.OrderTypeEnum));
            retVal.Add(new SqlParameter("@OrderNumber", obj.OrderNumber));
            retVal.Add(new SqlParameter("@ConservationArea", obj.ConservationArea));
            retVal.Add(new SqlParameter("@Conformationreceived", obj.Conformationreceived));

            return retVal.ToArray();
        }

        private List<Types.TreeProtectionOrder> ToObjectList(DataSet ds)
        {
            List<Types.TreeProtectionOrder> retVal = new List<Types.TreeProtectionOrder>();

            if (ds == null | ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.TreeProtectionOrder ToFirstObject(DataSet ds)
        {
            Types.TreeProtectionOrder retVal = new Types.TreeProtectionOrder();

            if (ds == null | ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToFirstObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        #endregion
        #region Internal
        internal Types.TreeProtectionOrder CreateInstance(DataRow row)
        {
            Types.TreeProtectionOrder obj = new Types.TreeProtectionOrder();

            obj.Id = (int)row["Id"];
            obj.QuoteId = (int)row["QuoteId"];
            obj.JobSiteId = (int)row["JobSiteId"];
            obj.QuoteReference = (string)row["QuoteReference"];
            obj.Status = (TreeProtectionStatusEnum)((int)row["Status"]);
            obj.ProgressStage = (TreeProtectionProgreesEnum)((int)row["ProgressStage"]);
            obj.LocalAuthorityName = (string)row["LocalAuthorityName"];
            obj.CouncilReference = (string)row["CouncilReference"];
            obj.InternalReference = (string)row["InternalReference"];
            obj.AppealReference = (string)row["AppealReference"];
            //Dates must use the base methods due to columns containing null values
            obj.ApplicationDate = base.DateTimeValue(row, "ApplicationDate");
            obj.RegistrationDate = base.DateTimeValue(row, "RegistrationDate");
            obj.InitialDecisionDate = base.DateTimeValue(row, "InitialDecisionDate");
            obj.AppealDate = base.DateTimeValue(row, "AppealDate");
            obj.AppealDecisionDate = base.DateTimeValue(row, "AppealDecisionDate");
            obj.SixWeekStatutory = base.DateTimeValue(row, "SixWeekStatutory");
            obj.EightWeekStatutory = base.DateTimeValue(row, "EightWeekStatutory");
            obj.Notes = (string)row["Notes"];
            obj.TpoOfficer = (string)row["TpoOfficer"];
            obj.ContactNumber = (string)row["ContactNumber"];
            // obj.OrderType = (string)row["OrderType"];
            obj.OrderTypeEnum = (TreeProtectionOrderTypeEnum)((int)row["OrderTypeEnum"]);
            obj.OrderNumber = (string)row["OrderNumber"];
            obj.ConservationArea = (bool)row["ConservationArea"];
            obj.Conformationreceived = (bool)row["Conformationreceived"];

            return obj;
        }
        #endregion
        #endregion
    }
}
