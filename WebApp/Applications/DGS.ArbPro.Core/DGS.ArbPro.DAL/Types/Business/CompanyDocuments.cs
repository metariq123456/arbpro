﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class CompanyDocuments : Contracts.DBContextBase
    {

        public CompanyDocuments(string databaseName) : base(databaseName, "CompanyDocuments", DateTime.Parse("1970-01-01")) { }

        public List<Types.CompanyDocumentsObject> GetDocumentsAll()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentImage_GetAll", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAll", "None", ex);
                Console.WriteLine("Error in CompanyDocumentsObject.DocumentImage_GetAll: {0}", ex.ToString());
            }
            return GetResultsDataSet(ds);
        }

        private List<Types.CompanyDocumentsObject> GetResultsDataSet(DataSet ds)
        {
            List<Types.CompanyDocumentsObject> retVal = new List<Types.CompanyDocumentsObject>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("GetResultsDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Staff.GetResultsDataSet: {0}", ex.ToString());
            }

            return retVal;
        }
        public Types.CompanyDocumentsObject GetForId(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Documents_GetForId", new SqlParameter[] { new SqlParameter("@Id", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int id: {0}", id.ToString()), ex);
                Console.WriteLine("Error in Documents.GetForId: {0}", ex.ToString());
            }
            return GetFirstFromDataSet(ds);
        }
        private Types.CompanyDocumentsObject GetFirstFromDataSet(DataSet ds)
        {
            Types.CompanyDocumentsObject retVal = new Types.CompanyDocumentsObject();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("GetFirstFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Staff.GetFirstFromDataSet: {0}", ex.ToString());
            }


            return retVal;
        }
        public int Save(Types.CompanyDocumentsObject obj)
        {
            if (obj.Id > 0)
                return Update(obj);
            else
                return Insert(obj);
        }
        public bool Delete(int Id) 
        {
            bool retval = false;
            try
            {
                int rows = DBHelper.ExecuteStoredProcRowCount("CompanyDocument_Delete", GetDeleteParams(Id), this.DatabaseName);
                retval = rows > 0;
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.CompanyDocumentsObject obj: {0}", Id.ToString()), ex);
                Console.WriteLine("Error in CompanyDocumentsObject: {0}", ex.ToString());
                retval = false;
            }
            return retval;

        }
        private int Insert(Types.CompanyDocumentsObject obj)
        {
            int retVal = -1;
            try
            {
                var p = GetInsertParams(obj);
                retVal = DBHelper.ExecuteStoredProcReturnID("CompanyDocument_Insert", GetInsertParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.CompanyDocumentsObject obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in CompanyDocumentsObject: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Update(Types.CompanyDocumentsObject obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("CompanyDocument_Update", GetUpdateParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.CompanyDocumentsObject obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in CompanyDocumentsObject: {0}", ex.ToString());
            }
            return retVal;
        }
        private SqlParameter[] GetInsertParams(Types.CompanyDocumentsObject obj)
        {
            //NOTE: An associated User account is not inserted into a new staff row
            return new SqlParameter[]
            {
                new SqlParameter("@DocumentName",obj.DocumentName),
                new SqlParameter("@DocumentType",obj.DocumentType),
                new SqlParameter("@DocumentDate",obj.DocumentDate),
                new SqlParameter("@Staff",obj.Staff),
                Parameterize("DocumentImage", obj.DocumentImage),
                new SqlParameter("DocumentFileName",obj.DocumentFileName),

            };
        }
        private SqlParameter[] GetUpdateParams(Types.CompanyDocumentsObject obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",obj.Id),
                new SqlParameter("@DocumentName",obj.DocumentName),
                new SqlParameter("@DocumentType",obj.DocumentType),
                new SqlParameter("@DocumentDate",obj.DocumentDate),
                new SqlParameter("@Staff",obj.Staff),
                Parameterize("DocumentImage", obj.DocumentImage),
                new SqlParameter("DocumentFileName",obj.DocumentFileName),
            };
        }
        private SqlParameter[] GetDeleteParams(int id)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",id),
            };
        }

        private Types.CompanyDocumentsObject CreateObject(DataRow row)
        {
            Types.CompanyDocumentsObject obj = new Types.CompanyDocumentsObject();
            obj.Id = (int)row["Id"];
            obj.DocumentName = (string)row["DocumentName"];
            obj.DocumentDate = (DateTime)row["DocumentDate"];
            obj.DocumentType = (string)row["DocumentType"];
            obj.Staff = (string)row["Staff"];
            obj.DocumentImage = (byte[])row["DocumentImage"];
            obj.DocumentFileName = (string)row["DocumentFileName"];
            obj.DefaultAttachment = BoolValue(row, "DefaultAttachment");
            return obj;
        }
    }

}
