﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.DAL
{
    public class Calendar : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Calendar(string databaseName)
            : base(databaseName, "Calendar", DateTime.Parse("1970-01-01"))
        { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get
        public List<Types.CalendarGeneric> GetGenericList(DateTime StartDate, DateTime EndDate)
        {
            List<Types.CalendarGeneric> retVal = null;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_GetGenericList", new SqlParameter[] {
                    Parameterize("@StartDate", StartDate),
                    Parameterize("@EndDate", EndDate)
                }, this.DatabaseName);
                retVal = PopulateGenericResults(ds);
            }
            catch (Exception ex)
            {
                LogError("GetGenericList", string.Format("startDate: {0} - endDate: {1}", StartDate, EndDate), ex);
                Console.WriteLine("Error in Calendar.GetGenericList: {0}", ex.ToString());
            }

            return retVal;
        }

        public Types.CalendarGeneric GetGeneric(Int32 ItemID)
        {
            Types.CalendarGeneric retVal = null;

            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_GetGeneric", new SqlParameter[] {
                    Parameterize("@ID", ItemID)
                }, this.DatabaseName);

                retVal = PopulateGenericResult(ds);
            }
            catch (Exception ex)
            {
                LogError("GetGeneric", string.Format("itemID: {0}", ItemID), ex);
                Console.WriteLine("Error in Calendar.GetGeneric: {0}", ex.ToString());
            }

            return retVal;
        }

        public List<Types.CalendarQuote> GetEnquiriesForJob(Int32 JobID)
        {
            List<Types.CalendarQuote> retVal = null;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_GetEnquiriesForJob", new SqlParameter[] {
                    Parameterize("@JobID", JobID)
                }, DatabaseName);
                retVal = PopulateQuoteResults(ds);
            }
            catch (Exception ex)
            {
                LogError("GetQuoteList", string.Format("sjobID: {0}", JobID), ex);
                Console.WriteLine("Error on Calendar.GetQuoteList: {0}", ex.ToString());
            }

            return retVal;
        }

        public List<Types.CalendarQuote> GetQuoteList(DateTime StartDate, DateTime EndDate)
        {
            List<Types.CalendarQuote> retVal = null;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_GetQuoteList", new SqlParameter[] {
                    Parameterize("@StartDate", StartDate),
                    Parameterize("@EndDate", EndDate)
                }, DatabaseName);
                retVal = PopulateQuoteResults(ds);
            }
            catch (Exception ex)
            {
                LogError("GetQuoteList", string.Format("startDate: {0} - endDate: {1}", StartDate, EndDate), ex);
                Console.WriteLine("Error on Calendar.GetQuoteList: {0}", ex.ToString());
            }

            return retVal;
        }

        public Types.CalendarQuote GetQuote(Int32 ItemID)
        {
            Types.CalendarQuote retVal = null;

            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_GetQuote", new SqlParameter[] {
                    Parameterize("@ID", ItemID)
                }, this.DatabaseName);

                retVal = PopulateQuoteResult(ds);
            }
            catch (Exception ex)
            {
                LogError("GetQuote", string.Format("itemID: {0}", ItemID), ex);
                Console.WriteLine("Error in Calendar.GetQuote: {0}", ex.ToString());
            }

            return retVal;
        }

        public List<Types.CalendarWork> GetWorkList(DateTime StartDate, DateTime EndDate)
        {
            List<Types.CalendarWork> retVal = null;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_GetWorkList", new SqlParameter[] {
                    Parameterize("@StartDate", StartDate),
                    Parameterize("@EndDate", EndDate)
                }, DatabaseName);
                 retVal = ToWorkBookEventList(ds);
                /*retVal = ToWorkEventList(ds);*/
            }
            catch (Exception ex)
            {
                LogError("GetQuoteList", string.Format("startDate: {0} - endDate: {1}", StartDate, EndDate), ex);
                Console.WriteLine("Error on Calendar.GetQuoteList: {0}", ex.ToString());
            }

            return retVal;
        }

        public Types.CalendarWork GetWorkListForWorkPlanId(int WorkPlanId)
        {
            Types.CalendarWork retVal = null;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_GetWorkByWorkPlanId", new SqlParameter[] {
                    Parameterize("@WorkPlanId", WorkPlanId)
                }, DatabaseName);
                retVal = ToWorkBookEventListNew(ds);
                /*retVal = ToWorkEventList(ds);*/
            }
            catch (Exception ex)
            {
                LogError("GetWorkListForWorkPlanId", string.Format("workPlanId: {0}", WorkPlanId), ex);
                Console.WriteLine("Error on Calendar.GetWorkListForWorkPlanId: {0}", ex.ToString());
            }

            return retVal;
        }

        public List<Types.CalendarWork> GetWorkList(DateTime startDate, DateTime endDate, int workPlanId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_GetAroundWorkPlan", new SqlParameter[] {
                    Parameterize("@StartDate", startDate),
                    Parameterize("@EndDate", endDate),
                    Parameterize("@WorkPlanId", workPlanId)
                }, DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetWorkList", string.Format("startDate: {0} - endDate: {1} - workPlanId: {2}", startDate, endDate, workPlanId), ex);
                Console.WriteLine("Error on Calendar.GetWorkList: {0}", ex.ToString());
            }

            return ToWorkEventList(ds);
        }

        public List<Types.CalendarWork> GetWorkListForStaff(DateTime StartDate, DateTime EndDate, Int32 StaffID)
        {
            var ReturnValue = GetWorkList(StartDate, EndDate);

            if (StaffID != -1)
            {
                var tsDB = new DAL.TeamStaff(DatabaseName);

                ReturnValue.RemoveAll((Event) =>
                {
                    var Staff = tsDB.GetForTeamId(Event.TeamId);
                    return !Staff.Exists(s => s.StaffId == StaffID);
                });
            }

            return ReturnValue;
        }

        public Types.CalendarWork GetForWorkPlan(int workPlanId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_Work_GetForWorkPlan", new SqlParameter[] { Parameterize("@WorkPlanId", workPlanId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWork", string.Format("workPlanId: {0}", workPlanId), ex);
            }
            return PopulateWorkResult(ds);
        }

        public Types.CalendarWork GetWork(Int32 ItemID)
        {
            Types.CalendarWork retVal = null;

            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_GetWork", new SqlParameter[] {
                    Parameterize("@ItemID", ItemID)
                }, this.DatabaseName);

                retVal = PopulateWorkResult(ds);
            }
            catch (Exception ex)
            {
                LogError("GetQuote", string.Format("itemID: {0}", ItemID), ex);
                Console.WriteLine("Error in Calendar.GetQuote: {0}", ex.ToString());
            }

            return retVal;
        }
        #endregion

        #region Set

        public bool DeleteWork(int appointmentId)
        {
            return DeleteAppointment("Calendar_DeleteWork", appointmentId);
        }

        public bool DeleteGeneric(int appointmentId)
        {
            return DeleteAppointment("Calendar_DeleteGeneric", appointmentId);
        }

        public bool DeleteQuote(int appointmentId)
        {
            return DeleteAppointment("Calendar_DeleteQuote", appointmentId);
        }

        public Boolean SaveGeneric(Types.CalendarGeneric obj)
        {
            DataSet ds = null;

            try
            {
                var Params = new SqlParameter[] {
                    Parameterize("ID", obj.Id),
                    Parameterize("StartDateTime", obj.StartDateTime),
                    Parameterize("EndDateTime", obj.EndDateTime),
                    Parameterize("AllDay", obj.AllDay),
                    Parameterize("Name", obj.Name),
                    Parameterize("Comment", obj.Comment),
                    Parameterize("EventType", (int)obj.Type),
                    Parameterize("StaffID", obj.StaffID)
                };

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_SaveGeneric", Params, DatabaseName);


            }
            catch (Exception ex)
            {
                LogError("SaveGeneric", string.Format("itemID: {0}", obj.Id), ex);
                Console.WriteLine("Error in Client.SaveGeneric: {0}", ex.ToString());
            }

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return false;

            foreach (DataRow Row in ds.Tables[0].Rows)
                obj.Id = IntValue(Row, "NewID");

            return true;
        }

        public Boolean SaveQuote(Types.CalendarQuote obj)
        {
            DataSet ds = null;

            try
            {
                var Params = new List<SqlParameter>();
                Params.Add(Parameterize("ID", obj.Id));
                Params.Add(Parameterize("StartDateTime", obj.StartDateTime));
                Params.Add(Parameterize("EndDateTime", obj.EndDateTime));
                Params.Add(Parameterize("SurveyorID", obj.SurveyorID));
                Params.Add(Parameterize("JobSiteID", obj.JobSiteID));
                Params.Add(Parameterize("Completed", obj.Completed));
                Params.Add(Parameterize("LastUpdated", obj.LastUpdated));
                Params.Add(Parameterize("LastDownloaded", obj.LastDownloaded));
                Params.Add(Parameterize("UploadTime", obj.UploadTime));
                Params.Add(Parameterize("TemplateId", obj.TemplateId));

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_SaveQuote", Params.ToArray(), DatabaseName);


            }
            catch (Exception ex)
            {
                LogError("SaveQuote", string.Format("itemID: {0}", obj.Id), ex);
                Console.WriteLine("Error in Calendar.SaveQuote: {0}", ex.ToString());
            }

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return false;

            foreach (DataRow Row in ds.Tables[0].Rows)
                obj.Id = IntValue(Row, "NewID");

            return true;
        }
        public Boolean SaveWork(Types.CalendarWork obj)
        {
            DataSet ds = null;

            try
            {
                var Params = new List<SqlParameter>();
                Params.Add(Parameterize("ID", obj.Id));
                Params.Add(Parameterize("StartDateTime", obj.StartDateTime));
                Params.Add(Parameterize("EndDateTime", obj.EndDateTime.Ticks > 0 ? obj.EndDateTime : obj.StartDateTime));
                Params.Add(Parameterize("SkipDays", (Int32)obj.SkipDays));
                Params.Add(Parameterize("JobSiteID", obj.JobSiteId));
                Params.Add(Parameterize("TeamId", obj.TeamId));
                Params.Add(Parameterize("WorkPlanId", obj.WorkPlanId));
                Params.Add(Parameterize("Comments", obj.Comments));

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_SaveWork", Params.ToArray(), DatabaseName);


            }
            catch (Exception ex)
            {
                LogError("SaveWork", string.Format("itemID: {0}", obj.Id), ex);
                Console.WriteLine("Error in Calendar.SaveWork: {0}", ex.ToString());
            }

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return false;

            foreach (DataRow Row in ds.Tables[0].Rows)
                obj.Id = IntValue(Row, "NewID");

            return true;
        }
        public Boolean SaveJobWork(Types.CalendarWork obj)
        {
            DataSet ds = null;

            try
            {
                var Params = new List<SqlParameter>();
                Params.Add(Parameterize("ID", obj.Id));
                Params.Add(Parameterize("StartDateTime", obj.StartDateTime));
                Params.Add(Parameterize("EndDateTime", obj.EndDateTime.Ticks > 0 ? obj.EndDateTime : obj.StartDateTime));
                Params.Add(Parameterize("SkipDays", (Int32)obj.SkipDays));
                Params.Add(Parameterize("JobSiteID", obj.JobSiteId));
                Params.Add(Parameterize("TeamId", obj.TeamId));
                Params.Add(Parameterize("WorkPlanId", obj.WorkPlanId));
                Params.Add(Parameterize("Comments", obj.Comments));
                Params.Add(Parameterize("RepeatTypeID", obj.RepeatTypeId));
                Params.Add(Parameterize("EveryFor", obj.EveryFor));
                Params.Add(Parameterize("EveryDay", obj.EveryDay));
                Params.Add(Parameterize("OnMonthDay", obj.OnMonthDay));
                Params.Add(Parameterize("WeekDayIDs", obj.WeekDay));
                Params.Add(Parameterize("IsRecurring", obj.IsRecurring));
                Params.Add(Parameterize("StartTimeDay1", obj.StartTimeDay1New));
                Params.Add(Parameterize("ConnectiveDayEndTime", obj.ConnectiveDayEndTimeNew));
                Params.Add(Parameterize("LastDayEndTime", obj.LastDayEndTimeNew));

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_SaveJobWork", Params.ToArray(), DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("SaveJobWork", string.Format("itemID: {0}", obj.Id), ex);
                Console.WriteLine("Error in Calendar.SaveJobWork: {0}", ex.ToString());
            }

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return false;

            foreach (DataRow Row in ds.Tables[0].Rows)
                obj.Id = IntValue(Row, "NewID");
            try
            {
                //multiple date entry in CalendarWorkdate
                if (obj.CalendarWorkDate.Count > 0 && obj.Id > 0)
                {
                    for (int i = 0; i < obj.CalendarWorkDate.Count; i++)
                    {
                        var Params = new List<SqlParameter>();
                        Params.Add(Parameterize("BookDate", obj.CalendarWorkDate[i].BookDate));
                        Params.Add(Parameterize("CalendarWorkId", obj.Id));
                        ds = DBHelper.ExecuteStoredProcDataSet("Calendar_SaveWorkBookDate", Params.ToArray(), DatabaseName);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("SaveJobWork", string.Format("Error in saving saving calendar dates"), ex);
                Console.WriteLine("Error in Calendar.SaveJobWork: {0}", ex.ToString());
            }

            return true;
        }
        public Boolean SaveJobWorkAppointmentForEdit(Types.CalendarWork obj)
        {
            DataSet ds = null;
            Boolean RetVal = true;
            try
            {
                var Params = new List<SqlParameter>();
               
                int rows = DBHelper.ExecuteStoredProcRowCount("CalendarWorkDate_Delete", new SqlParameter[] {
                Parameterize("@CalendarWorkId", obj.Id)}, this.DatabaseName);
                Params.Add(Parameterize("ID", obj.Id));
                Params.Add(Parameterize("StartDateTime", obj.StartDateTime));
                Params.Add(Parameterize("EndDateTime", obj.EndDateTime.Ticks > 0 ? obj.EndDateTime : obj.StartDateTime));
                Params.Add(Parameterize("SkipDays", (Int32)obj.SkipDays));
                Params.Add(Parameterize("JobSiteID", obj.JobSiteId));
                Params.Add(Parameterize("TeamId", obj.TeamId));
                Params.Add(Parameterize("WorkPlanId", obj.WorkPlanId));
                Params.Add(Parameterize("Comments", obj.Comments));
                Params.Add(Parameterize("RepeatTypeID", obj.RepeatTypeId));
                Params.Add(Parameterize("EveryFor", obj.EveryFor));
                Params.Add(Parameterize("EveryDay", obj.EveryDay));
                Params.Add(Parameterize("OnMonthDay", obj.OnMonthDay));
                Params.Add(Parameterize("WeekDayIDs", obj.WeekDay));
                Params.Add(Parameterize("IsRecurring", obj.IsRecurring));
                Params.Add(Parameterize("StartTimeDay1", obj.StartTimeDay1New));
                Params.Add(Parameterize("ConnectiveDayEndTime", obj.ConnectiveDayEndTimeNew));
                Params.Add(Parameterize("LastDayEndTime", obj.LastDayEndTimeNew));

                ds = DBHelper.ExecuteStoredProcDataSet("Calendar_SaveJobWork", Params.ToArray(), DatabaseName);
                int RetId;
                foreach (DataRow Row in ds.Tables[0].Rows)
                {
                    RetId = IntValue(Row, "NewID");
                }
                if(!obj.IsRecurring)
                {
                    //multiple date entry in CalendarWorkdate
                    if (obj.CalendarWorkDate.Count > 0)
                    {
                        rows = DBHelper.ExecuteStoredProcRowCount("CalendarWorkDate_Delete", new SqlParameter[] {
                            Parameterize("@CalendarWorkId", obj.Id)}, this.DatabaseName);
                        for (int i = 0; i < obj.CalendarWorkDate.Count; i++)
                        {
                            Params = new List<SqlParameter>();
                            Params.Add(Parameterize("BookDate", obj.CalendarWorkDate[i].BookDate));
                            Params.Add(Parameterize("CalendarWorkId", obj.Id));
                            ds = DBHelper.ExecuteStoredProcDataSet("Calendar_SaveWorkBookDate", Params.ToArray(), DatabaseName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("SaveJobWork", string.Format("itemID: {0}", obj.Id), ex);
                Console.WriteLine("Error in Calendar.SaveJobWork: {0}", ex.ToString());
                RetVal = false;
            }

            return RetVal;
        }
        #endregion
        #endregion

        #region Private
        private bool DeleteAppointment(string storedProcedure, int id)
        {
            bool retVal = false;
            try
            {
                int rows = DBHelper.ExecuteStoredProcRowCount(storedProcedure, new SqlParameter[] { Parameterize("@ID", id) }, this.DatabaseName);
                retVal = rows > 0;
            }
            catch (Exception ex)
            {
                retVal = false;
                LogError("DeleteAppointment", string.Format("storedProcedure: {0}, appointmentId: {1} ", storedProcedure, id), ex);
                Console.WriteLine("Error in Calendar.DeleteAppointment: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.CalendarGeneric> PopulateGenericResults(DataSet data)
        {
            List<Types.CalendarGeneric> ReturnValue = new List<Types.CalendarGeneric>();

            if (data == null || data.Tables == null || data.Tables.Count == 0 || data.Tables[0].Rows.Count == 0)
                return ReturnValue;

            try
            {
                foreach (DataRow Row in data.Tables[0].Rows)
                {
                    ReturnValue.Add(CreateGeneric(Row));
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateGenericResults", string.Format("DataSet: {0}", data.ToString()), ex);
                Console.WriteLine("Error in Client.PopulateGenericResults: {0}", ex.ToString());
            }
            return ReturnValue;

        }

        private Types.CalendarGeneric PopulateGenericResult(DataSet data)
        {
            Types.CalendarGeneric ReturnValue = null;

            if (data == null || data.Tables == null || data.Tables.Count == 0 || data.Tables[0].Rows.Count == 0)
                return ReturnValue;

            try
            {
                ReturnValue = CreateGeneric(data.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("PopulateGenericResult", string.Format("DataSet: {0}", data.ToString()), ex);
                Console.WriteLine("Error in Client.PopulateGenericResult: {0}", ex.ToString());
            }
            return ReturnValue;

        }

        private Types.CalendarGeneric CreateGeneric(DataRow Row)
        {
            Types.CalendarGeneric obj = new Types.CalendarGeneric();
            obj.Id = IntValue(Row, "ID");
            obj.StartDateTime = DateTimeValue(Row, "StartDateTime");
            obj.EndDateTime = DateTimeValue(Row, "EndDateTime");
            obj.AllDay = BoolValue(Row, "AllDay");
            obj.Name = StringValue(Row, "Name");
            obj.Comment = StringValue(Row, "Comment");
            obj.Type = (DGS.ArbPro.Types.Enums.CalendarGenericType)IntValue(Row, "EventType");
            obj.StaffID = IntValue(Row, "StaffID");
            obj.CalendarString = StringValue(Row, "CalendarString");
            return obj;
        }

        private List<Types.CalendarQuote> PopulateQuoteResults(DataSet data)
        {
            List<Types.CalendarQuote> ReturnValue = new List<Types.CalendarQuote>();

            if (data == null || data.Tables == null || data.Tables.Count == 0 || data.Tables[0].Rows.Count == 0)
                return ReturnValue;

            try
            {
                foreach (DataRow Row in data.Tables[0].Rows)
                {
                    ReturnValue.Add(CreateQuote(Row));
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateQuoteResults", string.Format("DataSet: {0}", data.ToString()), ex);
                Console.WriteLine("Error in Calendar.PopulateQuoteResults: {0}", ex.ToString());
            }
            return ReturnValue;
        }

        private Types.CalendarQuote PopulateQuoteResult(DataSet data)
        {
            Types.CalendarQuote ReturnValue = null;

            if (data == null || data.Tables == null || data.Tables.Count == 0 || data.Tables[0].Rows.Count == 0)
                return ReturnValue;

            try
            {
                ReturnValue = CreateQuote(data.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("PopulateQuoteResult", string.Format("DataSet: {0}", data.ToString()), ex);
                Console.WriteLine("Error in Calendar.PopulateQuoteResult: {0}", ex.ToString());
            }
            return ReturnValue;
        }

        private Types.CalendarQuote CreateQuote(DataRow Row)
        {
            Types.CalendarQuote obj = new Types.CalendarQuote();
            obj.Id = IntValue(Row, "ID");
            obj.StartDateTime = DateTimeValue(Row, "StartDateTime");
            obj.EndDateTime = DateTimeValue(Row, "EndDateTime");
            obj.SurveyorID = IntValue(Row, "SurveyorID");
            obj.JobSiteID = IntValue(Row, "JobSiteID");
            obj.JobSiteName = StringValue(Row, "JobSiteName");
            obj.Completed = BoolValue(Row, "Completed");
            obj.LastUpdated = DateTimeValue(Row, "LastUpdated");
            obj.LastDownloaded = DateTimeValue(Row, "LastDownloaded");
            obj.UploadTime = DateTimeValue(Row, "UploadTime");
            obj.CalendarString = StringValue(Row, "CalendarString");
            obj.TemplateId = IntValue(Row, "TemplateId");
            return obj;
        }
        private List<Types.CalendarWork> ToWorkEventList(DataSet data)
        {
            List<Types.CalendarWork> ReturnValue = new List<Types.CalendarWork>();

            if (!base.HasRows(data))
                return ReturnValue;

            try
            {
                foreach (DataRow Row in data.Tables[0].Rows)
                {
                    ReturnValue.Add(CreateWork(Row));
                }
            }
            catch (Exception ex)
            {
                LogError("ToWorkEventList", string.Format("DataSet: {0}", data.ToString()), ex);
                Console.WriteLine("Error in Calendar.ToWorkEventList: {0}", ex.ToString());
            }
            return ReturnValue;
        }
        private Types.CalendarWork ToWorkBookEventListNew(DataSet data)
        {
            Types.CalendarWork ReturnValue = new Types.CalendarWork();

            if (!base.HasRows(data))
                return ReturnValue;

            try
            {
                foreach (DataRow Row in data.Tables[0].Rows)
                {
                    var IsRec = BoolValue(Row, "IsRecurring");
                    if (IsRec)
                        ReturnValue = CreateWorkBook(Row);
                    var Id = IntValue(Row, "ID");
                    if (Id > 0 && !IsRec)
                    {
                        DataSet ds = null;
                        ds = DBHelper.ExecuteStoredProcDataSet("Calendar_GetWorkListBookDate", new SqlParameter[] {
                                    Parameterize("@Id", Id), }, DatabaseName);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow subRow in ds.Tables[0].Rows)
                            {
                                Types.CalendarWorkDate CalWorkDate = new Types.CalendarWorkDate();
                                CalWorkDate.BookDate = this.DateTimeValue(subRow, "BookDate");
                                ReturnValue.CalendarWorkDate.Add(CalWorkDate);
                            }
                            ReturnValue.Id = IntValue(Row, "ID");
                            ReturnValue.SkipDays = (Types.Enums.CalendarSkipDaysEnum)IntValue(Row, "SkipDays");
                            ReturnValue.JobSiteId = IntValue(Row, "JobSiteID");
                            ReturnValue.TeamId = IntValue(Row, "StaffRequired");
                            ReturnValue.WorkPlanId = IntValue(Row, "WorkPlanId");
                            ReturnValue.Comments = StringValue(Row, "Comments");
                            ReturnValue.RepeatTypeId = IntValue(Row, "RepeatTypeID");
                            ReturnValue.EveryFor = IntValue(Row, "EveryFor");
                            ReturnValue.EveryDay = BoolValue(Row, "EveryDay");
                            ReturnValue.OnMonthDay = IntValue(Row, "OnMonthDay");
                            ReturnValue.WeekDay = StringValue(Row, "WeekDayIDs");
                            ReturnValue.IsRecurring = BoolValue(Row, "IsRecurring");
                            ReturnValue.JobSiteName = StringValue(Row, "JobSiteName");
                            ReturnValue.CalendarString = StringValue(Row, "CalendarString");
                            ReturnValue.Color = Color.FromArgb(IntValue(Row, "Color"));
                            try
                            {
                                if (!DBNull.Value.Equals(Row["StartTimeDay1"]))
                                {
                                    ReturnValue.StartTimeDay1New = this.DateTimeValue(Row,"StartTimeDay1");
                                }

                                if (!DBNull.Value.Equals(Row["ConnectiveDayEndTime"]))
                                {
                                    ReturnValue.ConnectiveDayEndTimeNew = this.DateTimeValue(Row,"ConnectiveDayEndTime");
                                }
                                if (!DBNull.Value.Equals(Row["LastDayEndTime"]))
                                {
                                    ReturnValue.LastDayEndTimeNew = this.DateTimeValue(Row,"LastDayEndTime");
                                }
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        else
                        {
                            ReturnValue = CreateWork(Row);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("ToWorkEventList", string.Format("DataSet: {0}", data.ToString()), ex);
                Console.WriteLine("Error in Calendar.ToWorkEventList: {0}", ex.ToString());
            }
            return ReturnValue;
        }
        private List<Types.CalendarWork> ToWorkBookEventList(DataSet data)
        {
            List<Types.CalendarWork> ReturnValue = new List<Types.CalendarWork>();

            if (!base.HasRows(data))
                return ReturnValue;

            try
            {
                foreach (DataRow Row in data.Tables[0].Rows)
                {
                    var IsRec = BoolValue(Row, "IsRecurring");
                    if (IsRec)
                        ReturnValue.Add(CreateWorkBook(Row));
                    var Id = IntValue(Row, "ID");
                    if (Id > 0 && !IsRec)
                    {
                        DataSet ds = null;
                        ds = DBHelper.ExecuteStoredProcDataSet("Calendar_GetWorkListBookDate", new SqlParameter[] {
                                    Parameterize("@Id", Id), }, DatabaseName);
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            int DatesCount = 1;
                            foreach (DataRow subRow in ds.Tables[0].Rows)
                            {
                                Types.CalendarWork subobj = new Types.CalendarWork();
                                subobj.Id = IntValue(Row, "ID");
                                subobj.StartDateTime = DateTimeValue(subRow, "BookDate");
                                subobj.EndDateTime = DateTimeValue(subRow, "BookDate");
                                subobj.SkipDays = (Types.Enums.CalendarSkipDaysEnum)IntValue(Row, "SkipDays");
                                subobj.JobSiteId = IntValue(Row, "JobSiteID");
                                subobj.TeamId = IntValue(Row, "StaffRequired");
                                subobj.WorkPlanId = IntValue(Row, "WorkPlanId");
                                subobj.Comments = StringValue(Row, "Comments");
                                subobj.RepeatTypeId = IntValue(Row, "RepeatTypeID");
                                subobj.EveryFor = IntValue(Row, "EveryFor");
                                subobj.EveryDay = BoolValue(Row, "EveryDay");
                                subobj.OnMonthDay = IntValue(Row, "OnMonthDay");
                                subobj.WeekDay = StringValue(Row, "WeekDayIDs");
                                subobj.IsRecurring = BoolValue(Row, "IsRecurring");
                                subobj.JobSiteName = StringValue(Row, "JobSiteName");
                                subobj.CalendarString = StringValue(Row, "CalendarString");
                                subobj.Color = Color.FromArgb(IntValue(Row, "Color"));
                                subobj.IsMultiDates = true;
                                subobj.StartTimeDay1New = this.DateTimeValue(Row, "StartTimeDay1");
                                subobj.ConnectiveDayEndTimeNew = this.DateTimeValue(Row, "ConnectiveDayEndTime");
                                subobj.LastDayEndTimeNew = this.DateTimeValue(Row, "LastDayEndTime");
                                DateTime StartDate = new DateTime(subobj.StartDateTime.Year, subobj.StartDateTime.Month, subobj.StartDateTime.Day, subobj.StartTimeDay1New.Hour, subobj.StartTimeDay1New.Minute, subobj.StartTimeDay1New.Second);
                                subobj.StartDateTime = StartDate;
                                if (ds.Tables[0].Rows.Count > 1 && DatesCount == ds.Tables[0].Rows.Count)
                                {
                                    DateTime EndDate = new DateTime(subobj.EndDateTime.Year, subobj.EndDateTime.Month, subobj.EndDateTime.Day, subobj.LastDayEndTimeNew.Hour, subobj.LastDayEndTimeNew.Minute, subobj.LastDayEndTimeNew.Second);
                                    subobj.EndDateTime = EndDate;

                                }
                                else
                                {

                                   
                                    DateTime EndDate = new DateTime(subobj.EndDateTime.Year, subobj.EndDateTime.Month, subobj.EndDateTime.Day, subobj.ConnectiveDayEndTimeNew.Hour, subobj.ConnectiveDayEndTimeNew.Minute, subobj.ConnectiveDayEndTimeNew.Second);
                                    subobj.EndDateTime = EndDate;
                                }
                                ReturnValue.Add(subobj);
                                DatesCount += 1;
                            }
                        }
                        else
                        {
                            ReturnValue.Add(CreateWork(Row));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("ToWorkEventList", string.Format("DataSet: {0}", data.ToString()), ex);
                Console.WriteLine("Error in Calendar.ToWorkEventList: {0}", ex.ToString());
            }
            return ReturnValue;
        }

        private Types.CalendarWork PopulateWorkResult(DataSet data)
        {
            Types.CalendarWork ReturnValue = new Types.CalendarWork();

            if (data == null || data.Tables == null || data.Tables.Count == 0 || data.Tables[0].Rows.Count == 0)
                return ReturnValue;

            try
            {
                ReturnValue = CreateWork(data.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("PopulateWorkResult", string.Format("DataSet: {0}", data.ToString()), ex);
                Console.WriteLine("Error in Calendar.PopulateWorkResult: {0}", ex.ToString());
            }
            return ReturnValue;
        }

        private Types.CalendarWork CreateWorkBook(DataRow Row)
        {
            Types.CalendarWork obj = new Types.CalendarWork();
            obj.Id = IntValue(Row, "ID");
            obj.StartDateTime = DateTimeValue(Row, "StartDateTime");
            obj.EndDateTime = DateTimeValue(Row, "EndDateTime");
            obj.SkipDays = (Types.Enums.CalendarSkipDaysEnum)IntValue(Row, "SkipDays");
            obj.JobSiteId = IntValue(Row, "JobSiteID");
            obj.TeamId = IntValue(Row, "StaffRequired");
            obj.WorkPlanId = IntValue(Row, "WorkPlanId");
            obj.Comments = StringValue(Row, "Comments");
            obj.RepeatTypeId = IntValue(Row, "RepeatTypeID");
            obj.EveryFor = IntValue(Row, "EveryFor");
            obj.EveryDay = BoolValue(Row, "EveryDay");
            obj.OnMonthDay = IntValue(Row, "OnMonthDay");
            obj.WeekDay = StringValue(Row, "WeekDayIDs");
            obj.IsRecurring = BoolValue(Row, "IsRecurring");
            obj.JobSiteName = StringValue(Row, "JobSiteName");
            obj.CalendarString = StringValue(Row, "CalendarString");
            obj.Color = Color.FromArgb(IntValue(Row, "Color"));
            obj.StartTimeDay1New = this.DateTimeValue(Row, "StartTimeDay1");
            obj.ConnectiveDayEndTimeNew = this.DateTimeValue(Row, "ConnectiveDayEndTime");
            obj.LastDayEndTimeNew = this.DateTimeValue(Row, "LastDayEndTime");
            return obj;
        }
        private Types.CalendarWork CreateWork(DataRow Row)
        {
            Types.CalendarWork obj = new Types.CalendarWork();
            obj.Id = IntValue(Row, "ID");
            obj.StartDateTime = DateTimeValue(Row, "StartDateTime");
            obj.EndDateTime = DateTimeValue(Row, "EndDateTime");
            obj.SkipDays = (Types.Enums.CalendarSkipDaysEnum)IntValue(Row, "SkipDays");
            obj.JobSiteId = IntValue(Row, "JobSiteID");
            obj.TeamId = IntValue(Row, "StaffRequired");
            obj.WorkPlanId = IntValue(Row, "WorkPlanId");
            obj.Comments = StringValue(Row, "Comments");
            obj.JobSiteName = StringValue(Row, "JobSiteName");
            obj.CalendarString = StringValue(Row, "CalendarString");
            obj.Color = Color.FromArgb(IntValue(Row, "Color"));
            return obj;
        }
        #endregion
        #endregion
    }
}
