﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class RiskReference : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public RiskReference(string databaseName)
            : base(databaseName, "RiskReference")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #endregion
    }
}
