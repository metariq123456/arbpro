﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using log4net;
using System.Threading.Tasks;
using DGS.ArbPro.Types;
using DGS.ArbPro.Utility;
using System.Web.Configuration;


namespace DGS.ArbPro.DAL
{
    using System.Threading;

    public class User : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public User(string databaseName)
            : base(databaseName, "User")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public Methods
        public List<Types.User> GetForOrganisation(int orgId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("User_GetForOrganisation", new SqlParameter[] { Parameterize("@OrgId", orgId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForOrganisation", string.Format("int orgId: {0}", orgId), ex);
            }
            return this.ToObjectList(ds);
        }

        /// <summary>
        /// Marks the user with the email address passed in as logged in
        /// </summary>
        /// <param name="emailAddress">The email address of the user</param>
        /// <param name="SessionId">The unique GUID identifying this user's session.</param>
        public void Login(string emailAddress, string SessionId)
        {
            try
            {
                Utility.Logger.Log.InfoFormat("User {0} logged in with GUID {1}", emailAddress, SessionId);

                int ReturnValue;
                ReturnValue = DBHelper.ExecuteStoredProcReturnID("Business_User_Login", new SqlParameter[] { new SqlParameter("@EmailAddress", emailAddress.ToLower()), new SqlParameter("@SessionId", SessionId) }, this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("Login", string.Format("EmailAddress: {0}", emailAddress), ex);
                Console.WriteLine("Error in User.Login: {0}", ex.ToString());
            }

        }

        /// <summary>
        /// Marks the user with the email address passed in as logged out.
        /// </summary>
        /// <param name="emailAddress">The email address of the user to update.</param>
        public void Logout(string emailAddress)
        {
            try
            {
                Utility.Logger.Log.InfoFormat("User {0} logged out", emailAddress);

                int ReturnValue;
                ReturnValue = DBHelper.ExecuteStoredProcReturnID("Business_User_Logout", new SqlParameter[] { new SqlParameter("@EmailAddress", emailAddress.ToLower()) }, this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("Logout", string.Format("userId: {0}", emailAddress), ex);
                Console.WriteLine("Error in User.Logout: {0}", ex.ToString());
            }

        }
        public Types.User GetForUserId(int UserId)
        {
            Types.User ReturnValue = null;

            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("Business_User_UserId", new SqlParameter[] { new SqlParameter("@UserId", UserId) }, this.DatabaseName);

                var Results = ToObjectList(ds);

                if (Results.Count > 0)
                    ReturnValue = Results[0];
            }
            catch (Exception ex)
            {
                LogError("GetForUserId", string.Format("userId: {0}", UserId), ex);
                Console.WriteLine("Error in User.GetForUserId: {0}", ex.ToString());
            }

            return ReturnValue;
        }


        public Types.User GetForMobileUserId(int mobileUserId)
        {
            Types.User ReturnValue = null;

            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("Business_User_GetFor_MobileUserId", new SqlParameter[] { new SqlParameter("@MobileUserId", mobileUserId) }, this.DatabaseName);

                var Results = ToObjectList(ds);

                if (Results.Count > 0)
                    ReturnValue = Results[0];
            }
            catch (Exception ex)
            {
                LogError("GetForUserId", string.Format("mobileUserId: {0}", mobileUserId), ex);
                Console.WriteLine("Error in User.GetForMobileUserId: {0}", ex.ToString());
            }

            return ReturnValue;
        }

        /// <summary>
        /// Retrieves the session id for the user.
        /// </summary>
        /// <remarks>
        /// This only retrieves the session ID - it is called many times as requests are authorized, so needs to be as lightweight as possible, not retrieving
        /// any more data than is absolutely necessary.
        /// </remarks>
        /// <param name="Email"></param>
        /// <returns></returns>
        public string GetSessionId(String Email)
        {
            string ReturnValue = null;

            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("Business_User_GetSessionId", new SqlParameter[] { new SqlParameter("@Email", Email.ToLower()) }, this.DatabaseName);
                if (ds.Tables.Count == 1 && ds.Tables[0].Rows.Count == 1)
                {
                    ReturnValue = StringValue(ds.Tables[0].Rows[0], "SessionId");
                }
                else
                {
                    throw new Exception(string.Format("More than one row returned when getting session id for user {0}", Email));
                }

                return ReturnValue;
            }
            catch (Exception ex)
            {
                LogError("GetSessionId", string.Format("email: {0}", Email), ex);
                Console.WriteLine("Error in User.GetForEmail: {0}", ex.ToString());
            }

            return ReturnValue;
        }

        public Types.User GetForEmail(String Email)
        {
            Types.User ReturnValue = null;

            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("Business_User_Email", new SqlParameter[] { new SqlParameter("@Email", Email) }, this.DatabaseName);

                var Results = ToObjectList(ds);

                if (Results.Count > 0)
                    ReturnValue = Results[0];
            }
            catch (Exception ex)
            {
                LogError("GetForEmail", string.Format("email: {0}", Email), ex);
                Console.WriteLine("Error in User.GetForEmail: {0}", ex.ToString());
            }

            return ReturnValue;
        }

        /// <summary>
        /// Generates a unique Guid for the user, to be used in a password reset page.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string GenerateResetGuid(Types.User user)
        {
            string ReturnValue = "";

            try
            {
                DataSet ds;
                ds = DBHelper.ExecuteStoredProcDataSet("Business_User_PwdResetGenerate",
                    new SqlParameter[] { 
                        new SqlParameter("@EmailAddress", user.EmailAddress),
                    }, this.DatabaseName);

                ReturnValue = (string)ds.Tables[0].Rows[0]["Guid"];

            }
            catch (Exception ex)
            {
                LogError("GenerateResetGuid", string.Format("EmailAddress: {0}", user.EmailAddress), ex);
                Console.WriteLine("Error in User.GenerateResetGuid: {0}", ex.ToString());
                ReturnValue = "";
            }

            return ReturnValue;
        }

        /// <summary>
        /// Generates a unique Guid for the user, to be used in a password reset page.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool ClearResetGuid(Types.User user)
        {
            bool ReturnValue;

            try
            {
                int RowCount;
                RowCount = DBHelper.ExecuteStoredProcRowCount("Business_User_PwdResetClear",
                    new SqlParameter[] { 
                        new SqlParameter("@EmailAddress", user.EmailAddress),
                    }, this.DatabaseName);

                ReturnValue = RowCount == 1;

            }
            catch (Exception ex)
            {
                LogError("ClearResetGuid", string.Format("EmailAddress: {0}", user.EmailAddress), ex);
                Console.WriteLine("Error in User.ClearResetGuid: {0}", ex.ToString());
                ReturnValue = false;
            }

            return ReturnValue;
        }

        public Types.User GetForResetGuid(String guid)
        {
            Types.User returnValue = null;
            int timeoutMinutes;

            try
            {
                timeoutMinutes = int.Parse(WebConfigurationManager.AppSettings["PasswordResetTimeoutMins"]);
            }
            catch (Exception)
            {
                timeoutMinutes = 15;
            }

            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet(
                    "Business_User_ResetGuid",
                    new SqlParameter[]
                        {
                            new SqlParameter("@PwdResetGuid", guid), new SqlParameter("@TimeoutMinutes", timeoutMinutes),
                        },
                    this.DatabaseName);

                var results = this.ToObjectList(ds);

                if (results.Count > 0)
                {
                    returnValue = results[0];
                }
            }
            catch (Exception ex)
            {
                this.LogError("GetForResetGuid", string.Format("Guid: {0}", guid), ex);
                Console.WriteLine("Error in User.GetForGuid: {0}", ex.ToString());
            }

            return returnValue;
        }

        public int Save(Types.User obj)
        {
            //The save process has been updated to return an int to enable update of staff
            if (obj.Id == -1)
            {
                return Insert(obj);
            }
            else
            {
                return Update(obj);
            }
        }

        //TODO: Implement
        public int DisableMobileUser()
        {
            return -1;
        }

        public Types.User GetAdminUser(int OrgId)
        {
            Types.User UserInfo = new Types.User();
            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("Get_AdminUser", new SqlParameter[] { new SqlParameter("@ClientId", OrgId) }, this.DatabaseName);

                var Results = ToObjectList(ds);

                if (Results.Count > 0)
                {
                    UserInfo = Results[0];
                    UserInfo.PasswordHashed = "ArbPr012";
                }
                    
            }
            catch (Exception ex)
            {
                LogError("getAdminUser", string.Format("OrgId: {0}", OrgId), ex);
                Console.WriteLine("Error in User.getAdminUser: {0}", ex.ToString());
            }

            return UserInfo;
        }
        #endregion

        #region Private Methods

        int Insert(Types.User Object)
        {
            int retVal = -1;

            try
            {
                //Check if the creation date has been set
                if (Object.CreationDate == DateTime.MinValue)
                    Object.CreationDate = DateTime.UtcNow;

                retVal = DBHelper.ExecuteStoredProcReturnID("Business_User_Insert", Parameters(Object, ParameterMode.Insert).ToArray(), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("userId: {0}", Object.Id), ex);
                Console.WriteLine("Error in User.Save: {0}", ex.ToString());
                retVal = -1;
            }

            return retVal;
        }

        int Update(Types.User Object)
        {
            int retVal = -1;

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Business_User_Update", Parameters(Object, ParameterMode.Update).ToArray(), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("userId: {0}", Object.Id), ex);
                Console.WriteLine("Error in User.Save: {0}", ex.ToString());
                retVal = -1;
            }

            return retVal;
        }

        public int AccountOrCompanyExist(String EmailAddress,String CompanyName)
        {
            int retVal = -1;

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Check_Email_And_Company", Parameters(EmailAddress, CompanyName).ToArray(), this.DatabaseName);
               
            }
            catch (Exception ex)
            {
                LogError("AccountOrCompanyExist", string.Format("EmailAddress: {0} and CompanyName :{1}", EmailAddress, CompanyName), ex);
                Console.WriteLine("Error in AccountOrCompanyExist: {0}", ex.ToString());
                retVal = -1;
            }

            return retVal;
        }

        public int CreateDatabase(Types.ClientOrganisation ClientOrg, Types.User UserInfo)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("New_User_DataBase", Parameters(ClientOrg, UserInfo).ToArray(), this.DatabaseName);
               
            }
            catch (Exception ex)
            {
                LogError("CreateDatabase", string.Format("Error For Company: {0} and User :{1}", ClientOrg.OrganisationName, UserInfo.FirstName), ex);
                Console.WriteLine("Error in AccountOrCompanyExist: {0}", ex.ToString());
                retVal = -1;
            }
            return retVal;
        }

        public int SaveUser(Types.User UserInfo)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("New_User_Insert", Parameters(UserInfo).ToArray(), this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("SaveUser", string.Format("Error For User :{1}", UserInfo.FirstName), ex);
                Console.WriteLine("Error in SaveUser: {0}", ex.ToString());
                retVal = -1;
            }
            return retVal;
        }

        public int CheckUserExist(String Email)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Check_User_Exist", new SqlParameter[] { new SqlParameter("@Email", Email) }, this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("CheckUserExist", string.Format("Error For Email :{1}", Email), ex);
                Console.WriteLine("Error in CheckUserExist: {0}", ex.ToString());
                retVal = -1;
            }
            return retVal;
        }

        public int TotalNoOfDatabases()
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Count_TotalDatabases", new SqlParameter[] { }, this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("TotalNoOfDatabases", string.Format("Error in TotalNoOfDatabases:"), ex);
                Console.WriteLine("Error in TotalNoOfDatabases: {0}", ex.ToString());
                retVal = -1;
            }
            return retVal;
        }

        public int EnableAllUsersForOrg(int OrgId)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Enable_All_Users_ForOrg", new SqlParameter[] { new SqlParameter("@OrgId", OrgId) }, this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("EnableAllUsersForOrg", string.Format("Error in for Org Id:{0}", OrgId), ex);
                Console.WriteLine("Error in EnableAllUsersForOrg: {0}", ex.ToString());
                retVal = -1;
            }
            return retVal;
        }

        public int DisableAllUsersForOrg(int OrgId)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Disable_All_Users_ForOrg", new SqlParameter[] { new SqlParameter("@OrgId", OrgId) }, this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("DisableAllUsersForOrg", string.Format("Error for org id:{0}",OrgId), ex);
                Console.WriteLine("Error in DisableAllUsersForOrg: {0}", ex.ToString());
                retVal = -1;
            }
            return retVal;
        }
        public int CheckAllUsersEnabledForOrg(int OrgId)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Check_AllUserEnableForOrg", new SqlParameter[] { new SqlParameter("@OrgId", OrgId) }, this.DatabaseName);

            }
            catch (Exception ex)
            {
                LogError("CheckAllUsersEnabledForOrg", string.Format("Error for Org Id:{0}", OrgId), ex);
                Console.WriteLine("Error in CheckAllUsersEnabledForOrg: {0}", ex.ToString());
                retVal = -1;
            }
            return retVal;
        }
        private List<SqlParameter> Parameters(Types.User obj, ParameterMode mode)
        {
            List<SqlParameter> ReturnValue;
            ReturnValue = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                ReturnValue.Add(Parameterize("@Id", obj.Id));
            }

            ReturnValue.Add(Parameterize("@CompanyID", obj.CompanyID));
            ReturnValue.Add(Parameterize("@EmailAddress", obj.EmailAddress));
            ReturnValue.Add(Parameterize("@Salt", obj.Salt));
            ReturnValue.Add(Parameterize("@Password", obj.PasswordHashed));
            ReturnValue.Add(Parameterize("@Title", obj.Title));
            ReturnValue.Add(Parameterize("@FirstName", obj.FirstName));
            ReturnValue.Add(Parameterize("@LastName", obj.LastName));
            ReturnValue.Add(Parameterize("@Telephone", obj.Telephone));
            ReturnValue.Add(Parameterize("@UserAccountEnabled", obj.UserAccountEnabled));
            ReturnValue.Add(Parameterize("@MobileEditionEnabled", obj.MobileEditionEnabled));
            ReturnValue.Add(Parameterize("@AuthenticationFailures", obj.AuthenticationFailures));
            ReturnValue.Add(Parameterize("@CompanyPosition", obj.CompanyPosition));
            ReturnValue.Add(Parameterize("@TsAndCsAccepted", obj.TsAndCsAccepted));
            ReturnValue.Add(Parameterize("@Comment", obj.Comment));
            ReturnValue.Add(Parameterize("@CreationDate", obj.CreationDate));
            ReturnValue.Add(Parameterize("@IsOnline", obj.IsOnline));
            ReturnValue.Add(Parameterize("@LastActivityDate", obj.LastActivityDate));
            ReturnValue.Add(Parameterize("@LastLoginDate", obj.LastLoginDate));
            ReturnValue.Add(Parameterize("@IsWebEnabled", obj.IsWebEnabled));
            //ReturnValue.Add(Parameterize("@SessionId", obj.SessionId));

            return ReturnValue;
        }
        private List<SqlParameter> Parameters(Types.User obj)
        {
            List<SqlParameter> ReturnValue;
            ReturnValue = new List<SqlParameter>();
            ReturnValue.Add(Parameterize("@OrgId", obj.CompanyID));
            ReturnValue.Add(Parameterize("@EmailAddress", obj.EmailAddress));
            ReturnValue.Add(Parameterize("@Title", obj.Title));
            ReturnValue.Add(Parameterize("@FirstName", obj.FirstName));
            ReturnValue.Add(Parameterize("@LastName", obj.LastName));
            ReturnValue.Add(Parameterize("@Telephone", obj.Telephone));
            ReturnValue.Add(Parameterize("@Position", obj.CompanyPosition));
            ReturnValue.Add(Parameterize("@Comment", obj.Comment));
            return ReturnValue;
        }
        private List<SqlParameter> Parameters(String EmailAddress, String CompanyName)
        {
            List<SqlParameter> ReturnValue;
            ReturnValue = new List<SqlParameter>();
            ReturnValue.Add(Parameterize("@EmailAddress", EmailAddress));
            ReturnValue.Add(Parameterize("@Company", CompanyName));
            return ReturnValue;
        }
        private List<SqlParameter> Parameters(Types.ClientOrganisation ClientOrg,Types.User UserInfo)
        {
            List<SqlParameter> ReturnValue;
            ReturnValue = new List<SqlParameter>();
            ReturnValue.Add(Parameterize("@CompanyName", ClientOrg.OrganisationName));
            ReturnValue.Add(Parameterize("@AddressLn1", ClientOrg.Address1));
            ReturnValue.Add(Parameterize("@AddressLn2", ClientOrg.Address2));
            ReturnValue.Add(Parameterize("@Town", ClientOrg.Town));
            ReturnValue.Add(Parameterize("@County", ClientOrg.CountyOrState));
            ReturnValue.Add(Parameterize("@PostCode", ClientOrg.PostcodeOrZip));
            ReturnValue.Add(Parameterize("@Country", ClientOrg.CountryID));
            ReturnValue.Add(Parameterize("@License", ClientOrg.NumLicenses));
            ReturnValue.Add(Parameterize("@Value", ClientOrg.Value));
            ReturnValue.Add(Parameterize("@EmailAddress", UserInfo.EmailAddress));
            ReturnValue.Add(Parameterize("@Title", UserInfo.Title));
             ReturnValue.Add(Parameterize("@FirstName", UserInfo.FirstName));
            ReturnValue.Add(Parameterize("@LastName", UserInfo.LastName));
            ReturnValue.Add(Parameterize("@Telephone", UserInfo.Telephone));
            ReturnValue.Add(Parameterize("@Position", UserInfo.CompanyPosition));
            ReturnValue.Add(Parameterize("@Comment", UserInfo.Comment));
            return ReturnValue;
        }
        private List<Types.User> ToObjectList(DataSet ds)
        {
            List<Types.User> ReturnValue = new List<Types.User>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return ReturnValue;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    ReturnValue.Add(CreateResult(r));
                }
            }
            catch (Exception ex)
            {
                LogError("PPopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in User.PopulateResultsFromDataSet: {0}", ex.ToString());
            }

            return ReturnValue;
        }

        private Types.User CreateResult(DataRow row)
        {
            Types.User obj = new Types.User();
            obj.Id = IntValue(row, "Id");
            obj.CompanyID = IntValue(row, "CompanyID");
            obj.EmailAddress = StringValue(row, "EmailAddress");
            obj.Salt = StringValue(row, "Salt");
            obj.PasswordHashed = StringValue(row, "PasswordHashed");
            obj.Title = StringValue(row, "Title");
            obj.FirstName = StringValue(row, "FirstName");
            obj.LastName = StringValue(row, "LastName");
            obj.Telephone = StringValue(row, "Telephone");
            obj.UserAccountEnabled = BoolValue(row, "UserAccountEnabled");
            obj.MobileEditionEnabled = BoolValue(row, "MobileEditionEnabled");
            obj.IsWebEnabled = BoolValue(row, "IsWebEnabled");
            obj.AuthenticationFailures = IntValue(row, "AuthenticationFailures");
            obj.CompanyPosition = StringValue(row, "CompanyPosition");
            obj.TsAndCsAccepted = BoolValue(row, "TsAndCsAccepted");
            obj.Comment = StringValue(row, "Comment");
            obj.CreationDate = DateTimeValue(row, "CreationDate");
            obj.IsOnline = BoolValue(row, "IsOnline");
            obj.LastActivityDate = DateTimeValue(row, "LastActivityDate");
            obj.LastLoginDate = DateTimeValue(row, "LastLoginDate");
            obj.ClientDatabaseName = StringValue(row, "ClientDatabaseName");
            obj.SessionId = StringValue(row, "SessionId");
            //obj.StaffID = IntValue(row, "StaffId");
            return obj;
        }
        #endregion

        #endregion
    }
}
