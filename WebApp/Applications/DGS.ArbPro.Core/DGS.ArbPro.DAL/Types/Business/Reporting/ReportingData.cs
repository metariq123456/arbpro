﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.DAL
{
    using System.Data;
    using System.Data.SqlClient;

    public class ReportingData : Contracts.DBContextBase
    {
        #region Constructors
        public ReportingData(string databaseName)
            : base(databaseName, "ReportingData", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion
        
        #region Methods

        #region Public

        #region Get

        public List<Types.ReportingData> GetEnquiryForDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryLeadSrouceForDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSetForLeadSource(ds);
        }
        public List<Types.ReportingData> GetEnquiryForReceivedDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryReceivedForDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> GetEnquiryForSentDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForSentDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> GetEnquiryForAcceptedDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForAcceptedDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> GetEnquiryForWorkBookedDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForWorkBookedDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> GetEnquiryForWorkInProgressDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null; 
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForWorkInProgressDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> GetEnquiryForWorkCompletedDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForWorkCompletedDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> GetEnquiryForVatTotal(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForVatTotal", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> GetEnquiryForVatValueCollected(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForVatValueCollected", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        
        public List<Types.ReportingData> GetEnquiryForSalesHistoricallyDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForSalesHistoricallyDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> GetEnquiryForSalesOutstandingDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForSalesOutstandingDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> GetEnquiryForIndividualRateDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForIndividualRateDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> GetEnquiryForIndividualSalesDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForIndividualSalesDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> Reporting_GetEnquiryForCategoriesDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForCategoriesDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        public List<Types.ReportingData> GetEnquiryForLetterDates(DateTime startDate, DateTime endDate)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Reporting_GetEnquiryForLetterDates", new SqlParameter[] { new SqlParameter("@StartDate", startDate), new SqlParameter("@EndDate", endDate) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetEnquiryForDates", string.Format("startDate: {0}, endDate: {1}", startDate, endDate), ex);
                Console.WriteLine("Error in ReportingData.GetEnquiryForDates: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }
        


        #endregion

        #endregion

        #region Private

        /// <summary>
        /// Populate a list of ReportingData from a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>List of Types.ReportingData</returns>
        private List<Types.ReportingData> PopulateResultsFromDataSet(DataSet ds)
        {
            List<Types.ReportingData> retVal = new List<Types.ReportingData>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.ReportingData obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateInstance(r);
                    retVal.Add(obj);
                }

                double total = retVal.Sum(item => item.Amount);

                foreach (var o in retVal)
                {
                    o.PercentageAmount = Math.Round((o.Amount / total) * 100, 2);
                }
            }
            catch (Exception ex)
            {
                this.LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ReportingData.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }

            return retVal;
        }
        private List<Types.ReportingData> PopulateResultsFromDataSetForLeadSource(DataSet ds)
        {
            List<Types.ReportingData> retVal = new List<Types.ReportingData>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.ReportingData obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateInstance(r);
                    retVal.Add(obj);
                }

                double total = retVal.Sum(item => item.Amount);

                foreach (var o in retVal)
                {
                    o.PercentageAmount = Math.Round((o.Amount / total) * 100, 2);
                }
            }
            catch (Exception ex)
            {
                this.LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ReportingData.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }

            return retVal;
        }
        private List<Types.ReportingData> PopulateResultsForGraphFromDataSet(DataSet ds)
        {
            List<Types.ReportingData> retVal = new List<Types.ReportingData>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.ReportingData obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateInstanceforGraph(r);
                    retVal.Add(obj);
                }

                double total = retVal.Sum(item => item.Amount);

                foreach (var o in retVal)
                {
                    o.PercentageAmount = Math.Round((o.Amount / total) * 100, 2);
                }
            }
            catch (Exception ex)
            {
                this.LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ReportingData.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }

            return retVal;
        }
        


        private Types.ReportingData CreateInstance(DataRow row)
        {
            Types.ReportingData obj = new Types.ReportingData();

            obj.Name = this.StringValue(row, "Name");
            obj.Amount = this.IntValue(row, "Amount");
            obj.Value = this.DecimaleValue(row, "Value");

            return obj;
        }
        private Types.ReportingData CreateInstanceforGraph(DataRow row)
        {
            Types.ReportingData obj = new Types.ReportingData();

            obj.Name = this.StringValue(row, "Name");
            obj.Amount = this.IntValue(row, "Amount");
            obj.Value = this.DecimaleValue(row, "Value");

            return obj;
        }
       
        #endregion
        #endregion
    }
}
