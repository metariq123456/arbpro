﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class RiskAdditionalItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public RiskAdditionalItem(string databaseName)
            : base(databaseName, "RiskAdditionalItem", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion 

        #region Properties
        #endregion

        #region Methods
        #region Public
        public List<Types.RiskAdditionalItem> GetForRiskAssessment(int assessmentId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Risk_AdditionalRisk_GetForAssessment", new SqlParameter[] { new SqlParameter("RiskId", assessmentId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForRiskAssessment", string.Format("int assessmentId: {0}", assessmentId.ToString()), ex);
                Console.WriteLine("Error in RiskAdditionalItem.GetForRiskAssessment: {0}", ex.ToString());
            }
            return GetResultsFromDataSet(ds);
        }

        public Types.RiskAdditionalItem GetForId(int itemId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Risk_AdditionalRisk_GetForId", new SqlParameter[] { new SqlParameter("Id", itemId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForRiskAssessment", string.Format("int itemId: {0}", itemId.ToString()), ex);
                Console.WriteLine("Error in RiskAdditionalItem.GetForRiskAssessment: {0}", ex.ToString());
            }
            return GetFirstResultFromDataSet(ds);
        }

        public int Save(Types.RiskAdditionalItem obj)
        {
            if (obj.Id < 1)
                return Insert(obj);
            else
                return Update(obj);
        }
        #endregion
        #region Private
        private int Insert(Types.RiskAdditionalItem obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Risk_AdditionalRisk_Insert", GetInsertParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.RiskAdditionalItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in RiskAdditionalItem.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Update(Types.RiskAdditionalItem obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Risk_AdditionalRisk_Update", GetUpdateParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.RiskAdditionalItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in RiskAdditionalItem.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetInsertParams(Types.RiskAdditionalItem obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("RiskAssessmentId",obj.RiskAssessmentId),
                new SqlParameter("AssessedRisk",obj.AssessedRisk),
                new SqlParameter("ControlRequired",obj.ControlRequired)
            };
        }

        private SqlParameter[] GetUpdateParams(Types.RiskAdditionalItem obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("Id",obj.Id),
                new SqlParameter("RiskAssessmentId",obj.RiskAssessmentId),
                new SqlParameter("AssessedRisk",obj.AssessedRisk),
                new SqlParameter("ControlRequired",obj.ControlRequired)
            };
        }

        private Types.RiskAdditionalItem GetFirstResultFromDataSet(DataSet ds)
        {
            Types.RiskAdditionalItem retVal = new Types.RiskAdditionalItem();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("GetFirstResultFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in RiskAdditionalItem.GetFirstResultFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.RiskAdditionalItem> GetResultsFromDataSet(DataSet ds)
        {
            List<Types.RiskAdditionalItem> retVal = new List<Types.RiskAdditionalItem>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("GetResultsFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in RiskAdditionalItem.GetResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.RiskAdditionalItem CreateObject(DataRow row)
        {
            Types.RiskAdditionalItem obj = new Types.RiskAdditionalItem();
            obj.Id = (int)row["Id"];
	        obj.RiskAssessmentId = (int)row["RiskAssessmentId"];
	        obj.AssessedRisk = (string)row["AssessedRisk"];
            obj.ControlRequired = (string)row["ControlRequired"];
            return obj;
        }
        #endregion
        #endregion
    }
}
