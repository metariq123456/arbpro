﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class RiskSection :Contracts.DBContextBase
    {
        #region Members

        #endregion

        #region Constructors
        public RiskSection(string database) : base(database, "RiskSection", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties

        #endregion

        #region Methods
        #region Public
        public List<Types.RiskSection> GetForRiskAssessment(int assessmentId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessmentSection_GetForRisk", new SqlParameter[] { new SqlParameter("@RiskId", assessmentId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForRiskAssessment", string.Format("int assessmentId: {0}", assessmentId.ToString()), ex);
                Console.WriteLine("Error in RiskSection.GetForRiskAssessment: {0}", ex.ToString());
            }
            return GetResultsFromDataSet(ds);
        }

        public Types.RiskSection GetForId(int sectionId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessmentSection_GetForId", new SqlParameter[] { new SqlParameter("@SectionId", sectionId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForRiskAssessment", string.Format("int sectionId: {0}", sectionId.ToString()), ex);
                Console.WriteLine("Error in RiskSection.GetForRiskAssessment: {0}", ex.ToString());
            }
            return GetFirstResultFromDataSet(ds);
        }

        /// <summary>
        /// Update or insert the section.
        /// The Comments is the only field that can be updated.
        /// </summary>
        /// <param name="obj">Types.RiskSection</param>
        /// <returns>Int: The Id of the modified row. -1 if there is an error</returns>
        public int Save(Types.RiskSection obj)
        {
            if (obj.Id < 1)
                return Insert(obj);
            else
                return Update(obj);
        }

        /// <summary>
        /// Update or Insert the section and the section criteria 
        /// </summary>
        /// <param name="obj">Types.RiskSection</param>
        /// <returns>Int: The Id of the modified row. -1 if there is an error</returns>
        public int SaveAll(Types.RiskSection obj)
        {
            if (obj.Id < 1)
                return InsertAll(obj);
            else
                return UpdateAll(obj);
        }
        #endregion
        #region Private
        private int Insert(Types.RiskSection obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskAssessmentSection_Insert", GetInsertParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.RiskSection obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in RiskSection.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Update(Types.RiskSection obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskAssessmentSection_Update", new SqlParameter[] { new SqlParameter("@Id", obj.Id),new SqlParameter("@Comments", obj.Comments) }, this.DatabaseName);
          
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.RiskSection obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in RiskSection.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private int InsertAll(Types.RiskSection obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskAssessmentSection_Insert", GetInsertParams(obj), this.DatabaseName);
                if (retVal > 0)
                {
                    if (!SaveCriteria(obj))
                        retVal = -1;
                }
            }
            catch (Exception ex)
            {
                LogError("InsertAll", string.Format("Types.RiskSection obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in RiskSection.InsertAll: {0}", ex.ToString());
            }
            return retVal;
        }

        private int UpdateAll(Types.RiskSection obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskAssessmentSection_Update", new SqlParameter[] { new SqlParameter("@Id", obj.Id), new SqlParameter("@Comments", obj.Comments) }, this.DatabaseName);
                if (retVal > 0)
                {
                    if (!SaveCriteria(obj))
                        retVal = -1;
                }
            }
            catch (Exception ex)
            {
                LogError("UpdateAll", string.Format("Types.RiskSection obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in RiskSection.UpdateAll: {0}", ex.ToString());
            }
            return retVal;
        }

        private bool SaveCriteria(Types.RiskSection obj)
        {
            int updated = -1;
            DAL.RiskCriterion dbContext = new RiskCriterion(this.DatabaseName);
            foreach (Types.RiskCriterion criterion in obj.Criteria)
            {
                updated = dbContext.Save(criterion);
                if (updated < 1)
                    break;
            }
            return updated > 0;
        }

        private SqlParameter[] GetInsertParams(Types.RiskSection obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@RiskAssessmentId",obj.RiskAssessmentId),
                new SqlParameter("@Title",obj.Title),
                new SqlParameter("@Description",obj.Description),
                new SqlParameter("@ReferenceDoc",obj.ReferenceDoc),
                new SqlParameter("@Chapter",obj.Chapter),
                new SqlParameter("@Comments",obj.Comments)
            };
        }

        private Types.RiskSection GetFirstResultFromDataSet(DataSet ds)
        {
           Types.RiskSection retVal = new Types.RiskSection();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
                DAL.RiskCriterion dbContext = new RiskCriterion(this.DatabaseName);
                retVal.Criteria = dbContext.GetForSectionId(retVal.Id);
                
            }
            catch (Exception ex)
            {
                LogError("GetFirstResultFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in RiskSection.GetFirstResultFromDataSet: {0}", ex.ToString());
            }

            return retVal;
        }

        private List<Types.RiskSection> GetResultsFromDataSet(DataSet ds)
        {
            List<Types.RiskSection> retVal = new List<Types.RiskSection>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("GetResultsFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in RiskSection.GetResultsFromDataSet: {0}", ex.ToString());
            }
            
            DAL.RiskCriterion dbContext = new RiskCriterion(this.DatabaseName);
            foreach (Types.RiskSection section in retVal)
            {
                section.Criteria = dbContext.GetForSectionId(section.Id);
            }
            return retVal;
        }

        private Types.RiskSection CreateObject(DataRow row)
        {
            Types.RiskSection obj = new Types.RiskSection();
            obj.Id = (int)row["Id"];
            obj.RiskAssessmentId = (int)row["RiskAssessmentId"];
            obj.Title = (string)row["Title"];
            obj.Description = (string)row["Description"];
            obj.ReferenceDoc = (string)row["ReferenceDoc"];
            obj.Chapter = (string)row["Chapter"];
            obj.Comments = (string)row["Comments"];
            return obj;
        }
        #endregion
        #endregion
    }
}
