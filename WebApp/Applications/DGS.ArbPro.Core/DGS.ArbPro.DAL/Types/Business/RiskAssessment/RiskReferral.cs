﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class RiskReferral : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public RiskReferral(string databaseName)
            : base(databaseName, "RiskReferral", DateTime.Parse("1970-01-01"))
        {

        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public

        public Types.RiskReferral GetForRiskId(int RiskId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskReferral_GetForRiskId", new SqlParameter[] { new SqlParameter("@RiskId", RiskId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForRiskId", string.Format("int RiskId: {0}", RiskId.ToString()), ex);
                Debug.WriteLine("Error in RiskReferral.GetForRiskId: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public int Save(Types.RiskReferral obj)
        {
            if (obj.Id < 1)
                return Insert(obj);
            else
                return Update(obj);
        }
        #endregion
        #region Private

        private int Update(Types.RiskReferral obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskReferral_Update", GetUpdateParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.Risk obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in RiskReferral.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Insert(Types.RiskReferral obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskReferral_Insert", GetInsertParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.Risk obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in RiskReferral.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetUpdateParams(Types.RiskReferral obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter ( "@Id", obj.Id),
                new SqlParameter ( "@RiskId", obj.RiskAssessmentID),
                new SqlParameter ( "@TeamLeaderSpokenWith", obj.TeamLeaderSpokenWith),
                new SqlParameter ( "@ReferralDate", obj.ReferralDate),
                new SqlParameter ( "@ReferralComments", obj.ReferralComments),
                new SqlParameter ( "@AdditionalActions", obj.AdditionalActions)
            };
        }

        private SqlParameter[] GetInsertParams(Types.RiskReferral obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter ( "@RiskId", obj.RiskAssessmentID),
                new SqlParameter ( "@TeamLeaderSpokenWith", obj.TeamLeaderSpokenWith),
                new SqlParameter ( "@ReferralDate", obj.ReferralDate),
                new SqlParameter ( "@ReferralComments", obj.ReferralComments),
                new SqlParameter ( "@AdditionalActions", obj.AdditionalActions)
            };
        }

        private Types.RiskReferral ToObject(DataSet ds)
        {
            Types.RiskReferral retVal = new Types.RiskReferral();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in RiskReferral.ToObject: {0}", ex.ToString());
            }

            return retVal;
        }

        private Types.RiskReferral CreateObject(DataRow row)
        {
            Types.RiskReferral obj = new Types.RiskReferral();
            obj.Id = base.IntValue(row, "Id");
            obj.RiskAssessmentID = base.IntValue(row, "RiskId");
            obj.TeamLeaderSpokenWith = base.BoolValue(row, "TeamLeaderSpokenWith");
            obj.ReferralDate = base.DateTimeValue(row, "ReferralDate");
            obj.ReferralComments = base.StringValue(row, "ReferralComments");
            obj.AdditionalActions = base.StringValue(row, "AdditionalActions");
            return obj;
        }
        #endregion
        #endregion
    }
}
