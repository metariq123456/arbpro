﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class RescuePlan : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public RescuePlan(string databaseName)
            : base(databaseName, "RescuePlan", DateTime.Parse("1970-01-01"))
        {
        }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public Types.RescuePlan GetForId(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("AreialRescuePlan_GetForId", new SqlParameter[] { new SqlParameter("@Id", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int id: {0}",id), ex);
                Console.WriteLine("Error in RescuePlan.GetForId: {0}", ex.ToString());
            }
            return GetFirstResultFromDataSet(ds);
        }

        public List<Types.RescuePlan> GetList()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("AreialRescuePlan_GetAll", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetList", "None", ex);
                Console.WriteLine("Error in RescuePlan.GetList: {0}", ex.ToString());
            }
            return GetResultsFromDataSet(ds);
        }
        #endregion

        #region Private
        private int Update(Types.RescuePlan obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("AreialRescuePlan_Update", GetStoredProcParams(obj, false), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.RescuePlan obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in RescuePlan.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Insert(Types.RescuePlan obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("AreialRescuePlan_Insert", GetStoredProcParams(obj, true), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.RescuePlan obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in RescuePlan.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetStoredProcParams(Types.RescuePlan obj, bool isInsert)
        {
            if (isInsert)
            {
                return new SqlParameter[]
                {
                    new SqlParameter ("@Details",obj.Details)
                };
            }
            else
            {
                return new SqlParameter[]
                {
                    new SqlParameter("@Id",obj.Id),
                    new SqlParameter ("@Details",obj.Details)
                };
            }
        }

        private Types.RescuePlan GetFirstResultFromDataSet(DataSet ds)
        {
            Types.RescuePlan retVal = new Types.RescuePlan();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("GetFirstResultFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in RescuePlan.GetFirstResultFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.RescuePlan> GetResultsFromDataSet(DataSet ds)
        {
            List<Types.RescuePlan> retVal = new List<Types.RescuePlan>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("GetResultsFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in RescuePlan.GetResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        internal static Types.RescuePlan CreateObject(DataRow row)
        {
            Types.RescuePlan obj = new Types.RescuePlan();
            obj.Id = (int)row["Id"];
            obj.Details = (string)row["Detail"];
            return obj;
        }
        #endregion
        #endregion
    }
}
