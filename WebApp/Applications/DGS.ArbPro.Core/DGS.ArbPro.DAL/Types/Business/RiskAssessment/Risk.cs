﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Risk : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Risk(string databaseName)
            : base(databaseName, "Risk", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public Types.Risk CreateForQuoteFromTemplate(int templateId, int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_CreateFromTemplate_ForQuote", new SqlParameter[] { new SqlParameter("@TemplateId", templateId), new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("CreateForQuoteFromTemplate", string.Format("int templateId: {0}, int quoteId: {1}", templateId.ToString(), quoteId.ToString()), ex);
                Debug.WriteLine("Error in Risk.CreateForQuoteFromTemplate: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public Types.Risk CreateForJobSiteFromTemplate(int templateId, int jobSiteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_CreateFromTemplate_ForSite", new SqlParameter[] { new SqlParameter("@TemplateId", templateId), new SqlParameter("@JobSite", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("CreateForJobSiteFromTemplate", string.Format("int templateId: {0}, int jobSiteId: {1}", templateId.ToString(), jobSiteId.ToString()), ex);
                Debug.WriteLine("Error in Risk.CreateForJobSiteFromTemplate: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public Types.Risk CreateForJobSiteAndQuote(int templateId, int jobSiteId, int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_CreateFromTemplate_ForSiteAndQuote", new SqlParameter[] { new SqlParameter("@TemplateId", templateId), new SqlParameter("@JobSite", jobSiteId), new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("CreateForJobSiteAndQuote", string.Format("int templateId: {0}, int jobSiteId: {1}, int quoteId {2}", templateId.ToString(), jobSiteId.ToString(), quoteId.ToString()), ex);
                Debug.WriteLine("Error in Risk.CreateForJobSiteAndQuote: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public Types.Risk CreateForWorkFromTemplate(int templateId, int workId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_CreateFromTemplate_ForWork", new SqlParameter[] { new SqlParameter("@TemplateId", templateId), new SqlParameter("@WorkId", workId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int templateId: {0}, int workId: {1}", templateId.ToString(), workId.ToString()), ex);
                Debug.WriteLine("Error in Risk.GetForId: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public Types.Risk CreateFromDefaultTemplate(int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_CreateFromTemplate_Default", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("CreateFromDefaultTemplate", string.Format("int quoteId: {0}", quoteId.ToString()), ex);
                Debug.WriteLine("Error in Risk.CreateFromDefaultTemplate: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public Types.Risk GetForId(int assessmentId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_GetForId", new SqlParameter[] { new SqlParameter("@Id", assessmentId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int assessmentId: {0}", assessmentId.ToString()), ex);
                Debug.WriteLine("Error in Risk.GetForId: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public Types.Risk GetForAppointmentId(int appointmentId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_GetForAppointment", new SqlParameter[] { new SqlParameter("@AppointmentId", appointmentId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForAppointmentId", string.Format("int appointmentId: {0}", appointmentId.ToString()), ex);
                Debug.WriteLine("Error in Risk.GetForAppointmentId: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public Types.Risk GetForQuoteId(int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_GetForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForAppointmentId", string.Format("int quoteId: {0}", quoteId.ToString()), ex);
                Debug.WriteLine("Error in Risk.GetForQuoteId: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public Types.Risk GetForQuoteAndJobSiteId(int quoteId, int jobSiteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_GetForQuoteAndJobSiteId", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId), new SqlParameter("@JobSiteId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuoteAndJobSiteId", string.Format("int quoteId: {0}, int jobSiteId: {1}", quoteId.ToString(), jobSiteId.ToString()), ex);
                Debug.WriteLine("Error in Risk.GetForQuoteAndJobSiteId: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        /// <summary>
        /// Method to get a Risk Assessment for a job site
        /// </summary>
        /// <param name="jobSiteId">int: The Id of the Job site</param>
        /// <returns>Types.Risk: A populated risk assessment. The Id will be -1 if an error occured or there is no data for the job site id</returns>
        public Types.Risk GetForJobSite(int jobSiteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_GetForJobSite", new SqlParameter[] { new SqlParameter("@JobSiteId", jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForJobSite", string.Format("int jobSiteId: {0}", jobSiteId.ToString()), ex);
                Debug.WriteLine("Error in Risk.GetForJobSite: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public List<Types.Risk> GetAllForSite(int clientSiteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_GetForSite", new SqlParameter[] { new SqlParameter("ClientSiteId", clientSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForSite", string.Format("int clientSiteId: {0}", clientSiteId.ToString()), ex);
                Debug.WriteLine("Error in Risk.GetAllForSite: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.Risk> GetAllForClient(int clientId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_GetForClient", new SqlParameter[] { new SqlParameter("ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllForClient", string.Format("int clientId: {0}", clientId.ToString()), ex);
                Debug.WriteLine("Error in Risk.GetAllForClient: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.Risk> GetAllIncompleted()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_GetAllIncompleted", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAllIncompleted", "None", ex);
                Debug.WriteLine("Error in Risk.GetAllIncompleted: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public int Save(Types.Risk obj)
        {
            if (obj.Id < 1)
                return Insert(obj);
            else
                return Update(obj);
        }

        public int SaveAll(Types.Risk obj)
        {
            if (obj.Id < 1)
                return InsertAll(obj);
            else
                return UpdateAll(obj);
        }

        public int UpdateLastUpdatedBy(int AssessmentId, string UserName)
        {
            int retval = -1;
            try
            {
                //ds = DBHelper.ExecuteStoredProcDataSet("RiskAssessment_GetForClient", new SqlParameter[] { new SqlParameter("ClientId", clientId) }, this.DatabaseName);
                retval = DBHelper.ExecuteStoredProcReturnID("RiskAssessment_UpdateUpdatedBy", new SqlParameter[] { new SqlParameter("AssessmentId", AssessmentId), new SqlParameter("UserName", UserName) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("UpdateLastUpdateBy", "AssessmentId, UserName", ex);
                Debug.WriteLine("Error in Risk.UpdateLastUpdatedBy: {0}", ex.ToString());
            }
            return retval;
        }

        public int DuplicateRiskAssessmentForQuote(int OldRiskId, int NewQuoteId)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskAssessment_Duplicate", new SqlParameter[] { new SqlParameter("@OldRiskId", OldRiskId), new SqlParameter("@NewQuoteId", NewQuoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("DuplicateRiskAssessment", string.Format("OldRiskId: {0}, NewQuoteId: {1}", OldRiskId.ToString(), NewQuoteId.ToString()), ex);
            }

            return retVal;
        }
        #endregion
        #region Private
        private int Update(Types.Risk obj)
        {

            if (obj.AerialPlanId == 0 || obj.AerialPlanId == -1)
            {
                //Add new Plan Name
                if (!string.IsNullOrEmpty(obj.AerialPlan))
                {
                    try
                    {

                        int Id = DBHelper.ExecuteStoredProcReturnID("Mob_NewAerial_Insert", new SqlParameter[] {
                                                                                    Parameterize("@AerialPlan", obj.AerialPlan)
                                                                                }, this.DatabaseName);
                        obj.AerialPlanId = Id;
                    }
                    catch (Exception ex)
                    {
                        LogError("Insert New Plan Name", string.Format("Types.Risk obj: {0}", obj.ToString()), ex);
                        Debug.WriteLine("Error in obj.Insert: {0}", ex.ToString());
                    }
                }
            }



            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskAssessment_Update", GetUpdateParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.Risk obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in Risk.Update: {0}", ex.ToString());
            }

            //Add new Hospital Name
            if (!string.IsNullOrEmpty(obj.NearestHospital))
            {
                try
                {

                    int Id = DBHelper.ExecuteStoredProcReturnID("Mob_NewHospita_Insert", new SqlParameter[] {
                                                                                    Parameterize("@NearestHospital", obj.NearestHospital)
                                                                                }, this.DatabaseName);
                }
                catch (Exception ex)
                {
                    LogError("Insert New Hospital Name", string.Format("Types.Risk obj: {0}", obj.ToString()), ex);
                    Debug.WriteLine("Error in obj.Insert: {0}", ex.ToString());
                }
            }

            return retVal;
        }

        private int Insert(Types.Risk obj)
        {


            if (obj.AerialPlanId == 0 || obj.AerialPlanId == -1)
            {
                //Add new Plan Name
                if (!string.IsNullOrEmpty(obj.AerialPlan))
                {
                    try
                    {

                        int Id = DBHelper.ExecuteStoredProcReturnID("Mob_NewAerial_Insert", new SqlParameter[] {
                                                                                    Parameterize("@AerialPlan", obj.AerialPlan)
                                                                                }, this.DatabaseName);
                        obj.AerialPlanId = Id;
                    }
                    catch (Exception ex)
                    {
                        LogError("Insert New Plan Name", string.Format("Types.Risk obj: {0}", obj.ToString()), ex);
                        Debug.WriteLine("Error in obj.Insert: {0}", ex.ToString());
                    }
                }
            }



            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskAssessment_Insert", GetInsertParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.Risk obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in obj.Insert: {0}", ex.ToString());
            }

            //Add new Hospital Name
            if (!string.IsNullOrEmpty(obj.NearestHospital))
            {
                try
                {

                    int Id = DBHelper.ExecuteStoredProcReturnID("Mob_NewHospita_Insert", new SqlParameter[] {
                                                                                    Parameterize("@NearestHospital", obj.NearestHospital)
                                                                                }, this.DatabaseName);

                }
                catch (Exception ex)
                {
                    LogError("Insert New Hospital Name", string.Format("Types.Risk obj: {0}", obj.ToString()), ex);
                    Debug.WriteLine("Error in obj.Insert: {0}", ex.ToString());
                }
            }
            return retVal;
        }

        private int UpdateAll(Types.Risk obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskAssessment_Update", GetUpdateParams(obj), this.DatabaseName);
                if (retVal > 0)
                {
                    bool updated = UpdateChildCollections(obj);
                    if (!updated)
                        retVal = -1;
                }
            }
            catch (Exception ex)
            {
                LogError("UpdateAll", string.Format("Types.Risk obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in Risk.UpdateAll: {0}", ex.ToString());
            }
            return retVal;
        }

        private int InsertAll(Types.Risk obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskAssessment_Insert", GetInsertParams(obj), this.DatabaseName);
                if (retVal > 0)
                {
                    bool updated = UpdateChildCollections(obj);
                    if (!updated)
                        retVal = -1;
                }
            }
            catch (Exception ex)
            {
                LogError("InsertAll", string.Format("Types.Risk obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in Risk.InsertAll: {0}", ex.ToString());
            }
            return retVal;
        }

        private bool UpdateChildCollections(Types.Risk parent)
        {
            int updated = -1;
            DAL.RiskSection sectionContext = new DAL.RiskSection(this.DatabaseName);
            foreach (Types.RiskSection sec in parent.AssessmentSections)
            {
                sec.RiskAssessmentId = parent.Id;
                updated = sectionContext.SaveAll(sec);
                if (updated < 1)
                    break;
            }
            if (updated < 1)
                return false;

            DAL.RiskAdditionalItem itemContext = new DAL.RiskAdditionalItem(this.DatabaseName);
            foreach (Types.RiskAdditionalItem itm in parent.AdditionalRisks)
            {
                itm.RiskAssessmentId = parent.Id;
                updated = itemContext.Save(itm);
                if (updated < 1)
                    break;
            }
            if (updated < 1)
                return false;

            return true;
        }

        private SqlParameter[] GetInsertParams(Types.Risk obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter ("@QuoteId",obj.QuoteId),
                new SqlParameter ("@ClientId",obj.ClientId),
                new SqlParameter ("@AppointmentId",obj.AppointmentId ),
                new SqlParameter ("@JobSiteId",obj.JobSiteId),
                new SqlParameter ("@AssessingStaffId",obj.AssessingStaffId),
                new SqlParameter ("@AerialPlanId",obj.AerialPlanId),
                new SqlParameter ("@TeamLeaderStaffId",obj.TeamLeaderStaffId),
                new SqlParameter ("@AssignedAerialStaffId",obj.AssignedAerialStaffId),
                new SqlParameter ("@CS38Certifed",obj.CS38Certifed),
                new SqlParameter ("@AssessmentDate",obj.AssessmentDate),
                new SqlParameter ("@WorkDate",obj.WorkDate),
                new SqlParameter ("@CompletionDate",obj.CompletionDate),
                new SqlParameter ("@NearestHospital",obj.NearestHospital),
                new SqlParameter ("@ReferenceDoc",obj.ReferenceDoc),
                new SqlParameter ("@MobileSignal",obj.MobileSignal),
                new SqlParameter ("@SiteAccess",obj.SiteAccess),
                new SqlParameter ("@GridReference",obj.GridReference),
                new SqlParameter ("@AssessorSignatureImage",obj.AssessorSignatureImage),
                new SqlParameter ("@SatisfactoryCompletion",obj.SatisfactoryCompletion),
                new SqlParameter ("@ReferralRequired",obj.ReferralRequired),
                new SqlParameter ("@ReferralCompleted",obj.ReferralCompleted),
                new SqlParameter ("@ReferralComments",obj.ReferralComments),
                new SqlParameter ("@AuditId",obj.AuditId),
                new SqlParameter ("@Completed",obj.Completed),
                new SqlParameter ("@ManagementComments",obj.ManagementComments),
                new SqlParameter ("@LastUpdatedBy",obj.LastUpdatedBy)
            };
        }

        private SqlParameter[] GetUpdateParams(Types.Risk obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",obj.Id),
                new SqlParameter ("@ClientId",obj.ClientId),
                new SqlParameter ("@AppointmentId",obj.AppointmentId ),
                new SqlParameter ("@JobSiteId",obj.JobSiteId),
                new SqlParameter ("@AssessingStaffId",obj.AssessingStaffId),
                new SqlParameter ("@AerialPlanId",obj.AerialPlanId),
                new SqlParameter ("@TeamLeaderStaffId",obj.TeamLeaderStaffId),
                new SqlParameter ("@AssignedAerialStaffId",obj.AssignedAerialStaffId),
                new SqlParameter ("@CS38Certifed",obj.CS38Certifed),
                new SqlParameter ("@AssessmentDate",obj.AssessmentDate),
                new SqlParameter ("@WorkDate",obj.WorkDate),
                new SqlParameter ("@CompletionDate",obj.CompletionDate),
                new SqlParameter ("@NearestHospital",obj.NearestHospital),
                new SqlParameter ("@ReferenceDoc",obj.ReferenceDoc),
                new SqlParameter ("@MobileSignal",obj.MobileSignal),
                new SqlParameter ("@SiteAccess",obj.SiteAccess),
                new SqlParameter ("@GridReference",obj.GridReference),
                new SqlParameter ("@AssessorSignatureImage",obj.AssessorSignatureImage),
                new SqlParameter ("@SatisfactoryCompletion",obj.SatisfactoryCompletion),
                new SqlParameter ("@ReferralRequired",obj.ReferralRequired),
                new SqlParameter ("@ReferralCompleted",obj.ReferralCompleted),
                new SqlParameter ("@ReferralComments",obj.ReferralComments),
                new SqlParameter ("@AuditId",obj.AuditId),
                new SqlParameter ("@Completed",obj.Completed),
                new SqlParameter ("@ManagementComments",obj.ManagementComments),
                new SqlParameter ("@LastUpdatedBy",obj.LastUpdatedBy) //Just added
            };
        }

        private Types.Risk ToObject(DataSet ds)
        {
            Types.Risk retVal = new Types.Risk();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in Risk.ToObject: {0}", ex.ToString());
            }

            DAL.RiskSection dbContext = new DAL.RiskSection(this.DatabaseName);
            retVal.AssessmentSections = dbContext.GetForRiskAssessment(retVal.Id);
            DAL.RiskAdditionalItem itmDbContext = new DAL.RiskAdditionalItem(this.DatabaseName);
            retVal.AdditionalRisks = itmDbContext.GetForRiskAssessment(retVal.Id);
            return retVal;
        }

        private List<Types.Risk> ToObjectList(DataSet ds)
        {
            List<Types.Risk> retVal = new List<Types.Risk>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.Risk obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateObject(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in Risk.ToObjectList: {0}", ex.ToString());
            }

            DAL.RiskSection dbContext = new DAL.RiskSection(this.DatabaseName);
            DAL.RiskAdditionalItem itmDbContext = new DAL.RiskAdditionalItem(this.DatabaseName);
            foreach (Types.Risk assessment in retVal)
            {
                assessment.AssessmentSections = dbContext.GetForRiskAssessment(assessment.Id);
                assessment.AdditionalRisks = itmDbContext.GetForRiskAssessment(assessment.Id);
            }
            return retVal;
        }

        private Types.Risk CreateObject(DataRow row)
        {
            Types.Risk obj = new Types.Risk();
            obj.Id = base.IntValue(row, "Id");
            obj.ClientId = base.IntValue(row, "ClientId");
            obj.AppointmentId = base.IntValue(row, "AppointmentId");
            obj.JobSiteId = base.IntValue(row, "JobSiteId");
            obj.AssessingStaffId = base.IntValue(row, "AssessingStaffId");
            obj.AerialPlanId = base.IntValue(row, "AerialPlanId");
            obj.TeamLeaderStaffId = base.IntValue(row, "TeamLeaderStaffId");
            obj.AssignedAerialStaffId = base.IntValue(row, "AssignedAerialStaffId");
            obj.CS38Certifed = base.BoolValue(row, "CS38Certifed");
            obj.AssessmentDate = base.DateTimeValue(row, "AssessmentDate");
            obj.WorkDate = base.DateTimeValue(row, "WorkDate");
            obj.CompletionDate = base.DateTimeValue(row, "CompletionDate");
            obj.NearestHospital = base.StringValue(row, "NearestHospital");
            obj.ReferenceDoc = base.StringValue(row, "ReferenceDoc");
            obj.MobileSignal = base.StringValue(row, "MobileSignal");
            obj.SiteAccess = base.StringValue(row, "SiteAccess");
            obj.GridReference = base.StringValue(row, "GridReference");
            obj.AssessorSignatureImage = base.ByteValue(row, "AssessorSignatureImage");
            obj.SatisfactoryCompletion = base.BoolValue(row, "SatisfactoryCompletion");
            obj.ReferralRequired = base.BoolValue(row, "ReferralRequired");
            obj.ReferralCompleted = base.BoolValue(row, "ReferralCompleted");
            obj.ReferralComments = base.StringValue(row, "ReferralComments");
            obj.AuditId = base.IntValue(row, "AuditId");
            obj.Completed = base.BoolValue(row, "Completed");
            obj.RescueNomaniee = base.StringValue(row, "RescueNomaniee");
            obj.RescuePlan = base.StringValue(row, "RescuePlan");
            obj.LastUpdatedDate = base.DateTimeValue(row, "LastUpdatedDate");
            obj.LastUpdatedBy = base.StringValue(row, "LastUpdatedBy");
            obj.ManagementComments = base.StringValue(row, "ManagementComments");
            return obj;
        }
        #endregion
        #endregion
    }
}

