﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class RiskCriterion :Contracts.DBContextBase
    {
        #region Members

        #endregion

        #region Constructors
        public RiskCriterion(string database) : base(database, "RiskCriterion", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties

        #endregion

        #region Methods
        #region Public
        public List<Types.RiskCriterion> GetForSectionId(int sectionId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskSectionCriteria_GetForSection", new SqlParameter[] { new SqlParameter("@SectionId", sectionId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForSectionId", string.Format("int sectionId: {0}", sectionId.ToString()), ex);
                Console.WriteLine("Error in RiskCriterion.GetForSectionId: {0}", ex.ToString());
            }
            return GetResultsFromDataSet(ds);
        }

        public List<Types.RiskCriterion> GetForRiskId(int riskId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("RiskSectionCriteria_GetForRisk", new SqlParameter[] { new SqlParameter("@RiskId", riskId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForRiskId", string.Format("int riskId: {0}", riskId.ToString()), ex);
                Console.WriteLine("Error in RiskCriterion.GetForRiskId: {0}", ex.ToString());
            }
            return GetResultsFromDataSet(ds);
        }

        public int Save(Types.RiskCriterion obj)
        {
            if (obj.Id < 1)
                return Insert(obj);
            else
                return Update(obj);
        }

        #endregion
        #region Private
        private int Insert(Types.RiskCriterion obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskSectionCriteria_Insert", GetInsertParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.RiskCriterion obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in RiskCriterion.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Update(Types.RiskCriterion obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("RiskSectionCriterion_Update", GetUpdateParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.RiskCriterion obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in RiskCriterion.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetInsertParams(Types.RiskCriterion obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@RiskSectionId",obj.RiskSectionId),
                new SqlParameter("@Criterion",obj.Criterion),
                new SqlParameter("@ChapterRef",obj.ChapterRef),
                new SqlParameter("@AssessmentPhase",obj.AssessmentPhase),
                new SqlParameter("@ControlRequired",(int)obj.ControlRequired),
                new SqlParameter("@OnDayControlRequired",(int)obj.OnDayControlRequired)
            };
        }

        private SqlParameter[] GetUpdateParams(Types.RiskCriterion obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",obj.Id),
                new SqlParameter("@ControlRequired",(int)obj.ControlRequired),
                new SqlParameter("@OnDayControlRequired",(int)obj.OnDayControlRequired)
            };
        }

        private List<Types.RiskCriterion> GetResultsFromDataSet(DataSet ds)
        {
            List<Types.RiskCriterion> retVal = new List<Types.RiskCriterion>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("GetResultsFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in RiskCriterion.GetResultsFromDataSet: {0}", ex.ToString());

            }
            return retVal;
        }

        private Types.RiskCriterion CreateObject(DataRow row)
        {
            Types.RiskCriterion obj = new Types.RiskCriterion();
            obj.Id = (int)row["Id"];
	        obj.RiskSectionId = (int)row["RiskSectionId"];
	        obj.Criterion = (string)row["Criterion"];
	        obj.ChapterRef = (string)row["ChapterRef"];
	        obj.AssessmentPhase = (int)row["Phase"];
	        obj.ControlRequired = (RiskCriterionStatusEnum)((int)row["ControlRequired"]);
            obj.OnDayControlRequired = (RiskCriterionStatusEnum)((int)row["OnDayControlRequired"]);
            return obj;
        }
        #endregion
        #endregion
    }
}
