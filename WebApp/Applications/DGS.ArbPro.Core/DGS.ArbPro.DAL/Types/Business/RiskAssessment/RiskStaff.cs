﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class RiskStaff : Contracts.DBContextBase
    {
        #region Constructors
        public RiskStaff(string databasname) : base(databasname, "RiskStaff") { }
        #endregion

        #region Methods
        #region Public
        public int Save(Types.RiskStaff obj)
        {
            if (obj.Id > 0)
                return Update(obj, ParameterMode.Update);
            else
                return Update(obj, ParameterMode.Insert);
        }

        public int SetDeleted(int id)
        {
            int retVal = -1;

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Risk_Staff_SetDeleted", Parameterize("@Id",id), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("SetDeleted", string.Format("int id: {0}", id.ToString()), ex);
            }
            return retVal;
        }

        public Types.RiskStaff GetForId(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Risk_Staff_GetForId", Parameterize("@Id", id), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int id: {0}", id.ToString()), ex);
            }
            return ToObject(ds);
        }

        public List<Types.RiskStaff> GetForAssessment(int riskId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Risk_Staff_GetForAssessment", Parameterize("@RiskId", riskId), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForAssessment", string.Format("int riskId: {0}", riskId.ToString()), ex);
            }
            return ToObjectList(ds);
        }
        #endregion
        #region Private
        private int Update(Types.RiskStaff obj, ParameterMode mode)
        {
            int retVal = -1;
            string spName = "Risk_Staff_Update";
            if (mode == ParameterMode.Insert)
                spName = "Risk_Staff_Insert";

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(spName, Parameters(obj, mode), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.RiskStaff obj: {0}, ParameterMode mode: {1}", obj.ToString(), mode.ToString()), ex);
            }
            return retVal;
        }

        private SqlParameter[] Parameters(Types.RiskStaff obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();

            if(mode == ParameterMode.Update)
                retVal.Add(Parameterize("@Id", obj.Id));

            retVal.Add(Parameterize("@AssessmentId", obj.AssessmentId));
            retVal.Add(Parameterize("@StaffId", obj.StaffId));
            retVal.Add(Parameterize("@StaffName", obj.StaffName));
            retVal.Add(Parameterize("@Task", obj.Task));
            retVal.Add(Parameterize("@Comments", obj.Comments));

            return retVal.ToArray();
        }

        private Types.RiskStaff ToObject(DataSet ds)
        {
            Types.RiskStaff retVal = new Types.RiskStaff();

            if (!base.HasRows(ds))
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }

        private List<Types.RiskStaff> ToObjectList(DataSet ds)
        {
            List<Types.RiskStaff> retVal = new List<Types.RiskStaff>();

            if (!base.HasRows(ds))
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }

        private Types.RiskStaff CreateInstance(DataRow row)
        {
            Types.RiskStaff obj = new Types.RiskStaff();
            obj.Id = (int)row["Id"];
            obj.AssessmentId = (int)row["AssessmentId"];
            obj.StaffId = (int)row["StaffId"];
            obj.StaffName = (string)row["Name"];
            obj.Task = (string)row["Task"];
            obj.Comments = (string)row["Comments"];
            return obj;
        }
        #endregion
        #endregion
    }
}
