﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class CompanyContact :Contracts.DBContextBase
    {
        #region Constructors
        public CompanyContact(string databaseName) : base(databaseName, "CompanyContact", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Methods
        #region Public
        public int Save(Types.CompanyContact obj)
        {
            int retVal = -1;
            ParameterMode mode = ParameterMode.Update;
            string spName = "CompanyContact_Update";
            if (obj.Id < 1)
            {
                mode = ParameterMode.Insert;
                spName = "CompanyContact_Insert";
            }

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(spName, Parameters(obj, mode), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Save", obj.ToString(), ex);
            }
            return retVal;
        }

        public List<Types.CompanyContact> GetList()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("CompanyContact_GetList", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetList", "None", ex);
            }
            return ToObjectList(ds);
        }

        public Types.CompanyContact GetForId(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("CompanyContact_GetForId", new SqlParameter[] {new SqlParameter("@Id",id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int id: {0}", id.ToString()), ex);
            }
            return ToObject(ds);
        }
        #endregion
        #region Private
        private SqlParameter[] Parameters(Types.CompanyContact obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();
            if (mode == ParameterMode.Update)
                retVal.Add(new SqlParameter("@Id", obj.Id));

            retVal.Add(new SqlParameter("Title", obj.Title));
            retVal.Add(new SqlParameter("Forename", obj.Forename));
            retVal.Add(new SqlParameter("Surname", obj.Surname));
            retVal.Add(new SqlParameter("Email", obj.Email));
            retVal.Add(new SqlParameter("Mobile", obj.Mobile));
            retVal.Add(new SqlParameter("Telephone", obj.Telephone));
            retVal.Add(new SqlParameter("JobTitle", obj.JobTitle));
            retVal.Add(new SqlParameter("BusinessName", obj.BusinessName));
            retVal.Add(new SqlParameter("Department", obj.Department));
            retVal.Add(new SqlParameter("StreetNumber", obj.StreetNumber));
            retVal.Add(new SqlParameter("Address1", obj.Address1));
            retVal.Add(new SqlParameter("Address2", obj.Address2));
            retVal.Add(new SqlParameter("Town", obj.Town));
            retVal.Add(new SqlParameter("County", obj.County));
            retVal.Add(new SqlParameter("PostCode", obj.PostCode));
            retVal.Add(new SqlParameter("Comments", obj.Comments));

            return retVal.ToArray();
        }


        private Types.CompanyContact ToObject(DataSet ds)
        {
            Types.CompanyContact retVal = new Types.CompanyContact();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }

        private List<Types.CompanyContact> ToObjectList(DataSet ds)
        {
            List<Types.CompanyContact> retVal = new List<Types.CompanyContact>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
            }
            return retVal;
        }


        private Types.CompanyContact CreateInstance(DataRow row)
        {
            Types.CompanyContact obj = new Types.CompanyContact();
            obj.Id = (int)row["Id"];
            obj.Title = (string)row["Title"];
            obj.Forename = (string)row["Forename"];
            obj.Surname = (string)row["Surname"];
            obj.Email = (string)row["Email"];
            obj.Mobile = (string)row["Mobile"];
            obj.Telephone = (string)row["Telephone"];
            obj.JobTitle = (string)row["JobTitle"];
            obj.BusinessName = (string)row["BusinessName"];
            obj.Department = (string)row["Department"];
            obj.StreetNumber = (string)row["StreetNumber"];
            obj.Address1 = (string)row["Address1"];
            obj.Address2 = (string)row["Address2"];
            obj.Town = (string)row["Town"];
            obj.County = (string)row["County"];
            obj.PostCode = (string)row["PostCode"];
            obj.Comments = (string)row["Comments"];
            return obj;
        }
        #endregion
        #endregion

    }
}
