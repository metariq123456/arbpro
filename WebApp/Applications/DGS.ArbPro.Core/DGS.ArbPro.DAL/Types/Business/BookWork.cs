﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.DAL
{
    public class BookWork : Contracts.DBContextBase
    {
        #region Constructors
        public BookWork(string databaseName)
            : base(databaseName, "BookWork", DateTime.Parse("1970-01-01"))
        { }

        #endregion

        public bool SaveWork(DGS.ArbPro.Types.BookingDtl Bookingdetail)
        {
            bool Inserted = false;
            DataSet ds = null;
            try
            {
                int PlanId = DBHelper.ExecuteStoredProcReturnID("Mob_WorkPlan_Insert", new SqlParameter[] {
                    Parameterize("@QuoteId", Bookingdetail.QuoteId),
                    Parameterize("@WorkId", Bookingdetail.WorkId),
                    Parameterize("@TeamId", 1),
                    Parameterize("@StaffRequired", -1),
                    Parameterize("@JobSiteId",Bookingdetail.SiteId)
                }, this.DatabaseName);

                if (PlanId > 0)
                {

                    int StaffWorkPlanId = DBHelper.ExecuteStoredProcReturnID("Mob_WorkPlanStaff_Insert", new SqlParameter[]{
                        Parameterize("@PlanId",PlanId),
                    Parameterize("@UserId",Bookingdetail.UserId),
                    }, this.DatabaseName);


                    //Add WorkPlan Tool
                    int Tool = DBHelper.ExecuteStoredProcReturnID("Mob_WorkPlanTool_Insert", new SqlParameter[]{
                        Parameterize("@PlanId",PlanId),
                    Parameterize("@QuoteId",Bookingdetail.QuoteId),
                    }, this.DatabaseName);




                    Inserted = true;
                    //Normal Booking
                    if (Bookingdetail.StartDateTime.ToShortDateString().Equals(Bookingdetail.EndDateTime.ToShortDateString()))
                    {
                        DataSet dsWork = DBHelper.ExecuteStoredProcDataSet("Calendar_SaveJobWork", new SqlParameter[]{
                        Parameterize("@ID",0),
                        Parameterize("@StartDateTime",Bookingdetail.StartDateTime),
                        Parameterize("@EndDateTime",Bookingdetail.EndDateTime),
                        Parameterize("@SkipDays",0),
                        Parameterize("@JobSiteID",Bookingdetail.SiteId),
                        Parameterize("@TeamId",1),
                        Parameterize("@WorkPlanId",PlanId),
                        Parameterize("@Comments",string.Empty),
                        Parameterize("@RepeatTypeID",-1),
                        Parameterize("@EveryFor",-1),
                        Parameterize("@EveryDay",false),
                        Parameterize("@OnMonthDay",-1),
                        Parameterize("@WeekDayIDs",string.Empty),
                        Parameterize("@IsRecurring",false),
                        Parameterize("@StartTimeDay1",Bookingdetail.StartDateTime),
                        Parameterize("@ConnectiveDayEndTime",Bookingdetail.EndDateTime),
                        Parameterize("@LastDayEndTime",Bookingdetail.EndDateTime)
                    }, this.DatabaseName);

                        if (dsWork != null && dsWork.Tables.Count > 0)
                        {
                            int CalId = Convert.ToInt32(dsWork.Tables[0].Rows[0][0]);
                            var dateId = DBHelper.ExecuteStoredProcReturnID("Mob_AddCalenderWorkDate", new SqlParameter[]{
                        Parameterize("@CalederWorkId",CalId),
                        Parameterize("@StartDateTime",Bookingdetail.StartDateTime.ToShortDateString())
                    }, this.DatabaseName);
                        }


                    } // Recursive
                    else
                    {
                        var WorkCalId = DBHelper.ExecuteStoredProcDataSet("Calendar_SaveJobWork", new SqlParameter[]{
                        Parameterize("@ID",0),
                        Parameterize("@StartDateTime",Bookingdetail.StartDateTime),
                        Parameterize("@EndDateTime",Bookingdetail.EndDateTime),
                        Parameterize("@SkipDays",0),
                        Parameterize("@JobSiteID",Bookingdetail.SiteId),
                        Parameterize("@TeamId",1),
                        Parameterize("@WorkPlanId",PlanId),
                        Parameterize("@Comments",string.Empty),
                        Parameterize("@RepeatTypeID",1),
                        Parameterize("@EveryFor",-1),
                        Parameterize("@EveryDay",true),
                        Parameterize("@OnMonthDay",-1),
                        Parameterize("@WeekDayIDs",string.Empty),
                        Parameterize("@IsRecurring",true),
                        Parameterize("@StartTimeDay1",Bookingdetail.StartDateTime),
                        Parameterize("@ConnectiveDayEndTime",Bookingdetail.EndDateTime),
                        Parameterize("@LastDayEndTime",Bookingdetail.EndDateTime)
                    }, this.DatabaseName);
                    }

                    ////Create the property mappings for the data structure parameter
                    //Dictionary<string, string> propertyMapping = new Dictionary<string, string>();
                    //propertyMapping.Add("Id", "Id");
                    ////Create the parameter list and add the required params
                    //List<SqlParameter> dbParams = new List<SqlParameter>();
                    //dbParams.Add(Parameterize("@WorkPlanId", PlanId));
                    //dbParams.Add(Parameterize<DGS.ArbPro.Types.BookedWork>("@ItemIds", propertyMapping, Bookingdetail.BookedWorkItem));
                    ////Exicute the stored procedure
                    //var id = DBHelper.ExecuteStoredProcReturnID("WorkItem_WorkPlanItemsUpdate", dbParams.ToArray(), this.DatabaseName);

                    try
                    {

                        if (Bookingdetail != null && Bookingdetail.BookedWorkItem != null && Bookingdetail.BookedWorkItem.Count > 0)
                        {
                            foreach (var item in Bookingdetail.BookedWorkItem)
                            {
                                try
                                {

                                    int planitem = DBHelper.ExecuteStoredProcReturnID("Mob_WorkPlanItem_Insert", new SqlParameter[] {
                                                                                    Parameterize("@WorkPlanId", PlanId),
                                                                                    Parameterize("@WorkItemId", item.Id),
                                                                                }, this.DatabaseName);
                                }
                                catch (Exception ex)
                                {

                                    LogError("SaveWork", "Error in BookWork.SaveWorkWorkPlan: for " + item.Id + " " + ex.Message.ToString(), ex);
                                }
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        LogError("SaveWork", "Error in BookWork.SaveWorkWorkPlan:" + ex.Message.ToString(), ex);
                    }




                    return Inserted;
                }
            }
            catch (Exception ex)
            {
                LogError("SaveWork", "Error in BookWork.SaveWork:" + ex.Message.ToString(), ex);
                Console.WriteLine();
                return false;
            }
            return Inserted;
        }

        public bool AcceptQuote(int QuoteId)
        {
            try
            {
                var qId = DBHelper.ExecuteStoredProcReturnID("Quote_UpdateStatus", new SqlParameter[] { Parameterize("@Id", QuoteId), Parameterize("@Status", 3) }, this.DatabaseName);
                return true;
            }
            catch (Exception ex)
            {

                LogError("Accept Quote", "Error in BookWork.AcceptQuote:" + ex.Message.ToString(), ex);
                return false;

            }


        }
    }
}
