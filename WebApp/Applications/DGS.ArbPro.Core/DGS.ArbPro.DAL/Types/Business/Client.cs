﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Client : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Client(string databaseName)
            : base(databaseName, "Client", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        /// <summary>
        /// Method to return a single Client from the database
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>Types.Client</returns>
        public Types.Client GetForClientId(int clientId)
        {
            Types.Client retVal = null;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Client_GetForID", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                retVal = PopulateSingleResultFromDataSet(ds);


                DAL.ContactSearchResult contactDB = new ContactSearchResult(this.DatabaseName);
                retVal.Contacts = contactDB.GetForClientModel(clientId);

                DAL.QuoteSearchResult quoteDB = new QuoteSearchResult(this.DatabaseName);
                retVal.Quotes = quoteDB.GetForClientModel(clientId);

                DAL.InvoiceSearchResult invDB = new InvoiceSearchResult(this.DatabaseName);
                retVal.Invoices = invDB.GetForClientModel(clientId);

            }
            catch (Exception ex)
            {
                LogError("GetForClientId", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in Client.GetForClientId: {0}", ex.ToString());
            }
            return retVal;

        }

        /// <summary>
        /// Method to return a single light weight Client from the database
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>Types.Client</returns>
        public Types.Client GetLightClientForId(int clientId)
        {
            Types.Client retVal = null;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Client_GetForID", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                retVal = PopulateSingleResultFromDataSet(ds);

            }
            catch (Exception ex)
            {
                LogError("GetLightClientForId", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in Client.GetLightClientForId: {0}", ex.ToString());
            }

            return retVal;
        }

        /// <summary>
        /// Gets a list of all non-deleted clients
        /// </summary>
        /// <returns>
        /// The client list
        /// </returns>
        public List<Types.Client> GetLightClientAll()
        {
            List<Types.Client> retVal = null;
            try
            {
                DataSet ds = null;

                ds = DBHelper.ExecuteStoredProcDataSet("Client_GetAll", this.DatabaseName);
                retVal = this.PopulateResultsFromDataSet(ds);

            }
            catch (Exception ex)
            {
                this.LogError("GetLightClientAll", "Error in GetLightClientAll: ", ex);
                Console.WriteLine("Error in Client.GetLightClientAll: {0}", ex.ToString());
            }

            return retVal;
        }

        /// <summary>
        /// Async method to retreave a client from the database
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>Ttpes.Client</returns>
        public async Task<Types.Client> GetForClientIdAsync(int clientId)
        {
            Types.Client retVal = null;
            try
            {
                DAL.ContactSearchResult contactDB = new ContactSearchResult(this.DatabaseName);
                DAL.QuoteSearchResult quoteDB = new QuoteSearchResult(this.DatabaseName);
                DAL.InvoiceSearchResult invDB = new InvoiceSearchResult(this.DatabaseName);

                DataSet ds = null;

                Task<List<Types.ContactSearchResult>> contactsTask = contactDB.GetForClientModelAsync(clientId);
                Task<List<Types.QuoteSearchResult>> quotesTask = quoteDB.GetForClientModelAsync(clientId);
                Task<List<Types.InvoiceSearchResult>> invTask = invDB.GetForClientModelAsync(clientId);

                ds = DBHelper.ExecuteStoredProcDataSet("Client_GetForID", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                retVal = PopulateSingleResultFromDataSet(ds);

                retVal.Contacts = await contactsTask;
                retVal.Quotes = await quotesTask;
                retVal.Invoices = await invTask;

            }
            catch (Exception ex)
            {
                LogError("GetForClientIdAsync", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in Client.GetForClientIdAsync: {0}", ex.ToString());
            }

            return retVal;
        }

        #endregion
        #region Update Methods
        /// <summary>
        /// Method to mark a client as deleted.
        /// Records marked as deleted will not show in normal search results
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>bool completion status</returns>
        public bool Delete(int clientId)
        {
            bool hasCompleted = false;
            try
            {
                DBHelper.ExecuteNonQuery("Client_MarkDeleted", CommandType.StoredProcedure, new SqlParameter[] { new SqlParameter("ClientId", clientId) }, this.DatabaseName);
                //If we reach here we can assume that the operation completed
                hasCompleted = true;
            }
            catch (Exception ex)
            {
                LogError("Delete", string.Format("clientId:{0}", clientId), ex);
                Console.Write("Error in Client.Delete: {0}", ex.ToString());
                hasCompleted = false;
            }
            return hasCompleted;
        }

        /// <summary>
        /// Async method to mark a client as deleted.
        /// Records marked as deleted will not show in normal search results
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>bool completion status</returns>
        public Task<bool> DeleteAsync(int clientId)
        {
            var taskResult = new TaskCompletionSource<bool>();
            try
            {
                DBHelper.ExecuteNonQuery("Client_MarkDeleted", CommandType.StoredProcedure, new SqlParameter[] {new SqlParameter("ClientId",clientId)}, this.DatabaseName);
                //If we reach here we can assume that the operation completed
                taskResult.SetResult(true);
            }
            catch (Exception ex)
            {
                LogError("DeleteAsync", string.Format("clientId:{0}",clientId), ex);
                Console.Write("Error in Client.DeleteAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
                taskResult.SetResult(false);
            }
            return taskResult.Task;
        }

        /// <summary>
        /// Method to remove the deleted flag from a client and return the instance
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>Types.Client</returns>
        public Types.Client RecoverDeleted(int clientId)
        {
            Types.Client retVal = null;
            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("Client_RecoverDeleted", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);

                retVal = PopulateSingleResultFromDataSet(ds);

                DAL.ContactSearchResult contactDB = new ContactSearchResult(this.DatabaseName);
                retVal.Contacts = contactDB.GetForClientModel(clientId);

                DAL.QuoteSearchResult quoteDB = new QuoteSearchResult(this.DatabaseName);
                retVal.Quotes = quoteDB.GetForClientModel(clientId);

                DAL.InvoiceSearchResult invDB = new InvoiceSearchResult(this.DatabaseName);
                retVal.Invoices = invDB.GetForClientModel(clientId);
            }
            catch (Exception ex)
            {
                LogError("RecoverDeleted", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in Client.RecoverDeleted: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Async method to remove the deleted flag from a client and return the instance
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>Types.Client</returns>
        public async Task<Types.Client> RecoverDeletedAsync(int clientId)
        {
            Types.Client retVal = null;
            try
            {
                DAL.ContactSearchResult contactDB = new ContactSearchResult(this.DatabaseName);
                DAL.QuoteSearchResult quoteDB = new QuoteSearchResult(this.DatabaseName);
                DAL.InvoiceSearchResult invDB = new InvoiceSearchResult(this.DatabaseName);

                DataSet ds = null;

                Task<List<Types.ContactSearchResult>> contactsTask = contactDB.GetForClientModelAsync(clientId);
                Task<List<Types.QuoteSearchResult>> quotesTask = quoteDB.GetForClientModelAsync(clientId);
                Task<List<Types.InvoiceSearchResult>> invTask = invDB.GetForClientModelAsync(clientId);

                ds = DBHelper.ExecuteStoredProcDataSet("Client_RecoverDeleted", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
                retVal = PopulateSingleResultFromDataSet(ds);

                retVal.Contacts = await contactsTask;
                retVal.Quotes = await quotesTask;
                retVal.Invoices = await invTask;
            }
            catch (Exception ex)
            {
                LogError("RecoverDeletedAsync", string.Format("clientId: {0}", clientId), ex);
                Console.WriteLine("Error in Client.RecoverDeletedAsync: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to insert or update a client record
        /// </summary>
        /// <param name="obj">Types.Client</param>
        /// <returns>The id of the updated database row, -1 if there is an error</returns>
        public int Save(Types.Client obj)
        {
            //Check if the object is new
            if (obj.Id > 0) //Defaults to -1 for a new object
            {
                return Save("Client_Update",Parameters(obj,ParameterMode.Update));
            }
            else
            {
                return Save("Client_Insert", Parameters(obj, ParameterMode.Insert));
            }
        }

        /// <summary>
        /// Async method to insert or update a client record
        /// </summary>
        /// <param name="obj">Types.Client</param>
        /// <returns>bool completion status</returns>
        public async Task<bool> SaveAsync(Types.Client obj)
        {
            //Check if the object is new
            if (obj.Id > 0) //Defaults to -1 for a new object
            {
                return await UpdateAsync(obj);
            }
            else
            {
                return await InsertAsync(obj);
            }
        }

        #endregion
        #endregion

        #region Private
        private int Save(string procedure, SqlParameter[] parameters)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(procedure, parameters, this.DatabaseName);
            }
            catch (Exception ex)
            {
                base.LogError("Save", string.Format("procedure: {0}, parameters count: {1}", procedure, parameters == null ? "NULL" : parameters.Count().ToString()), ex);
            }
            return retVal;
        }


        /// <summary>
        /// Method to update a record in the database
        /// </summary>
        /// <param name="obj">Types.Client</param>
        /// <returns>bool completion status</returns>
        private bool Update(Types.Client obj)
        {
            bool hasCompleted = false;
            try
            {
                var ds = DBHelper.ExecuteStoredProcDataSet("Client_Update", this.GetInsertAndUpdateParameters(obj, ParameterMode.Update), this.DatabaseName);

                if (ds.Tables == null || ds.Tables.Count == 0)
                    return false;
                if (ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                    return false;


                hasCompleted = true;
            }
            catch (Exception ex)
            {
                LogError("Update", obj.ToString(), ex);
                Console.Write("Error in Client.Update: {0}", ex.ToString());
                hasCompleted = false;
            }
            return hasCompleted;
        }

        /// <summary>
        /// Async method to update a record in the database
        /// </summary>
        /// <param name="obj">Types.Client</param>
        /// <returns>bool completion status</returns>
        private Task<bool> UpdateAsync(Types.Client obj)
        {
            var taskResult = new TaskCompletionSource<bool>();
            try
            {
                DBHelper.ExecuteNonQuery("Client_Update", CommandType.StoredProcedure, this.GetInsertAndUpdateParameters(obj, ParameterMode.Update), this.DatabaseName);
                //If we reach here we can assume that the operation completed
                taskResult.SetResult(true);
            }
            catch (Exception ex)
            {
                LogError("UpdateAsync", obj.ToString(), ex);
                Console.Write("Error in Client.UpdateAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
                taskResult.SetResult(false);
            }
            return taskResult.Task;
        }

        /// <summary>
        /// Method to insert a new record into the database
        /// </summary>
        /// <param name="obj">Types.Client</param>
        /// <returns>bool completion status</returns>
        private bool Insert(Types.Client obj)
        {
            bool hasCompleted = false;
            try
            {
                var ds = DBHelper.ExecuteStoredProcDataSet("Client_Insert",  this.GetInsertAndUpdateParameters(obj, ParameterMode.Insert), this.DatabaseName);

                if (ds.Tables == null || ds.Tables.Count == 0)
                    return false;
                if (ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                    return false;

                obj.Id = (Int32)ds.Tables[0].Rows[0]["Column1"];

                hasCompleted = true;
            }
            catch (Exception ex)
            {
                LogError("Insert", obj.ToString(), ex);
                Console.Write("Error in Client.Insert: {0}", ex.ToString());
                hasCompleted = false;
            }
            return hasCompleted;
        }

        /// <summary>
        /// Async method to insert a new client into the database
        /// </summary>
        /// <param name="obj">Types.Client</param>
        /// <returns>bool to denote if the method completed correctly</returns>
        private Task<bool> InsertAsync(Types.Client obj)
        {
            var taskResult = new TaskCompletionSource<bool>();
            try
            {
                DBHelper.ExecuteNonQuery("Client_Insert", CommandType.StoredProcedure, this.GetInsertAndUpdateParameters(obj, ParameterMode.Insert), this.DatabaseName);
                //If we reach here we can assume that the operation completed
                taskResult.SetResult(true);
            }
            catch (Exception ex)
            {
                LogError("InsertAsync", obj.ToString(), ex);
                Console.Write("Error in Client.InsertAsync: {0}", ex.ToString());
                taskResult.SetException(ex);
                taskResult.SetResult(false);
            }
            return taskResult.Task;
        }

        /// <summary>
        /// Private helper method to get a parameter array from a quote object to be passed to an insert
        /// or update procedure
        /// </summary>
        /// <param name="obj">Types.Client: The object to be inserted or updated</param>
        /// <param name="mode">The mode of operation, insert or update</param>
        /// <returns>An array of parameters to be passed to a stored procedure</returns>
        private SqlParameter[] GetInsertAndUpdateParameters(Types.Client obj, ParameterMode mode)
        {
            var retVal = new List<SqlParameter>();
            //  Add the Id if we are in update mode
            if (mode == ParameterMode.Update)
                retVal.Add(Parameterize("@Id", obj.Id));
            //  Add remaining parameters to the list,
            retVal.Add(Parameterize("@AltAccountNo", obj.AltAccountNo));
            retVal.Add(Parameterize("@Title", obj.Title));
            retVal.Add(Parameterize("@Forename", obj.Forename));
            retVal.Add(Parameterize("@Surname", obj.Surname));
            retVal.Add(Parameterize("@BusinessName", obj.BusinessName));
            retVal.Add(Parameterize("@HouseNo", obj.HouseNameNumber));
            retVal.Add(Parameterize("@Address1", obj.Address1));
            retVal.Add(Parameterize("@Address2", obj.Address2));
            retVal.Add(Parameterize("@Town", obj.Town));
            retVal.Add(Parameterize("@County", obj.County));
            retVal.Add(Parameterize("@PostCode", obj.PostCode));
            retVal.Add(Parameterize("@Sector", obj.Sector));
            retVal.Add(Parameterize("@Telephone", obj.Telephone));
            retVal.Add(Parameterize("@Mobile", obj.Mobile));
            retVal.Add(Parameterize("@Email", obj.Email));
            retVal.Add(Parameterize("@AccountType", (int)obj.AccountType));
            retVal.Add(Parameterize("@DateOpened", obj.DateOpened));
            retVal.Add(Parameterize("@Longtitude", obj.AddressLongitude));
            retVal.Add(Parameterize("@Latitude", obj.AddressLatitude));
            retVal.Add(Parameterize("@TermsAgreed", obj.TermsAgreed));
            retVal.Add(Parameterize("@Website", obj.WebAddress));
            retVal.Add(Parameterize("@SendSmsMessage", obj.SendSmsMessage));
            retVal.Add(Parameterize("@IncludeInMarketing", obj.IncludeInMarketing));

            return retVal.ToArray();
        }

        ///// <summary>
        ///// Helper method to create the required parameters for an insert operation from a Types.Client object
        ///// </summary>
        ///// <param name="obj">Types.Client</param>
        ///// <returns>SQLParameter array</returns>
        //private SqlParameter[] GetInsertParameters(Types.Client obj)
        //{
        //    SqlParameter[] parameters = new SqlParameter[] 
        //    {
        //        new SqlParameter ("Title",obj.Title),
        //        new SqlParameter ("AltAccountNo",obj.AltAccountNo),
        //        new SqlParameter ("Forename",obj.Forename),
        //        new SqlParameter ("Surname",obj.Surname),
        //        new SqlParameter ("BusinessName",obj.BusinessName),
        //        new SqlParameter ("HouseNo",obj.HouseNameNumber),
        //        new SqlParameter ("Address1", obj.Address1),
        //        new SqlParameter ("Address2",obj.Address2),
        //        new SqlParameter ("Town",obj.Town),
        //        new SqlParameter ("County",obj.County),
        //        new SqlParameter ("PostCode",obj.PostCode),
        //        new SqlParameter ("Sector",obj.Sector),
        //        new SqlParameter ("Telephone",obj.Telephone),
        //        new SqlParameter ("Mobile",obj.Mobile),
        //        new SqlParameter ("Email",obj.Email),
        //        new SqlParameter ("AccountType",(int)obj.AccountType),
        //        new SqlParameter ("DateOpened",obj.DateOpened),
        //        new SqlParameter ("Longitude",obj.AddressLongitude),
        //        new SqlParameter ("Latitude",obj.AddressLatitude),
        //        new SqlParameter ("TermsAgreed",obj.TermsAgreed),
        //        new SqlParameter ("WebSite",obj.WebAddress),
        //        //V3.7.0
        //        new SqlParameter ("SendSmsMessage", obj.SendSmsMessage)
        //    };

        //    return parameters;
        //}

        ///// <summary>
        ///// Helper method to create the required update process parameters from a Types.Client object
        ///// </summary>
        ///// <param name="obj">Types.Client</param>
        ///// <returns>SQLParameter array</returns>
        //private SqlParameter[] GetUpdateParameters(Types.Client obj)
        //{
        //    SqlParameter[] parameters = new SqlParameter[] 
        //    {
        //        new SqlParameter("Id",obj.Id),
        //        new SqlParameter ("AltAccountNo",obj.AltAccountNo),
        //        new SqlParameter ("Title",obj.Title),
        //        new SqlParameter ("Forename",obj.Forename),
        //        new SqlParameter ("Surname",obj.Surname),
        //        new SqlParameter ("BusinessName",obj.BusinessName),
        //        new SqlParameter ("HouseNo",obj.HouseNameNumber),
        //        new SqlParameter ("Address1", obj.Address1),
        //        new SqlParameter ("Address2",obj.Address2),
        //        new SqlParameter ("Town",obj.Town),
        //        new SqlParameter ("County",obj.County),
        //        new SqlParameter ("PostCode",obj.PostCode),
        //        new SqlParameter ("Sector",obj.Sector),
        //        new SqlParameter ("Telephone",obj.Telephone),
        //        new SqlParameter ("Mobile",obj.Mobile),
        //        new SqlParameter ("Email",obj.Email),
        //        new SqlParameter ("AccountType",(int)obj.AccountType),
        //        new SqlParameter ("DateOpened",obj.DateOpened),
        //        new SqlParameter ("Longitude",obj.AddressLongitude),
        //        new SqlParameter ("Latitude",obj.AddressLatitude),
        //        new SqlParameter ("TermsAgreed",obj.TermsAgreed),
        //        new SqlParameter ("WebSite",obj.WebAddress)
        //    };

        //    return parameters;
        //}

        private SqlParameter[] Parameters(Types.Client obj, ParameterMode mode)
        {
            var parameters = new List<SqlParameter>();
            
            if (mode == ParameterMode.Update)
            {
                parameters.Add(Parameterize("@Id", obj.Id));
            }

            parameters.Add(Parameterize("@AltAccountNo", obj.AltAccountNo));
            parameters.Add(Parameterize("@Title", obj.Title));
            parameters.Add(Parameterize("@Forename", obj.Forename));
            parameters.Add(Parameterize("@Surname", obj.Surname));
            parameters.Add(Parameterize("@BusinessName", obj.BusinessName));
            parameters.Add(Parameterize("@HouseNo", obj.HouseNameNumber));
            parameters.Add(Parameterize("@Address1", obj.Address1));
            parameters.Add(Parameterize("@Address2", obj.Address2));
            parameters.Add(Parameterize("@Town", obj.Town));
            parameters.Add(Parameterize("@County", obj.County));
            parameters.Add(Parameterize("@PostCode", obj.PostCode));
            parameters.Add(Parameterize("@Sector", obj.Sector));
            parameters.Add(Parameterize("@Telephone", obj.Telephone));
            parameters.Add(Parameterize("@Mobile", obj.Mobile));
            parameters.Add(Parameterize("@Email", obj.Email));
            parameters.Add(Parameterize("@AccountType", (int)obj.AccountType));
            parameters.Add(Parameterize("@DateOpened", obj.DateOpened));
            parameters.Add(Parameterize("@Longitude", obj.AddressLongitude));
            parameters.Add(Parameterize("@Latitude", obj.AddressLatitude));
            parameters.Add(Parameterize("@TermsAgreed", obj.TermsAgreed));
            parameters.Add(Parameterize("@WebSite", obj.WebAddress));
            parameters.Add(Parameterize("@SendSmsMessage", obj.SendSmsMessage));
            parameters.Add(Parameterize("@IncludeInMarketing", obj.IncludeInMarketing));

            return parameters.ToArray();
        }

        /// <summary>
        /// Returns a single instance of a Client from the first row of a table in a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>A Types.Client instance</returns>
        private Types.Client PopulateSingleResultFromDataSet(DataSet ds)
        {
            Types.Client retVal = new Types.Client();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                    retVal = CreateClient(ds.Tables[0].Rows[0]);
                
            }
            catch (Exception ex)
            {
                LogError("PopulateSingleResultFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Client.PopulateSingleResultFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Populate a list of Clients from a dataset
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>List of Types.Client</returns>
        private List<Types.Client> PopulateResultsFromDataSet(DataSet ds)
        {
            List<Types.Client> retVal = new List<Types.Client>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.Client obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateClient(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Client.PopulateGenericResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>Client instance</returns>
        private Types.Client CreateClient(DataRow row)
        {
            Types.Client obj = new Types.Client();
            obj.Id = (int)row["Id"];
            obj.AltAccountNo = (string)row["AltAccountNo"];
            obj.Title = (string)row["Title"];
            obj.Forename = (string)row["Forename"];
            obj.Surname = (string)row["Surname"];
            obj.BusinessName = (string)row["BusinessName"];
            obj.HouseNameNumber = (string)row["HouseNo"];
            obj.Address1 = (string)row["Address1"];
            obj.Address2 = (string)row["Address2"];
            obj.Town = (string)row["Town"];
            obj.County = (string)row["County"];
            obj.PostCode = (string)row["PostCode"];
            obj.Sector = (string)row["Sector"];
            obj.Telephone = (string)row["Telephone"];
            obj.Mobile = (string)row["Mobile"];
            obj.Email = (string)row["Email"];
            obj.WebAddress = (string)row["Website"];
            obj.AccountType = (ClientTypeEnum)((int)row["AccountType"]);
            obj.DateOpened = (DateTime)row["DateOpened"];
            obj.AddressLongitude = (double)row["Longitude"];
            obj.AddressLatitude = (double)row["Latitude"];
            obj.TermsAgreed = (bool)row["TermsAgreed"];
            obj.SendSmsMessage = BoolValue(row, "SendSmsMessage");
            obj.IncludeInMarketing = BoolValue(row, "IncludeInMarketing");
            return obj;
        }
        #endregion
        #endregion
    }
}
