﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;
using System.Diagnostics;

namespace DGS.ArbPro.DAL
{
    public class ClientSite : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public ClientSite(string database) : base(database, "ClientSite", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        /// <summary>
        /// Method to get a ClientSite based on the Id
        /// </summary>
        /// <param name="siteId">int</param>
        /// <returns>Types.ClientSite</returns>
        public Types.ClientSite GetForId(int siteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ClientSite_GetForId", new SqlParameter[] { new SqlParameter("@Id", siteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int siteId: {0}", siteId), ex);
                Console.WriteLine("Error in ClientSite.GetForId: {0}", ex.ToString());
            }
            return PopulateSingleResultFromDataSet(ds);
        }

        /// <summary>
        /// Method to get a ClientSite for a risk assessment based on the assessment Id
        /// </summary>
        /// <param name="siteId">int</param>
        /// <returns>Types.ClientSite</returns>
        public Types.ClientSite GetForRiskAssessmentId(int riskId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ClientSite_GetForRiskAssessmentId", new SqlParameter[] { new SqlParameter("@RiskId", riskId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForRiskAssessmentId", string.Format("int riskId: {0}", riskId), ex);
                Console.WriteLine("Error in ClientSite.GetForRiskAssessmentId: {0}", ex.ToString());
            }
            return PopulateSingleResultFromDataSet(ds);
        }

        /// <summary>
        /// Method to get a list of Client Sites for a Client
        /// </summary>
        /// <param name="clientId">int</param>
        /// <returns>List of Types.ClientSite</returns>
        public List<Types.ClientSite> GetForClient(int clientId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ClientSite_GetForClient", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClient", string.Format("int clientId: {0}", clientId), ex);
                Console.WriteLine("Error in ClientSite.GetForClient: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }

        /// <summary>
        /// Checks if client site exists based on parameters passed and returns its id.
        /// </summary>
        /// <param name="clientId"> The client id. </param>
        /// <param name="streetNo"> The street no. </param>
        /// <param name="address1"> The address 1. </param>
        /// <param name="address2"> The address 2. </param>
        /// <param name="town"> The town. </param>
        /// <param name="county"> The county. </param>
        /// <param name="postCode"> The post code. </param>
        /// <returns> The client site id </returns>
        public int CheckIfExistsReturnId(int clientId, string streetNo, string address1, string address2, string town, string county, string postCode)
        {
            int retVal = -1;

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(
                    "ClientSite_CheckIfExists",
                    new SqlParameter[]
                        {
                            new SqlParameter("@ClientId", clientId), 
                            new SqlParameter("@StreetNumber", streetNo), new SqlParameter("@Address1", address1),
                            new SqlParameter("@Address2", address2), new SqlParameter("@Town", town),
                            new SqlParameter("@County", county), new SqlParameter("@PostCode", postCode)
                        },
                    this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("CheckIfExistsReturnId", string.Format("int clientId: {0}, int streetNo: {1}, string address1: {2}, string postCode: {3}", clientId, streetNo, address1, postCode), ex);
                Console.WriteLine("Error in ClientSite.CheckIfExistsReturnId: {0}", ex.ToString());
            }

            return retVal;
        }
        #endregion
        #region Update Methods
        /// <summary>
        /// Method to insert or update a client site object based on it's Id
        /// New contacts that are associated with this site must be saved first
        /// and the Id of the new record updated in the site object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int Save(Types.ClientSite obj)
        {
            if (obj.Id < 0)
                return Insert(obj);
            else
                return Update(obj);
        }
        #endregion
        #endregion
        #region Private Methods
        /// <summary>
        /// Method to insert a new record into the database
        /// </summary>
        /// <param name="obj">Types.ClientSite</param>
        /// <returns>int - The Id of the inserted record  > 0 if successful</returns>
        private int Insert(Types.ClientSite obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("ClientSite_Insert", GetInsertParamaters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.ClientSite obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in ClientSite.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to update an existing Client Site record
        /// </summary>
        /// <param name="obj">Types.ClientSite</param>
        /// <returns>int - The Id of the updated record > 0 if successful</returns>
        private int Update(Types.ClientSite obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("ClientSite_Update", GetUpdateParamaters(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.ClientSite obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in ClientSite.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create a paramater array for insert paramaters
        /// </summary>
        /// <param name="obj">Types.ClientSite</param>
        /// <returns>SqlParameter[]</returns>
        private SqlParameter[] GetInsertParamaters(Types.ClientSite obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter("ContactId", obj.ContactId),
                new SqlParameter("ClientId", obj.ClientId),
                new SqlParameter ("StreetNumber", obj.HouseNameNo ),
                new SqlParameter ("Address1", obj.Address1 ),
                new SqlParameter ("Address2", obj.Address2 ),
                new SqlParameter ("Town", obj.Town ),
                new SqlParameter ("County", obj.County ),
                new SqlParameter ("PostCode", obj.PostCode ),
                new SqlParameter ("Latitude", obj.Latitude ),
                new SqlParameter ("Longitude", obj.Longitude ),
                new SqlParameter ("Reference", obj.Reference )
            };

            return parameters;
        }

        /// <summary>
        /// Helper method to create a paramater array for update paramaters
        /// </summary>
        /// <param name="obj">Types.ClientSite</param>
        /// <returns>SqlParameter[]</returns>
        private SqlParameter[] GetUpdateParamaters(Types.ClientSite obj)
        {
            SqlParameter[] parameters = new SqlParameter[] 
            {
                new SqlParameter ("Id", obj.Id ),
                new SqlParameter ("ContactId", obj.ContactId),
                new SqlParameter("ClientId", obj.ClientId),
                new SqlParameter ("StreetNumber", obj.HouseNameNo ),
                new SqlParameter ("Address1", obj.Address1 ),
                new SqlParameter ("Address2", obj.Address2 ),
                new SqlParameter ("Town", obj.Town ),
                new SqlParameter ("County", obj.County ),
                new SqlParameter ("PostCode", obj.PostCode ),
                new SqlParameter ("Latitude", obj.Latitude ),
                new SqlParameter ("Longitude", obj.Longitude ),
                new SqlParameter ("Reference", obj.Reference )
            };

            return parameters;
        }

        /// <summary>
        /// Helper method to populate a list of objects from a dataset
        /// Returns an empty list if there is no data or an error
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>List of Types.ClientSite</returns>
        private List<Types.ClientSite> PopulateResultsFromDataSet(DataSet ds)
        {
            List<Types.ClientSite> retVal = new List<Types.ClientSite>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.ClientSite obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateObject(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ClientSite.PopulateResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to populate a single client site from a dataset
        /// If there are multiple rows in the dataset only the first row is used
        /// Returns a new instance of a ClientSite if there is an error or no data
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>Types.ClientSite</returns>
        private Types.ClientSite PopulateSingleResultFromDataSet(DataSet ds)
        {
            Types.ClientSite retVal = new Types.ClientSite();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("PopulateSingleResultFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ClientSite.PopulateSingleResultFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>ClientSite instance</returns>
        internal static Types.ClientSite CreateObject(DataRow row)
        {
            Types.ClientSite obj = new Types.ClientSite();
            
            //Create and populate a ClientSite object
            obj.Id = (int)row["SiteId"];
            obj.ClientId = (int)row["ClientId"];
            obj.ContactId = (int)row["SiteContactId"];
            obj.HouseNameNo = (string)row["SiteStreetNumber"];
            obj.Address1 = (string)row["SiteAddress1"];
            obj.Address2 = (string)row["SiteAddress2"];
            obj.Town = (string)row["SiteTown"];
            obj.County = (string)row["SiteCounty"];
            obj.PostCode = (string)row["SitePostCode"];
            obj.Latitude = (double)row["SiteLatitude"];
            obj.Longitude = (double)row["SiteLongitude"];
            
            if (row.Table.Columns.Contains("SiteReference"))
            {
                obj.Reference = (string)row["SiteReference"];
            }
                        
            return obj;
        }
        #endregion
        #endregion
    }
}