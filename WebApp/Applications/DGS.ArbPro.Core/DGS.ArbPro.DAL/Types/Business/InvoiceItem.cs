﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class InvoiceItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public InvoiceItem(string databaseName)
            : base(databaseName, "InvoiceItem")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public

        #endregion

        #region private

        #endregion
        #endregion
    }
}