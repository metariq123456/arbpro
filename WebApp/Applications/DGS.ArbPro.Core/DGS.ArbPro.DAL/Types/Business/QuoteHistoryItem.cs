﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class QuoteHistoryItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public QuoteHistoryItem(string databaseName)
            : base(databaseName, "QuoteHistoryItem")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #endregion
    }
}
