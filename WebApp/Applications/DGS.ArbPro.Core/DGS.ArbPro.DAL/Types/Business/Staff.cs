﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Staff : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Staff(string databaseName)
            : base(databaseName, "Staff", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public 
        public Types.Staff GetForId(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Staff_GetForId", new SqlParameter[] { new SqlParameter("@Id", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int id: {0}", id.ToString()), ex);
                Console.WriteLine("Error in Staff.GetForId: {0}", ex.ToString());
            }
            return GetFirstFromDataSet(ds);
        }

        /// <summary>
        /// Gets a staff member for mobile user id.
        /// </summary>
        /// <param name="id"> The mobile user id. </param>
        /// <returns> The staff object. </returns>
        public Types.Staff GetForMobileId(int id)
        {
            DataSet ds = null;

            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Staff_GetForMobileId", new SqlParameter[] { new SqlParameter("@MobileId", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                this.LogError("GetForMobileId", string.Format("int id: {0}", id.ToString()), ex);
                Console.WriteLine("Error in Staff.GetForMobileId: {0}", ex.ToString());
            }

            return this.GetFirstFromDataSet(ds);
        }

        public List<Types.Staff> GetForWorkOrder(int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Staff_GetForWorkOrder", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkOrder", string.Format("quoteId: {0}",quoteId), ex);
                Console.WriteLine("Error in Staff.GetForWorkOrder: {0}", ex.ToString());
            }
            return GetResultsDataSet(ds);
        }

        public List<Types.Staff> GetForWorkPlan(int workPlanId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Staff_GetForWorkPlan", new SqlParameter[] { new SqlParameter("@WorkPlanId", workPlanId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForWorkPlan", string.Format("workPlanId: {0}", workPlanId), ex);
                Console.WriteLine("Error in Staff.GetForWorkOrder: {0}", ex.ToString());
            }
            return GetResultsDataSet(ds);
        }
        public List<Types.Staff> GetAll()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Staff_GetAll", new SqlParameter[] {}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetAll", "None", ex);
                Console.WriteLine("Error in Staff.GetAll: {0}", ex.ToString());
            }
            return GetResultsDataSet(ds);
        } 


        public int Save(Types.Staff obj)
        {
            if (obj.Id > 0)
                return Update(obj);
            else
                return Insert(obj);
        }
        #endregion
        #region Private
        private int Insert(Types.Staff obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Staff_Insert", GetInsertParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.Staff obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in Staff.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Update(Types.Staff obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Staff_Update", GetUpdateParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.Staff obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in Staff.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.Staff GetFirstFromDataSet(DataSet ds)
        {
            Types.Staff retVal = new Types.Staff();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("GetFirstFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Staff.GetFirstFromDataSet: {0}", ex.ToString());
            }

            return retVal;
        }

        private List<Types.Staff> GetResultsDataSet(DataSet ds)
        {
            List<Types.Staff> retVal = new List<Types.Staff>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("GetResultsDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Staff.GetResultsDataSet: {0}", ex.ToString());
            }

            return retVal;
        }

        private SqlParameter[] GetInsertParams(Types.Staff obj)
        {
            //NOTE: An associated User account is not inserted into a new staff row
            return new SqlParameter[]
            {
                new SqlParameter("@Name",obj.Name),
                new SqlParameter("@Email",obj.Email),
                new SqlParameter("@Mobile",obj.Mobile),
                new SqlParameter("@Telephone",obj.Telephone),
                new SqlParameter("@Position",obj.Position),
                new SqlParameter("@StreetNumber",obj.StreetNumber),
                new SqlParameter("@Address1",obj.Address1 ),
                new SqlParameter("@Address2",obj.Address2),
                new SqlParameter("@Town",obj.Town),
                new SqlParameter("@County",obj.County),
                new SqlParameter("@PostCode",obj.PostCode),
                new SqlParameter("@StaffRole",obj.StaffRole),
                new SqlParameter("@Signature",obj.Signature)
            };
        }

        private SqlParameter[] GetUpdateParams(Types.Staff obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",obj.Id),
                new SqlParameter("@Name",obj.Name),
                new SqlParameter("@Email",obj.Email),
                new SqlParameter("@Mobile",obj.Mobile),
                new SqlParameter("@Telephone",obj.Telephone),
                new SqlParameter("@Position",obj.Position),
                new SqlParameter("@StreetNumber",obj.StreetNumber),
                new SqlParameter("@Address1",obj.Address1 ),
                new SqlParameter("@Address2",obj.Address2),
                new SqlParameter("@Town",obj.Town),
                new SqlParameter("@County",obj.County),
                new SqlParameter("@PostCode",obj.PostCode),
                new SqlParameter("@StaffRole",obj.StaffRole),
                new SqlParameter("@Signature",obj.Signature),
                new SqlParameter("@MobileEnabled",obj.IsMobileUser),
                new SqlParameter("@MobileUserId", obj.MobileUserId)
            };
        }

        private Types.Staff CreateObject(DataRow row)
        {
            Types.Staff obj = new Types.Staff();
            obj.Id = (int)row["Id"];
            obj.Name = (string)row["Name"];
            obj.Email = (string)row["Email"];
            obj.Mobile = (string)row["Mobile"];
            obj.Telephone = (string)row["Telephone"];
            obj.Position = (StaffRoleEnum)((int)row["Position"]);
            obj.StreetNumber = (string)row["StreetNumber"];
            obj.Address1 = (string)row["Address1"];
            obj.Address2 = (string)row["Address2"];
            obj.Town = (string)row["Town"];
            obj.County = (string)row["County"];
            obj.PostCode = (string)row["PostCode"];
            obj.StaffRole = (string)row["StaffRole"];
            obj.Signature = base.ByteValue(row, "Signature");
            obj.IsMobileUser = base.BoolValue(row, "MobileEnabled");
            obj.MobileUserId = IntValue(row, "MobileUserId");
            return obj;
        }
        #endregion
        #endregion
    }
}
