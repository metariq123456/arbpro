﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    using System.Web.UI.WebControls;

    public class ClientContact : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public ClientContact(string databaseName)
            : base(databaseName, "ClientContact", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public Types.ClientContact GetForId(int contactId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ClientContact_GetForId", new SqlParameter[] { new SqlParameter("@Id", contactId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ClientContact.GetForId: {0}", ex.ToString());
            }
            return PopulateSingleResultFromDataSet(ds);
        }

        public List<Types.ClientContact> GetForClient(int clientId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("ClientContact_GetForClient", new SqlParameter[] { new SqlParameter("@ClientId", clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForClient", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ClientContact.GetForClient: {0}", ex.ToString());
            }
            return PopulateResultsFromDataSet(ds);
        }

        public int Save(Types.ClientContact obj)
        {
            if (obj.Id > 0)
                return Update(obj);
            else
                return Insert(obj);
        }
        #endregion

        #region Private
        /// <summary>
        /// Method to insert a new record into the database
        /// </summary>
        /// <param name="obj">Types.ClientContact</param>
        /// <returns>int - The Id of the inserted record  > 0 if successful</returns>
        private int Insert(Types.ClientContact obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("ClientContact_Insert", this.Parameters(obj, ParameterMode.Insert), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.ClientContact obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in ClientContact.Insert: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Method to update an existing Client Contact record
        /// </summary>
        /// <param name="obj">Types.ClientContact</param>
        /// <returns>int - The Id of the updated record > 0 if successful</returns>
        private int Update(Types.ClientContact obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("ClientContact_Update", this.Parameters(obj, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.ClientContact obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in ClientContact.Update: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Private helper method to get a parameter array from a quote object to be passed to an insert
        /// or update procedure
        /// </summary>
        /// <param name="obj">Types.ClientContact: The object to be inserted or updated</param>
        /// <param name="mode">The mode of operation, insert or update</param>
        /// <returns>An array of parameters to be passed to a stored procedure</returns>
        private SqlParameter[] Parameters(Types.ClientContact obj, ParameterMode mode)
        {
            var retVal = new List<SqlParameter>();
            //  Add the Id if we are in update mode
            if (mode == ParameterMode.Update)
                retVal.Add(Parameterize("@Id", obj.Id));
            //  Add remaining parameters to the list,
            retVal.Add(Parameterize("@ClientId", obj.ClientId));
            retVal.Add(Parameterize("@Title", obj.Title));
            retVal.Add(Parameterize("@Forename", obj.Forename));
            retVal.Add(Parameterize("@Surname", obj.Surname));
            retVal.Add(Parameterize("@Email", obj.Email));
            retVal.Add(Parameterize("@Mobile", obj.Mobile));
            retVal.Add(Parameterize("@Telephone", obj.Telephone));
            retVal.Add(Parameterize("@JobTitle", obj.JobTitle));
            retVal.Add(Parameterize("@BusinessName", obj.BusinessName));
            retVal.Add(Parameterize("@Department", obj.Department));
            retVal.Add(Parameterize("@StreetNumber", obj.StreetNumber));
            retVal.Add(Parameterize("@Address1", obj.Address1));
            retVal.Add(Parameterize("@Address2", obj.Address2));
            retVal.Add(Parameterize("@Town", obj.Town));
            retVal.Add(Parameterize("@County", obj.County));
            retVal.Add(Parameterize("@PostCode", obj.PostCode));
            retVal.Add(Parameterize("@ContactType", (int)obj.ContactType));
            retVal.Add(Parameterize("@SendSmsMessage", obj.SendSmsMessage));

            return retVal.ToArray();
        }

        //private SqlParameter[] GetUpdateParamaters(Types.ClientContact obj)
        //{
        //    SqlParameter[] parameters = new SqlParameter[] 
        //    {
        //        new SqlParameter ("Id",obj.Id ),
        //        new SqlParameter ("ClientId",obj.ClientId ),
        //        new SqlParameter ("Title",obj.Title ),
        //        new SqlParameter ("Forename",obj.Forename ),
        //        new SqlParameter ("Surname",obj.Surname ),
        //        new SqlParameter ("Email",obj.Email ),
        //        new SqlParameter ("Mobile",obj.Mobile ),
        //        new SqlParameter ("Telephone",obj.Telephone ),
        //        new SqlParameter ("JobTitle",obj.JobTitle ),
        //        new SqlParameter ("BusinessName",obj.BusinessName ),
        //        new SqlParameter ("Department",obj.Department ),
        //        new SqlParameter ("StreetNumber",obj.StreetNumber ),
        //        new SqlParameter ("Address1",obj.Address1 ),
        //        new SqlParameter ("Address2",obj.Address2 ),
        //        new SqlParameter ("Town",obj.Town ),
        //        new SqlParameter ("County",obj.County ),
        //        new SqlParameter ("PostCode",obj.PostCode ),
        //        new SqlParameter ("ContactType",(int)obj.ContactType )
        //    };

        //    return parameters;
        //}

        //private SqlParameter[] GetInsertParamaters(Types.ClientContact obj)
        //{
        //    SqlParameter[] parameters = new SqlParameter[] 
        //    {
        //        new SqlParameter ("ClientId",obj.ClientId ),
        //        new SqlParameter ("Title",obj.Title ),
        //        new SqlParameter ("Forename",obj.Forename ),
        //        new SqlParameter ("Surname",obj.Surname ),
        //        new SqlParameter ("Email",obj.Email ),
        //        new SqlParameter ("Mobile",obj.Mobile ),
        //        new SqlParameter ("Telephone",obj.Telephone ),
        //        new SqlParameter ("JobTitle",obj.JobTitle ),
        //        new SqlParameter ("BusinessName",obj.BusinessName ),
        //        new SqlParameter ("Department",obj.Department ),
        //        new SqlParameter ("StreetNumber",obj.StreetNumber ),
        //        new SqlParameter ("Address1",obj.Address1 ),
        //        new SqlParameter ("Address2",obj.Address2 ),
        //        new SqlParameter ("Town",obj.Town ),
        //        new SqlParameter ("County",obj.County ),
        //        new SqlParameter ("PostCode",obj.PostCode ),
        //        new SqlParameter ("ContactType",(int)obj.ContactType )
        //    };

        //    return parameters;
        //}

        /// <summary>
        /// Helper method to populate a list of objects from a dataset
        /// Returns an empty list if there is no data or an error
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>List of Types.ClientContact</returns>
        private List<Types.ClientContact> PopulateResultsFromDataSet(DataSet ds)
        {
            List<Types.ClientContact> retVal = new List<Types.ClientContact>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            Types.ClientContact obj = null;
            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    obj = CreateObject(r);
                    retVal.Add(obj);
                }
            }
            catch (Exception ex)
            {
                LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ClientContact.PopulateResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to populate a single client site from a dataset
        /// If there are multiple rows in the dataset only the first row is used
        /// Returns a new instance of a ClientContact if there is an error or no data
        /// </summary>
        /// <param name="ds">DataSet</param>
        /// <returns>Types.ClientContact</returns>
        private Types.ClientContact PopulateSingleResultFromDataSet(DataSet ds)
        {
            Types.ClientContact retVal = new Types.ClientContact();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("PopulateSingleResultFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in ClientContact.PopulateSingleResultFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.ClientContact CreateObject(DataRow row)
        {
            Types.ClientContact obj = new Types.ClientContact();

            obj.Id = (int)row["Id"];
            obj.ClientId = (int)row["ClientId"];
            obj.Title = (string)row["Title"];
            obj.Forename = (string)row["Forename"];
            obj.Surname = (string)row["Surname"];
            obj.Email = (string)row["Email"];
            obj.Mobile = (string)row["Mobile"];
            obj.Telephone = (string)row["Telephone"];
            obj.JobTitle = (string)row["JobTitle"];
            obj.BusinessName = (string)row["BusinessName"];
            obj.Department = (string)row["Department"];
            obj.StreetNumber = (string)row["StreetNumber"];
            obj.Address1 = (string)row["Address1"];
            obj.Address2 = (string)row["Address2"];
            obj.Town = (string)row["Town"];
            obj.County = (string)row["County"];
            obj.PostCode = (string)row["PostCode"];
            obj.ContactType = (ContactTypeEnum)((int)row["Type"]);
            //V3.7.0
            obj.SendSmsMessage = this.BoolValue(row, "SendSmsMessage");

            return obj;
        }
        #endregion
        #endregion
    }
}
