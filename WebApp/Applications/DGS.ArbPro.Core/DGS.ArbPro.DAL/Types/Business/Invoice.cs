﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class Invoice : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Invoice(string databaseName)
            : base(databaseName, "Invoice")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #endregion
    }
}
