﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class CalendarItem : Contracts.DBContextBase
    {
        #region Constructors
        public CalendarItem(string databaseName)
            : base(databaseName, "CalendarItem", DateTime.Parse("1970-01-01"))
        {
        }
        #endregion Constructors

        #region Public Methods

        public List<Types.CalendarItem> GetQuoteAppointmentsForUser(string userId, DateTime startDate, DateTime endDate)
        {
            List<Types.CalendarItem> returnValue = null;
            try
            {
                var ds = DBHelper.ExecuteStoredProcDataSet("MOB_DownloadCalendarQuoteItemListForUser",
                    new SqlParameter[]
                        {
                            new SqlParameter("@UserId", userId),
                            new SqlParameter("@StartDate", startDate),
                            new SqlParameter("@EndDate", endDate)
                        }, this.DatabaseName);

                returnValue = this.PopulateResultsFromDataSet(ds, CalendarItemType.Quote);
            }
            catch (Exception ex)
            {
                this.LogError("GetForUser", string.Format("userId: {0}", userId), ex);
                Console.WriteLine("Error in CalendarItem.GetForUserId: {0}", ex);
            }
            return returnValue;
        }

        public List<Types.CalendarItem> GetGenericAppointmentsForUser(string userId, DateTime startDate, DateTime endDate)
        {
            List<Types.CalendarItem> returnValue = null;
            try
            {
                var ds = DBHelper.ExecuteStoredProcDataSet("MOB_DownloadCalendarGenericItemListForUser",
                    new SqlParameter[]
                        {
                            new SqlParameter("@UserId", userId),
                            new SqlParameter("@StartDate", startDate),
                            new SqlParameter("@EndDate", endDate)
                        }, this.DatabaseName);

                returnValue = this.PopulateResultsFromDataSet(ds, CalendarItemType.Generic);
            }
            catch (Exception ex)
            {
                this.LogError("GetForUser", string.Format("userId: {0}", userId), ex);
                Console.WriteLine("Error in CalendarItem.GetForUserId: {0}", ex);
            }
            return returnValue;
        }

        public List<Types.CalendarItem> GetWorkAppointments(DateTime startDate, DateTime endDate)
        {
            List<Types.CalendarItem> returnValue = null;
            List<Types.CalendarItem> CalenderItemList = new List<Types.CalendarItem>();
            try
            {
                var ds = DBHelper.ExecuteStoredProcDataSet("MOB_DownloadCalendarWorkList",
                    new SqlParameter[]
                        {
                            new SqlParameter("@StartDate", startDate),
                            new SqlParameter("@EndDate", endDate)
                        }, this.DatabaseName);

                returnValue = this.PopulateResultsFromDataSet(ds, CalendarItemType.Work);
                
                foreach (var item in returnValue)
                {
                    if (item.StartTime >= startDate && item.EndTime <= endDate)
                    {
                        CalenderItemList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                this.LogError("GetForUser", "Exception: ", ex);
                Console.WriteLine("Error in CalendarItem.GetForUserId: {0}", ex);
            }
            return CalenderItemList;
        }

        #endregion Public Methods

        #region Private Methods
        private List<Types.CalendarItem> PopulateResultsFromDataSet(DataSet ds, CalendarItemType itemType)
        {
            List<Types.CalendarItem> retVal = new List<Types.CalendarItem>();
            //Check if there is any data to be returned
            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    Types.CalendarItem obj;
                    var IsRec = BoolValue(r, "IsRecurring");
                    if (IsRec)
                    {
                        int repeat_type = this.IntValue(r,"RepeatTypeID");
                        bool every_day = this.BoolValue(r, "EveryDay");
                        if (repeat_type == 1 && every_day)
                        {
                            obj = this.CreateCalendarItem(r);
                            obj.ItemType = itemType;
                            retVal.Add(obj);
                        }
                        
                    }

                    var Id = IntValue(r, "ID");
                    if (Id > 0 && !IsRec)
                    {
                        DataSet Datesds = null;
                        Datesds = DBHelper.ExecuteStoredProcDataSet("Mob_Calendar_GetWorkListBookDate", new SqlParameter[] {
                                    Parameterize("@Id", Id), }, this.DatabaseName);
                        if (Datesds.Tables[0].Rows.Count > 0 && itemType == CalendarItemType.Work)
                        {
                            int DatesCount = 1;
                            foreach (DataRow subRow in Datesds.Tables[0].Rows)
                            {
                                Types.CalendarItem subobj = new Types.CalendarItem();

                                subobj = this.CreateCalendarItem(r);
                                subobj.StartTime = DateTimeValue(subRow, "BookDate");
                                subobj.EndTime = DateTimeValue(subRow, "BookDate");
                                DateTime StartTimeDay1New = this.DateTimeValue(r, "StartTimeDay1");
                                DateTime ConnectiveDayEndTimeNew = this.DateTimeValue(r, "ConnectiveDayEndTime");
                                DateTime LastDayEndTimeNew = this.DateTimeValue(r, "LastDayEndTime");
                                DateTime StartDate = new DateTime(subobj.StartTime.Year, subobj.StartTime.Month, subobj.StartTime.Day, StartTimeDay1New.Hour, StartTimeDay1New.Minute, StartTimeDay1New.Second);
                                subobj.StartTime = StartDate;
                                if (Datesds.Tables[0].Rows.Count > 1 && DatesCount == Datesds.Tables[0].Rows.Count)
                                {
                                    DateTime EndDate = new DateTime(subobj.EndTime.Year, subobj.EndTime.Month, subobj.EndTime.Day, LastDayEndTimeNew.Hour, LastDayEndTimeNew.Minute, LastDayEndTimeNew.Second);
                                    subobj.EndTime = EndDate;

                                }
                                else
                                {


                                    DateTime EndDate = new DateTime(subobj.EndTime.Year, subobj.EndTime.Month, subobj.EndTime.Day, ConnectiveDayEndTimeNew.Hour, ConnectiveDayEndTimeNew.Minute, ConnectiveDayEndTimeNew.Second);
                                    subobj.EndTime = EndDate;
                                }
                                subobj.ItemType = itemType;
                                retVal.Add(subobj);
                                DatesCount += 1;
                            }
                        }
                        else
                        {
                            obj = this.CreateCalendarItem(r);
                            obj.ItemType = itemType;
                            retVal.Add(obj);
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                this.LogError("PopulateResultsFromDataSet", string.Format("DataSet: {0}", ds), ex);
                Console.WriteLine("Error in CalendarItem.PopulateGenericResultsFromDataSet: {0}", ex);
            }
            
           
            return retVal;
        }

        private Types.CalendarItem CreateCalendarItem(DataRow row)
        {
            Types.CalendarItem obj = new Types.CalendarItem();

            obj.Summary = this.StringValue(row, "Summary");
            obj.Description = this.StringValue(row, "Description");
            obj.StartTime = this.DateTimeValue(row, "StartDateTime");
            obj.EndTime = this.DateTimeValue(row, "EndDateTime");
            obj.AllDay = this.BoolValue(row, "AllDay");

            if (row.Table.Columns.Contains("QuoteId"))
                obj.QuoteId = this.IntValue(row, "QuoteId");

            if (row.Table.Columns.Contains("WorkId"))
                obj.WorkId = this.IntValue(row, "WorkId");

            if (row.Table.Columns.Contains("JobId"))
                obj.JobId = this.IntValue(row, "JobId");

            if (row.Table.Columns.Contains("JobSiteID"))
                obj.JobSiteId = this.IntValue(row, "JobSiteID");

            if (row.Table.Columns.Contains("WorkId"))
                obj.WorkId = this.IntValue(row, "WorkId");

            return obj;
        }
        #endregion Private Methods
    }
}
