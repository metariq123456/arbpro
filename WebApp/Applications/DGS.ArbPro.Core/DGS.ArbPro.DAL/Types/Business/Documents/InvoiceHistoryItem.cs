﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class InvoiceHistoryItem : Contracts.DBContextBase
    {
        #region Constructors
        public InvoiceHistoryItem(string databaseName)
            : base(databaseName, "InvoiceHistoryItem", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public

        public Types.InvoiceHistoryItem GetDocument(InvoiceDocTypeEnum docType, int recId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_Invoice_GetForRecIdAndRptType", new SqlParameter[] { new SqlParameter("@DocType", (int)docType), new SqlParameter("@RecId", (int)recId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetDocument", string.Format("InvoiceDocTypeEnum docType: {0}, int recId: {1}", docType.ToString(), recId.ToString()), ex);
                Console.WriteLine("Error in InvoiceHistoryItem.GetDocument: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public List<Types.InvoiceHistoryItem> GetDocumentsForQuote(int quoteId)
        {
            DataSet ds = null;
            try
            {
                //new SqlParameter("@ItemType", 1),
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_Invoices_GetForQuote", new SqlParameter[] {  new SqlParameter("@QuoteId", (int)quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetDocumentsForInvoice", string.Format("int quoteId: {1}", quoteId.ToString()), ex);
                Console.WriteLine("Error in InvoiceHistoryItem.GetDocumentsForInvoice: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        

        public List<Types.InvoiceHistoryItem> GetDocumentsForInvoice(int recId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_GetForRec", new SqlParameter[] { new SqlParameter("@ItemType", 1), new SqlParameter("@RecId", (int)recId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetDocumentsForInvoice", string.Format("int recId: {1}", recId.ToString()), ex);
                Console.WriteLine("Error in InvoiceHistoryItem.GetDocumentsForInvoice: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.InvoiceHistoryItem> GetInvoiceDocumentsForJob(int jobId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_GetForJob", new SqlParameter[] { new SqlParameter("@ItemType", 1), new SqlParameter("@JobId", (int)jobId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetInvoiceDocumentsForJob", string.Format("int jobId: {0}", jobId.ToString()), ex);
                Console.WriteLine("Error in InvoiceHistoryItem.GetInvoiceDocumentsForJob: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.InvoiceHistoryItem> GetInvoiceDocumentsForJobSite(int jobSiteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_GetForSiteJob", new SqlParameter[] { new SqlParameter("@ItemType", 1), new SqlParameter("@RJobSiteId", (int)jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetInvoiceDocumentsForJobSite", string.Format("int jobSiteId: {1}", jobSiteId.ToString()), ex);
                Console.WriteLine("Error in InvoiceHistoryItem.GetInvoiceDocumentsForJobSite: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public List<Types.InvoiceHistoryItem> GetInvoiceDocumentsForClient(int clientId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_GetForClient", new SqlParameter[] { new SqlParameter("@ItemType", 1), new SqlParameter("@ClientId", (int)clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetQuoteDocumentsForClient", string.Format("int clientId: {1}", clientId.ToString()), ex);
                Console.WriteLine("Error in QuoteHistoryItem.GetQuoteDocumentsForClient: {0}", ex.ToString());
            }
            return ToObjectList(ds);
        }

        public Types.InvoiceHistoryItem CreateDupliacteDocument(int docHistoryId, byte[] document)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_MakeCopyRecord", new SqlParameter[] { new SqlParameter("@Id", docHistoryId), new SqlParameter("@Doc", document) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("CreateDupliacteDocument", string.Format("int docHistoryId: {0}, byte[] document:{1}", docHistoryId.ToString(), document.ToString()), ex);
                Console.WriteLine("Error in InvoiceHistoryItem.CreateDupliacteDocument: {0}", ex.ToString());
            }
            return ToObject(ds);
        }

        public bool Save(Types.InvoiceHistoryItem obj)
        {
            int recId = -1;
            try
            {
                var parameters = new SqlParameter[]
                {
                    new SqlParameter("@Id",obj.Id),
                    new SqlParameter("@DispatchMethod",obj.DispatchMethod),
                    Parameterize("@CreatedDate",obj.CreationDate),
                    Parameterize("@SentDate",obj.SentDate),
                    new SqlParameter("@EmailReference",obj.EmailReference),
                    Parameterize("@Document",obj.Doc.ToArray())
                };

                recId = DBHelper.ExecuteStoredProcReturnID("DocumentHistory_Update", parameters, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Save", string.Format("Types.InvoiceHistoryItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in InvoiceHistoryItem.Save: {0}", ex.ToString());
                recId = -1;
            }
            return recId > 0;
        }
        #endregion
        #region Private
        private Types.InvoiceHistoryItem ToObject(DataSet ds)
        {
            Types.InvoiceHistoryItem retVal = new Types.InvoiceHistoryItem();


            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in InvoiceHistoryItem.ToObject: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.InvoiceHistoryItem> ToObjectList(DataSet ds)
        {
            List<Types.InvoiceHistoryItem> retVal = new List<Types.InvoiceHistoryItem>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in InvoiceHistoryItem.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }


        

        private Types.InvoiceHistoryItem CreateObject(DataRow r)
        {
            Types.InvoiceHistoryItem obj = new Types.InvoiceHistoryItem();
            obj.Id = (int)r["id"];
            obj.RecordId = (int)r["Recid"];
            obj.ReferenceNo = (string)r["RefNo"];
            obj.DocumentType = (InvoiceDocTypeEnum)((int)r["DocType"]);
            obj.DispatchMethod = (DocDispatchMethodEnum)((int)r["DispatchMethod"]);
            obj.CreationDate = base.DateTimeValue(r, "CreationDate");
            obj.SentDate = base.DateTimeValue(r, "SentDate");
            obj.Doc = base.StreamValue(r, "Document");
            obj.EmailReference = (string)r["EmailReference"];
            return obj;
        }
        #endregion
        #endregion
    }
}
