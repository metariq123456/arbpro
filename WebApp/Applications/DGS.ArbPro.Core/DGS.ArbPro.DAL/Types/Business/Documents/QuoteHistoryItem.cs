﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class QuoteHistoryItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public QuoteHistoryItem(string databaseName)
            : base(databaseName, "QuoteHistoryItem", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public

        public Types.QuoteHistoryItem GetDocument(DocContentTypeEnum docType, int recId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_Quote_GetForRecIdAndRptType", new SqlParameter[] { new SqlParameter("@DocType", (int)docType), new SqlParameter("@RecId", (int)recId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetDocument", string.Format("DocContentTypeEnum docType: {0}, int recId: {1}", docType.ToString(), recId.ToString()), ex);
                Console.WriteLine("Error in QuoteHistoryItem.GetDocument: {0}", ex.ToString());
            }
            return GetFirstRowFromDataSet(ds);
        }

        public List<Types.QuoteHistoryItem> GetDocumentsForQuote(int recId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_GetForRec", new SqlParameter[] { Parameterize("@ItemType", 0), new SqlParameter("@RecId", (int)recId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetDocumentsForQuote", string.Format("int recId: {1}", recId.ToString()), ex);
                Console.WriteLine("Error in QuoteHistoryItem.GetDocumentsForQuote: {0}", ex.ToString());
            }
            return GetResultsFromDataSet(ds);
        }

        public List<Types.QuoteHistoryItem> GetQuoteDocumentsForJob(int jobId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_GetForJob", new SqlParameter[] { Parameterize("@ItemType", 0), new SqlParameter("@JobId", (int)jobId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetQuoteDocumentsForJob", string.Format("int jobId: {0}", jobId.ToString()), ex);
                Console.WriteLine("Error in QuoteHistoryItem.GetQuoteDocumentsForJob: {0}", ex.ToString());
            }
            return GetResultsFromDataSet(ds);
        }

        public Types.QuoteHistoryItem GetQuoteDocument(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_GetForId", new SqlParameter[] { Parameterize("@Id", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetQuoteDocument", string.Format("id: {0}", id), ex);
                Console.WriteLine("Error in QuoteHistoryItem.GetQuoteDocument: {0}", ex.ToString());
            }
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count == 1)
            {
                return CreateObject(ds.Tables[0].Rows[0]);    
            }
            else
            {
                return null;
            }
            
        }

        public List<Types.QuoteHistoryItem> GetQuoteDocumentsForJobSite(int jobSiteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_GetForSiteJob", new SqlParameter[] { Parameterize("@ItemType", 0), new SqlParameter("@RJobSiteId", (int)jobSiteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetQuoteDocumentsForJobSite", string.Format("int jobSiteId: {1}", jobSiteId.ToString()), ex);
                Console.WriteLine("Error in QuoteHistoryItem.GetQuoteDocumentsForJobSite: {0}", ex.ToString());
            }
            return GetResultsFromDataSet(ds);
        }

        public List<Types.QuoteHistoryItem> GetQuoteDocumentsForClient(int clientId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_GetForClient", new SqlParameter[] { Parameterize("@ItemType", 0), new SqlParameter("@ClientId", (int)clientId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetQuoteDocumentsForClient", string.Format("int clientId: {1}", clientId.ToString()), ex);
                Console.WriteLine("Error in QuoteHistoryItem.GetQuoteDocumentsForClient: {0}", ex.ToString());
            }
            return GetResultsFromDataSet(ds);
        }

        public Types.QuoteHistoryItem CreateDupliacteDocument(int docHistoryId, byte[] document)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DocumentHistory_MakeCopyRecord", new SqlParameter[] { new SqlParameter("@Id", docHistoryId), new SqlParameter("@Doc", document) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("CreateDupliacteDocument", string.Format("int docHistoryId: {0}, byte[] document:{1}", docHistoryId.ToString(), document.ToString()), ex);
                Console.WriteLine("Error in QuoteHistoryItem.CreateDupliacteDocument: {0}", ex.ToString());
            }
            return GetFirstRowFromDataSet(ds);
        }

        public bool Save(Types.QuoteHistoryItem obj)
        {
            int recId = -1;
            try
            {
                var parameters = new SqlParameter[]
                {
                    new SqlParameter("@Id",obj.Id),
                    new SqlParameter("@DispatchMethod",obj.DispatchMethod),
                    Parameterize("@CreatedDate",obj.CreationDate),
                    Parameterize("@SentDate",obj.SentDate),
                    new SqlParameter("@EmailReference",obj.EmailReference),
                    Parameterize("@Document",obj.Doc.ToArray())
                };

                recId = DBHelper.ExecuteStoredProcReturnID("DocumentHistory_Update", parameters, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Save", string.Format("Types.QuoteHistoryItem obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in QuoteHistoryItem.Save: {0}", ex.ToString());
                recId = -1;
            }
            return recId > 0;
        }
        #endregion
        #region Private
        private Types.QuoteHistoryItem GetFirstRowFromDataSet(DataSet ds)
        {
            Types.QuoteHistoryItem retVal = new Types.QuoteHistoryItem();


            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("GetFirstRowFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteHistoryItem.GetFirstRowFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private List<Types.QuoteHistoryItem> GetResultsFromDataSet(DataSet ds)
        {
            List<Types.QuoteHistoryItem> retVal = new List<Types.QuoteHistoryItem>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("GetResultsFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in QuoteHistoryItem.GetResultsFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.QuoteHistoryItem CreateObject(DataRow r)
        {
            Types.QuoteHistoryItem obj = new Types.QuoteHistoryItem();
            obj.Id = (int)r["id"];
            obj.RecordId = (int)r["Recid"];
            obj.ReferenceNo = (string)r["RefNo"];
            obj.DocumentType = (DocContentTypeEnum)((int)r["DocType"]);
            obj.DispatchMethod = (DocDispatchMethodEnum)((int)r["DispatchMethod"]);
            obj.CreationDate = base.DateTimeValue(r, "CreationDate");
            obj.SentDate = base.DateTimeValue(r, "SentDate");
            obj.Doc = base.StreamValue(r, "Document");
            // obj.EmailReference = (string)r["EmailReference"]; TODO: Not in Database
            return obj;
        }
        #endregion
        #endregion
    }
}
