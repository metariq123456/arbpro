﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class DocumentReference: Contracts.DBContextBase
    {
        #region Constructors
        public DocumentReference(string databaseName)
            : base(databaseName, "DocumentReference", DateTime.Parse("1970-01-01"))
        {
        }
        #endregion

        #region Methods
        #region Public
        #region Get
        public List<Types.DocumentReference> GetValues()
        {
            List<Types.DocumentReference> retVal = new List<Types.DocumentReference>();
            try
            {
                DataSet ds = DBHelper.ExecuteStoredProcDataSet("Document_Get_RefNumberValues", new SqlParameter[] { }, this.DatabaseName);

                if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                    return retVal;

                Types.DocumentReference obj = null;
                //Only one record should be returned with all columns
                DataRow row = ds.Tables[0].Rows[0];
                int Id = (int)row["Id"];
                //Quote
                obj = new Types.DocumentReference();
                obj.Id = Id;
                obj.Prefix = (string)row["QuotePrefix"];
                obj.Reference = (int)row["QuoteRef"];
                obj.Suffix = (string)row["QuoteSuffix"];
                retVal.Add(obj);
                //Invoice
                obj = new Types.DocumentReference();
                obj.Id = Id;
                obj.Prefix = (string)row["InvoicePrefix"];
                obj.Reference = (int)row["InvoiceRef"];
                obj.Suffix = (string)row["InvoiceSuffix"];
                retVal.Add(obj);
                //Credit
                obj = new Types.DocumentReference();
                obj.Id = Id;
                obj.Prefix = (string)row["CreditPrefix"];
                obj.Reference = (int)row["CreditRef"];
                obj.Suffix = (string)row["CreditSuffix"];
                retVal.Add(obj);

            }
            catch (Exception ex)
            {
                LogError("GetValues", "None", ex);
                Console.WriteLine("Error in DocumentReference.GetValues: {0}", ex.ToString());
            }
            return retVal;
        }

        public string GetNextQuoteRefNo()
        {
            string retVal = "";
            try
            {
                retVal = GetNextRefNumber(ReportTypeEnum.Quote);
            }
            catch (Exception ex)
            {
                LogError("GetNextQuoteRefNo", "None", ex);
                Console.WriteLine("Error in DocumentReference.GetNextQuoteRefNo: {0}", ex.ToString());
            }
            return retVal;
        }

        public string GetNextInvoiceRefNo()
        {
            string retVal = "";
            try
            {
                retVal = GetNextRefNumber(ReportTypeEnum.Invoice);
            }
            catch (Exception ex)
            {
                LogError("GetNextInvoiceRefNo", "None", ex);
                Console.WriteLine("Error in DocumentReference.GetNextInvoiceRefNo: {0}", ex.ToString());
            }
            return retVal;
        }

        public string GetNextCreditRefNo()
        {
            string retVal = "";
            try
            {
                retVal = GetNextRefNumber(ReportTypeEnum.CreditNote);
            }
            catch (Exception ex)
            {
                LogError("GetNextCreditRefNo", "None", ex);
                Console.WriteLine("Error in DocumentReference.GetNextCreditRefNo: {0}", ex.ToString());
            }
            return retVal;
        }
        #endregion
        #region Set
        public bool SetNextQuoteNumber(string priefix, int refNum, string suffix)
        {
            bool retVal = false;
            try
            {
                var parameters = UpdateParamaters(priefix, refNum, suffix);

                retVal = SetReferenceDetails("DocumentReference_Update_QuoteRef", parameters);
            }
            catch (Exception ex)
            {
                LogError("SetNextQuoteNumber", string.Format("priefix: {0}, refNum: {1}, suffix: {2}", priefix, refNum, suffix), ex);
                Console.WriteLine("Error in DocumentReference.SetNextQuoteNumber: {0}", ex.ToString());
                retVal = false;
            }
            return retVal;
        }

        public bool SetNextQuoteNumber(Types.DocumentReference obj)
        {
            bool retVal = false;
            try
            {
                var parameters = UpdateParamaters(obj.Prefix, obj.Reference , obj.Suffix);

                retVal = SetReferenceDetails("DocumentReference_Update_QuoteRef", parameters);
            }
            catch (Exception ex)
            {
                LogError("SetNextQuoteNumber", string.Format("priefix: {0}, refNum: {1}, suffix: {2}", obj.Prefix, obj.Reference, obj.Suffix), ex);
                Console.WriteLine("Error in DocumentReference.SetNextQuoteNumber: {0}", ex.ToString());
                retVal = false;
            }
            return retVal;
        }

        public bool SetNextInvoiceNumber(string priefix, int refNum, string suffix)
        {
            bool retVal = false;
            try
            {
                var parameters = UpdateParamaters(priefix, refNum, suffix);

                retVal = SetReferenceDetails("DocumentReference_Update_InvoiceRef", parameters);
            }
            catch (Exception ex)
            {
                LogError("SetNextInvoiceNumber", string.Format("priefix: {0}, refNum: {1}, suffix: {2}", priefix, refNum, suffix), ex);
                Console.WriteLine("Error in DocumentReference.SetNextInvoiceNumber: {0}", ex.ToString());
                retVal = false;
            }
            return retVal;
        }

        public bool SetNextInvoiceNumber(Types.DocumentReference obj)
        {
            bool retVal = false;
            try
            {
                var parameters = UpdateParamaters(obj.Prefix, obj.Reference, obj.Suffix);

                retVal = SetReferenceDetails("DocumentReference_Update_InvoiceRef", parameters);
            }
            catch (Exception ex)
            {
                LogError("SetNextInvoiceNumber", string.Format("priefix: {0}, refNum: {1}, suffix: {2}", obj.Prefix, obj.Reference, obj.Suffix), ex);
                Console.WriteLine("Error in DocumentReference.SetNextInvoiceNumber: {0}", ex.ToString());
                retVal = false;
            }
            return retVal;
        }

        public bool SetNextCreditNoteNumber(string priefix, int refNum, string suffix)
        {
            bool retVal = false;
            try
            {
                var parameters = UpdateParamaters(priefix, refNum, suffix);

                retVal = SetReferenceDetails("DocumentReference_Update_CreditRef", parameters);
            }
            catch (Exception ex)
            {
                LogError("SetNextCreditNoteNumber", string.Format("priefix: {0}, refNum: {1}, suffix: {2}", priefix, refNum, suffix), ex);
                Console.WriteLine("Error in DocumentReference.SetNextCreditNoteNumber: {0}", ex.ToString());
                retVal = false;
            }
            return retVal;
        }

        public bool SetNextCreditNoteNumber(Types.DocumentReference obj)
        {
            bool retVal = false;
            try
            {
                var parameters = UpdateParamaters(obj.Prefix, obj.Reference, obj.Suffix);

                retVal = SetReferenceDetails("DocumentReference_Update_CreditRef", parameters);
            }
            catch (Exception ex)
            {
                LogError("SetNextCreditNoteNumber", string.Format("priefix: {0}, refNum: {1}, suffix: {2}", obj.Prefix, obj.Reference, obj.Suffix), ex);
                Console.WriteLine("Error in DocumentReference.SetNextCreditNoteNumber: {0}", ex.ToString());
                retVal = false;
            }
            return retVal;
        }
        #endregion
        #endregion
        #region Private
        private string GetNextRefNumber(ReportTypeEnum refType)
        {
            DataSet ds = DBHelper.ExecuteStoredProcDataSet("Document_Get_NextRefNumber", new SqlParameter[] { new SqlParameter("@RefType", (int)refType) }, this.DatabaseName);

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return "Not Set";

            Types.DocumentReference obj = CreateObject(ds.Tables[0].Rows[0]);

            return obj.ToString();
        }

        private SqlParameter[] UpdateParamaters(string priefix, int refNum, string suffix)
        {
            return new SqlParameter[]
                {
                    new SqlParameter("@Prefix",priefix),
                    new SqlParameter ("@RefNo",refNum),
                    new SqlParameter ("@Suffix",suffix)
                };
        }

        private bool SetReferenceDetails(string spName, SqlParameter[] parameters)
        {
            int id = DBHelper.ExecuteStoredProcReturnID(spName, parameters, base.DatabaseName);
            if (id > 0)
                return true;
            else
                return false;
        }

        private Types.DocumentReference CreateObject(DataRow r)
        {
            Types.DocumentReference obj = new Types.DocumentReference();
            obj.Prefix = base.StringValue(r, "Prefix");
            obj.Reference = base.IntValue(r, "Reference");
            obj.Suffix = base.StringValue(r, "Suffix");
            return obj;
        }
        #endregion
        #endregion
    }
}
