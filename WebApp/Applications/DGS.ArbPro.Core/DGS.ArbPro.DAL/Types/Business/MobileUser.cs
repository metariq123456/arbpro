﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using log4net;
using System.Threading.Tasks;
using DGS.ArbPro.Types;
using DGS.ArbPro.Utility;
using System.Web.Configuration;

namespace DGS.ArbPro.DAL
{
    using System.Data.SqlClient;

    public class MobileUser : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public MobileUser(string databaseName)
            : base(databaseName, "MobileUser")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public Methods

        public int Save(Types.MobileUser obj)
        {
            if (obj.Id == -1)
            {
                return Insert(obj);
            }
            else
            {
                return Update(obj);
            }
        }

        int Insert(Types.MobileUser Object)
        {
            int retVal = -1;

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Mobile_User_Insert", Parameters(Object, ParameterMode.Insert).ToArray(), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("userId: {0}", Object.Id), ex);
                Console.WriteLine("Error in User.Save: {0}", ex.ToString());
                retVal = -1;
            }

            return retVal;
        }

        int Update(Types.MobileUser Object)
        {
            int retVal = -1;

            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("Mobile_User_Update", Parameters(Object, ParameterMode.Update).ToArray(), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("userId: {0}", Object.Id), ex);
                Console.WriteLine("Error in User.Save: {0}", ex.ToString());
                retVal = -1;
            }

            return retVal;
        }

        private List<SqlParameter> Parameters(Types.MobileUser obj, ParameterMode mode)
        {
            List<SqlParameter> ReturnValue;
            ReturnValue = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                ReturnValue.Add(Parameterize("@Id", obj.Id));
                ReturnValue.Add(Parameterize("@Guid", obj.UserGuid.ToString()));
                ReturnValue.Add(Parameterize("@GuidExpirationDate", obj.GuidExpirationDate));
            }

            if (mode == ParameterMode.Insert)
            {
                ReturnValue.Add(Parameterize("@Guid", Guid.Empty.ToString()));
                ReturnValue.Add(Parameterize("@GuidExpirationDate", DateTime.Now));
            }

            ReturnValue.Add(Parameterize("@CompanyID", obj.CompanyID));
            ReturnValue.Add(Parameterize("@EmailAddress", obj.EmailAddress));
            ReturnValue.Add(Parameterize("@Salt", obj.Salt));
            ReturnValue.Add(Parameterize("@Password", obj.PasswordHashed));
            ReturnValue.Add(Parameterize("@AuthenticationFailures", obj.AuthenticationFailures));
            //ReturnValue.Add(Parameterize("@SessionId", obj.SessionId));

            return ReturnValue;
        }

        public Types.MobileUser GetForEmail(String Email)
        {
            Types.MobileUser ReturnValue = null;

            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("MOB_User_Email", new SqlParameter[] { new SqlParameter("@Email", Email) }, this.DatabaseName);

                var Results = ToObjectList(ds);

                if (Results.Count > 0)
                    ReturnValue = Results[0];
            }
            catch (Exception ex)
            {
                LogError("GetForEmail", string.Format("email: {0}", Email), ex);
                Console.WriteLine("Error in User.GetForEmail: {0}", ex.ToString());
            }

            return ReturnValue;
        }

        public Types.MobileUser GetForGuid(string guid)
        {
            Types.MobileUser ReturnValue = null;

            try
            {
                DataSet ds = null;
                ds = DBHelper.ExecuteStoredProcDataSet("MOB_User_Guid", new SqlParameter[] { new SqlParameter("@Guid", guid) }, this.DatabaseName);

                var Results = ToObjectList(ds);

                if (Results.Count > 0)
                    ReturnValue = Results[0];
            }
            catch (Exception ex)
            {
                LogError("MobileUser.cs", "GetForGuid", ex);
                Console.WriteLine("Error in User.GetForGuid: {0}", ex);
            }

            return ReturnValue;
        }

        private List<Types.MobileUser> ToObjectList(DataSet ds)
        {
            List<Types.MobileUser> ReturnValue = new List<Types.MobileUser>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
                return ReturnValue;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    ReturnValue.Add(CreateResult(r));
                }
            }
            catch (Exception ex)
            {
                LogError("PPopulateResultsFromDataSet", string.Format("DataSet: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in User.PopulateResultsFromDataSet: {0}", ex.ToString());
            }

            return ReturnValue;
        }

        private Types.MobileUser CreateResult(DataRow row)
        {
            Types.MobileUser obj = new Types.MobileUser();
            obj.Id = IntValue(row, "Id");
            obj.CompanyID = IntValue(row, "CompanyID");
            obj.EmailAddress = StringValue(row, "EmailAddress");
            obj.Salt = StringValue(row, "Salt");
            obj.PasswordHashed = StringValue(row, "PasswordHashed");
            obj.AuthenticationFailures = IntValue(row, "AuthenticationFailures");
            obj.ClientDatabaseName = StringValue(row, "ClientDatabaseName");
            string tempGuid = StringValue(row, "UserGuid");
            obj.UserGuid = string.IsNullOrWhiteSpace(tempGuid) ? Guid.Empty : Guid.Parse(tempGuid);
            obj.GuidExpirationDate = DateTimeValue(row, "GuidExpirationDate");
            //obj.StaffID = IntValue(row, "StaffId");
            return obj;
        }

        #endregion
        #endregion
    }
}
