﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Security;
using System.IO;

namespace DGS.ArbPro.DAL
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Web.Compilation;

    /// <summary>
    /// Public class for a client's SMTP settings
    /// </summary>
    public class SmtpSettings : Contracts.DBContextBase
    {
        #region Members
        /// <summary>
        /// The salt for the encryption of the string
        /// </summary>
        static byte[] entropy = System.Text.Encoding.Unicode.GetBytes("This is the Salt for the Password for ArbPro SMTP User Settings");
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="databaseName">Client organization's database</param>
        public SmtpSettings(string databaseName)
            : base(databaseName, "SmtpSettings", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public

        /// <summary>
        /// Gets the Email SMTP settings
        /// </summary>
        /// <returns>A list of all required SMTP settings</returns>
        public Types.SmtpSettings GetEmailSettings()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("EmailSettings_Get", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Get", "None", ex);
                Console.WriteLine("Error in obj.Get: {0}", ex.ToString());
            }
            return PopulateEmailFromDataSet(ds);
        }


        /// <summary>
        /// Gets the User SMTP settings
        /// </summary>
        /// <returns>A list of all required SMTP settings</returns>
        public Types.SmtpSettings GetUserSettings(int userId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("UserSettings_Get",new SqlParameter("@UserId", userId), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Get", "None", ex);
                Console.WriteLine("Error in obj.Get: {0}", ex.ToString());
            }
            return PopulateUserFromDataSet(ds);
        }

        /// <summary>
        /// Inserts or updates SMTP settings for a client
        /// </summary>
        /// <param name="obj">Object containing the settings</param>
        /// <returns>The status of the inserted or updated procedures</returns>
        public bool SaveEmail(Types.SmtpSettings obj)
        {
            if (obj.Id < 1)
            {
                return this.InsertEmail(obj);
            }
            else
            {
                return this.UpdateEmail(obj);
            }
        }

        /// <summary>
        /// Updates a user's settings
        /// </summary>
        /// <param name="obj">The user object</param>
        /// <returns>Whether the update succeeded or not</returns>
        public bool SaveUser(Types.SmtpSettings obj)
        {

            return this.UpdateUser(obj);
        }

        #endregion
        #region Private

        /// <summary>
        /// Updates the currently existing settings
        /// </summary>
        /// <param name="obj">Object containing the settings</param>
        /// <returns>Status of completion</returns>
        private bool UpdateEmail(Types.SmtpSettings obj)
        {
            bool hasCompleted = false;
            try
            {
                DBHelper.ExecuteNonQuery("EmailSettings_Update", CommandType.StoredProcedure, EmailParameters(obj, ParameterMode.Update), this.DatabaseName);
                //If we reach here we can assume that the operation completed
                hasCompleted = true;
            }
            catch (Exception ex)
            {
                LogError("Update", obj.ToString(), ex);
                Console.Write("Error in EmailSettings.Update: {0}", ex.ToString());
                hasCompleted = false;
            }
            return hasCompleted;
        }

        /// <summary>
        /// Inserts new SMTP settings for the client
        /// </summary>
        /// <param name="obj">Object containing the settings</param>
        /// <returns>Status of completion</returns>
        private bool InsertEmail(Types.SmtpSettings obj)
        {
            bool hasCompleted = false;
            try
            {
                DBHelper.ExecuteNonQuery("EmailSettings_Insert", CommandType.StoredProcedure, EmailParameters(obj, ParameterMode.Insert), this.DatabaseName);
                //If we reach here we can assume that the operation completed
                hasCompleted = true;
            }
            catch (Exception ex)
            {
                LogError("Insert", obj.ToString(), ex);
                Console.Write("Error in EmailSettings.Insert: {0}", ex.ToString());
                hasCompleted = false;
            }
            return hasCompleted;
        }

        /// <summary>
        /// Updates the currently existing settings
        /// </summary>
        /// <param name="obj">Object containing the settings</param>
        /// <returns>Status of completion</returns>
        private bool UpdateUser(Types.SmtpSettings obj)
        {
            bool hasCompleted = false;
            try
            {
                DBHelper.ExecuteNonQuery("UserSettings_Update", CommandType.StoredProcedure, UserParameters(obj), this.DatabaseName);
                //If we reach here we can assume that the operation completed
                hasCompleted = true;
            }
            catch (Exception ex)
            {
                LogError("Update", obj.ToString(), ex);
                Console.Write("Error in UserSettings.Update: {0}", ex.ToString());
                hasCompleted = false;
            }
            return hasCompleted;
        }

        /// <summary>
        /// Converts the parameters from the SMTP object
        /// </summary>
        /// <param name="obj">Object containing the settings</param>
        /// <param name="mode">If it is update include the Id as well</param>
        /// <returns>List of parameters to be passed onto stored procedure</returns>
        private List<SqlParameter> EmailParameters(Types.SmtpSettings obj, ParameterMode mode)
        {
            List<SqlParameter> returnVal = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                returnVal.Add(Parameterize("@Id", obj.Id));
            }
            returnVal.Add(Parameterize("@Host", obj.HostAddress));
            returnVal.Add(Parameterize("@Port", obj.Port));
            returnVal.Add(Parameterize("@EnableSSL", obj.EnableSSL));
            returnVal.Add(Parameterize("@UseSmtp", obj.UseSmtp));

            return returnVal;
        }

        /// <summary>
        /// Converts the parameters from the SMTP object
        /// </summary>
        /// <param name="obj">Object containing the settings</param>
        /// <param name="mode">If it is update include the Id as well</param>
        /// <returns>List of parameters to be passed onto stored procedure</returns>
        private List<SqlParameter> UserParameters(Types.SmtpSettings obj)
        {
            List<SqlParameter> returnVal = new List<SqlParameter>();


            returnVal.Add(Parameterize("@Id", obj.Id));
            returnVal.Add(Parameterize("@UserId", obj.UserId));

            string pass = EncryptString(obj.Password);
            //ConfigurationManager.AppSettings["Password"] = EncryptString(ToSecureString(obj.Password));
            returnVal.Add(Parameterize("@Username", obj.Username));
            returnVal.Add(Parameterize("@Password", pass));

            return returnVal;
        }


        /// <summary>
        /// Converts the dataset parameters from the stored procedure into objects
        /// </summary>
        /// <param name="ds">The returned dataset from stored procedure</param>
        /// <returns>Converted dataset parameters</returns>
        private Types.SmtpSettings PopulateEmailFromDataSet(DataSet ds)
        {
            Types.SmtpSettings retVal = new Types.SmtpSettings();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateEmailObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("PopulateFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in obj.PopulateFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Converts the dataset parameters from the stored procedure into objects
        /// </summary>
        /// <param name="ds">The returned dataset from stored procedure</param>
        /// <returns>Converted dataset parameters</returns>
        private Types.SmtpSettings PopulateUserFromDataSet(DataSet ds)
        {
            Types.SmtpSettings retVal = new Types.SmtpSettings();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateUserObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("PopulateFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in obj.PopulateFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Adds the passed parameters into an object
        /// </summary>
        /// <param name="row">Data row of values</param>
        /// <returns>Converted object</returns>
        private Types.SmtpSettings CreateEmailObject(DataRow row)
        {
            Types.SmtpSettings obj = new Types.SmtpSettings();
            obj.Id = (int)row["Id"];
            obj.HostAddress = (string)row["HostAddress"];
            obj.Port = (string)row["Port"];
            obj.EnableSSL = (bool)row["EnableSSL"];
            obj.UseSmtp = (bool)row["UseSmtp"];
            return obj;
        }
    
        /// <summary>
        /// Adds the passed parameters into an object
        /// </summary>
        /// <param name="row">Data row of values</param>
        /// <returns>Converted object</returns>
        private Types.SmtpSettings CreateUserObject(DataRow row)
        {
            Types.SmtpSettings obj = new Types.SmtpSettings();
            obj.Id = (int)row["Id"];
            obj.UserId = (int)row["UserId"];
            obj.Username = (string)row["Username"];
            obj.Password = DecryptString((string)row["Password"]);
            return obj;
        }

        /// <summary>
        /// Method to encrypt a password for the SMTP settings of the user
        /// </summary>
        /// <param name="input">Password</param>
        /// <returns> Encrypted Base64 String</returns>
        private static string EncryptString(string input)
        {
            byte[] encryptedData = System.Security.Cryptography.ProtectedData.Protect(
                System.Text.Encoding.Unicode.GetBytes(input),
                entropy,
                System.Security.Cryptography.DataProtectionScope.CurrentUser);
            return Convert.ToBase64String(encryptedData);
        }

        /// <summary>
        /// Method to decrypt a password for the SMTP settings of the user
        /// </summary>
        /// <param name="encryptedData">Encrypted password passed from the db</param>
        /// <returns>Plain password</returns>
        private static string DecryptString(string encryptedData)
        {
            if (encryptedData.Equals(""))
            {
                return "";
            }
            try
            {
                byte[] decryptedData = System.Security.Cryptography.ProtectedData.Unprotect(
                    Convert.FromBase64String(encryptedData),
                    entropy,
                    System.Security.Cryptography.DataProtectionScope.CurrentUser);
                return System.Text.Encoding.Unicode.GetString(decryptedData);
            }
            catch
            {
                return "";
            }
        }

        #endregion
        #endregion
    }
}
