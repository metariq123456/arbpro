﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class Quote : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Quote(string databaseName)
            : base(databaseName, "Quote")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #endregion
    }
}
