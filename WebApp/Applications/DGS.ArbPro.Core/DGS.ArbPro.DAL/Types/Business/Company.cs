﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Company : Contracts.DBContextBase
    {
        #region Members
        /*
        [CoCode]
      ,[CompanyName]
      ,[CompanyAddress]
      ,[CompanyPostCode]
      ,[CompanyVATNo]
      ,[CompanyRegistrationNo]
      ,[CompanyWebSiteAddress]
      ,[CompanyTelephoneNumber]
      ,[CompanyDirectorName]
      ,[CompanyDirectorEmailAddress]
      ,[CompanyDirectorQualifications]
      ,[CompanyHeader]
      ,[CompanyFooter]
      ,[CompanyDirectorSignature]
      ,[SMTPServer]
      ,[SMTPUser]
      ,[SMTPPass]
      ,[SMTPAccountName]
      ,[EmailHeader]
      ,[EmailFooter]
      ,[SalesNominalCode]
      ,[SalesVATCode]
      ,[DefaultVATRate]
      ,[LatinCommon]
      ,[AccountsEmailAddress]
      ,[ReceiptNominalCode]
      ,[CompanyDirectorSignatureImage]
        */
        #endregion

        #region Constructors
        public Company(string databaseName)
            : base(databaseName, "Company", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        public Types.Company Get(bool includePictures = true)
        {
            DataSet ds = null;
            if (includePictures)
            {
                try
                {
                    ds = DBHelper.ExecuteStoredProcDataSet("Company_Get", new SqlParameter[] { }, this.DatabaseName);
                }
                catch (Exception ex)
                {
                    LogError("Get", "None", ex);
                    Console.WriteLine("Error in obj.Get: {0}", ex.ToString());
                }
            }
            else
            {
                try
                {
                    ds = DBHelper.ExecuteStoredProcDataSet("Company_GetNoImage", new SqlParameter[] { }, this.DatabaseName);
                }
                catch (Exception ex)
                {
                    LogError("Get", "None", ex);
                    Console.WriteLine("Error in obj.Get: {0}", ex.ToString());
                }
            }

            return PopulateFromDataSet(ds);
        }

        public bool SaveEmailHeader(int id, EmailItemTypeEnum dataType, byte[] dataArray)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("Company_Update_EmailHeader", CommandType.StoredProcedure, GetEmailUpdateParams(id, dataType, "Header", dataArray), this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("SaveEmailHeader", string.Format("EmailItemTypeEnum dataType: {0}, byte[] dataArray: {1}", dataType.ToString(), dataArray.ToString()), ex);
                Console.WriteLine("Error in obj.SaveEmailHeader: {0}", ex.ToString());
                retVal = false;
            }
            return retVal;
        }

        public bool SaveEmailFooter(int id, EmailItemTypeEnum dataType, byte[] dataArray)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("Company_Update_EmailFooter", CommandType.StoredProcedure, GetEmailUpdateParams(id, dataType, "Footer", dataArray), this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("SaveEmailFooter", string.Format("EmailItemTypeEnum dataType: {0}, byte[] dataArray: {1}", dataType.ToString(), dataArray.ToString()), ex);
                Console.WriteLine("Error in obj.SaveEmailFooter: {0}", ex.ToString());
                retVal = false;
            }
            return retVal;
        }

        public bool SaveDirectorsSignature(int id, byte[] imageArray)
        {
            bool retVal = false;
            try
            {
                DBHelper.ExecuteNonQuery("Company_Update_DirectorsSignature", CommandType.StoredProcedure, new SqlParameter[] { new SqlParameter("Id", id), new SqlParameter("@Signature", imageArray) }, this.DatabaseName);
                retVal = true;
            }
            catch (Exception ex)
            {
                LogError("SaveDirectorsSignature", string.Format("byte[] imageArray: {0}", imageArray.ToString()), ex);
                Console.WriteLine("Error in obj.SaveDirectorsSignature: {0}", ex.ToString());
                retVal = false;
            }
            return retVal;
        }

        /// <summary>
        /// Method to clear an image from the company table
        /// </summary>
        /// <param name="id">id is always 1 as the company table has only 1 entry</param>
        /// <param name="caseType">1 - email footer, 2 - director's signature</param>
        /// <returns></returns>
        public bool ClearCompanyImage(int id, int caseType)
        {
            DBHelper.ExecuteStoredProcDataSet("Company_Clear_Image", new SqlParameter[] { new SqlParameter("@Id", id), new SqlParameter("@Type", caseType) }, this.DatabaseName);
            return true;
        }

        public int ToggleDefaultAttachmentForCompany(int DocId, bool DefaultAttachment)
        {
            int RetVal = 1;
            DBHelper.ExecuteStoredProcDataSet("SetDefaultAttachmentForCompanyDocument", new SqlParameter[] { new SqlParameter("@DocId", DocId), new SqlParameter("@DefaultAttachment", DefaultAttachment) }, this.DatabaseName);
            return RetVal;
        }

        public List<Types.CompanyDocumentsObject> GetDefaultAttachmentForCompany()
        {
            DataSet ds = null;
            List<Types.CompanyDocumentsObject> Docs = new List<Types.CompanyDocumentsObject>();
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Get_AllDefaultAttachmentFromCompany", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetDefaultAttachmentForCompany", "None", ex);
                Console.WriteLine("Error in GetDefaultAttachmentForCompany: {0}", ex.ToString());
            }
            Docs = PopulateCompanyDocFromDataSet(ds);
            return Docs;
        }

        public bool Save(Types.Company obj)
        {
            if (obj.Id < 1)
                return Insert(obj);
            else
                return Update(obj);
        }
        #endregion
        #region Private
        private bool Update(Types.Company obj)
        {
            bool hasCompleted = false;
            try
            {
                DBHelper.ExecuteNonQuery("Company_Update", CommandType.StoredProcedure, Parameters(obj,ParameterMode.Update), this.DatabaseName);
                //If we reach here we can assume that the operation completed
                hasCompleted = true;
            }
            catch (Exception ex)
            {
                LogError("Update", obj.ToString(), ex);
                Console.Write("Error in Company.Update: {0}", ex.ToString());
                hasCompleted = false;
            }
            return hasCompleted;
        }

        private bool Insert(Types.Company obj)
        {
            bool hasCompleted = false;
            try
            {
                DBHelper.ExecuteNonQuery("Company_Insert", CommandType.StoredProcedure, Parameters(obj,ParameterMode.Insert), this.DatabaseName);
                //If we reach here we can assume that the operation completed
                hasCompleted = true;
            }
            catch (Exception ex)
            {
                LogError("Insert", obj.ToString(), ex);
                Console.Write("Error in Company.Insert: {0}", ex.ToString());
                hasCompleted = false;
            }
            return hasCompleted;
        }

        private SqlParameter[] GetEmailUpdateParams(int id, EmailItemTypeEnum dataType, string emailComponent, byte[] dataArray)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",id),
                new SqlParameter("@Type",(int)dataType),
                new SqlParameter(emailComponent,dataArray)
            };
        }

        private List<SqlParameter> Parameters(Types.Company obj, ParameterMode mode)
        {
            List<SqlParameter> ReturnVal = new List<SqlParameter>();

            if (mode == ParameterMode.Update)
            {
                ReturnVal.Add(Parameterize("@Id", obj.Id));
            }

            ReturnVal.Add(Parameterize("@RegistrationNo", obj.RegistrationNo));
            ReturnVal.Add(Parameterize("@VatRegNo", obj.VatRegNo));
            ReturnVal.Add(Parameterize("@Name", obj.Name));
            ReturnVal.Add(Parameterize("@StreetNumber", obj.StreetNumber));
            ReturnVal.Add(Parameterize("@Address1", obj.Address1));
            ReturnVal.Add(Parameterize("@Address2", obj.Address2));
            ReturnVal.Add(Parameterize("@Town", obj.Town));
            ReturnVal.Add(Parameterize("@County", obj.County));
            ReturnVal.Add(Parameterize("@PostCode", obj.PostCode));
            ReturnVal.Add(Parameterize("@Telephone", obj.Telephone));
            ReturnVal.Add(Parameterize("@Mobile", obj.Mobile));
            ReturnVal.Add(Parameterize("@WebSite", obj.WebSite));
            ReturnVal.Add(Parameterize("@Email", obj.Email));
            ReturnVal.Add(Parameterize("@AccountsEmail", obj.AccountsEmail));
            ReturnVal.Add(Parameterize("@DirectorId", obj.DirectorId));
            ReturnVal.Add(Parameterize("@DirectorsName", obj.DirectorsName));
            ReturnVal.Add(new SqlParameter("@HeaderType", obj.HeaderType));
            ReturnVal.Add(Parameterize("@Header", obj.Header));
            ReturnVal.Add(new SqlParameter("@FooterType", obj.FooterType));
            ReturnVal.Add(Parameterize("@Footer", obj.Footer));
            ReturnVal.Add(Parameterize("@DirectorsSignature", obj.DirectorsSignature));
            ReturnVal.Add(Parameterize("@DirectorsQualifications", obj.DirectorsQualifications));
            ReturnVal.Add(Parameterize("@VatRate", obj.VatRate, 5, 2));
            ReturnVal.Add(Parameterize("@SageVatCode", obj.SageVatCode));
            ReturnVal.Add(Parameterize("@SageSalesCode", obj.SageSalesCode));
            ReturnVal.Add(Parameterize("@SageReceiptCode", obj.SageReceiptCode));
            ReturnVal.Add(Parameterize("@CommonNames", obj.CommonNames));
            ReturnVal.Add(Parameterize("@DisplayMarketing", obj.DisplayMarketing));

            return ReturnVal;
        }

       

        private Types.Company PopulateFromDataSet(DataSet ds)
        {
            Types.Company retVal = new Types.Company();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("PopulateFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in obj.PopulateFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }


        private Types.Company CreateObject(DataRow row)
        {
            Types.Company obj = new Types.Company();
            obj.Id = (int)row["Id"];
            obj.RegistrationNo = (string)row["RegistrationNo"];
            obj.VatRegNo = (string)row["VatRegNo"];
            obj.Name = (string)row["Name"];
            obj.StreetNumber = (string)row["StreetNumber"];//
            obj.Address1 = (string)row["Address1"];
            obj.Address2 = (string)row["Address2"];
            obj.Town = (string)row["Town"];
            obj.County = (string)row["County"];
            obj.PostCode = (string)row["PostCode"];
            obj.Telephone = (string)row["Telephone"];
            obj.Mobile = (string)row["Mobile"];//
            obj.WebSite = (string)row["WebSite"];
            obj.Email = (string)row["Email"];//
            obj.AccountsEmail = (string)row["AccountsEmail"];
            obj.DirectorId = (int)row["DirectorId"];
            obj.DirectorsName = (string)row["DirectorsName"];
            obj.HeaderType = (EmailItemTypeEnum)((int)row["HeaderType"]);
            obj.Header = base.ByteValue(row, "Header");
            obj.FooterType = (EmailItemTypeEnum)((int)row["FooterType"]);
            obj.Footer = base.ByteValue(row, "Footer");
            obj.DirectorsSignature = base.ByteValue(row, "DirectorsSignature");
            obj.DirectorsQualifications = (string)row["DirectorsQualifications"];
            obj.VatRate = (decimal)row["VatRate"];
            obj.SageVatCode = (string)row["SageVatCode"];
            obj.SageSalesCode = (string)row["SageSalesCode"];
            obj.SageReceiptCode = (string)row["SageReceiptCode"];
            obj.CommonNames = (bool)row["CommonNames"];
            obj.DisplayMarketing = this.BoolValue(row, "DisplayMarketing");
            return obj;
        }
        private List<Types.CompanyDocumentsObject> PopulateCompanyDocFromDataSet(DataSet ds)
        {
            List<Types.CompanyDocumentsObject> retVal = new List<Types.CompanyDocumentsObject>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateCompanyDocObject(r));
                }
               
            }
            catch (Exception ex)
            {
                LogError("PopulateCompanyDocFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in PopulateCompanyDocFromDataSet: {0}", ex.ToString());
            }
            return retVal;
        }

        private Types.CompanyDocumentsObject CreateCompanyDocObject(DataRow row)
        {
            Types.CompanyDocumentsObject obj = new Types.CompanyDocumentsObject();
            obj.Id = (int)row["Id"];
            obj.DocumentType = (string)row["DocumentType"];
            obj.DocumentName = StringValue(row, "DocumentName");
            obj.DocumentDate = DateTimeValue(row, "DocumentDate");
            obj.DocumentFileName = (string)row["DocumentFileName"];
            obj.DefaultAttachment = (bool)row["DefaultAttachment"];
            obj.DocumentImage = base.ByteValue(row, "DocumentImage");
            return obj;
        }
        #endregion
        #endregion
    }
}
