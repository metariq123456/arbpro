﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Document : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public Document(string databaseName)
            : base(databaseName, "Document", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        public List<Types.JobDocumentsObject> GetDocumentsAll()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobDocument_GetAll", new SqlParameter[] { }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetDocumentsAll", "None", ex);
                Console.WriteLine("Error in JobDocumentsObject.JobDocument_GetAll: {0}", ex.ToString());
            }
            return GetResultsDataSet(ds);
        }

        public List<Types.JobDocumentsObject> GetJobDocuments(int JobId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobDocument_ForJob", new SqlParameter[] { new SqlParameter("@JobId", JobId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetJobDocuments", "None", ex);
                Console.WriteLine("Error in JobDocumentsObject.GetJobDocuments: {0}", ex.ToString());
            }
            return GetResultsDataSet(ds);
        }

        public List<Types.JobDocumentsObject> GetDefaultJobDocuments(int JobId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("DefaultJobDocument_ForJob", new SqlParameter[] { new SqlParameter("@JobId", JobId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetDefaultJobDocuments", "None", ex);
                Console.WriteLine("Error in JobDocumentsObject.GetDefaultJobDocuments: {0}", ex.ToString());
            }
            return GetResultsDataSet(ds);
        }
        public Types.JobDocumentsObject GetDefaultJobDocumentById(int DocId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobDocument_GetById", new SqlParameter[] { new SqlParameter("@DocId", DocId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetDefaultJobDocuments", "None", ex);
                Console.WriteLine("Error in JobDocumentsObject.GetDefaultJobDocuments: {0}", ex.ToString());
            }
            return GetFirstFromDataSet(ds);
        }
        public int Save(Types.JobDocumentsObject obj)
        {
            if (obj.Id > 0)
                return Update(obj);
            else
                return Insert(obj);
        }

        public int ToggleDefaultAttachmentForJob(int DocId, bool DefaultAttachment)
        {
            int RetVal = 1;
            DBHelper.ExecuteStoredProcDataSet("SetDefaultAttachmentForJobDocument", new SqlParameter[] { new SqlParameter("@DocId", DocId), new SqlParameter("@DefaultAttachment", DefaultAttachment) }, this.DatabaseName);
            return RetVal;
        }

        public Types.JobDocumentsObject GetForId(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("JobDocuments_GetForId", new SqlParameter[] { new SqlParameter("@Id", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int id: {0}", id.ToString()), ex);
                Console.WriteLine("Error in Documents.GetForId: {0}", ex.ToString());
            }
            return GetFirstFromDataSet(ds);
        }

        public bool Delete(int Id)
        {
            bool retval = false;
            try
            {
                int rows = DBHelper.ExecuteStoredProcRowCount("JobDocument_Delete", GetDeleteParams(Id), this.DatabaseName);
                retval = rows > 0;
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.JobDocumentsObject obj: {0}", Id.ToString()), ex);
                Console.WriteLine("Error in JobDocumentsObject: {0}", ex.ToString());
                retval = false;
            }
            return retval;

        }

        private SqlParameter[] GetDeleteParams(int id)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",id),
            };
        }

        private Types.JobDocumentsObject GetFirstFromDataSet(DataSet ds)
        {
            Types.JobDocumentsObject retVal = new Types.JobDocumentsObject();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("GetFirstFromDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Staff.GetFirstFromDataSet: {0}", ex.ToString());
            }

            return retVal;
        }
        private int Insert(Types.JobDocumentsObject obj)
        {
            int retVal = -1;
            try
            {
                var p = GetInsertParams(obj);
                retVal = DBHelper.ExecuteStoredProcReturnID("JobDocument_Insert", GetInsertParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.CompanyDocumentsObject obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in CompanyDocumentsObject: {0}", ex.ToString());
            }
            return retVal;
        }

        private int Update(Types.JobDocumentsObject obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("JobDocument_Update", GetUpdateParams(obj), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.CompanyDocumentsObject obj: {0}", obj.ToString()), ex);
                Console.WriteLine("Error in CompanyDocumentsObject: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] GetInsertParams(Types.JobDocumentsObject obj)
        {
            //NOTE: An associated User account is not inserted into a new staff row
            return new SqlParameter[]
            {
                new SqlParameter("@DocumentName",obj.DocumentName),
                new SqlParameter("@DocumentType",obj.DocumentType),
                new SqlParameter("@DocumentDate",obj.DocumentDate),
                new SqlParameter("@Staff",obj.Staff),
                Parameterize("DocumentImage", obj.DocumentImage),
                new SqlParameter("DocumentFileName",obj.DocumentFileName),
                new SqlParameter("@JobId",obj.JobId)

            };
        }

        private SqlParameter[] GetUpdateParams(Types.JobDocumentsObject obj)
        {
            return new SqlParameter[]
            {
                new SqlParameter("@Id",obj.Id),
                new SqlParameter("@DocumentName",obj.DocumentName),
                new SqlParameter("@DocumentType",obj.DocumentType),
                new SqlParameter("@DocumentDate",obj.DocumentDate),
                new SqlParameter("@Staff",obj.Staff),
                Parameterize("DocumentImage", obj.DocumentImage),
                new SqlParameter("DocumentFileName",obj.DocumentFileName),
                new SqlParameter("@JobId",obj.JobId)
            };
        }
        private List<Types.JobDocumentsObject> GetResultsDataSet(DataSet ds)
        {
            List<Types.JobDocumentsObject> retVal = new List<Types.JobDocumentsObject>();

            if (ds == null || ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateObject(r));
                }
            }
            catch (Exception ex)
            {
                LogError("GetResultsDataSet", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Console.WriteLine("Error in Document.GetResultsDataSet: {0}", ex.ToString());
            }

            return retVal;
        }
        private Types.JobDocumentsObject CreateObject(DataRow row)
        {
            Types.JobDocumentsObject obj = new Types.JobDocumentsObject();
            obj.Id = (int)row["Id"];
            obj.DocumentName = (string)row["DocumentName"];
            obj.DocumentDate = (DateTime)row["DocumentDate"];
            obj.DocumentType = (string)row["DocumentType"];
            obj.Staff = (string)row["Staff"];
            obj.DocumentImage = (byte[])row["DocumentImage"];
            obj.DocumentFileName = (string)row["DocumentFileName"];
            obj.DefaultAttachment = BoolValue(row, "DefaultAttachment");
            return obj;
        }
        #endregion
    }
}
