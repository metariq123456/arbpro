﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class Complaint : Contracts.DBContextBase
    {
        #region Constructors
        public Complaint(string databaseName) : base(databaseName, "Complaint", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Methods
        #region Public
        public int Save(Types.Complaint obj)
        {
            if (obj.Id < 1)
                return Update(ParameterMode.Insert, obj);
            else
                return Update(ParameterMode.Update, obj);
        }

        public Types.Complaint GetForId(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("Complaint_GetForId", new SqlParameter[] { new SqlParameter("@Id", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("int id: {0}", id.ToString()), ex);
                Debug.WriteLine(string.Format("Complaint.GetForId, int id: {0} error: {1}", id.ToString(), ex.ToString()));
            }
            return ToFirstObject(ds);
        }
        #endregion
        #region Private
        private SqlParameter[] Parameters(Types.Complaint obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();
            if (mode == ParameterMode.Update)
                retVal.Add(new SqlParameter("@Id", obj.Id));

            retVal.Add(new SqlParameter("@EnteredBy", obj.EnteredBy));
            retVal.Add(new SqlParameter("@AllocattedTo", obj.AllocattedTo));
            retVal.Add(new SqlParameter("@SiteVisitedBy", obj.SiteVisitedBy));
            retVal.Add(new SqlParameter("@ComplaintType", obj.ComplaintType));
            retVal.Add(new SqlParameter("@Title", obj.Title));
            retVal.Add(new SqlParameter("@Firstname", obj.Firstname));
            retVal.Add(new SqlParameter("@Surname", obj.Surname));
            retVal.Add(new SqlParameter("@AddressLn1", obj.AddressLn1));
            retVal.Add(new SqlParameter("@AddressLn2", obj.AddressLn2));
            retVal.Add(new SqlParameter("@Town", obj.Town));
            retVal.Add(new SqlParameter("@County", obj.County));
            retVal.Add(new SqlParameter("@PostCode", obj.PostCode));
            retVal.Add(new SqlParameter("@Telephone", obj.Telephone));
            retVal.Add(new SqlParameter("@Mobile", obj.Mobile));
            retVal.Add(new SqlParameter("@Email", obj.Email));
            retVal.Add(new SqlParameter("@Details", obj.Details));
            retVal.Add(new SqlParameter("@ActionTaken", obj.ActionTaken));
            retVal.Add(new SqlParameter("@AssociatedQuoteRef", obj.AssociatedQuoteRef));
            retVal.Add(new SqlParameter("@Closed", obj.Closed));
            retVal.Add(new SqlParameter("@ContactMethod", (int)obj.ContactMethod));
            retVal.Add(Parameterize("@DateOfComplaint", obj.DateOfComplaint));
            retVal.Add(Parameterize("@DateRectified", obj.DateRectified));

            return retVal.ToArray();
        }

        private int Update(ParameterMode mode, Types.Complaint obj)
        {
            int retVal = -1;
            try
            {
                string procedureName = mode == ParameterMode.Insert ? "Complaint_Insert" : "Complaint_Update";
                retVal = DBHelper.ExecuteStoredProcReturnID(procedureName, Parameters(obj,mode), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("ParameterMode mode: {0}, Types.Complaint obj: {1}", mode.ToString(),obj.ToString()), ex);
            }
            return retVal;
        }

        private Types.Complaint ToFirstObject(DataSet ds)
        {
            Types.Complaint retVal = new Types.Complaint();

            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToGenericList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.ToGenericList{0}{1}{0}", Environment.NewLine, ex.ToString());
            }

            return retVal;
        }



        private List<Types.Complaint> ToObjectList(DataSet ds)
        {
            List<Types.Complaint> retVal = new List<Types.Complaint>();

            if (ds == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                LogError("ToGenericList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in: ComplaintSearchResult.ToGenericList{0}{1}{0}", Environment.NewLine, ex.ToString());
            }

            return retVal;
        }

        private Types.Complaint CreateInstance(DataRow row)
        {
            Types.Complaint obj = new Types.Complaint();
            obj.Id = (int)row["Id"];
            obj.EnteredBy = (string)row["EnteredBy"];
            obj.AllocattedTo = (string)row["AllocattedTo"];
            obj.SiteVisitedBy = (string)row["SiteVisitedBy"];
            obj.ComplaintType = (string)row["ComplaintType"];
            obj.Title = (string)row["Title"];
            obj.Firstname = (string)row["Firstname"];
            obj.Surname = (string)row["Surname"];
            obj.AddressLn1 = (string)row["AddressLn1"];
            obj.AddressLn2 = (string)row["AddressLn2"];
            obj.Town = (string)row["Town"];
            obj.County = (string)row["County"];
            obj.PostCode = (string)row["PostCode"];
            obj.Telephone = (string)row["Telephone"];
            obj.Mobile = (string)row["Mobile"];
            obj.Email = (string)row["Email"];
            obj.Details = (string)row["Details"];
            obj.ActionTaken = (string)row["ActionTaken"];
            obj.AssociatedQuoteRef = (string)row["AssociatedQuoteRef"];
            obj.Closed = (bool)row["Closed"];
            obj.ContactMethod = (Types.Enums.ContactMethodEnum)((int)row["ContactMethod"]);
            obj.DateOfComplaint = DateTimeValue(row, "DateOfComplaint");
            obj.DateEntered = DateTimeValue(row, "DateEntered");
            obj.DateRectified = DateTimeValue(row, "DateRectified");
            obj.DateClosed = DateTimeValue(row, "DateClosed");
            return obj;
        }
        #endregion
        #endregion
    }
}
