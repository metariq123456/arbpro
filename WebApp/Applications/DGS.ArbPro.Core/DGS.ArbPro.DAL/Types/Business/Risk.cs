﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class Risk : Contracts.DBContextBase
    {
        #region Members
        int _Id = -1;
        int _QuoteId = -1;
        int _ClientId = -1;
        int _StaffId = -1;
        //The address information is stored in the Risk record as client site records maybe updated and break the historic link
        string _StreetNumber = "";
        string _Address1 = "";
        string _Address2 = "";
        string _Town = "";
        string _County = "";
        string _PostCode = "";

        DateTime _AssessedDate = DateTime.MinValue;
        #endregion

        #region Constructors
        public Risk(string databaseName)
            : base(databaseName, "Risk")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #endregion
    }
}

