﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class QuoteItem : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public QuoteItem(string databaseName)
            : base(databaseName, "QuoteItem")
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #endregion
    }
}
