﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using log4net;
using System.Threading.Tasks;
using DGS.ArbPro.Types;
using DGS.ArbPro.Utility;


namespace DGS.ArbPro.DAL
{
    public class AdminUser :Contracts.DBContextBase
    {
        #region Constructors
        /// <summary>
        /// Default constructer
        /// </summary>
        public AdminUser() : base("", "AdminUser") { }
        #endregion

        #region Methods
        #region Public
        /// <summary>
        /// Public method to insert or update an admin user
        /// </summary>
        /// <param name="obj">The populated object to be inserted or updated</param>
        /// <returns>The id of the inserted / updated row or -1 if there is an error</returns>
        public int Save(Types.AdminUser obj)
        {
            if(obj.Id < 1)
                return this.Save("AdminUser_Insert",this.Parameters(obj));
            else
                return this.Save("AdminUser_Update",this.Parameters(obj));
        }

        /// <summary>
        /// Public method to return a single user from the database
        /// with the provided id
        /// </summary>
        /// <param name="id">The id for the Admin user to be returned</param>
        /// <returns>A populated object. The Id of the object will be -1 if there is an error</returns>
        public Types.AdminUser GetForId(int id)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("AdminUser_Get_Id", new SqlParameter[] { Parameterize("@Id", id) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                base.LogError("GetForId", string.Format("int id: {0}", id), ex);
            }
            return this.ToObject(ds);
        }

        /// <summary>
        /// Public method to return a single user from the database
        /// with the provided username
        /// </summary>
        /// <param name="username">The username for the Admin user to be returned</param>
        /// <returns>A populated object. The Id of the object will be -1 if there is an error</returns>
        public Types.AdminUser GetForUserName(string username)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("AdminUser_Get_Username", new SqlParameter[] { Parameterize("@Username", username) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                base.LogError("GetForUserName",string.Format("string username: {0}",username),ex);
            }
            return this.ToObject(ds);
        }

        /// <summary>
        /// Public method to return a single user from the database
        /// with the provided email
        /// </summary>
        /// <param name="username">The username for the Admin user to be returned</param>
        /// <returns>A populated object. The Id of the object will be -1 if there is an error</returns>
        public Types.AdminUser GetForEmail(string email)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("AdminUser_Get_Email", new SqlParameter[] { Parameterize("@Email", email) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                base.LogError("GetForEmail", string.Format("string email: {0}", email), ex);
            }
            return this.ToObject(ds);
        }

        /// <summary>
        /// Public method to return all Admin users currently in the database
        /// </summary>
        /// <returns>A Populated list. The list will only be empty if there is an error</returns>
        public List<Types.AdminUser> GetAll()
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("AdminUser_Get_All", new SqlParameter[] {}, this.DatabaseName);
            }
            catch (Exception ex)
            {
                base.LogError("GetAll", "None", ex);
            }
            return this.ToObjectList(ds);
        }
        #endregion
        #region Private
        /// <summary>
        /// Private method to insert or update a row in the database
        /// </summary>
        /// <param name="procedure">The procedure to be run</param>
        /// <param name="parameters">The paramater array the procedure requires</param>
        /// <returns>The Id of the last inserted row</returns>
        private int Save(string procedure, SqlParameter[] parameters)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID(procedure, parameters, this.DatabaseName);
            }
            catch (Exception ex)
            {
                base.LogError("Save",string.Format("procedure: {0}, parameters: {1}",procedure,parameters.ToString()),ex);
            }
            return retVal;
        }

        /// <summary>
        /// Private method generate a parameter array for insert or update stored procedures 
        /// </summary>
        /// <param name="obj">The object containing the values to be parameteriseed</param>
        /// <returns>An Array of SQL Paramaters</returns>
        private SqlParameter[] Parameters(Types.AdminUser obj)
        {
            var parameters = new List<SqlParameter>();
            if(obj.Id > 0)
                parameters.Add(Parameterize("@Id",obj.Id));

            parameters.Add(Parameterize("@Username", obj.Username));
            parameters.Add(Parameterize("@EmailAddress", obj.EmailAddress));
            parameters.Add(Parameterize("@Company", obj.Company));
            parameters.Add(Parameterize("@FirstName", obj.FirstName));
            parameters.Add(Parameterize("@LastName", obj.LastName));
            parameters.Add(Parameterize("@PasswordHashed", obj.PasswordHashed));
            parameters.Add(Parameterize("@Salt", obj.Salt));
            parameters.Add(Parameterize("@AuthenticationFailures", obj.AuthenticationFailures));

            return parameters.ToArray();
        }

        /// <summary>
        /// Private method to inflate a single object from the first row of the first table of a dataset
        /// </summary>
        /// <param name="ds">The Dataset containing the row to be inflated</param>
        /// <returns>A single object</returns>
        private Types.AdminUser ToObject(DataSet ds)
        {
            var retVal = new Types.AdminUser();

            if (!base.HasRows(ds))
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                base.LogError("ToObject", "DataSet ds", ex);
            }
            return retVal;
        }

        /// <summary>
        /// Private method to generate a list of objects from the first table in a dataset
        /// </summary>
        /// <param name="ds">The dataset containing the data to be inflated into objects</param>
        /// <returns>A List of objects</returns>
        private List<Types.AdminUser> ToObjectList(DataSet ds)
        {
            var retVal = new List<Types.AdminUser>();

            if (!base.HasRows(ds))
                return retVal;

            try
            {
                foreach(DataRow r in ds.Tables[0].Rows)
                {
                    retVal.Add(this.CreateInstance(r));
                }
            }
            catch (Exception ex)
            {
                base.LogError("ToObjectList", "Dataset: ds", ex);
            }

            return retVal;
        }

        /// <summary>
        /// Private method to inflate a single object from a data row
        /// </summary>
        /// <param name="r">DataRow that is to be inflated into an object</param>
        /// <returns>A populated Types.AdminUser</returns>
        private Types.AdminUser CreateInstance(DataRow r)
        {
            var obj = new Types.AdminUser();

            obj.Id = base.IntValue(r, "Id");
            obj.Username = base.StringValue(r, "Username");
            obj.EmailAddress = base.StringValue(r, "Email");
            obj.FirstName = base.StringValue(r, "Firstname");
            obj.LastName = base.StringValue(r, "LastName");
            obj.Company = base.StringValue(r, "Company");
            obj.AuthenticationFailures = base.IntValue(r, "AuthenticationFailures");
            obj.PasswordHashed = base.StringValue(r, "Password");
            obj.Salt = base.StringValue(r, "Salt");

            return obj;
        }
        #endregion
        #endregion
    }
}
