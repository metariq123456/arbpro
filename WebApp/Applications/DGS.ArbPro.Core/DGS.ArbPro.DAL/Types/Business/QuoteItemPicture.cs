﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.DAL
{
    public class QuoteItemPicture : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public QuoteItemPicture(string databaseName)
            : base(databaseName, "QuoteItemPicture", DateTime.Parse("1970-01-01"))
        {
        }

        #endregion

        #region Properties
        #endregion

        #region Methods
        #endregion
    }
}

