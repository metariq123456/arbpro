﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DGS.ArbPro.Types;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.DAL
{
    public class PermitsNecessary : Contracts.DBContextBase
    {
        #region Constructors
        public PermitsNecessary(string databaseName)
            : base(databaseName, "TreeProtectionOrder", DateTime.Parse("1970-01-01"))
        {
        }
        #endregion

        #region Methods
        public int Save(Types.PermitsNecessary obj)
        {
            if (obj.Id > 0)
                return this.Update(obj);
            else
                return this.Insert(obj);
        }

        public Types.PermitsNecessary GetForQuote(int quoteId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("PermitsNecessary_GetForQuote", new SqlParameter[] { new SqlParameter("@QuoteId", quoteId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForQuote", string.Format("int quoteId: {0}", quoteId), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.GetForQuote: {0}", ex.ToString());
            }
            return ToFirstObject(ds);
        }
        private int Insert(Types.PermitsNecessary obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("PermitsNecessary _Insert", Parameters(obj, ParameterMode.Insert), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Insert", string.Format("Types.PermitsNecessary obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in PermitsNecessary.Insert: {0}", ex.ToString());
            }
            return retVal;
        }
        private int Update(Types.PermitsNecessary obj)
        {
            int retVal = -1;
            try
            {
                retVal = DBHelper.ExecuteStoredProcReturnID("PermitsNecessary_Update", Parameters(obj, ParameterMode.Update), this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("Update", string.Format("Types.PermitsNecessary obj: {0}", obj.ToString()), ex);
                Debug.WriteLine("Error in PermitsNecessary.Update: {0}", ex.ToString());
            }
            return retVal;
        }
        private Types.PermitsNecessary ToFirstObject(DataSet ds)
        {
            Types.PermitsNecessary retVal = new Types.PermitsNecessary();

            if (ds == null | ds.Tables == null || ds.Tables.Count == 0 || ds.Tables[0].Rows == null || ds.Tables[0].Rows.Count == 0)
                return retVal;

            try
            {
                retVal = CreateInstance(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToFirstObject", string.Format("DataSet ds: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in TreeProtectionOrder.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        private SqlParameter[] Parameters(Types.PermitsNecessary obj, ParameterMode mode)
        {
            List<SqlParameter> retVal = new List<SqlParameter>();
            //Only add the objects Id to the params collection if it is being updated
            if (mode == ParameterMode.Update)
                retVal.Add(new SqlParameter("@Id", obj.Id));

            retVal.Add(new SqlParameter("@QuoteId", obj.QuoteId));
            retVal.Add(new SqlParameter("@PermitNecessary", obj.PermitNecessary));
            retVal.Add(new SqlParameter("@PermitObtained", obj.PermitObtained));
            retVal.Add(new SqlParameter("@PermitType", obj.PermitType));
            retVal.Add(new SqlParameter("@PermitComments", obj.PermitComments));
            return retVal.ToArray();
        }
        internal Types.PermitsNecessary CreateInstance(DataRow row)
        {
            Types.PermitsNecessary obj = new Types.PermitsNecessary();
            obj.Id = base.IntValue(row,"Id");
            obj.QuoteId = base.IntValue(row,"QuoteId");
            obj.PermitNecessary = base.IntValue(row, "PermitNecessary");
            obj.PermitObtained = base.IntValue(row, "PermitObtained");
            obj.QuoteReference = base.StringValue(row,"QuoteReference");
            obj.PermitComments = base.StringValue(row, "PermitComments");
            obj.PermitType = base.StringValue(row, "PermitType");
            return obj;
        }
        #endregion
    }
}
