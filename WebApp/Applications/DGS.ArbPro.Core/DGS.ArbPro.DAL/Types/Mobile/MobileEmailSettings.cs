﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DGS.ArbPro.DAL
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Diagnostics;

    public class MobileEmailSettings : Contracts.DBContextBase
    {
        #region Members
        #endregion

        #region Constructors
        public MobileEmailSettings(string database) : base(database, "MobileEmailSettings", DateTime.Parse("1970-01-01")) { }
        #endregion

        #region Properties
        #endregion

        #region Methods
        #region Public
        #region Get Methods
        public Types.MobileEmailSettings GetForAppointMentId(int appointmentId)
        {
            DataSet ds = null;
            try
            {
                ds = DBHelper.ExecuteStoredProcDataSet("MobileEmailSettings_GetForAppointmentID", new SqlParameter[] { new SqlParameter("@AppointmentId", appointmentId) }, this.DatabaseName);
            }
            catch (Exception ex)
            {
                LogError("GetForId", string.Format("appointment Id: {0}", appointmentId), ex);
                Debug.WriteLine("Error in JobSite.GetForId: {0}", ex.ToString());
            }
            return ToObject(ds);
        }
        #endregion
        #endregion
        #region Private Methods


        private Types.MobileEmailSettings ToObject(DataSet ds)
        {
            Types.MobileEmailSettings retVal = null;
            //Check if there is any data to be returned
            if (!base.HasRows(ds))
            {
                return retVal;
            }

            try
            {
                retVal = CreateObject(ds.Tables[0].Rows[0]);
            }
            catch (Exception ex)
            {
                LogError("ToObjectList", string.Format("DataSet: {0}", ds.ToString()), ex);
                Debug.WriteLine("Error in MobileEmailSettings.ToObjectList: {0}", ex.ToString());
            }
            return retVal;
        }

        /// <summary>
        /// Helper method to create an object instance from a data row
        /// </summary>
        /// <param name="row">DataRow</param>
        /// <returns>MobileEmailSettings instance populated with details required to send an email</returns>
        private Types.MobileEmailSettings CreateObject(DataRow row)
        {
            Types.MobileEmailSettings obj = new Types.MobileEmailSettings();
            obj.AppointmentId = base.IntValue(row, "AppointmentId");
            obj.SurveyorId = base.IntValue(row, "SurveyorId");
            obj.JobSiteId = base.IntValue(row, "JobSiteId");
            obj.ContactId = base.IntValue(row, "ContactId");
            obj.QuoteId = base.IntValue(row, "QuoteId");
            obj.QuoteLetterTemplateId = base.IntValue(row, "QuoteLetterTemplateId");
            obj.DefaultLetterContent = base.StringValue(row, "DefaultLetterContent");
            obj.DefaultSalutation = base.StringValue(row, "DefaultSalutation");
            obj.DefaultEmailBody = base.StringValue(row, "DefaultEmailBody");
            obj.DefaultEmailSubject = base.StringValue(row, "DefaultEmailSubject");
            obj.FromAddress = base.StringValue(row, "FromAddress");
            obj.DefaultSmsText = base.StringValue(row, "DefaultSmsText");

            return obj;
        }
        #endregion
        #endregion
    }
}
