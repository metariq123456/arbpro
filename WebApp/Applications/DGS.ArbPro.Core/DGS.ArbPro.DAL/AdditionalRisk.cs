﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class AdditionalRisk
    {
        public string AssessedRisk { get; set; }
        public string ControlMeasures { get; set; }
    }
}

