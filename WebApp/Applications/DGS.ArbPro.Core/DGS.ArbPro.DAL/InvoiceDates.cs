﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace DGS.ArbPro.DAL
{
    public class InvoiceDates
    {
        public Int32 ID;
        public Int32 ClientID;
        public Int32 QuoteID;
        public Int32 InvoiceID;
        public DateTime InvoiceSentDate;
        public String InvoiceType;
        public String UserCR;
        public Boolean Success;

        public InvoiceDates(Int32 Client, Int32 Quote, Int32 Invoice, DateTime SentDate, String InvoiceType, String UserCR, Boolean Success)
        {
            this.ID = -1;
            this.ClientID = Client;
            this.QuoteID = Quote;
            this.InvoiceID = Invoice;
            this.InvoiceSentDate = SentDate;
            this.InvoiceType = InvoiceType;
            this.UserCR = UserCR;
            this.Success = Success;
        }
        public InvoiceDates(SqlDataReader Row)
        {
            ID = Row.IntegerValue("InvoiceDatesAutoNo");
            ClientID = Row.IntegerValue("ClientIDNo");
            QuoteID = Row.IntegerValue("QuoteNo");
            InvoiceID = Row.IntegerValue("InvoiceNo");
            InvoiceSentDate = Row.DateValue("InvoiceDatesDateSent");
            InvoiceType = Row.StringValue("InvoiceDatesType");
            UserCR = Row.StringValue("UserCR");
            Success = Row.BooleanValue("Success");
        }

        public void Save(String DatabaseName)
        {
            this.ID = InvoiceDatesDB.Save(DatabaseName, this);
        }
    }
}
