﻿<%@ Page Title="Company Setup" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Company.aspx.cs" Inherits="DGS.ArbPro.Web.Company" %>

<asp:Content runat="server" ID="TopContent" ContentPlaceHolderID="TopContent">
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <script type="text/javascript" src="Content/js/Company.js?V=3.23.5"></script>
    <asp:Panel runat="server" ID="pnlTabsContainer">
        <asp:Panel ID="pnlCompanyTopPanels" CssClass="width-full float-left" runat="server" ClientIDMode="Static">
            <asp:Panel runat="server" ID="pnlCompanyInformation" CssClass="companyPanel" ClientIDMode="Static">
                <asp:Panel runat="server" ID="pnlCompanyInfoHeader" CssClass="colored-raise companyPanelHeader  width-full" ClientIDMode="Static">
                    <asp:Label ID="Label1" runat="server" CssClass="CompanyLabel">Company Information</asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlCompanyInfoTabs" runat="server" CssClass="companyTabBar" ClientIDMode="Static">
                    <asp:HyperLink runat="server" ID="pnlCompanyInfoTab_0" ClientIDMode="Static" CssClass="companyTab activeTab" NavigateUrl="javascript:Information_CurTab = 0; SwitchInfoTab();">
                            General
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="pnlCompanyInfoTab_1" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Information_CurTab = 1; SwitchInfoTab();">
                            Teams
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="pnlCompanyInfoTab_2" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Information_CurTab = 2; SwitchInfoTab();">
                            Staff
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="pnlCompanyInfoTab_3" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Information_CurTab = 3; SwitchInfoTab();">
                            Contacts
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="pnlCompanyInfoTab_4" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Information_CurTab = 4; SwitchInfoTab();">
                            Defaults
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="pnlCompanyInfoTab_5" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Information_CurTab = 5; SwitchInfoTab();">
                            Documents
                    </asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="pnlCompanyInfoDetails" runat="server" CssClass="width-full companyScrollBar" ClientIDMode="Static">
                    <asp:Panel ID="pnlCompanyInfoDetail_0" runat="server" CssClass="" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="center">
                            <asp:Image ID="cmpImage1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlCompanyInfoDetail_1" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="center">
                            <asp:Image ID="cmpImage2" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlCompanyInfoDetail_2" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="center">
                            <asp:Image ID="cmpImage3" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlCompanyInfoDetail_3" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="center">
                            <asp:Image ID="cmpImage8" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlCompanyInfoDetail_4" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="center">
                            <asp:Image ID="cmpImage9" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlCompanyInfoDetail_5" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="center">
                            <asp:Image ID="cmpImage10" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlReportOptions" CssClass="companyPanel" ClientIDMode="Static">
                <asp:Panel runat="server" ID="pnlReportOptionsHeader" CssClass="colored-raise companyPanelHeader  width-full" ClientIDMode="Static">
                    <asp:Label runat="server" CssClass="CompanyLabel">Reports Options and Data Export</asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlReportOptionsTabs" runat="server" CssClass="companyTabBar" ClientIDMode="Static">
                    <asp:HyperLink runat="server" ID="tabReportOption_0" ClientIDMode="Static" CssClass="companyTab activeTab" NavigateUrl="javascript:Reports_CurTab = 0; SwitchReportsTab();">
                            PDF Options
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabReportOption_1" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Reports_CurTab = 1; SwitchReportsTab();">
                            Email Options
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabReportOption_2" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Reports_CurTab = 2; SwitchReportsTab();">
                            Accounts Export
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabReportOption_3" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Reports_CurTab = 3; SwitchReportsTab();">
                            Data Export
                    </asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="pnlReportOptionsDetail" runat="server" CssClass="width-full" ClientIDMode="Static">
                    <asp:Panel ID="pnlReportOptionsDetail_0" runat="server" CssClass="" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="float-left width-full">
                            <asp:Panel runat="server" CssClass="float-left bordered padded" Width="98%">
                                PDF Header
                            <asp:Button runat="server" CssClass="button-thin float-right padded" ID="btnReportOptionsHeaderUpload"
                                ClientIDMode="Static" Text="View/Edit" Width="160px"
                                OnClientClick="UploadHeader(); return false;" />
                            </asp:Panel>
                        </asp:Panel><br/><br/><br/>
                        <asp:Panel runat="server" CssClass="float-left width-full">
                            <asp:Panel runat="server" CssClass="float-left bordered padded" Width="98%">
                                PDF Footer
                            <asp:Button runat="server"  CssClass="button-thin float-right padded" ID="btnReportOptionsFooterUpload"
                                ClientIDMode="Static" Text="View/Edit" Width="160px"
                                OnClientClick="UploadFooter(); return false;" />
                            </asp:Panel>
                        </asp:Panel><br/><br/><br/>
                        <asp:Panel ID="pnlRptOptSignature" runat="server" CssClass="float-left width-full">
                            <asp:Panel ID="pnlRptOptSignatureHeader" runat="server" CssClass="float-left bordered padded" Width="98%">
                                Director's Signature
                            <asp:Button runat="server"  CssClass="button-thin float-right padded" ID="btnReportOptionsSignatureUpload"
                                ClientIDMode="Static" Text="View/Edit" Width="160px"
                                OnClientClick="UploadSignature(); return false;" />
                            </asp:Panel>
                        </asp:Panel><br/><br/><br/>
                            <asp:Panel ID="pnlPreviewReport" runat="server" CssClass="float-left bordered padded" Width="98%">
                                Preview Example PDF
                            <asp:Button runat="server" CssClass="button-thin float-right padded" ID="btnPreview" 
                                Text="Preview Report" OnClientClick="PreviewReport(); return false;" />
                            </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlReportOptionsDetail_1" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <%--<asp:Panel runat="server" CssClass="center">
                            <asp:Image ID="cmpImage12" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>--%>
                        <asp:Panel ID="pnlEmailFtrUpload" runat="server" CssClass="float-left width-full">
                            <br/><asp:Panel ID="pnlEmailFtrTitle" runat="server" CssClass="float-left bordered padded" Width="98%">
                                Email Footer
                            <asp:Button runat="server" CssClass="button-thin float-right padded" ID="btnEmailFooterUpload"
                                ClientIDMode="Static" Text="View/Edit" Width="160px"
                                OnClientClick="UploadEmailFooter(); return false;" />
                            </asp:Panel><br/><br/><br/><br/>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlReportOptionsDetail_2" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel ID="pnlCompatability" runat="server" CssClass="hidden" ClientIDMode="Static">
                            <br />
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="clientField" Style="float: none;">
                            <asp:Label runat="server" CssClass="clientLabel">Export To:</asp:Label>
                            <asp:DropDownList runat="server" CssClass="clientInput"
                                ID="ddlExportTo" ClientIDMode="Static" OnChange="ExportToChanged();">
                                <asp:ListItem Enabled="true" Value="0">Sage 13</asp:ListItem>
                                <asp:ListItem Enabled="true" Value="1">Sage 09</asp:ListItem>
                                <asp:ListItem Enabled="true" Value="2">Xero</asp:ListItem>
                                <asp:ListItem Enabled="true" Value="4">Quickbooks</asp:ListItem>
                                <asp:ListItem Enabled="true" Value="3">Kashflow</asp:ListItem>
                                <asp:ListItem Enabled="true" Value="5">CSV</asp:ListItem>
                            </asp:DropDownList>
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="clientField" Style="float: none;">
                            <asp:Label runat="server" CssClass="clientLabel">Export Type:</asp:Label>
                            <asp:DropDownList runat="server" CssClass="clientInput"
                                ID="ddlExportType" ClientIDMode="Static" OnChange="ExportTypeChanged();">
                                <asp:ListItem Enabled="true" Value="0">Clients</asp:ListItem>
                                <asp:ListItem Enabled="true" Value="1">Invoices Sent</asp:ListItem>
                                <asp:ListItem Enabled="true" Value="2">Invoices Paid</asp:ListItem>
                                <asp:ListItem Enabled="true" Value="4">Invoices Raised</asp:ListItem>
                                <asp:ListItem Enabled="true" Value="5">Invoices Unpaid</asp:ListItem>
                                <asp:ListItem Enabled="true" Value="6">Invoices Paid</asp:ListItem>
                                <asp:ListItem Enabled="false" Value="3">Credit Notes</asp:ListItem>
                            </asp:DropDownList>
                        </asp:Panel>
                        <asp:Panel ID="pnlExportStart" runat="server" CssClass="clientField" Style="float: none;">
                            <asp:Label ID="lblExportFrom" runat="server" CssClass="clientLabel" ClientIDMode="Static">Export From:</asp:Label>
                            <asp:TextBox runat="server" ID="txtExportFrom" CssClass="clientInput" ClientIDMode="Static"></asp:TextBox>
                        </asp:Panel>
                        <asp:Panel ID="pnlExportTo" runat="server" CssClass="clientField" Style="float: none;">
                            <asp:Label ID="lblExportTo" runat="server" CssClass="clientLabel" ClientIDMode="Static">Export To:</asp:Label>
                            <asp:TextBox runat="server" ID="txtExportTo" CssClass="clientInput" ClientIDMode="Static"></asp:TextBox>
                        </asp:Panel>
                        <asp:Button runat="server" CssClass="button-basic float-right" Text="Export" OnClientClick="ExportData(); return false;" />
                    </asp:Panel>
                    <asp:Panel ID="pnlReportOptionsDetail_3" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="center">
                            Coming Soon: From here you will be able to select client, quote and invoice information to be exported and downloaded.
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel ID="pnlCompnyBottomPanel" runat="server" CssClass="width-full float-left" ClientIDMode="Static">
            <asp:Panel runat="server" ID="pnlCompanyComplaints" CssClass="companyPanel">
                <asp:Panel runat="server" ID="pnlCmpComplaintTabsHeader" CssClass="colored-raise companyPanelHeader  width-full" ClientIDMode="Static">
                    <asp:Label runat="server" CssClass="CompanyLabel">Complaints</asp:Label>
                    <asp:Button runat="server" ID="btnCmpNewComplaint" ClientIDMode="Static" CssClass="button-basic float-right padded" Text="New Complaint" OnClientClick="ShowComplaintDialog(-1); return false;" />
                </asp:Panel>
                <asp:Panel ID="pnlCmpComplaintTabBar" runat="server" CssClass="companyTabBar" ClientIDMode="Static">
                    <asp:HyperLink runat="server" ID="tabComplaint_0" ClientIDMode="Static" CssClass="companyTab activeTab" NavigateUrl="javascript:Complaint_CurTab = 0; SwitchComplaintTab();">
                            Open
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabComplaint_1" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Complaint_CurTab = 1; SwitchComplaintTab();">
                            Closed
                    </asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="pnlComplaintsDetail" runat="server" CssClass="width-full companyScrollBar">
                    <asp:Panel ID="pnlComplaintTab_0" runat="server" CssClass="" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="center">
                            <asp:Image ID="cmpImage4" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlComplaintTab_1" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="center">
                            <asp:Image ID="cmpImage5" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlDocuments" CssClass="companyPanel" ClientIDMode="Static">
                <asp:Panel runat="server" ID="pnlDocumentsHeader" CssClass="colored-raise companyPanelHeader width-full">
                    <asp:Label runat="server" CssClass="CompanyLabel">Documents</asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlDocumentTabs" runat="server" CssClass="companyTabBar" ClientIDMode="Static">
                    <asp:HyperLink runat="server" ID="tabDocument_0" ClientIDMode="Static" CssClass="companyTab activeTab" NavigateUrl="javascript:Documents_CurTab = 0; SwitchDocumentsTab();">
                            Emails
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabDocument_1" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Documents_CurTab = 1; SwitchDocumentsTab();">
                            Letters
                    </asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="pnlDocumentDetails" runat="server" CssClass="width-full companyScrollBar" ClientIDMode="Static">
                    <asp:Panel ID="pnlDocumentDetail_0" runat="server" CssClass="" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="center">
                            <asp:Image ID="cmpImage6" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlDocumentDetail_1" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel runat="server" CssClass="center">
                            Coming Soon: From here you will be able to view all letters that have been generated and downloaded via ArbPro.
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <script type="text/javascript" async>
        $(function () {
            LoadCompanyScreenItems();
        });
    </script>
</asp:Content>