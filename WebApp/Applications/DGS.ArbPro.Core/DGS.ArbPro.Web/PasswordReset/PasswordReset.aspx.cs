﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PasswordReset.aspx.cs" company="Microsec Ltd">
//   Copyright 2015 Microsec Ltd
// </copyright>
// <summary>
//   Code behind for the password reset form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace DGS.ArbPro.Web.PasswordReset
{
    using System;

    using DGS.ArbPro.Types;

    /// <summary>
    /// The password reset form.
    /// </summary>
    public partial class PasswordReset : System.Web.UI.Page
    {
        #region Fields

        /// <summary>
        /// The user to reset.
        /// </summary>
        private User userToReset;

        #endregion

        #region Methods

        /// <summary>
        /// Handler for the login button - redirects to the default page.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
       protected void BtnLoginClick(object sender, EventArgs e)
        {
            this.Response.Redirect("/Default.aspx");
        }

        /// <summary>
        /// The click event for the Reset button
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void BtnResetClick(object sender, EventArgs e)
        {
            var userManager = new DAL.User("");

            this.btnLogin.Enabled = false;

            var guid = this.Request.Params["Id"];
            this.userToReset = userManager.GetForResetGuid(guid);
            if (this.userToReset == null)
            {
                this.lblFailureText.Text = "Reset request not found - it may have expired.";
                this.btnReset.Enabled = false;
                return;
            }

            if (string.IsNullOrEmpty(this.txtUserName.Text.Trim()))
            {
                this.lblFailureText.Text = "Please enter your username";
                return;
            }

            var userEntered = userManager.GetForEmail(this.txtUserName.Text.Trim());
            if (userEntered == null)
            {
                this.lblFailureText.Text = "Username not found.";
                return;
            }

            if (userEntered.EmailAddress != this.userToReset.EmailAddress)
            {
                this.lblFailureText.Text = "User entered does not match this request.";
                return;
            }

            if (string.IsNullOrEmpty(this.txtPassword1.Text.Trim())
                && string.IsNullOrEmpty(this.txtPassword2.Text.Trim()))
            {
                this.lblFailureText.Text = "Please enter both password and confirmation of password";
                return;
            }

            if (this.txtPassword1.Text != this.txtPassword2.Text)
            {
                this.lblFailureText.Text = "Passwords do not match.";
                return;
            }

            Utility.Logger.Log.InfoFormat("Password reset by user {0}", userEntered.EmailAddress);
            try
            {
                userEntered.PasswordHashed = userEntered.HashPassword(this.txtPassword1.Text);
                userEntered.Unlock();

                userManager.Save(userEntered);
                userManager.ClearResetGuid(userEntered);
            }
            catch (Exception)
            {
                this.lblFailureText.Text = "An error occurred resetting your password. Please contact support.";
                return;
            }

            this.lblFailureText.Text = "Your password has been reset.";
            this.btnLogin.Enabled = true;
        }

        /// <summary>
        /// The page load event.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.btnLogin.Enabled = false;

            this.lblFailureText.Text = string.Empty;

            var userManager = new DAL.User("");

            var guid = this.Request.Params["Id"];
            this.userToReset = userManager.GetForResetGuid(guid);
            if (this.userToReset == null)
            {
                this.lblFailureText.Text = "Reset request not found - it may have expired.";
                this.btnReset.Enabled = false;
            }
        }

        #endregion
    }
}