﻿<%@ Page Title="Password Reset" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PasswordReset.aspx.cs" Inherits="DGS.ArbPro.Web.PasswordReset.PasswordReset" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <asp:Panel runat="server" ID="pnlPasswordReset" CssClass="PasswordResetPane white">
        <asp:Panel ID="Panel1" runat="server"
            CssClass="colored width-full curved-top"
            Style="padding-top: 5px; padding-bottom: 3px; text-align: left;">
            <asp:Label ID="Label1" runat="server" CssClass="lblTitle">
                Password Reset
            </asp:Label>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server" CssClass="width-full" Style="margin-top: 40px">
            <asp:TextBox runat="server"
                ID="txtUserName"
                CssClass="txtLogin"
                placeholder="Email Address" />
            <asp:RequiredFieldValidator runat="server"
                ID="rfvUserNamePasswordReset"
                ControlToValidate="txtUserName"
                ValidationGroup="PasswordReset"
                CssClass="errPasswordReset"
                ErrorMessage="*" />
        </asp:Panel>
        <asp:Panel ID="Panel3" runat="server" CssClass="width-full">
            <asp:TextBox runat="server"
                ID="txtPassword1"
                CssClass="txtLogin"
                TextMode="Password"
                placeholder="New password" />
            <asp:RequiredFieldValidator runat="server"
                ID="rfvPassword1"
                ControlToValidate="txtPassword1"
                CssClass="errPasswordReset"
                ErrorMessage="*"
                ValidationGroup="PasswordReset" />
        </asp:Panel>
        <asp:Panel ID="Panel6" runat="server" CssClass="width-full">
            <asp:TextBox runat="server"
                ID="txtPassword2"
                CssClass="txtLogin"
                TextMode="Password"
                placeholder="Confirm new password" />
            <asp:RequiredFieldValidator runat="server"
                ID="RequiredFieldValidator1"
                ControlToValidate="txtPassword2"
                CssClass="errPasswordReset"
                ErrorMessage="*"
                ValidationGroup="PasswordReset" />
        </asp:Panel>
        <asp:Panel ID="Panel4" runat="server"
            Style="text-align: center;"
            CssClass="errPasswordReset">
            <asp:Literal runat="server"
                ID="lblFailureText" />
        </asp:Panel>
        <asp:Panel runat="server" Style="margin-top: 10px;">
            <div style="float: left">

                <asp:Button runat="server"
                    ID="btnReset"
                    OnClick="BtnResetClick"
                    CssClass="button-basic"
                    Text="Reset password"
                    Style="margin-left: 25px;" />
            </div>

            <div style="float: right">
                <asp:Button runat="server"
                    ID="btnLogin"
                    OnClick="BtnLoginClick"
                    CssClass="button-basic"
                    Text="Log In"
                    Style="margin-right: 25px;" />
            </div>

        </asp:Panel>
    </asp:Panel>
</asp:Content>
