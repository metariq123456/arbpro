﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class Calendar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var DB = new DAL.Staff(AspSession.UserDatabase);
            var Staff = DB.GetAll();

            ddlEventStaff.Items.Add(new ListItem("All", "-1"));

            foreach (var Member in Staff)
            {
                ddlEventStaff.Items.Add(new ListItem(Member.Name, Member.Id.ToString()));
            }
        }
    }
}