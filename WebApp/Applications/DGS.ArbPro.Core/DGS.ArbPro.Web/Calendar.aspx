﻿<%@ Page Title="Calendar" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Calendar.aspx.cs" Inherits="DGS.ArbPro.Web.Calendar" %>

<asp:Content runat="server" ID="TopContent" ContentPlaceHolderID="TopContent">
    <script type="text/javascript" src="Content/js/Calendar.js?V=3.23.5"></script>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <asp:Panel runat="server" ID="pnlCalFilter" CssClass="calendarFilters white">
        <asp:Panel runat="server" CssClass="calendarFilterHeader">
            Calendar Options
        </asp:Panel>
        <asp:Button runat="server" ID="btnCreateDatabase" ClientIDMode="Static" CssClass="float-right padded center btnBookJob" Text="Book Job"  OnClientClick="BookAcceptedJobsDialog(); return false;" Width="120px" Style="font-size:18px;"/>
        <asp:Panel runat="server" CssClass="clientField" style="float: none">
            <asp:Label runat="server" CssClass="clientLabel">Show Events: </asp:Label>
            <asp:DropDownList runat="server" CssClass="clientInput"
                ID="ddlEventType" ClientIDMode="Static" 
                onChange="UpdateFilters();">
                <asp:ListItem Value="0">All</asp:ListItem>
                <asp:ListItem Value="1">Quotes</asp:ListItem>
                <asp:ListItem Value="2">Work</asp:ListItem>
                <asp:ListItem Value="3">Generic</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
        <asp:Panel runat="server" CssClass="clientField" style="float: none">
            <asp:Label runat="server" CssClass="clientLabel">Staff: </asp:Label>
            <asp:DropDownList runat="server" CssClass="clientInput"
                ID="ddlEventStaff" ClientIDMode="Static" 
                onChange="UpdateFilters();"/>
        </asp:Panel>
        
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlCalendar" ClientIDMode="Static">
    </asp:Panel>
    <script type="text/javascript" async>
        $(function () {
            StartCalendar();
        });
    </script>
</asp:Content>
