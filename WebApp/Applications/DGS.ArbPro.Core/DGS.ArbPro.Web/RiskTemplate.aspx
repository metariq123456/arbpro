﻿<%@ Page Title="Risk Templates" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RiskTemplate.aspx.cs" Inherits="DGS.ArbPro.Web.RiskTemplate" %>

<asp:Content runat="server" ID="TopContent" ContentPlaceHolderID="TopContent">
    <asp:Panel ID="Panel1" runat="server" CssClass="width-full bordered-bottom" Height="28px">
        <asp:Button runat="server"
            ID="btnDeleteTemplate" ClientIDMode="Static"
            Text="Delete Template"
            CssClass="button-thin float-right padded"
            OnClientClick="DeleteTemplate(); return false;" />
        <asp:Button runat="server"
            ID="btnNewTemplate" ClientIDMode="Static"
            Text="New Template"
            CssClass="button-thin float-right padded"
            OnClientClick="NewTemplate(); return false;" />
    </asp:Panel>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <script type="text/javascript" src="Content/js/TemplateRisk.js?V=3.23.5"></script>
    <asp:Panel runat="server" ID="pnlEdit" ClientIDMode="static" CssClass="templateContent height-full">
        <asp:Panel runat="server" ID="pnlTemplateInfo" ClientIDMode="Static" CssClass="templateInfo">
            <asp:Panel ID="Panel2" runat="server" CssClass="colored-raise templateHeader">
                <asp:Label ID="Label1" runat="server">
                        Template Information
                </asp:Label>
                <asp:HiddenField runat="server" ID="hfTemplateID" ClientIDMode="Static" Value="1" />
                <asp:HiddenField runat="server" ID="hfSectionTotal" ClientIDMode="Static" />
                <asp:DropDownList runat="server" Enabled="false"
                    ID="ddlTemplateType" ClientIDMode="Static"
                    CssClass="templateInput float-right"
                    onfocus="riskWatcher.watchForChange(this)"
                    onblur="riskWatcher.cancelWatchForChange()"
                    onchange="riskWatcher.changed(this)">
                    <asp:ListItem Value="1">Default</asp:ListItem>
                    <asp:ListItem Value="2">General</asp:ListItem>
                    <asp:ListItem Value="3">Specific</asp:ListItem>
                </asp:DropDownList>
            </asp:Panel>
            <asp:Panel ID="Panel3" runat="server" CssClass="float-left">
                <asp:Panel runat="server" ID="pnlTemplateDetails" ClientIDMode="Static">
                    <asp:Panel ID="Panel4" runat="server" CssClass="templateField">
                        <asp:Label ID="Label2" runat="server" CssClass="templateLabel">
                                Name
                        </asp:Label>
                        <asp:TextBox runat="server"
                            ID="txtTemplateName" ClientIDMode="Static"
                            CssClass="templateInput"
                            onfocus="templateWatcher.watchForChange(this);"
                            onblur="templateWatcher.cancelWatchForChange();"
                            onchange="templateWatcher.changed(this)"
                            AutoCompleteType="Search" Enabled="false" />
                    </asp:Panel>
                    <asp:Panel ID="Panel5" runat="server" CssClass="templateField">
                        <asp:Label ID="Label3" runat="server" CssClass="templateLabel">
                                Reference Document
                        </asp:Label>
                        <asp:TextBox runat="server" Enabled="false"
                            ID="txtTemplateRefDoc" ClientIDMode="Static"
                            CssClass="templateInput"
                            onfocus="templateWatcher.watchForChange(this)"
                            onblur="templateWatcher.cancelWatchForChange()"
                            onchange="templateWatcher.changed(this)" />
                    </asp:Panel>
                    <br />
                    <asp:Panel ID="Panel6" runat="server" CssClass="templateField">
                        <asp:Label ID="Label4" runat="server" CssClass="templateLabel">
                                Description
                        </asp:Label>
                        <asp:TextBox runat="server" TextMode="multiline" Enabled="false"
                            ID="txtTemplateDescription" ClientIDMode="Static"
                            CssClass="templateInputArea"
                            onfocus="templateWatcher.watchForChange(this)"
                            onblur="templateWatcher.cancelWatchForChange()"
                            onchange="templateWatcher.changed(this)" />
                    </asp:Panel>
                    <asp:Panel ID="Panel8" runat="server">
                        <asp:Button runat="server" Enabled="false"
                            ID="btnSave" ClientIDMode="Static"
                            CssClass="button-basic float-right padded"
                            Text="Save"
                            OnClientClick="SaveRiskTemplate(); return false;" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlClientLists" CssClass="clientTabs">
            <asp:Panel ID="pnlTabs" runat="server" CssClass="clientTabBar">
                <% CreateSectionTabs(); %>
            </asp:Panel>
            <% CreateSectionContent(); %>
       </asp:Panel>
    </asp:Panel>
    <script type="text/javascript" async>
    $(function () {
        try {
            var param = GetURLParameter('curTab');
            if (param != 'null')
                Section_curTab = parseInt(param);
        } catch (e) { }
        SwitchTab();
        //LayoutSectionList();
    });
    </script>
</asp:Content>
