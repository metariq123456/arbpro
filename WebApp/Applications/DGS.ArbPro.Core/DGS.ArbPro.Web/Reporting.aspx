﻿<%@ Page Title="Reporting" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Reporting.aspx.cs" Inherits="DGS.ArbPro.Web.Reporting" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="SyncFusion/Current/content/bootstrap.min.css" rel="stylesheet">
    <link href="SyncFusion/Current/content/ejthemes/default-theme/ej.web.all.min.css" rel="stylesheet" />
    <link href="SyncFusion/Current/content/default.css" rel="stylesheet" />
	<link href="SyncFusion/Current/content/default-responsive.css" rel="stylesheet" />
    <%--<script src="Scripts/jquery-3.0.0.min.js?V=3.23.5" type="text/javascript"></script>--%>
    <script src="Scripts/ej.web.all.min.js?V=3.23.5" type="text/javascript"></script>
	<script src="SyncFusion/Current/scripts/properties.js?V=3.23.5" type="text/javascript"></script>    
     
    <script type="text/javascript" src="Content/js/Reporting.js?V=3.23.5"></script>

<asp:Panel runat="server" ID="pnlContentReporting" ClientIDMode="Static" CssClass="jobContent">
    <br/>
    <asp:Panel runat="server" CssClass="float-clear width-full">
        <asp:Panel ID="pnlReporting" runat="server" Width="80%" CssClass="jobTabs align-left" Style="margin-bottom: 10px;">
            <asp:Panel ID="pnlReportingHeader" runat="server" CssClass="colored-raise jobHeader">
                <asp:Label ID="lblReportingHealer" runat="server">
                    Reporting
                </asp:Label>
            </asp:Panel>
            <asp:Panel ID="pnlFilterReportingContent" runat="server" ClientIDMode="Static" CssClass="width-full">
            </asp:Panel> 
            <br/>
            <asp:Panel ID="pnlReportingGraph" runat="server" ClientIDMode="Static" CssClass="width-full">
	            <div class="content-container-fluid">      
                    <div class="row">                
                        <div class="cols-sample-area">                                  				
				            <div id="container"></div> 					                   
                        </div>				
                    </div>
	            </div>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>
    
 <script type="text/javascript" language="javascript" async>
     $(function () {
         debugger;
         LoadReporting();
     });

     function seriesRender(sender) {
         if (sender.model.theme == "flatdark" || sender.model.theme == "gradientdark")
             sender.data.series.marker.dataLabel.connectorLine.color = "white";
     }
 </script>

</asp:Content>