﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DGS.ArbPro.Web._Default" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <script type="text/javascript" src="Content/js/Dashboard.js?V=3.23.5"></script>
    <asp:Panel runat="server" CssClass="float-left width-full">
        <asp:Panel ID="Panel19" runat="server" CssClass="float-left width-full">
            <!-- Invoices -->
            <asp:Panel ID="pnlInvoiceTabs" runat="server" CssClass="dashboardPanel white-light" ClientIDMode="Static">
                <asp:Panel runat="server" ID="pnlInvoiceTabsHeader" CssClass="width-full colored-raise dashboardPanelHeader" ClientIDMode="Static">
                    <asp:Label runat="server" ID="lblInvoiceTabsHeader" CssClass="dashboardLabel" ClientIDMode="Static">Invoices</asp:Label>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlInvoiceTabsBar" CssClass="dashboardTabBar width-full">
                    <asp:HyperLink runat="server" ID="tabInvoices_0" ClientIDMode="Static" CssClass="dashboardTab activeTab" NavigateUrl="javascript:Invoice_curTab = 0; SwitchInvoiceTab();">
                        Overdue
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabInvoices_1" ClientIDMode="Static" CssClass="dashboardTab" NavigateUrl="javascript:Invoice_curTab = 1; SwitchInvoiceTab();">
                        Awaiting Dispatch
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabInvoices_2" ClientIDMode="Static" CssClass="dashboardTab" NavigateUrl="javascript:Invoice_curTab = 2; SwitchInvoiceTab();">
                        Payments Due
                    </asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="pnlInvoiceDetailsTabs" runat="server" CssClass="float-left width-full">

                    <!-- Overdue Invoices Detail panel-->
                    <asp:Panel ID="pnlInvoicesTab_0" runat="server" ClientIDMode="Static" CssClass="width-full">
                        <asp:Panel runat="server" ID="pnlOverdueInvoicesHeader" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="lblOverdueInvoices" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                                Invoices where payment has not been received as scheduled.
                            </asp:Label>
                            <asp:Label ID="lblOverdueInvoicesValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="pnlOverdueInvoices" runat="server" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel white-light">
                            <asp:Panel ID="Panel10" runat="server" CssClass="center width-full white">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                    <!-- Work completed awaiting invoice tab -->
                    <asp:Panel ID="pnlInvoicesTab_1" runat="server" ClientIDMode="Static" CssClass="width-full  hidden">
                        <asp:Panel runat="server" ID="pnlPendingInvoicesHeader" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="lblPendingInvoices" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                                Invoices where payment has not been received as scheduled.
                            </asp:Label>
                            <asp:Label ID="lblPendingInvoicesValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlPendingInvoices" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel white-light">
                            <asp:Panel ID="Panel3" runat="server" CssClass="center width-full white">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                    <!-- Scheduled Invoices Detail panel-->
                    <asp:Panel ID="pnlInvoicesTab_2" runat="server" ClientIDMode="Static" CssClass="width-full  hidden">
                        <asp:Panel runat="server" ID="pnlScheduledPaymentsHeader" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="lblScheduledPayments" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                                Invoices where payment has not been received as scheduled.
                            </asp:Label>
                            <asp:Label ID="lblScheduledPaymentsValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlScheduledPayments" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel white-light">
                            <asp:Panel runat="server" CssClass="center width-full white">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                </asp:Panel>
            </asp:Panel>
            <!-- Quotes -->
            <asp:Panel runat="server" ID="pnlQuoteTabs" CssClass="dashboardPanel  white-light" ClientIDMode="Static">
                <asp:Panel runat="server" ID="pnlQuoteTabsHeader" CssClass="width-full colored-raise dashboardPanelHeader" ClientIDMode="Static">
                    <asp:Label runat="server" ID="lblQuoteTabsHeader" CssClass="dashboardLabel" ClientIDMode="Static">Sales Enquiries and Quotes</asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlQuoteTabsBar" runat="server" CssClass="dashboardTabBar width-full">
                    <asp:HyperLink runat="server" ID="tabQuotes_0" ClientIDMode="Static" CssClass="dashboardTab activeTab" NavigateUrl="javascript:Quote_curTab = 0; SwitchQuoteTab();">
                        Pending Enquiries
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabQuotes_1" ClientIDMode="Static" CssClass="dashboardTab" NavigateUrl="javascript:Quote_curTab = 1; SwitchQuoteTab();">
                        Pending Quotes
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabQuotes_2" ClientIDMode="Static" CssClass="dashboardTab" NavigateUrl="javascript:Quote_curTab = 2; SwitchQuoteTab();">
                        Accepted Quotes
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabQuotes_3" ClientIDMode="Static" CssClass="dashboardTab" NavigateUrl="javascript:Quote_curTab = 3; SwitchQuoteTab();">
                        Notes
                    </asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="pnlQuoteDetailsTabs" runat="server" CssClass="float-left width-full">
                    <!-- Pending Sales Enquiries -->
                    <asp:Panel ID="pnlQuoteTab_0" runat="server" CssClass="width-full">
                        <asp:Panel runat="server" ID="pnlEnquiriesOutstandingHeader" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="lblPendingEnquiries" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                            Enquiries awaiting sales appointments
                            </asp:Label>
                            <asp:Label ID="lblPendingEnquiriesValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="pnlPendingEnquiries" runat="server" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel white-light">
                            <asp:Panel ID="Panel14" runat="server" CssClass="center width-full white">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                    <!-- Quotes waiting to be sent -->
                    <asp:Panel ID="pnlQuoteTab_1" runat="server" CssClass="width-full hidden">
                        <asp:Panel runat="server" ID="pnlPendingQuotesHeader" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="lblPendingQuotes" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                            Quotes awaiting approval and dispatch
                            </asp:Label>
                            <asp:Label ID="lblPendingQuotesValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel ID="pnlPendingQuotes" runat="server" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel white-light">
                            <asp:Panel ID="Panel16" runat="server" CssClass="center width-full white">
                                <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                    <!-- Pending Sales Enquiries -->
                    <asp:Panel ID="pnlQuoteTab_2" runat="server" CssClass="width-full hidden">
                        <asp:Panel runat="server" ID="pnlPendingWorkHeader" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="lblPendingWork" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                            Quotes accepted pending start date
                            </asp:Label>
                            <asp:Label ID="lblPendingWorkValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlPendingWork" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel white-light">
                            <asp:Panel ID="Panel18" runat="server" CssClass="center width-full white">
                                <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                    <!-- Rolling Quote Notes -->
                    <asp:Panel ID="pnlQuoteTab_3" runat="server" CssClass="width-full hidden">
                        <asp:Panel runat="server" ID="pnlPendingNotesHeader" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="lblPendingNotes" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                            Recent notes for active quotes
                            </asp:Label>
                            <asp:Label ID="lblPendingNotesValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlPendingNotes" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel white-light">
                            <asp:Panel ID="Panel7" runat="server" CssClass="center width-full white">
                                <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                </asp:Panel>
            </asp:Panel>
        </asp:Panel>

        <asp:Panel ID="Panel1" runat="server" CssClass="float-left width-full">
            <!-- Appointments -->
            <asp:Panel ID="pnlAppointmentTabs" runat="server" CssClass="dashboardPanel  white-light" ClientIDMode="Static">
                <asp:Panel runat="server" ID="pnlAppointmentTabsHeader" CssClass="width-full colored-raise dashboardPanelHeader" ClientIDMode="Static">
                    <asp:Label CssClass="dashboardLabel" runat="server" ID="lblAppointmentTabsHeader" ClientIDMode="Static">Appointments</asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlAppointmentTabsBar" runat="server" CssClass="dashboardTabBar" ClientIDMode="Static">
                    <asp:HyperLink runat="server" ID="tabAppointments_0" ClientIDMode="Static" CssClass="dashboardTab activeTab" NavigateUrl="javascript:Appointment_curTab = 0; SwitchAppointmentTab();">
                        Todays
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabAppointments_1" ClientIDMode="Static" CssClass="dashboardTab" NavigateUrl="javascript:Appointment_curTab = 1; SwitchAppointmentTab();">
                        Sales Appointments
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabAppointments_2" ClientIDMode="Static" CssClass="dashboardTab" NavigateUrl="javascript:Appointment_curTab = 2; SwitchAppointmentTab();">
                        Scheduled Work
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabAppointments_3" ClientIDMode="Static" CssClass="dashboardTab" NavigateUrl="javascript:Appointment_curTab = 3; SwitchAppointmentTab();">
                        General
                    </asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="pnlAppointmentsDetailsTabs" runat="server" CssClass="width-full ">
                    <!-- Todays Appointments Detail panel-->
                    <asp:Panel ID="pnlAppointmentsTab_0" runat="server" ClientIDMode="Static" CssClass="">
                        <asp:Panel runat="server" ID="pnlTodaysAppointmentsHeader" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="lblTodaysAppointments" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                            All Sales, Work and General appointments for today.
                            </asp:Label>
                            <asp:Label ID="lblTodaysAppointmentsValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlTodaysAppointments" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel white-light">
                            <asp:Panel ID="Panel24" runat="server" CssClass="center width-full white">
                                <asp:Image ID="Image13" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                    <!-- Sales Appointments Detail panel-->
                    <asp:Panel ID="pnlAppointmentsTab_1" runat="server" ClientIDMode="Static" CssClass="width-full hidden">
                        <asp:Panel runat="server" ID="pnlSalesAppointmentsHeader" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="lblSalesAppointments" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                        Scheduled Survyors Appointments for the next 6 days.
                            </asp:Label>
                            <asp:Label ID="lblSalesAppointmentsValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlScheduledSales" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel white-light">
                            <asp:Panel ID="Panel6" runat="server" CssClass="center width-full white">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                    <!-- Work Appointments Detail panel-->
                    <asp:Panel ID="pnlAppointmentsTab_2" runat="server" ClientIDMode="Static" CssClass="width-full hidden">
                        <asp:Panel runat="server" ID="pnlScheduledWorkHeader" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="lblScheduledWork" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                            Work scheduled for the following 6 days (new and ongoing)
                            </asp:Label>
                            <asp:Label ID="lblScheduledWorkValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlScheduledWork" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel">
                            <asp:Panel ID="Panel8" runat="server" CssClass="center width-full white">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                    <!-- General Appointments Detail panel-->
                    <asp:Panel ID="pnlAppointmentsTab_3" runat="server" ClientIDMode="Static" CssClass="width-full hidden">
                        <asp:Panel runat="server" ID="pnlGeneralAppoinmentsHeader" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label runat="server" ID="lblGeneralAppointments" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                            General appointments and holidays for the following 6 days.
                            </asp:Label>
                            <asp:Label ID="lblGeneralAppointmentsValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlGeneralAppointments" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel">
                            <asp:Panel ID="Panel28" runat="server" CssClass="center width-full white">
                                <asp:Image ID="Image14" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>

                </asp:Panel>
            </asp:Panel>
            <!-- Pending Items -->

            <asp:Panel runat="server" ID="pnlTpoInformation" CssClass="dashboardPanel" ClientIDMode="Static">
                <asp:Panel runat="server" ID="pnlTpoInfoHeader" CssClass="colored-raise dashboardPanelHeader  width-full" ClientIDMode="Static">
                    <asp:Label ID="tpoLabel1" runat="server" CssClass="dashboardLabel">Tree Protection</asp:Label>
                </asp:Panel>
                <asp:Panel ID="pnlTpoInfoTabs" runat="server" CssClass="dashboardTabBar" ClientIDMode="Static">
                    <asp:HyperLink runat="server" ID="pnlTpoTab_0" ClientIDMode="Static" CssClass="dashboardTab activeTab" NavigateUrl="javascript:Tpo_curTab = 0; SwitchTpoTab();">
                            Conservation App Dates
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="pnlTpoTab_1" ClientIDMode="Static" CssClass="dashboardTab" NavigateUrl="javascript:Tpo_curTab = 1; SwitchTpoTab();">
                            TPO App Dates
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="pnlTpoTab_2" ClientIDMode="Static" CssClass="dashboardTab" NavigateUrl="javascript:Tpo_curTab = 2; SwitchTpoTab();">
                            Appeals
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="pnlTpoTab_3" ClientIDMode="Static" CssClass="dashboardTab" NavigateUrl="javascript:Tpo_curTab = 3; SwitchTpoTab();">
                            Granted not Booked
                    </asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="pnlTpoDetails" runat="server" CssClass="width-full" ClientIDMode="Static">
                    <asp:Panel ID="pnlTpoDetail_0" runat="server" CssClass="" ClientIDMode="Static">
                        <asp:Panel runat="server" ID="pnlTpoDetailHeader_0" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="tpoLabel2" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                            All TPOs that have reach the 6 week determination date.
                            </asp:Label>
                            <asp:Label ID="tpoLabel2Value" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlTpoDetailContent_0" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel">
                            <asp:Panel ID="tpoImgPanel2" runat="server" CssClass="center">
                                <asp:Image ID="cmpImage1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlTpoDetail_1" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel runat="server" ID="pnlTpoDetailHeader_1" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="tpoLabel3" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                            All TPOs that have reach the 8 week determination date.
                            </asp:Label>
                            <asp:Label ID="tpoLabel3Value" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlTpoDetailContent_1" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel">
                            <asp:Panel ID="tpoImgPanel4" runat="server" CssClass="center">
                                <asp:Image ID="cmpImage2" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlTpoDetail_2" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel runat="server" ID="pnlTpoDetailHeader_2" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="tpoLabel4" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                                    All TPOs that are currently at Appeal stage.
                            </asp:Label>
                            <asp:Label ID="tpoLabel4Value" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlTpoDetailContent_2" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel">
                            <asp:Panel ID="tpoImgPanel5" runat="server" CssClass="center">
                                <asp:Image ID="cmpImage3" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlTpoDetail_3" runat="server" CssClass="hidden" ClientIDMode="Static">
                        <asp:Panel runat="server" ID="pnlTpoDetailHeader_3" ClientIDMode="Static" CssClass="width-full">
                            <asp:Label ID="tpoLabel5" runat="server" CssClass="dashboardDetailLabel" ClientIDMode="Static">
                                    All TPOs that have been granted but not booked.
                            </asp:Label>
                            <asp:Label ID="tpoLabel5Value" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlTpoDetailContent_3" ClientIDMode="Static" CssClass="searchGrid width-full dashboardDetailPanel">
                            <asp:Panel ID="tpoImgPanel6" runat="server" CssClass="center">
                                <asp:Image ID="cmpImage4" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <script type="text/javascript" async>
        $(function () {
            Dashboard_Load();
        });
    </script>
</asp:Content>
