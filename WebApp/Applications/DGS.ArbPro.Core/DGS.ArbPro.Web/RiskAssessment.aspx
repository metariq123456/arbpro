﻿<%@ Page Title="Risk Assessment" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RiskAssessment.aspx.cs" Inherits="DGS.ArbPro.Web.RiskAssessment" %>

<asp:Content runat="server" ID="TopContent" ContentPlaceHolderID="Content">
    <asp:HiddenField runat="server" ID="hfJobID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hfRiskID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hfQuoteID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hfAppId" ClientIDMode="Static" />
    <script type="text/javascript" src="Content/js/RiskAssessment.js?V=3.23.5"></script>
    <asp:Panel runat="server" ID="pnlJobsList" ClientIDMode="Static" CssClass="jobsList ">
        <asp:Panel runat="server"
            ID="pnlJobsListFilter" ClientIDMode="Static"
            CssClass="jobsList top">
            <asp:HyperLink runat="server"
                ID="lnkReturnToJob"
                ClientIDMode="Static"
                CssClass="float-right">
                    Return to Work Management
            </asp:HyperLink>
        </asp:Panel>
        <asp:Panel runat="server"
            ID="pnlClientRiskAssessmentList"
            ClientIDMode="Static"
            CssClass="jobsList list">
            <asp:Panel runat="server" CssClass="center width-full">
                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlAssessmentDetailsSection" runat="server" CssClass="riskInfoSml">
        <asp:Panel ID="pnlAssessmentDetailsHeader" runat="server" CssClass="colored-raise dashboardHeader">
            <asp:Label ID="lblAssessmentDetails" runat="server">
                    Risk Assessment Details
            </asp:Label>
        </asp:Panel>

        <asp:Panel ID="pnlTabDetailsHeaders" runat="server" CssClass="riskTabBar">
            <asp:HyperLink runat="server" ID="tabDetails1" ClientIDMode="Static" CssClass="jobTab activeTab" NavigateUrl="javascript:Details_curTab = 1; SwitchDetailsTab();">
                    Client
            </asp:HyperLink>
            <asp:HyperLink runat="server" ID="tabDetails2" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:Details_curTab = 2; SwitchDetailsTab();">
                    Site
            </asp:HyperLink>
            <asp:HyperLink runat="server" ID="tabDetails3" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:Details_curTab = 3; SwitchDetailsTab();">
                    Emergency
            </asp:HyperLink>
            <asp:HyperLink runat="server" ID="tabDetails4" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:Details_curTab = 4; SwitchDetailsTab();">
                    Staff
            </asp:HyperLink>
            <asp:HyperLink runat="server" ID="tabDetails5" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:Details_curTab = 5; SwitchDetailsTab();">
                    Dates
            </asp:HyperLink>
        </asp:Panel>

        <asp:Panel runat="server" ID="pnlAssessmentDetailTabs" ClientIDMode="static" CssClass="width-full">
            <asp:Panel ID="grdTabDetails1" runat="server" CssClass="width-full">
                <asp:Panel runat="server" CssClass="center width-full">
                    <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="grdTabDetails2" runat="server" CssClass="width-full hidden">
                <asp:Panel runat="server" CssClass="center width-full">
                    <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="grdTabDetails3" runat="server" CssClass="width-full hidden">
                <asp:Panel runat="server" CssClass="center width-full">
                    <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="grdTabDetails4" runat="server" CssClass="width-full hidden">
                <asp:Panel runat="server" CssClass="center width-full">
                    <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="grdTabDetails5" runat="server" CssClass="width-full hidden">
                <asp:Panel runat="server" CssClass="center width-full">
                    <asp:Image ID="Image10" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                </asp:Panel>
            </asp:Panel>
            <!--
        //Create the tab headers
        //Create the Client details content panel
        strHtml += '<div id="grdTabDetails1" class="width-full">';
        strHtml += '<div Class="riskField">';
        strHtml += '<label Class="riskLabel">Name</label><label Class="riskInput">';
        strHtml += riskAssessment.ClientName;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Telephone</label><label Class="riskInput">';
        strHtml += riskAssessment.ClientTelephone;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Email</label><label Class="riskInput">';
        strHtml += riskAssessment.ClientEmail;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Address</label><label Class="riskInput">';
        strHtml += riskAssessment.ClientAddress;
        strHtml += '</label></div>';
        strHtml += '</div>';
        //Create the Site Details content panel
        strHtml += '<div id="grdTabDetails2" class="width-full hidden">';
        strHtml += '<div Class="riskField">';
        strHtml += '<label Class="riskLabel">Site Reference</label><label Class="riskInput">';
        strHtml += riskAssessment.SiteRef;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Telephone</label><label Class="riskInput">';
        strHtml += riskAssessment.SiteTelephone;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Address Ln 1</label><label Class="riskInput">';
        strHtml += riskAssessment.SiteAddLn1;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Address Ln 2</label><label Class="riskInput">';
        strHtml += riskAssessment.SiteAddLn2;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Town</label><label Class="riskInput">';
        strHtml += riskAssessment.SiteTown;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">County</label><label Class="riskInput">';
        strHtml += riskAssessment.SiteCounty;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Post Code</label><label Class="riskInput">';
        strHtml += riskAssessment.SitePostCode;
        strHtml += '</label></div>';
        strHtml += '</div>';
        //Create the Emergency Details content panel
        strHtml += '<div id="grdTabDetails3" class="width-full hidden">';
        strHtml += '<div Class="riskField">';
        strHtml += '<label Class="riskLabel">Mobile Signal</label><label Class="riskInput">';
        strHtml += riskAssessment.MobileSignal;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Areial Rescue Plan</label><label Class="riskInput">';
        strHtml += riskAssessment.AreialPlan;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Aerial Rescue Nomaniee</label><label Class="riskInput">';
        strHtml += riskAssessment.AssignedAerialStaffName;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">CS38 Certified</label><label Class="riskInput">';
        if (riskAssessment.CS38Certifed) {
            strHtml += 'Yes';
        } else {
            strHtml += 'No';
        }
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Site Access</label><label Class="riskInput">';
        strHtml += riskAssessment.SiteAccess;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Nearest Hospital</label><label Class="riskInput">';
        strHtml += riskAssessment.NearestHospital;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Grid Reference</label><label Class="riskInput">';
        strHtml += riskAssessment.GridReference;
        strHtml += '</label></div>';
        strHtml += '</div>';
        //Create the Staff Details Panel
        strHtml += '<div id="grdTabDetails4" class="width-full hidden">';
        strHtml += '<div Class="riskField">';
        strHtml += '<label Class="riskLabel">Assessor</label><label Class="riskInput">';
        strHtml += riskAssessment.Assessor;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Team Leader</label><label Class="riskInput">';
        strHtml += riskAssessment.TeamLeader;
        strHtml += '</label></div><div><table>';
        var staffCount = riskAssessment.StaffPresent.Length
        for (i = 0; i < staffCount; i++) {
            strHtml += '<tr><td>';
            strHtml += riskAssessment.StaffPresent[i];
            strHtml += '</td></tr>';
        }
        strHtml += '</table></div>';
        strHtml += '</div>';
        //Create the Date Details Panel
        strHtml += '<div id="grdTabDetails5" class="width-full hidden">';
        strHtml += '<div Class="riskField">';
        strHtml += '<label Class="riskLabel">Assessment Date</label><label Class="riskInput">';
        strHtml += riskAssessment.AssessmentDate.toString();
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Work Date</label><label Class="riskInput">';
        strHtml += riskAssessment.WorkDate.toString();
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Review Date</label><label Class="riskInput">';
        strHtml += riskAssessment.ReviewDate.toString();
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Completion Date</label><label Class="riskInput">';
        strHtml += riskAssessment.CompletionDate.toString();
        strHtml += '</label></div>';
        strHtml += '</div>';
        //Set the html to the correct panel
        var pnlDetailsHeader = document.getElementById('pnlAssessmentDetailTabs');
        pnlDetailsHeader.innerHTML = strHtml;

        -->
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlAssessmentManagementSection" runat="server" CssClass="riskInfoSml">
        <asp:Panel ID="pnlAssessmentManagementHeader" runat="server" CssClass="colored-raise dashboardHeader">
            <asp:Label ID="lbllAssessmentManagement" runat="server">
                    Risk Management
            </asp:Label>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlAssessmentManagementTabs" ClientIDMode="static" CssClass="width-full">
            <asp:Panel ID="Panel6" runat="server" CssClass="center width-full">
                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlAssessmentSections" runat="server" CssClass="riskInfoLrg">
        <asp:Panel ID="pnlAssessmentSectionsHeader" runat="server" CssClass="colored-raise dashboardHeader">
            <asp:Label ID="lblAssessmentSections" runat="server">
                    Risk Assessment Sections
            </asp:Label>
            <asp:Button runat="server"
                    ID="btnNewPrint" ClientIDMode="Static"
                    CssClass="button-thin float-right padded"
                    Text="Print" OnClientClick="DisplayRiskAssessmentPDF(); return false;" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlAssessmentSectionsTabs" ClientIDMode="static" CssClass="width-full">
            <asp:Panel runat="server" CssClass="center width-full">
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="pnlAdditionalRisksSection" runat="server" CssClass="riskInfoLrg">
        <asp:Panel ID="pnlAdditionalRisksHdr" runat="server" CssClass="colored-raise dashboardHeader">
            <asp:Label ID="lblAdditionalRisks" runat="server">
                    Additional Risks Assessed
            </asp:Label>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlAdditionalRisks" ClientIDMode="static" CssClass="width-full center">
            <asp:Panel runat="server" CssClass="center width-full">
                <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlAddAdditionalRisks" ClientIDMode="static" CssClass="width-full center">
                <asp:Button runat="server" ID="btnEdit" ClientIDMode="Static" CssClass="button-basic float-right padded" Text="Add" OnClientClick="AddAdditionalRisk(); return false;" />
        </asp:Panel>
    </asp:Panel>
    <script type="text/javascript" async>
        $(function () {
            LoadRiskAssessment();
        });
    </script>
</asp:Content>
