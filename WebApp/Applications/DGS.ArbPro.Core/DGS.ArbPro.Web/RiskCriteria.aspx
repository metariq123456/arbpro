﻿<%@ Page Title="Risk Criteria" Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="RiskCriteria.aspx.cs" Inherits="DGS.ArbPro.Web.RiskCriteria" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <script type="text/javascript" src="Content/js/TemplateRiskCriteria.js?V=3.23.5"></script>
    <asp:Panel runat="server" ID="Panel1" ClientIDMode="Static" CssClass="width-full center">
        <asp:HiddenField runat="server" ID="hfCriteriaTotal" ClientIDMode="Static" />
        <asp:Panel runat="server" ID="pnlCriteriaInfo" Width="800px" HorizontalAlign="Center" CssClass="templateInfo center">
            <asp:Panel ID="pnlTabs" runat="server" CssClass="colored-raise templateHeader">
                <asp:Label runat="server">Criteria</asp:Label>
            </asp:Panel>
                <asp:Button runat="server"
                    ID="btnTemplateManager" ClientIDMode="Static"
                    Text="Go To Templates"
                    CssClass="button-thin float-left padded"
                    OnClientClick="window.location='/RiskTemplateEdit.aspx';return false;" />
                <asp:Button runat="server"
                    ID="btnNewSection" ClientIDMode="Static"
                    Text="New Criterion"
                    CssClass="button-thin float-right padded"
                    OnClientClick="NewEditOrCopyCriterion(-1); return false;" />
            <br/><br/>
            <% CreateCriteria(); %>
            <% CreateCriterionContent(); %>
       </asp:Panel>
    </asp:Panel>
    <script type="text/javascript" async>
        $(function () {
            try {
                var Param = GetURLParameter('curTab');
                if (Param != 'null')
                    Criterion_curTab = parseInt(Param);
            } catch (e) { }
            SwitchCriterion();
        });
    </script>
</asp:Content>
