﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminLogin.aspx.cs" Inherits="DGS.ArbPro.Web.Admin.AdminLogin" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="Content" runat="server">
     <asp:Panel runat="server" CssClass="center">
        <script type="text/javascript" src="../Content/js/User.js"></script>               
          <asp:Panel runat="server" ID="pnlLogin" CssClass="LoginPane white" Width="700">
        <asp:Panel runat="server"
            CssClass="colored width-full curved-top"
            Style="padding-top: 5px; padding-bottom: 3px; text-align: left;">
            <asp:Label runat="server" CssClass="lblTitle">
                Admin Login
            </asp:Label>
        </asp:Panel>
              <asp:Panel runat="server" CssClass="width-full" Style="margin-top: 40px">
                   <asp:TextBox runat="server"
                        ID="UserName"
                        CssClass="txtLogin"
                        placeholder="Email Address" />
                    <asp:RequiredFieldValidator runat="server"
                        ID="rfvUserNameLogin"
                        ControlToValidate="UserName"
                        ValidationGroup="Login"
                        CssClass="errLogin"
                        ErrorMessage="*" />
                                        
              </asp:Panel>
              <asp:Panel runat="server" CssClass="width-full">
                    <asp:TextBox runat="server"
                        ID="Password"
                        CssClass="txtLogin"
                        TextMode="Password"
                        placeholder="Password" />
                    <asp:RequiredFieldValidator runat="server"
                       ID="rfvPassword"
                        ControlToValidate="Password"
                        CssClass="errLogin"
                        ErrorMessage="*"
                        ValidationGroup="Login" />
                </asp:Panel>
                <asp:Panel runat="server"
                    Style="text-align: center;"
                    CssClass="errLogin">
                    <asp:Literal runat="server"
                        ID="FailureText" />
                </asp:Panel>
              <asp:Panel runat="server" Style="text-align: center;">
                     <asp:Button runat="server" CommandName="Login" ID="Button1" ValidationGroup="Login" CssClass="button-basic" Text="Log In" Style="width: 50%;" OnClick="Button1_Click" />
                </asp:Panel>
       </asp:Panel>
      </asp:Panel>
</asp:Content>
