﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateDatabase.aspx.cs" Inherits="DGS.ArbPro.Web.Admin.CreateDatabase" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript" src="../Content/js/User.js"></script>
    <asp:Panel runat="server" CssClass="center">
            <asp:Panel runat="server" ID="pnlUserMainForm" CssClass="userMainForm" Width="900px">
                <asp:Panel runat="server" CssClass="userMainTitle colored-raise">
                    Arb Pro Admin Screen 
                    <asp:HyperLink runat="server" ID="lnkReturnToClient" ClientIDMode="Static" CssClass="float-right">
                         Back
                    </asp:HyperLink>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDatabaseInfo" ClientIDMode="Static" CssClass="userMainInfo" Width="880px">
                          <asp:Label runat="server" CssClass="userCompanyInfo bold">
                                        Add Company Information 
                          </asp:Label> 
                           
                          <asp:Panel runat="server" ID="pnlUserDatabaseInfo" ClientIDMode="Static">
                            
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                       Company Name 
                                </asp:Label>
                               <asp:TextBox ID="txtCompanyName" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Company Name"/>
                                 <asp:RequiredFieldValidator runat="server"
                                    ID="rfvCompanyName"
                                    ControlToValidate="txtCompanyName"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" />
                            </asp:Panel>
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                       Address 1
                                </asp:Label>
                               <asp:TextBox ID="txtAddress1" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder=" Address 1"/>
                                 <asp:RequiredFieldValidator runat="server"
                                    ID="rfvAddress1"
                                    ControlToValidate="txtAddress1"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" />
                            </asp:Panel>
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                       Address 2
                                </asp:Label>
                               <asp:TextBox ID="txtAddress2" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Address 2"/>
                               <asp:RequiredFieldValidator runat="server"
                                    ID="rfvAddress2"
                                    ControlToValidate="txtAddress2"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                            </asp:Panel>

                             <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                       Town
                                </asp:Label>
                               <asp:TextBox ID="txtTown" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Town"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvTown"
                                    ControlToValidate="txtTown"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" />
                             </asp:Panel>
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      County
                                </asp:Label>
                               <asp:TextBox ID="txtCounty" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="County"/>
                               <asp:RequiredFieldValidator runat="server"
                                    ID="rfvCounty"
                                    ControlToValidate="txtCounty"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                            </asp:Panel>
                             <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Post code
                                </asp:Label>
                               <asp:TextBox ID="txtPostCode" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder=" Post code"/>
                               <asp:RequiredFieldValidator runat="server"
                                    ID="rfvPostCode"
                                    ControlToValidate="txtPostCode"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" />  
                             </asp:Panel>
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Country 
                                </asp:Label>
                               <asp:TextBox ID="txtCountry" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Country" Text="230" Enabled="false"/>
                               <asp:RequiredFieldValidator runat="server"
                                    ID="rfvCountry"
                                    ControlToValidate="txtCountry"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                            </asp:Panel>
                             <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Licenses 
                                </asp:Label>
                               <asp:TextBox ID="txtLicenses" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Licenses"/>
                               <asp:RequiredFieldValidator runat="server"
                                    ID="rfvLicenses"
                                    ControlToValidate="txtLicenses"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                             </asp:Panel>
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Value &pound;
                                </asp:Label>
                               <asp:TextBox ID="txtValue" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Value &pound;" Text="50" Enabled="false"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvValue"
                                    ControlToValidate="txtValue"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                            </asp:Panel>
                        </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="Panel1" ClientIDMode="Static" CssClass="userMainInfo" Width="880px">
                          <asp:Label runat="server" CssClass="userCompanyInfo bold">
                                       Add First User 
                          </asp:Label>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Email Address 
                                </asp:Label>
                               <asp:TextBox ID="txtEmailAddress" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Email Address"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvEmailAddress"
                                    ControlToValidate="txtEmailAddress"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Title  
                                </asp:Label>
                               <asp:TextBox ID="txtTitle" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Title"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvTitle"
                                    ControlToValidate="txtTitle"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      First Name  
                                </asp:Label>
                               <asp:TextBox ID="txtFirstName" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="First Name"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvFirstName"
                                    ControlToValidate="txtFirstName"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" />  
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Last Name  
                                </asp:Label>
                               <asp:TextBox ID="txtLastName" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Last Name"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvLastName"
                                    ControlToValidate="txtLastName"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Telephone  
                                </asp:Label>
                               <asp:TextBox ID="txtTelephone" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Telephone"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvTelephone"
                                    ControlToValidate="txtTelephone"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" />  
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Position   
                                </asp:Label>
                               <asp:TextBox ID="txtPosition" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Position"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvPosition"
                                    ControlToValidate="txtPosition"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" />         
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Comment   
                                </asp:Label>
                               <asp:TextBox ID="txtComment" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Comment"/>
                               <asp:RequiredFieldValidator runat="server"
                                    ID="rfvComment"
                                    ControlToValidate="txtComment"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                          </asp:Panel>
                </asp:Panel>
                <asp:Button runat="server" ID="btnCreateDatabase" ClientIDMode="Static" ValidationGroup="Create" CssClass="button-basic float-right padded" Text="Create Database & Add first User & Admin Login"  OnClientClick="SaveDatabase(); return false;"/>
           </asp:Panel>
      </asp:Panel>
    
</asp:Content>
