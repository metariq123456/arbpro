﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web.Admin
{
    public partial class CreateDatabase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["AdminLogin"] == null)
            {
                Response.Redirect("/Admin/AdminLogin.aspx");
            }
            lnkReturnToClient.NavigateUrl = "~/Admin/Main.aspx";
        }
    }
}