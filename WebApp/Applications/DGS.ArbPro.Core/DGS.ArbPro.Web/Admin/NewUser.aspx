﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewUser.aspx.cs" Inherits="DGS.ArbPro.Web.Admin.NewUser" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript" src="../Content/js/User.js"></script>
     <asp:Panel runat="server" CssClass="center">
            <asp:Panel runat="server" ID="pnlUserMainForm" CssClass="userMainForm" Width="900px">
                <asp:Panel runat="server" CssClass="userMainTitle colored-raise">
                    Add New User
                    <asp:HyperLink runat="server" ID="lnkReturnToClient" ClientIDMode="Static" CssClass="float-right">
                         Back
                    </asp:HyperLink>
                </asp:Panel>
                 <asp:Panel runat="server" ID="pnlDatabaseInfo" ClientIDMode="Static" CssClass="userMainInfo" Width="880px">
                          <asp:Label runat="server" CssClass="userCompanyInfo bold">
                                        Add User 
                          </asp:Label> 
                            <asp:HiddenField runat="server" ID="hfOrgId" ClientIDMode="Static" />
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel" ID="EmailAddress">
                                      Email Address 
                                </asp:Label>
                               <asp:TextBox ID="txtEmailAddress" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Email Address"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvEmailAddress"
                                    ControlToValidate="txtEmailAddress"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Title  
                                </asp:Label>
                               <asp:TextBox ID="txtTitle" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Title"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvTitle"
                                    ControlToValidate="txtTitle"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      First Name  
                                </asp:Label>
                               <asp:TextBox ID="txtFirstName" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="First Name"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvFirstName"
                                    ControlToValidate="txtFirstName"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" />  
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Last Name  
                                </asp:Label>
                               <asp:TextBox ID="txtLastName" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Last Name"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvLastName"
                                    ControlToValidate="txtLastName"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Telephone  
                                </asp:Label>
                               <asp:TextBox ID="txtTelephone" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Telephone"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvTelephone"
                                    ControlToValidate="txtTelephone"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" />  
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Position   
                                </asp:Label>
                               <asp:TextBox ID="txtPosition" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Position"/>
                                <asp:RequiredFieldValidator runat="server"
                                    ID="rfvPosition"
                                    ControlToValidate="txtPosition"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" />         
                          </asp:Panel>
                          <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Comment   
                                </asp:Label>
                               <asp:TextBox ID="txtComment" ClientIDMode="Static" runat="server" CssClass="userInputLarge"  placeholder="Comment"/>
                               <asp:RequiredFieldValidator runat="server"
                                    ID="rfvComment"
                                    ControlToValidate="txtComment"
                                    ValidationGroup="Create"
                                    CssClass="errLogin"
                                    ErrorMessage="*" /> 
                          </asp:Panel>
                </asp:Panel>
                <asp:Button runat="server" ID="btnCreateDatabase" ClientIDMode="Static" ValidationGroup="Create" CssClass="button-basic float-right padded" Text="Add New User" onClientClick="SaveUser(); return false;"/>
            </asp:Panel>
           
     </asp:Panel>
</asp:Content>
