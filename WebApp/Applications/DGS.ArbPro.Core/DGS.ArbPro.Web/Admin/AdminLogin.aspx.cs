﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web.Admin
{
    public partial class AdminLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
               
               if(((string)Session["AdminLogin"]) == "true" && !Page.IsPostBack)
               {
                   Response.Redirect("/Admin/Main.aspx");
               }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if(UserName.Text == "mark@arbprosoftware.com" && Password.Text == "new1")
            {
                Session["AdminLogin"] = "true";
                Response.Redirect("/Admin/Main.aspx");
                
            }
            FailureText.Text = "Login is not valid, try again.";

        }
    }
}