﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="DGS.ArbPro.Web.Admin.Main" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="Content" runat="server">
     <script type="text/javascript" src="../Content/js/User.js"></script>
        <asp:Panel runat="server" CssClass="center">
            <asp:Panel runat="server" ID="pnlUserMainForm" CssClass="userMainForm" Width="900px">
                <asp:Panel runat="server" CssClass="userMainTitle colored-raise">
                    Arb Pro Admin Screen 
                </asp:Panel>
                 <asp:Panel runat="server" ID="pnlDatabaseInfo" ClientIDMode="Static" CssClass="userMainInfo" Width="880px">
                      <asp:Panel runat="server" CssClass="float-left" Width="430px">
                        <asp:Panel runat="server" ID="pnlUserDatabaseInfo" ClientIDMode="Static">
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                        Total databases
                                </asp:Label>
                                <asp:Label runat="server"
                                    ID="txtNoOfDatabases" ClientIDMode="Static"
                                    CssClass="userInput"/>
                                </asp:Panel>
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                        Total Value &pound;
                                </asp:Label>
                                <asp:Label runat="server"
                                    ID="txtTotalValue" ClientIDMode="Static"
                                    CssClass="userInput"/>
                                </asp:Panel>
                        </asp:Panel>
                     <asp:Panel runat="server" CssClass="float-left" Width="430px">
                         <asp:Panel runat="server" ID="pnlUserDatabaseBtn" ClientIDMode="Static">
                            
                                 <asp:Button runat="server" ID="btnCreateDatabases" ClientIDMode="Static" CssClass="button-basic padded" Width="200px" Text="Create New Database" PostBackUrl="~/Admin/CreateDatabase.aspx"/>
                                
                        </asp:Panel>
                            
                         </asp:Panel>
                     </asp:Panel>
                     <asp:Panel runat="server" ID="Panel1" ClientIDMode="Static" CssClass="userMainInfo" Width="870px">
                           <asp:Panel runat="server" CssClass="userMainTitle colored-raise">
                                Database Users  
                         </asp:Panel> 
                          <asp:Panel runat="server" ID="grdOrg" ClientIDMode="Static" CssClass="width-full" style="max-height: 400px; overflow-y: scroll;">
                            <asp:Panel runat="server" CssClass="center width-full">
                                <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                          </asp:Panel> 
                     </asp:Panel>
                       </asp:Panel>
                                       
                 </asp:Panel>
           
        <script type="text/javascript" async>
            $(function () {
                loadOrganization();
                loadTotalNoOfDatabases();
                loadTotalClientsAmount();
            });
        </script>
    
</asp:Content>
