﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web.Admin
{
    public partial class NewUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["AdminLogin"] == null)
            {
                Response.Redirect("/Admin/AdminLogin.aspx");
            }
            hfOrgId.Value = Request["OrgId"];
            lnkReturnToClient.NavigateUrl = "~/Admin/ManageUser.aspx?OrgId=" + Request["OrgId"].ToString();
        }
    }
}