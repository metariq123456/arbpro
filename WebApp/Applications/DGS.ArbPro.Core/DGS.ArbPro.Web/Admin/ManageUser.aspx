﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageUser.aspx.cs" Inherits="DGS.ArbPro.Web.Admin.ManageUser" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="Content" runat="server">
    <script type="text/javascript" src="../Content/js/User.js"></script>
     <asp:Panel runat="server" CssClass="center">
            <asp:Panel runat="server" ID="pnlUserMainForm" CssClass="userMainForm" Width="900px">
                <asp:Panel runat="server" CssClass="userMainTitle colored-raise">
                    Manage User Information
                    <asp:HyperLink runat="server" ID="lnkReturnToClient" ClientIDMode="Static" CssClass="float-right">
                         Back
                    </asp:HyperLink> 
                </asp:Panel>
                 <asp:Panel runat="server" ID="pnlDatabaseInfo" ClientIDMode="Static" CssClass="userMainInfo" Width="880px">
                          <asp:Label runat="server" CssClass="userCompanyInfo bold">
                                        Add Company Information 
                          </asp:Label> 
                           
                          <asp:Panel runat="server" ID="pnlUserDatabaseInfo" ClientIDMode="Static">
                            
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                       Company Name 
                                </asp:Label>
                               <asp:TextBox ID="txtCompanyName" ClientIDMode="Static" runat="server" CssClass="userInputLarge"/>
                                 
                            </asp:Panel>
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                       Address 1
                                </asp:Label>
                               <asp:TextBox ID="txtAddress1" ClientIDMode="Static" runat="server" CssClass="userInputLarge"/>
                                 
                            </asp:Panel>
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                       Address 2
                                </asp:Label>
                               <asp:TextBox ID="txtAddress2" ClientIDMode="Static" runat="server" CssClass="userInputLarge"/>
                               
                            </asp:Panel>

                             <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                       Town
                                </asp:Label>
                               <asp:TextBox ID="txtTown" ClientIDMode="Static" runat="server" CssClass="userInputLarge"/>
                                
                             </asp:Panel>
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      County
                                </asp:Label>
                               <asp:TextBox ID="txtCounty" ClientIDMode="Static" runat="server" CssClass="userInputLarge"/>
                              
                            </asp:Panel>
                             <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Post code
                                </asp:Label>
                               <asp:TextBox ID="txtPostCode" ClientIDMode="Static" runat="server" CssClass="userInputLarge"/>
                                
                             </asp:Panel>
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Country 
                                </asp:Label>
                               <asp:TextBox ID="txtCountry" ClientIDMode="Static" runat="server" CssClass="userInputLarge"/>
                              
                            </asp:Panel>
                             <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                      Licenses 
                                </asp:Label>
                               <asp:TextBox ID="txtLicenses" ClientIDMode="Static" runat="server" CssClass="userInputLarge"/>
                             </asp:Panel>
                          </asp:Panel>
                </asp:Panel>
                 <asp:Panel runat="server" ID="pnlAdmin" ClientIDMode="Static" CssClass="userMainInfo" Width="880px">
                          <asp:Label runat="server" CssClass="userCompanyInfo bold">
                                        Admin Login
                          </asp:Label> 
                           
                          <asp:Panel runat="server" ID="pnlAdminUserInfo" ClientIDMode="Static">
                            
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                       Admin User Name 
                                </asp:Label>
                               <asp:TextBox ID="txtAdminUserName" ClientIDMode="Static" runat="server" CssClass="userInputLarge"/>
                                 
                            </asp:Panel>
                            <asp:Panel runat="server" CssClass="userField">
                                <asp:Label runat="server" CssClass="userLabel">
                                       Admin Password 
                                </asp:Label>
                               <asp:TextBox ID="txtAdminPassword" ClientIDMode="Static" runat="server" CssClass="userInputLarge"/>
                                 
                            </asp:Panel>
                              </asp:Panel>
                     </asp:Panel>
                     <asp:Panel runat="server" ID="pnlUserList" ClientIDMode="Static" CssClass="userMainInfo" Width="880px">
                         <asp:Label runat="server" CssClass="userCompanyInfo bold">
                                       Users
                         </asp:Label> 
                          <asp:Panel runat="server" CssClass="float-left" Width="425px">
                               
                                <asp:Panel runat="server" ID="grdUsers" ClientIDMode="Static" CssClass="width-full" style="max-height: 400px; overflow-y: scroll;">
                                <asp:Panel runat="server" CssClass="center width-full">
                                    <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                </asp:Panel>
                          </asp:Panel> 
                           
                          </asp:Panel>
                         <asp:Panel runat="server" CssClass="float-left" Width="425px">
                             <asp:Panel runat="server" ID="pnlUserAddBtn" ClientIDMode="Static">
                            
                                     <asp:Button runat="server" ID="btnAddUser" ClientIDMode="Static" CssClass="button-basic padded" Width="200px" Text="Click to add new users" OnClick="btnAddUser_Click" />
                                     <br />
                                     <asp:Button runat="server" ID="BtnBlockCompany" ClientIDMode="Static" CssClass="button-basic padded block" Width="200px" Text="Block database access" OnClientClick="DisableUsers(); return false;" />
                                     
                                     <asp:Button runat="server" ID="BtnAllUserUnBlock" ClientIDMode="Static" CssClass="button-basic padded unblock" Width="200px" Text="UnBlock database access" OnClientClick="EnableUsers(); return false;" />
                            </asp:Panel>
                            
                         </asp:Panel>
                     </asp:Panel>
                     </asp:Panel>
            </asp:Panel>
   
    <script>
        
        $(function () {
            $("input[type='text']").prop("disabled", true);
            loadAdminUser(<%= Request["OrgId"] %>);
            loadSingleOrganization(<%= Request["OrgId"] %>)
            loadSingleOrgUsers(<%= Request["OrgId"] %>)
            CheckAllUsersEnabledForOrg(<%= Request["OrgId"] %>);
        });

        function EnableUsers()
        {
            EnableAllUsersForOrg(<%= Request["OrgId"] %>);
        }

        function DisableUsers()
        {
            DisableAllUsersForOrg(<%= Request["OrgId"] %>);
        }
    </script>
</asp:Content>
