﻿<%@ Page Title="Book Work" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BookWork.aspx.cs" Inherits="DGS.ArbPro.Web.BookWork" %>

<asp:Content ID="HeadContent" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript" src="Content/js/WorkPlanBooking.js?V=3.23.5"></script>
</asp:Content>
<asp:Content runat="server" ID="TopContent" ContentPlaceHolderID="TopContent" CssClass="colored-raise">
    <asp:Panel ID="Panel1" runat="server" CssClass="width-full bordered-bottom padded">
        <asp:Button runat="server"
            ID="btnNavigateTojob" ClientIDMode="Static"
            Text="Return to Work Management"
            CssClass="button-thin float-right padded"
            OnClientClick="ClosePage(3); return false;" />
    </asp:Panel>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="Content" runat="server">
    <asp:HiddenField runat="server" ID="hfJobID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hfQuoteId" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hfWorkId" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hfReferrerSiteId" ClientIDMode="Static" />
    <!-- Left hand side panel containing all the tabs and details -->
    <asp:Panel runat="server" ID="pnlLeftContainer" ClientIDMode="Static" CssClass="bookWorkContentLeft">
        <!-- Summary container -->
        <asp:Panel runat="server" ID="pnlWorkSummary" ClientIDMode="Static" CssClass="bookWorkPanel white">
            <asp:Panel runat="server" ID="pnlSummaryHeader" CssClass="width-full colored-raise bookWorkPanelHeader" ClientIDMode="Static">
                <asp:Panel runat="server" ID="pnlWorkTabHeader" CssClass="width-full colored-raise bookWorkPanelHeader" ClientIDMode="Static">
                    <asp:Label runat="server" ID="lblWorkPlan" CssClass="bookWorkLabel float-left" ClientIDMode="Static">Work Planning</asp:Label>
                    <asp:Label runat="server" ID="lblQuoteNo" CssClass="bookWorkLabel float-right" ClientIDMode="Static" ForeColor="White">Pending</asp:Label>
                </asp:Panel>
                <!-- Tab button bar -->
                <asp:Panel runat="server" ID="pnlWorkDetailTabs" ClientIDMode="Static" CssClass="bookWorkTabBar width-full">
                    <asp:HyperLink runat="server" ID="tabSummary_0" ClientIDMode="Static" CssClass="bookWorkTab" NavigateUrl="javascript:SwitchSummaryPanel(0);">
                    Details
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="tabSummary_1" ClientIDMode="Static" CssClass="bookWorkTab" NavigateUrl="javascript:SwitchSummaryPanel(1);">
                    Notes
                    </asp:HyperLink>
                </asp:Panel>
                <!--<asp:Label runat="server" ID="Label1" CssClass="bookWorkLabel" ClientIDMode="Static">Work Summary</asp:Label>-->
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlSummary_0" ClientIDMode="Static" CssClass="float-left width-full white">
                <asp:Panel runat="server" ID="pnlSummaryDetail" ClientIDMode="Static" CssClass="float-left width-full white">
                    <asp:Table runat="server" ID="tblSummary" ClientIDMode="Static" CssClass="float-left width-full center">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" CssClass="width-full">Please Wait, Loading</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" CssClass="width-full">
                                <asp:Image ID="imgSummaryWait" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:Panel>
            <!--Depricated Section -->
            <%--<!-- Site / Team / Tools Tab container -->
        <asp:Panel runat="server" ID="pnlWorkDetails" ClientIDMode="Static" CssClass="bookWorkPanel white">
            <asp:Panel runat="server" ID="pnlWorkTabHeader" CssClass="width-full colored-raise bookWorkPanelHeader" ClientIDMode="Static">
                <asp:Label runat="server" ID="lblWorkPlan" CssClass="bookWorkLabel" ClientIDMode="Static">Work Planning</asp:Label>
            </asp:Panel>
            <!-- Tab button bar -->
            <asp:Panel runat="server" ID="pnlWorkDetailTabs" ClientIDMode="Static" CssClass="bookWorkTabBar width-full">
                <asp:HyperLink runat="server" ID="tabWork_4" ClientIDMode="Static" CssClass="bookWorkTab" NavigateUrl="javascript:SwitchWorkPanel(4);">
                    Info
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabWork_3" ClientIDMode="Static" CssClass="bookWorkTab activeTab" NavigateUrl="javascript:SwitchWorkPanel(3);">
                    Scheduled
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabWork_0" ClientIDMode="Static" CssClass="bookWorkTab" NavigateUrl="javascript:SwitchWorkPanel(0);">
                    Site
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabWork_1" ClientIDMode="Static" CssClass="bookWorkTab" NavigateUrl="javascript:SwitchWorkPanel(1);">
                    Team
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabWork_2" ClientIDMode="Static" CssClass="bookWorkTab" NavigateUrl="javascript:SwitchWorkPanel(2);">
                    Tools
                </asp:HyperLink>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlWorkDetailContent" ClientIDMode="Static" CssClass="float-left width-full">
                <!-- Info tab -->
                <!-- NOTE: This tab was added after the initial design and so is numbered out of sequence-->
                <asp:Panel runat="server" ID="pnlWorkDetail_4" ClientIDMode="Static" CssClass="hidden">
                </asp:Panel>
                <!-- Scheduled details tab -->
                <!-- NOTE: This tab was added after the initial design and so is numbered out of sequence-->
                <asp:Panel runat="server" ID="pnlWorkDetail_3" ClientIDMode="Static" CssClass="hidden">
                    <asp:Table runat="server" ID="tblWorkDetail_3" ClientIDMode="Static" CssClass="float-left width-full center">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" CssClass="width-full">Please Wait, Loading</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" CssClass="width-full">
                                <asp:Image ID="imgScheduledWork" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <!-- Site details tab -->
                <asp:Panel runat="server" ID="pnlWorkDetail_0" ClientIDMode="Static" CssClass="hidden">
                    <asp:Table runat="server" ID="tblWorkDetail_0" ClientIDMode="Static" CssClass="float-left width-full center">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" CssClass="width-full">Please Wait, Loading</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" CssClass="width-full">
                                <asp:Image ID="imgWorkDetail_0" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <!-- Team Details Tab -->
                <asp:Panel runat="server" ID="pnlWorkDetail_1" ClientIDMode="Static" CssClass="hidden">
                    <asp:Table runat="server" ID="tblWorkDetail_1" ClientIDMode="Static" CssClass="float-left width-full center">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" CssClass="width-full">Please Wait, Loading</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" CssClass="width-full">
                                <asp:Image ID="imgWorkDetail_1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <!-- Tools Details Tab -->
                <asp:Panel runat="server" ID="pnlWorkDetail_2" ClientIDMode="Static" CssClass="hidden">
                    <asp:Table runat="server" ID="tblWorkDetail_2" ClientIDMode="Static" CssClass="float-left width-full center">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" CssClass="width-full">Please Wait, Loading</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" CssClass="width-full">
                                <asp:Image ID="imgWorkDetail_2" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>--%>

            <!-- Quote notes container -->
            <asp:Panel runat="server" ID="pnlSummary_1" ClientIDMode="Static" CssClass="width-full white hidden">
                <asp:Panel runat="server" ID="pnlNotes" ClientIDMode="Static" CssClass="width-full white">
                    <asp:Panel runat="server" ID="pnlNotesTabBar" ClientIDMode="Static" CssClass="bookWorkTabBar width-full">
                        <asp:HyperLink runat="server" ID="tabNotes_0" ClientIDMode="Static" CssClass="bookWorkTab" NavigateUrl="javascript:SwitchNotesPanel(0);">
                    Quote
                        </asp:HyperLink>
                        <asp:HyperLink runat="server" ID="tabNotes_1" ClientIDMode="Static" CssClass="bookWorkTab" NavigateUrl="javascript:SwitchNotesPanel(1);">
                    Work
                        </asp:HyperLink>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlNotesDetail_0" ClientIDMode="Static" CssClass="float-left white">
                        <asp:Table runat="server" ID="tblNotesDetail_0" ClientIDMode="Static" CssClass="float-left width-full center">
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server" CssClass="width-full">Please Wait, Loading</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server" CssClass="width-full">
                                    <asp:Image ID="imgNoteWait_0" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlNotesDetail_1" ClientIDMode="Static" CssClass="float-left white hidden">
                        <asp:Table runat="server" ID="tblNotesDetail_1" ClientIDMode="Static" CssClass=" float-left width-full center">
                            <asp:TableRow runat="server">
                                <asp:TableCell ID="TableCell1" runat="server">Please Wait, Loading</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server" CssClass="width-full">
                                    <asp:Image ID="imgNoteWait_1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <!-- Right hand side panel containing the calendar -->
    <asp:Panel runat="server" ID="pnlRightContainer" ClientIDMode="Static" CssClass="bookWorkContentRight center .scroll-vertical">
        <!-- Panel where errors and user information will be displayed -->
        <asp:Panel runat="server" ID="pnlErrorWork" ClientIDMode="Static" CssClass="width-full">
        </asp:Panel>
        <!-- Calendar container -->
        <asp:Panel runat="server" ID="pnlCalendar" ClientIDMode="Static" CssClass="width-full">
        </asp:Panel>
    </asp:Panel>
    <script type="text/javascript" async>
        $(function () {
            WorkPlanBooking_Load();
        });
    </script>
</asp:Content>
