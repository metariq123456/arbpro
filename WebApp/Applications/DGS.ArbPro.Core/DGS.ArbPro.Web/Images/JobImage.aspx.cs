﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGS.ArbPro.BL;

namespace DGS.ArbPro.Web.Images
{
    using DGS.ArbPro.Types;

    using Microsoft.Ajax.Utilities;

    using PDFReport;

    public partial class JobImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Int32 Stage;
            Int32 ItemID;

            Int32.TryParse(Request["Stage"], out Stage);
            Int32.TryParse(Request["ItemID"], out ItemID);

            int JobItemID = -1;
            List<ImageItem> companyImage = new List<ImageItem>();
            Types.Company directorAndFooter = new Company();
            bool isCompanyImage = false;
            bool isDirector = false;
            bool isFooter = false;
            Byte[] ImageBytes = null;

            switch (Stage)
            {
                case 0:
                    JobItemID = ItemID;
                    break;
                case 1:
                    var qDB = new DAL.QuoteItem(AspSession.UserDatabase);
                    JobItemID = qDB.GetForId(ItemID).JobItemId;
                    break;
                case 2:
                    var wDB = new DAL.WorkItem(AspSession.UserDatabase);
                    JobItemID = wDB.GetForId(ItemID).JobItemId;
                    break;
                case 3:
                    var iDB = new DAL.InvoiceItem(AspSession.UserDatabase);
                    JobItemID = iDB.GetForId(ItemID).JobItemId;
                    break;
                case 4:
                    var eDB = new DAL.Company(AspSession.UserDatabase);
                    directorAndFooter = eDB.Get();
                    isFooter = true;
                    break;
                case 5:
                    var hDB = new DAL.ImageItem(AspSession.UserDatabase);
                    companyImage = hDB.GetForGroup(ItemID);
                    isCompanyImage = true;
                    break;
                case 6:
                    var fDB = new DAL.ImageItem(AspSession.UserDatabase);
                    companyImage = fDB.GetForGroup(ItemID);
                    isCompanyImage = true;
                    break;
                case 7:
                    var dDB = new DAL.Company(AspSession.UserDatabase);
                    directorAndFooter = dDB.Get();
                    isDirector = true;
                    break;
                default:
                    ImageBytes = null;
                    break;
            }

            if (isCompanyImage)
            {
                var cPics = companyImage;
                if (cPics.Count > 0)
                {
                    ImageBytes = cPics[0].GetImageBytes();
                }
            }
            else if (isDirector || isFooter)
            {
                var dirPics = directorAndFooter;
                if (isDirector)
                {
                    ImageBytes = dirPics.DirectorsSignature;
                }
                else
                {
                    ImageBytes = dirPics.Footer;
                }
            }
            else
            {
                var DB = new DAL.ItemPicture(AspSession.UserDatabase);
                var Pics = DB.GetForJobItem(JobItemID);

                if (Pics.Count > 0) ImageBytes = Pics[0].ImageBytes;
            }

            if (ImageBytes == null || ImageBytes.Length == 0)
            {
                ImageBytes = File.ReadAllBytes(AppDomain.CurrentDomain.BaseDirectory + @"Images\img_NotAvailable.png");
            }


            var memStream = new MemoryStream(ImageBytes);
            var Image = System.Drawing.Image.FromStream(memStream);
            ImageCodecInfo FileCodec = null;
            Guid Guid = Image.RawFormat.Guid;
            ImageFormat ImageFormat = ImageFormat.Png;

            foreach (var Codec in System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders())
            {
                if (Codec.FormatID == Guid) FileCodec = Codec;
            }

            try
            {
                if (Response.OutputStream.CanWrite)
                {
                    Response.Clear();
                    Response.ContentType = FileCodec.MimeType;
                    memStream.WriteTo(Response.OutputStream);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                memStream.Close();
            }

        }
    }
}