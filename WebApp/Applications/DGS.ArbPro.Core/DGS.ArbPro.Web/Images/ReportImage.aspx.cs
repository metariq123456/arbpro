﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DGS.ArbPro.DAL;
using DGS.ArbPro.BL;
using DGS.ArbPro.BL.WorkFlowHandlers;

namespace DGS.ArbPro.Web.Images
{
    public partial class ReportImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int RecId = -1;
            //Try to pass the id parameter
            if (Request["Id"] != null)
            {
                int.TryParse(Request["Id"], out RecId);
            }
            //SageTypeEnum ExportType, ExportTypeEnum ObjectType
            int intSageType = -1;
            int intExportType = -1;

            //Try to pass the content parameters
            if (Request["SageType"] != null && Request["ExportType"] != null)
            {
                int.TryParse(Request["SageType"], out intSageType);
                int.TryParse(Request["ExportType"], out intExportType);

                if (intSageType >= 0 && intExportType >= 0)
                    LoadExport((Types.Enums.SageEnums.SageTypeEnum)intSageType, (Types.Enums.SageEnums.ExportTypeEnum)intExportType);

            }
            else
            {
                LoadHistory(RecId);
            }

        }

        private void LoadHistory(int id)
        {
            Types.DocumentHistory Item;
            if(id >0)
            {
                Item = QuoteHandler.GetHistoryRec(AspSession.UserDatabase, id);
                if (Item.Doc != null)
                {
                    Response.ContentType = "application/pdf";
                    Item.Doc.CopyTo(Response.OutputStream);
                    Response.Flush();
                    Response.Close();
                }
                else
                {
                    Response.Write("No document found to view");
                    Response.Flush();
                    Response.Close();
                }
            }
            else
            {
                Response.Write("No document Id supplied.");
                Response.Flush();
                Response.Close();
            }
        }

        private void LoadExport(Types.Enums.SageEnums.SageTypeEnum sageType, Types.Enums.SageEnums.ExportTypeEnum exportType)
        {
            var data = BL.ExportHandlers.SageExportHandler.Export(sageType, exportType, DateTime.MinValue, DateTime.Today, AspSession.UserDatabase); //
           
            StreamWriter sw = null;
            try
            {
                Response.ContentType = "text/plain";
                Response.AddHeader("content-disposition", "attachment;filename=" + string.Format("SageExport-{0}.csv", string.Format("{0:ddMMyyyy}", DateTime.Today)));
                Response.Clear();
                sw = new StreamWriter(Response.OutputStream, System.Text.Encoding.UTF8);
                data.GenerateFile(ref sw);
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.ErrorFormat("({0}:{1}) Sage Eport Error {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, ex);
                Response.Write("No document found to view");
                Response.Flush();
                Response.Close();
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                    sw = null;
                }
            }
            Response.Close();
        }
    }
}