﻿<%@ Page Title="Work Management" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Job.aspx.cs" Inherits="DGS.ArbPro.Web.Job" %>

<asp:Content runat="server" ID="TopContent" ContentPlaceHolderID="TopContent">
    
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <%--<asp:Panel runat="server" CssClass="scroll">--%>
    <asp:HiddenField runat="server" ID="hfClientID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hfJobID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hfQuoteId" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hfInvoiceId" ClientIDMode="Static" />
    <!--<script type="text/javascript" src="Content/js/Job.js"></script>-->
    <script type="text/javascript" src="Content/js/WorkManagement.js?V=3.23.5"></script>
    <script type="text/javascript" src="Content/js/Quote.js?V=3.23.5"></script>
    <script type="text/javascript" src="Content/js/Work.js?V=3.23.5"></script>
    <script type="text/javascript" src="Content/js/Invoice.js?V=3.23.5"></script>

    <div id='winQuoteItems'>
        <div>Edit Items</div>
        <div id='Content'>
            <div id='tblQuoteItemsAdd' class="hidden"></div>
            <div id='tblQuoteItems'></div>
            <div style='float: right; margin-top: 15px;'>
                <input type='button' id='btnWinQuoteItemsOk' value='OK' />
                <input type='button' id='btnWinQuoteItemsCancel' value='Cancel' />
            </div>
        </div>
    </div>

    <asp:Panel runat="server" ID="pnlJobsList" ClientIDMode="Static" CssClass="jobsList ">
        <asp:Panel runat="server"
            ID="pnlJobsListFilter" ClientIDMode="Static"
            CssClass="jobsList top">
            <asp:Label runat="server"
                ID="lblClientName" ClientIDMode="Static"
                CssClass="width-full">
                    Client Name
            </asp:Label>
            <br />
            <asp:HyperLink runat="server"
                ID="lnkReturnToClient"
                ClientIDMode="Static"
                CssClass="float-right">
                    Return to Client
            </asp:HyperLink>
        </asp:Panel>
        <asp:Panel runat="server"
            CssClass="jobsList drag" onclick="ToggleList();">
        </asp:Panel>
        <asp:Panel runat="server"
            ID="pnlJobsListList"
            ClientIDMode="Static"
            CssClass="jobsList list">
        </asp:Panel>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlEdit" ClientIDMode="Static" CssClass="jobContent">
        <asp:Panel ID="Panel6" runat="server" CssClass="width-full bordered-bottom white" Height="28px">
        <asp:Button runat="server"
            ID="btnNewJob" ClientIDMode="Static"
            Text="New Job"
            CssClass="button-thin float-right padded"
            OnClientClick="NewJob(); return false;" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlEditTop" CssClass="width-full">
            <asp:Panel runat="server" ID="pnlJobInfo" CssClass="jobInfo ap-draggable float-left" Width="40%">
            <asp:Panel runat="server" CssClass="colored-raise jobHeader">
                <asp:Label runat="server">
                    Sites
                </asp:Label>
            </asp:Panel>
            <asp:Panel runat="server" CssClass="white-light">
                <asp:Label ID="Label1" runat="server" CssClass="padded">
                    Click on the site name to edit the details
                </asp:Label>
            </asp:Panel>
            <asp:Panel runat="server" ID="lstJobSites" ClientIDMode="Static" Style="max-height: 200px; overflow-y: auto;"></asp:Panel>
            <asp:Panel runat="server">
                <asp:Button runat="server" ID="btnAddJobSiteNew" ClientIDMode="Static" CssClass="button-thin float-right padded" Text="Add Site To Job" OnClientClick="AddSite(); return false;" />
            </asp:Panel>
        </asp:Panel>
            <asp:Panel ID="pnlClientInfo" runat="server" CssClass="jobInfo ap-draggable float-left" Width="40%">
                <asp:Panel runat="server" CssClass="colored-raise jobHeader">
                    <asp:Label runat="server">
                        Client Information
                    </asp:Label>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Email:
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtJobClientEmail" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
                <br/>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Telephone:
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtJobClientTelephone" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
                <br/>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Mobile:
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtJobClientMobile" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlJobActionPlatform" ClientIDMode="Static" CssClass="jobInfo ap-draggable  float-left">
            <asp:Panel runat="server" CssClass="colored-raise jobHeader">
                <asp:Label runat="server">
                    Action Platform
                </asp:Label>
            </asp:Panel>
            <asp:Button runat="server" ID="btnBroken" CssClass="button-basic padded float-left" Text="Risk Assessment" Width="140px" OnClientClick="OpenRiskAssessment(); return false;" />
            <%--<asp:Button runat="server" CssClass="button-basic padded" Text="Risk Assessment" Width="160px" OnClientClick="window.location = window.location.hostname + '/RiskTemplate.aspx';"/>--%>
            <asp:Button runat="server" CssClass="button-basic padded float-left" Text="Tree Protection" Width="140px" OnClientClick="OpenTreeProtection(); return false;" />
            <asp:Button runat="server" CssClass="button-basic padded float-left" Text="Permits Necessary" Width="140px" OnClientClick="OpenPermitsNecessery(); return false;" />
            <asp:Button runat="server" CssClass="button-basic padded float-left" Text="Print Schedule" Width="140px" OnClientClick="WorkSchedulePrint(); return false;" />
            <asp:Button runat="server" CssClass="button-basic padded float-left" Text="Edit Tools" Width="140px" OnClientClick="EditTools(); return false;" />
            <asp:Button ID="btnClientMarketing" runat="server" CssClass="button-basic padded float-left" Text="Marketing" Width="140px" OnClientClick="ClientMarketing(); return false;" />
            <%--<asp:Button runat="server" CssClass="button-basic padded float-left" Text="Return Time Mgmt" Width="140px" OnClientClick="return false;" Enabled="false" />--%>
        </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" CssClass="float-clear width-full">
            <asp:Panel runat="server" ID="pnlJobDetails" CssClass="jobTabs align-left ap-draggable" Style="margin-bottom: 50px;">
            <asp:Panel ID="pnlJobWorkflow" runat="server" CssClass="colored-raise jobHeader">
                <asp:Label ID="lblJobWorkflow" runat="server">
                    Job Workflow
                </asp:Label>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlJobDetailsTabs" CssClass="jobTabBar">
                <asp:HyperLink runat="server" ID="tabWorkStage_0" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchJobStageTab(0);">
                    Enquiry
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabWorkStage_1" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchJobStageTab(1);">
                    Quote
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabWorkStage_2" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchJobStageTab(2);">
                    Work
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabWorkStage_3" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchJobStageTab(3);">
                    Invoice
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabWorkStage_5" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchJobStageTab(5);">
                    Credit Note
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabWorkStage_4" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchJobStageTab(4);">
                    History
                </asp:HyperLink>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlJobDetailsContent" CssClass="width-full">
                <asp:Panel runat="server" ID="pnlWorkStage_0" ClientIDMode="Static" CssClass="hidden">
                    <asp:Panel runat="server" ID="pnlEnquiryDetailsWait" ClientIDMode="Static" CssClass="float-left width-full">
                        <asp:Panel ID="Panel4" runat="server" CssClass="center width-full">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlEnquiryDetails" ClientIDMode="Static" CssClass="float-left width-full hidden">
                        <%--<asp:Panel runat="server" ID="pnlEnqDetailsWrapper" ClientIDMode="Static" CssClass="float-left width-full">
                            <asp:Panel ID="Panel7" runat="server" CssClass="width-full white-light">
                                    <asp:Button ID="btnEditJob" runat="server" CssClass="button-thin padded float-right" Text="Edit Enquiry" OnClientClick="OpenJobEditDialog(); return false;" />
                            </asp:Panel>
                            <asp:Panel ID="Panel8" runat="server" CssClass="jobEditField">
                                <asp:Label runat="server" CssClass="jobEditLabel" >
                            Job Status
                                </asp:Label>
                                <asp:Label runat="server"
                                    ID="lblJobEnquiryStatus"
                                    ClientIDMode="Static"
                                    Font-Bold="true"
                                    CssClass="jobEditInput"></asp:Label>
                            </asp:Panel>
                            <asp:Panel ID="Panel10" runat="server" CssClass="jobEditField">
                                <asp:Label ID="Label2" runat="server" CssClass="jobEditLabel" >
                            Dashboard
                                </asp:Label>
                                <asp:Label runat="server"
                                    ID="lblJobEnquiryShowInDb"
                                    ClientIDMode="Static"
                                    Font-Bold="true"
                                    CssClass="jobEditInput"></asp:Label>
                            </asp:Panel>
                            <asp:Panel runat="server" CssClass="jobEditField">
                                <asp:Label runat="server" CssClass="jobEditLabel" >
                            Source
                                </asp:Label>
                                <asp:Label runat="server"
                                    ID="lblJobEnquirySource"
                                    ClientIDMode="Static"
                                    Font-Bold="true"
                                    CssClass="jobEditInput"></asp:Label>
                            </asp:Panel>
                            <asp:Panel runat="server" CssClass="jobEditField">
                                <asp:Label runat="server" CssClass="jobEditLabel">
                            Reporter
                                </asp:Label>
                                <asp:Label runat="server"
                                    CssClass="jobEditInput" Font-Bold="true" ID="lblJobEnquiryStaff" ClientIDMode="Static">
                                </asp:Label>
                            </asp:Panel>
                            <asp:Panel ID="Panel9" runat="server" CssClass="jobEditField">
                                <asp:Label  runat="server"
                                    CssClass="jobEditLabel">
                            Default Surveyor
                                </asp:Label>
                                <asp:Label runat="server"
                                    ID="lblJobEnquiryDefaultSurveyor"
                                    ClientIDMode="Static"
                                    CssClass="jobEditInput"></asp:Label>
                            </asp:Panel>
                            <asp:Panel runat="server" CssClass="jobEditField">
                                <asp:Label runat="server" CssClass="jobEditLabel">
                            Comments
                                </asp:Label>
                                <asp:Label runat="server" CssClass="jobEditInput" ID="lblJobEnquiryComments"
                                    ClientIDMode="Static" Font-Bold="true">
                                </asp:Label>
                            </asp:Panel>--%>
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="float-left width-full bordered-bottom">
                            <asp:Panel runat="server" CssClass="jobEditField">
                                <asp:Label runat="server" CssClass="jobEditLabel bold">
                            Upcoming Appointments
                                </asp:Label>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="float-left width-full"
                            Style="overflow-x: scroll; overflow-y: hidden;">
                            <asp:Panel runat="server" ID="pnlJobEnquiryAppointments"
                                ClientIDMode="Static" Style="display: inline-flex;">
                                <asp:Panel runat="server" CssClass="center width-full">
                                    <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="float-left width-full">
                            <asp:Button runat="server" CssClass="button-thin float-right padded"
                                Text="New Appointment" OnClientClick="NewAppointment(); return false;" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlWorkStage_1" ClientIDMode="Static" CssClass="hidden">
                    <asp:Panel runat="server" Height="25px">
                        <asp:DropDownList runat="server" ID="ddlQuoteID"
                            ClientIDMode="Static" CssClass="clientInput float-right"
                            onchange="SelectedQuoteChanged();" Width="120px">
                            <asp:ListItem Value="-1">New</asp:ListItem>
                        </asp:DropDownList>
                        <asp:Label runat="server" CssClass="clientLabel float-right">Quote #: </asp:Label>
                        <asp:HiddenField runat="server" ID="hfQuoteStatus" ClientIDMode="Static" />
                        <asp:Panel ID="Panel1" runat="server" CssClass="companyTabBar">
                            <asp:HyperLink runat="server" ID="tabQuoteDetails" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Quote_curTab = 1; SwitchQuoteTab();">
                    Details
                            </asp:HyperLink>
                            <asp:HyperLink runat="server" ID="tabQuoteNotes" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Quote_curTab = 2; SwitchQuoteTab();">
                    Notes
                            </asp:HyperLink>
                            <asp:HyperLink runat="server" ID="tabQuoteDocuments" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:Quote_curTab = 3; SwitchQuoteTab();">
                    Assoc Docs
                            </asp:HyperLink>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlQuotesDescriptionContainer" ClientIDMode="Static" CssClass="width-full hidden">
                        <asp:Panel runat="server" ID="grdQuotes" ClientIDMode="Static" CssClass="width-full">
                            <asp:Panel runat="server" CssClass="center width-full">
                                <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                        <hr style="margin-bottom: 2px;" />
                        <asp:Panel runat="server" ID="pnlQuoteControls" CssClass="width-full" ClientIDMode="Static" Height="35px">
                            <asp:Button ID="btnRejectQuote" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-right" Text="Mark Quote Rejected" OnClientClick="ChangeQuoteStatus(4); return false;" />
                            <asp:Button ID="btnDuplicate" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-right" Text="Re-Quote" OnClientClick="Requote(); return false;" />
                            <asp:Button ID="btnAcceptQuote" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-right" Text="Mark Quote Accepted" OnClientClick="ChangeQuoteStatus(3); return false;" />
                            <asp:Button ID="btnResendQuote" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-left" Text="Re-Send Quote" OnClientClick="ResendItems(); return false;" />
                            <asp:Button ID="btnFollowSales" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-left" Text="Follow-Up Sales" OnClientClick="FollowUpSales(); return false;" />
                            <asp:Button ID="btnProduceQuote" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-right" Text="Produce Quote" Width="160px" OnClientClick="ShowProduceQuoteDialog(); return false;" />
                            <asp:Button ID="btnDeletePendingQuote" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-right" Text="Delete Quote" Width="160px" OnClientClick="DeletePendingQuote(); return false;" />
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="width-full" ID="pnlQuoteHistory" ClientIDMode="Static">
                            <asp:Panel runat="server" CssClass="center width-full">
                                <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlQuotesNotesContainer" ClientIDMode="Static" CssClass="width-full hidden">
                        <asp:Panel runat="server" ID="pnlNotesGridContainer" CssClass="width-full">
                            <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                     <asp:Panel runat="server" ID="pnlQuotesDocumentContainer" ClientIDMode="Static" CssClass="width-full hidden">
                         <asp:Panel runat="server" ID="pnlDocumentsGridContainer" CssClass="width-full">
                            <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                     </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlWorkStage_2" ClientIDMode="Static" CssClass="hidden">
                    <asp:Panel ID="pnlWorkTabs" runat="server" Height="25px">
                        <asp:Panel ID="Panel5" runat="server" CssClass="companyTabBar">
                            <asp:HyperLink runat="server" ID="tabWorkDetail_0" ClientIDMode="Static" CssClass="workTab" NavigateUrl="javascript:SwitchWorkTab(0);">
                            Pending Items
                            </asp:HyperLink>
                            <asp:HyperLink runat="server" ID="tabWorkDetail_2" ClientIDMode="Static" CssClass="workTab" NavigateUrl="javascript:SwitchWorkTab(2);">
                            Cancelled Items
                            </asp:HyperLink>
                            <asp:HyperLink runat="server" ID="tabWorkDetail_1" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:SwitchWorkTab(1);">
                            Notes
                            </asp:HyperLink>
                            <asp:Label ID="lblWorkQuoteNo" runat="server" CssClass="clientLabel white float-right"  Width="100px" ClientIDMode="Static">Pending</asp:Label>
                            <asp:Label ID="lblForWorkQuoteNo" runat="server" CssClass="clientLabel float-right" ClientIDMode="Static">Work for Quote #: </asp:Label>
                            <!--<asp:DropDownList runat="server" ID="DropDownList1"
                            ClientIDMode="Static" CssClass="clientInput float-right"
                            onchange="SelectedWorkChanged();" Width="100px">
                        </asp:DropDownList>-->
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlWorkDetail_0" ClientIDMode="Static" CssClass="width-full hidden">
                        <asp:Panel runat="server" ID="pnlWorkSiteItems" ClientIDMode="Static" CssClass="width-full">
                            <asp:Panel runat="server" CssClass="center width-full">
                                <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                        <hr style="margin-bottom: 2px;" />
                        <asp:Panel ID="pnlWorkControls" runat="server" CssClass="width-full">
                            <asp:Button ID="btnCancelQuote" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-left"
                                Text="Cancel Quote/Work" Width="160px" OnClientClick="CancelQuote(); return false;" />
                            <asp:Button ID="btnMarkCompleted" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-right"
                                Text="Mark Completed" Width="160px" OnClientClick="MarkWorkCompleted(); return false;" />
                            <%--<asp:Button ID="btnProduceWorkPacks" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-right"
                                Text="Produce Work Packs" Width="160px" OnClientClick="SendWorkPack(); return false;" />--%>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlWorkDetail_2" ClientIDMode="Static" CssClass="width-full hidden">
                        <asp:Panel runat="server" ID="pnlCancelledWorkSiteItems" ClientIDMode="Static" CssClass="width-full">
                            <asp:Panel runat="server" CssClass="center width-full">
                                <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlWorkDetail_1" ClientIDMode="Static" CssClass="width-full hidden">
                        <asp:Panel ID="pnlWorkDetail_1Image" runat="server" CssClass="center width-full">
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlWorkStage_3" ClientIDMode="Static" CssClass="hidden">
                    <%--<asp:Panel runat="server" Height="25px">
                    <asp:DropDownList runat="server" ID="ddlInvoiceID"
                        ClientIDMode="Static" CssClass="clientInput float-right"
                        onchange="UpdateJobInvoiceItemList();" Width="100px">
                        <asp:ListItem Value="-1">New</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label runat="server" CssClass="clientLabel float-right">Invoice #: </asp:Label>--%>
                    <asp:Panel ID="pnlInvoiceControls" runat="server" Height="25px">
                            <asp:Label ID="lblInvQuoteNo" runat="server" CssClass="clientLabel white float-right"  Width="100px" ClientIDMode="Static">Pending</asp:Label>
                            <asp:Label ID="lblInvQuoteNoHeader" runat="server" CssClass="clientLabel float-right" ClientIDMode="Static">Invoices for Quote #: </asp:Label>
                        <asp:HiddenField runat="server" ID="hfInvoiceQuoteId" ClientIDMode="Static" />
                        <asp:Panel ID="pnlInvoiceTabBar" runat="server" CssClass="companyTabBar">
                            <asp:HyperLink runat="server" ID="tabInvoiceControls_0" ClientIDMode="Static" CssClass="jobTab activeTab" NavigateUrl="javascript:SwitchInvoiceTab(0);">
                    Summary
                            </asp:HyperLink>
                            <asp:HyperLink runat="server" ID="tabInvoiceControls_1" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchInvoiceTab(1);">
                    Notes
                            </asp:HyperLink>
                            <asp:HyperLink runat="server" ID="tabInvoiceControls_2" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchInvoiceTab(2);">
                    Assoc Docs
                            </asp:HyperLink>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="tabInvoiceDetails_0" ClientIDMode="Static" CssClass="">
                        <asp:Panel ID="pnlInvoiceLoad1" runat="server" CssClass="center width-full">
                            <asp:Image ID="imgInvoiceLoad1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="tabInvoiceDetails_1" ClientIDMode="Static" CssClass="hidden">
                        <asp:Panel ID="pnlInvoiceLoad4" runat="server" CssClass="center width-full">
                            <asp:Image ID="imgInvoiceLoad4" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="tabInvoiceDetails_2" ClientIDMode="Static" CssClass="hidden">
                        <asp:Panel ID="Panel3" runat="server" CssClass="center width-full">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlWorkStage_5" ClientIDMode="Static" CssClass="hidden">
                    <!-- This is the credit notes tab: COMING SOON -->
                    <asp:Panel ID="Panel2" runat="server" CssClass="center width-full">
                        <asp:Label runat="server" CssClass="width-full center">Coming Soon</asp:Label>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlWorkStage_4" ClientIDMode="Static" CssClass="hidden">
                    <asp:Panel runat="server" ID="grdHistory" ClientIDMode="Static" CssClass="width-full">
                        <asp:Panel runat="server" CssClass="center width-full">
                            <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
        </asp:Panel>
    <%--</asp:Panel>--%>

    <script type="text/javascript" async>
        $(function () {
            WorkManagement_Load();
        });
    </script>
    <%--</asp:Panel>--%>
</asp:Content>
