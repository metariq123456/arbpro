﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class CompanyContactInfo : System.Web.UI.Page
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            Types.Company Company = null;

            var DB = new DAL.Company   (AspSession.CurrentUser.ClientDatabaseName);

            Company = DB.Get(false);

            txtCompanyName.Text = Company.Name;
            txtAddress1.Text = Company.Address1;
            txtAddress2.Text = Company.Address2;
            txtCounty.Text = Company.County;
            txtEmail.Text = Company.AccountsEmail;
            txtPostcode.Text = Company.PostCode;
            txtTelephone.Text = Company.Telephone;
            txtTown.Text = Company.Town;
            txtWebsite.Text = Company.WebSite;
        }
    }
}