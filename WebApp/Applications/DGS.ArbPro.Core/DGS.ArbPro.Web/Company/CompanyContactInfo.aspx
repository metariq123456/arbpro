﻿<%@ Page Title="Company Contact Information" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CompanyContactInfo.aspx.cs" Inherits="DGS.ArbPro.Web.CompanyContactInfo" %>

<asp:Content runat="server" ID="TopContent" ContentPlaceHolderID="TopContent">
    <script type="text/javascript" src="Content/js/CompanyContactInfo.js?V=3.23.5"></script>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <asp:Panel runat="server" CssClass="companyForm" Width="450px">
        <asp:Panel runat="server" CssClass="companyFormTitle colored-raise">
            Company Contact Details
        </asp:Panel>
        <asp:Panel runat="server" CssClass="companyField">
            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="companyInput" placeholder="Company Name" Width="96%"
                onfocus="Watcher.watchForChange(this);"
                onblur="Watcher.cancelWatchForChange();"
                onchange="Watcher.changed(this)">
            </asp:TextBox>
        </asp:Panel>
        <asp:Panel runat="server" CssClass="companyField">
            <asp:TextBox ID="txtAddress1" runat="server" CssClass="companyInput" placeholder="Address Line 1" Width="96%"></asp:TextBox>
            <asp:TextBox ID="txtAddress2" runat="server" CssClass="companyInput" placeholder="Address Line 2" Width="96%"></asp:TextBox>
            <asp:TextBox ID="txtTown" runat="server" CssClass="companyInput" placeholder="Town" Width="96%"></asp:TextBox>
            <asp:TextBox ID="txtCounty" runat="server" CssClass="companyInput" placeholder="County" Width="96%"></asp:TextBox>
            <asp:TextBox ID="txtPostcode" runat="server" CssClass="companyInput" placeholder="Post Code" Width="96%"></asp:TextBox>
        </asp:Panel>
        <asp:Panel runat="server" CssClass="companyField">
            <asp:TextBox ID="txtTelephone" runat="server" CssClass="companyInput" placeholder="Telephone No" Width="96%"></asp:TextBox>
        </asp:Panel>
        <asp:Panel runat="server" CssClass="companyField">
            <asp:TextBox ID="txtWebsite" runat="server" CssClass="companyInput" placeholder="Website" Width="96%"></asp:TextBox>
            <asp:TextBox ID="txtEmail" runat="server" CssClass="companyInput" placeholder="Email Address" Width="96%"></asp:TextBox>
        </asp:Panel>

        <asp:Panel ID="Panel1" runat="server">
            <asp:Button runat="server"
                ID="btnSave" ClientIDMode="Static"
                CssClass="button-basic float-right padded"
                Text="Save"
                OnClientClick="Save(); return false;" />
        </asp:Panel>
    </asp:Panel>


</asp:Content>
