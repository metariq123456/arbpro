﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class CompanyInfo : System.Web.UI.Page
    {
        public String AppRoot
        {
            get
            {
                return Request.Url.Scheme + "://" + Request.Url.Host + (Request.Url.Port != 80 ? ":" + Request.Url.Port.ToString() : "");
            }
        }

        private Int32 curClientID = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            Types.Client Client = null;

            var DB = new DAL.Client(AspSession.CurrentUser.ClientDatabaseName);

            if (Request["ClientID"] != null)
            {
                curClientID = Int32.Parse(Request["ClientID"]);
            }
            else
            {
                var SearchDB = new DAL.ClientSearchResult(AspSession.CurrentUser.ClientDatabaseName);
                curClientID = SearchDB.GetForNameLike("")[0].Id;
            }

            Client = DB.GetForClientId(curClientID);

            switch (Client.AccountType)
            {
                case Types.Enums.ClientTypeEnum.Commercial:
                    if (!IsPostBack)
                        ddlClientType.SelectedIndex = 1;
                    pnlClientCommercial.Visible = true;
                    pnlClientResidential.Visible = false;
                    break;
                case Types.Enums.ClientTypeEnum.Residential:
                    if (!IsPostBack)
                        ddlClientType.SelectedIndex = 0;
                    pnlClientCommercial.Visible = false;
                    pnlClientResidential.Visible = true;
                    break;
            }

            hfClientID.Value = Client.Id.ToString();
            hfClientType.Value = ((Int32)Client.AccountType).ToString();

            txtClientTitle.Text = Client.Title;
            txtClientForename.Text = Client.Forename;
            txtClientSurname.Text = Client.Surname;
            txtClientBusinessName.Text = Client.BusinessName;
            txtClientWebsite.Text = Client.WebAddress;
            //TODO: Address Type
            txtClientAlternateAccountNo.Text = Client.AltAccountNo;
            txtClientEmail.Text = Client.Email;
            txtClientTelephone.Text = Client.Telephone;
            txtClientMobile.Text = Client.Mobile;
            txtClientHouseNameOrNumber.Text = Client.HouseNameNumber;
            txtClientAddress1.Text = Client.Address1;
            txtClientAddress2.Text = Client.Address2;
            txtClientTown.Text = Client.Town;
            txtClientCounty.Text = Client.County;
            txtClientPostCode.Text = Client.PostCode;

            btnDeleteClient.Click += btnDeleteClient_Click;
        }

        protected void ddlClientType_SelectedIndexChanged1(object sender, EventArgs e)
        {
            var DB = new DAL.Client(AspSession.CurrentUser.ClientDatabaseName);
            var Client = DB.GetForClientId(curClientID);
            switch (ddlClientType.SelectedIndex)
            {
                case 0:
                    Client.AccountType = Types.Enums.ClientTypeEnum.Residential;
                    DB.Save(Client);
                    break;
                case 1:
                    Client.AccountType = Types.Enums.ClientTypeEnum.Commercial;
                    DB.Save(Client);
                    break;
            }

            switch (Client.AccountType)
            {
                case Types.Enums.ClientTypeEnum.Commercial:
                    pnlClientCommercial.Visible = true;
                    pnlClientResidential.Visible = false;
                    break;
                case Types.Enums.ClientTypeEnum.Residential:
                    pnlClientCommercial.Visible = false;
                    pnlClientResidential.Visible = true;
                    break;
            }
        }

        void btnDeleteClient_Click(object sender, EventArgs e)
        {
            DAL.Client DB = new DAL.Client(AspSession.CurrentUser.ClientDatabaseName);
            Types.Client Client = DB.GetForClientId(curClientID);

            //TODO
        }
    }
}