﻿<%@ Page Title="Settings" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="DGS.ArbPro.Web.Settings" %>

<asp:Content runat="server" ID="TopContent" ContentPlaceHolderID="TopContent">
    <script type="text/javascript" src="Content/js/Settings.js?V=3.23.5"></script>
</asp:Content>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <asp:Panel ID="Panel1" runat="server" CssClass="width-full height-full float-clear">
        <asp:Panel ID="content" runat="server" CssClass="center">
            <asp:Panel runat="server" ID="pnlSettingsTypeButtons" CssClass="enquiryButtons">
                <table>
                    <tr>
                        <td style="vertical-align: top;">
                            <asp:Label ID="lblSettings" runat="server" CssClass="enquiryHeader">Settings</asp:Label>
                            <br />
                            <asp:Button runat="server" ID="btnTool" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Tools" OnClientClick="OpenToolsDialog('Tools');return false;" />
                            <asp:Button runat="server" ID="btnTreesNames" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Tree Names" OnClientClick="OpenTreesDialog('Tree Names');return false;" />
                            <asp:Button runat="server" ID="btnWorkDescription" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Work Description" OnClientClick="OpenWorkDialog('Work Descriptions');return false;" />

<%--                            <asp:Button runat="server" ID="btnNewDialog" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Work Description" OnClientClick="OpenANewkDialog('Work Descriptions');alert('Client side dialog activation');return false;" />--%>
                            <asp:Button runat="server" ID="btnCurrency" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Currency" OnClientClick="OpenCurrencyDialog('Currency');return false;" />
                            <br />
                            <asp:Label ID="lblItemTypes" runat="server" CssClass="enquiryHeader">Item Types</asp:Label>
                            <br />
                            <asp:Button runat="server" ID="btnTitle" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Titles" OnClientClick="OpenEnumDialog('Titles', '0');return false;" />
                            <asp:Button runat="server" ID="btnTown" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Towns" OnClientClick="OpenEnumDialog('Towns', '1');return false;" />
                            <asp:Button runat="server" ID="btnCounty" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Counties" OnClientClick="OpenEnumDialog('Counties','2');return false;" />
                            <asp:Button runat="server" ID="btnCouncil" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Council" OnClientClick="OpenEnumDialog('Councils','3');return false;" />
                            <br />
                            <asp:Button runat="server" ID="btnWasteTypes" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Waste types" OnClientClick="OpenEnumDialog('Waste Types','4');return false;" />
                            <asp:Button runat="server" ID="btnStaffTypes" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Staff types" OnClientClick="OpenEnumDialog('Staff Types','5');return false;" />
                            <br />
                            <asp:Button runat="server" ID="btnLeadSource" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Lead Source" OnClientClick="OpenEnumDialog('Lead Source','6');return false;" />
                            <asp:Button runat="server" ID="btnComplaintTypes" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Complaint Types" OnClientClick="OpenEnumDialog('Complaint Types','8');return false;" />
                            <br />
                            <asp:Button runat="server" ID="btnPermitType" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Permit Type" OnClientClick="OpenEnumDialog('Permit Type','9');return false;" />
                            <br />
                            <asp:Label ID="Label4" runat="server" CssClass="enquiryHeader">Risk Assessment Templates</asp:Label>
                            <br />
                            <%--<asp:Button runat="server" ID="btnRiskTemplate" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Default Template" OnClientClick="window.location='/RiskTemplate.aspx';return false;" />--%>
                            <asp:Button runat="server" ID="btnRiskTempl" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Templates" OnClientClick="window.location='/RiskTemplateEdit.aspx';return false;" />
                            <asp:Button runat="server" ID="btnRiskSection" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Sections" OnClientClick="window.location='/RiskSection.aspx';return false;" />
                            <asp:Button runat="server" ID="btnRiskCriteria" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Criteria" OnClientClick="window.location='/RiskCriteria.aspx';return false;" />
                            <asp:Button runat="server" ID="btnHospitals" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Hospitals" OnClientClick="OpenEnumDialog('Hospitals','7');return false;" />
                            <asp:Button runat="server" ID="Button10" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Aerial Rescue Plans" OnClientClick="OpenResucePlanDialog();return false;" />
                            <br />
                            <asp:Label ID="Label3" runat="server" CssClass="enquiryHeader">Marketing</asp:Label>
                            <br />
                            <asp:Button runat="server" ID="btnMarketingGeneral" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="General" OnClientClick="OpenGeneralMarketingDialog();return false;" />
                            <br />
                            <asp:Button runat="server" ID="btnWorkCategories" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Work Categories" OnClientClick="OpenWorkCategoriesDialog();return false;" />
                            <asp:Button runat="server" ID="Button8" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Letter" OnClientClick="OpenTemplatesDialog('Letter Templates',3,2);return false;" />
                            <asp:Button runat="server" ID="Button9" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Email" OnClientClick="OpenTemplatesDialog('Email Templates',3,1);return false;" />
                        </td>
                        <td style="vertical-align: top;">
                            <asp:Label ID="lblReports" runat="server" CssClass="enquiryHeader">Quote Templates</asp:Label>
                            <br />
                            <asp:Button runat="server" ID="btnLetter" ClientIDMode="Static" CssClass="button-settings" Width="140px" Text="Letter" OnClientClick="OpenTemplatesDialog('Letters Templates',0,0);return false;" />
                            <!--<asp:Button runat="server" ID="btnSchedule" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Schedule" OnClientClick="OpenTemplatesDialog('Schedule',0,2);return false;" />-->
                            <asp:Button runat="server" ID="btnEmail" ClientIDMode="Static" CssClass="button-settings" Width="140px" Text="Email" OnClientClick="OpenTemplatesDialog('Email Templates',0,1);return false;" />
                            <!-- V3.7.0 -->
                            <asp:Button runat="server" ID="btnSmsQuote" ClientIDMode="Static" CssClass="button-settings" Width="140px" Text="SMS" OnClientClick="OpenTemplatesDialog('SMS Templates',0,3);return false;" />
                            <br />
                            <asp:Label ID="Label1" runat="server" CssClass="enquiryHeader">Invoice Templates</asp:Label>
                            <br />
                            <asp:Button runat="server" ID="Button1" ClientIDMode="Static" CssClass="button-settings" Width="140px" Text="Letter" OnClientClick="OpenTemplatesDialog('Letters Templates',1,0);return false;" />
                            <!--<asp:Button runat="server" ID="Button2" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Schedule" OnClientClick="OpenTemplatesDialog('Schedule',1,2);return false;" />-->
                            <asp:Button runat="server" ID="Button3" ClientIDMode="Static" CssClass="button-settings" Width="140px" Text="Email" OnClientClick="OpenTemplatesDialog('Email Templates',1,1);return false;" />
                            <!-- V3.7.0 -->
                            <asp:Button runat="server" ID="btnSmsTemplate" ClientIDMode="Static" CssClass="button-settings" Width="140px" Text="SMS" OnClientClick="OpenTemplatesDialog('SMS Templates',1,3);return false;" />
                            <br />
                            <asp:Label ID="Label2" runat="server" CssClass="enquiryHeader">Coming Soon - Credit Note Templates</asp:Label>
                            <br />
                            <asp:Button runat="server" ID="Button4" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Letter" OnClientClick="return false;" Enabled="false" />
                            <asp:Button runat="server" ID="Button5" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Schedule" OnClientClick="OpenTemplatesDialog('Schedule',2,2);return false;" Enabled="false" />
                            <asp:Button runat="server" ID="Button6" ClientIDMode="Static" CssClass="button-settings" Width="200px" Text="Email" OnClientClick="OpenTemplatesDialog('Email',2,1);return false;" Enabled="false" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <script type="text/javascript" async>
        $(function () {
            Settings_Load();
        });
    </script>
</asp:Content>

