﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class _Default : Page
    {
        public String AppRoot
        {
            get
            {
                return Request.Url.Scheme + "://" + Request.Url.Host + (Request.Url.Port != 80 ? ":" + Request.Url.Port.ToString() : "");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}