﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using DGS.ArbPro.BL.SessionHandlers;

namespace DGS.ArbPro.Web
{
    public class AspSession
    {
        /// <summary>
        /// Kill Session
        /// Kills all session info
        /// </summary>
        public static bool KillSession()
        {
            bool bTerminated = false;
            string sCurrUserID = AspSession.CurrentUser.Id.ToString();
            string sCurrUserName = AspSession.CurrentUser.NameStr;
            try
            {
                List<string> aKeys = new List<string>();
                
                // Get all the keys
                foreach (string sKey in HttpContext.Current.Session.Keys)
                {
                    aKeys.Add(sKey);
                }

                // Clear all the keys
                foreach (string sKey in aKeys)
                {
                    HttpContext.Current.Session[sKey] = null;
                }
                bTerminated = true;

            }
            catch (Exception ex)
            {
                // Log the fact that we could not Kill/Clear the session
               Utility.Logger.Log.ErrorFormat("({0}:{1}) KillSession - Error - " + ex.ToString(), sCurrUserID, sCurrUserName);
            }

            return bTerminated;
        }

        /// <summary>
        /// Authentication result
        /// Enables access to all user specific information, including database
        /// </summary>
        public static Types.User CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session["objCurrentUser"] == null)
                {
                    var User = AspSessionHandler.GetForEmail(HttpContext.Current.User.Identity.Name);
                    HttpContext.Current.Session["objCurrentUser"] = User;
                    return User;
                }
                else
                {
                    return (Types.User)HttpContext.Current.Session["objCurrentUser"];
                }
            }
            set
            {
                HttpContext.Current.Session["objCurrentUser"] = value;
            }
        }

        /// <summary>
        /// The database name for the current user
        /// </summary>
        public static string UserDatabase
        {
            get { return CurrentUser.ClientDatabaseName; }
        }

        public static List<Types.UserDocument> TempEmailAttachments
        {
            get
            {
                if (HttpContext.Current.Session["lstTempEmailAttachments"] == null)
                    HttpContext.Current.Session["lstTempEmailAttachments"] = new List<Types.UserDocument>();
                return (List<Types.UserDocument>)HttpContext.Current.Session["lstTempEmailAttachments"];
            }
            set
            {
                HttpContext.Current.Session["lstTempEmailAttachments"] = value;
            }
        }

        public static MemoryStream PDFViewerMemoryStream
        {
            get
            {
                return (MemoryStream)HttpContext.Current.Session["pdfViewerMemStream"];
            }
            set
            {
                HttpContext.Current.Session["pdfViewerMemStream"] = value;
            }
        }

    }
}