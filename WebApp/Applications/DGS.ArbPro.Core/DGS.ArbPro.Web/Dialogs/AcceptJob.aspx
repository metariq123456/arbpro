﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AcceptJob.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.AcceptJob" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmAcceptJob" runat="server">
         <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
             This job is not yet marked as accepted, do you want to mark this job as accepted and
             proceed to the booking form?
         </asp:Panel>
    </form>
</body>
</html>
