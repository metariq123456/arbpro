﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobDetails.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.JobDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmJobDetails" runat="server">
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
            <!-- The following div is a content wrapper that is populated and displayed by the .js file if an error occures -->
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlJobDetailError" CssClass="width-full hidden">
                <asp:Label ID="lblDefaultError" runat="server">Error: Unable to continue operation.</asp:Label>
            </asp:Panel>
            <!-- The following div is a content wrapper that is populated by the .js file that is loaded with this dialog is displayed -->
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlJobDetails" CssClass="width-full">
                <asp:Label runat="server">Loading Please Wait...</asp:Label>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
