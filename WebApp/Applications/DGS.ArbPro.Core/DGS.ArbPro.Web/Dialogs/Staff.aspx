﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Staff.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.Staff" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="Staff" runat="server">
        <asp:Panel runat="server" ID="content" ClientIDMode="Static" style="padding-left: 10px;">
            <asp:Panel runat="server" ID="pnlStaffError" CssClass="width-full">
            </asp:Panel>
            <asp:Panel runat="server" ID="StaffDetails" style="margin-bottom: 0px" CssClass="float-left">
                <%--<asp:TextBox runat="server" ID="txtStaffUsername" ClientIDMode="Static" CssClass="clientField" placeholder="Username" ToolTip="Username"/>
                <asp:TextBox runat="server" ID="txtStaffPassword" ClientIDMode="Static" CssClass="clientField" TextMode="Password" placeholder="Password" ToolTip="Password"/>--%>
                <asp:TextBox runat="server" ID="txtStaffName" ClientIDMode="Static" CssClass="clientField" placeholder="Name" ToolTip="Staff name" Width="85%"/>
                <asp:TextBox runat="server" ID="txtStaffEmail" ClientIDMode="Static" CssClass="clientField" placeholder="Email" ToolTip="Staff email" Width="85%"/>
                <asp:TextBox runat="server" ID="txtStaffMobile" ClientIDMode="Static" CssClass="clientField" placeholder="Mobile" ToolTip="Staff mobile" Width="85%"/>
                <asp:TextBox runat="server" ID="txtStaffTelephone" ClientIDMode="Static" CssClass="clientField" placeholder="Telephone" ToolTip="Staff telephone" Width="85%"/>
                <%--<asp:TextBox runat="server" ID="txtStaffPosition" ClientIDMode="Static" CssClass="clientField" placeholder="Position" ToolTip="Staff position"/>--%>
                <asp:DropDownList runat="server" ID="drpDownPostion" ClientIDMode="Static" CssClass="clientField" ToolTip="Staff Position" Width="85%">
                    <asp:ListItem Value="0" Text="None" />
                    <asp:ListItem Value="1" Text="General" />
                    <asp:ListItem Value="2" Text="Team Member"/>
                    <asp:ListItem Value="3" Text="Driver" />
                    <asp:ListItem Value="4" Text="Team Leader" />
                    <asp:ListItem Value="5" Text="Surveyor" />
                    <asp:ListItem Value="10" Text="Director" />
                </asp:DropDownList>
                <asp:TextBox runat="server" ID="txtStaffStreetNum" ClientIDMode="Static" CssClass="clientField" placeholder="Street Number" ToolTip="Staff street number" Width="85%"/>
                <asp:TextBox runat="server" ID="txtStaffAddress1" ClientIDMode="Static" CssClass="clientField" placeholder="Address Line 1" ToolTip="Staff Address line 1" Width="85%"/>
                <asp:TextBox runat="server" ID="txtStaffAddress2" ClientIDMode="Static" CssClass="clientField" placeholder="Address Line 2" ToolTip="Staff Address line 2" Width="85%"/>
                <asp:TextBox runat="server" ID="txtStaffTown" ClientIDMode="Static" CssClass="clientField" placeholder="Town" ToolTip="Staff town" Width="85%"/>
                <asp:TextBox runat="server" ID="txtStaffCounty" ClientIDMode="Static" CssClass="clientField" placeholder="County" ToolTip="Staff county" Width="85%"/>
                <asp:TextBox runat="server" ID="txtStaffPostCode" ClientIDMode="Static" CssClass="clientField" placeholder="Post Code" ToolTip="Staff Postcode" Width="85%"/>
                <asp:TextBox runat="server" ID="txtStaffRole" ClientIDMode="Static" CssClass="clientField" placeholder="Staff Role" ToolTip="Staff Role" Width="85%"/>
                <asp:Image runat="server" ID="imgStaffSignature" ClientIDMode="Static" CssClass="clientField hidden"  Width="210px" Height="50px"/>
                <asp:CheckBox runat="server" ID="chkStaffIsMobileEnabled" ClientIDMode="Static"  Text="Mobile Enabled " CssClass="clientField" TextAlign="Left" Width="85%" />
                <asp:TextBox runat="server" ID="txtMobilePassword" TextMode="Password" ClientIDMode="Static" CssClass="clientField" placeholder="Mobile Password" ToolTip="The password that the user will use for accessing ArbPro mobile applciations" Width="85%"/>
                <asp:TextBox runat="server" ID="txtMobPasswordConformation" TextMode="Password" ClientIDMode="Static" CssClass="clientField" placeholder="Password Confirmation" ToolTip="Retype the password" Width="85%"/>
                <%--<asp:Button runat="server" ID="btnUploadImage" ClientIDMode="Static" CssClass="button-thin clientField"  Text="Upload" ToolTip="Staff Signature"/>--%>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>