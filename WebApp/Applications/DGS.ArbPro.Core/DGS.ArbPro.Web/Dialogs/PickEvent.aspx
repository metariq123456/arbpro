﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PickEvent.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.PickEvent" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmPickEvent" runat="server">
         <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
             <asp:Panel runat="server" CssClass="center">
                 <p>Do you want to book work or create a generic event?</p>
                 <p>Generic events are calendar events for anything like 
                    holiday booked or date reminders for example...</p>
             </asp:Panel>
         </asp:Panel>
    </form>
</body>
</html>
