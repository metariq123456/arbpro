﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RiskAssessmentDetails.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.RiskAssessmentDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmRiskDetails" runat="server">
    <asp:Panel runat="server" ID="content">
       <asp:HiddenField runat="server" ID="hfRiskId" ClientIDMode="static" />
            <asp:Panel ID="pnlRiskDetails" runat="server" CssClass="width-full">
            <asp:Panel runat="server" CssClass="width-full center">
                <asp:Image ID="imgEditWait" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
            </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
