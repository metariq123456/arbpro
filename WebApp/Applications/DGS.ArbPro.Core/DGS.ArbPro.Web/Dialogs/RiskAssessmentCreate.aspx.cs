﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web.Dialogs
{
    public partial class RiskAssessmentCreate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int value = -1;

            if (Request["QuoteID"] != null && int.TryParse(Request["QuoteID"], out value))
            {
                hfRecordId.Value = value.ToString();
                hfRecordType.Value = ((int)Types.Enums.RiskCreateFromObjectType.Quote).ToString();
            }
        }
    }
}