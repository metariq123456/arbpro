﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SmtpSettings.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.SmtpSettings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" CssClass="center">
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlSmtpDisplay" CssClass="width-full center">
                <asp:Panel runat="server" ID="pnlSmtpProcessing" ClientIDMode="Static" CssClass="hidden">
                    <asp:Table ID="Table1" runat="server" CssClass="waitTimer">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" runat="server">Processing Please Wait</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow2" runat="server">
                        <asp:TableCell ID="TableCell2" runat="server">
                            <asp:Image ID="imgSmtpProcessing" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:TableCell>
                    </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlSmtpError" CssClass="warning-text center" Width="90%">
            </asp:Panel><br/>
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlSmtpContent" CssClass="width-full center" Width="90%">
                <asp:Label runat="server" CssClass="companyFormTitle">Server Settings</asp:Label>
                <asp:CheckBox ID="chkUseSmtp" runat="server" CssClass="companyInput align-left" Text="Use Own Server" Width="90%"/>
                <asp:Label ID="lblHostAddress" runat="server" CssClass="companyInput align-left" placeholder="Host Address" Width="23%">Host</asp:Label>
                <asp:DropDownList ID="drpHostAddress" runat="server" CssClass="companyInput" placeholder="Host Address" Width="66%" OnSelectedIndexChanged="drpHostAddress_OnSelectedIndexChanged" onchange="checkSelected()">
                    <asp:ListItem Text="Gmail" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Windows Live(Web)" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Yahoo Mail" Value="2"></asp:ListItem>
                    <asp:ListItem Text="GoDaddy" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Exchange Mail" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Other" Value="4"></asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtHostAddress" runat="server" CssClass="companyInput" placeholder="Host Address" Width="90%" Enabled="false"></asp:TextBox>
                <asp:TextBox ID="txtPortNumber" runat="server" CssClass="companyInput" placeholder="Port" Width="90%" Enabled="False"></asp:TextBox>
                <asp:CheckBox ID="chkEnableSsl" runat="server" CssClass="companyInput align-left" Text="Enable SSL" Width="90%" ></asp:CheckBox>
                <asp:Label runat="server" CssClass="about-version" Width="90%">Select your email server's SMTP settings from the dropdown list above. 
                    <br/>If the server you are using is not among them please select "Other" and fill the settings manually. Suggested ports are 587 and 25. 
                    <br/>Watch the training video "Email set up" if you need further help.</asp:Label><br/><br/>
                <asp:Label runat="server" CssClass="companyFormTitle">User Account</asp:Label>
                <asp:TextBox ID="txtUserName" runat="server" CssClass="companyInput" placeholder="Email Address" Width="90%"></asp:TextBox>
                <asp:TextBox ID="txtPassword" runat="server" CssClass="companyInput" TextMode="Password" placeholder="Password" Width="90%"></asp:TextBox><br/><br/>
                <asp:Label runat="server" CssClass="about-version" Width="90%">
                    Enter your desired email account's information above and click the "Test Connection" button below in order to check if your settings have been successful. Ensure that your email address is valid.
                </asp:Label><br/>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>

