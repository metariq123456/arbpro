﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web.Dialogs
{
    public partial class RiskAssessmentAdditionalRisk : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfRiskId.Value = Request["RiskID"];
            hfAdditionalRiskId.Value = Request["ItemID"];
        }
    }
}