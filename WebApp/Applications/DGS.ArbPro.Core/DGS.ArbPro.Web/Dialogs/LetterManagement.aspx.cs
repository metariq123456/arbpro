﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web.Dialogs
{
    public partial class LetterManagement : System.Web.UI.Page
    {
        public String InvoiceQuote
        {
            get { return hfInvoiceQuote.Value; }
            set { hfInvoiceQuote.Value = value; }
        }
        public Int32 ItemID
        {
            get { return Int32.Parse(hfItemNo.Value); }
            set { hfItemNo.Value = value.ToString(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            InvoiceQuote = Request["Type"];
            ItemID = Int32.Parse(Request["ID"]);
        }
    }
}