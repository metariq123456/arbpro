﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BookWork.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.BookWork" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
            <asp:Panel runat="server" ID="pnlBookWorkLeft" ClientIDMode="Static"
                Width="200px" CssClass="float-left border-right height-full">

                <asp:Panel runat="server" CssClass="width-full bookField bordered-bottom">
                    <asp:Label runat="server" CssClass="bookLabel">Team</asp:Label>
                    <asp:DropDownList runat="server" ID="ddlBookWorkTeam"
                        ClientIDMode="Static" CssClass="bookSelect">
                    </asp:DropDownList>
                </asp:Panel>

                <asp:Panel runat="server" CssClass="width-full bookField bordered-bottom">
                    <asp:Label runat="server" CssClass="bookLabel">Quotes</asp:Label>
                    <asp:Panel runat="server" ID="pnlBookWorkItems"
                        ClientIDMode="Static" CssClass="bookInput bordered"
                        Height="160px" Width="95%" style="overflow-y: auto; overflow-x: hidden;">

                    </asp:Panel>
                </asp:Panel>

                <asp:Panel runat="server" CssClass="width-full bookField">
                    <asp:Label runat="server" CssClass="bookLabel">Tools</asp:Label>
                    <asp:Panel runat="server" ID="pnlBookWorkTools"
                        ClientIDMode="Static" CssClass="bookInput bordered"
                        Height="160px" Width="95%" style="overflow-y: auto; overflow-x: hidden;">

                    </asp:Panel>
                    <asp:TextBox runat="server" CssClass="bookInput"
                        ID="txtBookWorkNewTool" ClientIDMode="Static"
                        placeholder="New Tool"></asp:TextBox>
                    <asp:TextBox runat="server" CssClass="bookInput"
                        ID="txtBookWorkToolNum" ClientIDMode="Static"
                        style="width: 20%" placeholder="#">1</asp:TextBox>
                    <asp:Button runat="server" CssClass="button-thin float-right"
                        ID="btnBookWorkAddTool" ClientIDMode="Static"
                        style="width: 30%; margin-right: 10px;" OnClientClick="AddTool(); return false;"
                        Text="+" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlBookWorkRight" ClientIDMode="Static"
                Width="680px" CssClass="float-left">
                <asp:Panel runat="server" ID="pnlBookWorkCalendar" ClientIDMode="Static"
                    CssClass="width-full">

                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
