﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ItemMap.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.ItemMap" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmItemMap" runat="server">
        <asp:Panel runat="server" ID="content">
            <!-- Error panel where error messages can be displayed -->
            <asp:Panel ID="pnlError" runat="server" ClientIDMode="Static" CssClass="white float-left width-full"></asp:Panel>
            <!-- This form only displays the map for the items to be displayed on -->
            <asp:Panel ID="pnlItemMapCanvas" runat="server" ClientIDMode="Static" CssClass="white-light float-left width-full height-full">
                <asp:Image runat="server" ID="imgMapViewer" ClientIDMode="Static" ImageUrl="~/Images/load_Big.gif" style="height: 100%; width: auto"/>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
