﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateRiskCriteriaEdit.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.TemplateRiskCriteriaEdit" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
</head>
<body>
<form name="template" id="template" runat="server" action="TemplateRiskCriteriaEdit.aspx" method="post">
        <asp:Panel runat="server" ID="content" CssClass="width-full">
            <asp:HiddenField runat="server" ID="hfCriterionId" ClientIDMode="static" />
            <!-- criteria header table -->
            <table data-container="criterion" class="searchGrid width-full" data-field="crit-id" data-val="<%=EditRiskCriterion.Id%>">
                <thead>
                    <tr style="font-size: 0.9em;">
                        <th>Field</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="font-size: 0.9em;">
                        <td>Criterion Title</td>
                        <td>
                            <asp:TextBox data-field="cri-title" ID="CriterionTitle" runat="server" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="font-size: 0.9em;">
                        <td>Chapter Reference</td>
                        <td>
                            <asp:TextBox ID="ChapterReference" data-field="cri-chapref" runat="server" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="font-size: 0.9em;">
                        <td>Assessment Phase</td>
                        <td>
                            <asp:TextBox data-field="cri-phase" ID="AssessmentPhase" runat="server" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </form>
</body>
</html>