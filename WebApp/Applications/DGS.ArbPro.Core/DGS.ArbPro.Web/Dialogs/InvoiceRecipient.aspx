﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceRecipient.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.InvoiceRecpient" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmInvoiceRecipient" runat="server">
        <asp:Panel runat="server" ID="content" ClientIDMode="Static" >
            <asp:Panel runat="server" CssClass="width-full InvoiceMargin">
                <asp:Panel ID="invPnlRecipientError" runat="server" ClientIDMode="Static" CssClass="width-full float-left">
                </asp:Panel>
                <asp:Panel ID="invPnlContacts" runat="server" ClientIDMode="Static" CssClass="width-half float-left">
                    <asp:Table runat="server" CssClass="width-full center">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">Loading Contacts Please Wait</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow  runat="server">
                            <asp:TableCell  runat="server">
                                <asp:Image ID="imgInvPnlRecipients" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel ID="invPnlInvAddresses" runat="server" ClientIDMode="Static" CssClass="width-half float-right">
                    <asp:Table runat="server" CssClass="width-full center">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">Please Select/Enter an Invoice Address</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow  runat="server">
                            <asp:TableCell runat="server">
                                <asp:Image ID="imgPnlInvWaitAddress" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
