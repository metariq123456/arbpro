﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditInvoiceDate.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.EditInvoiceDate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form name="EditInvoiceDate" id="EditInvoiceDate" runat="server" method="post">
                <asp:Panel runat="server" ID="pnlEditInvoiceDate"  CssClass="80%" style="padding-left: 20px; padding-right: 20px;">
                     <br /> 
                    <asp:Panel ID="Panel7" runat="server">
                        <asp:Label ID="Label6" runat ="server" Width="37%" Text="Invoice Number" CssClass="float-left"/>
                        <asp:TextBox ID="txtInvoiceNo" runat="server" ReadOnly="true" Width="61%" ToolTip="Invoice Number" PlaceHolder="Invoice Number" CssClass="float-right" BackColor="#D3D3D3"/>
                    </asp:Panel>
                        <br />  <br /> 
                    <asp:Panel runat="server" ID="Panel5">
                        <asp:Label ID="Label4" runat="server" Width="37%" Text="Net Value" CssClass="float-left" />
                        <asp:TextBox ID="txtNetValue" runat="server" Width="61%" ReadOnly="true" ToolTip="Net Value" PlaceHolder="Document Type" CssClass="float-right" BackColor="#D3D3D3"/>
                    </asp:Panel>
                       <br /> <br /> 
                    <asp:Panel ID="Panel1" runat="server" Visible="true">
                        <asp:Label ID="Label1" runat ="server" Width="37%" Text="Date Due" CssClass="float-left"/>
                    <asp:TextBox ID="txtDateDue" runat="server" Width="61%" ToolTip="Date Due" PlaceHolder="Date Due" CssClass="float-right"/>
                    </asp:Panel>
                    <br /> <br /> 
                    <asp:Panel ID="Panel2" runat="server" Visible="true">
                        <asp:Label ID="Label7" runat ="server" Width="37%" Text="Status" CssClass="float-left"/>
                        <asp:TextBox ID="StatusString" runat="server" ReadOnly="true" Width="61%" ToolTip="Status" PlaceHolder="Status" CssClass="float-right" BackColor="#D3D3D3"/>
                    </asp:Panel>
                    <br /> <br /> 
                    <asp:Panel ID="Panel4" runat="server" Visible="true">
                        <asp:Label ID="Label3" runat ="server" Width="37%" Text="Sent Date" CssClass="float-left"/>
                        <asp:TextBox ID="txtSentDate" runat="server" Width="61%" ToolTip="Sent Date" PlaceHolder="Status" CssClass="float-right"/>
                    </asp:Panel>
                    <br /> <br /> 
                         <asp:Panel ID="Panel3" runat="server" Visible="true">
                        <asp:Label ID="Label2" runat ="server" Width="37%" Text="Paid Date" CssClass="float-left"/>
                        <asp:TextBox ID="txtPaidDate" runat="server" Width="61%" ToolTip="Paid Date" PlaceHolder="Paid Date" CssClass="float-right"/>
                    </asp:Panel>
                <br /> <br /> 
                </asp:Panel>
    </form>
</body>
</html>
