﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class About : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtVersionNo.Text = String.Format("Version {0}",
                Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }
    }
}