﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Alert.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.Alert" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmAlertBox" runat="server">
    <asp:Panel runat="server" ID="content" ClientIDMode="Static">
        <asp:Panel runat="server" ID="pnlAlertBoxDlg" CssClass="float-left width-full center" ClientIDMode="Static">
            <asp:Label ID="lblAlertMessage" runat="server" ClientIDMode="Static" CssClass="enquiryLabel"></asp:Label>
        </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
