﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MarketingGeneralSettings.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.MarketingGeneralSettings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content">
            <asp:Panel ID="pnlGeneralMarketingSettings" runat="server" CssClass="width-full">
                <br/>
                <asp:CheckBox ID="chkDisplayMarketing" runat="server" Text="Show Marketing on Quote Generation and Acceptance" TextAlign="left" CssClass="clientLabel paddedExtra width-full"/>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
