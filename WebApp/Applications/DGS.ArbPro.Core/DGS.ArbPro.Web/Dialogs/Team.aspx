﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Team.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.Team" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content">

            <asp:Panel runat="server" ID="pnlItemControls" CssClass="float-left width-full">
                <asp:Panel runat="server" CssClass="float-left width-full">
                    <asp:Panel runat="server" CssClass="dialog-detail float-left">
                        <asp:Panel runat="server" CssClass="companyField float-left">
                            <asp:Label ID="Label1" runat="server" Text="Name:" CssClass="companyLabel" />
                            <asp:TextBox ID="txtName" runat="server" ToolTip="Team Name" PlaceHolder="Team Name" CssClass="companyInput" ClientIDMode="Static" />
                        </asp:Panel>
                        <asp:Panel ID="Panel3" runat="server" CssClass="companyField">
                            <asp:Label runat="server" ID="lblColour" Text="Colour:" CssClass="companyLabel" />
                            <asp:DropDownList runat="server" ID="liColour" CssClass="companyInput" onchange="ColourChanged();" ClientIDMode="Static">
                                <asp:ListItem Text="Red" Value="1" />
                                <asp:ListItem Text="Blue" Value="2" />
                                <asp:ListItem Text="Green" Value="3" />
                                <asp:ListItem Text="Purple" Value="4" />
                                <asp:ListItem Text="Yellow" Value="5" />
                                <asp:ListItem Text="Brown" Value="6" />
                                <asp:ListItem Text="White" Value="7" />
                                <asp:ListItem Text="Black" Value="8" />
                            </asp:DropDownList>

                        </asp:Panel>
                        
                    <asp:Panel ID="pnlTeamLeader" runat="server" CssClass="companyField">
                        <asp:Label runat="server" ID="lblTeamLeader" Text="Team Leader:" CssClass="companyLabel" />
                        <asp:DropDownList runat="server" ID="ddlTeamLeader" CssClass="companyInput" ClientIDMode="Static">
                        </asp:DropDownList>
                    </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="dialog-detail float-left">
                        <asp:Panel runat="server" CssClass="companyField">
                            <asp:Label runat="server" ID="lblNumber" Text="Number:" CssClass="companyLabel" />
                            <asp:TextBox runat="server" ID="txtNumber" ToolTip="Team Number" PlaceHolder="Team Number" CssClass="companyInput" ClientIDMode="Static" />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlCurColour" CssClass="companyField">
                            <asp:Label runat="server" ID="lblColourSwatch" Text=" " CssClass="companyLabel" />
                            <asp:TextBox runat="server" CssClass="companyInput" ID="txtCurColour" BackColor="" Enabled="false" ClientIDMode="Static" />
                        </asp:Panel>
                        
                    <asp:Panel ID="pnlDesignatedDriver" runat="server" CssClass="companyField">
                        <asp:Label runat="server" ID="lblDesignatedDriver" Text="Team Driver:" CssClass="companyLabel" />
                        <asp:DropDownList runat="server" ID="ddlDesignatedDriver" CssClass="companyInput" ClientIDMode="Static">
                        </asp:DropDownList>
                    </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
<%--                <asp:Panel runat="server" ID="pnlItemButtons" CssClass="float-button">
                    <asp:Button runat="server" ID="btnTeamDelete" Text="Delete" CssClass="button-thin float-right padded" OnClientClick="TeamDelete();" />
                </asp:Panel>--%>
            </asp:Panel>
            <asp:Panel runat="server" CssClass="float-left width-full">
                <asp:Panel runat="server" ID="pnlTableStaffContainer" CssClass="float-left width-full">
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
