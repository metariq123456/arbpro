﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PDFPreview.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.PDFPreview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" CssClass="" Width="95%" Height="95">
            <asp:HiddenField runat="server" ID="hfDocType" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfDocID" ClientIDMode="Static" />
            <asp:Panel runat="server" ID="pnlContainer" CssClass="width-full height-full">
                <asp:Panel runat="server" ID="pnlImgDisplayContainer" CssClass="">
                    <asp:Panel runat="server" ID="pnlImgDisplay" CssClass="clientField height-full width-full">
                        <asp:Image ID="imgLoadGif" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlPdfDisplayContainer" CssClass="hidden">
                    <asp:Panel ID="pnlPDFPreviewPane" runat="server" CssClass="clientField height-full width-full">
                        <div id="previewToolbar" style="position: absolute; top: 16px; right: 32px; height: 24px; width: 128px; text-align: right; visibility:hidden;">
                            <button id="btnDownload" class="pdfButton button ui-dialog-button ui-button ui-widget ui-state-default ui-corner-all" style="width: 128px; height: 24px; font-size: 0.8em;" title="Download the PDF">
                                <span style="color: black;">Download PDF</span>
                            </button>
                        </div>
                        <iframe id="pdfPreview" clientidmode="Static" style="height: 633px; width: 450px;"></iframe>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlErrorDisplay" CssClass="hidden">
                    <asp:Panel ID="pnlError" runat="server" CssClass="clientField height-full width-full">
                        <asp:Label ID="txtError" runat="server" Text="" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>

