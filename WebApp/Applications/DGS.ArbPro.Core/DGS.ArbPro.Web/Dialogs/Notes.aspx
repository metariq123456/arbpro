﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Notes.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.Notes" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" ClientIDMode="Static">
            <asp:Panel runat="server" ID="pnlNotesDlg" CssClass="float-left" Width="90%" ClientIDMode="Static">
                <asp:Panel ID="pnlNotesDetails" runat="server" CssClass="width-full" ClientIDMode="Static">
                    <asp:Panel ID="NotesEnteredBy" runat="server" CssClass="" ClientIDMode="Static">
                        <asp:Panel ID="Panel1" runat="server" CssClass="center">
                            <asp:TextBox runat="server" ID="txtEnteredBy" CssClass="clientField width-full" ToolTip="Entered By" PlaceHolder="Entered By" Enabled="false" />
                        </asp:Panel>
                        <asp:Panel ID="Panel2" runat="server">
                            <asp:CheckBox runat="server"  CssClass="clientField" ID="chkDashboard" Text="Show Note on Dashboard" ClientIDMode="Static"/>
                        </asp:Panel>
                        <asp:Panel ID="pnlTextOfNote" runat="server" CssClass="width-full" ClientIDMode="Static">
                            <asp:TextBox runat="server" ID="txtNoteText" CssClass="width-full clientField" TextMode="MultiLine" ToolTip="Note Text" PlaceHolder="Note Text" Height="280px" />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="DateFields" CssClass="width-full">
                            <asp:Panel ID="pnlEnteredDate" runat="server" CssClass="width-full" ClientIDMode="Static">
                                <asp:TextBox runat="server" ID="txtEnteredDate" CssClass="width-full clientField" ToolTip="Entered Date" PlaceHolder="Entered Date" Enabled="false"/>
                            </asp:Panel>
                            <asp:Panel ID="pnlLastUpDate" runat="server" CssClass="width-full" ClientIDMode="Static">
                                <asp:TextBox runat="server" ID="txtUpDate" CssClass="width-full clientField" ToolTip="Last Up-Date" PlaceHolder="Last Up-Date" Enabled="false" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
