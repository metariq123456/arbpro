﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web.Dialogs
{
    public partial class CalendarGenericEdit : System.Web.UI.Page
    {
        DateTime Date = DateTime.Now;
        Boolean AllDay = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                hfEventId.Value = (Request["EventId"]).ToString();
                if (Request["Date"] != null || Request["allDay"] != null)
                {
                    Date = DateTime.Parse(Request["Date"].Replace("_", " ").Replace("?", ""));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(string.Format("Error in CalendarGenericEdit.aspx.cs.Page_Load(). Exception: {0}", ex.ToString()));
            }

            var Staff = new DAL.Staff(AspSession.UserDatabase).GetAll();

            ddlEventStaff_DLG.Items.Clear();
            ddlEventStaff_DLG.Items.Add(new ListItem("Select Staff (optional)", "-1"));
            foreach (var S in Staff)
            {
                ddlEventStaff_DLG.Items.Add(new ListItem(S.Name, S.Id.ToString()));
            }

            if (hfEventId.Value != "-1")
            {
                var cDB = new DAL.Calendar(AspSession.UserDatabase);
                var Event = cDB.GetGeneric(Int32.Parse(hfEventId.Value));

                AllDay = Event.AllDay;


                if (AllDay)
                {
                    pnlTimed.CssClass = "hidden";
                    pnlAllDay.CssClass = "";
                }
                else
                {
                    pnlTimed.CssClass = "";
                    pnlAllDay.CssClass = "hidden";
                }

                txtEventName.Text = Event.Name;

                txtDateFrom.Text = Event.StartDateTime.ToString("dd/MM/yy HH:mm");
                txtDateTo.Text = Event.EndDateTime.ToString("dd/MM/yy HH:mm");

                txtDayFrom.Text = Event.StartDateTime.ToShortDateString();
                txtDayTo.Text = Event.EndDateTime.ToShortDateString();

                ddlEventStaff_DLG.SelectedValue = Event.StaffID.ToString();
                ddlEventType_DLG.SelectedValue = ((Int32)Event.Type).ToString();

                txtEventComment.Text = Event.Comment;
            }
            else
            {
                ToggleAllDayPannel();
            }
        }

        private void ToggleAllDayPannel()
        {
            if (AllDay)
            {
                pnlTimed.CssClass = "hidden";
                pnlAllDay.CssClass = "";
            }
            else
            {
                pnlTimed.CssClass = "";
                pnlAllDay.CssClass = "hidden";
            }

            txtDateFrom.Text = Date.ToString("dd/MM/yy HH:mm");
            txtDateTo.Text = Date.AddMinutes(30).ToString("dd/MM/yy HH:mm");

            txtDayFrom.Text = Date.ToShortDateString();
            txtDayTo.Text = Date.ToShortDateString();
        }
    }
}