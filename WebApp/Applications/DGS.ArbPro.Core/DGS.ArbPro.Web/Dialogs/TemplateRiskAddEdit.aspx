﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateRiskAddEdit.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.TemplateRiskAddEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form name="template" id="template" runat="server" action="TemplateRiskAddEdit.aspx" method="post">
        <asp:Panel runat="server" ID="Panel9" Width="85%" Height="250px" CssClass="center">
                <asp:Panel runat="server" ID="pnlTemplateDetailsEdit"  CssClass="width-full">
                     <br /> 
                    <asp:Panel ID="Panel7" runat="server">
                        <asp:Label ID="Label6" runat ="server" Width="37%" Text="Type:" CssClass="float-left"/>
                    </asp:Panel>
                    <asp:DropDownList Width="62%" runat="server" ID="ddlTemplateType" ClientIDMode="Static" CssClass="templateInput float-right">
                        <asp:ListItem Value="0">Default</asp:ListItem>
                        <asp:ListItem Value="1">General</asp:ListItem>
                        <asp:ListItem Value="2">Specific</asp:ListItem>
                    </asp:DropDownList>
                        <br />  <br /> 
                    <asp:Panel runat="server" ID="Panel5">
                        <asp:Label ID="Label4" runat="server" Width="37%" Text="Template Name:" CssClass="float-left" />
                    </asp:Panel>
                    <asp:TextBox ID="txtTemplateName" runat="server" Width="61%" ToolTip="New Template Name" PlaceHolder="New Template Name" CssClass="float-right"/>
                        <br /> <br /> 
                    <asp:Panel ID="Panel6" runat="server">
                        <asp:Label ID="Label5" runat ="server" Width="37%" Text="Reference Document:" CssClass="float-left"/>
                    </asp:Panel>
                    <asp:TextBox ID="txtDocument" runat="server" Width="61%" ToolTip="Reference Document" PlaceHolder ="Reference Document" CssClass="float-right"/>
                        <br /> <br /> 
                    <asp:Panel ID="Panel8" runat="server">
                        <asp:Label ID="Label7" runat ="server" Width="37%" Text="Description:" CssClass="float-left"/>
                    </asp:Panel>
                    <asp:TextBox ID="txtDescription" runat="server" ToolTip="Description" PlaceHolder="Description" TextMode="MultiLine" Width="61%"  CssClass="settings-textboxMedium float-right width-full "/> 
                </asp:Panel>
            </asp:Panel>
    </form>
</body>
</html>
