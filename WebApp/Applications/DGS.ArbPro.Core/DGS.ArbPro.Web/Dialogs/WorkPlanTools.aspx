﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkPlanTools.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.WorkPlanTools" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmWorkPlanTool" runat="server">
        <asp:Panel runat="server" ID="content">
            <!-- Error panel - This is at the top so an error message should always be visible -->
            <asp:Panel ID="pnlToolsError" runat="server" ClientIDMode="Static" CssClass="width-full InvoiceMargin">
            </asp:Panel>
            <!-- The main content panel -->
            <asp:Panel ID="pnlPlanTool" runat="server" CssClass="width-full InvoiceMargin">
                <asp:Table runat="server" CssClass="width-full center">
                    <asp:TableRow runat="server">
                        <asp:TableCell runat="server">Loading Tools Please Wait</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow runat="server">
                        <asp:TableCell runat="server">
                            <asp:Image ID="imgPnlTool" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
