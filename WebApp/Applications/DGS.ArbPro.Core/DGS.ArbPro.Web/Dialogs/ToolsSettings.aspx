﻿#<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ToolsSettings.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.ToolsSettings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
         <asp:Panel runat="server" ID="content">
            <asp:Panel ID="Panel1" runat="server" CssClass="float-left border-right" Width="45%">
                <asp:Panel runat="server" ID="pnlTableContainer" Height="375px" Style="overflow-y: scroll;" CssClass="float-left width-full">
                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server" CssClass="float-left width-full">
                    <asp:Button runat="server" ID="btnNewItem" Text="New" CssClass="float-right button-thin" OnClientClick="NewItem();" />
                </asp:Panel>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlItemControls" CssClass="clientField float-left hidden">
                <asp:Panel runat="server" ID="pnlLabel">
                    <asp:Label ID="Label1" runat="server" Text="Tool Name:" />
                </asp:Panel>
                <asp:TextBox ID="txtValue" runat="server" ToolTip="New Tool Name" PlaceHolder="New Item Name" TextMode="MultiLine" CssClass="settings-textbox"/>
                <asp:Panel runat="server">
                <asp:Label runat ="server" Text="Current Stock:"/>
                </asp:Panel>
                <asp:TextBox ID="txtStock" runat="server" ToolTip="Current Tool Stock" PlaceHolder ="Current Tool Stock" />
                <asp:Panel runat="server" ID="pnlItemButtons" CssClass="center">
                    <br />
                    <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button-thin" OnClientClick="SaveItem();"/>
                    <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="button-thin" OnClientClick="DeleteItem();"/>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
