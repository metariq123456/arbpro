﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Site.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.Site" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content">
            <asp:Panel runat="server" CssClass="float-left" Width="300px" Height="415px" style="padding-left: 10px;"><br/>
                <asp:Panel runat="server" CssClass="warning-text center">You can edit or change the site address using the fields below</asp:Panel>
                <asp:Panel runat="server" Height="100px"></asp:Panel>
                <asp:HiddenField runat="server" ID="hfClientSiteID" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="hfClientSiteClientID" ClientIDMode="Static" />
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:TextBox runat="server" ToolTip="Site Reference"
                        ID="txtClientSiteReference"
                        CssClass="clientInput" ClientIDMode="Static"
                        placeholder="Reference" MaxLength="25">
                    </asp:TextBox>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:TextBox runat="server" ToolTip="House Name / Number"
                        ID="txtClientSiteHouseNameNo"
                        CssClass="clientInput" ClientIDMode="Static"
                        placeholder="House Name / Number">
                    </asp:TextBox>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:TextBox runat="server" ToolTip="Address Line 1"
                        ID="txtClientSiteAddress1"
                        CssClass="clientInput" ClientIDMode="Static"
                        placeholder="Address Line 1">
                    </asp:TextBox>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:TextBox runat="server" ToolTip="Address Line 2"
                        ID="txtClientSiteAddress2"
                        CssClass="clientInput" ClientIDMode="Static"
                        placeholder="Address Line 2">
                    </asp:TextBox>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:TextBox runat="server" ToolTip="Town"
                        ID="txtClientSiteTown"
                        CssClass="clientInput" ClientIDMode="Static"
                        placeholder="Town">
                    </asp:TextBox>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:TextBox runat="server" ToolTip="County"
                        ID="txtClientSiteCounty"
                        CssClass="clientInput" ClientIDMode="Static"
                        placeholder="County">
                    </asp:TextBox>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:TextBox runat="server" ToolTip="Post Code"
                        ID="txtClientSitePostCode"
                        CssClass="clientInput"
                        placeholder="Post Code" ClientIDMode="Static"
                        onfocus="addressWatcher.watchForChange(this)"
                        onblur="addressWatcher.cancelWatchForChange()"
                        onchange="addressWatcher.changed(this)">
                    </asp:TextBox>
                </asp:Panel>
                <asp:HiddenField runat="server" ID="hfClientSiteLatitude" ClientIDMode="Static" />
                <asp:HiddenField runat="server" ID="hfClientSiteLongitude" ClientIDMode="Static" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="white-light float-left" Width="390px" Height="415px">
                <asp:Panel runat="server" ID="mapcanvas" Width="390px" Height="415px"></asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
