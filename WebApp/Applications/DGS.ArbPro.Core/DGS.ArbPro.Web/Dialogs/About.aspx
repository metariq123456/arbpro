﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="DGS.ArbPro.Web.About" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" Width="585px" style="padding-left: 10px;">
            <asp:Panel runat="server" Height="64" Style="padding: 10px;">
                <asp:Panel runat="server" Style="float: left;">
                    <asp:Image runat="server" ImageUrl="../Images/arb_LeafTextLogo.png" />
                </asp:Panel>
                <asp:Panel runat="server" Style="float: right; text-align: right;">
                    <br />
                    <asp:Label runat="server" CssClass="about-version">Cloud Edition</asp:Label><br />
                    <asp:Label ID="txtVersionNo" runat="server" CssClass="about-version">Version 3.0.0.0</asp:Label>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server">
                <asp:Panel runat="server">
                    <asp:Label runat="server" CssClass="about-text" Font-Bold="true">Arb Pro Software Ltd</asp:Label><br />
                    <asp:HyperLink runat="server" CssClass="about-text" NavigateUrl="www.arbprosoftware.com">www.arbprosoftware.com</asp:HyperLink><br />
                    <asp:Label runat="server" CssClass="about-text">+44 (0) 2380 847287</asp:Label>
                </asp:Panel>
                <asp:Panel runat="server" Style="margin-top: 10px">
                    <asp:Label runat="server" CssClass="about-text" Font-Bold="true">Technical Support</asp:Label><br />
                    <asp:HyperLink runat="server" NavigateUrl="mailto:support@arbprosoftware.com" CssClass="about-text">support@arbprosoftware.com</asp:HyperLink>
                </asp:Panel>
                <asp:Panel runat="server" Style="margin-top: 10px">
                    <asp:Label runat="server" CssClass="about-text">Copyright © 2014 Arb Pro Software Ltd. All rights reserved</asp:Label>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
