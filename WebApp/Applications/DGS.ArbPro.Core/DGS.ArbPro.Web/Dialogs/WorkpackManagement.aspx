﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkpackManagement.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.WorkpackManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="frmWorkpacks" runat="server">
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
            <asp:HiddenField runat="server" ID="hfInvoiceQuote" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfItemNo" ClientIDMode="Static" />
            <asp:Panel runat="server"
                ID="pnlOptions" ClientIDMode="Static"
                CssClass="lmPanel"
                Style="overflow-y: auto; overflow-x: hidden;">
                <asp:Panel runat="server" ID="pnlPackOptions" ClientIDMode="Static">
                    <asp:Panel runat="server" ID="pnlLetterHead" ClientIDMode="Static" CssClass="lmHeader" Width="100%" Style="float: left;" onclick="LetterOptionsToggle();">
                        <asp:Label ID="lblPDFOptions" runat="server" CssClass="lmHeaderText" Style="float: left;">
                        PDF Options
                        </asp:Label>
                        <asp:Label runat="server" CssClass="lmHeaderText" ID="lblLetterOptionsToggle" ClientIDMode="Static" Width="10%" Style="float: right; padding-right: 5px; font-weight: bolder; text-align: center;">
                        -
                        </asp:Label>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlLetter" ClientIDMode="Static" CssClass="lmEdit" Width="100%">
                        <asp:Panel runat="server" ID="divLetterOptions" ClientIDMode="Static" Width="100%">
                            <asp:Panel ID="Panel1" runat="server" CssClass="lmField wifth-full">
                                <asp:Panel runat="server" ID="pnlDocumentSelection" CssClass="lmField wifth-full">
                                    <asp:Label ID="Label2" runat="server" AssociatedControlID="ddlPackType" CssClass="lmFieldLabel"  Style="margin-top: 10px;">
                                    Select Item to Create
                                    </asp:Label>
                                    <asp:DropDownList runat="server" CssClass="lmFieldControl" ID="ddlPackType" Style="margin-top: 10px;" ClientIDMode="Static" onchange="ShowLetterTemplate();">
                                        <asp:ListItem Value="0">Work Schedule</asp:ListItem>
                                        <asp:ListItem Value="1">Risk Assessment</asp:ListItem>
                                        <asp:ListItem Value="2">All Items</asp:ListItem>
                                    </asp:DropDownList>
                                    <br />
                                </asp:Panel>
                                <asp:Panel ID="Panel3" runat="server"
                                    CssClass="lmField"
                                    Width="100%"
                                    Height="30px">
                                    <asp:Button runat="server"
                                        CssClass="button-basic float-right paddedd"
                                        ID="btnPrintLetter" ClientIDMode="Static"
                                        Text="Generate PDF"
                                        Style="margin-top: 10px; margin-right: 10px;"
                                        OnClientClick="LoadPDF(false); return false;" />
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlEmailOptions">
                    <asp:Panel ID="Panel4" runat="server" CssClass="lmHeader" Width="100%" onclick="EmailOptionsToggle();">
                        <asp:Label ID="Label5" runat="server" CssClass="lmHeaderText" Style="float: left;">
                        Email Options
                        </asp:Label>
                        <asp:Label runat="server" CssClass="lmHeaderText" ClientIDMode="Static" ID="lblEmailOptionsToggle" Width="10%" Style="float: right; padding-right: 5px; font-weight: bolder; text-align: center;">
                        -
                        </asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="Panel5" runat="server" CssClass="lmEdit" Width="100%">
                        <asp:Panel runat="server" ID="divEmailOptions" ClientIDMode="Static" Width="100%">
                            <asp:Panel ID="Panel6" runat="server" CssClass="lmField" Width="100%">
                                <asp:Label ID="Label6" runat="server" CssClass="lmFieldLabel">
                            Recipient:
                                </asp:Label>
                                <asp:TextBox runat="server" ID="txtRecipient" ClientIDMode="Static" CssClass="lmFieldControl" Width="54%" />
                            </asp:Panel>
                            <asp:Panel ID="Panel7" runat="server" CssClass="lmField" Width="100%">
                                <asp:Label ID="Label8" runat="server" AssociatedControlID="txtEmailSubject" CssClass="lmFieldLabel">
                                Subject
                                </asp:Label>
                                <asp:TextBox runat="server" ID="txtEmailSubject" ClientIDMode="Static" CssClass="lmFieldControl" Width="54%" />
                                <asp:TextBox runat="server" ID="txtEmailText" ClientIDMode="Static"
                                    TextMode="MultiLine"
                                    CssClass="lmFieldText"
                                    Font-Size="Small"
                                    Font-Names="Helvetica" />
                            </asp:Panel>
                            <asp:Panel ID="Panel8" runat="server" CssClass="lmField" Width="100%">
                                <asp:Panel runat="server" ID="pnlAttachments" ClientIDMode="Static" Width="100%">
                                    <asp:Table runat="server" ID="grdAttachments" ClientIDMode="Static" Width="94%" CssClass="lmGrid">
                                        <asp:TableHeaderRow>
                                            <asp:TableHeaderCell Text="File Name" Width="80%" HorizontalAlign="Left" />
                                            <asp:TableHeaderCell Text="Attach" Width="20%" />
                                        </asp:TableHeaderRow>
                                    </asp:Table>
                                </asp:Panel>
                                <asp:Panel ID="Panel9" runat="server" Width="100%">
                                    <asp:FileUpload runat="server" ID="fuAttachment" ClientIDMode="Static" />
                                    <asp:Button runat="server" ID="btnUpload" ClientIDMode="Static" CssClass="button-basic float-right"
                                        Text="Attach" Style="margin-right: 10px;" OnClientClick="AddAttachment(); return false;" />
                                </asp:Panel>
                            </asp:Panel>
                            <br />
                            <br />
                            <asp:Panel ID="Panel10" runat="server" CssClass="lmField" Width="100%" Height="30px">
                                <asp:Button runat="server" CssClass="button-basic float-right" ID="btnSendEmail" ClientIDMode="Static"
                                    Text="Send Email" Style="margin-right: 10px;" OnClientClick="EmailPDF(); return false;" />
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlPreview" ClientIDMode="Static"
                CssClass="lmPanel lmPreview">
                <iframe id="pdfPreview" clientidmode="Static"
                    style="width: 100%; height: 100%;"></iframe>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
