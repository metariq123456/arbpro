﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AcceptedQuote.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.AcceptedQuote" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmAcceptedQuote" runat="server">
     <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
         <asp:Panel runat="server" ID="pnlAcceptedQuoteHeader" ClientIDMode="Static" CssClass="width-full">
                <asp:Label ID="lblTotalValue" runat="server" CssClass="dashboardDetailLabel float-right" ClientIDMode="Static">
                                Loading...
                </asp:Label>
         </asp:Panel>
         <asp:Panel ID="pnlCalander" runat="server" CssClass="float-left" Width="30%" Style="margin-right: 10px;">
             <asp:Panel runat="server">
                <asp:Label ID="lblQuoteStatus" runat="server" CssClass="" style="display: block;">Selected Quote Status:</asp:Label>
                <asp:DropDownList runat="server" CssClass="clientInput"
                    ID="ddlQuoteStatus" ClientIDMode="Static" >
                    <asp:ListItem Enabled="true" Value="0">Quote Accepted</asp:ListItem>
                    <asp:ListItem Enabled="true" Value="1">Quotes Not Accepted</asp:ListItem>
                    <asp:ListItem Enabled="true" Value="2">Quotes All</asp:ListItem>
                    </asp:DropDownList>
             </asp:Panel>
         </asp:Panel>
         <asp:Panel ID="Panel2" runat="server" CssClass="float-left" Width="30%" Style="margin-right: 10px;">
             <asp:Panel runat="server">
                <asp:Label ID="lblQuoteStatusDate" runat="server" CssClass="" style="display: block;">Selected Date Quote Status</asp:Label>
                <asp:DropDownList runat="server" CssClass="clientInput"
                    ID="ddlQuoteDates" ClientIDMode="Static" style="width:241px;">
                    <asp:ListItem Enabled="true" Value="1">1 Month</asp:ListItem>
                    <asp:ListItem Enabled="true" Value="2">2 Months</asp:ListItem>
                    <asp:ListItem Enabled="true" Value="3">3 Months</asp:ListItem>
                    <asp:ListItem Enabled="true" Value="6">6 Months</asp:ListItem>
                    <asp:ListItem Enabled="true" Value="12">12 Months</asp:ListItem>
                 </asp:DropDownList>
             </asp:Panel>
         </asp:Panel>
         <asp:Panel ID="Panel4" runat="server" CssClass="float-left" Width="35%">
              <asp:Button runat="server" ID="btnCreateDatabase" ClientIDMode="Static" CssClass="button-basic padded center" style="display:inline-block;margin-top:-50px;margin-bottom:10px;" Text="Search"  OnClientClick="PopulateAcceptedQuotePanal(); return false;"/>
         </asp:Panel>
         <br />
           <asp:Panel runat="server" ID="grdAcceptedQuotes" ClientIDMode="Static" CssClass="width-full" style="max-height: 400px; overflow-y: scroll;">
                            <asp:Panel ID="loading" runat="server" CssClass="center width-full">
                                <asp:Image ID="imgLoading" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:Panel>
            </asp:Panel> 
     </asp:Panel>
    </form>
</body>
</html>
