﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web.Dialogs
{
    public partial class Site : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Int32 SiteID = Int32.Parse(Request["ID"] == null ? "-1" : Request["ID"]);

            var Site = new Types.ClientSite();

            if (SiteID != -1)
            {
                var DB = new DAL.ClientSite(AspSession.CurrentUser.ClientDatabaseName);
                Site = DB.GetForId(SiteID);
            }

            hfClientSiteID.Value = Site.Id.ToString();
            hfClientSiteClientID.Value = Site.ClientId.ToString();
            hfClientSiteLatitude.Value = Site.Latitude.ToString();
            hfClientSiteLongitude.Value = Site.Longitude.ToString();

            txtClientSiteReference.Text = Site.Reference;
            txtClientSiteHouseNameNo.Text = Site.HouseNameNo;
            txtClientSiteAddress1.Text = Site.Address1;
            txtClientSiteAddress2.Text = Site.Address2;
            txtClientSiteTown.Text = Site.Town;
            txtClientSiteCounty.Text = Site.County;
            txtClientSitePostCode.Text = Site.PostCode;
        }
    }
}