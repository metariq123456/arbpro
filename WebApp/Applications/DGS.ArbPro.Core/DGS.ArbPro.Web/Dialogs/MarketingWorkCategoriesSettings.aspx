﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MarketingWorkCategoriesSettings.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.MarketingWorkCategoriesSettings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="content">
            <asp:Panel runat="server" CssClass="float-left border-right" Width="40%">
                <asp:Panel runat="server" ID="pnlTableContainer" Height="395px" Style="overflow-y: scroll;" CssClass="float-left width-full">
                </asp:Panel>
                <asp:Panel runat="server" CssClass="float-left width-full">
                    <asp:Button runat="server" ID="btnNewItem" Text="New" CssClass="float-right button-thin" OnClientClick="NewMarketingItem();" />
                </asp:Panel>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlItemControls" Width="50%" CssClass="clientField float-right hidden">
                <asp:Panel runat="server">
                    <asp:Label runat="server" Text="Category Name:" />
                </asp:Panel>
                <asp:TextBox ID="txtName" runat="server" ToolTip="New Category Name" PlaceHolder="New Item Name" TextMode="MultiLine" CssClass="settings-textboxMedium width-full"/>
                <asp:Panel runat="server">
                <asp:Label runat ="server" Text="Description:"/>
                </asp:Panel>
                <asp:TextBox ID="txtDesc" runat="server" ToolTip="Category Description" PlaceHolder="New Item Description" TextMode="MultiLine" CssClass="settings-textboxExtraLarge width-full" />
                <asp:Panel runat="server">
                <asp:Label runat ="server" Text="Default Return Time:"/>
                </asp:Panel>
                <asp:DropDownList ID="ddlRtnTime" runat="server" CssClass="width-full">
                    <asp:ListItem Value="1" Text="1 Month"></asp:ListItem>
                    <asp:ListItem Value="2" Text="2 Months"></asp:ListItem>
                    <asp:ListItem Value="3" Text="3 Months"></asp:ListItem>
                    <asp:ListItem Value="4" Text="4 Months"></asp:ListItem>
                    <asp:ListItem Value="5" Text="5 Months"></asp:ListItem>
                    <asp:ListItem Value="6" Text="6 Months"></asp:ListItem>
                    <asp:ListItem Value="7" Text="7 Months"></asp:ListItem>
                    <asp:ListItem Value="8" Text="8 Months"></asp:ListItem>
                    <asp:ListItem Value="9" Text="9 Months"></asp:ListItem>
                    <asp:ListItem Value="10" Text="10 Months"></asp:ListItem>
                    <asp:ListItem Value="11" Text="11 Months"></asp:ListItem>
                    <asp:ListItem Value="12" Text="12 Months"></asp:ListItem>
                    <asp:ListItem Value="18" Text="18 Months"></asp:ListItem>
                    <asp:ListItem Value="24" Text="24 Months"></asp:ListItem>
                    <asp:ListItem Value="30" Text="30 Months"></asp:ListItem>
                    <asp:ListItem Value="36" Text="36 Months"></asp:ListItem>
                    <asp:ListItem Value="42" Text="42 Months"></asp:ListItem>
                    <asp:ListItem Value="48" Text="48 Months"></asp:ListItem>
                    <asp:ListItem Value="54" Text="54 Months"></asp:ListItem>
                    <asp:ListItem Value="60" Text="60 Months"></asp:ListItem>
                    <asp:ListItem Value="99" Text="N/A"></asp:ListItem>
                </asp:DropDownList>
                <asp:Panel runat="server" ID="pnlItemButtons" CssClass="center">
                    <br />
                    <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button-thin" OnClientClick="SaveItem();"/>
                    <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="button-thin" OnClientClick="DeleteItem();"/>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
