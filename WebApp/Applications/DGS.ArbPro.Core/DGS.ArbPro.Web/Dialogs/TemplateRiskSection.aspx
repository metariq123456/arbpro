﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateRiskSection.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.TemplateRiskSection" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
</head>
<body>
    <form name="template" id="template" runat="server" action="TemplateRiskSection.aspx" method="post">
        <asp:Panel runat="server" ID="content" CssClass="width-full">
            <asp:HiddenField runat="server" ID="hfSectionId" ClientIDMode="static" />
            <asp:HiddenField runat="server" ID="hfSectionTitle" ClientIDMode="static" />
            <!-- section header table -->
            <table data-container="section" class="searchGrid width-full" data-field="crit-id" data-val="<%=RiskSection.Id%>">
                <thead>
                    <tr style="font-size: 0.9em;">
                        <th>Field</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="font-size: 0.9em;">
                        <td>Reference Document</td>
                        <td>
                            <asp:TextBox data-field="sect-docref" ID="DocumentReference" runat="server" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="font-size: 0.9em;">
                        <td>Reference Chapter</td>
                        <td>
                            <asp:TextBox ID="ChapterReference" data-field="sect-chapref" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="font-size: 0.9em;">
                        <td>Description</td>
                        <td class="riskGreyedOut">
                            <asp:Label ID="Description" data-field="sect-desc" runat="server"></asp:Label>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- section criteria table -->
            <table data-container="criterion" class="searchGrid width-full" style="table-layout: fixed;">
                <thead>
                    <tr style="font-size: 0.9em;">
                        <th style="width: 60%;">Criteria</th>
                        <th style="width: 20%;">Chapter Reference</th>
                        <th style="width: 20%; text-align: center;">Assessment Phase</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="CriteriaRepeater" runat="server">
                        <ItemTemplate>
                            <tr style="font-size: 0.9em;" data-field="crit-id" data-val="<%#Eval("Id") %>">
                                <td class="riskGreyedOut">
                                    <asp:Label ID="CriterionDescription" runat="server" data-field="crit-desc" Text='<%#Eval("Criterion") %>'></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="CriterionChapterReference" data-field="crit-chapref" runat="server" Text='<%#Eval("ChapterRef") %>' Width="97%"></asp:TextBox>
                                </td>
                                <td class="riskGreyedOut" style="text-align: center;">
                                    <asp:Label ID="CriterionAssessmentPhase" data-field="crit-assess" runat="server" Text='<%#Eval("AssessmentPhase") %>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </asp:Panel>
    </form>
</body>
</html>
