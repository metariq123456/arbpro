﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobItem.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.JobItem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" style="padding-left: 10px;">
           <asp:Panel runat="server" ID ="ItemDetails">
               <asp:Table ID="tblItemDetails" runat="server" CssClass="width-full center">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" runat="server">Loading Please Wait</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow2" runat="server">
                            <asp:TableCell ID="TableCell2" runat="server">
                                <asp:Image ID="imgInvPnlSites" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
           </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
