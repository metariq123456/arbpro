﻿namespace DGS.ArbPro.Web.Dialogs
{
    using System;

    /// <summary>
    /// The template risk criteria edit.
    /// </summary>
    public partial class TemplateRiskCriteriaEdit : System.Web.UI.Page
    {
        /// <summary>
        /// The handler.
        /// </summary>
        private BL.RiskTemplateHandler handler;

        /// <summary>
        /// Gets or sets the edit risk criterion.
        /// </summary>
        protected Types.Templates.RiskCriterion EditRiskCriterion { get; set; }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> The event arguments. </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.HttpMethod.Equals("POST"))
            {
                this.hfCriterionId.Value = this.Request["ID"];
                this.handler = new BL.RiskTemplateHandler(AspSession.UserDatabase);

                // get the risk criterion by calling the Risk Assessment WS
                this.EditRiskCriterion = this.handler.GetCriterionForId(int.Parse(this.hfCriterionId.Value));

                // criterion header information
                this.CriterionTitle.Text = this.EditRiskCriterion.Criterion;
                this.ChapterReference.Text = this.EditRiskCriterion.ChapterRef;
                this.AssessmentPhase.Text = this.EditRiskCriterion.AssessmentPhase.ToString();
            }
        }
    }
}