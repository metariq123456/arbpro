﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SearchResults.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.SearchResults" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Search Results</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" ClientIDMode="Static" CssClass="width-full">
            <asp:HiddenField runat="server" ID="HFSearchFor" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="HFSearchCriteria" ClientIDMode="Static" />
            <asp:Panel runat="server" ID="grdSearchResults" ClientIDMode="Static" style="margin-right: 10px; margin-top: 10px;" >
                <asp:Panel runat="server" CssClass="center width-full">
                    <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
