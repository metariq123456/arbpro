﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobSite.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.JobSite" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
            <asp:Panel runat="server" CssClass="width-full center">
                <asp:Button runat="server" ClientIDMode="Static" ID="btnAddNew" Text="Add New" CssClass="button-basic" OnClientClick="AddNewClick(); return false;" />
                <asp:Button runat="server" ClientIDMode="Static" ID="btnExisting" Text="Select Existing" CssClass="button-basic" OnClientClick="ExistingClick(); return false;" />
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlJobSiteAddNew"
                ClientIDMode="Static" CssClass="width-full float-left">
                <asp:Panel runat="server" CssClass="float-left"
                    Width="270px" Height="180px">
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:TextBox runat="server"
                            ID="txtJobSiteReference"
                            CssClass="clientInput" ClientIDMode="Static"
                            placeholder="Reference" MaxLength="10">
                        </asp:TextBox>
                        <asp:TextBox runat="server"
                            ID="txtJobSiteHouseNameNo"
                            CssClass="clientInput" ClientIDMode="Static"
                            placeholder="House Name / Number">
                        </asp:TextBox>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:TextBox runat="server"
                            ID="txtJobSiteAddress1"
                            CssClass="clientInput" ClientIDMode="Static"
                            placeholder="Address Line 1">
                        </asp:TextBox>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:TextBox runat="server"
                            ID="txtJobSiteAddress2"
                            CssClass="clientInput" ClientIDMode="Static"
                            placeholder="Address Line 2">
                        </asp:TextBox>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:TextBox runat="server"
                            ID="txtJobSiteTown"
                            CssClass="clientInput" ClientIDMode="Static"
                            placeholder="Town">
                        </asp:TextBox>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:TextBox runat="server"
                            ID="txtJobSiteCounty"
                            CssClass="clientInput" ClientIDMode="Static"
                            placeholder="County">
                        </asp:TextBox>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:TextBox runat="server"
                            ID="txtJobSitePostCode"
                            CssClass="clientInput"
                            placeholder="Post Code" ClientIDMode="Static"
                            onfocus="addressWatcher.watchForChange(this)"
                            onblur="addressWatcher.cancelWatchForChange()"
                            onchange="addressWatcher.changed(this)">
                        </asp:TextBox>
                    </asp:Panel>
                    <asp:HiddenField runat="server" ID="hfJobSiteLatitude" ClientIDMode="Static" />
                    <asp:HiddenField runat="server" ID="hfJobSiteLongitude" ClientIDMode="Static" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="white-light float-left" Width="260px" Height="180px">
                    <asp:Panel runat="server" ID="mapcanvas" Width="260px" Height="180px"></asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlJobSiteAddExisting"
                ClientIDMode="Static" CssClass="float-left padded" Width="540px">
                <asp:Panel runat="server" ID="grdJobSiteList" CssClass="width-full">
                    <asp:Panel runat="server" CssClass="center width-full">
                        <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
