﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.Contact" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" style="padding-left: 10px;">
            <asp:HiddenField runat="server" ID="hfContactID" ClientIDMode="static" />
            <asp:HiddenField runat="server" ID="hfContactType" ClientIDMode="static" />
            <asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactTitle" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="Title" ToolTip="Title" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="clientField" >
                <asp:TextBox runat="server" 
                    ID="txtContactForename" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="Forename" ToolTip ="Forename" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="clientField" >
                <asp:TextBox runat="server" 
                    ID="txtContactSurname" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="Surname" ToolTip="Surname" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactTelephone" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="Telephone Number" ToolTip="Telephone Number" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactMobile" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="Mobile Number" ToolTip="Mobile Number" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactEmail" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="Email Address" ToolTip="Email Address" />
            </asp:Panel>
            <%--<asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactJobTitle"
                    CssClass="clientInput"
                    placeholder="Job Title" ToolTip="Job Title" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactDepartment"
                    CssClass="clientInput"
                    placeholder="Department" ToolTip="Department" />
            </asp:Panel>--%>
            <asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactStreetNumber" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="House Name / Number" ToolTip="House Name / Number" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactAddress1" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="Address Line 1" ToolTip="Address Line 1" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactAddress2" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="Address Line 2" ToolTip="Address Line 2" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactTown" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="Town" ToolTip="Town" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactCounty" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="County" ToolTip="County" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="clientField">
                <asp:TextBox runat="server" 
                    ID="txtContactPostCode" ClientIDMode="static"
                    CssClass="clientInput"
                    placeholder="Post Code" ToolTip="Post Code" />
            </asp:Panel>
            <!-- DGSAP-855 -->
            <asp:Panel runat="server" CssClass="clientField">
                <asp:CheckBox runat="server" Checked="true" CssClass="clientInput"
                        ID="chkSendSmsContact" ClientIDMode="Static"
                        Text="Send SMS confirmation of emails" Width="100%"/>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
