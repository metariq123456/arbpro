﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateRiskEdit.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.TemplateRiskEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<form name="template" id="template" runat="server" action="TemplateRiskEdit.aspx" method="post">
        <asp:Panel runat="server" ID="templateContent" CssClass="width-full">
            <asp:Panel runat="server" CssClass="float-left border-right" Width="45%">
                <asp:Panel runat="server" ID="pnlTableContainer" Height="585px" Style="overflow-y: scroll;" CssClass="float-left width-full">
                </asp:Panel>
                <asp:Panel runat="server" CssClass="float-left width-full">
                    <asp:Button runat="server" ID="btnNewSection" Text="New" CssClass="float-right button-thin" OnClientClick="PopulateFields();" />
                </asp:Panel>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlEditSectionDetailsMain" CssClass="clientField float-left" Width="45%">
                    <br /> <br />
                <asp:Panel runat="server" ID="pnlInfoTextDetails" CssClass="center">
                        <asp:Label ID="Label2" runat="server" CssClass="warning-text" Text="Select New to add some new sections for risk assessment, sections are like the name of a group of risk items like “Welfare facilities” for example and you will then add criteria items for new section created" />
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlEditSectionDetails" CssClass="hidden">
                    <asp:Panel runat="server" ID="pnlLabel">
                        <asp:Label ID="Label1" runat="server" Text="Section Name:" />
                    </asp:Panel>
                    <asp:TextBox ID="txtName" runat="server" ToolTip="New Section Name" PlaceHolder="Enter new section name" CssClass="width-full"/>
                        <br /> <br />
                    <asp:Panel runat="server">
                        <asp:Label runat ="server" Text="Reference Document:"/>
                    </asp:Panel>
                    <asp:TextBox ID="txtDocument" runat="server" ToolTip="Reference Document" PlaceHolder ="Enter document name" CssClass="width-full"/>
                        <br /> <br />
                    <asp:Panel runat="server">
                        <asp:Label runat ="server" Text="Chapter Reference:"/>
                    </asp:Panel>
                    <asp:TextBox ID="txtChapter" runat="server" ToolTip="Chapter Reference" PlaceHolder ="Ref for control measures" CssClass="width-full"/>
                        <br /> <br />
                    <asp:Panel runat="server">
                        <asp:Label runat ="server" Text="Description:"/>
                    </asp:Panel>
                    <asp:TextBox ID="txtDescription" runat="server" ToolTip="Description" PlaceHolder="Enter a description for this section" TextMode="MultiLine" CssClass="settings-textbox width-full"/>
                    <asp:Panel runat="server" ID="pnlItemButtons" CssClass="center">
                        <br /> <br />
                        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button-thin float-right" OnClientClick="SaveSection();"/>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
