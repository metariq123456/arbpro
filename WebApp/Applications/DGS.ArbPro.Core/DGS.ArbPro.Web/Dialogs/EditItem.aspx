﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditItem.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.EditItem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmEditItem" runat="server">
    <asp:Panel runat="server" ID="content" ClientIDMode="Static" >
            <asp:Panel runat="server" CssClass="width-full InvoiceMargin">
                <!-- The first div is to enable an error message to be displayed -->
                <asp:Panel ID="pnlEditItemError" runat="server" ClientIDMode="Static" CssClass="width-full float-left">
                </asp:Panel>
                <asp:Panel ID="pnlEditItemContent" runat="server" ClientIDMode="Static" CssClass="width-full float-left">
                    <asp:Table runat="server" CssClass="width-full center">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">Loading Item Please Wait</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <asp:Image ID="imgEditItem" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
