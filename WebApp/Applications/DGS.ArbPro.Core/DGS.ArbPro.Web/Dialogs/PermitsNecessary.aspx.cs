﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web.Dialogs
{
    public partial class PermitsNecessary : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int value = -1;
            //Check if there is a quote number attached to the URL
            if (Request["QuoteID"] != null && int.TryParse(Request["QuoteID"], out value))
            {
                hfQuoteId.Value = value.ToString();
            }
            else
            {
                hfQuoteId.Value = "-1";
            }

        }
    }
}