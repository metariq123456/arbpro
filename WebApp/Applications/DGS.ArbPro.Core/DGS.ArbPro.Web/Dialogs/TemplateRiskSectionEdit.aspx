﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateRiskSectionEdit.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.TemplateRiskSectionEdit" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
</head>
<body>
<form name="template" id="template" runat="server" action="TemplateRiskSectionEdit.aspx" method="post">
        <asp:Panel runat="server" ID="content" CssClass="width-full">
            <asp:HiddenField runat="server" ID="hfSectionId" ClientIDMode="static" />
            <!-- section header table -->
            <table data-container="section" class="searchGrid width-full" data-field="crit-id" data-val="<%=EditRiskSection.Id%>">
                <thead>
                    <tr style="font-size: 0.9em;">
                        <th>Field</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr style="font-size: 0.9em;">
                        <td>Section Title</td>
                        <td>
                            <asp:TextBox data-field="sect-title" ID="SectionTitle" runat="server" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="font-size: 0.9em;">
                        <td>Reference Document</td>
                        <td>
                            <asp:TextBox data-field="sect-docref" ID="DocumentReference" runat="server" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="font-size: 0.9em;">
                        <td>Reference Chapter</td>
                        <td>
                            <asp:TextBox ID="ChapterReference" data-field="sect-chapref" runat="server" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr style="font-size: 0.9em;">
                        <td>Description</td>
                        <td class="riskGreyedOut">
                            <asp:TextBox ID="Description" style="-moz-resize:vertical; -ms-resize:vertical; -o-resize:vertical; resize:vertical" data-field="sect-desc" TextMode="MultiLine" Rows="2"  runat="server" Width="97%"></asp:TextBox>
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </form>
</body>
</html>
