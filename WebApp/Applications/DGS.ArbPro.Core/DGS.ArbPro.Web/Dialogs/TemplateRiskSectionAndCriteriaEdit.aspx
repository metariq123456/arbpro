﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplateRiskSectionAndCriteriaEdit.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.TemplateRiskSectionAndCriteriaEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
</head>
<body>
<form name="template" id="template" runat="server" action="TemplateRiskSectionAndCriteriaEdit.aspx" method="post">
        <asp:Panel runat="server" ID="templateSectionContent" CssClass="width-full">
            <asp:Panel runat="server" ID="Panel9" Width="85%" Height="250px" CssClass="center">
                <asp:Panel runat="server" ID="pnlSectionDetailsEdit"  CssClass="width-full">
                        <asp:Label CssClass="center" runat="server" Text="Section Details" />
                        <br /> 
                    <asp:Panel runat="server" ID="Panel5">
                        <asp:Label ID="Label4" runat="server" Width="40%" Text="Section Name:" CssClass="float-left" />
                    </asp:Panel>
                    <asp:TextBox ID="txtSectionName" runat="server" Width="58%" ToolTip="New Section Name" PlaceHolder="Enter new section name" CssClass="float-right"/>
                        <br /> <br /> 
                    <asp:Panel ID="Panel6" runat="server">
                        <asp:Label ID="Label5" runat ="server" Width="40%" Text="Reference Document:" CssClass="float-left"/>
                    </asp:Panel>
                    <asp:TextBox ID="txtDocument" runat="server" Width="58%" ToolTip="Reference Document" PlaceHolder ="Enter document name" CssClass="float-right"/>
                        <br /> <br /> 
                    <asp:Panel ID="Panel7" runat="server">
                        <asp:Label ID="Label6" runat ="server" Width="40%" Text="Chapter Reference:" CssClass="float-left"/>
                    </asp:Panel>
                    <asp:TextBox ID="txtSectionRef" runat="server" Width="58%" ToolTip="Chapter Reference" PlaceHolder ="Ref for control measures" CssClass="float-right"/>
                        <br /> <br /> 
                    <asp:Panel ID="Panel8" runat="server">
                        <asp:Label ID="Label7" runat ="server" Width="40%" Text="Description:" CssClass="float-left"/>
                    </asp:Panel>
                    <asp:TextBox ID="txtDescription" runat="server" ToolTip="Description" PlaceHolder="Enter a description for this section" TextMode="MultiLine" Width="58%"  CssClass="settings-textboxMedium float-right width-full "/>
                </asp:Panel>
            </asp:Panel>
            
            <asp:Panel runat="server" ID="Panel10" Width="95%" CssClass="clientField center">
                 <asp:Label ID="Label8" CssClass="center width-full" runat="server" Text="Section Criteria" />
                 <asp:Label ID="Label9" CssClass="center width-full warning-text" runat="server" Text="You can only add new or select existing section criteria." />

                <asp:Panel ID="Panel1" runat="server" CssClass="float-left border-right" Width="45%">
                    <asp:Panel runat="server" Height="280px" ID="pnlCriteriaTableContainer" Style="overflow-y: scroll;" CssClass="float-left width-full">
                    </asp:Panel>
                    <asp:Panel ID="Panel2" runat="server" CssClass="float-left width-full">
                        <asp:Button runat="server" ID="btnNewCriterion" Text="New" CssClass="float-right button-thin" OnClientClick="PopulateFields();" />
                    </asp:Panel>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlEditCriterionDetails" CssClass="clientField float-left hidden" Width="45%">
                    <asp:Panel runat="server" ID="pnlLabel">
                        <asp:Label ID="Label1" runat="server" Text="Criterion Name:" />
                    </asp:Panel>
                    <asp:TextBox ID="txtName" runat="server" ToolTip="New Criterion Name" PlaceHolder="Enter new criterion name" CssClass="width-full"/><br /> <br /> 
                    <asp:Panel ID="Panel4" runat="server">
                        <asp:Label ID="Label3" runat ="server" Text="Chapter Reference:"/>
                    </asp:Panel>
                    <asp:TextBox ID="txtChapter" runat="server" ToolTip="Chapter Reference" PlaceHolder ="Ref for control measures" CssClass="width-full"/><br /> <br /> 
                    <asp:Panel ID="Panel3" runat="server">
                        <asp:Label ID="Label2" runat ="server" Text="Assessment Phase:"/>
                    </asp:Panel>
                    <asp:TextBox ID="txtPhase" runat="server" ToolTip="Assessment Phase" PlaceHolder ="Enter phase 1 or 2" CssClass="width-full"/><br /> <br /> 
                    <asp:Panel runat="server" ID="pnlItemButtons" CssClass="center">
                        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button-thin float-right" OnClientClick="SaveCriterion();"/>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
