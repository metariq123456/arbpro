﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PDFWorkScheduleGeneration.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.PDFWorkScheduleGeneration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
<form id="frmPDFWorkScheduleGeneration" runat="server">
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
            <!-- This panel will be used to update the user on any errors or failures -->
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFError" CssClass="width-full float-left">
            </asp:Panel>
            <!-- This panel is used to contain all the page controls -->
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFConent" CssClass="width-full height-full float-left">
                <!-- This panel is used to contain the settings for the generation and emailing of the PDF -->
                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFSettings" CssClass="width-full float-left">
                    <asp:Panel runat="server">
                        <!-- PDF Letter generation body -->
                        <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFBody_0">
                            <asp:Table runat="server" CssClass="width-full center">
                                <asp:TableRow runat="server">
                                    <asp:TableCell runat="server">Loading PDF Settings Please Wait</asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow runat="server">
                                    <asp:TableCell runat="server">
                                        <asp:Image ID="imgPDFSettings" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="warningMessage" ClientIDMode="Static">
                        </asp:Panel>
                    </asp:Panel>
                    <br />
                </asp:Panel>
                <!-- This panel is used to display the generated PDF -->
                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFDisplay" CssClass="width-full height-full float-left">
                    <asp:Panel runat="server" ID="pnlLmProcessing" ClientIDMode="Static" CssClass="hidden">
                        <asp:Table runat="server" CssClass="waitTimer">
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">Processing Please Wait</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">
                                    <asp:Image ID="imgPDFProcessing" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlLmPdfView" ClientIDMode="Static" CssClass="lmPnlLrg">
                        <asp:Panel runat="server" ID="pnlPreview" ClientIDMode="Static" CssClass="lmPnlLrg lmPreview">
                            <div id="previewToolbar" style="position: absolute; top: 16px; right: 32px; height: 24px; width: 128px; text-align: right; visibility: hidden;">
                                <button id="btnDownload" class="pdfButton button ui-dialog-button ui-button ui-widget ui-state-default ui-corner-all" style="width: 128px; height: 24px; font-size: 0.8em;" title="Download the PDF">
                                    <span style="color: black;">Download PDF</span>
                                </button>
                            </div>
                            <iframe id="pdfPreview" clientidmode="Static" style="width: 99%; height: 99%;"></iframe>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
