﻿namespace DGS.ArbPro.Web.Dialogs
{
    using System;

    /// <summary>
    /// The template risk section.
    /// </summary>
    public partial class TemplateRiskSection : System.Web.UI.Page
    {
        /// <summary>
        /// Gets or sets the risk section.
        /// </summary>
        protected Types.Templates.RiskSection RiskSection { get; set; }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> The event arguments. </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.HttpMethod.Equals("POST"))
            {
                this.hfSectionId.Value = this.Request["ID"];
                    
                using (var service = new Services.Web.Template())
                {
                    // get the risk section by calling the Risk Assessment WS
                    this.RiskSection = service.GetSectionTemplateForId(1, int.Parse(this.hfSectionId.Value));

                    // section header information
                    this.hfSectionTitle.Value = this.RiskSection.Title;
                    this.DocumentReference.Text = this.RiskSection.ReferenceDoc;
                    this.ChapterReference.Text = this.RiskSection.ChapterRef;
                    this.Description.Text = this.RiskSection.Description;

                    // criterion repeater
                    this.CriteriaRepeater.DataSource = this.RiskSection.TemplateCriteria;
                    this.CriteriaRepeater.DataBind();
                }
            }
        }
    }
}