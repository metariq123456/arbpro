﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceItem.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.InvoiceItem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmInvoiceItem" runat="server">
        <asp:Panel runat="server" ID="content">
            <asp:Panel runat="server" CssClass="width-full InvoiceMargin">
                <asp:Panel ID="invPnlSites" runat="server" ClientIDMode="Static" CssClass="width-full">
                    <asp:Table ID="Table1" runat="server" CssClass="width-full center">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" runat="server">Loading Sites Please Wait</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <asp:Image ID="imgInvPnlSites" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel ID="invPnlWorkItems" runat="server" ClientIDMode="Static" CssClass="width-full InvoiceMargin">
                    <asp:Table runat="server" CssClass="width-full center">
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">Please Select A Site for invoicing</asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server">
                                <asp:Image ID="imgPnlInvWait" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel ID="invPnlItemError" runat="server" ClientIDMode="Static" CssClass="width-full">
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
