﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RiskStaffMember.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.RiskStaffMember" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmRiskStaffMember" runat="server">
    <asp:Panel runat="server" ID="content" ClientIDMode="Static">
            <asp:Panel runat="server" ID="pnlRiskAssessmentStaff" CssClass="center" ClientIDMode="Static">
                <asp:Panel ID="pnlWorkTeamStaff" runat="server" CssClass="float-left width-half"  ClientIDMode="Static">
                        <asp:Panel ID="pnlRiskStaffImg_2" runat="server" CssClass="width-full center">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlRiskStaffMember" runat="server" CssClass="float-left width-half"  ClientIDMode="Static">
                        <asp:Panel ID="pnlRiskStaffImg_1" runat="server" CssClass="width-full center">
                            <asp:Image ID="contactImage1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
