﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyInfo.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.CompanyInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" CssClass="center">
            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="companyInput" placeholder="Company Name" Width="90%" ToolTip="Company Name"></asp:TextBox>
            <asp:TextBox ID="txtCompanyNumber" runat="server" CssClass="companyInput" placeholder="House Name/Number" Width="90%" ToolTip="House Name/Number" Enabled="true"></asp:TextBox>
            <asp:TextBox ID="txtCompanyAddress1" runat="server" CssClass="companyInput" placeholder="Address Line 1" Width="90%" ToolTip="Address Line 1"></asp:TextBox>
            <asp:TextBox ID="txtCompanyAddress2" runat="server" CssClass="companyInput" placeholder="Address Line 2" Width="90%" ToolTip="Adrress Line 2"></asp:TextBox>
            <asp:TextBox ID="txtCompanyTown" runat="server" CssClass="companyInput" placeholder="Town" Width="90%" ToolTip="Town"></asp:TextBox>
            <asp:TextBox ID="txtCompanyCounty" runat="server" CssClass="companyInput" placeholder="County" Width="90%" ToolTip="County"></asp:TextBox>
            <asp:TextBox ID="txtCompanyPostCode" runat="server" CssClass="companyInput" placeholder="Post Code" Width="90%" ToolTip="Post Code"></asp:TextBox>
            <asp:TextBox ID="txtCompanyTelephoneNum" runat="server" CssClass="companyInput" placeholder="Telephone No" Width="90%" ToolTip="Telephone"></asp:TextBox>
            <asp:TextBox ID="txtCompanyMobileNum" runat="server" CssClass="companyInput" placeholder="Mobile No" Width="90%" ToolTip="Mobile" Enabled="true"></asp:TextBox>
            <asp:TextBox ID="txtCompanyWeb" runat="server" CssClass="companyInput" placeholder="Website" Width="90%" ToolTip="Website"></asp:TextBox>
            <asp:TextBox ID="txtCompanyEmailAddress" runat="server" CssClass="companyInput" placeholder="Email Address" Width="90%" ToolTip="Email Address" Enabled="true"></asp:TextBox>
            <asp:TextBox ID="txtCompanyAccountsEmail" runat="server" CssClass="companyInput" placeholder="Accounts Email Address" Width="90%" ToolTip="Accounts Email Address" Enabled="true"></asp:TextBox>
            <asp:TextBox ID="txtDirectorsName" runat="server" CssClass="companyInput" placeholder="Director's Name/Position" Width="90%" ToolTip="Director's name and position" Enabled="true"></asp:TextBox>
            <asp:TextBox ID="txtDirectorsQualifications" runat="server" CssClass="companyInput" placeholder="Director's Qualifications" Width="90%" ToolTip="Director's qualifications" Enabled="true"></asp:TextBox>
        </asp:Panel>
    </form>
</body>
</html>
