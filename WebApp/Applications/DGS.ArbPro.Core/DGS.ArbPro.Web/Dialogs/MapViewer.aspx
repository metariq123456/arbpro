﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MapViewer.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.MapViewer" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="content" style="padding-left: 10px;">
            <asp:Panel ID="pnlSiteMapPane" runat="server" CssClass="clientField height-full" Width="1050" Height ="800" >
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>