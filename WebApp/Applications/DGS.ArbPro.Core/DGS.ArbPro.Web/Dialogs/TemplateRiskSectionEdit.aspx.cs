﻿namespace DGS.ArbPro.Web.Dialogs
{
    using System;

    /// <summary>
    /// The template risk section edit.
    /// </summary>
    public partial class TemplateRiskSectionEdit : System.Web.UI.Page
    {
        /// <summary>
        /// The handler.
        /// </summary>
        private BL.RiskTemplateHandler handler;

        /// <summary>
        /// Gets or sets the edit risk section.
        /// </summary>
        protected Types.Templates.RiskSection EditRiskSection { get; set; }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> The event arguments. </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Request.HttpMethod.Equals("POST"))
            {
                this.hfSectionId.Value = this.Request["ID"];
                this.handler = new BL.RiskTemplateHandler(AspSession.UserDatabase);

                // get the risk section by calling the Risk Assessment WS
                this.EditRiskSection = this.handler.GetSectionForId(-1, int.Parse(this.hfSectionId.Value));

                // section header information
                this.SectionTitle.Text = this.EditRiskSection.Title;
                this.DocumentReference.Text = this.EditRiskSection.ReferenceDoc;
                this.ChapterReference.Text = this.EditRiskSection.ChapterRef;
                this.Description.Text = this.EditRiskSection.Description;
            }
        }
    }
}