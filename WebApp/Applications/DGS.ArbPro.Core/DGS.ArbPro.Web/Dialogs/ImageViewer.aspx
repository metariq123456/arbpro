﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImageViewer.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.ImageViewer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server"> 
     <asp:Panel runat="server" ID="content">
         <br/>
             		<asp:Label ID="lblTotalImage" runat="server" Text="0 of 0" style="padding-left:85%; padding-top:0px"></asp:Label>

         <asp:Panel ID="pnlImageViewerPane" runat="server" CssClass="center" Width="500" Height="345" >
             <asp:Image runat="server" ID="imgImageViewer" ClientIDMode="Static" ImageUrl="~/Images/load_Big.gif" style="height: 100%; width: auto"/>
         </asp:Panel>
         <br/>
         <asp:Panel ID="pnlPictureControlButtons" runat="server" CssClass="center" Width="495" Height="50" >

             <asp:ImageButton ID="btnfirst" runat="server" ImageUrl="../Images/First.png" CssClass="button-basic" Width="30" Height="40" OnClientClick="FirstImage(); return false;"/>
             <asp:ImageButton ID="btnprev" runat="server" ImageUrl="../Images/Prev.png" CssClass="button-basic" Width="30" Height="40" OnClientClick="PreviousImage(); return false;" />
             <asp:ImageButton ID="btnnext" runat="server" ImageUrl="../Images/Next.png" CssClass="button-basic" Width="30" Height="40" OnClientClick="NextImage(); return false;"/>
             <asp:ImageButton ID="btnlast" runat="server" ImageUrl="../Images/Last.png" CssClass="button-basic" Width="30" Height="40" OnClientClick="LastImage(); return false;"/>

             <asp:ImageButton ID="ibtnRotateCCW" runat="server" ImageUrl="../Images/ico_RotateCounterClockWise.png" CssClass="button-basic" Width="45" Height="40" OnClientClick="Image_Rotate(-90); return false;"/>
             <asp:ImageButton ID="ibtnFlipVertical" runat="server" ImageUrl="../Images/ico_FlipVertical.png" CssClass="button-basic" Width="40" Height="40" OnClientClick="Image_Flip('Vertical'); return false;"/>
             <asp:ImageButton ID="ibtnFlipHorizontal" runat="server" ImageUrl="../Images/ico_FlipHorizontal.png" CssClass="button-basic" Width="40" Height="40" OnClientClick="Image_Flip('Horizontal'); return false;"/>
             <asp:ImageButton ID="ibtnRotateCW" runat="server" ImageUrl="../Images/ico_RotateClockWise.png" CssClass="button-basic" Width="45" Height="40" OnClientClick="Image_Rotate(90); return false;"/>


         </asp:Panel>
         <asp:Panel ID="pnlUploadProgress" ClientIDMode="Static" runat="server" CssClass="center hidden" Width="500" Height="15">
                 <asp:Button runat="server" ID="btnCancelUpload" ClientIDMode="Static" OnClientClick="Image_Cancel(); return false;" Text="X" CssClass="button-basic float-right" />
             <asp:Panel runat="server" ID="pnlUploadBarContainer" CssClass="loadContainer">
                 <asp:Panel runat="server" ID="pnlUploadBar" ClientIDMode="Static" CssClass="loadBar float-left"></asp:Panel>
             </asp:Panel>
         </asp:Panel>
         <asp:Panel ID="pnlUploadContainer" ClientIDMode="Static" runat="server" CssClass="center" Width="500" Height="20">
             <br/>
             <asp:FileUpload AllowMultiple="false" runat="server" ID="fuImage" ClientIDMode="Static" CssClass="client-input float-left" Width="230"  />
             <asp:Button runat="server" ID="btnUploadFile" ClientIDMode="Static" CssClass="button-basic float-left" Text="Upload" OnClientClick="Image_Upload(); return false;" />
             <asp:Button runat="server" ID="btnRemoveImage" ClientIDMode="Static" CssClass="button-basic float-left" Text="Remove" OnClientClick="Image_Clear(); return false;" />
             <asp:Button runat="server" ID="btnPrimtImage" ClientIDMode="Static" CssClass="button-basic float-left" Text="Print" OnClientClick="PrintImage(); return false;" />
         </asp:Panel>
     </asp:Panel>
    </form>
</body>
</html>
