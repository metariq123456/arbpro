﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RiskAssessmentManagement.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.RiskAssessmentManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="content" CssClass="center">
       <asp:HiddenField runat="server" ID="hfRiskId" ClientIDMode="static" />
            <asp:Panel ID="pnlRiskManagement" runat="server" CssClass="width-full center">
            <asp:Panel runat="server" CssClass="width-full center">
                <asp:Image ID="imgEditManWait" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
            </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
