﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyDefaults.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.CompanyDefaults" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmComapnyDefaults" runat="server">
        <asp:Panel runat="server" ClientIDMode="Static" ID="content" CssClass="center">
            <!-- Loading wait timer div -->
            <asp:Panel runat="server" ClientIDMode="Static" ID="waitTimer" CssClass="width-full center">
                <asp:Panel ID="Panel1" runat="server" CssClass="center">
                    <asp:Image ID="cmpImage1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                </asp:Panel>
            </asp:Panel>
            <!-- Content controls div -->
            <asp:Panel runat="server" ClientIDMode="Static" ID="contentControls" CssClass="hidden">
                <asp:Panel ID="Panel4" runat="server" ClientIDMode="Static" CssClass="cmpDialogDivider">
                    <asp:Panel ID="pnlTreeDefaults" runat="server" ClientIDMode="Static">
                        <asp:Panel runat="server" ClientIDMode="Static">
                            <asp:Label ID="lblTreeNames" runat="server" ClientIDMode="Static" Text="Tree Names"></asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ClientIDMode="Static">
                            <asp:DropDownList ID="ddlTreeNames" runat="server" ClientIDMode="Static" ToolTip="Select the type of name to be displayed within the application">
                                <asp:ListItem Value="0">Common</asp:ListItem>
                                <asp:ListItem Value="1">Latin</asp:ListItem>
                            </asp:DropDownList>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlVATDefault" ClientIDMode="Static">
                    <asp:Panel runat="server" ClientIDMode="Static" CssClass="cmpDialogDivider">
                        <asp:Panel runat="server" ClientIDMode="Static">
                            <asp:Label runat="server" ClientIDMode="Static" ID="lblDefaultVAT" Text="Default VAT:"/>
                        </asp:Panel>
                        <asp:Panel runat="server" ClientIDMode="Static">
                            <asp:TextBox runat="server" ID="txtVatRate" ToolTip="The default VAT to be used when adding items" Placeholder="Default VAT" CssClass="companyDlgInput" ClientIDMode="Static"/> <asp:Label runat="server" ID="lblPercent" Text="%" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="Panel3" runat="server" ClientIDMode="Static" CssClass="cmpDialogDivider">
                    <asp:Panel runat="server" ClientIDMode="Static" ID="pnlQuoteDefaults">
                        <asp:Panel runat="server" ClientIDMode="Static">
                            <asp:Label ID="lblQuoteRef" runat="server" ClientIDMode="Static" Text="Next Quote Number"></asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ClientIDMode="Static">
                            <asp:TextBox ID="txtQuotePrefix" CssClass="companyDlgInput" runat="server" ClientIDMode="Static" ToolTip="The Prefix for the next Quote" Placeholder="Prefix"></asp:TextBox>
                            <asp:TextBox ID="txtQuoteNumber" CssClass="companyDlgInput" runat="server" ClientIDMode="Static" ToolTip="The next Quote reference number" Placeholder="Number"></asp:TextBox>
                            <asp:TextBox ID="txtQuoteSuffix" CssClass="companyDlgInput" runat="server" ClientIDMode="Static" ToolTip="The Suffix for the next Quote" Placeholder="Suffix"></asp:TextBox>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="Panel2" runat="server" ClientIDMode="Static" CssClass="cmpDialogDivider">
                    <asp:Panel runat="server" ClientIDMode="Static" ID="pnlInvoiceDefaults">
                        <asp:Panel runat="server" ClientIDMode="Static">
                            <asp:Label ID="lblInvoiceRef" runat="server" ClientIDMode="Static" Text="Next Invoice Number"></asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ClientIDMode="Static">
                            <asp:TextBox ID="txtInvoicePrefix" CssClass="companyDlgInput" runat="server" ClientIDMode="Static" ToolTip="The Prefix for the next Invoice" Placeholder="Prefix"></asp:TextBox>
                            <asp:TextBox ID="txtInvoiceNumber" CssClass="companyDlgInput" runat="server" ClientIDMode="Static" ToolTip="The next Invoice reference number" Placeholder="Number"></asp:TextBox>
                            <asp:TextBox ID="txtInvoiceSuffix" CssClass="companyDlgInput" runat="server" ClientIDMode="Static" ToolTip="The Suffix for the next Invoice" Placeholder="Suffix"></asp:TextBox>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ClientIDMode="Static" CssClass="cmpDialogDivider">
                    <asp:Panel runat="server" ClientIDMode="Static" ID="pnlCreditDefaults">
                        <asp:Panel runat="server" ClientIDMode="Static">
                            <asp:Label ID="lblCreditRef" runat="server" ClientIDMode="Static" Text="Next Credit Number"></asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" ClientIDMode="Static">
                            <asp:TextBox ID="txtCreditPrefix" CssClass="companyDlgInput" runat="server" ClientIDMode="Static" ToolTip="The Prefix for the next Credit Note" Placeholder="Prefix"></asp:TextBox>
                            <asp:TextBox ID="txtCreditNumber" CssClass="companyDlgInput" runat="server" ClientIDMode="Static" ToolTip="The next Credit Note reference number" Placeholder="Number"></asp:TextBox>
                            <asp:TextBox ID="txtCreditSuffix" CssClass="companyDlgInput" runat="server" ClientIDMode="Static" ToolTip="The Suffix for the next Credit Note" Placeholder="Suffix"></asp:TextBox>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
