﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;

namespace DGS.ArbPro.Web.Dialogs
{
    public partial class SearchResults : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            //string SearchCriteria = Request["searchCriteria"];
            //string SearchFor = Request["Search"];
            //HFSearchFor.Value = SearchFor;
            //HFSearchCriteria.Value = SearchCriteria;
            
        }
//            string SearchCriteria = Request["searchCriteria"];
//            string SearchFor = Request["Search"];
//            Image1.Visible = false;

//            try
//            {
//                switch (SearchCriteria)
//                {
//                    case "1":
//                        SearchForName(SearchFor);
//                        break;
//                    case "2":
//                        SearchForClientAddress(SearchFor);
//                        break;
//                    case "3":
//                        SearchForClientTelephone(SearchFor);
//                        break;
//                    case "4":
//                        SearchForClientPostCode(SearchFor);
//                        break;
//                    case "6":
//                    case "7":
//                    case "8":
//                    case "9":
//                        SearchForContactDetails(SearchFor);
//                        break;
//                    case "11":
//                        SearchForQuoteClientNumber(SearchFor);
//                        break;
//                    case "12":
//                        SearchForQuoteName(SearchFor);
//                        break;
//                    case "13":
//                        SearchForQuoteSiteAddress(SearchFor);
//                        break;
//                    default:
//                        lblSearchInfo.Text = "Not Available Yet";
//                        break;
//                }
//            }
//            catch(Exception ex)
//            {
//                Debug.WriteLine(ex.Message);
//            }
//        }

//        #region Methods

//        #region Client Search
//        public void SearchForName(string SearchFor)
//        {
//            var nameSearchResults = BL.SearchHandlers.SearchResultHandler.GetClientForNameLike(AspSession.UserDatabase, SearchFor);
//            lblSearchInfo.Text = "Search for '" + SearchFor + "' returned " + nameSearchResults.Count.ToString() + " result(s)";
//            for (int i = 0; i < nameSearchResults.Count; i++)
//            {
//                TextBox txtResultsBox = new TextBox();
//                txtResultsBox.Width = 750;
//                txtResultsBox.Style.Value = "clientField";
//                txtResultsBox.Text = nameSearchResults[i].Title;
//                grdSearchResults.Controls.Add(txtResultsBox);
//            }
//        }

//        public void SearchForClientAddress(string SearchFor)
//        {
//            var clientAddressSearchResults = BL.SearchHandlers.SearchResultHandler.GetClientForAddressLike(AspSession.UserDatabase, SearchFor);

//            lblSearchInfo.Text = "Search for '" + SearchFor + "' returned " + clientAddressSearchResults.Count.ToString() + " result(s)";
//            for (int i = 0; i < clientAddressSearchResults.Count; i++)
//            {
//                TextBox txtResultsBox = new TextBox();
//                txtResultsBox.Width = 750;
//                txtResultsBox.Style.Value = "clientField";
//                txtResultsBox.Text = clientAddressSearchResults[i].Detail.Remove(0, 13);
//                txtResultsBox.Text.Insert(0, clientAddressSearchResults[i].Title);
//                grdSearchResults.Controls.Add(txtResultsBox);
//            }
//        }

//        public void SearchForClientTelephone(string SearchFor)
//        {
//            var clientTelephoneSearchResults = BL.SearchHandlers.SearchResultHandler.GetClientForTelephoneLike(AspSession.UserDatabase, SearchFor);

//            lblSearchInfo.Text = "Search for '" + SearchFor + "' returned " + clientTelephoneSearchResults.Count.ToString() + " result(s)";
//            for (int i = 0; i < clientTelephoneSearchResults.Count; i++)
//            {
//                TextBox txtResultsBox = new TextBox();
//                txtResultsBox.Width = 750;
//                txtResultsBox.Style.Value = "clientField";
//                txtResultsBox.Text = clientTelephoneSearchResults[i].Title;
//                grdSearchResults.Controls.Add(txtResultsBox);
//            }
//        }

//        public void SearchForClientPostCode(string SearchFor)
//        {
//            var clientPostCodeSearchResults = BL.SearchHandlers.SearchResultHandler.GetClientForPostCodeLike(AspSession.UserDatabase, SearchFor);

//            lblSearchInfo.Text = "Search for '" + SearchFor + "' returned " + clientPostCodeSearchResults.Count.ToString() + " result(s)";
//            for (int i = 0; i < clientPostCodeSearchResults.Count; i++)
//            {
//                TextBox txtResultsBox = new TextBox();
//                txtResultsBox.Width = 750;
//                txtResultsBox.Style.Value = "clientField";
//                txtResultsBox.Text = clientPostCodeSearchResults[i].Title;
//                txtResultsBox.Text += " | " + clientPostCodeSearchResults[i].Detail;
//                grdSearchResults.Controls.Add(txtResultsBox);
//            }
//        }
//        #endregion

//        #region Contact Search
//        public void SearchForContactDetails(string SearchFor)
//        {
//            var ContactDetailsSearchResults = BL.SearchHandlers.SearchResultHandler.GetContactDetailsLike(AspSession.UserDatabase, SearchFor);

//            lblSearchInfo.Text = "Search for '" + SearchFor + "' returned " + ContactDetailsSearchResults.Count.ToString() + " result(s)";
//            for (int i = 0; i < ContactDetailsSearchResults.Count; i++)
//            {
//                TextBox txtResultsBox = new TextBox();
//                txtResultsBox.Width = 750;
//                txtResultsBox.Style.Value = "clientField";
//                txtResultsBox.Text += ContactDetailsSearchResults[i].Detail;
//                grdSearchResults.Controls.Add(txtResultsBox);
//            }
//        }
//        #endregion
//        #region Quote Search
//        public void SearchForQuoteClientNumber(string SearchFor)
//        {
//            var ClientNumberSearchResults = BL.SearchHandlers.SearchResultHandler.GetQuoteClientNumberLike(AspSession.UserDatabase, SearchFor);

//            lblSearchInfo.Text = "Search for '" + SearchFor + "' returned " + ClientNumberSearchResults.Count.ToString() + " result(s)";
//            for (int i = 0; i < ClientNumberSearchResults.Count; i++)
//            {
//                TextBox txtResultsBox = new TextBox();
//                txtResultsBox.Width = 750;
//                txtResultsBox.Style.Value = "clientField";
//                txtResultsBox.Text += ClientNumberSearchResults[i].Detail;
//                grdSearchResults.Controls.Add(txtResultsBox);
//            }
//        }

//        public void SearchForQuoteName(string SearchFor)
//        {
//            var ClientNumberSearchResults = BL.SearchHandlers.SearchResultHandler.GetQuoteNameLike(AspSession.UserDatabase, SearchFor);

//            lblSearchInfo.Text = "Search for '" + SearchFor + "' returned " + ClientNumberSearchResults.Count.ToString() + " result(s)";
//            for (int i = 0; i < ClientNumberSearchResults.Count; i++)
//            {
//                TextBox txtResultsBox = new TextBox();
//                txtResultsBox.Width = 750;
//                txtResultsBox.Style.Value = "clientField";
//                txtResultsBox.Text += ClientNumberSearchResults[i].Detail;
//                grdSearchResults.Controls.Add(txtResultsBox);
//            }
//        }

//        public void SearchForQuoteSiteAddress(string SearchFor)
//        {
//            var ClientNumberSearchResults = BL.SearchHandlers.SearchResultHandler.GetQuoteSiteAddressLike(AspSession.UserDatabase, SearchFor);

//            lblSearchInfo.Text = "Search for '" + SearchFor + "' returned " + ClientNumberSearchResults.Count.ToString() + " result(s)";
//            for (int i = 0; i < ClientNumberSearchResults.Count; i++)
//            {
//                TextBox txtResultsBox = new TextBox();
//                txtResultsBox.Width = 750;
//                txtResultsBox.Style.Value = "clientField";
//                txtResultsBox.Text += ClientNumberSearchResults[i].Detail;
//                grdSearchResults.Controls.Add(txtResultsBox);
//            }
//        }
//        #endregion
//        #endregion
    }
}