﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SageExport.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.SageExport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" CssClass="center">
            <asp:TextBox runat="server" ID="txtSalesInvoiceCode" ToolTip="Sales Invoice Code" Placeholder="Sales Invoice Nominal Code" Width="90%" CssClass="clientInput" />
            <asp:TextBox runat="server" ID="txtSalesReceipCode" ToolTip="Sales Receipt Code" Placeholder="Sales Receipt Nominal Code" Width="90%" CssClass="clientInput" />
            <asp:TextBox runat="server" ID="txtSageVatCode" ToolTip="Sage VAT Code" Placeholder="Sage VAT Code" Width="90%" CssClass="clientInput" />
        </asp:Panel>
    </form>
</body>
</html>
