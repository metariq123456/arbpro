﻿namespace DGS.ArbPro.Web.Dialogs
{
    using System;

    /// <summary>
    /// The template risk edit.
    /// </summary>
    public partial class TemplateRiskEdit : System.Web.UI.Page
    {
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}