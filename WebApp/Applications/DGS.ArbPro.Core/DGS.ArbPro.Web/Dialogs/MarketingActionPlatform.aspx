﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MarketingActionPlatform.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.MarketingActionPlatform" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel runat="server" ID="content">
            <asp:Panel runat="server" CssClass="float-left width-full" >
                <asp:Panel runat="server" ID="pnlTableContainer" Style="overflow-y: scroll;" CssClass="float-left width-full">
                </asp:Panel>
            </asp:Panel>

        </asp:Panel>
    </form>
</body>
</html>
