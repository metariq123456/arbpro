﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobDocumentsEdit.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.JobDocumentsEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form name="JobDocuments" id="JobDocsEdit" runat="server" method="post">
                <asp:Panel runat="server" ID="pnlCompanyDocuments"  CssClass="80%" style="padding-left: 20px; padding-right: 20px;">
                     <br /> 
                    <asp:Panel ID="Panel7" runat="server">
                        <asp:Label ID="Label6" runat ="server" Width="37%" Text=" Document Name:" CssClass="float-left"/>
                        <asp:TextBox ID="txtName" runat="server" Width="61%" ToolTip="New Document Name " PlaceHolder="New Document Name" CssClass="float-right"/>
                    </asp:Panel>
                        <br />  <br /> 
                    <asp:Panel runat="server" ID="Panel5">
                        <asp:Label ID="Label4" runat="server" Width="37%" Text=" Document Type:" CssClass="float-left" />
                    </asp:Panel>
                    <asp:TextBox ID="txtType" runat="server" Width="61%" ToolTip="New Document Type" PlaceHolder="New Document Type" CssClass="float-right"/>
                        <br /> <br /> 
                    <asp:Panel ID="Panel1" runat="server" Visible="true">
                        <asp:Label ID="Label1" runat ="server" Width="37%" Text=" Uploaded Date:" CssClass="float-left"/>
                    </asp:Panel>
                    <asp:TextBox ID="txtDate" runat="server" ReadOnly="true" Width="61%" ToolTip="New Document Date" PlaceHolder="New Document Date" CssClass="float-right"/>
                    <asp:Panel ID="Panel8" runat="server">
                    <br /> <br /> 
                        <asp:Label ID="Label7" runat ="server" Width="37%" Text=" Uploaded By:" CssClass="float-left"/>
                    </asp:Panel>
                        <asp:DropDownList runat="server" ID="ddlStaffTable" Width="61%" ClientIDMode="Static">
                        <asp:ListItem>Staff Names Here</asp:ListItem>
                        </asp:DropDownList>
                    <br /> <br /> 
                    <asp:TextBox ID="txtFileName" runat="server" Width="61%" ToolTip="File Name" PlaceHolder="File Name" CssClass="float-right"/>
                    <asp:Panel ID="Panel2" runat="server" Visible="true">
                        <asp:Label ID="Label2" runat ="server" Width="37%" Text="File Name" CssClass="float-left"/>
                                            <br /> <br /> 
                    <asp:Label ID="Label5" runat="server" Width="37%" Text="Upload formats" CssClass="float-left"/>
                    <asp:Label ID="Label3" runat="server" Width="37%" Text="jpg, jpeg, png, gif, pdf" CssClass="center"/>
                    </asp:Panel>
                    <br /> <br /> 
                 </asp:Panel>
                    <asp:Panel ID="pnlUploadContainer" runat="server" style="padding-left: 10px;">
                        <asp:FileUpload runat="server" CssClass="float-left"
                        ID="fuDocument" ClientIDMode="Static"
                        style="width : 70%;"/>
                        <br /> <br /> 
                        <asp:Button runat="server" ID="btnUploadFile" ClientIDMode="Static" CssClass="button-basic float-left" Text="Update" OnClientClick="File_UploadEdit(); return false;" />
                        <asp:Button runat="server" ID="btnDeleteDocument" ClientIDMode="Static" CssClass="button-basic float-left" Text="Delete" OnClientClick="if (!window.confirm('You are about to delete this document, are you sure?')){return false;}else{DocumentDelete(); return false;}" />
                           
                        <asp:Panel ID="pnlUploadProgress" ClientIDMode="Static" runat="server" CssClass="center hidden" Width="500" Height="30" Style="width: 50%;background-color: #ddd;margin-left:20px;display:inline-block">
                            <asp:Panel runat="server" ID="pnlUploadBarContainer" Style="width: 1%;height: 30px;background-color: #4CAF50;">
                 
                            </asp:Panel>
                      </asp:Panel>           
                        </asp:Panel>
                     <br />
                   <asp:Panel ID="Panel3" runat="server" ClientIDMode="Static" style="padding-left: 15px; padding-right: 15px;display: table-cell;vertical-align: bottom;" >
                     <span style="color:red">WARNING!:</span> Please be aware, the more documents you upload and select as default email attachments,
                    the longer the email screen may take to load when sending quotations or invoices due to file
                    sizes.
                </asp:Panel>
                          
    </form>
</body>
</html>
