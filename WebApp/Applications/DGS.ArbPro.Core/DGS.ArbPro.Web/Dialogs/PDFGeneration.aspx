﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PDFGeneration.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.PDFCreation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmPDFGeneration" runat="server">
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
           <!-- This panel will be used to update the user on any errors or failures -->
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFError" CssClass="width-full float-left">
            </asp:Panel>
            <!-- This panel is used to contain all the page controls -->
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFConent" CssClass="width-full height-full float-left">
                <!-- This panel is used to contain the settings for the generation and emailing of the PDF -->
                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFSettings" CssClass="lmScrollPanel">
                    <asp:Panel runat="server">
                        <!-- PDF Letter generation header -->
                        <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFHeader_0" CssClass="lmHeader" onclick="ToggleDsiplay(0);">
                            <asp:Label ID="lblHeaderTitle_0" runat="server" CssClass="lmHeaderText  float-left">PDF Generation</asp:Label>
                            <asp:Label runat="server" CssClass="lmHeaderText lmHeaderToggle" ID="lblHeaderToggle_0" ClientIDMode="Static">-</asp:Label>
                        </asp:Panel>
                        <!-- PDF Letter generation body -->
                        <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFBody_0">
                            <asp:Table runat="server" CssClass="width-full center">
                                <asp:TableRow runat="server">
                                    <asp:TableCell runat="server">Loading PDF Settings Please Wait</asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow runat="server">
                                    <asp:TableCell runat="server">
                                        <asp:Image ID="imgPDFSettings" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="warningMessage" ClientIDMode="Static">
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="EmailContainerPanel">
                        <!-- PDF Email generation header -->
                        <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFHeader_1" CssClass="lmHeader" onclick="ToggleEmailOptionDisplay(1);">
                            <asp:Label ID="lblHeaderTitle_1" runat="server" CssClass="lmHeaderText  float-left">Email Options</asp:Label>
                            <asp:Label runat="server" CssClass="lmHeaderText lmHeaderToggle" ID="lblHeaderToggle_1" ClientIDMode="Static">-</asp:Label>
                        </asp:Panel>
                        <!-- PDF Email generation body -->
                        <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFBody_1">
                            <asp:Table runat="server" CssClass="width-full center">
                                <asp:TableRow runat="server">
                                    <asp:TableCell runat="server">Loading Email Settings Please Wait</asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow runat="server">
                                    <asp:TableCell runat="server">
                                        <asp:Image ID="imgPDFEmail" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:Panel>
                    </asp:Panel>

                    <asp:Panel runat="server" ID="SmsContainerPanel">
                        <!-- SMS generation header -->
                        <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFHeader_2" CssClass="lmHeader" onclick="ToggleEmailOptionDisplay(2);">
                            <asp:Label ID="lblHeaderTitle_2" runat="server" CssClass="lmHeaderText  float-left">SMS Options</asp:Label>
                            <asp:Label runat="server" CssClass="lmHeaderText lmHeaderToggle" ID="lblHeaderToggle_2" ClientIDMode="Static">-</asp:Label>
                        </asp:Panel>
                        <!-- SMS generation body -->
                        <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFBody_2">
                            <asp:Table  runat="server" CssClass="width-full center">
                                <asp:TableRow runat="server">
                                    <asp:TableCell ID="TableCell1" runat="server">Loading SMS Template Please Wait</asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow runat="server">
                                    <asp:TableCell runat="server">
                                        <asp:Image ID="imgSms" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>

                <!-- This panel is used to display the generated PDF -->
                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFDisplay" CssClass="width-half height-full float-left">
                    <asp:Panel runat="server" ID="pnlLmProcessing" ClientIDMode="Static" CssClass="hidden">
                        <asp:Table runat="server" CssClass="waitTimer">
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">Processing PDF and/or Email Generation.<br/>Please Wait</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server">
                                <asp:TableCell runat="server">
                                    <asp:Image ID="imgPDFProcessing" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlLmPdfView" ClientIDMode="Static" CssClass="lmPnlLrg">
                        <asp:Panel runat="server" ID="pnlPreview" ClientIDMode="Static" CssClass="lmPnlLrg lmPreview">
                            <div id="previewToolbar" style="position: absolute; top: 16px; right: 32px; height: 24px; width: 128px; text-align: right; visibility: hidden;">
                                <button id="btnDownload" class="pdfButton button ui-dialog-button ui-button ui-widget ui-state-default ui-corner-all" style="width: 128px; height: 24px; font-size: 0.8em;" title="Download the PDF">
                                    <span style="color: black;">Download PDF</span>
                                </button>
                            </div>
                            <iframe id="pdfPreview" clientidmode="Static" style="width: 100%; height: 100%;"></iframe>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
             <div id="DocAttachModel" class="hidden" style="padding-left: 10px;padding-right:10px;">
                 <asp:Panel runat="server" ClientIDMode="Static">
                    <span style="color:red">INFORMATION!:</span> Attaching documents, please wait a moment.
                 </asp:Panel>
                 <p>Select the documents you would like to attach to this email</p>
                 <asp:Panel runat="server" ID="grdDoc" ClientIDMode="Static" CssClass="width-full" style="max-height: 400px; overflow-y: scroll;">
                            <asp:Panel ID="loading" runat="server" CssClass="center width-full" ClientIDMode="Static">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" ClientIDMode="Static"/>
                            </asp:Panel>
                 </asp:Panel> 
                 <p>Select Arb Pro documents you would like to attach to this email</p>
                  <asp:Panel runat="server" ID="grdArbProDoc" ClientIDMode="Static" CssClass="width-full" style="max-height: 400px; overflow-y: scroll;">
                            <asp:Panel ID="arbproLoading" runat="server" CssClass="center width-full" ClientIDMode="Static">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" ClientIDMode="Static"/>
                            </asp:Panel>
                 </asp:Panel>
                 <asp:Panel runat="server" ID="pnlDocloading" ClientIDMode="Static" >
                        <asp:Table runat="server" CssClass="waitTimer" ClientIDMode="Static">
                            <asp:TableRow  runat="server" ClientIDMode="Static">
                                <asp:TableCell  runat="server" ClientIDMode="Static">Adding document(s) to email.<br/>Please Wait</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow runat="server" ClientIDMode="Static">
                                <asp:TableCell runat="server" ClientIDMode="Static">
                                    <asp:Image  runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" ClientIDMode="Static"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
            </div>
          </asp:Panel>
    </form>
</body>
</html>
