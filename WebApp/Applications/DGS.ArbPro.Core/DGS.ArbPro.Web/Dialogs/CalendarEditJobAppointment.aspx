﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalendarEditJobAppointment.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.CalendarEditJobAppointment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:Panel runat="server" ID="content" Style="overflow: hidden; padding-left: 20px; padding-right: 20px">
            <asp:HiddenField runat="server" ID="hfAppSurveyor" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfJobSiteId" ClientIDMode="Static" />
            <asp:Label runat="server" ID="lblJobNo" CssClass="center" Width="100%">Appointment Info For Job No. </asp:Label><br />
            <asp:Panel runat="server" CssClass="width-full float-left">
                <asp:Panel ID="pnlErr" runat="server" CssClass="hidden">
                    <asp:Label runat="server" ID="lblErr" Text="" CssClass="clientLabel" />
                </asp:Panel><br/>
                <asp:Label ID="Label4" CssClass="clientLabel float-left" runat="server" Width="35%">Client Name: </asp:Label>            
                <asp:TextBox runat="server" ID="txtClientName" ClientIDMode="static" CssClass="clientInput float-right" placeholder="Client Name" Enabled="false" Width="63%">
                </asp:TextBox><br /><br />
                <asp:Label ID="lblSite" runat="server" CssClass="clientLabel float-left" Text="Job Site:" Width="35%" />
                <asp:TextBox runat="server"
                    ID="txtClientSites" ClientIDMode="Static"
                    CssClass="clientInput float-right"
                    Enabled="false" Width="63%" TextMode="multiline" Rows="3" style="resize: none">
                </asp:TextBox>
                <asp:Label ID="Label3" runat="server" CssClass="warning-text center" 
                    Text="To edit the site address, please select the View button in order to be redirected to the job screen. From there you can select and edit the job sites under the Sites panel." 
                    Width="100%" /><br /><br />
            </asp:Panel>
            <asp:Panel runat="server" Width="100%" ID="pnlTimed">
                <asp:Label ID="Label5" runat="server" CssClass="center" Width="100%">
                    Calendar Navigation
                </asp:Label><br /><br />
                    <asp:Button runat="server" ID="btnPrev" CssClass="button-thin float-left" Text="Prev Month" />
                    <asp:Button runat="server" ID="btnNext" CssClass="button-thin float-right" Text="Next Month" /> <br /><br />
                <asp:Label ID="Label2" runat="server" CssClass="center" Width="100%">
                    Appointment Details
                </asp:Label><br /><br />
                <asp:Label ID="Label1" runat="server" CssClass="clientLabel float-left" Width="30%">Surveyor: </asp:Label>
                <asp:DropDownList runat="server"
                    ID="ddlSurveyors" ClientIDMode="Static"
                    CssClass="clientInput float-right"
                    onchange="SurveyorChanged();" Width="67%">
                </asp:DropDownList> <br /><br />
                <asp:Label runat="server" CssClass="clientLabel float-left" Width="30%">Start Date:</asp:Label>    
                <asp:TextBox runat="server" ID="txtDateFrom"
                    ClientIDMode="static" CssClass="clientInput float-right" Width="66%"
                    placeholder="Date From"></asp:TextBox><br /><br />
                <asp:Label runat="server" CssClass="clientLabel float-left" Width="30%">End Date: </asp:Label>    
                <asp:TextBox runat="server" ID="txtDateTo"
                    ClientIDMode="static" CssClass="clientInput float-right" Width="66%"
                    placeholder="Date To"></asp:TextBox>
            </asp:Panel><br /><br />
            <asp:CheckBox runat="server" CssClass="clientInput float-left" ID="chkAppointmentCompleted" ClientIDMode="Static" Text="Appointment Completed" /><br />
            <asp:Panel runat="server" CssClass="clientInput float-left hidden" Width="100%" ID="pnlReDownload">
                <asp:CheckBox runat="server" ID="chkDownloadAgain" ClientIDMode="Static" Text="Release For Download" />
            </asp:Panel>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
