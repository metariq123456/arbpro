﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CompanyContact.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.CompanyContact" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmCompanyContact" runat="server">
    <asp:Panel runat="server" ID="content" ClientIDMode="Static">
            <asp:Panel runat="server" ID="pnlCmpContactDlg" CssClass="float-left width-full" ClientIDMode="Static">
                <asp:Panel ID="pnlCmpContactDlgDetails" runat="server" CssClass="width-full"  ClientIDMode="Static">
                        <asp:Panel ID="Panel1" runat="server" CssClass="center">
                            <asp:Image ID="contactImage1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
