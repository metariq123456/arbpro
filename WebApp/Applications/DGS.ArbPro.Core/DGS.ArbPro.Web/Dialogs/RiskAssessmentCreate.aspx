﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RiskAssessmentCreate.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.RiskAssessmentCreate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="frmRiskCreate" runat="server">
    <asp:Panel runat="server" ID="content">
       <asp:HiddenField runat="server" ID="hfRecordId" ClientIDMode="static" />
       <asp:HiddenField runat="server" ID="hfRecordType" ClientIDMode="static" />
       <asp:HiddenField runat="server" ID="hfJobSiteId" ClientIDMode="Static" />
            <asp:Panel ID="pnlRiskTemplate" runat="server" CssClass="width-full">
            <asp:Panel runat="server" CssClass="width-full center">
                <asp:Image ID="imgEditWait" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
            </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>