﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web.Dialogs
{
    public partial class Contact : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hfContactID.Value = Request["ID"];
            DAL.ClientContact DB = new DAL.ClientContact(AspSession.CurrentUser.ClientDatabaseName);
            if (hfContactID.Value != "-1")
            {
                Types.ClientContact Contact = DB.GetForId(Int32.Parse(hfContactID.Value));

                hfContactType.Value = ((int)Contact.ContactType).ToString();
                txtContactTitle.Text = Contact.Title;
                txtContactForename.Text = Contact.Forename;
                txtContactSurname.Text = Contact.Surname;
                txtContactTelephone.Text = Contact.Telephone;
                txtContactMobile.Text = Contact.Mobile;
                txtContactEmail.Text = Contact.Email;
                //txtContactJobTitle.Text = Contact.JobTitle;
                //txtContactDepartment.Text = Contact.Department;
                txtContactStreetNumber.Text = Contact.StreetNumber;
                txtContactAddress1.Text = Contact.Address1;
                txtContactAddress2.Text = Contact.Address2;
                txtContactTown.Text = Contact.Town;
                txtContactCounty.Text = Contact.County;
                txtContactPostCode.Text = Contact.PostCode;
                //DGSAP-855
                chkSendSmsContact.Checked = Contact.SendSmsMessage;
            }
            else
            {
                hfContactType.Value = ((int)Types.Enums.ContactTypeEnum.General).ToString();
            }
        }
    }
}