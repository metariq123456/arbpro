﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BookDatesEdit.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.BookDatesEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmBookDatesEdit" runat="server">
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
            <script type="text/javascript" src="Content/js/RiskAssessment.js?V=3.23.5"></script>
            <script type="text/javascript" src="Content/js/BookJobDatesEdit.js?V=3.23.5"></script>
            <asp:Panel ID="Panel10" runat="server" style="position: relative;">
            <asp:Panel runat="server" ID="pnlUserMainForm" CssClass="userMainForm" Width="80%" style="margin: auto;position: absolute;left:0;right: 0;top: 0;bottom: 0;">
                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlDlgClndrError" CssClass="warning-text center">
                    <!-- This panel will be used to display error messages to the user -->
            </asp:Panel>
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlDlgClndrItmError" CssClass="warning-text center">
                 <!-- DGSAP-841 - this panel will be used to inform the client on whether he hasn't selected all of the items -->
            </asp:Panel>
                <asp:Panel runat="server" ID="Panel11" CssClass="calendarFilters white" style="overflow:hidden">
                    <asp:Panel ID="Panel12" runat="server" CssClass="filterHeader">
                        Job Booking Form
                    </asp:Panel>
                    <asp:Panel ID="Panel1" runat="server" CssClass="float-clear width-full">
            <asp:Panel runat="server" ID="pnlJobDetails" CssClass="workJobTabs align-left ap-draggable" Style="margin-bottom: 50px;">
            <asp:Panel ID="pnlJobWorkflow" runat="server" CssClass="colored-raise jobHeader">
                <asp:Label ID="lblJobWorkflow" runat="server">
                    Job Workflow
                </asp:Label>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlJobDetailsTabs" CssClass="jobTabBar">
                <asp:HyperLink runat="server" ID="tabDates_1" ClientIDMode="Static" CssClass="workJobTab activeTab" NavigateUrl="javascript:Dates_curTab=1;SwitchDatesTab(1);">
                    Job Booking
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabDates_2" ClientIDMode="Static" CssClass="workJobTab" NavigateUrl="javascript:Dates_curTab=2;SwitchDatesTab(2);">
                    Dates & Time
                </asp:HyperLink>
                
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlJobDetailsContent" CssClass="width-full">
                    <asp:Panel runat="server" ID="pnlDates_1" ClientIDMode="Static">
                            <asp:Panel ID="Panel8" runat="server" Height="25px">
                                <asp:HiddenField runat="server" ID="hfQuoteStatus" ClientIDMode="Static" />
                                    <asp:Panel ID="Panel9" runat="server" CssClass="companyTabBar">
                                        <asp:HyperLink runat="server" ID="tabWorkJobStage_1" ClientIDMode="Static" CssClass="workJobTab activeTab" NavigateUrl="javascript:Booking_curTab = 1; SwitchBookingTab(1);">
                                Staff
                                        </asp:HyperLink>
                                        <asp:HyperLink runat="server" ID="tabWorkJobStage_2" ClientIDMode="Static" CssClass="workJobTab " NavigateUrl="javascript:Booking_curTab = 2; SwitchBookingTab(2);">
                                Tools
                                        </asp:HyperLink>
                                        <asp:HyperLink runat="server" ID="tabWorkJobStage_3" ClientIDMode="Static" CssClass="workJobTab " NavigateUrl="javascript:Booking_curTab = 3; SwitchBookingTab(3);">
                                Comments
                                        </asp:HyperLink>
                                         <asp:HyperLink runat="server" ID="tabWorkJobStage_4" ClientIDMode="Static" CssClass="workJobTab " NavigateUrl="javascript:Booking_curTab = 4; SwitchBookingTab(4);">
                                 Associated Docs
                                        </asp:HyperLink>
                                         <asp:HyperLink runat="server" ID="tabWorkJobStage_5" ClientIDMode="Static" CssClass="workJobTab " NavigateUrl="javascript:Booking_curTab = 5; SwitchBookingTab(5);">
                                Map
                                        </asp:HyperLink>
                                         <asp:HyperLink runat="server" ID="tabWorkJobStage_6" ClientIDMode="Static" CssClass="workJobTab " NavigateUrl="javascript:Booking_curTab = 6; SwitchBookingTab(6);">
                               Risk Assessment
                                        </asp:HyperLink>
                                    </asp:Panel>
                        </asp:Panel>
                         <asp:Panel runat="server" ID="pnlWorkJobDetailsContent" CssClass="width-full">
                            <asp:Panel runat="server" ID="pnlWorkJobStage_1" ClientIDMode="Static" CssClass="activeTab">
                                <%--<asp:Panel ID="pnlWorkJobLoad2" runat="server" CssClass="center width-full">
                                    <asp:Image ID="imgWorkJobLoad2" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                </asp:Panel>--%>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlWorkJobStage_2" ClientIDMode="Static" CssClass="hidden">
                               <%-- <asp:Panel ID="pnlWorkJobLoad3" runat="server" CssClass="center width-full">
                                    <asp:Image ID="imgWorkJobLoad3" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                </asp:Panel>--%>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlWorkJobStage_3" ClientIDMode="Static" CssClass="hidden">
                                <%--<asp:Panel ID="pnlWorkJobLoad4" runat="server" CssClass="center width-full">
                                    <asp:Image ID="imgWorkJobLoad4" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                </asp:Panel>--%>


                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlWorkJobStage_4" ClientIDMode="Static" CssClass="hidden">
                                <asp:Panel ID="pnlWorkJobLoad5" runat="server" CssClass="center width-full">
                                    <asp:Image ID="imgWorkJobLoad5" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                </asp:Panel>

                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlWorkJobStage_5" ClientIDMode="Static" CssClass="hidden">
                                <asp:Panel ID="pnlError" runat="server" ClientIDMode="Static" CssClass="white width-full"></asp:Panel>
                                    <asp:Panel runat="server" ID="pnlMapControls" CssClass="width-full" ClientIDMode="Static" Height="35px">
                                        <asp:Button ID="btnPrintMap" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-right" Text="Print Map" Width="160px" OnClientClick="PrintQuoteItemMap(); return false;" />
                                    </asp:Panel>
                                    <!-- This form only displays the map for the items to be displayed on -->
                                    <asp:Panel ID="pnlItemMapCanvas" runat="server" ClientIDMode="Static" CssClass="white-light width-full" Height="700">
                                        <asp:Image runat="server" ID="imgMapViewer" ClientIDMode="Static" ImageUrl="~/Images/load_Big.gif" style="height: 100%; width: auto"/>
                                    </asp:Panel>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlWorkJobStage_6" ClientIDMode="Static" CssClass="hidden">
                                <asp:Panel ID="pnlAssessmentDetailsSection" runat="server" CssClass="riskInfoSmlOnBookWork">
                                    <asp:HiddenField runat="server" ID="hfJobID" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfRiskID" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfQuoteID" ClientIDMode="Static" />
                                    <asp:HiddenField runat="server" ID="hfAppId" ClientIDMode="Static" />
                                    <asp:Panel runat="server"
                                        ID="pnlClientRiskAssessmentList"
                                        ClientIDMode="Static"
                                        CssClass="jobsList list hidden">
                                        <asp:Panel ID="Panel15" runat="server" CssClass="center width-full">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                        </asp:Panel>
                                    </asp:Panel>
                                    <asp:Panel ID="pnlAssessmentDetailsHeader" runat="server" CssClass="colored-raise dashboardHeader">
                                        <asp:Label ID="lblAssessmentDetails" runat="server">
                                                Risk Assessment Details
                                        </asp:Label>
                                    </asp:Panel>

                                    <asp:Panel ID="pnlTabDetailsHeaders" runat="server" CssClass="riskTabBar">
                                        <asp:HyperLink runat="server" ID="tabDetails1" ClientIDMode="Static" CssClass="jobTab activeTab" NavigateUrl="javascript:Details_curTab = 1; SwitchDetailsTab();">
                                                Client
                                        </asp:HyperLink>
                                        <asp:HyperLink runat="server" ID="tabDetails2" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:Details_curTab = 2; SwitchDetailsTab();">
                                                Site
                                        </asp:HyperLink>
                                        <asp:HyperLink runat="server" ID="tabDetails3" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:Details_curTab = 3; SwitchDetailsTab();">
                                                Emergency
                                        </asp:HyperLink>
                                        <asp:HyperLink runat="server" ID="tabDetails4" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:Details_curTab = 4; SwitchDetailsTab();">
                                                Staff
                                        </asp:HyperLink>
                                        <asp:HyperLink runat="server" ID="tabDetails5" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:Details_curTab = 5; SwitchDetailsTab();">
                                                Dates
                                        </asp:HyperLink>
                                    </asp:Panel>

                                <asp:Panel runat="server" ID="pnlAssessmentDetailTabs" ClientIDMode="static" CssClass="width-full">
                                    <asp:Panel ID="grdTabDetails1" runat="server" CssClass="width-full">
                                        <asp:Panel ID="Panel2" runat="server" CssClass="center width-full">
                                            <asp:Image ID="Image6" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                        </asp:Panel>
                                    </asp:Panel>
                                    <asp:Panel ID="grdTabDetails2" runat="server" CssClass="width-full hidden">
                                        <asp:Panel ID="Panel3" runat="server" CssClass="center width-full">
                                            <asp:Image ID="Image7" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                        </asp:Panel>
                                    </asp:Panel>
                                    <asp:Panel ID="grdTabDetails3" runat="server" CssClass="width-full hidden">
                                        <asp:Panel ID="Panel4" runat="server" CssClass="center width-full">
                                            <asp:Image ID="Image8" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                        </asp:Panel>
                                    </asp:Panel>
                                    <asp:Panel ID="grdTabDetails4" runat="server" CssClass="width-full hidden">
                                        <asp:Panel ID="Panel5" runat="server" CssClass="center width-full">
                                            <asp:Image ID="Image9" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                        </asp:Panel>
                                    </asp:Panel>
                                    <asp:Panel ID="grdTabDetails5" runat="server" CssClass="width-full hidden">
                                        <asp:Panel ID="Panel6" runat="server" CssClass="center width-full">
                                            <asp:Image ID="Image10" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                        </asp:Panel>
                                    </asp:Panel>
                       
                        </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlAssessmentManagementSection" runat="server" CssClass="riskInfoSmlOnBookWork">
                    <asp:Panel ID="pnlAssessmentManagementHeader" runat="server" CssClass="colored-raise dashboardHeader">
                        <asp:Label ID="lbllAssessmentManagement" runat="server">
                                Risk Management
                        </asp:Label>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlAssessmentManagementTabs" ClientIDMode="static" CssClass="width-full">
                        <asp:Panel ID="Panel7" runat="server" CssClass="center width-full">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlAssessmentSections" runat="server" CssClass="riskInfoLrgOnBookWork">
                    <asp:Panel ID="pnlAssessmentSectionsHeader" runat="server" CssClass="colored-raise dashboardHeader">
                        <asp:Label ID="lblAssessmentSections" runat="server">
                                Risk Assessment Sections
                        </asp:Label>
                        <asp:Button runat="server"
                                ID="btnNewPrint" ClientIDMode="Static"
                                CssClass="button-thin float-right padded"
                                Text="Print" OnClientClick="DisplayRiskAssessmentPDF(); return false;" />
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlAssessmentSectionsTabs" ClientIDMode="static" CssClass="width-full">
                        <asp:Panel ID="Panel13" runat="server" CssClass="center width-full">
                            <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel ID="pnlAdditionalRisksSection" runat="server" CssClass="riskInfoLrgOnBookWork">
                    <asp:Panel ID="pnlAdditionalRisksHdr" runat="server" CssClass="colored-raise dashboardHeader">
                        <asp:Label ID="lblAdditionalRisks" runat="server">
                                Additional Risks Assessed
                        </asp:Label>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlAdditionalRisks" ClientIDMode="static" CssClass="width-full center">
                        <asp:Panel ID="Panel14" runat="server" CssClass="center width-full">
                            <asp:Image ID="Image5" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlAddAdditionalRisks" ClientIDMode="static" CssClass="width-full center">
                            <asp:Button runat="server" ID="btnEdit" ClientIDMode="Static" CssClass="button-basic float-right padded" Text="Add" OnClientClick="AddAdditionalRisk(); return false;" />
                    </asp:Panel>
                </asp:Panel>
              </asp:Panel>
           </asp:Panel>
                    
                   
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlDates_2" ClientIDMode="Static" CssClass="hidden">
                            <asp:Panel ID="pnlCalander" runat="server" CssClass="float-left" Width="60%" Style="margin-right: 10px;">
                                    <asp:Panel runat="server" ID="pnlShowCalendar" ClientIDMode="Static" CssClass="width-full bookDatesPanelDivider">
                                        <asp:Panel ID="Panel16" runat="server" CssClass="float-left"  ClientIDMode="Static" Width="54%">
                                            <asp:Panel ID="multidates" runat="server" ClientIDMode="Static">
                                            </asp:Panel>
                                        </asp:Panel>
                                         <asp:Panel ID="Panel17" runat="server" ClientIDMode="Static" Width="45%" CssClass="float-left">
                                            
                                             <h3>Recurring Dates</h3>
                                             <p>For repeate work,tick the recurring tick box below and enter the dates this work
                                                recurrs on in the future.</p>
                                              <asp:Panel ID="pnlRecurringButton" runat="server" ClientIDMode="Static" Width="100%">
                                              </asp:Panel>
                                         </asp:Panel>
                                         <asp:Panel ID="Panel18" runat="server"  CssClass="float-clear"></asp:Panel>
                                        <asp:Panel ID="pnlTimes" runat="server" ClientIDMode="Static" Width="100%" style="margin-top:30px;margin-left: 10px;">
                                            <p>Select the times you want the job to start and end each day.</p>
                                            <asp:Panel runat="server" ID="pnlTimesForWork" ClientIDMode="Static">
                                            </asp:Panel>
                                        </asp:Panel>
                                        <asp:Button ID="btnSave" ClientIDMode="Static" runat="server" CssClass="button-basic padded float-right" Text="Delete" OnClientClick="if(confirm('Do you want to delete this booked work?', 'Confirm deletion')){CalendarBookWork_Delete(); return false;}" />
                                    </asp:Panel>
                                    
                                </asp:Panel>
                            <asp:Panel ID="pnlWorkItems" runat="server" CssClass="float-left" Width="35%" ClientIDMode="Static" Style="margin-right: 10px;">
                                <asp:Panel runat="server" ID="pnlClientInformation" ClientIDMode="Static" CssClass="width-full bookDatesPanelDivider">
                                    <asp:Label ID="lblOverdueInvoices" runat="server" CssClass="detailLabel" ClientIDMode="Static" Style="float: left; padding-left: 10px;">
                                        DATES
                                    </asp:Label>
                                    <asp:Label ID="Label2" runat="server" ClientIDMode="Static" Style="float: left; padding-left: 10px;">
                                        Use your mouse to select calander dates to book the work items detailed below
                                    </asp:Label>
                                   
                                    <asp:Panel runat="server" ID="pnlWorkItemTable" ClientIDMode="Static" Style="float: left; padding-left: 10px;">
                                    </asp:Panel>
                                </asp:Panel>
                            </asp:Panel>  
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
                </asp:Panel>
                
            </asp:Panel>
     </asp:Panel>
    
    <script>

        $('#multidates').multiDatesPicker();
        $(function () {
            Load_BookJobDatesEdit();
        });

    </script>
        </asp:Panel>
    </form>
</body>
</html>
