﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalendarEditAppointment.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.EditAppointmentCalendar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" Style="overflow: hidden; padding-left: 10px;">
            <asp:HiddenField runat="server" ID="hfAppSurveyor" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfAppId" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfAppStart" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfAppEnd" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfJobSiteId" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfTemplateId" ClientIDMode="Static" />
            <asp:Panel runat="server" CssClass="width-full center">
                <asp:Panel ID="pnlErr" runat="server" CssClass="hidden">
                    <asp:Label runat="server" ID="lblErr" Text="" CssClass="clientLabel" />
                </asp:Panel>
                <asp:Label ID="lblSite" runat="server" CssClass="clientLabel" Text="Site:" />
                <asp:DropDownList runat="server"
                    ID="ddlClientSites" ClientIDMode="Static"
                    CssClass="clientInput"
                    onchange="SiteChanged();">
                </asp:DropDownList>
                <asp:Label runat="server" CssClass="clientLabel">Surveyor: </asp:Label>
                <asp:DropDownList runat="server"
                    ID="ddlSurveyors" ClientIDMode="Static"
                    CssClass="clientInput"
                    onchange="SurveyorChanged();">
                </asp:DropDownList>
                <br/>
                <asp:Label ID="Label1" runat="server" CssClass="clientLabel">Risk Assessment Template: </asp:Label>
                <asp:DropDownList runat="server"
                    ID="ddlTemplate" ClientIDMode="Static"
                    CssClass="clientInput"
                    onchange="TemplateChanged();">
                </asp:DropDownList>
                <asp:Label ID="Label2" runat="server" CssClass="warning-text width-full center">Note that if you have an existing template for the job site it will be used instead. </asp:Label>
                <asp:Panel runat="server" Height="10px" CssClass="width-full center" />
                <asp:CheckBox runat="server" CssClass="clientInput"
                    ID="chkAppointmentCompleted" ClientIDMode="Static"
                    Text="Mark Completed" />
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlCalendar" ClientIDMode="Static">
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
