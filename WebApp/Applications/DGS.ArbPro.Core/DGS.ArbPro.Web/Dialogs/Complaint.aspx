﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Complaint.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.Complaint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmComplaint" runat="server">
    <asp:Panel runat="server" ID="content" ClientIDMode="Static">
            <asp:Panel runat="server" ID="pnlComplaintDlg" CssClass="float-left width-full" ClientIDMode="Static">
                <asp:Panel ID="pnlComplaintDlgTabs" runat="server" CssClass="companyTabBar"  ClientIDMode="Static">
                    <asp:HyperLink runat="server" ID="pnlComplaintDlgTab_0" ClientIDMode="Static" CssClass="companyTab activeTab" NavigateUrl="javascript:DlgComplaint_CurTab = 0; SwitchDlgComplaintTab();">
                            Complainant
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="pnlComplaintDlgTab_1" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:DlgComplaint_CurTab = 1; SwitchDlgComplaintTab();">
                            Details
                    </asp:HyperLink>
                    <asp:HyperLink runat="server" ID="pnlComplaintDlgTab_2" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:DlgComplaint_CurTab = 2; SwitchDlgComplaintTab();">
                            Resolution
                    </asp:HyperLink>
                </asp:Panel>
                <asp:Panel ID="pnlComplaintDlgDetails" runat="server" CssClass="width-full"  ClientIDMode="Static">
                    <asp:Panel ID="pnlComplaintDlgDetail_0" runat="server" CssClass=""  ClientIDMode="Static">
                        <asp:Panel ID="Panel1" runat="server" CssClass="center">
                            <asp:Image ID="cmpImage1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlComplaintDlgDetail_1" runat="server" CssClass="hidden"  ClientIDMode="Static">
                        <asp:Panel ID="Panel2" runat="server" CssClass="center">
                            <asp:Image ID="cmpImage2" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel ID="pnlComplaintDlgDetail_2" runat="server" CssClass="hidden"  ClientIDMode="Static">
                        <asp:Panel ID="Panel3" runat="server" CssClass="center">
                            <asp:Image ID="cmpImage3" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
