﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalendarGenericEdit.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.CalendarGenericEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField runat="server" ID="hfEventId" ClientIDMode="Static" />
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
            <asp:TextBox runat="server" ID="txtEventName"
                ClientIDMode="Static" Width="98%"
                placeholder="Event Name" MaxLength="50"
                Style="margin-top: 5px;"></asp:TextBox>
            <asp:Label runat="server" Text="All Day" />
            <asp:CheckBox runat="server" ID="chkAllDay" OnChange="CheckChanged()" Checked="true" ClientIDMode="Static" />
            <asp:Panel runat="server" Width="100%" ID="pnlTimed">
                <asp:Label runat="server" Width="100%">
                    Dates & Times
                </asp:Label>
                <asp:TextBox runat="server" ID="txtDateFrom"
                    ClientIDMode="static" Width="46%"
                    placeholder="Date From"></asp:TextBox>
                -
                <asp:TextBox runat="server" ID="txtDateTo"
                    ClientIDMode="static" Width="47%"
                    placeholder="Date To"></asp:TextBox>
            </asp:Panel>
            <asp:Panel runat="server" Width="100%" ID="pnlAllDay">
                <asp:Label runat="server" Width="100%">
                    Dates
                </asp:Label>
                <asp:TextBox runat="server" ID="txtDayFrom"
                    ClientIDMode="static" Width="46%"
                    placeholder="Date From"></asp:TextBox>
                -
                <asp:TextBox runat="server" ID="txtDayTo"
                    ClientIDMode="static" Width="47%"
                    placeholder="Date To"></asp:TextBox>
            </asp:Panel>
            <asp:TextBox runat="server" ID="txtEventComment"
                ClientIDMode="Static" Width="98%"
                placeholder="Comment" TextMode="MultiLine"></asp:TextBox>
            <asp:DropDownList runat="server" ID="ddlEventType_DLG"
                ClientIDMode="Static" Width="99%">
                <asp:ListItem Value="0">Bank Holiday</asp:ListItem>
                <asp:ListItem Value="1">Company Holiday</asp:ListItem>
                <asp:ListItem Value="2">Staff Holiday</asp:ListItem>
                <asp:ListItem Value="3">Event</asp:ListItem>
                <asp:ListItem Value="4">Staff Meeting</asp:ListItem>
                <asp:ListItem Value="5">Meeting</asp:ListItem>
                <asp:ListItem Value="6">Interview</asp:ListItem>
                <asp:ListItem Value="7">Other</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList runat="server" ID="ddlEventStaff_DLG"
                ClientIDMode="Static" Width="99%">
                <asp:ListItem>Staff Names Here</asp:ListItem>
            </asp:DropDownList>
        </asp:Panel>
    </form>
</body>
</html>
