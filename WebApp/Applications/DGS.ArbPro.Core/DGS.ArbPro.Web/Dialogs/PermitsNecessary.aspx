﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PermitsNecessary.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.PermitsNecessary" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmPermitsNecessary" runat="server">
        <asp:Panel runat="server" ID="content"  CssClass="width-full white-light">
           <asp:HiddenField runat="server" ID="hfQuoteId" ClientIDMode="static" />
           <asp:Panel ID="pnlPermitsDetails" runat="server" CssClass="width-full">
            <asp:Panel runat="server" CssClass="center width-full white">
                <asp:Image ID="imgEditWait" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
            </asp:Panel>
           </asp:Panel>
        </asp:Panel>
        
    </form>
</body>
</html>
