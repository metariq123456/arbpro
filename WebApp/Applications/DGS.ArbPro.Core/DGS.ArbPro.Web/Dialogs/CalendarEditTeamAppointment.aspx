﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalendarEditTeamAppointment.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.CalendarEditTeamAppointment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" Style="overflow: hidden; padding-left: 10px;">
            <asp:HiddenField runat="server" ID="hfAppTeam" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfAppId" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfAppStart" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfAppEnd" ClientIDMode="Static" />
            <asp:Panel runat="server" CssClass="width-full left"><br />
                <asp:Label runat="server" CssClass="clientLabel">Team: </asp:Label>
                <asp:Label runat="server" CssClass="clientLabel" ID="lblTeamName"></asp:Label>
                <asp:Panel runat="server" Height="10px" Width="20px" />
                <asp:Panel runat="server" Width="100%" ID="pnlTimed">
                    <asp:Label ID="Label1" runat="server" Width="100%">
                        Dates & Times
                    </asp:Label>
                    <asp:TextBox runat="server" ID="txtDateFrom"
                        ClientIDMode="static" Width="95%"
                        placeholder="Date From"></asp:TextBox><br /><br />
                    <asp:TextBox runat="server" ID="txtDateTo"
                        ClientIDMode="static" Width="95%"
                        placeholder="Date To"></asp:TextBox>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlCalendar" ClientIDMode="Static">
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
