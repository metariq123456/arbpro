﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Templates.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.Templates" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="<%= AppRoot %>/Content/Site.css" />
    <title></title>
    <style type="text/css">
        .width-full {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" CssClass="center">
            <asp:Panel runat="server" ID="Panel1">
                <asp:Panel ID="Panel2" runat="server" CssClass="float-left border-right" Width="30%">
                    <asp:Panel runat="server" ID="pnlTableContainer" Height="100%" CssClass="float-left width-full">
                        <asp:Label ID="Label1" runat="server" CssClass="templateLabel" Text="Available Templates"></asp:Label>
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                    </asp:Panel>
                    <asp:Panel ID="pnlButtonNew" runat="server" CssClass="float-left width-full">
                        <asp:Button runat="server" ID="btnNewItem" Text="New" CssClass="float-right button-thin" OnClientClick="NewTemplate();" />
                    </asp:Panel>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlItemControls" CssClass="clientField float-left width-full hidden" Width="60%">
                    <asp:Panel ID="pnlType" runat="server" HorizontalAlign="Left" Width="100%">
                        <asp:Label runat="server" Text="Type:" CssClass="companyLabel" Width="30%" />
                        <asp:DropDownList runat="server" ID="drpQuoteInvoice" Width="100px" OnChange="DropDownChanged();" ClientIDMode="Static">
                            <asp:ListItem Value="0" Text="Quote" />
                            <asp:ListItem Value="1" Text="Invoice" />
                        </asp:DropDownList>
                    </asp:Panel>
                    <asp:Panel runat="server" HorizontalAlign="Left">
                        <asp:Label ID="lbl" runat="server" Text="Name:" Width="30%" />
                        <asp:TextBox ID="txtTemplateName" runat="server" ToolTip="Template Name" PlaceHolder="Template Name" Width="67%" />
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlSalutations" Width="100%" HorizontalAlign="Left">
                        <asp:Label runat="server" ID="lblSalutation" Text="Subject:" Width="30%" />
                        <asp:TextBox runat="server" ID="txtSalutations" ToolTip="Salutaion" PlaceHolder="Subject" Width="67%" />
                    </asp:Panel>
                    
                    <asp:Label runat="server" ID="lblTags" Text="Useful Tags:" CssClass ="hidden align-left" />
                    <asp:Panel runat="server" ID="pnlSmsTags" HorizontalAlign="Left" CssClass ="hidden">
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlContentText" HorizontalAlign="Left">
                        <asp:Label runat="server" ID="lblContentt" Text="Content:" />
                    </asp:Panel>
                    <asp:TextBox runat="server" ID="txtContentText" ToolTip="Content Text" PlaceHolder="Content" Height="200px" CssClass="width-full" TextMode="MultiLine" Width="100%" />
                    <asp:Label runat="server" ID="lblSmsLength" CssClass="hidden" Text="SMS message is too long. Max Length 150 characters."></asp:Label>

                    <asp:Panel runat="server" ID="pnlInvoicePaymentInfo" CssClass="hidden" HorizontalAlign="Left">
                        <asp:Panel runat="server" ID="pnlPaymentInfoLbl">
                            <asp:Label runat="server" ID="lblPaymentInfo" Text="Payment Information:" />
                        </asp:Panel>
                        <asp:TextBox runat="server" ID="txtPaymentInfo" ToolTip="Payment Information" PlaceHolder="Payment Information" CssClass="width-full" TextMode="MultiLine" />
                    </asp:Panel>

                    <asp:Panel runat="server" ID="pnlItemButtons" CssClass="center">
                        <br />
                        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button-thin" OnClientClick="SaveTemplate();" />
                        <asp:Button runat="server" ID="btnDelete" Text="Delete" CssClass="button-thin" OnClientClick="DeleteTemplate();" />
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlComingSoon" CssClass="center hidden">
                    <asp:Label runat="server" ID="lblComingSoon" Text="Coming Soon" />
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlNoResults" CssClass="center hidden">
                    <asp:Label runat="server" ID="lblNoResults" Text="No Results" />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
