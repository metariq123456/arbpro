﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalendarBookJob.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.CalendarBookJob" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmCalendarBookJob" runat="server">
        <asp:Panel runat="server" ID="content" Style="overflow: hidden; padding-left: 10px;">
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlDlgClndrError" CssClass="warning-text center">
                <!-- This panel will be used to display error messages to the user -->
            </asp:Panel>
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlDlgClndrAppointment" CssClass="width-full float-left">
                <!-- This panel will be used to display dynamically created controls to the user -->
                <!-- The following table will be replaced once content has been generated -->
                <asp:Table runat="server" ID="tblDlgClndrWait" ClientIDMode="Static" CssClass=" float-left width-full center">
                    <asp:TableRow ID="TableRow1" runat="server">
                        <asp:TableCell ID="TableCell1" runat="server">Please Wait, Loading</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow ID="TableRow2" runat="server">
                        <asp:TableCell ID="TableCell2" runat="server" CssClass="width-full">
                            <asp:Image ID="imgDlgClndrWait" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
