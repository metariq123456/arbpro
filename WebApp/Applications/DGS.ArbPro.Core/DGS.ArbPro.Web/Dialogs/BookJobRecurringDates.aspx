﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BookJobRecurringDates.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.BookJobRecurringDates" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmRecurringDates" runat="server">
     <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
         <asp:Panel runat="server" ID="pnlBookJobRecurringDates" ClientIDMode="Static"  Style="float: left; padding-left: 10px;">
         </asp:Panel>    
     </asp:Panel>
    </form>
</body>
</html>
