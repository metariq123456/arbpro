﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobItemEdit.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.JobItemEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px; overflow: hidden;">
            <asp:HiddenField runat="server" ID="hfStage" ClientIDMode="static" />
            <asp:Panel runat="server" Style="margin-right: 10px; margin-top: 10px;">
                <asp:Table runat="server"
                    CssClass="searchGrid width-full"
                    Style="font-size: 0.7em;">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell Width="7%" Style="text-align: center;">
                            Item Order
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="7%" Style="text-align: center;">
                            Tree No.
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="18%" Style="text-align: center;">
                            Tree Name
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="38%" Style="text-align: center;">
                            Description
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="13%" Style="text-align: center;">
                            Value
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell Width="12%" Style="text-align: center;">
                            VAT Rate
                        </asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                    <asp:TableRow>
                        <asp:TableCell Width="7%">
                            <asp:TextBox runat="server"
                                ID="txtNewItemOrderId" ClientIDMode="Static"
                                CssClass="jobInput float-right"
                                Width="90%" TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell Width="7%">
                            <asp:TextBox runat="server"
                                ID="txtNewItemTreeNo" ClientIDMode="Static"
                                CssClass="jobInput width-full"
                                Width="85%" TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell Width="18%">
                            <asp:TextBox runat="server"
                                ID="txtNewItemTreeName" ClientIDMode="Static"
                                CssClass="jobInput width-full"
                                Width="90%" TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell Width="38%">
                            <asp:TextBox runat="server"
                                ID="txtNewItemDescription" ClientIDMode="Static"
                                CssClass="jobInput width-full"
                                Width="95%" TextMode="MultiLine" />
                        </asp:TableCell>
                        <asp:TableCell Width="13%">
                            <asp:TextBox runat="server"
                                ID="txtNewItemValue" ClientIDMode="Static"
                                CssClass="jobInput float-right"
                                Width="80%" TextMode="MultiLine" />
                            <asp:Label ID="Label1" runat="server"
                                CssClass="float-right"
                                Style="padding-top: 7px; padding-right: 4px;">
                            </asp:Label>
                        </asp:TableCell>
                        <asp:TableCell Width="12%">
                            <asp:Label ID="Label2" runat="server"
                                CssClass="float-right"
                                Style="padding-top: 7px; padding-right: 4px;">
                                %
                            </asp:Label>
                            <asp:TextBox runat="server"
                                ID="txtNewItemVatRate" ClientIDMode="Static"
                                CssClass="jobInput float-right"
                                Width="50%" TextMode="MultiLine" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
