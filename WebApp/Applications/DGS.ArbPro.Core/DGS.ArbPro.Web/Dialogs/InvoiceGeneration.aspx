﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="InvoiceGeneration.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.InvoiceGeneration" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="frmInvoiceGeneration" runat="server">
        <!-- This panel is the content div that wraps all controls -->
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
            <!-- This panel will be used to display the tab bar for users to access create or send functionality-->
            <asp:Panel runat="server" ID="pnlInvNavigation" ClientIDMode="Static" CssClass="pdfTabBar">
                <asp:Label ID="lblPdfQuoteNo" runat="server" CssClass="clientLabel white float-right"  Width="100px" ClientIDMode="Static">Pending</asp:Label>
                <asp:Label ID="lblForPdfQuoteNo" runat="server" CssClass="clientLabel float-right" ClientIDMode="Static">Invoice for Quote #: </asp:Label>
                <asp:HyperLink runat="server" ID="tabInvPdf_0" ClientIDMode="Static" CssClass="pdfTab activeTab" NavigateUrl="javascript:SwitchInvPdfTab(0);">
                    Generate
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabInvPdf_1" ClientIDMode="Static" CssClass="pdfDisabledTab" NavigateUrl="javascript:SwitchInvPdfTab(1);">
                    Send
                </asp:HyperLink>
            </asp:Panel>
            <!-- This panel will be used as a wrapper for all details panels to be displayed in-->
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlInvPdfWrapper" CssClass="width-full float-left" Height="90%">
                <!-- This panel will be used to display creation / edit functionality-->
                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlInvPdf_0" CssClass="width-full float-left height-full">
                    <!-- This table will be destroyed when the invoice load process is completed-->
                    <asp:Table CssClass="width-full" runat="server" >
                        <asp:TableRow runat="server">
                            <asp:TableCell runat="server" CssClass="center">
                                <asp:Label runat="server">Loading Invoice Details. Please Wait</asp:Label>
                            </asp:TableCell>
                            <asp:TableCell runat="server" CssClass="center">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <!-- This panel will be used to display send functionality-->
                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlInvPdf_1" CssClass="hidden">
                    <!-- This panel will be used to update the user on any errors or failures on the PDF page-->
                    <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFError" CssClass="width-full float-left">
                    </asp:Panel>
                    <!-- This panel is used to contain all the page controls -->
                    <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFConent" CssClass="width-full height-full float-left">
                        <!-- This panel is used to contain the settings for the generation and emailing of the PDF -->
                        <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFSettings" CssClass="lmScrollPanel">
                            <asp:Panel ID="pnlPDFDropdown" runat="server">
                                <!-- PDF Letter generation header -->
                                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFHeader_0" CssClass="lmHeader" onclick="ToggleDsiplay(0);">
                                    <asp:Label ID="lblHeaderTitle_0" runat="server" CssClass="lmHeaderText  float-left">PDF Generation</asp:Label>
                                    <asp:Label runat="server" CssClass="lmHeaderText lmHeaderToggle" ID="lblHeaderToggle_0" ClientIDMode="Static">-</asp:Label>
                                </asp:Panel>
                                <!-- PDF Letter generation body -->
                                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFBody_0">
                                    <asp:Table ID="Table1" runat="server" CssClass="width-full center">
                                        <asp:TableRow ID="TableRow1" runat="server">
                                            <asp:TableCell ID="TableCell1" runat="server">Loading PDF Settings Please Wait</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="TableRow2" runat="server">
                                            <asp:TableCell ID="TableCell2" runat="server">
                                                <asp:Image ID="imgPDFSettings" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                                <asp:Panel runat="server" ID="warningMessage" ClientIDMode="Static">
                                </asp:Panel>
                            </asp:Panel>
                            <asp:Panel ID="Panel2" runat="server">
                                <!-- PDF Email generation header -->
                                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFHeader_1" CssClass="lmHeader" onclick="ToggleEmailOptionDisplay(1);">
                                    <asp:Label ID="lblHeaderTitle_1" runat="server" CssClass="lmHeaderText  float-left">Email Options</asp:Label>
                                    <asp:Label runat="server" CssClass="lmHeaderText lmHeaderToggle" ID="lblHeaderToggle_1" ClientIDMode="Static">-</asp:Label>
                                </asp:Panel>
                                <!-- PDF Email generation body -->
                                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFBody_1">
                                    <asp:Table ID="Table2" runat="server" CssClass="width-full center">
                                        <asp:TableRow ID="TableRow3" runat="server">
                                            <asp:TableCell ID="TableCell3" runat="server">Loading Email Settings Please Wait</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="TableRow4" runat="server">
                                            <asp:TableCell ID="TableCell4" runat="server">
                                                <asp:Image ID="imgPDFEmail" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                            </asp:Panel>
                            <!-- DGSAP-855 -->
                            <asp:Panel ID="Panel3" runat="server">
                                 <!-- SMS generation header -->
                                 <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFHeader_2" CssClass="lmHeader" onclick="ToggleEmailOptionDisplay(2);">
                                    <asp:Label ID="lblHeaderTitle_2" runat="server" CssClass="lmHeaderText  float-left">SMS Options</asp:Label>
                                    <asp:Label runat="server" CssClass="lmHeaderText lmHeaderToggle" ID="lblHeaderToggle_2" ClientIDMode="Static">-</asp:Label>
                                </asp:Panel>
                                <!-- SMS generation body -->
                                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFBody_2">
                                    <asp:Table ID="Table4"  runat="server" CssClass="width-full center">
                                        <asp:TableRow ID="TableRow7" runat="server">
                                            <asp:TableCell ID="TableCell7" runat="server">Loading SMS Template Please Wait</asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow ID="TableRow8" runat="server">
                                            <asp:TableCell ID="TableCell8" runat="server">
                                                <asp:Image ID="imgSms" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>
                        <!-- This panel is used to display the generated PDF -->
                        <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFDisplay" CssClass="width-half height-full float-left">
                            <asp:Panel runat="server" ID="pnlLmProcessing" ClientIDMode="Static" CssClass="hidden">
                                <asp:Table ID="Table3" runat="server" CssClass="waitTimer">
                                    <asp:TableRow ID="TableRow5" runat="server">
                                        <asp:TableCell ID="TableCell5" runat="server">Processing Please Wait</asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="TableRow6" runat="server">
                                        <asp:TableCell ID="TableCell6" runat="server">
                                            <asp:Image ID="imgPDFProcessing" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlLmPdfView" ClientIDMode="Static" CssClass="lmPnlLrg">
                                <asp:Panel runat="server" ID="pnlPreview" ClientIDMode="Static" CssClass="lmPnlLrg lmPreview">
                                    <div id="previewToolbar" style="position: absolute; top: 16px; right: 32px; height: 24px; width: 128px; text-align: right; visibility: hidden;">
                                        <button id="btnDownload" class="pdfButton button ui-dialog-button ui-button ui-widget ui-state-default ui-corner-all" style="width: 128px; height: 24px; font-size: 0.8em;" title="Download the PDF">
                                            <span style="color: black;">Download PDF</span>
                                        </button>
                                    </div>
                                    <iframe id="pdfPreview" clientidmode="Static" style="width: 100%; height: 100%;"></iframe>
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <div id="DocAttachModel" class="hidden" style="padding-left: 10px;padding-right:10px;" runat="server"  ClientIDMode="Static">
                 <asp:Panel ID="Panel1" runat="server" ClientIDMode="Static">
                    <span style="color:red">INFORMATION!:</span> Attaching documents, please wait a moment.
                  </asp:Panel>
                 <p>Select the documents you would like to attach to this email</p>
                 <asp:Panel runat="server" ID="grdDoc" ClientIDMode="Static" CssClass="width-full" style="max-height: 400px; overflow-y: scroll;">
                            <asp:Panel ID="loading" runat="server" CssClass="center width-full" ClientIDMode="Static">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" ClientIDMode="Static"/>
                            </asp:Panel>
                            
                 </asp:Panel>
                <p>Select Arb Pro documents you would like to attach to this email</p>
                  <asp:Panel runat="server" ID="grdArbProDoc" ClientIDMode="Static" CssClass="width-full" style="max-height: 400px; overflow-y: scroll;">
                            <asp:Panel ID="arbproLoading" runat="server" CssClass="center width-full" ClientIDMode="Static">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" ClientIDMode="Static"/>
                            </asp:Panel>
                 </asp:Panel>
                   <asp:Panel runat="server" ID="pnlDocloading" ClientIDMode="Static" >
                        <asp:Table  runat="server" CssClass="waitTimer" ClientIDMode="Static">
                            <asp:TableRow   runat="server" ClientIDMode="Static">
                                <asp:TableCell   runat="server" ClientIDMode="Static">Adding document(s) to email.<br/>Please Wait</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow  runat="server" ClientIDMode="Static">
                                <asp:TableCell  runat="server" ClientIDMode="Static">
                                    <asp:Image ID="Image3"  runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" ClientIDMode="Static"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
            </div>
        </asp:Panel>
    </form>
</body>
</html>

