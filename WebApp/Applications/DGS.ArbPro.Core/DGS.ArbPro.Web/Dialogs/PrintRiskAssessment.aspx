﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintRiskAssessment.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.PrintRiskAssessment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmRiskAssessement" runat="server">
         <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
                <!-- This panel is used to display the generated PDF -->
                <asp:Panel runat="server" ClientIDMode="Static" ID="pnlPDFDisplay" CssClass="height-full">
                    <asp:Panel runat="server" ID="pnlLmProcessing" ClientIDMode="Static" CssClass="hidden">
                        <asp:Table ID="Table1" runat="server" CssClass="waitTimer">
                            <asp:TableRow ID="TableRow1" runat="server">
                                <asp:TableCell ID="TableCell1" runat="server">Processing PDF.Please Wait</asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="TableRow2" runat="server">
                                <asp:TableCell ID="TableCell2" runat="server">
                                    <asp:Image ID="imgPDFProcessing" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlLmPdfView" ClientIDMode="Static" CssClass="lmPnlLrg">
                        <asp:Panel runat="server" ID="pnlPreview" ClientIDMode="Static" CssClass="lmPnlLrg lmPreview">
                            <div id="previewToolbar" style="position: absolute; top: 16px; right: 32px; height: 24px; width: 128px; text-align: right; visibility: hidden;">
                                <button id="btnDownload" class="pdfButton button ui-dialog-button ui-button ui-widget ui-state-default ui-corner-all" style="width: 128px; height: 24px; font-size: 0.8em;" title="Download the PDF">
                                    <span style="color: black;">Download PDF</span>
                                </button>
                            </div>
                            <iframe id="pdfPreview" clientidmode="Static" style="width: 100%; height: 100%;"></iframe>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
         </asp:Panel>
    <div>
    
    </div>
    </form>
</body>
</html>
