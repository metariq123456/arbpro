﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TreeProtection.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.TreeProtection" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="frmTreeProtectionOrder" runat="server">
        <asp:Panel runat="server" ID="content"  CssClass="width-full white-light">
            <asp:HiddenField runat="server" ID="hfTpoId" ClientIDMode="static" />
            <asp:HiddenField runat="server" ID="hfQuoteId" ClientIDMode="static" />
            <asp:Panel ID="pnlTpoDetails" runat="server" CssClass="width-full">
            <asp:Panel ID="Panel1" runat="server" CssClass="center width-full white">
                <asp:Image ID="imgEditWait" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
            </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>

