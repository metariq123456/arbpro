﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseOrderNoEdit.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.PurchaseOrderNoEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmPurchaseOrderEdit" runat="server">
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
            <asp:Label ID="LblPONo" runat="server" Width="100%">
                   Please add or amend the purchase order number for this quote
                </asp:Label>
                <p></p>
                <asp:TextBox runat="server" ID="txtPONo"
                    ClientIDMode="static" Width="80%"
                    placeholder="Enter Number here"></asp:TextBox>
        </asp:Panel>
    </form>
</body>
</html>
