﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LetterManagement.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.LetterManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" Style="padding-left: 10px;">
            <asp:HiddenField runat="server" ID="hfInvoiceQuote" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfItemNo" ClientIDMode="Static" />
            <asp:Panel runat="server"
                ID="pnlOptions" ClientIDMode="Static"
                CssClass="lmPanel"
                Style="overflow-y: auto; overflow-x: hidden;">
                <asp:Panel runat="server"
                    ID="pnlLetterOptions" ClientIDMode="Static">
                    <asp:Panel runat="server"
                        ID="pnlLetterHead" ClientIDMode="Static"
                        CssClass="lmHeader"
                        Width="100%"
                        Style="float: left;"
                        onclick="LetterOptionsToggle();">
                        <asp:Label runat="server"
                            CssClass="lmHeaderText"
                            Style="float: left;">
                        PDF Options
                        </asp:Label>
                        <asp:Label runat="server"
                            CssClass="lmHeaderText"
                            ID="lblLetterOptionsToggle" ClientIDMode="Static"
                            Width="10%"
                            Style="float: right; padding-right: 5px; font-weight: bolder; text-align: center;">
                        -
                        </asp:Label>
                    </asp:Panel>
                    <asp:Panel runat="server"
                        ID="pnlLetter" ClientIDMode="Static"
                        CssClass="lmEdit"
                        Width="100%">
                        <asp:Panel runat="server"
                            ID="divLetterOptions" ClientIDMode="Static"
                            Width="100%">
                            <asp:Panel runat="server"
                                CssClass="lmField"
                                Width="100%">
                                <asp:Label runat="server"
                                    AssociatedControlID="ddlLetterType"
                                    CssClass="lmFieldLabel">
                                    Letter Type
                                </asp:Label>
                                <asp:DropDownList runat="server"
                                    CssClass="lmFieldControl"
                                    ID="ddlLetterType" ClientIDMode="Static"
                                    onchange="ShowLetterTemplate();">
                                    <asp:ListItem Value="0">Letter</asp:ListItem>
                                    <asp:ListItem Value="1">Schedule</asp:ListItem>
                                    <asp:ListItem Value="2">Letter & Schedule</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Panel runat="server"
                                    CssClass="lmField"
                                    Width="100%">
                                    <asp:Label runat="server"
                                        AssociatedControlID="ddlLetterRecipient"
                                        CssClass="lmFieldLabel">
                                    Recipient
                                    </asp:Label>
                                    <asp:DropDownList runat="server"
                                        CssClass="lmFieldControl"
                                        ID="ddlLetterRecipient" ClientIDMode="Static" />
                                </asp:Panel>
                                <asp:Panel runat="server"
                                    ID="pnlLetterTemplate" ClientIDMode="Static"
                                    CssClass="lmField"
                                    Width="100%">
                                    <asp:Label runat="server"
                                        AssociatedControlID="ddlLetterTemplate"
                                        CssClass="lmFieldLabel">
                            Letter Text Template
                                    </asp:Label>
                                    <asp:DropDownList runat="server"
                                        CssClass="lmFieldControl"
                                        ID="ddlLetterTemplate" ClientIDMode="Static"
                                        onchange="LoadLetterTemplate();" />
                                    <asp:TextBox runat="server"
                                        ID="txtLetterSalutation" ClientIDMode="Static"
                                        CssClass="lmFieldControl"
                                        Font-Size="Small"
                                        Font-Names="Helvetica" />
                                    <asp:TextBox runat="server"
                                        ID="txtLetterText" ClientIDMode="Static"
                                        TextMode="MultiLine"
                                        CssClass="lmFieldText"
                                        Font-Size="Small"
                                        Font-Names="Helvetica" />
                                    <asp:TextBox runat="server"
                                        ID="txtLetterText2" ClientIDMode="Static"
                                        TextMode="MultiLine"
                                        CssClass="lmFieldText"
                                        Font-Size="Small"
                                        Font-Names="Helvetica" />
                                </asp:Panel>
                                <asp:Panel runat="server"
                                    CssClass="lmField"
                                    Width="100%"
                                    Height="30px">
                                    <asp:Button runat="server"
                                        CssClass="button-basic float-right" 
                                        Style="margin-right: 10px;"
                                        ID="btnPrintLetter" ClientIDMode="Static"
                                        Text="Generate PDF"
                                        OnClientClick="LoadPDF(false); return false;" />
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server"
                        ID="pnlEmailOptions">
                        <asp:Panel runat="server"
                            CssClass="lmHeader"
                            Width="100%"
                            onclick="EmailOptionsToggle();">
                            <asp:Label runat="server"
                                CssClass="lmHeaderText"
                                Style="float: left;">
                        Email Options
                            </asp:Label>
                            <asp:Label runat="server"
                                CssClass="lmHeaderText" ClientIDMode="Static"
                                ID="lblEmailOptionsToggle"
                                Width="10%"
                                Style="float: right; padding-right: 5px; font-weight: bolder; text-align: center;">
                        -
                            </asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server"
                            CssClass="lmEdit"
                            Width="100%">
                            <asp:Panel runat="server"
                                ID="divEmailOptions" ClientIDMode="Static"
                                Width="100%">
                                <asp:Panel runat="server"
                                    CssClass="lmField"
                                    Width="100%">
                                    <asp:Label runat="server"
                                        CssClass="lmFieldLabel">
                            Recipient:
                                    </asp:Label>
                                    <asp:DropDownList runat="server"
                                        CssClass="lmFieldControl"
                                        ID="ddlEmailRecipient" ClientIDMode="Static" />
                                </asp:Panel>
                                <asp:Panel runat="server"
                                    CssClass="lmField"
                                    Width="100%">
                                    <asp:Label runat="server"
                                        AssociatedControlID="ddlEmailTemplate"
                                        CssClass="lmFieldLabel">
                                Email Text Template
                                    </asp:Label>
                                    <asp:DropDownList runat="server"
                                        ID="ddlEmailTemplate" ClientIDMode="Static"
                                        CssClass="lmFieldControl"
                                        onchange="LoadEmailTemplate();" />
                                    <asp:Label runat="server"
                                        AssociatedControlID="txtEmailSubject"
                                        CssClass="lmFieldLabel">
                                Subject
                                    </asp:Label>
                                    <asp:TextBox runat="server"
                                        ID="txtEmailSubject" ClientIDMode="Static"
                                        CssClass="lmFieldControl" Width="54%" />
                                    <asp:TextBox runat="server"
                                        ID="txtEmailText" ClientIDMode="Static"
                                        TextMode="MultiLine"
                                        CssClass="lmFieldText"
                                        Font-Size="Small"
                                        Font-Names="Helvetica" />
                                </asp:Panel>
                                <asp:Panel runat="server"
                                    CssClass="lmField"
                                    Width="100%">
                                    <asp:Panel runat="server"
                                        ID="pnlAttachments" ClientIDMode="Static"
                                        Width="100%">
                                        <asp:Table runat="server"
                                            ID="grdAttachments" ClientIDMode="Static"
                                            Width="94%" CssClass="lmGrid">
                                            <asp:TableHeaderRow>
                                                <asp:TableHeaderCell
                                                    Text="File Name" Width="80%"
                                                    HorizontalAlign="Left" />
                                                <asp:TableHeaderCell
                                                    Text="Attach" Width="20%" />
                                            </asp:TableHeaderRow>
                                        </asp:Table>
                                    </asp:Panel>
                                    <asp:Panel runat="server"
                                        Width="100%">
                                        <asp:FileUpload runat="server"
                                            ID="fuAttachment" ClientIDMode="Static" />
                                        <asp:Button runat="server"
                                            ID="btnUpload" ClientIDMode="Static"
                                            CssClass="button-basic float-right"
                                            Text="Attach" Style="margin-right: 10px;"
                                            OnClientClick="AddAttachment(); return false;" />
                                    </asp:Panel>
                                </asp:Panel>
                                <br />
                                <br />
                                <asp:Panel runat="server"
                                    CssClass="lmField"
                                    Width="100%"
                                    Height="30px">
                                    <asp:Button runat="server"
                                        CssClass="button-basic float-right"
                                        ID="btnSendEmail" ClientIDMode="Static"
                                        Text="Send Email" Style="margin-right: 10px;"
                                        OnClientClick="SendEmail(); return false;" />
                                </asp:Panel>
                            </asp:Panel>
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server"
                ID="pnlPreview" ClientIDMode="Static"
                CssClass="lmPanel lmPreview">
                <iframe id="pdfPreview" clientidmode="Static"
                    style="width: 100%; height: 100%;"></iframe>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
