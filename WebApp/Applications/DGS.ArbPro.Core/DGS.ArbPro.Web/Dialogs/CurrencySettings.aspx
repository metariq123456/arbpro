﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CurrencySettings.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.CurrencySettings" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head runat="server">
    <title></title>
</head>
<body>
    <form id="form2" runat="server">
         <asp:Panel runat="server" ID="content" CssClass="80%" style="padding-left: 20px; padding-right: 20px;">
                        <asp:Panel ID="Panel3" runat="server" CssClass="float-center">
                            <asp:Label runat="server" ID="lblCurrency" Text="Select Currency:" CssClass="companyLabel" />
                        <asp:DropDownList runat="server" ID="ddlCurrencyList" Width="58%" ClientIDMode="Static">
                        <asp:ListItem>Currency List Here</asp:ListItem>
                        </asp:DropDownList>
                        </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>

