﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewClient.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.NewClient" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
    </style>
</head>
<body>
    <form id="frmAbout" runat="server">
        <asp:Panel runat="server" ID="content">
            <asp:HiddenField runat="server" ID="hfNewClientID" ClientIDMode="Static" />
            <asp:Panel runat="server" CssClass="clientField center padded">
                <asp:Panel runat="server">
                    <asp:Button runat="server" ClientIDMode="Static" ID="btnResidentialSelect" Text="Residential" CssClass="button-basic" OnClientClick="ResidentialClick(); return false;" />
                    <asp:Button runat="server" ClientIDMode="Static" ID="btnCommercialSelect" Text="Commercial" CssClass="button-basic" OnClientClick="CommercialClick(); return false;" />
                </asp:Panel>
                
                <asp:Panel runat="server" ID="pnlValidationMsg"></asp:Panel>

                <asp:Panel ID="pnlCommercial" ClientIDMode="Static" runat="server" CssClass="clientField">
                    <asp:TextBox runat="server" ID="txtNewClientCompanyName" MaxLength="100" ClientIDMode="static" CssClass="clientInput" placeholder="Company Name" ToolTip="Company Name" />
                    <asp:TextBox runat="server" ID="txtNewClientCompanySector" MaxLength="100" ClientIDMode="static" CssClass="clientInput" placeholder="Company Sector" ToolTip="Company Sector" />
                </asp:Panel>

                <asp:Panel ID="pnlResidential" ClientIDMode="Static" runat="server" CssClass="clientField">
                    <asp:Panel runat="server" CssClass="width-full float-left">
                        <asp:TextBox runat="server" ID="txtNewClientTitle" MaxLength="50" ClientIDMode="static" CssClass="clientInput float-left" placeholder="Title" ToolTip="Client Title" />
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="width-full">
                        <asp:TextBox runat="server" ID="txtNewClientFirstName" MaxLength="100" ClientIDMode="static" CssClass="clientInput" placeholder="First Name" ToolTip="Client First Name" />
                        <asp:TextBox runat="server" ID="txtNewClientSurname" MaxLength="100" ClientIDMode="static" CssClass="clientInput" placeholder="Surmame" ToolTip="Client Surname" />
                    </asp:Panel>
                </asp:Panel>

                <asp:Panel ID="pnlAltAccountNo" ClientIDMode="Static" runat="server" CssClass="clientField">
                    <asp:TextBox runat="server" ID="txtNewClientAltAccountNo" MaxLength="20" ClientIDMode="Static" CssClass="clientInput" placeholder="Alt. Account No." ToolTip="Alt. Account No." />
                </asp:Panel>

                <asp:Panel ID="MapAndTextFieldsContainer" runat="server" CssClass="clientField">
                    <asp:Panel runat="server" CssClass="float-left">
                        <asp:TextBox runat="server" ID="txtNewClientHouseNameNo" MaxLength="100" ClientIDMode="static" CssClass="clientInput" placeholder="House name/number" ToolTip="House name/number" />
                        <br />
                        <asp:TextBox runat="server" ID="txtNewClientAddress1" MaxLength="100" ClientIDMode="static" CssClass="clientInput" placeholder="Address line 1" ToolTip="Address line 1" />
                        <br />
                        <asp:TextBox runat="server" ID="txtNewClientAddress2" MaxLength="100" ClientIDMode="static" CssClass="clientInput" placeholder="Address line 2" ToolTip="Address line 2" />
                        <br />
                        <asp:TextBox runat="server" ID="txtNewClientTown" MaxLength="100" ClientIDMode="static" CssClass="clientInput" placeholder="Town" ToolTip="Town" />
                        <br />
                        <asp:TextBox runat="server" ID="txtNewClientCounty" MaxLength="100" ClientIDMode="static" CssClass="clientInput" placeholder="County" ToolTip="County" />
                        <br />
                        <asp:TextBox runat="server" ID="txtNewClientPostcode" MaxLength="20" ClientIDMode="static" CssClass="clientInput" placeholder="Postcode" ToolTip="Postcode"
                            onfocus="addressWatcher.watchForChange(this)"
                            onblur="addressWatcher.cancelWatchForChange()"
                            onchange="addressWatcher.changed(this)" />
                    </asp:Panel>
                    <asp:HiddenField runat="server" ID="hfNewClientLatitude" ClientIDMode="Static" />
                    <asp:HiddenField runat="server" ID="hfNewClientLongitude" ClientIDMode="Static" />
                    <asp:Panel ID="pnlMapCanvas" ClientIDMode="Static" runat="server"
                        CssClass="float-right" Width="228px" Height="153px">
                        Error Loading Map
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:TextBox runat="server" ID="txtNewClientTelephone" MaxLength="20" ClientIDMode="static" CssClass="clientInput" placeholder="Telephone" ToolTip="Telephone" />
                    <asp:TextBox runat="server" ID="txtNewClientMobile" MaxLength="20" ClientIDMode="static" CssClass="clientInput" placeholder="Mobile" ToolTip="Mobile" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:TextBox runat="server" ID="txtNewClientEmail" MaxLength="100" ClientIDMode="static" CssClass="clientInput" placeholder="Email" ToolTip="Email" />
                    <asp:TextBox runat="server" ID="txtNewClientWebsite" MaxLength="100" ClientIDMode="static" CssClass="clientInput" placeholder="Website" ToolTip="Website" />
                </asp:Panel>             
                <asp:Panel runat="server" CssClass="clientField" Align="right">
                    <asp:CheckBox runat="server" CssClass="clientInput" ID="chkNewSendSms" ClientIDMode="Static" Text="Send SMS Confirmation of Emails" Width="100%" />
                </asp:Panel>             
                <asp:Panel ID="Panel1" runat="server" CssClass="clientField" Align="right">
                    <asp:CheckBox runat="server" CssClass="clientInput" ID="chkNewIncludeMarketing" ClientIDMode="Static" Text="Include in Marketing" Width="100%" />
                </asp:Panel>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
