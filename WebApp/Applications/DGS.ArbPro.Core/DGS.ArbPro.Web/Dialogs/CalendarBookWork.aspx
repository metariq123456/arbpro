﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CalendarBookWork.aspx.cs" Inherits="DGS.ArbPro.Web.Dialogs.CalendarBookWork" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frmCalendarBookWork" runat="server">
        <asp:Panel runat="server" ID="content" Style="overflow: hidden; padding-left: 10px;">
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlDlgClndrError" CssClass="warning-text center">
                <!-- This panel will be used to display error messages to the user -->
            </asp:Panel>
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlDlgClndrItmError" CssClass="warning-text center">
            <!-- DGSAP-841 - this panel will be used to inform the client on whether he hasn't selected all of the items -->
            </asp:Panel>
            <asp:Panel runat="server" ClientIDMode="Static" ID="pnlDlgClndrAppointment" CssClass="width-full float-left">
                <!-- This panel will be used to display dynamically created controls to the user -->
                <!-- The following table will be replaced once content has been generated -->
                <asp:Table runat="server" ID="tblDlgClndrWait" ClientIDMode="Static" CssClass=" float-left width-full center">
                    <asp:TableRow runat="server">
                        <asp:TableCell runat="server">Please Wait, Loading</asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow runat="server">
                        <asp:TableCell runat="server" CssClass="width-full">
                            <asp:Image ID="imgDlgClndrWait" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
