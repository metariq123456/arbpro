﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class Enquiry : System.Web.UI.Page
    {
        String EnquiryType = null;
        Int32 ClientID = -1;

        protected void Page_Load(object sender, EventArgs e)
        {
            // If Direct Instruction - do not add Job / Quote - Change Form title - Surveyor becomes Staff Member, remove Source

            EnquiryType = Request["Type"];

            if (EnquiryType == null)
            {
                pnlEnquirySales.Visible = false;
            }
            else
            {
                pnlEnquiryTypeButtons.Visible = false;

                if (Request["ClientID"] != null)
                {
                    ClientID = Int32.Parse(Request["ClientID"]);
                    hfClientID.Value = ClientID.ToString();
                }

                if (ClientID == -1)
                {
                    pnlEnquirySalesEdit.Visible = false;
                }
                else
                {
                    pnlEnquirySalesClient.Visible = false;
                    var DB = new DAL.Client(AspSession.CurrentUser.ClientDatabaseName);
                    var Client = DB.GetForClientId(ClientID);

                    switch (Client.AccountType)
                    {
                        case Types.Enums.ClientTypeEnum.Residential:
                            lblEnquiryClientName.Text = Client.Title + ' ' + (Client.Surname.Length > 0 ? Client.Surname : Client.Forename);
                            break;
                        case Types.Enums.ClientTypeEnum.Commercial:
                            lblEnquiryClientName.Text = Client.BusinessName;
                            break;
                    }

                    lblEnquiryClientAddress.Text = String.Format(
                            "{0}{1}{2}{3}{4}{5}",
                            Client.HouseNameNumber.Length > 0 ? Client.HouseNameNumber + " " : "",
                            Client.Address1.Length > 0 ? Client.Address1 + "<br />" : "",
                            Client.Address2.Length > 0 ? Client.Address2 + "<br />" : "",
                            Client.Town.Length > 0 ? Client.Town + "<br />" : "",
                            Client.County.Length > 0 ? Client.County + "<br />" : "",
                            Client.PostCode.Length > 0 ? Client.PostCode + "<br />" : "");
                    if (lblEnquiryClientAddress.Text.Length > 0)
                        lblEnquiryClientAddress.Text = lblEnquiryClientAddress.Text.Substring(0, lblEnquiryClientAddress.Text.Length - 6);

                    lblEnquirySiteAddress.Text = lblEnquiryClientAddress.Text;

                    lblEnquiryClientContactInfo.Text = String.Format(
                        "{0}{1}{2}",
                        Client.Telephone.Length > 0 ? "Tel: " + Client.Telephone + "<br />" : "",
                        Client.Mobile.Length > 0 ? "Mob: " + Client.Mobile + "<br />" : "",
                        Client.Email.Length > 0 ? Client.Email + "<br />" : "");
                    if (lblEnquiryClientContactInfo.Text.Length > 0)
                        lblEnquiryClientContactInfo.Text = lblEnquiryClientContactInfo.Text.Substring(0, lblEnquiryClientContactInfo.Text.Length - 6);

                    var DBS = new DAL.Staff(AspSession.CurrentUser.ClientDatabaseName);
                    var Staff = DBS.GetAll();
                    ddlEnquirySurveyor.DataSource = Staff.Where(s => s.Position == Types.Enums.StaffRoleEnum.Surveyor);
                    ddlEnquirySurveyor.DataTextField = "Name";
                    ddlEnquirySurveyor.DataValueField = "Id";
                    ddlEnquirySurveyor.DataBind();

                    var DBCS = new DAL.ClientSite(AspSession.UserDatabase);
                    var SiteList = DBCS.GetForClient(ClientID);
                    ddlEnquirySite.DataSource = SiteList;
                    ddlEnquirySite.DataTextField = "Line1";
                    ddlEnquirySite.DataValueField = "Id";
                    ddlEnquirySite.DataBind();

                    switch (EnquiryType)
                    {
                        case "Sales":
                            break;
                        case "Direct":
                            break;
                        case "MultiSite":
                            break;
                    }
                }
            }
        }

        protected void btnSales_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.AbsoluteUri + "?Type=Sales");
        }

        protected void btnDirect_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.AbsoluteUri + "?Type=Direct");
        }

        protected void btnMultiSite_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.AbsoluteUri + "?Type=MultiSite");
        }

        protected void btnEnquiyClientOk_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.Url.AbsoluteUri + "&ClientID=" + hfClientID.Value);
        }

        protected void btnCancelSales_Click(object sender, EventArgs e)
        {
            //The user has cancelled the operation so redirect to the Sales type selection
            Response.Redirect("~/Enquiry.aspx");
        }
    }
}