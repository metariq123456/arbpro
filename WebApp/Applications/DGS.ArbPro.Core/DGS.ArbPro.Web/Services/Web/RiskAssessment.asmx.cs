﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DGS.ArbPro.Types;
using DGS.ArbPro.DAL;

namespace DGS.ArbPro.Web.Services.Web
{
    /// <summary>
    /// Summary description for RiskAssessment
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RiskAssessment : System.Web.Services.WebService
    {
        #region Initalizers
        [WebMethod(EnableSession = true)]
        public Types.Risk RiskCreateForReordTypeIdAndTemplate(int templateId, int recordType, int recordId)
        {
            if (recordType < 0 || recordType > 2)
                return BL.RiskAssessmentHandler.CreateRiskAssessmentFromTemplate(AspSession.UserDatabase, -1, Types.Enums.RiskCreateFromObjectType.NotSpecified, recordId);
            else
                return BL.RiskAssessmentHandler.CreateRiskAssessmentFromTemplate(AspSession.UserDatabase, templateId, (Types.Enums.RiskCreateFromObjectType)recordType, recordId);
        }

        [WebMethod(EnableSession = true)]
        public Types.Risk RiskCreateForJobSiteQuoteTemplate(int jobSiteId, int quoteId, int templateId)
        {
            return BL.RiskAssessmentHandler.CreateRiskAssessmentFromTemplate(jobSiteId,quoteId,templateId,AspSession.UserDatabase);
        }

        /// <summary>
        /// Service endpoint to enable the creation of a risk assessment from a template for a job site
        /// </summary>
        /// <param name="jobSiteId">int: The Id of the Job Site</param>
        /// <param name="templateId">int: The Id of the template the risk assessment is to be vbased upon</param>
        /// <returns>Bool: True if a risk assessment was created</returns>
        [WebMethod(EnableSession = true)]
        public bool RiskCreateForJobSiteIdAndTemplate(int quoteId, int jobSiteId, int templateId)
        {
            var assessment = BL.RiskAssessmentHandler.CreateRiskAssessmentFromTemplate(AspSession.UserDatabase, templateId, quoteId, jobSiteId);
           // var assessment = BL.RiskAssessmentHandler.CreateRiskAssessmentFromTemplate(AspSession.UserDatabase, templateId, Types.Enums.RiskCreateFromObjectType.JobSite, jobSiteId);
            return assessment.Id > 0;
        }
        #endregion

        #region Accessors
        [WebMethod(EnableSession = true)]
        public Types.RiskReferral getReferralForRiskId(int RiskId)
        {
            return BL.RiskAssessmentHandler.GetRiskReferalForRiskId(AspSession.UserDatabase, RiskId);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.RiskTemplateSearchResult> ValidateAssessmentForSite(int quoteId, int jobSiteId)
        {
            List<Types.RiskTemplateSearchResult> retVal = new List<Types.RiskTemplateSearchResult>();
            //Check if a risk assessment exists and populate the return list if it does not
            if (!BL.RiskAssessmentHandler.ValidateAssessmentForQuoteAndJobSite(AspSession.UserDatabase,quoteId, jobSiteId))
                retVal = BL.RiskAssessmentHandler.GetTemplateSearchResults(AspSession.UserDatabase);

            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.RiskTemplateSearchResult> SearchRiskTemplates()
        {
            return BL.RiskAssessmentHandler.GetTemplateSearchResults(AspSession.UserDatabase);
        }

        public Types.Risk RiskGetForAppointmentId(int appId)
        {
            return BL.RiskAssessmentHandler.RiskGetForAppointmentId(AspSession.UserDatabase, appId);
        }

        [WebMethod(EnableSession = true)]
        public Types.Risk RiskGetForQuoteAndJobSite(int quoteId, int jobSiteId)
        {
            return BL.RiskAssessmentHandler.RiskGetForQuoteAndJobSite(AspSession.UserDatabase, quoteId, jobSiteId);
        }

        [WebMethod(EnableSession = true)]
        public Types.Risk RiskGetForQuoteId(int quoteId)
        {
            return BL.RiskAssessmentHandler.RiskGetForQuoteId(AspSession.UserDatabase, quoteId);
        }

        [WebMethod(EnableSession = true)]
        public Types.Risk RiskGetForId(int riskId)
        {
            return BL.RiskAssessmentHandler.RiskGetForId(AspSession.UserDatabase, riskId);
        }

        [WebMethod(EnableSession = true)]
        public Types.ClientSite GetAssessmentSite(int riskId)
        {
            return BL.RiskAssessmentHandler.GetAssessmentSite(AspSession.UserDatabase, riskId);
        }


        [WebMethod(EnableSession = true)]
        public List<Types.RiskSection> RiskSectionsGetForRiskId(int riskId)
        {
            return BL.RiskAssessmentHandler.RiskSectionsGetForRiskId(AspSession.UserDatabase, riskId);
        }


        [WebMethod(EnableSession = true)]
        public Types.RiskSection AssessmentSectionGetForId(int sectionId)
        {
            return BL.RiskAssessmentHandler.AssessmentSectionGetForId(AspSession.UserDatabase, sectionId);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.RiskAdditionalItem> OtherRisksGetForRiskId(int riskId)
        {
            return BL.RiskAssessmentHandler.OtherRisksGetForRiskId(AspSession.UserDatabase, riskId);
        }

        [WebMethod(EnableSession = true)]
        public Types.RiskAdditionalItem OtherRiskGetForId(int additionalRiskId)
        {
            return BL.RiskAssessmentHandler.OtherRiskGetForId(AspSession.UserDatabase, additionalRiskId);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.RescuePlan> RiskAreialRescuePlans()
        {
            return BL.RiskAssessmentHandler.RiskAreialRescuePlans(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.TeamStaff> RiskWorkTeamMembers(int selectedId, int riskId)
        {
            return BL.WorkProgrammingHandler.TeamStaffGetForRisk(AspSession.UserDatabase, selectedId, riskId);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.RiskStaff> RiskStaffPresent(int riskId)
        {
            return BL.RiskAssessmentHandler.GetStaffForAssessment(AspSession.UserDatabase, riskId);
        }

        [WebMethod(EnableSession = true)]
        public Types.RiskStaff RiskStaffMemberGet(int id)
        {
            return BL.RiskAssessmentHandler.GetRiskStaffMember(AspSession.UserDatabase, id);
        }
        #endregion

        #region Modifiers

        [WebMethod(EnableSession = true)]
        public bool RiskAssessmentReferralSave(Types.RiskReferral Ref)
        {
            return BL.RiskAssessmentHandler.RiskAssessmentReferralSave(AspSession.UserDatabase, Ref, string.Format("{0} {1}", AspSession.CurrentUser.FirstName, AspSession.CurrentUser.LastName));
        }

        [WebMethod(EnableSession = true)]
        public bool RiskAssessmentUpdateDetails(Types.Risk assessment)
        {
            return BL.RiskAssessmentHandler.RiskAssessmentUpdate(AspSession.UserDatabase, assessment, string.Format("{0} {1}", AspSession.CurrentUser.FirstName, AspSession.CurrentUser.LastName));
        }

        [WebMethod(EnableSession = true)]
        public bool RiskAssessmentSectionUpdate(Types.RiskSection section)
        {
            return BL.RiskAssessmentHandler.RiskAssessmentSectionUpdate(AspSession.UserDatabase, section, string.Format("{0} {1}", AspSession.CurrentUser.FirstName, AspSession.CurrentUser.LastName));
        }

        [WebMethod(EnableSession = true)]
        public bool RiskOtherRiskUpdate(Types.RiskAdditionalItem item)
        {
            return BL.RiskAssessmentHandler.RiskOtherRiskUpdate(AspSession.UserDatabase, item, string.Format("{0} {1}", AspSession.CurrentUser.FirstName, AspSession.CurrentUser.LastName));
        }

        [WebMethod(EnableSession = true)]
        public bool RiskStaffMemberSave(Types.RiskStaff obj)
        {
            return BL.RiskAssessmentHandler.SaveRiskStaffMember(AspSession.UserDatabase, obj, string.Format("{0} {1}", AspSession.CurrentUser.FirstName, AspSession.CurrentUser.LastName));
        }

        [WebMethod(EnableSession = true)]
        public bool RiskStaffMemberRemove(int id)
        {
            return BL.RiskAssessmentHandler.RemoveRiskStaffMember(AspSession.UserDatabase, id /*, string.Format("{0} {1}", AspSession.CurrentUser.FirstName, AspSession.CurrentUser.LastName)*/);
        }
        #endregion
    }
}
