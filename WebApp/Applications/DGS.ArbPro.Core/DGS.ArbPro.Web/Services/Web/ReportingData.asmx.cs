﻿namespace DGS.ArbPro.Web.Services.Web
{
    using System;
    using System.Collections.Generic;
    using System.Web.Script.Services;
    using System.Web.Services;

    using DGS.ArbPro.BL;

    /// <summary>
    /// Summary description for ReportingData
    /// </summary>
    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ReportingData : WebService
    {
        /// <summary>
        /// The marketing handler.
        /// </summary>
        private readonly ReportingDataHandler reportingDataHandler = new ReportingDataHandler(AspSession.UserDatabase);

        /// <summary>
        /// Main method used to distribute which data should be acquired based on filtering criteria
        /// </summary>
        /// <param name="subCatId">subcategory id (used to determine the method to be called and type of category)</param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns>A list of reporting data items</returns>
        [WebMethod(EnableSession = true)]
        public List<Types.ReportingData> MainFilteringReportingData(int subCatId, DateTime startDate, DateTime endDate)
        {
            List<Types.ReportingData> retVal;
            switch (subCatId)
            {
                case 0:
                    retVal = this.reportingDataHandler.GetEnquiryForDates(startDate, endDate);
                    break;
                case 1:
                    retVal = this.reportingDataHandler.GetEnquiryForReceivedDates(startDate, endDate);
                    break;
                case 2:
                    retVal = this.reportingDataHandler.GetEnquiryForSentDates(startDate, endDate);
                    break;
                case 3:
                    retVal = this.reportingDataHandler.GetEnquiryForAcceptedDates(startDate, endDate);
                    break;
                case 4:
                    retVal = this.reportingDataHandler.GetEnquiryForWorkBookedDates(startDate, endDate);
                    break;
                case 5:
                    retVal = this.reportingDataHandler.GetEnquiryForWorkCompletedDates(startDate, endDate);
                    break;
                case 6:
                    retVal = this.reportingDataHandler.GetEnquiryForSalesHistoricallyDates(startDate, endDate);
                    break;
                case 7:
                    retVal = this.reportingDataHandler.GetEnquiryForVatValueCollected(startDate, endDate);
                    break;
                case 8:
                    retVal = this.reportingDataHandler.GetEnquiryForVatTotal(startDate, endDate);
                    break;
                case 9:
                    retVal = this.reportingDataHandler.GetEnquiryForIndividualRateDates(startDate, endDate);
                    break;
                case 10:
                    retVal = this.reportingDataHandler.GetEnquiryForIndividualSalesDates(startDate, endDate);
                    break;
                case 11:
                    retVal = this.reportingDataHandler.GetEnquiryForViewDates(startDate, endDate);
                    break;
                case 12:
                    retVal = this.reportingDataHandler.GetEnquiryForLetterDates(startDate, endDate);
                    break;
                default:
                    retVal = new List<Types.ReportingData>();
                    break;
            }

            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.ReportingData> GetSentForReturnMarketing(DateTime startDate, DateTime endDate)
        {
            return this.reportingDataHandler.GetEnquiryForDates(startDate, endDate);
        }
                [WebMethod(EnableSession = true)]
        public string GraphExportforLeadSource(String FromDate, String ToDate)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            var data = BL.ExportHandlers.GraphExportHandler.GraphExportLeadSource(FromDate, ToDate, AspSession.UserDatabase); 
            return data.StringValue;
        }
         [WebMethod(EnableSession = true)]
                public string GraphExportforReceived(String FromDate, String ToDate)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            var data = BL.ExportHandlers.GraphExportHandler.GraphExportforReceived(FromDate, ToDate, AspSession.UserDatabase); 
            return data.StringValue;
        }
        [WebMethod(EnableSession = true)]
         public string GraphExportforQuotationSent(String FromDate, String ToDate)
         {
             Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
             var data = BL.ExportHandlers.GraphExportHandler.GraphExportforQuotationSent(FromDate, ToDate, AspSession.UserDatabase);
             return data.StringValue;
         }

         [WebMethod(EnableSession = true)]
        public string GraphExportforQuotationAccepted(String FromDate, String ToDate)
         {
             Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
             var data = BL.ExportHandlers.GraphExportHandler.GraphExportforQuotationAccepted(FromDate, ToDate, AspSession.UserDatabase);
             return data.StringValue;
         }
         [WebMethod(EnableSession = true)]
         public string GraphExportforTreeWorkBooked(String FromDate, String ToDate)
         {
             Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
             var data = BL.ExportHandlers.GraphExportHandler.GraphExportforTreeWorkBooked(FromDate, ToDate, AspSession.UserDatabase);
             return data.StringValue;
         }
         [WebMethod(EnableSession = true)]
         public string GraphExportforTreeProgress(String FromDate, String ToDate)
         {
             Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
             var data = BL.ExportHandlers.GraphExportHandler.GraphExportforTreeProgress(FromDate, ToDate, AspSession.UserDatabase);
             return data.StringValue;
         }
         [WebMethod(EnableSession = true)]
         public string GraphExportforTreeCompleted(String FromDate, String ToDate)
         {
             Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
             var data = BL.ExportHandlers.GraphExportHandler.GraphExportforTreeCompleted(FromDate, ToDate, AspSession.UserDatabase);
             return data.StringValue;
         }
         [WebMethod(EnableSession = true)]
         public string GraphExportforSalesHistorically(String FromDate, String ToDate)
         {
             Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
             var data = BL.ExportHandlers.GraphExportHandler.GraphExportforSalesHistorically(FromDate, ToDate, AspSession.UserDatabase);
             return data.StringValue;
         }
         [WebMethod(EnableSession = true)]
         public string GraphExportforSalesOutstanding(String FromDate, String ToDate)
         {
             Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
             var data = BL.ExportHandlers.GraphExportHandler.GraphExportforSalesOutstanding(FromDate, ToDate, AspSession.UserDatabase);
             return data.StringValue;
         }
         [WebMethod(EnableSession = true)]
         public string GraphExportforSuccessRate(String FromDate, String ToDate)
         {
             Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
             var data = BL.ExportHandlers.GraphExportHandler.GraphExportforSuccessRate(FromDate, ToDate, AspSession.UserDatabase);
             return data.StringValue;
         }
         [WebMethod(EnableSession = true)]
         public string GraphExportforSales(String FromDate, String ToDate)
         {
             Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
             var data = BL.ExportHandlers.GraphExportHandler.GraphExportforSales(FromDate, ToDate, AspSession.UserDatabase);
             return data.StringValue;
         }
        [WebMethod(EnableSession = true)]
         public string GraphExportforInvoicesPaid(String FromDate, String ToDate)
         {
             Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
             var data = BL.ExportHandlers.GraphExportHandler.GraphExportforInvoicesPaid(FromDate, ToDate, AspSession.UserDatabase);
             return data.StringValue;
         }
        [WebMethod(EnableSession = true)]
        public string GraphExportforInvoicesVatPaid(String FromDate, String ToDate)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            var data = BL.ExportHandlers.GraphExportHandler.GraphExportforInvoicesVatPaid(FromDate, ToDate, AspSession.UserDatabase);
            return data.StringValue;
        }
        [WebMethod(EnableSession = true)]
        public string GraphExportforInvoicesTotalSaleIncVat(String FromDate, String ToDate)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            var data = BL.ExportHandlers.GraphExportHandler.GraphExportforInvoicesTotalSaleIncVat(FromDate, ToDate, AspSession.UserDatabase);
            return data.StringValue;
        }
        
        
    }
}
