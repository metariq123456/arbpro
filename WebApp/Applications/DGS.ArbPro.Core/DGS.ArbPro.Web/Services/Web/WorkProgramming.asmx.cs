﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace DGS.ArbPro.Web.Services.Web
{
    /// <summary>
    /// Summary description for WorkProgramming
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class WorkProgramming : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public Types.WorkSummary GetWorkSummaryForCalendar(int jobSiteId)
        {
            WorkFlow wf = new WorkFlow();
            var jobSite = wf.GetJobSite(jobSiteId);
            var quotesForJob = wf.GetQuotes(jobSite.JobId);

            Types.Quote max = new Types.Quote();
            for (var i = 0; i < quotesForJob.Count; i++)
            {
                if (quotesForJob[i].Id > max.Id)
                    max = quotesForJob[i];
            }

            return BL.WorkProgrammingHandler.WorkSummaryForQuote(AspSession.UserDatabase, max.Id);
        }

        [WebMethod(EnableSession = true)]
        public Types.WorkSummary GetWorkSummaryForWorkPlan(int id)
        {
            return BL.WorkProgrammingHandler.WorkSummaryForWorkPlan(AspSession.UserDatabase, id);
        }

        /// <summary>
        /// Service method to get a summary for the work to enable new work to be booked
        /// </summary>
        /// <param name="quoteId">The Id of the quote the work is associated with</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Types.WorkSummary GetWorkSummaryForQuote(int id)
        {
            return BL.WorkProgrammingHandler.WorkSummaryForQuote(AspSession.UserDatabase, id);
        }

        /// <summary>
        /// Service method to get a summary for the work to enable new work to be booked
        /// </summary>
        /// <param name="quoteId">The Id of the work record that work is being booked for</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Types.WorkSummary GetWorkSummaryForWork(int id)
        {
            return BL.WorkProgrammingHandler.WorkSummaryForWork(AspSession.UserDatabase, id);
        }

        [WebMethod(EnableSession = true)]
        public Types.WorkSummary GetWorkSummaryForWorkOnQuote(int id)
        {
            return BL.WorkProgrammingHandler.WorkSummaryForWorkForQuote(AspSession.UserDatabase, id);
        }
        [WebMethod(EnableSession = true)]
        public Types.WorkSummary GetJobWorkSummaryForWork(int id)
        {
            return BL.WorkProgrammingHandler.WorkSummaryForJobWork(AspSession.UserDatabase, id);
        }
        /// <summary>
        /// Service method to get a list of all the job sites for the quote
        /// </summary>
        /// <param name="quoteId">int: The id of the quote that work is to be booked for</param>
        /// <returns>List of Types.JobSite objects</returns>
        [WebMethod(EnableSession = true)]
        public List<Types.JobSite> GetJobSitesForQuote(int quoteId)
        {
            return BL.WorkFlowHandlers.JobHandler.GetJobSitesForQuote(AspSession.UserDatabase, quoteId, false);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.JobSite> GetJobSitesForQuote(int quoteId, bool includeItems)
        {
            return BL.WorkFlowHandlers.JobHandler.GetJobSitesForQuote(AspSession.UserDatabase, quoteId, includeItems);
        }

        /// <summary>
        /// Service method to get a list of all the work plans for the quote
        /// </summary>
        /// <param name="quoteId">int: The id of the quote that work is to be booked for</param>
        /// <returns>List of Types.JobSite objects</returns>
        [WebMethod(EnableSession = true)]
        public List<Types.WorkPlan> GetWorkPlansForQuote(int quoteId)
        {
            return BL.WorkProgrammingHandler.GetWorkPlansForQuote(AspSession.UserDatabase, quoteId);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Team> GetAllTeam()
        {
            return BL.WorkProgrammingHandler.GetTeamList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public bool TeamSaveAll(Types.Team team, string strColour)
        {
            //Utility.Logger.Log.DebugFormat("({0}:{1}-{2}) Saved Team {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, id, name);
            return BL.WorkProgrammingHandler.TeamSaveAll(AspSession.UserDatabase, team, strColour);
        }

        [WebMethod(EnableSession = true)]
        public bool TeamDelete(int teamId)
        {
            //Utility.Logger.Log.DebugFormat("({0}:{1}-{2}) Saved Team {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, id, name);
            return BL.WorkProgrammingHandler.TeamDelete(AspSession.UserDatabase, teamId);
        }

        [WebMethod(EnableSession = true)]
        public Types.Team GetTeamForID(int ID)
        {

            var test = BL.WorkProgrammingHandler.GetForID(AspSession.UserDatabase, ID);
            return test;
        }

        /// <summary>
        /// Service endpoint to get a new work plan for a quote
        /// The work plan is automatically saved to the database
        /// </summary>
        /// <param name="quoteId">Int: The Id of the associated quote</param>
        /// <param name="workId">Int: The Id of the associated work record</param>
        /// <param name="jobSiteId">Int: The Id of the associated job site</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Types.WorkPlan GetNewWorkPlan(int quoteId, int workId, int jobSiteId)
        {
            return BL.WorkProgrammingHandler.GetNewWorkPlan(AspSession.UserDatabase, quoteId, workId, jobSiteId);
        }

        /// <summary>
        /// Service end point to get a work plan for it's Id.
        /// returns a new object if the id is -1
        /// </summary>
        /// <param name="id">The Id of the database record to return</param>
        /// <param name="includeTools">flag to denote if the work plan tools are to be included</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Types.WorkPlan GetWorkPlan(int id, bool includeTools)
        {
            return BL.WorkProgrammingHandler.WorkPlanForId(AspSession.UserDatabase, id, includeTools);
        }


        [WebMethod(EnableSession = true)]
        public Types.WorkPlan GetWorkPlanForAppointment(int appointmentId, bool includeTools)
        {
            return BL.WorkProgrammingHandler.WorkPlanForAppointment(AspSession.UserDatabase, appointmentId, includeTools);
        }

        [WebMethod(EnableSession = true)]
        public bool SaveWorkPlan(Types.WorkPlan workPlan, Types.CalendarWork appointment)
        {
            //Check for any tools that have been set to quantity of 0
            foreach (var tool in workPlan.Tools.ToList())
            {
                if (tool.Number <= 0)
                    workPlan.Tools.Remove(tool);
            }
            return BL.WorkProgrammingHandler.SaveWorkPlanAndAppointment(AspSession.UserDatabase, workPlan, appointment, true);
        }

        [WebMethod(EnableSession = true)]
        public bool SaveWorkPlanForBookJob(Types.WorkPlan workPlan, Types.CalendarWork appointment)
        {
            //Check for any tools that have been set to quantity of 0
            foreach (var tool in workPlan.Tools.ToList())
            {
                if (tool.Number <= 0)
                    workPlan.Tools.Remove(tool);
            }
            return BL.WorkProgrammingHandler.SaveWorkPlanAndAppointmentForBookJob(AspSession.UserDatabase, workPlan, appointment, true);
        }
        //New Method as per Book from Calendar
        [WebMethod(EnableSession = true)]
        public bool SaveWorkPlanNew(Types.WorkPlan workPlan, Types.CalendarWork appointment)
        {
            //Check for any tools that have been set to quantity of 0
            foreach (var tool in workPlan.Tools.ToList())
            {
                if (tool.Number <= 0)
                    workPlan.Tools.Remove(tool);
            }
            return BL.WorkProgrammingHandler.SaveWorkPlanAndAppointmentNew(AspSession.UserDatabase, workPlan, appointment, true);
        }

        [WebMethod(EnableSession = true)]
        public int SaveJobWorkPlanNew(Types.WorkPlan workPlan, Types.CalendarWork appointment)
        {
            //Check for any tools that have been set to quantity of 0
            foreach (var tool in workPlan.Tools.ToList())
            {
                if (tool.Number <= 0)
                    workPlan.Tools.Remove(tool);
            }
            return BL.WorkProgrammingHandler.SaveJobWorkPlanAndAppointmentNew(AspSession.UserDatabase, workPlan, appointment, true);
        }

        [WebMethod(EnableSession = true)]
        public bool SaveOnlyAppointmentOfWorkPlan(Types.CalendarWork appointment)
        {
            return BL.WorkProgrammingHandler.SaveOnlyAppointmentForWorkPlan(AspSession.UserDatabase, appointment);
        }

        [WebMethod(EnableSession = true)]
        public bool SaveQuoteTools(int workId, List<Types.WorkTool> toolsList)
        {
            //Check for any tools that have been set to quantity of 0
            foreach (var tool in toolsList)
            {
                if (tool.Number <= 0)
                    toolsList.Remove(tool);
            }
            var retVal = BL.WorkProgrammingHandler.UpdateWorkTool(AspSession.UserDatabase, workId, toolsList);
            if (retVal != null)
            {
                return true;
            }

            return false;
        }

        [WebMethod(EnableSession = true)]
        public bool DeleteWorkAppointment(int appointmentId)
        {
            return BL.WorkProgrammingHandler.DeleteWorkAppointment(AspSession.UserDatabase, appointmentId);
        }

        /// <summary>
        /// Web method to obtain a list of staff for a work plan based on Id
        /// </summary>
        /// <param name="wpId">INT: The Id of the work plan</param>
        /// <returns>List of Types.TeamStaff</returns>
        [WebMethod(EnableSession = true)]
        public List<Types.TeamStaff> GetStaffForWorkPlan(int wpId)
        {
            return BL.WorkProgrammingHandler.GetStaffForWorkPlan(AspSession.UserDatabase, wpId);
        }

        /// <summary>
        /// Web method to save a list of team staff to a work plan
        /// </summary>
        /// <param name="wpId">int: The Id of the work plan</param>
        /// <param name="staffList">List of Types.TeamStaff: The staff to be set to the work plan</param>
        /// <returns>True if the operation updated 1 or more rows</returns>
        [WebMethod(EnableSession = true)]
        public bool SaveStaffForWorkPlan(int wpId, List<Types.TeamStaff> staffList)
        {
            return BL.WorkProgrammingHandler.SaveStaffForWorkPlan(AspSession.UserDatabase, wpId, staffList);
        }

        [WebMethod(EnableSession = true)]
        public int UpdateWorkPlan(Types.WorkPlan workPlan)
        {
            foreach (var tool in workPlan.Tools.ToList())
            {
                if (tool.Number <= 0)
                    workPlan.Tools.Remove(tool);
            }
            return BL.WorkProgrammingHandler.UpdateWorkPlan(AspSession.UserDatabase, workPlan);
        }

        [WebMethod(EnableSession = true)]
        public int UpdateWorkTools(int workPlanId, List<Types.WorkTool> tools)
        {
            foreach (var tool in tools.ToList())
            {
                if (tool.Number <= 0)
                    tools.Remove(tool);
            }
            return BL.WorkProgrammingHandler.UpdateWorkTool(AspSession.UserDatabase, workPlanId, tools);
        }

        [WebMethod(EnableSession = true)]
        public int DeleteWorkTool(int Id)
        {
            return BL.WorkProgrammingHandler.DeleteWorkTool(AspSession.UserDatabase, Id);
        }

        [WebMethod(EnableSession = true)]
        public bool DeleteWorkPlan(int workPlanId)
        {
            return BL.WorkProgrammingHandler.DeleteWorkPlan(AspSession.UserDatabase, workPlanId);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Work> GetWorkForQuoteId(int quoteId)
        {
            return BL.WorkProgrammingHandler.GetWorkForQuote(AspSession.UserDatabase, quoteId);
        }

        #region Work Planning Pick lists - REworking for work planning rationailastion

        /// <summary>
        /// Web method to return a list of al staff in the client database, as Team Staff objects
        /// </summary>
        /// <returns>List of Types.TeamStaff, an empty list will be returned if there is an error</returns>
        [WebMethod(EnableSession = true)]
        public List<Types.TeamStaff> GetStaffListAsTeamStaff()
        {
            return BL.WorkProgrammingHandler.GetStaffListAsTeamStaff(AspSession.UserDatabase);
        }

        /// <summary>
        /// Web method to return a list of all tools in the client database, as Work Tool objects
        /// </summary>
        /// <returns>List of Types.WorkTool, an empty list will be returned if there is an error</returns>
        [WebMethod(EnableSession = true)]
        public List<Types.WorkTool> GetToolListAsWorkTools()
        {
            return BL.WorkProgrammingHandler.GetToolListAsWorkTools(AspSession.UserDatabase);
        }
        #endregion

    }
}
