﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using DGS.ArbPro.BL.ListItemHandlers;
using DGS.ArbPro.BL.SearchHandlers;

namespace DGS.ArbPro.Web.Services.Web
{
    /// <summary>
    /// Summary description for SearchService
    /// </summary>
    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class SearchService : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public List<Types.RiskSearchResult> GetRiskAssessmentListForQuote(int quoteId)
        {
            var Result = SearchResultHandler.GetRiskAssessmentListForQuote(AspSession.UserDatabase, quoteId);
            return Result;
        }


        [WebMethod(EnableSession = true)]
        public List<Types.RiskSearchResult> GetClientRiskAssessmentList(int clientId)
        {
            var Result = SearchResultHandler.GetClientRiskAssessmentList(AspSession.UserDatabase, clientId);
            return Result;
        }

        [WebMethod(EnableSession = true)]
        public Types.ClientSearchResult GetClientForId(int clientId)
        {
            var Result = SearchResultHandler.GetClientForId(AspSession.UserDatabase, clientId);
            return Result;
        }

        //Client search
        [WebMethod(EnableSession = true)]
        public Types.ClientSearchResult GetForClientId(String FilterText)
        {
            int clientId;
            bool res = int.TryParse(FilterText, out clientId);
            clientId = res ? clientId : -1;
            var Result = SearchResultHandler.GetClientForId(AspSession.UserDatabase, clientId);
            return Result;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetForClientIdOrAccounts(String FilterText)
        {
            //  DGSAP-802 - Wrong handler method being called
            var Result = SearchResultHandler.GetForClientIdOrAccounts(AspSession.UserDatabase, FilterText);
            return Result;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetClientNameList(String FilterText)
        {
            var Result = SearchResultHandler.GetClientForNameLike(AspSession.UserDatabase, FilterText);
            return Result;
        }
        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetClientNameFullList(String FilterText)
        {
            var Result = SearchResultHandler.GetClientForNameFull(AspSession.UserDatabase, FilterText);
            return Result;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetClientAddressList(String FilterText)
        {
            var Result = SearchResultHandler.GetClientForAddressLike(AspSession.UserDatabase, FilterText);
            return Result;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetClientTelephoneList(String FilterText)
        {
            var Result = SearchResultHandler.GetClientForTelephoneLike(AspSession.UserDatabase, FilterText);
            return Result;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetClientPostCodeList(String FilterText)
        {
            return SearchResultHandler.GetClientForPostCodeLike(AspSession.UserDatabase, FilterText);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetForJobId(String FilterText)
        {
            return SearchResultHandler.GetForJobId(AspSession.UserDatabase, FilterText);
        }

        //Contact Search
        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetContactDetailsList(String FilterText)
        {
            return SearchResultHandler.GetContactDetailsLike(AspSession.UserDatabase, FilterText);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetContactNameList(String FilterText)
        {
            return SearchResultHandler.GetContactNameLike(AspSession.UserDatabase, FilterText);
        }

        //Quote Search
        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetQuoteNumberList(String FilterText)
        {
            return SearchResultHandler.GetQuoteClientNumberLike(AspSession.UserDatabase, FilterText);
        }
        
        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetQuoteNumberListById(String FilterText)
        {
            return SearchResultHandler.GetQuoteIdLike(AspSession.UserDatabase, FilterText);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetQuoteNameList(String FilterText)
        {
            return SearchResultHandler.GetQuoteNameLike(AspSession.UserDatabase, FilterText);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetQuoteSiteAddressList(String FilterText)
        {
            return SearchResultHandler.GetQuoteSiteAddressLike(AspSession.UserDatabase, FilterText);
        }

        //Document Search
        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetDocumentList(String FilterText)
        {
            return SearchResultHandler.GetDocumentList(AspSession.UserDatabase, FilterText);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetQuoteDocumentList(String FilterText)
        {
            return SearchResultHandler.GetQuoteDocumentList(AspSession.UserDatabase, FilterText);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetInvoiceDocumentList(String FilterText)
        {
            return SearchResultHandler.GetInvoiceDocumentList(AspSession.UserDatabase, FilterText);
        }

        //Invoices
        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetInvoiceForId(String FilterText)
        {
            return SearchResultHandler.GetInvoiceForId(AspSession.UserDatabase, FilterText);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetForInvClientId(String FilterText)
        {
            return SearchResultHandler.GetForInvClientId(AspSession.UserDatabase, FilterText);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetForInvNameLike(String FilterText)
        {
           return SearchResultHandler.GetForInvNameLike(AspSession.UserDatabase, FilterText);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetForInvSiteAddress(String FilterText)
        {
            return SearchResultHandler.GetForInvSiteAddress(AspSession.UserDatabase, FilterText);
        }
    }
}
