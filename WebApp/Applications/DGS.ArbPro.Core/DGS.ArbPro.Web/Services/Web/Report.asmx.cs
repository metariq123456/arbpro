﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using DGS.ArbPro.BL;
using DGS.ArbPro.BL.ReportItemHandlers;
using log4net;
using System.Net;

namespace DGS.ArbPro.Web.Services.Web
{
    using DGS.ArbPro.Types;

    /// <summary>
    /// The Report service enables access to all reporting functionality.
    /// Including Quotes, Invoicing and Work packages.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class Report : System.Web.Services.WebService
    {
        #region Static Members
        static ILog Logger = LogManager.GetLogger("Report Service");
        #endregion

        #region General Methods
        [WebMethod(EnableSession = true)]
        public Boolean ClearTempEmailAttachments()
        {
            AspSession.TempEmailAttachments.Clear();
            Utility.Logger.Log.DebugFormat("({0}:{1}) Cleared Temp Attachments", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            return AspSession.TempEmailAttachments.Count == 0;
        }

        /// <summary>
        /// Method to get the work plan id if it exists and if it doesn't create a new work plan
        /// </summary>
        /// <param name="qId">quote id</param>
        /// <param name="jsId">job site id</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public int GetWorkPlanId(int qId, int jsId)
        {
            var wpDB = new DAL.WorkPlan(AspSession.UserDatabase);
            WorkPlan wPlan = new WorkPlan();
            wPlan = wpDB.GetForQuote(qId, false);
            if (wPlan != null && wPlan.Id != null && wPlan.Id > 0)
            {
                return wPlan.Id;
            }
            else
            {
                var WorkPlan = new Types.WorkPlan()
                {
                    QuoteId = qId,
                    WorkId = -1,
                    TeamId = -1,
                    StaffRequired = 0,
                    JobSiteId = jsId
                };
                WorkPlan.Id = wpDB.Save(WorkPlan);
                return WorkPlan.Id;
            }
        }

        [WebMethod(EnableSession = true)]
        public Boolean AddTempDocument(Int32 ItemID, String Description, String ContentType, String ByteString, String FilePath)
        {
            try
            {
                var Info = new FileInfo(FilePath);
                var Bytes = Convert.FromBase64String(ByteString);
                var Doc = new Types.UserDocument()
                              {
                                  Id = AspSession.TempEmailAttachments.Count,
                                  ClientID = -1,
                                  UploadedByID = AspSession.CurrentUser.Id,
                                  Description = Description,
                                  Bytes = Bytes,
                                  ContentType = ContentType,
                                  UploadedTime = DateTime.Now,
                                  Name = Info.Name,
                                  Extension = Info.Extension,
                                  FileName = Path.GetFileNameWithoutExtension(FilePath),
                                  DocumentType = Types.Enums.UserDocumentTypeEnum.Temporary
                              };

                AspSession.TempEmailAttachments.Add(Doc);

                Utility.Logger.Log.DebugFormat(
                    "({0}:{1}) Uploaded Temp Attachment {2}",
                    AspSession.CurrentUser.Id,
                    AspSession.CurrentUser.NameStr,
                    Description);

                return true;
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("({0}:{1}) Failed to upload Temp Attachment", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                Utility.Logger.Log.DebugFormat("Exception: {0}", ex.Message);
                return false;
            }
        }
        [WebMethod(EnableSession = true)]
        public Boolean AddJobDocumentToAttachment(List<int> DocIdList)
        {
            try
            {
                foreach(int DocId in  DocIdList)
                {
                    var DB = new DAL.Document(AspSession.UserDatabase);
                    Types.JobDocumentsObject JobDocuments = DB.GetDefaultJobDocumentById(DocId);
                    String FilePath = JobDocuments.DocumentName + Path.GetExtension(JobDocuments.DocumentFileName);
                    var Info = new FileInfo(FilePath);
                    var Doc = new Types.UserDocument()
                    {
                        Id = AspSession.TempEmailAttachments.Count,
                        ClientID = -1,
                        UploadedByID = AspSession.CurrentUser.Id,
                        Description = "Temporary Document For Email Attachment",
                        Bytes = JobDocuments.DocumentImage,
                        ContentType = JobDocuments.DocumentType,
                        UploadedTime = DateTime.Now,
                        Name = Info.Name,
                        Extension = Info.Extension,
                        FileName = Path.GetFileNameWithoutExtension(FilePath),
                        DocumentType = Types.Enums.UserDocumentTypeEnum.Temporary
                    };

                    AspSession.TempEmailAttachments.Add(Doc);

                    Utility.Logger.Log.DebugFormat(
                        "({0}:{1}) Uploaded Temp Attachment {2}",
                        AspSession.CurrentUser.Id,
                        AspSession.CurrentUser.NameStr,
                        "Temporary Document For Email Attachment");
                    }
                return true;
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("({0}:{1}) Failed to upload Temp Attachment", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                Utility.Logger.Log.DebugFormat("Exception: {0}", ex.Message);
                return false;
            }
        }
        [WebMethod(EnableSession = true)]
        public List<int> AddCompanyDocumentToAttachment()
        {
            List<int> DocIdList = new List<int>();
            try
            {
                List<Types.CompanyDocumentsObject> CompanyDocList = BL.CompanyHandler.GetDefaultAttachmentForCompany(AspSession.UserDatabase);
                
                foreach (Types.CompanyDocumentsObject CompanyDoc in CompanyDocList)
                {
                    String FilePath = CompanyDoc.DocumentName + Path.GetExtension(CompanyDoc.DocumentFileName);
                    var Info = new FileInfo(FilePath);
                    var Doc = new Types.UserDocument()
                    {
                        Id = AspSession.TempEmailAttachments.Count,
                        ClientID = -1,
                        UploadedByID = AspSession.CurrentUser.Id,
                        Description = "Temporary Document For Email Attachment",
                        Bytes = CompanyDoc.DocumentImage,
                        ContentType = CompanyDoc.DocumentType,
                        UploadedTime = DateTime.Now,
                        Name = Info.Name,
                        Extension = Info.Extension,
                        FileName = Path.GetFileNameWithoutExtension(FilePath),
                        DocumentType = Types.Enums.UserDocumentTypeEnum.Temporary
                    };
                    DocIdList.Add(CompanyDoc.Id);
                    AspSession.TempEmailAttachments.Add(Doc);

                    Utility.Logger.Log.DebugFormat(
                        "({0}:{1}) Uploaded Temp Attachment {2}",
                        AspSession.CurrentUser.Id,
                        AspSession.CurrentUser.NameStr,
                        "Temporary Document For Email Attachment");
                }
                return DocIdList;
                
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("({0}:{1}) Failed to upload Temp Attachment", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                Utility.Logger.Log.DebugFormat("Exception: {0}", ex.Message);
                return DocIdList;
            }
        }
        [WebMethod(EnableSession = true)]
        public List<Types.UserDocument> GetTempDocuments(Int32 ItemID)
        {
            try
            {
                var ReturnList = new List<Types.UserDocument>();
                // TODO: Add Client Docs
                ReturnList.AddRange(AspSession.TempEmailAttachments);
                return ReturnList;
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat(
                    "({0}:{1}) Failed to upload Temp Attachment",
                    AspSession.CurrentUser.Id,
                    AspSession.CurrentUser.NameStr);
                Utility.Logger.Log.DebugFormat("Exception: {0}", ex.Message);
                return null;
            }
        }
        [WebMethod(EnableSession = true)]
        public bool RemoveTempDocumentsById(Int32 ItemID)
        {
            try
            {
                for (int i = 0; i < AspSession.TempEmailAttachments.Count; i++)
                {
                    if (AspSession.TempEmailAttachments[i].Id == ItemID) // there will only one item with Number = 1   
                    {
                        AspSession.TempEmailAttachments.Remove(AspSession.TempEmailAttachments[i]);
                        break;//so that we won't iterate the whole list in the best case
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat(
                    "({0}:{1}) Failed to remove Temp Attachment",
                    AspSession.CurrentUser.Id,
                    AspSession.CurrentUser.NameStr);
                Utility.Logger.Log.DebugFormat("Exception: {0}", ex.Message);
                return false;
            }

        }
        [WebMethod(EnableSession = true)]
        public Types.TextTemplate GetTemplate(int ID)
        {
            var DB = new DAL.TextTemplate(AspSession.UserDatabase);
            var List = DB.GetList();
            var ReturnValue = List.Find(t => t.Id == ID);
            return ReturnValue;
        }
        #endregion

        #region PDF
        #region Quote
        [WebMethod(EnableSession = true)]
        public Boolean OpenCombinedManualQuoteDocs(int calendarId)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                var Handler = new ReportQuoteHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = Handler.GenerateCombinedManualReport(calendarId);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("PDF Error generating work schedule for database {0} quote Id {1} : {3}", AspSession.UserDatabase, calendarId, ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }

        [WebMethod(EnableSession = true)]
        public Boolean OpenQuoteLetter(Int32 RecID, Boolean IsCopy, String LetterText)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                var Handler = new ReportHandler(
                    AspSession.UserDatabase,
                    @"C:\",
                    Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = Handler.OpenQuoteLetter(RecID, IsCopy, Types.Enums.DocDispatchMethodEnum.Letter, LetterText);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Opened Quote Letter {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
            }
            catch (Exception) { }
            return AspSession.PDFViewerMemoryStream != null;
        }

        [WebMethod(EnableSession = true)]
        public Boolean OpenQuoteSchedule(Int32 RecID, Boolean IsCopy)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                var Handler = new ReportHandler(
                    AspSession.UserDatabase,
                    @"C:\",
                    Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = Handler.OpenQuoteSchedule(RecID, IsCopy, Types.Enums.DocDispatchMethodEnum.Letter);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Opened Quote Schedule {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
            }
            catch { }
            return AspSession.PDFViewerMemoryStream != null;
        }

        [WebMethod(EnableSession = true)]
        public Boolean OpenCombinedQuoteDocuments(Int32 RecID, Boolean IsCopy, string LetterText)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                var Handler = new ReportHandler(
                    AspSession.UserDatabase,
                    @"C:\",
                    Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = Handler.OpenCombinedQuoteDocuments(RecID, IsCopy, Types.Enums.DocDispatchMethodEnum.Letter, false, LetterText);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Opened Combined Quote {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
            }
            catch { }
            return AspSession.PDFViewerMemoryStream != null;
        }
        #region Methods Using a settings object

        #endregion
        #endregion
        #region Invoices
        [WebMethod(EnableSession = true)]
        public Boolean OpenInvoiceLetter(Int32 RecID, Types.Enums.InvoiceDocTypeEnum dockType, Boolean IsCopy, String LetterText)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                var Handler = new ReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = Handler.OpenInvoiceLetter(RecID, IsCopy, dockType, Types.Enums.DocDispatchMethodEnum.Letter, LetterText);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Opened Invoice Letter {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error in OpenInvoiceLetter : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }

        [WebMethod(EnableSession = true)]
        public Boolean OpenInvoiceLetter(Types.InvoicePdfSettings settings)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                var handler = new InvoiceReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.DownloadDocuments(settings, AspSession.CurrentUser);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Opened Invoice Letter {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, settings.Id);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error in OpenInvoiceLetter : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }

        [WebMethod(EnableSession = true)]
        public Boolean OpenInvoiceSchedule(Int32 RecID, Boolean IsCopy)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                var Handler = new ReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = Handler.OpenInvoiceSchedule(RecID, IsCopy, Types.Enums.DocDispatchMethodEnum.Letter);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Opened Invoice Schedule {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error in OpenInvoiceSchedule : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }

        [WebMethod(EnableSession = true)]
        public Boolean OpenInvoiceSchedule(Types.InvoicePdfSettings settings)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                var Handler = new InvoiceReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = Handler.DownloadDocuments(settings, AspSession.CurrentUser);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Opened Invoice Schedule {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, settings.Id);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error in OpenInvoiceSchedule : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }

        [WebMethod(EnableSession = true)]
        public Boolean OpenCombinedInvoiceDocuments(Int32 RecID, Types.Enums.InvoiceDocTypeEnum dockType, Boolean IsCopy, String LetterText)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                var Handler = new ReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = Handler.OpenCombinedInvoiceDocuments(RecID, IsCopy, dockType, Types.Enums.DocDispatchMethodEnum.Letter, false, LetterText);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Opened Combined Invoice {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error in OpenCombinedInvoiceDocuments : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }
        #endregion
        #endregion

        #region Document Generation using a settings object

        /// <summary>
        /// PDF Quote generation method to be downloaded
        /// </summary>
        /// <param name="obj">The object to be saved</param>
        /// <param name="settings">The settings top be applied to the quote</param>
        /// <returns>bool: Success status of the generation process</returns>
        [WebMethod(EnableSession = true)]
        public bool UpdateQuoteAndOpenReport(Types.Quote obj, Types.QuotePdfSettings settings)
        {
            bool retVal = BL.WorkFlowHandlers.QuoteHandler.SaveQuote(AspSession.UserDatabase, obj);
            AspSession.PDFViewerMemoryStream = null;
            if (retVal)
            {
                try
                {
                    QuoteReportHandler handler = new QuoteReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                    AspSession.PDFViewerMemoryStream = handler.GenerateDocumentDownload(settings, AspSession.CurrentUser);
                    Utility.Logger.Log.DebugFormat("({0}:{1}) Update Quote And Open Report type {2} for {3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, settings.GenerationEnum.ToString(), settings.Id);
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("Error in OpenReport : {0}", ex.ToString());
                    AspSession.PDFViewerMemoryStream = null;
                }
                retVal = AspSession.PDFViewerMemoryStream != null;
            }
            return retVal;
        }

        /// <summary>
        /// PDF Quote generation method to be emailed
        /// </summary>
        /// <param name="obj">The object to be saved</param>
        /// <param name="pdfSettings">The settings to be applied to the quote</param>
        /// <param name="emailSettings">The sttings to be applied to the generation of the email</param>
        /// <returns>bool: Success status of the generation process and email dispatch</returns>
        [WebMethod(EnableSession = true)]
        public bool UpdateQuoteAndEmailReport(Types.Quote obj, Types.QuotePdfSettings pdfSettings, Types.EmailSettings emailSettings)
        {
            //
            bool retVal = BL.WorkFlowHandlers.QuoteHandler.SaveQuote(AspSession.UserDatabase, obj);
            AspSession.PDFViewerMemoryStream = null;
            if (retVal)
            {
                try
                {
                    QuoteReportHandler handler = new QuoteReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                    AspSession.PDFViewerMemoryStream = handler.EmailDocuments(pdfSettings, emailSettings, AspSession.CurrentUser, AspSession.TempEmailAttachments);
                    Utility.Logger.Log.DebugFormat("({0}:{1}) Update Quote And Email Report type {2} for {3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, pdfSettings.GenerationEnum.ToString(), pdfSettings.Id);
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("Error in OpenReport : {0}", ex.ToString());
                    AspSession.PDFViewerMemoryStream = null;
                }
                retVal = AspSession.PDFViewerMemoryStream != null;
            }
            return retVal;
        }
        [WebMethod(EnableSession = true)]
        public Types.UserDocument DownloadGoogleImage(String MapUrl)
        {

            Types.UserDocument Doc = new Types.UserDocument();
            byte[] Map = null;
            try
            {
                WebClient wc = new WebClient();
                Map = wc.DownloadData(MapUrl);
                Doc.Bytes = Map;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error in DownloadGoogleImage : {0}", ex.ToString());
            }
            return Doc;
        }
        /// <summary>
        /// The update marketing and email report.
        /// </summary>
        /// <param name="obj">
        /// The marketing object.
        /// </param>
        /// <param name="pdfSettings">
        /// The pdf settings.
        /// </param>
        /// <param name="emailSettings">
        /// The email settings.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [WebMethod(EnableSession = true)]
        public bool UpdateMarketingAndEmailReport(Types.Marketing obj, Types.MarketingPdfSettings pdfSettings, Types.EmailSettings emailSettings)
        {
            string guid;
            List<string> recipients = new List<string>();

            try
            {
                recipients.Add(obj.Email);
                recipients.Add(AspSession.CurrentUser.EmailAddress);

                MarketingReportHandler handler = new MarketingReportHandler(AspSession.UserDatabase, @"C:\", this.Server.MapPath("/ReportLayouts/"));
                guid = handler.EmailDocuments(pdfSettings, emailSettings, AspSession.CurrentUser, recipients);

                Utility.Logger.Log.DebugFormat("({0}:{1}) Update Marketing And Email Report type {2} for {3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, pdfSettings.GenerationEnum, pdfSettings.Id);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error in marketing emailing (Report.asmx - UpdateMarketingAndEmailReport) : {0}", ex);
                guid = string.Empty;
            }

            if (string.IsNullOrEmpty(guid))
            {
                return false;
            }
            
            return true;
        }

        /// <summary>
        /// Generates a PDF for Return Marketing and updates the Marketing status if needed
        /// </summary>
        /// <param name="obj"> The Marketing object. </param>
        /// <param name="settings"> The pdf settings. </param>
        /// <returns> Success or failure. </returns>
        [WebMethod(EnableSession = true)]
        public bool UpdateMarketingAndOpenReport(Types.Marketing obj, MarketingPdfSettings settings)
        {
            AspSession.PDFViewerMemoryStream = null;

            try
            {
                MarketingReportHandler handler = new MarketingReportHandler(AspSession.UserDatabase, @"C:\", this.Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.GenerateDocumentDownload(settings, AspSession.CurrentUser);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Update Marketing And Open Report type {2} for {3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, settings.GenerationEnum, settings.Id);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error in OpenReport : {0}", ex);
                AspSession.PDFViewerMemoryStream = null;
            }

            bool retVal = AspSession.PDFViewerMemoryStream != null;

            if (retVal && !settings.IsPreview)
            {
                obj.EmailSent = true;
                obj.EmailSentDate = DateTime.Now;
                MarketingHandler mh = new MarketingHandler(AspSession.UserDatabase);
                retVal = mh.SaveClientWorkCategory(obj) > 0;
            }

            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public bool TestSmtpConnection(string usrEmail, SmtpSettings emailSettings, SmtpSettings usrSettings)
        {
            var retVal = false;
            List<String> recList = new List<string>();
            recList.Add(usrEmail);
            recList.Add(usrEmail);
            string testMsg = "Dear Sir/Madam, \n \n This message confirms your email is set up correctly using the SMTP settings you entered. Arb Pro will now use this email account to send all email communication from Arb Pro. \n \n Regards, \n ArbPro Service Team";
            string htmlReplace = testMsg.Replace("\n", "<br/>");
            string testCon = "";
            try
            {
                testCon = BL.ReportItemHandlers.ReportEmailHandler.SendMessage(
                    recList,
                    usrEmail,
                    "From name",
                    usrEmail,
                    "Reply to name",
                    "ArbPro Email Service Test Connection",
                    testMsg,
                    "<div><p>" + htmlReplace + "</p></div>",
                    null,
                    emailSettings,
                    usrSettings);
                if (!String.IsNullOrWhiteSpace(testCon))
                {
                    retVal = true;
                    Logger.InfoFormat("The email has been sent successfully : {0}", usrEmail);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error in TestSmtpConnection : {0}", ex.ToString());
                retVal = false;
            }

            return retVal;
        }


        [WebMethod(EnableSession = true)]
        public bool DownloadTpoWorkSchedule(Types.WorkPdfSettings settings)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                WorkReportHandler handler = new WorkReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.OpenTpoWorkSchedule(settings);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("PDF Error generating work pack using PDF Settings : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }

        [WebMethod(EnableSession = true)]
        public bool PrintImage(int PictureId, Byte[] picByte)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                WorkReportHandler handler = new WorkReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.PrintImage( PictureId, picByte);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("PDF Error generating work pack using PDF Settings : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }



        [WebMethod(EnableSession = true)]
        public bool DownloadWorkPack(Types.WorkPdfSettings settings)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                WorkReportHandler handler = new WorkReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.OpenWorkPack(settings);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("PDF Error generating work pack using PDF Settings : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }

        [WebMethod(EnableSession = true)]
        public bool EmailWorkPack(Types.WorkPdfSettings settings, string toAddress, string subject, string content)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                WorkReportHandler handler = new WorkReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.EmailWorkPack(settings, toAddress, subject, content, AspSession.CurrentUser, AspSession.TempEmailAttachments);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("PDF Error emailing work pack using PDF Settings : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }

        [WebMethod(EnableSession = true)]
        public bool AttachWorkPackLetter(Types.WorkPdfSettings settings)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                WorkReportHandler handler = new WorkReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.AttachWorkPackLetter(settings, AspSession.TempEmailAttachments);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("PDF Error attaching work pack using PDF Settings : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }
        [WebMethod(EnableSession = true)]
        public bool AttachRiskAssessmentDoc(Types.WorkPdfSettings settings)
        {
            AspSession.PDFViewerMemoryStream = null;
            try
            {
                WorkReportHandler handler = new WorkReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.AttachRiskAssessmentDoc(settings, AspSession.TempEmailAttachments);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("PDF Error attaching risk assessment doc using PDF Settings : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            return AspSession.PDFViewerMemoryStream != null;
        }
        [WebMethod(EnableSession = true)]
        public bool UpdateInvoiceAndOpenReport(Types.Invoice obj, Types.InvoicePdfSettings settings)
        {
            //
            bool retVal = (BL.InvoiceHandler.SaveInvoice(AspSession.UserDatabase, obj, null, null)) > 0;
            AspSession.PDFViewerMemoryStream = null;
            if (retVal)
            {
                try
                {
                    InvoiceReportHandler handler = new InvoiceReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                    AspSession.PDFViewerMemoryStream = handler.GenerateDocumentDownload(settings, AspSession.CurrentUser);
                    Utility.Logger.Log.DebugFormat("({0}:{1}) Update Invoice And Open Report type {2} for {3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, settings.GenerationEnum.ToString(), settings.Id);
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("Error in OpenReport : {0}", ex.ToString());
                    AspSession.PDFViewerMemoryStream = null;
                }
                retVal = AspSession.PDFViewerMemoryStream != null;
            }
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public bool UpdateInvoiceAndEmailReport(Types.Invoice obj, Types.InvoicePdfSettings pdfSettings, Types.EmailSettings emailSettings)
        {
            //
            bool retVal = (BL.InvoiceHandler.SaveInvoice(AspSession.UserDatabase, obj, null, null)) > 0;
            AspSession.PDFViewerMemoryStream = null;
            if (retVal)
            {
                try
                {
                    InvoiceReportHandler handler = new InvoiceReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                    AspSession.PDFViewerMemoryStream = handler.EmailDocuments(pdfSettings, emailSettings, AspSession.CurrentUser, AspSession.TempEmailAttachments);
                    Utility.Logger.Log.DebugFormat("({0}:{1}) Update Invoice And Email Report type {2} for {3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, pdfSettings.GenerationEnum.ToString(), pdfSettings.Id);
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("Error in OpenReport : {0}", ex.ToString());
                    AspSession.PDFViewerMemoryStream = null;
                }
                retVal = AspSession.PDFViewerMemoryStream != null;
            }
            return retVal;
        }
        #endregion

        #region Emails
        #region Quote Emails
        [WebMethod(EnableSession = true)]
        public Boolean EmailQuoteLetter(Int32 RecID, Boolean IsCopy, String LetterText, Int32 RecipientContactID, String Subject, String EmailText)
        {
            try
            {
                BL.ReportHandler handler = new ReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.SendQuoteLetterByEmail(AspSession.CurrentUser, RecID, RecipientContactID, IsCopy, LetterText, EmailText, Subject, AspSession.TempEmailAttachments);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Send Quote Letter", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error sending email for database {0} quote Id {1} : {3}", AspSession.UserDatabase, RecID, ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
                return false;
            }
            #region Refactored code
            //var Attachments = new List<ReportEmailHandler.EmailAttachment>();

            //AspSession.PDFViewerMemoryStream = null;
            //try
            //{
            //    var Handler = new ReportHandler(
            //        AspSession.UserDatabase,
            //        @"C:\",
            //        Server.MapPath("/ReportLayouts/"));
            //    AspSession.PDFViewerMemoryStream = Handler.OpenQuoteLetter(RecID, IsCopy, Types.Enums.DocDispatchMethodEnum.Email, LetterText);
            //}
            //catch (Exception ex) 
            //{
            //}

            //if (AspSession.PDFViewerMemoryStream == null)
            //    return false;

            //var Attachment = new ReportEmailHandler.EmailAttachment();
            //Attachment.AttachmentBytes = new Byte[AspSession.PDFViewerMemoryStream.Length];
            //AspSession.PDFViewerMemoryStream.Read(Attachment.AttachmentBytes, 0, (Int32)AspSession.PDFViewerMemoryStream.Length);
            //Attachment.AttachmentName = "QuoteLetter.pdf";
            //Attachments.Add(Attachment);

            //foreach (var Doc in AspSession.TempEmailAttachments)
            //{
            //    Attachments.Add(new ReportEmailHandler.EmailAttachment()
            //    {
            //        AttachmentName = Doc.Name,
            //        AttachmentBytes = Doc.Bytes
            //    });
            //}

            //ReportEmailHandler.SendPDFEmail(
            //    AspSession.UserDatabase,
            //    RecipientContactID,
            //    AspSession.CurrentUser,
            //    Subject,
            //    EmailText,
            //    Attachments);

            //return false;
            #endregion
        }

        [WebMethod(EnableSession = true)]
        public Boolean EmailQuoteSchedule(Int32 RecID, Boolean IsCopy, Int32 RecipientContactID, String Subject, String EmailText)
        {
            try
            {
                BL.ReportHandler handler = new ReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.SendQuoteScheduleByEmail(AspSession.CurrentUser, RecID, RecipientContactID, IsCopy, "", EmailText, Subject, AspSession.TempEmailAttachments);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Sent Quote Schedule {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error sending email for database {0} quote Id {1} : {3}", AspSession.UserDatabase, RecID, ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
                return false;
            }

            #region Refactored Code
            //var Attachments = new List<ReportEmailHandler.EmailAttachment>();

            //AspSession.PDFViewerMemoryStream = null;
            //try
            //{
            //    var Handler = new ReportHandler(
            //        AspSession.UserDatabase,
            //        @"C:\",
            //        Server.MapPath("/ReportLayouts/"));
            //    AspSession.PDFViewerMemoryStream = Handler.OpenQuoteSchedule(RecID, IsCopy, Types.Enums.DocDispatchMethodEnum.Email);
            //}
            //catch (Exception ex) { }

            //if (AspSession.PDFViewerMemoryStream == null)
            //    return false;

            //var Attachment = new ReportEmailHandler.EmailAttachment();
            //Attachment.AttachmentBytes = new Byte[AspSession.PDFViewerMemoryStream.Length];
            //AspSession.PDFViewerMemoryStream.Read(Attachment.AttachmentBytes, 0, (Int32)AspSession.PDFViewerMemoryStream.Length);
            //Attachment.AttachmentName = "QuoteSchedule.pdf";
            //Attachments.Add(Attachment);

            //foreach (var Doc in AspSession.TempEmailAttachments)
            //{
            //    Attachments.Add(new ReportEmailHandler.EmailAttachment()
            //    {
            //        AttachmentName = Doc.Name,
            //        AttachmentBytes = Doc.Bytes
            //    });
            //}

            //ReportEmailHandler.SendPDFEmail(
            //    AspSession.UserDatabase,
            //    RecipientContactID,
            //    AspSession.CurrentUser,
            //    Subject,
            //    EmailText,
            //    Attachments);

            //return false;
            #endregion
        }

        [WebMethod(EnableSession = true)]
        public Boolean EmailQuoteDocuments(Int32 RecID, Boolean IsCopy, String LetterText, Int32 RecipientContactID, String Subject, String EmailText)
        {
            try
            {
                BL.ReportHandler handler = new ReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.SendQuoteDocumentsByEmail(AspSession.CurrentUser, RecID, RecipientContactID, IsCopy, LetterText, EmailText, Subject, AspSession.TempEmailAttachments);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Send Combined Quote {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error sending email for database {0} quote Id {1} : {3}", AspSession.UserDatabase, RecID, ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
                return false;
            }
        }
        #endregion

        #region Invoice Emails
        [WebMethod(EnableSession = true)]
        public Boolean EmailInvoiceLetter(Int32 RecID, Boolean IsCopy, String LetterText, Int32 RecipientContactID, String Subject, String EmailText)
        {
            try
            {
                BL.ReportHandler handler = new ReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.SendQuoteLetterByEmail(AspSession.CurrentUser, RecID, RecipientContactID, IsCopy, LetterText, EmailText, Subject, AspSession.TempEmailAttachments);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Sent Invoice Letter {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error sending email for database {0} quote Id {1} : {3}", AspSession.UserDatabase, RecID, ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
                return false;
            }
        }

        [WebMethod(EnableSession = true)]
        public Boolean EmailInvoiceSchedule(Int32 RecID, Boolean IsCopy, Int32 RecipientContactID, String Subject, String EmailText)
        {
            try
            {
                BL.ReportHandler handler = new ReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.SendQuoteScheduleByEmail(AspSession.CurrentUser, RecID, RecipientContactID, IsCopy, "", EmailText, Subject, AspSession.TempEmailAttachments);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Sent Invoice Schedule {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error sending email for database {0} quote Id {1} : {3}", AspSession.UserDatabase, RecID, ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
                return false;
            }

        }

        [WebMethod(EnableSession = true)]
        public Boolean EmailInvoiceDocuments(Int32 RecID, Boolean IsCopy, String LetterText, Int32 RecipientContactID, String Subject, String EmailText)
        {
            try
            {
                BL.ReportHandler handler = new ReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.SendQuoteDocumentsByEmail(AspSession.CurrentUser, RecID, RecipientContactID, IsCopy, LetterText, EmailText, Subject, AspSession.TempEmailAttachments);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Sent Combined Invoice {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, RecID);
                return true;
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error sending email for database {0} quote Id {1} : {3}", AspSession.UserDatabase, RecID, ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
                return false;
            }
        }
        #endregion
        #region Work Programming
        #endregion
        #endregion

        #region Javascript Helpers
        [WebMethod(EnableSession = true)]
        public Types.QuotePdfSettings GetQuotePdfSetting()
        {
            return new Types.QuotePdfSettings();
        }

        [WebMethod(EnableSession = true)]
        public Types.WorkPdfSettings GetWorkPdfSetting()
        {
            return new Types.WorkPdfSettings();
        }

        [WebMethod(EnableSession = true)]
        public Types.InvoicePdfSettings GetInvoicePdfSetting()
        {
            return new Types.InvoicePdfSettings();
        }

        /// <summary>
        /// The get marketing pdf setting.
        /// </summary>
        /// <returns>
        /// The <see cref="MarketingPdfSettings"/>.
        /// </returns>
        [WebMethod(EnableSession = true)]
        public Types.MarketingPdfSettings GetMarketingPdfSetting()
        {
            return new Types.MarketingPdfSettings();
        }

        [WebMethod(EnableSession = true)]
        public Types.EmailSettings GetEmailSetting()
        {
            return new Types.EmailSettings();
        }

        #endregion

        #region Preview
        [WebMethod(EnableSession = true)]
        public bool OpenPreview()
        {
            try
            {
                Types.QuotePdfSettings settings = new Types.QuotePdfSettings();
                settings.IsPreview = true;
                settings.ShownItemValues=true;
                settings.IncludeAllSites = true;
                settings.TextBody = "This is an example of a quote letter";
                //settings.Id = 2199;
                settings.GenerationEnum = Types.Enums.DocGenerationEnum.Example;

                AspSession.PDFViewerMemoryStream = null;
                QuoteReportHandler handler = new QuoteReportHandler(AspSession.UserDatabase, @"C:\", Server.MapPath("/ReportLayouts/"));
                AspSession.PDFViewerMemoryStream = handler.GenerateDocumentDownload(settings, AspSession.CurrentUser);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Update Quote And Open Report type {2} for {3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, settings.GenerationEnum.ToString(), settings.Id);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error in OpenReport : {0}", ex.ToString());
                AspSession.PDFViewerMemoryStream = null;
            }
            bool retVal = AspSession.PDFViewerMemoryStream != null;

            return retVal;
        }
        #endregion
    }
}
