﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using DGS.ArbPro.BL.ListItemHandlers;
using DGS.ArbPro.BL.SearchHandlers;
using DGS.ArbPro.Types.Enums;

namespace DGS.ArbPro.Web.Services.Web
{
    using System.Diagnostics;

    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class ClientData : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Int32 SaveClient(Int32 ClientID, Int32 ClientType, String ClientTitle, String ClientForeName, String ClientSurname, String ClientBusinessName, string ClientSector, String ClientWebsite, String ClientAlternateAccountNo, String ClientEmail, String ClientTelephone, String ClientMobile, String ClientHouseNameOrNumber, String ClientAddress1, String ClientAddress2, String ClientTown, String ClientCounty, String ClientPostCode, bool ClientSmsMessage, bool ClientIncludeInMarketing)
        {
            var DB = new DAL.Client(AspSession.UserDatabase);

            if (ClientID == -1)
            {
                var Client = new Types.Client()
                {
                    AccountType = (Types.Enums.ClientTypeEnum)ClientType,
                    Title = ClientTitle,
                    Forename = ClientForeName,
                    Surname = ClientSurname,
                    BusinessName = ClientBusinessName,
                    Sector = ClientSector,
                    WebAddress = ClientWebsite,
                    AltAccountNo = ClientAlternateAccountNo,
                    Email = ClientEmail,
                    Telephone = ClientTelephone,
                    Mobile = ClientMobile,
                    HouseNameNumber = ClientHouseNameOrNumber,
                    Address1 = ClientAddress1,
                    Address2 = ClientAddress2,
                    Town = ClientTown,
                    County = ClientCounty,
                    PostCode = ClientPostCode,
                    DateOpened = DateTime.Now,
                    SendSmsMessage = ClientSmsMessage,
                    IncludeInMarketing = ClientIncludeInMarketing
                };

                Client.Id = DB.Save(Client);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Client {2} Saved", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, Client.Id);
                return Client.Id;
            }
            else
            {
                var Client = DB.GetForClientId(ClientID);

                Client.AccountType = (Types.Enums.ClientTypeEnum)ClientType;
                Client.Title = ClientTitle;
                Client.Forename = ClientForeName;
                Client.Surname = ClientSurname;
                Client.BusinessName = ClientBusinessName;
                Client.Sector = ClientSector;
                Client.AltAccountNo = ClientAlternateAccountNo;
                Client.Email = ClientEmail;
                Client.Telephone = ClientTelephone;
                Client.Mobile = ClientMobile;
                Client.HouseNameNumber = ClientHouseNameOrNumber;
                Client.Address1 = ClientAddress1;
                Client.Address2 = ClientAddress2;
                Client.Town = ClientTown;
                Client.County = ClientCounty;
                Client.PostCode = ClientPostCode;
                Client.WebAddress = ClientWebsite;
                Client.SendSmsMessage = ClientSmsMessage;
                Client.IncludeInMarketing = ClientIncludeInMarketing;

                Client.Id = DB.Save(Client);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Client {2} Saved", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, Client.Id);
                return Client.Id;
            }
        }

        [WebMethod(EnableSession = true)]
        public Types.Client GetClient(Int32 ClientID)
        {
            var DB = new DAL.Client(AspSession.UserDatabase);
            return DB.GetForClientId(ClientID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetClientList(String FilterText)
        {
            return SearchResultHandler.GetClientForNameLike(AspSession.UserDatabase, FilterText);
        }

        [WebMethod(EnableSession = true)]
        public bool SaveContactObject(Types.ClientContact obj)
        {
            var retVal = BL.ClientHandler.SaveClientContact(AspSession.UserDatabase, obj);
            return retVal > 0;
        }
        [WebMethod(EnableSession = true)]
        public bool DocumentsSave(Types.ClientContact obj)
        {
            var retVal = BL.ClientHandler.SaveDocumentsFile(AspSession.UserDatabase, obj);
            return retVal > 0;
        }


        [WebMethod(EnableSession = true)]
        public Boolean SetContactType(Int32 ContactID, Int32 ContactType)
        {
            return BL.ClientHandler.SetContactStatus(AspSession.UserDatabase, ContactID, ContactType);
        }

        [WebMethod(EnableSession = true)]
        public Boolean SaveClientSite(Int32 ClientSiteID, Int32 ClientID, Int32 ContactID, String Reference, String HouseNameNo, String Address1, String Address2, String Town, String County, String PostCode, double Latitude, double Longitude)
        {
            var DB = new DAL.ClientSite(AspSession.UserDatabase);
            Types.ClientSite obj = null;
            if (ClientSiteID == -1)
            {
                obj = new Types.ClientSite();
            }
            else
            {
                obj = DB.GetForId(ClientSiteID);
            }

            if (obj != null)
            {
                obj.ClientId = ClientID;
                obj.ContactId = ContactID;
                obj.HouseNameNo = HouseNameNo;
                obj.Address1 = Address1;
                obj.Address2 = Address2;
                obj.Town = Town;
                obj.County = County;
                obj.PostCode = PostCode;
                obj.Latitude = Latitude;
                obj.Longitude = Longitude;
                obj.Reference = Reference;
                var ID = DB.Save(obj);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Client Site {2} Saved", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, ID);
                return true;
            }
            else return false;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.ContactSearchResult> GetClientContactList(Int32 ClientID)
        {
            return SearchResultHandler.GetClientContactList(AspSession.UserDatabase, ClientID);
        }

        // DGSAP-873
        [WebMethod(EnableSession = true)]
        public List<Types.QuoteContactSearchResult> GetQuoteClientContactList(Int32 ClientID)
        {
            return SearchResultHandler.GetQuoteClientContactList(AspSession.UserDatabase, ClientID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.ClientContact> GetClientContacts(int clientID)
        {
            return BL.ClientHandler.GetContactsList(AspSession.UserDatabase, clientID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.ClientSite> GetClientSiteList(Int32 ClientID)
        {
            return SearchResultHandler.GetClientSiteList(AspSession.UserDatabase, ClientID);
        }

        [WebMethod(EnableSession = true)]
        public Types.ClientSite GetClientSite(Int32 ClientSiteID)
        {
            DAL.ClientSite DB = new DAL.ClientSite(AspSession.UserDatabase);
            return DB.GetForId(ClientSiteID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetClientJobList(Int32 ClientID)
        {
            return SearchResultHandler.GetClientJobList(AspSession.UserDatabase, ClientID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.AppointmentSearchResult> GetClientAppointmentList(Int32 ClientID)
        {
            var ReturnValue = SearchResultHandler.GetClientAppointmentList(AspSession.UserDatabase, ClientID, true);
            return ReturnValue;
        }

        [WebMethod(EnableSession = true)]
        public Int32 CreateSalesEnquiry(
            Int32 ClientID,
            Int32 SurveyorID,
            String Source,
            String Comments,
            DateTime ApptStart,
            DateTime ApptEnd,
            String SiteReference,
            Int32 SiteID,
            String SiteHouseNameNumber,
            String SiteAddress1,
            String SiteAddress2,
            String SiteTown,
            String SiteCounty,
            String SitePostCode,
            int TemplateId)
        {
            var ClientDB = new DAL.Client(AspSession.UserDatabase);
            Types.Client enqClient = ClientDB.GetForClientId(ClientID);

            var JobDB = new DAL.Job(AspSession.UserDatabase);
            Types.Job newJob = new Types.Job()
            {
                ClientId = enqClient.Id,
                ContactId = enqClient.Contacts.Find(ct => ct.ContactType == Types.Enums.ContactTypeEnum.Default).Id,
                Source = Source,
                Comments = Comments,
                ReporterID = AspSession.CurrentUser.Id,
                Reporter = string.Format("{0} {1}",AspSession.CurrentUser.FirstName,AspSession.CurrentUser.LastName),
                SurveyorId = SurveyorID
            };

            newJob.Id = JobDB.Save(newJob);

            if (newJob.Id == -1)
                return -1;

            DAL.ClientSite ClientSiteDB = new DAL.ClientSite(AspSession.UserDatabase);
            Types.ClientSite CSite = null;

            if (SiteID == -1)
            {
                CSite = new Types.ClientSite()
                {
                    HouseNameNo = SiteHouseNameNumber,
                    Address1 = SiteAddress1,
                    Address2 = SiteAddress2,
                    Town = SiteTown,
                    County = SiteCounty,
                    PostCode = SitePostCode,
                    ClientId = enqClient.Id,
                    ContactId = newJob.ContactId,
                    Reference = SiteReference
                };
                CSite.Id = ClientSiteDB.Save(CSite);
            }
            else
            {
                CSite = ClientSiteDB.GetForId(SiteID);
            }

            DAL.JobSite JobSiteDB = new DAL.JobSite(AspSession.UserDatabase);
            Types.JobSite newJSite = new Types.JobSite()
            {
                ClientId = enqClient.Id,
                JobId = newJob.Id,
                Site = CSite,
                ContactId = newJob.ContactId
            };

            newJSite.Id = JobSiteDB.Save(newJSite);
            SiteID = newJSite.Id;

            if (SiteID == -1)
                return -1;

            //DGSAP-668
            if (ApptStart != DateTime.MinValue && ApptStart != null)
            {
                var CalendarQuoteDB = new DAL.Calendar(AspSession.UserDatabase);
                Types.CalendarQuote newQuote = new Types.CalendarQuote()
                {
                    StartDateTime = ApptStart,
                    EndDateTime = ApptEnd,
                    SurveyorID = SurveyorID,
                    JobSiteID = SiteID,
                    Completed = false,
                    LastUpdated = DateTime.Now,
                    LastDownloaded = DateTime.MinValue,
                    UploadTime = DateTime.MinValue,
                    TemplateId = TemplateId
                };
                CalendarQuoteDB.SaveQuote(newQuote);
            }

            Utility.Logger.Log.DebugFormat("({0}:{1}) New Job {2} Saved", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, newJob.Id);

            return newJob.Id;
        }

        [WebMethod(EnableSession = true)]
        public bool SetDeleted(int clientId)
        {
            return BL.ClientHandler.SetClientDeleted(AspSession.UserDatabase, clientId);
        }

    }
}
