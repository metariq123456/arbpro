﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Services;
using DGS.ArbPro.BL.ListItemHandlers;
using DGS.ArbPro.Types.Enums.SageEnums;

namespace DGS.ArbPro.Web.Services.Web
{
    /// <summary>
    /// Summary description for Settings
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Settings : System.Web.Services.WebService
    {
        #region PickItem
        [WebMethod(EnableSession = true)]
        public List<Types.PickItem> GetPickItemList(int itemType)
        {
            var ItemList = BL.ListItemHandlers.PickItemHandler.GetItems(AspSession.UserDatabase, (Types.Enums.PickItemTypeEnum)itemType);
            return ItemList;
        }

        [WebMethod(EnableSession = true)]
        public bool SaveNewItem(int Id, Types.Enums.PickItemTypeEnum _enum, string value)
        {
            Types.PickItem Item = new Types.PickItem();
            Item.Id = Id;
            Item.ItemType = _enum;
            Item.Value = value;
            var Save = BL.ListItemHandlers.PickItemHandler.SavePickItem(AspSession.UserDatabase, Item);
            Utility.Logger.Log.DebugFormat("({0}:{1}) Saved PickItem {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, _enum.ToString(), value);
            return Save;
        }
        public bool SaveCurrencySettings(string currentCurrency)       
        {

            return true;        
        }


        [WebMethod(EnableSession = true)]
        public bool DeleteItem(int Id)
        {
            return BL.ListItemHandlers.PickItemHandler.DeletePickItem(AspSession.UserDatabase, Id);
        }
        #endregion
        [WebMethod(EnableSession = true)]
        public List<Types.Currency> GetCurrencyList()
        {
            return BL.ListItemHandlers.CurrencyHandler.GetItems(AspSession.UserDatabase);
        }
        [WebMethod(EnableSession = true)]
        public bool SetSelected(int Id)
        {
            return BL.ListItemHandlers.CurrencyHandler.SetSelected(AspSession.UserDatabase, Id);
        }
         [WebMethod(EnableSession = true)]
        public List<Types.Currency> GetSelectedCurrency()
        {
            return BL.ListItemHandlers.CurrencyHandler.GetSelectedCurrency(AspSession.UserDatabase);
        }


        #region Tools
        [WebMethod(EnableSession = true)]
        public List<Types.Tool> GetToolList()
        {
            return BL.ListItemHandlers.ToolHandler.GetItems(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public bool SaveTool(int _id, int _stock, string _item)
        {
            Types.Tool Tool = new Types.Tool();
            Tool.Id = _id;
            Tool.Stock = _stock;
            Tool.Item = _item;
            Utility.Logger.Log.DebugFormat("({0}:{1}) Saved Tool {2}: {3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, _item, _stock);
            return BL.ListItemHandlers.ToolHandler.SaveTool(AspSession.UserDatabase, Tool);
        }

        [WebMethod(EnableSession = true)]
        public bool DeleteTool(int Id)
        {
            return BL.ListItemHandlers.ToolHandler.DeleteTool(AspSession.UserDatabase, Id);
        }
        #endregion

        #region TreeNames
        [WebMethod(EnableSession = true)]
        public List<Types.TreeName> GetTreeList()
        {
            return BL.ListItemHandlers.TreeNameHandler.GetItems(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public bool SaveTree(int _id, string _common, string _latin)
        {
            Types.TreeName Tree = new Types.TreeName();
            Tree.Id = _id;
            Tree.Common = _common;
            Tree.Latin = _latin;
            Utility.Logger.Log.DebugFormat("({0}:{1}) Saved Tree {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, _common, _latin);
            return BL.ListItemHandlers.TreeNameHandler.SaveItem(AspSession.UserDatabase, Tree);
        }

        [WebMethod(EnableSession = true)]
        public bool DeleteTree(int Id)
        {
            return BL.ListItemHandlers.TreeNameHandler.DeleteItem(AspSession.UserDatabase, Id);
        }
        #endregion

        #region WorkDescription
        [WebMethod(EnableSession = true)]
        public List<Types.WorkDescription> GetWorkDescriptionList()
        {
            return BL.ListItemHandlers.WorkDescriptionHandler.GetItems(AspSession.UserDatabase);
        }
        //[WebMethod(EnableSession = true)]
        //public List<Types.Currency> GetCurrencyItems()
        //{
        //    return BL.ListItemHandlers.CurrencyHandler.GetItems(AspSession.UserDatabase);
        //}


        [WebMethod(EnableSession = true)]
        public bool SaveWorkDescription(int _id, string _description, decimal _hours)
        {
            Types.WorkDescription WorkDescription = new Types.WorkDescription();
            WorkDescription.Id = _id;
            WorkDescription.Item = _description;
            WorkDescription.AverageHrsRequired = _hours;
            Utility.Logger.Log.DebugFormat("({0}:{1}) Saved Work Description {2}: {3}hrs", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, _description, _hours);
            return BL.ListItemHandlers.WorkDescriptionHandler.SaveWorkDescription(AspSession.UserDatabase, WorkDescription);
        }

        [WebMethod(EnableSession = true)]
        public bool DeleteWorkDescription(int Id)
        {
            return BL.ListItemHandlers.WorkDescriptionHandler.Delete(AspSession.UserDatabase, Id);
        }
        #endregion

        #region Templates
        [WebMethod(EnableSession = true)]
        public List<Types.TextTemplate> GetTemplateListForType(Types.Enums.MessageTypeEnum Format)
        {
            return BL.TemplateHandler.ListFilteredByMessageType(Format, AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.TextTemplate> GetTemplateListForTypeAndStage(Types.Enums.MessageTypeEnum Format, Types.Enums.TextTemplateTypeEnum tmplType)
        {
            return BL.TemplateHandler.ListFilteredByMessageType(AspSession.UserDatabase, Format, tmplType);
        }

        /// <summary>
        /// The get marketing template list for type and stage.
        /// </summary>
        /// <param name="format"> The format. </param>
        /// <param name="tmplType"> The template type. </param>
        /// <returns> List of text templates. </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.TextTemplate> GetMarketingTemplateListForTypeAndStage(Types.Enums.MessageTypeEnum format, Types.Enums.TextTemplateTypeEnum tmplType)
        {
            return BL.TemplateHandler.MarketingListFilteredByMessageType(AspSession.UserDatabase, format, tmplType);
        }

        // DGSAP-873
        [WebMethod(EnableSession = true)]
        public List<Types.SmsTextTemplate> GetSmsTemplateListForTypeAndStage(int quoteId, Types.Enums.TextTemplateTypeEnum tmplType)
        {
            return BL.TemplateHandler.ListFilteredSmsByMessageType(AspSession.UserDatabase, quoteId, tmplType);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.TextTemplate> GetTextTemplateList()
        {
            return BL.TemplateHandler.GetTextTemplatesList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public void SaveTemplate(Types.TextTemplate templateObj)
        {
           Utility.Logger.Log.DebugFormat("({0}:{1}) Saved Template {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, templateObj.TemplateName);
           BL.TemplateHandler.Save(templateObj, AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public bool DeleteTemplate(Types.TextTemplate templateObj)
        {
          return  BL.TemplateHandler.Delete(templateObj, AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public Types.TextTemplate GetEmptyTemplate()
        {
            var empty = new Types.TextTemplate();
            empty.Id = -1;
            return empty;
        }
        #endregion

        #region SageExport
        [WebMethod(EnableSession = true)]
        public string SageExport(SageTypeEnum ExportType, ExportTypeEnum ObjectType, DateTime FromDate, DateTime ToDate)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Accounting Export {2}/{3}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, ExportType.ToString(), ObjectType.ToString());
            var data = BL.ExportHandlers.SageExportHandler.Export(ExportType, ObjectType, FromDate, ToDate, AspSession.UserDatabase);
            return data.StringValue;
        }
        #endregion

        #region MarketingExport

        /// <summary>
        /// The marketing export.
        /// </summary>
        /// <param name="obj"> Marketing objects. </param>
        /// <returns> The <see cref="string"/>. </returns>
        [WebMethod(EnableSession = true)]
        public string MarketingExport(List<Types.Marketing> obj)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Performed Marketing Export", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);

            var data = BL.ExportHandlers.MarketingExportHandler.Export(obj);
            return data.StringValue;
        }

        #endregion

        #region Auto Complete Endpoints

        [WebMethod(EnableSession = true)]
        public List<string> ListTitles()
        {
            return PickItemHandler.GetTitleList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListTowns()
        {
            return PickItemHandler.GetTownList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListCounties()
        {
            return PickItemHandler.GetCountyList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListEnquirySource()
        {
            return PickItemHandler.GetLeadSourcesList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListTreeNames()
        {
            return TreeNameHandler.GetDefaultList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListWorkDescriptions()
        {
            return WorkDescriptionHandler.GetList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListHospitals()
        {
            return PickItemHandler.GetHospitalList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListCouncil()
        {
            return PickItemHandler.GetCouncilList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListTpoOrderType()
        {
            return new List<string>() { "None", "TPO Exists", "Conservation Order" };
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListTools()
        {
            return ToolHandler.GetList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListComplaintType()
        {
            return PickItemHandler.GetComplaintTypesList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListPermitType()
        {
            return PickItemHandler.GetPermitTypeList(AspSession.UserDatabase);
        }
        #endregion

        #region Enum List Entries
        [WebMethod(EnableSession = true)]
        public List<string> TpoStatusOptions()
        {
            List<string> values = new List<string>();
            for (int index = 0; index < 3; index++)
            {
                values.Add(((Types.Enums.TreeProtectionStatusEnum)index).ToString());
            }
            return values;
        }

        [WebMethod(EnableSession = true)]
        public List<string> TpoProgressStageOptions()
        {
            List<string> values = new List<string>();
            for (int index = 0; index < 7; index++)
            {
                values.Add(((Types.Enums.TreeProtectionProgreesEnum)index).ToString().Replace("_", " "));
            }
            return values;
        }
        #endregion

        #region Aerial Rescue Plan Methods
        [WebMethod(EnableSession = true)]
        public List<Types.AerialRescuePlan> GetAerialPlanList()
        {
            return BL.ListItemHandlers.AerialPlanHandler.GetAll(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public Types.AerialRescuePlan GetAerialPlan(int id)
        {
            return BL.ListItemHandlers.AerialPlanHandler.GetForId(id,AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public int SaveAerialPlan(Types.AerialRescuePlan obj)
        {
            return BL.ListItemHandlers.AerialPlanHandler.Save(obj, AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public bool DeleteAerialPlan(Types.AerialRescuePlan obj)
        {
            return BL.ListItemHandlers.AerialPlanHandler.Delete(obj, AspSession.UserDatabase);
        }

        //[WebMethod(EnableSession = true)]
        //public bool DeleteAerialPlan(int id)
        //{
        //    return BL.ListItemHandlers.AerialPlanHandler.Delete(id, AspSession.UserDatabase);
        //}
        #endregion
    }
}
