﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using DGS.ArbPro.BL.SearchHandlers;
using DGS.ArbPro.BL.WorkFlowHandlers;

namespace DGS.ArbPro.Web.Services.Web
{
    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class WorkFlow : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public Types.Job GetJob(int jobId)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Loading Job {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, jobId);
            return JobHandler.GetJob(AspSession.UserDatabase, jobId);
        }

        [WebMethod(EnableSession = true)]
        public Types.Quote GetQuote(Int32 QuoteID)
        {
            return QuoteHandler.GetForID(AspSession.UserDatabase, QuoteID, false);
        }

        [WebMethod(EnableSession = true)]
        public Types.Quote GetOnlyQuote(Int32 QuoteID)
        {
            return QuoteHandler.GetOnlyQuoteForID(AspSession.UserDatabase, QuoteID);
        }

        [WebMethod(EnableSession = true)]
        public int DeletePONoFromQuote(Int32 QuoteID)
        {
            return QuoteHandler.DeletePONoFromQuote(AspSession.UserDatabase, QuoteID);
        }

        [WebMethod(EnableSession = true)]
        public int SavePONoOnQuote(Int32 QuoteID, string PONo)
        {
            return QuoteHandler.SavePONoOnQuote(AspSession.UserDatabase, QuoteID, PONo);
        }

        [WebMethod(EnableSession = true)]
        public Types.Quote DuplicateQuote(Int32 QuoteID, bool keepRiskAssessment)
        {
            return QuoteHandler.DuplicateQuote(AspSession.UserDatabase, QuoteID, keepRiskAssessment);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Quote> GetQuotes(Int32 JobID)
        {
            return QuoteHandler.GetForJob(AspSession.UserDatabase, JobID);
        }

        [WebMethod(EnableSession = true)]
        public Boolean DeletePendingQuote(Int32 Quoteid)
        {
            var retVal = false;
            retVal = QuoteHandler.DeletePendingForId(AspSession.UserDatabase, Quoteid);
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.Invoice GetInvoice(Int32 InvoiceID)
        {
            var DB = new DAL.Invoice(AspSession.UserDatabase);
            return DB.GetForId(InvoiceID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Invoice> GetInvoices(Int32 QuoteID)
        {
            var DB = new DAL.Invoice(AspSession.UserDatabase);
            return DB.GetForQuote(QuoteID);
        }

        #region Appointment
        [WebMethod(EnableSession = true)]
        public Types.Job GetJobEnquiry(Int32 JobID)
        {
            var DB = new DAL.Job(AspSession.UserDatabase);
            return DB.GetForId(JobID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.CalendarQuote> GetUpcomingQuoteAppointmentsForJob(Int32 JobID)
        {
            var cDB = new DAL.Calendar(AspSession.UserDatabase);
            var jsDB = new DAL.JobSite(AspSession.UserDatabase);

            var JobSites = jsDB.GetForJob(JobID, false);
            var ReturnValue = cDB.GetQuoteList(DateTime.Now.Date, DateTime.MaxValue);

            ReturnValue.RemoveAll(r => !JobSites.Exists(js => js.Id == r.JobSiteID));
            ReturnValue.RemoveAll(r => r.Completed);

            Boolean Changed = true;

            while (Changed)
            {
                Changed = false;
                for (int i = 0; i < ReturnValue.Count; i++)
                {
                    if (i > 0)
                    {
                        if (ReturnValue[i].StartDateTime < ReturnValue[i - 1].StartDateTime)
                        {
                            var Buffer = ReturnValue[i];
                            ReturnValue[i] = ReturnValue[i - 1];
                            ReturnValue[i - 1] = Buffer;
                            Changed = true;
                        }
                    }
                }
            }

            return ReturnValue;
        }

        [WebMethod(EnableSession = true)]
        public Types.CalendarQuote GetQuoteAppointmentForId(Int32 ApptId)
        {
            var cDB = new DAL.Calendar(AspSession.UserDatabase);
            return cDB.GetQuote(ApptId);
        }

        [WebMethod(EnableSession = true)]
        public Boolean SaveQuoteAppointment(Int32 ApptID, int JobSiteID, Int32 SurveyorID, DateTime StartDateTime, DateTime EndDateTime, Boolean Completed, Boolean ReDownload, int TemplateId)
        {
            var aDB = new DAL.Calendar(AspSession.UserDatabase);

            if (ApptID > -1)
            {
                var Appt = aDB.GetQuote(ApptID);

                Appt.StartDateTime = StartDateTime;
                Appt.EndDateTime = EndDateTime;
                Appt.SurveyorID = SurveyorID;
                Appt.JobSiteID = JobSiteID;
                Appt.Completed = Completed;
                Appt.TemplateId = TemplateId;
                if (ReDownload)
                {
                    Appt.LastDownloaded = DateTime.MinValue;
                }
                Appt.LastUpdated = DateTime.Now;

                aDB.SaveQuote(Appt);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Created Quote Appointment {2} {3}-{4}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, Appt.JobSiteID, Appt.StartDateTime.ToString(), Appt.EndDateTime.ToString());
                return true;
            }
            else
            {
                var Appt = new Types.CalendarQuote()
                {
                    StartDateTime = StartDateTime,
                    EndDateTime = EndDateTime,
                    SurveyorID = SurveyorID,
                    LastUpdated = DateTime.Now,
                    JobSiteID = JobSiteID,
                    LastDownloaded = DateTime.MinValue,
                    UploadTime = DateTime.MinValue,
                    Completed = Completed,
                    TemplateId = TemplateId
                };

                aDB.SaveQuote(Appt);
            }

            return false;
        }
        #endregion


        [WebMethod(EnableSession = true)]
        public int CreateJob(Types.Job job, List<Types.ClientSite> selectedSites)
        {
            return BL.WorkFlowHandlers.JobHandler.CreateJob(AspSession.CurrentUser, job, selectedSites);
        }


        [WebMethod(EnableSession = true)]
        public Int32 CreateNewJob(Int32 ClientID)
        {
            var DB = new DAL.Job(AspSession.UserDatabase);
            var cDB = new DAL.Client(AspSession.UserDatabase);
            var Client = cDB.GetForClientId(ClientID);

            var Job = new Types.Job()
            {
                ClientId = ClientID,
                ContactId = Client.Contacts.Find(ct => ct.ContactType == Types.Enums.ContactTypeEnum.Default).Id,
                Source = "N/A",
                Comments = "",
                ReporterID = AspSession.CurrentUser.Id,
                Reporter = string.Format("{0} {1}", AspSession.CurrentUser.FirstName, AspSession.CurrentUser.LastName)
            };

            Job.Id = DB.Save(Job);

            if (Job.Id == -1)
                return -1;

            var csDB = new DAL.ClientSite(AspSession.UserDatabase);
            var ClientSite = csDB.GetForClient(ClientID).FirstOrDefault();

            var jsDB = new DAL.JobSite(AspSession.UserDatabase);
            var JobSite = new Types.JobSite()
            {
                ClientId = ClientID,
                JobId = Job.Id,
                Site = ClientSite,
                ContactId = Job.ContactId
            };

            JobSite.Id = jsDB.Save(JobSite);

            Utility.Logger.Log.DebugFormat("({0}:{1}) Created New Job {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, Job.Id);

            return Job.Id;
        }

        #region Sites
        [WebMethod(EnableSession = true)]
        public List<Types.JobSite> GetJobSiteList(Int32 JobID)
        {
            List<Types.JobSite> List = new List<Types.JobSite>();

            var DB = new DAL.JobSite(AspSession.UserDatabase);
            List.AddRange(DB.GetForJob(JobID, false));

            return List;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.JobSite> GetJobSiteListForRiskAssessmentForJobAndQuote(int quoteId, int jobID)
        {
            List<Types.JobSite> list = new List<Types.JobSite>();
            var DB = new DAL.JobSite(AspSession.UserDatabase);
            list.AddRange(DB.GetForJob(jobID, false));

            foreach (Types.JobSite js in list.ToList())
            {
                bool valid = BL.RiskAssessmentHandler.ValidateAssessmentForQuoteAndJobSite(AspSession.UserDatabase, quoteId, js.Id);
                if (valid)
                {
                    list.Remove(js);
                }
            }

            return list;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.JobSite> GetJobSiteListForRiskAssessment(Int32 JobID)
        {
            List<Types.JobSite> list = new List<Types.JobSite>();
            var DB = new DAL.JobSite(AspSession.UserDatabase);
            list.AddRange(DB.GetForJob(JobID, false));

            foreach (Types.JobSite js in list.ToList())
            {
                bool valid = BL.RiskAssessmentHandler.ValidateAssessmentForJobSite(AspSession.UserDatabase, js.Id);
                if (valid)
                {
                    list.Remove(js);
                }
            }

            return list;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.JobSite> GetJobSiteListForQuote(Int32 JobID, Int32 QuoteID)
        {
            List<Types.JobSite> List = new List<Types.JobSite>();

            var jsDB = new DAL.JobSite(AspSession.UserDatabase);
            List.AddRange(jsDB.GetForJob(JobID, false));

            var qDB = new DAL.QuoteItem(AspSession.UserDatabase);
            var QuoteItems = qDB.GetAllForQuote(QuoteID);

            List.RemoveAll(js => !QuoteItems.Exists(qi => qi.JobSiteId == js.Id));

            return List;
        }

        [WebMethod(EnableSession = true)]
        public Types.JobSite GetJobSite(Int32 JobSiteID)
        {
            var jsDB = new DAL.JobSite(AspSession.UserDatabase);
            return jsDB.GetForId(JobSiteID, false);
        }

        [WebMethod(EnableSession = true)]
        public Boolean RemoveJobSiteFromJob(Int32 JobID, Int32 SiteID)
        {
            var jsDB = new DAL.JobSite(AspSession.UserDatabase);
            return jsDB.Remove(JobID, SiteID);
        }

        [WebMethod(EnableSession = true)]
        public Boolean AddJobSiteToJobNew(Int32 JobID, String Reference, String HouseNameNo, String Address1, String Address2, String Town, String County, String PostCode, Double Latitude, Double Longitude)
        {
            var jDB = new DAL.Job(AspSession.UserDatabase);
            var cDB = new DAL.Client(AspSession.UserDatabase);
            var csDB = new DAL.ClientSite(AspSession.UserDatabase);
            var jsDB = new DAL.JobSite(AspSession.UserDatabase);

            var Job = jDB.GetForId(JobID);
            var Client = cDB.GetForClientId(Job.ClientId);

            var ClientSite = new Types.ClientSite()
            {
                ClientId = Client.Id,
                ContactId = Client.Contacts.Find(ct => ct.ContactType == Types.Enums.ContactTypeEnum.Default).Id,
                HouseNameNo = HouseNameNo,
                Address1 = Address1,
                Address2 = Address2,
                Town = Town,
                County = County,
                PostCode = PostCode,
                Latitude = Latitude,
                Longitude = Longitude,
                Reference = Reference
            };

            ClientSite.Id = csDB.Save(ClientSite);

            if (ClientSite.Id != -1)
            {
                var JobSite = new Types.JobSite()
                {
                    JobId = Job.Id,
                    Site = ClientSite,
                    ContactId = ClientSite.ContactId
                };

                JobSite.Id = jsDB.Save(JobSite);

                if (JobSite.Id != -1)
                {
                    return true;
                }
            }

            return false;
        }

        [WebMethod(EnableSession = true)]
        public Boolean ToggleJobSitesFromJob(Int32 JobID, Object[] ClientSiteIDs, Object[] Enableds)
        {
            var jDB = new DAL.Job(AspSession.UserDatabase);
            var jsDB = new DAL.JobSite(AspSession.UserDatabase);
            var csDB = new DAL.ClientSite(AspSession.UserDatabase);

            Boolean ReturnValue = true;

            //Get the current job so that if the site has no contact details the job site can be defaulted to the Job Contact
            var SelectedJob = jDB.GetForId(JobID);

            var JobSites = jsDB.GetForJob(JobID, true);

            for (int i = 0; i < ClientSiteIDs.Length; i++)
            {
                if ((Boolean)Enableds[i])
                {
                    if (!JobSites.Exists(js => js.SiteId == (Int32)ClientSiteIDs[i]))
                    {
                        var ClientSite = csDB.GetForId((Int32)ClientSiteIDs[i]);

                        var JobSite = new Types.JobSite()
                        {
                            JobId = JobID,
                            ClientId = ClientSite.ClientId,
                            Site = ClientSite,
                            ContactId = ClientSite.ContactId,
                            Disabled = false
                        };

                        JobSite.ContactId = ClientSite.ContactId;
                        //Validate the contact Id for the job site
                        if (JobSite.ContactId < 1)
                        {
                            JobSite.ContactId = SelectedJob.ContactId;
                        }

                        JobSite.Id = jsDB.Save(JobSite);

                        if (JobSite.Id == -1)
                            ReturnValue = false;
                    }
                    else
                    {
                        var JobSite = JobSites.Find(js => js.SiteId == (Int32)ClientSiteIDs[i]);
                        JobSite.Disabled = false;
                        jsDB.Save(JobSite);
                    }
                }
                else
                {
                    if (JobSites.Exists(js => js.SiteId == (Int32)ClientSiteIDs[i]))
                    {
                        var JobSite = JobSites.Find(js => js.SiteId == (Int32)ClientSiteIDs[i]);
                        JobSite.Disabled = true;
                        jsDB.Save(JobSite);
                    }
                }
            }

            return ReturnValue;
        }
        #endregion

        #region Quote
        [WebMethod(EnableSession = true)]
        public List<Types.JobItem> GetJobItemListForClientSite(Int32 ClientSiteID)
        {
            return BL.WorkFlowHandlers.JobHandler.GetItemsForClientSite(AspSession.UserDatabase, ClientSiteID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.JobItem> GetJobSiteQuoteItemList(Int32 JobSiteID)
        {
            List<Types.JobItem> List = new List<Types.JobItem>();
            var jsDB = new DAL.JobSite(AspSession.UserDatabase);
            var js = jsDB.GetForId(JobSiteID, false);
            var DB = new DAL.JobItem(AspSession.UserDatabase);
            List.AddRange(DB.GetAllForClientSite(js.SiteId));
            return List;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.JobItem> GetClientSiteQuoteItemList(Int32 ClientSiteID)
        {
            List<Types.JobItem> List = new List<Types.JobItem>();

            var DB = new DAL.JobItem(AspSession.UserDatabase);
            List.AddRange(DB.GetAllForClientSite(ClientSiteID));

            return List;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.QuoteItem> GetJobQuoteItemList(Int32 JobSiteID, Int32 QuoteID)
        {
            List<Types.QuoteItem> List = new List<Types.QuoteItem>();

            var DB = new DAL.QuoteItem(AspSession.UserDatabase);
            List.AddRange(DB.GetAllForJobSite(JobSiteID));

            return List.FindAll(i => i.QuoteId == QuoteID);
        }

        [WebMethod(EnableSession = true)]
        public Types.MapDetailsSearchResult GetQuoteSiteMap(Int32 JobSiteID, Int32 QuoteID)
        {
            var data = BL.SearchHandlers.SearchResultHandler.GetMapDetailsFroQuoteSite(QuoteID, JobSiteID, AspSession.UserDatabase);
            return data;
        }

        [WebMethod(EnableSession = true)]
        public Boolean AddJobSiteQuoteItemExisting(Int32 StageID, Int32 JobSiteID, Int32 ItemID)
        {
            if (StageID == -1)
            {
                var JSDB = new DAL.JobSite(AspSession.UserDatabase);
                var JS = JSDB.GetForId(JobSiteID, false);

                var Quote = new Types.Quote()
                {
                    JobSiteId = JobSiteID,
                    Status = Types.Enums.QuoteStatusEnum.Pending,
                    ClientId = JS.ClientId,
                    ContactId = JS.ContactId,
                };

                var QDB = new DAL.Quote(AspSession.UserDatabase);
                StageID = QDB.Save(Quote);
            }

            Types.JobItem newJobItem;

            var qiDB = new DAL.QuoteItem(AspSession.UserDatabase);
            var ExistingQuoteItems = qiDB.GetAllForQuote(StageID);

            var jiDB = new DAL.JobItem(AspSession.UserDatabase);
            var JobItem = jiDB.GetForId(ItemID);

            if (JobItem == null || JobItem.Id == -1) return false;
            int savedID = -1;
            if (ExistingQuoteItems.Exists(qi => qi.JobItemId == ItemID))
            {
                newJobItem = new Types.JobItem()
                {
                    JobId = JobItem.JobId,
                    JobSiteId = JobSiteID,
                    TPOId = JobItem.TPOId,
                    Value = JobItem.Value,
                    VatRate = JobItem.VatRate,
                    Code = JobItem.Code,
                    Name = JobItem.Name,
                    Description = JobItem.Description,
                    Latitude = JobItem.Latitude,
                    Longitude = JobItem.Longitude,
                    ImageGuid = JobItem.ImageGuid,
                    Status = JobItem.Status,
                    PreservationStatus = JobItem.PreservationStatus
                };
                savedID = jiDB.Save(newJobItem);
            }
            else
            {
                JobItem.JobSiteId = JobSiteID;
                savedID = jiDB.Save(JobItem);
            }

            var QuoteItem = new Types.QuoteItem();
            QuoteItem.JobItemId = savedID;
            QuoteItem.QuoteId = StageID;
            QuoteItem.Name = JobItem.Name;
            QuoteItem.Description = JobItem.Description;
            QuoteItem.VatRate = JobItem.VatRate;
            QuoteItem.Value = JobItem.Value;
            QuoteItem.Code = JobItem.Code;

            qiDB.Save(QuoteItem);

            Utility.Logger.Log.DebugFormat("({0}:{1}) Added Existing Quote Item", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);

            return true;
        }

        [WebMethod(EnableSession = true)]
        public Boolean AddJobSiteQuoteItemNew(Int32 JobID, Int32 StageID, Int32 JobSiteID, String Code, String Name, String Description, Decimal Value, Decimal VatRate)
        {
            if (StageID == -1)
            {
                var JDB = new DAL.Job(AspSession.UserDatabase);
                var J = JDB.GetForId(JobID);

                var Quote = new Types.Quote();
                Quote.JobSiteId = JobSiteID;
                Quote.Status = Types.Enums.QuoteStatusEnum.Pending;
                Quote.ClientId = J.ClientId;
                Quote.ContactId = J.ContactId;

                var QDB = new DAL.Quote(AspSession.UserDatabase);
                StageID = QDB.Save(Quote);
            }

            var Item = new Types.JobItem();
            Item.JobSiteId = JobSiteID;
            Item.Code = Code;
            Item.Name = Name;
            Item.Description = Description;
            Item.Value = Value;
            Item.VatRate = VatRate;

            var JIDB = new DAL.JobItem(AspSession.UserDatabase);
            Item.Id = JIDB.Save(Item);

            var QItem = new Types.QuoteItem();
            QItem.JobItemId = Item.Id;
            QItem.QuoteId = StageID;
            QItem.JobSiteId = JobSiteID;
            QItem.Code = Code;
            QItem.Name = Name;
            QItem.Description = Description;
            QItem.Value = Value;
            QItem.VatRate = VatRate;


            var QIDB = new DAL.QuoteItem(AspSession.UserDatabase);
            QIDB.Save(QItem);

            Utility.Logger.Log.DebugFormat("({0}:{1}) Added New Quote Item", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);

            return true;
        }

        /// <summary>
        /// Web method to save a list of new and existing items to the database.
        /// Only edited or new should be present in the list
        /// </summary>
        /// <param name="jobId">The Id for the job</param>
        /// <param name="quoteId">The Id for the Quote. This can be -1 if the quote is to be created</param>
        /// <param name="jobSiteId">The Id for the selected jobsite</param>
        /// <param name="items">The list of new and / or existing items to be updated</param>
        /// <returns>boolean success flag</returns>
        [WebMethod(EnableSession = true)]
        public bool QuoteItemsSave(int jobId, int quoteId, int jobSiteId, List<Types.QuoteItem> items)
        {
            //Check if there are any items that are to be inserted or updated
            if (items == null || items.Count == 0)
            {
                Utility.Logger.Log.DebugFormat("({0}:{1}) Aborting save process, no items to save", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                return true;
            }
            else
            {
                Utility.Logger.Log.DebugFormat("({0}:{1}) Saving items {2} to quote: {3} New Quote Item", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, items.Count, quoteId);
                return BL.WorkFlowHandlers.QuoteHandler.SaveQuoteItems(AspSession.UserDatabase, jobId, quoteId, jobSiteId, items);
            }
        }

        /// <summary>
        /// Supplies an empty QuoteItem object. This is used on the client side to populate a new
        /// QuoteItem for subsequent saving.
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public Types.QuoteItem FactoryQuoteItem()
        {
            return new Types.QuoteItem();
        }

        /// <summary>
        /// Deletes the QuoteItem whose Id is supplied.
        /// </summary>
        /// <returns>True on success, false on failuree</returns>
        [WebMethod(EnableSession = true)]
        public bool DeleteQuoteItem(int itemID)
        {
            var qiDB = new DAL.QuoteItem(AspSession.UserDatabase);
            return qiDB.Delete(itemID);
        }

        [WebMethod(EnableSession = true)]
        public Types.QuoteItem GetQuoteItem(Int32 ItemID)
        {
            var qiDB = new DAL.QuoteItem(AspSession.UserDatabase);
            return qiDB.GetForId(ItemID);
        }

        /// <summary>
        /// Saves a QuoteItem - the entire QuoteItem being supplied as the only argument
        /// </summary>
        /// <param name="Obj">The QuoteItem to save</param>
        /// <returns>True on success, false on failure.</returns>
        [WebMethod(EnableSession = true)]
        public Boolean SaveQuoteItemObject(Types.QuoteItem Obj)
        {
            var qiDB = new DAL.QuoteItem(AspSession.UserDatabase);
            return qiDB.Save(Obj) > 0;
        }

        [WebMethod(EnableSession = true)]
        public Boolean SaveQuoteItem(Int32 ItemID, String Code, String Name, String Description, Decimal Value, Decimal VatRate)
        {
            var qiDB = new DAL.QuoteItem(AspSession.UserDatabase);
            var QuoteItem = qiDB.GetForId(ItemID);

            QuoteItem.Code = Code;
            QuoteItem.Name = Name;
            QuoteItem.Description = Description;
            QuoteItem.Value = Value;
            QuoteItem.VatRate = VatRate;

            qiDB.Save(QuoteItem);
            return true;
        }



        [WebMethod(EnableSession = true)]
        public Boolean SetQuoteStatus(Int32 QuoteID, Int32 Status)
        {
            if (QuoteHandler.SetQuoteStatus(AspSession.UserDatabase, QuoteID, Status))
            {
                Utility.Logger.Log.DebugFormat("({0}:{1}) Set Quote Status {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, ((Types.Enums.QuoteStatusEnum)Status).ToString());
                return true;
            }
            else
            {
                Utility.Logger.Log.WarnFormat("({0}:{1}) Failed to set quote status for Quote Id {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, QuoteID);
                return false;
            }
        }

        [WebMethod(EnableSession = true)]
        public List<Types.DocumentHistory> GetQuoteHistoryForJob(Int32 JobID)
        {
            return JobHandler.GetHistoryForJob(AspSession.UserDatabase, JobID);
        }
        #endregion

        #region Work

        [WebMethod(EnableSession = true)]
        public Types.WorkItem WorkItemNew(int jobSiteId)
        {
            return BL.WorkFlowHandlers.WorkHandler.WorkItemGet(AspSession.UserDatabase, -1, jobSiteId);
        }

        [WebMethod(EnableSession = true)]
        public Types.WorkItem WorkItemLoad(int itemId)
        {
            return BL.WorkFlowHandlers.WorkHandler.WorkItemGet(AspSession.UserDatabase, itemId);
        }


        [WebMethod(EnableSession = true)]
        public bool WorkItemSave(Types.WorkItem item)
        {
            var itemId = BL.WorkFlowHandlers.WorkHandler.WorkItemSave(AspSession.UserDatabase, item);
            return itemId > 0;
        }


        [WebMethod(EnableSession = true)]
        public bool SetWorkItemsCompleted(List<Types.WorkItem> CompletedItems)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}:{2}) Setting {3} Work Item(s) as completed", AspSession.UserDatabase, AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, CompletedItems != null ? CompletedItems.Count.ToString() : "Null");
            var completed = BL.WorkFlowHandlers.WorkHandler.SetWorkItemsCompleted(AspSession.UserDatabase, CompletedItems);
            if (!completed)
            {
                Utility.Logger.Log.WarnFormat("({0}:{1}:{2}) Failed to set one or more work items to be completed", AspSession.UserDatabase, AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            }
            return completed;
        }

        [WebMethod(EnableSession = true)]
        public Boolean AddJobSiteWorkItemNew(Int32 JobID, Int32 StageID, Int32 JobSiteID, String Code, String Name, String Description, Decimal Value, Decimal VatRate)
        {
            if (StageID == -1)
                return false;

            var Item = new Types.JobItem();
            Item.JobSiteId = JobSiteID;
            Item.Code = Code;
            Item.Name = Name;
            Item.Description = Description;
            Item.Value = Value;
            Item.VatRate = VatRate;

            var JIDB = new DAL.JobItem(AspSession.UserDatabase);
            Item.Id = JIDB.Save(Item);

            var wDB = new DAL.Work(AspSession.UserDatabase);
            var Works = wDB.GetForQuote(StageID);

            Types.Work Work = null;

            if (Works != null && Works.Count > 0)
                Work = Works[0];

            if (Work == null)
            {
                Work = new Types.Work();
                Work.QuoteId = StageID;
                Work.JobId = JobID;
                Work.JobSiteId = JobSiteID;
                Work.AppointmentId = -1;
                Work.TeamId = -1;

                Work.Id = wDB.Save(Work);
            }

            var WItem = new Types.WorkItem();
            WItem.JobItemId = Item.Id;
            WItem.JobSiteId = JobSiteID;
            WItem.Code = Code;
            WItem.Name = Name;
            WItem.Description = Description;
            WItem.Value = Value;
            WItem.VatRate = VatRate;
            WItem.WorkId = Work.Id;
            WItem.QuoteItemId = -1;
            WItem.Completed = false;
            WItem.Status = Types.Enums.ItemStatusEnum.Quoted;

            var wiDB = new DAL.WorkItem(AspSession.UserDatabase);
            wiDB.Save(WItem);

            Utility.Logger.Log.DebugFormat("({0}:{1}) Added New Work Item", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            return true;
        }

        [WebMethod(EnableSession = true)]
        public Types.WorkItem GetWorkItem(Int32 ItemID)
        {
            var qiDB = new DAL.WorkItem(AspSession.UserDatabase);
            return qiDB.GetForId(ItemID);
        }

        [WebMethod(EnableSession = true)]
        public Boolean SaveWorkItem(Int32 ItemID, String Code, String Name, String Description, Decimal Value, Decimal VatRate)
        {
            var wiDB = new DAL.WorkItem(AspSession.UserDatabase);
            var WorkItem = wiDB.GetForId(ItemID);

            WorkItem.Code = Code;
            WorkItem.Name = Name;
            WorkItem.Description = Description;
            WorkItem.Value = Value;
            WorkItem.VatRate = VatRate;

            wiDB.Save(WorkItem);
            return true;
        }
        #endregion

        #region Invoice
        [WebMethod(EnableSession = true)]
        public List<Types.JobItem> GetJobSiteInvoiceItemList(Int32 JobSiteID, Int32 QuoteID)
        {
            List<Types.JobItem> List = new List<Types.JobItem>();

            var wDB = new DAL.Work(AspSession.UserDatabase);
            var jiDB = new DAL.JobItem(AspSession.UserDatabase);
            var wiDB = new DAL.WorkItem(AspSession.UserDatabase);

            var Works = wDB.GetForQuote(QuoteID);

            foreach (var Work in Works)
            {
                var Items = jiDB.GetAllForJobSite(Work.JobSiteId);
                var WorkItems = wiDB.GetAllForWork(Work.Id).FindAll(wi => wi.Status == Types.Enums.ItemStatusEnum.WorkCompleted);
                foreach (var WorkItem in WorkItems)
                {
                    List.Add(Items.Find(i => i.Id == WorkItem.JobItemId));
                }
            }

            return List;
        }

        [WebMethod(EnableSession = true)]
        public Types.InvoiceItem GetInvoiceItem(Int32 ItemID)
        {
            return BL.InvoiceHandler.GetInvoiceItem(AspSession.UserDatabase, ItemID);
        }

        [WebMethod(EnableSession = true)]
        public Boolean SaveInvoiceItem(Int32 ItemID, String Code, String Name, String Description, Decimal Value, Decimal VatRate)
        {
            var iiDB = new DAL.InvoiceItem(AspSession.UserDatabase);
            var InvoiceItem = iiDB.GetForId(ItemID);

            InvoiceItem.Code = Code;
            InvoiceItem.Name = Name;
            InvoiceItem.Description = Description;
            InvoiceItem.Value = Value;
            InvoiceItem.VatRate = VatRate;

            iiDB.Save(InvoiceItem);
            return true;
        }

        [WebMethod(EnableSession = true)]
        public bool SaveInvoiceItem(Types.InvoiceItem obj)
        {
            return BL.InvoiceHandler.SaveInvoiceItem(AspSession.UserDatabase, obj);
        }

        [WebMethod(EnableSession = true)]
        public Boolean AddJobSiteInvoiceItemExisting(Int32 StageID, Int32 JobSiteID, Int32 ItemID, Int32 QuoteID)
        {
            if (StageID == -1)
            {
                var JSDB = new DAL.JobSite(AspSession.UserDatabase);
                var JS = JSDB.GetForId(JobSiteID, false);

                var Invoice = new Types.Invoice();
                Invoice.QuoteId = QuoteID;
                Invoice.JobId = JS.JobId;
                Invoice.Status = Types.Enums.InvoiceStatusEnum.Pending;
                Invoice.ClientId = JS.ClientId;
                Invoice.ContactId = JS.ContactId;

                var IDB = new DAL.Invoice(AspSession.UserDatabase);
                StageID = IDB.Save(Invoice);
            }

            Types.JobItem newJobItem;

            var iiDB = new DAL.InvoiceItem(AspSession.UserDatabase);
            var ExistingInvoiceItems = iiDB.GetAllForInvoice(StageID);

            var JobItemDB = new DAL.JobItem(AspSession.UserDatabase);
            var JobItem = JobItemDB.GetForId(ItemID);

            if (JobItem == null || JobItem.Id == -1) return false;

            if (ExistingInvoiceItems.Exists(ii => ii.JobItemId == JobItem.Id))
            {
                newJobItem = new Types.JobItem()
                {
                    JobId = JobItem.JobId,
                    JobSiteId = JobItem.JobSiteId,
                    TPOId = JobItem.TPOId,
                    Value = JobItem.Value,
                    VatRate = JobItem.VatRate,
                    Code = JobItem.Code,
                    Name = JobItem.Name,
                    Description = JobItem.Description,
                    Latitude = JobItem.Latitude,
                    Longitude = JobItem.Longitude,
                    ImageGuid = JobItem.ImageGuid,
                    Status = JobItem.Status,
                    PreservationStatus = JobItem.PreservationStatus
                };
                newJobItem.Id = JobItemDB.Save(newJobItem);
            }
            else
            {
                newJobItem = JobItem;
            }

            var InvoiceItem = new Types.InvoiceItem();
            InvoiceItem.JobItemId = newJobItem.Id;
            InvoiceItem.InvoiceId = StageID;
            InvoiceItem.Name = newJobItem.Name;
            InvoiceItem.Description = newJobItem.Description;
            InvoiceItem.Value = newJobItem.Value;
            InvoiceItem.VatRate = newJobItem.VatRate;
            InvoiceItem.Code = newJobItem.Code;

            iiDB.Save(InvoiceItem);

            Utility.Logger.Log.DebugFormat("({0}:{1}) Added Existing Invoice Item", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            return true;
        }

        [WebMethod(EnableSession = true)]
        public Boolean AddJobSiteInvoiceItemNew(Int32 JobID, Int32 StageID, Int32 JobSiteID, String Code, String Name, String Description, Decimal Value, Decimal VatRate)
        {
            if (StageID == -1)
            {
                var JDB = new DAL.Job(AspSession.UserDatabase);
                var J = JDB.GetForId(JobID);

                var Invoice = new Types.Invoice();
                Invoice.Site = J.Site;
                Invoice.Status = Types.Enums.InvoiceStatusEnum.Pending;
                Invoice.ClientId = J.ClientId;
                Invoice.ContactId = J.ContactId;

                var iDB = new DAL.Invoice(AspSession.UserDatabase);
                StageID = iDB.Save(Invoice);
            }

            var Item = new Types.JobItem();
            Item.JobSiteId = JobSiteID;
            Item.Code = Code;
            Item.Name = Name;
            Item.Description = Description;
            Item.Value = Value;
            Item.VatRate = VatRate;

            var JIDB = new DAL.JobItem(AspSession.UserDatabase);
            Item.Id = JIDB.Save(Item);

            var QItem = new Types.InvoiceItem();
            QItem.JobItemId = Item.Id;
            QItem.InvoiceId = StageID;
            QItem.JobSiteId = JobSiteID;
            QItem.Code = Code;
            QItem.Name = Name;
            QItem.Description = Description;
            QItem.Value = Value;
            QItem.VatRate = VatRate;


            var iiDB = new DAL.InvoiceItem(AspSession.UserDatabase);
            iiDB.Save(QItem);

            Utility.Logger.Log.DebugFormat("({0}:{1}) Added New Invoice Item}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);

            return true;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.InvoiceHistoryItem> GetInvoiceHistoryForJob(Int32 JobID)
        {
            var DB = new DAL.InvoiceHistoryItem(AspSession.UserDatabase);
            return DB.GetInvoiceDocumentsForJob(JobID);
        }
        #endregion

        [WebMethod(EnableSession = true)]
        public Boolean SetWorkCompleted(Int32 WorkItemID, Boolean Completed)
        {
            var DB = new DAL.WorkItem(AspSession.UserDatabase);
            var WI = DB.GetForId(WorkItemID);
            WI.Completed = Completed;
            WI.Status = Completed ? Types.Enums.ItemStatusEnum.WorkCompleted : Types.Enums.ItemStatusEnum.Scheduled;
            DB.Save(WI);
            return true;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.DocumentHistory> GetHistoryForJob(Int32 JobID)
        {
            var QDB = new DAL.QuoteHistoryItem(AspSession.UserDatabase);
            var IDB = new DAL.InvoiceHistoryItem(AspSession.UserDatabase);

            var List = new List<Types.DocumentHistory>();
            List.AddRange(QDB.GetQuoteDocumentsForJob(JobID));
            List.AddRange(IDB.GetInvoiceDocumentsForJob(JobID));
            List.Sort(new Types.DocumentHistory.Comparer());

            foreach (var Doc in List)
            {
                Doc.Doc = null;
            }

            return List;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.JobDocumentsObject> getAllDefaultJobDocuments()
        {
            var DB = new DAL.Document(AspSession.UserDatabase);
            List<Types.JobDocumentsObject> JobDocuments = DB.GetDocumentsAll();
            return JobDocuments;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.JobDocumentsObject> getJobDocuments(int JobId)
        {
            var DB = new DAL.Document(AspSession.UserDatabase);
            List<Types.JobDocumentsObject> JobDocuments = DB.GetJobDocuments(JobId);
            return JobDocuments;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.JobDocumentsObject> getDefaultJobDocuments(int JobId)
        {
            var DB = new DAL.Document(AspSession.UserDatabase);
            List<Types.JobDocumentsObject> JobDocuments = DB.GetDefaultJobDocuments(JobId);
            return JobDocuments;
        }

        [WebMethod(EnableSession = true)]
        public Types.JobDocumentsObject getDefaultJobDocumentById(int Id)
        {
            var DB = new DAL.Document(AspSession.UserDatabase);
            Types.JobDocumentsObject JobDocuments = DB.GetDefaultJobDocumentById(Id);
            return JobDocuments;
        }
        [WebMethod(EnableSession = true)]
        public Boolean UploadDocument(String Chunk, Boolean New, Boolean Completed, string DocumentName, string DocumentType, string DocumentStaff, string DocumentFileName, int Id, int JobId)
        {

            Utility.Logger.Log.DebugFormat("({0}:{1}) DocumentUploaded starting", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            if (ImageByteString == null || New)
                ImageByteString = "";

            ImageByteString += Chunk;
            var retval = false;
            if (Completed)
            {
                if (ImageByteString == "")
                {

                    Types.JobDocumentsObject obj = new Types.JobDocumentsObject();
                    obj.DocumentDate = DateTime.Now;
                    obj.DocumentName = DocumentName;
                    obj.DocumentType = DocumentType;
                    obj.Staff = DocumentStaff;
                    obj.Id = Id;
                    obj.JobId = JobId;
                    retval = BL.JobHandler.SaveDocumentsHandler(AspSession.UserDatabase, obj) > 0;
                    Utility.Logger.Log.DebugFormat("({0}:{1}) Document Uploaded", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                }
                else
                {
                    var ImageBytes = Convert.FromBase64String(ImageByteString);

                    Types.JobDocumentsObject obj = new Types.JobDocumentsObject();
                    obj.DocumentImage = ImageBytes;
                    obj.DocumentDate = DateTime.Now;
                    obj.DocumentName = DocumentName;
                    obj.DocumentType = DocumentType;
                    obj.Staff = DocumentStaff;
                    obj.DocumentFileName = DocumentFileName;
                    obj.Id = Id;
                    obj.JobId = JobId;
                    ImageByteString = null;
                    retval = BL.JobHandler.SaveDocumentsHandler(AspSession.UserDatabase, obj) > 0;
                    Utility.Logger.Log.DebugFormat("({0}:{1}) Document Uploaded", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                }
            }

            return retval;

        }
        [WebMethod(EnableSession = true)]
        public int ToggleDefaultAttachmentForJob(int DocId, bool DefaultAttachment)
        {
            Utility.Logger.Log.DebugFormat("ToggleDefaultAttachmentForCompany for Doc: {0}", DocId);
            return BL.JobHandler.ToggleDefaultAttachmentForCompany(AspSession.UserDatabase, DocId, DefaultAttachment);
        }
        [WebMethod(EnableSession = true)]
        public Types.JobDocumentsObject GetDocumentForId(int Id)
        {
            var Document_File = BL.JobHandler.GetDocumentForID(AspSession.UserDatabase, Id);
            return Document_File;
        }

        [WebMethod(EnableSession = true)]
        public Boolean DeleteDocument(int Id)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) DocumentUploaded starting", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            var retval = new DAL.Document(AspSession.UserDatabase);
            return retval.Delete(Id);
        }

        [WebMethod(EnableSession = true)]
        public string SaveImageFileOnServer(int Id)
        {
            string fileName = "";
            string filePath = "";

            int id = Id;
            var Document_File = BL.JobHandler.GetDocumentForID(AspSession.UserDatabase, id);

            fileName = Document_File.DocumentFileName;
            Byte[] DocumentFile = Document_File.DocumentImage;
            filePath = "/JobDocuments/" + fileName;
            File.WriteAllBytes(Server.MapPath(filePath), DocumentFile);

            return filePath;
        }
        #region Tree Protection
        [WebMethod(EnableSession = true)]
        public int SaveTpo(Types.TreeProtectionOrder obj)
        {
            return BL.TreeProtectionHandler.SaveTpo(AspSession.UserDatabase, obj);
        }

        [WebMethod(EnableSession = true)]
        public Types.TreeProtectionOrder TpoGetForQuote(int quoteId)
        {
            return BL.TreeProtectionHandler.GetTpoForQuoteId(AspSession.UserDatabase, quoteId);
        }

        [WebMethod(EnableSession = true)]
        public Types.TreeProtectionOrder TpoGetForQuoteAndJobSite(int quoteId, int jobSiteId)
        {
            return BL.TreeProtectionHandler.GetTpoForQuoteIdAndJobSiteId(AspSession.UserDatabase, quoteId, jobSiteId);
        }

        /// <summary>
        /// Retrieve a list of TPO objects for a given quote ID.
        /// </summary>
        /// <param name="quoteId">
        /// The quote ID for which to retrieve the quote.
        /// </param>
        /// <returns>
        /// Returns a list of TPOs for the associated quote ID.
        /// </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.TreeProtectionOrder> TposGetForQuote(int quoteId)
        {
            return BL.TreeProtectionHandler.GetTposForQuoteId(AspSession.UserDatabase, quoteId);
        }

        [WebMethod(EnableSession = true)]
        public Types.TreeProtectionOrder TpoGetForId(int id)
        {
            return BL.TreeProtectionHandler.GetTpoForId(AspSession.UserDatabase, id);
        }
        [WebMethod(EnableSession = true)]
        public List<Types.TreeProtectionOrder> TpoGetForStatus(int status)
        {
            return BL.TreeProtectionHandler.GetTpoForStatus(AspSession.UserDatabase, (Types.Enums.TreeProtectionStatusEnum)status);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.TreeProtectionOrder> TpoGetForStatusNot(int status)
        {
            return BL.TreeProtectionHandler.GetTpoForStatusNot(AspSession.UserDatabase, (Types.Enums.TreeProtectionStatusEnum)status);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.TreeProtectionOrder> TpoGetForProgressStage(int stage)
        {
            return BL.TreeProtectionHandler.GetTpoForProgressStage(AspSession.UserDatabase, (Types.Enums.TreeProtectionProgreesEnum)stage);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.TreeProtectionOrder> TpoGetForProgressStageBelow(int stage)
        {
            return BL.TreeProtectionHandler.GetTpoForProgressStageBelow(AspSession.UserDatabase, (Types.Enums.TreeProtectionProgreesEnum)stage);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.TreeProtectionOrder> TpoGetForProgressStageAbove(int stage)
        {
            return BL.TreeProtectionHandler.GetTpoForProgressStageAbove(AspSession.UserDatabase, (Types.Enums.TreeProtectionProgreesEnum)stage);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.TreeProtectionOrder> TpoGetForRefernceLike(int refType, string refValue)
        {
            return BL.TreeProtectionHandler.GetTpoForRefernceLike(AspSession.UserDatabase, (Types.Enums.TpoReferenceTypeEnum)refType, refValue);
        }
        #endregion
        #region Permits Necessary
        [WebMethod(EnableSession = true)]
        public Types.PermitsNecessary PermitsNecessaryGetForQuote(int quoteId)
        {
            return BL.PermitsNecessaryHandler.GetPermitsNecessaryForQuoteId(AspSession.UserDatabase, quoteId);
        }

        [WebMethod(EnableSession = true)]
        public int SavePermitsNecessary(Types.PermitsNecessary obj)
        {
            return BL.PermitsNecessaryHandler.SavePermitsNecessary(AspSession.UserDatabase, obj);
        }
        #endregion
        #region Images
        String ImageByteString
        {
            get { return (String)Session["ImageByteString"]; }
            set { Session["ImageByteString"] = value; }
        }

        [WebMethod(EnableSession = true)]
        public Boolean UploadImage(Int32 Stage, Int32 ItemID, String Chunk, Boolean New, Boolean Completed)
        {
            if (ImageByteString == null || New)
                ImageByteString = "";

            ImageByteString += Chunk;

            if (Completed)
            {
                var Bytes = Convert.FromBase64String(ImageByteString);
                ImageByteString = null;

                Types.ItemPicture Pic = null;
                var DB = new DAL.ItemPicture(AspSession.UserDatabase);

                int JobItemID = -1;

                switch (Stage)
                {
                    case 0:
                        JobItemID = ItemID;
                        break;
                    case 1:
                        var qDB = new DAL.QuoteItem(AspSession.UserDatabase);
                        JobItemID = qDB.GetForId(ItemID).JobItemId;
                        break;
                    case 2:
                        var wDB = new DAL.WorkItem(AspSession.UserDatabase);
                        JobItemID = wDB.GetForId(ItemID).JobItemId;
                        break;
                    case 3:
                        var iDB = new DAL.InvoiceItem(AspSession.UserDatabase);
                        JobItemID = iDB.GetForId(ItemID).JobItemId;
                        break;
                }

                //var Pics = DB.GetForJobItem(JobItemID);
                //if (Pics.Count == 0)
                //{
                Pic = new Types.ItemPicture();
                Pic.JobItemId = JobItemID;
                Pic.ParentId = ItemID;
                switch (Stage)
                {
                    case 0:
                        Pic.ParentItemType = Types.Enums.ItemTypeEnum.JobItem;
                        break;
                    case 1:
                        Pic.ParentItemType = Types.Enums.ItemTypeEnum.QuoteItem;
                        break;
                    case 2:
                        Pic.ParentItemType = Types.Enums.ItemTypeEnum.WorkItem;
                        break;
                    case 3:
                        Pic.ParentItemType = Types.Enums.ItemTypeEnum.InvoiceItem;
                        break;
                }
                //                }

                //else
                //    Pic = Pics[0];

                Pic.ImageBytes = Bytes;
                DB.Save(Pic);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Uploaded {2} Image", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            }

            return false;
        }


        [WebMethod(EnableSession = true)]
        public List<Types.ItemPicture> GetAllImageByItemId(Int32 Stage, Int32 ItemID)
        {
            List<Types.ItemPicture> lstImage = null;

            int JobItemID = -1;
            switch (Stage)
            {
                case 0:
                    JobItemID = ItemID;
                    break;
                case 1:
                    var qDB = new DAL.QuoteItem(AspSession.UserDatabase);
                    JobItemID = qDB.GetForId(ItemID).JobItemId;
                    break;
                case 2:
                    var wDB = new DAL.WorkItem(AspSession.UserDatabase);
                    JobItemID = wDB.GetForId(ItemID).JobItemId;
                   // QuoteId = wDB.GetForId(ItemID).q;
                    break;
                case 3:
                    var iDB = new DAL.InvoiceItem(AspSession.UserDatabase);
                    JobItemID = iDB.GetForId(ItemID).JobItemId;
                    break;
                default:
                    lstImage = null;
                    break;
            }

            var DB = new DAL.ItemPicture(AspSession.UserDatabase);
            lstImage = DB.GetForJobItem(JobItemID);

            return lstImage;
        }

        [WebMethod(EnableSession = true)]
        public void GenerateImageFromByte(byte[] ImageBytes)
        {
            HttpResponse Response = HttpContext.Current.Response;

            var memStream = new MemoryStream(ImageBytes);
            var Image = System.Drawing.Image.FromStream(memStream);
            ImageCodecInfo FileCodec = null;
            Guid Guid = Image.RawFormat.Guid;
            ImageFormat ImageFormat = ImageFormat.Png;

            foreach (var Codec in System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders())
            {
                if (Codec.FormatID == Guid) FileCodec = Codec;
            }

            try
            {
                if (Response.OutputStream.CanWrite)
                {
                    Response.Clear();
                    Response.ContentType = FileCodec.MimeType;
                    memStream.WriteTo(Response.OutputStream);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                memStream.Close();
            }

        }


        /// <summary>
        /// Method to clear an image of the specified type from the company
        /// </summary>
        /// <param name="Stage"> 0 - job item, 1 - quote item, 2 - work item, 3 - invoice item</param>
        /// <param name="ItemID"> the job item id</param>
        /// <returns>true or false</returns>
        [WebMethod(EnableSession = true)]
        public Boolean ClearImage(Int32 Stage, int ItemID, int PictureId = -1)
        {
            var retval = false;

            int JobItemID = -1;

            switch (Stage)
            {
                case 0:
                    JobItemID = ItemID;
                    break;
                case 1:
                    var qDB = new DAL.QuoteItem(AspSession.UserDatabase);
                    JobItemID = qDB.GetForId(ItemID).JobItemId;
                    break;
                case 2:
                    var wDB = new DAL.WorkItem(AspSession.UserDatabase);
                    JobItemID = wDB.GetForId(ItemID).JobItemId;
                    break;
                case 3:
                    var iDB = new DAL.InvoiceItem(AspSession.UserDatabase);
                    JobItemID = iDB.GetForId(ItemID).JobItemId;
                    break;
            }

            var DB = new DAL.ItemPicture(AspSession.UserDatabase);
            retval = DB.ClearImage(JobItemID, PictureId);

            return retval;
        }

        [WebMethod(EnableSession = true)]
        public Boolean RotateImage(Int32 Stage, Int32 ItemID, Int32 Degrees, Int32 PictureId = -1)
        {
            #region Retrieve Picture
            Types.ItemPicture Pic = null;
            var DB = new DAL.ItemPicture(AspSession.UserDatabase);

            int JobItemID = -1;

            switch (Stage)
            {
                case 0:
                    JobItemID = ItemID;
                    break;
                case 1:
                    var qDB = new DAL.QuoteItem(AspSession.UserDatabase);
                    JobItemID = qDB.GetForId(ItemID).JobItemId;
                    break;
                case 2:
                    var wDB = new DAL.WorkItem(AspSession.UserDatabase);
                    JobItemID = wDB.GetForId(ItemID).JobItemId;
                    break;
                case 3:
                    var iDB = new DAL.InvoiceItem(AspSession.UserDatabase);
                    JobItemID = iDB.GetForId(ItemID).JobItemId;
                    break;
            }

            var Pics = DB.GetForJobItem(JobItemID);

            if (Pics.Count == 0)
                return false;

            if (PictureId != -1)
                Pic = Pics[PictureId];
            else
                Pic = Pics[0];
            #endregion

            #region Create Bitmap
            var ms = new MemoryStream(Pic.ImageBytes);
            var Image = System.Drawing.Image.FromStream(ms);
            ImageCodecInfo FileCodec = null;
            Guid Guid = Image.RawFormat.Guid;
            ImageFormat ImageFormat = ImageFormat.Png;

            foreach (var Codec in System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders())
            {
                if (Codec.FormatID == Guid)
                    FileCodec = Codec;
            }
            #endregion

            switch (Degrees)
            {
                case 90:
                    Image.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);
                    break;
                case -90:
                    Image.RotateFlip(System.Drawing.RotateFlipType.Rotate270FlipNone);
                    break;
            }

            #region Save Image
            ms.Dispose();
            var ms2 = new MemoryStream();
            Image.Save(ms2, ImageFormat);

            Pic.ImageBytes = ms2.ToArray();
            Image.Dispose();
            ms2.Dispose();
            DB.Save(Pic);
            #endregion

            return true;
        }

        [WebMethod(EnableSession = true)]
        public Boolean FlipImage(Int32 Stage, Int32 ItemID, String Axis, Int32 PictureId = -1)
        {
            #region Retrieve Picture
            Types.ItemPicture Pic = null;
            var DB = new DAL.ItemPicture(AspSession.UserDatabase);

            int JobItemID = -1;

            switch (Stage)
            {
                case 0:
                    JobItemID = ItemID;
                    break;
                case 1:
                    var qDB = new DAL.QuoteItem(AspSession.UserDatabase);
                    JobItemID = qDB.GetForId(ItemID).JobItemId;
                    break;
                case 2:
                    var wDB = new DAL.WorkItem(AspSession.UserDatabase);
                    JobItemID = wDB.GetForId(ItemID).JobItemId;
                    break;
                case 3:
                    var iDB = new DAL.InvoiceItem(AspSession.UserDatabase);
                    JobItemID = iDB.GetForId(ItemID).JobItemId;
                    break;
            }

            var Pics = DB.GetForJobItem(JobItemID);

            if (Pics.Count == 0)
                return false;

            if (PictureId != -1)
                Pic = Pics[PictureId];
            else
                Pic = Pics[0];
            #endregion

            #region Create Bitmap
            var ms = new MemoryStream(Pic.ImageBytes);
            var Image = System.Drawing.Image.FromStream(ms);
            ImageCodecInfo FileCodec = null;
            Guid Guid = Image.RawFormat.Guid;
            ImageFormat ImageFormat = ImageFormat.Png;

            foreach (var Codec in System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders())
            {
                if (Codec.FormatID == Guid)
                    FileCodec = Codec;
            }
            #endregion

            switch (Axis)
            {
                case "Horizontal":
                    Image.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipY);
                    break;
                case "Vertical":
                    Image.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipX);
                    break;
            }

            #region Save Image
            ms.Dispose();
            var ms2 = new MemoryStream();
            Image.Save(ms2, ImageFormat);

            Pic.ImageBytes = ms2.ToArray();
            Image.Dispose();
            ms2.Dispose();
            DB.Save(Pic);
            #endregion

            return true;
        }
        #endregion

        [WebMethod(EnableSession = true)]
        public Int32 SaveWorkPlan(Int32 QuoteID, Int32 WorkID, Int32 TeamID, Int32 StaffRequired, DateTime StartDate, DateTime EndDate, Int32 SkipDays, Int32 JobSiteID, String Comments)
        {
            var wpDB = new DAL.WorkPlan(AspSession.UserDatabase);
            var WorkPlan = new Types.WorkPlan()
            {
                QuoteId = QuoteID,
                WorkId = WorkID,
                TeamId = TeamID,
                StaffRequired = StaffRequired
            };
            WorkPlan.Id = wpDB.Save(WorkPlan);

            var cwDB = new DAL.Calendar(AspSession.UserDatabase);
            var CalendarWork = new Types.CalendarWork()
            {
                StartDateTime = StartDate,
                EndDateTime = EndDate,
                SkipDays = (Types.Enums.CalendarSkipDaysEnum)SkipDays,
                JobSiteId = JobSiteID,
                TeamId = TeamID,
                WorkPlanId = WorkPlan.Id,
                Comments = Comments
            };
            if (cwDB.SaveWork(CalendarWork))
            {
                Utility.Logger.Log.DebugFormat("({0}:{1}) Saved Work Plan", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                return WorkPlan.Id;
            }
            return -1;
        }

        [WebMethod(EnableSession = true)]
        public Boolean SaveWorkPlanTool(Int32 WorkPlanID, String Tool, Int32 Quantity)
        {
            var tDB = new DAL.WorkTool(AspSession.UserDatabase);
            var WorkTool = new Types.WorkTool()
            {
                Id = -1,
                Item = Tool,
                Number = Quantity,
                ToolId = -1,
                WorkPlanId = WorkPlanID
            };
            var Return = tDB.Save(WorkTool);

            Utility.Logger.Log.DebugFormat("({0}:{1}) Saved Work Tool", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);

            return Return;
        }

        #region Notes
        [WebMethod(EnableSession = true)]
        public Types.Note GetNote(int parentId, int noteId, int type)
        {
            return BL.NoteHandler.GetNote(AspSession.CurrentUser, parentId, noteId, (Types.Enums.NoteTypeEnum)type);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Note> GetNotesList(int parentId, int type)
        {
            return BL.NoteHandler.GetNotesForParent(AspSession.UserDatabase, parentId, (Types.Enums.NoteTypeEnum)type);
        }

        //[WebMethod(EnableSession = true)]
        //public Types.Note GetQuoteNote(int quoteId, int id)
        //{
        //    if (id < 1)
        //        return BL.NoteHandler.GetNewNote(quoteId, Types.Enums.NoteTypeEnum.Quote, AspSession.CurrentUser.NameStr);
        //    else
        //        return BL.NoteHandler.GetForId(AspSession.UserDatabase, id);
        //}

        //[WebMethod(EnableSession = true)]
        //public Types.Note GetWorkNote(int workId, int id)
        //{
        //    if (id < 1)
        //        return BL.NoteHandler.GetNewNote(workId, Types.Enums.NoteTypeEnum.Work, AspSession.CurrentUser.NameStr);
        //    else
        //        return BL.NoteHandler.GetForId(AspSession.UserDatabase, id);
        //}

        //[WebMethod(EnableSession = true)]
        //public Types.Note GetInvoiceNote(int invoiceId, int id)
        //{
        //    if (id < 1)
        //        return BL.NoteHandler.GetNewNote(invoiceId, Types.Enums.NoteTypeEnum.Invoice, AspSession.CurrentUser.NameStr);
        //    else
        //        return BL.NoteHandler.GetForId(AspSession.UserDatabase, id);
        //}

        //[WebMethod(EnableSession = true)]
        //public List<Types.Note> GetNotesForQuote(int quoteId)
        //{
        //    return BL.NoteHandler.GetNotesForParent(AspSession.UserDatabase, quoteId, Types.Enums.NoteTypeEnum.Quote);
        //}

        //[WebMethod(EnableSession = true)]
        //public List<Types.Note> GetNotesForWork(int workId)
        //{
        //    return BL.NoteHandler.GetNotesForParent(AspSession.UserDatabase, workId, Types.Enums.NoteTypeEnum.Work);
        //}

        [WebMethod(EnableSession = true)]
        public int SaveNote(Types.Note obj)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Created Note {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, obj.Text);
            return BL.NoteHandler.Save(AspSession.UserDatabase, obj);
        }
        #endregion

        #region Invoices
        [WebMethod(EnableSession = true)]
        public Types.Invoice GetInvoiceForId(int invoiceId)
        {
            return BL.InvoiceHandler.GetInvoice(AspSession.UserDatabase, invoiceId);
        }

        [WebMethod(EnableSession = true)]
        public Types.InvoiceItem GetNewInvoiceItem()
        {
            return new Types.InvoiceItem();
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Note> GetInvoiceNotesForQuote(int quoteId)
        {
            return BL.NoteHandler.GetInvoiceNotesForQuote(AspSession.UserDatabase, quoteId);
        }

        [WebMethod(EnableSession = true)]
        public Types.InvoiceSummary GetInvoiceSummaryForQuote(int quoteId)
        {
            return BL.InvoiceHandler.GetSummaryForQuote(AspSession.UserDatabase, quoteId);
        }

        [WebMethod(EnableSession = true)]
        public Types.Invoice GetInvoiceForQuote(int quoteId)
        {
            return BL.InvoiceHandler.GetCurrentInvoice(AspSession.UserDatabase, quoteId);
        }
        [WebMethod(EnableSession = true)]
        public Types.Invoice GetInvoiceForInvoice(int invoiceId)
        {
            return BL.InvoiceHandler.GetCurrentInvoiceForPaid(AspSession.UserDatabase, invoiceId);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Invoice> GetSentInvoicesForQuote(int quoteId)
        {
            return BL.InvoiceHandler.GetSentInvoicesForQuote(AspSession.UserDatabase, quoteId);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.InvoiceItem> GetInvoicableWorkItems(int quoteId)
        {
            return BL.InvoiceHandler.GetInvoicableItems(AspSession.UserDatabase, quoteId);
        }

        [WebMethod(EnableSession = true)]
        public bool SetInvoiceStatus(int invoiceId, Types.Enums.InvoiceStatusEnum status)
        {
            return BL.InvoiceHandler.SetStatus(AspSession.UserDatabase, invoiceId, status);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.InvoiceHistoryItem> GetInvoiceHistoryForQuote(int quoteId)
        {
            return BL.InvoiceHandler.GetInvoiceHistoryForQuote(AspSession.UserDatabase, quoteId, false);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.JobSite> GetWorkSitesForQuote(int quoteId)
        {
            return BL.WorkFlowHandlers.JobHandler.GetJobSitesForQuote(AspSession.UserDatabase, quoteId, false);
        }

        [WebMethod(EnableSession = true)]
        public int SaveInvoice(Types.Invoice obj, List<Types.InvoiceItem> selectedItems, List<Types.InvoiceItem> deletedItems)
        {
            return BL.InvoiceHandler.SaveInvoice(AspSession.UserDatabase, obj, selectedItems, deletedItems);
        }
        [WebMethod(EnableSession = true)]
        public int UpdatePaidInvoiceDate(DateTime PaidDate, int Id, DateTime DateSent, DateTime DueDate)
        {
            Types.Invoice obj = new Types.Invoice();
            obj.Id = Id;
            obj.PaidDate = PaidDate;
            obj.DateSent = DateSent;
            obj.DueDate = DueDate;
            return BL.InvoiceHandler.SaveInvoicePaidDate(AspSession.UserDatabase, obj);
        }
        [WebMethod(EnableSession = true)]
        public int UpdateSentInvoiceDate(int Id, DateTime DateSent, DateTime DueDate)
        {
            Types.Invoice obj = new Types.Invoice();
            obj.Id = Id;
            obj.DateSent = DateSent;
            obj.DueDate = DueDate;
            return BL.InvoiceHandler.SaveInvoiceSentDate(AspSession.UserDatabase, obj);
        }

        #endregion

        #region
        [WebMethod(EnableSession = true)]
        public bool JobDetailsUpdate(Types.Job obj)
        {
            var updatedId = BL.WorkFlowHandlers.JobHandler.UpdateJobDetails(obj, AspSession.UserDatabase);
            return updatedId > 0;
        }
        #endregion
    }
}
