﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;

namespace DGS.ArbPro.Web.Services.Web
{
    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Calendar : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true)]
        public List<Types.CalendarGeneric> GetGenericEvents(Int32 StaffID)
        {
            List<Types.CalendarGeneric> ReturnValue = new List<Types.CalendarGeneric>();

            var DB = new DAL.Calendar(AspSession.UserDatabase);
            ReturnValue.AddRange(DB.GetGenericList(DateTime.MinValue, DateTime.MaxValue));

            if (StaffID != -1)
            {
                ReturnValue.RemoveAll(e => e.StaffID > 0 && e.StaffID != StaffID);
            }

            return ReturnValue;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.CalendarGeneric> GetGenericEvents(DateTime Start, DateTime End, Int32 StaffID)
        {
            List<Types.CalendarGeneric> ReturnValue = new List<Types.CalendarGeneric>();

            var DB = new DAL.Calendar(AspSession.UserDatabase);
            ReturnValue.AddRange(DB.GetGenericList(Start, End));

            if (StaffID != -1)
            {
                ReturnValue.RemoveAll(e => e.StaffID > 0 && e.StaffID != StaffID);
            }

            return ReturnValue;
        }

        [WebMethod(EnableSession = true)]
        public bool DeleteGenericEvent(int eventId)
        {
            return BL.CalendarHandler.DeleteGenericAppointment(AspSession.UserDatabase, eventId);
        }

        [WebMethod(EnableSession = true)]
        public bool DeleteQuoteEvent(int eventId)
        {
            return BL.CalendarHandler.DeleteQuoteAppointment(AspSession.UserDatabase, eventId);
        }

        [WebMethod(EnableSession = true)]
        public Boolean SaveGenericEvent(Int32 EventID, String Start, String End, Boolean AllDay, String Name, String Comment, Int32 EventType, Int32 StaffID)
        {
            DateTime StartDT = DateTime.Parse(Start);
            DateTime EndDT = DateTime.Parse(End);

            Types.CalendarGeneric Event = new Types.CalendarGeneric()
            {
                Id = EventID,
                StartDateTime = StartDT,
                EndDateTime = EndDT,
                AllDay = AllDay,
                Name = Name,
                Comment = Comment,
                Type = (Types.Enums.CalendarGenericType)EventType,
                StaffID = StaffID
            };

            var DB = new DAL.Calendar(AspSession.UserDatabase);
            var ReturnValue = DB.SaveGeneric(Event);
            Utility.Logger.Log.DebugFormat("({0}:{1}) Generic Event Saved", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            return ReturnValue;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.CalendarQuote> GetQuoteEventsNoDate(Int32 StaffID)
        {
            List<Types.CalendarQuote> ReturnValue = new List<Types.CalendarQuote>();

            var DB = new DAL.Calendar(AspSession.UserDatabase);
            ReturnValue.AddRange(DB.GetQuoteList(DateTime.MinValue, DateTime.MaxValue));

            if (StaffID != -1)
            {
                ReturnValue.RemoveAll(e => e.SurveyorID != StaffID);
            }

            return ReturnValue;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.CalendarQuote> GetQuoteEvents(DateTime Start, DateTime End, Int32 StaffID)
        {
            List<Types.CalendarQuote> ReturnValue = new List<Types.CalendarQuote>();

            var DB = new DAL.Calendar(AspSession.UserDatabase);
            ReturnValue.AddRange(DB.GetQuoteList(Start, End));

            if (StaffID != -1)
            {
                ReturnValue.RemoveAll(e => e.SurveyorID != StaffID);
            }

            return ReturnValue;
        }

        [WebMethod(EnableSession = true)]
        public Boolean SaveQuoteEvent(Int32 EventID, DateTime Start, DateTime End, Int32 StaffID, Int32 JobSiteID, Boolean Completed, DateTime LastUpdated, DateTime LastDownloaded, DateTime UploadTime, int TemplateId)
        {
            Types.CalendarQuote Event = new Types.CalendarQuote()
            {
                Id = EventID,
                StartDateTime = Start,
                EndDateTime = End,
                SurveyorID = StaffID,
                JobSiteID = JobSiteID,
                Completed = Completed,
                LastUpdated = LastUpdated,
                LastDownloaded = LastDownloaded,
                UploadTime = UploadTime,
                TemplateId = TemplateId
            };

            var DB = new DAL.Calendar(AspSession.UserDatabase);
            var ReturnValue = DB.SaveQuote(Event);
            Utility.Logger.Log.DebugFormat("({0}:{1}) Quote Event Saved", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            return ReturnValue;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.CalendarWork> GetWorkEvents(DateTime Start, DateTime End, Int32 StaffID)
        {
            var DB = new DAL.Calendar(AspSession.UserDatabase);
            var ReturnValue = DB.GetWorkListForStaff(Start, End, StaffID);
            return ReturnValue;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.CalendarWork> GetWorkBookingsForTeam(Int32 TeamID, DateTime Start, DateTime End)
        {
            var DB = new DAL.Calendar(AspSession.UserDatabase);
            var ReturnValue = DB.GetWorkList(Start, End);
            ReturnValue.RemoveAll(w => w.TeamId != TeamID);
            return ReturnValue;
        }

        /// <summary>
        /// Service endpoint to get all work bookings to be displayed on
        /// the calendar for a given date range
        /// </summary>
        /// <param name="Start">DateTime: The start date</param>
        /// <param name="End">DateTime: The end date</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public List<Types.CalendarWork> GetWorkBookings(DateTime Start, DateTime End)
        {
            return BL.WorkProgrammingHandler.GetBookedWork(AspSession.UserDatabase, Start, End);
        }

        /// <summary>
        /// Servie endpoint to get all work booking to be displayed on the calendar
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="workPlanId"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public List<Types.CalendarWork> GetWorkBookingsAroundWorkPlan(DateTime start, DateTime end, int workPlanId)
        {
            return BL.WorkProgrammingHandler.GetBookedWork(AspSession.UserDatabase, start, end, workPlanId);
        }

        [WebMethod(EnableSession = true)]
        public Types.CalendarWork GetAppointmentForWorkPlan(int workPlanId)
        {
            return BL.WorkProgrammingHandler.GeAppointmentForWorkPlan(AspSession.UserDatabase, workPlanId);
        }

        /// <summary>
        /// Returns an appointment <see cref="CalendarWork"/> for a given appointment ID (CalendarWork.Id).
        /// </summary>
        /// <param name="appointmentId">
        /// The ID of the appointment to return.
        /// </param>
        /// <returns>
        /// The <see cref="CalendarWork"/> for a given ID.
        /// </returns>
        [WebMethod(EnableSession = true)]
        public Types.CalendarWork GeAppointmentForId(int appointmentId)
        {
            return BL.WorkProgrammingHandler.GeAppointmentForId(AspSession.UserDatabase, appointmentId);
        }
    }
}
