﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.IO;

using DGS.ArbPro.BL;
using DGS.ArbPro.BL.ListItemHandlers;
using DGS.ArbPro.BL.SearchHandlers;
using DGS.ArbPro.Utility;

namespace DGS.ArbPro.Web.Services.Web
{
    using DGS.ArbPro.DAL;

    using ImageCodecInfo = System.Drawing.Imaging.ImageCodecInfo;

    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Company : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public int SaveCompanyContact(Types.CompanyContact obj)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Company Contact {2} Saved", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, obj.Id);
            return BL.CompanyHandler.SaveContact(AspSession.UserDatabase, obj);
        }

        [WebMethod(EnableSession = true)]
        public Types.CompanyContact GetContact(int id)
        {
            return BL.CompanyHandler.GetContact(AspSession.UserDatabase, id);
        }

        [WebMethod(EnableSession = true)]
        public Types.SmtpSettings GetEmailSettings()
        {
            DAL.SmtpSettings dl = new DAL.SmtpSettings(AspSession.UserDatabase);
            return dl.GetEmailSettings();
        }

        [WebMethod(EnableSession = true)]
        public bool SaveEmailSmtpSettings(Types.SmtpSettings emailObj)
        {
            bool email = false;
            DAL.SmtpSettings dl = new DAL.SmtpSettings(AspSession.UserDatabase);
            email = dl.SaveEmail(emailObj);

            return email;
        }

        [WebMethod(EnableSession = true)]
        public bool SaveUserSmtpSettings(Types.SmtpSettings userObj)
        {
            bool usr = false;
            DAL.SmtpSettings dl = new DAL.SmtpSettings(AspSession.UserDatabase);
            usr = dl.SaveUser(userObj);

            return usr;
        }

        [WebMethod(EnableSession = true)]
        public Types.SmtpSettings GetUserSettings()
        {
            DAL.SmtpSettings dl = new DAL.SmtpSettings(AspSession.UserDatabase);
            return dl.GetUserSettings(AspSession.CurrentUser.Id);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.CompanyContact> GetCompanyContacts()
        {
            return BL.CompanyHandler.GetCompanyContacts(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.DocumentReference> GetDocumentReferences()
        {
            return BL.CompanyHandler.GetDocumentReferences(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public bool UpdateQuoteReferences(Types.DocumentReference obj)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Quote References Updated", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            return BL.CompanyHandler.UpdateQuoteReferences(AspSession.UserDatabase, obj);
        }

        [WebMethod(EnableSession = true)]
        public bool UpdateInvoiceReferences(Types.DocumentReference obj)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Invoice References Updated", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            return BL.CompanyHandler.UpdateInvoiceReferences(AspSession.UserDatabase, obj);
        }

        [WebMethod(EnableSession = true)]
        public bool UpdateCreditReferences(Types.DocumentReference obj)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Credit References Updated", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            return BL.CompanyHandler.UpdateCreditRReferences(AspSession.UserDatabase, obj);
        }

        [WebMethod(EnableSession = true)]
        public int ToggleDefaultAttachmentForCompany(int DocId, bool DefaultAttachment)
        {
            Utility.Logger.Log.DebugFormat("ToggleDefaultAttachmentForCompany for Doc: {0}", DocId);
            return BL.CompanyHandler.ToggleDefaultAttachmentForCompany(AspSession.UserDatabase, DocId, DefaultAttachment);
        }
        [WebMethod(EnableSession = true)]
        public List<Types.CompanyDocumentsObject> GetDefaultAttachmentForCompany()
        {
            Utility.Logger.Log.DebugFormat("GetDefaultAttachmentForCompany is executed");
            return BL.CompanyHandler.GetDefaultAttachmentForCompany(AspSession.UserDatabase);
        }
        [WebMethod(EnableSession = true)]
        public Types.Company GetCompany()
        {
            return BL.CompanyHandler.GetCompany(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public decimal GetCompanyTaxSetting()
        {
            return BL.CompanyHandler.GetCompany(AspSession.UserDatabase).VatRate;
        }


        [WebMethod(EnableSession=true)]
        public bool SaveCompany(Types.Company Company)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Company Data Updated", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            return BL.CompanyHandler.SaveCompany(AspSession.UserDatabase, Company);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Staff> ListAllStaff()
        {
            var StaffList = BL.CompanyHandler.ListAllStaff(AspSession.UserDatabase);
            return StaffList;
        }
        [WebMethod(EnableSession = true)]
        public List<Types.CompanyDocumentsObject> ListAllDocuments()
        {
            var DocumentList = BL.CompanyHandler.ListAllDocuments(AspSession.UserDatabase);
            return DocumentList;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Staff> ListSurveyors()
        {
            var StaffList = BL.CompanyHandler.ListAllStaff(AspSession.UserDatabase);
            StaffList.RemoveAll(s => s.Position != Types.Enums.StaffRoleEnum.Surveyor);
            return StaffList;
        }

        [WebMethod(EnableSession = true)]
        public Types.Staff GetStaffForId(int Id)
        {
            var StaffMember = BL.CompanyHandler.GetStaffForID(AspSession.UserDatabase, Id);
            return StaffMember;
        }
        [WebMethod(EnableSession = true)]
        public Types.CompanyDocumentsObject GetDocumentForId(int Id)
        {
            var Document_File = BL.CompanyHandler.GetDocumentForID(AspSession.UserDatabase, Id);
            return Document_File;
        }

        [WebMethod(EnableSession = true)]
        public Types.Staff GetNewStaffObj()
        {
            var obj = new Types.Staff();
            obj.Signature = new Byte[0];
            return obj;
        }

        [WebMethod(EnableSession = true)]
        public int SaveStaff(Types.Staff staff)
        {
            var Save = BL.CompanyHandler.SaveStaffMember(AspSession.UserDatabase, staff, AspSession.CurrentUser.CompanyID);
            Utility.Logger.Log.DebugFormat("({0}:{1}) Staff Member {2} Saved", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, Save);
            return Save;
        }
        
        [WebMethod(EnableSession = true)]
        public Types.Client GetClient(Int32 ClientID)
        {
            var DB = new DAL.Client(AspSession.UserDatabase);
            return DB.GetForClientId(ClientID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetClientList(String FilterText)
        {
            return SearchResultHandler.GetClientForNameLike(AspSession.UserDatabase, FilterText);
        }

        //[WebMethod(EnableSession = true)]
        //public bool SaveContactObject(Types.ClientContact obj)
        //{
        //    var retVal = BL.ClientHandler.SaveClientContact(AspSession.UserDatabase, obj);
        //    return retVal > 0;
        //}

        [WebMethod(EnableSession = true)]
        public Boolean SaveContact(Int32 ContactID, Int32 ClientID, String Title, String Forename, String Surname, String Telephone, String Mobile, String Email, String StreetNumber, String Address1, String Address2, String Town, String County, String PostCode, bool SendSms)
        {
            var DB = new DAL.ClientContact(AspSession.UserDatabase);
            Types.ClientContact obj = null;
            if (ContactID == -1)
                obj = new Types.ClientContact();
            else
                obj = DB.GetForId(ContactID);

            if (obj != null)
            {
                obj.Title = Title;
                obj.ClientId = ClientID;
                obj.Forename = Forename;
                obj.Surname = Surname;
                obj.Telephone = Telephone;
                obj.Mobile = Mobile;
                obj.Email = Email;
                obj.StreetNumber = StreetNumber;
                obj.Address1 = Address1;
                obj.Address2 = Address2;
                obj.Town = Town;
                obj.County = County;
                obj.PostCode = PostCode;
                obj.Id = DB.Save(obj);
                //V3.7.0
                obj.SendSmsMessage = true;
                Utility.Logger.Log.DebugFormat("({0}:{1}) Contact {2} Saved", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, obj.Id);
                return obj.Id != -1;
            }
            else return false;
        }

        [WebMethod(EnableSession = true)]
        public Boolean SaveClientSite(Int32 ClientSiteID, Int32 ClientID, Int32 ContactID, String Reference, String HouseNameNo, String Address1, String Address2, String Town, String County, String PostCode, double Latitude, double Longitude)
        {
            var DB = new DAL.ClientSite(AspSession.UserDatabase);
            Types.ClientSite obj = null;
            if (ClientSiteID == -1)
            {
                obj = new Types.ClientSite();
            }
            else
            {
                obj = DB.GetForId(ClientSiteID);
            }

            if (obj != null)
            {
                obj.ClientId = ClientID;
                obj.ContactId = ContactID;
                obj.HouseNameNo = HouseNameNo;
                obj.Address1 = Address1;
                obj.Address2 = Address2;
                obj.Town = Town;
                obj.County = County;
                obj.PostCode = PostCode;
                obj.Latitude = Latitude;
                obj.Longitude = Longitude;
                obj.Reference = Reference;
                var ID = DB.Save(obj);
                Utility.Logger.Log.DebugFormat("({0}:{1}) Client Site {2} Saved", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, ID);
                return true;
            }
            else return false;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.ContactSearchResult> GetClientContactList(Int32 ClientID)
        {
            return SearchResultHandler.GetClientContactList(AspSession.UserDatabase, ClientID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.ClientSite> GetClientSiteList(Int32 ClientID)
        {
            return SearchResultHandler.GetClientSiteList(AspSession.UserDatabase, ClientID);
        }

        [WebMethod(EnableSession = true)]
        public Types.ClientSite GetClientSite(Int32 ClientSiteID)
        {
            DAL.ClientSite DB = new DAL.ClientSite(AspSession.UserDatabase);
            return DB.GetForId(ClientSiteID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.SearchResult> GetClientJobList(Int32 ClientID)
        {
            return SearchResultHandler.GetClientJobList(AspSession.UserDatabase, ClientID);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.AppointmentSearchResult> GetClientAppointmentList(Int32 ClientID)
        {
            return SearchResultHandler.GetClientAppointmentList(AspSession.UserDatabase, ClientID, true);
        }

        [WebMethod(EnableSession = true)]
        public Int32 CreateSalesEnquiry(
            Int32 ClientID,
            Int32 SurveyorID,
            String Source,
            String Comments,
            DateTime ApptStart,
            DateTime ApptEnd,
            String SiteReference,
            Int32 SiteID,
            String SiteHouseNameNumber,
            String SiteAddress1,
            String SiteAddress2,
            String SiteTown,
            String SiteCounty,
            String SitePostCode,
            int TemplateId)
        {
            var ClientDB = new DAL.Client(AspSession.UserDatabase);
            Types.Client enqClient = ClientDB.GetForClientId(ClientID);

            var JobDB = new DAL.Job(AspSession.UserDatabase);
            Types.Job newJob = new Types.Job()
            {
                ClientId = enqClient.Id,
                ContactId = enqClient.Contacts.Find(ct => ct.ContactType == Types.Enums.ContactTypeEnum.Default).Id,
                Source = Source,
                Comments = Comments,
                ReporterID = AspSession.CurrentUser.Id,
                Reporter = string.Format("{0} {1}", AspSession.CurrentUser.FirstName, AspSession.CurrentUser.LastName)
            };

            newJob.Id = JobDB.Save(newJob);

            if (newJob.Id == -1)
                return -1;

            DAL.ClientSite ClientSiteDB = new DAL.ClientSite(AspSession.UserDatabase);
            Types.ClientSite CSite = null;

            if (SiteID == -1)
            {
                CSite = new Types.ClientSite()
                {
                    HouseNameNo = SiteHouseNameNumber,
                    Address1 = SiteAddress1,
                    Address2 = SiteAddress2,
                    Town = SiteTown,
                    County = SiteCounty,
                    PostCode = SitePostCode,
                    ClientId = enqClient.Id,
                    ContactId = newJob.ContactId,
                    Reference = SiteReference
                };
                CSite.Id = ClientSiteDB.Save(CSite);
            }
            else
            {
                CSite = ClientSiteDB.GetForId(SiteID);
            }

            DAL.JobSite JobSiteDB = new DAL.JobSite(AspSession.UserDatabase);
            Types.JobSite newJSite = new Types.JobSite()
            {
                ClientId = enqClient.Id,
                JobId = newJob.Id,
                Site = CSite,
                ContactId = newJob.ContactId
            };

            newJSite.Id = JobSiteDB.Save(newJSite);
            SiteID = newJSite.Id;

            if (SiteID == -1)
                return -1;

            if (ApptStart != null)
            {
                var CalendarQuoteDB = new DAL.Calendar(AspSession.UserDatabase);
                Types.CalendarQuote newQuote = new Types.CalendarQuote()
                {
                    StartDateTime = ApptStart,
                    EndDateTime = ApptEnd,
                    SurveyorID = SurveyorID,
                    JobSiteID = SiteID,
                    Completed = false,
                    LastUpdated = DateTime.Now,
                    LastDownloaded = DateTime.MinValue,
                    UploadTime = DateTime.MinValue,
                    TemplateId = TemplateId
                };
                CalendarQuoteDB.SaveQuote(newQuote);
            }

            Utility.Logger.Log.DebugFormat("({0}:{1}) New Job {2} Created", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, newJob.Id);
            return newJob.Id;
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListTitles()
        {
            return PickItemHandler.GetTitleList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListTowns()
        {
            return PickItemHandler.GetTownList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListCounties()
        {
            return PickItemHandler.GetCountyList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListEnquirySource()
        {
            return PickItemHandler.GetLeadSourcesList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListTreeNames()
        {
            return TreeNameHandler.GetDefaultList(AspSession.UserDatabase);
        }

        [WebMethod(EnableSession = true)]
        public List<string> ListWorkDescriptions()
        {
            return WorkDescriptionHandler.GetList(AspSession.UserDatabase);
        }

        String ImageByteString
        {
            get { return (String)Session["ImageByteString"]; }
            set { Session["ImageByteString"] = value; }
        }

        /// <summary>
        /// Method to flip images in the company's PDF and Email Options
        /// </summary>
        /// <param name="Stage">Indicates the type of image uploaded: 
        /// 4 - Email Footer, 5 - PDF Header, 6 - PDF Footer, 7 - Director's Signature</param>
        /// <param name="ItemID">The ID of the item depending on which table it is in:
        /// for PDF Headers and Footers 1 is Header 2 is Footer as they are in the same table
        /// for Email Footers and Director's Signatures both IDs are 1 as they are both in the Company table which has 1 entry only</param>
        /// <param name="Chunk">A chunk of the image</param>
        /// <param name="New">If the upload has just started</param>
        /// <param name="Completed">If this is the last chunk</param>
        /// <returns>success/fail</returns>
        [WebMethod(EnableSession = true)]
        public Boolean UploadImage(Int32 Stage, Int32 ItemID, String Chunk, Boolean New, Boolean Completed)
        {
            if (ImageByteString == null || New)
                ImageByteString = "";

            ImageByteString += Chunk;
            var retval = false;
            if (Completed)
            {
                var ImageBytes = Convert.FromBase64String(ImageByteString);

                ImageByteString = null;

                switch (Stage)
                {
                    case 4:
                        retval = CompanyHandler.SaveEmailFooterImage(AspSession.UserDatabase, ImageBytes);
                        Utility.Logger.Log.DebugFormat("({0}:{1}) Email Footer Uploaded - Success = {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, retval.ToString());
                        break;
                    case 5:
                        retval = CustomGroupHandler.UploadPDFGroupImage(AspSession.UserDatabase, "Header", ImageBytes);
                        Utility.Logger.Log.DebugFormat("({0}:{1}) PDF Header Uploaded", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                        break;
                    case 6:
                        retval = CustomGroupHandler.UploadPDFGroupImage(AspSession.UserDatabase, "Footer", ImageBytes);
                        Utility.Logger.Log.DebugFormat("({0}:{1}) PDF Footer Uploaded", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                        break;
                    case 7:
                        retval = CompanyHandler.SaveDirectorsSignature(AspSession.UserDatabase, ImageBytes);
                        Utility.Logger.Log.DebugFormat("({0}:{1}) Signature Uploaded - Success = {2}", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, retval.ToString());
                        break;
                }
            }

            return retval;
        }
        [WebMethod(EnableSession = true)]
        public Boolean DeleteDocument(int Id)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) DocumentUploaded starting", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            var retval = new DAL.CompanyDocuments(AspSession.UserDatabase);
            return retval.Delete(Id);
        }

        [WebMethod(EnableSession = true)]
        public Boolean UploadDocument(String Chunk, Boolean New, Boolean Completed, string DocumentName, string DocumentType, string DocumentStaff, string DocumentFileName, int Id)
        {

            Utility.Logger.Log.DebugFormat("({0}:{1}) DocumentUploaded starting", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
            if (ImageByteString == null || New)
                ImageByteString = "";

            ImageByteString += Chunk;
            var retval = false;
            if (Completed)
            {
                if (ImageByteString == "")
                {

                    DGS.ArbPro.Types.CompanyDocumentsObject obj = new Types.CompanyDocumentsObject();
                    obj.DocumentDate = DateTime.Now;
                    obj.DocumentName = DocumentName;
                    obj.DocumentType = DocumentType;
                    obj.Staff = DocumentStaff;
                    obj.Id = Id;
                    retval = BL.CompanyHandler.SaveDocumentsHandler(AspSession.UserDatabase, obj, AspSession.CurrentUser.CompanyID) > 0;
                    Utility.Logger.Log.DebugFormat("({0}:{1}) Document Uploaded", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                }
                else
                {
                    var ImageBytes = Convert.FromBase64String(ImageByteString);

                    DGS.ArbPro.Types.CompanyDocumentsObject obj = new Types.CompanyDocumentsObject();
                    obj.DocumentImage = ImageBytes;
                    obj.DocumentDate = DateTime.Now;
                    obj.DocumentName = DocumentName;
                    obj.DocumentType = DocumentType;
                    obj.Staff = DocumentStaff;
                    obj.DocumentFileName = DocumentFileName;
                    obj.Id = Id;

                    ImageByteString = null;
                    retval = BL.CompanyHandler.SaveDocumentsHandler(AspSession.UserDatabase, obj, AspSession.CurrentUser.CompanyID) > 0;
                    Utility.Logger.Log.DebugFormat("({0}:{1}) Document Uploaded", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                }
            }

            return retval;

        }

        /// <summary>
        /// Method to clear an image of the specified type from the company
        /// </summary>
        /// <param name="Stage"> 4 - email footer, 5 - pdf header, 6 - pdf footer, 7 - director's signature</param>
        /// <param name="ItemID"> item id is needed for cases 0 - 3 and is here for code reuse purposes.</param>
        /// <returns>true or false</returns>
        [WebMethod(EnableSession = true)]
        public Boolean ClearImage(Int32 Stage, int ItemID)
        {
            var retval = false;

            switch (Stage)
            {
                case 4:
                    retval = CompanyHandler.ClearEmailFooterImage(AspSession.UserDatabase);
                    Utility.Logger.Log.DebugFormat("({0}:{1}) Email Footer Cleared", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                    break;
                case 5:
                    retval = CustomGroupHandler.ClearPDFGroupImage(AspSession.UserDatabase, "Header");
                    Utility.Logger.Log.DebugFormat("({0}:{1}) PDF Header Cleared", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                    break;
                case 6:
                    retval = CustomGroupHandler.ClearPDFGroupImage(AspSession.UserDatabase, "Footer");
                    Utility.Logger.Log.DebugFormat("({0}:{1}) PDF Footer Cleared", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                    break;
                case 7:
                    retval = CompanyHandler.ClearDirectorsSignature(AspSession.UserDatabase);
                    Utility.Logger.Log.DebugFormat("({0}:{1}) Signature Cleared", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr);
                    break;
            }

            return retval;
        }

        /// <summary>
        /// Method to Rotate images in the company's PDF and Email Options
        /// </summary>
        /// <param name="Stage">Indicates the type of image uploaded: 
        /// 4 - Email Footer, 5 - PDF Header, 6 - PDF Footer, 7 - Director's Signature</param>
        /// <param name="ItemID">The ID of the item depending on which table it is in:
        /// for PDF Headers and Footers 1 is Header 2 is Footer as they are in the same table
        /// for Email Footers and Director's Signatures both IDs are 1 as they are both in the Company table which has 1 entry only</param>
        /// <param name="Degrees">Rotation amount in degrees</param>
        /// <returns>success/fail</returns>
        [WebMethod(EnableSession = true)]
        public Boolean RotateImage(Int32 Stage, Int32 ItemID, Int32 Degrees)
        {
            #region Retrieve Picture

            List<PDFReport.ImageItem> companyImage = new List<PDFReport.ImageItem>();
            Types.Company directorAndFooter = new Types.Company();
            bool isCompanyImage = false;
            bool isDirector = false;
            bool isFooter = false;
            Byte[] ImageBytes = null;

            switch (Stage)
            {
                case 4:
                    var eDB = new DAL.Company(AspSession.UserDatabase);
                    directorAndFooter = eDB.Get();
                    isFooter = true;
                    break;
                case 5:
                    var hDB = new DAL.ImageItem(AspSession.UserDatabase);
                    companyImage = hDB.GetForGroup(ItemID);
                    isCompanyImage = true;
                    break;
                case 6:
                    var fDB = new DAL.ImageItem(AspSession.UserDatabase);
                    companyImage = fDB.GetForGroup(ItemID);
                    isCompanyImage = true;
                    break;
                case 7:
                    var dDB = new DAL.Company(AspSession.UserDatabase);
                    directorAndFooter = dDB.Get();
                    isDirector = true;
                    break;
            }

            if (isCompanyImage)
            {
                var cPics = companyImage;

                if (cPics.Count == 0)
                {
                    return false;
                }

                ImageBytes = cPics[0].GetImageBytes();
            }
            else if (isDirector || isFooter)
            {
                var dirPics = directorAndFooter;
                if (isDirector)
                {
                    ImageBytes = dirPics.DirectorsSignature;
                }
                else
                {
                    ImageBytes = dirPics.Footer;
                }
            }

            #endregion

            #region Create Bitmap

            var ms = new MemoryStream(ImageBytes);
            var Image = System.Drawing.Image.FromStream(ms);
            ImageCodecInfo FileCodec = null;
            Guid Guid = Image.RawFormat.Guid;
            System.Drawing.Imaging.ImageFormat ImageFormat = System.Drawing.Imaging.ImageFormat.Png;

            foreach (var Codec in System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders())
            {
                if (Codec.FormatID == Guid)
                    FileCodec = Codec;
            }
            #endregion

            switch (Degrees)
            {
                case 90:
                    Image.RotateFlip(System.Drawing.RotateFlipType.Rotate90FlipNone);
                    break;
                case -90:
                    Image.RotateFlip(System.Drawing.RotateFlipType.Rotate270FlipNone);
                    break;
            }

            #region Save Image
            ms.Dispose();
            var ms2 = new MemoryStream();
            Image.Save(ms2, ImageFormat);

            ImageBytes = ms2.ToArray();
            Image.Dispose();
            ms2.Dispose();
            this.UploadImage(Stage, ItemID, Convert.ToBase64String(ImageBytes), true, true);
            #endregion

            return true;
        }

        /// <summary>
        /// Method to flip images in the company's PDF and Email Options
        /// </summary>
        /// <param name="Stage">Indicates the type of image uploaded: 
        /// 4 - Email Footer, 5 - PDF Header, 6 - PDF Footer, 7 - Director's Signature</param>
        /// <param name="ItemID">The ID of the item depending on which table it is in:
        /// for PDF Headers and Footers 1 is Header 2 is Footer as they are in the same table
        /// for Email Footers and Director's Signatures both IDs are 1 as they are both in the Company table which has 1 entry only</param>
        /// <param name="Axis">Rotation axis</param>
        /// <returns>success/fail</returns>
        [WebMethod(EnableSession = true)]
        public Boolean FlipImage(Int32 Stage, Int32 ItemID, String Axis)
        {
            #region Retrieve Picture
            List<PDFReport.ImageItem> companyImage = new List<PDFReport.ImageItem>();
            Types.Company directorAndFooter = new Types.Company();
            bool isCompanyImage = false;
            bool isDirector = false;
            bool isFooter = false;
            Byte[] ImageBytes = null;

            switch (Stage)
            {
                case 4:
                    var eDB = new DAL.Company(AspSession.UserDatabase);
                    directorAndFooter = eDB.Get();
                    isFooter = true;
                    break;
                case 5:
                    var hDB = new DAL.ImageItem(AspSession.UserDatabase);
                    companyImage = hDB.GetForGroup(ItemID);
                    isCompanyImage = true;
                    break;
                case 6:
                    var fDB = new DAL.ImageItem(AspSession.UserDatabase);
                    companyImage = fDB.GetForGroup(ItemID);
                    isCompanyImage = true;
                    break;
                case 7:
                    var dDB = new DAL.Company(AspSession.UserDatabase);
                    directorAndFooter = dDB.Get();
                    isDirector = true;
                    break;
            }

            if (isCompanyImage)
            {
                var cPics = companyImage;

                if (cPics.Count == 0)
                {
                    return false;
                }

                ImageBytes = cPics[0].GetImageBytes();
            }
            else if (isDirector || isFooter)
            {
                var dirPics = directorAndFooter;
                if (isDirector)
                {
                    ImageBytes = dirPics.DirectorsSignature;
                }
                else
                {
                    ImageBytes = dirPics.Footer;
                }
            }
            #endregion

            #region Create Bitmap
            var ms = new MemoryStream(ImageBytes);
            var Image = System.Drawing.Image.FromStream(ms);
            ImageCodecInfo FileCodec = null;
            Guid Guid = Image.RawFormat.Guid;
            System.Drawing.Imaging.ImageFormat ImageFormat = System.Drawing.Imaging.ImageFormat.Png;

            foreach (var Codec in System.Drawing.Imaging.ImageCodecInfo.GetImageDecoders())
            {
                if (Codec.FormatID == Guid)
                    FileCodec = Codec;
            }
            #endregion

            switch (Axis)
            {
                case "Horizontal":
                    Image.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipY);
                    break;
                case "Vertical":
                    Image.RotateFlip(System.Drawing.RotateFlipType.RotateNoneFlipX);
                    break;
            }

            #region Save Image
            ms.Dispose();
            var ms2 = new MemoryStream();
            Image.Save(ms2, ImageFormat);

            ImageBytes = ms2.ToArray();
            Image.Dispose();
            ms2.Dispose();
            this.UploadImage(Stage, ItemID, Convert.ToBase64String(ImageBytes), true, true);
            #endregion

            return true;
        }
        [WebMethod(EnableSession = true)]
        public string SaveImageFileOnServer(int Id)
        {
            string fileName = "";
            string filePath = "";

                int id = Id;
                var Document_File = BL.CompanyHandler.GetDocumentForID(AspSession.UserDatabase, id);

                fileName = Document_File.DocumentFileName;
                Byte[] DocumentFile = Document_File.DocumentImage;
                filePath = "/CompanyDocuments/" + fileName;
                File.WriteAllBytes(Server.MapPath(filePath), DocumentFile);

                return filePath;
        }

        #region Complaints
        [WebMethod(EnableSession = true)]
        public List<Types.ComplaintSearchResult> SearchComplaintsForStatus(bool closed)
        {
            return BL.CompanyHandler.SearchComplaintsForStatus(AspSession.UserDatabase,closed);
        }

        [WebMethod(EnableSession = true)]
        public Types.Complaint GetComplaint(int id)
        {
            return BL.CompanyHandler.GetComplaint(AspSession.UserDatabase, id);
        }

        [WebMethod(EnableSession = true)]
        public int SaveComplaint(Types.Complaint complaint)
        {
            Utility.Logger.Log.DebugFormat("({0}:{1}) Complaint {2} Saved", AspSession.CurrentUser.Id, AspSession.CurrentUser.NameStr, complaint.Id);
            return BL.CompanyHandler.SaveComplaint(AspSession.UserDatabase, complaint);
        }
        #endregion

        #region Document History
        #region Elastic Email

        [WebMethod(EnableSession = true)]
        public List<Types.EmailHistorySearchResult> GetEmailHistory(DateTime startDate, DateTime endDate)
        {
            return BL.ReportItemHandlers.ReportEmailHandler.GetElasticEmailHistory(AspSession.UserDatabase, startDate, endDate);
        }
        #endregion
        #region Letters
        //TODO: Show the document history of all letters between specified dates
        #endregion
        #endregion
    }
}
