﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DGS.ArbPro.Types;
using System.Web.Script.Services;

namespace DGS.ArbPro.Web.Services.Web
{
    using DGS.ArbPro.BL;

    /// <summary>
    /// Summary description for Template
    /// </summary>
    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Template : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        public bool SaveTemplate(Types.Templates.Risk risk)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.SaveTemplate(risk);
        }

        [WebMethod(EnableSession = true)]
        public bool SaveSectionAndMapToTemplate(int riskId, Types.Templates.RiskSection section)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.SaveSectionAndMapToTemplate(riskId, section);
        }

        [WebMethod(EnableSession = true)]
        public bool SaveCriterionAndMapToTemplate(int riskId, int sectionId, Types.Templates.RiskCriterion criterion)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.SaveAndMapCriterionTemplate(riskId, sectionId, criterion);
        }

        [WebMethod(EnableSession = true)]
        public bool MapSectionToTemplate(int riskId, int sectionId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.MapSectionToTemplate(riskId, sectionId);
        }

        [WebMethod (EnableSession=true)]
        public Types.Templates.Risk GetRiskTemplatesForId(int riskTemplateId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.GetRiskTemplate(riskTemplateId);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Templates.Risk> GetAllRiskTemplates()
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.GetAllRiskTemplates();
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Templates.RiskSection> GetAllRiskSections()
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.GetAllRiskSectionTemplates();
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Templates.RiskCriterion> GetAllRiskCriteria()
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.GetAllCriterionTemplates();
        }

        [WebMethod(EnableSession = true)]
        public int InsertOrUpdateRiskTemplate(Types.Templates.Risk risk)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.InsertOrUpdateRiskTemplate(risk);
        }

        [WebMethod(EnableSession = true)]
        public int CancelRiskTemplate(int riskId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            Types.Templates.Risk risk = riskHandler.GetRiskTemplate(riskId);

            risk.IsDeleted = true;
            return riskHandler.InsertOrUpdateRiskTemplate(risk);
        }

        [WebMethod(EnableSession = true)]
        public int InsertTemplateRiskSectionMapping(int riskId, int sectionId, int criterionId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.InsertTemplateRiskSectionMapping(riskId, sectionId, criterionId);
        }

        [WebMethod(EnableSession = true)]
        public bool RemoveTemplateRiskSectionMapping(int riskId, int sectionId, int criterionId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.RemoveTemplateRiskSectionMapping(riskId, sectionId, criterionId);
        }

        [WebMethod(EnableSession = true)]
        public int InsertTemplateRiskMapping(int sectionId, int riskId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.InsertTemplateRiskMapping(sectionId, riskId);
        }

        [WebMethod(EnableSession = true)]
        public bool RemoveTemplateRiskMapping(int riskId, int sectionId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.RemoveTemplateRiskMapping(riskId, sectionId);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Templates.RiskSection> GetSectionTemplatesForRiskId(int riskTemplateId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.GetSectionsForTemplateId(riskTemplateId);
        }

        [WebMethod(EnableSession = true)]
        public List<Types.Templates.RiskCriterion> GetCriteriaTemplatesForSectionId(int sectionTemplateId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.GetCriterionForSectionId(sectionTemplateId);
        }

        [WebMethod(EnableSession = true)]
        public int InsertOrUpdateCriterionTemplate(Types.Templates.RiskCriterion criterion)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.InsertOrUpdateCriterionTemplate(criterion);
        }

        [WebMethod(EnableSession = true)]
        public int CancelCriterionTemplate(int criterionId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            Types.Templates.RiskCriterion criterion = riskHandler.GetCriterionForId(criterionId);

            criterion.IsDeleted = true;
            return riskHandler.InsertOrUpdateCriterionTemplate(criterion);
        }

        [WebMethod(EnableSession = true)]
        public Types.Templates.RiskSection GetSectionTemplateForId(int riskId, int sectionTemplateId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.GetSectionForId(riskId, sectionTemplateId);
        }

        [WebMethod(EnableSession = true)]
        public Types.Templates.RiskCriterion GetCriterionTemplateForId(int criterionId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.GetCriterionForId(criterionId);
        }

        [WebMethod(EnableSession = true)]
        public int UpdateDocumentReferencesForSectionId(Types.Templates.RiskSection section)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.InsertOrUpdateSectionTemplate(section);
        }

        [WebMethod(EnableSession = true)]
        public int InsertOrUpdateSectionTemplate(Types.Templates.RiskSection section)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            return riskHandler.InsertOrUpdateSectionTemplate(section);
        }

        [WebMethod(EnableSession = true)]
        public int CancelSectionTemplate(int sectionId)
        {
            RiskTemplateHandler riskHandler = new RiskTemplateHandler(AspSession.UserDatabase);
            Types.Templates.RiskSection section = riskHandler.GetSectionForId(-1, sectionId);

            section.IsDeleted = true;
            return riskHandler.InsertOrUpdateSectionTemplate(section);
        }
    }
}
