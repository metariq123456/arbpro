﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using DGS.ArbPro.BL;

namespace DGS.ArbPro.Web.Services.Web
{
    /// <summary>
    /// This web service facilitates display of the default web page
    /// </summary>
    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Dashboard : System.Web.Services.WebService
    {
        public delegate Types.SearchSummary DashboardServiceAsyncStub();

        public class MyState
        {
            public object previousState;
            public DashboardServiceAsyncStub asyncStub;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetOverdueInvoices()
        {
            var retVal = new Types.SearchSummary();
            try
            {
                string title = "Invoices Overdue";
                retVal = BL.DashBoardHandler.GetOverdueInvoicesSummary(AspSession.UserDatabase, title);
                Debug.WriteLine("GetOverdueInvoices - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("Error in GetOverdueInvoices {0}", ex.ToString());
            }
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetPendingInvoices()
        {
            var retVal = new Types.SearchSummary();
            try
            {
                string title = "Invoices Pending";
                retVal = BL.DashBoardHandler.GetPendingInvoicesSummary(AspSession.UserDatabase, title);
                Debug.WriteLine("GetPendingInvoices - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("Error in GetPendingInvoices {0}", ex.ToString());
            }
            return retVal;
            
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetInvoicesDueInWeek()
        {
            string title = "Invoice payments Due within 7 days";
            var retVal = BL.DashBoardHandler.GetInvoicesDueInWeekSummary(AspSession.UserDatabase, title);
            Debug.WriteLine("GetInvoicesDueInWeek - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetAllInvoicesDue()
        {
            var retVal = new Types.SearchSummary();
            try
            {
                string title = "Invoice Payments Due";
                retVal = BL.DashBoardHandler.GetInvoicesDue(AspSession.UserDatabase, title);
                Debug.WriteLine("GetInvoicesDue - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("Error in GetPendingInvoices {0}", ex.ToString());
            }
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetAllInvoicesSent()
        {
            var retVal = new Types.SearchSummary();
            try
            {
                string title = "Invoices Sent";
                retVal = BL.DashBoardHandler.GetInvoicesSent(AspSession.UserDatabase, title);
                Debug.WriteLine("GetAllInvoicesSent - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("Error in GetPendingInvoices {0}", ex.ToString());
            }
            return retVal;
        }
        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetAllInvoicesPaid()
        {
            var retVal = new Types.SearchSummary();
            try
            {
                string title = "Invoices Paid.";
                retVal = BL.DashBoardHandler.GetInvoicesPaid(AspSession.UserDatabase, title);
                Debug.WriteLine("GetAllInvoicesPaid - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("Error in GetPendingInvoices {0}", ex.ToString());
            }
            return retVal;
        }
        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetPendingJobs()
        {
            string title = "Enquiries awaiting appointment date";
            var retVal = BL.DashBoardHandler.GetPendingJobsSummary(AspSession.UserDatabase, title);
            Debug.WriteLine("GetPendingJobs - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetPendingQuotes()
        {
            var retVal = new Types.SearchSummary();
            try
            {
                string title = "Quotes uploaded awaiting proof and dispatch";
                retVal = BL.DashBoardHandler.GetPendingQuotesSummary(AspSession.UserDatabase, title);
                Debug.WriteLine("GetPendingQuotes - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("Error in GetPendingQuotes {0}", ex.ToString());
            }
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetSentQuotes()
        {
            var retVal = new Types.SearchSummary();
            try
            {
                const string title = "Quotes dispatched in the last 60 days awaiting customer approval";
                retVal = BL.DashBoardHandler.GetSentQuotesSummary(AspSession.UserDatabase, title, 60);
                Debug.WriteLine("GetPendingQuotes - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("Error in GetSentQuotes {0}", ex.ToString());
            }
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetAllSentQuotes()
        {
            var retVal = new Types.SearchSummary();
            try
            {
                const string title = "All Quotes dispatched";
                retVal = BL.DashBoardHandler.GetAllSentQuotesSummary(AspSession.UserDatabase, title);
                Debug.WriteLine("GetAllSentQuotes - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("Error in GetAllSentQuotes {0}", ex.ToString());
            }
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetPendingWork()
        {
            var retVal = new Types.SearchSummary();
            try
            {
                string title = "Quotes accepted pending start date";
                retVal = BL.DashBoardHandler.GetPendingWorkSummary(AspSession.UserDatabase, title);
                Debug.WriteLine("GetPendingWork - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("Error in GetAllSentQuotes {0}", ex.ToString());
            }
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetJobPendingWork(int Month)
        {
             var retVal = new Types.SearchSummary();
             try
             {
                 string title = "Quotes accepted pending start date";
                 retVal = BL.DashBoardHandler.GetJobPendingWorkSummary(AspSession.UserDatabase, title, Month);
                 Debug.WriteLine("GetJobPendingWork - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
             }
             catch (Exception ex)
             {
                 Utility.Logger.Log.DebugFormat("Error in GetJobPendingWork {0}", ex.ToString());
             }
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetJobNotAcceptedWork(int Month)
        {
            var retVal = new Types.SearchSummary();
            try
            {

                string title = "Quotes accepted pending start date";
                retVal = BL.DashBoardHandler.GetJobNotAcceptedWorkSummary(AspSession.UserDatabase, title, Month);
                Debug.WriteLine("GetJobPendingWork - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.DebugFormat("Error in GetJobNotAcceptedWork {0}", ex.ToString());
            }
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetAllJobsWork(int Month)
        {
             var retVal = new Types.SearchSummary();
             try
             {
                 string title = "Quotes accepted pending start date";
                 retVal = BL.DashBoardHandler.GetAllJobWorkSummary(AspSession.UserDatabase, title, Month);
                 Debug.WriteLine("GetJobPendingWork - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
             }
             catch (Exception ex)
             {
                 Utility.Logger.Log.DebugFormat("Error in GetAllJobsWork {0}", ex.ToString());
             }
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetRecentQuoteNotes()
        {
            string title = "Recent notes for active quotes";
            var retVal = BL.DashBoardHandler.GetRecentQuoteNotesSummary(AspSession.UserDatabase, title);
            Debug.WriteLine("GetRecentQuoteNotes - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetQuoteAppsForWeek()
        {
            string title = "Appointments for Surveyors";
            var retVal = BL.DashBoardHandler.GetQuoteAppsForWeekSummary(AspSession.UserDatabase, title);
            Debug.WriteLine("GetQuoteAppsForWeek - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetTodaysAppointments()
        {
            string title = "All Appointments Today";
            var retVal = BL.DashBoardHandler.GetTodaysAppointmentSummary(AspSession.UserDatabase, title);
            Debug.WriteLine("GetTodaysAppointments - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetWorkAppsForWeek()
        {
            string title = "Work booked for next 30 days";
            var retVal = BL.DashBoardHandler.GetWorkAppsSummaryForMonth(AspSession.UserDatabase, title);
            Debug.WriteLine("GetWorkAppsForWeek - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetGenericAppsDateRange()
        {
            string title = "Appointments booked 30 days forward of today";
            var retVal = BL.DashBoardHandler.GetGenericAppSummaryForDateRange(AspSession.UserDatabase, title, 31);
            Debug.WriteLine("GetGenericAppsDateRange - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary GetCompanyAppsForWeek()
        {
            string title = "";
            var retVal = BL.DashBoardHandler.GetCompanyAppsForWeekSummary(AspSession.UserDatabase, title);
            Debug.WriteLine("GetCompanyAppsForWeek - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }

        #region TPO
        [WebMethod(EnableSession = true)]
        public Types.SearchSummary TposGetEightWeekExpired()
        {
            string title = "All planning applications submitted.";
            var retVal = BL.DashBoardHandler.TposGetEightWeekExpiredSummary(AspSession.UserDatabase, title);
            Debug.WriteLine("TposGetEightWeekExpired - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary TposGetSixWeekExpired()
        {
            string title = "All planning applications submitted.";
            var retVal = BL.DashBoardHandler.TposGetSixWeekExpiredSummary(AspSession.UserDatabase, title);
            Debug.WriteLine("TposGetSixWeekExpired - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary TposGetAppeals()
        {
            string title = "ALL planning appeals submitted.";
            var retVal = BL.DashBoardHandler.TposGetAppealsSummary(AspSession.UserDatabase, title);
            Debug.WriteLine("TposGetAppeals - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.SearchSummary TposGetGranted()
        {
            string title = "All TPOs that have been granted but not booked.";
            var retVal = BL.DashBoardHandler.TposGetGrantedSummary(AspSession.UserDatabase, title);
            Debug.WriteLine("TposGetGranted - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }
        #endregion

        #region Finance


        [WebMethod(EnableSession = true)]
        public Types.SearchSummary CompanyFinanceSummary()
        {
            string title = string.Format("Company Finance Summary for {0} at {1}", DateTime.Today.ToShortDateString(), DateTime.Now.ToShortTimeString());
            var retVal = BL.DashBoardHandler.CompanyFinanceSummary(AspSession.UserDatabase, title);
            Debug.WriteLine("CompanyFinanceSummary - {0}", retVal != null ? retVal.Results.Count().ToString() : "Null");
            return retVal;
        }


        #endregion
    }
}
