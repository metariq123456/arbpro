﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;

namespace DGS.ArbPro.Web.Services.Web
{
    using DGS.ArbPro.BL;
    using DGS.ArbPro.DAL;

    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Account : System.Web.Services.WebService
    {
        //private String DatabaseName = System.Configuration.ConfigurationManager.AppSettings["MasterDatabaseName"];

        [WebMethod(EnableSession = true)]
        public Types.User ReturnUserForEmail(string UserName)
        {
            DAL.User DB = new DAL.User("");
            var User = DB.GetForEmail(UserName);
            return User;
        }

        [WebMethod(EnableSession = true)]
        public Types.User GetUserForID(Int32 UserID)
        {
            var DB = new DAL.User("");
            var User = DB.GetForUserId(UserID);
            return User;
        }

        [WebMethod(EnableSession = true)]
        public Boolean MarkUserTsAndCsAccepted(string UserName, bool Accepted)
        {
            DAL.User DB = new DAL.User("");
            var User = DB.GetForEmail(UserName);
            User.TsAndCsAccepted = Accepted;
            DB.Save(User);
            Utility.Logger.Log.InfoFormat("({0}:{1}) Accepted Terms and Conditions", User.Id, User.NameStr);
            return true;
        }

        [WebMethod(EnableSession = true)]
        public Boolean ChangeUserPassword(string UserName, string OldPassword, string ConfirmPassword, string NewPassword)
        {
            DAL.User DB = new DAL.User("");
            var User = DB.GetForEmail(UserName);
            if (OldPassword == ConfirmPassword)
            {
                if (User.PasswordMatch(OldPassword))
                {
                    if (Types.User.PassesRegex(NewPassword))
                    {
                        User.PasswordHashed = User.HashPassword(NewPassword);
                        DB.Save(User);
                        Utility.Logger.Log.InfoFormat("({0}:{1}) Changed Password", User.Id, User.NameStr);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        [WebMethod(EnableSession = true)]
        public Boolean UserLogOut()
        {
            var DB = new DAL.User("");
            DB.Logout(HttpContext.Current.User.Identity.Name);

            AspSession.KillSession();

            HttpContext.Current.Response.Cookies.Remove("UserGUID");
            FormsAuthentication.SignOut();
            return true;
        }

        static Thread LogoutThread = new Thread(() => { });

        [WebMethod(EnableSession = true)]
        public Boolean StartLogoutTimer()
        {
            //if (!HttpContext.Current.User.Identity.IsAuthenticated)
            //    return false;

            //String Name = HttpContext.Current.User.Identity.Name;

            //if (LogoutThread.IsAlive)
            //{
            //    LogoutThread.Abort();
            //}

            //var Ctxt = Context;
            //var SessionID = Session.SessionID;

            //LogoutThread = new Thread(() =>
            //{
            //    Thread.Sleep(20000);

            //    var DB = new DAL.User(DatabaseName);
            //    var Client = DB.GetForEmail(Name);
            //    if (Client != null)
            //    {
            //        Client.IsOnline = false;
            //        Client.LastActivityDate = DateTime.Now;
            //        DB.Save(Client);

            //        if (Ctxt.Cache["LoggedInUsers"] == null)
            //            Ctxt.Cache["LoggedInUsers"] = new Dictionary<String, Types.User>();

            //        Dictionary<String, Types.User> LoggedInUsers = (Dictionary<String, Types.User>)Ctxt.Cache["LoggedInUsers"];
            //        LoggedInUsers.Remove(SessionID);
            //        Ctxt.Cache["LoggedInUsers"] = LoggedInUsers;
            //    }
            //});

            //LogoutThread.Start();

            return false;
        }

        [WebMethod(EnableSession = true)]
        public Boolean StopLogoutTimer()
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                return false;

            LogoutThread.Abort();
            return true;
        }

        [WebMethod(EnableSession = true)]
        public string SendPasswordResetEmail(string userName)
        {
            switch (
                UserHandler.SendPasswordResetEmail(
                    userName,
                    HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)))
            {
                case UserHandler.SendPasswordResetResult.ErrorGeneratingGuid:
                    return "An error occurred generating the password reset id. Please contact support.";
                case UserHandler.SendPasswordResetResult.UserNotFound:
                    return string.Format("User {0} was not found. Please check username and try again.", userName);
                case UserHandler.SendPasswordResetResult.ErrorSendingEmail:
                    return "An error occurred generating the password  email. Please contact support.";
                default:
                    return
                        string.Format(
                            "An email has been sent to {0} with instructions on how to reset your password. Please check your email.",userName);
            }
        }
        
        [WebMethod(EnableSession = true)]
        public int SaveDataBase(String CompanyName, String Address1,String Address2, String Town,String County, String PostCode, Int32 Country, Int32 Licenses,float AmountValue,String EmailAddress,String Title, String FirstName, String LastName, String Telephone, String Position, String Comment)
        {
            var ClientOrg = new Types.ClientOrganisation()
            {
                OrganisationName = CompanyName,
                Address1 = Address1,
                Address2 = Address2,
                Town = Town,
                CountyOrState=County,
                PostcodeOrZip = PostCode,
                CountryID = 230,
                NumLicenses = Licenses,
                UserAccountsEnabled = true,
                MobileEditionEnabled = true,
                Value = AmountValue
            };

            var FirstUser = new Types.User()
            {
                Title=Title,
                FirstName = FirstName,
                LastName = LastName,
                EmailAddress = EmailAddress,
                Telephone = Telephone,
                CompanyPosition = Position,
                Comment = Comment
            };
            var DB = new DAL.User("");
            int retVal = DB.AccountOrCompanyExist(EmailAddress, CompanyName);
            if (retVal == 0)
            {
                retVal = DB.CreateDatabase(ClientOrg, FirstUser);
                if(retVal > 0)
                {
                    retVal = 0;
                }
                else
                {
                    retVal = -1;
                }
            }
            return retVal;
        }
        
        [WebMethod(EnableSession = true)]
        public List<Types.DatabaseUser> GetAllOrganization()
        {
            var DB = new DAL.DatabaseUser("");
            List<Types.DatabaseUser> AllOrgs = DB.GetAllDatabaseUser();
            return AllOrgs;
        }

        [WebMethod(EnableSession = true)]
        public Types.User GetAdminUser(int OrgId)
        {
            var DB = new DAL.User("");
            Types.User AdminUser = DB.GetAdminUser(OrgId);
            return AdminUser;
        }

        [WebMethod(EnableSession = true)]
        public Types.ClientOrganisation getSingleOrganization(int OrgId)
        {
            Types.ClientOrganisation ClientOrg = new Types.ClientOrganisation();
            var DB = new DAL.ClientOrganisation("");
            ClientOrg = DB.GetForId(OrgId);
            return ClientOrg;
        }

        [WebMethod(EnableSession = true)]
        public List<Types.User> getSingleOrganizationUsers(int OrgId)
        {
            List<Types.User> OrgUsers = new List<Types.User>();
            var DB = new DAL.User("");
            OrgUsers = DB.GetForOrganisation(OrgId);
            return OrgUsers;
        }

        [WebMethod(EnableSession = true)]
        public int SaveUser(String EmailAddress, String Title, String FirstName, String LastName, String Telephone, String Position, String Comment,int OrgId)
        {
            var DB = new DAL.User("");
            var NewUser = new Types.User()
            {
                Title = Title,
                FirstName = FirstName,
                LastName = LastName,
                EmailAddress = EmailAddress,
                Telephone = Telephone,
                CompanyPosition = Position,
                Comment = Comment,
                CompanyID = OrgId
            };
            int retVal = DB.CheckUserExist(EmailAddress);
            if (retVal == 0)
            {
                retVal = DB.SaveUser(NewUser);
            }
           

            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public int TotalNoOfDatabases()
        {
            var DB = new DAL.User("");
            int retVal = DB.TotalNoOfDatabases();
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public Types.ClientOrganisation TotalClientAmount()
        {
            var DB = new DAL.ClientOrganisation("");
            Types.ClientOrganisation ClientOrg = DB.TotalClientsAmount();
            return ClientOrg;
        }
        [WebMethod(EnableSession = true)]
        public int CheckAllUsersEnabledForOrg(int OrgId)
        {
            var DB = new DAL.User("");
            int retVal = DB.CheckAllUsersEnabledForOrg(OrgId);
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public int EnableAllUsersForOrg(int OrgId)
        {
            var DB = new DAL.User("");
            int retVal = DB.EnableAllUsersForOrg(OrgId);
            return retVal;
        }

        [WebMethod(EnableSession = true)]
        public int DisableAllUsersForOrg(int OrgId)
        {
            var DB = new DAL.User("");
            int retVal = DB.DisableAllUsersForOrg(OrgId);
            return retVal;
        }
    }
}
