﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Marketing.asmx.cs" company="Microsec Ltd">
//   Copyright 2016 Microsec Ltd
// </copyright>
// <summary>
//   Summary description for Marketing
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.Web.Services.Web
{
    using System.Collections.Generic;
    using System.Web.Script.Services;
    using System.Web.Services;

    using DGS.ArbPro.BL;

    /// <summary>
    /// The marketing web service.
    /// </summary>
    [ScriptService]
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Marketing : WebService
    {
        /// <summary>
        /// The marketing handler.
        /// </summary>
        private readonly MarketingHandler marketingHandler = new MarketingHandler(AspSession.UserDatabase);

        #region Work Category

        /// <summary>
        /// Inserts or updates a work category.
        /// </summary>
        /// <param name="obj"> The work category. </param>
        /// <returns> Success or failure. </returns>
        [WebMethod(EnableSession = true)]
        public bool SaveWorkCategory(Types.Marketing obj)
        {
            return this.marketingHandler.SaveWorkCategory(obj) > 0;
        }

        /// <summary>
        /// Gets all work categories.
        /// </summary>
        /// <returns> A list of work categories. </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.Marketing> GetAllWorkCategories()
        {
            return this.marketingHandler.GetAllWorkCategories();
        }

        /// <summary>
        /// Gets a work category based on category Id
        /// </summary>
        /// <param name="categoryId"> The category id. </param>
        /// <returns> A work category. </returns>
        [WebMethod(EnableSession = true)]
        public Types.Marketing GetWorkCategoryForId(int categoryId)
        {
            return this.marketingHandler.GetWorkCategoryForId(categoryId);
        }

        /// <summary>
        /// Deletes a work category for a category Id
        /// </summary>
        /// <param name="categoryId"> The category id. </param>
        /// <returns> Success or failure. </returns>
        [WebMethod(EnableSession = true)]
        public bool DeleteWorkCategoryForId(int categoryId)
        {
            return this.marketingHandler.DeleteWorkCategoryForId(categoryId);
        }

        #endregion

        #region Client Work Category

        /// <summary>
        /// Inserts or updates a client work category.
        /// </summary>
        /// <param name="obj"> The client work category. </param>
        /// <returns> Success or failure. </returns>
        [WebMethod(EnableSession = true)]
        public bool SaveClientWorkCategory(Types.Marketing obj)
        {
            return this.marketingHandler.SaveClientWorkCategory(obj) > 0;
        }

        /// <summary>
        /// The delete client work category for id.
        /// </summary>
        /// <param name="id">
        /// The client work category id.
        /// </param>
        /// <returns> Success or failure. </returns>
        [WebMethod(EnableSession = true)]
        public bool DeleteClientWorkCategoryForId(int id)
        {
            return this.marketingHandler.DeleteClientWorkCategoryForId(id);
        }

        /// <summary>
        /// The remove client work categories for job.
        /// </summary>
        /// <param name="jobId"> The job id. </param>
        /// <returns> Success or failure. </returns>
        [WebMethod(EnableSession = true)]
        public bool UpdateAcceptedWorkClientCategoriesForJobId(int jobId)
        {
            return this.marketingHandler.UpdateAcceptedWorkClientCategoriesForJobId(jobId);
        }

        /// <summary>
        /// Gets all client work categories.
        /// </summary>
        /// <returns> A list of client work categories. </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.Marketing> GetAllClientWorkCategories()
        {
            return this.marketingHandler.GetAllClientWorkCategories();
        }

        /// <summary>
        /// The get all distinct for seasonal marketing.
        /// </summary>
        /// <returns> A list of client work categories. </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.Marketing> GetAllDistinctForSeasonalMarketing()
        {
            return this.marketingHandler.GetAllDistinctForSeasonalMarketing();
        }

        /// <summary>
        /// The get all distinct accepted for seasonal marketing.
        /// </summary>
        /// <returns> List of client work categories. </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.Marketing> GetAllDistinctAcceptedForSeasonalMarketing()
        {
            return this.marketingHandler.GetAllDistinctAcceptedForSeasonalMarketing();
        }

        /// <summary>
        /// The get all distinct not accepted for seasonal marketing.
        /// </summary>
        /// <returns> List of client work categories. </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.Marketing> GetAllDistinctNotAcceptedForSeasonalMarketing()
        {
            return this.marketingHandler.GetAllDistinctNotAcceptedForSeasonalMarketing();
        }

        /// <summary>
        /// Get all clients for marketing.
        /// </summary>
        /// <returns> A list of all clients for marketing. </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.Marketing> GetAllClientMarketing()
        {
            return this.marketingHandler.GetAllClientMarketing();
        }

        /// <summary>
        /// The get all for return marketing.
        /// </summary>
        /// <returns> A list of client work categories. </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.Marketing> GetAllForReturnMarketing()
        {
            return this.marketingHandler.GetAllForReturnMarketing();
        }

        /// <summary>
        /// The get sent for return marketing.
        /// </summary>
        /// <returns> A list of client work categories. </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.Marketing> GetSentForReturnMarketing()
        {
            return this.marketingHandler.GetSentForReturnMarketing();
        }

        /// <summary>
        /// Gets all client work categories for a client Id.
        /// </summary>
        /// <param name="clientId"> The client id. </param>
        /// <returns> A list of client work categories. </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.Marketing> GetClientWorkCategoriesForClientId(int clientId)
        {
            return this.marketingHandler.GetClientWorkCategoriesForClientId(clientId);
        }

        /// <summary>
        /// The get client work categories for job id.
        /// </summary>
        /// <param name="jobId"> The job id. </param>
        /// <returns> List of client work categories. </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.Marketing> GetClientWorkCategoriesForJobId(int jobId)
        {
            return this.marketingHandler.GetClientWorkCategoriesForJobId(jobId);
        }

        /// <summary>
        /// The get all return time categories.
        /// </summary>
        /// <returns>
        /// A list of all return time categories
        /// </returns>
        [WebMethod(EnableSession = true)]
        public List<Types.Marketing> GetAllReturnTimeCategories()
        {
            return this.marketingHandler.GetAllReturnTimeCategories();
        }

        /// <summary>
        /// Gets a client work category based on category Id
        /// </summary>
        /// <param name="categoryId"> The category id. </param>
        /// <returns> A client work category. </returns>
        [WebMethod(EnableSession = true)]
        public Types.Marketing GetClientWorkCategoryForId(int categoryId)
        {
            return this.marketingHandler.GetClientWorkCategoryForId(categoryId);
        }

        #endregion
    }
}
