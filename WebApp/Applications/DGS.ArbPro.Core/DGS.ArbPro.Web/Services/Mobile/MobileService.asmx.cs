﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace DGS.ArbPro.Web.Services.Mobile
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class MobileService : System.Web.Services.WebService
    {

        [WebMethod]
        public Boolean Test()
        {
            return true;
        }
    }
}
