﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class RiskTemplate : System.Web.UI.Page
    {
        Types.Templates.Risk _Template = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            Services.Web.Template service = new Services.Web.Template();
            _Template = service.GetRiskTemplatesForId(1);

            txtTemplateName.Text = _Template.Name;
            txtTemplateRefDoc.Text = _Template.ReferenceDoc;
            txtTemplateDescription.Text = _Template.Description;
            hfSectionTotal.Value = _Template.TemplateSections.Count.ToString();
        }

        public void CreateSectionTabs()
        {
            int curTab = 1;
            foreach (Types.Templates.RiskSection s in _Template.TemplateSections)
            {
                //a id="tabContacts" class="clientTab activeTab" href="javascript:Client_curTab = 1; SwitchTab();">
                Response.Write(string.Format(@"<a id=""tab{0}"" class=""clientTab activeTab"" href=""javascript:Section_curTab = {0}; SwitchTab();"">", curTab));
                Response.Write(string.Format(@"{0}{1}{0}", Environment.NewLine, s.Title));
                Response.Write(@"</a>");
                curTab++;
            }

            Response.Write(string.Format(@"<a id=""tabAdd"" class=""clientTab"" href=""javascript:Section_curTab = {0}; /*AddTab();*/"">", curTab));
            Response.Write(string.Format(@"{0}{1}{0}", Environment.NewLine, " + "));
            Response.Write(@"</a>");
        }

        public void CreateSectionContent()
        {
            int curTab = 1;
            foreach (Types.Templates.RiskSection s in _Template.TemplateSections)
            {
                Response.Write(string.Format(@"<div id=""pnl{0}"" class=""hidden"">{1}", curTab, Environment.NewLine));
                Response.Write(string.Format(@"<div id=""grd{0}"" class=""width-full"">{1}", curTab, Environment.NewLine));
                Response.Write(string.Format(@"<table data-section='" + s.Id + @"' class=""searchGrid width-full"">{0}<tr><th>Field</th><th>Value</th></tr>{0}", Environment.NewLine));
                Response.Write(@"<tr>");
                Response.Write(string.Format("<td> Reference Document </td><td><span id='secRefDoc'>{0}</span></td>", s.ReferenceDoc));
                Response.Write(@"</tr>");
                Response.Write(string.Format("<td> Reference Chapter </td><td><span id='secRefChapter'>{0}</span></td>", s.ChapterRef));
                Response.Write(@"<tr>");
                Response.Write(@"</tr>");
                Response.Write(string.Format("<td> Description </td><td> {0} </td>", s.Description));
                Response.Write(@"<tr>");
                Response.Write(@"</tr>");
                Response.Write(string.Format(@"</table>"));
                Response.Write(string.Format(@"<input type=""submit"" name=""btnEditSection{0}"" value=""Edit"" id=""btnEditSection{0}"" class=""button-basic float-right padded"" onclick=""EditSection({0}); return false;"" />", curTab));
                Response.Write(@"<br/>");
                Response.Write(string.Format(@"<table data-critsec='" + s.Id + @"' class=""searchGrid width-full"">{0}<tr><th>Criteria</th><th>Chapter Reference</th><th>Assessment Phase</th></tr>", Environment.NewLine));
                foreach(Types.Templates.RiskCriterion c in s.TemplateCriteria)
                {
                    Response.Write(@"<tr data-criterion='" + c.Id + "' >");
                    Response.Write(string.Format("<td> {0} </td><td><span id='critRefDoc'> {1} </span></td><td> {2} </td>", c.Criterion, c.ChapterRef, c.AssessmentPhase));
                    Response.Write(@"</tr>");

                }
                Response.Write(string.Format(@"</table>"));
                //Response.Write(string.Format(@"<input type=""submit"" name=""btnNewCriteria{0}"" value=""Add Criteria"" id=""btnNewCriteria{0}"" class=""button-basic float-right padded"" />", curTab));
                Response.Write("</div>");
                Response.Write("</div>");
                curTab++;
            }
        }

        protected void ddlClientType_SelectedIndexChanged1(object sender, EventArgs e)
        {
            
        }
    }
}