﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class Job : System.Web.UI.Page
    {
        public String AppRoot
        {
            get
            {
                return Request.Url.Scheme + "://" + Request.Url.Host + (Request.Url.Port != 80 ? ":" + Request.Url.Port.ToString() : "");
            }
        }

        private Int32 curJobID = 1;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request["JobID"] != null)
            {
                curJobID = Int32.Parse(Request["JobID"]);
            }

            if (Request["QuoteID"] != null)
            {
                hfQuoteId.Value = (Request["QuoteID"]).ToString();
                hfInvoiceId.Value = "-1";
            }

            if (Request["InvoiceID"] != null)
            {
                hfQuoteId.Value = "-1";
                hfInvoiceId.Value = (Request["InvoiceID"]).ToString(); ;
            }

            hfJobID.Value = curJobID.ToString();

            var DB = new DAL.Job(AspSession.CurrentUser.ClientDatabaseName);
            var Job = DB.GetForId(curJobID);

            hfClientID.Value = Job.ClientId.ToString();
            lnkReturnToClient.NavigateUrl = "~/Client.aspx?ClientID=" + Job.ClientId.ToString();
            lblClientName.Text = Job.ClientName;
        }
    }
}