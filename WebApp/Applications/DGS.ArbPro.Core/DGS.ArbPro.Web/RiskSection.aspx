﻿<%@ Page Title="Risk Sections" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RiskSection.aspx.cs" Inherits="DGS.ArbPro.Web.RiskSection" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <script type="text/javascript" src="Content/js/TemplateRiskSection.js?V=3.23.5"></script>
    <asp:Panel runat="server" ID="Panel1" ClientIDMode="Static" CssClass="width-full center">
        <asp:HiddenField runat="server" ID="hfSectionTotal" ClientIDMode="Static" />
        <asp:Panel runat="server" ID="pnlSectionInfo" Width="800px" ClientIDMode="Static" CssClass="templateInfo center">
            <asp:Panel runat="server" CssClass="colored-raise templateHeader">
                <asp:Label runat="server"> Sections</asp:Label>
            </asp:Panel>
                <asp:Button runat="server"
                    ID="btnTemplateManager" ClientIDMode="Static"
                    Text="Go To Templates"
                    CssClass="button-thin float-left padded"
                    OnClientClick="window.location='/RiskTemplateEdit.aspx';return false;" />
                <asp:Button runat="server"
                    ID="btnNewSection" ClientIDMode="Static"
                    Text="New Section"
                    CssClass="button-thin float-right padded"
                    OnClientClick="NewEditOrCopySection(-1); return false;" />
            <br/><br/>
            <% CreateSections(); %>
            <% CreateSectionContent(); %>
       </asp:Panel>
    </asp:Panel>
    <script type="text/javascript" async>
        $(function () {
            try {
                var Param = GetURLParameter('curTab');
                if (Param != 'null')
                    Section_curTab = parseInt(Param);
            } catch (e) { }
            SwitchSection();
            //LayoutSectionList();
        });
    </script>
</asp:Content>
