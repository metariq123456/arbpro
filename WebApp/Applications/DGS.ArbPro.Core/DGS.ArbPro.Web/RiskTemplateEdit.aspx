﻿<%@ Page Title="Risk Templates" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RiskTemplateEdit.aspx.cs" Inherits="DGS.ArbPro.Web.RiskTemplateEdit" %>

<asp:Content runat="server" ID="TopContent" ContentPlaceHolderID="TopContent">
    <asp:Panel ID="Panel3" runat="server" CssClass="width-full bordered-bottom" Height="28px">

    </asp:Panel>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <script type="text/javascript" src="Content/js/TemplateRiskEdit.js?V=3.23.5"></script>

    <asp:Panel runat="server" ID="pnlRiskList" ClientIDMode="Static" CssClass="jobsList ">
        <asp:Panel runat="server"
            ID="pnlRiskListFilter" ClientIDMode="Static"
            CssClass="jobsList top">
            <asp:Label runat="server"
                ID="lblClientName" ClientIDMode="Static"
                CssClass="width-full">
                    Risk Templates
            </asp:Label>
            <br />
            <asp:Button runat="server"
                ID="btnNewTemplate" ClientIDMode="Static"
                Text="Add New Template"
                CssClass="button-thin float-right padded"
                OnClientClick="NewOrEditRiskTemplate(-1); return false;"/>
        </asp:Panel>
        <asp:Panel ID="Panel1" runat="server"
            CssClass="jobsList drag" onclick="ToggleList();">
        </asp:Panel>
        <asp:Panel runat="server"
            ID="pnlRiskListList"
            ClientIDMode="Static"
            CssClass="jobsList list">
        </asp:Panel>
    </asp:Panel>
    
    <asp:Panel runat="server" ID="pnlEdit" ClientIDMode="Static" CssClass="jobContent">
    <asp:Panel ID="Panel2" runat="server" CssClass="float-clear width-full">
            <asp:Panel runat="server" ID="pnlRiskTemplateDetails" CssClass="jobTabs align-left" Style="margin-bottom: 50px;">
            <asp:Panel ID="pnlJobWorkflow" runat="server" CssClass="colored-raise jobHeader">
                <asp:Label ID="lblJobWorkflow" runat="server">
                    Selected Template
                </asp:Label>
            </asp:Panel>
                

            <asp:Panel runat="server" ID="pnlRiskDetailsTabs" CssClass="jobTabBar">
                <asp:HyperLink runat="server" ID="tabRiskStage_0" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchRiskStageTab(0);">
                    Summary
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabRiskStage_1" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchRiskStageTab(1);">
                    Sections
                </asp:HyperLink>
            </asp:Panel>
                
            
            <asp:Panel runat="server" ID="pnlRiskDetailsContent" CssClass="width-full">
                <asp:Panel runat="server" ID="pnlRiskStage_0" ClientIDMode="Static" CssClass="hidden">
                    <asp:Panel runat="server" ID="pnlRiskDetailsWait" ClientIDMode="Static" CssClass="float-left width-full">
                        <asp:Panel ID="Panel4" runat="server" CssClass="center width-full">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlRiskDetails" ClientIDMode="Static" CssClass="float-left width-full hidden">
                        
                    </asp:Panel>
                </asp:Panel>
                

                <asp:Panel runat="server" ID="pnlRiskStage_1" ClientIDMode="Static" CssClass="hidden">
                    <asp:Panel runat="server" ID="pnlSectionDetailsWait" ClientIDMode="Static" CssClass="float-left width-full">
                        <asp:Panel ID="Panel5" runat="server" CssClass="center width-full">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    
                    <asp:Panel runat="server" ID="pnlSectionListHead" Width="30%" ClientIDMode="Static" CssClass="float-left">
                        <asp:Label Height="26px" CssClass="apptTitle width-full float-left loadBar" runat="server"> Name </asp:Label>
                        <asp:Panel runat="server" ID="pnlSectionList" ClientIDMode="Static" CssClass="float-left width-full">
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlSectionDetailsHead" Width="70%" ClientIDMode="Static" CssClass="float-right">
                        <asp:Panel runat="server" Height="26px" ClientIDMode="Static" CssClass="apptTitle width-full float-left loadBar">
                            <asp:Label ID="Label1" CssClass="float-left" runat="server"> Details </asp:Label>
                            <asp:Button ID="btnAddSections" runat="server" ClientIDMode="Static" Text="Add Sections" CssClass="button-thin float-right padded" OnClientClick="AddSection(); return false;" />
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlSectionDetails"  ClientIDMode="Static" CssClass="float-left width-full">
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
               
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    </asp:Panel>
    
    <script type="text/javascript" async>
        $(function () {
            SidePanel_Display();
            SwitchRiskStageTab(0);
        });
    </script>
</asp:Content>
