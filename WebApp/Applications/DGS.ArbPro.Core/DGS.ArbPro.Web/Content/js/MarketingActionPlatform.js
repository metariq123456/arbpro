﻿
var WorkCategories = null;
var ClientWorkCategories = null;
var JobWorkCategories = null;
var Categories = null;
var JobId = 0;
var IsQuoteAccepted = false;
var IncludeInMarketing = false;

// Function to load the content of the dialogue window for a client and job
function LoadWorkCategoriesForClient(clientId, jobId, includeInMarketing) {
    IncludeInMarketing = includeInMarketing;
    JobId = jobId;
    WorkCategories = GetAllWorkCategories();
    ClientWorkCategories = GetAllClientWorkCategoriesForClientId(clientId);
    JobWorkCategories = GetAllClientWorkCategoriesForJobId(jobId);
    Categories = GetAllReturnTimeCategories();

    PopulateWorkCategoriesList();
}

// Function used to check if work categories were found and call the display functions if they are
function PopulateWorkCategoriesList() {
    var listPanel = document.getElementById('pnlTableContainer');
    var strHtml = '';

    if (!WorkCategories) {
        strHtml += '<div style="height: 126px; overflow: auto; width-full;">';
        strHtml += '</div>';
        strHtml += '<table class="rtGrid width-full">';
        strHtml += '<tr><td class="center">There are no work categories found. </td></tr>';
        strHtml += '<tr><td class="center">Please go to the Settings screen and click on "Work Categories" to add new work categories.</td></tr>';
        strHtml += '</table>';
    } else {
        strHtml += PopulateClientWorkCategories();

        if (IncludeInMarketing) {
            strHtml += PopulateJobWorkCategories();
        } else {
            strHtml += PopulateNotIncluded();
        }
    }

    listPanel.innerHTML = strHtml;
}

// Function to display all of the work categories related to a client
function PopulateClientWorkCategories() {
    var strHtml = '';

    strHtml += '<div style="height: 85px; overflow: auto; width-full;">';
    strHtml += '<table Width="97%" class="rtGrid"><col width="20%"></col><col width="30%"></col><col width="11%"></col><col width="13%"></col><col width="26%"></col>';
    strHtml += '<tr><td colspan="5" class="rowHeader warning-text"> &nbsp &nbsp &nbsp &nbsp To disable the automated display of this screen go to Settings > Marketing > General & un-tick.</td></tr>';
    strHtml += '<tr><th colspan="5">All Work Categories For Client</th></tr>';
    strHtml += '<tr><td class="rowHeader">Name</td><td class="rowHeader">Description</td><td class="rowHeader">Type</td><td class="rowHeader">Job Number</td><td class="rowHeader">Return Date/ Status</td></tr>';
    strHtml += '</table>';
    strHtml += '</div>';

    if (ClientWorkCategories && ClientWorkCategories.length > 0) {
        strHtml += '<div style="height: 120px; overflow: auto; width-full;">';

        if (ClientWorkCategories.length > 5) {
            strHtml += '<table Width="99%" class="rtGrid"><col width="20%"></col><col width="30%"></col><col width="11%"></col><col width="13%"><col width="26%"></col>';
        } else {
            strHtml += '<table Width="97%" class="rtGrid"><col width="20%"></col><col width="30%"></col><col width="11%"></col><col width="13%"></col><col width="26%"></col>';
        }

        ClientWorkCategories.forEach(function (c) {
            strHtml += '<tr><td class="rowData" disabled="true">' + c.Name + '</td>';
            strHtml += '<td class="rowData" disabled="true">' + c.Description + '</td>';

            if (c.IsForReturnMarketing) {
                strHtml += '<td class="rowData" disabled="true">Return</td>';
            } else {
                strHtml += '<td class="rowData" disabled="true">Seasonal</td>';
            }

            strHtml += '<td class="rowData" disabled="true">' + c.JobId + '</td>';

            if (c.IsForReturnMarketing) {
                if (c.ReturnTimeCategory === 99) {
                    strHtml += '<td class="rowData" disabled="true"> N/A </td>';
                } else if (c.EmailSent) {
                    strHtml += '<td class="rowData" disabled="true"> Sent on ' + toFormattedDateStringFromJson(c.EmailSentDate, '', 'en_gb') + ' </td>';
                } else {
                    strHtml += '<td class="rowData" disabled="true">' + toFormattedDateStringFromJson(c.ReturnDate, '', 'en_gb') + '</td>';
                }
            } else {
                if (c.QuoteAccepted) {
                    strHtml += '<td class="rowData" disabled="true">Accepted</td>';
                } else {
                    strHtml += '<td class="rowData" disabled="true">Not Accepted</td>';
                }
            }
        });

        strHtml += '</table>';
        strHtml += '</div>';
    } else {
        strHtml += '<div style="height: 40px; overflow: auto; width-full;">';
        strHtml += '</div>';
        strHtml += '<div style="height: 80px; overflow: auto; width-full;">';
        strHtml += '<label class="clientField center"> There are no work categories associated with this client. You can add work categories to this client from the list below.</label>';
        strHtml += '</div>';
    }

    return strHtml;
}

// Function to display all of the work categories for the job
// This goes through the job work categories and matches them up against all existing work categories
function PopulateJobWorkCategories() {
    var strHtml = '';

    strHtml += '<div style="height: 85px; overflow: auto; width-full;">';
    strHtml += '<table class="rtGrid width-full"><col width="19%"></col><col width="15%"></col><col width="34%"></col><col width="32%"></col>';
    strHtml += '<tr><td colspan="4" class="rowHeader">&nbsp</td></tr>';
    strHtml += '<tr><th colspan="4">Work Categories For This Job</th></tr>';
    strHtml += '<tr><td class="rowHeader">Name</td><td class="rowHeader">Return Time</td><td class="rowHeader">Marketing Type/ Status</td><td class="rowHeader">Comments</td></tr>';
    strHtml += '</table>';
    strHtml += '</div>';

    if (WorkCategories && WorkCategories.length > 0) {
        strHtml += '<div style="height: 250px; overflow: auto; width-full;">';
        strHtml += '<table class="rtGrid width-full"><col width="20%"></col><col width="15%"></col><col width="31%"></col><col width="34%"></col>';
        strHtml += DisplayJobWorkCategories();
        strHtml += '</table>';
        strHtml += '</div>';
    } else {
        strHtml += '<div style="height: 60px; overflow: auto; width-full;">';
        strHtml += '</div>';
        strHtml += '<div style="height: 190px; overflow: auto; width-full;">';
        strHtml += '<label class="clientField center"> There are no existing Work Categories. Please go to the Settings page and click on Work Categories in order to add some. </label>';
        strHtml += '</div>';
    }

    return strHtml;
}

// Function to populate the table with work categories for the job
function DisplayJobWorkCategories() {
    var strHtml = '';

    WorkCategories.forEach(function (w) {
        var isAdded = false;
        if (JobWorkCategories && JobWorkCategories.length > 0) {
            JobWorkCategories.forEach(function (j) {
                if (j.Name === w.Name && j.Description === w.Description) {
                    strHtml += '<tr><td class="rowData">' + j.Name + '</td>';
                    if (j.EmailSent) {
                        strHtml += '<td class="rowData"> Sent </td>';
                    } else {
                        strHtml += '<td class="rowData">';
                        strHtml += PopulateReturnTimeDropDown(j.ReturnTimeCategory, w.Id);
                        strHtml += '</td>';
                    }

                    strHtml += '<td class="rowData">';
                    if (j.EmailSent) {
                        strHtml += '<select id="ddlAction' + w.Id + '" class="jobInput" name="ddlCategories" disabled="true">';
                    } else {
                        strHtml += '<select id="ddlAction' + w.Id + '" class="jobInput" name="ddlCategories">';
                    }

                    if (j.IsForReturnMarketing) {
                        strHtml += '<option value="0">Not Included</option>';
                        strHtml += '<option value="1">Seasonal (Not Accepted)</option>';
                        strHtml += '<option value="2">Seasonal (Accepted)</option>';
                        strHtml += '<option value="3" selected="true">Return & Seasonal (Accepted)</option>';
                    } else if (j.QuoteAccepted) {
                        strHtml += '<option value="0">Not Included</option>';
                        strHtml += '<option value="1">Seasonal (Not Accepted)</option>';
                        strHtml += '<option value="2" selected="true">Seasonal (Accepted)</option>';
                        strHtml += '<option value="3">Return & Seasonal (Accepted)</option>';
                    } else {
                        strHtml += '<option value="0">Not Included</option>';
                        strHtml += '<option value="1" selected="true">Seasonal (Not Accepted)</option>';
                        strHtml += '<option value="2">Seasonal (Accepted)</option>';
                        strHtml += '<option value="3">Return & Seasonal (Accepted)</option>';
                    }
                    strHtml += '</select>';
                    strHtml += '</td>';
                    strHtml += '<td class="rowData">';
                    if (j.EmailSent) {
                        strHtml += '<textarea id="txtComments' + w.Id + '" class="width-full settings-textboxSmall" TextMode="MultiLine" style="width:95%;" name="txtComments" disabled="true">' + j.Comments + '</textarea></td>';
                    } else {
                        strHtml += '<textarea id="txtComments' + w.Id + '" class="width-full settings-textboxSmall" TextMode="MultiLine" style="width:95%;" name="txtComments">' + j.Comments + '</textarea></td>';
                    }
                    strHtml += '</tr>';
                    isAdded = true;
                }
            });
            if (!isAdded) {
                strHtml += '<tr><td class="rowData">' + w.Name + '</td>';
                strHtml += '<td class="rowData">';
                strHtml += PopulateReturnTimeDropDown(w.ReturnTimeCategory, w.Id);
                strHtml += '</td>';
                strHtml += '<td class="rowData">';
                strHtml += '<select id="ddlAction' + w.Id + '" class="jobInput" name="ddlCategories">';
                strHtml += '<option value="0" selected="true">Not Included</option>';
                strHtml += '<option value="1">Seasonal (Not Accepted)</option>';
                strHtml += '<option value="2">Seasonal (Accepted)</option>';
                strHtml += '<option value="3">Return & Seasonal (Accepted)</option>';
                strHtml += '</select>';
                strHtml += '</td>';
                strHtml += '<td class="rowData">';
                strHtml += '<textarea id="txtComments' + w.Id + '" class="width-full settings-textboxSmall" TextMode="MultiLine" style="width:95%;" name="txtComments">' + w.Comments + '</textarea></td>';
                strHtml += '</tr>';
            }
        } else {
            strHtml += '<tr><td class="rowData">' + w.Name + '</td>';
            strHtml += '<td class="rowData">';
            strHtml += PopulateReturnTimeDropDown(w.ReturnTimeCategory, w.Id);
            strHtml += '</td>';
            strHtml += '<td class="rowData">';
            strHtml += '<select id="ddlAction' + w.Id + '" class="jobInput" name="ddlCategories">';
            strHtml += '<option value="0" selected="true">Not Included</option>';
            strHtml += '<option value="1">Seasonal (Not Accepted)</option>';
            strHtml += '<option value="2">Seasonal (Accepted)</option>';
            strHtml += '<option value="3">Return & Seasonal (Accepted)</option>';
            strHtml += '</select>';
            strHtml += '</td>';
            strHtml += '<td class="rowData">';
            strHtml += '<textarea id="txtComments' + w.Id + '" class="width-full settings-textboxSmall" TextMode="MultiLine" style="width:95%;" name="txtComments">' + w.Comments + '</textarea></td>';
            strHtml += '</tr>';
        }
    });

    return strHtml;
}

// Function to display message if no work categories were found
function PopulateNotIncluded() {
    var strHtml = '';

    strHtml += '<div style="height: 126px; overflow: auto; width-full;">';
    strHtml += '</div>';
    strHtml += '<table class="rtGrid width-full">';
    strHtml += '<tr><td class="center">This client has opted out of the Marketing service.</td></tr>';
    strHtml += '<tr><td class="center">Please go to the Clients screen and edit the client should you want to include them in future Marketing.</td></tr>';
    strHtml += '</table>';

    return strHtml;
}

// Function to populate the Return Time dropdown menu with all options
function PopulateReturnTimeDropDown(categoryId, id) {
    var strHtml = '';
    strHtml += '<select id="ddlCategories' + id +'" class="jobInput" name="ddlCategories">';
    Categories.forEach(function (c) {
        if (c.TimeCategoryId === categoryId) {
            strHtml += '<option value="' + c.TimeCategoryId + '" selected="true"> ' + c.TimeCategoryName + '</option>';
        } else {
            strHtml += '<option value="' + c.TimeCategoryId + '"> ' + c.TimeCategoryName + '</option>';
        }
    });
    strHtml += '</select>';

    return strHtml;
}

// Function to sort out which client work categories need to be saved and which removed
function Save() {
    var newCategories = [];
    WorkCategories.forEach(function(w) {
        var selectedCategory = document.getElementById('ddlAction' + w.Id);
        var selectedCategoryValue = parseInt(selectedCategory[selectedCategory.selectedIndex].value);

        if (selectedCategoryValue && selectedCategoryValue > 0) {
            var curDate = new Date();
            var doesExist = false;
            var temp = {};
            var ddl = document.getElementById('ddlCategories' + w.Id);
            if (ddl) {
                temp.ReturnTimeCategory = parseInt(ddl[ddl.selectedIndex].value);
            }

            temp.Name = w.Name;
            temp.Description = w.Description;
            temp.Comments = document.getElementById('txtComments' + w.Id).value;
            if (temp.ReturnTimeCategory === 99) {
                temp.ReturnDate = toDateFromJson(0);
            } else {
                var newDate = curDate.setMonth(curDate.getMonth() + temp.ReturnTimeCategory);
                temp.ReturnDate = toDateFromJson(newDate);
            }

            temp.EmailSentDate = toDateFromJson(0);
            temp.JobId = JobId;
            temp.QuoteAccepted = selectedCategoryValue > 1;
            temp.IsForReturnMarketing = selectedCategoryValue === 3;

            ClientWorkCategories.forEach(function(c) {
                for (var i = JobWorkCategories.length - 1; i >= 0; i--) {
                    if (JobWorkCategories[i].Id === c.Id && JobWorkCategories[i].Name === w.Name && JobWorkCategories[i].Description === w.Description) {
                        temp.Id = JobWorkCategories[i].Id;
                        if (!ddl || JobWorkCategories[i].EmailSent) {
                            JobWorkCategories.splice(i, 1);
                            return;
                        }

                        doesExist = true;
                        JobWorkCategories.splice(i, 1);
                    }
                }
            });

            if (!doesExist) {
                temp.Id = -1;
            }

            if (ddl) {
                newCategories.push(temp);
            }

            temp = {};
        }
    });

    for (var i = JobWorkCategories.length - 1; i >= 0; i--) {
        var name = JobWorkCategories[i].Name;
        var didFind = false;
        WorkCategories.forEach(function(w) {
            if (name === w.Name) {
                didFind = true;
            }
        });

        if (!didFind) {
            JobWorkCategories.splice(i, 1);
        }
    }

    return SaveJobWorkCategories(newCategories, JobWorkCategories);
}

// Function to get all work categories
function GetAllWorkCategories() {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetAllWorkCategories',
        {
        },
        false
    );

    return catList;
}

// Function to get all return time categories
function GetAllReturnTimeCategories() {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetAllReturnTimeCategories',
        {
        },
        false
    );
    return catList;
}

// Function to get all client work categories for client id
function GetAllClientWorkCategoriesForClientId(clientId) {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetClientWorkCategoriesForClientId',
        {
            clientId: clientId
        },
        false
    );

    return catList;
}

// Function to get all client work categories for job id
function GetAllClientWorkCategoriesForJobId(jobId) {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetClientWorkCategoriesForJobId',
        {
            jobId: jobId
        },
        false
    );

    return catList;
}

// Function to save job work categories
function SaveJobWorkCategories(categories, jobCategories) {
    try {
        if (categories && categories.length > 0) {
            // Save any new ones or update any existing ones
            categories.forEach(function(c) {
                Ajax_CallService(
                    'Services/Web/Marketing.asmx',
                    'SaveClientWorkCategory',
                    {
                        obj: c
                    },
                    false
                );
            });
        }

        if (jobCategories && jobCategories.length > 0) {
            // Delete any left over ones that have been unchecked.
            jobCategories.forEach(function(j) {
                Ajax_CallService(
                    'Services/Web/Marketing.asmx',
                    'DeleteClientWorkCategoryForId',
                    {
                        id: j.Id
                    },
                    false
                );
            });
        }
    } catch (e) {
        DisplayMessage('pnlTableContainer', 'ERROR: Unable to update client work categories - ' + e.toString());
        return false;
    }

    return true;
}
