﻿/*
    This java script file contains methods and functions to display location data
    for all items at a job site on a map
*/

//Scope variables
var QuoteId = -1;           //The quote id for the linked items
var CurDetails = null;         //The map details search result for the quote site
var GlgMap = null;          //Reference to the map control

/*  Initial load function: Loads a site and creates the map then loads the Quote Items
    Params:
        quoteId:    Int - The Id of the quote for the items
        siteId:     Int - The Id of the site the items are located at
*/
function QuoteItemMap_Load(quoteId, siteId) {
    //debugger;
    //Error messages raised in this function are displayed in the main content view not the error messages panel
    if (quoteId < 1 || siteId < 1) {
        //The ids supplied are not valid, display an error
        DisplayMessage('pnlItemMapCanvas', 'Error: The Quote Id or the Site are not valid.');
    }
    else {
        try {
            QuoteId = quoteId;
            MapDetailLoad(siteId);
        } catch (err) {
            DisplayMessage('pnlItemMapCanvas', 'Error: Unable to display item map - ' + err.toString());
        }
    }
}

/*
    Function to generate an Async service call to load the map details for the specified quote and job site
    PARAMS:
            siteId:     Int - The Job Site Id
*/
function MapDetailLoad(siteId) {
    try {
        Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'GetQuoteSiteMap',
                {
                    JobSiteID: siteId,
                    QuoteID: QuoteId
                },
                true,
                MapDetailLoadCallBack
       );
    } catch (err) {
        DisplayMessage('pnlError', 'Error: Unable to load Quote Items for Job Site - ' + err.toString());
    }
}

/*
    Async service callback to display map details
    PARAMS:
        result:     Types.MapDetailsSearchResult - The map data for the selected quote site
*/
function MapDetailLoadCallBack(result) {
    var panel = document.getElementById('pnlItemMapCanvas');
    try {
        //The returned object is valid so setup the map
        if (result && result.Id > 0) {
            CurDetails = result;
            //Create the map options
            var mapOptions = {
                center: new google.maps.LatLng(54, -2),
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.SATELLITE
            };
            //Create the map and set it to the panel
            GlgMap = new google.maps.Map(panel, mapOptions);
            
            //Create the marker for the Site if possible
            if (CurDetails.SitePostCode && CurDetails.SitePostCode.length > 0) {
                var geocoder = new google.maps.Geocoder();
                try{
                    geocoder.geocode({ 'address': CurDetails.SitePostCode + ', UK' },
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var loc = results[0].geometry.location;
                                SetMarker(loc.lat(), loc.lng(), CurDetails.SitePostCode, CurDetails.SiteAddress, true);
                            }
                        });
                }
                catch (ex) {
                    alert('Error when trying to asign item markers: ' + ex.toString());
                }
            }
            //Add the info panel to the map
            AddInfoControl();

            //The main details have been generated now generate the item markers
            QuoteItemsDisplay(CurDetails.SiteItems);

        }
        else {
            DisplayMessage('pnlError', 'ERROR: Arb Pro service did not return valid map details');
        }
    }catch(err){
        DisplayMessage('pnlError','Error: Unable to generate map data - ' + err.toString());
    }
}


/*
    Async service call back function to populate the map with pins for each Quote Item
    PARAMS:
            results:    List<Types.QuoteItem> - A list of all the quote items for the quote job site
*/
function QuoteItemsDisplay(LstQuoteItems) {
    //check if the returned list is valid
    if (!LstQuoteItems || LstQuoteItems.length == 0) {
        DisplayMessage('pnlError', 'Error: Arb Pro service did not return a valid list of Quote items');
    }
    else {
        try {
            LstQuoteItems.forEach(function (itm) {
                //Check if this item could be within the correct bounds
                if (itm.Latitude > 0 || itm.Longitude > 0) {
                    SetMarker(itm.Latitude, itm.Longitude, itm.Code, itm.Name, false);
                }
            });
        } catch (err) {
            DisplayMessage('pnlError', 'Error: Unable to add all map pins for Quote items - ' + err.toString());
        }
    }
}

/*
    Function to add a map pin to the map
    PARAMS:
        lat:        Float - The latitude of the object
        long:       Float - The longitude of the object
        pinTitle:   String - The title to be displayed for the pin
        centerMap:  Bool - Flag to say if the map should be centered on this marker
*/
function SetMarker(lat, long, label, pinTitle, centerMap) {
    var myLatLng = new google.maps.LatLng(lat,long);
    var mapPin = new MarkerWithLabel({ //google.maps.Marker({
        position: myLatLng,
        map: GlgMap,
        title: pinTitle,
        labelContent: label,
        labelAnchor: new google.maps.Point(0, 0),
        labelClass: "map-label" // the CSS class for the label
    });

    if(centerMap){
        GlgMap.panTo(myLatLng);
    }
}

/*
    Function to generate and add the quote details to the map
*/
function AddInfoControl() {
    var details = document.createElement('div');

    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = 'white';
    controlUI.style.borderStyle = 'solid';
    controlUI.style.borderWidth = '2px';
    controlUI.style.cursor = 'pointer';
    controlUI.style.textAlign = 'left';
    controlUI.title = 'Quote Details';
    details.appendChild(controlUI);

    var controlText = document.createElement('div');
    controlText.style.fontFamily = 'Arial,sans-serif';
    controlText.style.fontSize = '12px';
    controlText.style.paddingLeft = '4px';
    controlText.style.paddingRight = '4px';
    controlText.innerHTML = CurDetails.SideBarDetail;
    controlUI.appendChild(controlText);

    details.index = 2;
    GlgMap.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(details);
}


/*
    Function to print the map.
    This function copies the map canvase into another window, prints the contents of that window then closes it.
*/
function PrintQuoteItemMap() {
    var map = document.getElementById('pnlItemMapCanvas').innerHTML;
    var html = '<html><head><title>Quote Items for Quote Id: ' + QuoteId + '</title>';
    html += "<style>body { padding: 15px; } .map-label {";
    html += "color: black;";
    html += "background-color: white;";
    html += 'font-family: "Arial";';
    html += "font-size: 10px;";
    html += "font-weight: bold;";
    html += "text-align: center;";
    html += "width: 60px;";
    html += "border: 2px solid black;";
    html += "white-space: nowrap;";
    html += "}</style>";
    html += "</head>";
    html += "<body><div id='myprint'>" + map + "</div></body></html>";

    var theWork = window.open('', 'PrintWindow');

    theWork.document.open();
    theWork.document.write(html);
    theWork.document.close();

    setTimeout(function () {
        theWork.print();
        theWork.close();
    }, 10);


    return false;
}