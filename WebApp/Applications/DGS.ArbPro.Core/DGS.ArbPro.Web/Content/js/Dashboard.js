﻿var Quote_curTab = 0;
var Invoice_curTab = 0;
var Appointment_curTab = 4;
var Tpo_curTab = 0;

function switchTab(tabCount, tabName, detailName, selectedTab) {
    for (var i = 0; i < tabCount; i++) {
        var tabElement = document.getElementById(tabName + i.toString());
        var detailElement = document.getElementById(detailName + i.toString());
        if (!tabElement) {
            tabElement = document.getElementById('Content_' + tabName + i.toString());
        }
        if (!detailElement) {
            detailElement = document.getElementById('Content_' + detailName + i.toString());
        }
        if (i == selectedTab) {
            tabElement.className = 'dashboardTab activeTab';
            detailElement.className = '';
        } else {
            tabElement.className = 'dashboardTab';
            detailElement.className = 'hidden';
        }
    }
}

function SwitchAppointmentTab() {

    switchTab(5, 'tabAppointments_', 'pnlAppointmentsTab_', Appointment_curTab);
    GetAppointmentDataForTab(Appointment_curTab);
}

function SwitchInvoiceTab() {
    switchTab(5, 'tabInvoices_', 'pnlInvoicesTab_', Invoice_curTab);
    GetInvoiceDataForTab(Invoice_curTab);
}

function SwitchQuoteTab() {
    switchTab(6, 'tabQuotes_', 'pnlQuoteTab_', Quote_curTab);
    GetQuoteDataForTab(Quote_curTab);
}

function SwitchTpoTab() {
    switchTab(4, 'pnlTpoTab_', 'pnlTpoDetail_', Tpo_curTab);
    GetTpoDataForTab(Tpo_curTab);
}

/*
    INVOICE PANEL METHODS
*/

function GetInvoiceDataForTab(tabNumber) {
    switch (tabNumber) {

        case 0:
            GetInvoicesOverdue();
            break;
        case 1:
            GetInvoicesPending();
            break;
        case 2:
            GetAllInvoicesDue();
            break;
        case 3:
            GetAllInvoicesSent();
            break;
        case 4:
            GetAllInvoicesPaid();
            break;
    }
}

function GetInvoicesOverdue() {
    var SearchedList = Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetOverdueInvoices',
        {},
        true,
        PopulateOverdueInvoices
    );
}

function GetInvoicesPending() {
    var SearchedList = Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetPendingInvoices',
        {},
        true,
        PopulateInvoicesPending
    );
}

function GetAllInvoicesDue() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetAllInvoicesDue',
        {},
        true,
        PopulateInvoicesDue
    );
}

function GetAllInvoicesSent() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetAllInvoicesSent',
        {},
        true,
        PopulateInvoicesSent
    );
}
function GetAllInvoicesPaid() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetAllInvoicesPaid',
        {},
        true,
        PopulateInvoicesPaid
    );
}

function PopulateInvoicesDue(results) {
    PopulateGenericPanel('pnlScheduledPayments', 'lblScheduledPayments', results, function (i) { return i.QuoteNumber; });
}

function PopulateInvoicesPending(results) {
    PopulateGenericPanel('pnlPendingInvoices', 'lblPendingInvoices', results, function (i) { return i.QuoteNumber; });
}

function PopulateOverdueInvoices(results) {
    PopulateGenericPanel('pnlOverdueInvoices', 'lblOverdueInvoices', results, function (i) { return i.QuoteNumber; });
}

function PopulateInvoicesSent(results) {
    PopulateGenericPanel('pnlQuoteSentAll', 'lblInvoiceSentAll', results, function (i) { return i.QuoteNumber; });
}

function PopulateInvoicesPaid(results) {
    PopulateGenericPanel('pnlQuotePaidAll', 'lblInvoicePaidAll', results, function (i) { return i.QuoteNumber; });
}
/*
    QUOTE PANEL METHODS
*/

function GetQuoteDataForTab(tabNumber) {
    switch (tabNumber) {
        case 0:
            GetEnquiriesPending();
            break;
        case 1:
            GetQuotesPending();
            break;
        case 2:
            GetWorkPending();
            break;
        case 3:
            GetRecentQuoteNotes();
            break;
        case 4:
            GetQuoteSent();
            break;
        case 5:
            GetAllQuoteSent();
            break;
    }
}

function GetEnquiriesPending() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetPendingJobs',
        {},
        true,
        PopulateEnquiriesPending
    );
}

function GetQuotesPending() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetPendingQuotes',
        {},
        true,
        PopulateQuotesPending
    );
}

function GetQuoteSent() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
       'GetSentQuotes',
        {},
        true,
        PopulateQuotesSent
    );
}

function GetAllQuoteSent() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetAllSentQuotes',
        {},
        true,
        PopulateAllQuotesSent
    );
}

function GetWorkPending() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetPendingWork',
        {},
        true,
        PopulateWorkPending
    );
}

function GetRecentQuoteNotes() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetRecentQuoteNotes',
        {},
        true,
        PopulateRecentQuoteNotes
    );
}

function PopulateWorkPending(results) {
    //function SortByParentId(a, b) { return b.ParentId - a.ParentId; }
    //results.Results.sort(SortByParentId);
    PopulateGenericPanel('pnlPendingWork', 'lblPendingWork', results);
}

function PopulateQuotesPending(results) {
    //function SortById(a, b) { return b.Id - a.Id; }
    //results.Results.sort(SortById);
    PopulateGenericPanel('pnlPendingQuotes', 'lblPendingQuotes', results);
}

function PopulateQuotesSent(results) {
    //function SortById(a, b) { return b.Id - a.Id; }
    //results.Results.sort(SortById);
    PopulateGenericPanel('pnlSentQuotes', 'lblSentQuotes', results);
}

function PopulateAllQuotesSent(results) {
    //function SortById(a, b) { return b.Id - a.Id; }
    //results.Results.sort(SortById);
    PopulateGenericPanel('pnlAllQuoteSent', 'lblAllQuoteSent', results);
}
function PopulateEnquiriesPending(results) {
    //function SortById(a, b) { return b.Id - a.Id; }
    //results.Results.sort(SortById);
    PopulateGenericPanel('pnlPendingEnquiries', 'lblPendingEnquiries', results);
}

function PopulateRecentQuoteNotes(results) {
    //function SortById(a, b) { return b.Id - a.Id; }
    //results.Results.sort(SortById);
    PopulateGenericPanel('pnlPendingNotes', 'lblPendingNotes', results);
}

/*
    TPO PANEL METHODS
*/

function GetTpoDataForTab(tabNumber) {
    switch (tabNumber) {
        case 0:
            GetSixWeekExpiredTpos();
            break;
        case 1:
            GetEightWeekExpiredTpos();
            break;
        case 2:
            GetAppealedTpos();
            break;
        case 3:
            GetGrantedTpos();
            break;
    }
}

function GetSixWeekExpiredTpos() {
    try {
        Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'TposGetSixWeekExpired',
        {},
        true,
        PopulateTpoSixWeekTab
        );
    }
    catch (err) {
        DisplayMessage('pnlTpoDetailContent_0', 'Error: Unable to communicate with ArbPro service.');
    }
}

function GetEightWeekExpiredTpos() {
    try {
        Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'TposGetEightWeekExpired',
        {},
        true,
        PopulateTpoEightWeekTab
        );
    }
    catch (err) {
        DisplayMessage('pnlTpoDetailContent_1', 'Error: Unable to communicate with ArbPro service.');
    }
}

function GetAppealedTpos() {
    try {
        Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'TposGetAppeals',
        {},
        true,
        PopulateTpoAppealedTab
        );
    }
    catch (err) {
        DisplayMessage('pnlTpoDetailContent_2', 'Error: Unable to communicate with ArbPro service.');
    }
}

function GetGrantedTpos() {
    try {
        Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'TposGetGranted',
        {},
        true,
        PopulateTpoGrantedTab
        );
    }
    catch (err) {
        DisplayMessage('pnlTpoDetailContent_2', 'Error: Unable to communicate with ArbPro service.');
    }
}

function PopulateTpoSixWeekTab(results) {
    //PopulateSectionLabel('tpoLabel2', 'All TPOs that have reach the 6 week determination date.', results);
    PopulateGenericPanel('pnlTpoDetailContent_0', 'tpoLabel2', results, function (o) { return o.JobId; });
}

function PopulateTpoEightWeekTab(results) {
    //PopulateSectionLabel('tpoLabel3', 'All TPOs that have reach the 8 week determination date.', results);
    PopulateGenericPanel('pnlTpoDetailContent_1', 'tpoLabel3', results, function (o) { return o.JobId; });
}

function PopulateTpoAppealedTab(results) {
    //PopulateSectionLabel('tpoLabel4', 'ALL TPO decsions that have been appealed and awaiting decision.', results);
    PopulateGenericPanel('pnlTpoDetailContent_2', 'tpoLabel4', results, function (o) { return o.JobId; });
}

function PopulateTpoGrantedTab(results) {
    //PopulateSectionLabel('tpoLabel4', 'ALL TPO decsions that have been appealed and awaiting decision.', results);
    PopulateGenericPanel('pnlTpoDetailContent_3', 'tpoLabel5', results, function (o) { return o.JobId; });
}


/*
    APPOINTMENT PANEL METHODS
*/

function GetAppointmentDataForTab(tabNumber) {
    switch (tabNumber) {
        case 0:
            GetTodaysAppointments();
            break;
        case 1:
            GetPendingSalesAppointments();
            break;
        case 2:
            GetScheduledWorkAppointments();
            break;
        case 3:
            GetScheduledGenericAppointments();
            break;
        case 4:
            GetFinanceSummary();
            break;
    }
}

function GetFinanceSummary() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'CompanyFinanceSummary',
        {},
        true,
        PopulateFinanceSummaryTab
    );
}

function GetTodaysAppointments() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetTodaysAppointments',
        {},
        true,
        PopulateTodaysAppionments
    );
}

function PopulateFinanceSummaryTab(results) {
    //PopulateSectionLabel('tpoLabel4', 'ALL TPO decsions that have been appealed and awaiting decision.', results);
    PopulateFinanceSummary('pnlFinance', 'lblFinanceHeader', results);
}

function PopulateTodaysAppionments(results) {
    //PopulateSectionLabel('lblTodaysAppointments', 'Appointments Today', results);
    PopulateGenericPanel('pnlTodaysAppointments', 'lblTodaysAppointments', results);
}

function GetPendingSalesAppointments() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetQuoteAppsForWeek',
        {},
        true,
        PopulatePendingSalesAppointments
    );
}

function PopulatePendingSalesAppointments(results) {
    //PopulateSectionLabel('lblSalesAppointments', 'Appointments for Surveyors', results);
    PopulateGenericPanel('pnlScheduledSales', 'lblSalesAppointments', results);
}

function GetScheduledWorkAppointments() {
    Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetWorkAppsForWeek',
        {},
        true,
        PopulateScheduledWorkAppointments
    );
}

function PopulateScheduledWorkAppointments(results) {

    //PopulateSectionLabel('lblScheduledWork', 'Work booked for the week', results);
    PopulateGenericPanel('pnlScheduledWork', 'lblScheduledWork', results);
}

function GetScheduledGenericAppointments() {
    var SearchedList = Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetGenericAppsDateRange',
        {},
        true,
        PopulateScheduledGenericApps
    );
}

function PopulateScheduledGenericApps(results) {
    PopulateGenericPanel('pnlGeneralAppointments', 'lblGeneralAppointments', results);
}

function GetScheduledCompanyAppointments() {
    //var SearchedList = Ajax_CallService(
    //    'Services/Web/Dashboard.asmx',
    //    'GetCompanyAppsForWeek',
    //    {},
    //    true,
    //    PopulateScheduledCompanyApps
    //);
}

function PopulateScheduledCompanyApps(results) {
    //PopulateSectionLabel('lblCompanyAppointments', 'Appointments for Company', results);
    PopulateGenericPanel('pnlCompanyAppointments', 'lblCompanyAppointments', results);
}


function Dashboard_Load() {
    SwitchAppointmentTab();
    SwitchQuoteTab();
    SwitchTpoTab();
    SwitchInvoiceTab();
}

function PopulateFinanceSummary(panelName, lblName, result) {
    PopulateSectionLabel(lblName, result);
    var ListPanel = document.getElementById(panelName);
    var strHtml = '<div class="width-full center"><div style="width: 90%; margin:0px auto;">';
    strHtml += '<br>'; //'<p>Navigate to the relevant dashboard tab to manage finances</p>';
    strHtml += '<table class="searchGrid width-full white">';
    try {
        if (!result) {
            strHtml += '<tr><td style="center;">Error: Unable to display entries</td></tr>';
        } else {
            strHtml += '<tr><th></th><th style="text-align: right;">Total Value</th><tr>';
            result.Results.forEach(function (c) {
                strHtml += '<tr><td>' + c.Title + '</td>';
                strHtml += '<td style="text-align: right;">' + c.Detail + '</td></tr>';
            });
        }
    }
    catch (err) {
        strHtml += '<tr><td style="padding-left: 10px;">Unable to display results</td></tr>';
    }
    strHtml += '</table>';
    strHtml += '<p>Navigate to the relevant dashboard tab to manage finances</p>';
    strHtml += '</div></div>';
    ListPanel.innerHTML = strHtml;
}


function PopulateGenericPanel(panelName, lblName, result, idSelector) {
    PopulateSectionLabel(lblName, result);
    var ListPanel = document.getElementById(panelName);
    var strHtml = '<table class="searchGrid width-full white">';
    try {
        if (!result) {
            strHtml += '<tr><td style="center;">Error: Unable to display entries</td></tr>';
        } else {
            result.Results.forEach(function (c) {
                strHtml += '<tr class="searchGrid" onclick="FireResultClicked(' + c.SearchType + ',' + c.ParentId + ',' + (idSelector != null ? idSelector(c) : c.Id) + ');">';
                strHtml += '<td style="padding-left: 1px; padding-right: 1px"><table class="width-full">';
                strHtml += '<tr><td class="colored-wash">' + c.Title + '</td></tr><tr><td>' + c.Detail + '</td></tr></table>';
                strHtml += '</tr>';
            });
        }
    }
    catch (err) {
        strHtml += '<tr><td style="padding-left: 10px;">Unable to display results</td></tr>';
    }
    strHtml += '</table>';
    ListPanel.innerHTML = strHtml;
}

function PopulateSectionLabel(lblName, summary) {

    var hdrLabel = document.getElementById(lblName);
    var valueLabel = document.getElementById(lblName + 'Value');
    try {
        hdrLabel.textContent = summary.Title;
        valueLabel.textContent = summary.AdditionalText;
    }
    catch (err)
    { }
}

function FireResultClicked(type, parentId, id) {
    /*
        Client = 0,
        Quote = 1,
        Invoice = 2,
        Appointment = 3,
        Contact = 4,
        Work = 5,
        Job = 6,
        TPO = 10
    */
    switch (type) {
        case 0:
            navigateParams(Page_strAppRoot + 'Client.aspx', [{ paramName: 'ClientID', paramValue: id }]);
            break;
        case 1:
            navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: parentId }, { paramName: 'QuoteID', paramValue: id }, { paramName: 'curTab', paramValue: 2 }]);
            break;
        case 2:
            navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: parentId }, { paramName: 'QuoteID', paramValue: id }, { paramName: 'curTab', paramValue: 4 }]);
            //navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: parentId }, { paramName: 'InvoiceID', paramValue: id }, { paramName: 'curTab', paramValue: 4 }]);
            break;
        case 3:
            navigateParams(Page_strAppRoot + 'Calendar.aspx', [{ paramName: 'AppointmentID', paramValue: id }]);
            break;
        case 4:
            navigateParams(Page_strAppRoot + 'Client.aspx', [{ paramName: 'ClientID', paramValue: parent }]);
            break;
        case 5:
            navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: parentId }, { paramName: 'WorkID', paramValue: id }, { paramName: 'curTab', paramValue: 3 }]);
            break;
        case 6:
            navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: id }, { paramName: 'curTab', paramValue: 1 }]);
            break;
        case 10:
            navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: parentId }, { paramName: 'QuoteID', paramValue: id }, { paramName: 'curTab', paramValue: 2 }]);
            break;
        case 13:
            navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: parentId }, { paramName: 'QuoteID', paramValue: id }, { paramName: 'curTab', paramValue: 2 }, { paramName: 'subTab', paramValue: 2 }]);
            break;
    }
}
