﻿/*
        This file contains functionality for editing the Job details, once a job has been created.
        The file has been added to the solution to resolve issue DGSAP-835

        Files used by this script:
                                    Common.js
                                    WorkManagement.js
                                    DateFunctions.js
*/

var pnlErrorName = 'pnlJobDetailError';         //  Reference to the name of the div where errors should be displayed
var pnlContentName = 'pnlJobDetails';           //  Reference to the name of the div where all content will be displayed
var listSurvyors = null;                        //  The list of survyors in the system.
var listJobStatuses = [
    { "Status": "Pending", "Value": 0 },
    { "Status": "Quoted", "Value": 1 },
    { "Status": "Scheduled", "Value": 2 },
    { "Status": "Work Completed", "Value": 3 },
    { "Status": "Invoiced", "Value": 4 },
    { "Status": "Paid", "Value": 5 },
    { "Status": "Credited", "Value": 6 },
    { "Status": "Closed", "Value": 98 },
    { "Status": "Cancelled", "Value": 99 }
];
/*
        *****   Initialisation and Service Call functions *****
*/

// Initialisation function to display controls, and current job detials to enable editing
function JobDetails_Load() {
    var panel = document.getElementById(pnlContentName);

    //  Show the loading image while the page is constructed
    panel.innerHTML = GetWaitImageForPanel(pnlContentName, 'Loading Please Wait...');

    if (!ObjJob || ObjJob == null) {
        // The job object is not valid an so no operation can be performed, hide the detail panel
        DisplayError('Error: The Job details have not been loaded. Unable to continue.');
        panel.class = "hidden";
        return;
    }

    Survyors_Load();
}

//  Async service call to populate the list of staff members who have been assign a role of survyor
function Survyors_Load() {
    try {
        Ajax_CallService(
                'Services/Web/Company.asmx',
                'ListSurveyors',
                {
                },
                true,
                Survyors_LoadCallBack
                );
        
    } catch (err) {
        DisplayError('Error: Unable to access Arb Pro service -' + err.toString());
    } 
}

function JobDetails_Save() {
    ObjJob.DateCreated = toDateFromJson(ObjJob.DateCreated);
    var didComplete = false;
    try {

        JobDetailsPopulate();

        didComplete = Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'JobDetailsUpdate',
                {
                    obj: ObjJob
                },
                false,
                null
                );
    } catch (err) {
        DisplayError('Error: Unable to update Job details - ' + err.toString());
        didComplete = false;
    }
    return didComplete;
}

/*
        *****   Async service callback functions *****
*/

// Callback function for loading the survyors list
function Survyors_LoadCallBack(results) {
    //  Check if the service return a valid data array
    if (!results || results.length == 0) {
        //  NOTE: This error should not prevent the edit of the job details
        DisplayError('Error:The Arb Pro service did not return a valid list of Survyors. Please ensure that at least on staff member is configured for the role.');
        results = [];   // Initialise the array incase it is undefined
    }

    listSurvyors = results;
    PopulateJobDetailPanel();
}

/*
        *****   Local functions *****
*/

//  function to construct controls and display the current job details for editing
function PopulateJobDetailPanel() {
    try {
        var panel = document.getElementById(pnlContentName);
        panel.innerHTML = GenerateJobHtml();

        AutoComplete('txtEnquirySource', 'ListEnquirySource');
    } catch (err) {
        DisplayError('Error: Unable to display Job details -' + err.toString());
    } 
}

//  Function to generate the html content for the conent div. This includes all controls and function setup
function GenerateJobHtml() {
    var strHtml = '<div style="width: 90%; margin:0px auto;">';
    //  Reporter section
    strHtml += '<div class="jobEditField">';
    strHtml += '<label  class="jobEditLabel bold">Reporter</label>';
    strHtml += '<input id="txtEnquiryReporter" class="jobEditInput" type="text" placeholder="Source" name="txtEnquiryReporter" autocomplete="off"';
    strHtml += ' value="' + ObjJob.Reporter + '" disabled>';
    strHtml += '</div>';

    //  Status selection
    strHtml += GenerateStatusSelect();

    //  Show in dashboard
    strHtml += '<div class="jobEditField">';
    strHtml += '<label  class="jobEditLabel bold">Display in Dashboard</label>';
    strHtml += '<input id="chkShowJobOnDb" type="checkbox" style="cursor: pointer;"';
    if (ObjJob.ShowInDashboard) {
        strHtml += 'checked';
    }
    strHtml += '/>';
    strHtml += '</div>';

    //  Survyor section
    strHtml += GenerateSurvyorSelect();

    //  Source section
    strHtml += '<div class="jobEditField">';
    strHtml += '<label  class="jobEditLabel bold">Enquiry Source</label>';
    strHtml += '<input id="txtEnquirySource" class="jobEditInput ui-autocomplete-input" type="text" placeholder="Source" name="txtEnquirySource" autocomplete="off"';
    strHtml += ' value="' + ObjJob.Source + '">';
    strHtml += '</div>';

    //  Comments section
    strHtml += '<div class="jobEditField">';
    strHtml += '<span class="jobEditLabel bold" style="display:inline-block;width:100%;">Comments</span>';
    strHtml += '<textarea id="txtEnquiryComments" class="jobEditField small" style="width:98%;" cols="20" rows="5" name="txtEnquiryComments">';
    strHtml += ObjJob.Comments;
    strHtml += '</textarea>';
    strHtml += '</div>';

    return strHtml;
}

function GenerateSurvyorSelect() {
    var strHtml = '';
    if (listSurvyors.length > 0) {
        strHtml = '<div class="jobEditField">';
        strHtml += '<label class="jobEditLabel bold">Surveyor</label>';
        strHtml += '<select id="ddlEnquirySurveyor" class="jobEditInput" name="ddlEnquirySurveyor">';
        strHtml += '<option value="-1">Please Select</option>';
        listSurvyors.forEach(function(s) {
            strHtml += '<option value="' + s.Id.toString() + '"';
            if(ObjJob.SurveyorId == s.Id){
                strHtml += 'selected="selected"';
            }
            strHtml += '>' + s.Name + '</option>';
        });
        strHtml += '</select>';
        strHtml += '</div>';
    }
    return strHtml;
}

function GenerateStatusSelect() {
    var strHtml = '<div class="jobEditField">';
        strHtml += '<label class="jobEditLabel bold">Current Status</label>';
        strHtml += '<select id="ddlEnquiryStatus" class="jobEditInput" name="ddlEnquiryStatus">';
        listJobStatuses.forEach(function (s) {
            strHtml += '<option value="' + s.Value.toString() + '"';
            if (ObjJob.Status == s.Value) {
                strHtml += 'selected="selected"';
            }
            strHtml += '>' + s.Status + '</option>';
        });
        strHtml += '</select>';
        strHtml += '</div>';
    return strHtml;
}

function DisplayError(message) {
    DisplayMessage(pnlErrorName, message);
    var panel = document.getElementById(pnlErrorName);
    panel.class = "width-full center";
}

//  Function to populate the job object from the captured data
function JobDetailsPopulate() {
    // debugger;
    var ddlSurv = document.getElementById('ddlEnquirySurveyor');
    var ddlStatus = document.getElementById('ddlEnquiryStatus');

    ObjJob.Status = ddlStatus.options[ddlStatus.selectedIndex].value;
    ObjJob.Source = document.getElementById('txtEnquirySource').value;
    ObjJob.SurveyorID = ddlSurv.options[ddlSurv.selectedIndex].value;
    ObjJob.Comments = document.getElementById('txtEnquiryComments').value;
    ObjJob.ShowInDashboard = document.getElementById('chkShowJobOnDb').checked;

    if (ObjJob.Status < 0) {
        ObjJob.Status = 0;
    }
    ObjJob.Invoices = [];
    ObjJob.WorkSites = [];
    ObjJob.Quotes = [];
    ObjJob.WorkSites = [];
}
