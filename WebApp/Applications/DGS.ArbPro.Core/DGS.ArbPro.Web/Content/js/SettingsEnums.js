﻿var Enum = 0;
var SelectedResultId = -1;
var IsNewItem = false;
PickItemList = null;


function Settings_Load(PickItemNum) {
    try {
        // 
        Enum = parseInt(PickItemNum);
        PickItemList = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'GetPickItemList',
           {
               itemType: Enum
           },
           false
       );
    } catch (err) {
        // 
    }
    UpdateTable(PickItemList);
}

function UpdateTable(PickItemList) {
    var ListPanel = document.getElementById('pnlTableContainer');
    var strHtml = '<table class="searchGrid width-full white">';
    for (var i = 0; i < PickItemList.length; i++) {
        if (PickItemList[i].Id == SelectedResultId) {
            strHtml += '<tr class="selected" onclick="ItemSelected(\'' + i + '\');">';
        }
        else {
            strHtml += '<tr onclick="ItemSelected(\'' + i + '\');">';
        }
        strHtml += '<td style="padding-left: 10px;">' + PickItemList[i].Value + '</td>';
        strHtml += '</tr>';
    }
    strHtml += '</table>';
    ListPanel.innerHTML = strHtml;
}

function ItemSelected(index) {
    var item = PickItemList[index];
    IsNewItem = false;
    SelectedResultId = item.Id;
    PopulateHiddenFields(item.Value);
    UpdateTable(PickItemList);
}

function PopulateHiddenFields(Value) {
    document.getElementById('btnDelete').className = 'button-thin';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtValue').value = Value;
    if (Value == 'Surveyor' || Value == 'Team Leader' || Value == 'Director') {
        //  
        document.getElementById('btnDelete').disabled = true;
        document.getElementById('btnSave').disabled = true;
    }
    else {
        document.getElementById('btnDelete').disabled = false;
        document.getElementById('btnSave').disabled = false;
    }
}

function NewItem() {
    SelectedResultId = -1;
    UpdateTable(PickItemList);
    document.getElementById('btnDelete').className = 'button-thin hidden';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtValue').value = '';
}

function SaveItem() {

        DoSave = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'SaveNewItem',
           {
               Id: SelectedResultId,
               _enum: Enum,
               value: document.getElementById('txtValue').value
           },
           false
       );
        Settings_Load(Enum);
        document.getElementById('pnlItemControls').className = 'clientField float-left hidden';
        SelectedResultId = -1;
        UpdateTable(PickItemList);
}

function DeleteItem() {
    DoDelete = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'DeleteItem',
           {
               Id: SelectedResultId
           },
           false
       );

    document.getElementById('pnlItemControls').className = 'clientField float-left hidden';
    Settings_Load(Enum);
}