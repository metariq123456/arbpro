﻿/*
        NOTE: This js file may be loaded in as an alternate option for generating a work pack
        Due to this the file must contain the following functions:
            ToggleDsiplay(elementIndex)         - function to collapse or expand the relevent section
            LoadAttachments()                   - function to load user attachments
            AddAttachment()                     - function to enable the user to add an attachment to an email
            ShowAttachments()                   - function to display the attachment names in the grid
            SetSelected(attachment, selected)   - function to set which attachments are selected
*/

//REQUIRED SCOPE VARIABLES - These are used for the attachment functionality
var lm_FileNames;
var lm_Attachments;
var lm_SelectedAttachments;

var PdfQuoteId = -1;            //The parent Quote for the work
var IsForWorkPlan = false;      //Flag to denote if the generation is for a work plan
var ValidRisk = false;          //Flag to track if the selected site has a valid risk assessment
var ListSites = null;           //The list of sites that are associated with work record
var ObjPdfSettings = null;      //The settings object for the PDF
var IsForTpo = false;           // DGSAP-894. Checks if request is comming from TPO
var TpoQuoteId = -1;            // DGSAP-894. Quote Id only if comming from TPO
//state flags
var ArrIsPanelShown = [true, true];
var EmailSmtpObj = [];

//Initialisation function
function PDFGeneration_Load(quoteId, isTpo) {
    debugger;

    if (!isTpo) {
        document.getElementById('SmsContainerPanel').className = "hidden";
    }
    //Initialise and set scope variables
    lm_FileNames = new Array();
    lm_SelectedAttachments = new Array();
    TpoQuoteId = quoteId;
    PdfQuoteId = quoteId;
    IsForWorkPlan = false;
    IsForTpo = isTpo;

    //Blocking service call to delete any existing email attachments
    Ajax_CallService(
        'Services/Web/Report.asmx',
        'ClearTempEmailAttachments',
        {},
        false);

    //Get the list of sites for the quote
    GetSites(quoteId);
    //Get new PDF settings object
    //Note: work pack generation does not use the email settings object
    GetSettings();
    //Show the default selection
    if (!IsForTpo) {
        PopulateEmailPanel();
        ShowLetterTemplate();
    }
}

//Initialisation function
function PDFGeneration_LoadWorkPlan(workPlanId) {
    //debugger;
    //Initialise and set scope variables
    document.getElementById('SmsContainerPanel').className = "hidden";
    lm_FileNames = new Array();
    lm_SelectedAttachments = new Array();
    PdfQuoteId = workPlanId;
    IsForWorkPlan = true;

    //Blocking service call to delete any existing email attachments
    Ajax_CallService(
        'Services/Web/Report.asmx',
        'ClearTempEmailAttachments',
        {},
        false);

    PopulateEmailPanel();
    //Get the list of sites for the quote
    GetSiteForWorkPlan(workPlanId)
    //Get new PDF settings object
    //Note: work pack generation does not use the email settings object
    GetSettings();
    //Show the default selection
    ShowLetterTemplate();
}

//Async service call to get a PDF settings object
function GetSettings() {
    try {
        Ajax_CallService(
            'Services/Web/Report.asmx',
            'GetWorkPdfSetting',
            {},
            true,
            SetPdfSettings);
    } catch (err) {
        DisplayMessage('pnlPDFError', 'ERROR: Unable to get settings for report generation - ' + err.toString());
    }
}

//Async call back to set the PDF settings object
function SetPdfSettings(result) {
    ObjPdfSettings = result;
}

function SetEmailSettings() {
    ObjEmailSettings = result;
}

//Async service call to load the list of sites for the Quote
function GetSites(quoteId) {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
       'Services/Web/WorkFlow.asmx',
       'GetWorkSitesForQuote',
       {
           quoteId: quoteId
       },
       false,
       PopulatePDFPanel
   );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

function GetSiteForWorkPlan(workPlanId) {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        //  Find the required work plan
        var wps = $.grep(ObjWorkSummary.BookedWork, function (e) { return e.Id == workPlanId; });
        //  .BookedWork
        //
        var failed = false;
        if (wps && wps.length > 0) {
            var jsId = wps[0].JobSiteId;
            var wpSites = $.grep(ObjWorkSummary.JobSites, function (e) { return e.Id == jsId; });
            if (wpSites && wpSites.length > 0) {
                PopulatePDFPanel(wpSites);
            }
            else {
                failed = true;
            }
        } else {
            failed = true;
        }

        if (failed) {
            DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - Failed to load Job Site for work plan');
        }
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service -' + err.toString());
    }
}

//Async service call to check if there is a valid risk assessment for the selected site
//  NOTE: This function access a variable that is loaded in from the WorkPlanBooking.js file.
function ValidateRiskAssessment() {
    var errPanel = document.getElementById('pnlRiskValidation');
    //Check if the assessment is required
    if (document.getElementById('ddlPackType').selectedIndex > 0) {
        ValidRisk = false;
        //Get the site that is to be validated against
        var ddlSite = document.getElementById('ddlSelectedSite')
        var selectedSiteId = ddlSite.options[ddlSite.selectedIndex].value;
        //Check if a valid site option has been selected
        if (selectedSiteId > 0) {
            errPanel.innerHTML = GetWaitImageForPanel(errPanel.id, 'Validating Risk Assessment, Please wait.');
            try {
                errPanel.className = 'lmField';
                //Disable the  generation buttons
                document.getElementById('btnGenerateWorkPack').disabled = true;
                document.getElementById('btnSendEmail').disabled = true;
                Ajax_CallService(
                   'Services/Web/RiskAssessment.asmx',
                   'ValidateAssessmentForSite',
                   {
                       quoteId: ObjWorkSummary.QuoteId,
                       jobSiteId: selectedSiteId
                   },
                   true,
                   PopulateRiskAssessmentPanel
               );
            }
            catch (err) {
                DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
            }
        } else {
            DisplayMessage(errPanel.id, 'Please select a Site');
        }
    } else {
        ValidRisk = true;
        errPanel.className = 'lmField hidden';
    }
}

//Async service call to create a risk assessment for the work pack
//  NOTE: This function access a variable that is loaded in the WorkPlanBooking.js file
function CreateRiskAssessment() {
    var errPanel = document.getElementById('pnlRiskValidation');
    try {
        errPanel.innerHTML = GetWaitImageForPanel(errPanel.id, 'Creating Risk Assessment, Please wait.');
        //Get the site that the assessment is to be created for
        var ddlSite = document.getElementById('ddlSelectedSite')
        var selectedSiteId = ddlSite.options[ddlSite.selectedIndex].value;
        //  TODO: IMPLEMENT THE FOLLOWING WHEN RISK TEMPLATES ARE SUPPORTED
        ////Get the template id
        //var ddlTemplate = document.getElementById('ddlSelectedSite')
        //var selectedTemplateId = ddlTemplate.options[ddlTemplate.selectedIndex].value;

        Ajax_CallService(
                    'Services/Web/RiskAssessment.asmx',
                    'RiskCreateForJobSiteIdAndTemplate',
                    {
                        quoteId: ObjWorkSummary.QuoteId,
                        jobSiteId: selectedSiteId,
                        templateId: 1
                    },
                    true,
                    RiskAssessmentCreated);

    } catch (err) {
        DisplayMessage(errPanel.id, 'ERROR: Unable to communicate with ArbPro server - ' + err.toString());
    }
}

//Async callback to display the result of the creation operation
function RiskAssessmentCreated(result) {
    var panel = document.getElementById('pnlRiskValidation');
    if (result) {
        ValidRisk = true;
        //Enable the generation buttons and display a valid message
        document.getElementById('btnGenerateWorkPack').disabled = false;
        document.getElementById('btnSendEmail').disabled = false;
        DisplayMessage(panel.id, 'Risk Assessment for site created.');
    } else {
        ValidRisk = false;
        DisplayMessage(panel.id, 'Creation of Risk Assessment failed.');
    }
}

//Async callback to generate the html for the PDF settings panel
function PopulatePDFPanel(results) {
    var panel = document.getElementById('pnlPDFBody_0');
    if (!results || results.length == 0) {
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
    } else {
        try {
            ListSites = results;
            //Generate the html for the div
            var strHtml = '';
            //Create the site selection container
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlSelectedSite"> Work Site </label>';
            if (!IsForTpo) {
                strHtml += '<select id="ddlSelectedSite" class="lmFieldControl" onchange="ValidateRiskAssessment();" name="ddlSelectedSite">';
            } else {
                strHtml += '<select id="ddlSelectedSite" class="lmFieldControl" onchange="" name="ddlSelectedSite">';
            }
            if (ListSites.length > 1) {
                strHtml += '<option value="-1" selected="selected"> Select Work Site</option>';
                ListSites.forEach(function (js) {
                    strHtml += '<option value="' + js.Id + '">' + js.SiteString + '</option>';
                });
            } else {
                strHtml += '<option value="' + ListSites[0].Id + '" selected="selected">' + ListSites[0].SiteString + '</option>';
            }
            strHtml += '</select>';
            strHtml += '</div>';
            //Create the check box for including pictures - Defaults to checked
            strHtml += '<div class="lmField" style="width: 100%;">';
            strHtml += '<label class="lmFieldLabel" for="chkIncludePics"> Show Item Pictures</label>';
            strHtml += '<input type="checkbox" id="chkIncludePics" checked/>';
            if (!IsForTpo) {
                strHtml += '</div>';
                //Create the risk assessment validation container
                strHtml += '<div id="pnlRiskValidation" class="lmField" style="width: 100%;">';
                strHtml += '<br>';
                strHtml += '</div>';
                //Create the document selection panel

                strHtml += '<div class="lmField" style="width: 100%;">';
                strHtml += '<label class="lmFieldLabel" for="ddlPackType"> Document Type</label>';
                strHtml += '<select id="ddlPackType" class="lmFieldControl" onchange="ValidateRiskAssessment(); ShowLetterTemplate();" name="ddlPackType">';
                strHtml += '<option value="0" selected="selected">Work Schedule</option>';
                strHtml += '<option value="1">Risk Assessment</option>';
                strHtml += '<option value="2">All Items</option>';
                strHtml += '</select>';
                strHtml += '</div>';
                //Create the button section
                strHtml += '<div class="lmField" style="height:30px;width:100%;">';
            }
            strHtml += '<input id="btnGenerateWorkPack" class="button-thin float-right padded" type="submit" style="margin-right: 10px;" onclick="LoadPDF(); return false;" value="Generate Work Pack" name="btnGenerateWorkPack">';
            strHtml += '</div>';

            //Update the panel
            panel.innerHTML = strHtml;
            //Call the risk assessment validation process
            if (IsForTpo) {
                document.getElementById('btnGenerateWorkPack').value = "Generate Site Schedule";
            } else {
                ValidateRiskAssessment();
            }
        }
        catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display work pack settings - ' + err.toString());
        }
    }
}

//Async callback to populate a section of the PDF panel
function PopulateRiskAssessmentPanel(results) {
    var panel = document.getElementById('pnlRiskValidation');
    try {
        //Check if a risk for the site has been found
        if (results.length == 0) {
            //Enable the generation buttons and display a valid message
            ValidRisk = true;
            document.getElementById('btnGenerateWorkPack').disabled = false;
            document.getElementById('btnSendEmail').disabled = false;
            DisplayMessage(panel.id, 'Valid Risk Assessment for site found.');
        } else {
            //The service call has return a list of available templates to enable the user to create a risk assessment for the selected site.
            //Generate the html for the user to be able to create the risk assessment 
            var strHtml = '';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlRiskTemplate"> Risk Template </label>';
            strHtml += '<select id="ddlRiskTemplate" class="lmFieldControl" name="ddlRiskTemplate">';
            if (results.length > 1) {
                strHtml += '<option value="-1" selected="selected">Select Assessment Template</option>';
                results.forEach(function (tmp) {
                    strHtml += '<option value="' + tmp.Id + '">' + tmp.Name + '</option>';
                });
            } else {
                strHtml += '<option value="' + results[0].Id + '" selected="selected">' + results[0].Name + '</option>';
            }
            strHtml += '</select>';
            strHtml += '</div>';
            //Create the button section
            strHtml += '<div class="lmField" style="height:30px;width:100%;">';
            strHtml += '<input id="btnCreateRiskAssessment" class="button-thin float-right" type="submit" style="margin-right: 10px;" onclick="CreateRiskAssessment(); return false;" value="Create Assessment" name="btnCreateRiskAssessment">';
            strHtml += '</div>';

            panel.innerHTML = strHtml;
        }
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display Risk Assessment validation - ' + err.toString());
    }

}

//Async callback to generate the html fot the email settings panel
function PopulateEmailPanel() {
    var panel = document.getElementById('pnlPDFBody_1');

    try {
        //Generate the html for the div
        var strHtml = '';
        strHtml += '<div class="lmEdit" style="width:100%;">';
        strHtml += '<div id="divEmailOptions" style="width: 100%; display: block;">';
        strHtml += '<div class="lmField" style="width:100%;">';
        strHtml += '<label class="lmFieldLabel" for="txtEmailRecipient"> Recipient Address </label>';
        strHtml += '<input id="txtEmailRecipient" class="lmFieldControl" type="text" style="width:54%;" name="txtEmailRecipient">';
        strHtml += '<label class="lmFieldLabel" for="txtEmailSubject"> Subject </label>';
        strHtml += '<input id="txtEmailSubject" class="lmFieldControl" type="text" style="width:54%;" name="txtEmailSubject">';
        strHtml += '<textarea id="txtEmailText" class="lmFieldText" style="font-family:Helvetica;font-size:Small;" cols="20" rows="2" name="txtEmailText"></textarea>';
        strHtml += '</div>';
        strHtml += '<div class="lmField" style="width:100%;">';
        strHtml += '<div id="pnlAttachments" style="width:100%;">';
        strHtml += '<table id="grdAttachments" class="lmGrid" style="width:94%;">';
        strHtml += '<tbody>';
        strHtml += '</table>';
        strHtml += '</div>';
        strHtml += '<div style="width:100%;">';
        strHtml += '<input id="fuAttachment" type="file" name="fuAttachment" style="width:70%;">';
        strHtml += '<input id="btnUpload" class="button-thin float-right" type="submit" style="margin-right: 10px;" onclick="AddAttachment(); return false;" value="Attach" name="btnUpload">';
        strHtml += '</div>';
        strHtml += '</div>';
        strHtml += '<br>';
        strHtml += '<div class="lmField" style="height:30px;width:100%;">';
        strHtml += '<input id="btnSendEmail" class="button-thin float-right" type="submit" style="margin-right: 10px;" onclick="SendEmail(); return false;" value="Send Email" name="btnSendEmail">';
        strHtml += '</div>';
        strHtml += '</div>';
        strHtml += '</div>';
        strHtml += '</div>';

        //Update the panel
        panel.innerHTML = strHtml;
        //Colapse the panel
        ToggleDsiplay(1);
        //show any attachments that are in the session
        ShowAttachments();
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display Email settings - ' + err.toString());
    }
}

function GetSmtpSettings() {
    EmailSmtpObj = [];

    EmailSmtpObj = Ajax_CallService(
        '../Services/Web/Company.asmx',
        'GetEmailSettings',
        {},
        false
    );
}

function DisplayEmailMsg(result) {
    if (result) {
        GetSmtpSettings();
        if (EmailSmtpObj.UseSmtp) {
            message = 'Emails and Text messages (if applicable) have been generated and queued for dispatch.<br/> Please check your email client for confirmation';
        } else {
            message = 'Emails and Text messages (if applicable) have been generated and queued for dispatch.<br/> <a href="/Company.aspx"> See here - under the emails panel for all sent emails. </a>';
        }
    }
    else {
        message = 'Error: emails not sent';
    }

    Dialog_OpenPage(
   'Email',
   'Dialogs/Alert.aspx',
   'content',
   500,
   150,
   true,
   {
       'Ok': {
           text: 'Ok',
           class: 'ui-dialog-button',
           click: function () {
               $(this).dialog('close');
           }
       }
   },
   'Content/js/AlertBox.js?V=3.23.5',
   function () {
       AlertBox_Show(message);
   });
    //ShowGenerationTimer(false);
    //if (!result) {
    //    DisplayMessage('pnlPDFError', 'Error: Unable to display PDF, generation failed. Emails have not been sent');
    //} else {
    //    $('#pdfPreview').attr('src', 'Controls/PDFViewer.aspx?v=' + Math.floor((Math.random() * 1000000) + 1));
    //    alert('Emails have been generated and queued for dispatch.');
    //}
}

//Async callback to display a PDF
function DisplayPDF(result) {
    ShowGenerationTimer(false);
    if (!result) {
        DisplayMessage('pnlPDFError', 'Error: Unable to display PDF, generation failed.');
    } else {
        $('#pdfPreview').attr('src', 'Controls/PDFViewer.aspx?v=' + Math.floor((Math.random() * 1000000) + 1));
        HookPdfToDownload();
    }
}

//      Required! - Attach the click event handler for the Download button
function HookPdfToDownload() {
    $("#previewToolbar").css('visibility', 'visible');
    var openPdf = document.getElementById('btnDownload');

    openPdf.onclick = function () {
        window.open($("#pdfPreview").contents().find("#hiddenGeneratedPdfUrl").val());
    }
}

//REQUIRED! - Function to toggle the display of the required body panel
function ToggleDsiplay(elementIndex) {
    //NOTE: The element index is appended to the end of the ID in the aspx file This function depends on the nameing convention
    var bodyElement = document.getElementById('pnlPDFBody_' + elementIndex.toString());
    var toggleElement = document.getElementById('lblHeaderToggle_' + elementIndex.toString());
    if (ArrIsPanelShown[elementIndex]) {
        toggleElement.textContent = '+';
        $(bodyElement).slideUp();
    } else {
        toggleElement.textContent = '-';
        $(bodyElement).slideDown();
    }
    ArrIsPanelShown[elementIndex] = !ArrIsPanelShown[elementIndex]
}

function ToggleEmailOptionDisplay() {
    ToggleDsiplay(1);
    $('#Dialog_Content').animate({ scrollTop: "450px" });
}


//REQUIRED!
function LoadAttachments() {
    var ddlSite = document.getElementById('ddlSelectedSite')
    var selectedSiteId = ddlSite.options[ddlSite.selectedIndex].value;

    lm_Attachments = new Array();

    lm_FileNames.forEach(function (fn) {
        lm_Attachments.push({ id: lm_FileNames.indexOf(fn), type: -1, fileName: fn });
    });

    var Docs = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetTempDocuments',
        {
            ItemID: selectedSiteId
        },
        false);

    Docs.forEach(function (file) {
        lm_Attachments.push({ id: file.ID, type: file.DocumentTypeID, fileName: file.FileName });
    });

    ShowAttachments();
}
//REQUIRED!
function AddAttachment() {
    var ddlSite = document.getElementById('ddlSelectedSite')
    var selectedSiteId = ddlSite.options[ddlSite.selectedIndex].value;

    var Upload = document.getElementById('fuAttachment');
    if (Upload.files.length > 0) {
        if (Upload.value.indexOf('.exe') != -1
            || Upload.value.indexOf('.jar') != -1
            || Upload.value.indexOf('.bat') != -1
            || Upload.value.indexOf('.bash') != -1
            || Upload.value.indexOf('.msi') != -1
            || Upload.value.indexOf('.dll') != -1) {
            alert("Invalid file type");
            return false;
        }

        var file = Upload.files[0];

        var fr = new FileReader();
        fr.onload = function (event) {
            var result = event.target.result;
            var base64 = result.substring(result.indexOf("base64,") + 7);

            var Doc = Ajax_CallService(
                'Services/Web/Report.asmx',
                'AddTempDocument',
                {
                    ItemID: selectedSiteId,
                    Description: "Temporary Document For Email Attachment",
                    ContentType: file.type,
                    ByteString: base64,
                    FilePath: Upload.value
                },
                false);

            SetSelected({ id: Doc.Id, type: Doc.DocumentTypeID }, true);
            LoadAttachments();
            Upload.value = "";
        };
        fr.readAsDataURL(file);
    } else {
        alert('No File Selected');
    }
}
//REQUIRED!
function ShowAttachments() {

    if (!lm_Attachments)
        lm_Attachments = new Array();

    var grid = document.getElementById('grdAttachments');

    for (var i = grid.rows.length; i > 0; i--) {
        grid.deleteRow(i - 1);
    }

    lm_Attachments.forEach(function (attachment) {
        var newRow = grid.insertRow(-1);

        newRow.className = 'GridItem';

        var nameCell = newRow.insertCell(0);

        nameCell.innerHTML = attachment.fileName;

        switch (attachment.type) {
            case -1:
                newRow.className += ' GridItem_Report';
                break;
            case 0:
                newRow.className += ' GridItem_Global';
                break;
            case 1:
                newRow.className += ' GridItem_Client';
                break;
            case 2:
                newRow.className += ' GridItem_Temp';
                break;
        }

        var checkCell = newRow.insertCell(1);

        checkCell.style.textAlign = 'center';

        var cb = document.createElement('input');
        cb.type = 'checkbox';
        cb.id = 'attachmentCheckbox_' + attachment.type + '_' + attachment.id;
        cb.disabled = attachment.type == -1;
        checkCell.appendChild(cb);

        var checked = false;
        lm_SelectedAttachments.forEach(function (sel) {
            if (sel.id == attachment.id
                && sel.type == attachment.type)
                checked = true;
        });
        if (attachment.type == -1) {
            checked = true;
        }
        cb.checked = checked;

        cb.addEventListener('change', function () {
            var check = document.getElementById(cb.id);
            SetSelected({ id: attachment.id, type: attachment.type }, check.checked);
        }, false);
    });
}
//REQUIRED!
function SetSelected(attachment, selected) {
    if (selected) {
        lm_SelectedAttachments.push({ id: attachment.id, type: attachment.type });
    } else {
        if (lm_SelectedAttachments.length > 0) {
            var index = 0;
            for (var i = 0; i < lm_SelectedAttachments.length; i++) {
                if (lm_SelectedAttachments[i].id == attachment.id
                    && lm_SelectedAttachments[i].type == attachment.type) {
                    index = i;
                }
            }
            lm_SelectedAttachments.splice(index, 1);
        }
    }
}

//Function to show the letter templates for the emails
function ShowLetterTemplate() {
    var list = document.getElementById('ddlPackType');

    if (list.options[list.selectedIndex].value == 0) {
        lm_FileNames[0] = 'Work Schedule.pdf';
        lm_FileNames.splice(1, 1);
    } else if (list.options[list.selectedIndex].value == 1) {
        lm_FileNames[0] = 'Risk Assessment.pdf';
        lm_FileNames.splice(1, 1);
    } else {
        lm_FileNames[0] = 'Work Schedule.pdf';
        lm_FileNames[1] = 'Risk Assessment.pdf';
    }
    LoadAttachments();
}

//Async service call to generate the required PDF document
function LoadPDF() {
    debugger;
    //Display the wait timer
    ShowGenerationTimer(true);
    if (Validate(false)) {
        PopulatePdfSettings();
        console.log(ObjPdfSettings);
        console.log(IsForTpo);
        if (IsForTpo) {
            try {
                Ajax_CallService(
                    'Services/Web/Report.asmx',
                    'DownloadTpoWorkSchedule',
                    {
                        settings: ObjPdfSettings
                    },
                    true,
                    DisplayPDF);
            } catch (err) {
                DisplayMessage('pnlPDFError', 'Error: Unable to communicate with Arb Pro service - ', err.toString());
                ShowGenerationTimer(false);
            }
        } else {
            try {
                Ajax_CallService(
                    'Services/Web/Report.asmx',
                    'DownloadWorkPack',
                    {
                        settings: ObjPdfSettings
                    },
                    true,
                    DisplayPDF);
            } catch (err) {
                DisplayMessage('pnlPDFError', 'Error: Unable to communicate with Arb Pro service - ', err.toString());
                ShowGenerationTimer(false);
            }
        }
    } else {
        DisplayMessage('pnlPDFError', 'Please select a valid site and validate the risk assessment.');
        ShowGenerationTimer(false);
    }
}
//Async service call to send an invoice item by email. This function creates history items for the generated documents
function SendEmail() {
    try {
        //Display the wait timer
        ShowGenerationTimer(true);
        //Check if the due date has been set
        if (Validate(true)) {
            //Get the settings for the PDF
            PopulatePdfSettings();
            //Get the email value params
            var emailAddress = document.getElementById('txtEmailRecipient').value;
            var emailSubject = document.getElementById('txtEmailSubject').value;
            var emailBody = document.getElementById('txtEmailText').value;

            try {
                Ajax_CallService(
                    'Services/Web/Report.asmx',
                    'EmailWorkPack',
                    {
                        settings: ObjPdfSettings,
                        toAddress: emailAddress,
                        subject: emailSubject,
                        content: emailBody
                    },
                    true,
                    DisplayEmailMsg
                );

            } catch (err) {
                DisplayMessage('pnlPDFError', 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
                //Ensure that the processing wait timer is not being displayed
                ShowGenerationTimer(false);
            }
        }
        else {
            alert('Email not sent.\nPlease check a valid recipient and subject have been entered.');
        }
    }
    catch (err) {
        DisplayMessage('pnlPDFError', 'Error: Unable to send email - ' + err.toString());
        //Ensure that the processing wait timer is not being displayed
        ShowGenerationTimer(false);
    }
}


//Function to show / hide a wait timer by changing the CSS class of 2 elements
function ShowGenerationTimer(show) {
    if (show) {
        document.getElementById('pnlLmPdfView').className = 'hidden';
        document.getElementById('pnlLmProcessing').className = 'waitTimer';
    } else {
        document.getElementById('pnlLmProcessing').className = 'hidden';
        document.getElementById('pnlLmPdfView').className = 'lmPnlLrg';
    }
}

//Function to validate and set the due date of the invoice before updating the record and generating the documents
function Validate(isEmail) {
    var isValid = !(!ObjPdfSettings);
    try {
        var ddlSite = document.getElementById('ddlSelectedSite');
        var selectedSiteId = ddlSite.options[ddlSite.selectedIndex].value;
        if (!IsForTpo) {
            isValid = isValid && selectedSiteId > 0 && ValidRisk;
        } else {
            isValid = isValid && selectedSiteId > 0;
        }
        //Check if validation is for the email and check that the values are valid
        if (isEmail) {
            var validEmail = ValidateEmailAddress(document.getElementById('txtEmailRecipient').value);
            isValid = isValid && validEmail;
            isValid = isValid && document.getElementById('txtEmailSubject').value.length > 0;
        }
    }
    catch (err) {
        isValid = false;
    }
    return isValid;
}

//function to populate a PDF settings object based on the state of the form
function PopulatePdfSettings() {
    //Set the parent type for the Id so that the reports can be generated correctly
    if (IsForWorkPlan) {
        ObjPdfSettings.ReportParentType = 5
    } else {
        ObjPdfSettings.ReportParentType = 4
    }
    //Set the site Id for generation
    var ddlSite = document.getElementById('ddlSelectedSite')
    ObjPdfSettings.Id = ddlSite.options[ddlSite.selectedIndex].value;
    //Set the type of report to create
    if (!IsForTpo) {
        var workPackOption = document.getElementById('ddlPackType').selectedIndex;
    } else {
        var workPackOption = 0;
    }
    ObjPdfSettings.GenerationEnum = workPackOption;
    if (workPackOption > 1) {
        ObjPdfSettings.GenerationEnum = 3;
    }
    //Check if pictures are to be included
    ObjPdfSettings.ShowPictures = document.getElementById('chkIncludePics').checked;

    //Update the quote Id
    if (IsForTpo) {
        ObjPdfSettings.QuoteId = TpoQuoteId;
    } else {
        ObjPdfSettings.QuoteId = PdfQuoteId;
    }
}

function GenerateImageReport(PictureId, imgByte) {
    debugger;
    //Display the wait timer
    ShowGenerationTimer(true);
    document.getElementById('pnlPDFSettings').style.display = "none";

    try {
        Ajax_CallService(
            'Services/Web/Report.asmx',
            'PrintImage',
            {
                PictureId: PictureId,
                picByte: imgByte
            },
            true,
            DisplayPDF);
    } catch (err) {
        DisplayMessage('pnlPDFError', 'Error: Unable to communicate with Arb Pro service - ', err.toString());
        ShowGenerationTimer(false);
    }
}
