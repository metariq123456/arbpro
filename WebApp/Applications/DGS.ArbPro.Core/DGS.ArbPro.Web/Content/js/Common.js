﻿var Page_strAppRoot;
var Page_strUserEmail;

function Page_Initialise(strAppRootURL, strUserEmail, strClientDBName) {
    Page_strAppRoot = strAppRootURL;
    Page_strUserEmail = strUserEmail;

    Dialog_Initialise(strAppRootURL);

    // Initialise the root URL for the common jqx sources
    jqxRootUrl = strAppRootURL;

    $.ajaxSetup({ cache: false });

    Ajax_CallService(
        'Services/Web/Account.asmx',
        'StopLogoutTimer',
        { },
        false);
}

function Page_Exit() {
    Ajax_CallService(
        'Services/Web/Account.asmx',
        'StartLogoutTimer',
        { },
        false);
}


function GetURLParameter(name) {
    return decodeURI(
        (RegExp(name + '=' + '(.+?)(&|$)').exec(location.search) || [, null])[1]
    );
}

function navigateParams(strDestinationUrl, objParams) {
    var finalUrl = strDestinationUrl;
    objParams.forEach(function (p) {
        if (objParams.indexOf(p) === 0) {
            finalUrl += '?';
        } else {
            finalUrl += '&';
        }
        finalUrl = finalUrl + p.paramName;
        finalUrl = finalUrl + '=';
        finalUrl = finalUrl + p.paramValue;
    });

    window.location = finalUrl;
}

function Ajax_CallService(strServiceURL, strServiceEndPoint, objData, bAsync) {
    Ajax_CallService(strServiceURL, strServiceEndPoint, objData, bAsync, null);
}

function Ajax_CallService(strServiceURL, strServiceEndPoint, objData, bAsync, fSuccess) {
    var objReturn;

    $.ajax({
        type: 'POST',
        data: JSON.stringify(objData),
        url: Page_strAppRoot + strServiceURL + '/' + strServiceEndPoint,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: bAsync,
        success: function (result) {
            objReturn = result.d;
            if (fSuccess != null)
            {
                fSuccess(result.d);
            }
        },
        error: function (result) {
            debugger;
            var ServiceEndpoint = strServiceURL + '/' + strServiceEndPoint;
       }
    });

    return objReturn;
}

dateTimeReviver = function (key, value) {
    var a;
    if (typeof value === 'string') {
        a = /\/Date\((\d*)\)\//.exec(value);
        if (a) {
            return new Date(+a[1]);
        }
    }
    return value;
};

function AutoComplete(strTextBoxID, strListMethodName) {
    var List = Ajax_CallService(
        'Services/Web/Settings.asmx',
        strListMethodName,
        {},
        false);
    $('#' + strTextBoxID).autocomplete({ source: List });//.insertAfter($('#Dialog_Window').parent());
}

//Function to display a message in a named control
function DisplayMessage(panel, message, colour) {
    debugger;
    try {
        var font = '';
        var endFont = '</font>';
        if (!colour) {
            font = '<font color="black">';
        } else {
            font = '<font color="' + colour + '">';
        }

        var strHtml = '<table class="width-full white"><tr><td>'+ font + message + endFont +'</td></tr></table>';
        var details = document.getElementById(panel);
        if (!details) {
            details = document.getElementById('Content_' + panel);
        }
        details.innerHTML = strHtml;
    } catch (err) {
        alert('Sorry, there has been an error updating the page. Please use Crtl & F5 to refresh');
    }
}

function zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
}


function ShowAlert(success, type, operation) {
    var msg = 'Failed to save ' + type + '.';
    if (success > 0)
        msg = type + ' saved successfully.';

    Dialog_OpenPage(operation,
            'Dialogs/Alert.aspx?',
            'content',
            300,
            350,
            true,
            {
                'Close': {
                    text: 'Ok',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/AlertBox.js?V=3.23.5',
            function () {
                AlertBox_Show(msg);
            });
}

//Helper function to enable a styled alert box to be displayed with a simple message
function ShowAlertMessage(message) {
    Dialog_OpenPage('Information',
            'Dialogs/Alert.aspx?',
            'content',
            400,
            250,
            true,
            {
                'Close': {
                    text: 'Ok',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/AlertBox.js?V=3.23.5',
            function () {
                AlertBox_Show(message);
            });
}

//Helper function to create HTML for display of a wait timer image
function GetWaitImageForPanel(panelName, message) {
    var strHtml = '<div id="waitPnl' + panelName + '" class="center width-full float-left">';
    if(!message){
        strHtml += '<img id="waitImg' + panelName + '" class="padded" src="Images/load_Medium.gif">';
    }else{
        strHtml += '<table class="waitTimer"><tr><td>';
        strHtml += message;
        strHtml += '</td></tr><tr><td class="width-full">';
        strHtml += '<img id="waitImg' + panelName + '" class="padded" src="Images/load_Medium.gif">';
        strHtml += '</td></tr></table>';
    }
    strHtml += '</div>';
    return strHtml;
}

//Helper function to format a decimal value to format a decimal
/**
 * Number.prototype.format(n, x, s, c)
 * 
 * @param integer n: length of decimal
 * @param integer x: length of whole part
 * @param mixed   s: sections delimiter
 * @param mixed   c: decimal delimiter
 */
Number.prototype.format = function (n, x, s, c) {
    if (!n) n = 2;
    if (!x) x = 3;
    if (!s) s = ',';
    if (!c) c = '.';
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

//Helper function to create the html for the subtotal of a number of items
//SiteVatRates: An array of the VAT rte and the Values
//SiteCount: The number of Job sites for the item
//strHdrTotal: The total type ie Site / Quote / Invoice
function GetSiteTotalHtml(SiteVatRates, siteCount, strHdrTotal) {
    
    var SiteTotal = 0.0;
    //Calculate the total of all the items for the list
    SiteVatRates.forEach(function (vr) {
        SiteTotal += vr.value;
    });
    //Display the net value
    var strHtml = '<div style="text-align: right; margin-top: 5px; margin-right: 10px;">';
    strHtml += '<span>' + strHdrTotal + ' Subtotal: </span><span><b>' + CurrentCurrency + '' + SiteTotal.format() + '</b></span><br>';
    //
    SiteVatRates.forEach(function (vr) {
        strHtml += '<span>' + strHdrTotal + ' VAT @ ' + (vr.rate) + '%: </span><span><b>' + CurrentCurrency + '' + (vr.value * (vr.rate / 100)).format() + '</b></span><br>';
        SiteTotal += vr.value * (vr.rate / 100);
    });

    strHtml += '<span>' + strHdrTotal + ' Total inc. VAT: </span><span><b>' + CurrentCurrency + '' + SiteTotal.format() + '</b></span><br>';
    strHtml += '</div>';

    return strHtml;
}

//Helper function to display an items picture
// iStage : The stage the item is at 0 = JobItem : 1 = Quote, 2 = Work, 3 = Invoice,
// 4 - email footer, 5 - pdf header, 6 - pdf footer, 7 - director's signature
// iItemID: The id of the items record in cases 0 - 3, also used to distinguish between pdf header(1) and pdf footer(2)
// isCompany: determines whether the request is for a company image or a job item image
function OpenImage(iStage, iItemID, isCompany) {
    debugger;
    Dialog_OpenPage('Image Preview',
        'Dialogs/ImageViewer.aspx?Stage=' + iStage.toString() + '&ItemID=' + iItemID.toString(),
        'content',
        600,
        600,
        true,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/ImageViewer.js?V=3.23.5',
        function () { ImageViewer_Load(iStage, iItemID, isCompany); });
}


//Message to show a styled alert box to the user
function DisplayMessageDialog(title, message) {
    try {
        Dialog_OpenPage(title,
            'Dialogs/Alert.aspx?',
            'content',
            700,
            350,
            true,
            {
                'Close': {
                    text: 'Ok',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/AlertBox.js?V=3.23.5',
            function () {
                AlertBox_Show(message);
            });
    }
    catch (Err) {
        DisplayMessage('pnlErrorWork', 'Error: Unable to display alert dialog');
    }
}


/***** NOTES *****/
//      Async service call function to load notes from the service
//          NOTE: The callback function will be passed the results of the service call
function NotesList_Load(parentId, wfStage, callback) {
    Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetNotesList',
           {
               parentId: parentId,
               type:    wfStage
           },
           true,
           callback
       );
}

//      Function to construct the html for a given panel
function PopulateNotes(notesPanel, wfStage, parentId, results, callback) {
    /*      NOTE: wfStage - This is the stage for the note
                0   =>  Quote    
                1   =>  Work
                2   =>  Invoice
                3   =>  Credit 
    */
    try {
        //debugger;
        if (!results) {
            DisplayMessage(notesPanel.id, 'Error: Arb Pro service did not return valid data');
        } else {
            var strHtml = '<div class="width-full float-left white-light"><button type="button" onclick="ShowNotesDialog(-1,' + wfStage + ',' + parentId +',' + callback +');" class="button-thin float-right padded">Add Note</button></div>';
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid width-full">';
            if (results.length == 0) {
                strHtml += '<tr><td>No notes to display</td></tr>';
            } else {
                strHtml += '<tr class="white-light"><th>Entered By</th><th>Details</th><th>Created</th>'
                results.forEach(function (c) {
                    strHtml += '<tr onclick="ShowNotesDialog(' + c.Id + ',' + wfStage + ',' + parentId + ',' + callback + ');">';
                    strHtml += '<td style="padding-left: 10px;">' + c.EnteredBy + '</td><td  style="padding-left: 10px;">' + c.Text + '</td><td style="padding-left: 10px;">' + toDateStringFromJson(c.EnteredDate, '', 'dd/mm/yyyy') + '</td>';
                    strHtml += '</tr>';
                });
            }
            strHtml += '</table>';
            strHtml += '</div>';
            notesPanel.innerHTML = strHtml;
        }
    }
    catch (err) {
        DisplayMessage(notesPanel.id, 'Error: Unable to display notes - ' + err.toString());
    }
}

//      function to display the add/edit notes dialog
//          NOTE: The callback function must not take any parameters
function ShowNotesDialog(notesId, wfStage,recId, callback) {
    try {
        //TODO: Ensure Note wfStage flag consistancy
        /*      NOTE: wfStage - This is the stage for the note
                0   =>  Quote    
                1   =>  Work
                2   =>  Invoice
                3   =>  Credit 
        */
        Dialog_OpenPage('Note',
            'Dialogs/Notes.aspx?',
            'content',
            500,
            500,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        Note_Save();
                        //debugger;
                        if (callback) {
                            callback();
                        }
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/Notes.js?V=3.23.5',
            function () {
                Notes_Load(notesId, recId, wfStage);
            });
    }
    catch (Err) {
        DisplayMessage('pnlNotesDetail_' + panelIndex.toString(), 'Error: Unable to display notes dialog');
    }
}

function LoadGoogleApi(callback) {
    //debugger;
    $.getJSON('https://maps.googleapis.com/maps/api/js?key=AIzaSyAz6eEvWek3D1f_JsjA3nbd73A0apeOP7w&sensor=false');
    callback();
}

//Helper function to determin browser and browser version
//Available at http://stackoverflow.com/questions/5916900/detect-version-of-browser last accessed 07/01/2015
function get_browser() {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) { return 'Opera ' + tem[1]; }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
    return M[0];
}

function get_browser_version() {
    var ua = navigator.userAgent, tem, M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\bOPR\/(\d+)/)
        if (tem != null) { return 'Opera ' + tem[1]; }
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
    return M[1];
}

//  Function to display a dialog to enable a user to setup a new Job, without setting up a new Sales Enquiry
//
//          clientId: Int - The Id of the client the Job is for
//          callback: function - The function that will be called once the Job has been saved
function ShowNewJobDialog(clientId, callback) {
    try {
        Dialog_OpenPage('New Job',
            'Dialogs/JobDetails.aspx?',
            'content',
            500,
            500,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function() {
                        var id = NewJob_Save();
                        //debugger;
                        if (id > 0) {
                            if (callback) {
                                callback(id);
                            }
                            $(this).dialog('close');
                        }
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/NewJob.js?V=3.23.5',
            function() {
                NewJob_Load(clientId);
            });
    } catch (err) {
        alert('Error: Unable to display New Job dialog -' + err.toString());
    }
}

//  regex replacement function to replace a specified tag (replacementPattern) with another (replacedText) inside a text (bodytext).
function SmsTemplateConversion(bodyText, replacementPattern, replacedText) {
    return bodyText.replace(replacementPattern, replacedText);
}

// function that uses regex to match a specified tag
function SmsPatternPassing(bodyText, contactNameAndTitle, contactEmail, letterType, officeTel, officeMob, companyContact, companyName) {
    var i = 0;
    var date = new Date();
    var replacedPatterns = [/<SALUTATION>/m, /<EMAIL_TYPE>/m, /<EMAIL>/m, /<TIME>/m, /<OFFICE_TEL>/m, /<OFFICE_MOBILE>/m, /<COMPANY_CONTACT>/m, /<COMPANY_NAME>/m];
    var replacedText = [contactNameAndTitle, letterType, contactEmail, date.getHours() + ":" + date.getMinutes(), officeTel, officeMob, companyContact, companyName];

    var count = replacedPatterns.length;
    while(i<count) {
        bodyText = SmsTemplateConversion(bodyText, replacedPatterns[i], replacedText[i]);
        i++;
    };
    return bodyText;
}

// function that uses regex to match a specified tag
function MarketingPatternPassing(bodyText, contactNameAndTitle, categoryName, officeTel, officeMob, companyEmail, companyName) {
    var i = 0;
    var date = new Date();
    var replacedPatterns = [/<SALUTATION>/m, /<CATEGORY>/m, /<TIME>/m, /<OFFICE_TEL>/m, /<OFFICE_MOBILE>/m, /<COMPANY_EMAIL>/m, /<COMPANY_NAME>/m];
    var replacedText = [contactNameAndTitle, categoryName, date, officeTel, officeMob, companyEmail, companyName];

    var count = replacedPatterns.length;
    while (i < count) {
        bodyText = SmsTemplateConversion(bodyText, replacedPatterns[i], replacedText[i]);
        i++;
    };
    return bodyText;
}

function itemSort(objArray) {
    objArray.sort(function (a, b) {
        var c = parseInt(a.OrderId);
        var d = parseInt(b.OrderId);
        if (c == d) {
            return parseInt(a.JobItemId) - parseInt(b.JobItemId);
        }
        return c - d;
    });
}