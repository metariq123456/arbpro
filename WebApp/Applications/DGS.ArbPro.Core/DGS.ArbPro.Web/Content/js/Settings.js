﻿function Settings_Load() {
}


function OpenEnumDialog(SettingsName, PickItemNum) {
    Dialog_OpenPage(SettingsName,
        'Dialogs/SettingsConfig.aspx',
        'content',
        550,
        490,
        false,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function() {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/SettingsEnums.js?V=3.23.5',
        function() { Settings_Load(PickItemNum); });
};

//Addition to enable the display of the Aerial Rescue Plan dialog
function OpenResucePlanDialog() {
    Dialog_OpenPage(
        'Aerial Rescue Plans',
        'Dialogs/SettingsRescuePlan.aspx',
        'content',
        550,
        490,
        false,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function() {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/SettingsRescuePlan.js?V=3.23.5',
        function() { RescuePlans_Load(); });
};

function OpenToolsDialog(SettingsName) {
    Dialog_OpenPage(SettingsName,
        'Dialogs/ToolsSettings.aspx',
        'content',
        550,
        490,
        false,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function() {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/ToolsSettings.js?V=3.23.5',
        function() { Settings_Load(); });
};

function OpenTreesDialog(SettingsName) {
    Dialog_OpenPage(SettingsName,
        'Dialogs/TreeSettings.aspx',
        'content',
        550,
        490,
        false,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function() {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/TreeSettings.js?V=3.23.5',
        function() { Settings_Load(); });
};

function OpenWorkDialog(SettingsName) {
    Dialog_OpenPage(SettingsName,
        'Dialogs/WorkDescriptionSettings.aspx',
        'content',
        550,
        490,
        false,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function() {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/WorkDescriptionSettings.js?V=3.23.5',
        function() { Settings_Load(); });
};

function OpenANewkDialog(SettingsName) {
    Dialog_OpenPage(SettingsName,
        'Dialogs/WorkDescriptionSettings.aspx',
        'content',
        200,
        200,
        false,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/WorkDescriptionSettings.js?V=3.23.5',
        function () { Settings_Load(); });
};

function OpenCurrencyDialog(SettingsName)
{
    Dialog_OpenPage(SettingsName,
        'Dialogs/CurrencySettings.aspx',
        'content',
        200,
        200,
        false,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    if (SaveCurrencySettings()) {
                        $(this).dialog('close');
                    }
                }
            },
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
      'Content/js/CurrencySettings.js?V=3.23.5',
    function() { Settings_Load(); });
};

function OpenTemplatesDialog(TemplateName, ObjectType, TemplateType) {
    if (TemplateType === 3) {
        Dialog_OpenPage(
            TemplateName,
            'Dialogs/Templates.aspx',
            'content',
            800,
            650,
            false,
            {
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/SmsTemplates.js?V=3.23.5',
            function() { Templates_Load(ObjectType, TemplateType); });
    } else if (ObjectType === 3) {
        Dialog_OpenPage(
            TemplateName,
            'Dialogs/Templates.aspx',
            'content',
            800,
            650,
            false,
            {
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/MarketingTemplates.js?V=3.23.5',
            function () { Templates_Load(ObjectType, TemplateType); });
    } else {
        Dialog_OpenPage(
            TemplateName,
            'Dialogs/Templates.aspx',
            'content',
            800,
            650,
            false,
            {
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/Templates.js?V=3.23.5',
            function() { Templates_Load(ObjectType, TemplateType); });
    }
}

function OpenWorkCategoriesDialog() {
    Dialog_OpenPage('Work Categories',
        'Dialogs/MarketingWorkCategoriesSettings.aspx',
        'content',
        550,
        510,
        false,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/MarketingWorkCategoriesSettings.js?V=3.23.5',
        function () { Settings_Load(); });
}

function OpenGeneralMarketingDialog() {
    Dialog_OpenPage('General Settings',
        'Dialogs/MarketingGeneralSettings.aspx',
        'content',
        550,
        510,
        false,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    if (SaveGeneralSettings()) {
                        $(this).dialog('close');
                    }
                }
            },
            'Close': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/MarketingGeneralSettings.js?V=3.23.5',
        function () { Settings_Load(); });
}

//function AddButtons() {
    //var ListPanel = document.getElementById('Content_pnlSettingsTypeButtons');
    //var strHtml = '<label ID="Label1" class="enquiryHeader">Settings</label>';
    //strHtml += '<br />'
    //strHtml += '<button type="button" onclick="OpenDialog(\''+ "Tools" + '\');" class="button-settings">Tools</button>';
    //strHtml += '<button type="button" onclick="OpenDialog(\'' + "TreeNames" + '\');" class="button-settings">Tree Names</button>';
    //strHtml += '<button type="button" onclick="OpenDialog(\'' + "WorkDescription" + '\');" class="button-settings">Work Description</button>';
    ////strHtml += '<br />';
    ////strHtml += '<label ID="Label1" class="enquiryHeader">Item Types</label>';
    //strHtml += '<br />';
    //strHtml += '<button type="button" onclick="OpenDialog(\'Titles\', 0);" class="button-settings">Titles</button>';
    //strHtml += '<button type="button" onclick="OpenDialog(\'' + "Towns" + "," + 1 + '\');" class="button-settings">Towns</button>';
    //strHtml += '<button type="button" onclick="OpenDialog(\'' + "Counties" + "," + 2 + '\');" class="button-settings" width="250px">Counties</button>';
    //strHtml += '<br />';
    //strHtml += '<button type="button" onclick="OpenDialog(\'' + "Council" + "," + 3 + '\');" class="button-settings">Council</button>';
    //strHtml += '<button type="button" onclick="OpenDialog(\'' + "WasteTypes" + "," + 4 + '\');" class="button-settings">Waste Types</button>';
    //strHtml += '<button type="button" onclick="OpenDialog(\'' + "StaffTypes" + "," + 5 + '\');" class="button-settings">Staff Types</button>';
    //strHtml += '<br />';
    //strHtml += '<button type="button" onclick="OpenDialog(\'' + "Leadsource" + "," + 6 + '\');" class="button-settings">Lead Source</button>';
    ////Hospital?
    //ListPanel.innerHTML = strHtml;
//}