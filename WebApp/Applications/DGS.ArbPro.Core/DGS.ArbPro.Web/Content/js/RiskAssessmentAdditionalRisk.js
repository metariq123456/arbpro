﻿//Global variables
var additionalRisk;

/*
TODO: Remove from file once common routine has been validated
//Method to display a message in a named control
function DisplayMessage(panel, message) {
    var strHtml = '<table class="width-full white"><tr><td>' + message + '</td></tr></table>';
    var details = document.getElementById(panel);
    details.innerHTML = strHtml;
}
*/

//Load function -creates an async JQuery service call
function RiskAssessmentAdditionalRisk_Load() {
    try {
        var additionalRiskId = document.getElementById("hfAdditionalRiskId").value;
        Ajax_CallService(
                    '../Services/Web/RiskAssessment.asmx',
                    'OtherRiskGetForId',
                    {
                        additionalRiskId: additionalRiskId
                    },
                    true,
                    DisplayAdditionalRisk);
    }
    catch (err) {
        // 
        DisplayMessage("pnlSectionDetails", "Error loading additional risk details.");
    }
}

//Async JQuery callback to display an additional risk object
function DisplayAdditionalRisk(item) {
    if (!item) {
        DisplayMessage("pnlSectionDetails", "Error communicating with service.");
    } else {
        try{
            var panel = document.getElementById("pnlSectionDetails");
            var strHtml = '<div class="riskField">'
            strHtml += '<textarea id="txtAssessedRisk" placeholder="Assessed Risk" class="riskInput" rows="4" name="txtAssessedRisk">' + item.AssessedRisk + '</textarea>'
            strHtml += '</div>';
            strHtml += '<div class="riskField">'
            strHtml += '<textarea id="txtControlRequired" placeholder="Control Required" class="riskInput" rows="6" name="txtControlRequired">' + item.ControlRequired + '</textarea>'
            strHtml += '</div>';
            panel.innerHTML = strHtml;

            additionalRisk = item
        } catch (err) {
            DisplayMessage("pnlSectionDetails", "Error displaying additional risk.");
        }
    }
}



//Save function
function RiskAssessmentAdditionalRisk_Save() {
    try {
        if (additionalRisk.Id < 1) {
            additionalRisk.RiskAssessmentId = document.getElementById("hfRiskId").value;
        }
        additionalRisk.AssessedRisk = document.getElementById("txtAssessedRisk").value;
        additionalRisk.ControlRequired = document.getElementById("txtControlRequired").value;

        Ajax_CallService(
                    '../Services/Web/RiskAssessment.asmx',
                    'RiskOtherRiskUpdate',
                    {
                        item: additionalRisk
                    },
                    false);
    }
    catch (err) {
        // 
        DisplayMessage("pnlSectionDetails", "Error loading additional risk details.");
    }
}