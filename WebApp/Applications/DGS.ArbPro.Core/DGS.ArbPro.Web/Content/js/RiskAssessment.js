﻿var Sections_curTab = 0;
var SectionTab_Count = 0;
var Details_curTab = 1;
var Management_curTab = 1;
var StaffList;
var AssessorId;

function SwitchDetailsTab() {
    for (i = 1; i < 6; i++) {
        if (i == Details_curTab) {
            document.getElementById('tabDetails' + i.toString()).className = 'jobTab activeTab';
            document.getElementById('Content_grdTabDetails' + i.toString()).className = '';
        } else {
            document.getElementById('tabDetails' + i.toString()).className = 'jobTab';
            document.getElementById('Content_grdTabDetails' + i.toString()).className = 'hidden';
        }
    }
}

function SwitchSectionsTab() {
    for (i = 0; i < SectionTab_Count; i++) {
        if (i == Sections_curTab) {
            document.getElementById('tabSections' + i.toString()).className = 'clientTab activeTab';
            document.getElementById('grdTabSections' + i.toString()).className = '';
        } else {
            document.getElementById('tabSections' + i.toString()).className = 'clientTab';
            document.getElementById('grdTabSections' + i.toString()).className = 'hidden';
        }
    }
}

function SwitchManagementTab() {
    for (i = 1; i < 3; i++) {
        if (i == Management_curTab) {
            document.getElementById('tabManagement' + i.toString()).className = 'clientTab activeTab';
            document.getElementById('grdTabManagement' + i.toString()).className = '';
        } else {
            document.getElementById('tabManagement' + i.toString()).className = 'clientTab';
            document.getElementById('grdTabManagement' + i.toString()).className = 'hidden';
        }
    }
}

function ReloadSections() {
    var riskId = document.getElementById('hfRiskID').value;
    //Display the wait timmers for the sections as they reload
    //NOTE: The reload process could be enhanced by reloading only the section that has been edited
    for (i = 0; i < SectionTab_Count; i++) {
        document.getElementById('grdTabSections' + i.toString()).innerHTML = '<img id="SectionImage' + i + '" class="padded" src="Images/load_Medium.gif">';
    }
    // 
    //Load the risk assessment sections : Spawns aysnc ajax call
    LoadRiskAssessmentSections(riskId);
}

function ReloadAdditionalRisk() {
    var riskId = document.getElementById('hfRiskID').value;
    //Display the wait timer for the additional risks as they reload
    document.getElementById('pnlAdditionalRisks').innerHTML = '<img id="AdditionalRisksImage" class="padded" src="Images/load_Medium.gif">';
    // 
    //Load the risk assessment sections : Spawns aysnc ajax call
    LoadAdditionalRisks(riskId);

}


function LoadRiskAssessment() {
    debugger;
    var link;
    var WorkItmId = GetURLParameter('WorkItmId');
    if (WorkItmId == 'null') {
        link = document.getElementById('lnkReturnToJob');
    }
    var riskId = document.getElementById('hfRiskID').value;
    var quoteId = document.getElementById('hfQuoteID').value;
    if (quoteId == "") {
        quoteId = GetURLParameter('QuoteId');
    }
    var appointmentId = document.getElementById('hfAppId').value;
    var siteId = GetURLParameter('SiteId');
    var linkUrl = '/Job.aspx?JobID=' + document.getElementById('hfJobID').value;
    // 
    try {
        if (riskId > -1 && riskId !="") {
            //Load the assessment from the Id
            Ajax_CallService(
            'Services/Web/RiskAssessment.asmx',
            'RiskGetForId',
            {
                riskId: riskId
            },
            true,
            DisplayRiskAssessment);
        } else if (quoteId > -1 && siteId > -1) {
            linkUrl += '&QuoteID=' + quoteId.toString();
            //Load the risk assessment from the Quote record Id
            Ajax_CallService(
            'Services/Web/RiskAssessment.asmx',
            'RiskGetForQuoteAndJobSite',//RiskGetForQuoteId',
            {
                quoteId: quoteId,
                jobSiteId: siteId
            },
            true,
            DisplayRiskAssessment);
        } else if (appointmentId > -1) {
            //Load the risk assessment from the appointment record Id
            Ajax_CallService(
            'Services/Web/RiskAssessment.asmx',
            'RiskGetForAppointmentId',
            {
                appId: appointmentId
            },
            true,
            DisplayRiskAssessment);
        } else {
            DisplayMessage('pnlAssessmentDetailTabs', 'No risk assessment available.');
            DisplayMessage('pnlAssessmentSectionsTabs', 'No risk assessment available.');
            DisplayMessage('pnlAssessmentManagementTabs', 'No risk assessment available.');
        }
    }
    catch (err) {
        DisplayMessage('pnlAssessmentDetailTabs', 'Error: Unable to load risk assessment.');
        DisplayMessage('pnlAssessmentSectionsTabs', 'Error: Unable to load risk assessment.');
        DisplayMessage('pnlAssessmentManagementTabs', 'Error: Unable to load risk assessment.');
    }
    if (WorkItmId == 'null') {
        link.href = linkUrl;
    }
}

function LoadRiskAssessmentSections(riskId) {
    try {
        Ajax_CallService(
                    'Services/Web/RiskAssessment.asmx',
                    'RiskSectionsGetForRiskId',
                    {
                        riskId: riskId
                    },
                    true,
                    DisplaySections);
    }
    catch (err) {
        DisplayMessage('pnlAssessmentSectionsTabs', 'Error: Unable to load risk assessment.');
    }
}

function LoadAdditionalRisks(riskId) {
    try {
        Ajax_CallService(
                    'Services/Web/RiskAssessment.asmx',
                    'OtherRisksGetForRiskId',
                    {
                        riskId: riskId
                    },
                    true,
                    DisplayOtherRisks);
    }
    catch (err) {
        //  pnlAdditionalRisks
        DisplayMessage('pnlAdditionalRisks', 'Error: Unable to load additional risks.');
    }
}

function LoadClientRiskAssessments(clientId) {
    try {
        var quoteId = GetURLParameter('QuoteID');
        if (quoteId == 'null') {
            var quoteId = GetURLParameter('QuoteId');
        }
        Ajax_CallService(
                    'Services/Web/SearchService.asmx',
                    'GetRiskAssessmentListForQuote',
                    {
                        quoteId: quoteId
                    },
                    true,
                    DisplayClientRisks);
    }
    catch (err) {
        DisplayMessage("Content_grdTabDetails1", "Error loading client details.");
    }
}

function LoadClientDetails(clientId) {
    try {
        Ajax_CallService(
                    'Services/Web/SearchService.asmx',
                    'GetClientForId',
                    {
                        clientId: clientId
                    },
                    true,
                    DisplayClientDetails);
    }
    catch (err) {
        // 
        DisplayMessage("Content_grdTabDetails1", "Error loading client details.");
    }
}

function LoadSiteDetails(riskId) {
    try {
        Ajax_CallService(
                    'Services/Web/RiskAssessment.asmx',
                    'GetAssessmentSite',
                    {
                        riskId: riskId
                    },
                    true,
                    DisplaySiteDetails);
    }
    catch (err) {
        DisplayMessage("Content_grdTabDetails2", "Error loading site details.");
    }
}

function LoadStaffDetails(riskId) {
    try {
        Ajax_CallService(
                    'Services/Web/RiskAssessment.asmx',
                    'RiskStaffPresent',
                    {
                        riskId: riskId
                    },
                    true,
                    DisplayStaffDetails);
    }
    catch (err) {
        DisplayMessage("Content_grdTabDetails4", "Error loading staff details.");
    }
}

function LoadCreateDialog() {
    debugger;
    var quoteId = document.getElementById('hfQuoteID').value;
    if (quoteId == "") {
        quoteId = GetURLParameter('QuoteId');
    }
    Dialog_OpenPage(
       'Create Risk Assessment',
       'Dialogs/RiskAssessmentCreate.aspx?QuoteID=' + quoteId.toString(),
       'content',
       500,
       300,
       true,
       {
           'Save': {
               text: 'Create',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
                   var NewRisk = CreateRiskAssessment();

                   DisplayRiskAssessment(NewRisk);
               }
           },
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/RiskAssessmentCreate.js?V=3.23.5',
       function () {
           TemplateOptions_Load();
       });
}

function DisplaySiteDetails(clientSite) {
    try {
        //Create the Site Details content panel
        var emgPanel = document.getElementById("Content_grdTabDetails2");
        var strHtml = '<div Class="riskField">';
        strHtml += '<label Class="riskLabel">Site Reference</label><label Class="riskInput">';
        strHtml += clientSite.HouseNameNo;
        //strHtml += '</label></div><div Class="riskField">';
        //strHtml += '<label Class="riskLabel">Telephone</label><label Class="riskInput">';
        //strHtml += riskAssessment.SiteTelephone;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Address Ln 1</label><label Class="riskInput">';
        strHtml += clientSite.Address1;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Address Ln 2</label><label Class="riskInput">';
        strHtml += clientSite.Address2;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Town</label><label Class="riskInput">';
        strHtml += clientSite.Town;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">County</label><label Class="riskInput">';
        strHtml += clientSite.County;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Post Code</label><label Class="riskInput">';
        strHtml += clientSite.PostCode;
        strHtml += '</label></div>';
        strHtml += '</div>';
        emgPanel.innerHTML = strHtml;
    } catch (err) {
        DisplayMessage("Content_grdTabDetails2", "Error displaying site details.");
    }

}

function DisplayStaffDetails(staffPresent) {
    var panel = document.getElementById('Content_grdTabDetails4');
    if (!staffPresent) {
        DisplayMessage(panel.id, 'Error: ArbPro server communication error');
    } else {
        try {
            StaffList = GetStaffList();
            var strHtml = '<div class="width-full">';
            //Add button
            strHtml += '<div class="width-full float-left white-light">';
            strHtml += '<button type"button" onclick="SetAssessor();return false;" class="button-thin float-right padded">Save</button>';
            strHtml += '<select id="ddlAssessorSelect" class="float-right padded">';
            strHtml += '<option value="-2">Please Select</option>';
            if (StaffList.length > 0) {
                StaffList.forEach(function (s) {
                    strHtml += '<option value="' + s.Id + '">' + s.Name + '</option>';
                });
            }
            strHtml += '</select>';
            strHtml += '<label Class="riskLabel float-right padded">Assessor:</label>';
            strHtml += '</div>';//here
            strHtml += '<div class="width-full float-left white-light"><hr noshade color=black size=1><button type="button" onclick="ShowRiskStaffDialog(-1,-1);" class="button-thin float-right padded">Add Staff Member</button></div>';

            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid width-full">';
            strHtml += '<tr><th>Staff Name</th><th>Role</th></tr>';
            if (staffPresent.length == 0) {
                strHtml += '<tr class="center"><td colspan="2">No staff members to display</td></tr>';
            } else {
                staffPresent.forEach(function (tm) {
                    strHtml += '<tr onclick="ShowRiskStaffDialog(' + tm.Id.toString() + ',' + tm.StaffId.toString() + ')" ><td>';
                    strHtml += tm.StaffName;
                    strHtml += '</td><td>';
                    strHtml += tm.Task;
                    strHtml += '</td></tr>';
                });
            }
            strHtml += '</table>';
            strHtml += '</div>';
            strHtml += '</div>';
            panel.innerHTML = strHtml;
            if (AssessorId > -1) {
                document.getElementById('ddlAssessorSelect').value = AssessorId;
            }
        }
        catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display staff details');
        }
    }
}

function SetAssessor() {
    var riskId = document.getElementById('hfRiskID').value;

    //Grab the current risk assessment
    var riskAssessment = Ajax_CallService(
        'Services/Web/RiskAssessment.asmx',
        'RiskGetForId',
        {
            riskId: riskId
        },
        false);

    var ddl = document.getElementById("ddlAssessorSelect");
    var assessorId = ddl.options[ddl.selectedIndex].value;
    //Default value of 'Please select' has an Id of -2
    if (assessorId != -2) {
        //Refresh the dates
        riskAssessment.AssessmentDate = toDateFromJson(riskAssessment.AssessmentDate);
        riskAssessment.WorkDate = toDateFromJson(riskAssessment.WorkDate);
        riskAssessment.ReviewDate = toDateFromJson(riskAssessment.ReviewDate);
        riskAssessment.CompletionDate = toDateFromJson(riskAssessment.CompletionDate);
        riskAssessment.LastUpdatedDate = toDateFromJson(riskAssessment.LastUpdatedDate);
        riskAssessment.ReferralDate = toDateFromJson(riskAssessment.ReferralDate);

        //Insert the new assessor Id into the risk assesment object
        riskAssessment.AssessingStaffId = assessorId;

        //Update
        Ajax_CallService(
            'Services/Web/RiskAssessment.asmx',
            'RiskAssessmentUpdateDetails',
            {
                assessment: riskAssessment
            },
            true,
            setAssessorCallback);
    }
    else {
        setAssessorCallback(false)
    }
}
function setAssessorCallback(result) {
    var riskId = document.getElementById('hfRiskID').value;
    if (result == true) {
        var message = "The assessor has been saved";
    }
    else {
        var message = "The assessor has not been saved";
    }

    Dialog_OpenPage(
       'Assessor',
       'Dialogs/Alert.aspx',
       'content',
       500,
       150,
       true,
       {
           'Ok': {
               text: 'Ok',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/AlertBox.js?V=3.23.5',
       function () {
           AlertBox_Show(message);
       });
}
function GetStaffList() {
    try {
        var list = Ajax_CallService(
                       'Services/Web/Company.asmx',
                       'ListSurveyors',
                       {
                       },
                       false);
        return list;
    }
    catch (err) {
        DisplayMessage("Content_grdTabDetails4", "Error loading staff drop down list.");
    }
}

function DisplayRiskAssessment(riskAssessment) {
    //Check if the risk assessment is valid
    if (!riskAssessment || riskAssessment.Id < 1) {
        //Load the template dialog to enable the user to create a risk assessment
        LoadCreateDialog();
    } else {
        AssessorId = riskAssessment.AssessingStaffId;
        //The risk assessment is valid so populate the page.
        document.getElementById('hfRiskID').value = riskAssessment.Id.toString();
        document.getElementById('hfAppId').value = riskAssessment.AppointmentId.toString();
        //Load the list of client risk assessments : Spawns aysnc ajax call
        LoadClientRiskAssessments(riskAssessment.ClientId);
        //Load client details and site details : Spawns aysnc ajax calls
        LoadClientDetails(riskAssessment.ClientId);
        LoadSiteDetails(riskAssessment.Id);
        LoadStaffDetails(riskAssessment.Id);

        //Load the risk assessment sections : Spawns aysnc ajax call
        LoadRiskAssessmentSections(riskAssessment.Id);
        //Load the additional risks for this assessment : Spawns aysnc ajax call
        LoadAdditionalRisks(riskAssessment.Id);
        //Display the standard risk assessment components
        DisplayDetailsSection(riskAssessment);
        DisplayManagementSection(riskAssessment);
    }
}

function DisplayClientDetails(Client) {
    try {
        //Create the Client details content panel
        var clientPanel = document.getElementById("Content_grdTabDetails1");
        var strHtml = '<div Class="riskField">';
        strHtml += '<label Class="riskLabel">Type</label><label Class="riskInput">';
        strHtml += Client.ClientTypeString;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Name</label><label Class="riskInput">';
        strHtml += Client.Name;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Telephone</label><label Class="riskInput">';
        strHtml += Client.Telephone;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Email</label><label Class="riskInput">';
        strHtml += '<a href="mailto:' + Client.Email + '">' + Client.Email + '</a>' ;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Address</label><label Class="riskInput">';
        strHtml += Client.Address;
        strHtml += '</label></div>';
        strHtml += '</div>';
        clientPanel.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage("Content_grdTabDetails1", "Error displaying client details.");
    }
}

function DisplayDetailsSection(riskAssessment) {
    //Create the Emergency Details content panel
    try {
        var emgPanel = document.getElementById("Content_grdTabDetails3");
        strHtml = '<div Class="riskField">';
        strHtml += '<label Class="riskLabel">Mobile Signal</label><label Class="riskInput">';
        strHtml += riskAssessment.MobileSignal;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Aerial Rescue Plan</label><label Class="riskInput">';
        strHtml += riskAssessment.RescuePlan;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Aerial Rescue Nomaniee</label><label Class="riskInput">';
        strHtml += riskAssessment.RescueNomaniee;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">CS38 Certified</label><label Class="riskInput">';
        if (riskAssessment.CS38Certifed) {
            strHtml += 'Yes';
        } else {
            strHtml += 'No';
        }
        strHtml += '</label></div>';
        strHtml += '<div Class="riskField">';
        strHtml += '<label Class="riskLabel">Site Access</label><label Class="riskInput">';
        strHtml += riskAssessment.SiteAccess;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Nearest Hospital</label><label Class="riskInput">';
        strHtml += riskAssessment.NearestHospital;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Grid Reference</label><label Class="riskInput">';
        strHtml += riskAssessment.GridReference;
        strHtml += '</label></div>';
        strHtml += '<div Class="riskField">';
        strHtml += '<input type="button" class="button-thin float-right" value="Edit" onclick="DisplayEditDetails(' + riskAssessment.Id + '); return false;" />';
        strHtml += '</div></div>';
        emgPanel.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage('Content_grdTabDetails3', 'Error: Unable to display emergency details.');
    }

    //Create the Date Details Panel
    try {
        // 
        /*
            NOTE:
                Microsoft's ASP.NET AJAX encodes .NET DateTime values as a JSON string,
                where the content of the string is /Date(ticks)/ and where ticks
                represents milliseconds since epoch (UTC). So November 29, 1989, 4:55:30 AM, in UTC is 
                encoded as "\/Date(628318530718)\/".
        */
        var datePanel = document.getElementById("Content_grdTabDetails5");
        strHtml = '<div Class="riskField">';
        strHtml += '<label Class="riskLabel">Assessment Date</label><label Class="riskInput">';
        var ticks = parseInt(riskAssessment.AssessmentDate.replace("/Date(", "").replace(")/", ""), 10);
        if (ticks > 0) {
            var dateValue = new Date(ticks);
            strHtml += dateValue.toDateString();
        } else {
            strHtml += 'Pending';
        }
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Work Date</label><label Class="riskInput">';
        ticks = parseInt(riskAssessment.WorkDate.replace("/Date(", "").replace(")/", ""), 10);
        if (ticks > 0) {
            var dateValue = new Date(ticks);
            strHtml += dateValue.toDateString();
        } else {
            strHtml += riskAssessment.Completed ? 'N/A' : 'Pending';
        }
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Review Date</label><label Class="riskInput">';
        ticks = parseInt(riskAssessment.ReviewDate.replace("/Date(", "").replace(")/", ""), 10);
        if (ticks > 0) {
            var dateValue = new Date(ticks);
            strHtml += dateValue.toDateString();
        } else {
            if (riskAssessment.ReferralRequired) {
                strHtml += (riskAssessment.Completed ? 'Completed' : 'Pending');
            } else {
                strHtml += 'N/A';
            }
        }
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Completion Date</label><label Class="riskInput">';
        // 
        ticks = parseInt(riskAssessment.CompletionDate.replace("/Date(", "").replace(")/", ""), 10);
        if (ticks > 0) {
            var dateValue = new Date(ticks);
            strHtml += dateValue.toDateString();
        } else {
            strHtml += (riskAssessment.Completed ? 'Completed' : 'Pending');
        }
        strHtml += '</label></div>';
        strHtml += '</div>';
        datePanel.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage('Content_grdTabDetails5', 'Error: Unable to display date details.');
    }
    SwitchDetailsTab();
}

function DisplayOtherRisks(otherRisks) {
    try {
        // 
        var riskCount = otherRisks.length;
        var pnlSections = document.getElementById('pnlAdditionalRisks');
        var strHtml = '';
        if (!riskCount || riskCount < 1) {
            strHtml += '<label Class="riskLabel">No additional risks to display</label>';
        } else {
            strHtml += '<div class="width-full centerPanel"><table  class=" width-full riskCriteriaGrid">';
            if (riskCount > 0) {
                strHtml += '<tr><th>Assessed Risk</th><th>Control Measures</th></tr>';
                var currentCriteria = otherRisks[0];
                for (j = 0; j < riskCount; j++) {
                    currentCriteria = otherRisks[j];
                    strHtml += '<tr class="selected" onclick="DislpayEditAdditionalRisk(' + currentCriteria.Id + ');"><td>';
                    strHtml += currentCriteria.AssessedRisk;
                    strHtml += '</td>';
                    strHtml += '<td>';
                    strHtml += currentCriteria.ControlRequired;
                    strHtml += '</td>';
                    strHtml += '</tr>';
                }
            } else {
                strHtml += '<tr><td>No additional risks to display</td></tr>';
            }
            strHtml += '</table></div>';
        }
        pnlSections.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage('pnlAdditionalRisks', 'Error: Unable to display additional risks.');
    }
}

function DisplaySections(AssessmentSections) {
    try {
        // 
        var sectionCount = AssessmentSections.length;
        var pnlSections = document.getElementById('pnlAssessmentSectionsTabs');
        var strHtml = '';
        if (!sectionCount || sectionCount < 1) {
            strHtml += '<div id="pnlTabSectionHeaders" class="center">';
            strHtml += '<label Class="riskLabel">There are no sections to display</label></div>';
        } else {
            strHtml += '<div id="pnlTabSectionHeaders" class="riskTabBar">';
            var currentSection = AssessmentSections[0];
            for (i = 0; i < sectionCount; i++) {
                currentSection = AssessmentSections[i];
                strHtml += '<a id="tabSections' + i.toString() + '" class="clientTab activeTab" href="javascript:Sections_curTab = ' + i.toString() + '; SwitchSectionsTab();">';
                strHtml += currentSection.Title;
                strHtml += '</a>';
            }
            strHtml += '</div>';
            var criteriaCount = 0;
            for (i = 0; i < sectionCount; i++) {
                currentSection = AssessmentSections[i];
                criteriaCount = currentSection.Criteria.length;
                //Set up the panel for this section
                strHtml += '<div id="grdTabSections' + i.toString() + '"class="width-full"><div ><div class="riskField">';
                strHtml += '<label class="riskLabel">Description</label><label class="riskLongInput">';
                strHtml += currentSection.Description;
                strHtml += '</label></div><div Class="riskField">';
                strHtml += '<label class="riskLabel">Reference</label><label class="riskInput">';
                strHtml += currentSection.ReferenceDoc;
                strHtml += '</label></div><div Class="riskField">';
                strHtml += '<label class="riskLabel">Chapter</label><label class="riskInput">';
                strHtml += currentSection.Chapter;
                strHtml += '</label></div></div>';
                strHtml += '<div class="width-full centerPanel"><table  class="riskCriteriaGrid">';
                if (criteriaCount > 0) {
                    var currentCriteria = currentSection.Criteria[0];
                    strHtml += '<tr><th>Criteria</th><th>Ref</th><th>Phase</th><th>Control Required</th><th>On Work Day</th><th class="criteriaExtended">Comments</th></tr>';
                    for (j = 0; j < criteriaCount; j++) {
                        currentCriteria = currentSection.Criteria[j];
                        strHtml += '<tr><td class="riskDetail">';
                        strHtml += currentCriteria.Criterion;
                        strHtml += '</td>';
                        strHtml += '<td class="riskCriteria">';
                        strHtml += currentCriteria.ChapterRef;
                        strHtml += '</td>';
                        strHtml += '<td class="riskCriteria">';
                        strHtml += currentCriteria.AssessmentPhase;
                        strHtml += '</td>';
                        strHtml += '<td class="riskCriteria">';
                        if (currentCriteria.AssessmentPhase > 1) {
                            strHtml += 'N/A';
                        } else {
                            strHtml += getControlStatus(currentCriteria.ControlRequired);
                        }
                        strHtml += '</td>';
                        strHtml += '<td class="riskCriteria">';
                        strHtml += getControlStatus(currentCriteria.OnDayControlRequired);
                        strHtml += '</td>';
                        if (j == 0) {
                            strHtml += '<td  class="riskDetail" rowspan="' + criteriaCount.toString() + '">';
                            strHtml += currentSection.Comments;
                            strHtml += '</td>';
                        }
                        strHtml += '</tr>';
                    }
                } else {
                    strHtml += '<tr><td>No criteria assigned to section</td></tr>';
                }
                strHtml += '</table><table class="riskEditSection">';
                strHtml += '<tr><td><input type="button" class="button-thin float-right" value="Edit Assessment" onclick="DisplayEditSection(' + currentSection.Id + '); return false;" /></td></tr>';
                strHtml += '</table></div></div>';
            }
        }
        // 
        SectionTab_Count = sectionCount;
        pnlSections.innerHTML = strHtml;

        SwitchSectionsTab();
    }
    catch (err) {
        DisplayMessage('pnlAssessmentSectionsTabs', 'Error: Unable to display assessment sections.');
    }
}

function DisplayManagementSection(riskAssessment) {
    try {
        //Create the tab headers
        var strHtml = '<div id="pnlTabDetailsHeaders" class="riskTabBar">';
        strHtml += '<a id="tabManagement1" class="clientTab activeTab" href="javascript:Management_curTab = 1; SwitchManagementTab();">Status</a>';
        strHtml += '<a id="tabManagement2" class="clientTab" href="javascript:Management_curTab = 2; SwitchManagementTab();">Referral</a>';
        strHtml += '</div>';
        //Create the Client details content panel
        strHtml += '<div id="grdTabManagement1" class="width-full">';
        strHtml += '<div Class="riskField">';
        strHtml += '<label Class="riskLabel">Status</label><label Class="riskInput">';
        if (riskAssessment.Completed) {
            strHtml += 'Completed';
        } else if (riskAssessment.ReferralCompleted) {
            strHtml += 'Referral Completed';
        } else if (riskAssessment.ReferralRequired) {
            strHtml += 'Referral Required';
        } else {
            strHtml += 'Pending';
        }
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Last Updated</label><label Class="riskInput">';
        var ticks = parseInt(riskAssessment.LastUpdatedDate.replace("/Date(", "").replace(")/", ""), 10);
        if (ticks > 0) {
            var dateValue = new Date(ticks);
            strHtml += dateValue.toDateString();
        }
        else {
            strHtml += 'N/A';
        }
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Last Updated By</label><label Class="riskInput">';
        strHtml += riskAssessment.LastUpdatedBy;
        strHtml += '</label></div><div Class="riskField">';
        strHtml += '<label Class="riskLabel">Comments</label><label Class="riskInput">';
        strHtml += riskAssessment.ManagementComments;
        strHtml += '</label></div>';
        strHtml += '<div Class="riskField">';
        strHtml += '<input type="button" class="button-thin float-right" value="Edit" onclick="DisplayEditManagement(' + riskAssessment.Id + '); return false;" />';
        strHtml += '</div></div>';

        riskId = riskAssessment.Id;
        var referral = Ajax_CallService(
            'Services/Web/RiskAssessment.asmx',
            'getReferralForRiskId',
            {
                RiskId: riskId
            },
            false);
        strHtml += '<div id="grdTabManagement2" class="width-full hidden">';
        if (referral.Id != -1) {
            //Create the Referral content panel
            strHtml += '<div Class="riskField">';
            strHtml += '<label Class="riskLabel">Date of referral</label><label Class="riskInput">';
            var date = toDateFromJson(referral.ReferralDate).format("dd/MMM/yyyy");
            if (date && date != "NaN") {
                strHtml += date;
            } else {
                strHtml += "N/A";
            }
            //var ticks = parseInt(referral.ReferralDate.replace("/Date(", "").replace(")/", ""), 10);
            //if (ticks > 0) {
            //    var dateValue = new Date(ticks);
            //    strHtml += formatDateString(dateValue, 'd/m/YYYY');
            //}
            //else {
            //    strHtml += 'N/A';
            //}
            strHtml += '</label></div><div Class="riskField">';
            strHtml += '<label Class="riskLabel">Team Leader Spoken With</label><label Class="riskInput">';
            if (referral.TeamLeaderSpokenWith) {
                strHtml += 'Yes';
            } else {
                strHtml += 'No';
            }
            strHtml += '</label></div><div Class="riskField">';
            strHtml += '<label Class="riskLabel">Referral Comments</label><label Class="riskInput">';
            strHtml += referral.ReferralComments;
            strHtml += '</label></div><div Class="riskField">';
            strHtml += '<label Class="riskLabel">Additional Actions</label><label Class="riskInput">';
            strHtml += referral.AdditionalActions;
            strHtml += '</label></div>';
            strHtml += '<div Class="riskField">';
            strHtml += '<input type="button" class="button-thin float-right" value="Edit" onclick="DisplayEditManagement(' + riskAssessment.Id + '); return false;" />';
            strHtml += '</div></div>';
        }
        else {
            //Create the Referral content panel
            strHtml += '<div Class="riskField">';
            strHtml += '<label Class="riskLabel center"><b>No Risk Referral Found</b></label><br>';
            strHtml += '<label Class="riskLabel">Date of referral</label><label Class="riskInput">';
            strHtml += 'N/A';
            strHtml += '</label></div><div Class="riskField">';
            strHtml += '<label Class="riskLabel">Team Leader Spoken With</label><label Class="riskInput">';
            strHtml += 'Unknown';
            strHtml += '</label></div><div Class="riskField">';
            strHtml += '<label Class="riskLabel">Referral Comments</label><label Class="riskInput">';
            strHtml += 'N/A'
            strHtml += '</label></div><div Class="riskField">';
            strHtml += '<label Class="riskLabel">Additional Actions</label><label Class="riskInput">';
            strHtml += 'N/A'
            strHtml += '</label></div>';
            strHtml += '<div Class="riskField">';
            strHtml += '<input type="button" class="button-thin float-right" value="Edit" onclick="DisplayEditManagement(' + riskAssessment.Id + '); return false;" />';
            strHtml += '</div></div>';
        }
        //Set the html to the correct panel
        var pnlManagement = document.getElementById('pnlAssessmentManagementTabs');
        pnlManagement.innerHTML = strHtml;

        SwitchManagementTab();
    }
    catch (err) {
        DisplayMessage('pnlAssessmentManagementTabs', 'Error: Unable to display risk management section.' + err);
    }
}

function DisplayClientRisks(assessments) {
    //pnlClientRiskAssessmentList
    var riskCount = assessments.length;
    var pnlSections = document.getElementById('pnlClientRiskAssessmentList');
    try {
        if (!riskCount || riskCount < 1) {
            DisplayMessage('pnlClientRiskAssessmentList', 'There are no results to display');
        } else {
            var riskId = document.getElementById('hfRiskID').value;
            var quoteId = GetURLParameter('QuoteID');
            if (quoteId == 'null') {
                quoteId = GetURLParameter('QuoteId');
            }
            var strHtml = '<table class="searchGrid width-full">';
            for (j = 0; j < riskCount; j++) {
                strHtml += '<tr onclick="window.location = \'/RiskAssessment.aspx?SiteId=' + assessments[j].JobSiteId + '&RiskId=' + assessments[j].Id + '&JobId=' + assessments[j].ParentId + '&QuoteID=' + assessments[j].QuoteId + '\';">';
                strHtml += '<td class="' + (assessments[j].Id == riskId ? 'selected' : '') + '">';
                strHtml += '<table class="width-full"><tr><td>';
                strHtml += assessments[j].SideBarDetail;
                strHtml += '</td></tr></table>';
                strHtml += '</td></tr>';
            }
            strHtml += '</table>';
            pnlSections.innerHTML = strHtml;
        }
    } catch (err) {
        DisplayMessage('pnlClientRiskAssessmentList', 'Error: Unable to display risk assessments for the client.');
    }
}

function DisplayEditSection(sectionId) {
    // 
    Dialog_OpenPage(
       'Edit Section',
       'Dialogs/RiskAssessmentSection.aspx?SectionID=' + sectionId.toString(),
       'content',
       800,
       450,
       true,
       {
           'Save': {
               text: 'Save',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
                   var NewID = RiskAssessmentSection_Save();
                   ReloadSections();
               }
           },
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/RiskAssessmentSection.js?V=3.23.5',
       function () {
           RiskAssessmentSection_Load();
       });
}

function ShowRiskStaffDialog(objId, staffId) {
    var riskId = document.getElementById('hfRiskID').value;
    if (objId > 0) {
        Dialog_OpenPage(
           'Staff Present',
           'Dialogs/RiskStaffMember.aspx',
           'content',
           600,
           500,
           true,
           {
               'Remove': {
                   text: 'Remove',
                   class: 'ui-dialog-button',
                   click: function () {
                       RemoveStaffMember();
                       LoadStaffDetails(riskId);
                       $(this).dialog('close');
                   }
               },

               'Save': {
                   text: 'Save',
                   class: 'ui-dialog-button',
                   click: function () {
                       Save_StaffMember();
                       LoadStaffDetails(riskId);
                       $(this).dialog('close');
                   }
               },
               'Close': {
                   text: 'Close',
                   class: 'ui-dialog-button',
                   click: function () {
                       LoadStaffDetails(riskId);
                       $(this).dialog('close');
                   }
               }
           },
           'Content/js/RiskStaffMember.js?V=3.23.5',
           function () {
               Load_StaffMemberDialog(staffId, riskId, objId);
           });
    }
    else {
        Dialog_OpenPage(
   'Staff Present',
   'Dialogs/RiskStaffMember.aspx',
   'content',
   600,
   500,
   true,
   {
       'Save': {
           text: 'Save',
           class: 'ui-dialog-button',
           click: function () {
               Save_StaffMember();
               LoadStaffDetails(riskId);
               $(this).dialog('close');
           }
       },
       'Close': {
           text: 'Close',
           class: 'ui-dialog-button',
           click: function () {
               LoadStaffDetails(riskId);
               $(this).dialog('close');
           }
       }
   },
   'Content/js/RiskStaffMember.js?V=3.23.5',
   function () {
       Load_StaffMemberDialog(staffId, riskId, objId);
   });
    }
}

function DisplayEditManagement(riskId) {
    var isBookDatesPage = GetURLParameter('WorkId');
    var linkUrl;
    if (isBookDatesPage == 'null') {
        linkUrl = '?JobID=' + document.getElementById('hfJobID').value;
        linkUrl += '&QuoteID=' + document.getElementById('hfQuoteID').value;
        linkUrl += '&RiskId=' + riskId;
    }
    else {
        linkUrl = '?WorkId=' + GetURLParameter('WorkId')
        linkUrl += '&WorkItmId=' + GetURLParameter('WorkItmId')
        linkUrl += '&QuoteId=' + GetURLParameter('QuoteId')
        linkUrl += '&SiteId=' + GetURLParameter('SiteId')
        linkUrl += '&JobId=' + GetURLParameter('JobId')
        linkUrl += '&RiskId=' + riskId;
        linkUrl += '&CurTab=6';
        linkUrl += '&Save=3';
        var WorkPlanId = GetURLParameter('WorkPlanId');
        if (WorkPlanId != 'null') {
            linkUrl += '&WorkPlanId=' + WorkPlanId;
        }
    }
 
    if (Management_curTab == 1) {
        Dialog_OpenPage(
           'Edit Risk Management',
           'Dialogs/RiskAssessmentManagement.aspx?RiskID=' + riskId.toString(),
           'content',
           260,
           300,
           true,
           {
               'Save': {
                   text: 'Save',
                   class: 'ui-dialog-button',
                   click: function () {
                       $(this).dialog('close');
                       RiskAssessmentManagement_Save();
                       window.location.search = linkUrl;
                   }
               },
               'Close': {
                   text: 'Close',
                   class: 'ui-dialog-button',
                   click: function () {
                       $(this).dialog('close');
                   }
               }
           },
           'Content/js/RiskAssessmentManagement.js?V=3.23.5',
           function () {
               RiskAssessmentManagement_Load(Management_curTab);
           });
    }
    else {
        Dialog_OpenPage(
       'Edit Risk Management',
       'Dialogs/RiskAssessmentManagement.aspx?RiskID=' + riskId.toString(),
       'content',
       300,
       400,
       true,
       {
           'Save': {
               text: 'Save',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
                   RiskAssessmentManagement_Save();
                   window.location.search = linkUrl;
               }
           },
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/RiskAssessmentManagement.js?V=3.23.5',
       function () {
           RiskAssessmentManagement_Load(Management_curTab);
       });
    }
}

function DisplayEditDetails(riskId) {
    var isBookDatesPage = GetURLParameter('WorkId');
    var linkUrl ='';
    if (isBookDatesPage == 'null') {
        linkUrl = '?JobID=' + document.getElementById('hfJobID').value;
        linkUrl += '&QuoteID=' + document.getElementById('hfQuoteID').value;
        linkUrl += '&RiskId=' + riskId;
    }
    else {
        linkUrl = '?WorkId=' + GetURLParameter('WorkId')
        linkUrl += '&WorkItmId=' + GetURLParameter('WorkItmId')
        linkUrl += '&QuoteId=' + GetURLParameter('QuoteId')
        linkUrl += '&SiteId=' + GetURLParameter('SiteId')
        linkUrl += '&JobId=' + GetURLParameter('JobId')
        linkUrl += '&RiskId=' + riskId;
        linkUrl += '&CurTab=6';
        linkUrl += '&Save=3';
        var WorkPlanId = GetURLParameter('WorkPlanId');
        if (WorkPlanId != 'null') {
            linkUrl += '&WorkPlanId=' + WorkPlanId;
        }
    }
    Dialog_OpenPage(
       'Assessment Details',
       'Dialogs/RiskAssessmentDetails.aspx?RiskID=' + riskId.toString(),
       'content',
       265,
       300,
       true,
       {
           'Save': {
               text: 'Save',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
                   RiskAssessmentDetails_Save();
                   window.location.search = linkUrl;
               }
           },
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/RiskAssessmentDetails.js?V=3.23.5',
       function () {
           RiskAssessmentDetails_Load();
       });
}

function DislpayEditAdditionalRisk(additionalRiskId) {

    var riskId = document.getElementById('hfRiskID').value;
    Dialog_OpenPage(
       additionalRiskId == -1 ? 'Add Additional Risk' : 'Edit Additional Risk',
       'Dialogs/RiskAssessmentAdditionalRisk.aspx?RiskID=' + riskId.toString() + '&ItemID=' + additionalRiskId.toString(),
       'content',
       280,
       380,
       true,
       {
           'Save': {
               text: 'Save',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
                   RiskAssessmentAdditionalRisk_Save();
                   ReloadAdditionalRisk();
               }
           },
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/RiskAssessmentAdditionalRisk.js?V=3.23.5',
       function () {
           RiskAssessmentAdditionalRisk_Load();
       });
}

function AddAdditionalRisk() {
    DislpayEditAdditionalRisk(-1);
}

function getControlStatus(enumValue) {
    switch (enumValue) {
        case 1:
            return "No";
            break;
        case 2:
            return "Yes";
            break;
        default:
            return "Not Assessed";
            break;
    }
}

function DisplayRiskAssessmentPDF() {
   Dialog_OpenPage(
       'Print Risk Assessment',
       'Dialogs/PrintRiskAssessment.aspx',
       'content',
       800,
       600,
       true,
       {
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/RiskAssessmentDetails.js?V=3.23.5',
       function () {
           LoadPDF();
       });
}
