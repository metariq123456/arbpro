﻿var CurList;
var SelectedResultId;
var SelectedObj;
var CurMessageType;
var CurObjectType;

// loads marketing templates
function Templates_Load(objType, msgType) {
    SelectedResultId = -1;
    CurObjectType = objType;
    CurMessageType = msgType;
    GetTemplateList();
    document.getElementById('pnlType').className = 'hidden';
    document.getElementById('pnlInvoicePaymentInfo').className = 'hidden';
}

// gets the results from the ajax call
function ServiceCallback(results) {
    CurList = results;
    UpdateTable();
}

// updates the table with the templates
function UpdateTable() {
    if (CurList.length < 1) {
        document.getElementById('pnlItemControls').className = 'hidden';
        document.getElementById('pnlNoResults').className = '';
        document.getElementById('pnlTableContainer').innerHTML = '';
    } else {
        var listPanel = document.getElementById('pnlTableContainer');
        var strHtml = '<table class="searchGrid width-full white">';

        for (var i = 0; i < CurList.length; i++) {
            if (i === SelectedResultId) {
                strHtml += '<tr class="selected" onclick="ItemSelected(' + i + ');">';
            } else {
                strHtml += '<tr onclick="ItemSelected(' + i + ');">';
            }
            strHtml += '<td style="padding-left: 10px;">' + CurList[i].TemplateName + '</td>';
            strHtml += '</tr>';
        }
        strHtml += '</table>';
        listPanel.innerHTML = strHtml;

        document.getElementById('pnlNoResults').className = 'hidden';
    }
}

// displays the selected template
function ItemSelected(id) {
    SelectedResultId = id;
    SelectedObj = CurList[id];
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('pnlComingSoon').className = 'hidden';
    PopulateFields();
    UpdateTable();
}

// gets the marketing templates
function GetTemplateList() {
    SelectedResultId = -1;
    try {
        Ajax_CallService(
            'Services/Web/Settings.asmx',
            'GetTemplateListForTypeAndStage',
            {
                Format: CurMessageType,
                tmplType: CurObjectType
            },
            true,
            ServiceCallback
        );
    } catch (err) {
        DisplayMessage('pnlItemControls', 'There was a problem getting the template list - ' + err.toString());
    }
}

// populates the fields of the template
function PopulateFields() {
    document.getElementById('txtTemplateName').value = SelectedObj.TemplateName;
    document.getElementById('txtContentText').value = SelectedObj.BodyText;
    document.getElementById('txtSalutations').value = SelectedObj.Salutation;

    if (CurMessageType === 2) {
        document.getElementById('pnlSalutations').className = "hidden";
    }
    document.getElementById('txtPaymentInfo').className = "hidden";
    document.getElementById('lblPaymentInfo').className = "hidden";
    document.getElementById('lblTags').className = "align-left";

    DisplayMarketingTags();
}

// gets a new template
function NewTemplate() {

    SelectedResultId = -1;
    UpdateTable();
    try {
        SelectedObj = Ajax_CallService(
            'Services/Web/Settings.asmx',
            'GetEmptyTemplate',
            {},
            false
        );
    } catch (err) {
        DisplayMessage('pnlItemControls', 'There was a problem creating an empty template - ' + err.toString());
    }

    document.getElementById('txtTemplateName').value = '';
    document.getElementById('txtContentText').value = '';
    document.getElementById('txtSalutations').value = '';
    document.getElementById('pnlNoResults').className = 'hidden';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('lblTags').className = "align-left";

    if (CurMessageType === 2) {
        document.getElementById('pnlSalutations').className = "hidden";
    }

    document.getElementById('pnlComingSoon').className = 'hidden';
    document.getElementById('txtPaymentInfo').className = "hidden";
    document.getElementById('lblPaymentInfo').className = "hidden";
    DisplayMarketingTags();
}

// saves a new or edited template
function SaveTemplate() {
    SelectedObj.ObjectType = CurObjectType;
    SelectedObj.TemplateName = document.getElementById('txtTemplateName').value;
    SelectedObj.BodyText = document.getElementById('txtContentText').value;
    SelectedObj.MessageType = CurMessageType;
    document.getElementById('lblSmsLength').className = "hidden";

    if (CurMessageType === 1) {
        SelectedObj.Salutation = document.getElementById('txtSalutations').value;
    }

    if (SelectedObj.TemplateName !== "") {
        try {
            Ajax_CallService(
                'Services/Web/Settings.asmx',
                'SaveTemplate',
                { templateObj: SelectedObj },
                false
            );
        } catch (err) {
            DisplayMessage('pnlItemControls', 'There was a problem saving the template - ' + err.toString());
        }
    }

    document.getElementById('pnlItemControls').className = 'hidden';
    GetTemplateList();
}

// deletes a template
function DeleteTemplate() {
    try {
        var result = Ajax_CallService(
            'Services/Web/Settings.asmx',
            'DeleteTemplate',
            { templateObj: SelectedObj },
            false
        );
        if (!result) {
            DisplayMessage('pnlItemControls', 'There was a problem deleting the template.');
        }
    } catch (err) {
        DisplayMessage('pnlItemControls', 'There was a problem deleting the template - ' + err.toString());
    }
    if (CurList.length < 1) {
        document.getElementById('pnlNoResults').className = '';
    }
    document.getElementById('pnlItemControls').className = 'hidden';
    GetTemplateList();
}

// displays the special tags users can use for string replacement
function DisplayMarketingTags() {
    var panel = document.getElementById('pnlSmsTags');
    panel.className = "";


    var strHtml = '';
    strHtml += '<div style="width:100%;">';
    strHtml += '<br><label style="width:100%;"> &lt;SALUTATION&gt; - client title, name and surname </label><br>';
    strHtml += '<label> &lt;CATEGORY&gt; - work category name</label><br>';
    strHtml += '<label> &lt;TIME&gt; - current date and time</label><br>';
    strHtml += '<label> &lt;OFFICE_TEL&gt; - office telephone number</label><br>';
    strHtml += '<label> &lt;OFFICE_MOBILE&gt; - office mobile number</label><br>';
    strHtml += '<label> &lt;COMPANY_EMAIL&gt; - company email</label><br>';
    strHtml += '<label> &lt;COMPANY_NAME&gt; - company name</label><br><br>';
    strHtml += '</div>';

    panel.innerHTML = strHtml;
}