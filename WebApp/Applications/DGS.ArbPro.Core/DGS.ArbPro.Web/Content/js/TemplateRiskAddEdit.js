﻿var risk = {};
var rId = -1;
function AddOrEditRiskTemplate(riskId) {
    if (riskId && riskId > 0) {
        risk = GetRiskForId(riskId);
    }

    rId = riskId;

    UpdateRiskDetails();
}

function UpdateRiskDetails() {
    var name = document.getElementById('txtTemplateName');
    var ref = document.getElementById('txtDocument');
    var desc = document.getElementById('txtDescription');
    var type = document.getElementById('ddlTemplateType');

    if (rId === 1) {
        name.disabled = true;
        type.disabled = true;
        desc.disabled = true;
    }

    if (rId > 0) {
        name.value = risk.Name;
        ref.value = risk.ReferenceDoc;
        desc.value = risk.Description;
        type.value = risk.TemplateType;
    } else {
        name.value = "";
        ref.value = "";
        desc.value = "";
        type.value = 1;
    }
}

function SaveRiskTemplate() {
    risk.Id = rId;
    risk.Name = document.getElementById('txtTemplateName').value;
    risk.ReferenceDoc = document.getElementById('txtDocument').value;
    risk.Description = document.getElementById('txtDescription').value;
    risk.TemplateType = parseInt(document.getElementById('ddlTemplateType').value);

    return Save(risk);
}

function Save(risk) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'InsertOrUpdateRiskTemplate',
        {
            risk: risk
        },
        false
    );

    return retVal;
}

function GetRiskForId(riskId) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'GetRiskTemplatesForId',
        {
            riskTemplateId: riskId
        },
        false
    );

    return retVal;
}