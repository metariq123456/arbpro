﻿var Lat;
var Long;
var ZoomLevel;
var map;
var marker;
var AddType = 0;
var ExistingCount = 0;

function JobSite_Load() {
    Lat = 54;
    Long = -2;
    ZoomLevel = 5;

    AutoComplete('txtJobSiteTown', 'ListTowns');
    AutoComplete('txtJobSiteCounty', 'ListCounties');

    var btnAddNew = document.getElementById('btnAddNew');
    var btnExisting = document.getElementById('btnExisting');

    btnAddNew.className = 'button-blue';
    btnExisting.className = 'button-basic';

    var pnlJobSiteAddNew = document.getElementById('pnlJobSiteAddNew');
    var pnlJobSiteAddExisting = document.getElementById('pnlJobSiteAddExisting');

    pnlJobSiteAddNew.className = 'clientField';
    pnlJobSiteAddExisting.className = 'hidden';

    var mapOptions = {
        center: new google.maps.LatLng(Lat, Long),
        zoom: ZoomLevel,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var mapcanvas = document.getElementById('mapcanvas');
    map = new google.maps.Map(mapcanvas, mapOptions);

    var PostCode = document.getElementById('txtJobSitePostCode').value;

    if (PostCode != '') {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': PostCode + ', UK' },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var location = results[0].geometry.location;
                    map.panTo(location);
                    map.setZoom(12);

                    document.getElementById('hfJobSiteLatitude').value = location.lat();
                    document.getElementById('hfJobSiteLongitude').value = location.lng();

                    if (marker == null) {
                        marker = new google.maps.Marker({
                            position: location,
                            map: map,
                            title: PostCode
                        });
                    } else {
                        marker.setMap(map);
                        marker.setPosition(location);
                    }
                }
            });
    }

    CreateSiteList();
}

function JobSite_Close() {
    if (AddType == 0) {
        var JobID = document.getElementById('hfJobID').value;
        var Reference = document.getElementById('txtJobSiteReference').value;
        var HouseNameNo = document.getElementById('txtJobSiteHouseNameNo').value;
        var Address1 = document.getElementById('txtJobSiteAddress1').value;
        var Address2 = document.getElementById('txtJobSiteAddress2').value;
        var Town = document.getElementById('txtJobSiteTown').value;
        var County = document.getElementById('txtJobSiteCounty').value;
        var PostCode = document.getElementById('txtJobSitePostCode').value;
        var Latitude = document.getElementById('hfJobSiteLatitude').value;
        var Longitude = document.getElementById('hfJobSiteLongitude').value;

        return Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'AddJobSiteToJobNew',
            {
                JobID: JobID,
                Reference: Reference,
                HouseNameNo: HouseNameNo,
                Address1: Address1,
                Address2: Address2,
                Town: Town,
                County: County,
                PostCode: PostCode,
                Latitude: Latitude,
                Longitude: Longitude
            },
            false);
    } else {
        var JobID = document.getElementById('hfJobID').value;
        var ClientID = document.getElementById('hfClientID').value;
        var ClientSiteIDs = [];
        var Enableds = [];

        var ClientSites = Ajax_CallService(
            'Services/Web/ClientData.asmx',
            'GetClientSiteList',
            {
                ClientID: ClientID
            },
            false);

        ClientSites.forEach(function (cs) {
            ClientSiteIDs[ClientSiteIDs.length] = cs.Id;

            var Check = document.getElementById('chkClientSite' + cs.Id.toString());
            Enableds[Enableds.length] = Check.checked;
        });

        return Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'ToggleJobSitesFromJob',
            {
                JobID: JobID,
                ClientSiteIDs: ClientSiteIDs,
                Enableds: Enableds
            },
            false);
    }
}

function AddNewClick() {
    var btnAddNew = document.getElementById('btnAddNew');
    var btnExisting = document.getElementById('btnExisting');

    btnAddNew.className = 'button-blue';
    btnExisting.className = 'button-basic';

    var pnlJobSiteAddNew = document.getElementById('pnlJobSiteAddNew');
    var pnlJobSiteAddExisting = document.getElementById('pnlJobSiteAddExisting');

    pnlJobSiteAddNew.className = 'clientField';
    pnlJobSiteAddExisting.className = 'hidden';

    AddType = 0;
}

function ExistingClick() {
    var btnAddNew = document.getElementById('btnAddNew');
    var btnExisting = document.getElementById('btnExisting');

    btnAddNew.className = 'button-basic';
    btnExisting.className = 'button-blue';

    var pnlJobSiteAddNew = document.getElementById('pnlJobSiteAddNew');
    var pnlJobSiteAddExisting = document.getElementById('pnlJobSiteAddExisting');

    pnlJobSiteAddNew.className = 'hidden';
    pnlJobSiteAddExisting.className = 'clientField';

    AddType = 1;
}

function UpdateMap() {
    var PostCode = document.getElementById('txtJobSitePostCode').value;

    if (PostCode != '') {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': PostCode + ', UK' },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var location = results[0].geometry.location;
                    map.panTo(location);
                    map.setZoom(12);

                    document.getElementById('hfJobSiteLatitude').value = location.lat();
                    document.getElementById('hfJobSiteLongitude').value = location.lng();

                    if (marker == null) {
                        marker = new google.maps.Marker({
                            position: location,
                            map: map,
                            title: PostCode
                        });
                    } else {
                        marker.setMap(map);
                        marker.setPosition(location);
                    }
                }
            });
    }
}

var addressWatcher = {
    timeout: null,
    currentValue: '',
    watchForChange: function (el) {
        if (el.value != this.currentValue) {
            this.changed(el);
        }
        this.timeout = setTimeout(function () {
            addressWatcher.watchForChange(el)
        }, 1000);
    },
    cancelWatchForChange: function () {
        clearTimeout(this.timeout);
        this.timeout = null;
    },
    changed: function (el) {
        this.currentValue = el.value;
        UpdateMap();
    }
}

function CreateSiteList() {
    var Grid = document.getElementById('grdJobSiteList');
    var ClientID = document.getElementById('hfClientID').value;
    var JobID = document.getElementById('hfJobID').value;

    var strHtml = '';
    var ClientSites = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'GetClientSiteList',
        {
            ClientID: ClientID
        },
        false);
    var JobSites = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetJobSiteList',
        {
            JobID: JobID
        },
        false);

    strHtml = strHtml + '<table class="searchGrid small width-full"><col width="95%" /><col width="5%" />';

    ExistingCount = ClientSites.length;
    
    ClientSites.forEach(function (cs) {
        var result = $.grep(JobSites, function (e) { return e.SiteId == cs.Id; });
        if (result.length > 0) {
            strHtml = strHtml + '<tr>';
            strHtml = strHtml + '<td class="white-light">' + cs.Line1 + '</td>';
            strHtml = strHtml + '<td class="white-light"><input type="checkbox" id="chkClientSite' + cs.Id + '" disabled';
            strHtml = strHtml + ' checked = "true"';
        } else {
            strHtml = strHtml + '<tr onclick="ToggleSiteItem(' + cs.Id + ');">';
            strHtml = strHtml + '<td>' + cs.Line1 + '</td>';
            strHtml = strHtml + '<td><input type="checkbox" id="chkClientSite' + cs.Id + '" onclick="ToggleSiteItem(' + cs.Id + ');"';
        }

        strHtml = strHtml + '></td>';
        strHtml = strHtml + '</tr>';
        /*
        strHtml = strHtml + '<tr onclick="ToggleSiteItem(' + cs.Id + ');">';
        strHtml = strHtml + '<td>' + cs.Line1 + '</td>';
        strHtml = strHtml + '<td><input type="checkbox" id="chkClientSite' + cs.Id + '" onclick="ToggleSiteItem(' + cs.Id + ');"';

        JobSites.forEach(function (js) {
            if (js.SiteId == cs.Id) {
                strHtml = strHtml + ' checked = "true"';
            }
        });

        strHtml = strHtml + '></td>';
        strHtml = strHtml + '</tr>';
        */
    });

    strHtml = strHtml + '</table>';

    Grid.innerHTML = strHtml;
}

function ToggleSiteItem(iCSiteID) {
    var chk = document.getElementById('chkClientSite' + iCSiteID.toString());
    chk.checked = !chk.checked;
}

