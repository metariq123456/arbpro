﻿// This file contains commonly used data sources and adapters.
// It is NOT intended that it should contain all sources and adapters - for example, those used in specific dialogs etc. should
// be defined in that dialog's source file

// Global variables

// The root URL to put on the front of the service URLs
var jqxRootUrl;

//=================================================
// Sources
//=================================================

function jqxTreeSource() {
    var TreeSource = {
        type: "POST",
        datatype: "json",
        datafields: [
            { name: 'Common', type: 'string' },
        ],
        url: jqxRootUrl + 'Services/Web/Settings.asmx/GetTreeList',
        cache: true,
    };

    return TreeSource;
}

// Name:        jqxDescriptionSource
// Description: Creates a source which returns all descriptions in the system
//
// Parameters
// -----------
// None.
//
// Returns: the newly created source
function jqxDescriptionSource() {
    var DescriptionSource = {
        type: "POST",
        datatype: "json",
        datafields: [
            { name: 'Item', type: 'string' },
        ],
        url: jqxRootUrl + 'Services/Web/Settings.asmx/GetWorkDescriptionList',
        cache: true,
    };

    return DescriptionSource;
}

//=================================================
// Adapters
//=================================================

// Name:        jqxDescriptionDataAdapterForEditor
// Description: Creates a data adaptor for use in an editor
//
// Parameters
// -----------
// fieldName: the name of the field to display/edit from the underlying source
//
// Returns: the newly created adaptor
function jqxDescriptionDataAdapterForEditor(fieldName, cb) {
    var Adaptor = new $.jqx.dataAdapter(jqxDescriptionSource(),
        {
            contentType: 'application/json; charset=utf-8',
            uniqueDataFields: [fieldName],
            loadComplete: function () {
                if (cb != null) {
                    cb();
                }
            }
        });

    return Adaptor;
}

// Name:        jqxTreeDataAdapterForEditor
// Description: Creates a data adaptor for use in an editor
//
// Parameters
// -----------
// fieldName: the name of the field to display/edit from the underlying source
//
// Returns: the newly created adaptor
function jqxTreeDataAdapterForEditor(fieldName, cb) {
    var Adaptor = new $.jqx.dataAdapter(jqxTreeSource(),
        {
            contentType: 'application/json; charset=utf-8',
            uniqueDataFields: [fieldName],
            loadComplete: function () {
                if (cb != null) {
                    cb();
                }
            }
        });

    return Adaptor;
}

