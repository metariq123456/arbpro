﻿var Results;
var SelectedResult = null;

//Search types:
// 1: Client
// 2: Contact
// 3: Quote
var SearchType = -1;

function GetSearchResults(SearchCriteria, FilterText)
{ 
    try {
        var endPoint = '';
        switch (SearchCriteria) {
            case "0":
                SearchType = 1;
                //TODO Combine following calls into single service call
                /*
                Results = DoSearch('GetClientNameList', FilterText);
                Results = Results.concat(DoSearch('GetClientAddressList', FilterText));
                Results = Results.concat(DoSearch('GetClientTelephoneList', FilterText));
                Results = Results.concat(DoSearch('GetClientPostCodeList', FilterText));
                */
                endPoint = 'GetClientNameFullList';
                break;
            case "1":
                SearchType = 1;
                endPoint = 'GetClientNameList';
                break;
            case "2":
                SearchType = 1;
                endPoint = 'GetClientAddressList';
                break;
            case "3":
                SearchType = 1;
                endPoint = 'GetClientTelephoneList';
                break;
            case "4":
                SearchType = 1;
                endPoint = 'GetClientPostCodeList';
                break;
            case "5":
            case "6":
                SearchType = 2;
                endPoint = 'GetContactDetailsList';
                break;
            case "10":
                SearchType = 3;
                endPoint = 'GetQuoteNameAndSiteList';
                //Results = DoSearch('GetQuoteNameList', FilterText);
                //Results = Results.concat(DoSearch('GetQuoteSiteAddressList', FilterText));
                break;
            case "11":
                SearchType = 3;
                endPoint = 'GetQuoteNumberList';
                break;
            case "12":
                SearchType = 3;
                endPoint = 'GetQuoteNameList';
                break;
            case "13":
                SearchType = 3;
                endPoint = 'GetQuoteSiteAddressList';
                break;
            case "14":
                SearchType = 4;
                endPoint = "GetDocumentList";
                break;
            case "15":
                SearchType = 4;
                endPoint = "GetQuoteDocumentList";
                break;
            case "16":
                SearchType = 4;
                endPoint = "GetInvoiceDocumentList";
                break;
            case "17":
                SearchType = 3;
                endPoint = "GetQuoteNumberListById";
                break;
            case "19":
                SearchType = 2;
                endPoint = "GetInvoiceForId";
                break;
            case "20":
                SearchType = 2;
                endPoint = "GetForInvClientId";
                break;
            case "21":
                SearchType = 2;
                endPoint = "GetForInvNameLike";
                break;
            case "22":
                SearchType = 2;
                endPoint = "GetForInvSiteAddress";
                break;
            case "23":
                SearchType = 6;
                endPoint = "GetForJobId";
                break;
            case "24":
                SearchType = 1;
                endPoint = "GetForClientIdOrAccounts"; 
                break;
            default:
                SearchType = 1;
                endPoint = 'GetContactDetailsList';
                break;
        }
        DoSearch(endPoint, FilterText);
        //DisplayResults();
    }
    catch (err) {
        var ListPanel = document.getElementById('grdSearchResults');
        DisplayMessage('grdSearchResults', 'Error, displaying search results');
    }
}

function DoSearch(Method, FilterText) {
    Ajax_CallService(
        'Services/Web/SearchService.asmx',
        Method,
        {
            FilterText: FilterText
        },
        true,
        SetResults
    );
}

function SetResults(results) {
    Results = results;
    DisplayResults();
}

function DisplayResults() {
    var ListPanel = document.getElementById('grdSearchResults');
    var strHtml = '<table class="searchGrid width-full white">';
    if (Results != null){
        if (Results.length > 0) {
            Results.forEach(function (c) {
                if (SelectedResult && c.Id == SelectedResult.Id) {
                    strHtml += '<tr class="selected" onclick="ResultClicked(' + c.Id + ');">';
                } else {
                    strHtml += '<tr onclick="ResultClicked(' + c.Id + ');">';
                }
                strHtml += '<td style="padding-left: 10px;">' + c.Title + '</td><td style="padding-left: 10px;">' + c.Detail + '</td>';
                strHtml += '</tr>';
            });
        }
        else { strHtml += '<p class="center">No results to display</p>'; }
    }
    else { strHtml += '<p class="center">No results to display</p>'; }
    strHtml += '</table>';
    ListPanel.innerHTML = strHtml;
}

function ResultClicked(Id) {
    for (var i = 0; i < Results.length; i++) {
        if (Results[i].Id == Id) {
            SelectedResult = Results[i];
            break;
        }
    }
    DisplayResults();
}

//function ResultClickedQuote(Id, ParentId) {
//    SelectedResultId = Id;
//    SelectedResultParentId = ParentId;
//    DisplayResults();
//}

function SelectResult() {
    if (!SelectedResult) {
        return;
    }
    else {
        switch (SelectedResult.SearchType) {
            case 0:
                navigateParams(Page_strAppRoot + 'Client.aspx', [{ paramName: 'ClientID', paramValue: SelectedResult.Id }]);
                break;
            case 1:
                navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: SelectedResult.ParentId }, { paramName: 'QuoteID', paramValue: SelectedResult.Id }]);;
                break;
            case 2:
                navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: SelectedResult.ParentId }, { paramName: 'InvoiceID', paramValue: SelectedResult.Id }]);;
                //navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: SelectedResult }]);
                break;
            case 3:
                navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: SelectedResult.ParentId }, { paramName: 'QuoteID', paramValue: SelectedResult.Id }]);;
                //navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: SelectedResult }]);
                break;
            case 4:
                navigateParams(Page_strAppRoot + 'Client.aspx', [{ paramName: 'ClientID', paramValue: SelectedResult.ParentId }, { paramName: 'ContactID', paramValue: SelectedResult.Id }]);
                break;
            case 6:
                navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: SelectedResult.Id }]);
                break;
            case 12:
                navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: SelectedResult.ParentId }]);
                break;
        }
    }

}
//Client = 0,
//Quote = 1,
//Invoice = 2,
//Appointment = 3,
//Contact = 4,
//Work = 5,
//Job = 6,
//Risk = 7

