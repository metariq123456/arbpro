﻿function Load_BookJobDatesRecurring() {
    DisplayRecurringDates();
}

function DisplayRecurringDates() {
    var strHtml = '';
    strHtml += '<div class="float-left width-full">';
    strHtml += '<div class="workJobField">';
    strHtml += '<label class ="workJobLabel">Start Date</label>';
    strHtml += '<input type="text" id="txtDateFrom" class ="workJobInput" placeholder="Start Date" />';
    strHtml += '</div>';
    strHtml += '<br/><br/>';
    strHtml += '<div class="workJobField">';
    strHtml += '<label class ="workJobLabel">End Date</label>';
    strHtml += '<input type="text" id="txtDateEnd" class ="workJobInput"  placeholder="End Date" />';
    strHtml += '</div>';
    strHtml += '<br/><br/>';
    strHtml += '<div class="workJobField">';
    strHtml += '<label class ="workJobLabel">Repeat Type</label>';
    strHtml += '<select class ="workJobInput" id="RepeatTypeID" onchange="CheckRepeatType(this)"> <option value="1">Daily</option><option value="2">Weekly</option><option value="3">Monthly</option></select>';
    strHtml += '</div>';
    strHtml += '<br/><br/>';
    strHtml += '<div id="repeatType"></div>'
    strHtml += '</div>';
    strHtml += '<hr style="width:95%;"/>'
    //strHtml += '<div class="float-left"><strong>Repeat For Every</strong><br>Select 0 to repeat every week.<br>Select 1 to repeat every 2nd week.<br>Select 2 to repeat every 3rd week and so on.</div>'
    //strHtml += '<div class="float-right" style="margin-right:400px;display: inline-block;"><strong>Week Days</strong><br>Select 0 to repeat every week.<br>Select 1 to repeat every 2nd week.<br>Select 2 to repeat every 3rd week and so on.</div>'
    strHtml += '<table width="100%"><tr><th align="left">Repeat For Every</th><th align="left">Week Days</th></tr><tr><td>Select 0 to repeat every week.<br>Select 1 to repeat every 2nd week.<br>Select 2 to repeat every 3rd week and so on.</td><td>Start to type the days of the week you want to<br/> repeat this work on and select '+
    'every repeat day<br> until completed.</td></tr></table>';
    strHtml += '<table width="100%"><tr><th align="left">Repeat Type</th><th align="left">Start Date and End Date</th></tr><tr><td width="400px;">Daily:Select to repeat every week day<br>Weekly: Select specific days of the week to repeat on<br>Monthly: Select the day you want to book and number' +
    ' of months to recur the booking on</td><td valign="top">Select a start and end date for the booking.</td></tr></table>';
    document.getElementById('pnlBookJobRecurringDates').innerHTML = strHtml;
    $('#txtDateFrom').datetimepicker({
        format: 'd/M/y H:i',
        timepicker: true,
        minDate: 0
    });

    $('#txtDateEnd').datetimepicker({
        format: 'd/M/y H:i',
        timepicker: true,
        minDate: 0
    });

    RepeatTypeDaily();
}

function CheckRepeatType(RepeatTypeElement) {
    
    if (RepeatTypeElement.value == "1") {
        RepeatTypeDaily();
        
    }
    else if (RepeatTypeElement.value == "2") {
        RepeatTypeWeekly();
       
    }
    else if (RepeatTypeElement.value == "3") {
        RepeatTypeMonthly();
    }
    
}

function RepeatTypeDaily() {
    var strHtml = '';
    strHtml += '<div class="workJobField">';
    strHtml += '<label class ="workJobLabel"></label>';
    strHtml += '<input type="radio" id="rdEveryDaydaily" class ="workJobInput" checked="checked"  name="Daily"> Every Day</input>';
    strHtml += '</div>';
    strHtml += '<div class="workJobField">';
    strHtml += '<label class ="workJobLabel"></label>';
    strHtml += '<input type="radio" id="rdRepeatForDaily" class ="workJobInput" name="Daily"  style="display: inline-block;"> Repeat for every <input type="number" id="txtRepeatForDaily" style="display: inline-block;width:40px;" /> day(s)';
    strHtml += '</div>';
    document.getElementById('repeatType').innerHTML = strHtml;
}

function RepeatTypeWeekly() {
    var strHtml = '';
    strHtml += '<div class="workJobField">';
    strHtml += '<label class ="workJobLabel"></label>';
    strHtml += '<input type="radio" class ="workJobInput" id="rdRepeatForWeekly"  name="Weekly" style="display: inline-block;" checked="checked"> Repeat for every <input type="number" id="txtRepeatForWeekly" style="display: inline-block;width:40px;" value="0"/> Week(s)';
    strHtml += '</div>';
    strHtml += '<div class="workJobField">';
    strHtml += '<label class ="workJobLabel">Week Day(s)</label>';
    strHtml += '<input type="text" class ="workJobInput" id="weekday" style="min-width:100px !important;display: inline-block;"></input>';
    strHtml += '</div>';
    document.getElementById('repeatType').innerHTML = strHtml;
    $("#weekday").tokenInput([
                { id: 1, name: "Monday" },
                { id: 2, name: "Tuesday" },
                { id: 3, name: "Wednesday" },
                { id: 4, name: "Thursday" },
                { id: 5, name: "Friday" },
                { id: 6, name: "Saturday" },
                { id: 7, name: "Sunday" }               
    ], {
        theme: "facebook"
    });
}

function RepeatTypeMonthly() {
    var strHtml = '';
    strHtml += '<div class="workJobField">';
    strHtml += '<label class ="workJobLabel"></label>';
    strHtml += '<input type="radio" class ="workJobInput" checked="checked" style="display: inline-block;"> On day ' + DaysSelectOption() + ' of every <input type="number" id="txtMonthlyForDaily" style="display: inline-block;width:40px;" /> month(s)';
    strHtml += '</div>';
    document.getElementById('repeatType').innerHTML = strHtml;
}

function DaysSelectOption() {
    strHtml = '';
    strHtml += '<select style="display:inline-block;width:60px;" id="onMonthDay"><option value="1">1</option>';
    strHtml += '<option value="2">2</option>';
    strHtml += '<option value="3">3</option>';
    strHtml += '<option value="4">4</option>';
    strHtml += '<option value="5">5</option>';
    strHtml += '<option value="6">6</option>';
    strHtml += '<option value="7">7</option>';
    strHtml += '<option value="8">8</option>';
    strHtml += '<option value="9">9</option>';
    strHtml += '<option value="10">10</option>';
    strHtml += '<option value="11">11</option>';
    strHtml += '<option value="12">12</option>';
    strHtml += '<option value="13">13</option>';
    strHtml += '<option value="14">14</option>';
    strHtml += '<option value="15">15</option>';
    strHtml += '<option value="16">16</option>';
    strHtml += '<option value="17">17</option>';
    strHtml += '<option value="18">18</option>';
    strHtml += '<option value="19">19</option>';
    strHtml += '<option value="20">20</option>';
    strHtml += '<option value="21">21</option>';
    strHtml += '<option value="22">22</option>';
    strHtml += '<option value="23">23</option>';
    strHtml += '<option value="24">24</option>';
    strHtml += '<option value="25">25</option>';
    strHtml += '<option value="26">26</option>';
    strHtml += '<option value="27">27</option>';
    strHtml += '<option value="28">28</option>';
    strHtml += '<option value="29">29</option>';
    strHtml += '<option value="30">30</option>';
    strHtml += '<option value="31">31</option>';
    strHtml += '</select>';
    return strHtml;
}