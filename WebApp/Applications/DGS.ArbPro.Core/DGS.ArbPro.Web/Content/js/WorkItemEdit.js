﻿/*
    WorkItemEdit.js

    This file contains functionality for editing or adding work items for a work record
    This file utilises objects loaded in WorkManagement.js and Work.js files

    An item added at work stage will not have a corisponding QuoteItem. However, the application must construct a jobitem for the 
    a new work item.
*/

//  Global scope variables
var ObjWorkItem = null;             //  This is the work item that is being edited or added
var DefaultTax = 0.00;              //  The default comapany setting for the tax rate
/*
        Initialisation and service call Functions
*/


function WorkItem_Load(id, isNew) {
    DefaultTaxRate_Load();
    if (isNew) {
        WorkItem_LoadNew(id);
    } else {
        WorkItem_LoadItem(id);
    }
}

// function to load a work item
function WorkItem_LoadItem(itemId) {

    Ajax_CallService(
       'Services/Web/WorkFlow.asmx',
       'WorkItemLoad',
       {
           itemId: itemId
       },
       true,
       WorkItem_LoadCallBack);
}

function WorkItem_LoadNew(jobSiteId) {

    Ajax_CallService(
       'Services/Web/WorkFlow.asmx',
       'WorkItemNew',
       {
           jobSiteId: jobSiteId
       },
       true,
       WorkItem_LoadCallBack);
}

//  Async service call to load the default tax rate
function DefaultTaxRate_Load() {
    
    Ajax_CallService(
    'Services/Web/Company.asmx',
    'GetCompanyTaxSetting',
    {},
    true,
    DefaultTaxRate_CallBack);
}

function WorkItem_Save() {
    var panel = document.getElementById('ItemDetails');
    // Populate the WorkItem properties
    //NOTE: This must be done before the wait timer is displayed otherwise the controls will be disposed of
    if (PopulateWorkItem()) {
        panel.innerHTML = GetWaitImageForPanel(panel.id, 'Saving Please Wait.');
        var didComplete = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'WorkItemSave',
        {
            item: ObjWorkItem
        },
        false);
        // Check if the item has been updated.
        if (!didComplete) {
            DisplayMessage(panel.id, 'Error: Arb Pro service failed to save the updates to the item.');
        }

        return didComplete;
    } else {
        alert('Work Item value or tax rate is not valid.');
        return false;
    }
}


/*
        Async service call back functions
*/

//  Async service call back to populate a
function WorkItem_LoadCallBack(item) {
    var panel = document.getElementById('ItemDetails');
    if (!item) {
        //  The service did not return a valid work item object 
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid work item');
    } else {
        try {
            ObjWorkItem = item;
            var strHtml = '<div class="width-full">';
            //  Generate the html controls
            strHtml += '<table class="searchGrid padded">';
            strHtml += '<tr>';
            strHtml += '<th style="width:7%;text-align: center;"> Item Order </th>';
            strHtml += '<th style="width:7%;text-align: center;"> Tree No. </th>';
            strHtml += '<th style="width:18%;text-align: center;"> Tree Name </th>';
            strHtml += '<th style="width:38%;text-align: center;"> Description </th>';
            strHtml += '<th style="width:13%;text-align: center;"> Value </th>';
            strHtml += '<th style="width:12%;text-align: center;"> VAT Rate </th>';
            strHtml += '</tr>';
            strHtml += '<tr>';
            strHtml += '<td style="width:7%; vertical-align:top;">';
            strHtml += '<textarea id="txtNewItemOrderId" class="jobInput width-full" TextMode="MultiLine"  style="width:95%;" name="txtNewItemOrderId"></textarea>';
            strHtml += '</td>';
            strHtml += '<td style="width:7%; vertical-align:top;">';
            strHtml += '<textarea id="txtNewItemTreeNo" class="jobInput width-full" TextMode="MultiLine" style="width:95%;" name="txtNewItemTreeNo"></textarea>';
            strHtml += '</td>';
            strHtml += '<td style="width:20%; vertical-align:top;">';
            strHtml += '<textarea id="txtNewItemTreeName" class="jobInput width-full" TextMode="MultiLine" style="width:90%;" name="txtNewItemTreeName"></textarea>';
            strHtml += '</td>';
            strHtml += '<td style="width:40%; vertical-align:top;">';
            strHtml += '<textarea id="txtNewItemDescription" class="jobInput width-full" TextMode="MultiLine" style="width:95%;" name="txtNewItemDescription"></textarea>';
            strHtml += '</td>';
            strHtml += '<td style="width:15%; vertical-align:top;">';
            strHtml += '<textarea id="txtNewItemValue" class="jobInput float-right" TextMode="MultiLine" style="width:80%;" value="0" name="txtNewItemValue"></textarea>';
            strHtml += '<span class="float-right" style="padding-top: 7px; padding-right: 4px;"> ' + CurrentCurrency + ' </span>';
            strHtml += '</td>';
            strHtml += '<td style="width:13%; vertical-align:top;">';
            strHtml += '<span class="float-right" style="padding-top: 7px; padding-right: 3px;"> % </span>';
            strHtml += '<textarea id="txtNewItemVatRate" class="jobInput float-right" TextMode="MultiLine" style="width:70%;" value="0" name="txtNewItemVatRate"></textarea>';
            strHtml += '</td>';
            strHtml += '</tr>';
            strHtml += '</table>';
            strHtml += '</div>';
            // Update the panel with the controls
            panel.innerHTML = strHtml;
            //  Populate the form as required
            UpdateControls();
        } catch (err) {
            //  Display the error message to the user.
            DisplayMessage(panel.id, 'Error: Unable to display work item details - ' + err.toString());
        }
    }
}

//  Async service call back to validate and set the default tax rate for the item
function DefaultTaxRate_CallBack(value) {
    // check if the service returned a value and that it is a number
    if (!value || isNaN(value)) {
        // The service call failed or the value returned is not a number, so set to a default of 20%
        DefaultTax = 20;
    } else {
        DefaultTax = value;
    }
}

/*
        Local Functions
*/

//  Function to popualte the page controls based on the status of the item
function UpdateControls() {
    if (ObjWorkItem.Id < 1) {
        //This is a new item, so only the default vat value needs to updated
        SetControlValue('txtNewItemVatRate',DefaultTax.format());
    } else {
        SetControlValue('txtNewItemOrderId', ObjWorkItem.OrderId);
        SetControlValue('txtNewItemTreeNo', ObjWorkItem.Code);
        SetControlValue('txtNewItemTreeName', ObjWorkItem.Name);
        SetControlValue('txtNewItemDescription', ObjWorkItem.Description);
        SetControlValue('txtNewItemValue', ObjWorkItem.Value.format());
        SetControlValue('txtNewItemVatRate', ObjWorkItem.VatRate.format());
    }
    //Setup the lookup lists for the pick items
    AutoComplete('txtNewItemTreeName', 'ListTreeNames');
    AutoComplete('txtNewItemDescription', 'ListWorkDescriptions');
}

function PopulateWorkItem() {
    var isValid = true;
    ObjWorkItem.OrderId = GetControlValue('txtNewItemOrderId');
    ObjWorkItem.Code = GetControlValue('txtNewItemTreeNo');
    ObjWorkItem.Name = GetControlValue('txtNewItemTreeName');
    ObjWorkItem.Description = GetControlValue('txtNewItemDescription');
    ObjWorkItem.Value = GetControlValue('txtNewItemValue').replace(',','');
    ObjWorkItem.VatRate = GetControlValue('txtNewItemVatRate');
    ObjWorkItem.JobId = ObjJob.Id;
    ObjWorkItem.WorkId = ObjWorkSummary.Id;
    if (isNaN(ObjWorkItem.Value) || isNaN(ObjWorkItem.VatRate))
        isValid = false;

    return isValid;
}

//  Simple function to set the value of a control;
function SetControlValue(controlId, newValue) {
    document.getElementById(controlId).value = newValue;
}



function GetControlValue(controlId, defaultValue) {
    if (!defaultValue) defaultValue = '';
    var control = document.getElementById(controlId);
    if (!control || control == null)
        return defaultValue;
    else {
        return control.value;
    }
}