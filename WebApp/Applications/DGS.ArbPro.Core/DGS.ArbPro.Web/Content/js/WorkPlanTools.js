﻿var AllTools = new Array();
var SelectedTools = new Array();

function WorkPlanTool_Load(selectedTools) {
    var panel = document.getElementById('pnlToolsError');
    SelectedTools = selectedTools;
    try {
        Ajax_CallService(
               '../Services/Web/Settings.asmx',
               'GetToolList',
               {
               },
               true,
               PopulatToolsDialog
           );
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to communicate with Arb Pro server.');
    }
}

function WorkPlanTool_Save() {

    SelectedTools = new Array();
    try {
        var element;
        AllTools.forEach(function (t) {
            //get the element for the relevent tool Id
            element = document.getElementById('txtToolNum_' + t.Id.toString());
            //Check the element is valid and there are a number of tools required
            var isNotNum = isNaN(element.value) || element.value.length == 0;
            if (element && t.Id > 0 && isNotNum == false) {
                var obj = {
                    Id: -1,
                    WorkPlanId: -1,
                    ToolId: t.Id,
                    Item: t.Item,
                    Number: parseInt(element.value)
                };
                SelectedTools.push(obj);
            }
        });
    }
    catch (err) {
        DisplayMessage('pnlToolsError', 'Error: Unable to create list of selected tools - ' + err.toString());
        SelectedTools = null;
    }
    return SelectedTools;
}
//Function to populate the tools dialog from the staff list
function PopulatToolsDialog(results) {
    var panel = document.getElementById('pnlPlanTool');
    try {
        if (!results || results.length == 0) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid list of tools.');
        } else {
            //Set the scope variable
            AllTools = results;
            var strHtml = '<table class="searchGrid width-full white" style="table-layout:fixed; word-wrap:break-word;">';
            strHtml += '<tr><th>Tool</th><th>Number Required</th></tr>';
            //Iterate through all the tools and create a row in the table
            AllTools.forEach(function (c) {
                strHtml += '<tr>';
                strHtml += '<td style="maxwidth: 50%; padding-left: 10px;">' + c.Item + '</td>';
                strHtml += '<td class="companyInput"><input type="text" Id="txtToolNum_' + c.Id + '" placeholder="Total Stock: ' + c.Stock + '" title="The number of tools required for the work."/>  </td> ';
                strHtml += '</tr>';
            })
            strHtml += '</table>';
            //Set the panels inner html to be the generated html;
            panel.innerHTML = strHtml;
            //Now the list has been created and display, set the selected tools
            SetSelectedTools();
        }
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display staff list - ' + err.toString());
    }
}

//Function to set the quantity of all tools already selected
function SetSelectedTools() {
    var panel = document.getElementById('pnlToolsError');
    try {
        //Check if there is a list of currently selected tools
        if (SelectedTools && SelectedTools.length > 0) {
            var element;

            SelectedTools.forEach(function (t) {
                //get the element for the relevent tool Id
                element = document.getElementById('txtToolNum_' + t.ToolId.toString());
                //Check the element is valid and there are a number of tools required
                if (element && t.Number > 0) {
                    //Set the value to be displayed
                    element.value = t.Number.toString();
                }
            });
        }
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display currently selected tools list - ' + err.toString());
    }
}

//Function to get a new tool
function GetNewTool(tool, quantity) {
    var obj = new Object();
    obj.Id = -1;
    obj.WorkPlanId = -1;
    obj.ToolId = tool.Id;
    obj.Item = tool.Item;
    obj.Number = parseInt(quantity);
    return obj;
}