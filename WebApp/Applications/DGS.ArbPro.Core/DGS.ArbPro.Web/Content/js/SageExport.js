﻿var CompanyObject;

function SageLoad(_companyObject) {
    CompanyObject = _companyObject;
    PopulateTextBoxes();
}


function PopulateTextBoxes() {
    if (CompanyObj.SageSalesCode != null) {
        document.getElementById('txtSalesInvoiceCode').value = CompanyObject.SageSalesCode;
    }
    if (CompanyObj.SageReceiptCode != null) {
        document.getElementById('txtSalesReceipCode').value = CompanyObject.SageReceiptCode;
    }
    if (CompanyObj.SageVatCode != null) {
        document.getElementById('txtSageVatCode').value = CompanyObject.SageVatCode;
    }
}

function SageSave() {

    CompanyObject.SageSalesCode = document.getElementById('txtSalesInvoiceCode').value;
    CompanyObject.SageReceiptCode = document.getElementById('txtSalesReceipCode').value;
    CompanyObject.SageVatCode = document.getElementById('txtSageVatCode').value;
     
    var SaveCompany = Ajax_CallService(
        'Services/Web/Company.asmx',
        'SaveCompany',
        {
            Company: CompanyObject
        },
        false
    );
}