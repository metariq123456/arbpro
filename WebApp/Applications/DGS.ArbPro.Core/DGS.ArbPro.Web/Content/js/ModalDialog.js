﻿var Dialog_strAppRoot;
var Dialog_bDialogOpen = false;
var Dialog_bHasDialog = false;

function Dialog_Initialise(strAppRootURL) {
    Dialog_strAppRoot = strAppRootURL;
}

// Opens a Modal Dialog - Call this function as it allows for easy switching between dialogs.
/*
    strTitle: The Title of the Dialog
    strPageURL: The URL of the dialog content, relative to root (no preceding /)
    strPageContendDivID: the ID of the dis/panel to load into the dialog (no preceding #)
    iWidth: the Width of the Dialog Content
    iHeight: the Height of the Dialog Content
    bModal: True if you want the background darkened & the page behind it to be unaccessible
    objButtons: The buttons to appear at the bottom of the dialog. Null if you don't want the button panel to appear
    strJavaContentURL: The javascript file to load alongside the content, null if none
    funcRunAfterOpen: the function to run once the content has loaded. Does not require strJavaContentURL - but will run after that is loaded if it is specified
*/
function Dialog_OpenPage(strTitle, strPageURL, strPageContentDivID, iWidth, iHeight, bModal, objButtons, strJavaContentURL, funcRunAfterOpen) {

    // We need to limit the width and height to cater for smaller screens.
    // If the dimensions have in fact been adjusted, then we need to take a bit more off

    var iAdjustedHeight = Math.min(iHeight, screen.availHeight);
    if (iAdjustedHeight < iHeight) {
        iAdjustedHeight -= 100;
    }

    var iAdjustedWidth = Math.min(iWidth, screen.availWidth);
    if (iAdjustedWidth < iWidth) {
        iAdjustedWidth -= 80;
    }

    if (Dialog_bDialogOpen) {
        $('#Dialog_Window').dialog('close');
        setTimeout(function () {
            Dialog_CreateDialog(strTitle, strPageURL, strPageContentDivID, iAdjustedWidth, iAdjustedHeight, bModal, objButtons, strJavaContentURL, funcRunAfterOpen)
        }, 210);
    } else {
        Dialog_CreateDialog(strTitle, strPageURL, strPageContentDivID, iAdjustedWidth, iAdjustedHeight, bModal, objButtons, strJavaContentURL, funcRunAfterOpen);
    }
}

// Creates a Modal Dialog and displays it - used for grunt work, use the above function instead
function Dialog_CreateDialog(strTitle, strPageURL, strPageContentDivID, iWidth, iHeight, bModal, objButtons, strJavaContentURL, funcRunAfterOpen) {
    $('#Dialog_Window').dialog({
        title: strTitle,
        height: iHeight + (Dialog_bHasDialog ? ($.browser.mozilla ? 0 : 40) : 0),
        width: iWidth,
        modal: bModal,
        resizable: false,
        draggable: true,
        closeOnEscape: true,
        show: {
            effect: 'fade',
            duration: 500
        },
        hide: {
            effect: 'fade',
            duration: 200,
        },
        buttons: objButtons,
        open: function () {
            Dialog_bDialogOpen = true;
            $('#Dialog_Content').load(Dialog_strAppRoot + strPageURL + ' #' + strPageContentDivID, function () {
                $('#' + strPageContentDivID).height($('#Dialog_Content').height() - 5);

                if (!$.browser.webkit) {
                    Dialog_bHasDialog = true;
                }

                if (strJavaContentURL != null && strJavaContentURL != '') {

                    $.getScript(Dialog_strAppRoot + strJavaContentURL, function (script, textStatus, jqXHR) {
                        if (funcRunAfterOpen != null)
                            funcRunAfterOpen();
                    });
                } else {

                    if (funcRunAfterOpen != null)
                        funcRunAfterOpen();
                }
            });
        },
        close: function () {
            Dialog_bDialogOpen = false;
        }
    });
}