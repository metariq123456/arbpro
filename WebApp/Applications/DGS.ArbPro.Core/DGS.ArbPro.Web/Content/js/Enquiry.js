﻿var ClientListWatcher = {
    timeout: null,
    currentValue: '',
    watchForChange: function (el) {
        if (el.value != this.currentValue) {
            this.changed(el);
        }
        this.timeout = setTimeout(function () {
            ClientListWatcher.watchForChange(el)
        }, 1000);
    },
    cancelWatchForChange: function () {
        clearTimeout(this.timeout);
        this.timeout = null;
    },
    changed: function (el) {
        this.currentValue = el.value;
        UpdateClientList();
    }
}

var templateId = 1;


function EditClient() {
    var hfClientID = document.getElementById('hfClientID');
    Dialog_OpenPage(
        'Edit Client',
        'Dialogs/NewClient.aspx?ClientID=' + hfClientID.value,
        'content',
        560,
        495,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    var NewID = NewClient_Save();
                    if (NewID > -1) {
                        $(this).dialog('close');
                        CloseEditClient(hfClientID.value);
                    }
                }
            },
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/NewClient.js?V=3.25.0',
        function () {
            NewClient_Load();
        });
}


function UpdateClientList() {
    var FilterText = document.getElementById('txtClientListFilter').value;
    var Client = GetUser(Page_strUserEmail);
    var ID = document.getElementById('hfClientID').value;

    var ClientList = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'GetClientList',
        {
            FilterText: FilterText
        },
        false
    );

    var ListPanel = document.getElementById('pnlClientList');
    var strHtml = '<table class="searchGrid width-full white">';
    ClientList.forEach(function (c) {
        if (ID == c.Id) {
            strHtml += '<tr onclick="PopulateClient(' + c.Id + ');" class="selected">';
        } else {
            strHtml += '<tr onclick="PopulateClient(' + c.Id + ');">';
        }

        if (c.Name == '') {
            strHtml += '<td>ID#' + String(c.Id) + ': Unnamed</td>';
        } else {
            strHtml += '<td>' + c.Name + '</td>';
        }
        strHtml += '</tr>';
    });
    strHtml += '</table>';
    ListPanel.innerHTML = strHtml;

    AutoComplete('txtEnquiryClientTitle', 'ListTitles');
    AutoComplete('txtEnquiryClientTown', 'ListTowns');
    AutoComplete('txtEnquiryClientCounty', 'ListCounties');
}

function PopulateClient(iClientID) {
    document.getElementById('pnlEnquiryClientEdit').className = 'enquiryPane border-left';

    //var DBName = GetUser(Page_strUserEmail).ClientDatabaseName;

    var Client = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'GetClient',
        {
            ClientID: iClientID
        },
        false
    );

    var e = document.getElementById('ddlEnquiryClientType');
    e.selectedIndex = Client.AccountType;
    if (e.selectedIndex == -1)
        e.selectedIndex = 0;
    document.getElementById('hfClientID').value = Client.Id;
    document.getElementById('txtEnquiryClientTitle').value = Client.Title;
    document.getElementById('txtEnquiryClientForename').value = Client.Forename;
    document.getElementById('txtEnquiryClientSurname').value = Client.Surname;
    document.getElementById('txtEnquiryClientBusinessName').value = Client.BusinessName;
    document.getElementById('txtEnquiryClientWebsite').value = Client.WebAddress;
    document.getElementById('hfEnquiryClientAltAccountNo').value = Client.AltAccountNo;
    document.getElementById('txtEnquiryClientHouseNameNo').value = Client.HouseNameNumber;
    document.getElementById('txtEnquiryClientAddress1').value = Client.Address1;
    document.getElementById('txtEnquiryClientAddress2').value = Client.Address2;
    document.getElementById('txtEnquiryClientTown').value = Client.Town;
    document.getElementById('txtEnquiryClientCounty').value = Client.County;
    document.getElementById('txtEnquiryClientPostCode').value = Client.PostCode;
    document.getElementById('txtEnquiryClientTelephone').value = Client.Telephone;
    document.getElementById('txtEnquiryClientMobile').value = Client.Mobile;
    document.getElementById('txtEnquiryClientEmail').value = Client.Email;
    document.getElementById('chkEnquirySms').checked = Client.SendSmsMessage;
    document.getElementById('chkEnquiryIncludeMarketing').checked = Client.IncludeInMarketing;
    UpdateClientList();
    SwitchClientMode();
}

function SaveClient() {
    var ddl = document.getElementById('ddlEnquiryClientType');

    var ID = document.getElementById('hfClientID');
    var Title = document.getElementById('txtEnquiryClientTitle');
    var ForeName = document.getElementById('txtEnquiryClientForename');
    var Surname = document.getElementById('txtEnquiryClientSurname');
    var BusinessName = document.getElementById('txtEnquiryClientBusinessName');
    var Website = document.getElementById('txtEnquiryClientWebsite');
    var AltAccountNo = document.getElementById('hfEnquiryClientAltAccountNo');
    var Email = document.getElementById('txtEnquiryClientEmail');
    var Telephone = document.getElementById('txtEnquiryClientTelephone');
    var Mobile = document.getElementById('txtEnquiryClientMobile');
    var HouseNameOrNo = document.getElementById('txtEnquiryClientHouseNameNo');
    var Address1 = document.getElementById('txtEnquiryClientAddress1');
    var Address2 = document.getElementById('txtEnquiryClientAddress2');
    var Town = document.getElementById('txtEnquiryClientTown');;
    var County = document.getElementById('txtEnquiryClientCounty');;
    var PostCode = document.getElementById('txtEnquiryClientPostCode');
    var SendSmsMessage = document.getElementById('chkEnquirySms');
    var IncludeInMarketing = document.getElementById('chkEnquiryIncludeMarketing');

    //var DBName = GetUser(Page_strUserEmail).ClientDatabaseName;

    var Return = Ajax_CallService(
            'Services/Web/ClientData.asmx',
            'SaveClient',
            {
                ClientID: ID != null ? ID.value : -1,
                ClientType: ddl.options[ddl.selectedIndex].value,
                ClientTitle: Title != null ? Title.value : '',
                ClientForeName: ForeName != null ? ForeName.value : '',
                ClientSurname: Surname != null ? Surname.value : '',
                ClientBusinessName: BusinessName != null ? BusinessName.value : '',
                ClientSector: '',
                ClientWebsite: Website != null ? Website.value : '',
                ClientAlternateAccountNo: AltAccountNo != null ? AltAccountNo.value : '',
                ClientEmail: Email != null ? Email.value : '',
                ClientTelephone: Telephone != null ? Telephone.value : '',
                ClientMobile: Mobile != null ? Mobile.value : '',
                ClientHouseNameOrNumber: HouseNameOrNo != null ? HouseNameOrNo.value : '',
                ClientAddress1: Address1 != null ? Address1.value : '',
                ClientAddress2: Address2 != null ? Address2.value : '',
                ClientTown: Town != null ? Town.value : '',
                ClientCounty: County != null ? County.value : '',
                ClientPostCode: PostCode != null ? PostCode.value : '',
                ClientSmsMessage: SendSmsMessage != null ? SendSmsMessage.checked : '',
                ClientIncludeInMarketing: IncludeInMarketing != null ? IncludeInMarketing.checked : ''
            },
            false
        );

    var ID = document.getElementById('hfClientID');
    ID.value = Return.toString();
    if (Return) {
        if ((typeof (FilterText) != 'undefined')) {
            var ClientList = Ajax_CallService(
                'Services/Web/ClientData.asmx',
                'GetClientList',
                {
                    FilterText: FilterText
                },
                false
            );
        }
        return true;
    } else {
        alert('Client Save Failed');
        return true;
    }
}

function SwitchClientType() {
    SwitchClientMode();
}

function SwitchClientMode() {
    var ddl = document.getElementById('ddlEnquiryClientType');

    if (ddl.options[ddl.selectedIndex].value == "0") {
        document.getElementById('pnlEnquiryClientResidential').className = 'enquiryField';
        document.getElementById('pnlEnquiryClientCommercial').className = 'enquiryField hidden';
    } else {
        document.getElementById('pnlEnquiryClientResidential').className = 'enquiryField hidden';
        document.getElementById('pnlEnquiryClientCommercial').className = 'enquiryField';
    }
}

function LoadEnquiryPanel() {
    ToggleClientSite();
    AutoComplete('txtEnquirySource', 'ListEnquirySource');
    AutoComplete('txtEnquirySiteTown', 'ListTowns');
    AutoComplete('txtEnquirySiteCounty', 'ListCounties');
}

function ToggleClientSite() {
    var UseSite = document.getElementById('chkUseClientAddress').checked;
    if (UseSite) {
        document.getElementById('pnlEnquirySalesSiteEditable').className = 'enquiryFieldDark hidden';
        document.getElementById('pnlEnquirySalesSiteView').className = 'enquiryFieldDark';

        var ddlSite = document.getElementById('ddlEnquirySite');
        document.getElementById('hfEnquirySiteID').value = ddlSite.options[ddlSite.selectedIndex].value;
    } else {
        document.getElementById('pnlEnquirySalesSiteEditable').className = 'enquiryFieldDark';
        document.getElementById('pnlEnquirySalesSiteView').className = 'enquiryFieldDark hidden';

        document.getElementById('hfEnquirySiteID').value = '-1';
    }
}

function SwitchClientSite() {
    var ddlSite = document.getElementById('ddlEnquirySite');
    document.getElementById('hfEnquirySiteID').value = ddlSite.options[ddlSite.selectedIndex].value;

    var Site = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'GetClientSite',
        {
            ClientSiteID: ddlSite.options[ddlSite.selectedIndex].value
        },
        false);

    document.getElementById('lblEnquirySiteAddress').innerHTML =
        (Site.HouseNameNo == '' ? '' : Site.HouseNameNo + ' ') +
        (Site.Address1 == '' ? '' : Site.Address1 + '<br />') +
        (Site.Address2 == '' ? '' : Site.Address2 + '<br />') +
        (Site.Town == '' ? '' : Site.Town + '<br />') +
        (Site.County == '' ? '' : Site.County + '<br />') +
        (Site.PostCode == '' ? '' : Site.PostCode + '<br />');
}

function OpenCalendar() {
    var SurveyorID = document.getElementById('ddlEnquirySurveyor').options[document.getElementById('ddlEnquirySurveyor').selectedIndex].value;
    var clientId = document.getElementById('hfClientID').value;
    var SiteList = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'GetClientSiteList',
        {
            ClientID: clientId
        },
        false);

    var edit = false;
    var enquiry = true;

    Dialog_OpenPage('Book Appointment',
        'Dialogs/CalendarEditAppointment.aspx?SurveyorID=' + SurveyorID,
        'content',
        1200,
        670,
        true,
        {
            'Done': {
                text: 'Done',
                class: 'ui-dialog-button',
                click: function () {
                    var conf = confirm("Are you sure you want to use the selected risk assessment template for this appointment?");
                    if (conf) {
                        CloseCalendar();
                        $(this).dialog('close');
                    }
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/CalendarEditAppointment.js?V=3.23.5',
        function () {
            LoadCalendar(SiteList, edit, enquiry);
        });
}

function CloseCalendar() {
    document.getElementById('pnlApptFieldEdit').className = '';
    document.getElementById('pnlApptFieldNone').className = 'hidden';

    $('#hfStartDateTime').val($('#hfAppStart').val());
    $('#hfEndDateTime').val($('#hfAppEnd').val());

    var SurveyorID = $('#hfAppSurveyor').val();
    templateId = $('#hfTemplateId').val();

    var ddlSurv = document.getElementById('ddlEnquirySurveyor');
    ddlSurv.value = SurveyorID;

    var options = {
        hour: "2-digit", minute: "2-digit"
    };

    document.getElementById('lblApptDate').textContent = new Date(document.getElementById('hfStartDateTime').value).toLocaleDateString("en-GB");
    document.getElementById('lblApptTime').textContent = new Date(document.getElementById('hfStartDateTime').value).toLocaleTimeString("en-GB", options) + " - " + new Date(document.getElementById('hfEndDateTime').value).toLocaleTimeString("en-GB", options);
}

function SaveSalesEnquiry() {

    var ClientID = document.getElementById('hfClientID').value;
    var ddlSurv = document.getElementById('ddlEnquirySurveyor');
    var SurveyorID = ddlSurv.options[ddlSurv.selectedIndex].value;
    var Source = document.getElementById('txtEnquirySource').value;
    var Comments = document.getElementById('txtEnquiryComments').value;
    var ApptStart = document.getElementById('hfStartDateTime').value;
    var ApptEnd = document.getElementById('hfEndDateTime').value;
    var SiteReference = document.getElementById('txtEnquirySiteReference').value;
    var SiteID = document.getElementById('hfEnquirySiteID').value;
    var SiteHouseNameNumber = document.getElementById('txtEnquirySiteHouseNameNo').value;
    var SiteAddress1 = document.getElementById('txtEnquirySiteAddress1').value;
    var SiteAddress2 = document.getElementById('txtEnquirySiteAddress2').value;
    var SiteTown = document.getElementById('txtEnquirySiteTown').value;
    var SiteCounty = document.getElementById('txtEnquirySiteCounty').value;
    var SitePostCode = document.getElementById('txtEnquirySitePostCode').value;
    var UseSite = document.getElementById('chkUseClientAddress').checked;
    var JobID = -1;

    if (UseSite || (SiteAddress1 && SiteAddress1 != "")) {
        //Needs to be updated
        JobID = Ajax_CallService(
            'Services/Web/ClientData.asmx',
            'CreateSalesEnquiry',
            {
                ClientID: ClientID,
                SurveyorID: SurveyorID,
                Source: Source,
                Comments: Comments,
                ApptStart: ApptStart,
                ApptEnd: ApptEnd,
                SiteReference: SiteReference,
                SiteID: SiteID,
                SiteHouseNameNumber: SiteHouseNameNumber,
                SiteAddress1: SiteAddress1,
                SiteAddress2: SiteAddress2,
                SiteTown: SiteTown,
                SiteCounty: SiteCounty,
                SitePostCode: SitePostCode,
                TemplateId: templateId
            },
            false);
    }

    // debugger 
    document.getElementById('hfJobID').value = JobID.toString();
    return JobID > 0;
}

function NavigateToJob() {
    var JobID = document.getElementById('hfJobID').value;
    window.location = '/Job.aspx?JobID=' + JobID;
}

function NavigateToDefault() {
    window.location = '/Default.aspx';
}


function CloseEditClient(ClientId) {
    var client = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'GetClient',
        {
            ClientID: ClientId
        },
        false
    );
    //Company
    document.getElementById('lblEnquiryClientName').innerHTML = client.AccountType === 0 ? client.Surname : client.BusinessName;

    document.getElementById('lblEnquiryClientAddress').innerHTML = client.HouseNameNumber + ' ' +
        (client.Address1.length <= 0 ? '' : (client.Address1 + '<br /> ')) +
        (client.Address2 <= 0 ? '' : (client.Address2 + '<br /> ')) +
        (client.Town.length <= 0 ? '' : (client.Town + '<br />')) +
        (client.County.length <= 0 ? '' : (client.County + '<br />')) +
        (client.PostCode.length <= 0 ? '' : (client.PostCode + '<br />'));

    document.getElementById('lblEnquiryClientContactInfo').innerHTML = 'Tel: ' +
        client.Telephone + '<br />' + 'Mob: ' +
        client.Mobile + '<br />' + client.Email;
}


