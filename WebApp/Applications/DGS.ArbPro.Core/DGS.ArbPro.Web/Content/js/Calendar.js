﻿// http://arshaw.com/fullcalendar/docs/
var timer;
var BookedWork = false;
var AcceptedQuote = null;
function UpdateFilters() {
    $('#pnlCalendar').fullCalendar('refetchEvents');
}

var updateQueryStringParam = function (key, value) {

    var baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
        urlQueryString = document.location.search,
        newParam = key + '=' + value,
        params = '?' + newParam;

    // If the "search" string exists, then build params from it
    if (urlQueryString) {

        updateRegex = new RegExp('([\?&])' + key + '[^&]*');
        removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

        if (typeof value == 'undefined' || value == null || value == '') { // Remove param if value is empty

            params = urlQueryString.replace(removeRegex, "$1");
            params = params.replace(/[&;]$/, "");

        } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it

            params = urlQueryString.replace(updateRegex, "$1" + newParam);

        } else { // Otherwise, add it to end of query string

            params = urlQueryString + '&' + newParam;

        }

    }
    window.history.replaceState({}, "", baseUrl + params);
}

function StartCalendar() {
    $('#pnlCalendar').fullCalendar({
        header: {
            left: 'title',
            center: 'agendaDay,agendaWeek,month',
            right: 'prev,today,next'
        },
        aspectRatio: 2,
        defaultView: 'month',
        events: function (start, end, callback) {
            var EventsList = new Array();

            var ddlEventType = document.getElementById('ddlEventType');
            var EventType = ddlEventType.options[ddlEventType.selectedIndex].value;

            var ddlStaff = document.getElementById('ddlEventStaff');
            var StaffID = ddlStaff.options[ddlStaff.selectedIndex].value;

            if (EventType == 0 || EventType == 1) {
                console.log('Start:' + start.toDateString());
                console.log('End:' + end.toDateString());
                console.log('StaffID:' + StaffID);
                var qAppts = Ajax_CallService(
                    'Services/Web/Calendar.asmx',
                    'GetQuoteEvents',
                    {
                        Start: start.toDateString(),
                        End: end.toDateString(),
                        StaffID: StaffID
                    },
                    false);

                qAppts.forEach(function (ev) {
                    EventsList.push({
                        id: ev.Id,
                        data: ev,
                        title: ev.CalendarString,
                        allDay: false,
                        start: new Date(parseInt(ev.StartDateTime.substr(6))),
                        end: new Date(parseInt(ev.EndDateTime.substr(6))),
                        className: 'cal-event-quote clickable',
                        editable: true,
                        type: 1,
                        objectId: ev.JobSiteID
                    });
                });
            }
            debugger;
            if (EventType == 0 || EventType == 2) {

                var wAppts = Ajax_CallService(
                    'Services/Web/Calendar.asmx',
                    'GetWorkEvents',
                    {
                        Start: start.toDateString(),
                        End: end.toDateString(),
                        StaffID: StaffID
                    },
                    false);
                wAppts.forEach(function (ev) {
                    var StartEnd = new Date(parseInt(ev.StartDateTime.substr(6)));
                    StartEnd = addMinutes(StartEnd, 30);
                    var EndEnd = new Date(parseInt(ev.EndDateTime.substr(6)));
                    EndEnd = addMinutes(EndEnd, 30);
                    console.log(ev);
                    var StartTimeDay1Date = toDateFromJson(ev.StartTimeDay1New);
                    var ConnectiveDayEndTimeDate = toDateFromJson(ev.ConnectiveDayEndTimeNew);
                    var LastDayEndTimeNewDate = toDateFromJson(ev.LastDayEndTimeNew);
                    
                    var total = ev.Color.R + ev.Color.G + ev.Color.B;
                    var textCol = total > (255 * 3) / 2 ? 'black' : 'white';
                    if (!ev.IsRecurring) {   // Regular Multiple Date Book
                        EventsList.push({
                            id: ev.Id,
                            workPlanId: ev.WorkPlanId,
                            data: ev,
                            title: ev.CalendarString,
                            allDay: false,
                            start: new Date(parseInt(ev.StartDateTime.substr(6))),
                            end: new Date(parseInt(ev.EndDateTime.substr(6))),
                            className: 'cal-event-work clickable',
                            editable: true,
                            type: 2,
                            objectId: ev.JobSiteId,
                            backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                            textColor: textCol

                        });
                    }
                    else {
                        if (ev.EveryDay && ev.RepeatTypeId == 1 && (ev.EveryFor == undefined || ev.EveryFor == 0 || ev.EveryFor == -1 || ev.EveryFor == null)) {   //Daily EveryDay
                            EventsList.push({
                                id: ev.Id,
                                workPlanId: ev.WorkPlanId,
                                data: ev,
                                title: ev.CalendarString,
                                allDay: false,
                                start: new Date(parseInt(ev.StartDateTime.substr(6))),
                                end: new Date(parseInt(ev.EndDateTime.substr(6))),
                                className: 'cal-event-work clickable',
                                editable: true,
                                type: 2,
                                objectId: ev.JobSiteId,
                                backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                textColor: textCol

                            });
                        }
                        else if (!ev.EveryDay && ev.RepeatTypeId == 1) {   //Daily With Skip
                            var DateList = [];
                            var DateDiff = daysBetween(StartEnd, EndEnd);
                            var StartDt = null;
                            var EndDt = null
                            var DateEndCount = parseInt(DateDiff / 2);
                            for (var i = 0; i <= DateEndCount; i++) {
                                if (i == 0) {
                                    StartDt = StartEnd;
                                    StartDt = new Date(StartDt);
                                    var year = StartDt.getFullYear();
                                    var month = StartDt.getMonth();
                                    var day = StartDt.getDate();
                                    var hours = StartTimeDay1Date.getHours();
                                    var min = StartTimeDay1Date.getMinutes();
                                    var sec = StartTimeDay1Date.getSeconds();
                                    StartDt = new Date(year, month, day, hours, min, sec);
                                    EndDt = new Date(StartDt);
                                    EndDt = new Date(EndDt.getFullYear(), EndDt.getMonth(), EndDt.getDate(), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                }
                                else {
                                    StartDt = new Date(StartDt.setDate(StartDt.getDate() + parseInt(ev.EveryFor) + 1));
                                    StartDt = new Date(StartDt.getFullYear(), StartDt.getMonth(), StartDt.getDate(), StartTimeDay1Date.getHours(), StartTimeDay1Date.getMinutes(), StartTimeDay1Date.getSeconds(), 0);
                                    if (i == DateEndCount) {
                                        EndDt = new Date(StartDt);
                                        EndDt = new Date(EndDt.getFullYear(), EndDt.getMonth(), EndDt.getDate(), LastDayEndTimeNewDate.getHours(), LastDayEndTimeNewDate.getMinutes(), LastDayEndTimeNewDate.getSeconds(), 0);
                                    }
                                    else {
                                        EndDt = new Date(StartDt);
                                        EndDt = new Date(EndDt.getFullYear(), EndDt.getMonth(), EndDt.getDate(), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                    }
                                   
                                }
                                EventsList.push({
                                    id: ev.Id,
                                    workPlanId: ev.WorkPlanId,
                                    data: ev,
                                    title: ev.CalendarString,
                                    allDay: false,
                                    start: new Date(StartDt),
                                    end: new Date(EndDt),
                                    className: 'cal-event-work clickable',
                                    editable: true,
                                    type: 2,
                                    objectId: ev.JobSiteId,
                                    backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                    textColor: textCol

                                });

                            }
                        }
                        else if ((ev.EveryFor == 0 || ev.EveryFor == -1 || ev.EveryFor == null) && ev.RepeatTypeId == 2) {   //Weekly With No Skip with selected Days

                            var WeekDays = ev.WeekDay.split(',');
                            var StartDt = null;
                            var EndDt = null
                            
                            var StartDayList = [];
                            var EndDayList = [];
                            for (var i = StartEnd; i <= EndEnd;) {
                                var IsWeDay = IsWeekDay(WeekDays, i);
                                if (IsWeDay) {
                                    var StartTime = new Date(i.getFullYear(), i.getMonth(), i.getDate(), StartTimeDay1Date.getHours(), StartTimeDay1Date.getMinutes(), StartTimeDay1Date.getSeconds(), 0);
                                    StartDayList.push(StartTime);
                                    EndTime = new Date(i.getFullYear(), i.getMonth(), i.getDate(), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                    EndDayList.push(EndTime);
                                }
                                i.setTime(i.getTime() + 1000 * 60 * 60 * 24);
                            }
                            var LastDateIndex;
                            if (EndDayList.length > 0 && StartDayList.length > 0) {
                                LastDateIndex = EndDayList.length - 1;
                                LastDateTime = EndDayList[LastDateIndex];
                                if (EndDayList.length > 1) {
                                    EndDayList[LastDateIndex] = new Date(LastDateTime.getFullYear(), LastDateTime.getMonth(), LastDateTime.getDate(), LastDayEndTimeNewDate.getHours(), LastDayEndTimeNewDate.getMinutes(), LastDayEndTimeNewDate.getSeconds(), 0);
                                }
                                $(StartDayList).each(function (i, o) {
                                    EventsList.push({
                                        id: ev.Id,
                                        workPlanId: ev.WorkPlanId,
                                        data: ev,
                                        title: ev.CalendarString,
                                        allDay: false,
                                        start: new Date(o),
                                        end: new Date(EndDayList[i]),
                                        className: 'cal-event-work clickable',
                                        editable: true,
                                        type: 2,
                                        objectId: ev.JobSiteId,
                                        backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                        textColor: textCol

                                    });
                                });
                            }

                        }
                        else if (ev.EveryFor != 0 && ev.EveryFor != null && ev.EveryFor != -1 && ev.RepeatTypeId == 2) {   //Weekly With Skip with selected Days
                            debugger;
                            var WeekDays = ev.WeekDay.split(',');
                            var StartDt = null;
                            var EndDt = null;
                            var StartDayList = [];
                            var EndDayList = [];
                            for (var i = StartEnd; i <= EndEnd;) {
                                var WeekEdnDate = null;
                                var x = new Date(StartEnd);
                                WeekEdnDate = new Date(new Date(x.setDate(x.getDate() + 7 - x.getDay())));
                                for (var j = StartEnd; j < WeekEdnDate;) {
                                    var IsWeDay = IsWeekDay(WeekDays, j);
                                    if (IsWeDay) {
                                        var StartTime = new Date(j.getFullYear(), j.getMonth(), j.getDate(), StartTimeDay1Date.getHours(), StartTimeDay1Date.getMinutes(), StartTimeDay1Date.getSeconds(), 0);
                                        var EndTime = new Date(j.getFullYear(), j.getMonth(), j.getDate(), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                        StartDayList.push(StartTime);
                                        EndDayList.push(EndTime);
                                        
                                    }
                                    j.setTime(j.getTime() + 1000 * 60 * 60 * 24);
                                }
                                i.setTime(i.getTime() + 1000 * 60 * 60 * 24 * (7 * parseInt(ev.EveryFor)));
                            }
                            var LastDateIndex;
                            if (EndDayList.length > 0 && StartDayList.length > 0) {
                                debugger;
                                LastDateIndex = EndDayList.length - 1;
                                LastDateTime = EndDayList[LastDateIndex];
                                if (EndDayList.length > 1) {
                                    EndDayList[LastDateIndex] = new Date(LastDateTime.getFullYear(), LastDateTime.getMonth(), LastDateTime.getDate(), LastDayEndTimeNewDate.getHours(), LastDayEndTimeNewDate.getMinutes(), LastDayEndTimeNewDate.getSeconds(), 0);
                                }
                                $(StartDayList).each(function (i, o) {
                                    EventsList.push({
                                        id: ev.Id,
                                        workPlanId: ev.WorkPlanId,
                                        data: ev,
                                        title: ev.CalendarString,
                                        allDay: false,
                                        start: new Date(o),
                                        end: new Date(EndDayList[i]),
                                        className: 'cal-event-work clickable',
                                        editable: true,
                                        type: 2,
                                        objectId: ev.JobSiteId,
                                        backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                        textColor: textCol

                                    });
                                });
                            }
                        }
                        else if ((ev.EveryFor == 0 || ev.EveryFor == -1 || ev.EveryFor == null) && ev.RepeatTypeId == 3) {   //Monthly NO Skip
                            debugger;
                            var StartDayList = [];
                            var EndDayList = [];
                            for (var i = StartEnd; i <= EndEnd;) {
                                var x = new Date(StartEnd);
                                var MonthEdnDate = new Date(new Date(StartEnd.getFullYear(), StartEnd.getMonth() + 1, 0));
                                for (var j = StartEnd; j <= MonthEdnDate;) {
                                    var StartTime = new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay), StartTimeDay1Date.getHours(), StartTimeDay1Date.getMinutes(), StartTimeDay1Date.getSeconds(), 0);
                                    StartDayList.push(StartTime);
                                    var EndTime = new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                    EndDayList.push(EndTime);
                                    /*EventsList.push({
                                        id: ev.Id,
                                        workPlanId: ev.WorkPlanId,
                                        data: ev,
                                        title: ev.CalendarString,
                                        allDay: false,
                                        start: new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay)),
                                        end: new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay)),
                                        className: 'cal-event-work clickable',
                                        editable: true,
                                        type: 2,
                                        objectId: ev.JobSiteId,
                                        backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                        textColor: textCol
                                    });*/
                                    break;
                                }

                                //i.setTime(i.getTime() + 1000 * 60 * 60 * 24 * (30 * parseInt(ev.EveryFor)));
                                i.setMonth(i.getMonth() + 1);

                            }
                            var LastDateIndex;
                            if (EndDayList.length > 0 && StartDayList.length > 0) {
                                LastDateIndex = EndDayList.length - 1;
                                LastDateTime = EndDayList[LastDateIndex];
                                if (EndDayList.length > 1) {
                                    EndDayList[LastDateIndex] = new Date(LastDateTime.getFullYear(), LastDateTime.getMonth(), LastDateTime.getDate(), LastDayEndTimeNewDate.getHours(), LastDayEndTimeNewDate.getMinutes(), LastDayEndTimeNewDate.getSeconds(), 0);
                                }
                                $(StartDayList).each(function (i, o) {
                                    EventsList.push({
                                        id: ev.Id,
                                        workPlanId: ev.WorkPlanId,
                                        data: ev,
                                        title: ev.CalendarString,
                                        allDay: false,
                                        start: new Date(o),
                                        end: new Date(EndDayList[i]),
                                        className: 'cal-event-work clickable',
                                        editable: true,
                                        type: 2,
                                        objectId: ev.JobSiteId,
                                        backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                        textColor: textCol

                                    });
                                });
                            }
                        }

                        else if (ev.EveryFor != 0 && ev.EveryFor != null && ev.EveryFor != -1 && ev.RepeatTypeId == 3) {   //Monthly with Skip
                            var StartDayList = [];
                            var EndDayList = [];
                            for (var i = StartEnd; i <= EndEnd;) {
                                var x = new Date(StartEnd);
                                var MonthEdnDate = new Date(new Date(StartEnd.getFullYear(), StartEnd.getMonth() + 1, 0));
                                for (var j = StartEnd; j <= MonthEdnDate;) {
                                    var StartTime = new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay), StartTimeDay1Date.getHours(), StartTimeDay1Date.getMinutes(), StartTimeDay1Date.getSeconds(), 0);
                                    StartDayList.push(StartTime);
                                    var EndTime = new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                    EndDayList.push(EndTime);
                                    /*EventsList.push({
                                        id: ev.Id,
                                        workPlanId: ev.WorkPlanId,
                                        data: ev,
                                        title: ev.CalendarString,
                                        allDay: false,
                                        start: new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay)),
                                        end: new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay)),
                                        className: 'cal-event-work clickable',
                                        editable: true,
                                        type: 2,
                                        objectId: ev.JobSiteId,
                                        backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                        textColor: textCol
                                    });*/
                                    break;
                                }

                                //i.setTime(i.getTime() + 1000 * 60 * 60 * 24 * (30 * parseInt(ev.EveryFor)));
                                i.setMonth(i.getMonth() + 1 + ev.EveryFor);

                            }
                            var LastDateIndex;
                            if (EndDayList.length > 0 && StartDayList.length > 0) {
                                LastDateIndex = EndDayList.length - 1;
                                LastDateTime = EndDayList[LastDateIndex];
                                if (EndDayList.length > 1) {
                                    EndDayList[LastDateIndex] = new Date(LastDateTime.getFullYear(), LastDateTime.getMonth(), LastDateTime.getDate(), LastDayEndTimeNewDate.getHours(), LastDayEndTimeNewDate.getMinutes(), LastDayEndTimeNewDate.getSeconds(), 0);
                                }
                                $(StartDayList).each(function (i, o) {
                                    EventsList.push({
                                        id: ev.Id,
                                        workPlanId: ev.WorkPlanId,
                                        data: ev,
                                        title: ev.CalendarString,
                                        allDay: false,
                                        start: new Date(o),
                                        end: new Date(EndDayList[i]),
                                        className: 'cal-event-work clickable',
                                        editable: true,
                                        type: 2,
                                        objectId: ev.JobSiteId,
                                        backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                        textColor: textCol

                                    });
                                });
                            }
                        }
                    }
                });
            }

            if (EventType == 0 || EventType == 3) {
                var gAppts = Ajax_CallService(
                    'Services/Web/Calendar.asmx',
                    'GetGenericEvents',
                    {
                        Start: start.toDateString(),
                        End: end.toDateString(),
                        StaffID: StaffID
                    },
                    false);
                gAppts.forEach(function (ev) {
                    EventsList.push({
                        id: ev.Id,
                        data: ev,
                        title: ev.CalendarString,
                        allDay: ev.AllDay,
                        start: new Date(parseInt(ev.StartDateTime.substr(6))),
                        end: new Date(parseInt(ev.EndDateTime.substr(6))),
                        className: 'cal-event-generic clickable',
                        editable: true,
                        type: 0,
                        objectId: ev.Id
                    });
                });
            }

            callback(EventsList);
        },

        dayClick: function (date, allDay, jsEvent, view) {
            if (!BookedWork) {
                Dialog_OpenPage(
                    'Pick Event',
                    'Dialogs/PickEvent.aspx?EventId=-1&date=' + date.toDateString().replace(/ /g, '_') + '_' + date.toLocaleTimeString().replace(/ /g, '_') + '&allDay=' + allDay.toString(),
                    'content',
                    500,
                    300,
                    true,
                    {
                        'BookWork': {
                            text: 'Book Work',
                            class: 'ui-dialog-button',
                            click: function () {
                                BookAcceptedJobsDialog();
                                $(this).dialog('close');
                            }
                        },
                        'Generic': {
                            text: 'Generic',
                            class: 'ui-dialog-button',
                            click: function () {
                                LoadGenericDialog(date, allDay);
                                $(this).dialog('close');


                            }
                        },
                        'Close': {
                            text: 'Cancel',
                            class: 'ui-dialog-button',
                            click: function () {
                                $(this).dialog('close');
                            }
                        }
                    },
                    'Content/js/CalendarGenericEdit.js?V=3.23.5',
                    function () {
                        $('#txtDateFrom').datetimepicker({ format: 'd/m/Y H:i', step: 30 });
                        $('#txtDateTo').datetimepicker({ format: 'd/m/Y H:i', step: 30 });
                        $('#txtDayFrom').datetimepicker({ format: 'd/m/Y', timepicker: false, closeOnDateSelect: true });
                        $('#txtDayTo').datetimepicker({ format: 'd/m/Y', timepicker: false, closeOnDateSelect: true });
                    }
                    );
            }
            else {
                
                var CreatedQuoteDate = new Date(parseInt(AcceptedQuote.DateCreated.substr(6)))
                var target = '/BookDates.aspx?WorkId=' + AcceptedQuote.Id + '&QuoteId=' + AcceptedQuote.QuoteId + '&WorkItmId=200&SiteId=' + AcceptedQuote.SiteId + '&ClientSiteId=' + AcceptedQuote.ClientSiteId + '&DateCreated=' + CreatedQuoteDate.format('dd/MM/yy') + '&JobId=' + AcceptedQuote.ParentId + '&CalendarDate=' + date.format('yyyy/MM/dd');
                AcceptedQuote = null;
                BookedWork = false;
                window.location = target;
            }
            /*Dialog_OpenPage(
               'Generic Event',
               'Dialogs/CalendarGenericEdit.aspx?EventId=-1&date=' + date.toDateString().replace(/ /g, '_') + '_' + date.toLocaleTimeString().replace(/ /g, '_') + '&allDay=' + allDay.toString(),
               'content',
               500,
               300,
               true,
               {
                   'Save': {
                       text: 'Save',
                       class: 'ui-dialog-button',
                       click: function () {
                           allDay = document.getElementById('chkAllDay').checked;
                           SaveGenericEvent(-1, allDay);
                           $('#pnlCalendar').fullCalendar('next');
                           $('#pnlCalendar').fullCalendar('today');
                           $(this).dialog('close');
                       }
                   },
                   'Close': {
                       text: 'Close',
                       class: 'ui-dialog-button',
                       click: function () {
                           $(this).dialog('close');
                       }
                   }
               },
               'Content/js/CalendarGenericEdit.js?V=3.23.5',
               function () {
                   $('#txtDateFrom').datetimepicker({ format: 'd/m/Y H:i', step: 30 });
                   $('#txtDateTo').datetimepicker({ format: 'd/m/Y H:i', step: 30 });
                   $('#txtDayFrom').datetimepicker({ format: 'd/m/Y', timepicker: false, closeOnDateSelect: true });
                   $('#txtDayTo').datetimepicker({ format: 'd/m/Y', timepicker: false, closeOnDateSelect: true });
               }
               );*/
        },
        eventClick: function (event, jsEvent, view) {
            console.log(event);
            switch (event.type) {
                case 0: // Generic
                    Dialog_OpenPage(
                        'Generic Event',
                        'Dialogs/CalendarGenericEdit.aspx?EventId=' + event.objectId,
                        'content',
                        500,
                        300,
                        true,
                        {
                            'Delete': {
                                text: 'Delete',
                                class: 'ui-dialog-button',
                                click: function () {
                                    if (confirm('Are you sure you want to delete this entry?')) {
                                        DeleteGenericEvent(event.objectId);
                                        $('#pnlCalendar').fullCalendar('next');
                                        $('#pnlCalendar').fullCalendar('today');
                                        $(this).dialog('close');
                                    }

                                }
                            },
                            'Save': {
                                text: 'Save',
                                class: 'ui-dialog-button',
                                click: function () {
                                    event.allDay = document.getElementById('chkAllDay').checked;
                                    SaveGenericEvent(event.objectId, event.allDay);
                                    $('#pnlCalendar').fullCalendar('next');
                                    $('#pnlCalendar').fullCalendar('today');
                                    $(this).dialog('close');
                                }
                            },
                            'Close': {
                                text: 'Close',
                                class: 'ui-dialog-button',
                                click: function () {
                                    $(this).dialog('close');
                                }
                            }
                        },
                        'Content/js/CalendarGenericEdit.js?V=3.23.5',
                        function () {
                            CalendarGeneric_Load(event);
                            $('#txtDateFrom').datetimepicker({ format: 'd/m/y H:i', step: 30 });
                            $('#txtDateTo').datetimepicker({ format: 'd/m/y H:i', step: 30 });
                            $('#txtDayFrom').datetimepicker({
                                format: 'd/m/y',
                                timepicker: false,
                                closeOnDateSelect: true
                            });
                            $('#txtDayTo').datetimepicker({
                                format: 'd/m/y',
                                timepicker: false,
                                closeOnDateSelect: true
                            });
                        });
                    break;

                case 1: // Quote
                    var JobSite = Ajax_CallService(
                        'Services/Web/WorkFlow.asmx',
                        'GetJobSite',
                        {
                            JobSiteID: event.objectId
                        },
                        false);

                    var isEdit = false;
                    // opens dialog that displays Quote appointment information and gives the user the controls as stated on the button names
                    Dialog_OpenPage('Appointment Management',
                        'Dialogs/CalendarEditJobAppointment.aspx?SurveyorID=' + event.data.SurveyorID.toString() + '&AppId=' + event.id.toString(),
                        'content',
                        400,
                        630,
                        true,
                        {
                            'View': {
                                text: 'View',
                                class: 'ui-dialog-button',
                                click: function () {
                                    window.location = '/Job.aspx?JobID=' + JobSite.JobId;
                                }
                            },
                            'Edit': {
                                text: 'Edit',
                                class: 'ui-dialog-button',
                                click: function () {
                                    isEdit = true;
                                    EditableAppointment(JobSite, isEdit, event);
                                }
                            },
                            'Delete': {
                                text: 'Delete',
                                class: 'ui-dialog-button',
                                click: function () {
                                    ApptDelete(event.id);
                                    UpdateFilters();
                                    $(this).dialog('close');
                                }
                            },
                            'Cancel': {
                                text: 'Cancel',
                                class: 'ui-dialog-button',
                                click: function () {
                                    $(this).dialog('close');
                                }
                            }
                        },
                        'Content/js/CalendarEditJobAppointment.js?V=3.23.5',
                        function () {
                            LoadAppointment(JobSite, isEdit, event);
                        });
                    break;

                case 2: // Work
                    window.location = GetBookWorkRedirectForJobSiteID(event.workPlanId, event.objectId);
                    break;
            }
        },
        eventMouseover: function (event, jsEvent, view) {
            if (event.type == 2) { // work
                timer = setTimeout(function () {
                    var summary = Ajax_CallService(
                    'Services/Web/WorkProgramming.asmx',
                    'GetWorkSummaryForCalendar',
                    {
                        jobSiteId: event.objectId
                    },
                    false);
                    debugger;
                    var StartTimeDay1Date = toDateFromJson(event.data.StartTimeDay1New);
                    var strStartTimeDay1Date = StartTimeDay1Date.format('hh:mm tt');


                    var ConnectiveDayEndTimeDate = toDateFromJson(event.data.ConnectiveDayEndTimeNew);
                    var strConnectiveDayEndTimeDate = ConnectiveDayEndTimeDate.format('hh:mm tt');


                    var LastDayEndTimeNewDate = toDateFromJson(event.data.LastDayEndTimeNew);
                    var strLastDayEndTimeNewDate = LastDayEndTimeNewDate.format('hh:mm tt');
                    if (event.data.IsRecurring || event.data.IsMultiDates) {

                        tooltip = '<div class="tooltiptopicevent" style="max-width: 220px; width:auto;height:auto;background:#fff;border: solid grey 1px; position:absolute;z-index:10001;padding:10px 10px 10px 10px; line-height: 100%; box-shadow: 10px 10px 5px #888888;">' + '<span style="font-size: 0.9em; font-weight: bold;">' + event.title + '</span><br/><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Total Work Value:</span><br/>' + CurrentCurrency + (summary.Value + summary.VatVal).format() + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Start:</span> ' + strStartTimeDay1Date + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">End:</span> ' + strConnectiveDayEndTimeDate + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Last Day:</span> ' + strLastDayEndTimeNewDate + '</div>';
                    }
                    else {
                        tooltip = '<div class="tooltiptopicevent" style="max-width: 220px; width:auto;height:auto;background:#fff;border: solid grey 1px; position:absolute;z-index:10001;padding:10px 10px 10px 10px; line-height: 100%; box-shadow: 10px 10px 5px #888888;">' + '<span style="font-size: 0.9em; font-weight: bold;">' + event.title + '</span><br/><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Total Work Value:</span><br/>' + CurrentCurrency  + (summary.Value + summary.VatVal).format() + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Start:</span><br/>' + event.start.format("dd-MM-yyyy hh:mm tt") + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">End:</span><br/>' + event.end.format("dd-MM-yyyy hh:mm tt") + '</div>';
                    }
                    
                    $("body").append(tooltip);
                    $(this).css('z-index', 10000);
                    //Set tooltip to position of mouse
                    $('.tooltiptopicevent').css('top', jsEvent.pageY + 10);
                    $('.tooltiptopicevent').css('left', jsEvent.pageX + 20);
                    $('.tooltiptopicevent').hide().fadeIn('slow');
                }, 800);

                $(this).mousemove(function (e) {
                    $('.tooltiptopicevent').fadeOut();
                });
            }
        },
        eventMouseout: function (event, jsEvent, view) {
            clearTimeout(timer);
            $(this).css('z-index', 8);
            $('.tooltiptopicevent').fadeOut('slow', removeToolTip);
            function removeToolTip() { $('.tooltiptopicevent').remove(); }
        },
        eventResize: function (event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
            if (confirm('Are you sure you want to edit the selected appointment?\nAll Emails will be resent.\n[FromDate Selector] - [ToDate Selector]')) {
                //TODO: Edit Appointment
            } else {
                revertFunc();
            }
        },
        eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
            if (event.type == 2 && (event.data.IsRecurring || event.data.IsMultiDates)) {
                alert("You cannot drag and drop this booking, please edit to make changes to the booking date");
                revertFunc();
            }
            else if(confirm('Are you sure you want to edit the selected appointment?\nAll Emails will be resent.\n[FromDate Selector] - [ToDate Selector]')) {
                //TODO: Edit Appointment
                debugger;
                console.log(event);
                if (event.type == 0) {
                    saveGenericEventOnDrag(event);
                }
                else if (event.type == 1) {
                    saveQuoteEventOnDrag(event);
                }
                else if (event.type == 2) {
                    SaveWorkPlaneOnDropEvent(event);
                 
                }
            } else {
                revertFunc();
            }
        },
        firstHour: 6,
        allDayText: 'All Day',
        axisFormat: 'HH:mm',
        titleFormat: {
            week: 'MMMM d[ yyyy]{ &#8212;[ MMM] d yyyy}',
            day: 'dddd, dd MMMM yyyy'
        },
        columnFormat: {
            week: 'ddd dd/MM',
            day: 'dddd, dd MMMM'
        },
        timeFormat: '',
        slotMinutes: '60'
    });
    var BookWork = GetURLParameter('BookWork');
    if (BookWork == 1) {
        updateQueryStringParam("BookWork", "2");
       debugger;
       var QuoteId = GetURLParameter('QuoteId');
       var result = Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetJobPendingWork',
        {
            Month: -1
        },
        false
        );
       var refId;
       result.Results.forEach(function (c) {
           if (c.QuoteId == QuoteId) {
               refId = c.Reference;
               AcceptedQuote = c;
               BookedWork = true;
           }
       });
   
       DisplayAlertMessage("To being booking this quotation navigate through the calendar until you find a day that " +
           "you want to start the work. Then use your mouse to click on the calendar and open the booking form.", refId);
    }
}

function LoadGenericDialog(date, allDay) {
    Dialog_OpenPage(
                'Generic Event',
                'Dialogs/CalendarGenericEdit.aspx?EventId=-1&date=' + date.toDateString().replace(/ /g, '_') + '_' + date.toLocaleTimeString().replace(/ /g, '_') + '&allDay=' + allDay.toString(),
                'content',
                500,
                300,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            allDay = document.getElementById('chkAllDay').checked;
                            SaveGenericEvent(-1, allDay);
                            $('#pnlCalendar').fullCalendar('next');
                            $('#pnlCalendar').fullCalendar('today');
                            $(this).dialog('close');
                        }
                    },
                    'Close': {
                        text: 'Close',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/CalendarGenericEdit.js?V=3.23.5',
                function () {
                    $('#txtDateFrom').datetimepicker({ format: 'd/m/Y H:i', step: 30 });
                    $('#txtDateTo').datetimepicker({ format: 'd/m/Y H:i', step: 30 });
                    $('#txtDayFrom').datetimepicker({ format: 'd/m/Y', timepicker: false, closeOnDateSelect: true });
                    $('#txtDayTo').datetimepicker({ format: 'd/m/Y', timepicker: false, closeOnDateSelect: true });
                }
                );
}

//Check isWeekDay
function IsWeekDay(lstWeekDay, currentDate) {

    for (var i = 0; i < lstWeekDay.length; i++) {
        var day = currentDate.getDay();
        if (day == 0) {
            day = 7;
        }
        if (day == parseInt(lstWeekDay[i])) {
            return true;
            break;
        }
    }
    return false;
}

// function called when Quote appointment is in edit stage (removes Edit and Delete buttons and adds Save button)
function EditableAppointment(JobSite, isEdit, event) {
    Dialog_OpenPage('Appointment Management',
    'Dialogs/CalendarEditJobAppointment.aspx?SurveyorID=' + event.data.SurveyorID.toString() + '&AppId=' + event.id.toString(),
    'content',
    400,
    630,
    true,
    {
        'View': {
            text: 'View',
            class: 'ui-dialog-button',
            click: function () {
                window.location = '/Job.aspx?JobID=' + JobSite.JobId;
            }
        },
        'Save': {
            text: 'Save',
            class: 'ui-dialog-button',
            click: function () {
                ApptSave(event.id, event.objectId);
                UpdateFilters();
                $(this).dialog('close');
            }
        },
        'Cancel': {
            text: 'Cancel',
            class: 'ui-dialog-button',
            click: function () {
                $(this).dialog('close');
            }
        }
    },
    'Content/js/CalendarEditJobAppointment.js?V=3.23.5',
    function () {
        LoadAppointment(JobSite, isEdit, event);
    });
}

function DeleteGenericEvent(eventId) {
    var result = Ajax_CallService(
         'Services/Web/Calendar.asmx',
         'DeleteGenericEvent',
         {
             eventId: eventId
         },
         false);

    if (!result)
        alert("Error: Unable to delete appointment.");
}

function SaveGenericEvent(EventID, AllDay) {
    var EventName = document.getElementById('txtEventName').value;
    var Comment = document.getElementById('txtEventComment').value;
    var ddlEventType = document.getElementById('ddlEventType_DLG');
    var EventTypeID = ddlEventType.options[ddlEventType.selectedIndex].value;
    var ddlEventStaff = document.getElementById('ddlEventStaff_DLG');
    var EventStaffID = ddlEventStaff.options[ddlEventStaff.selectedIndex].value;
    var startDate;
    var endDate;
    if (AllDay) {
        startDate = document.getElementById('txtDayFrom').value;
        endDate = document.getElementById('txtDayTo').value;
    } else {
        startDate = document.getElementById('txtDateFrom').value;
        endDate = document.getElementById('txtDateTo').value;
    }
    var result = Ajax_CallService(
         'Services/Web/Calendar.asmx',
         'SaveGenericEvent',
         {
             EventID: EventID,
             Start: startDate,
             End: endDate,
             AllDay: AllDay,
             Name: EventName,
             Comment: Comment,
             EventType: EventTypeID,
             StaffID: EventStaffID
         },
         false);

    if (!result)
        alert("An error occured when trying to save the event");
}

// Returns a redirect URL for a given JobSite ID by identifying the Job and the
// the Quote for the identified Job. This information is then used to build a
// redirect for the BookWork page.
/*function GetBookWorkRedirectForJobSiteID(workPlanId, jobSiteId) {
    return '/BookWork.aspx?WorkPlanID=' + workPlanId + '&ReferrerSiteID=' + jobSiteId;
}*/

function GetBookWorkRedirectForJobSiteID(workPlanId, jobSiteId) {
    return '/BookDatesEdit.aspx?WorkPlanId=' + workPlanId + '&ReferrerSiteId=' + jobSiteId;
}

function saveGenericEventOnDrag(event) {
    var startDate = event._start.format('dd/MMM/yy HH:mm');
    var endDate;
    if (event._end != null) {
        endDate = event._end.format('dd/MMM/yy HH:mm');
    }
    else {
        endDate = event._start.format('dd/MMM/yy HH:mm');
    }
    var result = Ajax_CallService(
         'Services/Web/Calendar.asmx',
         'SaveGenericEvent',
         {
             EventID: event.objectId,
             Start: startDate,
             End: endDate,
             AllDay: event.allDay,
             Name: event.data.Name,
             Comment: event.data.Comment,
             EventType: event.type,
             StaffID: event.data.StaffID
         },
         false);

    if (!result) {
        alert("An error occured when trying to save the event");
    }

}

function saveQuoteEventOnDrag(event) {
    var startDate = event._start.format('dd/MMM/yy HH:mm');
    var endDate;
    if (event._end != null) {
        endDate = event._end.format('dd/MMM/yy HH:mm');
    }
    else {
        endDate = event._start.format('dd/MMM/yy HH:mm');
    }

    console.log(event);
    var result = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'SaveQuoteAppointment',
        {
            ApptID: event.id,
            JobSiteID: event.objectId,
            SurveyorID: event.data.SurveyorID,
            StartDateTime: startDate.toLocaleString(),
            EndDateTime: endDate.toLocaleString(),
            Completed: event.data.Completed,
            ReDownload: "false",
            TemplateId: event.data.TemplateId
        },
        false);

    if (!result) {
        alert("An error occured when trying to save the event");
    }
}

function SaveWorkPlaneOnDropEvent(event) {
    var startDate = event._start.format('dd/MMM/yy HH:mm');
    var endDate;
    if (event._end != null) {
        endDate = event._end.format('dd/MMM/yy HH:mm');
    }
    else {
        endDate = event._start.format('dd/MMM/yy HH:mm');
    }
    var appointment = {
        StartDateTime: startDate,
        EndDateTime: endDate,
        Comments: event.data.Comments,
        SkipDays: event.data.SkipDays != null? event.data.SkipDays:0,
        WorkPlanId: event.data.WorkPlanId,
        JobSiteId: event.data.JobSiteId,
        TeamId: event.data.TeamId,
        id: event.data.Id
    };
    console.log(appointment);
    var result = Ajax_CallService(
                'Services/Web/WorkProgramming.asmx',
                'SaveOnlyAppointmentOfWorkPlan',
                {
                    appointment: appointment
                },
                    false,
                    null
                );
    if (!result) {
        alert("An error occured when trying to save the Work Plan");
    }

}

// function called when Quote appointment is in edit stage (removes Edit and Delete buttons and adds Save button)
function BookAcceptedJobsDialog() {
    Dialog_OpenPage('Accepted Jobs Pending Start Date',
    'Dialogs/AcceptedQuote.aspx',
    'content',
    900,
    630,
    true,
    {
        'Cancel': {
            text: 'Cancel',
            class: 'ui-dialog-button',
            click: function () {
                $(this).dialog('close');
            }
        }
    },
    'Content/js/CalendarEditJobAppointment.js?V=3.23.5',
    function () {
        PopulateAcceptedQuotePanal();
    });
}

function PopulateSectionLabel(TotalAmount) {

    var hdrLabel = document.getElementById("lblTotalValue");
    try {
        hdrLabel.textContent = TotalAmount.toString();
    }
    catch (err) {
        console.log("Error:" + err.toString());
    }
}

function daysBetween(date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
}
function addDays(theDate, days) {
    return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
}

function PopulateAcceptedQuotePanal() {

    var QuoteStatus = document.getElementById("ddlQuoteStatus").value;
    var QuoteMonth = document.getElementById("ddlQuoteDates").value;
    console.log(QuoteStatus);
    console.log(QuoteMonth);
    if (QuoteStatus == 0)
    {
        Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetJobPendingWork',
        {
            Month: QuoteMonth
        },
        true,
        BindJobGrid
       );
    }
    else if (QuoteStatus == 1) {
        Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetJobNotAcceptedWork',
        {
            Month: QuoteMonth
        },
        true,
        BindJobGrid
       );
    }
    else if (QuoteStatus == 2) {
        Ajax_CallService(
        'Services/Web/Dashboard.asmx',
        'GetAllJobsWork',
        {
            Month: QuoteMonth
        },
        true,
        BindJobGrid
       );
    }
    
}

function SortAcceptedQuotesByRefrence(a, b) {
    return parseInt(a.Reference, 10) - parseInt(b.Reference, 10);
}
homes.sort(mycomparator);
function BindJobGrid(result) {

    var Grid = document.getElementById("grdAcceptedQuotes");
    listHtml = '';
    listHtml = '<table class="searchGrid width-full"><tr><th>Quote No</th><th>Client Name</th><th>Site Address</th><th>Contact</th><th>Job Value Per VAT</th><th>Book Work</th></tr>';
    if (result.Results == null || result.Results.length <= 0)
        listHtml += '<tr><td style="center;" colspan=6>' + result.Title + '</td></tr>';
    else {
        result.Results.sort(SortAcceptedQuotesByRefrence);
        result.Results.forEach(function (c) {
            console.log(c);
            var CreatedQuoteDate = new Date(parseInt(c.DateCreated.substr(6)))
            listHtml = listHtml + '<tr>';
            listHtml = listHtml + '<td>' + c.Reference + '</td>';
            listHtml = listHtml + '<td>' + c.ClientName + '</td>';
            listHtml = listHtml + '<td>' + c.SideBarDetail + '</td>';
            listHtml = listHtml + '<td>' + c.ContactDTL + '</td>';
            listHtml = listHtml + '<td style="vertical-align: middle; text-align:center;"> £' + c.QuoteValue.format() + '</td>';
            if (c.Id == -1) {
                listHtml = listHtml + '<td style="vertical-align: middle; text-align:center;"><button type="submit" class="button-thin padded" onclick="javascript: window.location =\'/BookJob.aspx?WorkId=' + c.Id + '&QuoteId=' + c.QuoteId + '&QuoteRef=' + c.Reference + '&SiteId=' + c.SiteId + '&ClientSiteId=' + c.ClientSiteId + '&DateCreated=' + CreatedQuoteDate.format('dd/MM/yy') + '&JobId=' + c.ParentId + '\'">View/Book Job</button></td></tr>';

            }
            else {
                listHtml = listHtml + '<td style="vertical-align: middle; text-align:center;"><button type="submit" class="button-thin padded" onclick="javascript: window.location =\'/BookDates.aspx?WorkId=' + c.Id + '&QuoteId=' + c.QuoteId + '&WorkItmId=200&SiteId=' + c.SiteId + '&ClientSiteId=' + c.ClientSiteId + '&DateCreated=' + CreatedQuoteDate.format('dd/MM/yy') + '&JobId=' + c.ParentId + '\'">View/Book Job</button></td></tr>';

            }
            
            
        });
        PopulateSectionLabel(result.AdditionalText);
        //listHtml = listHtml + '<tr><td>123</td>';
        //listHtml = listHtml + '<td>Mr. David Biggs</td>';
        //listHtml = listHtml + '<td>22 Gods Hill Devon <br/>STR 465</td>';
        //listHtml = listHtml + '<td>123456 <br/>123456 <br/> dave@biggs.com</td>';
        //listHtml = listHtml + '<td style="vertical-align: middle; text-align:center;">3000</td>';
        //listHtml = listHtml + '<td style="vertical-align: middle; text-align:center;"><button type="button" class="button-thin padded" onclick="javascript: window.location =\'/BookJob.aspx\'">View/Book Job</button></td></tr>';
    }
    listHtml = listHtml + '</table>';
    Grid.innerHTML = listHtml;
}


function DisplayAlertMessage(message,QuoteRef) {
    Dialog_OpenPage(
        'Quote: ' + QuoteRef,
        'Dialogs/Alert.aspx',
        'content',
        500,
        250,
        true,
        {
            'Close': {
                text: 'Close',
                "class": 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                    //var target = '/Calendar.aspx';
                    //window.location = target;
                }
            }
        },
        'Content/js/AlertBox.js?V=3.23.5',
        function () {
            AlertBox_Show(message);
        });
}



