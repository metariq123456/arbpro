﻿/*
    This java script file enables the user to edit the job items that have already been assigned to an invoice.

    This may be expanded to replace the JobItemEdit.js file functionality

*/
var ObjItem = null;
/*
    The Stage variable is an integer that represents the following
    0 = Quote
    1 = Work
    2 = Invoice
    3 = Credit
*/
var IntStage = 0;
var StrError = ''; //Global variable to hold the type of item for display when reporting error messages

//Function that is called to intialise a dialog
function EditItem_Load(stage, id) {
    IntStage = stage;
    switch (IntStage) {
        case 0:
            StrError = 'Quote Item';
            GetItem(id, 'GetQuoteItem');
            break;
        case 1:
            StrError = 'Work Item';
            GetItem(id, 'GetWorkItem');
            break;
        case 2:
            StrError = 'Invoice Item';
            GetItem(id, 'GetInvoiceItem');
            break;
        case 3:
            StrError = 'Credit Item';
            GetItem(id, 'GetCreditItem');
            break;
    }
}

//Function that is called to save the item to the database
function EditItem_Save() {
    var status = false;
    switch (IntStage) {
        case 0:
            status = UpdateItem('SaveQuoteItem');
            break;
        case 1:
            status = UpdateItem('SaveWorkItem');
            break;
        case 2:
            status = UpdateItem('SaveInvoiceItem');
            break;
        case 3:
            status = UpdateItem('SaveCreditItem');
            break;
    }
    return status;
}
/* SERVICE CALLS */

//Async service call to load an item from the database
function GetItem(id, endPoint) {
    try
    {
        Ajax_CallService(
          'Services/Web/WorkFlow.asmx',
          endPoint,
          {
              ItemID: id
          },
          true,
          DisplayItem
      );
    }
    catch (err) {
        DisplayMessage('pnlEditItemError', 'Error: Unable to communicate with the Arb Pro service - ' + err.toString());
    }
}

//Synchronus service call to update an object and save it, returns a success status
function UpdateItem(endPoint){
    try {
        var code = document.getElementById('txtItemTreeNo').value;
        var name = document.getElementById('txtItemTreeName').value;
        var work = document.getElementById('txtItemDescription').value;
        var value = document.getElementById('txtItemValue').value;
        var vat = document.getElementById('txtItemVatRate').value;
        //Todo: Validate the object values
        var valid = true;

        if (!valid) {
            //TODO: Highlite the fields that are invalid
            DisplayMessage('pnlEditItemError', 'Information: Unable to save ' + StrError + ' - There are invalid entries in the item');
            return false;
        }
        //Populate the object with the updated values
        ObjItem.Code = code;
        ObjItem.Name = name;
        ObjItem.Description = work;
        ObjItem.Value = value;
        ObjItem.VatRate = vat;
        
        var retVal = Ajax_CallService(
          'Services/Web/WorkFlow.asmx',
          endPoint,
          {
              obj: ObjItem
          },
          false,
          null
      );

        if (!retVal) {
            DisplayMessage('pnlEditItemError', 'Error: ' + StrError + ' was not saved correctly. No details reported.');
        }

       return retVal;
    }
    catch(err){
        DisplayMessage('pnlEditItemError', 'Error: Unable to save ' + StrError + ' - ' + err.toString());
        return false;
    }
}

//Async call back to display an item returned from the service
function DisplayItem(result) {
    
    var panel = document.getElementById('pnlEditItemContent');
    if (!result) {
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid ' + StrError);
    } else {
        ObjItem = result;
        try {
            var strHtml = '<div style="margin-left: 2px; margin-right: 2px; margin-top: 10px;">';
            strHtml += '<table class="searchGrid width-full" style="font-size: 0.7em;">';
            strHtml += '<tr><th style="width:7%;text-align: center;"> Tree No. </th><th style="width:20%;text-align: center;"> Tree Name </th><th style="width:40%;text-align: center;"> Description </th>';
            strHtml += '<th style="width:15%;text-align: center;"> Value </th><th style="width:13%;text-align: center;"> VAT Rate </th></tr>';
            strHtml += '<tr><td style="width:7%;">';
            strHtml += '<input id="txtItemTreeNo" class="jobInput width-full" type="text" style="width:85%;" name="txtItemTreeNo" value="' + ObjItem.Code + '">';
            strHtml += '</td><td style="width:20%;"><span class="ui-helper-hidden-accessible" role="status" aria-live="polite"></span>';
            strHtml += '<input id="txtItemTreeName" class="jobInput width-full ui-autocomplete-input" type="text" style="width:90%;" name="txtItemTreeName" autocomplete="off" value="' + ObjItem.Name + '">';
            strHtml += '</td><td style="width:40%;"><span class="ui-helper-hidden-accessible" role="status" aria-live="polite"></span>';
            strHtml += '<input id="txtItemDescription" class="jobInput width-full ui-autocomplete-input" type="text" style="width:95%;" name="txtItemDescription" autocomplete="off" value="' + ObjItem.Description + '">';
            strHtml += '</td><td style="width:15%;">';
            strHtml += '<input id="txtItemValue" class="jobInput float-right" type="text" style="width:80%;" name="txtItemValue" value="' + ObjItem.Value.format() + '">';
            strHtml += '<span id="Label1" class="float-right" style="padding-top: 7px; padding-right: 4px;"> ' + CurrentCurrency + ' </span>';
            strHtml += '</td><td style="width:13%;"><span id="Label2" class="float-right" style="padding-top: 7px; padding-right: 4px;"> % </span>';
            strHtml += '<input id="txtItemVatRate" class="jobInput float-right" type="text" style="width:50%;" name="txtItemVatRate" value="' + ObjItem.VatRate.format() + '">';
            strHtml += '</td></tr></table></div>';

            //Populate the panels inner html
            panel.innerHTML = strHtml;

            //hookup the autocomplete list objects
            AutoComplete('txtItemTreeName', 'ListTreeNames');
            AutoComplete('txtItemDescription', 'ListWorkDescriptions');
        }
        catch (err) {
            DisplayMessage('pnlEditItemError', 'Error: Unable to display ' + StrError + ' - ' + err.toString());
        }
    }
}

//TODO: Add map functionality to the dialog
function SetMapFunctionality(lat, long) {
    var mapOptions = {
        center: new google.maps.LatLng(Lat, Long),
        zoom: ZoomLevel,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
}
