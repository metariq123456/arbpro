﻿
var CompanyObj = null;

function Settings_Load() {
    CompanyObj = GetCompanyDetails();

    DisplayGeneralSettings();
}

function DisplayGeneralSettings() {
    var chkDisplayMarketing = document.getElementById('chkDisplayMarketing');

    if (CompanyObj) {
        chkDisplayMarketing.checked = CompanyObj.DisplayMarketing;
    } else {
        DisplayMessage('pnlGeneralMarketingSettings', 'Could not get the Marketing settings. Please refresh the page and try again.');
    }
}

function SaveGeneralSettings() {
    var chkDisplayMarketing = document.getElementById('chkDisplayMarketing');

    if (CompanyObj) {
        CompanyObj.DisplayMarketing = chkDisplayMarketing.checked;

        try {
            Ajax_CallService(
                'Services/Web/Company.asmx',
                'SaveCompany',
                {
                    Company: CompanyObj
                },
                false
            );
        } catch (e) {
            DisplayMessage('pnlGeneralMarketingSettings', 'ERROR: Unable to update the general marketing settings - ' + e.toString());
            return false;
        }

        return true;
    } else {
        DisplayMessage('pnlGeneralMarketingSettings', 'Could not update marketing settings. Please refresh the page and try again.');
        return false;
    }
}

function GetCompanyDetails() {
    var compDetails = Ajax_CallService(
        'Services/Web/Company.asmx',
        'GetCompany',
        {},
        false
    );
    return compDetails;
}