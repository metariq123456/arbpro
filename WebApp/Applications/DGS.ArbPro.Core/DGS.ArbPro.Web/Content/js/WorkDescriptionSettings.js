﻿var SelectedResultId = -1;
var IsNewItem = false;
HoursRequired = 1.00;
var WorkList = null;


function Settings_Load() {
    _workList = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'GetWorkDescriptionList',
           {
           },
           false
       );
    WorkList = _workList;
    UpdateTable();
}

function UpdateTable() {
    var ListPanel = document.getElementById('pnlTableContainer');
    var strHtml = '<table class="searchGrid width-full white">';
    for (var i = 0; i < WorkList.length; i++) {
        if (WorkList[i].Id == SelectedResultId) {
            strHtml += '<tr class="selected" onclick="ItemSelected( \''+ i + '\');">';
        }
        else {
            strHtml += '<tr onclick="ItemSelected(\'' + i + '\');">';
        }
        strHtml += '<td style="padding-left: 10px;">' + WorkList[i].Item + '</td>';
        strHtml += '</tr>';
    }
    strHtml += '</table>';
    ListPanel.innerHTML = strHtml;
}

function ItemSelected(index){//Hours, ResultId, Value) {
    var item = WorkList[index];
    IsNewItem = false;
    SelectedResultId = item.Id;
    HoursRequired = item.AverageHrsRequired;
    PopulateHiddenFields(item.Item);
    UpdateTable();
}

function PopulateHiddenFields(Value) {
    document.getElementById('btnDelete').className = 'button-thin';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtValue').value = Value;
    document.getElementById('txtHours').value = HoursRequired;
}


function NewItem() {
    SelectedResultId = -1;
    HoursRequired = 1.00;
    UpdateTable();
    document.getElementById('btnDelete').className = 'button-thin hidden';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtValue').value = '';
    document.getElementById('txtHours').value = '';
}

function SaveItem() {
    HoursRequired = document.getElementById('txtHours').value;
    try {
        DoSave = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'SaveWorkDescription',
           {
               _id: SelectedResultId,
               _description: document.getElementById('txtValue').value,
               _hours: HoursRequired
           },
           false
       );
    }
    catch (err) {
         
    }
    document.getElementById('pnlItemControls').className = 'clientField float-left hidden';
    SelectedResultId = -1;
    Settings_Load();
}


function DeleteItem() {
    DoDelete = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'DeleteWorkDescription',
           {
               Id: SelectedResultId
           },
           false
       );

    document.getElementById('pnlItemControls').className = 'clientField float-left hidden';
    Settings_Load();
}