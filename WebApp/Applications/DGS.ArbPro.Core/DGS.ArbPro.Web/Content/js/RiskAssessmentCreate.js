﻿var templates = null;

//Async function to create a risk assessment from the selected template
function CreateRiskAssessment() {
    debugger;
    var templateId = templates[document.getElementById('ddlTemplateName').selectedIndex].Id;
    var recordType = parseInt(document.getElementById('hfRecordType').value);
    var recordId = parseInt(document.getElementById('hfRecordId').value);
    var curJobSiteId = parseInt(document.getElementById('hfJobSiteId').value);
    try {
        return Ajax_CallService(
                    'Services/Web/RiskAssessment.asmx',
                    'RiskCreateForJobSiteQuoteTemplate',
                    {
                        jobSiteId : curJobSiteId, 
                        quoteId : recordId,
                        templateId : templateId
                    },
                    false,
                    null);
    }
    catch (err) {
        DisplayMessage('pnlRiskTemplate', 'Error: Unable to load risk assessment.');
    }
}

//Async service call to load search results for all available Risk Templates
function TemplateOptions_Load() {
    try {
        Ajax_CallService(
                    'Services/Web/RiskAssessment.asmx',
                    'SearchRiskTemplates',
                    {},
                    true,
                    CreateTemplateDialog);
    }
    catch (err) {
        DisplayMessage('pnlRiskTemplate', 'Error: Unable to communicate with service.');
    }
}

//Async callback function to populate the dialog
function CreateTemplateDialog(results) {
    try {
        if (!results || results.length  < 1) {
            DisplayMessage('pnlRiskTemplate', 'Error: No Risk Assessment Templates to display');
        } else {
            templates = results;
            var listJobSites = 'empty';
            var jobId = GetURLParameter('JobId');
            var quoteId = GetURLParameter('QuoteID');
            if (quoteId == 'null') {
                var quoteId = GetURLParameter('QuoteId');
            }
            if (jobId != 'null' && quoteId != 'null') {
                listJobSites = Ajax_CallService(
                    'Services/Web/WorkFlow.asmx',
                    'GetJobSiteListForRiskAssessmentForJobAndQuote',
                    {
                        quoteId: quoteId,
                        jobID: jobId
                    },
                    false);
            }
            PopulateInnerHtml(results, listJobSites);
            DisplaySelectedTemplate();
        }
    }
    catch (err) {
        // 
        DisplayMessage('pnlRiskTemplate', 'Error: Unable to load risk assessment templates.');
    }
}

//Function to build the HTML for the panel
function PopulateInnerHtml(results, listJobSites) {
    //<label Class="riskLabel">Post Code</label><label Class="riskInput">
    var strHtml = '<div id="pnlTemplateSelection">';
    strHtml += '<div class="riskField">';
    strHtml += '<label Class="riskLabel">Template</label>';
    strHtml += '<select id="ddlTemplateName" class="clientInput" onchange="DisplaySelectedTemplate();">';
    var count = results.length;
    for (var index = 0; index < count; index++) {
        if (0 == index) {
            strHtml += '<option value="' + index + '" selected="selected">' + results[index].Name + '</option>';
        } else {
            strHtml += '<option value="' + index + '">' + results[index].Name + '</option>';
        }
    }
    strHtml += '</select>';

    strHtml += '<label Class="riskLabel">Job Site:</label></br>';
    strHtml += '<select id="ddlJobSite" class="clientInput" onchange="jobSiteChange();">';
    if (listJobSites != 'empty' && listJobSites != null) {
        //set the jobsite id once on the hidden field, will be updated via event later if needed
        document.getElementById('hfJobSiteId').value = listJobSites[0].Id;
        listJobSites.forEach(function (jobSite) {
            strHtml += '<option value="'+ jobSite.Id +'">' + jobSite.SiteString + '</option>';
        });
    }
    else { strHtml += '<option value="-1" selected="selected">No job sites found</option>'; }
    strHtml += '</select></br>';

    strHtml += '</div>'; //Close the riskField
    strHtml += '</div>'; //Close pnlTemplateSelection
    strHtml += '<div id="pnlSelectedTemplateDetails">';
    strHtml += '<div class="riskField center">';
    strHtml += '<label id="lblTemplateDetails" class="clientInput">Template Details</label>';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label Class="riskLabel">Name</label>';
    strHtml += '<label id="lblTemplateName" class="clientInput">Name</label>';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label Class="riskLabel">Reference</label>';
    strHtml += '<label id="lblTemplateReference" class="clientInput">Reference</label>';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label Class="riskLabel">Description</label>';
    strHtml += '<label id="lblTemplateDescription" class="clientInput">Details</label>';
    strHtml += '</div>';
    strHtml += '</div>';
    // 
    document.getElementById('pnlRiskTemplate').innerHTML = strHtml;
}

//Drop down selection changed event handler
function DisplaySelectedTemplate() {
    var index = document.getElementById('ddlTemplateName').selectedIndex;
    if (index >= 0 && index < templates.length) {
        var t = templates[index];
        document.getElementById('lblTemplateName').innerHTML = t.Name;
        document.getElementById('lblTemplateReference').innerHTML = t.Reference;
        document.getElementById('lblTemplateDescription').innerHTML = t.Name;
    }
}

function jobSiteChange() {
    var newId = document.getElementById('ddlJobSite').value;
    document.getElementById('hfJobSiteId').value = newId;
}