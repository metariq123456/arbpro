﻿
/*
    THE FOLLOWING SECTION IS FOR INVOICE MANAGEMENT
    The invoice panel has been sub divided into 4 panels.
        -Summary - A summary of all invoices sent for the Quote
        -Create - The process of creating an Invoice by adding items to it and producing the document
        -History - Displays the current status of all invoices for the Quote and enables the user to send reminders and set a received
        -Notes - Any notes that the user may feel are relevent to the invoicing process
*/

//  Global scope variables
var ObjInvoiceSummary = null;       //  The details of all the invoices sent for the currently selected Quote
var Invoice_File = null;
var InvoiceValue = null;
var DocList = [];
var includeTreePicture = false;
var includeSitePlan = false;
var includeRiskAssessment = false;
//Function to display the required html div element and hide all others
//NOTE - this function relies on the naming convention used in the aspx file to be correct
function SwitchInvoiceTab(selectedTab) {
    for (var index = 0; index < 3; index++) {
        var tabElement = document.getElementById('tabInvoiceControls_' + index.toString());
        var pnlElement = document.getElementById('tabInvoiceDetails_' + index.toString());
        if (index == selectedTab) {
            tabElement.className = 'jobTab activeTab';
            pnlElement.className = '';
            pnlElement.innerHTML = GetWaitImageForPanel('InvoiceDetails_' + index.toString(), 'Loading Please Wait' );
        } else {
            tabElement.className = 'jobTab';
            pnlElement.className = 'hidden';
        }
    }
    //  Now all the panels have been shoen
    LoadSelectedInvoiceTab(selectedTab);
}

//Function to call the correct load function for the panel that is being displayed
function LoadSelectedInvoiceTab(selectedTab) {
    var ddlQuoteNum = document.getElementById('ddlQuoteID');
    if (ddlQuoteNum) {
        var quoteId = ddlQuoteNum.options[ddlQuoteNum.selectedIndex].value; //TODO: Get quote Id
        switch (selectedTab) {
            case 0:
                GetInvoiceSummaryForQuote(quoteId);
                break;
            case 1:
                GetInvoiceNotesForQuote();
                //GetInvoiceLatestForQuote(quoteId);
                break;
            case 2:
                GetInvoiceHistoryForQuote(quoteId);
                //GetSentInvoicesForQuote(quoteId);
                break;
            case 3:
                GetInvoiceNotesForQuote(quoteId);
                break;
            case 4:
                GetInvoiceHistoryForQuote(quoteId);
                break;
        }
    }
}

//  Async service call to get an invoice summary object
function GetInvoiceSummaryForQuote(quoteId) {
    var panel = document.getElementById('tabInvoiceDetails_0');
    try {
        Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetInvoiceSummaryForQuote',
           {
               quoteId: quoteId
           },
           true,
           DisplayInvoiceSummary
       );
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//  Async service call to get all notes for all invoices for a quote
function GetInvoiceNotesForQuote() {
    var panel = document.getElementById('tabInvoiceDetails_1');
    try {
        NotesList_Load(ObjInvoiceSummary.Id, 2, DisplayInvoiceNotes);
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//  Async service call to get all document history for all invoices for a quote
function GetInvoiceHistoryForQuote(quoteId) {
    var panel = document.getElementById('tabInvoiceDetails_2');
    try {
        Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetInvoiceHistoryForQuote',
           {
               quoteId: quoteId
           },
           true,
           DisplayQuoteInvoiceHistory
       );
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//  Aysnc callback function to display the summary of invoices for a quote
function DisplayInvoiceSummary(result) {
    var panel = document.getElementById('tabInvoiceDetails_0');
    try {
        if (!result) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
        } else {
            //  Set the scope variable
            ObjInvoiceSummary = result;
            //  Set the Quote No reference
            document.getElementById('lblInvQuoteNo').textContent = ObjInvoiceSummary.QuoteNo;

            //  Generate the summary overview table
            //debugger;
            var strHtml = '<div id="pnlInvSummary" class="width-full float-left">';
            strHtml += GenerateSummaryHtml();
            strHtml += '</div>';
            //  Generate the management table
            strHtml += '<div id="pnlInvManagement" class="width-full float-left">';
            strHtml += GenerateManagementHtml();
            strHtml += '</div>';

            panel.innerHTML = strHtml;
        }
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display invoice summary');
    }
}

//      Function to generate Html for the summary details
function GenerateSummaryHtml() {
    var strHtml = '<table class="InvoiceSummary"><tr><th></th><th style="text-align: right;">Net Value (' + CurrentCurrency + ')</th><th style="text-align: right;">Vat Value (' + CurrentCurrency + ')</th><th style="text-align: right;">Gross Value (' + CurrentCurrency + ')</th></tr>';
    strHtml += '<tr><td>Quote Value</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.QuoteValue.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.QuoteVatValue.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += (ObjInvoiceSummary.QuoteValue + ObjInvoiceSummary.QuoteVatValue).format();
    strHtml += '</td></tr>';
    strHtml += '<tr><td>Total Work Value</td><td style="text-align: right;">';
    //debugger
    strHtml += ObjInvoiceSummary.TotalWorkValue.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.TotalWorkVatValue.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += (ObjInvoiceSummary.TotalWorkValue + ObjInvoiceSummary.TotalWorkVatValue).format();
    strHtml += '</td></tr>';
    strHtml += '<tr><td>Completed Work Value</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.CompletedWorkValue.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.CompletedWorkVatValue.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += (ObjInvoiceSummary.CompletedWorkValue + ObjInvoiceSummary.CompletedWorkVatValue).format();
    strHtml += '</td></tr>';
    strHtml += '<tr><td>Pending Invoice Value</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.PendingValue.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.PendingVat.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += (ObjInvoiceSummary.PendingValue + ObjInvoiceSummary.PendingVat).format();
    strHtml += '</td></tr>';
    strHtml += '<tr><td>Invoiced Value</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.InvoicedValue.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.InvoicedVatValue.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += (ObjInvoiceSummary.InvoicedValue + ObjInvoiceSummary.InvoicedVatValue).format();
    strHtml += '</td></tr>';
    strHtml += '<tr><td>Receipts Value</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.InvoiceReceipts.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.InvoiceReceiptVat.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += (ObjInvoiceSummary.InvoiceReceipts + ObjInvoiceSummary.InvoiceReceiptVat).format();
    strHtml += '</td></tr>';
    strHtml += '<tr><td>Overdue Value</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.OverdueValue.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += ObjInvoiceSummary.OverdueVat.format();
    strHtml += '</td><td style="text-align: right;">';
    strHtml += (ObjInvoiceSummary.OverdueValue + ObjInvoiceSummary.OverdueVat).format();
    strHtml += '</td></tr></table>';

    return strHtml;
}

//      Function to generate Html for the management table
function GenerateManagementHtml() {
    /*
        Pending = 0,
        Sent = 1,
        Paid = 2,
        Overdue = 3,
        FurtherAction = 4,
        Credited = 5,
        Cancelled = 99
    */
    // Get all invoices based on status
    var pending = $.grep(ObjInvoiceSummary.Invoices, function (e) { return e.Status == 0; });
    var sent = $.grep(ObjInvoiceSummary.Invoices, function (e) { return e.Status == 1; });
    var paid = $.grep(ObjInvoiceSummary.Invoices, function (e) { return e.Status == 2; });
    var cancelled = $.grep(ObjInvoiceSummary.Invoices, function (e) { return e.Status == 99 || e.Status == 5; });
    var other = $.grep(ObjInvoiceSummary.Invoices, function (e) { return e.Status == 3 || e.Status == 4; });
    var strHtml = '<div class="width-full jobTabBar"><span class="padded"><b>Invoice Management</b></span></div>';
    strHtml += '<table class="width-full searchGrid">';
    //strHtml += '<tr><th colspan="6">Invoice Management</th></tr>';
    //  Add the pending / creation items
    strHtml += '<tr><th colspan="6">';
    if (pending && pending.length > 0) {
        //  There is already a pending invoice that should be sent or deleted
        strHtml += 'Pending invoices to be sent or deleted</th></tr>';
        strHtml += '<tr><th>Invoice N0.</th><th>Net Value (' + CurrentCurrency + ')</th><th>Date Due</th><th>Status</th><th>Edit</th><th>Change Status</th><tr>';
        pending.forEach(function (inv) {
            strHtml += '<tr><td>Pending</td><td>' + inv.GrossValue.format() + '</td>';
            strHtml += '<td>Pending</td>';
            strHtml += '<td>Pending</td>';
            strHtml += '<td>';
            //  Add the edit button
            strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + inv.Id.toString() + ', \'Create Invoice\');" class="button-thin padded">Send</button>';
            //  As the invoice has not been sent enable cancel 
            strHtml += '</td><td>';
            strHtml += '<button type="button" onclick="SetInvoiceStatus(' + inv.Id + ',99);" class="button-thin padded">Cancel</button>';
            strHtml += '</td></tr>';
        });
    } else {
        //There are currently no pending incvoices so enable generation
        strHtml += '<span class="padded">There are currently no pending invoices</span>';
        strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(-1, \'Create Invoice\');" class="button-thin padded float-right">Create</button></th></tr>';
    }
    strHtml += '</table>';
    //  Add the sent items
    if (sent && sent.length > 0) {
        strHtml += '<div class="width-full jobTabBar"><span class="padded"><b>Sent Invoices</b></span></div>';
        strHtml += GenerateSentInvoiceTable(sent);
    }
    //  Add all invoices that have a Overdue or Further Action Status
    if (other && other.length > 0) {
        strHtml += '<div class="width-full jobTabBar"><span class="padded"><b>Overdue Invoices</b></span></div>';
        strHtml += GenerateLateInvoiceTable(other);
    }
    //  Add all Paid Invoices
    if (paid && paid.length > 0) {
        strHtml += '<div class="width-full jobTabBar"><span class="padded"><b>Paid Invoices</b></span></div>';
        strHtml += GeneratePaidInvoiceTable(paid);

    }
    //  Add all cancelled and credited invoices
    if (cancelled && cancelled.length > 0) {
        strHtml += '<div class="width-full jobTabBar"><span class="padded"><b>Invoices that have been credited or cancelled</b></span></div>';
        //strHtml += '<tr><th colspan="6">Invoices that have been credited or cancelled</th></tr>';
        strHtml += GerenrateCancelledInvoiceTable(cancelled);
    }

    //strHtml += '</table>';

    return strHtml;
}

function GenerateSentInvoiceTable(invoices) {
    var strHtml = '<table class="width-full searchGrid">';
    strHtml += '<tr><th>Invoice No</th><th>Net Value (' + CurrentCurrency + ')</th><th>Date Due</th><th>Date Sent</th><th>Status</th><th>Update</th><th>Send Reminder</th><th>Cancel</th><th>Resend</th><th>Edit Invoice Dates</th><tr>';
    invoices.forEach(function (inv) {
        var strDueDate = toFormattedDateStringFromJson(inv.DueDate, '', 'dd/mm/yyyy');
        var strSerntDate = toFormattedDateStringFromJson(inv.DateSent, '', 'dd/mm/yyyy');
        strHtml += '</td><td>';
        strHtml += inv.Reference;
        strHtml += '</td><td>';
        strHtml += inv.GrossValue.format();
        strHtml += '</td><td>';
        strHtml += strDueDate;
        strHtml += '</td><td>';
        strHtml += strSerntDate;
        strHtml += '</td><td>';
        strHtml += inv.StatusString;
        strHtml += '</td><td';
        //  The invoice has not been paid so add a paid button
        strHtml += '><button type="button" onclick="SetInvoiceStatus(' + inv.Id + ',2);" class="button-thin padded">Paid</button>';
        strHtml += '</td><td>';
        
        strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + inv.Id + ',\'Reminder\');" class="button-thin padded">Send Reminder</button>';
        strHtml += '</td><td>';
        //  The invoice has been created and possibly sent - generate a cancel button incase it was created in error
        strHtml += '<button type="button" onclick="SetInvoiceStatus(' + inv.Id + ',99);" class="button-thin padded">Cancel</button>';
        strHtml += '</td>';
        var today = new Date();
        if (toDateFromJson(inv.DueDate) < today) {
            strHtml += '<td><button type="button" disabled="disabled" onclick="ShowResendInvoiceDialog(' + inv.Id + ',\'Resend Invoice\');" class="button-thin padded">Resend</button>';
        } else {
            strHtml += '<td><button type="button" onclick="ShowResendInvoiceDialog(' + inv.Id + ',\'Resend Invoice\');" class="button-thin padded">Resend</button>';
        }
        strHtml += '</td>';
        strHtml += '<td><button type="button" onclick="ShowEditInvoiceSent(' + inv.Id + ');" class="button-thin float-left padded">Edit</td>';
        strHtml += '</tr>';
    });

    strHtml += '</table>';
    return strHtml;
}



function GenerateLateInvoiceTable(invoices) {
    var strHtml = '<table class="width-full searchGrid">';
    strHtml += '<tr><th>Invoice N0.</th><th>Net Value (' + CurrentCurrency + ')</th><th>Date Due</th><th>Status</th><th>Update</th><th>Send</th><tr>';
    invoices.forEach(function (inv) {
        var strDueDate = toFormattedDateStringFromJson(inv.DueDate, '', 'dd/mm/yyyy');
        strHtml += '</td><td>';
        strHtml += inv.Reference;
        strHtml += '</td><td>';
        strHtml += inv.GrossValue.format();
        strHtml += '</td><td>';
        strHtml += strDueDate;
        strHtml += '</td><td>';
        strHtml += inv.StatusString;
        strHtml += '</td><td';
        //The invoice has not been paid
        strHtml += '><button type="button" onclick="SetInvoiceStatus(' + inv.Id + ',2);" class="button-thin padded">Paid</button>';
        strHtml += '</td><td>';

        //The invoice is overdue
        /*
            Overdue = 3,
            FurtherAction = 4,
        */
                switch (inv.Status) {
                    case 4:
                        strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + inv.Id + ',\'Legal Action Letter\');" class="button-thin padded">Send Legal</button>';
                        break;
                    default:
                        strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + inv.Id + ',\'Reminder  Letter\');" class="button-thin padded">Send Reminder</button>';
                        break;

                        }
                
        strHtml += '</td>';
        strHtml += '</tr>';
    });

    strHtml += '</table>';
    return strHtml;
}

function GeneratePaidInvoiceTable(invoices) {
    var strHtml = '<table class="width-full searchGrid">';
    strHtml += '<tr><th>Invoice No</th><th>Net Value ' + CurrentCurrency + '</th><th>Date Due</th><th>Status</th><th>Sent Date</th><th>Paid Date</th><th>Edit Paid Date</th><tr>';
    invoices.forEach(function (inv) {
        var strDueDate = toFormattedDateStringFromJson(inv.DueDate, '', 'dd/mm/yyyy');
        var strPaidDate = toFormattedDateStringFromJson(inv.PaidDate, '', 'dd/mm/yyyy');
        var test = inv.DateSent;
        var strSentDate = toFormattedDateStringFromJson(inv.DateSent, '', 'dd/mm/yyyy');
        strHtml += '</td><td>';
        strHtml += inv.Reference;
        strHtml += '</td><td>';
        strHtml += inv.GrossValue.format();
        strHtml += '</td><td>';
        strHtml += strDueDate;
        strHtml += '</td><td>';
        strHtml += inv.StatusString;
        strHtml += '</td><td>';
        strHtml += strSentDate;
        strHtml += '</td><td>';
        strHtml += strPaidDate;
        strHtml += '</td>';
        strHtml += '<td><button type="button" onclick="ShowEditInvoice(' + inv.Id +');" class="button-thin float-left padded">Edit</td>';
        strHtml += '</tr>';
    });

    strHtml += '</table>';
    return strHtml;
}

function GerenrateCancelledInvoiceTable(invoices) {
    var strHtml = '<table class="width-full searchGrid">';
    strHtml += '<tr><th>Invoice No</th><th>Net Value ' + CurrentCurrency +' </th><th>Date Due</th><th>Status</th><tr>';
    invoices.forEach(function (inv) {
        var strDueDate = toFormattedDateStringFromJson(inv.DueDate, '', 'dd/mm/yyyy');
        var strPaidDate = toFormattedDateStringFromJson(inv.PaidDate, '', 'dd/mm/yyyy');
        strHtml += '</td><td>';
        strHtml += inv.Reference;
        strHtml += '</td><td>';
        strHtml += inv.GrossValue.format();
        strHtml += '</td><td>';
        strHtml += strDueDate;
        strHtml += '</td><td>';
        strHtml += inv.StatusString;
        strHtml += '</td>';
        strHtml += '</tr>';
    });

    strHtml += '</table>';
    return strHtml;
}

function GenerateInvoiceItemHtml(invoices) {
    //debugger;
    var strHtml = '<tr><th>Invoice N0.</th><th>Net Value ' + CurrentCurrency + '</th><th>Date Due</th><th>Status</th><th>Edit</th><th>Change Status</th><tr>';
    invoices.forEach(function (inv) {
        var strDueDate = toFormattedDateStringFromJson(inv.DueDate, '', 'dd/mm/yyyy');
        strHtml += '</td><td>';
        strHtml += inv.Reference;
        strHtml += '</td><td>';
        strHtml += inv.GrossValue.format();
        strHtml += '</td><td>';
        strHtml += strDueDate;
        strHtml += '</td><td>';
        strHtml += inv.StatusString;
        strHtml += '</td><td';
        if (inv.Status == 2) {
            //The invoice has been paid
            strHtml += '>Paid</td><td><button type="button" onclick="CreditInvoice(' + inv.Id + ');" class="button-thin padded" disabled="disabled">Credit</button></td>'
        } else {
            if (inv.Status == 99) {
                //The invoice has been cancelled
                strHtml += ' colspan="2">Cancelled';
            } else if (inv.Status == 5) {
                //The invoice has been credited
                strHtml += ' colspan="2">Credited';
            } else {
                //The invoice has not been paid
                strHtml += '><button type="button" onclick="SetInvoiceStatus(' + inv.Id + ',2);" class="button-thin padded">Paid</button>';
                strHtml += '</td><td>';

                //if (inv.Status > 2 || ParseLocalDate(strDueDate, 'en_GB') < new Date()) {
                    //The invoice is overdue
                    /*
                        Overdue = 3,
                        FurtherAction = 4,
                    */
                    switch (inv.Status) {
                        case 1:
                        case 2:
                            inv.Status = 3;
                            strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + inv.Id + ',\'First Reminder Letter\');" class="button-thin padded">Send Reminder</button>';
                            break;
                        case 3:
                            strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + inv.Id + ',\'Reminder  Letter\');" class="button-thin padded">Send Reminder</button>';
                            break;
                        case 4:
                            strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + inv.Id + ',\'Legal Action Letter\');" class="button-thin padded">Send Legal</button>';
                            break;

                    //}
                }
            }
            strHtml += '</td>';
        }
        strHtml += '</tr>';
    });
    return strHtml;
}

//Async callback to display all invoice notes for a quote
function DisplayInvoiceNotes(results) {
    //debugger;
    var notesPanel = document.getElementById('tabInvoiceDetails_1');
    //Use the common.js method to populate and control functionality
    PopulateNotes(notesPanel, 2, ObjInvoiceSummary.Id, results, GetInvoiceNotesForQuote);
}

//Async function to display history items for a quote
function DisplayQuoteInvoiceHistory(results) {
    var historyPanel = document.getElementById('tabInvoiceDetails_2');
    try {
        var strHtml = '';
        if (!results) {
            DisplayMessage(historyPanel.id, 'Error: The service return no history data');
        } else if (results.length == 0) {
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid width-full">';
            strHtml += '<tr><td>No history to display</td></tr>';
        } else {
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid small width-full">';
            strHtml += '<col width="15%"><col width="15%"><col width="20%"><col width="25%"><col width="25%">';
            strHtml += '<tr>';
            strHtml += '<th>Invoice No</th>';
            strHtml += '<th>Doc Type</th>';
            strHtml += '<th>Created On</th>';
            strHtml += '<th>Sent Via</th>';
            strHtml += '<th>Sent On</th>';
            strHtml += '<th></th>';
            strHtml += '<th></th>';
            strHtml += '</tr>';

            results.forEach(function (h) {
                strHtml += '<tr>';
                strHtml += '<td style="vertical-align: top; text-align: center;">' + h.ReferenceNo + '</td>';
                strHtml += '<td style="vertical-align: top;">' + h.DocTypeString + '</td>';
                var CreationDate = new Date(parseInt(h.CreationDate.substr(6)));
                var SentDate = new Date(parseInt(h.SentDate.substr(6)));
                strHtml += '<td style="vertical-align: top; text-align: center;">' + (CreationDate.getYear() > 1 ? CreationDate.toDateString() : 'N/A') + '</td>';
                strHtml += '<td style="vertical-align: top;">' + h.DispatchMethodString + '</td>';
                strHtml += '<td style="vertical-align: top; text-align: center;">' + (SentDate.getYear() > 1 ? SentDate.toDateString() : 'N/A') + '</td>';
                strHtml += '<td style="vertical-align: top; text-align: center;"><a target="_blank" href="Images/ReportImage.aspx?Id=' + h.Id + '">View</a></td>';
                strHtml += '</tr>';
            });

            //strHtml = strHtml + '</table>';
            //var docList = results; //GetStaffList();
            //strHtml += '<div class="width-full float-left">';
            //strHtml += '<table class="searchGrid width-full">';
            //strHtml += '<tr class="white-light"><th>Entered By</th><th>Details</th><th>Created</th>'
            //docList.forEach(function (c) {
            //    strHtml += '<tr onclick="ShowDocumentHistoryDialog(' + c.Id + ');">';
            //    strHtml += '<td style="padding-left: 10px;">' + c.EnteredBy + '</td><td style="padding-left: 10px;">' + c.Text + '</td><td  style="padding-left: 10px;">' + toDateStringFromJson(c.EnteredDate, '', 'dd/mm/yyyy') + '</td>'
            //    strHtml += '</tr>';
            //});
        }
        strHtml += '</table>';
        strHtml += '</div>';
        strHtml += '<br/>';
        var JobId = getParameterByName("JobID");
        var JobDocs = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'getJobDocuments',
            {
                JobId: JobId
            },
            false
            );
        strHtml += '<div class="width-full float-left white-light"><button type="button" onclick="ShowDocumentsDialog(-1);" class="button-thin float-left padded">Add New Documents</button></div>';
        strHtml += '<div class="width-full float-left">';
        console.log('JobDocs:' + JobDocs.length);
        if (!JobDocs) {
            DisplayMessage(historyPanel.id, 'Error: no data return from service');
        }
        else {
            strHtml += '<table class="searchGrid width-full">';
            if (JobDocs.length == 0) {
                strHtml += '<tr class="white-light">There are no documents records to display. Please add documents by clicking the "Add New Documents" button.</tr>';
            } else {
                strHtml += '<tr class="white-light"><th style="padding-left: 10px;">Documents Name</th><th style="text-align:center;">Email attachment</th></tr>';
                JobDocs.forEach(function (c) {
                    console.log(c.DefaultAttachment);
                    strHtml += '<tr>';
                    strHtml += '<td onclick="ShowDocumentsDialogEdit(' + c.Id + ');" style="padding-left: 10px;">' + c.DocumentName + '</td>';
                    if (c.DefaultAttachment) {
                        strHtml += '<td style="width:21%; vertical-align: middle; text-align:center;" ><input type="checkbox" id="chkDefaultAttachment" checked onChange="ToggleDefaultAttachment(' + c.Id + ',this);" id="(' + c.Id + ');"/></td>';
                    }
                    else {
                        strHtml += '<td style="width:21%; vertical-align: middle; text-align:center;" ><input type="checkbox" id="chkDefaultAttachment" onChange="ToggleDefaultAttachment(' + c.Id + ',this);" id="(' + c.Id + ');"/></td>';
                    }
            
                    strHtml += '</tr>';
                });
            }
        }
        strHtml += '</table>';
        strHtml += '</div>';
        historyPanel.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage(historyPanel.id, 'Error: Unable to display invoice history - ' + err.toString());
    }
}

function ToggleDefaultAttachment(DocumentId, thisElement) {
    console.log(thisElement.checked);
    var Orgs = Ajax_CallService(
            'Services/web/WorkFlow.asmx',
            'ToggleDefaultAttachmentForJob',
            { DocId: DocumentId, DefaultAttachment: thisElement.checked },
            false
        );
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function PopulateDocuments() {
    var Grid = document.getElementById('grdDoc');
    var listHtml = '<table class="searchGrid width-full"><tr><th>Document Name</th><th>Action</th></tr>';
    var JobId = getParameterByName("JobID");
    var JobDocs = Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'getDefaultJobDocuments',
                {
                    JobId: JobId
                },
                false
                );
    JobDocs.forEach(function (c) {
        listHtml = listHtml + '<tr><td>' + c.DocumentName + '</td>';
        listHtml = listHtml + '<td  style="width:21%; vertical-align: middle; text-align:center;"><input type="checkbox" checked="checked" onChange="ToggleSingleAttachment(\'' + c.Id + '_QuoteDoc\',this);" id="(' + c.Id + ');"></td></tr>';
        AddDoc(c.Id + '_QuoteDoc');
    });
    listHtml = listHtml + '</table>';
    Grid.innerHTML = listHtml;
    var CheckAssesmentGen = false;
    ObjJob.WorkSites.forEach(function (js) {
        Validate = Ajax_CallService(
                   'Services/Web/RiskAssessment.asmx',
                   'ValidateAssessmentForSite',
                   {
                       quoteId: ObjSelectedQuote.Id,
                       jobSiteId: js.Id
                   },
                   false
               );
        if (Validate.length == 0) {
            CheckAssesmentGen = true;
        }

    });
    Grid = document.getElementById('grdArbProDoc');
    listHtml = '';
    listHtml = '<table class="searchGrid width-full"><tr><th>Document Name</th><th>Action</th></tr>';
    if (CheckAssesmentGen) {
        listHtml = listHtml + '<tr><td>Risk Assessment</td>';
        listHtml = listHtml + '<td style="width:21%; vertical-align: middle; text-align:center;"><input type="checkbox" onChange="ToggleRiskAssessment(this);"></td></tr>';
    }
    else {
        listHtml = listHtml + '<tr><td>Risk Assessment(<span style="color:red;">No Doc, go create the document</span>)</td>';
        listHtml = listHtml + '<td style="width:21%; vertical-align: middle; text-align:center;"><input type="checkbox" disabled="disabled"></td></tr>';
        includeRiskAssessment = false;
    }
    listHtml = listHtml + '<tr><td>Work schedule and tree pictures</td>';
    listHtml = listHtml + '<td style="width:21%; vertical-align: middle; text-align:center;"><input type="checkbox"  onChange="ToggleTreePictures(this);"></td></tr>';
    listHtml = listHtml + '<tr><td>Site Plan</td>';
    listHtml = listHtml + '<td style="width:21%; vertical-align: middle; text-align:center;"><input type="checkbox" onChange="ToggleSitePlan(this);"></td></tr>';
    listHtml = listHtml + '</table>';
    Grid.innerHTML = listHtml;
}

function ToggleTreePictures(ActionElement) {
    if (ActionElement.checked) {
        includeTreePicture = true;
    }
    else {
        includeTreePicture = false;
    }
}

function ToggleSitePlan(ActionElement) {
    if (ActionElement.checked) {
        includeSitePlan = true;
    }
    else {
        includeSitePlan = false;
    }
}

function ToggleRiskAssessment(ActionElement) {
    if (ActionElement.checked) {
        includeRiskAssessment = true;
    }
    else {
        includeRiskAssessment = false;
    }
}
function ToggleSingleAttachment(DocId, ActionElement) {
    if (ActionElement.checked) {
        AddDoc(DocId);
    }
    else {
        RemoveDoc(DocId);
    }
    console.log(DocList);
}

function RemoveDoc(DocId) {
    var index = DocList.indexOf(DocId);
    if (index > -1) {
        DocList.splice(index, 1);
    }
}

function AddDoc(DocId) {
    DocList.push(DocId);
}

function AddDocAsAttacmentToEmailForInvoice(DialogRef) {
     $('#pnlDocloading').show();
    if (includeSitePlan == true) {
        includeSitePlan = false;
        var FileNo = 1;
        ObjJob.WorkSites.forEach(function (js) {
            var result = Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'GetQuoteSiteMap',
                {
                    JobSiteID: js.Id,
                    QuoteID: ObjSelectedQuote.Id
                },
                false
              );


            if (result && result.Id > 0) {
                var CurDetails = result;
                console.log(CurDetails);
                if (CurDetails.SitePostCode && CurDetails.SitePostCode.length > 0) {
                    var loc;
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ 'address': CurDetails.SitePostCode + ', UK' },
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                loc = results[0].geometry.location;
                                var SiteMapURL = ExportSiteMapURL(loc, CurDetails.SitePostCode);
                                console.log(SiteMapURL);
                                MapImage = DownloadGoogleMap(SiteMapURL);
                                var base64String = btoa([].reduce.call(new Uint32Array(MapImage.Bytes), function (p, c) { return p + String.fromCharCode(c) }, ''));
                                //console.log(base64String);
                                var Doc = Ajax_CallService(
                                   'Services/Web/Report.asmx',
                                   'AddTempDocument',
                                   {
                                       ItemID: ObjSelectedQuote.Id,
                                       Description: "Temporary Document For Email Attachment",
                                       ContentType: "png",
                                       ByteString: base64String,
                                       FilePath: "Site Plan-" + FileNo.toString() + ".png"

                                   },
                                   false);
                                SetSelected({ id: Doc.Id, type: Doc.DocumentTypeID }, true);
                                if (FileNo == ObjJob.WorkSites.length) {
                                    otherInvoiceDocAsAttachmentToEmail(DialogRef);
                                }
                                FileNo += 1;
                            }
                        });

                }
            }

        });


    }
    else {
        otherInvoiceDocAsAttachmentToEmail(DialogRef);
    }
    
}
function otherInvoiceDocAsAttachmentToEmail(DialogRef) {
    AttachDocumentsForInvoice();
    if (includeTreePicture == true) {
        includeTreePicture = false;
        createSitePlanDocForEmail();
    }
    if (includeRiskAssessment) {
        includeRiskAssessment = false;
        CreateRiskAssessmentDocForEmail();
    }
    LoadAttachments();
    DocList = [];
    $("#DocAttachModel").addClass("hidden");
    includeTreePicture = false;
    includeSitePlan = false;
    includeRiskAssessment = false;
    DialogRef.dialog('destroy');
    $('#pnlDocloading').hide();

}
function Load_Documents(id) {
    if (id.indexOf("_CompanyDoc") != -1) {
        var DocId = id.replace("_CompanyDoc", "");
        console.log("Company Doc Id:" + DocId);
        if (DocId > 0) {
            var Document_File = Ajax_CallService(
                     'Services/Web/Company.asmx',
                     'GetDocumentForId',
                     {
                         Id: DocId
                     },
                     false
                 );
            return Document_File;
        }
    }
    else if (id.indexOf("_QuoteDoc") != -1) {
        var DocId = id.replace("_QuoteDoc", "");
        console.log("Quote Doc Id:" + DocId);
        if (DocId > 0) {
            var Document_File = Ajax_CallService(
                     'Services/Web/WorkFlow.asmx',
                     'getDefaultJobDocumentById',
                     {
                         Id: DocId
                     },
                     false
                 );
            return Document_File;
        }
    }

}
function AttachDocumentsForInvoice() {
    var AllDocList = [];
    for (var DocListIndex = 0; DocListIndex < DocList.length; DocListIndex++) {
        var id = DocList[DocListIndex];
        if (id.indexOf("_QuoteDoc") != -1) {
            var DocId = id.replace("_QuoteDoc", "");
            if (DocId > 0) {
                AllDocList.push(DocId);
                SetSelected({ id: DocId, type: 4 }, true);
            }
        }
    }
    if (AllDocList.length > 0) {
        var Document_File = Ajax_CallService(
            'Services/Web/Report.asmx',
            'AddJobDocumentToAttachment',
            {
                DocIdList: AllDocList
            },
            false
        );
        return true;
    }
    return false;
}

function DownloadGoogleMap(MapURL) {
    var Doc = Ajax_CallService(
                'Services/Web/Report.asmx',
                'DownloadGoogleImage',
                {
                    MapUrl: MapURL,
                },
                false
              );

    return Doc;
}
function createSitePlanDocForEmail() {
    var ListSites = Ajax_CallService(
      'Services/Web/WorkFlow.asmx',
      'GetWorkSitesForQuote',
      {
          quoteId: ObjSelectedQuote.Id
      },
      false);
    var DocNo = 1;
    ListSites.forEach(function (js) {
        console.log("Site Id:" + js.Id);
        var ObjPdfSettings = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetWorkPdfSetting',
        {},
        false
        );
        ObjPdfSettings.Id = '' + js.Id + '';
        ObjPdfSettings.ReportParentType = 4;
        ObjPdfSettings.GenerationEnum = 0;
        ObjPdfSettings.ShowPictures = true;
        ObjPdfSettings.QuoteId = ObjSelectedQuote.Id;
        ObjPdfSettings.DocNo = DocNo;
        console.log(ObjPdfSettings);
        Ajax_CallService(
                'Services/Web/Report.asmx',
                'AttachWorkPackLetter',
                {
                    settings: ObjPdfSettings,
                },
                false
            );
        DocNo += 1;

    });

    var Docs = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetTempDocuments',
        {
            ItemID: ObjSelectedQuote.Id
        },
        false);
    Docs.forEach(function (file) {
        if (file.ClientID == -5) {
            SetSelected({ id: file.Id, type: file.DocumentTypeID }, true);
        }
    });
}
function CreateRiskAssessmentDocForEmail() {
    var ListSites = Ajax_CallService(
      'Services/Web/WorkFlow.asmx',
      'GetWorkSitesForQuote',
      {
          quoteId: ObjSelectedQuote.Id
      },
      false);
    var DocNo = 1;
    ListSites.forEach(function (js) {
        Validate = Ajax_CallService(
                   'Services/Web/RiskAssessment.asmx',
                   'ValidateAssessmentForSite',
                   {
                       quoteId: ObjSelectedQuote.Id,
                       jobSiteId: js.Id
                   },
                   false
               );
        if (Validate.length == 0) {
            var ObjPdfSettings = Ajax_CallService(
            'Services/Web/Report.asmx',
            'GetWorkPdfSetting',
            {},
            false
            );
            var WorkPlanId = Ajax_CallService(
            'Services/Web/Report.asmx',
            'GetWorkPlanId',
            {
                qId: ObjSelectedQuote.Id,
                jsId: js.Id
            },
            false
            );
            ObjPdfSettings.Id = '' + js.Id + '';
            ObjPdfSettings.ReportParentType = 5;
            ObjPdfSettings.GenerationEnum = 1;
            ObjPdfSettings.ShowPictures = true;
            ObjPdfSettings.QuoteId = WorkPlanId;
            ObjPdfSettings.DocNo = DocNo;
            Ajax_CallService(
                'Services/Web/Report.asmx',
                'AttachRiskAssessmentDoc',
                {
                    settings: ObjPdfSettings,
                },
                false
            );
            DocNo += 1;
        }

    });
    var Docs = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetTempDocuments',
        {
            ItemID: ObjSelectedQuote.Id
        },
        false);
    Docs.forEach(function (file) {
        if (file.ClientID == -6) {
            SetSelected({ id: file.Id, type: file.DocumentTypeID }, true);
        }
    });
}
function ExportSiteMapURL(Loc, PostalCode) {
    var mapOptions = {
        center: new google.maps.LatLng(54, -2),
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.SATELLITE
    };
    //URL of Google Static Maps.
    var staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap";

    //Set the Google Map Center.
    staticMapUrl += "?center=" + PostalCode + ",UK";

    //Set the Google Map Size.
    staticMapUrl += "&size=800x600";

    //Set the Google Map Zoom.
    staticMapUrl += "&zoom=" + mapOptions.zoom;

    //Set the Google Map Type.
    staticMapUrl += "&maptype=" + mapOptions.mapTypeId;

    //Loop and add Markers.
    staticMapUrl += "&markers=color:red|" + Loc.lat() + "," + Loc.lng();

    return staticMapUrl;
}
function getFileExtension(DocumentFileName) {

    FileExtension = /^.+\.([^.]+)$/.exec(DocumentFileName);
    return FileExtension == null ? "" : FileExtension[1];
}
/*

        Dialog Display Functions

*/

function ShowDocumentsDialog(docId) {
  
    Dialog_OpenPage('Associated Documents',
        'Dialogs/JobDocumentsAdd.aspx',
        'JobDocs',
        500,
        600,
        true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        var ddlQuoteNum = document.getElementById('ddlQuoteID');
                        var quoteId = ddlQuoteNum.options[ddlQuoteNum.selectedIndex].value;
                        GetInvoiceHistoryForQuote(quoteId)
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        var ddlQuoteNum = document.getElementById('ddlQuoteID');
                        var quoteId = ddlQuoteNum.options[ddlQuoteNum.selectedIndex].value;
                        GetInvoiceHistoryForQuote(quoteId)
                        $(this).dialog('close');
                    }
                }
            },
                    'Content/js/JobDocuments.js?V=3.23.5',
                    function () { LoadDateAndStaff(docId); }
        );
}

function ShowDocumentsDialogEdit(docId) {
    Dialog_OpenPage('Associated Documents',
        'Dialogs/JobDocumentsEdit.aspx',
        'JobDocsEdit',
        500,
        600,
        true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        File_UploadEdit();
                        var ddlQuoteNum = document.getElementById('ddlQuoteID');
                        var quoteId = ddlQuoteNum.options[ddlQuoteNum.selectedIndex].value;
                        GetInvoiceHistoryForQuote(quoteId);
                        $(this).dialog('close');
                    }
                },
                'Open': {
                    text: 'Open',
                    class: 'ui-dialog-button',
                    click: function () {
                        DocumentPreview();
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        var ddlQuoteNum = document.getElementById('ddlQuoteID');
                        var quoteId = ddlQuoteNum.options[ddlQuoteNum.selectedIndex].value;
                        GetInvoiceHistoryForQuote(quoteId);
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/JobDocuments.js?V=3.23.5',
            function () {
                var ddlQuoteNum = document.getElementById('ddlQuoteID');
                var quoteId = ddlQuoteNum.options[ddlQuoteNum.selectedIndex].value;
                JobDocumentsLoad(docId, 1, quoteId);
            }
        );
}
//      Function to show the edit / send invoice dialog
function ShowEditInvoiceSent(invoiceId) {
    try {
        debugger;
        Dialog_OpenPage('Edit Invoice Date',
            'Dialogs/EditInvoiceDateSent.aspx',
            'EditInvoiceDateSent',
            500,
            600,
            false,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            (SaveInvoiceDateSent(invoiceId))
                            $(this).dialog('close');
                            SwitchInvoiceTab(0);
                        }
                    },
                    'Close': {
                        text: 'Close',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/Invoice.js?V=3.23.5',
                function () { LoadCurrentInvoiceSent(invoiceId); });
    }
    catch (err) {
        alert('Error adding invoice item: ' + err.toString());
    }
}

    function ShowEditInvoice(invoiceId)
{
        try {
            debugger;
            Dialog_OpenPage('Edit Invoice Paid Date',
                'Dialogs/EditInvoiceDate.aspx',
                'EditInvoiceDate',
                500,
                600,
                false,
                    {
                        'Save': {
                            text: 'Save',
                            class: 'ui-dialog-button',
                            click: function () {
                                (SaveInvoiceDate(invoiceId))
                                $(this).dialog('close');
                                SwitchInvoiceTab(0);
                            }
                        },
                        'Close': {
                            text: 'Close',
                            class: 'ui-dialog-button',
                            click: function () {
                                $(this).dialog('close');
                            }
                        }
                    },          
                    'Content/js/Invoice.js?V=3.23.5',
                    function () { LoadCurrentInvoice(invoiceId); });
        }
        catch (err) {
            alert('Error adding invoice item: ' + err.toString());
        }
    }
    function SaveInvoiceDate(invoiceId)
    {
        var Id = invoiceId;
        var PaidDateEdit = document.getElementById('txtPaidDate').value;
        var DateSentEdit = document.getElementById('txtSentDate').value;
        var DueDateEdit = document.getElementById('txtDateDue').value;
        var DueDate = toUTCString(DueDateEdit, "en_GB");
        var DateSent = toUTCString(DateSentEdit, "en_GB");
        var PaidDate = toUTCString(PaidDateEdit, "en_GB");
        Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'UpdatePaidInvoiceDate',
                {
                    Id: Id,
                    PaidDate: PaidDate,
                    DueDate: DueDate,
                    DateSent: DateSent
                },
    true);
    }

    function SaveInvoiceDateSent(invoiceId) {
        debugger;
        var Id = invoiceId;
        //var DueDateUnformatted = document.getElementById('txtDateDue').value;
        //var DateSentUnformatted = document.getElementById('txtSentDate').value;
        //var DueDate = DateSentUnformatted.toISOString();
        //var DateSent = DateSentUnformatted.toISOString();
        var DueDateEdit = document.getElementById('txtDateDue').value;
        var DateSentEdit = document.getElementById('txtSentDate').value;
        var DueDate = toUTCString(DueDateEdit, "en_GB");
        var DateSent = toUTCString(DateSentEdit, "en_GB");

        //var DueDate = toDateFromJson(DateSentUnformatted);
        //var DateSent = toDateFromJson(DateSentUnformatted);
        Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'UpdateSentInvoiceDate',
                {
                    Id: Id,
                    DueDate: DueDate,
                    DateSent: DateSent
                },
    true);
    }

    function LoadCurrentInvoice(invoiceId) {
        if (invoiceId > 0) {
            Invoice_File = Ajax_CallService(
                   'Services/Web/WorkFlow.asmx',
                   'GetInvoiceForInvoice',
                   {
                       invoiceId: invoiceId
                   },
                   false
               );
            DisplayInvoiceDates(Invoice_File);
        }
    }
    function DisplayInvoiceDates(Invoice_File)
    {
        debugger;
        var strDueDate = toFormattedDateStringFromJson(Invoice_File.DueDate, '', 'dd/mm/yyyy');
        var strPaidDate = toFormattedDateStringFromJson(Invoice_File.PaidDate, '', 'dd/mm/yyyy');
        var strDateSent = toFormattedDateStringFromJson(Invoice_File.DateSent, '', 'dd/mm/yyyy');
        document.getElementById('txtInvoiceNo').value = Invoice_File.Reference;
        document.getElementById('txtNetValue').value = Invoice_File.GrossValue.format();
        document.getElementById('txtDateDue').value = strDueDate;
        document.getElementById('StatusString').value = 'Paid';
        document.getElementById('txtPaidDate').value = strPaidDate;
        document.getElementById('txtSentDate').value = strDateSent;
        $('#txtDateDue').datepicker({ dateFormat: 'dd/mm/yy', timepicker: false, closeOnDateSelect: true });
        $('#txtSentDate').datepicker({ dateFormat: 'dd/mm/yy', timepicker: false, closeOnDateSelect: true });
        $('#txtPaidDate').datepicker({ dateFormat: 'dd/mm/yy', timepicker: false, closeOnDateSelect: true });

    }
    function LoadCurrentInvoiceSent(invoiceId) {
        if (invoiceId > 0) {
            Invoice_File = Ajax_CallService(
                   'Services/Web/WorkFlow.asmx',
                   'GetInvoiceForInvoice',
                   {
                       invoiceId: invoiceId
                   },
                   false
               );
            DisplayInvoiceDatesSent(Invoice_File);
        }
    }
    function DisplayInvoiceDatesSent(Invoice_File) {
        debugger;
        var strDueDate = toFormattedDateStringFromJson(Invoice_File.DueDate, '', 'dd/mm/yyyy');
        var strDateSent = toFormattedDateStringFromJson(Invoice_File.DateSent, '', 'dd/mm/yyyy');
        document.getElementById('txtInvoiceNo').value = Invoice_File.Reference;
        document.getElementById('txtNetValue').value = Invoice_File.GrossValue.format();
        document.getElementById('txtDateDue').value = strDueDate;
        document.getElementById('txtSentDate').value = strDateSent;
        $('#txtDateDue').datepicker({ dateFormat: 'dd/mm/yy', timepicker: false, closeOnDateSelect: true });
        $('#txtSentDate').datepicker({ dateFormat: 'dd/mm/yy', timepicker: false, closeOnDateSelect: true });

    }

    function CreateInvoice(invoiceId) {
        Dialog_OpenPage(
            'Send ' + text,
            'Dialogs/InvoiceGeneration.aspx',
            'content',
            1000,
            800,
            true,
             {
                 'Close': {
                     text: 'Close',
                     class: 'ui-dialog-button',
                     click: function () {
                         $(this).dialog('close');
                     }
                 }
             },
            'Content/js/InvoiceGeneration.js?V=3.23.5',
            function () { InvoiceGeneration_Load(invoiceId, false); });
    }

    //Function to display a dialog to add an invoice item to an invoice
    function AddInvoiceItem(invoiceId) {

        var ddl = document.getElementById('ddlQuoteID');
        var quoteId = ddl.options[ddl.selectedIndex].value;
        try {
            Dialog_OpenPage(
                'Add Items to Invoice',
                'Dialogs/InvoiceItem.aspx',
                'content',
                800,
                700,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (InvoiceItems_Save()) {
                                $(this).dialog('close');
                                SwitchInvoiceTab(1);
                            }
                        }
                    },
                    'Cancel': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            //Close the dialog and reload the tab to display the added items
                            $(this).dialog('close');
                            SwitchInvoiceTab(1);
                        }
                    }
                },
           'Content/js/InvoiceItems.js?V=3.23.5',
           function () { InvoiceItems_PageLoad(quoteId, invoiceId, ListJobSites); });
        }
        catch (err) {
            alert('Error adding invoice item: ' + err.toString());
        }
    }

    //Function to display a dialog to edit an invoice item
    function EditInvoiceItem(itemId) {
        try {
            Dialog_OpenPage(
                'Edit Invoice Item',
                'Dialogs/EditItem.aspx',
                'content',
                800,
                150,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (EditItem_Save()) {
                                $(this).dialog('close');
                                SwitchInvoiceTab(1);
                            }
                        }
                    },
                    'Cancel': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            //Close the dialog and reload the tab to display the added items
                            $(this).dialog('close');
                            SwitchInvoiceTab(1);
                        }
                    }
                },
           'Content/js/EditItem.js?V=3.23.5',
           function () { EditItem_Load(2, itemId); });
        }
        catch (err) {
            alert('Error editing invoice item: ' + err.toString());
        }
    }

    //      Function to display all invoice items
    function DisplayInvoiceItems(parentId, itemList) {

        var strHtml = '';
        var JobVatRates = new Array();
        ListJobSites.forEach(function (js) {

            strHtml += '<table class="searchGrid small width-full"><col width="5%"><col width="20%"><col width="55%"><col width="10%"><col width="10%">';
            //Display the site address for the items
            strHtml += '<tr>';
            strHtml += '<th colspan="5">' + js.SiteString + '</th></tr>';

            var SiteVatRates = new Array();

            //Iterate through each item to see if it is for the current site
            itemList.forEach(function (qi) {
                if (qi.JobSiteId == js.Id) {
                    var found = false;
                    //Evaluate the VAT rates for the Site and add if required 
                    SiteVatRates.forEach(function (vr) {
                        if (vr.rate == qi.VatRate) {
                            vr.value += qi.Value;
                            found = true;
                        }
                    });

                    if (!found)
                        SiteVatRates.push({ rate: qi.VatRate, value: qi.Value });

                    found = false;
                    //Evaluate the VAT rates for the Job and add if required
                    JobVatRates.forEach(function (vr) {
                        if (vr.rate == qi.VatRate) {
                            vr.value += qi.Value;
                            found = true;
                        }
                    });

                    if (!found)
                        JobVatRates.push({ rate: qi.VatRate, value: qi.Value });

                    //Display the item.
                    strHtml += '<tr>';
                    strHtml += '<td style="vertical-align: top; text-align: center;" onclick="EditInvoiceItem(' + qi.Id + ')">' + qi.Code + '</td>';
                    strHtml += '<td style="vertical-align: top;" onclick="EditInvoiceItem(' + qi.Id + ')">' + qi.Name + '</td>';
                    strHtml += '<td onclick="EditInvoiceItem(' + qi.Id + ')">' + qi.Description + '</td>';
                    strHtml += '<td style="text-align: center" onclick="event.preventDefault(); OpenImage(1, ' + qi.Id + ');"><a href="#">Image</a></td>';
                    strHtml += '<td style="text-align: right; padding-right: 3px; padding-left: 3px; vertical-align: bottom;"(' + qi.Id + ')">' + (qi.Value > 0 ? ' + CurrentCurrency + ' + qi.Value.format() : '') + '</td>';
                    strHtml += '</tr>';
                }
            });
            strHtml += '</table>';
            //Check if site subtotals need to be displayed
            if (ListJobSites.length > 1) {
                strHtml += GetSiteTotalHtml(SiteVatRates, ListJobSites.length, 'Site');
                strHtml += '<hr/>';
            }
        });
        //Display the invoice totals
        strHtml += GetSiteTotalHtml(JobVatRates, ListJobSites.length, 'Invoice');
        return strHtml;
    }

    //      Function to set an invoice as paid
    function SetInvoicePaid(invoiceId) {
        var updated = false;
        try {
            updated = Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'SetInvoiceStatus',
                {
                    invoiceId: invoiceId,
                    status: 2
                },
                false
            );
        }
        catch (err) {
            updated = false;
        }
        if (updated) {
            alert('Invoice set as paid');
        } else {
            alert('Failed to update invoice status');
        }
        //Update the panel
        SwitchInvoiceTab(3);
    }

    //      Function to display the create / send invoice dialog
    function ShowSendInvoiceDialog(invoiceId, text) {
        // debugger;
        Dialog_OpenPage(
            'Send ' + text,
            'Dialogs/InvoiceGeneration.aspx',
            'content',
            900,
            800,
            true,
             {
                 'Close': {
                     text: 'Close',
                     class: 'ui-dialog-button',
                     click: function () {
                         $(this).dialog('close');
                     }
                 }
             },
            'Content/js/InvoiceGeneration.js?V=3.23.5',
            function () { InvoiceGeneration_Load(invoiceId, false); });
    }



    //      Function to display the create / send invoice dialog
    function ShowResendInvoiceDialog(invoiceId, text) {
        // debugger;
        Dialog_OpenPage(
            text,
            'Dialogs/InvoiceGeneration.aspx',
            'content',
            900,
            800,
            true,
             {
                 'Close': {
                     text: 'Close',
                     class: 'ui-dialog-button',
                     click: function () {
                         $(this).dialog('close');
                     }
                 }
             },
            'Content/js/InvoiceGeneration.js?V=3.23.5',
            function () { InvoiceGeneration_Load(invoiceId, true); });
    }

    //      Function to set the recipient details for an invoice
    function InvoiceRecipientDetails(invoice) {
        var strHtml = '<div class="float-left width-full">';
        //Construct the table to hold the contact details
        strHtml += '<table class="searchGrid width-full"><tr><th colspan="2">Contact Details</th><th>';
        strHtml += '<input type="button" class="button-thin float-right padded" value="Edit" onclick="EditInvoiceContact();" />';
        strHtml += '</th></tr>';
        //Check if the invoice exists and has a contact set
        if (invoice.Id < 1 || invoice.PointOfContact.Id < 1) {
            strHtml += '<tr><td colspan="3" class="center">Please select a contact for this invoice</td></tr>';
        } else {
            //Display the contact details
            strHtml += '<tr><td>Contact</td>';
            strHtml += '<td colspan="2">';
            strHtml += invoice.PointOfContact.Title;
            strHtml += '</td></tr>';
            //Display the invoice address
            strHtml += '<tr><th colspan="3">Invoice Address</th></tr>';
            strHtml += '<tr><td>House No/Name</td>';
            strHtml += '<td colspan="2">'
            strHtml += invoice.HouseNameNo;
            strHtml += '</td></tr>';
            strHtml += '<tr><td>Address 1</td>';
            strHtml += '<td colspan="2">'
            strHtml += invoice.Address1;
            strHtml += '</td></tr>';
            strHtml += '<tr><td>Address 2</td>';
            strHtml += '<td colspan="2">'
            strHtml += invoice.Address2;
            strHtml += '</td></tr>';
            strHtml += '<tr><td>Town</td>';
            strHtml += '<td colspan="2">'
            strHtml += invoice.Town;
            strHtml += '</td></tr>';
            strHtml += '<tr><td>County</td>';
            strHtml += '<td colspan="2">'
            strHtml += invoice.County;
            strHtml += '</td></tr>';
            strHtml += '<tr><td>Post Code</td>';
            strHtml += '<td colspan="2">'
            strHtml += invoice.PostCode;
            strHtml += '</td></tr>';
        }
        strHtml += '</table></div>';
        return strHtml;
    }

    //      Function to edit a contact for an invoice
    function EditInvoiceContact() {

        var ddlQuoteID = document.getElementById('ddlQuoteID');
        var quoteId = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;
        var clientId = document.getElementById('hfClientID').value;
        try {
            Dialog_OpenPage(
                'Edit Invoice Contact and Address',
                'Dialogs/InvoiceRecipient.aspx',
                'content',
                700,
                420,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (InvoiceRecipient_Save()) {
                                $(this).dialog('close');
                                SwitchInvoiceTab(1);
                            }
                        }
                    },
                    'Cancel': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            //Close the dialog and reload the tab to display the added items
                            $(this).dialog('close');
                            SwitchInvoiceTab(1);
                        }
                    }
                },
           'Content/js/InvoiceRecipient.js?V=3.23.5',
           function () { InvoiceRecipient_Load(clientId, quoteId); });
        }
        catch (err) {
            alert('Error display Invoice contacts: ' + err.toString());
        }
    }

    //      Function to change the status of an invoice and display the invoice management panel
    function SetInvoiceStatus(invId, status) {
        try {
            var process = true;
            //  Check if the status is being set to cancelled
            if (status == 99) {
                if (!confirm('Are you sure you want to cancel this invoice?')) {
                    process = false;
                }
            }

            if (process) {
                var updated = Ajax_CallService(
                   'Services/Web/WorkFlow.asmx',
                   'SetInvoiceStatus',
                   {
                       invoiceId: invId,
                       status: status
                   },
                   false,
                   null
               );
                if (updated) {
                    SwitchInvoiceTab(2);
                }
                return updated;
            } else {
                return false;
            }
        } catch (err) {
            return false;
        }
    }

    function AttachDocDialogForInvoice() {
        var modal = document.getElementById('DocAttachModel');
        $("#DocAttachModel").removeClass("hidden");
        $('#pnlDocloading').hide();
        PopulateDocuments();
        var iHeight = 500;
        var iWidth = 600;
        var iAdjustedHeight = Math.min(iHeight, screen.availHeight);
        if (iAdjustedHeight < iHeight) {
            iAdjustedHeight -= 100;
        }

        var iAdjustedWidth = Math.min(iWidth, screen.availWidth);
        if (iAdjustedWidth < iWidth) {
            iAdjustedWidth -= 80;
        }
        var strTitle = "Associated Documents"
        $("#DocAttachModel").dialog({
            autoOpen: false,
            title: strTitle,
            modal: true,
            height: iAdjustedHeight,
            width: iAdjustedWidth,
            resizable: false,
            draggable: true,
            show: {
                effect: 'fade',
                duration: 500
            },
            hide: {
                effect: 'fade',
                duration: 200,
            },
            buttons: {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        AddDocAsAttacmentToEmailForInvoice($(this));
                        //$("#DocAttachModel").addClass("hidden");
                        //$(this).dialog('destroy');
                    }
                },
                'Cancel': {
                    text: 'Cancel',
                    class: 'ui-dialog-button',
                    click: function () {
                        //Close the dialog and reload the tab to display the added items
                        DocList = [];
                        $("#DocAttachModel").addClass("hidden");
                        $(this).dialog('destroy');
                  }
                }
            }
        });

        $('#DocAttachModel').dialog('open');
    }
