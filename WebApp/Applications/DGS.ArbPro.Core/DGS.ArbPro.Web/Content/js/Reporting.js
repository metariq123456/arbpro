﻿var list = null;

function LoadReporting() {
    PopulateFilteringPanel();
}

// Function that populates the part of Seasonal Marketing that contains all of the filters - dropdowns for category and quote status and export to CSV
function PopulateFilteringPanel() {
    debugger;
    var browser = get_browser();
    if (browser.toLowerCase() == 'safari' || browser.toLowerCase() == 'ie 11') {
        var panel = document.getElementById('pnlFilterReportingContent');
        try {
            var curDate = "Select Date";

            var strHtml = '';
            strHtml += '<div style="width:100%; height: 30px;">';
            strHtml += '<label class="clientField"> Select the data you would like to export to CSV.</label>';
            strHtml += '</div>';
            strHtml += '<div style="width:100%; height: 35px;">';
            strHtml += '<label class="paddedExtra float-left" for="ddlReportCategory"> Filter Category:</label>';
            strHtml += '<select id="ddlReportCategory" class="float-left " onchange="FilterReportingCategory();" name="ddlReportCategory">';
            strHtml += '<option value="-1" selected="selected" >Select Category</option>';
            strHtml += '<option value="0">Enquiries/Appointments</option>';
            strHtml += '<option value="1">Quotations</option>';
            strHtml += '<option value="2">Tree Work</option>';
            strHtml += '<option value="3">Sales</option>';
            strHtml += '</select>';

            strHtml += '<label class="paddedExtra float-left" for="ddlReportSubcategory"> Subcategory:</label>';
            strHtml += '<select id="ddlReportSubcategory" class="float-left" name="ddlReportSubcategory">';
            strHtml += '<option value="-1" class="ddl01" selected="selected">Select Subcategory</option>';
            strHtml += '<option value="0" class="ddl0" >Lead Source</option>';
            strHtml += '<option value="1" class="ddl1" >Received</option>';
            strHtml += '<option value="2" class="ddl2" >Sent</option>';
            strHtml += '<option value="3" class="ddl3" >Accepted</option>';
            strHtml += '<option value="4" class="ddl4" >Work Booked</option>';
            strHtml += '<option value="5" class="ddl6" >Work completed</option>';
            strHtml += '<option value="6" class="ddl7" >Sales historically</option>';
            strHtml += '<option value="7" class="ddl8" >Vat value only collected-Paid</option>';
            strHtml += '<option value="8" class="ddl9" > Total Sale value inc VAT - Paid</option>';
            strHtml += '</select>';

            strHtml += '<label class="paddedExtra float-left" for="ddlChartType"> Chart Type:</label>';
            strHtml += '<select id="ddlChartType" class="float-left" onchange="ChartFilter();" name="ddlChartType">';
            strHtml += '<option value="Select" selected="selected">Select Type</option>';
            strHtml += '<option value="pie">Pie</option>';
            strHtml += '<option value="column">Column</option>';
            strHtml += '<option value="line">Line</option>';
            strHtml += '</select>';

            strHtml += '<button type="button" onclick="FilterData();" class="button-thin paddedExtra"> Filter</button>';
            strHtml += '</div>';

            strHtml += '<div style="width:100%; height: 30px;">';
            strHtml += '<label class="paddedExtra" for="txtReportingDateFrom"> From:</label>';
            strHtml += '<input id="txtReportingDateFrom" class="companyInput padded" type="text" placeholder="From Date" name="txtRptDateFrom" value="' + curDate + '"/>';
            strHtml += '<label class="paddedExtra" for="txtReportingDateTo"> To:</label>';
            strHtml += '<input id="txtReportingDateTo" class="companyInput padded" type="text" placeholder="To Date" name="txtRptDateTo" value="' + curDate + '"/>';
            strHtml += '<button id="btnExport" type="button" onclick="ExportReportingToCsv();" class="button-thin paddedExtra"> Export To CSV</button>';
            strHtml += '</div>';

            panel.innerHTML = strHtml;

            $('#txtReportingDateFrom').datepicker({ dateFormat: 'dd/mm/yy', timepicker: false, closeOnDateSelect: true });
            $('#txtReportingDateTo').datepicker({ dateFormat: 'dd/mm/yy', timepicker: false, closeOnDateSelect: true });
            $(".ddl0").attr('disabled', 'disabled');
            $(".ddl1").attr('disabled', 'disabled');
            $(".ddl2").attr('disabled', 'disabled');
            $(".ddl3").attr('disabled', 'disabled');
            $(".ddl4").attr('disabled', 'disabled');
            $(".ddl5").attr('disabled', 'disabled');
            $(".ddl6").attr('disabled', 'disabled');
            $(".ddl7").attr('disabled', 'disabled');
            $(".ddl8").attr('disabled', 'disabled');
            $(".ddl9").attr('disabled', 'disabled');
            $(".ddl10").attr('disabled', 'disabled');

 


        } catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display filter panel- ' + err.toString());
        }
    }
    else {

        var panel = document.getElementById('pnlFilterReportingContent');
        try {
            var curDate = "Select Date";

            var strHtml = '';

            strHtml += '<div style="width:100%; height: 30px;">';
            strHtml += '<label class="clientField"> Select the data you would like to export to CSV.</label>';
            strHtml += '</div>';
            strHtml += '<div style="width:100%; height: 35px;">';
            strHtml += '<label class="paddedExtra float-left" for="ddlReportCategory"> Filter Category:</label>';
            strHtml += '<select id="ddlReportCategory" class="float-left " onchange="FilterReportingCategory();" name="ddlReportCategory">';
            strHtml += '<option value="-1" selected="selected" >Select Category</option>';
            strHtml += '<option value="0">Enquiries/Appointments</option>';
            strHtml += '<option value="1">Quotations</option>';
            strHtml += '<option value="2">Tree Work</option>';
            strHtml += '<option value="3">Sales</option>';
            strHtml += '</select>';

            strHtml += '<label class="paddedExtra float-left" for="ddlReportSubcategory"> Subcategory:</label>';
            strHtml += '<select id="ddlReportSubcategory" class="float-left" name="ddlReportSubcategory">';
            strHtml += '<option value="-1" selected="selected">Select Subcategory</option>';
            strHtml += '<option value="0" hidden="hidden">Lead Source</option>';
            strHtml += '<option value="1" hidden="hidden">Received</option>';
            strHtml += '<option value="2" hidden="hidden">Sent</option>';
            strHtml += '<option value="3" hidden="hidden">Accepted</option>';
            strHtml += '<option value="4" hidden="hidden">Work Booked</option>';
            strHtml += '<option value="5" hidden="hidden">Work completed</option>';
            strHtml += '<option value="6" hidden="hidden">Sales historically</option>';
            strHtml += '<option value="7" class="ddl8" >Vat value only collected-Paid</option>';
            strHtml += '<option value="8" class="ddl9" > Total Sale value inc VAT - Paid</option>';
            strHtml += '</select>';

            strHtml += '<label class="paddedExtra float-left" for="ddlChartType"> Chart Type:</label>';
            strHtml += '<select id="ddlChartType" class="float-left" onchange="ChartFilter();" name="ddlChartType">';
            strHtml += '<option value="Select" selected="selected">Select Type</option>';
            strHtml += '<option value="pie">Pie</option>';
            strHtml += '<option value="column">Column</option>';
            strHtml += '<option value="line">Line</option>';
            strHtml += '</select>';

            strHtml += '<button type="button" onclick="FilterData();" class="button-thin paddedExtra"> Filter</button>';
            strHtml += '</div>';

            strHtml += '<div style="width:100%; height: 30px;">';
            strHtml += '<label class="paddedExtra" for="txtReportingDateFrom"> From:</label>';
            strHtml += '<input id="txtReportingDateFrom" class="companyInput padded" type="text" placeholder="From Date" name="txtRptDateFrom" value="' + curDate + '"/>';
            strHtml += '<label class="paddedExtra" for="txtReportingDateTo"> To:</label>';
            strHtml += '<input id="txtReportingDateTo" class="companyInput padded" type="text" placeholder="To Date" name="txtRptDateTo" value="' + curDate + '"/>';
            strHtml += '<button type="button" id="btnExport" onclick="ExportReportingToCsv();" class="button-thin paddedExtra"> Export To CSV</button>';
            strHtml += '</div>';

            panel.innerHTML = strHtml;

            $('#txtReportingDateFrom').datepicker({ dateFormat: 'dd/mm/yy', timepicker: false, closeOnDateSelect: true });
            $('#txtReportingDateTo').datepicker({ dateFormat: 'dd/mm/yy', timepicker: false, closeOnDateSelect: true });

        } catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display filter panel- ' + err.toString());
        }
    }
}

function FilterReportingCategory() {
    debugger;
    var browser = get_browser();
    if (browser.toLowerCase() == 'safari' || browser.toLowerCase() == 'ie 11') {
    
        var category = document.getElementById('ddlReportCategory');
        var categorySelected = category.options[category.selectedIndex].value;

        var subcategory = document.getElementById('ddlReportSubcategory');

        for (var i = 1; i < subcategory.options.length; i++) {
            subcategory.options[i].hidden = true;
        }

        switch (categorySelected) {
            case "0":
                $(".ddl0").attr('disabled', false);
                $(".ddl1").attr('disabled', false);
                $(".ddl2").attr('disabled', 'disabled');
                $(".ddl3").attr('disabled', 'disabled');
                $(".ddl4").attr('disabled', 'disabled');
                $(".ddl5").attr('disabled', 'disabled');
                $(".ddl6").attr('disabled', 'disabled');
                $(".ddl7").attr('disabled', 'disabled');
                $(".ddl8").attr('disabled', 'disabled');
                $(".ddl9").attr('disabled', 'disabled');



                subcategory.selectedIndex = 1;
                document.getElementById("txtReportingDateFrom").disabled = false;
                document.getElementById("txtReportingDateTo").disabled = false;
                document.getElementById("btnExport").disabled = false;
                break;
            case "1":
                $(".ddl2").attr('disabled', false);
                $(".ddl3").attr('disabled', false);
                $(".ddl0").attr('disabled', 'disabled');
                $(".ddl1").attr('disabled', 'disabled');
                $(".ddl4").attr('disabled', 'disabled');
                $(".ddl5").attr('disabled', 'disabled');
                $(".ddl6").attr('disabled', 'disabled');
                $(".ddl7").attr('disabled', 'disabled');
                $(".ddl8").attr('disabled', 'disabled');
                $(".ddl9").attr('disabled', 'disabled');

                subcategory.selectedIndex = 3;
                document.getElementById("txtReportingDateFrom").disabled = false;
                document.getElementById("txtReportingDateTo").disabled = false;
                document.getElementById("btnExport").disabled = false;
                break;
            case "2":
                $(".ddl4").attr('disabled', false);
                $(".ddl5").attr('disabled', false);
                $(".ddl6").attr('disabled', false);
                $(".ddl0").attr('disabled', 'disabled');
                $(".ddl1").attr('disabled', 'disabled');
                $(".ddl2").attr('disabled', 'disabled');
                $(".ddl3").attr('disabled', 'disabled');
                $(".ddl7").attr('disabled', 'disabled');
                $(".ddl8").attr('disabled', 'disabled');
                $(".ddl9").attr('disabled', 'disabled');

                subcategory.selectedIndex = 5;
                document.getElementById("txtReportingDateFrom").disabled = false;
                document.getElementById("txtReportingDateTo").disabled = false;
                document.getElementById("btnExport").disabled = false;
                break;
            case "3":
                $(".ddl7").attr('disabled', false);
                $(".ddl8").attr('disabled', false);
                $(".ddl0").attr('disabled', 'disabled');
                $(".ddl1").attr('disabled', 'disabled');
                $(".ddl2").attr('disabled', 'disabled');
                $(".ddl3").attr('disabled', 'disabled');
                $(".ddl4").attr('disabled', 'disabled');
                $(".ddl5").attr('disabled', 'disabled');
                $(".ddl6").attr('disabled', 'disabled');
                $(".ddl9").attr('disabled', false);
                subcategory.selectedIndex = 8;
                document.getElementById("txtReportingDateFrom").disabled = false;
                document.getElementById("txtReportingDateTo").disabled = false;
                document.getElementById("btnExport").disabled = false;
                break;
            default:
                $(".ddl0").attr('disabled', 'disabled');
                $(".ddl1").attr('disabled', 'disabled');
                $(".ddl2").attr('disabled', 'disabled');
                $(".ddl3").attr('disabled', 'disabled');
                $(".ddl4").attr('disabled', 'disabled');
                $(".ddl5").attr('disabled', 'disabled');
                $(".ddl6").attr('disabled', 'disabled');
                $(".ddl7").attr('disabled', 'disabled');
                $(".ddl8").attr('disabled', 'disabled');
                $(".ddl9").attr('disabled', 'disabled');
                break;
        }
    }
    else {
        var category = document.getElementById('ddlReportCategory');
        var categorySelected = category.options[category.selectedIndex].value;

        var subcategory = document.getElementById('ddlReportSubcategory');

        for (var i = 1; i < subcategory.options.length; i++) {
            subcategory.options[i].hidden = true;
        }

        switch (categorySelected) {
            case "0":
                subcategory.options[1].hidden = false;
                subcategory.options[2].hidden = false;
                subcategory.selectedIndex = 1;
                document.getElementById("txtReportingDateFrom").disabled = false;
                document.getElementById("txtReportingDateTo").disabled = false;
                document.getElementById("btnExport").disabled = false;
                break;
            case "1":
                subcategory.options[3].hidden = false;
                subcategory.options[4].hidden = false;
                subcategory.selectedIndex = 3;
                document.getElementById("txtReportingDateFrom").disabled = false;
                document.getElementById("txtReportingDateTo").disabled = false;
                document.getElementById("btnExport").disabled = false;
                break;
            case "2":
                subcategory.options[5].hidden = false;
                subcategory.options[6].hidden = false;
                subcategory.selectedIndex = 5;
                document.getElementById("txtReportingDateFrom").disabled = false;
                document.getElementById("txtReportingDateTo").disabled = false;
                document.getElementById("btnExport").disabled = false;
                break;
            case "3":
                subcategory.options[7].hidden = false;
                subcategory.options[8].hidden = false;
                subcategory.options[9].hidden = false;
                subcategory.selectedIndex = 8;
                document.getElementById("txtReportingDateFrom").disabled = false;
                document.getElementById("txtReportingDateTo").disabled = false;
                document.getElementById("btnExport").disabled = false;
                break;
            default:
                subcategory.options[0].hidden = false;
                break;
        }
    }
}

function FilterData() {
    debugger;
    $("#container").ejChart(
        {
            //Initializing empty series                                             
            series: [{}],
        });
    ClearPoints();
    list = null;
    var subcategory = document.getElementById('ddlReportSubcategory');
    var subcategorySelected = subcategory.options[subcategory.selectedIndex].value;
    var start = document.getElementById('txtReportingDateFrom').value;
    var end = document.getElementById('txtReportingDateTo').value;
    var startDate = toUTCString(start, "en_GB");
    var endDate = toUTCString(end, "en_GB");
    var categoryint = parseInt(subcategorySelected)
    switch (categoryint) {

        case 0:
            try {
                var categoryint = parseInt(subcategorySelected)
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%"
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 1:
            try {
                 list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%"
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 2:
            try {
                 list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%," + " Value " + CurrentCurrency + a.Value
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }

            break;
        case 3:
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%, " + " Value " + CurrentCurrency + a.Value
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 4:
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%" + " Value " + CurrentCurrency + a.Value
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 5:
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%" + " Value " + CurrentCurrency + a.Value
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 6:
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%" + " Value " + CurrentCurrency + a.Value
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 7:
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%" + " Value " + CurrentCurrency + a.Value
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 8:
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%" + " Value " + CurrentCurrency + a.Value
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 9:
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%" + " Value " + CurrentCurrency + a.Value
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 10:
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%" + " Value " + CurrentCurrency + a.Value
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 11:
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name + ", " + a.PercentageAmount + "%" + " Value " + CurrentCurrency + a.Value
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 12:
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );
                var points = [];
                //points.push({
                //    x: ' ',
                //    y: ' ',
                //    text: ' '
                //});
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " +
                            "\n" + a.Name
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        case 13:
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'MainFilteringReportingData',
                    {
                        subCatId: categoryint,
                        startDate: startDate,
                        endDate: endDate
                    },
                    false
                );

                var points = [];
                list.forEach(function (a) {
                    points.push({
                        x: a.Name,
                        y: a.Amount,
                        text: "Amount: " + a.Amount + ", " + a.Name
                    });
                });

                ChartFilter(points, subcategory.options[subcategory.selectedIndex].text);
            } catch (e) {

            }
            break;
        default:
            subcategory.options[0].hidden = false;
            break;
    }



}

function ExportReportingToCsv() {
    debugger;
    var subcategory = document.getElementById('ddlReportSubcategory');
    var subcategorySelected = subcategory.options[subcategory.selectedIndex].value;
    var start = document.getElementById('txtReportingDateFrom').value;
    var end = document.getElementById('txtReportingDateTo').value;
    var FromDate = toUTCString(start, "en_GB");
    var ToDate = toUTCString(end, "en_GB");
    switch (subcategorySelected) {

        case "0":
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'GraphExportforLeadSource',
                    {
                        FromDate: FromDate,
                        ToDate: ToDate
                    },
                    false
                );

                if (window.navigator.msSaveOrOpenBlob) {
                    var blobObject = new Blob([list]);
                    var success = window.navigator.msSaveOrOpenBlob(blobObject, "Lead Source.csv");
                } else {
                    var a = window.document.createElement('a');
                    a.href = window.URL.createObjectURL(new Blob([list], { type: 'text/csv' }));
                    a.download = "Lead Source.csv";

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();
                    // Remove anchor from body
                    document.body.removeChild(a);
                }

            } catch (e) {

            }
            break;
        case "1":
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'GraphExportforReceived',
                    {
                        FromDate: FromDate,
                        ToDate: ToDate
                    },
                    false
                );

                if (window.navigator.msSaveOrOpenBlob) {
                    var blobObject = new Blob([list]);
                    var success = window.navigator.msSaveOrOpenBlob(blobObject, "Received.csv");
                } else {
                    var a = window.document.createElement('a');
                    a.href = window.URL.createObjectURL(new Blob([list], { type: 'text/csv' }));
                    a.download = "Received.csv";

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();
                    // Remove anchor from body
                    document.body.removeChild(a);
                }
            } catch (e) {

            }
            break;
        case "2":
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'GraphExportforQuotationSent',
                    {
                        FromDate: FromDate,
                        ToDate: ToDate
                    },
                    false
                );

                if (window.navigator.msSaveOrOpenBlob) {
                    var blobObject = new Blob([list]);
                    var success = window.navigator.msSaveOrOpenBlob(blobObject, "Sent Quotes.csv");
                } else {
                    var a = window.document.createElement('a');
                    a.href = window.URL.createObjectURL(new Blob([list], { type: 'text/csv' }));
                    a.download = "Sent Quotes.csv";

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();
                    // Remove anchor from body
                    document.body.removeChild(a);
                }
            } catch (e) {

            }
            break;
        case "3":
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'GraphExportforQuotationAccepted',
                    {
                        FromDate: FromDate,
                        ToDate: ToDate
                    },
                    false
                );

                if (window.navigator.msSaveOrOpenBlob) {
                    var blobObject = new Blob([list]);
                    var success = window.navigator.msSaveOrOpenBlob(blobObject, "Accepted Quotes.csv");
                } else {
                    var a = window.document.createElement('a');
                    a.href = window.URL.createObjectURL(new Blob([list], { type: 'text/csv' }));
                    a.download = "Accepted Quotes.csv";

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();
                    // Remove anchor from body
                    document.body.removeChild(a);
                }
            } catch (e) {

            }
            break;
        case "4":
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'GraphExportforTreeWorkBooked',
                    {
                        FromDate: FromDate,
                        ToDate: ToDate
                    },
                    false
                );

                if (window.navigator.msSaveOrOpenBlob) {
                    var blobObject = new Blob([list]);
                    var success = window.navigator.msSaveOrOpenBlob(blobObject, "Work Booked.csv");
                } else {
                    var a = window.document.createElement('a');
                    a.href = window.URL.createObjectURL(new Blob([list], { type: 'text/csv' }));
                    a.download = "Work Booked.csv";

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();
                    // Remove anchor from body
                    document.body.removeChild(a);
                }
            } catch (e) {

            }
            break;
        case "5":
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'GraphExportforTreeCompleted',
                    {
                        FromDate: FromDate,
                        ToDate: ToDate
                    },
                    false
                );

                if (window.navigator.msSaveOrOpenBlob) {
                    var blobObject = new Blob([list]);
                    var success = window.navigator.msSaveOrOpenBlob(blobObject, "Work Completed.csv");
                } else {
                    var a = window.document.createElement('a');
                    a.href = window.URL.createObjectURL(new Blob([list], { type: 'text/csv' }));
                    a.download = "Work Completed.csv";

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();
                    // Remove anchor from body
                    document.body.removeChild(a);
                }
            } catch (e) {

            }
            break;
        case "6":
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'GraphExportforSalesHistorically',
                    {
                        FromDate: FromDate,
                        ToDate: ToDate
                    },
                    false
                );

                if (window.navigator.msSaveOrOpenBlob) {
                    var blobObject = new Blob([list]);
                    var success = window.navigator.msSaveOrOpenBlob(blobObject, "Sales Historically.csv");
                } else {
                    var a = window.document.createElement('a');
                    a.href = window.URL.createObjectURL(new Blob([list], { type: 'text/csv' }));
                    a.download = "Sales Historically.csv";

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();
                    // Remove anchor from body
                    document.body.removeChild(a);
                }
            } catch (e) {

            }
            break;
        case "7":
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'GraphExportforInvoicesVatPaid',
                    {
                        FromDate: FromDate,
                        ToDate: ToDate
                    },
                    false
                );

                if (window.navigator.msSaveOrOpenBlob) {
                    var blobObject = new Blob([list]);
                    var success = window.navigator.msSaveOrOpenBlob(blobObject, "Sales Outstanding.csv");
                } else {
                    var a = window.document.createElement('a');
                    a.href = window.URL.createObjectURL(new Blob([list], { type: 'text/csv' }));
                    a.download = "Invoices VAT Paid.csv";

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();
                    // Remove anchor from body
                    document.body.removeChild(a);
                }
            } catch (e) {

            }
            break;
        case "8":
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'GraphExportforInvoicesTotalSaleIncVat',
                    {
                        FromDate: FromDate,
                        ToDate: ToDate
                    },
                    false
                );

                if (window.navigator.msSaveOrOpenBlob) {
                    var blobObject = new Blob([list]);
                    var success = window.navigator.msSaveOrOpenBlob(blobObject, "Invoiced Paid.csv");
                } else {
                    var a = window.document.createElement('a');
                    a.href = window.URL.createObjectURL(new Blob([list], { type: 'text/csv' }));
                    a.download = "Invoices Total Paid.csv";

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();
                    // Remove anchor from body
                    document.body.removeChild(a);
                }
            } catch (e) {

            }
            break;
        case "9":
            try {
                list = Ajax_CallService(
                    'Services/Web/ReportingData.asmx',
                    'GraphExportforInvoicesVatTotalSales',
                    {
                        FromDate: FromDate,
                        ToDate: ToDate
                    },
                    false
                );

                if (window.navigator.msSaveOrOpenBlob) {
                    var blobObject = new Blob([list]);
                    var success = window.navigator.msSaveOrOpenBlob(blobObject, "Invoices Paid.csv");
                } else {
                    var a = window.document.createElement('a');
                    a.href = window.URL.createObjectURL(new Blob([list], { type: 'text/csv' }));
                    a.download = "Invoices Vat Paid.csv";

                    // Append anchor to body.
                    document.body.appendChild(a);
                    a.click();
                    // Remove anchor from body
                    document.body.removeChild(a);
                }
            } catch (e) {

            }
            break;

        default:
            subcategory.options[0].hidden = false;
            break;
    }



}
function ExportToCSV(list)
{
    debugger;
    var Export = list;
    if (window.navigator.msSaveOrOpenBlob) {
        var blobObject = new Blob([Export]);
        var success = window.navigator.msSaveOrOpenBlob(blobObject, filename);
    } else {
        var a = window.document.createElement('a');
        a.href = window.URL.createObjectURL(new Blob([Export], { type: 'text/csv' }));
        a.download = filename;

        // Append anchor to body.
        document.body.appendChild(a);
        a.click();
        // Remove anchor from body
        document.body.removeChild(a);
    }


}

function ChartFilter(pts, title) {
    debugger;
    var chart = document.getElementById('ddlChartType');
    var chartType = chart.options[chart.selectedIndex].value;

  $("#container")
        .ejChart(
        {
    //Initializing Series
            series:
            [
                {
                    points: pts,
                    marker:
                    {
                        dataLabel:
                        {
                            visible: true,
                            shape: 'none',
                            enableWrap : true,
                            labelPosition: "OutsideExtended",
                            angle : 0,
                            //connectorLine: { type: 'bezier', color: 'black' },
                            font: { size: '12px' }
                        }
                    },
                    border: { width: 2, color: 'white' },
                    name: title,
                    type: chartType,    // options include: pie, bar, column, line, stepline, area, steparea, pyramid, funnel etc. - visible from demo on syncfusion
                    enableAnimation: true,
                    labelPosition: 'outsideExtended',
                    enableSmartLabels: true,
                    startAngle: 90
                }
            ],
            load: "loadTheme",
            seriesRendering: "seriesRender",
            title: { text: title },
            isResponsive: true,
            size: { height: "600" },
            legend: { visible: false }, // theme: options available - visible from properties.js
            exportCanvasRendering : true
        });
}
function ClearPoints() {

    var chartObj = $("#container").ejChart("instance");

    for (var i = 0; i < chartObj.model.series.length; i++)
        //Empty the point collection in each series before updating
        chartObj.model.series[i].points = [];
}
function Redraw() {
    var chartObj = $("#container").data("ejChart");
    chartObj.model.series[0].points[5].y = 5;
    chartObj.model.series[0].type = "column";
    $("#container").ejChart("redraw");
}


