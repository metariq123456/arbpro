﻿var ListContacts = null;
var SelectedId = -1;
var ClientId;
var ObjInvoice;

//Function to load the dialog content
function InvoiceRecipient_Load(clientId, quoteId) {
    if (GetCurrentInvoiceForQuote(quoteId)) {
        if (ObjInvoice.Id < 1) {
            ObjInvoice.QuoteId = quoteId;
            ObjInvoice.ClientId = clientId;
        }
        GetContactsForClient(clientId);
    }else{
        //There has been an error and the invoice has not been populated
        //This blocks all functionality and so nothing can be done
        DisplayMessage('invPnlRecipientError', 'Error: The Invoice has not been loaded from the service correctly. Pl');
    }
}

//Function to update the invoice with the information from the dialog
function InvoiceRecipient_Save() {
    try
    {
        //Set the invoice valuies ready for updateing
        //NOTE: Unlike other objects the invoice address is stored in the invoice table. This is to ensure data integrity and prevent 
        //client addresses being deleted and invalidating the invoice record
        ObjInvoice.PointOfContact.Id = SelectedId;
        ObjInvoice.HouseNameNo = document.getElementById('txtContactStreetNumber').value;
        ObjInvoice.Address1 = document.getElementById('txtContactAddress1').value;
        ObjInvoice.Address2 = document.getElementById('txtContactAddress2').value;
        ObjInvoice.Town = document.getElementById('txtContactTown').value;
        ObjInvoice.County = document.getElementById('txtContactCounty').value;
        ObjInvoice.PostCode = document.getElementById('txtContactPostCode').value;
        //The date components must be refreshed and are irrelivant at this stage
        ObjInvoice.DateSent = new Date(0);
        ObjInvoice.DueDate = new Date(0);
        var retVal = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'SaveInvoice',
            {
                obj: ObjInvoice,
                selectedItems: []
            },
            false,
            null);

        if (!retVal) {
            DisplayMessage('invPnlRecipientError', 'Error: The Invoice has not been saved correctly.');
        }

        return retVal;
    }
    catch (err) {
        DisplayMessage('invPnlRecipientError', 'Error: The Invoice has not been saved correctly: ' + err.toString());
        return false;
    }
}

//Async method to load all the client contact for the Invoice from the Quote
function GetContactsForClient(clientId) {
    try {
        Ajax_CallService(
           'Services/Web/ClientData.asmx',
           'GetClientContacts',
           {
               clientID: clientId
           },
           true,
           PopulateContactList
       );
    }
    catch (err) {
        DisplayMessage('invPnlRecipientError', 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//Synchronus call to get a current / new invoice
function GetCurrentInvoiceForQuote(quoteId) {
    try {
        
        ObjInvoice = Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetInvoiceForQuote',
           {
               quoteId: quoteId
           },
           false,
           null
       );
        return true;
    }
    catch (err) {
        DisplayMessage('invPnlRecipientError', 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
        return false;
    }
}

//Async callback to populate the possible contacts for the invoice
function PopulateContactList(results) {
    
    var panel = document.getElementById('invPnlContacts');
    //Check if there is valid data. NOTE: There should always be at least one contact
    if (!results || results.length == 0) {
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data.');
    }
    try
    {
        ListContacts = results;
        var strHtml = '';
        strHtml += '<div id="pnlContactContainer" class="float-left width-full" style="overflow-y: scroll;">';
        strHtml += '<table class="searchGrid width-full white">';
        strHtml += '<tr><th>Name</th><th>Type</th></tr>';
        var count = ListContacts.length;
        var selected = -1;
        for (var index = 0; index < count; index++) {
            //Check if the current contact is the selected contact
            if (ObjInvoice.PointOfContact.Id == ListContacts[index].Id || count == 1) {
                selected = index;
            }
            //Generate the html for the row that represents the current contact
            strHtml += '<tr onclick="ContactSelected(' + index.toString() +');">';
            strHtml += '<td>'
            strHtml += ListContacts[index].FormatedName;
            strHtml += '</td>';
            strHtml += '<td>'
            strHtml += ListContacts[index].ContactTypeString;
            strHtml += '</td>';
            strHtml += '</tr>';
        }

        panel.innerHTML = strHtml;
        //Create the panel to display the contact details
        CreateContactDisplay(selected);
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display client contacts: ' + err.toString());
    }
}

//Function to generate the html to display a selected contact
function CreateContactDisplay(selected) {
    var panel = document.getElementById('invPnlInvAddresses');
    try
    {
        var strHtml = '';

        strHtml += '<div class="width-full center float-left">';
        strHtml += '<div>';
        strHtml += '<div class="clientField">';
        strHtml += '<span class="ui-helper-hidden-accessible" role="status" aria-live="polite"></span>';
        strHtml += '<input id="txtContactTitle" class="clientInput ui-autocomplete-input" type="text" placeholder="Title" title="Title" name="txtContactTitle" autocomplete="off" disabled="disabled">';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<input id="txtContactForename" class="clientInput" type="text" placeholder="Forename" title="Forename" name="txtContactForename" disabled="disabled">';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<input id="txtContactSurname" class="clientInput" type="text" placeholder="Surname" title="Surname" name="txtContactSurname" disabled="disabled">';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<input id="txtContactTelephone" class="clientInput" type="text" placeholder="Telephone Number" title="Telephone Number" name="txtContactTelephone" disabled="disabled">';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<input id="txtContactMobile" class="clientInput" type="text" placeholder="Mobile Number" title="Mobile Number" name="txtContactMobile" disabled="disabled">';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<input id="txtContactEmail" class="clientInput" type="text" placeholder="Email Address" title="Email Address"  name="txtContactEmail" disabled="disabled">';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<input id="txtContactStreetNumber" class="clientInput" type="text" placeholder="House Name / Number" title="House Name / Number" name="txtContactStreetNumber">';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<input id="txtContactAddress1" class="clientInput" type="text" placeholder="Address Line 1" title="Address Line 1"  name="txtContactAddress1">';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<input id="txtContactAddress2" class="clientInput" type="text" placeholder="Address Line 2" title="Address Line 2"  name="txtContactAddress2">';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<span class="ui-helper-hidden-accessible" role="status" aria-live="polite"></span>';
        strHtml += '<input id="txtContactTown" class="clientInput ui-autocomplete-input" type="text" placeholder="Town" title="Town"  name="txtContactTown" autocomplete="off">';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<span class="ui-helper-hidden-accessible" role="status" aria-live="polite"></span>';
        strHtml += '<input id="txtContactCounty" class="clientInput ui-autocomplete-input" type="text" placeholder="County" title="County" name="txtContactCounty" autocomplete="off">';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<input id="txtContactPostCode" class="clientInput" type="text" placeholder="Post Code" title="Post Code" value="XXXX X1" name="txtContactPostCode">';
        strHtml += '</div>';
        strHtml += '</div>';
        strHtml += '</div>';

        panel.innerHTML = strHtml;
        //The html has been created and add to the page now populate the selected contact
        ContactSelected(selected);

        //Now that the default values have been selected they must be overloaded with any values in the invoice
        //but only if the street number or address line one are populated
        if (ObjInvoice.HouseNameNo.length > 0 || ObjInvoice.Address1.length > 0) {
            document.getElementById('txtContactStreetNumber').value = ObjInvoice.HouseNameNo;
            document.getElementById('txtContactAddress1').value = ObjInvoice.Address1;
            document.getElementById('txtContactAddress2').value = ObjInvoice.Address2;
            document.getElementById('txtContactTown').value = ObjInvoice.Town;
            document.getElementById('txtContactCounty').value = ObjInvoice.County;
            document.getElementById('txtContactPostCode').value = ObjInvoice.PostCode;
        }
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to create HTML elements: ' + err.to());
    }
}

//Row selected event handler to display the selected contacts details
function ContactSelected(listIndex) {
    //Check if the param is a valid index
    if (listIndex >= 0 && listIndex < ListContacts.length) {
        var contact = ListContacts[listIndex];
        try{
            document.getElementById('txtContactTitle').value = contact.Title;
            document.getElementById('txtContactForename').value = contact.Forename;
            document.getElementById('txtContactSurname').value = contact.Surname;
            document.getElementById('txtContactStreetNumber').value = contact.StreetNumber;
            document.getElementById('txtContactTelephone').value = contact.Telephone;
            document.getElementById('txtContactMobile').value = contact.Mobile;
            document.getElementById('txtContactEmail').value = contact.Email;
            document.getElementById('txtContactAddress1').value = contact.Address1;
            document.getElementById('txtContactAddress2').value = contact.Address2;
            document.getElementById('txtContactTown').value = contact.Town;
            document.getElementById('txtContactCounty').value = contact.County;
            document.getElementById('txtContactPostCode').value = contact.PostCode;
            SelectedId = contact.Id;
        }
        catch (err) {
            DisplayMessage('invPnlInvAddresses', 'Error: Unable to display details of the selected contact.');
        }
    }
}
