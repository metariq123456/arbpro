﻿
var Risk_curTab = 1;
var Section_curTab = 1;
var Criterion_curTab = 1;
var sections = null;
var riskList = null;
var risk = null;

var Job_ListShown = false;

//      Async callback function to display the side bar details
function SidePanel_Display() {
    riskList = GetAllRiskTemplates();
    var riskId = -1;
    var panel = document.getElementById('pnlRiskListList');

    if (riskList && riskList.length > 0) {
        try {
            var strHtml = '<table id="riskListTable" class="searchGrid width-full">';

            //Construct the html
            riskList.forEach(function(r) {
                if (!r.IsDeleted) {
                    strHtml += '<tr id="risk' + r.Id + '" onclick="TemplateLoad(' + r.Id + ')";"><td>' + r.Name + '</td></tr>';
                    if (riskId < 0) {
                        riskId = r.Id;
                    }
                }
            });
            strHtml += '</table>';
            //display the grid
            panel.innerHTML = strHtml;
            TemplateLoad(riskId);
        } catch (err) {
            DisplayMessage(panel.id, 'ERROR: Unable to display Risk list - ' + err.toString());
        }
    } else {
        DisplayMessage(panel.id, 'There are no Risks to display. Please Select "Create New Template" in order to add a new risk template.');
    }
}

function TemplateLoad(riskId) {
    risk = [];

    // get the correct risk template
    riskList.forEach(function(r) {
        if (!r.IsDeleted) {
            if (r.Id == riskId) {
                risk = r;
                document.getElementById("risk" + r.Id).className = "selected";
            } else {
                document.getElementById("risk" + r.Id).className = "";
            }
        }
    });

    PopulateRiskDetails(risk);
    PopulateSectionList(risk.TemplateSections);

    SwitchRiskStageTab(0);
}

function PopulateRiskDetails(objRisk) {
    var panel = document.getElementById('pnlRiskStage_0');

    try {
        var strHtml = '';

        strHtml += '<div style="height: 130px; overflow: auto; width-full;">';
        strHtml += '<table id="riskListTable" class="enquiryGrid width-full"><col width="25%"></col><col width="73%"></col>';
        strHtml += '<tr><td colspan="2">&nbsp </td></tr>';
        strHtml += '<tr><td class="rowHeader">Name </td><td>' + objRisk.Name + '</td></tr>';
        strHtml += '<tr><td class="rowHeader">Reference Document </td><td>' + objRisk.ReferenceDoc + '</td></tr>';
        strHtml += '<tr><td class="rowHeader">Description </td><td>' + objRisk.Description + '</td></tr>';
        strHtml += '<tr><td class="rowHeader">Type </td><td>';
        if (objRisk.TemplateType == 0) {
            strHtml += 'Default';
        }
        else if (objRisk.TemplateType == 1) {
            strHtml += 'General';
        }
        else if (objRisk.TemplateType == 2) {
            strHtml += 'Specific';
        }
        strHtml += '</td></tr>';
        strHtml += '</table>';
        strHtml += '</div>';

        strHtml += PopulateSectionInRiskdetails(objRisk.TemplateSections);

        strHtml += '<button type="button" onclick="NewOrEditRiskTemplate(' + objRisk.Id + ');" class="button-thin padded float-right padded"> Edit Template </button>';
        if (objRisk.IsEditable) {
            document.getElementById('btnAddSections').disabled = false;
            strHtml += '<button type="button" onclick="CancelTemplate(' + objRisk.Id + ');" class="button-thin padded float-left padded"> Remove Template</button>';
        } else {
            document.getElementById('btnAddSections').disabled = true;
            strHtml += '<button type="button" disabled="true" onclick="CancelTemplate(' + objRisk.Id + ');" class="button-thin padded float-left padded"> Remove Template</button>';
        }

        panel.innerHTML = strHtml;
    } catch (err) {
        DisplayMessage(panel.id, 'ERROR: Unable to display Risk Details - ' + err.toString());
    }
}

function PopulateSectionInRiskdetails(sectionList) {
    var strHtml = '';

    if (sectionList && sectionList.length > 0) {
        strHtml += '<div style="height: 86px; overflow: auto; width-full;">';
        strHtml += '<table Width="98%" class="rtGrid"><col width="19%"></col><col width="19%"></col><col width="19%"></col><col width="47%"></col>';
        strHtml += '<tr><td colspan="4" class="rowHeader">&nbsp</td></tr>';
        strHtml += '<tr><th colspan="4">Template Sections </th></tr>';
        strHtml += '<tr><td class="rowHeader">Name</td><td class="rowHeader">Reference Document</td><td class="rowHeader">Chapter Reference</td><td class="rowHeader">Description</td></tr>';
        strHtml += '</table>';
        strHtml += '</div>';

        strHtml += '<div style="height: 310px; overflow: auto; width-full;">';
        strHtml += '<table Width="98%" id="criteriaDetailsTable" class="rtGrid"><col width="19%"></col><col width="19%"></col><col width="19%"></col><col width="47%"></col>';

        sectionList.forEach(function (s) {
            if (!s.IsDeleted) {
                strHtml += '<tr><td class="rowData">' + s.Title + '</td>';
                strHtml += '<td class="rowData">' + s.ReferenceDoc + '</td>';
                strHtml += '<td class="rowData">' + s.ChapterRef + '</td>';
                strHtml += '<td class="rowData">' + s.Description + '</td></tr>';
            }
        });

        strHtml += '</table>';
        strHtml += '</div>';
    } else {
        strHtml += '<div style="height: 126px; overflow: auto; width-full;">';
        strHtml += '</div>';
        strHtml += '<div style="height: 260px; overflow: auto; width-full;">';
        strHtml += '<table id="criteriaDetailsTable" Width="98%" class="rtGrid">';
        strHtml += '<tr><td class="center warning-text">There are no sections for this template. </td></tr>';
        strHtml += '<tr><td class="center warning-text">Please go to the Sections tab and click on "Add Sections" to create new or add sections to this template.</td></tr>';
        strHtml += '</table>';
        strHtml += '</div>';
    }


    return strHtml;
}

function PopulateSectionList(riskSections) {
    sections = [];
    document.getElementById('pnlSectionDetailsWait').className = "hidden";
    var panel = document.getElementById('pnlSectionList');
    sections = riskSections;

    try {
        var strHtml = '';
        
        strHtml += '<div style="height: 530px; overflow: auto; width-full;">';
        strHtml += '<table id="sectionListTable" class="searchGrid width-full">';

        //Construct the html
        sections.forEach(function (s) {
            if (!s.IsDeleted) {
                strHtml += '<tr id="section' + s.Id + '" onclick="SectionDetailsLoad(' + s.Id + ')";"><td>' + s.Title + '</td></tr>';
            }
        });
        strHtml += '</table>';
        strHtml += '</div>';

        //display the grid
        panel.innerHTML = strHtml;
        SectionDetailsLoad();
    } catch (err) {
        DisplayMessage(panel.id, 'ERROR: Unable to display Risk list - ' + err.toString());
    }
}

function SectionDetailsLoad(sectionId) {
    var panel = document.getElementById('pnlSectionDetails');
    var strHtml = '';

    if (sectionId) {
        var section = [];
        sections.forEach(function (s) {
            if (!s.IsDeleted) {
                document.getElementById("section" + s.Id).className = "";
                if (s.Id == sectionId) {
                    section = s;
                    document.getElementById("section" + s.Id).className = "lmGridItem_Report";

                    strHtml += '<div style="height: 170px; overflow: auto; width-full;">';
                    strHtml += '<table id="sectionDetailsTable" class="enquiryGrid width-full"><col width="35%"></col><col width="64%">';
                    strHtml += '<tr><th colspan="2">Selected Section Details </th></tr>';
                    strHtml += '<tr><td class="rowHeader">Name </td><td>' + section.Title + '</td></tr>';
                    strHtml += '<tr><td class="rowHeader">Reference Document </td><td>' + section.ReferenceDoc + '</td></tr>';
                    strHtml += '<tr><td class="rowHeader">Chapter Reference </td><td>' + section.ChapterRef + '</td></tr>';
                    strHtml += '<tr><td class="rowHeader">Description </td><td>' + section.Description + '</td></tr>';
                    strHtml += '</table>';
                    strHtml += '</div>';

                    strHtml += PopulateCriterionDetails(section.TemplateCriteria);

                    if (risk.IsEditable) {
                        strHtml += '<button type="button" onclick="EditSection(' + section.Id + ');" class="button-thin padded float-right padded"> Add Criteria & Edit Section </button>';
                        strHtml += '<button type="button" onclick="window.location=\'/RiskCriteria.aspx\';" class="button-thin padded float-right padded"> Go to Criteria </button>';
                        strHtml += '<button type="button" onclick="RemoveFromTemplate(' + section.Id + ');" class="button-thin padded float-left padded"> Remove From Template</button>';
                    }
                    else {
                        strHtml += '<button type="button" disabled="true" onclick="EditSection(' + section.Id + ');" class="button-thin padded float-right padded"> Add Criteria & Edit Section </button>';
                        strHtml += '<button type="button" disabled="true" onclick="window.location=\'/RiskCriteria.aspx\';" class="button-thin padded float-right padded"> Go to Criteria </button>';
                        strHtml += '<button type="button" disabled="true" onclick="RemoveFromTemplate(' + section.Id + ');" class="button-thin padded float-left padded"> Remove From Template</button>';
                    }
                }
            }
        });
    } else {
        strHtml += '<div style="height: 230px; overflow: auto; width-full;">';
        strHtml += '</div>';
        strHtml += '<div style="height: 300px; overflow: auto; width-full;">';
        strHtml += '<table id="criteriaDetailsTable" Width="98%" class="rtGrid">';
        strHtml += '<tr><td class="center warning-text">Please select a section in order to view its details and criteria or</td></tr>';
        strHtml += '<tr><td class="center warning-text">select the "Add Sections" button to create new, add or remove sections to the template.</td></tr>';
        strHtml += '</table>';
        strHtml += '</div>';
    }

    panel.innerHTML = strHtml;
}

function PopulateCriterionDetails(criteria) {
    var strHtml = '';
    if (criteria && criteria.length > 0) {
        strHtml += '<div style="height: 86px; overflow: auto; width-full;">';
        strHtml += '<table id="criteriaDetailsTable" Width="98%" class="rtGrid"><col width="48%"></col><col width="25%"></col><col width="26%"></col>';
        strHtml += '<tr><td colspan="3" class="rowData warning-text">Select the "Add Criteria & Edit Section" button in order to edit the section details and add or remove criteria.</td></tr>';
        strHtml += '<tr><th colspan="3">Section Criteria Details </th></tr>';
        strHtml += '<tr><td class="rowHeader">Name</td><td class="rowHeader">Chapter Reference</td><td class="rowHeader">Assessment Phase</td></tr>';
        strHtml += '</table>';
        strHtml += '</div>';

        strHtml += '<div style="height: 244px; overflow: auto; width-full;">';
        strHtml += '<table id="criteriaDetailsTable" Width="98%" class="rtGrid"><col width="48%"></col><col width="25%"></col><col width="26%"></col>';
        criteria.forEach(function(c) {
            if (!c.IsDeleted) {
                strHtml += '<tr><td class="rowData">' + c.Criterion + '</td>';
                strHtml += '<td class="rowData">' + c.ChapterRef + '</td>';
                strHtml += '<td class="rowData">' + c.AssessmentPhase + '</td></tr>';
            }
        });
        strHtml += '</table>';
        strHtml += '</div>';

    } else {
        strHtml += '<div style="height: 330px; overflow: auto; width-full;">';
        strHtml += '<table id="criteriaDetailsTable" Width="98%" class="rtGrid"><col width="48%"></col><col width="25%"></col><col width="26%"></col>';
        strHtml += '<tr><td colspan="3" class="rowHeader"></td></tr>';
        strHtml += '<tr><th colspan="3">There are no criteria for this section. </th></tr>';
        strHtml += '<tr><td colspan="3" class="warning-text">Select the "Add Criteria & Edit Section" button in order to edit the section details and add or remove criteria.</td></tr>';
        strHtml += '</table>';
        strHtml += '</div>';
    }

    return strHtml;
}

//       Function to set the css classes for the work flow tabs to display the selected tab only
//       This function is dependant on the naming convention used in the aspx file
function SwitchRiskStageTab(selectedTab) {

    Risk_curTab = selectedTab + 1;
    var tab;
    var panel;
    var count = 2;  //The total number of stages
    for (var i = 0; i < count; i++) {
        //get the current items being iterated
        tab = document.getElementById('tabRiskStage_' + i.toString());
        panel = document.getElementById('pnlRiskStage_' + i.toString());
        if (i == selectedTab) {
            //The current index is the panel to be displayed
            tab.className = 'jobTab activeTab';
            panel.className = '';
        }
        else {
            //The panel is not selected and should be hidden
            tab.className = 'jobTab';
            panel.className = 'hidden';
        }
    }
}

// function to display the dialog for new/edit section screen
function NewOrEditRiskTemplate(riskId) {
    Dialog_OpenPage(
        riskId === -1 ? 'New Template' : 'Edit Template',
        'Dialogs/TemplateRiskAddEdit.aspx?',
        'pnlTemplateDetailsEdit',
        600,
        400,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    var newRiskId = SaveRiskTemplate();
                    if (newRiskId > 0) {
                        SidePanel_Display();
                        TemplateLoad(newRiskId);
                        SwitchRiskStageTab(1);
                        $(this).dialog('close');
                    } else {
                        alert('Risk Template Save Failed');
                    }
                }
            },
            'Cancel': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function() {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/TemplateRiskAddEdit.js?V=3.23.5',
        function() {
            AddOrEditRiskTemplate(riskId);
        });
}

function AddSection() {
    if (risk) {
        var objRisk = risk;
        Dialog_OpenPage(
            'Edit Template Sections',
            'Dialogs/TemplateRiskEdit.aspx',
            'templateContent',
            600,
            700,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function() {
                        if (UpdateTemplateSections()) {
                            riskList = GetAllRiskTemplates();
                            risk = GetRiskForId(objRisk.Id);
                            PopulateRiskDetails(risk);
                            PopulateSectionList(risk.TemplateSections);
                            $(this).dialog('close');
                        } else {
                            alert('Section Save Failed');
                        }
                    }
                },
                'Cancel': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/TemplateRiskSectionsAdd.js?V=3.23.5',
            function() {
                AddOrEditTemplateSections(objRisk);
            });
    }
}

function EditSection(sectionId) {
    Dialog_OpenPage(
        'Edit Section and Criteria',
        'Dialogs/TemplateRiskSectionAndCriteriaEdit.aspx',
        'templateSectionContent',
        600,
        700,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function() {
                    if (UpdateSectionAndCriteria()) {
                        riskList = GetAllRiskTemplates();
                        risk = GetRiskForId(risk.Id);
                        PopulateRiskDetails(risk);
                        PopulateSectionList(risk.TemplateSections);
                        SectionDetailsLoad(sectionId);
                        $(this).dialog('close');
                    } else {
                        alert('Section Save Failed');
                    }
                }
            },
            'Cancel': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function() {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/TemplateRiskCriteriaAddForSection.js?V=3.23.5',
        function() {
            AddOrEditTemplateCriteria(risk.Id, sectionId);
        });
}

function CancelTemplate(riskId) {
    var retVal = confirm("Are you sure you want to remove this Risk Template?");

    if (retVal) {
        Ajax_CallService(
            'Services/Web/Template.asmx',
            'CancelRiskTemplate',
            {
                riskId: riskId
            },
            false
        );

        SidePanel_Display();
    }
}

function RemoveFromTemplate(sectionId) {
    var confMsg = confirm("Are you sure you want to remove this Risk Template?");
    if (confMsg) {
        var retVal = Ajax_CallService(
            'Services/Web/Template.asmx',
            'RemoveTemplateRiskMapping',
            {
                riskId: risk.Id,
                sectionId: sectionId
            },
            false
        );

        if (retVal) {
            riskList = GetAllRiskTemplates();
            risk = GetRiskForId(risk.Id);
            PopulateRiskDetails(risk);
            PopulateSectionList(risk.TemplateSections);
        } else {
            alert('Section Save Failed');
        }
    }
}

function GetCriteriaForSectionid(sectionId) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'GetCriteriaTemplatesForSectionId',
        {
            sectionTemplateId: sectionId
        },
        false
    );

    return retVal;
}

function GetAllRiskTemplates() {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'GetAllRiskTemplates',
        {
        },
        false
    );

    return retVal;
}

function GetRiskForId(riskId) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'GetRiskTemplatesForId',
        {
            riskTemplateId: riskId
        },
        false
    );

    return retVal;
}