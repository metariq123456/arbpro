﻿
function LoadWorkJobScreenItems() {
    //debugger;
    var QuoteId = -1;

    //Get the id of the work to be booked 
    var ParamW = GetURLParameter('WorkId');
    //This Parameter is required
    if (ParamW != 'null') {
        workId = parseInt(ParamW);
    }



    //Get the id of the work to be booked 
    var ParamQ = GetURLParameter('QuoteId');
    //This Parameter is required
    if (ParamQ != 'null') {
        QuoteId = parseInt(ParamQ);
    }


    //Get the id of the work to be booked 
    var ParamSiteId = GetURLParameter('SiteId');
    //This Parameter is required
    if (ParamSiteId != 'null') {
        JobSiteId = parseInt(ParamSiteId);
    }
    var ParamJobId = GetURLParameter('JobId');
    //This Parameter is required
    if (ParamSiteId != 'null') {
        JobId = parseInt(ParamJobId);
    }
    DisplayAcceptJob();

}

function DisplayAcceptJob() {
    var QuoteRef = GetURLParameter('QuoteRef');
    Dialog_OpenPage(
        'Quotation:' + QuoteRef,
        'Dialogs/AcceptJob.aspx',
        'content',
        500,
        250,
        true,
        {
            'Yes': {
                text: 'Yes',
                "class": 'ui-dialog-button',
              click: function () {
                    $(this).dialog('close');
                    ChangeQuoteStatus(3);

                }
            },
            'No': {
                text: 'No',
                "class": 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                    var target = '/Calendar.aspx';
                    window.location = target;
                }
            }
        },
        'Content/js/AlertBox.js?V=3.23.5',
        function () {

        });
}

function ChangeQuoteStatus(status) {
    try {
        var QuoteId = GetURLParameter('QuoteId');
        Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'SetQuoteStatus',
            {
                QuoteID: QuoteId,
                Status: status
            },
            false);
        var WorkList = Ajax_CallService(
              'Services/Web/WorkProgramming.asmx',
              'GetWorkForQuoteId',
              {
                  quoteId: QuoteId
              },
              false
          );
        if (WorkList.length > 0) {
            var target = '/BookDates.aspx?WorkId=' + WorkList[0].Id + '&QuoteId=' + QuoteId + '&WorkItmId=200&SiteId=' + JobSiteId + '&JobId=' + JobId;
            window.location = target;
        }

    }
    catch (err) {
        DisplayMessageDialog('Error', 'Unable to change status for Quote - ' + err.toString());
    }

}

