﻿/*
        NOTE: This js file may be loaded in as an alternate option for generating an Invoice
        Due to this the file must contain the following functions:
            ToggleDsiplay(elementIndex)         - function to collapse or expand the relevent section
            LoadAttachments()                   - function to load user attachments
            AddAttachment()                     - function to enable the user to add an attachment to an email
            ShowAttachments()                   - function to display the attachment names in the grid
            SetSelected(attachment, selected)   - function to set which attachments are selected
*/

//REQUIRED SCOPE VARIABLES - These are used for the attachment functionality
var lm_FileNames;
var lm_Attachments;
var lm_SelectedAttachments;

var IsPreview = false;
var ObjInvoice = null;              //  The invoice to be loaded
var ListInvoiceableItems = null;    //  The invoiceable work items    
var ListContacts = null;            //  The contact list for the client
var ListPDFTemplates = null;        //  The text templates that can be used for PDF generation
var ListEmailTemplates = null;      //  The text templates that can be used for Email generation
var ObjNewInvItem = null;           //  Local memory store for a new Invoice item
var ListNewItems = null;            //  The list of all additional invoice items that have been added to the invoice
var ListSmsTemplates = null;        // DGSAP-852 - The text templates that can be used for SMS generation
var isSmsLoaded = false;            // DGSAP-852
var isEmailLoaded = false;          // DGSAP-852
var itemCount = 0;
//state flags
var ArrIsPanelShown = [true, true, true];
var ArrCreatePnlShown = [true, true];
var EmailSmtpObj = [];
var isResend = false;
var isDownloaded = false;

/*

        Initialisation and Service Call Functions

*/

//      Initialisation function
function InvoiceGeneration_Load(invId, resend) {
    isResend = resend;
    //  General setup
    ClearAttachments();
    //  Load the invoice
    Invoice_Load(invId, Invoice_LoadCallBack);
    //Display the selected Quote Reference
    var element = document.getElementById('lblPdfQuoteNo');
    element.textContent = ObjInvoiceSummary.QuoteNo;
}

//      Async service call to load an invoice: This will load a new invoice if the id is -1
function Invoice_Load(invoiceId, callBack) {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetInvoiceForId',
           {
               invoiceId: invoiceId
           },
           true,
           callBack
       );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
        return false;
    }
}

//      Async service call load all the client contact for the Invoice
function ClientContacts_Load(clientId) {
    //  NOTE:   This method must only be called once the main invoice object has been 
    //          loaded and the base html of the form created.
    try {
        Ajax_CallService(
           'Services/Web/ClientData.asmx',
           'GetClientContacts',
           {
               clientID: clientId
           },
           true,
           ClientContacts_LoadCallback
       );
    }
    catch (err) {
        DisplayMessage('invPnlRecipientError', 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//      Async function to load all completed work items that have not been invoiced
function InvoiceableItems_Load(quoteId) {
    try {
        Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetInvoicableWorkItems',
           {
               quoteId: quoteId
           },
           true,
           InvoiceableItems_LoadCallback
       );
    }
    catch (err) {
        DisplayMessage('invPnlRecipientError', 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//      Async service call to clear email attachments
function ClearAttachments() {
    //  Setup new arrays for the attachments list
    lm_FileNames = new Array();
    lm_SelectedAttachments = new Array();
    //  Clear any templates attached to previous emails
    Ajax_CallService(
        'Services/Web/Report.asmx',
        'ClearTempEmailAttachments',
        {},
        true,
        function (result) {
            //Do nothing
        }
    );
}

//      Async service call to load valid templates
function GetPDFTemplates() {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
           'Services/Web/Settings.asmx',
           'GetTemplateListForTypeAndStage',
           {
               Format: 0,
               tmplType: 1
           },
           true,
           PopulatePDFPanel
       );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

//      Async service call to load valid templates
function GetEmailTemplates() {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
           'Services/Web/Settings.asmx',
           'GetTemplateListForTypeAndStage',
           {
               Format: 1,
               tmplType: 1
           },
           true,
           PopulateEmailPanel
       );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

// DGSAP-852 Async service call to load valid templates
function GetSmsTemplates(quoteId) {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
           'Services/Web/Settings.asmx',
           'GetSmsTemplateListForTypeAndStage',
           {
               quoteId: quoteId,
               tmplType: 1
           },
           true,
           PopulateSmsPanel
       );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

//      Async service call to generate the required PDF document
//      isPreview denotes if the generated PDF is to be inserted into the database and have a history created
function LoadPDF(isPreview) {
    isDownloaded = false;
    $("#pnlPDFError").html("");
    //Set the preview flag for the callback to access
    IsPreview = isPreview;
    //Display the wait timer
    ShowGenerationTimer(true);
    try {
        var pdfSettings = GetPdfSettings(isPreview, false);
        if (ValidateDueDate()) {
            //Check if the date is to be set on the invoice 
            if (!isPreview) {
                ObjInvoice.DateSent = new Date();
            } else {
                ObjInvoice.DateSent = toDateFromJson(ObjInvoice.DateSent);
            }

            ObjInvoice.DueDate = toDateFromJson(ObjInvoice.DueDate);
            ObjInvoice.PaidDate = toDateFromJson(ObjInvoice.PaidDate);

            ObjInvoice.DateCreated = toDateFromJson(ObjInvoice.DateCreated);
            var b = Date.parse(ObjInvoice.DateCreated);
            if (b === 0) {
                DisplayMessage('pnlPDFError', 'Warning: Could not find Invoice Creation Date. You may want to contact the customer to provide it.', "red");
            }

            //  Update the invoice and generate the PDF
            try {
                var tempArray = [];
                tempArray = ObjInvoice.InvoicedItems;
                ObjInvoice.InvoicedItems = [];
                Ajax_CallService(
                    'Services/Web/Report.asmx',
                    'UpdateInvoiceAndOpenReport',
                    {
                        obj: ObjInvoice,
                        settings: pdfSettings
                    },
                    true,
                    DisplayPDF
                );
                ObjInvoice.InvoicedItems = tempArray;
            } catch (err) {
                DisplayMessage('pnlPDFError', 'Error: Unable to communicate with Arb Pro service - ', er.toString());
            }
        } else {
            DisplayMessage('pnlPDFError', 'Please select a valid date for the invoice to be paid. This date must be in the future.');
        }
    } catch (err) {
        DisplayMessage('pnlPDFError', 'Error: Unable to generate PDF - ' + err.toString());
    } finally {
        //  Ensure that no matter the code path the wait timer display is removed
        ShowGenerationTimer(false);
    }
}

//      Async service call to save an invoice
function Invoice_Save(selectedItems,deletedItems) {
    try {
        //  Clear the invoiced items array as this may have been changed
        ObjInvoice.InvoicedItems = [];

        ObjInvoice.HouseNameNo = document.getElementById('txtContactStreetNumber').value;
        ObjInvoice.Address1 = document.getElementById('txtContactAddress1').value;
        ObjInvoice.Address2 = document.getElementById('txtContactAddress2').value;
        ObjInvoice.Town = document.getElementById('txtContactTown').value;
        ObjInvoice.County = document.getElementById('txtContactCounty').value;
        ObjInvoice.PostCode = document.getElementById('txtContactPostCode').value;
        //  Set the dates so the upload can be processed corectly
        ObjInvoice.DateSent = toDateFromJson(ObjInvoice.DateSent);
        ObjInvoice.DueDate = toDateFromJson(ObjInvoice.DueDate);
        ObjInvoice.PaidDate = toDateFromJson(ObjInvoice.PaidDate);
        ObjInvoice.DateCreated = toDateFromJson(ObjInvoice.DateCreated);

        //  Ensure the work id is set correctly
        if (ObjInvoice.WorkId <= 0) {
            //  Access the Invoice Summary object that was loaded in the Invoice.js file
            ObjInvoice.WorkId = ObjInvoiceSummary.WorkId;
        }

        //  Call the service
        Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'SaveInvoice',
            {
                obj: ObjInvoice,
                selectedItems: selectedItems,
                deletedItems: deletedItems
            },
            true,
            Invoice_SaveCallback
        );
    }
    catch (err) {
        DisplayMessage('pnlPDFError', 'Error: failed to update invoice - ' + err.toString());
    }
}

//      Async service call to set the status of an invoice
function UpdateInvoiceStatus() {
    /*
        strHtml += '<option value="1">Letter</option>';
        strHtml += '<option value="0">Schedule</option>';
        strHtml += '<option value="7">Letter & Schedule</option>';
        strHtml += '<option value="2">Reminder</option>';
        strHtml += '<option value="3">Second Reminder</option>';
        strHtml += '<option value="4">Final Reminer</option>';
        strHtml += '<option value="5">Legal Action</option>';
        strHtml += '<option value="6">Final Legal Action</option>';
        */
    //Set the status of the invoice dependant on the type of document created
    //Note once an invoice is set to a status it cannot be set to a lower status unless that status is paid. 
    var ddl = document.getElementById('ddlLetterType');
    var itemType = ddl.options[ddl.selectedIndex].value;
    var newStatus = 1;
    switch (itemType) {
        case 0:
        case 1:
        case 7:
            //Initial items have been sent so the status is sent
            newStatus = 1;
            break;
        case 2:
        case 3:
        case 4:
            //Reminder items have been sent so the status is overdue
            newStatus = 3;
            break;
        case 5:
        case 6:
            //Legal items have been sent so the status is LegalAction
            newStatus = 4;
            break;

    }
    //Update the status of the invoice and set the PDF generation buttons to be disabled
    if (SetInvoiceStatus(ObjInvoice.Id, newStatus)) {
        document.getElementById('btnPreviewPdf').disabled = true;
        document.getElementById('btnPrintLetter').disabled = true;
    }
}

function GetSmtpSettings() {
    EmailSmtpObj = [];

    EmailSmtpObj = Ajax_CallService(
        '../Services/Web/Company.asmx',
        'GetEmailSettings',
        {},
        false
    );
}

//      Async service call to send an invoice item by email. This function creates history items for the generated documents
function SendEmail() {
    try {
        ColapsePanels(1);
        ColapsePanels(2);
        //Check if the due date has been set
        if (ValidateDueDate()) {
            //Display the wait timer
            ShowGenerationTimer(true);

            //Set the due date to the selected date and save the record
            ObjInvoice.DueDate = toDateFromJson(ObjInvoice.DueDate);

            // DGSAP-852
            ObjInvoice.PaidDate = toDateFromJson(ObjInvoice.PaidDate);
            ObjInvoice.DateCreated = toDateFromJson(ObjInvoice.DateCreated);

            //Get the settings to be applied to the email
            var emailSettings = GetEmailSettings();
            var pdfSettings = GetPdfSettings();
            //UpdateInvoiceAndEmailReport(Types.Invoice obj, Types.InvoicePdfSettings pdfSettings, Types.EmailSettings emailSettings)

            ColapsePanels(0);

            if (!isResend) {
                ObjInvoice.DateSent = new Date();
            } else {
                ObjInvoice.PointOfContact.Id = ObjInvoice.ContactId;
            }

            try {
                ObjInvoice.InvoicedItems = [];
                Ajax_CallService(
                    'Services/Web/Report.asmx',
                    'UpdateInvoiceAndEmailReport',
                    {
                        obj: ObjInvoice,
                        pdfSettings: pdfSettings,
                        emailSettings: emailSettings
                    },
                    true,
                    DisplayEmailMsg
                );

            } catch (err) {
                DisplayMessage('pnlPDFError', 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
                //Ensure that the processing wait timer is not being displayed
                ShowGenerationTimer(false);
            }
        } else {
            //document.getElementById('pnlPDFDisplay').hidden = true;
            DisplayMessage('pnlPDFError', 'Please select a valid date for the invoice to be paid. This date must be in the future.');
        }
    } catch (err) {
        DisplayMessage('pnlPDFError', 'Error: Unable to send email - ' + err.toString());
        //Ensure that the processing wait timer is not being displayed
        ShowGenerationTimer(false);
    }
}

// Function to colapse all of the active panels
function ColapsePanels(elementIndex) {
    var bodyElement = document.getElementById('pnlPDFBody_' + elementIndex.toString());
    var toggleElement = document.getElementById('lblHeaderToggle_' + elementIndex.toString());
    if (ArrIsPanelShown[elementIndex]) {
        toggleElement.textContent = '+';
        $(bodyElement).slideUp();
    }

    ArrIsPanelShown[elementIndex] = !ArrIsPanelShown[elementIndex];
}

//      Helper function to get the settings for Pdf generation
function GetPdfSettings(isPreview, isCopy) {
    var pdfSettings = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetInvoicePdfSetting',
        {},
        false);
    pdfSettings.Id = ObjInvoice.Id;
    pdfSettings.IsCopy = isCopy;
    pdfSettings.IsPreview = isPreview;
    var ddl = document.getElementById('ddlLetterType');
    pdfSettings.GenerationEnum = parseInt(ddl.options[ddl.selectedIndex].value);
    ddl = document.getElementById('ddlLetterTemplate');
    pdfSettings.TemplateId = parseInt(ddl.options[ddl.selectedIndex].value);
    pdfSettings.TextBody = document.getElementById('txtLetterText').value;
    pdfSettings.PaymentDetails = document.getElementById('txtPaymentDetails').value;

    return pdfSettings;
}

//      Helper function to get the settings for sending an email
function GetEmailSettings() {
    /*
    List<int> _Recipients = null;
        string _Subject = "";
        string _BodyText = "";
        string _FromAddress = "";
    */
    var CCEmails = document.getElementById('txtEmailCC').value
    var emails = CCEmails.split(';');

    var invalidEmails = [];
    for (i = 0; i < emails.length; i++) {
        if ((!validateEmail(emails[i].trim())) && emails[i] != "") {
            invalidEmails.push(emails[i].trim())

        }
    }
    if (invalidEmails.length > 0) {
        alert("The following emails are invalid:" + invalidEmails);
    }

    var CCEmailList = $.unique(emails);
    var emailSettings = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetEmailSetting',
        {},
        false);
    emailSettings.CCRecipients = CCEmailList;
    emailSettings.Id = ObjInvoice.Id;
    emailSettings.Recipients = new Array();
    emailSettings.Recipients.push(ObjInvoice.PointOfContact.Id);
    emailSettings.Subject = document.getElementById('txtEmailSubject').value;
    emailSettings.BodyText = document.getElementById('txtEmailText').value;
    emailSettings.FromAdress = ''; //For invoice generation this is set later

    // DGSAP-852
    if (isSmsLoaded) {
        emailSettings.SmsText = document.getElementById('txtSmsText').value; 
    } else {
        emailSettings.SmsText = "";
    }

    return emailSettings;
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}


//      Function to load a blank invoice item
function NewInvoiceItem_Load(jobSiteId) {
    try {
        Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetNewInvoiceItem',
            {},
            true,
            function (result) {
                NewInvoiceItem_LoadCallback(result, jobSiteId);
            });
    }
    catch (err) {
        //Do nothing, however this must be here to prevent the js from breaking if the ajax calls fails
    }
}

/*

        Service Callback Functions

*/

//      Async service callback for loading and display an invoice
function Invoice_LoadCallBack(result) {
    var panel = document.getElementById('pnlInvPdf_0');
    try {
        if (!result) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid invoice');
        } else {
            //  The return object is valid set the scope variable
            ObjInvoice = result;
            //Check if this is a new invoice
            if (ObjInvoice.Id < 1 || ObjInvoice.ClientId < 1) {
                //  Access the main Job object for the Work Flow - This is populated and loacted in the WorkProgramming.js file
                ObjInvoice.ClientId = ObjJob.ClientId;
                //  Access the invoice summary - This is populated and located in the Invoice.js file
                ObjInvoice.QuoteId = ObjInvoiceSummary.Id;
            }
            //  Check if the work record id has been set - NOTE: This is legacy support
            if (ObjInvoice.WorkId < 1) {
                ObjInvoice.WorkId = ObjInvoiceSummary.WorkId;
            }
            //  Initialise the new items array
            ListNewItems = [];
            //  Load the panel
            Invoice_Display(panel);

            //  Now that the main html has been generated the contact details, invoicable item details, and templates can be loaded
            ClientContacts_Load(result.ClientId);

            //  Load template items for document
            GetPDFTemplates();
            GetEmailTemplates();
            // DGSAP-852
            GetSmsTemplates(result.QuoteId);
            HookPdfToDownload();

        }
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to load Invoice details - ' + err.toString());
    }
}

function Invoice_RefreshCallback(result) {
    var panel = document.getElementById('pnlInvPdf_0');
    try {
        if (!result || result.Id < 1) {
            DisplayMessage(panel.id, 'Refresh Error: Arb Pro service did not return a valid invoice');
        } else {
            //  Set the scope variable
            ObjInvoice = result;
            //  Setup the display
            Invoice_Display(panel);
            //  Reload the contact details
            ClientContacts_LoadCallback(ListContacts);            
        }
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to refresh Invoice details - ' + err.toString());
    }
}

//      Async service callback to handle an update operation for an invoice
function Invoice_SaveCallback(result) {
    if (result && result > 0) {
        // The save opertaion was successful
        var panel = document.getElementById('pnlInvPdf_0');
        panel.innerHTML = GetWaitImageForPanel(panel.id, 'Refreshing Invoice, Please Wait');
        //  Reset scope variables
        ListNewItems = [];
        ObjNewInvItem = null;
        //  Reolad the invoice
        Invoice_Load(result, Invoice_RefreshCallback);
        //  Refresh the parent page - This is an Invoice.js function
        GetInvoiceSummaryForQuote(ObjInvoiceSummary.QuoteId);
    } else {
        // Display an error to the user

        //  TODO:
    }
}

//      Async serrvice callback to setup a new invoice item
function NewInvoiceItem_LoadCallback(result, jobSiteId) {
    var panel = document.getElementById('pnlInvItemEdit');
    try {
        if (!result) {
            DisplayMessage(panel.id, 'Error: The Arb Pro service did not return a valid invoice item.');
        }
        ObjNewInvItem = result;
        ObjNewInvItem.JobSiteId = jobSiteId;
        //  To enable multiple new invoice items set the work item id to a discreat identifier
        //  This id is to be based on the number of new items already added
        ObjNewInvItem.Id -= (ListNewItems.length + 1);

        //  Generate the html content for the panel
        panel.innerHTML = GenerateEditItemPanel(ObjNewInvItem, true);
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display add invoice item view - ' + err.toString());
    }
}

//      Async call back to populate the invoice contact details
function ClientContacts_LoadCallback(results) {
    var panel = document.getElementById('pnlInvContactList');
    try {
        if (!results || results.length == 0) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid list of contacts');
        } else {
            //  The return object is valid set the scope variable
            ListContacts = results;
            //  Generate the html for the contacts section
            var strHtml = GenerateContactLeftPanel();
            strHtml += GenerateContactRightPanel();
            //   Add the html to the panel
            panel.innerHTML = strHtml;
            //  Now the html has been added to the DOM we can set selected items as required
            if (ObjInvoice.ContactId < 1) {
                //  The contact id is not valid so set it to the default contact
                ObjInvoice.ContactId = GetDefaultContactId();
            }
            //Set the selected details for the Invoice contact
            ContactSelected(ObjInvoice.ContactId);
            //Now that the default values have been selected they must be overloaded with any values in the invoice
            //but only if the street number or address line one are populated
            if (ObjInvoice.HouseNameNo.length > 0 || ObjInvoice.Address1.length > 0) {
                document.getElementById('txtContactStreetNumber').value = ObjInvoice.HouseNameNo;
                document.getElementById('txtContactAddress1').value = ObjInvoice.Address1;
                document.getElementById('txtContactAddress2').value = ObjInvoice.Address2;
                document.getElementById('txtContactTown').value = ObjInvoice.Town;
                document.getElementById('txtContactCounty').value = ObjInvoice.County;
                document.getElementById('txtContactPostCode').value = ObjInvoice.PostCode;
            }

            //  A contact should now be selected so colapse the panel
            ToggleCreationDisplay(0);

            //  Select the send tab if the invoice is in a state it can be sent
            if (ObjInvoice.ContactId > 0 && ObjInvoice.InvoicedItems.length > 0) {
                document.getElementById('tabInvPdf_1').className = "pdfTab";
                SwitchInvPdfTab(1);
            }
        }
        SmsNotSent();
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to load contact details - ' + err.toString());
    }
}

//      Async service callback to populate the invoice items sections
function InvoiceableItems_LoadCallback(results) {
    //debugger;
    var panel = document.getElementById('pnlInvItemList');
    try {
        if (!results) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid list of items');
        } else {
            //  The return object is valid set the scope variable
            ListInvoiceableItems = results;
            //  Generate the html for the items section
            var strHtml = GenerateInvItemPanel();

            //   Add the html to the panel
            panel.innerHTML = strHtml;
        }
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display invoice items - ' + err.toString());
    }
}

//      Async callback to generate the html for the PDF settings panel
function PopulatePDFPanel(results) {
    var panel = document.getElementById('pnlPDFBody_0');
    if (!results || results.length == 0) {
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
    } else {
        try {
            /*
                Invoice doc type enum values
                Schedule,
                Letter,
                FirstReminder,
                SecondReminder,
                FinalReminer,
                LegalAction,
                LegalAction2,
                Letter_And_Schedule
            */
            ListPdfTemplates = results;
            //Generate the html for the div
            var strHtml = '';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlLetterType"> Invoice Due Date </label>';
            strHtml += '<input id="txtDueDate" class="lmFieldControl" type="text" style="font-family:Helvetica;font-size:Small;" name="txtDueDate" ';
            if (ObjInvoice && ObjInvoice.Status > 0) {
                strHtml += ' disabled="true" value="' + toDateStringFromJson(ObjInvoice.DueDate, '', 'en_GB') + '">';
            } else {
                strHtml += ' value="' + toDateStringFromJson(ObjInvoice.DueDate, '', 'en_GB') + '">';
            }
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlLetterType"> Letter Type </label>';
            strHtml += '<select id="ddlLetterType" class="lmFieldControl" onchange="ShowLetterTemplate();" name="ddlLetterType">';
            strHtml += '<option value="1">Letter</option>';
            strHtml += '<option value="0">Schedule</option>';
            strHtml += '<option value="7">Letter & Schedule</option>';
            strHtml += '<option value="2">Reminder</option>';
            strHtml += '<option value="3">Second Reminder</option>';
            strHtml += '<option value="4">Final Reminer</option>';
            strHtml += '<option value="5">Legal Action</option>';
            strHtml += '<option value="6">Final Legal Action</option>';
            strHtml += '</select>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="width: 100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlLetterTemplate"> Template Text</label>';
            strHtml += '<select id="ddlLetterTemplate" class="lmFieldControl" onchange="LetterTemplateSelected();" name="ddlLetterTemplate">';
            ListPdfTemplates.forEach(function (tmp) {
                strHtml += '<option value="' + tmp.Id + '">' + tmp.TemplateName + '</option>';
            });
            strHtml += '</select>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="width: 100%;">';
            //strHtml += '<input id="txtLetterSalutation" class="lmFieldControl" type="text" style="font-family:Helvetica;font-size:Small;" name="txtLetterSalutation">';
            strHtml += '<textarea id="txtLetterText" class="lmFieldText" style="font-family:Helvetica;font-size:Small;" cols="20" rows="2" name="txtLetterText"></textarea>';
            strHtml += '<textarea id="txtPaymentDetails" class="lmFieldText" style="font-family: Helvetica; font-size: small;" cols="20" rows="2" name="txtPaymentDetails"></textarea>';
            strHtml += '</div>';
            strHtml += '<div class="lmField center" style="width:100%;">';
            strHtml += '<label id="lblWarnig" class="lmFieldText"> Please use the Preview button before Generating a PDF. Generating a PDF will lock the invoice into Sent status. </label>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="height:30px;width:100%;">';
            strHtml += '<input id="btnPreviewPdf" class="button-thin padded float-left" type="submit" style="margin-right: 10px;" onclick="LoadPDF(true); return false;" value="Preview PDF" name="btnPreviewPdf">';
            strHtml += '<input id="btnPrintLetter" class="button-thin padded float-right" type="submit" style="margin-right: 10px;" onclick="LoadPDF(false); return false;" value="Generate PDF" name="btnPrintLetter">';
            strHtml += '</div>';
            //Update the panel
            panel.innerHTML = strHtml;
            //  Set the selected type to Letter and schedule
            document.getElementById('ddlLetterType').selectedIndex = 2;
            //Set the text to the first selected item 
            LetterTemplateSelected();
            //attach the date picker to the duedate control
            $('#txtDueDate').datetimepicker({ format: 'd/M/Y', timepicker: false, closeOnDateSelect: true });
            if (isResend) {
                document.getElementById('pnlPDFDropdown').className = "hidden";
                LoadPDF(true);
            }
        }
        catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display PDF settings - ' + err.toString());
        }
    }
}

//      Async callback to generate the html fot the email settings panel
function PopulateEmailPanel(results) {
    var panel = document.getElementById('pnlPDFBody_1');
    if (!results || results.length == 0) {
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
    } else {
        try {
            ListEmailTemplates = results;
            //Generate the html for the div
            var strHtml = '';

            if (isResend) {
                // if invoice is being re-sent display warning message
                strHtml += '<div class="lmField center" style="width:100%;">';
                strHtml += '<label class="lmField center"> Selecting a contact will not change the details in the PDF. However it will change the email address to which the PDF is sent to. </label>';
                strHtml += '</div>';
                strHtml += '<br/>';
            }

            strHtml += '<div class="lmEdit" style="width:100%;">';
            strHtml += '<div id="divEmailOptions" style="width: 100%; display: block;">';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlEmailTemplate"> Email Text Template </label>';
            strHtml += '<select id="ddlEmailTemplate" class="lmFieldControl" onchange="ShowEmailTemplate();" name="ddlEmailTemplate">';
            strHtml += '<option value="-1" selected="selected">Select Template</option>';
            ListEmailTemplates.forEach(function (tmp) {
                strHtml += '<option value="' + tmp.Id + '">' + tmp.TemplateName + '</option>';
            });
            strHtml += '</select>';
            strHtml += '<label class="lmFieldLabel" for="txtEmailSubject"> Subject </label>';
            strHtml += '<input id="txtEmailSubject" class="lmFieldControl" type="text" style="width:54%;" name="txtEmailSubject">';
            strHtml += '<label class="lmFieldLabel" for="txtEmailCC"> CC </label>';
            strHtml += '<input id="txtEmailCC" class="lmFieldControl" type="text" style="width:54%;" name="txtEmailCC">';
            strHtml += '<textarea id="txtEmailText" class="lmFieldText" style="font-family:Helvetica;font-size:Small;" cols="20" rows="2" name="txtEmailText"></textarea>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<div id="pnlAttachments" style="width:100%;">';
            strHtml += '<table id="grdAttachments" class="lmGrid" style="width:94%;">';
            strHtml += '<tbody>';
            strHtml += '</table>';
            strHtml += '</div>';
            strHtml += '<div style="width:100%;">';
            strHtml += '<input id="fuAttachment" type="file" name="fuAttachment" style="width:60%">';
            strHtml += '<input id="btnUpload" class="button-thin float-right" type="submit" style="margin-right: 10px;width:128px;" onclick="AddAttachment(); return false;" value="Attach" name="btnUpload">';
            strHtml += '<div class="warning-text" width:100%> Maximum file size is 8 MB. </div>';
            strHtml += '<div id="alertAttach" class="hidden" width:100%>Uploading large files may result in a slow upload time depending on your connection.</div>';
            strHtml += '</div>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="text-align:center; height:4px;width:100%;">';
            strHtml += '<input id="btnAttachDoc" class="button-thin float-right" type="submit" style="margin-right: 10px;margin-top:15px;" return false;" value="Attach Docs" name="btnAttachDoc" onclick="AttachDocDialogForInvoice();">';
            strHtml += '</div>';
            strHtml += '<br>';
            strHtml += '<div class="lmField" style="text-align:center;width:100%;">';
            strHtml += '<label id="alertSms"> SMS will not be sent </label> </div>';
            strHtml += '<div class="lmField" style="height:5px;width:100%;">';
            strHtml += '<input id="btnSendEmail" class="button-thin float-right" type="submit" style="margin-right: 10px;width:129.833px;" onclick="SendEmail(); return false;" value="Send Email" name="btnSendEmail">';
            strHtml += '</div>';
            strHtml += '</div>';
            strHtml += '</div>';
            strHtml += '</div>';

            isEmailLoaded = true;
            //Update the panel
            panel.innerHTML = strHtml;
            if (!isResend) {
                //Colapse the panel
                ToggleDsiplay(1);
            }
            SmsNotSent();

        }
        catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display Email settings - ' + err.toString());
        }
    }
}

// DGSAP-852 - Async callback to generate the html fot the sms settings panel
function PopulateSmsPanel(results) {
    var panel = document.getElementById('pnlPDFBody_2');
    if (!results || results.length == 0) {
        DisplayMessage(panel.id, 'Error: Could not find any SMS templates');
    } else {
        try {
            ListSmsTemplates = results;
            //Generate the html for the div
            var strHtml = '';
            strHtml += '<div class="lmEdit" style="width:100%;">';
            strHtml += '<label class="warning-text" for="ddlSmsTemplate" style="width:100%;"> Maximum length of SMS messages is 160 characters. </label>';
            strHtml += '<div class="lmEdit" style="width:100%;">';
            strHtml += '<div id="divSmsOptions" style="width: 100%; display: block;">';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlSmsTemplate"> Sms Text Template </label>';
            strHtml += '<select id="ddlSmsTemplate" class="lmFieldControl" onchange="ShowSmsTemplate();" name="ddlSmsTemplate">';
            ListSmsTemplates.forEach(function (tmp) {
                strHtml += '<option value="' + tmp.Id + '">' + tmp.TemplateName + '</option>';
            });
            strHtml += '</select>';
            strHtml += '<textarea id="txtSmsText" class="lmFieldText" style="font-family:Helvetica;font-size:Small;" cols="20" rows="2" name="txtSmsText" oninput="contentTooLong()"></textarea>';
            strHtml += '<div class="lmField" style="text-align:center; height:30px;width:100%;">';
            strHtml += '<label id="alertSmsLength" class="hidden"> SMS is too long </label> </div>';
            strHtml += '</div>';
            strHtml += '</div>';
            strHtml += '</div>';

            isSmsLoaded = true;
            //Update the panel
            panel.innerHTML = strHtml;
            //Colapse the panel
            ToggleDsiplay(2);
            ShowSmsTemplate();
            SmsNotSent();
        }
        catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display SMS settings - ' + err.toString());
        }
    }
}

function contentTooLong() {
    var s = document.getElementById('txtSmsText');
    if (!ValidateConvertedSmsLength(s.value)) {
        document.getElementById('alertSmsLength').className = "center";
    } else {
        document.getElementById('alertSmsLength').className = "hidden";
    }
    SmsNotSent();
}

//      Async callback to display a PDF
function DisplayPDF(result) {
    ShowGenerationTimer(false);
    if (!result) {
        DisplayMessage('pnlPDFError', 'Error: Unable to display PDF, generation failed.');
    } else {
        RenderPDF();
    }
}

/*

        Local Functions

*/
//      Function to display the invoice details
function Invoice_Display(panel) {
    var tab = document.getElementById('tabInvPdf_1');
    //  Check if the send tab should be displayed
    if (ObjInvoice.Id < 1 || ObjInvoice.InvoicedItems.length == 0 || ObjInvoice.ContactId < 1) {
        //The invoice has not finshed being created and so is not in a state it can be sent
        tab.className = 'pdfTab hidden';
    } else {
        tab.className = 'pdfTab';
    }

    //  Generate the main html elements for the creation dialog
    //  Add the contact section
    var strHtml = '<div id="pnlCreateODFInvoice" class="lmFullScrollPanel">';
    //  Add the contact section
    strHtml += '<div id="pnlInvContactContainer">';
    strHtml += GenerationContactSectionHtml();
    strHtml += '</div>';
    strHtml += '</br>';
    //  Add the item section
    strHtml += '<div id="pnlInvitemsContainer">';
    strHtml += GenerationInvItemsSectionHtml();
    strHtml += '</div>';
    strHtml += '</div>';
    //  Add the generated html to the panel
    panel.innerHTML = strHtml;

    //  Load any invoiceable items for the quote
    InvoiceableItems_Load(ObjInvoice.QuoteId);

}

//      Function to display the selected invoice PDF panel
function SwitchInvPdfTab(pnlIndex) {
    for (var i = 0; i < 2; i++) {
        var tab = document.getElementById('tabInvPdf_' + i.toString());
        var panel = document.getElementById('pnlInvPdf_' + i.toString());
        if (i == pnlIndex) {
            //  The current index is the selected item
            tab.className = 'pdfTab activeTab';
            panel.className = 'width-full float-left height-full';
        } else {
            //  The current index needs to be hidden
            tab.className = 'pdfTab';
            panel.className = 'hidden';
        }
        // debugger
        if (i == 1) {
            SmsNotSent();
            ShowSmsTemplate();
        }
    }
}

//      Function to display a wait timer while a PDF is being generated
function ShowGenerationTimer(show) {
    if (show) {
        document.getElementById('pnlLmPdfView').className = 'hidden';
        document.getElementById('pnlLmProcessing').className = 'waitTimer';
    } else {
        document.getElementById('pnlLmProcessing').className = 'hidden';
        document.getElementById('pnlLmPdfView').className = 'lmPnlLrg';
    }
}

//      Function to display the PDF in the viewer
function RenderPDF() {
    $('#pdfPreview').attr('src', 'Controls/PDFViewer.aspx?v=' + Math.floor((Math.random() * 1000000) + 1));
    $("#previewToolbar").css('visibility', 'visible');
    //Update the status of the Invoice to be sent if required
    if (!IsPreview) {
        UpdateInvoiceStatus();
    }
}

//      Function to show the letter templates for the emails
function ShowLetterTemplate() {
    var list = document.getElementById('ddlLetterType');

    lm_FileNames = new Array();

    var reg = document.getElementById('pnlLetterTemplate');
    if (list.options[list.selectedIndex].value == 0) {
        if ($(reg) != null) {
            $(reg).slideDown();
        }
        lm_FileNames[0] = 'Invoice Schedule.pdf';
        lm_FileNames.splice(1, 1);
    } else if (list.options[list.selectedIndex].value == 1) {
        if ($(reg) != null) {
            $(reg).slideUp();
        }
        lm_FileNames[0] = 'Invoice Letter.pdf';
        lm_FileNames.splice(1, 1);
    } else {
        if ($(reg) != null) {
            $(reg).slideDown();
        }
        lm_FileNames[0] = 'Invoice Letter.pdf';
        lm_FileNames[1] = 'Invoice Schedule.pdf';
    }
    LoadAttachments();
}

//      Function to populate the UI from the seklected template
function ShowEmailTemplate() {
    var ddl = document.getElementById('ddlEmailTemplate');
    var tempId = ddl.options[ddl.selectedIndex].value;
    var count = ListEmailTemplates.length;
    var template = null;
    //A for loop is being used to enable the break statement
    for (var index = 0; index < count; index++) {
        template = ListEmailTemplates[index];
        if (template.Id == tempId) {
            document.getElementById('txtEmailSubject').value = template.Salutation;
            document.getElementById('txtEmailText').value = template.BodyText;
            break;
        }
    }
}

// DGSAP-852 - Function to populate the UI from the seklected template
function ShowSmsTemplate() {
    if (isSmsLoaded) {
        var ddl = document.getElementById('ddlSmsTemplate');
        var tempId = ddl.options[ddl.selectedIndex].value;
        var count = ListSmsTemplates.length;
        var template = null;
        //A for loop is being used to enable the break statement
        for (var index = 0; index < count; index++) {
            template = ListSmsTemplates[index];
            if (template.Id == tempId) {
                document.getElementById('txtSmsText').value = template.BodyText;
                SmsTextConversion();
                break;
            }
        }
    }
}

//      Function to display the text of the selected template
function LetterTemplateSelected() {
    var ddl = document.getElementById('ddlLetterTemplate');
    var tempId = ddl.options[ddl.selectedIndex].value;
    var count = ListPdfTemplates.length;
    var template = null;
    //A for loop is being used to enable the break statement
    for (var index = 0; index < count; index++) {
        template = ListPdfTemplates[index];
        if (template.Id == tempId) {
            document.getElementById('txtLetterText').value = template.BodyText;
            document.getElementById('txtPaymentDetails').value = template.AdditionalText;
            break;
        }
    }
}

//      Function to validate and set the due date of the invoice before updateing the record and generating the documents
function ValidateDueDate() {
    //TODO VALIDATE
    var isValid = false;
    try {
        var strDate = document.getElementById('txtDueDate').value;
        var dueDate = parseNormalDate(strDate);
        var today = new Date();
        
        isValid = (+dueDate > +today) || (+dueDate <= +today && ObjInvoice.Status > 0);
        //Set the invoice object due date
        if (isValid) {
            ObjInvoice.DueDate = dueDate;
        }
        else {
            ObjInvoice.DueDate = new Date(0);
        }
    }
    catch (err) {

        isValid = false;
    }
    return isValid;
}

//      Function to generate the contact details for an invoice
function GenerationContactSectionHtml() {
    var strHtml = '';
    //  Create the header for the contact section
    strHtml += '<div id="pnlCreateInvHeader_0" class="lmHeader" onclick="ToggleCreationDisplay(0)" style="clear:both;">';
            strHtml += '<label id="lblCreateInvTitle_0" class="lmHeaderText  float-left">Contact: Not Selected</label>';
            strHtml += '<label id="lblCreateInvToggle_0" class="lmHeaderText lmHeaderToggle">-</label>';
    strHtml += '</div>';
    //  Create the colapsable body container
    strHtml += '<div id="pnlCreateInvBody_0"  style="clear:both;">';
    //  Create the item list container
    strHtml += '<div id="pnlInvContactList" class="width-full">';
            //  Add a wait timer as a place holder tille the list is loaded and the Html created
            strHtml += '<table class="waitTimer"><tr><td>';
            strHtml += 'Loading Contact Details. Please Wait.';
            strHtml += '</td></tr><tr><td class="width-full">';
            strHtml += '<img id="waitImg_pnlInvContact" class="padded" src="Images/load_Medium.gif">';
            strHtml += '</td></tr></table>';
        strHtml += '</div>';
    //  Close the container
    strHtml += '</div>';
    //  Return the constructed Html
    return strHtml;
}

//      Function to generate the html for the contact edit section content
function GenerateContactRightPanel() {
    //Generate the edit section
    var strHtml = '<div id="pnlInvAddRight" class="width-half float-right">';
            strHtml += '<div class="width-full center float-left">';
                //  Title field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactTitle" class="clientInput ui-autocomplete-input" type="text" disabled="disabled" autocomplete="off" name="txtContactTitle" title="Title" placeholder="Title">';
                strHtml += '</div>';
                //  Forename field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactForename" class="clientInput" type="text" disabled="disabled" name="txtContactForename" title="Forename" placeholder="Forename">';
                strHtml += '</div>';
                //  Surname Field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactSurname" class="clientInput" type="text" disabled="disabled" name="txtContactSurname" title="Surname" placeholder="Surname">';
                strHtml += '</div>';
                //  Telephone field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactTelephone" class="clientInput" type="text" disabled="disabled" name="txtContactTelephone" title="Telephone Number" placeholder="Telephone Number">';
                strHtml += '</div>';
                //  Mobile field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactMobile" class="clientInput" type="text" disabled="disabled" name="txtContactMobile" title="Mobile Number" placeholder="Mobile Number">';
                strHtml += '</div>';
                //  Email field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactEmail" class="clientInput" type="text" disabled="disabled" name="txtContactEmail" title="Email Address" placeholder="Email Address">';
                strHtml += '</div>';
                //  Street No Field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactStreetNumber" class="clientInput" type="text" name="txtContactStreetNumber" title="House Name / Number" placeholder="House Name / Number">';
                strHtml += '</div>';
                //  Address 1 field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactAddress1" class="clientInput" type="text" name="txtContactAddress1" title="Address Line 1" placeholder="Address Line 1">';
                strHtml += '</div>';
                //  Address 2 field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactAddress2" class="clientInput" type="text" name="txtContactAddress2" title="Address Line 2" placeholder="Address Line 2">';
                strHtml += '</div>';
                //  Town Field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactTown" class="clientInput ui-autocomplete-input" type="text" autocomplete="off" name="txtContactTown" title="Town" placeholder="Town">';
                strHtml += '</div>';
                //  County field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactCounty" class="clientInput ui-autocomplete-input" type="text" autocomplete="off" name="txtContactCounty" title="County" placeholder="County">';
                strHtml += '</div>';
                //  Post Code field
                strHtml += '<div class="clientField">';
                    strHtml += '<input id="txtContactPostCode" class="clientInput" type="text" name="txtContactPostCode" value="XXXX X1" title="Post Code" placeholder="Post Code">';
                strHtml += '</div>';
                // DGSAP-852 - Label to display whether contact is receiving SMS
                strHtml += '<div class="clientField">';
                strHtml += '<label id="contactSmsSend" class="clientInput" style = "width:100%;"> Client will receive SMS confirmation </label>';
                strHtml += '</div>';
            strHtml += '</div>';
        strHtml += '</div>';
    //  Return the constructed html
    return strHtml;
}

//      Function to generate the html for the contact list content
function GenerateContactLeftPanel() {
    //Generate the pick list
    var strHtml = '<div id="pnlInvAddLeft" class="width-half float-left">';
        strHtml += '<div id="pnlInvAddLeft_Content" class="float-left width-full" style="overflow-y: scroll;">';
                strHtml += '<table class="searchGrid width-full white">';;
                    strHtml += '<tr><th>Name</th><th>Type</th></tr>';
                    var count = ListContacts.length;
                    ListContacts.forEach(function (c) {
                        //  Generate the html for the row that represents the current contact
                        strHtml += '<tr onclick="ContactSelected(' + c.Id.toString() + ');"><td>';
                            strHtml += c.FormatedName;
                            strHtml += '</td><td>';
                            strHtml += c.ContactTypeString;
                        strHtml += '</td></tr>';
                    });
                strHtml += '</table>';
            strHtml += '</div>';
        strHtml += '</div>';
    //  Return the constructed html
    return strHtml;
}

//      Function to generate the contact details for an invoice
function GenerationInvItemsSectionHtml() {
    var strHtml = '';
    //  Create the header for the invocie items section
    strHtml += '<div id="pnlCreateInvHeader_1" class="lmHeader" onclick="ToggleCreationDisplay(1)"  style="clear:both;">';
            strHtml += '<label id="lblCreateInvTitle_1" class="lmHeaderText  float-left">Invoice Items</label>';
            strHtml += '<label id="lblCreateInvToggle_1" class="lmHeaderText lmHeaderToggle">-</label>';
    strHtml += '</div>';
    //  Create the colapsable body container
    strHtml += '<div id="pnlCreateInvBody_1"  style="clear:both;">';
    //  Create the item list container
        strHtml += '<div id="pnlInvItemList" class="width-full">';
            //  Add a wait timer as a place holder tille the list is loaded and the Html created
            strHtml += '<table class="waitTimer"><tr><td>';
            strHtml += 'Loading Item Details. Please Wait.';
            strHtml += '</td></tr><tr><td class="width-full">';
            strHtml += '<img id="waitImg_pnlInvContact" class="padded" src="Images/load_Medium.gif">';
            strHtml += '</td></tr></table>';
        strHtml += '</div>';
        //  Create the hidden Add / Edit Item panel
        strHtml += '<div id="pnlInvItemEdit" class="hidden width-full float-left">';
            //  Add a wait timer as a place holder tille the list is loaded and the Html created
            strHtml += '<table class="waitTimer"><tr><td>';
            strHtml += 'Loading Item Details. Please Wait.';
            strHtml += '</td></tr><tr><td class="width-full">';
            strHtml += '<img id="waitImg_pnlInvContact" class="padded" src="Images/load_Medium.gif">';
            strHtml += '</td></tr></table>';
        strHtml += '</div>';
   
    //  Close the container
    strHtml += '</div>';
    //  Return the constructed Html
    return strHtml;
}

//      Function to generate the html contant for the items section
function GenerateInvItemPanel() {
    var strHtml = '';
    strHtml += '<div class="width-full float-left">';
        //  The way the items are displayed is dependant on the invoice status
    if (ObjInvoice.Status == 0) {
        //debugger
        //  The invoice is new or has net been sent so items can be added and edited
        //  Only display the completed work section if there are un invoiced items
        if (ListInvoiceableItems.length > 0) {
            strHtml += '<div class="companyTabBar width-full float-left">';
            strHtml += '<span class="padded">Completed work items that can be added to an invoice.</span>';
            strHtml += '</div>';
            strHtml += GenerateEditableInvPdfItems(ListInvoiceableItems, false, 'Uninvoiced');
        } else {
            strHtml += '<div class="companyTabBar width-full float-left">';
            strHtml += '<span class="padded">Completed work items that can be added to an invoice.</span>';
            strHtml += '</div>';
            ObjJob.WorkSites.forEach(function(js) {
                strHtml += '<table class="searchGrid small width-full"><col width="5%"><col width="20%"><col width="55%"><col width="10%"><col width="10%">';
                strHtml += '<tr>';
                strHtml += '<th colspan="5"><span class="float-left padded">' + js.SiteString + '</span>';
                strHtml += '<input type="button" class="button-thin padded float-right" value="Add Item" onclick="AddInvoiceItem(' + js.Id + ');" />';
                strHtml += '</th></tr>';
                
                if (ObjInvoice.InvoicedItems.length == 0 && ListNewItems.length == 0) {
                    strHtml += '<th colspan="5">There are no completed work items to display. Please add an item. </th></tr>';
                }
                strHtml += '</table>';
                strHtml += '</br>'; //  Force a line break
            });
        }
        //  Only display the Invoice items section if there are invoiced items
        if (ObjInvoice.InvoicedItems.length > 0 || ListNewItems.length > 0) {
            strHtml += '<div class="colored-raise width-full float-left">';
            strHtml += '<span class="padded">Items assigned to invoice.</span>';
            strHtml += '</div>';
            strHtml += GenerateEditableInvPdfItems(ObjInvoice.InvoicedItems, true, 'Invoice');
        }
        strHtml += '</br>'; //  Force a line break
        //  Add an update button
        strHtml += '<input type="button" class="button-thin padded float-right" value="Create Invoice" onclick="SaveInvoice();" /></th>';
    } else {
        //  The items are not editable and the invoice cannot be modified
        strHtml += '<div class="colored-raise width-full float-left">';
        strHtml += '<span class="padded">The invoice has been dispatched and cannot be edited.</span>';
        strHtml += '</div>';
        //  For the invoice to have passed validation there must have been at least one item
        strHtml += GenerateInvoicedPdfItems();
    }
    strHtml += '</div>';
        return strHtml;
}

//      Function to generate html for an items section
function GenerateEditableInvPdfItems(list, isChecked, totalLabel) {
    var strHtml = '';
    var JobVatRates = new Array();
    ObjJob.WorkSites.forEach(function(js) {
        var SiteVatRates = new Array();
        //  Add the site identifier
        //  Setup the table
        strHtml += '<table class="searchGrid small width-full"><col width="5%"><col width="20%"><col width="55%"><col width="10%"><col width="10%">';
        strHtml += '<tr>';
        strHtml += '<th colspan="5"><span class="float-left padded">' + js.SiteString + '</span>';
        if (!isChecked) {
            //  The items being rendered are not checked, add a select all check box
            strHtml += '<input id="chkPdfSelectAll_' + js.Id.toString() + '" class="float-right padded" type="checkbox" onclick="SelectAllInvItems(' + js.Id + ');" style="cursor: pointer; vertical-align: center"/>';
            strHtml += '<label class="float-right padded">Select All</label>';
            //check if we need to add an add button for the site
            var invItems = $.grep(ObjInvoice.InvoicedItems, function(e) { return e.JobSiteId == js.Id; });
            itemSort(invItems);
            var newItems = $.grep(ListNewItems, function(e) { return e.JobSiteId == js.Id; });
            itemSort(newItems);
            if ((invItems && invItems.length == 0) && (newItems && newItems.length == 0)) {
                strHtml += '<input type="button" class="button-thin padded float-right" value="Add Item" onclick="AddInvoiceItem(' + js.Id + ');" />';
            }
        } else {
            strHtml += '<input type="button" class="button-thin padded float-right" value="Add Item" onclick="AddInvoiceItem(' + js.Id + ');" />';
        }
        strHtml += '</th></tr>';
        //  Sort the items based on the current job site - This removes an additional service call for each site
        var items = $.grep(list, function (e) { return e.JobSiteId == js.Id; });
        itemSort(items);
        //debugger;
        if (isChecked) {
            //  We are currently building the section for invoiced items
            BuildItemTable(items, SiteVatRates, JobVatRates, function(itm) {
                strHtml += '<tr>';
                strHtml += '<td style="vertical-align: top; text-align: center;"  onclick="EditInvoiceItem(' + itm.Id + ')">' + itm.Code + '</td>';
                strHtml += '<td style="vertical-align: top;" onclick="EditInvoiceItem(' + itm.Id + ')">' + itm.Name + '</td>';
                strHtml += '<td onclick="EditInvoiceItem(' + itm.Id + ')">' + itm.Description + '</td>';
                strHtml += '<td style="text-align: right; padding-right: 3px; padding-left: 3px; vertical-align: bottom;"' + (itm.Status == 0 ? ' onclick="EditInvoiceItem(' + itm.Id + ')"' : '') + '>' + (itm.Value > 0 ? ' ' + CurrentCurrency + '' + itm.Value.format() : '') + '</td>';
                strHtml += '<td style="text-align: center; padding-right: 3px; padding-left: 3px; vertical-align: center;">';
                strHtml += '<input id="chkPdfInvoiceItem_' + itm.Id + '" type="checkbox" style="cursor: pointer;" checked="checked"/></td>';
                strHtml += '</tr>';
            });

            //  Sort new invoice items items based on the current job site - This removes an additional service call for each site
            var newItems = $.grep(ListNewItems, function(e) { return e.JobSiteId == js.Id; });
            itemSort(newItems);
            if (newItems && newItems.length > 0) {
                BuildItemTable(newItems, SiteVatRates, JobVatRates, function(itm) {
                    strHtml += '<tr>';
                    strHtml += '<td style="vertical-align: top; text-align: center;"  onclick="EditInvoiceItem(' + itm.Id + ')">' + itm.Code + '</td>';
                    strHtml += '<td style="vertical-align: top;" onclick="EditInvoiceItem(' + itm.Id + ')">' + itm.Name + '</td>';
                    strHtml += '<td onclick="EditInvoiceItem(' + itm.Id + ')">' + itm.Description + '</td>';
                    strHtml += '<td style="text-align: right; padding-right: 3px; padding-left: 3px; vertical-align: bottom;"' + (itm.Status == 0 ? ' onclick="EditInvoiceItem(' + itm.Id + ')"' : '') + '>' + (itm.Value > 0 ? ' ' + CurrentCurrency + '' + itm.Value.format() : '') + '</td>';
                    strHtml += '<td style="text-align: center; padding-right: 3px; padding-left: 3px; vertical-align: center;">';
                    strHtml += '<input id="chkPdfInvoiceItem_' + itm.Id + '" type="checkbox" style="cursor: pointer;" checked="checked"/></td>';
                    strHtml += '</tr>';
                });
            }

        } else {
            //   We are currently building the section for invoiceable (completed work) items
            BuildItemTable(items, SiteVatRates, JobVatRates, function(itm) {
                strHtml += '<tr>';
                strHtml += '<td style="vertical-align: top; text-align: center;"  onclick="EditWorkItem(' + itm.WorkItemId + ')">' + itm.Code + '</td>';
                strHtml += '<td style="vertical-align: top;" onclick="EditWorkItem(' + itm.WorkItemId + ')">' + itm.Name + '</td>';
                strHtml += '<td onclick="EditWorkItem(' + itm.WorkItemId + ')">' + itm.Description + '</td>';
                strHtml += '<td style="text-align: right; padding-right: 3px; padding-left: 3px; vertical-align: bottom;" onclick="EditWorkItem(' + itm.WorkItemId + ')">' + (itm.Value > 0 ? ' ' + CurrentCurrency + '' + itm.Value.format() : '') + '</td>';
                strHtml += '<td style="text-align: center; padding-right: 3px; padding-left: 3px; vertical-align: center;">';
                strHtml += '<input id="chkPdfWorkItem_' + itm.WorkItemId + '" type="checkbox" style="cursor: pointer;"/></td>';
                strHtml += '</tr>';
            });
        }

        strHtml += '</table>';
        //  NOTE: Value updates
        //  Check if site subtotals need to be displayed
        if (ObjJob.WorkSites.length > 1) {
            strHtml += GetSiteTotalHtml(SiteVatRates, ObjJob.WorkSites.length, 'Site');
            strHtml += '<hr/>';
        }
    });
    //Display the invoice totals
    strHtml += GetSiteTotalHtml(JobVatRates, ObjJob.WorkSites.length, totalLabel);

    return strHtml;
}

//      Function to generate the html for the items section of an invoice that has been sent
function GenerateInvoicedPdfItems() {
    var strHtml = ''
    var JobVatRates = new Array();
    var editable = ObjInvoice.Status == 0;
    ObjJob.WorkSites.forEach(function (js) {
        var SiteVatRates = new Array();
        //  Add the site identifier
        //  Setup the table
        strHtml += '<table class="searchGrid small width-full"><col width="5%"><col width="20%"><col width="55%"><col width="10%"><col width="10%">';
        strHtml += '<tr>';
        strHtml += '<th colspan="5"><span class="float-left padded">' + js.SiteString + '</span></th>';
        strHtml += '</tr>';
        //  Sort the invoiced items items based on the current job site - This removes an additional service call for each site
        var items = $.grep(ObjInvoice.InvoicedItems, function (e) { return e.JobSiteId == js.Id; });
        itemSort(items);
        //As the invoice has been sent, the items are to be displayed only
        BuildItemTable(items, SiteVatRates, JobVatRates, function (itm) {
            strHtml += BuildInvoiceItemHtml(itm, editable);
        });
        strHtml += '</table>';
        //  NOTE: Value updates
        //  Check if site subtotals need to be displayed
        if (ObjJob.WorkSites.length > 1) {
            strHtml += GetSiteTotalHtml(SiteVatRates, ObjJob.WorkSites.length, 'Site');
            strHtml += '<hr/>';
        }
    });
    //Display the invoice totals
    strHtml += GetSiteTotalHtml(JobVatRates, ObjJob.WorkSites.length, 'Invoice');
    return strHtml;
}

//      Function to generate the html for the edit item panel
function GenerateEditItemPanel(item, isInvoiceItem, isUpdatingNewInvoiceItem) {
    //  Create the controls to capture
    var strHtml = '<table class="searchGrid width-full" style="font-size: 0.7em;">';
    strHtml += '<tr><th style="width:7%;text-align: center;"> Item Order </th><th style="width:7%;text-align: center;"> Tree No. </th><th style="width:18%;text-align: center;"> Tree Name </th><th style="width:38%;text-align: center;"> Description </th>';
    strHtml += '<th style="width:14%;text-align: center;"> Value </th><th style="width:11%;text-align: center;"> VAT Rate </th></tr>';
    if (isInvoiceItem) {
        strHtml += '<tr><td style="width:7%;">';
        if (isUpdatingNewInvoiceItem || item.OrderId > 0) {
            strHtml += '<input id="txtItemOrder" class="jobInput width-full" type="text" style="width:85%;" name="txtItemOrder" value="' + item.OrderId + '">';
        } else {
            strHtml += '<input id="txtItemOrder" class="jobInput width-full" type="text" style="width:85%;" name="txtItemOrder" value="1">';
        }
        strHtml += '<td style="width:7%;">';
        strHtml += '<input id="txtItemTreeNo" class="jobInput width-full" type="text" style="width:85%;" name="txtItemTreeNo" value="' + item.Code + '">';
        strHtml += '</td><td style="width:18%;"><span class="ui-helper-hidden-accessible" role="status" aria-live="polite"></span>';
        strHtml += '<input id="txtItemTreeName" class="jobInput width-full ui-autocomplete-input" type="text" style="width:90%;" name="txtItemTreeName" autocomplete="off" value="' + item.Name + '">';
    } else {
        strHtml += '<tr><td style="width:7%;">';
        strHtml += '<input id="txtItemOrder" class="jobInput width-full" type="text" style="width:85%;" name="txtItemOrder" value="' + item.OrderId + '">';
        strHtml += '<td style="width:7%;">';
        strHtml += '<input id="txtItemTreeNo" class="jobInput width-full" type="text" style="width:85%;" name="txtItemTreeNo" value="' + item.Code + '" disabled = "true">';
        strHtml += '</td><td style="width:18%;"><span class="ui-helper-hidden-accessible" role="status" aria-live="polite"></span>';
        strHtml += '<input id="txtItemTreeName" class="jobInput width-full ui-autocomplete-input" type="text" style="width:90%;" name="txtItemTreeName" autocomplete="off" value="' + item.Name + '" disabled = "true">';
    }
    strHtml += '</td><td style="width:38%;"><span class="ui-helper-hidden-accessible" role="status" aria-live="polite"></span>';
    strHtml += '<input id="txtItemDescription" class="jobInput width-full ui-autocomplete-input" type="text" style="width:95%;" name="txtItemDescription" autocomplete="off" value="' + item.Description + '">';
    strHtml += '</td><td style="width:14%;">';
    strHtml += '<input id="txtItemValue" class="jobInput float-right" type="text" style="width:80%;" name="txtItemValue" value="' + item.Value.format() + '">';
    strHtml += '<span id="Label1" class="float-right" style="padding-top: 7px; padding-right: 4px;"> ' + CurrentCurrency + ' </span>';
    strHtml += '</td><td style="width:11%;"><span id="Label2" class="float-right" style="padding-top: 7px; padding-right: 4px;"> % </span>';
    strHtml += '<input id="txtItemVatRate" class="jobInput float-right" type="text" style="width:50%;" name="txtItemVatRate" value="' + item.VatRate.format() + '">';
    strHtml += '</td></tr>';
    //  Add the update button
    strHtml += '<tr><td colspan="6">';
    strHtml += '<button type="button" onclick="';
    if (isInvoiceItem) {
        if (isUpdatingNewInvoiceItem) {
            strHtml += 'UpdateInvoiceItem(' + item.Id.toString();
        } else {
            strHtml += 'SaveInvoiceItem(' + item.Id.toString();
        }
    } else {
        strHtml += 'SaveWorkItem(' + item.WorkItemId.toString();
    }
    strHtml += ');" class="button-thin padded float-right">Save</button>';
    strHtml += '<button type="button" onclick="CancelEditItem();" class="button-thin padded float-right">Cancel</button>';
    strHtml += '</td></tr>';
    strHtml += '</table>';

    return strHtml;
}

//      Function to generate the html for a single invoiced item
function BuildInvoiceItemHtml(itm) {
    var strHtml = '<tr>';
    strHtml += '<td style="vertical-align: top; text-align: center;">' + itm.Code + '</td>';
    strHtml += '<td style="vertical-align: top;">' + itm.Name + '</td>';
    strHtml += '<td colspan="2">' + itm.Description + '</td>';
    strHtml += '<td style="text-align: right; padding-right: 3px; padding-left: 3px; vertical-align: bottom;">' + (itm.Value > 0 ? ' ' + CurrentCurrency + '' + itm.Value.format() : '') + '</td>';
    strHtml += '</tr>';
    return strHtml;
}

//      Function to colapse / expand the invoice creation panels
function ToggleCreationDisplay(elementIndex) {
    var bodyElement = document.getElementById('pnlCreateInvBody_' + elementIndex.toString());
    var toggleElement = document.getElementById('lblCreateInvToggle_' + elementIndex.toString());
    if (ArrCreatePnlShown[elementIndex]) {
        toggleElement.textContent = '+';
        $(bodyElement).slideUp();
    } else {
        toggleElement.textContent = '-';
        $(bodyElement).slideDown();
    }
    ArrCreatePnlShown[elementIndex] = !ArrCreatePnlShown[elementIndex];
}

//      Function to populate the contact details when a contact is selected
function ContactSelected(contactId) {
    var contact = GetSelectedContact(contactId);

    //  This is the cantact that is to be displayed
    //  Display the contact
    document.getElementById('txtContactTitle').value = contact.Title;
    document.getElementById('txtContactForename').value = contact.Forename;
    document.getElementById('txtContactSurname').value = contact.Surname;
    document.getElementById('txtContactStreetNumber').value = contact.StreetNumber;
    document.getElementById('txtContactTelephone').value = contact.Telephone;
    document.getElementById('txtContactMobile').value = contact.Mobile;
    document.getElementById('txtContactEmail').value = contact.Email;
    document.getElementById('txtContactAddress1').value = contact.Address1;
    document.getElementById('txtContactAddress2').value = contact.Address2;
    document.getElementById('txtContactTown').value = contact.Town;
    document.getElementById('txtContactCounty').value = contact.County;
    document.getElementById('txtContactPostCode').value = contact.PostCode;
    var contactAlert = document.getElementById('contactSmsSend');
    if (!contact.SendSmsMessage) {
        contactAlert.innerText = "SMS will not be sent";
    } else {
        contactAlert.innerText = "SMS will be sent";
    }

    if (!isResend) {
        //  Set the invoice contact Id 
        ObjInvoice.ContactId = contact.Id;
    }

    ObjInvoice.PointOfContact.Id = contact.Id;
    //  Set the contact details bar to display the selected contacts name
    document.getElementById('lblCreateInvTitle_0').textContent = 'Contact: ' + contact.Forename + ' ' + contact.Surname + ' (' + contact.ContactTypeString + ')';
    //  As the contact has been found and the controls populated we can exit the loop
    return contact;
}

// function to get the current selected contact
function GetSelectedContact(id) {
    var retVal;
    var count = ListContacts.length;
    if (id > 0) {
        for (var i = 0; i < count; i++) {
            var contact = ListContacts[i];
            if (contact.Id == id) {
                retVal = contact;
                break;
            }
        }
    }
    return retVal;
}

//      Function to get the default contacts Id
function GetDefaultContactId() {
    var id = -1;
    var count = ListContacts.length;
    if (count > 1) {
        for (var i = 0; i < count; i++) {
            if (ListContacts[i].ContactType == 0) {
                id = ListContacts[i].Id;
                break;
            }
        }
    } else {
        id = ListContacts[0].Id;
    }
    return id;
}

//      Function to select all invoiceable work items for a job site
function SelectAllInvItems(jobSiteId) {
    var workItems = $.grep(ListInvoiceableItems, function (e) { return e.JobSiteId == jobSiteId; });
    var select = document.getElementById('chkPdfSelectAll_' + jobSiteId.toString()).checked;
    if (workItems && workItems.length > 0) {
        workItems.forEach(function (itm) {
            var chkBox = document.getElementById('chkPdfWorkItem_' + itm.WorkItemId.toString());
            if (chkBox) {
                chkBox.checked = select;
            }
        });
    }
}

//      Function to validate and save the invoice
function SaveInvoice() {
    var ListUpdateItems = [];
    var ListDeletedItems = [];
    //  Sort all current items associated with the invoice
    //  The current items associated with the invoice
    ObjInvoice.InvoicedItems.forEach(function (invItm) {
        var chkBox = document.getElementById('chkPdfInvoiceItem_' + invItm.Id.toString());
        if (chkBox) {
            if (chkBox.checked) {
                //  The item has been selected and may have been updated add it to the updated array
                ListUpdateItems.push(invItm);
            } else {
                //  The Item has been removed from the invoice add it to the deleted array
                ListDeletedItems.push(invItm);
            }
        }
    });
    //  The new items assoiciated with invoice
    ListNewItems.forEach(function (newItm) {
        var chkBox = document.getElementById('chkPdfInvoiceItem_' + newItm.Id.toString());
        if (chkBox && chkBox.checked) {
                //  The item has been added so add it to the updated array
                ListUpdateItems.push(newItm);
            }
    });
    //  The completed work items
    ListInvoiceableItems.forEach(function (completedItm) {
        var chkBox = document.getElementById('chkPdfWorkItem_' + completedItm.WorkItemId.toString());
        if (chkBox && chkBox.checked) {
            //  The item has been added so add it to the updated array
            ListUpdateItems.push(completedItm);
        }
    });

    Invoice_Save(ListUpdateItems, ListDeletedItems);
}

//      Function to validate the status of the invoice for saving: Returns true if valid
function InvoiceIsValid() {

}

//      Function to display the edit item panel for a work item
function EditWorkItem(itemId) {
    var pnlItemList = document.getElementById('pnlInvItemList');
    var pnlEditItem = document.getElementById('pnlInvItemEdit');
    try {
        var item = GetItemForId(ListInvoiceableItems, itemId, true);
        if (item == null) {
            // The item for the specified Id has not been found
            DisplayMessage(pnlItemList.id, 'Error: Unable to display add invoice item view - Invalid work item Id');
        } else {
            //  Generate the html content for the panel
            pnlEditItem.innerHTML = GenerateEditItemPanel(item, false);
            pnlItemList.className = 'width-full float-left hidden';
            pnlEditItem.className = 'width-full float-left';
        }

    } catch (err) {
        DisplayMessage(pnlItemList.id, 'Error: Unable to display add invoice item view for work item - ' + err.toString());
    }
}

//      Function to update a work item (locally only)
function SaveWorkItem(workItemId) {
    var panel = document.getElementById('pnlInvItemEdit');
    try {
        var item = GetItemForWorkItemId(ListInvoiceableItems, workItemId);
        if (item == null) {
            // The item for the specified Id has not been found
            DisplayMessage(panel.id, 'Error: Unable to save item, invoicable item not found');
        } else {
            UpdateItemWaitDisplay();
            //  Save the item updates
            item = UpdateItemValues(item);
            //  Show the updated item div
            InvoiceableItems_LoadCallback(ListInvoiceableItems);
        }
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to save item, invoicable item not found - ' + err.toString());
    }
}

//      Function to add a new invoice item to the invoice
function AddInvoiceItem(jobSiteId) {
    //  Get references to the panels being modified 
    var pnlItemList = document.getElementById('pnlInvItemList');
    var pnlNewItem = document.getElementById('pnlInvItemEdit');
    //  Set the css of the panels so the correct panel is displayed
    pnlItemList.className = 'width-full float-left hidden';
    pnlNewItem.className = 'width-full float-left';
    //  Display a wait timer in the edit item panel
    pnlNewItem.innerHTML = GetWaitImageForPanel(pnlNewItem.id, 'Loading Please Wait');
    //  Retreave a new item from the service
    NewInvoiceItem_Load(jobSiteId);
}

//      Function to display the edit item panel for an invoice item
function EditInvoiceItem(itemId) {
    var pnlItemList = document.getElementById('pnlInvItemList');
    var pnlEditItem = document.getElementById('pnlInvItemEdit');
    try {
        var item = null;
        if (itemId < 0) {
            // new items are added to the listnewitems but are not work items.
            item = GetItemForWorkItemId(ListNewItems, itemId, true);
        } else {
            item = GetItemForId(ObjInvoice.InvoicedItems, itemId);
        }
        //  Check if the item has been found
        if (item == null) {
            //  The item was not in the invoiceable items so check 
            DisplayMessage(pnlItemList.id, 'Error:  Unable to display add invoice item view - Invalid invoice item Id');
        } else {
            //  Generate the html content for the panel
            if (itemId < 0) {
                pnlEditItem.innerHTML = GenerateEditItemPanel(item, true, true);
            } else {
                pnlEditItem.innerHTML = GenerateEditItemPanel(item, true);
            }
            pnlItemList.className = 'width-full float-left hidden';
            pnlEditItem.className = 'width-full float-left';
        }

    } catch (err) {
        DisplayMessage(pnlItemList.id, 'Error: Unable to display add invoice item view - ' + err.toString());
    }
}

function UpdateInvoiceItem(invoiceItemId) {
    try {
        UpdateItemWaitDisplay();
        //  If the id is -1 or less this is a new item
        if (invoiceItemId == ObjNewInvItem.Id) {
            ObjNewInvItem = UpdateItemValues(ObjNewInvItem);
        }
        InvoiceableItems_LoadCallback(ListInvoiceableItems);
    } catch (err) {
        alert('Error: Failed to update item - ', err.toString());
    }
}

//      Function to Update / save an invoice item
function SaveInvoiceItem(invoiceItemId) {
    try {
        UpdateItemWaitDisplay();
        //  If the id is -1 or less this is a new item
        var item = null;
        if (invoiceItemId > 0) {
            //  The item exists in loacal memory and needs to be updated
            item = GetItemForId(ObjInvoice.InvoicedItems, invoiceItemId);
            item = UpdateItemValues(item);
        } else {
            //  The item is a new item that has been added
            if (invoiceItemId == ObjNewInvItem.Id) {
                ObjNewInvItem = UpdateItemValues(ObjNewInvItem);
                ListNewItems.push(ObjNewInvItem);
            } else {
                item = GetItemForId(ListNewItems, invoiceItemId);
                item = UpdateItemValues(item);
            }
        }
        InvoiceableItems_LoadCallback(ListInvoiceableItems);
    } catch (err) {
        alert('Error: Failed to update item - ', err.toString());
    }
}

//      Function to update a generic item based on a the users input
function UpdateItemValues(item){
    item.OrderId = document.getElementById('txtItemOrder').value;
    if (!item.OrderId || item.OrderId === ""){
        item.OrderId = 1;
    }
    item.Code = document.getElementById('txtItemTreeNo').value;
    item.Name = document.getElementById('txtItemTreeName').value;
    item.Description = document.getElementById('txtItemDescription').value;
    item.Value = parseFloat(document.getElementById('txtItemValue').value.replace(',', ''));
    item.VatRate = parseFloat(document.getElementById('txtItemVatRate').value);

    return item;
}

//      Function to cancel the edit / add of an invoice item
function CancelEditItem() {
    //  Get references to the panels being modified 
    var pnlItemList = document.getElementById('pnlInvItemList');
    var pnlNewItem = document.getElementById('pnlInvItemEdit');
    //  Set the css of the panels so the correct panel is displayed
    pnlNewItem.className = 'width-full float-left hidden';
    pnlItemList.className = 'width-full float-left';
}

//      Function to display wait timer durring item update
function UpdateItemWaitDisplay() {
    //  Get references to the panels being modified 
    var pnlItemList = document.getElementById('pnlInvItemList');
    var pnlNewItem = document.getElementById('pnlInvItemEdit');
    //  Generate a wait timer to be displayed
    pnlItemList.innerHtml = GetWaitImageForPanel(pnlItemList.id, 'Updating Items, Please Wait');
    //  Set the css of the panels so the correct panel is displayed
    pnlNewItem.className = 'width-full float-left hidden';
    pnlItemList.className = 'width-full float-left';
}

//      Function to get a specific item from a list based on the item Id
function  GetItemForId(listItems, itemId, isWorkItem){
    var item = null;
    var count = listItems.length;
    //  Itereate throught the items list to see if the item can be found
    for (var i = 0; i < count; i++) {
        var itm = listItems[i];
        if (isWorkItem) {
            if (itm.WorkItemId == itemId) {
                item = itm;
                break;
            }
        } else {
            if (itm.Id == itemId) {
                item = itm;
                break;
            }
        }
    }
    return item;
}

//      Function to get a specific work item from a list based on the work item Id
function GetItemForWorkItemId(listItems, itemId, isNewInvoiceItem) {
    //  NOTE: The work items list is a list of invoice items with an Id of -1
    var item = null;
    var count = listItems.length;
    //  Itereate throught the items list to see if the item can be found
    for (var i = 0; i < count; i++) {
        var itm = listItems[i];
        if (isNewInvoiceItem) {
            if (itm.Id == itemId) {
                item = itm;
                break;
            }
        } else {
            if (itm.WorkItemId == itemId) {
                item = itm;
                break;
            }
        }
    }
    return item;
}

function GetSelectedInvItems(sourceList) {
    var items = [];
    sourceList.forEach(function(itm){
        var chkBox = document.getElementById('' + itm.Id.toString());
        if(chkBox && chkBox.checked){
            items.push(itm);
        }
    });
    return items;
}

/*

        Dialog Display Functions

*/

//      Function to display a dialog to inform users an email has been sent
function DisplayEmailMsg(result) {
    UpdateInvoiceStatus();
    var message;
    if (result) {
        GetSmtpSettings();
        if (EmailSmtpObj.UseSmtp) {
            message = 'Emails and Text messages (if applicable) have been generated and queued for dispatch.<br/> Please check your email client for confirmation';
        } else {
            message = 'Emails and Text messages (if applicable) have been generated and queued for dispatch.<br/> <a href="/Company.aspx"> See here - under the emails panel for all sent emails. </a>';
        }

        var b = Date.parse(ObjInvoice.DateCreated);
        if (b === 0) {
            message += '<br/><br/><font color="red">Warning: Could not find Invoice Creation Date. You may want to contact the customer to provide it.</font>';
        }
    }
    else {
        message = 'Error: emails not sent';
    }

    Dialog_OpenPage(
   'Email',
   'Dialogs/Alert.aspx',
   'content',
   500,
   250,
   true,
   {
       'Ok': {
           text: 'Ok',
           class: 'ui-dialog-button',
           click: function () {
               $(this).dialog('close');
           }
       }
   },
   'Content/js/AlertBox.js?V=3.23.5',
   function () {
       AlertBox_Show(message);
   });

}

/*

        Required Functions

*/

//      Required! - Attach the click event handler for the Download button
function HookPdfToDownload() {
    var openPdf = document.getElementById('btnDownload');

    openPdf.onclick = function () {
        window.open($("#pdfPreview").contents().find("#hiddenGeneratedPdfUrl").val());
    }
}

//      REQUIRED! - Function to toggle the display of the required body panel
function ToggleDsiplay(elementIndex) {
    //NOTE: The element index is appended to the end of the ID in the aspx file This function depends on the nameing convention
    //debugger;
    var bodyElement = document.getElementById('pnlPDFBody_' + elementIndex.toString());
    var toggleElement = document.getElementById('lblHeaderToggle_' + elementIndex.toString());
    if (ArrIsPanelShown[elementIndex]) {
        toggleElement.textContent = '+';
        $(bodyElement).slideUp();
    } else {
        //Check if the panel being expanded is for the emails
        if (elementIndex == 1) {
            //populate the letter templates
            ShowLetterTemplate();
        }
        toggleElement.textContent = '-';
        $(bodyElement).slideDown();
    }
    ArrIsPanelShown[elementIndex] = !ArrIsPanelShown[elementIndex];
}

//      REQUIRED! - Function to toggle the display of the email options
function ToggleEmailOptionDisplay(index) {
    ToggleDsiplay(index);
    $('#Dialog_Content').animate({ scrollTop: "450px" });
}


//      REQUIRED! - Function to load the list of all email attachments
function LoadAttachments() {
    lm_Attachments = new Array();
    lm_FileNames.forEach(function(fn) {
        lm_Attachments.push({ id: lm_FileNames.indexOf(fn), type: -1, fileName: fn });
    });

    var Docs = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetTempDocuments',
        {
            ItemID: ObjInvoice.Id
        },
        false);
    console.log("temp Docs");
    console.log(Docs);
    Docs.forEach(function(file) {
        lm_Attachments.push({ id: file.Id, type: file.DocumentTypeID, fileName: file.FileName });
    });
    ShowAttachments();
}

//      REQUIRED! - Function to add an email attachment
function AddAttachment() {
    
    var Upload = document.getElementById('fuAttachment');
    if (Upload.files.length > 0) {
        if (Upload.value.indexOf('.exe') != -1
            || Upload.value.indexOf('.jar') != -1
            || Upload.value.indexOf('.bat') != -1
            || Upload.value.indexOf('.bash') != -1
            || Upload.value.indexOf('.msi') != -1
            || Upload.value.indexOf('.dll') != -1) {
            alert("Invalid file type");
            return false;
        }

        document.getElementById('alertAttach').className = "warning-text";
        var file = Upload.files[0];

        var fr = new FileReader();
        fr.onload = function (event) {
            var result = event.target.result;
            var base64 = result.substring(result.indexOf("base64,") + 7);

            var Doc = Ajax_CallService(
                'Services/Web/Report.asmx',
                'AddTempDocument',
                {
                    ItemID: ObjInvoice.Id,
                    Description: "Temporary Document For Email Attachment",
                    ContentType: file.type,
                    ByteString: base64,
                    FilePath: Upload.value
                },
                false);

            SetSelected({ id: Doc.Id, type: Doc.DocumentTypeID }, true);
            LoadAttachments();
            Upload.value = "";
        };
        fr.readAsDataURL(file);
    } else {
        alert('No File Selected');
    }
}

//      REQUIRED! - Function to display the list of email attahcments
function ShowAttachments() {
    if (!lm_Attachments)
        lm_Attachments = new Array();

    var grid = document.getElementById('grdAttachments');
    if (grid == null) {
    }
    for (var i = grid.rows.length; i > 0; i--) {
        grid.deleteRow(i - 1);
    }

    lm_Attachments.forEach(function (attachment) {
        var newRow = grid.insertRow(-1);

        newRow.className = 'GridItem';

        var nameCell = newRow.insertCell(0);

        nameCell.innerHTML = attachment.fileName;

        switch (attachment.type) {
            case -1:
                newRow.className += ' GridItem_Report';
                break;
            case 0:
                newRow.className += ' GridItem_Global';
                break;
            case 1:
                newRow.className += ' GridItem_Client';
                break;
            case 2:
                newRow.className += ' GridItem_Temp';
                break;
        }

        var checkCell = newRow.insertCell(1);

        checkCell.style.textAlign = 'center';

        var cb = document.createElement('input');
        cb.type = 'checkbox';
        cb.id = 'attachmentCheckbox_' + attachment.type + '_' + attachment.id;
        cb.disabled = attachment.type == -1;
        checkCell.appendChild(cb);

        var checked = false;
        lm_SelectedAttachments.forEach(function (sel) {
            if (sel.id == attachment.id
                && sel.type == attachment.type)
                checked = true;
        });
        if (attachment.type == -1) {
            checked = true;
        }
        cb.checked = true;

        cb.addEventListener('change', function () {
            var check = document.getElementById(cb.id);
            SetSelected({ id: attachment.id, type: attachment.type }, check.checked);
        }, false);
    });
    console.log(lm_Attachments);
}

//      REQUIRED! - Function to select email attachments to be added to the email
function SetSelected(attachment, selected) {
    if (selected) {
        lm_SelectedAttachments.push({ id: attachment.id, type: attachment.type });
    } else {
        if (lm_SelectedAttachments.length > 0) {
            var index = 0;
            for (var i = 0; i < lm_SelectedAttachments.length; i++) {
                if (lm_SelectedAttachments[i].id == attachment.id
                    && lm_SelectedAttachments[i].type == attachment.type) {
                    index = i;
                }
            }
            lm_SelectedAttachments.splice(index, 1);
            console.log("attachment.id:" + attachment.id);
            var Doc = Ajax_CallService(
                'Services/Web/Report.asmx',
                'RemoveTempDocumentsById',
                {
                    ItemID: attachment.id,
                },
                false);
        }
    }
    LoadAttachments();
}

function SmsNotSent() {
    if (ListContacts != null && isSmsLoaded && isEmailLoaded) {
        var count = ListContacts.length;
        for (var i = 0; i < count; i++) {
            var contact = ListContacts[i];
            if (contact.Id == ObjInvoice.ContactId) {
                var smsAlert = document.getElementById('txtSmsText');
                var contactAlert = document.getElementById('alertSms');

                if (smsAlert == null) {
                    contactAlert.innerText = "SMS template error";
                } else if (!contact.SendSmsMessage) {
                    contactAlert.innerText = "SMS will not be sent";
                } else if (!ValidateConvertedSmsLength(smsAlert.value)) {
                    contactAlert.innerText = "SMS will not be sent: message is too long";
                } else {
                    contactAlert.innerText = "SMS will be sent";
                }
                break;
            }
        }
    }
}

function SmsTextConversion() {
    if (ListContacts != null) {
        // GetSelectedContact cannot return null
        var contact = GetSelectedContact(ObjInvoice.PointOfContact.Id);
        var smsText = document.getElementById('txtSmsText');
        smsText.value = SmsPatternPassing(smsText.value, contact.Title + " " + contact.Surname, contact.Email, "Invoice", "<OFFICE_TEL>", "<OFFICE_MOBILE>", "<COMPANY_CONTAT>", "<COMPANY_NAME>");
        contentTooLong();
    }
}

