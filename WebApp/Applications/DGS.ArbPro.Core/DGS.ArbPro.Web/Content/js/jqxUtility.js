﻿// Microsec utilities to support jqWidgets

// Utility function to render the toolbar for a  jqxDataTable
// Parameters:
// toolBar - the toolbar object to render - supplied by the parent jqxDataTable
// tableId - the Id of the table, including the # sign - for example "#tblQuoteItems

function renderJqxDataTableToolbar(toolBar, tableId) {

    var theme = 'arbpro';

    var toTheme = function (className) {
        if (theme == "") return className;
        return className + " " + className + "-" + theme;
    }
    // appends buttons to the status bar.
    var container = $("<div style='overflow: hidden; position: relative; height: 100%; width: 100%;'></div>");
    var buttonTemplate = "<div style='float: left; padding: 3px; margin: 2px;'><div style='margin: 4px; width: 16px; height: 16px;'></div></div>";
    var addButton = $(buttonTemplate);
    var editButton = $(buttonTemplate);
    var deleteButton = $(buttonTemplate);
    var cancelButton = $(buttonTemplate);
    var updateButton = $(buttonTemplate);
    container.append(addButton);
    container.append(editButton);
    container.append(deleteButton);
    container.append(cancelButton);
    container.append(updateButton);
    toolBar.append(container);
    addButton.jqxButton({ cursor: "pointer", enableDefault: false, height: 25, width: 25 });
    addButton.find('div:first').addClass(toTheme('jqx-icon-plus'));
    addButton.jqxTooltip({ position: 'bottom', content: "Add" });
    editButton.jqxButton({ cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25 });
    editButton.find('div:first').addClass(toTheme('jqx-icon-edit'));
    editButton.jqxTooltip({ position: 'bottom', content: "Edit" });
    deleteButton.jqxButton({ cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25 });
    deleteButton.find('div:first').addClass(toTheme('jqx-icon-delete'));
    deleteButton.jqxTooltip({ position: 'bottom', content: "Delete" });
    updateButton.jqxButton({ cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25 });
    updateButton.find('div:first').addClass(toTheme('jqx-icon-save'));
    updateButton.jqxTooltip({ position: 'bottom', content: "Save Changes" });
    cancelButton.jqxButton({ cursor: "pointer", disabled: true, enableDefault: false, height: 25, width: 25 });
    cancelButton.find('div:first').addClass(toTheme('jqx-icon-cancel'));
    cancelButton.jqxTooltip({ position: 'bottom', content: "Cancel" });
    var updateButtons = function (action) {
        switch (action) {
            case "Select":
                addButton.jqxButton({ disabled: false });
                deleteButton.jqxButton({ disabled: false });
                editButton.jqxButton({ disabled: false });
                cancelButton.jqxButton({ disabled: true });
                updateButton.jqxButton({ disabled: true });
                break;
            case "Unselect":
                addButton.jqxButton({ disabled: false });
                deleteButton.jqxButton({ disabled: true });
                editButton.jqxButton({ disabled: true });
                cancelButton.jqxButton({ disabled: true });
                updateButton.jqxButton({ disabled: true });
                break;
            case "Edit":
                addButton.jqxButton({ disabled: true });
                deleteButton.jqxButton({ disabled: true });
                editButton.jqxButton({ disabled: true });
                cancelButton.jqxButton({ disabled: false });
                updateButton.jqxButton({ disabled: false });
                break;
            case "End Edit":
                addButton.jqxButton({ disabled: false });
                deleteButton.jqxButton({ disabled: false });
                editButton.jqxButton({ disabled: false });
                cancelButton.jqxButton({ disabled: true });
                updateButton.jqxButton({ disabled: true });
                break;
        }
    }
    var rowIndex = null;
    $(tableId).on('rowSelect', function (event) {
        var args = event.args;
        rowIndex = args.index;
        updateButtons('Select');
    });
    $(tableId).on('rowUnselect', function (event) {
        updateButtons('Unselect');
    });
    $(tableId).on('rowEndEdit', function (event) {
        updateButtons('End Edit');
    });
    $(tableId).on('rowBeginEdit', function (event) {
        updateButtons('Edit');
    });
    addButton.click(function (event) {
        if (!addButton.jqxButton('disabled')) {
            // add new empty row.
            $(tableId).jqxDataTable('addRow', null, {}, 'first');
            // select the first row and clear the selection.
            $(tableId).jqxDataTable('clearSelection');
            $(tableId).jqxDataTable('selectRow', 0);
            // edit the new row.
            $(tableId).jqxDataTable('beginRowEdit', 0);
            updateButtons('add');
        }
    });
    cancelButton.click(function (event) {
        if (!cancelButton.jqxButton('disabled')) {
            // cancel changes.
            $(tableId).jqxDataTable('endRowEdit', rowIndex, true);
        }
    });
    updateButton.click(function (event) {
        if (!updateButton.jqxButton('disabled')) {
            // save changes.
            $(tableId).jqxDataTable('endRowEdit', rowIndex, false);
        }
    });
    editButton.click(function () {
        if (!editButton.jqxButton('disabled')) {
            $(tableId).jqxDataTable('beginRowEdit', rowIndex);
            updateButtons('edit');
        }
    });
    deleteButton.click(function () {
        if (!deleteButton.jqxButton('disabled')) {
            $(tableId).jqxDataTable('deleteRow', rowIndex);
            updateButtons('delete');
        }
    });
}

