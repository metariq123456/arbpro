﻿var PermitsNecessaryInfo = null;
var CurrentQuoteId = -1;
function UpdatePermitTypeList() {
    AutoComplete('txtPermitType', 'ListPermitType');
}
function Permits_Load(QuoteId) {
    CurrentQuoteId = QuoteId;
    PermitsNecessaryInfo = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'PermitsNecessaryGetForQuote',
            {
                quoteId: QuoteId
            },
            false);
    document.getElementById('pnlPermitsDetails').innerHTML = GenerateHTML(PermitsNecessaryInfo);
    UpdatePermitTypeList();
}

function PermitsNecessary_Save() {
    try {
            UpdatePermitsNecessary();
            Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'SavePermitsNecessary',
                {
                    obj: PermitsNecessaryInfo
                },
                false,
                null);
    }
    catch (err) {
        DisplayMessage('pnlPermitsError', 'Error: Permits Necessary failed to save.' + err.toString());
    }
}
function UpdatePermitsNecessary() {
    debugger;
    var PermitNecessaryElement = document.getElementById('PermitNecessaryOptions');
    var PermitNecessary = PermitNecessaryElement.options[PermitNecessaryElement.selectedIndex].value;
    var PermitObtainedElement = document.getElementById('PermitObtainedOptions');
    var PermitObtained = PermitObtainedElement.options[PermitObtainedElement.selectedIndex].value;
    var PermitType = document.getElementById('txtPermitType').value;
    var PermitComments = document.getElementById('txtPermitComments').value;
    var QuoteId = CurrentQuoteId;
    PermitsNecessaryInfo.QuoteId = QuoteId;
    PermitsNecessaryInfo.PermitNecessary = PermitNecessary;
    PermitsNecessaryInfo.PermitObtained = PermitObtained;
    PermitsNecessaryInfo.PermitType = PermitType;
    PermitsNecessaryInfo.PermitComments = PermitComments;

}
function GenerateHTML(PermitsNecessaryInfo) {
    debugger;
    var strHtml = '';
    strHtml += '<div style="width:595px;">'; //Write the containers opening tag
    strHtml += '<div id="pnlTpoDetails" class="tpoTabContentSection">'; //Details container tag
    strHtml += '<div id="pnlTpoDetailsHeader"  class="riskTabBar">'; //Details header container tag
    strHtml += '<label>Quote Reference: ' + PermitsNecessaryInfo.QuoteReference + '</label>';
    strHtml += '</div>';//Close pnlDetailsHeader tag
    strHtml += '<div id="pnlTpoDetailsContent">'; //Details body container tag
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Permit necessary</label>';
    if (PermitsNecessaryInfo.PermitNecessary == 1) {
        strHtml += '<select id="PermitNecessaryOptions" class="clientInput"><option value="1"  selected="selected">Yes</option><option value="0">No</option></select>';
    }
    else
    {
        strHtml += '<select id="PermitNecessaryOptions" class="clientInput"><option value="1">Yes</option><option value="0"  selected="selected">No</option></select>';
    }
    
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Permit Type</label>';
    strHtml += '<input id="txtPermitType" class="clientInput" type="text" placeholder="Permit Type" name="txtPermitType" value="' + PermitsNecessaryInfo.PermitType + '">';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Permit obtained</label>';
    if (PermitsNecessaryInfo.PermitObtained == 1) {
        strHtml += '<select id="PermitObtainedOptions" class="clientInput"><option value="1" selected="selected">Yes</option><option value="0">No</option></select>';
    }
    else {
        strHtml += '<select id="PermitObtainedOptions" class="clientInput"><option value="1" >Yes</option><option value="0" selected="selected">No</option></select>';
    }
    
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Permit Comments</label>';
    strHtml += '<textarea id="txtPermitComments" class="clientInput" rows="4" cols="30" placeholder="Permit Comments" name="txtPermitComments">' + PermitsNecessaryInfo.PermitComments + '</textarea>';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '</div>';
    return strHtml;
}