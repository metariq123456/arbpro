﻿/*
        NOTE: This js file may be loaded in as an alternate option for generating an Invoice
        Due to this the file must contain the following functions:
            ToggleDsiplay(elementIndex)         - function to collapse or expand the relevent section
            LoadAttachments()                   - function to load user attachments
            AddAttachment()                     - function to enable the user to add an attachment to an email
            ShowAttachments()                   - function to display the attachment names in the grid
            SetSelected(attachment, selected)   - function to set which attachments are selected
*/

//REQUIRED SCOPE VARIABLES - These are used for the attachment functionality
var lm_FileNames;
var lm_Attachments;
var lm_SelectedAttachments;

var IsPreview = false;
var ObjQuote = null;            //The Quote to be loaded
var ListContacts = null;        //The clients list of contacts
var ListPDFTemplates = null;    //The text templates that can be used for PDF generation
var ListEmailTemplates = null;  //The text templates that can be used for Email generation
var ListSmsTemplates = null;    // DGSAP-852 - The text templates that can be used for SMS generation
var isPdfLoaded = false;        // DGSAP-852
var isEmailLoaded = false;      // DSSAP-852
var isSmsLoaded = false;        // DSSAP-852
var isResend = false;           // bool determining if quote is being re-sent or not
//state flags
var ArrIsPanelShown = [true, true, true];
var EmailSmtpObj = [];

//Initialisation function
function PDFGeneration_Load(quoteId, reSend) {
    if (reSend) {
        isResend = reSend;
    }

    lm_FileNames = new Array();
    lm_SelectedAttachments = new Array();
    var e = document.getElementById('btnPreviewPdf');
    if (e) {
        e.disabled = true;
    }

    e = document.getElementById('btnGeneratePdf');
    if (e) {
        e.disabled = true;
    }

    Ajax_CallService(
        'Services/Web/Report.asmx',
        'ClearTempEmailAttachments',
        {},
        false);

    if (GetQuote(quoteId)) {
        GetClientContacts();
        GetEmailTemplates();
        // DGSAP-852
        GetSmsTemplates(quoteId);
        DisplayMessage('pnlPDFError', 'Please wait adding documents to email.');
        LoadDefaultCompanyDocuments();
        DisplayMessage('pnlPDFError', '');
    }

    //debugger
    ShowLetterTemplate();
}

function UpdateQuote() {
    try {
        ObjQuote.DateCreated = toDateFromJson(ObjQuote.DateCreated);
        ObjQuote.DateSent = toDateFromJson(ObjQuote.DateSent);
        var success = Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'SaveQuote',
           {
               obj: ObjQuote,
               selectedItems: []
           },
           false,
           null
       );

        return success;
    }
    catch (err) {
        DisplayMessage('pnlPDFError', 'Error: failed to update quote - ' + err.toString());
        return false;
    }
}

//Syhncronus service call to load a quote
function GetQuote(quoteId) {

    //NOTE: This is a blocking service call as no other functionality is relevant without the quote
    var errPanel = document.getElementById('pnlPDFError');
    try {
        ObjQuote = Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetQuote',
           {
               QuoteID: quoteId
           },
           false,
           null
       );
        //check the object is populated
        if (!ObjQuote) {
            DisplayMessage(errPanel.id, 'Error: Arb Pro service did not return a valid quote record');
            return false;
        } else if (ObjQuote.Id < 1) {
            DisplayMessage(errPanel.id, 'Information: The quote must be created, and items added before it can be sent.');
            return false;
        } else {
            return true;
        }
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
        return false;
    }
}
//Async service call to load a list of contacts for the email
function GetClientContacts() {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
           'Services/Web/ClientData.asmx',
           'GetQuoteClientContactList',
           {
               ClientID: ObjQuote.ClientId
           },
           true,
           SetClientContacts
       );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

//Async service call to load valid templates
function GetPDFTemplates() {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
            'Services/Web/Settings.asmx',
            'GetTemplateListForTypeAndStage',
            {
                Format: 0,
                tmplType: 0
            },
            true,
            PopulatePDFPanel
        );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}
//Async service call to load valid templates
function GetEmailTemplates() {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
           'Services/Web/Settings.asmx',
           'GetTemplateListForTypeAndStage',
           {
               Format: 1,
               tmplType: 0
           },
           true,
           PopulateEmailPanel
       );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

// DGSAP-852 Async service call to load valid templates
function GetSmsTemplates(quoteId) {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
           'Services/Web/Settings.asmx',
           'GetSmsTemplateListForTypeAndStage',
           {
               quoteId: quoteId,
               tmplType: 0
           },
           true,
           PopulateSmsPanel
       );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

////synchronus service call to set the status of the quote
function SetQuoteStatus(quoteId, status) {
    //debugger;
    var errPanel = document.getElementById('pnlPDFError');
    var retVal = false;
    try {
        retVal = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'SetQuoteStatus',
            {
                QuoteID: quoteId,
                Status: status
            },
            false,
            null
       );
       //Reload the job as the Quote will now have a reference
       Job_Refresh(ObjJob.Id);
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
        retVal = false;
    }
    return retVal;
}



//function SetQuoteStatus(quoteId, iStatus) {
//    var retVal = false;
//    try {

//        retVal = Ajax_CallService(
//            'Services/Web/WorkFlow.asmx',
//            'SetQuoteStatus',
//            {
//                QuoteID: quoteId,
//                Status: iStatus
//            },
//            false);

//        UpdateJobQuoteItemList();
//        UpdateJobWorkItemList();
//    }
//    catch (err) {
//        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
//        retVal = false;
//    }

//    return retVal;
//}


//Async service call to generate the required PDF document
//isPreview denotes if the generated PDF is to be inserted into the database and have a history created
function LoadPDF(isPreview) {
    //debugger;
    //Set the preview flag for the callback to access
    IsPreview = isPreview;
    //Display the wait timer
    ShowGenerationTimer(true);
    var pdfSettings = GetPdfSettings(isPreview, false);

    try {
        ObjQuote.JobItems = [];
        ObjQuote.QuotedItems = [];
        ObjQuote.History = [];
        ObjQuote.DateCreated = toDateFromJson(ObjQuote.DateCreated);
        ObjQuote.DateSent = toDateFromJson(ObjQuote.DateSent);

        var ddlContact = document.getElementById('ddlQuoteContact');
        ObjQuote.PointOfContact.Id = ddlContact.options[ddlContact.selectedIndex].value;

        Ajax_CallService(
            'Services/Web/Report.asmx',
            'UpdateQuoteAndOpenReport',
            {
                obj: ObjQuote,
                settings: pdfSettings
            },
            true,
            DisplayPDF
        );

    } catch (err) {
        DisplayMessage('pnlPDFError', 'Error: Unable to communicate with Arb Pro service - ', er.toString());
    }

}
//Async callback to generate the html for the PDF settings panel
function PopulatePDFPanel(results) {
    var panel = document.getElementById('pnlPDFBody_0');
    if (!results || results.length === 0) {
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
    } else {
        try {
            /*
                Quote doc type enum values
                Schedule,
                Letter,
                Both
            */
            ListPdfTemplates = results;
            //Generate the html for the div
            var strHtml = '';
            //Display the select box for the clients
            if (isResend) {
                // if quote is being re-sent display warning message
                strHtml += '<div class="lmField center" style="width:100%;">';
                strHtml += '<label class="lmField center"> Selecting a contact will not change the details in the PDF. However it will change the email address to which the PDF is sent to. </label>';
                strHtml += '</div>';
                strHtml += '<br/>';
            }
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlLetterType"> Contact </label>';
            strHtml += '<select id="ddlQuoteContact" class="lmFieldControl" onchange="QuoteContactChanged();" name="ddlQuoteContact">';
            if (ListContacts.length === 1) {
                //There is only one contact so auto select it
                strHtml += '<option value="' + ListContacts[0].Id + '" selected>' + ListContacts[0].SideBarDetail + '</option>';
            } else {
                strHtml += '<option value="-1"> Please select a contact </option>';
                ListContacts.forEach(function(c) {
                    strHtml += '<option value="' + c.Id + '" ';
                    if (ObjQuote.PointOfContact.Id === c.Id) {
                        strHtml += 'selected';
                    }
                    strHtml += '>' + c.NameAndEmail + '</option>';
                });
            }
            strHtml += '</select>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlLetterType"> Letter Type </label>';
            strHtml += '<select id="ddlLetterType" class="lmFieldControl" onchange="ShowLetterTemplate();" name="ddlLetterType">';
            strHtml += '<option value="0">Letter</option>';
            strHtml += '<option value="1">Schedule</option>';
            strHtml += '<option value="3">Letter & Schedule</option>';
            strHtml += '</select>';
            strHtml += '</div>';
            // div that hides unneeded content when quote is being re-sent
            strHtml += '<div id="hideWhenResend" class="lmField" style="width:100%;">';
            strHtml += '<div class="lmField" style="width: 100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlLetterTemplate"> Template Text</label>';
            strHtml += '<select id="ddlLetterTemplate" class="lmFieldControl" onchange="LetterTemplateSelected();" name="ddlLetterTemplate">';
            ListPdfTemplates.forEach(function(tmp) {
                strHtml += '<option value="' + tmp.Id + '">' + tmp.TemplateName + '</option>';
            });
            strHtml += '</select>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="width: 100%;">';
            strHtml += '<textarea id="txtLetterText" class="lmFieldText" style="font-family:Helvetica;font-size:Small;" cols="20" rows="2" name="txtLetterText"></textarea>';
            //strHtml += '<textarea id="txtPaymentDetails" class="lmFieldText" style="font-family: Helvetica; font-size: small;" cols="20" rows="2" name="txtPaymentDetails"></textarea>';
            strHtml += '</div>';
            strHtml += '<br />';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label id="lblIncludeItemValues" class="lmFieldText"> Show Item Values </label>';
            strHtml += '<input type="checkbox" id="chkIncludeItemValues" checked/>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label id="lblIncludeTotals" class="lmFieldText"> Show Totals </label>';
            strHtml += '<input type="checkbox" id="chkIncludeTotals" checked/>';
            strHtml += '</div>';
            //The following section is included for full support of multi site: This is still under development
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label id="lblIncludeTotals" class="lmFieldText"> Include all sites </label>';
            strHtml += '<input type="checkbox" id="chkIncludeAllSites" disabled="disabled" checked/>';
            strHtml += '</div>';
            strHtml += '<br />';
            strHtml += '<div class="lmField center" style="width:100%;">';
            strHtml += '<label id="lblWarnig" class="lmFieldText"> Please use the Preview button before Generating a PDF. Generating a schedule PDF will lock the quote into Sent status. </label>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="height:30px;width:100%;">';
            strHtml += '<input id="btnPreviewPdf" class="button-thin padded float-left" type="submit" style="margin-right: 10px;" onclick="LoadPDF(true); return false;" value="Preview PDF" name="btnPreviewPdf">';
            strHtml += '<input id="btnGeneratePdf" class="button-thin padded float-right" type="submit" style="margin-right: 10px;" onclick="LoadPDF(false); return false;" value="Generate PDF" name="btnGeneratePdf">';
            strHtml += '</div>';
            strHtml += '</div>';
            strHtml += '<br/>';
            //Update the panel
            panel.innerHTML = strHtml;
            $("#ddlLetterType").val('3');
            //Set the text to the first selected item 
            LetterTemplateSelected();
            isPdfLoaded = true;
            SmsNotSent();
            if (isResend) {
                // if quote is being re-sent colapse panel and hide unneeded content as well as preview the pdf
                document.getElementById('hideWhenResend').className = "hidden";
                LoadPDF(true);
                ToggleDsiplay(0);
            }
        } catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display PDF settings - ' + err.toString());
        }
    }
}
//Async callback to generate the html fot the email settings panel
function PopulateEmailPanel(results) {
    var panel = document.getElementById('pnlPDFBody_1');
    if (!results || results.length === 0) {
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
    } else {
        try {
            ListEmailTemplates = results;
            //Generate the html for the div
            var strHtml = '';
            strHtml += '<div class="lmEdit" style="width:100%;">';
            strHtml += '<div id="divEmailOptions" style="width: 100%; display: block;">';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlEmailTemplate"> Email Text Template </label>';
            strHtml += '<select id="ddlEmailTemplate" class="lmFieldControl" onchange="ShowEmailTemplate();" name="ddlEmailTemplate">';
            ListEmailTemplates.forEach(function (tmp) {
                strHtml += '<option value="' + tmp.Id + '">' + tmp.TemplateName + '</option>';
            });
            strHtml += '</select>';
            strHtml += '<label class="lmFieldLabel" for="txtEmailSubject"> Subject </label>';
            strHtml += '<input id="txtEmailSubject" class="lmFieldControl" type="text" style="width:54%;" name="txtEmailSubject">';
            strHtml += '<label class="lmFieldLabel" for="txtEmailCC"> CC </label>';
            strHtml += '<input id="txtEmailCC" class="lmFieldControl" type="text" style="width:54%;" name="txtEmailCC">';
            strHtml += '<textarea id="txtEmailText" class="lmFieldText" style="font-family:Helvetica;font-size:Small;" cols="20" rows="2" name="txtEmailText"></textarea>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<div id="pnlAttachments" style="width:100%;">';
            strHtml += '<table id="grdAttachments" class="lmGrid" style="width:94%;">';
            strHtml += '<tbody>';
            strHtml += '</table>';
            strHtml += '</div>';
            strHtml += '<div style="width:100%;">';
            strHtml += '<input id="fuAttachment" type="file" name="fuAttachment" style = "width:60%;">';
            strHtml += '<input id="btnUpload" class="button-thin float-right" type="submit" style="margin-right: 10px;width:128px;" onclick="AddAttachment(); return false;" value="Attach" name="btnUpload">';
            strHtml += '<div class="warning-text" width:100%> Maximum file size is 8 MB. </div>';
            strHtml += '<div id="alertAttach" class="hidden" width:100%>Uploading large files may result in a slow upload time depending on your connection.</div>';
            strHtml += '</div>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="text-align:center; height:5px;width:100%;">';
            strHtml += '<input id="btnAttachDoc" class="button-thin float-right" type="submit" style="margin-right: 10px;margin-top:15px;" return false;" value="Attach Docs" name="btnAttachDoc" onclick="AttachDocDialogForQuote();">';
            strHtml += '</div>';
            strHtml += '<br>';
            strHtml += '<div class="lmField" style="height:30px;width:100%;margin-top:20px;">';
            strHtml += '<div id="alertSms" class="float-left" width:50%> SMS will be sent </div>';
            strHtml += '<input id="btnSendEmail" class="button-thin float-right" type="submit" style="margin-right: 10px;width:129.833px;" onclick="SendEmail(); return false;" value="Send Email" name="btnSendEmail" width:40%>';
            strHtml += '</div>';
            strHtml += '</div>';
            strHtml += '</div>';
            strHtml += '</div>';

            //Update the panel
            panel.innerHTML = strHtml;
            //Colapse the panel only if quote is not being resent
            if (!isResend) {
                ToggleDsiplay(1);
            }
            //show any attachments that are in the session
            ShowAttachments();
            ShowEmailTemplate();
            isEmailLoaded = true;
            SmsNotSent();
        }
        catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display Email settings - ' + err.toString());
        }
    }
}

// checks if an sms should not be sent due to length or client not wanting sms
function SmsNotSent() {
    if (isEmailLoaded && isPdfLoaded) {
        var list = document.getElementById('ddlQuoteContact');
        var SelectedIndex;

        if (list == null) {
            SelectedIndex = 0;
        } else {
            SelectedIndex = list.selectedIndex;
        }
        var s = document.getElementById('txtSmsText');

        if (ListContacts.length === 1) {
            displaySmsMessageSent(s, SelectedIndex);
        } else
        {
            displaySmsMessageSent(s, SelectedIndex - 1);
        }
    }
}

function displaySmsMessageSent(s, index) {
    smsTextConversion(s, index);
    document.getElementById('alertSms').className = "float-left";
    if (s == null || index < 0 || ListContacts.length === 0 || !ListContacts[index].SendSmsMessage) {
        document.getElementById('alertSms').innerText = "SMS will not be sent";
    } else if (!ValidateConvertedSmsLength(s.value)) {
        document.getElementById('alertSms').innerText = "SMS message is too long";
    } else {
        document.getElementById('alertSms').innerText = "SMS will be sent";
    }
}

// checks if text in sms template is over 160 characters (1 message).
function contentTooLong() {
    var s = document.getElementById('txtSmsText');
    if (!ValidateConvertedSmsLength(s.value)) {
        document.getElementById('alertSmsLength').className = "center";
    } else {
        document.getElementById('alertSmsLength').className = "hidden";
    }
}

// function that replaces the special strings in an sms template
function smsTextConversion(s, index) {
    if (isSmsLoaded) {
        s.value = SmsPatternPassing(s.value, ListContacts[index].SurnameAndTitle, ListContacts[index].Email, "Quote", "<OFFICE_TEL>", "<OFFICE_MOBILE>", "<COMPANY_CONTACT>", "<COMPANY_NAME>");
        contentTooLong();
    }
}

// DGSAP-852 - Async callback to generate the html fot the sms settings panel
function PopulateSmsPanel(results) {
    var panel = document.getElementById('pnlPDFBody_2');
    if (!results || results.length === 0) {
        DisplayMessage(panel.id, 'Error: Could not find any SMS templates');
    } else {
        try {
            ListSmsTemplates = results;
            //Generate the html for the div
            var strHtml = '';
            strHtml += '<div class="lmEdit" style="width:100%;">';
            strHtml += '<label class="warning-text" for="ddlSmsTemplate" style="width:100%;"> Maximum length of SMS messages is 160 characters. </label>';
            strHtml += '<div class="lmEdit" style="width:100%;">';
            strHtml += '<div id="divSmsOptions" style="width: 100%; display: block;">';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlSmsTemplate"> Sms Text Template </label>';
            strHtml += '<select id="ddlSmsTemplate" class="lmFieldControl" onchange="ShowSmsTemplate();" name="ddlSmsTemplate">';
            ListSmsTemplates.forEach(function (tmp) {
                strHtml += '<option value="' + tmp.Id + '">' + tmp.TemplateName + '</option>';
            });
            strHtml += '</select>';
            strHtml += '<textarea id="txtSmsText" class="lmFieldText" style="font-family:Helvetica;font-size:Small;" cols="20" rows="2" name="txtSmsText" oninput="contentTooLong()"></textarea>';
            strHtml += '<div class="lmField" style="text-align:center; height:30px;width:100%;">';
            strHtml += '<label id="alertSmsLength" class="hidden"> SMS is too long </label> </div>';
            strHtml += '</div>';
            strHtml += '</div>';
            strHtml += '</div>';

            isSmsLoaded = true;
            //Update the panel
            panel.innerHTML = strHtml;
            //Colapse the panel
            ToggleDsiplay(2);
            ShowSmsTemplate();
        }
        catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display Email settings - ' + err.toString());
        }
    }
}

//Async callback to display a PDF
function DisplayPDF(result) {
    ShowGenerationTimer(false);
    if (!result) {
        DisplayMessage('pnlPDFError', 'Error: Unable to display PDF, generation failed.');
    } else {
        RenderPDF();
    }
}

//      Required! - Attach the click event handler for the Download button
function HookPdfToDownload() {
    $("#previewToolbar").css('visibility', 'visible');
    var openPdf = document.getElementById('btnDownload');

    openPdf.onclick = function () {
        window.open($("#pdfPreview").contents().find("#hiddenGeneratedPdfUrl").val());
    }
}

function RenderPDF() {
    $('#pdfPreview').attr('src', 'Controls/PDFViewer.aspx?v=' + Math.floor((Math.random() * 1000000) + 1));
    HookPdfToDownload();

    //Update the status of the Invoice to sent if required
    if (!IsPreview) {
        /*
        strHtml += '<option value="0">Letter</option>';
        strHtml += '<option value="1">Schedule</option>';
        strHtml += '<option value="2">Letter & Schedule</option>';
        */
        //Set the status of the quote dependant on the type of document created
        //Note once an quote is set to a status it cannot be set to a lower status unless that status is paid. 
        var ddl = document.getElementById('ddlLetterType');
        var itemType = ddl.options[ddl.selectedIndex].value;
        //Only set the status if a schedule has been successfully created
        if (itemType > 0) {
            if (SetQuoteStatus(ObjQuote.Id, 1)) {
                var e = document.getElementById('btnPreviewPdf');
                if (e) {
                    e.disabled = true;
                }

                var c = document.getElementById('btnGeneratePdf');
                if (c) {
                    c.disabled = true;
                }
            }
        }
    }
}
//REQUIRED! - Function to toggle the display of the required body panel
function ToggleDsiplay(elementIndex) {
    //NOTE: The element index is appended to the end of the ID in the aspx file This function depends on the nameing convention
    var bodyElement = document.getElementById('pnlPDFBody_' + elementIndex.toString());
    var toggleElement = document.getElementById('lblHeaderToggle_' + elementIndex.toString());
    if (ArrIsPanelShown[elementIndex]) {
        toggleElement.textContent = '+';
        $(bodyElement).slideUp();
    } else {
        toggleElement.textContent = '-';
        $(bodyElement).slideDown();
    }
    ArrIsPanelShown[elementIndex] = !ArrIsPanelShown[elementIndex];
}

function ToggleEmailOptionDisplay(index) {
    ToggleDsiplay(index);
    $('#Dialog_Content').animate({ scrollTop: "900px" });
}

function LoadDefaultCompanyDocuments() {
    var DocsIdList = Ajax_CallService(
            'Services/Web/Report.asmx',
            'AddCompanyDocumentToAttachment',
            {},
            false
        );
    DocsIdList.forEach(function (Id) {
        SetSelected({ id: Id, type: 4, checked: true }, true);
    });
    
}

//REQUIRED!
function LoadAttachments() {
    lm_Attachments = new Array();

    lm_FileNames.forEach(function (fn) {
        lm_Attachments.push({ id: lm_FileNames.indexOf(fn), type: -1, fileName: fn });
    });
    
    var Docs = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetTempDocuments',
        {
            ItemID: ObjQuote.Id
        },
        false);
    Docs.forEach(function (file) {
        lm_Attachments.push({ id: file.Id, type: file.DocumentTypeID, fileName: file.FileName });
    });


    ShowAttachments();
}
//REQUIRED!
function AddAttachment() {
    var Upload = document.getElementById('fuAttachment');
    if (Upload.files.length > 0) {
        if (Upload.value.indexOf('.exe') !== -1
            || Upload.value.indexOf('.jar') !== -1
            || Upload.value.indexOf('.bat') !== -1
            || Upload.value.indexOf('.bash') !== -1
            || Upload.value.indexOf('.msi') !== -1
            || Upload.value.indexOf('.dll') !== -1) {
            alert("Invalid file type");
            return false;
        }

        document.getElementById('alertAttach').className = "warning-text";
        var file = Upload.files[0];

        var fr = new FileReader();
        fr.onload = function (event) {
            var result = event.target.result;
            var base64 = result.substring(result.indexOf("base64,") + 7);

            var Doc = Ajax_CallService(
                'Services/Web/Report.asmx',
                'AddTempDocument',
                {
                    ItemID: ObjQuote.Id,
                    Description: "Temporary Document For Email Attachment",
                    ContentType: file.type,
                    ByteString: base64,
                    FilePath: Upload.value
                },
                false);
            console.log(file.type);
            console.log(file.type);
            SetSelected({ id: Doc.Id, type: Doc.DocumentTypeID }, true);
            LoadAttachments();
            Upload.value = "";
        };
        fr.readAsDataURL(file);
    } else {
        alert('No File Selected');
    }
}

//REQUIRED!
function ShowAttachments() {

    if (!lm_Attachments)
        lm_Attachments = new Array();

    var grid = document.getElementById('grdAttachments');

    if (grid != null) {
        for (var i = grid.rows.length; i > 0; i--) {
            grid.deleteRow(i - 1);
        }

        lm_Attachments.forEach(function (attachment) {
            var newRow = grid.insertRow(-1);

            newRow.className = 'GridItem';

            var nameCell = newRow.insertCell(0);

            nameCell.innerHTML = attachment.fileName;

            switch (attachment.type) {
                case -1:
                    newRow.className += ' GridItem_Report';
                    break;
                case 0:
                    newRow.className += ' GridItem_Global';
                    break;
                case 1:
                    newRow.className += ' GridItem_Client';
                    break;
                case 2:
                    newRow.className += ' GridItem_Temp';
                    break;
            }

            var checkCell = newRow.insertCell(1);

            checkCell.style.textAlign = 'center';

            var cb = document.createElement('input');
            cb.type = 'checkbox';
            cb.id = 'attachmentCheckbox_' + attachment.type + '_' + attachment.id;
            cb.disabled = attachment.type === -1;
            checkCell.appendChild(cb);

            var checked = false;
            lm_SelectedAttachments.forEach(function (sel) {
                if (sel.id === attachment.id
                    && sel.type === attachment.type)
                    checked = true;
            });
            if (attachment.type === -1) {
                checked = true;
            }
            cb.checked = true;

            cb.addEventListener('change', function () {
                var check = document.getElementById(cb.id);
                SetSelected({ id: attachment.id, type: attachment.type }, check.checked);
            }, false);
        });
    }
    console.log(lm_Attachments);
}
//REQUIRED!
function SetSelected(attachment, selected) {
    if (selected) {
        lm_SelectedAttachments.push({ id: attachment.id, type: attachment.type });
        console.log("Selected");
    } else {
        if (lm_SelectedAttachments.length > 0) {
            var index = 0;
            for (var i = 0; i < lm_SelectedAttachments.length; i++) {
                if (lm_SelectedAttachments[i].id === attachment.id
                    && lm_SelectedAttachments[i].type === attachment.type) {
                    index = i;
                }
            }
            console.log("Removed");
            var Doc = Ajax_CallService(
                'Services/Web/Report.asmx',
                'RemoveTempDocumentsById',
                {
                    ItemID: attachment.id,
                },
                false);
            lm_SelectedAttachments.splice(index, 1);
        }
    }
    LoadAttachments();
}

//Async callback to set the client contacts list and the call
//the load process for the PDF templates
function SetClientContacts(results) {
    if (!results || results.length === 0) {
        DisplayMessage('pnlPDFBody_0', 'ERROR: Arbpro service did not return a valid list of contacts.');
    } else {
        //The client contact list has been populated correctly now the PDF templates can be loaded
        ListContacts = results;
        GetPDFTemplates();
    }
}

//  Function to handle the change of the selected contact
function QuoteContactChanged() {
    var list = document.getElementById('ddlQuoteContact');
    var SelectedIndex;


    if (list == null) {
        SelectedIndex = 0;
    }
    else {
        SelectedIndex = list.selectedIndex;
    }
    if (SelectedIndex === 0) {
        var e = document.getElementById('btnPreviewPdf');
        if (e) {
            e.disabled = true;
        }

        var c = document.getElementById('btnGeneratePdf');
        if (c) {
            c.disabled = true;
        }

        var d = document.getElementById('btnSendEmail');
        if (d) {
            d.disabled = true;
        }
    }
    else {
        var a = document.getElementById('btnPreviewPdf');
        if (a) {
            a.disabled = false;
        }

        var b = document.getElementById('btnGeneratePdf');
        if (b) {
            b.disabled = false;
        }

        var f = document.getElementById('btnSendEmail');
        if (f) {
            f.disabled = false;
        }
    }
    SmsNotSent();
    ShowSmsTemplate();
}

//Function to show the letter templates for the emails
function ShowLetterTemplate() {
    var list = document.getElementById('ddlLetterType');
    var SelectedIndex;
    // if the letter type dropdown hasn't been constructed yet, default to the 1st item in the list
    if (list == null) {
        SelectedIndex = 0;
    }
    else {
        SelectedIndex = parseInt(list.options[list.selectedIndex].value);
    }

    lm_FileNames = [];

    var reg = document.getElementById('pnlLetterTemplate');

    if (SelectedIndex === 0) {
        if ($(reg) != null) {
            $(reg).slideDown();
        }

        lm_FileNames[0] = 'Quote ' + 'Letter.pdf';
        lm_FileNames.splice(1, 1);
    } else if (SelectedIndex === 1) {
        if ($(reg) != null) {
            $(reg).slideUp();
        }
        lm_FileNames[0] = 'Quote ' + 'Schedule.pdf';
        lm_FileNames.splice(1, 1);
    } else {
        if ($(reg) != null) {
            $(reg).slideDown();
        }
        lm_FileNames[0] = 'Quote ' + 'Letter.pdf';
        lm_FileNames[1] = 'Quote ' + 'Schedule.pdf';
    }
    LoadAttachments();
}

//Function to populate the UI from the seklected template
function ShowEmailTemplate() {
    var ddl = document.getElementById('ddlEmailTemplate');
    var tempId = parseInt(ddl.options[ddl.selectedIndex].value);
    var count = ListEmailTemplates.length;
    var template = null;
    //A for loop is being used to enable the break statement
    for (var index = 0; index < count; index++) {
        template = ListEmailTemplates[index];
        if (template.Id === tempId) {
            document.getElementById('txtEmailSubject').value = template.Salutation;
            document.getElementById('txtEmailText').value = template.BodyText;
            break;
        }
    }
}

// DGSAP-852 - Function to populate the UI from the seklected template
function ShowSmsTemplate() {
    var ddl = document.getElementById('ddlSmsTemplate');
    var tempId = parseInt(ddl.options[ddl.selectedIndex].value);
    var count = ListSmsTemplates.length;
    var template = null;
    //A for loop is being used to enable the break statement
    for (var index = 0; index < count; index++) {
        template = ListSmsTemplates[index];
        if (template.Id === tempId) {
            document.getElementById('txtSmsText').value = template.BodyText;
            SmsNotSent();
            break;
        }
    }
}

function GetSmtpSettings() {
    EmailSmtpObj = [];

    EmailSmtpObj = Ajax_CallService(
        '../Services/Web/Company.asmx',
        'GetEmailSettings',
        {},
        false
    );
}

function DisplayEmailMsg(result) {
    var openMarketing = false;      // bool determining if marketing dialogue should be opened
    var message = '';
    if (result) {
        GetSmtpSettings();
        if (!isResend) {
            if (SetQuoteStatus(ObjQuote.Id, 1)) {
                if (EmailSmtpObj.UseSmtp) {
                    message = 'Emails and Text messages (if applicable) have been generated and queued for dispatch. <br/> Please check your email client for confirmation';
                } else {
                    message = 'Emails and Text messages (if applicable) have been generated and queued for dispatch.<br/> <a href="/Company.aspx"> See here - under the emails panel for all sent emails. </a>';
                }
                openMarketing = true;
            } else {
                message = 'Error: Emails has been sent. Quote status update failed';
            }
        } else {
            // if quote is being re-sent display the adequate message
            if (EmailSmtpObj.UseSmtp) {
                message = 'Emails have been queued for resend.<br/> Please check your email client for confirmation.';
            } else {
                message = 'Emails have been queued for resend.<br/> <a href="/Company.aspx"> See here - under the emails panel for all sent emails. </a>';
            }
        }
    }
    else {
        message = 'Error: emails not sent';
    }

    Dialog_OpenPage(
   'Email',
   'Dialogs/Alert.aspx',
   'content',
   500,
   150,
   true,
   {
       'Ok': {
           text: 'Ok',
           class: 'ui-dialog-button',
           click: function () {
               $(this).dialog('close');
               if (ObjCompany.DisplayMarketing && openMarketing && ObjQuote.PointOfContact.IncludeInMarketing) {
                   ClientMarketingNotAccepted();
               }
           }
       }
   },
   'Content/js/AlertBox.js?V=3.23.5',
   function () {
       AlertBox_Show(message);
   });

    //ShowGenerationTimer(false);
    //if (!result) {
    //    DisplayMessage('pnlPDFError', 'Error: Unable to display PDF, generation failed. Emails have not been sent');
    //} else {
    //    RenderPDF();
    //    alert('Emails have been generated and queued for dispatch.');
    //}
}

//Async service call to send an quote item by email. This function creates history items for the generated documents
function SendEmail() {
    try {
        //Display the wait timer
        ShowGenerationTimer(true);

        ObjQuote.JobItems = [];
        ObjQuote.QuotedItems = [];
        ObjQuote.History = [];

        var ddlContact = document.getElementById('ddlQuoteContact');
        ObjQuote.PointOfContact.Id = ddlContact.options[ddlContact.selectedIndex].value;

        //Get the settings to be applied to the email
        var emailSettings = GetEmailSettings();
        var pdfSettings = GetPdfSettings();

        ColapsePanels(0);
        ColapsePanels(1);
        ColapsePanels(2);

        ObjQuote.DateCreated = toDateFromJson(ObjQuote.DateCreated);
        ObjQuote.DateSent = toDateFromJson(ObjQuote.DateSent);

        try {
            ObjQuote.InvoicedItems = [];
            Ajax_CallService(
                'Services/Web/Report.asmx',
                'UpdateQuoteAndEmailReport',
                {
                    obj: ObjQuote,
                    pdfSettings: pdfSettings,
                    emailSettings: emailSettings
                },
                true,
                DisplayEmailMsg
            );

        } catch (err) {
            DisplayMessage('pnlPDFError', 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
            //Ensure that the processing wait timer is not being displayed
            ShowGenerationTimer(false);
        }
    }

    catch (er) {
        DisplayMessage('pnlPDFError', 'Error: Unable to send email - ' + er.toString());
        //Ensure that the processing wait timer is not being displayed
        ShowGenerationTimer(false);
    }
}

// Function to colapse all of the active panels
function ColapsePanels(elementIndex) {
    var bodyElement = document.getElementById('pnlPDFBody_' + elementIndex.toString());
    var toggleElement = document.getElementById('lblHeaderToggle_' + elementIndex.toString());
    if (ArrIsPanelShown[elementIndex]) {
        toggleElement.textContent = '+';
        $(bodyElement).slideUp();
    }

    ArrIsPanelShown[elementIndex] = !ArrIsPanelShown[elementIndex];
}

//Function to display the text of the selected template
function LetterTemplateSelected() {
    var ddl = document.getElementById('ddlLetterTemplate');
    var tempId = parseInt(ddl.options[ddl.selectedIndex].value);
    var count = ListPdfTemplates.length;
    var template = null;
    //A for loop is being used to enable the break statement
    for (var index = 0; index < count; index++) {
        template = ListPdfTemplates[index];
        if (template.Id === tempId) {
            document.getElementById('txtLetterText').value = template.BodyText;
            //document.getElementById('txtPaymentDetails').value = template.AdditionalText;
            break;
        }
    }
    //  DGSAP-713
    //  Update the attachments displayed in the letter section
    ShowLetterTemplate();
}

function ShowGenerationTimer(show) {
    if (show) {
        document.getElementById('pnlLmPdfView').className = 'hidden';
        document.getElementById('pnlLmProcessing').className = 'waitTimer';
    } else {
        document.getElementById('pnlLmProcessing').className = 'hidden';
        document.getElementById('pnlLmPdfView').className = 'lmPnlLrg';
    }
}

//Helper function to get the settings for Pdf generation
function GetPdfSettings(isPreview, isCopy) {
    var pdfSettings = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetQuotePdfSetting',
        {},
        false);
    pdfSettings.Id = ObjQuote.Id;
    pdfSettings.IsCopy = isCopy;
    pdfSettings.IsPreview = isPreview;
    var ddl = document.getElementById('ddlLetterType');
    pdfSettings.GenerationEnum = parseInt(ddl.options[ddl.selectedIndex].value);
    ddl = document.getElementById('ddlLetterTemplate');
    pdfSettings.TemplateId = parseInt(ddl.options[ddl.selectedIndex].value);
    pdfSettings.TextBody = document.getElementById('txtLetterText').value;
    pdfSettings.ShowTotals = document.getElementById('chkIncludeTotals').checked;
    pdfSettings.ShownItemValues = document.getElementById('chkIncludeItemValues').checked;
    pdfSettings.IncludeAllSites = document.getElementById('chkIncludeAllSites').checked;

    return pdfSettings;
}

//Helper function to get the settings for sending an email
function GetEmailSettings() {
    /*
    List<int> _Recipients = null;
        string _Subject = "";
        string _BodyText = "";
        string _FromAddress = "";
    */
    var CCEmails = document.getElementById('txtEmailCC').value
    var emails = CCEmails.split(';');

    var invalidEmails = [];
   for (i = 0; i < emails.length; i++) {
       if ((!validateEmail(emails[i].trim())) && emails[i] != "") {
            invalidEmails.push(emails[i].trim())
                  
        }
    }
   if (invalidEmails.length > 0) {
        alert("The following emails are invalid:" + invalidEmails);
    }
    
   var CCEmailList = $.unique(emails);
   
    
    var emailSettings = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetEmailSetting',
        {},
        false);
    emailSettings.Id = ObjQuote.Id;
    emailSettings.CCRecipients = CCEmailList;
    emailSettings.Recipients = new Array();
    emailSettings.Recipients.push(ObjQuote.PointOfContact.Id);
    emailSettings.Subject = document.getElementById('txtEmailSubject').value;
    emailSettings.BodyText = document.getElementById('txtEmailText').value;
    emailSettings.FromAdress = ''; //For quote generation this is set later
    // DGSAP-852
    if (!isSmsLoaded) {
        emailSettings.SmsText = "";
    } else {
        emailSettings.SmsText = document.getElementById('txtSmsText').value;
    }

    return emailSettings;
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

