﻿function Contact_Load() {
    AutoComplete('txtContactTitle', 'ListTitles');
    AutoComplete('txtContactTown', 'ListTowns');
    AutoComplete('txtContactCounty', 'ListCounties');
}

function SaveContactObject() {
    var Client = GetUser(Page_strUserEmail);
    var ContactID = document.getElementById('hfContactID').value;
    var ContactType = document.getElementById('hfContactType').value;
    var ContactClientID = document.getElementById('hfClientID').value;
    var ContactTitle = document.getElementById('txtContactTitle').value;
    var ContactForename = document.getElementById('txtContactForename').value;
    var ContactSurname = document.getElementById('txtContactSurname').value;
    var ContactTelephone = document.getElementById('txtContactTelephone').value;
    var ContactMobile = document.getElementById('txtContactMobile').value;
    var ContactEmail = document.getElementById('txtContactEmail').value;
    var ContactStreetNumber = document.getElementById('txtContactStreetNumber').value;
    var ContactAddress1 = document.getElementById('txtContactAddress1').value;
    var ContactAddress2 = document.getElementById('txtContactAddress2').value;
    var ContactTown = document.getElementById('txtContactTown').value;
    var ContactCounty = document.getElementById('txtContactCounty').value;
    var ContactPostCode = document.getElementById('txtContactPostCode').value;
    //DGSAP-855
    var ContactSendSms = document.getElementById('chkSendSmsContact').checked;

    //debugger;
    var contactObject = {
        ContactType: parseInt(ContactType),
        Id: ContactID,
        ClientID: ContactClientID,
        Title: ContactTitle,
        Forename: ContactForename,
        Surname: ContactSurname,
        Telephone: ContactTelephone,
        Mobile: ContactMobile,
        Email: ContactEmail,
        StreetNumber: ContactStreetNumber,
        Address1: ContactAddress1,
        Address2: ContactAddress2,
        Town: ContactTown,
        County: ContactCounty,
        PostCode: ContactPostCode,
        //DGSAP-855
        SendSmsMessage: ContactSendSms
    }

    var r = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'SaveContactObject',
    {
        obj: contactObject
    },
        false);

    //var Result = Ajax_CallService(
    //    'Services/Web/ClientData.asmx',
    //    'SaveContact',
    //    {
    //        ContactID: ContactID,
    //        ClientID: ContactClientID,
    //        Title: ContactTitle,
    //        Forename: ContactForename,
    //        Surname: ContactSurname,
    //        Telephone: ContactTelephone,
    //        Mobile: ContactMobile,
    //        Email: ContactEmail,
    //        StreetNumber: ContactStreetNumber,
    //        Address1: ContactAddress1,    
    //        Address2: ContactAddress2,
    //        Town: ContactTown,
    //        County: ContactCounty,
    //        PostCode: ContactPostCode,
    //        //DGSAP-855
    //        SendSms: ContactSendSms
    //    },
    //    false);

    return r;
}