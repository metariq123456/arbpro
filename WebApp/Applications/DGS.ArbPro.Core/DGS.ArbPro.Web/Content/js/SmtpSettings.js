﻿var EmailSmtpObj;
var UserSmtpObj;

function SmtpSettings_Load() {
    GetSmtpSettings();
    PopulateFields();
}


function SmtpSave() {
    SmtpUserSave();
    SmtpEmailSave();
    if (SmtpUserSave && SmtpEmailSave) {
        return true;
    }
    return false;
}

function SmtpUserSave() {
    var retVal = false;
    try {
        UserSmtpObj.Username = document.getElementById('txtUserName').value;
        UserSmtpObj.Password = document.getElementById('txtPassword').value;

        retVal = Ajax_CallService(
                'Services/Web/Company.asmx',
                'SaveUserSmtpSettings',
                {
                    userObj: UserSmtpObj
                },
                false
            );
    }
    catch (err) {
        alert('Error: Unable to update User Settings information. If this error continues please contact support');
    }

    return retVal;
}

function SmtpEmailSave() {
    var retVal = false;
    try {
        EmailSmtpObj.UseSmtp = document.getElementById('chkUseSmtp').checked;
        EmailSmtpObj.HostAddress = document.getElementById('txtHostAddress').value;
        EmailSmtpObj.Port = document.getElementById('txtPortNumber').value;
        EmailSmtpObj.EnableSSL = document.getElementById('chkEnableSsl').checked;
        retVal = Ajax_CallService(
            'Services/Web/Company.asmx',
            'SaveEmailSmtpSettings',
            {
                emailObj: EmailSmtpObj
            },
            false
        );
    } catch (err) {
        alert('Error: Unable to update Email Settings information. If this error continues please contact support');
    }

    return retVal;
}

function PopulateFields() {

    document.getElementById('txtHostAddress').value = EmailSmtpObj.HostAddress;
    document.getElementById('txtPortNumber').value = EmailSmtpObj.Port;
    document.getElementById('chkEnableSsl').checked = EmailSmtpObj.EnableSSL;
    document.getElementById('chkUseSmtp').checked = EmailSmtpObj.UseSmtp;

    document.getElementById('txtUserName').value = UserSmtpObj.Username;
    document.getElementById('txtPassword').value = UserSmtpObj.Password;

    checkLoaded();
}

function checkLoaded() {
    var host = document.getElementById('txtHostAddress');
    var port = document.getElementById('txtPortNumber');
    var ssl = document.getElementById('chkEnableSsl');
    host.disabled = true;
    port.disabled = true;
    ssl.disabled = true;

    if (host.value === "smtp.gmail.com" && port.value === "587") {
        document.getElementById('drpHostAddress').value = 0;

    } else if (host.value === "smtp.live.com" && port.value === "587") {
        document.getElementById('drpHostAddress').value = 1;

    } else if (host.value === "smtp.mail.yahoo.com" && port.value === "587") {
        document.getElementById('drpHostAddress').value = 2;

    } else if (host.value === "smtpout.europe.secureserver.net" && port.value === "80") {
        document.getElementById('drpHostAddress').value = 5;

    } else if (port.value == "25") {
        document.getElementById('drpHostAddress').value = 3;
        host.disabled = false;
        port.disabled = false;
        ssl.disabled = false;
    } else {
        document.getElementById('drpHostAddress').value = 4;
        host.disabled = false;
        port.disabled = false;
        ssl.disabled = false;
    }
}

function checkSelected() {
    var index = document.getElementById('drpHostAddress').value;
    var host = document.getElementById('txtHostAddress');
    var port = document.getElementById('txtPortNumber');
    var ssl = document.getElementById('chkEnableSsl');
    port.value = "587";
    host.disabled = true;
    port.disabled = true;
    ssl.disabled = true;
    ssl.checked = true;
    if (index == 0) {
        host.value = "smtp.gmail.com";
    } else if (index == 1) {
        host.value = "smtp.live.com";
    } else if (index == 2) {
        host.value = "smtp.mail.yahoo.com";
    } else if (index == 3) {
        host.value = "";
        port.value = "25";
        host.disabled = false;
        port.disabled = false;
        ssl.disabled = false;
    } else if (index == 4) {
        host.value = "";
        port.value = "";
        host.disabled = false;
        port.disabled = false;
        ssl.disabled = false;
        ssl.checked = false;
    } else if (index == 5) {
        host.value = "smtpout.europe.secureserver.net";
        port.value = "80";
        ssl.checked = false;
    }
}

function GetSmtpSettings() {
    EmailSmtpObj = [];
    UserSmtpObj = [];

    EmailSmtpObj = Ajax_CallService(
        '../Services/Web/Company.asmx',
        'GetEmailSettings',
        {},
        false
    );
    UserSmtpObj = Ajax_CallService(
        '../Services/Web/Company.asmx',
        'GetUserSettings',
        {},
        false
    );
}

// function to test the smtp server connection with the user's details
function TestConnection() {
    // display the wait timer
    document.getElementById('pnlSmtpProcessing').className = '';
    

    // get the latest smtp settings
    GetSmtpSettings();

    try {
        // try to send an email
        Ajax_CallService(
            'Services/Web/Report.asmx',
            'TestSmtpConnection',
            {
                usrEmail: UserSmtpObj.Username,
                emailSettings: EmailSmtpObj,
                usrSettings: UserSmtpObj
            },
            true,
            testConnectionCallback
        );
    } catch (err) {
        document.getElementById('pnlSmtpProcessing').className = 'hidden';
        DisplayMessage('pnlSmtpError', 'There was a problem connecting to the server - ' + err.toString());
    }

}

// the result from the TestConnection() function is passed through to here in order to display the success or fail messages
function testConnectionCallback(result) {
    document.getElementById('pnlSmtpProcessing').className = 'hidden';
    if (result) {
        DisplayMessage('pnlSmtpError', 'The test email has been sent successfully to the address specified below. You may click the "Save" or "Close" button in order to close the window.', 'red');
    } else {
        DisplayMessage('pnlSmtpError', 'There was a problem sending the test email to the below specified address. Please check your settings or the validity of the address.', 'red');
    }
}

// function to validate whether there are empty fields upon testing the connection and to check if the Use Own Server checkbox has been selected
function validateSettings() {
    if (EmailSmtpObj.UseSmtp && EmailSmtpObj.Port && EmailSmtpObj.Port.length > 0 && UserSmtpObj.Username.length > 0 && UserSmtpObj.Password.length > 0) {
        return true;
    } else if (!EmailSmtpObj.UseSmtp) {
        DisplayMessage('pnlSmtpError', 'Please ensure that you have selected "Use Own Server".', 'red');
    } else {
        DisplayMessage('pnlSmtpError', 'Please ensure that the following fields are not empty: Server Name, Port, Email Address and Password', 'red');
    }
    document.getElementById('pnlSmtpProcessing').className = 'hidden';
    return false;
}

// connect function to save, validate settings and test the connection - it is called upon the Test Connection button press
function Connect() {
    var save = SmtpSave();
    var validate = validateSettings();
    if (save && validate) {
        TestConnection();
    }
}

var Watcher = {
    timeout: null,
    currentValue: '',
    watchForChange: function (el) {
        alert('watching');
        this.currentValue = el.value;
        this.timeout = setTimeout(function () {
            Watcher.watch(el);
        }, 200);
    },
    watch: function (el) {
        if (el.value != this.currentValue) {
            this.changed(el);
        }
        this.timeout = setTimeout(function () {
            Watcher.watch(el);
        }, 200);
    },
    cancelWatchForChange: function () {
        clearTimeout(this.timeout);
        this.timeout = null;
    },
    changed: function (el) {
        this.currentValue = el.value;
        Changed(true);
    }
}

function Changed(bChanged) {
    var Button = $('#btnSave');
    if (bChanged) {
        Button.attr('class', 'button-alert float-right padded');
    } else {
        Button.attr('class', 'button-basic float-right padded');
    }
}