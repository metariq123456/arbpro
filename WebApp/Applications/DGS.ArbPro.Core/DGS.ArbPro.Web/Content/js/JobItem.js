﻿var Stage = 'Quote';
var StageID = -1;

function JobItem_PageLoad() {
    Stage = document.getElementById('hfStage').value;
    StageID = document.getElementById('hfStageID').value;
    UpdateList();
    $('input[name=radioItems][value=-1]').prop('checked', true);

    AutoComplete('txtNewItemTreeName', 'ListTreeNames');
    AutoComplete('txtNewItemDescription', 'ListWorkDescriptions');
}

function UpdateList() {
    var ddl = document.getElementById('ddlAddItemSite');
    var SiteID = ddl.options[ddl.selectedIndex].value;
    var InvoiceQuoteID = document.getElementById('hfInvoiceQuoteID').value;

    var Items = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetJobSite' + Stage + 'ItemList',
        {
            JobSiteID: SiteID,
            QuoteID: InvoiceQuoteID
        },
        false);

    var grid = document.getElementById('grdExistingItems');

    var strHtml = '<table class="searchGrid width-full" style="font-size: 0.75em">';
    Items.forEach(function (i) {
        strHtml = strHtml + '<tr onclick="$(\'input[name=radioItems][value=' + i.Id.toString() + ']\').prop(\'checked\', true);">';
        strHtml = strHtml + '<td width="5%" style="text-align: center;" ><input type="radio" name="radioItems" value="' + i.Id.toString() + '"/></td>';
        strHtml = strHtml + '<td width="10%" style="text-align: center;">' + i.Code + '</td>';
        strHtml = strHtml + '<td width="25%" style="text-align: center;">' + i.Name + '</td>';
        strHtml = strHtml + '<td width="40%" style="text-align: center;">' + i.Description + '</td>';
        strHtml = strHtml + '<td width="20%" style="text-align: center;">' + CurrentCurrency + '' + i.Value.format() + '</td>';
        strHtml = strHtml + '<td width="10%" style="text-align: center;">' + i.VatRate.format() + '%</td>';
        strHtml = strHtml + '</tr>';
    });
    strHtml = strHtml + '</table>';
    grid.innerHTML = strHtml;
}

function AddItemToJob() {
    var ddl = document.getElementById('ddlAddItemSite');
    var SiteID = ddl.options[ddl.selectedIndex].value;
    var ItemID = $('input[name=radioItems]:checked')[0].value;
    var InvoiceQuoteID = document.getElementById('hfInvoiceQuoteID').value;

    if (ItemID > -1) {
        return Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'AddJobSite' + Stage + 'ItemExisting',
                {
                    StageID: StageID,
                    JobSiteID: SiteID,
                    ItemID: ItemID,
                    QuoteID: InvoiceQuoteID
                },
                false);
    } else {
        var JobID = document.getElementById('hfJobID').value;
        var Code = document.getElementById('txtNewItemTreeNo').value;
        var Name = document.getElementById('txtNewItemTreeName').value;
        var Description = document.getElementById('txtNewItemDescription').value;
        var Value = document.getElementById('txtNewItemValue').value;
        var VatRate = document.getElementById('txtNewItemVatRate').value;

        return Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'AddJobSite' + Stage + 'ItemNew',
            {
                JobID: JobID,
                StageID: StageID,
                JobSiteID: SiteID,
                ItemID: ItemID,
                Code: Code,
                Name: Name,
                Description: Description,
                Value: Value,
                VatRate: VatRate
            },
            false);
    }
}