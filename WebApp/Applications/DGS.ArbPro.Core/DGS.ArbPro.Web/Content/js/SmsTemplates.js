﻿var CurList;
var SelectedResultId;
var TemplateType;
var SelectedObj;
var CurMessageType;
var CurObjectType;

// loads sms templates
function Templates_Load(objType, msgType) {
    SelectedResultId = -1;
    CurObjectType = objType;
    CurMessageType = msgType;
    GetTemplateList();
}

// if quote then no payment info field
function DropDownChanged() {
    var templateType = document.getElementById('drpQuoteInvoice').value;
    TemplateType = templateType;
    if (TemplateType === '1') {
        document.getElementById('pnlInvoicePaymentInfo').className = '';
    }
    else {
        document.getElementById('pnlInvoicePaymentInfo').className = 'hidden';
    }
}

// gets the results from the ajax call
function ServiceCallback(results) {
    CurList = results;
    UpdateTable();
}

// updates the table with the templates
function UpdateTable() {
    if (CurList.length < 1) {
        document.getElementById('pnlItemControls').className = 'hidden';
        document.getElementById('pnlNoResults').className = '';
        document.getElementById('pnlTableContainer').innerHTML = '';
    }
    else {
        var listPanel = document.getElementById('pnlTableContainer');
        var strHtml = '<table class="searchGrid width-full white">';

        for (var i = 0; i < CurList.length; i++) {
            if (i === SelectedResultId) {
                strHtml += '<tr class="selected" onclick="ItemSelected(' + i + ');">';
            }
            else {
                strHtml += '<tr onclick="ItemSelected(' + i + ');">';
            }
            strHtml += '<td style="padding-left: 10px;">' + CurList[i].TemplateName + '</td>';
            strHtml += '</tr>';
        }
        strHtml += '</table>';
        listPanel.innerHTML = strHtml;

        document.getElementById('pnlNoResults').className = 'hidden';
    }
}

// displays the selected template
function ItemSelected(id) {
    SelectedResultId = id;
    SelectedObj = CurList[id];
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('pnlComingSoon').className = 'hidden';
    PopulateFields();
    UpdateTable();
}

// gets the sms templates
function GetTemplateList() {
    SelectedResultId = -1;
    try {
        Ajax_CallService(
       'Services/Web/Settings.asmx',
       'GetTemplateListForTypeAndStage',
       {
           Format: CurMessageType,
           tmplType: CurObjectType
       },
       true,
       ServiceCallback
    );
    }
    catch (err) {
    }
}

// populates the fields of the template
function PopulateFields() {
    //Quote
    if (SelectedObj.ObjectType === 0) {
        document.getElementById('drpQuoteInvoice').value = '0';
        document.getElementById('txtTemplateName').value = SelectedObj.TemplateName;
        document.getElementById('txtContentText').value = SelectedObj.BodyText;

        DropDownChanged();
    }
    //Invoice
    else if (SelectedObj.ObjectType === 1) {
        document.getElementById('drpQuoteInvoice').value = '1';
        document.getElementById('txtTemplateName').value = SelectedObj.TemplateName;
        document.getElementById('txtContentText').value = SelectedObj.BodyText;
        DropDownChanged();
    } else if (SelectedObj.ObjectType > 1) {
        document.getElementById('pnlItemControls').className = 'hidden';
        document.getElementById('pnlComingSoon').className = 'center';
    }
    document.getElementById('txtSalutations').className = "hidden";
    document.getElementById('txtPaymentInfo').className = "hidden";
    document.getElementById('lblPaymentInfo').className = "hidden";
    document.getElementById('lblSalutation').textContent = 'Useful tags: ';

    DisplaySmsTags();
}

// gets a new template
function NewTemplate() {
    SelectedResultId = -1;
    UpdateTable();
    try {
        SelectedObj = Ajax_CallService(
            'Services/Web/Settings.asmx',
            'GetEmptyTemplate',
            {},
            false
        );
    } catch (err) {
    }
    document.getElementById('drpQuoteInvoice').value = '0';
    document.getElementById('txtTemplateName').value = '';
    document.getElementById('txtContentText').value = '';
    document.getElementById('pnlNoResults').className = 'hidden';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('pnlComingSoon').className = 'hidden';

    document.getElementById('txtSalutations').className = "hidden";
    document.getElementById('txtPaymentInfo').className = "hidden";
    document.getElementById('lblSalutation').className = "hidden";
    document.getElementById('lblPaymentInfo').className = "hidden";
    DropDownChanged();
    DisplaySmsTags();
}

// saves a new or edited template
function SaveTemplate() {
    SelectedObj.ObjectType = parseInt(document.getElementById('drpQuoteInvoice').value);

    SelectedObj.TemplateName = document.getElementById('txtTemplateName').value;
    SelectedObj.BodyText = document.getElementById('txtContentText').value;
    if (!ValidateSmsTemplateLength(SelectedObj.BodyText)) {
        document.getElementById('lblSmsLength').className = "center";
    } else {
        SelectedObj.MessageType = CurMessageType;
        document.getElementById('lblSmsLength').className = "hidden";

        try {
            Ajax_CallService(
                'Services/Web/Settings.asmx',
                'SaveTemplate',
                { templateObj: SelectedObj },
                false
            );
        } catch (err) {
        }
        document.getElementById('pnlItemControls').className = 'hidden';
        GetTemplateList();
    }
}

// deletes a template
function DeleteTemplate() {
    try {
        var result = Ajax_CallService(
            'Services/Web/Settings.asmx',
            'DeleteTemplate',
            { templateObj: SelectedObj },
            false
        );
        if (!result) {
            DisplayMessage('pnlItemControls', 'There was a problem deleting the template.');
        }
    } catch (err) {
        DisplayMessage('pnlItemControls', 'There was a problem deleting the template - ' + err.toString());
    }
    if (CurList.length < 1) {
        document.getElementById('pnlNoResults').className = '';
    }
    document.getElementById('pnlItemControls').className = 'hidden';
    GetTemplateList();
}

// displays the special tags users can use for string replacement
function DisplaySmsTags() {
    var panel = document.getElementById('pnlSmsTags');
    panel.className = "";


    var strHtml = '';
    strHtml += '<div style="width:100%;">';
    strHtml += '<label style="width:100%;"> &lt;SALUTATION&gt; - client contact surname and title</label><br>';
    strHtml += '<label> &lt;EMAIL&gt; - client contact email</label><br>';
    strHtml += '<label> &lt;EMAIL_TYPE&gt; - type of email sent (Quote/Invoice)</label><br>';
    strHtml += '<label> &lt;TIME&gt; - current time</label><br>';
    strHtml += '<label> &lt;OFFICE_TEL&gt; - office telephone number</label><br>';
    strHtml += '<label> &lt;OFFICE_MOBILE&gt; - office mobile number</label><br>';
    strHtml += '<label> &lt;COMPANY_CONTACT&gt; - default surveyor</label><br>';
    strHtml += '<label> &lt;COMPANY_NAME&gt; - company name</label><br><br>';
    strHtml += '</div>';

    panel.innerHTML = strHtml;
}