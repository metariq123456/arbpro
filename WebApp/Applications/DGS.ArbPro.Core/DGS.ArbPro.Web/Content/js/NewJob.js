﻿/*
    This file holds functions for displaying a dialog to the user to capture
    new Job details. The user does not have to go through the Sales Enquiry process for this dialog to be displayed

    Dependancies:           JobDetails.aspx
    Required Parameters:    The id of the client the Job is for
                        

*/

/*
        SCOPE VARIABLES
*/
var ObjClient = null;       //  The client object for who the Job is being created
var LstSites = [];          //  The list of client sites that are associated with the client
var LstSurveyors = [];      //  The list of staff that have a role of Surveyor


/*
        INITIALISATION AND SERVICE CALL FUNCTIONS
*/

function NewJob_Load(clientId) {
    //  Check the client Id is valid
    if (clientId < 1) {

    } else {
        Surveyors_LoadAsync();
        ClientSite_LoadAsync(clientId);
        Client_LoadAsync(clientId);
    }

}

//  Async service call to load a client object
function Client_LoadAsync(clientId) {
    try {
        Ajax_CallService(
                'Services/Web/ClientData.asmx',
                'GetClient',
                {
                    ClientID: clientId
                },
                true,
                ClientLoad_Callback
                );

    } catch (err) {
        DisplayError('Error: Unable to access Arb Pro service -' + err.toString());
    }
}

//  Async service call to load all client sites for a client
function ClientSite_LoadAsync(clientId) {
    try {
        Ajax_CallService(
                'Services/Web/Clientdata.asmx',
                'GetClientSiteList',
                {
                    ClientID: clientId
                },
                true,
                ClientSiteLoad_Callback
                );

    } catch (err) {
        DisplayError('Error: Unable to access Arb Pro service -' + err.toString());
    }
}

//  Async service call to load all staff objects that have been assigned a role of surveyor
function Surveyors_LoadAsync() {
    try {
        Ajax_CallService(
                'Services/Web/Company.asmx',
                'ListSurveyors',
                {
                },
                true,
                SurveyorsLoad_Callback
                );
        
    } catch (err) {
        DisplayError('Error: Unable to access Arb Pro service -' + err.toString());
    } 
}

function NewJob_Save() {
    //  Get the id of the selected surveyor
    var ddlSurveyor = document.getElementById('ddlEnquirySurveyor');
    var surveyorId = ddlSurveyor.options[ddlSurveyor.selectedIndex].value;
    //  Get the Id of the primary contact for the Job
    var ddlContact = document.getElementById('ddlEnquiryContact');
    var contactId = ddlContact.options[ddlContact.selectedIndex].value;

    //  Get the additional properties
    var source = document.getElementById('txtEnquirySource').value;
    var comments = document.getElementById('txtEnquiryComments').value;
    //  Generate and populate a job object to be passed to the service
    var job = {
        Id: -1,
        ClientId: ObjClient.Id,
        ContactId: contactId,
        Source: source,
        ReporterId: -1,
        Reporter: '',
        Comments: comments,
        Status: 0,
        SurveyorId: surveyorId,
        IncludeOnDashboard: 1
    }
    //  Get the selected sites
    var selectedSites = GetSelectedSites();
    var newId = -1;
    if (selectedSites.length == 0) {
        DisplayError('Warning: Please select a site address for this job');
    } else {
        //CreateJob(Types.Job job, List<Types.ClientSite> selectedSites)
        try {
            newId = Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'CreateJob',
                {
                    job: job,
                    selectedSites: selectedSites
                },
                false,
                null
            );

        } catch (err) {
            DisplayError('Error: Unable to save new Job -' + err.toString());
            newId = -1;
        }

        if (newId < 1) {
            DisplayError('Error: The Arb Pro service did not save the Job correctly');
        }
    }

    return newId;
}

/*
        SERVICE CALLBACK FUNCTIONS
*/

function ClientLoad_Callback(result) {
    //  Check if the service return a valid data array
    if (!result) {
        DisplayError('Error:The Arb Pro service did not return a Customer.');
    } else {
        ObjClient = result;
        GenerateNewJobHtml();
    }
}


function ClientSiteLoad_Callback(results) {
    //  Check if the service return a valid data array
    if (!results || results.length == 0) {
        //  NOTE: This error should not prevent the edit of the job details
        DisplayError('Error:The Arb Pro service did not return a valid list of Sites. Please ensure that the client is configured with at least oone site.');
        results = []; // Initialise the array incase it is undefined
    } else {
        LstSites = results;
        GenerateNewJobHtml();
    }
}

function SurveyorsLoad_Callback(results) {
    //  Check if the service return a valid data array
    if (!results || results.length == 0) {
        //  NOTE: This error should not prevent the edit of the job details
        DisplayError('Error:The Arb Pro service did not return a valid list of Survyors. Please ensure that at least on staff member is configured for the role.');
        results = []; // Initialise the array incase it is undefined
    } else {
        LstSurveyors = results;
        GenerateNewJobHtml();
    }
}


/*
        GENERAL FUNCTIONS
*/


//  Function to generate the html content for the conent div. This includes all controls and function setup
function GenerateNewJobHtml() {
    //Check if all items have loaded
    if (ObjClient && LstSites != null && LstSites.length > 0 && LstSurveyors != null && LstSurveyors.length > 0) {
        //  All items are now loaded
        var panel = document.getElementById('pnlJobDetails');
        try {
            var strHtml = '<div style="width: 90%; margin:0px auto;">';
            //  Survyor section
            strHtml += GenerateContactSelect();

            //  Survyor section
            strHtml += GenerateSurvyorSelect();

            //  Source section
            strHtml += '<div class="jobEditField">';
            strHtml += '<label  class="jobEditLabel bold">Enquiry Source</label>';
            strHtml += '<input id="txtEnquirySource" class="jobEditInput ui-autocomplete-input" type="text" placeholder="Source" name="txtEnquirySource" autocomplete="off">';
            strHtml += '</div>';

            //  Comments section
            strHtml += '<div class="jobEditField">';
            strHtml += '<span class="jobEditLabel bold" style="display:inline-block;width:100%;">Comments</span>';
            strHtml += '<textarea id="txtEnquiryComments" class="jobEditField small" style="width:98%;" cols="20" rows="5" name="txtEnquiryComments">';
            strHtml += '</textarea>';
            strHtml += '</div>';

            //  Show in dashboard
            strHtml += '<div class="jobEditField">';
            strHtml += '<label  class="jobEditLabel bold">Display in Dashboard</label>';
            strHtml += '<input id="chkShowJobOnDb" type="checkbox" style="cursor: pointer;" checked/>';
            strHtml += '</div>';

            strHtml += '<div class="jobEditField">';
            strHtml += GenerateSiteTable();
            strHtml += '</div>';

            panel.innerHTML = strHtml;

            AutoComplete('txtEnquirySource', 'ListEnquirySource');
        } catch (err) {
            DisplayError('Error: Unable to load edit panel - ' + err.toString());
        }
    }
}


//  Function to display an error message into a predefined panel
function DisplayError(message) {
    DisplayMessage('pnlJobDetailError', '' + message, 'red');
    var panel = document.getElementById('pnlJobDetailError');
    panel.className = "width-full center";
}

//  Function to generate the HTML for a drop down selection element that is populated with the list of surveyors
function GenerateSurvyorSelect() {
    var strHtml = '';
    if (LstSurveyors.length > 0) {
        strHtml = '<div class="jobEditField">';
        strHtml += '<label class="jobEditLabel bold">Default Surveyor</label>';
        strHtml += '<select id="ddlEnquirySurveyor" class="jobEditInput" name="ddlEnquirySurveyor">';
        strHtml += '<option value="-1">Please Select</option>';
        LstSurveyors.forEach(function (s) {
            strHtml += '<option value="' + s.Id.toString() + '">' + s.Name + '</option>';
        });
        strHtml += '</select>';
        strHtml += '</div>';
    }
    return strHtml;
}

//  Function to generate and the HTML for a selection list containing the contact details
function GenerateContactSelect() {
    var strHtml = '';
    if (LstSurveyors.length > 0) {
        strHtml = '<div class="jobEditField">';
        strHtml += '<label class="jobEditLabel bold">Contact</label>';
        strHtml += '<select id="ddlEnquiryContact" class="jobEditInput" name="ddlEnquirySurveyor">';
        strHtml += '<option value="-1">Please Select</option>';
        ObjClient.Contacts.forEach(function (s) {
            strHtml += '<option value="' + s.Id.toString() + '">' + s.NameAndEmail + '</option>';
        });
        strHtml += '</select>';
        strHtml += '</div>';
    }
    return strHtml;
}



function GenerateSiteTable() {
    var strHtml = '<table class="searchGrid small width-full"><col width="95%" /><col width="5%" />';
    strHtml += '<tr<th colspan="2">Select Sites</th>';
    LstSites.forEach(function (cs) {
            strHtml = strHtml + '<tr onclick="ToggleSiteItem(' + cs.Id + ');">';
            strHtml = strHtml + '<td class="white-light">' + cs.SiteDetails + '</td>';
            strHtml = strHtml + '<td class="white-light"><input type="checkbox" id="chkClientSite_' + cs.Id + '" disabled></td>';
        strHtml = strHtml + '</tr>';
    });
    strHtml = strHtml + '</table>';
    return strHtml;
}

function ToggleSiteItem(sId) {

    var chk = document.getElementById('chkClientSite_' + sId.toString());
    chk.checked = !chk.checked;
}

//  Function to get an array of selected sites
function GetSelectedSites() {
    var sites = [];
    var chkElement = null;
    LstSites.forEach(function(s) {
        chkElement = document.getElementById('chkClientSite_' + s.Id.toString());
        if (chkElement && chkElement.checked) {
            //  The element exists and it is checked so add it to the return array
            sites.push(s);
        }
    });
    return sites;
}
