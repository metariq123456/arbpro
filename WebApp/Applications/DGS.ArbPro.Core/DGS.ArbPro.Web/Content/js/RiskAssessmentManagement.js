﻿//Global variables
var assessment;
var newStatus = -1;
var Type = 0;
/*
TODO: Remove from file once common routine has been validated
//Method to display a message in a named control
function DisplayMessage(panel, message) {
    var strHtml = '<table class="width-full white"><tr><td>' + message + '</td></tr></table>';
    var details = document.getElementById(panel);
    details.innerHTML = strHtml;
}
*/
function RiskAssessmentManagement_Save() {
    if (Type == 1) {
        try {
            //Refresh the dates
            assessment.ManagementComments = document.getElementById("txtManagementComments").value;
            assessment.AssessmentDate = toDateFromJson(assessment.AssessmentDate);
            assessment.WorkDate = toDateFromJson(assessment.WorkDate);
            assessment.LastUpdatedDate = toDateFromJson(assessment.LastUpdatedDate);
            assessment.ReferralDate = toDateFromJson(assessment.ReferralDate);
            switch (newStatus) {
                case -1:
                    assessment.ReviewDate = toDateFromJson(assessment.ReviewDate);
                    assessment.CompletionDate = toDateFromJson(assessment.CompletionDate);
                    break;
                case 0:
                    assessment.ReviewDate = minJQueryDate();
                    assessment.CompletionDate = minJQueryDate();
                    assessment.SatisfactoryCompletion = false;
                    assessment.ReferralRequired = false;
                    assessment.ReferralCompleted = false;
                    assessment.Completed = false;
                    break;
                case 1:
                    assessment.ReviewDate = minJQueryDate();
                    assessment.CompletionDate = minJQueryDate();
                    assessment.SatisfactoryCompletion = false;
                    assessment.ReferralRequired = true;
                    assessment.ReferralCompleted = false;
                    assessment.Completed = false;
                    break;
                case 2:
                    assessment.ReviewDate = toDateFromJson(assessment.ReviewDate);
                    assessment.CompletionDate = minJQueryDate();
                    assessment.SatisfactoryCompletion = false;
                    assessment.ReferralRequired = true;
                    assessment.ReferralCompleted = true;
                    assessment.Completed = false;
                    break;
                case 3:
                    assessment.ReviewDate = toDateFromJson(assessment.ReviewDate);
                    assessment.CompletionDate = new Date();
                    if (assessment.ReferralRequired == 0 || assessment.ReferralCompleted == 0) {
                        assessment.SatisfactoryCompletion = true;
                    } else {
                        assessment.SatisfactoryCompletion = false;
                    }
                    assessment.Completed = true;
                    break;
                default:
                    assessment.ReviewDate = toDateFromJson(assessment.ReviewDate);
                    assessment.CompletionDate = toDateFromJson(assessment.CompletionDate);
                    break;
            }
            Ajax_CallService(
                        '../Services/Web/RiskAssessment.asmx',
                        'RiskAssessmentUpdateDetails',
                        {
                            assessment: assessment
                        },
                        false);
        } catch (err) {
            DisplayMessage("pnlRiskDetails", "Error saving risk assessment details.");
        }
    }

    else {
        try {
            assessment.RiskAssessmentID = document.getElementById("hfRiskId").value;
            assessment.AdditionalActions = document.getElementById('txtAdditionalActions').value;
            assessment.ReferralComments = document.getElementById('txtReferralComments').value;
            assessment.TeamLeaderSpokenWith = document.getElementById('teamLeaderSpokenWith').checked;
            var date = document.getElementById('txtReferralDate').value;
            if (date && date != 'pending') {
                var newDate = new Date(date);
                assessment.ReferralDate = newDate;
            } else
                assessment.ReferralDate = '1970-01-01';

            Ajax_CallService(
            '../Services/Web/RiskAssessment.asmx',
            'RiskAssessmentReferralSave',
            {
                Ref: assessment
            },
            false);
        }
        catch (ex) {
            DisplayMessage("pnlRiskDetails", "Error saving risk assessment referral details.");
        }
    }
}

function RiskAssessmentManagement_Load(type) {
    try {
        Type = type;
        if (Type == 1) {
            var riskId = document.getElementById("hfRiskId").value;
            Ajax_CallService(
                        '../Services/Web/RiskAssessment.asmx',
                        'RiskGetForId',
                        {
                            riskId: riskId
                        },
                        true,
                        DisplayDetails);
        }
        else {
            var riskId = document.getElementById("hfRiskId").value;
            Ajax_CallService(
                        '../Services/Web/RiskAssessment.asmx',
                        'getReferralForRiskId',
                        {
                            RiskId: riskId
                        },
                        true,
                        DisplayReferalDetails);
        }
    }
    catch (err) {
        DisplayMessage("pnlRiskManagement", "Error loading risk management details.");
    }
}

//Async JQuery callback to display specific risk assessment management items
function DisplayDetails(risk) {
    if (!risk) {
        DisplayMessage("pnlRiskManagement", "Error communicating with service.");
    } else {
        try {
            // 
            var panel = document.getElementById("pnlRiskManagement");
            var strHtml = '<div class="width-full">';
            strHtml += '<div class="clientField">';
            var selectedIndex = getSelectedStatusIndex(risk);
            strHtml += '<select id="cmbStatus" title="Assessment Status" class="width-full clientInput" onchange="SetStatusValue();">';
            strHtml += '<option value="0" ' + (selectedIndex == 0 ? ' selected="selected"' : '') + '>Pending</option>';
            strHtml += '<option value="1" ' + (selectedIndex == 1 ? ' selected="selected"' : '') + '>Referral Required</option>';
            strHtml += '<option value="2" ' + (selectedIndex == 2 ? ' selected="selected"' : '') + '>Referral Completed</option>';
            strHtml += '<option value="3" ' + (selectedIndex == 3 ? ' selected="selected"' : '') + '>Completed</option>';
            strHtml += '</select>';
            strHtml += '<br>';
            strHtml += '<textarea id="txtManagementComments" title="Management Comments" placeholder="Comments" class="riskLongInput" rows="4" name="txtManagementComments">' + risk.ManagementComments + '</textarea>';
            strHtml += '</div></div>';
            panel.innerHTML = strHtml;

            assessment = risk;
        } catch (err) {
            DisplayMessage("pnlRiskManagement", "Error displaying risk management details.");
        }
    }
}

function getSelectedStatusIndex(riskAssessment) {
    if (riskAssessment.Completed) {
        return 3;
    } else if (riskAssessment.ReferralCompleted) {
        return 2;
    } else if (riskAssessment.ReferralRequired) {
        return 1;
    } else {
        return 0;
    }
}

function SetStatusValue() {
    // 
    var dropdown = document.getElementById("cmbStatus");
    newStatus = parseInt(dropdown.options[dropdown.selectedIndex].value);
}

function DisplayReferalDetails(risk) {
    try {
        var a = Date.parse(risk.ReferralDate);

        var referralDate;
        if (a && a != "NaN") {
            referralDate = toDateFromJson(risk.ReferralDate).format("dd/MMM/yyyy");
        } else {
            referralDate = "Pending";
        }

        var panel = document.getElementById("pnlRiskManagement");
        var strHtml = '<div class="width-full">';
        strHtml += '<div class="clientField">';
        strHtml += '</br> <label class="riskLabel">Team leader spoken with: </label> <input id="teamLeaderSpokenWith" type="checkbox" name="TeamLeaderSpokenWith" value="1"/>';
        strHtml += '</br> <label class="riskLabel">Date of referral:</label></br><input id="txtReferralDate" class="companyInput padded" type="text" placeholder="Date Of Referral" name="txtRefDate" value="' + referralDate + '"/>';
        strHtml += '</br> <textarea id="txtReferralComments" title="Referral Comments" placeholder="Comments" class="riskLongInput" rows="4" name="txtManagementComments">' + (risk.ReferralComments == null ? '' : risk.ReferralComments) + '</textarea>';
        strHtml += '</br> <textarea id="txtAdditionalActions" title="Additional Actions" placeholder="Additional Actions" class="riskLongInput" rows="4" name="txtManagementComments">' + (risk.AdditionalActions == null ? '' : risk.AdditionalActions) + '</textarea>';
        panel.innerHTML = strHtml;

        $('#txtReferralDate').datetimepicker({
            format: 'd/M/Y',
            timepicker: false
        });

        document.getElementById('teamLeaderSpokenWith').checked = risk.TeamLeaderSpokenWith;

        assessment = risk;
    }
    catch (ex) {
        DisplayMessage('pnlRiskManagement', 'Error: Unable to display risk referral details');
    }
}