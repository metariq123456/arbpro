﻿var SelectedResultId = -1;
var IsNewItem = false;
var CurrentStock = 0;
ToolsList = null;


function Settings_Load() {
    _toolsList = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'GetToolList',
           {
           },
           false
       );
    ToolsList = _toolsList;
    UpdateTable();
}

function UpdateTable() {
    var ListPanel = document.getElementById('pnlTableContainer');
    var strHtml = '<table class="searchGrid width-full white">';
    for (var i = 0; i < ToolsList.length; i++) {
        if (ToolsList[i].Deleted != true) {
            if (ToolsList[i].Id == SelectedResultId) {
                strHtml += '<tr class="selected" onclick="ItemSelected( \'' + i + '\');">';
            }
            else {
                strHtml += '<tr onclick="ItemSelected( \'' + i + '\');">';
            }
            strHtml += '<td style="padding-left: 10px;">' + ToolsList[i].Item + '</td>';
            strHtml += '</tr>';
        }
    }
    strHtml += '</table>';
    //strHtml += '<button type="button" onclick="NewItem(\'' + "Tools" + '\');" class="float-right button-thin">New</button>';
    ListPanel.innerHTML = strHtml;
}

function ItemSelected(index) {
    var item = ToolsList[index];
    IsNewItem = false;
    SelectedResultId = item.Id;
    CurrentStock = item.Stock;
    PopulateHiddenFields(item.Item);
    UpdateTable();
}

function PopulateHiddenFields(Value) {
    document.getElementById('btnDelete').className = 'button-thin';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtValue').value = Value;
    document.getElementById('txtStock').value = CurrentStock;
}


function NewItem() {
    SelectedResultId = -1;
    CurrentStock = 0;
    UpdateTable();
    document.getElementById('btnDelete').className = 'button-thin hidden';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtValue').value = '';
    document.getElementById('txtStock').value = '';
}

function SaveItem() {
    CurrentStock = document.getElementById('txtStock').value;
    try {
        DoSave = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'SaveTool',
           {
               _id: SelectedResultId,
               _stock: CurrentStock,
               _item: document.getElementById('txtValue').value
           },
           false
       );
    }
    catch (err) {
         
    }
    SelectedResultId = -1;
    document.getElementById('pnlItemControls').className = 'clientField float-left hidden';
    Settings_Load();
}


function DeleteItem() {
    DoDelete = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'DeleteTool',
           {
               Id: SelectedResultId
           },
           false
       );

    document.getElementById('pnlItemControls').className = 'clientField float-left hidden';
    Settings_Load();
}