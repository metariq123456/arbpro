﻿
var Criterion_curTab = 1;

// function to switch between criterions to display the selected criterion's details
function SwitchCriterion() {
    var tabCount = document.getElementById('hfCriteriaTotal').value;
    for (var index = 1; index <= tabCount; index++) {
        if (index == Criterion_curTab) {
            document.getElementById('tab' + index.toString()).className = 'activeTab lmGridItem_Global';
            document.getElementById('pnl' + index.toString()).className = '';
            document.getElementById('lblDetails' + index.toString()).className = 'clientTabBar';
        }
        else {
            document.getElementById('tab' + index.toString()).className = '';
            document.getElementById('pnl' + index.toString()).className = 'hidden';
            document.getElementById('lblDetails' + index.toString()).className = 'clientTabBar hidden';
        }
    }
}

// function to display the dialog for new/edit criterion screen
function NewEditOrCopyCriterion(criterionId, isCopy) {
    Dialog_OpenPage(
    criterionId == -1 || isCopy ? 'New Criterion' : 'Edit Criterion',
    'Dialogs/TemplateRiskCriteriaEdit.aspx?ID=' + criterionId,
    'content',
    600,
    400,
    true,
    {
        'Save': {
            text: 'Save',
            class: 'ui-dialog-button',
            click: function () {
                if (SaveTemplateCriterion(isCopy)) {
                    $(this).dialog('close');
                } else {
                    alert('Criterion Save Failed');
                }
            }
        },
        'Cancel': {
            text: 'Close',
            class: 'ui-dialog-button',
            click: function () {
                $(this).dialog('close');
            }
        }
    },
    'RiskCriteria.aspx'
    );
}

// saves the new, edited or copied criterion
function SaveTemplateCriterion(isCopy) {

    var criterion = {};
    var criterionEl = $("table[data-container='criterion']");

    // criterion
    if (isCopy) {
        criterion.Id = -1;
    } else {
        criterion.Id = criterionEl.attr("data-val");
    }
    criterion.Criterion = $("[data-field='cri-title']").val();
    criterion.ChapterRef = criterionEl.find("[data-field='cri-chapref']").val();
    criterion.AssessmentPhase = criterionEl.find("[data-field='cri-phase']").val();

    if (criterion.Criterion === "") {
        alert('Please add a criterion name.');
        return false;
    }

    if (criterion.AssessmentPhase.length !== 1 || isNaN(parseInt(criterion.AssessmentPhase, 10)) || criterion.AssessmentPhase < 1 || criterion.AssessmentPhase > 2) {
        alert('Please put 1 or 2 for the Assessment Phase and try again.');
        return false;
    }

    // update the criteria document references
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'InsertOrUpdateCriterionTemplate',
        {
            criterion: criterion
        },
        false
    );


    location.reload();
    return retVal > 0;
}

function CancelCriterion(criterionId) {
    var retVal = confirm("Are you sure you want to remove this Risk Criteria?");

    if (retVal) {
        Ajax_CallService(
            'Services/Web/Template.asmx',
            'CancelCriterionTemplate',
            {
                criterionId: criterionId
            },
            false
        );

        location.reload();
    }
}