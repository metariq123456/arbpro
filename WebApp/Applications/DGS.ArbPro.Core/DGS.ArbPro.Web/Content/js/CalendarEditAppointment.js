﻿var editEvent = {
    id: 0,
    title: 'New Appt',
    allDay: false,
    start: null,
    end: null,
    className: 'cal-event-editable'
}

function LoadCalendar(JobSiteList, edit, enquiry, jobSiteId, appt) {
    //Start populating lists
    var ddlSite = document.getElementById('ddlClientSites');
    var siteId = document.getElementById('hfJobSiteId');

    if (enquiry != true) {
        if (JobSiteList.length > 1) {
            var option = document.createElement("option");
            option.text = 'Please Select';
            option.value = -1;
            ddlSite.add(option);
        }
        JobSiteList.forEach(function (s) {
            var option = document.createElement("option");
            option.text = s.SiteString;
            option.value = s.Id;
            ddlSite.add(option);
            if (edit) { if (s.Id == jobSiteId) { ddlSite.options[ddlSite.selectedIndex] = option; ddlSite.disabled = true; } }
        });

        var selectedJobSiteId = ddlSite.options[ddlSite.selectedIndex].value;
        document.getElementById('hfJobSiteId').value = selectedJobSiteId;
    }
    else {
        document.getElementById('lblSite').className = 'hidden';
        document.getElementById('ddlClientSites').className = 'hidden';
    }



    var Surveyors = Ajax_CallService(
        'Services/Web/Company.asmx',
        'ListSurveyors',
        {},
        false);
    var ddlSurveyor = document.getElementById('ddlSurveyors');
    var StaffID = document.getElementById('hfAppSurveyor').value;

    Surveyors.forEach(function (s) {
        var option = document.createElement("option");
        option.text = s.Name;
        option.value = s.Id;
        ddlSurveyor.add(option);

        if (StaffID == s.Id) {
            ddlSurveyor.selectedIndex = ddlSurveyor.options.length - 1;
        }
    });

    var templates = Ajax_CallService(
        'Services/Web/Template.asmx',
        'GetAllRiskTemplates',
        {
        },
        false);

    var ddlTemplates = document.getElementById('ddlTemplate');
    ddlTemplates.selectedIndex = 0;
    templates.forEach(function (t) {
        var option = document.createElement("option");
        option.text = t.Name;
        option.value = t.Id;
        ddlTemplates.add(option);

        if (appt && appt.TemplateId == t.Id) {
            ddlTemplates.selectedIndex = ddlTemplates.options.length - 1;
        } 
    });


    //start calendar
    $('#pnlCalendar').fullCalendar({
        header: {
            left: 'title',
            center: 'agendaWeek,agendaDay',
            right: 'prev,today,next'
        },
        height: 470,
        aspectRatio: 2,
        defaultView: 'agendaWeek',
        events: function (start, end, callback) {

            var AppID = document.getElementById('hfAppId').value;

            if (AppID != '' & editEvent.id == 0) {
                var App = Ajax_CallService(
                    'Services/Web/WorkFlow.asmx',
                    'GetQuoteAppointmentForId',
                    {
                        ApptId: AppID
                    },
                    false);

                editEvent.id = App.Id;
                editEvent.title = App.JobSiteName;
                editEvent.start = new Date(parseInt(App.StartDateTime.substr(6)));
                editEvent.end = new Date(parseInt(App.EndDateTime.substr(6)));

                $('#hfAppStart').val(editEvent.start.toJSON());
                $('#hfAppEnd').val(editEvent.end.toJSON());

                $('#pnlCalendar').fullCalendar('gotoDate', new Date(parseInt(App.StartDateTime.substr(6))));
            }

            if (editEvent.id != 0) {
                $('#pnlCalendar').fullCalendar('renderEvent', editEvent);
            }

            var StaffID = document.getElementById('hfAppSurveyor').value;
            var EventsList = new Array();

            var SurveyorQuoteEvents = Ajax_CallService(
                'Services/Web/Calendar.asmx',
                'GetQuoteEvents',
                {
                    Start: start.toDateString(),
                    End: end.toDateString(),
                    StaffID: StaffID
                },
                false
            );

            SurveyorQuoteEvents.forEach(function (ev) {
                if (AppID == '' || AppID != ev.Id) {
                    EventsList.push({
                        id: ev.Id,
                        data: ev,
                        title: ev.CalendarString,
                        allDay: false,
                        start: new Date(parseInt(ev.StartDateTime.substr(6))),
                        end: new Date(parseInt(ev.EndDateTime.substr(6))),
                        className: 'cal-event-quote',
                        editable: false
                    });
                }
            });


            var SurveyorGenericEvents = Ajax_CallService(
                'Services/Web/Calendar.asmx',
                'GetGenericEvents',
                {
                    Start: start.toDateString(),
                    End: end.toDateString(),
                    StaffID: StaffID
                },
                false
            );
            SurveyorGenericEvents.forEach(function (ev) {

                EventsList.push({
                    id: ev.Id,
                    data: ev,
                    title: ev.CalendarString,
                    allDay: ev.AllDay,
                    start: new Date(parseInt(ev.StartDateTime.substr(6))),
                    end: new Date(parseInt(ev.EndDateTime.substr(6))),
                    className: 'cal-event-generic',
                    editable: false
                });
            });
            callback(EventsList);
        },
        dayClick: function (date, allDay, jsEvent, view) {
            if (document.getElementById('hfJobSiteId').value == -1 && enquiry == false) {
                //Display err msg
                document.getElementById('pnlErr').className = '';
                document.getElementById('lblErr').textContent = 'Please select a site for the appointment';
            }
            else {
                document.getElementById('pnlErr').className = 'hidden';
                if (allDay) return;

                var endDate = new Date(date);
                endDate.setMinutes(endDate.getMinutes() + 30);
                editEvent.start = date;
                editEvent.end = endDate;

                if ($('#hfAppStart').val() == "") {
                    //Stick - true as when the calendar refetches event sources(prev/next pressed)
                    //The event dissapears, meaning that there is no event to 'update'. This keeps it on the
                    //calendar
                    var stick = true;
                    $('#pnlCalendar').fullCalendar('renderEvent', editEvent, stick);
                } else {
                    $('#pnlCalendar').fullCalendar('updateEvent', editEvent);
                }

                $('#hfAppStart').val(editEvent.start.toJSON());
                $('#hfAppEnd').val(editEvent.end.toJSON());
            }
        },
        eventDrop: function (event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
            if (allDay) {
                revertFunc();
            }

            $('#hfAppStart').val(editEvent.start.toJSON());
            $('#hfAppEnd').val(editEvent.end.toJSON());
        },
        eventResize: function (event, revertFunc, jsEvent, ui, view) {
            $('#hfAppStart').val(editEvent.start.toJSON());
            $('#hfAppEnd').val(editEvent.end.toJSON());
        },
        viewRender: function (view, element) {
            $('#pnlCalendar').fullCalendar('refetchEvents');
            $('#pnlCalendar').fullCalendar('updateEvent', editEvent);
        },
        editable: true,
        firstHour: 6,
        allDayText: 'All Day',
        axisFormat: 'HH:mm',
        titleFormat: {
            week: 'MMMM d[ yyyy]{ &#8212;[ MMM] d yyyy}',
            day: 'dddd, dd MMMM yyyy'
        },
        columnFormat: {
            week: 'ddd dd/MM',
            day: 'dddd, dd MMMM'
        }
    });
    TemplateChanged();
}
//End calendar

function SurveyorChanged() {
    var ddl = document.getElementById('ddlSurveyors');
    var selectedId = ddl.options[ddl.selectedIndex].value;

    document.getElementById('hfAppSurveyor').value = selectedId;
    $('#pnlCalendar').fullCalendar('refetchEvents');
}

function SiteChanged() {
    //get current selected site id
    var ddl = document.getElementById('ddlClientSites');
    var selectedSiteId = ddl.options[ddl.selectedIndex].value;

    //set the site value hidden field to the Id of the selected site
    document.getElementById('hfJobSiteId').value = selectedSiteId;
}

function TemplateChanged() {
    var ddl = document.getElementById('ddlTemplate');
    var selectedId = ddl.options[ddl.selectedIndex].value;

    document.getElementById('hfTemplateId').value = selectedId;
}