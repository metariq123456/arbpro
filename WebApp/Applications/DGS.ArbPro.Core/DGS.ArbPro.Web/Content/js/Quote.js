﻿
var ObjSelectedQuote = null;    //The currently selected quote object
var Quote_curTab = 1;           //Global scope variable for display of the 

// Variable to indicate whether we've initialized the QuoteItemsDialog
var QuoteItemsDialogInitialized = false;

//The following three variables allow us to track the items retrieved from the service, those updated and those deleted
// - the jqWidgets tools don't have a built-in mechanism for tracking updates and deletions 
var QuoteItems = [];            // The raw QuoteItems retrieved from the services
var QuoteItemsUpdated = [];     // Those QuoteItems updated or added
var QuoteItemIdsDeleted = [];   // The Ids of the QuoteItems deleted
var JobSiteBeingEdited = -1;
var jsId;
var DocList = [];
var includeTreePicture = false;
var includeSitePlan = false;
var includeRiskAssessment = false;
var noOfDocAttached = 0;
/*

        ****** Initialisation and service functions **********

*/

//      Function to load and display a selected quote
function Quote_Load(id) {
    //Show the wait image while the details load
    SwitchQuoteTab(0);

    var panel = document.getElementById('grdQuotes');
    panel.innerHTML = GetWaitImageForPanel(panel.id, 'Loading Quote details please wait.');
    var panel = document.getElementById('pnlQuoteHistory');
    panel.innerHTML = GetWaitImageForPanel(panel.id, 'Loading Quote documents please wait.');
    //If the Id is < 0 a new Quote object will be returned
    Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetQuote',
        {
            QuoteID: id
        },
        true,
        Quote_LoadCallback
    );
}

//      Function to create a duplicate quote
function Requote() {
    DisplayAlertMessage("Would you like to keep the existing Risk Assessment for the new Quote?");
}

function Duplicate(keepRisk) {
    try {
        var duplicate = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'DuplicateQuote',
            {
                QuoteID: ObjSelectedQuote.Id,
                keepRiskAssessment: keepRisk
            },
            false);

        if (duplicate) {
            window.location = '/Job.aspx?JobID=' + ObjJob.Id.toString() + '&QuoteID=' + duplicate.Id.toString();
        }
    }
    catch (err) {
        DisplayMessageDialog('Quote Error', 'The Arb Pro service did not return a valid Quote - ' + err.to());
    }
    return false;
}

//      Function to set the stqatus for
function ChangeQuoteStatus(status) {
    try {
        Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'SetQuoteStatus',
            {
                QuoteID: ObjSelectedQuote.Id,
                Status: status
            },
            false);

        ObjSelectedQuote.Status = status;

        if (status < 2) {
            //Reload the quote
            Quote_Load(ObjSelectedQuote.Id);
        } else if (status === 2) {
            UpdateAcceptedWorkClientCategoriesForJobId(ObjJob.Id);
            Job_Refresh(ObjJob.Id);
        } else {
            if (status === 3) {
                if (ObjCompany.DisplayMarketing && ObjJob.PointOfContact.IncludeInMarketing) {
                    ClientMarketing();
                }
                Job_curTab++;       //This will cause the Work tab to be displayed
            }
            Job_Refresh(ObjJob.Id);
        }

    }
    catch (err) {
        DisplayMessageDialog('Error', 'Unable to change status for Quote - ' + err.toString());
    }

    //This is to prevent an invalid post back argument exception
    return false;
}

//      Function called to update the QuoteItems with the amendments made in the QuoteItems dialog
function QuoteItems_update() {
    try {

        //Ensure the quote id is set correctly: New items must have an Id of -1
        QuoteItemsUpdated.forEach(function (Item) {
            var result = $.grep(QuoteItems, function (e) { return e.Id == Item.Id; });
            if (result.length == 0) {
                Item.Id = -1;
            }
        });

        // deletes
        QuoteItemIdsDeleted.forEach(function (itemID) {
            Ajax_CallService(
                    "Services/Web/WorkFlow.asmx",
                    "DeleteQuoteItem",
                    {
                        'itemID': itemID
                    },
                    false,
                    function (result) { }
                );
        });

        //QuoteItemsSave(int jobId, int quoteId, int jobSiteId, List<Types.QuoteItem> items)
        Ajax_CallService(
                "Services/Web/WorkFlow.asmx",
                "QuoteItemsSave",
                {
                    'jobId': ObjJob.Id, 'quoteId': ObjSelectedQuote.Id, 'jobSiteId': JobSiteBeingEdited, 'items': QuoteItemsUpdated
                },
                false,
                function (result) {
                    if (!result) {
                        alert('Error durring quote item update: Some or all items may not have been added correctly.');
                    }
                }
            );
    } catch (err) {
        alert('Error durring quote item update: Some or all items may not have been added correctly - ' + err.toString());
    }

    // refresh the quote list
    if (ObjSelectedQuote.Id > 0) {
        //We have been editing an existing quote
        Quote_Load(ObjSelectedQuote.Id);
    } else {
        //A new Quote has been added to the job, so refresh the job
        Job_Refresh(ObjJob.Id);
    }
}

//      Function to load all notes for a quote
function GetQuoteNotesList() {
    NotesList_Load(ObjSelectedQuote.Id, 0, PopulateQuoteNotes);
}

/*

        ****** Service call back functions **********

*/

//      Async callback function to display the selected quote details
function Quote_LoadCallback(result) {
    try{
        if (!result) {
            DisplayMessage('grdQuotes', 'Error: Unable to load quote details - ' + err.toString());
        } else {
            ObjSelectedQuote = result;
            
            DisplayQuoteDetails();
            //      Must make sure the quote object is populated before loading the notes
            GetQuoteNotesList();

            PopulateQuoteDocuments();
        }
    } catch (err) {
        DisplayMessage('grdQuotes', 'Error: Unable to display quote details - ' + err.toString());
    }
}

//      Async callback function to populate the Quote notes panel      
function PopulateQuoteNotes(results) {
    debugger;
    var notesPanel = document.getElementById('pnlQuotesNotesContainer');
    //Use the common.js method to populate and control functionality
    if (ObjSelectedQuote.Id < 1) {
        var msg = 'You cannot add a quote note as no quotation has been created for this Job/Enquiry yet.';
        msg += '<br/><br/>*Please Note, Quote notes are an internal admin function designed to alert users there are notes related to a quote and alerts can appear in the dashboard. ';
        msg += 'Surveyor notes that appear on the mobile device are managed from the Enquiry tab and are not quote notes.';
        msg += '<br/><br/>To create a quote note you must first create a quote by clicking on the Quote Details tab and either add work items or upload a completed ';
        msg += 'appointment from the mobile device. After adding work items you will then be able to add a quote note and view it on the dashboard.';
        msg += '<br/><br/>To add or update comments for a surveyor to view on the mobile device, you must select the “Enquiry Tab” and click on the “Edit Enquiry” button, add/update the text in the comments box and finally select “Save”.';
        msg += '<br/><br/>If you are updating surveyor notes for a job already downloaded on a surveyors device, you will have to re-release the appointment for download after you have added or amended the comments. ';
        msg += 'To do this, navigate to the Calendar screen and click on the calendar appointment, then click edit and tick "Release for download" ';
        msg += 'and save. Finally ask the surveyor to download appointments on their device and the enquiry notes will be updated.';

        DisplayMessage(notesPanel.id, msg, 'red');
    } else {
        PopulateNotes(notesPanel, 0, ObjSelectedQuote.Id, results, GetQuoteNotesList);
    }
}

function PopulateQuoteDocuments() {
    var DocumentPanel = document.getElementById('pnlQuotesDocumentContainer');
    var JobId = getParameterByName("JobID");
    try
    {
        var JobDocs = Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'getJobDocuments',
                {
                    JobId: JobId
                },
                false
                );
        var strHtml = ''
        strHtml += '<div class="width-full float-left white-light"><button type="button" onclick="ShowDocumentsDialogOnQuote(-1);" class="button-thin float-left padded">Add New Documents</button></div>';
        strHtml += '<div class="width-full float-left">';
        console.log('JobDocs:' + JobDocs.length);
        if (!JobDocs) {
            DisplayMessage(DocumentPanel.id, 'Error: no data return from service');
        }
        else {
            strHtml += '<table class="searchGrid width-full">';
            if (JobDocs.length == 0) {
                strHtml += '<tr class="white-light">There are no documents records to display. Please add documents by clicking the "Add New Documents" button.</tr>';
            } else {
                strHtml += '<tr class="white-light"><th style="padding-left: 10px;">Documents Name</th><th style="text-align:center;">Email attachment</th></tr>';
                JobDocs.forEach(function (c) {
                    console.log(c.DefaultAttachment);
                    strHtml += '<tr>';
                    strHtml += '<td onclick="ShowDocumentsDialogOnQuoteEdit(' + c.Id + ');" style="padding-left: 10px;">' + c.DocumentName + '</td>';
                    if (c.DefaultAttachment) {
                        strHtml += '<td style="width:21%; vertical-align: middle; text-align:center;" ><input type="checkbox" id="chkDefaultAttachment" checked onChange="ToggleDefaultAttachment(' + c.Id + ',this);" id="(' + c.Id + ');"/></td>';
                    }
                    else {
                        strHtml += '<td style="width:21%; vertical-align: middle; text-align:center;" ><input type="checkbox" id="chkDefaultAttachment" onChange="ToggleDefaultAttachment(' + c.Id + ',this);" id="(' + c.Id + ');"/></td>';
                    }

                    strHtml += '</tr>';
                });
            }
        }
        strHtml += '</table>';
        strHtml += '</div>';
        DocumentPanel.innerHTML = strHtml;
    }
    catch (err) {
       alert(err.toString());
    }
}

function ToggleDefaultAttachment(DocumentId, thisElement) {
    console.log(thisElement.checked);
    var Orgs = Ajax_CallService(
            'Services/web/WorkFlow.asmx',
            'ToggleDefaultAttachmentForJob',
            { DocId: DocumentId, DefaultAttachment: thisElement.checked },
            false
        );
}

/*

        ****** General functions **********

*/

//      Functionto display the current scope variable
//      Any errors thrown by this function must be caught in the calling method stack
function DisplayQuoteDetails() {
    UpdateControlButtons();
    //alert('Controls Updated');
    UpdateQuoteItems();
    //alert('Items Updated');
    UpdateQuoteHistory();
    //SwitchQuoteTab();
   // GetNotesList();
}

//      Function to display the items for the selected quotes
function UpdateQuoteItems() {
    var Grid = document.getElementById('grdQuotes');

    //debugger;
    //document.getElementById('hfQuoteStatus').value = ObjSelectedQuote.Status;
    var JobVatRates = new Array();
    //  DGSAP-748: Display of surveyor names
    var strHtml = '<table class="searchGrid small width-full"><tr><th><span class="padded">Surveyor(s): </span>';
    strHtml += ObjSelectedQuote.SurveyorName;
    strHtml += '</span></th></tr></table>';
    //  DGSAP-748: END
    ObjJob.WorkSites.forEach(function (js) {
       //  Sort the Quote items based on the current job site - This removes an additional service call for each site
        QuoteItems = $.grep(ObjSelectedQuote.QuotedItems, function (e) { return e.JobSiteId == js.Id; });
        itemSort(QuoteItems);
        //  Check if the site has been disabled OR has been disabled and the Quote has been sent with at least one item for this site
        if (!js.Disabled || (js.Disabled && ObjSelectedQuote.Status > 0 && QuoteItems.length > 0)) {
            console.log("js.Id" + js.Id);
            strHtml += '<table class="searchGrid small width-full"><col width="5%"><col width="20%"><col width="55%"><col width="10%"><col width="10%">';

            strHtml += '<tr>';
            strHtml += '<th colspan="5"><span class="float-left padded">' + js.SiteString + '</span>';
            strHtml += '<input type="button" class="button-thin padded float-right' + (ObjSelectedQuote.Status == 0 ? "" : " hidden") + '" value="Edit Items" onclick="QuoteItemsDialog_Open(' + js.Id + ');" />';
            strHtml += '<input type="button" class="button-thin padded float-right" value="Purchase Order" onclick="DisplayPurchaseOrderNo(' + ObjSelectedQuote.Id + ');" />';
            strHtml += '<input type="button" class="button-thin padded float-right" value="View Map" onclick="ShowItemMap(' + js.Id + ');" /></th><th>';
            //strHtml += '<input type="button" class="button-thin float-right' + (Quote.Status == 0 ? "" : " hidden") + '" value="Add Items" onclick="AddItem(1, ' + js.Id + '); return false;" /></th>';
            strHtml += '</tr>';
            //debugger;
            var SiteVatRates = new Array();

            BuildItemTable(QuoteItems, SiteVatRates, JobVatRates, function(qi) {
                strHtml += '<tr>';
                //strHtml += '<td style="vertical-align: top; text-align: center;"' + (ObjSelectedQuote.Status == 0 ? ' onclick="EditQuoteItem(' + qi.Id + ')"' : '') + '>' + qi.OrderId + '</td>';
                strHtml += '<td style="vertical-align: top; text-align: center;"' + (ObjSelectedQuote.Status == 0 ? ' onclick="EditQuoteItem(' + qi.Id + ')"' : '') + '>' + qi.Code + '</td>';
                strHtml += '<td style="vertical-align: top;"' + (ObjSelectedQuote.Status == 0 ? ' onclick="EditQuoteItem(' + qi.Id + ')"' : '') + '>' + qi.Name + '</td>';
                strHtml += '<td' + (ObjSelectedQuote.Status == 0 ? ' onclick="EditQuoteItem(' + qi.Id + ')"' : '') + '>' + qi.Description + '</td>';
                strHtml += '<td style="text-align: center" onclick="event.preventDefault(); OpenImage(1, ' + qi.Id + ');"><a href="#">Image</a></td>';
                strHtml += '<td style="text-align: right; padding-right: 3px; padding-left: 3px; vertical-align: bottom;"' + (ObjSelectedQuote.Status == 0 ? ' onclick="EditQuoteItem(' + qi.Id + ')"' : '') + '>' + (qi.Value > 0 ? ' ' + CurrentCurrency + '' + qi.Value.format() : '') + '</td>';
                strHtml += '</tr>';
            });

            strHtml += '</table>';
            //NOTE: Value updates
            //Check if site subtotals need to be displayed
            if (ObjJob.WorkSites.length > 1) {
                strHtml += GetSiteTotalHtml(SiteVatRates, ObjJob.WorkSites.length, 'Site');
                strHtml += '<hr/>';
            }
        }
    });
    //Display the invoice totals
    strHtml += GetSiteTotalHtml(JobVatRates, ObjJob.WorkSites.length, 'Quote');

    Grid.innerHTML = strHtml;
}

//      Function to set the CSS class for Quote controls depending on the selected quotes status
function UpdateControlButtons() {
    //debugger;
    if (ObjSelectedQuote.Status == 0) {
        document.getElementById('btnProduceQuote').className = 'button-basic padded float-right';
        document.getElementById('btnDeletePendingQuote').className = 'button-basic padded float-left';
        document.getElementById('btnResendQuote').className = 'button-basic padded float-left hidden';
        document.getElementById('btnFollowSales').className = 'button-basic padded float-left hidden';
        document.getElementById('btnAcceptQuote').className = 'button-basic padded float-right hidden';
        document.getElementById('btnRejectQuote').className = 'button-basic padded float-right hidden';
        document.getElementById('btnDuplicate').className = 'button-basic padded float-right hidden';
        //Disable all work buttons as they are not relevent when the quote has not been accepted
        setTimeout(function () {
            $(".BookWork").prop("disabled", true);
        }, 0);
        document.getElementById('btnMarkCompleted').disabled = true;
        //document.getElementById('btnProduceWorkPacks').disabled = true;
    }
    else if (ObjSelectedQuote.Status == 1) {
        document.getElementById('btnProduceQuote').className = 'button-basic padded float-right hidden';
        document.getElementById('btnDeletePendingQuote').className = 'button-basic padded float-left hidden';
        document.getElementById('btnAcceptQuote').className = 'button-basic padded float-right';
        document.getElementById('btnRejectQuote').className = 'button-basic padded float-right';
        document.getElementById('btnDuplicate').className = 'button-basic padded float-right';
        document.getElementById('btnResendQuote').className = 'button-basic padded float-left';
        document.getElementById('btnFollowSales').className = 'button-basic padded float-left';
        //Disable all work buttons as they are not relevent when the quote has not been accepted
        setTimeout(function () {
            $(".BookWork").prop("disabled", true);
        }, 0);
        document.getElementById('btnMarkCompleted').disabled = true;
        //document.getElementById('btnProduceWorkPacks').disabled = true;

    } else {
        document.getElementById('btnProduceQuote').className = 'button-basic padded float-right hidden';
        document.getElementById('btnDeletePendingQuote').className = 'button-basic padded float-left hidden';
        document.getElementById('btnAcceptQuote').className = 'button-basic padded float-right hidden';
        document.getElementById('btnRejectQuote').className = 'button-basic padded float-right hidden';
        if (ObjSelectedQuote.Status !== 2) {
            document.getElementById('btnResendQuote').className = 'button-basic padded float-left';
        } else {
            document.getElementById('btnResendQuote').className = 'button-basic padded float-left hidden';
        }
        document.getElementById('btnFollowSales').className = 'button-basic padded float-left hidden';
        document.getElementById('btnDuplicate').className = 'button-basic padded float-right';
        //Enable all work buttons as are relevent when the quote has been accepted
        setTimeout(function () {
            $(".BookWork").prop("disabled", false);
        }, 0);
        if (ObjSelectedQuote.Status !== 2) {
            document.getElementById('btnMarkCompleted').disabled = false;
        }
        //document.getElementById('btnProduceWorkPacks').disabled = false;
    }
}

//      Function to display the History for a quote
function UpdateQuoteHistory() {
    var Grid = document.getElementById('pnlQuoteHistory');
    var strHtml = '<table class="searchGrid small width-full">';
    var History = ObjSelectedQuote.History;
    if (History == null || History.length == 0) {
        strHtml += '<tr><td style="vertical-align: top; text-align: center;">No documents have been created for the selected Quote</td></tr>';
    } else {
        strHtml += '<col width="15%"><col width="15%"><col width="20%"><col width="25%"><col width="25%">';
        strHtml += '<tr>';
        strHtml += '<th>Doc Type</th>';
        strHtml += '<th>Created On</th>';
        strHtml += '<th>Sent Via</th>';
        strHtml += '<th>Sent On</th>';
        strHtml += '<th>PDF</th>';
        strHtml += '</tr>';

        History.forEach(function (h) {
            strHtml += '<tr>';
            strHtml += '<td style="vertical-align: top;">' + h.DocTypeString + '</td>';
            var CreationDate = new Date(parseInt(h.CreationDate.substr(6)));
            var SentDate = new Date(parseInt(h.SentDate.substr(6)));
            strHtml += '<td style="vertical-align: top; text-align: center;">' + (CreationDate.getYear() > 1 ? CreationDate.toDateString() : 'N/A') + '</td>';
            strHtml += '<td style="vertical-align: top;">' + h.DispatchMethodString + '</td>';
            strHtml += '<td style="vertical-align: top; text-align: center;">' + (SentDate.getYear() > 1 ? SentDate.toDateString() : 'N/A') + '</td>';
            strHtml += '<td style="vertical-align: top; text-align: center;"><a target="_blank" href="Images/ReportImage.aspx?Id=' + h.Id + '">View</a></td>';
            strHtml += '</tr>';
        });
    }
    strHtml += '</table>';

    Grid.innerHTML = strHtml;
}

//      Function to pupulate the Quote No drop down selection element
function RefreshQuoteList(selectedId) {
    if (!selectedId) {
        //no paramater has been provided so default to -1
        selectedId = -1;
    }
    //Populate the Quote dropdown
    var ddl = document.getElementById('ddlQuoteID');
    var i = 0;

    ddl.options.length = 0;
    var hasOpenQuote = false;
    var selectedIndex = -1;

    ObjJob.Quotes.forEach(function (q) {
        var opt = document.createElement("option");
        opt.text = '('+ (1 + i) + ' of ' + ObjJob.Quotes.length + ') ' + q.Reference;
        opt.value = q.Id;
        ddl.add(opt, i);
        if (q.Id == selectedId) {
            selectedIndex = i;
        }
        i++;
        hasOpenQuote = (hasOpenQuote || q.Status < 2);
    });

    //DGSAP-541
    //Check if the "New" option should be displayed
    if (!hasOpenQuote) {
        var def = document.createElement("option");
        def.text = 'New';
        def.value = '-1';
        ddl.add(def, i);
        ddl.options.length = i + 1;
    }

    if (selectedIndex >= 0 && ddl.options.length > 0) {
        ddl.selectedIndex = selectedIndex;
    }
    else if (!hasOpenQuote && ddl.options.length > 1) {
        ddl.selectedIndex = ddl.options.length - 2;
    } else {
        ddl.selectedIndex = ddl.options.length - 1;
    }
}

function SetSelectedQuote(quoteId,  refreshQuote) {
    var ddl = document.getElementById('ddlQuoteID');
    var count = ddl.options.length;
    if (count > 0) {
        for (var i = 0; i < count; i++) {
            if (ddl.options[i].value == quoteId) {
                ddl.selectedIndex = i;
                break;
            }
        }
        SelectedQuoteChanged(0);
    }
}

//      Funtion to set the CSS of control buttons
function SwitchQuoteTab() {
    switch (Quote_curTab) {
        case 1: //pnlQuotesDescriptionContainer
            document.getElementById('tabQuoteDetails').className = 'jobTab activeTab';
            document.getElementById('tabQuoteNotes').className = 'jobTab';
            document.getElementById('tabQuoteDocuments').className = 'jobTab';

            document.getElementById('pnlQuotesDescriptionContainer').className = 'width-full';
            document.getElementById('pnlQuotesNotesContainer').className = 'hidden';
            document.getElementById('pnlQuotesDocumentContainer').className = 'hidden';

            break;
        case 2:
            document.getElementById('tabQuoteDetails').className = 'jobTab';
            document.getElementById('tabQuoteNotes').className = 'jobTab activeTab';
            document.getElementById('tabQuoteDocuments').className = 'jobTab';

            document.getElementById('pnlQuotesDescriptionContainer').className = 'hidden';
            document.getElementById('pnlQuotesNotesContainer').className = 'width-full';
            document.getElementById('pnlQuotesDocumentContainer').className = 'hidden';

            break;//pnlQuotesNotesContainer
        case 3:
            document.getElementById('tabQuoteDetails').className = 'jobTab';
            document.getElementById('tabQuoteNotes').className = 'jobTab';
            document.getElementById('tabQuoteDocuments').className = 'jobTab activeTab';

            document.getElementById('pnlQuotesDescriptionContainer').className = 'hidden';
            document.getElementById('pnlQuotesNotesContainer').className = 'hidden';
            document.getElementById('pnlQuotesDocumentContainer').className = 'width-full';

            break;
        default:
            document.getElementById('tabQuoteDetails').className = 'jobTab';
            document.getElementById('tabQuoteNotes').className = 'jobTab';

            document.getElementById('pnlQuotesDescriptionContainer').className = 'hidden';
            document.getElementById('pnlQuotesNotesContainer').className = 'hidden';
            break;
    }
}

//      Function wrapper to reload the currently selected Quote
function QuoteItemEdit_Update(){
    Quote_Load(ObjSelectedQuote.Id);
}

// Function that confirms the deletion of a pending quote
function DeletePendingQuote() {
    var c;
    if (ObjJob.Quotes.length > 1) {
        c = confirm('Are you sure you want to delete this quote?');
    } else {
        c = confirm('Deleting the quote will not delete the enquiry related to this quote. If you do not cancel or close the enquiry it will still be displayed on the dashboard. Are you sure you want to delete this quote?');
    }
    if (c) {
        DelPendingQuoteConfirmed();
    }

}

// function that performs the call for actual pending quote deletion
function DelPendingQuoteConfirmed() {
    var retVal = false;
    try {
        retVal = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'DeletePendingQuote',
            {
                Quoteid: ObjSelectedQuote.Id
            },
            false);

        Job_Refresh(ObjJob.Id);

    } catch (err) {
        DisplayMessageDialog('Error', 'Unable to delete pending Quote - ' + err.toString());
    }

    return retVal;
}

//      Function to Mark a Quote and all work as cancelled
function CancelQuote() {
    var r = confirm("This will cancel the work for the quote and the quote itself. Are you sure you want to proceed?");
    if (r) {
        // Call the change quote status method sending in 2 == Types.Enums.QuoteStatusEnum.Cancelled
        ChangeQuoteStatus(2);
    }
}
/*

        **********  Dialog Functions    **********

*/

//      Function to open the edit dialog for a quote item
function EditQuoteItem(itemId) {
    EditJobItemForStage(itemId, 'Quote', QuoteItemEdit_Update);
}

//      Function called to open the QuoteItemsDialog
function QuoteItemsDialog_Open(JobSiteID) {
    function buildQuoteEditSourceObject() {
        jsId = JobSiteID;
        return {
            type: "POST",
            datatype: "json",
            datafields: [
                { name: 'Id', type: 'int' },
                { name: 'OrderId' },
                { name: 'Code' },
                { name: 'Name' },
                { name: 'Description' },
                { name: 'Value', type: 'float' },
                { name: 'VatRate', type: 'float' }
            ],
            // The DataAdapter and table need an Id property to uniquely identify a row.
            id: 'Id',
            url: Dialog_strAppRoot + 'Services/Web/WorkFlow.asmx/GetJobQuoteItemList',
            // We don't want to cache the data
            cache: false,
            root: 'data',
            // The following functions are called when items are added, updated and deleted in the table
            addRow: function (rowID, rowData, position, commit)
            {
                AddRow(rowID, rowData, position, commit);
            },
            updateRow: buildQuoteEditUpdateRowAction(),
            deleteRow: buildQuoteEditDeleteRowAction()
        };
    };

    function buildQuoteEditDeleteRowAction() {
        return function (rowID, commit) {
            DeleteFunction(rowID, commit);
        }
    };

    function buildQuoteEditUpdateRowAction() {
        return function(rowID, rowData, commit) {
            EditSaveFunction(rowID, rowData, commit);
        };
    }

    // returns the 'Description' column property metadata for the Quote Edit datatable
    function buildQuoteEditDescriptionColumn() {
        return {
            text: 'Description', dataField: 'Description', width: 295,
            columnType: 'custom',
            createEditor: function (row, cellvalue, editor, cellText, width, height) {
                // construct the editor. 
                editor.jqxComboBox({
                    source: jqxDescriptionDataAdapterForEditor('Item', function () {
                        editor.jqxComboBox('val', cellvalue);
                    }),
                    autoComplete: true,
                    searchMode: 'containsignorecase',
                    displayMember: 'Item',
                    valueMember: 'Item',
                    width: width,
                    height: height,
                    dropDownWidth: 500
                });
            },
            initEditor: function (row, cellvalue, editor, celltext, width, height) {
                // set the editor's current value. The callback is called each time the editor is displayed.
                editor.jqxComboBox({ width: width, height: height });
                editor.val(cellvalue);
            },
            getEditorValue: function (row, cellvalue, editor) {
                // return the editor's value.
                return editor.val();
            }

        };
    }

    // returns the 'Name' column property metadata for the Quote Edit datatable
    function buildQuoteEditNameColumn() {
        return {
            text: 'Name', editable: true, dataField: 'Name', width: 170,
            columnType: 'custom',
            createEditor: function (row, cellvalue, editor, cellText, width, height) {
                // construct the editor. 
                editor.jqxComboBox({
                    source: jqxTreeDataAdapterForEditor('Common', function () {
                        editor.jqxComboBox('val', cellvalue);
                    }),
                    autoComplete: true,
                    searchMode: 'containsignorecase',
                    displayMember: 'Common',
                    valueMember: 'Common',
                    width: width,
                    height: height,
                    dropDownWidth: 400
                });
            },
            initEditor: function (row, cellvalue, editor, celltext, width, height) {
                // set the editor's current value. The callback is called each time the editor is displayed.
                editor.jqxComboBox({ width: width, height: height });
                editor.val(cellvalue);
            },
            getEditorValue: function (row, cellvalue, editor) {
                // return the editor's value.
                return editor.val();
            }

        };
    }


    if (Markers.company == null) {
        Markers.company = Ajax_CallService(
            "Services/Web/Company.asmx",
            "GetCompany",
            {},
            false);
    }

    //Get the quote Id to send through for get method
    QuoteID = ObjSelectedQuote.Id;
    JobSiteBeingEdited = JobSiteID;

    // The data adapter used to populate the table of items in the dialog
    var dataAdapter;

    // Initialise the dialog if it hasn't already been initialised
    if (QuoteItemsDialogInitialized == false) {


        // Set up the source to retrieve the QuoteItems from the service.
        // Note that we only need to declare the fields which we are going to use in the table
        var source = buildQuoteEditSourceObject();

        // Create a dataAdapter from the source
        var dataAdapter = new $.jqx.dataAdapter(source, {
            contentType: 'application/json; charset=utf-8',
            // Add the parameters to the call to the service
            formatData: function (data) {
                // Note that we need to return a JSON string here, not a JSON object
                return JSON.stringify({
                    JobSiteID: JobSiteBeingEdited,
                    QuoteID: QuoteID
                });
            },
            // We need to save the raw QuoteItem objects into our own array, as the jqWidgets don't manage this for us.
            downloadComplete: function (data, textStatus, jqXHR) {
                QuoteItems = [];
                data.d.forEach(function (item, index) {
                    if (item.JobSiteId == JobSiteBeingEdited) {
                        QuoteItems.push(item);
                    }
                });

                return QuoteItems;
            }
        });

        // Set up the jqWindow in which we will display the QuoteItems
        $("#winQuoteItems").jqxWindow({
            // Specify the theme - we have to do this for every jqWidget - there's no way to define a default theme
            theme: 'arbpro',
            height: 430,
            width: 800,
            resizable: false,
            isModal: true,
            // Specify the OK and Cancel buttons - their Ids are defined in the HTML template for the jqWindow
            okButton: $('#btnWinQuoteItemsOk'),
            cancelButton: $('#btnWinQuoteItemsCancel'),
            // Specify the function called to initialise the content of the jqWindow
            initContent: function () {
                $('#btnWinQuoteItemsOk').jqxButton({ theme: 'arbpro', width: '65px' });
                $('#btnWinQuoteItemsCancel').jqxButton({ theme: 'arbpro', width: '65px' });
                $('#btnWinQuoteItemsOk').focus();

                // Set up the DataTable
                $("#tblQuoteItems").jqxDataTable({
                    theme: 'arbpro',
                    width: '99%',
                    height: '350',
                    // The source is the dataAdapter created above
                    source: dataAdapter,
                    // Setting pageable to false makes it display scrollbars
                    pageable: false,
                    editable: true,
                    showToolbar: true,
                    altrows: true,
                    toolbarHeight: 35,
                    autoRowHeight: true,
                    // Specify the columns
                    columns: [
                        { text: 'Item Order', editable: true, dataField: 'OrderId', width: 82 },
                        { text: 'Ref', editable: true, dataField: 'Code', width: 65 },
                        buildQuoteEditNameColumn(),
                        buildQuoteEditDescriptionColumn(),
                        { text: 'Value', editable: true, dataField: 'Value', width: 80, cellsFormat: 'F2', cellsalign: 'right' },
                        { text: 'VAT Rate', editable: true, dataField: 'VatRate', width: 70, cellsFormat: 'F2', cellsalign: 'right' },
                    ],
                    // Set up the code to call to render the toolbar
                    renderToolbar: function (toolBar) {
                        // renderJqxDataTableToolbar is a utility function which can be called for any DataTable
                        renderJqxDataTableToolbar(toolBar, "#tblQuoteItems");
                    }
                });
            }
        });

        // Set up the close event for the dialog, to update the QuoteItems if the OK button was clicked
        $('#winQuoteItems').on('close', function (event) {
            if (event.args.dialogResult.OK) {
                QuoteItems_update();

                // Clear down the arrays
                QuoteItems = [];
                QuoteItemsUpdated = [];
                QuoteItemIdsDeleted = [];
            }
        });

        // Remember the fact that we've initialized the dialog
        QuoteItemsDialogInitialized = true;
    }
    else {
        // The dialog has been initialized already - we need to refresh the data adapter with the current JobSiteID by 
        // calling the udpdateBoundData method
        $("#tblQuoteItems").jqxDataTable('updateBoundData');

        // Open the dialog
        $('#winQuoteItems').jqxWindow('open');
    }
}


function EditSaveFunction(rowID, rowData, commit) {
    var success = true;
    try {
        var RecId = parseInt(rowID);

        // Look in the updated list first
        var Rec = $.grep(QuoteItemsUpdated, function (e) { return e.Id === RecId; });
        var Rec2 = $.grep(QuoteItems, function (e) { return e.Id === RecId; });
        if (Rec.length == 1) {
            // Remove this one from the updated list - we're going to replace it below
            QuoteItemsUpdated.pop(Rec[0]);

            // update its properties with the new values
            Rec[0].OrderId = rowData.OrderId;
            Rec[0].Code = rowData.Code;
            Rec[0].Name = rowData.Name;
            Rec[0].Description = rowData.Description;
            Rec[0].Value = rowData.Value;
            Rec[0].VatRate = rowData.VatRate;

            // Add to the updated list
            QuoteItemsUpdated.push(Rec[0]);
        } else if (Rec2.length == 1) {
            // update its properties with the new values
            Rec2[0].OrderId = rowData.OrderId;
            Rec2[0].Code = rowData.Code;
            Rec2[0].Name = rowData.Name;
            Rec2[0].Description = rowData.Description;
            Rec2[0].Value = rowData.Value;
            Rec2[0].VatRate = rowData.VatRate;

            // Add to the updated list
            QuoteItemsUpdated.push(Rec2[0]);
        }
    } catch (e) {
        success = false;
    }
    commit(success);
}

function AddRow(rowID, rowData, position, commit) {
    rowData.VatRate = Markers.company.VatRate;
    if (!rowData.OrderId) {
        rowData.OrderId = QuoteItems.length + QuoteItemsUpdated.length + 1;
    }

    var tablePanel = document.getElementById('tblQuoteItems');
    var okBtn = document.getElementById('btnWinQuoteItemsOk');
    var cancBtn = document.getElementById('btnWinQuoteItemsCancel');
    var addPanel = document.getElementById('tblQuoteItemsAdd');
    tablePanel.className = "hidden";
    okBtn.className = "hidden";
    cancBtn.className = "hidden";
    addPanel.className = "";
    var strHtml = '';
    strHtml += '<table class="searchGrid padded">';
    strHtml += '<tr>';
    strHtml += '<th style="width:7%;text-align: center;"> Item Order </th>';
    strHtml += '<th style="width:7%;text-align: center;"> Tree No. </th>';
    strHtml += '<th style="width:18%;text-align: center;"> Tree Name </th>';
    strHtml += '<th style="width:38%;text-align: center;"> Description </th>';
    strHtml += '<th style="width:13%;text-align: center;"> Value </th>';
    strHtml += '<th style="width:12%;text-align: center;"> VAT Rate </th>';
    strHtml += '</tr>';
    strHtml += '<tr>';
    strHtml += '<td style="width:7%; vertical-align:top;">';
    strHtml += '<textarea id="txtItmOrderId" class="width-full" TextMode="MultiLine"  style="width:95%;" name="txtItmOrderId">' + rowData.OrderId + '</textarea>';
    strHtml += '</td>';
    strHtml += '<td style="width:7%; vertical-align:top;">';
    strHtml += '<textarea id="txtItmTreeNo" class="width-full" TextMode="MultiLine" style="width:95%;" name="txtItmTreeNo"></textarea>';
    strHtml += '</td>';
    strHtml += '<td style="width:20%; vertical-align:top;">';
    strHtml += '<textarea id="txtItmTreeName" class="width-full" TextMode="MultiLine" style="width:90%;" name="txtItmTreeName"></textarea>';
    strHtml += '</td>';
    strHtml += '<td style="width:40%; vertical-align:top;">';
    strHtml += '<textarea id="txtItmDescription" class="width-full" TextMode="MultiLine" Rows="6" style="width:95%;" name="txtItmDescription"></textarea>';
    strHtml += '</td>';
    strHtml += '<td style="width:15%; vertical-align:top;">';
    strHtml += '<textarea id="txtItmValue" class="float-right" TextMode="MultiLine" style="width:80%;" value="0" name="txtItmValue"></textarea>';
    strHtml += '<span class="float-right" style="padding-top: 7px; padding-right: 4px;"> ' + CurrentCurrency + ' </span>';
    strHtml += '</td>';
    strHtml += '<td style="width:13%; vertical-align:top;">';
    strHtml += '<span class="float-right" style="padding-top: 7px; padding-right: 3px;"> % </span>';
    strHtml += '<textarea id="txtItmVatRate" class="float-right" TextMode="MultiLine" style="width:70%;" value="0" name="txtItmVatRate">' + rowData.VatRate + '</textarea>';
    strHtml += '</td>';
    strHtml += '</tr>';
    strHtml += '</table>';
    strHtml += '<button type="button" onclick="Cancelled();" class="button-thin padded float-right">Cancel</button>';
    strHtml += '<button type="button" onclick="Saved(' + rowID + ');" class="button-thin padded float-right">Save</button>';
    addPanel.innerHTML = strHtml;

    //Setup the lookup lists for the pick items
    AutoComplete('txtItmTreeName', 'ListTreeNames');
    AutoComplete('txtItmDescription', 'ListWorkDescriptions');
}


// function that saves added item
// note - this closes and reopens the datatable
function Saved(rowID) {
    var rowData = [];
    var tablePanel = document.getElementById('tblQuoteItems');
    var okBtn = document.getElementById('btnWinQuoteItemsOk');
    var cancBtn = document.getElementById('btnWinQuoteItemsCancel');
    var addPanel = document.getElementById('tblQuoteItemsAdd');
    rowData.OrderId = document.getElementById('txtItmOrderId').value;
    rowData.Code = document.getElementById('txtItmTreeNo').value;
    rowData.Name = document.getElementById('txtItmTreeName').value;
    rowData.Description = document.getElementById('txtItmDescription').value;
    rowData.Value = document.getElementById('txtItmValue').value;
    rowData.VatRate = document.getElementById('txtItmVatRate').value;
    var l = QuoteItems.length;
    QuoteItemsUpdated.push({ 'Id': rowID, 'OrderId': rowData.OrderId, 'Code': rowData.Code, 'Name': rowData.Name, 'Description': rowData.Description, 'Value': rowData.Value, 'VatRate': rowData.VatRate });

    QuoteItems_update();

    // Clear down the arrays
    QuoteItems = [];
    QuoteItemsUpdated = [];
    QuoteItemIdsDeleted = [];

    tablePanel.className = "";
    okBtn.className = "";
    cancBtn.className = "";
    $('#btnWinQuoteItemsOk').jqxButton({ theme: 'arbpro', width: '65px' });
    $('#btnWinQuoteItemsCancel').jqxButton({ theme: 'arbpro', width: '65px' });
    $('#btnWinQuoteItemsOk').focus();
    addPanel.className = "hidden";

    $('#tblQuoteItems').jqxDataTable('endRowEdit', 0, true);
    if (l == 0) {
        setTimeout(function() { QuoteItemsDialog_Open(jsId); }, 200);
    } else {
        QuoteItemsDialog_Open(jsId);
    }
}

// function to return back to datatable without any changes commited
function Cancelled() {
    var tablePanel = document.getElementById('tblQuoteItems');
    var okBtn = document.getElementById('btnWinQuoteItemsOk');
    var cancBtn = document.getElementById('btnWinQuoteItemsCancel');
    var addPanel = document.getElementById('tblQuoteItemsAdd');
    tablePanel.className = "";
    okBtn.className = "";
    cancBtn.className = "";
    $('#btnWinQuoteItemsOk').jqxButton({ theme: 'arbpro', width: '65px' });
    $('#btnWinQuoteItemsCancel').jqxButton({ theme: 'arbpro', width: '65px' });
    $('#btnWinQuoteItemsOk').focus();
    addPanel.className = "hidden";
    $('#tblQuoteItems').jqxDataTable('endRowEdit', 0, true);
}

// function to delete an item from the data table
function DeleteFunction(rowID, commit) {
    var success = true;
    var RecId = parseInt(rowID);

    // Add the id of the current row to the list of ids deleted
    try {
        var RecUpdate = $.grep(QuoteItemsUpdated, function (i) { return i.Id === RecId; });
        var RecDelete = $.grep(QuoteItemIdsDeleted, function (b) { return b === RecId; });
        var Rec = $.grep(QuoteItems, function (e) { return e.Id === RecId; });
        if (RecDelete.length == 0) {
            QuoteItemIdsDeleted.push(RecId);
        }
        if (RecUpdate.length == 1) {
            QuoteItemsUpdated.pop(RecUpdate[0]);
        }
        if (Rec.length == 1) {
            QuoteItems.pop(Rec[0]);
        }
    } catch (e) {
        success = false;
    }
    commit(success);
}

function ShowDocumentsDialogOnQuote(docId) {

    Dialog_OpenPage('Associated Documents',
        'Dialogs/JobDocumentsAdd.aspx',
        'JobDocs',
        500,
        600,
        true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        PopulateQuoteDocuments();
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        PopulateQuoteDocuments();
                        $(this).dialog('close');
                    }
                }
            },
                    'Content/js/JobDocuments.js?V=3.23.5',
                    function () { LoadDateAndStaff(docId); }
        );
}

function ShowDocumentsDialogOnQuoteEdit(docId) {
    Dialog_OpenPage('Associated Documents',
        'Dialogs/JobDocumentsEdit.aspx',
        'JobDocsEdit',
        500,
        600,
        true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        File_UploadEdit();
                        PopulateQuoteDocuments();
                        $(this).dialog('close');
                    }
                },
                'Open': {
                    text: 'Open',
                    class: 'ui-dialog-button',
                    click: function () {
                        DocumentPreview()
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        PopulateQuoteDocuments();
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/JobDocuments.js?V=3.23.5',
            function () { JobDocumentsLoad(docId,0,-1); }
        );
}
// function to re-send an email
function ResendItems() {
    Dialog_OpenPage(
        'Produce Quote',
        'Dialogs/PDFGeneration.aspx',
        'content',
        900,
        600,
        true,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function() {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/QuoteGeneration.js?V=3.23.5',
        function() {
            PDFGeneration_Load(ObjSelectedQuote.Id, true);
        });
}

// Function to send emails for follow-up sales
function FollowUpSales() {
    Dialog_OpenPage(
        'Follow-up Sales',
        'Dialogs/PDFGeneration.aspx',
        'content',
        900,
        600,
        true,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/QuoteGeneration.js?V=3.23.5',
        function () {
            PDFGeneration_Load(ObjSelectedQuote.Id, true);
        });
}

// Function to display the send quote dialog
function ShowProduceQuoteDialog() {
    Dialog_OpenPage(
        'Produce Quote',
        'Dialogs/PDFGeneration.aspx',
        'content',
        900,
        600,
        true,
         {
             'Close': {
                 text: 'Close',
                 class: 'ui-dialog-button',
                 click: function () {
                     $(this).dialog('close');
                     if (ObjCompany.DisplayMarketing && ObjSelectedQuote.Status === 1 && ObjJob.PointOfContact.IncludeInMarketing) {
                         ClientMarketingNotAccepted();
                     }
                 }
             }
         },
        'Content/js/QuoteGeneration.js?V=3.23.5',
        function () { PDFGeneration_Load(ObjSelectedQuote.Id); });
}

// Function to display the edit tools dialog
function EditTools() {
    if (ObjSelectedQuote && ObjSelectedQuote.Id > 0) {
        Dialog_OpenPage(
            'Edit Tools',
            'Dialogs/CalendarBookWork.aspx?',
            'content',
            550,
            630,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function() {
                        if (ToolsSave()) {
                            //Force the page to reload
                            $(this).dialog('close');
                        }
                    }
                },
                'Close': {
                    text: 'Cancel',
                    class: 'ui-dialog-button',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/CalendarBookWork.js?V=3.23.5',
            function () {
                GetWpId(ObjSelectedQuote.Id, ObjSelectedQuote.JobSiteId);
            }
        );
    }
}

// Function to display the edit tools dialog
function ClientMarketing() {
    if (ObjJob && ObjJob.Id > 0) {
        if (ObjJob.PointOfContact.IncludeInMarketing) {
            Dialog_OpenPage(
                'Client Marketing',
                'Dialogs/MarketingActionPlatform.aspx?',
                'content',
                800,
                630,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function() {
                            if (Save()) {
                                UpdateComplaintTotal();
                                $(this).dialog('close');
                            }
                        }
                    },
                    'Close': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function() {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/MarketingActionPlatform.js?V=3.23.5',
                function() {
                    LoadWorkCategoriesForClient(ObjJob.ClientId, ObjJob.Id, ObjJob.PointOfContact.IncludeInMarketing);
                }
            );
        } else {
            Dialog_OpenPage(
                'Client Marketing',
                'Dialogs/MarketingActionPlatform.aspx?',
                'content',
                800,
                630,
                true,
                {
                    'Close': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/MarketingActionPlatform.js?V=3.23.5',
                function () {
                    LoadWorkCategoriesForClient(ObjJob.ClientId, ObjJob.Id, ObjJob.PointOfContact.IncludeInMarketing);
                }
            );
        }
    }
}

// Function to display the edit tools dialog
function ClientMarketingNotAccepted() {
    if (ObjJob && ObjJob.Id > 0) {
        if (ObjJob.PointOfContact.IncludeInMarketing) {
            Dialog_OpenPage(
                'Client Marketing',
                'Dialogs/MarketingActionPlatform.aspx?',
                'content',
                800,
                630,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (Save()) {
                                UpdateComplaintTotal();
                                $(this).dialog('close');
                            }
                        }
                    },
                    'Close': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/MarketingActionPlatformNotAccepted.js?V=3.23.5',
                function () {
                    LoadWorkCategoriesForClient(ObjJob.ClientId, ObjJob.Id, ObjJob.PointOfContact.IncludeInMarketing);
                }
            );
        } else {
            Dialog_OpenPage(
                'Client Marketing',
                'Dialogs/MarketingActionPlatform.aspx?',
                'content',
                800,
                630,
                true,
                {
                    'Close': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/MarketingActionPlatform.js?V=3.23.5',
                function () {
                    LoadWorkCategoriesForClient(ObjJob.ClientId, ObjJob.Id, ObjJob.PointOfContact.IncludeInMarketing);
                }
            );
        }
    }
}

// function to remove client work categories for the selected job - useful if quote or job have been cancelled.
function UpdateAcceptedWorkClientCategoriesForJobId(jobId) {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'UpdateAcceptedWorkClientCategoriesForJobId',
        {
            jobId: jobId
        },
        false
    );

    return catList;
}

// function to open the print work schedule window
function WorkSchedulePrint() {
    if (ObjSelectedQuote && ObjSelectedQuote.Id > 0) {
        Dialog_OpenPage(
            'Work Schedule',
            'Dialogs/PDFWorkScheduleGeneration.aspx',
            'content',
            700,
            630,
            true,
            {
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function() {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/WorkPackGeneration.js?V=3.23.5',
            function() {
                PDFGeneration_Load(ObjSelectedQuote.Id, true);
            });
    }
}

//      Function to display the TPO details for the selected quote
function OpenTreeProtection() {
    if (ObjSelectedQuote && ObjSelectedQuote.Id > 0) {
        Dialog_OpenPage(
            'Tree Protection',
            'Dialogs/TreeProtection.aspx?QuoteID=' + ObjSelectedQuote.Id.toString(),
            'content',
            747,
            580,
            true,
            {
                'Print schedule': {
                    text: 'Print schedule',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                        Tpo_Save();
                        PrintWorkSchedule();
                    }
                },
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                        Tpo_Save();
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
           'Content/js/TreeProtection.js?V=3.23.5',
           function () {
               debugger;
               Tpo_Load(ObjSelectedQuote.Id,ObjJob.WorkSites);
           }
           );
    } else {
        DisplayMessageDialog('Tree Protection', 'The quote must be created, and items added before editing TPO details.');
    }
}

//      Function to display the TPO details for the selected quote
function OpenPermitsNecessery() {
    if (ObjSelectedQuote && ObjSelectedQuote.Id > 0) {
        Dialog_OpenPage(
            'Permits Necessary',
            'Dialogs/PermitsNecessary.aspx',
            'content',
            600,
            400,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                        PermitsNecessary_Save();
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
           'Content/js/PermitsNecessary.js?V=3.23.5',
           function () {
               // debugger;
               Permits_Load(ObjSelectedQuote.Id);
           }
           );
    } else {
        DisplayMessageDialog('Tree Protection', 'The quote must be created, and items added before editing TPO details.');
    }
}

//      Function to display a map dialog with the position of all items
function ShowItemMap(jobSiteId) {
    Dialog_OpenPage(
       'Site Map',
       'Dialogs/ItemMap.aspx',
       'content',
       600,
       700,
       true,
       {
           'Print': {
               text: 'Print',
               class: 'ui-dialog-button',
               click: function () {
                   PrintQuoteItemMap();
               }
           },
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/QuoteItemMap.js?V=3.23.5',
       function () { QuoteItemMap_Load(ObjSelectedQuote.Id, jobSiteId); });
}


// Function to display a message to the user
function DisplayAlertMessage(message) {
    Dialog_OpenPage(
        'Duplicate',
        'Dialogs/Alert.aspx',
        'content',
        500,
        150,
        true,
        {
            'Yes': {
                text: 'Yes',
                "class": 'ui-dialog-button',
                click: function () {
                    Duplicate(true);
                    $(this).dialog('close');
                }
            },
            'No': {
                text: 'No',
                "class": 'ui-dialog-button',
                click: function () {
                    Duplicate(false);
                    $(this).dialog('close');
                }
            },
            'Cancel': {
                text: 'Cancel',
                "class": 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/AlertBox.js?V=3.23.5',
        function () {
            AlertBox_Show(message);
        });
}

function DisplayPurchaseOrderNo(QuoteId)
{
    Dialog_OpenPage(
                   'Purchase Order Number',
                   'Dialogs/PurchaseOrderNoEdit.aspx?QuoteId=' + QuoteId,
                   'content',
                   700,
                   300,
                   true,
                   {
                       'Delete': {
                           text: 'Delete',
                           class: 'ui-dialog-button',
                           click: function () {
                               if (confirm('Are you sure you want to delete this entry?')) {
                                   retVal = Ajax_CallService(
                                      'Services/Web/WorkFlow.asmx',
                                      'DeletePONoFromQuote',
                                       {
                                           QuoteID: QuoteId
                                       },
                                      false);
                                   if (retVal == -1) {
                                       alert('Error in database, please try again.');
                                   }
                                   else
                                   {
                                       $(this).dialog('close');
                                   }
                                  
                               }

                           }
                       },
                       'Save': {
                           text: 'Save',
                           class: 'ui-dialog-button',
                           click: function () {
                               var PONo = document.getElementById('txtPONo').value
                               retVal = Ajax_CallService(
                                      'Services/Web/WorkFlow.asmx',
                                      'SavePONoOnQuote',
                                       {
                                           QuoteID: QuoteId,
                                           PONo: PONo
                                       },
                                      false);
                               if (retVal == -1) {
                                   alert('Error in database, please try again.');
                               }
                               else {
                                   $(this).dialog('close');
                               }
                           }
                       },
                       'Close': {
                           text: 'Close',
                           class: 'ui-dialog-button',
                           click: function () {
                               $(this).dialog('close');
                           }
                       }
                   },
                   null,
                   function () {
                       retVal = Ajax_CallService(
                               'Services/Web/WorkFlow.asmx',
                               'GetOnlyQuote',
                                {
                                    QuoteID: QuoteId
                                },
                               false);
                      document.getElementById('txtPONo').value = retVal.PurchaseOrderNumber;
                   });
}

function AttachDocDialogForQuote() {
    
    var modal = document.getElementById('DocAttachModel');
    $("#DocAttachModel").removeClass("hidden");
    //ShowDocLoadingForQuote(false);
   $('#pnlDocloading').hide();
    PopulateDocumentsForQuote();
    var iHeight = 500;
    var iWidth = 600;
    var iAdjustedHeight = Math.min(iHeight, screen.availHeight);
    if (iAdjustedHeight < iHeight) {
        iAdjustedHeight -= 100;
    }

    var iAdjustedWidth = Math.min(iWidth, screen.availWidth);
    if (iAdjustedWidth < iWidth) {
        iAdjustedWidth -= 80;
    }
    var strTitle = "Associated Documents"
    $("#DocAttachModel").dialog({
        autoOpen: false,
        title: strTitle,
        modal: true,
        height: iAdjustedHeight,
        width: iAdjustedWidth,
        resizable: false,
        draggable: true,
        show: {
            effect: 'fade',
            duration: 500
        },
        hide: {
            effect: 'fade',
            duration: 200,
        },
        buttons: {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    AddDocAsAttacmentToEmail($(this));
                    
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    //Close the dialog and reload the tab to display the added items
                    $("#DocAttachModel").addClass("hidden");
                    DocList = [];
                    $(this).dialog('destroy');
                }
            }
        }
    });

    $('#DocAttachModel').dialog('open');
}

function PopulateDocumentsForQuote()
{
    var Grid = document.getElementById('grdDoc');
    var listHtml = '<table class="searchGrid width-full"><tr><th>Document Name</th><th>Action</th></tr>';
    var JobId = getParameterByName("JobID");
    var JobDocs = Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'getDefaultJobDocuments',
                {
                    JobId: JobId
                },
                false
                );
    JobDocs.forEach(function (c) {
        listHtml = listHtml + '<tr><td>' + c.DocumentName + '</td>';
        listHtml = listHtml + '<td  style="width:21%; vertical-align: middle; text-align:center;"><input type="checkbox" checked="checked" id="chkDefaultAttachment" onChange="ToggleSingleAttachment(\'' + c.Id + '_QuoteDoc\',this);" id="(' + c.Id + ');"></td></tr>';
        AddDoc(c.Id + '_QuoteDoc');
    });
    listHtml = listHtml + '</table>';
    Grid.innerHTML = listHtml;
    var CheckAssesmentGen = false;
    ObjJob.WorkSites.forEach(function (js) {
        Validate = Ajax_CallService(
                   'Services/Web/RiskAssessment.asmx',
                   'ValidateAssessmentForSite',
                   {
                       quoteId: ObjSelectedQuote.Id,
                       jobSiteId: js.Id
                   },
                   false
               );
        if (Validate.length == 0) {
            CheckAssesmentGen = true;
        }

    });
    Grid = document.getElementById('grdArbProDoc');
    listHtml = '';
    listHtml = '<table class="searchGrid width-full"><tr><th>Document Name</th><th>Action</th></tr>';
    if (CheckAssesmentGen) {
        listHtml = listHtml + '<tr><td>Risk Assessment</td>';
        listHtml = listHtml + '<td style="width:21%; vertical-align: middle; text-align:center;"><input type="checkbox" onChange="ToggleRiskAssessment(this);"></td></tr>';
    }
    else {
        listHtml = listHtml + '<tr><td>Risk Assessment(<span style="color:red;">No Doc, go create the document</span>)</td>';
        listHtml = listHtml + '<td style="width:21%; vertical-align: middle; text-align:center;"><input type="checkbox" disabled="disabled"></td></tr>';
        includeRiskAssessment = false;
    }
    listHtml = listHtml + '<tr><td>Work schedule and tree pictures</td>';
    listHtml = listHtml + '<td style="width:21%; vertical-align: middle; text-align:center;"><input type="checkbox"  onChange="ToggleTreePictures(this);"></td></tr>';
    listHtml = listHtml + '<tr><td>Site Plan</td>';
    listHtml = listHtml + '<td style="width:21%; vertical-align: middle; text-align:center;"><input type="checkbox" onChange="ToggleSitePlan(this);"></td></tr>';
    listHtml = listHtml + '</table>';
    Grid.innerHTML = listHtml;
 }

function ToggleTreePictures(ActionElement) {
    if (ActionElement.checked) {
        includeTreePicture = true;
    }
    else {
        includeTreePicture = false;
    }
}

function ToggleSitePlan(ActionElement) {
    if (ActionElement.checked) {
        includeSitePlan = true;
    }
    else {
        includeSitePlan = false;
    }
}

function ToggleRiskAssessment(ActionElement) {
    if (ActionElement.checked) {
        includeRiskAssessment = true;
    }
    else {
        includeRiskAssessment = false;
    }
}
function ToggleSingleAttachment(DocId, ActionElement)
{
    if (ActionElement.checked) {
        AddDoc(DocId);
    }
    else
    {
        RemoveDoc(DocId);
    }
    console.log(DocList);
}

function RemoveDoc(DocId)
{
    var index = DocList.indexOf(DocId);
    if (index > -1) {
        DocList.splice(index, 1);
    }
}

function AddDoc(DocId)
{
    DocList.push(DocId);
}

function AddDocAsAttacmentToEmail(DialogRef)
{
    $('#pnlDocloading').show();
    if (includeSitePlan == true) {
        includeSitePlan = false;
        var FileNo = 1;
        ObjJob.WorkSites.forEach(function (js) {
            var result = Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'GetQuoteSiteMap',
                {
                    JobSiteID: js.Id,
                    QuoteID: ObjSelectedQuote.Id
                },
                false
              );


            if (result && result.Id > 0) {
                var CurDetails = result;
                console.log(CurDetails);
                if (CurDetails.SitePostCode && CurDetails.SitePostCode.length > 0) {
                    var loc;
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ 'address': CurDetails.SitePostCode + ', UK' },
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                loc = results[0].geometry.location;
                                var SiteMapURL = ExportSiteMapURL(loc, CurDetails.SitePostCode);
                                console.log(SiteMapURL);
                                MapImage = DownloadGoogleMap(SiteMapURL);
                                var base64String = btoa([].reduce.call(new Uint32Array(MapImage.Bytes), function (p, c) { return p + String.fromCharCode(c) }, ''));
                                //console.log(base64String);
                                var Doc = Ajax_CallService(
                                   'Services/Web/Report.asmx',
                                   'AddTempDocument',
                                   {
                                       ItemID: ObjSelectedQuote.Id,
                                       Description: "Temporary Document For Email Attachment",
                                       ContentType: "png",
                                       ByteString: base64String,
                                       FilePath: "Site Plan-" + FileNo.toString() + ".png"

                                   },
                                   false);
                                SetSelected({ id: Doc.Id, type: Doc.DocumentTypeID }, true);
                                if (FileNo == ObjJob.WorkSites.length) {
                                    otherDocAsAttachmentToEmail(DialogRef);
                                }
                                FileNo += 1;
                            }
                        });

                }
            }

        });


    }
    else {
          otherDocAsAttachmentToEmail(DialogRef);
    }
    
}

function otherDocAsAttachmentToEmail(DialogRef) {
    AttachDocumentsForQuote();
    if (includeTreePicture == true) {
        includeTreePicture = false;
        createSitePlanDocForEmail();
    }
    if (includeRiskAssessment) {
        includeRiskAssessment = false;
        CreateRiskAssessmentDocForEmail();
    }
    LoadAttachments();
    DocList = [];
    
    $("#DocAttachModel").addClass("hidden");
    includeTreePicture = false;
    includeSitePlan = false;
    includeRiskAssessment = false;
    DialogRef.dialog('destroy');
    $('#pnlDocloading').hide();
}
function AttachDocumentsForQuote() {
    var AllDocList = [];
    for (var DocListIndex = 0; DocListIndex < DocList.length; DocListIndex++) {
        var id = DocList[DocListIndex];
        if (id.indexOf("_QuoteDoc") != -1) {
            var DocId = id.replace("_QuoteDoc", "");
            if (DocId > 0) {
                AllDocList.push(DocId);
                SetSelected({ id: DocId, type: 4 }, true);
            }
        }
    }
    if (AllDocList.length > 0) {
        var Document_File = Ajax_CallService(
            'Services/Web/Report.asmx',
            'AddJobDocumentToAttachment',
            {
                DocIdList: AllDocList
            },
            false
        );
        return true;
    }
    return false;
}
function DownloadGoogleMap(MapURL) {
    var Doc = Ajax_CallService(
                'Services/Web/Report.asmx',
                'DownloadGoogleImage',
                {
                    MapUrl: MapURL,
                },
                false
              );

    return Doc;
}
function createSitePlanDocForEmail() {
    var ListSites = Ajax_CallService(
      'Services/Web/WorkFlow.asmx',
      'GetWorkSitesForQuote',
      {
          quoteId: ObjSelectedQuote.Id
      },
      false);
    var DocNo = 1;
    ListSites.forEach(function (js) {
        console.log("Site Id:" + js.Id);
        var ObjPdfSettings = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetWorkPdfSetting',
        {},
        false
        );
        ObjPdfSettings.Id = ''+js.Id+'';
        ObjPdfSettings.ReportParentType = 4;
        ObjPdfSettings.GenerationEnum = 0;
        ObjPdfSettings.ShowPictures = true;
        ObjPdfSettings.QuoteId = ObjSelectedQuote.Id;
        ObjPdfSettings.DocNo = DocNo;
        console.log(ObjPdfSettings);
        Ajax_CallService(
                'Services/Web/Report.asmx',
                'AttachWorkPackLetter',
                {
                    settings: ObjPdfSettings,
                },
                false
            );
        DocNo += 1;
        
    });

    var Docs = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetTempDocuments',
        {
            ItemID: ObjSelectedQuote.Id
        },
        false);
     Docs.forEach(function (file) {
        if (file.ClientID == -5) {
            SetSelected({id: file.Id, type: file.DocumentTypeID}, true);
        }
    });
}
function CreateRiskAssessmentDocForEmail() {
    var ListSites = Ajax_CallService(
      'Services/Web/WorkFlow.asmx',
      'GetWorkSitesForQuote',
      {
          quoteId: ObjSelectedQuote.Id
      },
      false);
    var DocNo = 1;
    ListSites.forEach(function (js) {
        Validate = Ajax_CallService(
                   'Services/Web/RiskAssessment.asmx',
                   'ValidateAssessmentForSite',
                   {
                       quoteId: ObjSelectedQuote.Id,
                       jobSiteId: js.Id
                   },
                   false
               );
        if (Validate.length == 0) {
            var ObjPdfSettings = Ajax_CallService(
            'Services/Web/Report.asmx',
            'GetWorkPdfSetting',
            {},
            false
            );
            var WorkPlanId = Ajax_CallService(
            'Services/Web/Report.asmx',
            'GetWorkPlanId',
            {
                qId: ObjSelectedQuote.Id,
                jsId: js.Id
            },
            false
            );
            ObjPdfSettings.Id = '' + js.Id + '';
            ObjPdfSettings.ReportParentType = 5;
            ObjPdfSettings.GenerationEnum = 1;
            ObjPdfSettings.ShowPictures = true;
            ObjPdfSettings.QuoteId = WorkPlanId;
            ObjPdfSettings.DocNo = DocNo;
            Ajax_CallService(
                'Services/Web/Report.asmx',
                'AttachRiskAssessmentDoc',
                {
                    settings: ObjPdfSettings,
                },
                false
            );
            DocNo += 1;
        }
        
    });
    var Docs = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetTempDocuments',
        {
            ItemID: ObjSelectedQuote.Id
        },
        false);
    Docs.forEach(function (file) {
        if (file.ClientID == -6) {
            SetSelected({ id: file.Id, type: file.DocumentTypeID }, true);
        }
    });
}
function ExportSiteMapURL(Loc,PostalCode) {
    var mapOptions = {
        center: new google.maps.LatLng(54, -2),
        zoom: 18,
        mapTypeId: google.maps.MapTypeId.SATELLITE
    };
    //URL of Google Static Maps.
    var staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap";

    //Set the Google Map Center.
    staticMapUrl += "?center=" + PostalCode + ",UK";

    //Set the Google Map Size.
    staticMapUrl += "&size=800x600";

    //Set the Google Map Zoom.
    staticMapUrl += "&zoom=" + mapOptions.zoom;

    //Set the Google Map Type.
    staticMapUrl += "&maptype=" + mapOptions.mapTypeId;

    //Loop and add Markers.
    staticMapUrl += "&markers=color:red|" + Loc.lat() + "," + Loc.lng();
       
    return staticMapUrl;
}
function Load_Documents(id) {
    if (id.indexOf("_CompanyDoc") != -1) {
        var DocId = id.replace("_CompanyDoc","");
        console.log("Company Doc Id:" + DocId);
        if (DocId > 0) {
            var Document_File = Ajax_CallService(
                     'Services/Web/Company.asmx',
                     'GetDocumentForId',
                     {
                         Id: DocId
                     },
                     false
                 );
            return Document_File;
        }
    }
    else if (id.indexOf("_QuoteDoc") != -1) {
        var DocId = id.replace("_QuoteDoc", "");
        console.log("Quote Doc Id:" + DocId);
        if (DocId > 0) {
            var Document_File = Ajax_CallService(
                     'Services/Web/WorkFlow.asmx',
                     'getDefaultJobDocumentById',
                     {
                         Id: DocId
                     },
                     false
                 );
            return Document_File;
        }
    }

}

function getFileExtension(DocumentFileName) {

    FileExtension = /^.+\.([^.]+)$/.exec(DocumentFileName);
    return FileExtension == null ? "" : FileExtension[1];
}

