﻿var TeamId;
var Colour = "1";
var CurTeam = null;
var CurColour;
var CurStaffList;
var SelectedStaffResult;
var SelectedStaffList;

function Team_Load(_id) {
    
    TeamId = _id;
    PopulateStaffList();
    PopulateFromId();

}

function TeamSave() {
    
    var drv = GetAssignedStaffId('ddlDesignatedDriver');
    var ldr = GetAssignedStaffId('ddlTeamLeader');

    var teamStaff = GetTeamMembers();


    CurTeam.Id = TeamId;
    CurTeam.Name = document.getElementById('txtName').value;
    var number = document.getElementById('txtNumber').value;
    if (!isNaN(number)) {
        CurTeam.Number = number;
    }
    if (GetAssignedStaffId != null) {
        CurTeam.LeaderId = GetAssignedStaffId('ddlTeamLeader');
        CurTeam.DriverId = GetAssignedStaffId('ddlDesignatedDriver');
    }
    CurTeam.Personnel = teamStaff;

    try {
        Ajax_CallService(
            'Services/Web/WorkProgramming.asmx',
            'TeamSaveAll',
            {
                team: CurTeam,
                strColour: CurColour
            },
            false
        );
    }
    catch (err) {
        
    }
}

function TeamDelete() {

    if (confirm("Are you sure you want to delete this team?")) {
        try {
            Ajax_CallService(
                'Services/Web/WorkProgramming.asmx',
                'TeamDelete',
                {
                    teamId: TeamId,
                },
                false
            );
        }
        catch (err) {
            
        }

        $('#Dialog_Window').dialog('close');
        LoadTeams();
    }
}

function GetAssignedStaffId(ddlElementName) {
    var index = document.getElementById(ddlElementName).options[document.getElementById(ddlElementName).selectedIndex].value;
    return CurStaffList[index].Id;
}

function GetTeamMembers() {
    var team = [];
    CurStaffList.forEach(function (c) {
        if (document.getElementById('chkTeamStaff' + c.Id + '').checked) {
            team.push({
                Id: -1,
                StaffId: c.Id,
                StaffName: c.Name,
                Task: document.getElementById('txtTeamTask' + c.Id + '').value,
                Deleted: false
            });
        } else {
            team.push({
                Id: -1,
                StaffId: c.Id,
                StaffName: c.Name,
                Task: document.getElementById('txtTeamTask' + c.Id + '').value,
                Deleted: true
            });
        }
    });
    var tCount = team.length;
    CurTeam.Personnel.forEach(function (ts) {
        team.forEach(function (nts) {
            if (nts.StaffId == ts.StaffId) {
                nts.Id = ts.Id;
            }
        });
    });
    return team;
}

function PopulateFromId() {
    Ajax_CallService(
   'Services/Web/WorkProgramming.asmx',
   'GetTeamForID',
   {
       ID: TeamId
   },
   true,
   PopulateTeam
    );
}

function PopulateTeam(result) {
    CurTeam = result
    document.getElementById('txtName').value = CurTeam.Name;
    document.getElementById('txtNumber').value = CurTeam.Number;
     //
    switch (CurTeam.RGBHexValue) {
        case "ff0000": //red
            CurColour = "Red";
            document.getElementById('liColour').value = 1;
            break;
        case "0000ff": //blue
            CurColour = "Blue";
            document.getElementById('liColour').value = 2;
            break;
        case "008000": //green
            CurColour = "Green";
            document.getElementById('liColour').value = 3;
            break;
        case "800080": //purple
            CurColour = "Purple";
            document.getElementById('liColour').value = 4;
            break;
        case "ffff00": //yellow
            CurColour = "Yellow";
            document.getElementById('liColour').value = 5;
            break;
        case "a52a2a": //brown
            CurColour = "Brown";
            document.getElementById('liColour').value = 6;
            break;
        case "ffffff": //white
            CurColour = "White";
            document.getElementById('liColour').value = 7;
            break;
        case "000000": //Black
            CurColour = "Black";
            document.getElementById('liColour').value = 8;
            break;
        default:
            CurColour = "Red";
            document.getElementById('liColour').value = 1;
            break;
    }

    var doc = document.getElementById('txtCurColour');
    doc.style.background = CurColour;
    PopulateDropDownoptions(document.getElementById('ddlTeamLeader'), CurTeam.LeaderId);
    PopulateDropDownoptions(document.getElementById('ddlDesignatedDriver'), CurTeam.DriverId);

    if (CurTeam.Personnel) {
        SetTeamMembers(CurTeam.Personnel);
    }
}

function PopulateDropDownoptions(ddlElement, selectedIndex) {
    var count = CurStaffList.length;
    var strHtml = '';
    //Removed to force users to pick a team leader and driver
    //if (selectedIndex == -1) {
    //    strHtml += '<option value="-1" selected="selected">Please Select</option>';
    //}
    for (var index = 0; index < count; index++) {
        if (selectedIndex == CurStaffList[index].Id) {
            strHtml += '<option value="' + index + '" selected="selected">' + CurStaffList[index].Name + '</option>';
        } else {
            strHtml += '<option value="' + index + '">' + CurStaffList[index].Name + '</option>';
        }
    }
    ddlElement.innerHTML = strHtml;
}

function PopulateStaffList() {
    CurStaffList = Ajax_CallService(
    'Services/Web/Company.asmx',
    'ListAllStaff',
    {},
    false
     );
     
    var ListPanel = document.getElementById('pnlTableStaffContainer');
    var strHtml = '<table class="searchGrid width-full white">';
    strHtml += '<tr><th>Staff Name</th><th>Team Member</th><th>Task</th></tr>';
    CurStaffList.forEach(function (c) {
        strHtml += '<tr onclick="SetStaffSelected(' + c.Id + ')">';
        strHtml += '<td style="width: 50%; padding-left: 10px;">' + c.Name + '</td>';
        strHtml += '<td style="text-align:center;"> <input type="checkbox" Id="chkTeamStaff' + c.Id + '"/>  </td> ';
        strHtml += '<td class="companyInput"><input type="text" Id="txtTeamTask' + c.Id + '" placeholder="Staff Role" title="The role within the team for the staff member"/>  </td> ';
        strHtml += '</tr>';
    })
    strHtml += '</table>';
    ListPanel.innerHTML = strHtml;
}

function SetTeamMembers(Personnel) {
    try {
        if (!Personnel || Personnel.length == 0)
            return;

        Personnel.forEach(function (p) {
            SetStaffSelected(p.StaffId);
            var elm = document.getElementById('txtTeamTask' + p.StaffId)
            if (elm)
                elm.value = p.Task;
        });

    }
    catch (err) {
    }
}


function SetStaffSelected(staffId) {
    var element = document.getElementById('chkTeamStaff' + staffId);
    if (element) {
        element.checked = !element.checked;
    }
}

function ColourChanged() {
    Colour = document.getElementById('liColour').value;
    // 
    switch (Colour) {
        case "1":
            CurColour = 'Red';
            break;
        case "2":
            CurColour = 'Blue';
            break;
        case "3":
            CurColour = 'Green';
            break;
        case "4":
            CurColour = 'Purple';
            break;
        case "5":
            CurColour = 'Yellow';
            break;
        case "6":
            CurColour = 'Brown';
            break;
        case "7":
            CurColour = 'White';
            break;
        case "8":
            CurColour = 'Black';
            break;
    }

    var doc = document.getElementById('txtCurColour');
    doc.style.background = CurColour;
    // 
}

function RoleSet() {
    //document.getElementById('pnlTeamRoleContainer').className = 'hidden';
    var val = document.getElementById('dropTeamRoles').value
    switch (val) {
        case "1":
            TeamLeaderId = SelectedStaffResult;
            break;
        case "2":
            TeamDriverId = SelectedStaffResult;
            break;
        case "3":
            TeamMemberId = SelectedStaffResult;
            break;
    }
}
