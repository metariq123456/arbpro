﻿var lstRescuePlans = null;
var SelectedResultId = -1;
var CurItem = null;

//Function to load the list of Aerial rescue plans
function RescuePlans_Load() {
    SelectedResultId = -1;
    CurItem = null;
    GetWaitImageForPanel('pnlTableContainer','Loading: Please Wait');

    Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'GetAerialPlanList',
           {},
           true,
           RescuePlansLoadCallback
       );
}

//Async call back function to populate the list of rescue plans
function RescuePlansLoadCallback(results) {
    var panel = document.getElementById('pnlTableContainer');
    if (!results) {
        //No data returned display an error
        DisplayMessage(panel.id, 'Error: ArbPro service did not return a valid list');
    }
    else if (results.Count == 0) {
        //The result list is empty display message
        DisplayMessage(panel.id, 'Information: The Aerail rescue plan list returned from the ArbPro service is empty');
    }
    else {
        //Set the scope array
        lstRescuePlans = results;
        //Display each results
        var strHtml = '<table class="searchGrid width-full white">';
        var count = lstRescuePlans.length;
        for (var i = 0; i < count; i++) {
            if (i == SelectedResultId) {
                strHtml += '<tr class="selected" onclick="ItemSelected(' + i + ');">';
            }
            else {
                strHtml += '<tr onclick="ItemSelected(' + i  + ');">';
            }
            strHtml += '<td style="padding-left: 10px;">' + lstRescuePlans[i].RescuePlan + '</td>';
            strHtml += '</tr>';
        }
        strHtml += '</table>';
        //Update the element
        panel.innerHTML = strHtml;
    }

}

//Event handler funcction to display a selected item
function ItemSelected(selectedIndex) {
    SelectedResultId = selectedIndex;
    PopulateHiddenFields(selectedIndex);
    RescuePlansLoadCallback(lstRescuePlans);
}

function NewItem() {
    SelectedResultId = -1;
    GetWaitImageForPanel('pnlTableContainer', 'Loading: Please Wait');
    Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'GetAerialPlan',
           {
               id: -1
           },
           true,
           NewItemCallBack
       );
}

function NewItemCallBack(result) {
    CurItem = result;
    RescuePlansLoadCallback(lstRescuePlans);
    document.getElementById('btnDelete').className = 'button-thin hidden';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtValue').value = '';
}


function PopulateHiddenFields(selectedIndex) {
    CurItem = lstRescuePlans[selectedIndex];
    document.getElementById('btnDelete').className = 'button-thin';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtValue').value = CurItem.RescuePlan;
    document.getElementById('btnDelete').disabled = false;
    document.getElementById('btnSave').disabled = false;
}

function SaveItem() {
    try{
        var newValue = document.getElementById('txtValue').value
        if (ValidateEntry(newValue)) {
            CurItem.RescuePlan = newValue;
            var saved = Ajax_CallService(
               '../Services/Web/Settings.asmx',
               'SaveAerialPlan',
               {
                   obj: CurItem
               },
               false
           );
            if (saved > 0) {
                document.getElementById('pnlItemControls').className = 'clientField float-left hidden';
                RescuePlans_Load();
            }
            else {
                DisplayMessage('pnlError', 'Error: ArbPro service failed to save the rescue plan');
            }
        }
        else {
            DisplayMessage('pnlError', 'Validation Error: Please ensure the rescue plan is less than 250 characters');
        }
    }
    catch (err) {
        DisplayMessage('pnlError', 'Error: Save failed - ' + err.toString());
    }
}

function DeleteItem() {
    var selectedItem = lstRescuePlans[SelectedResultId];
    var deleted = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'DeleteAerialPlan',
           {
               obj: selectedItem
           },
           false
       );
    if (deleted) {
        document.getElementById('pnlItemControls').className = 'clientField float-left hidden';
        RescuePlans_Load();
    }
    else {
        DisplayMessage('pnlError', 'Error: ArbPro service failed to delete the rescue plan');
    }
}

function ValidateEntry(value) {
    return value && value.length < 255;
}