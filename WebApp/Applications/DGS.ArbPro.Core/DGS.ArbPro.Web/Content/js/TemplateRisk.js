﻿
var templateWatcher = {
    timeout: null,
    currentValue: '',
    watchForChange: function (el) {
        this.currentValue = el.value;
        this.timeout = setTimeout(function () {
            templateWatcher.watch(el);
        }, 200);
    },
    watch: function (el) {
        if (el.value != this.currentValue) {
            this.changed(el);
        }
        this.timeout = setTimeout(function () {
            templateWatcher.watch(el);
        }, 200);
    },
    cancelWatchForChange: function () {
        clearTimeout(this.timeout);
        this.timeout = null;
    },
    changed: function (el) {
        this.currentValue = el.value;
        TemplateChanged(true);
    }
}

function TemplateChanged(bChanged) {
    var Button = $('#btnSave');
    if (bChanged) {
        Button.attr('class', 'button-alert float-right padded');
    } else {
        Button.attr('class', 'button-basic float-right padded');
    }
}

function LayoutSectionList() {
    var Grid = document.getElementById('grdSections');
    var User = GetUser(Page_strUserEmail);
    var templateID = document.getElementById('hfTemplateID').value;

    var sections = Ajax_CallService(
            'Services/Web/Template.asmx',
            'GetSectionTemplatesForRiskId',
            {
                riskTemplateId: templateID,
            },
            false
        );
    
    var listHtml = '';
    sections.forEach(function (s) {
        listHtml = listHtml + '<table class="searchGrid width-full">'
        listHtml = listHtml + '<tr class="colored-raise templateSubHeader"><td>Title<td><td onclick="EditSection(' + s.Id + '); return false;"><span id="secTitle' +s.Id+'"><label>' + s.Title + '</label></span></td></tr>';
        listHtml = listHtml + '<tr><td>Reference Document<td><td onclick="EditSection(' + s.Id + '); return false;"><span id="secRefDoc' + s.Id + '"><label>' + s.ReferenceDoc + '</label></span></td></tr>';
        listHtml = listHtml + '<tr><td>Reference Chapter<td><td onclick="EditSection(' + s.Id + '); return false;"><span id="secRefChapter' + s.Id + '"><label>' + s.ChapterRef + '</label></span></td></tr>';
        listHtml = listHtml + '<tr><td>Description<td><td onclick="EditSection(' + s.Id + '); return false;"><span id="secDescription' + s.Id + '"><label>' + s.Description + '</label></span></td></tr>';
        listHtml = listHtml + '<tr><td></td><td onclick="event.preventDefault(); RemoveSection(' + s.Id + '); return false;" style="text-align: center;"><a href="#">Remove Section</a></td></tr>';
        listHtml = listHtml + '<tr><td><div id="pnlSectId"' + s.Id + '></div></td></tr>';
        listHtml = listHtml + '</table><br/>';
    });
    Grid.innerHTML = listHtml;
    /*
    sections.forEach(function (s) {
        LayoutSectionCriteria(s.Id);
    });
    */
}

/*function UpdateSection(iSectionId) {
    var User = GetUser(Page_strUserEmail);
    var templateID = document.getElementById('hfTemplateID').value;

    var section = Ajax_CallService(
            'Services/Web/Template.asmx',
            'GetSectionTemplateForId',
            {
                riskTemplateId: templateID,
                sectionTemplateId: iSectionId
            },
            false
        );

    var titleSpan = document.getElementById('secTitle' + section.Id);
    titleSpan.innerHTML = '<label>' + section.Title + '</label>';
    var docSpan = document.getElementById('secRefDoc' + section.Id);
    docSpan.innerHTML = '<label>' + section.ReferenceDoc + '</label>';
    var chapterSpan = document.getElementById('secRefChapter' + section.Id);
    chapterSpan.innerHTML = '<label>' + section.ChapterRef + '</label>';
    var desSpan = document.getElementById('secDescription' + section.Id);
    desSpan.innerHTML = '<label>' + section.ChapterRef + '</label>';
}

function UpdateSectionCriteria(iSectionID) {
    var Grid = document.getElementById('pnlSectId' + iSectionID);
    var User = GetUser(Page_strUserEmail);
    var templateID = document.getElementById('hfTemplateID').value;

    var criterion = Ajax_CallService(
            'Services/Web/Template.asmx',
            'GetCriteriaTemplatesForSectionId',
            {
                riskTemplateId: templateID,
                sectionTemplateId: iSectionID
            },
            false
        );

    var listHtml = '<table class="searchGrid width-full">';
    criterion.forEach(function (c) {
        listHtml = listHtml + '<tr class="colored-wash templateSubHeader"><td onclick="event.preventDefault(); AddCriteria(' + iSectionID + '); return false;" style="text-align: center;"><a href="#">Add</a></td></tr>';
        listHtml = listHtml + '<tr><td>Criterion<td><td onclick="EditCriteria(' + iSectionID +',' + c.Id + '); return false;">' + c.Title + '</td></tr>';
        listHtml = listHtml + '<tr><td>Reference Chapter<td><td onclick="EditCriteria(' + iSectionID +',' + c.Id + '); return false;">' + c.ChapterRef + '</td></tr>';
        listHtml = listHtml + '<tr><td>Assessment Phase<td><td onclick="EditCriteria(' + iSectionID + ',' + c.Id + '); return false;">' + c.AssessmentPhase + '</td></tr>';
        listHtml = listHtml + '<tr><td></td><td onclick="event.preventDefault(); RemoveCriteria(' + iSectionID + ',' + c.Id + '); return false;" style="text-align: center;"><a href="#">Remove</a></td></tr>';
    });
    listHtml = listHtml + '</table>';
    Grid.innerHTML = listHtml;
}*/

function EditSection(iSectionID) {
    Dialog_OpenPage(
        iSectionID == -1 ? 'New Section' : 'Edit Section',
        'Dialogs/TemplateRiskSectionEdit.aspx?ID=' + iSectionID,
        'content',
        700,
        500,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    SaveRiskTemplateSection();
                    $(this).dialog('close');
                }
            },
            'Cancel': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/TemplateRisk.js?V=3.23.5');
}

function SaveRiskTemplateSection() {

    var section = {};
    var sectionEl = $("table[data-container='section']");
    var criterionEl = $("table[data-container='criterion']");

    // section
    section.Id = sectionEl.attr("data-val");
    section.Title = $("#hfSectionTitle").val();
    section.ReferenceDoc = sectionEl.find("[data-field='sect-docref']").val();
    section.ChapterRef = sectionEl.find("[data-field='sect-chapref']").val();
    section.Description = sectionEl.find("[data-field='sect-desc']").val();

    // criterion
    section.TemplateCriteria = [];
    criterionEl.find("tbody tr").each(function (index, element) {
        section.TemplateCriteria[index] = {
            Id: $(element).attr("data-val"),
            Criterion: $(element).find("[data-field='crit-desc']").text(),
            ChapterRef: $(element).find("[data-field='crit-chapref']").val(),
            AssessmentPhase: $(element).find("[data-field='crit-assess']").text()
        };
    });

    // update the section and criteria document references
    Ajax_CallService(
        'Services/Web/Template.asmx',
        'UpdateDocumentReferencesForSectionId',
        {
            section: section
        },
        false
    );

    // update the section and criteria document references
    UpdateDocumentReferencesForSectionId(section.Id);
}

function UpdateDocumentReferencesForSectionId(sectionId) {
    var User = GetUser(Page_strUserEmail);
    var templateID = document.getElementById('hfTemplateID').value;

    var section = Ajax_CallService(
        'Services/Web/Template.asmx',
        'GetSectionTemplateForId',
        {
            riskId: templateID,
            sectionTemplateId: sectionId
        },
        false
    );

    $("table[data-section='" + sectionId + "'] #secRefDoc").text(section.ReferenceDoc);
    $("table[data-section='" + sectionId + "'] #secRefChapter").text(section.ChapterRef);

    var allCriterion = $("table[data-critsec='" + sectionId + "'] #critRefDoc");
    $(section.TemplateCriteria).each(function (index, criterion) {
        $(allCriterion[index]).text(criterion.ChapterRef);
    });
}

function EditCriterion(iSectionId, iCriterionID) {
    Dialog_OpenPage(
        iCriterionID == -1 ? 'New Criterion' : 'Edit Criterion',
        'Dialogs/TemplateCriterion.aspx?ID=' + iCriterionID + '?Sec=' + iSectionId,
        'content',
        700,
        500,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    if (SaveTemplateCriterion()) {
                        UpdateSectionCriterion(iSectionId);
                        $(this).dialog('close');
                    } else {
                        alert('Criterion Template Save Failed');
                    }
                }
            },
            'Cancel': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/TemplateRisk.js?V=3.23.5',
        function () {
            Criterion_Load();
        });
}

var Section_curTab = 1;

function SwitchTab() {
    var tabCount = document.getElementById('hfSectionTotal').value;
    for (var index = 1; index <= tabCount; index++) {
        if (index == Section_curTab) {
            document.getElementById('tab' + index.toString()).className = 'clientTab activeTab';
            document.getElementById('pnl' + index.toString()).className = '';
        }
        else {
            document.getElementById('tab' + index.toString()).className = 'clientTab';
            document.getElementById('pnl' + index.toString()).className = 'hidden';
        }
    }
}

