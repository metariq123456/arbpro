﻿//Global variables
var assessment; // The risk assessment
var staffList;  // The list of staff
var aerialPlan; //The list of possible aerial rescue plans

//Save function
function RiskAssessmentDetails_Save() {
    try
    {
        assessment.AssignedAerialStaffId = document.getElementById("cmbAerialNomaniee").value;
        assessment.AerialPlanId = document.getElementById("cmbAerialRescuePlan").value;
        assessment.MobileSignal = document.getElementById("txtMobSignal").value;
        assessment.SiteAccess = document.getElementById("txtSiteAccess").value;
        assessment.NearestHospital = document.getElementById("txtHospital").value;
        assessment.CS38Certifed = document.getElementById("chkCS38").checked;
        assessment.GridReference = document.getElementById("txtGridReference").value;
        //Refresh the dates
        assessment.AssessmentDate = toDateFromJson(assessment.AssessmentDate);
        assessment.WorkDate = toDateFromJson(assessment.WorkDate);
        assessment.ReviewDate = toDateFromJson(assessment.ReviewDate);
        assessment.CompletionDate = toDateFromJson(assessment.CompletionDate);
        assessment.LastUpdatedDate = toDateFromJson(assessment.LastUpdatedDate);
        assessment.ReferralDate = toDateFromJson(assessment.ReferralDate);
        
        Ajax_CallService(
                    '../Services/Web/RiskAssessment.asmx',
                    'RiskAssessmentUpdateDetails',
                    {
                        assessment: assessment
                    },
                    false);
    } catch (err) {
        // 
        DisplayMessage("pnlRiskDetails", "Error displaying additional risk.");
    }
}

function RiskStaff_Load() {
    try {
        var riskId = document.getElementById("hfRiskId").value;
        
    }
    catch (err) {
        // 
        DisplayMessage("pnlRiskDetails", "Error loading risk assessment details.");
    }
}

function LoadReferenceData(service, endPoint) {
    return Ajax_CallService(
                    '../Services/Web/' + service + '.asmx',
                    endPoint,
                    {},
                    false);
}

//Async JQuery service call to load a risk assessment
function RiskAssessmentDetails_Load() {
    try {
        // 
        //populate the staff data list
        staffList = LoadReferenceData('Company', 'ListAllStaff');
        aerialPlan = LoadReferenceData('RiskAssessment', 'RiskAreialRescuePlans');
        //Populate the risk assessment - this call as asynchronous
        var riskId = document.getElementById("hfRiskId").value;
        Ajax_CallService(
                    '../Services/Web/RiskAssessment.asmx',
                    'RiskGetForId',
                    {
                        riskId: riskId
                    },
                    true,
                    DisplayDetails);
    }
    catch (err) {
        // 
        DisplayMessage("pnlRiskDetails", "Error loading risk assessment details.");
    }

}

//Async JQuery callback to display specific risk assessment details
function DisplayDetails(risk) {
    if (!risk) {
        DisplayMessage("pnlRiskDetails", "Error communicating with service.");
    } else {
        try {
            var panel = document.getElementById("pnlRiskDetails");
            var strHtml = '<div class="width-full">';
            strHtml += '<div class="clientField">';
            strHtml += '<input id="txtMobSignal" class="clientInput" type="text" placeholder="Mobile Service Provider and Signal" title="Mobile Service Provider and Signal" name="txtMobSignal" value="' + CleanStringValue(risk.MobileSignal) + '">';
            strHtml += '<br>';
            strHtml += CreateAerialDropdown('cmbAerialRescuePlan', risk.AerialPlanId);
            //strHtml += '<input id="txtAreialRescuePlav" class="clientInput" type="text" placeholder="Aerial Rescue Plan" title="Aerial Rescue Plan" name="txtAreialRescuePlav" value="' + risk.Aer + '>';
            strHtml += '<br>';
            strHtml += CreateStaffDropdown('cmbAerialNomaniee', risk.AssignedAerialStaffId);
            //strHtml += '<input id="txtAerialNomaniee" class="clientInput" type="text" placeholder="Aerial Rescue Nomaniee" title="Aerial Rescue Nomaniee" name="txtNewClientAddress2">';
            strHtml += '<br>';
            
            strHtml += '</div><div class="clientField">';
            strHtml += '<label for="chkCS38" class="clientLabel">CS38 Certified</label><input id="chkCS38" type="checkbox" title="CS38 Certified"  ';
            if (risk.CS38Certifed) {
                strHtml += 'checked>';
            } else {
                strHtml += '>';
            }
            strHtml += '</div><div class="clientField">';
            strHtml += '<input id="txtSiteAccess" class="clientInput" type="text" placeholder="Site Access" name="txtSiteAccess" autocomplete="off" value="' + CleanStringValue(risk.SiteAccess) + '">';
            strHtml += '<br>';
            strHtml += '<input id="txtHospital" class="clientInput ui-autocomplete-input" type="text" placeholder="Nearest Hospital" title="Nearest Hospital" name="txtHospital" autocomplete="off" value="' + CleanStringValue(risk.NearestHospital) + '">';
            strHtml += '<br>';
            strHtml += '<input id="txtGridReference" class="clientInput" type="text" placeholder="Grid Reference" title="Grid Reference" name="txtGridReference" autocomplete="off" value="' + CleanStringValue(risk.GridReference) + '">';
            strHtml += '</div></div>';
            panel.innerHTML = strHtml;

            assessment = risk;
            AutoComplete('txtHospital', 'ListHospitals');
        } catch (err) {
            // 
            DisplayMessage("pnlRiskDetails", "Error displaying additional risk.");
        }
    }
}

function CreateStaffDropdown(elementName, selectedId) {
    // 
    var listHtml = '<select id="' + elementName + '" class="width-full clientInput">';
    var count = staffList.length;
    listHtml += '<option value="-1"' + (selectedId == -1 ? ' selected="selected"' : '') + '>Select</option>';
    for (var index = 0; index < count; index++) {
        if (selectedId == staffList[index].Id) {
            listHtml += '<option value="' + staffList[index].Id + '" selected="selected">' + staffList[index].Name + '</option>';
        } else {
            listHtml += '<option value="' + staffList[index].Id + '">' + staffList[index].Name + '</option>';
        }
    }
    listHtml += '</select></td>';

    return listHtml;
}

function CreateAerialDropdown(elementName, selectedId) {
    // 
    var listHtml = '<select id="' + elementName + '" class="width-full clientInput">';
    var count = aerialPlan.length;
    listHtml += '<option value="-1"' + (selectedId == -1 ? ' selected="selected"' : '') + '>Select</option>';
    for (var index = 0; index < count; index++) {
        if (selectedId == aerialPlan[index].Id) {
            listHtml += '<option value="' + aerialPlan[index].Id + '" selected="selected">' + aerialPlan[index].Details + '</option>';
        } else {
            listHtml += '<option value="' + aerialPlan[index].Id + '">' + aerialPlan[index].Details + '</option>';
        }
    }
    listHtml += '</select></td>';

    return listHtml;
}

function CleanStringValue(strValue) {
    if (strValue == 'Not Set') {
        return '';
    } else {
        return strValue;
    }
}

//Async callback to display a PDF
function DisplayPDF(result) {
    ShowGenerationTimer(false);
    if (!result) {
        DisplayMessage('pnlPDFError', 'Error: Unable to display PDF, generation failed.');
    } else {
        $('#pdfPreview').attr('src', 'Controls/PDFViewer.aspx?v=' + Math.floor((Math.random() * 1000000) + 1));
        HookPdfToDownload();
    }
}

//      Required! - Attach the click event handler for the Download button
function HookPdfToDownload() {
    $("#previewToolbar").css('visibility', 'visible');
    var openPdf = document.getElementById('btnDownload');

    openPdf.onclick = function () {
        window.open($("#pdfPreview").contents().find("#hiddenGeneratedPdfUrl").val());
    }
}

function ShowGenerationTimer(show) {
    if (show) {
        document.getElementById('pnlLmPdfView').className = 'hidden';
        document.getElementById('pnlLmProcessing').className = 'waitTimer';
    } else {
        document.getElementById('pnlLmProcessing').className = 'hidden';
        document.getElementById('pnlLmPdfView').className = 'lmPnlLrg';
    }
}

function LoadPDF() {
    //Display the wait timer
    ShowGenerationTimer(true);
    var ObjPdfSettings = Ajax_CallService(
            'Services/Web/Report.asmx',
            'GetWorkPdfSetting',
            {},
            false
            );
    var SiteId = GetURLParameter('SiteId');
    var QuoteId = GetURLParameter('QuoteID');
    if (QuoteId == 'null') {
        QuoteId = GetURLParameter('QuoteId');
    }
    var WorkPlanId = Ajax_CallService(
    'Services/Web/Report.asmx',
    'GetWorkPlanId',
    {
        qId: QuoteId,
        jsId: SiteId
    },
    false
    );
    ObjPdfSettings.Id = '' + SiteId + '';
    ObjPdfSettings.ReportParentType = 5;
    ObjPdfSettings.GenerationEnum = 1;
    ObjPdfSettings.ShowPictures = true;
    ObjPdfSettings.QuoteId = WorkPlanId;
    Ajax_CallService('Services/Web/Report.asmx',
                'DownloadWorkPack',
                {
                    settings: ObjPdfSettings
                },
                true,
                DisplayPDF);



}