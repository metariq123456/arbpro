﻿
var IsPreview = false;           // Boolena indicating whether or not the pdf is being previewed or generated
var Marketing = null;            // The Marketing object to be loaded
var ListPDFTemplates = null;     // The text templates that can be used for PDF generation
var ListEmailTemplates = null;   // The text templates that can be used for Email generation

// state flags
var ArrIsPanelShown = [true, true, true];
var EmailSmtpObj = [];

// Function to load the marketing pdf and email generation content
function MarketingGenerationLoad(categoryId) {
    document.getElementById('SmsContainerPanel').className = "hidden";

    var e = document.getElementById('btnPreviewPdf');
    if (e) {
        e.disabled = true;
    }

    e = document.getElementById('btnGeneratePdf');
    if (e) {
        e.disabled = true;
    }

    Marketing = GetCategoryForId(categoryId);

    if (Marketing && Marketing.Id > 0) {
        GetPDFTemplates();
        GetMarketingTemplates();
    } else {
        DisplayMessage('pnlPDFError', 'Error: Unable to load Active Return Marketing Template');
    }
}

// Gets a client work category for it's id
function GetCategoryForId(id) {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetClientWorkCategoryForId',
        {
            categoryId: id
        },
        false
    );

    return catList;
}

// Async service call to load valid templates
function GetPDFTemplates() {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
            'Services/Web/Settings.asmx',
            'GetMarketingTemplateListForTypeAndStage',
            {
                format: 2,
                tmplType: 3
            },
            true,
            PopulatePDFPanel
        );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

// Async service call to load valid templates
function GetMarketingTemplates() {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
           'Services/Web/Settings.asmx',
           'GetMarketingTemplateListForTypeAndStage',
           {
               format: 1,
               tmplType: 3
           },
           true,
           PopulateEmailPanel
       );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

// Async service call to generate the required PDF document
// isPreview denotes if the generated PDF is to be inserted into the database and have a history created
function LoadPDF(isPreview) {
    //Set the preview flag for the callback to access
    IsPreview = isPreview;
    //Display the wait timer
    ShowGenerationTimer(true);

    var pdfSettings = GetPdfSettings(isPreview, false);
    Marketing.ReturnDate = toDateFromJson(Marketing.ReturnDate);
    Marketing.EmailSentDate = toDateFromJson(Marketing.EmailSentDate);

    try {
        Ajax_CallService(
            'Services/Web/Report.asmx',
            'UpdateMarketingAndOpenReport',
            {
                obj: Marketing,
                settings: pdfSettings
            },
            true,
            DisplayPDF
        );
    } catch (err) {
        DisplayMessage('pnlPDFError', 'Error: Unable to communicate with Arb Pro service - ', er.toString());
    }
}

// Helper function to get the settings for Pdf generation
function GetPdfSettings(isPreview, isCopy) {
    var pdfSettings = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetMarketingPdfSetting',
        {},
        false);
    pdfSettings.Id = Marketing.Id;
    pdfSettings.IsCopy = isCopy;
    pdfSettings.IsPreview = isPreview;
    pdfSettings.GenerationEnum = 0;
    var ddl = document.getElementById('ddlLetterTemplate');
    pdfSettings.TemplateId = parseInt(ddl.options[ddl.selectedIndex].value);
    pdfSettings.TextBody = document.getElementById('txtLetterText').value;

    return pdfSettings;
}

// Async callback to generate the html for the PDF settings panel
function PopulatePDFPanel(results) {
    var panel = document.getElementById('pnlPDFBody_0');
    if (!results || results.length === 0) {
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
    } else {
        try {
            window.ListPdfTemplates = results;
            //Generate the html for the div
            var strHtml = '';

            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" style="width:20%;"> Contact </label>';
            strHtml += '<label>' + Marketing.ClientName + '</label>';
            strHtml += '</div>';

            strHtml += '<div class="lmField" style="width: 100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlLetterTemplate" style="width:20%;"> Template</label>';
            strHtml += '<select id="ddlLetterTemplate" class="lmFieldControl" onchange="LetterTemplateSelected();" name="ddlLetterTemplate" style="width:76%;">';
            window.ListPdfTemplates.forEach(function (tmp) {
                strHtml += '<option value="' + tmp.Id + '">' + tmp.TemplateName + '</option>';
            });
            strHtml += '</select>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="width: 100%;">';
            strHtml += '<textarea id="txtLetterText" class="lmFieldText" style="font-family:Helvetica;font-size:Small;" cols="20" rows="2" name="txtLetterText"></textarea>';
            strHtml += '</div>';
            strHtml += '<br />';

            strHtml += '<br />';
            strHtml += '<div class="lmField center" style="width:100%;">';
            strHtml += '<label id="lblWarnig" class="lmFieldText"> Please use the Preview button before Generating a PDF. Generating a PDF will mark the entry as sent. </label>';
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="height:30px;width:100%;">';
            strHtml += '<input id="btnPreviewPdf" class="button-thin padded float-left" type="submit" style="margin-right: 10px;" onclick="LoadPDF(true); return false;" value="Preview PDF" name="btnPreviewPdf">';
            strHtml += '<input id="btnGeneratePdf" class="button-thin padded float-right" type="submit" style="margin-right: 10px;" onclick="LoadPDF(false); return false;" value="Generate PDF" name="btnGeneratePdf">';
            strHtml += '</div>';
            strHtml += '<br/>';
            //Update the panel
            panel.innerHTML = strHtml;

            //Set the text to the first selected item 
            LetterTemplateSelected();

        } catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display PDF settings - ' + err.toString());
        }
    }
}

// Async callback to generate the html fot the email settings panel
function PopulateEmailPanel(results) {
    var panel = document.getElementById('pnlPDFBody_1');
    if (!results || results.length === 0) {
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
    } else {
        try {
            ListEmailTemplates = results;
            //Generate the html for the div
            var strHtml = '';
            strHtml += '<div class="lmEdit" style="width:100%;">';
            strHtml += '<div id="divEmailOptions" style="width: 100%; display: block;">';
            strHtml += '<label class="lmFieldLabel" for="ddlEmailTemplate"> Email Text Template </label>';
            strHtml += '<select id="ddlEmailTemplate" class="lmFieldControl" onchange="ShowEmailTemplate();" name="ddlEmailTemplate">';
            ListEmailTemplates.forEach(function(tmp) {
                strHtml += '<option value="' + tmp.Id + '">' + tmp.TemplateName + '</option>';
            });
            strHtml += '</select>';
            strHtml += '<label class="lmFieldLabel" for="txtEmailSubject"> Subject </label>';
            strHtml += '<input id="txtEmailSubject" class="lmFieldControl" type="text" style="width:54%;" name="txtEmailSubject">';
            strHtml += '<textarea id="txtEmailText" class="lmFieldText" style="font-family:Helvetica;font-size:Small;" cols="20" rows="2" name="txtEmailText"></textarea>';
            strHtml += '</div>';
            strHtml += '<div id="pnlAttachments"  class="lmField" style="width:100%;">';
            strHtml += '<table id="grdAttachments" class="lmGrid" style="width:94%;">';
            strHtml += '</table>';
            strHtml += '</div>';
            strHtml += '<br>';
            strHtml += '<div class="lmField" style="height:30px;width:100%;">';
            strHtml += '<input id="btnSendEmail" class="button-thin float-right" type="submit" style="margin-right: 10px;" onclick="SendEmail(); return false;" value="Send Email" name="btnSendEmail" width:40%>';
            strHtml += '</div>';

            //Update the panel
            panel.innerHTML = strHtml;

            ToggleDsiplay(1);
            ShowEmailTemplate();
        } catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display Email settings - ' + err.toString());
        }
    }
}

// Async callback to display a PDF
function DisplayPDF(result) {
    ShowGenerationTimer(false);
    if (!result) {
        DisplayMessage('pnlPDFError', 'Error: Unable to display PDF, generation failed.');
    } else {
        RenderPDF();
    }
}

// Function to set up and display the pdf control buttons and functionality
function HookPdfToDownload() {
    $("#previewToolbar").css('visibility', 'visible');
    var openPdf = document.getElementById('btnDownload');

    openPdf.onclick = function () {
        window.open($("#pdfPreview").contents().find("#hiddenGeneratedPdfUrl").val());
    }
}

// Function to render the pdf
function RenderPDF() {
    $('#pdfPreview').attr('src', 'Controls/PDFViewer.aspx?v=' + Math.floor((Math.random() * 1000000) + 1));
    HookPdfToDownload();
}

//REQUIRED! - Function to toggle the display of the required body panel
function ToggleDsiplay(elementIndex) {
    //NOTE: The element index is appended to the end of the ID in the aspx file This function depends on the nameing convention
    var bodyElement = document.getElementById('pnlPDFBody_' + elementIndex.toString());
    var toggleElement = document.getElementById('lblHeaderToggle_' + elementIndex.toString());
    if (ArrIsPanelShown[elementIndex]) {
        toggleElement.textContent = '+';
        $(bodyElement).slideUp();
    } else {
        toggleElement.textContent = '-';
        $(bodyElement).slideDown();
    }
    ArrIsPanelShown[elementIndex] = !ArrIsPanelShown[elementIndex];
}

// Function to colapse all of the active panels
function ColapsePanels(elementIndex) {
    var bodyElement = document.getElementById('pnlPDFBody_' + elementIndex.toString());
    var toggleElement = document.getElementById('lblHeaderToggle_' + elementIndex.toString());
    if (ArrIsPanelShown[elementIndex]) {
        toggleElement.textContent = '+';
        $(bodyElement).slideUp();
    }

    ArrIsPanelShown[elementIndex] = !ArrIsPanelShown[elementIndex];
}

// Function to call the toggle display and animate the transition
function ToggleEmailOptionDisplay(index) {
    ToggleDsiplay(index);
    $('#Dialog_Content').animate({ scrollTop: "450px" });
}

// Function to populate the UI from the selected template
function ShowEmailTemplate() {
    var ddl = document.getElementById('ddlEmailTemplate');
    var tempId = parseInt(ddl.options[ddl.selectedIndex].value);
    var count = ListEmailTemplates.length;
    //A for loop is being used to enable the break statement
    for (var index = 0; index < count; index++) {
        var template = ListEmailTemplates[index];
        if (template.Id === tempId) {
            document.getElementById('txtEmailSubject').value = template.Salutation;
            template.BodyText = MarketingTextConversion(template.BodyText);
            document.getElementById('txtEmailText').value = template.BodyText;
            break;
        }
    }
}

// function that replaces the special strings in an sms template
function MarketingTextConversion(text) {
    return MarketingPatternPassing(text, Marketing.ClientName, Marketing.Name, "<OFFICE_TEL>", "<OFFICE_MOBILE>", "<COMPANY_EMAIL>", "<COMPANY_NAME>");
}

// Function to get the user's smtp settings
function GetSmtpSettings() {
    EmailSmtpObj = [];

    EmailSmtpObj = Ajax_CallService(
        '../Services/Web/Company.asmx',
        'GetEmailSettings',
        {},
        false
    );
}

// Function to update the marketing client category with an email sent flag after email has been sent successfully
function UpdateMarketingEmailSent() {
    Marketing.EmailSent = true;
    Marketing.EmailSentDate = toDateFromJson(new Date());

    var retVal = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'SaveClientWorkCategory',
        {
            obj: Marketing
        },
        false
    );

    return retVal;
}

// Function to display the result of the send email method - success or failure
function DisplayEmailMsg(result) {
    var message;
    if (result) {
        GetSmtpSettings();
        if (UpdateMarketingEmailSent()) {
            if (EmailSmtpObj.UseSmtp) {
                message = 'Emails have been generated and queued for dispatch. <br/> Please check your email client for confirmation';
            } else {
                message = 'Emails have been generated and queued for dispatch. </a>';
            }
        } else {
            message = 'Error: Email has been sent but the Marketing status update failed.';
        }
    } else {
        message = 'Error: email not sent';
    }

    Dialog_OpenPage(
        'Email',
        'Dialogs/Alert.aspx',
        'content',
        500,
        150,
        true,
        {
            'Ok': {
                text: 'Ok',
                "class": 'ui-dialog-button',
                click: function () {
                    LoadMarketing(1);
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/AlertBox.js?V=3.23.5',
        function() {
            AlertBox_Show(message);
        });
}

// Async service call to send an marketing item by email. This function creates history items for the generated documents
function SendEmail() {
    try {
        //Display the wait timer
        ShowGenerationTimer(true);
        
        //Get the settings to be applied to the email
        var emailSettings = GetEmailSettings();
        var pdfSettings = GetPdfSettings();

        Marketing.ReturnDate = toDateFromJson(Marketing.ReturnDate);
        Marketing.EmailSentDate = toDateFromJson(Marketing.EmailSentDate);

        ColapsePanels(0);
        ColapsePanels(1);

        try {
            Ajax_CallService(
                'Services/Web/Report.asmx',
                'UpdateMarketingAndEmailReport',
                {
                    obj: Marketing,
                    pdfSettings: pdfSettings,
                    emailSettings: emailSettings
                },
                true,
                DisplayEmailMsg
            );
        }
        catch (err) {
            DisplayMessage('pnlPDFError', 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
            //Ensure that the processing wait timer is not being displayed
            ShowGenerationTimer(false);
        }
    }
    catch (er) {
        DisplayMessage('pnlPDFError', 'Error: Unable to send email - ' + er.toString());
        //Ensure that the processing wait timer is not being displayed
        ShowGenerationTimer(false);
    }
}

// Helper function to get the settings for sending an email
function GetEmailSettings() {
    var emailSettings = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetEmailSetting',
        {},
        false);
    emailSettings.Id = Marketing.Id;
    emailSettings.Subject = document.getElementById('txtEmailSubject').value;
    emailSettings.BodyText = document.getElementById('txtEmailText').value;

    return emailSettings;
}

// Function to display the text of the selected template
function LetterTemplateSelected() {
    var ddl = document.getElementById('ddlLetterTemplate');
    var tempId = parseInt(ddl.options[ddl.selectedIndex].value);
    var count = ListPdfTemplates.length;
    //A for loop is being used to enable the break statement
    for (var index = 0; index < count; index++) {
        var template = ListPdfTemplates[index];
        if (template.Id === tempId) {
            template.BodyText = MarketingTextConversion(template.BodyText);
            document.getElementById('txtLetterText').value = template.BodyText;
            //document.getElementById('txtPaymentDetails').value = template.AdditionalText;
            break;
        }
    }
    //  DGSAP-713
    //  Update the attachments displayed in the letter section
    ShowLetterTemplate();
}

// Function to show the letter templates for the emails
function ShowLetterTemplate() {
    var reg = document.getElementById('pnlLetterTemplate');

    if ($(reg) != null) {
        $(reg).slideDown();
    }
}

// Function to display the loading gif when an action is taken
function ShowGenerationTimer(show) {
    if (show) {
        document.getElementById('pnlLmPdfView').className = 'hidden';
        document.getElementById('pnlLmProcessing').className = 'waitTimer';
    } else {
        document.getElementById('pnlLmProcessing').className = 'hidden';
        document.getElementById('pnlLmPdfView').className = 'lmPnlLrg';
    }
}



