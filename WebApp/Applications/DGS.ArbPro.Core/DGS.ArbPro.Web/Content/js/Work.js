﻿var ObjWorkSummary = null;
var isCancelled = false;

/*

        ****** Initialisation and service functions **********

*/

//      Function to load the work record for the selected quote
function Work_Load() {
    //debugger;
    //Display wait image
    SwitchWorkTab(0);
    var panel = document.getElementById('pnlWorkSiteItems');
    panel.innerHTML = GetWaitImageForPanel(panel.id, 'Loading Work details please wait.');
    console.log("Quote Details");
    console.log(ObjSelectedQuote);
    try
    {
        //When this panel is loaded the Job model must be populated
        var workSearchResults = [];            //This is the job object for the work record corrispoding to the selected Quote or the URL Work Id
        var isForQuote = false;
        if (ObjSelectedQuote && ObjSelectedQuote.Id > 0) {
            workSearchResults = $.grep(ObjJob.Work, function (e) { return e.ParentId == ObjSelectedQuote.Id; });
            isForQuote = true;
        } else if (UrlWorkId > 0) {
            workSearchResults = $.grep(ObjJob.Work, function (e) { return e.Id == UrlWorkId; });
        }
        //Check that we have a work record
        if (workSearchResults && workSearchResults.length > 0) {
            isCancelled = false;
            // Quote has been cancelled - update display
            if (workSearchResults[0].Status == 99) {
                isCancelled = true;
            } 
            if (!isForQuote) {
                //The quote has not yet been loadded
                SetSelectedQuote(workSearchResults[0].ParentId, false);
            }
            WorkSummary_Load(workSearchResults[0].Id);
        } else {
            //debugger;
            var msg = '';

            var btnCompleted = document.getElementById('btnMarkCompleted'); //.disabled = true;
            //var btnProduceWp = document.getElementById('btnProduceWorkPacks'); //.disabled = true;btnCancelQuote
            var btnCancel = document.getElementById('btnCancelQuote');
            if (!ObjSelectedQuote || ObjSelectedQuote.Id < 1) {
                //The currently selected quote is not valid
                msg = 'Unable to display work records for the current Quote No. Please check that the selected quote is valid';
                btnCompleted.disabled = true;
               // btnProduceWp.disabled = true;
                btnCancel.disabled = false;
            } else {
                //No work record exists for the selected item.
                //This could be because:
                if (ObjSelectedQuote.Status < 2) {
                    //      The Quote has not yet been accepted
                    msg = 'The currently selected Quote has not been progressed to the work stage yet.';
                } else if (ObjSelectedQuote.Status == 2) {
                    //      The Quote was cancelled or requoted
                    msg = 'The currently selected Quote has been cancelled.';
                } else if (ObjSelectedQuote.Status == 4) {
                    //      The Quote was rejected.
                    msg = 'The currently selected Quote has been rejected.';
                } else {
                    //Should not get here
                    msg = 'Error: Unable to display work record - Quote status not valid';
                }
                btnCompleted.disabled = true;
                //btnProduceWp.disabled = true;
                btnCancel.disabled = true;
            }
            //Display the Meassage in the details panel
            DisplayMessage(panel.id, msg);
        }
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to load work details - ' + err.toString());
    }
}

//  Async service call to get all notes for all invoices for a quote
function WorkNotes_Load(quoteId) {
    var panel = document.getElementById('pnlWorkDetail_1');
    try {
        Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetWorkNotesForQuote',
           {
               quoteId: quoteId
           },
           true,
           WorkNotes_LoadCallBack
       );
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//      Service call function to load a Work Summary
function WorkSummary_Load(id) {
    //Load the work summary from the service
    Ajax_CallService(
        'Services/Web/WorkProgramming.asmx',
        'GetWorkSummaryForWorkOnQuote',
        {
            id: id
        },
        true,
        DisplayWorkDetails
    );
}

//      Service call to update the status of selected items to be completed
function MarkWorkCompleted() {
    //debugger;
    var itemsToBeCompleted = [];
    ObjWorkSummary.WorkItems.forEach(function (wi) {
        var elm = document.getElementById('chkWorkItem_' + wi.Id.toString());
        if (elm && elm.checked) {
            itemsToBeCompleted.push(wi);
        }
    });

    var panel = document.getElementById('pnlWorkSiteItems');
    panel.innerHTML = GetWaitImageForPanel(panel.id, 'Updating Work details please wait.');

    //Only do an upload if at least one item has been mark as completed
    if (itemsToBeCompleted.length > 0) {
        Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'SetWorkItemsCompleted',
            {
                CompletedItems: itemsToBeCompleted
            },
            true,
            MarkWorkCompletedCallBack);
    } else {
        MarkWorkCompletedCallBack(false);
    }
}

function GetWorkNotesList() {
    NotesList_Load(ObjWorkSummary.Id, 1, PopulateWorkNotes);
}

/*

        ****** Service call back functions **********

*/

//      Async callback function to display the work summary for the 
function DisplayWorkDetails(result) {
    var panel = document.getElementById('pnlWorkSiteItems');
    debugger;
    try{
        if (!result) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid work record');
        } else {
            ObjWorkSummary = result;
            
            var strHtml = '';
            // construct the cancelled quote html if quote is cancelled
            if (isCancelled) {
                strHtml += CancelledItems_BuildHtml();
            } else {
                //construct the html for the tab if quote is not cancelled
                strHtml += WorkItems_BuildHtml();
            }

            // Populate the cancelled items list
            BuildCancelledWorkItemsTab();

            //      Must make sure the work object is populated before loading the notes
            GetWorkNotesList();

            //Display the HTML
            panel.innerHTML = strHtml;
        }
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display work for selected quote - ' + err.toString());
        console.log(err);
    }
}

//      Async call back function to refresh the work details after work has been marked as completed
function MarkWorkCompletedCallBack(result){
    if (result) {

        //debugger;
        SwitchJobStageTab(3);
        //  WorkSummary_Load(ObjWorkSummary.Id);
    } else {
        var panel = document.getElementById('pnlWorkSiteItems');
        try{
            var strHtml = '<table class="searchGrid width-full"><tr><th>Update Error</th></tr>';
            strHtml += '<tr><td style="text-align: center;">One or more of the items selected to be mark as completed were not updated correctly.</td><tr>';
            strHtml += '<tr><td style="text-align: center;">If the problem persists please contact support..</td><tr>';
            strHtml += '<tr><td style="text-align: center;">';
            strHtml += '<input type="button" class="BookWork button-thin padded" value="Refresh Work" onclick="WorkSummary_Load(' + ObjWorkSummary.Id + '); return false;"/></td></tr></table>';

            panel.innerHTML = strHtml;
        } catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display error message for failed work item update - ' + err.toString());
        }
    }
}

function PopulateWorkNotes(results) {
    var notesPanel = document.getElementById('pnlWorkDetail_1');
    //Use the common.js method to populate and control functionality
    if (isCancelled) {
        var strHtml = '';
        strHtml += '<table class="searchGrid width-full"><col width="8%" /><col width="22%" /><col width="70%" />';
        strHtml += '<tr><th colspan="3"><label class="float-left padded" style="vertical-align: center;">The work for this quote and the quote have been cancelled. <br/>';
        strHtml += 'Please return to the Quote tab where you will be able to Re-Quote if needed. </label></th></tr>';
        strHtml += '<tr><td colspan="3" style="padding:10px; background-color: white; cursor: default;">&nbsp;</td></tr>';
        strHtml += '</table>';
        notesPanel.innerHTML = strHtml;
    } else {
        PopulateNotes(notesPanel, 1, ObjWorkSummary.Id, results, GetWorkNotesList);
    }
}

/*

        ****** General functions **********

*/

//      Function to switch between work details and work notes tabs
function SwitchWorkTab(index) {
    for (var i = 0; i < 3; i++) {
        var tab = document.getElementById('tabWorkDetail_' + i.toString());
        var panel = document.getElementById('pnlWorkDetail_' + i.toString());
        if (i == index) {
            tab.className = 'workTab activeTab';
            panel.className = 'width-full';
        } else {
            tab.className = 'workTab';
            panel.className = 'hidden';
        }
    }
}

//      Function to generate the html for the work items
function WorkItems_BuildHtml() {
    var strHtml = '';
    var JobVatRates = new Array();
    Markers.workBooked = false;

    var printCount = 0;
    var pendingCount = 0;
    var completedCount = 0;
    itemSort(ObjWorkSummary.WorkItems);

    // calculate the total value for all unique work items. JobVatRates takes into account duplicates as well 
    // i.e. if we have an item Id = 1 booked alongside 3 weeks of work with skipping weekends
    // we end up with 3 items with Id = 1 - one for each week. That tripples the item value for JobVatRates and SiteVatRates.
    var jobTotal = new Array();
    ObjWorkSummary.WorkItems.forEach(function (item) {
        var found = false;
        if (item.Status < 99) {
            jobTotal.forEach(function(vr) {
                if (vr.rate == item.VatRate) {
                    vr.value += item.Value;
                    found = true;
                }
            });
            if (!found)
                jobTotal.push({ rate: item.VatRate, value: item.Value });
        }
    });

    ObjWorkSummary.JobSites.forEach(function (js) {
        // calculate the jobsite values for unique items only 
        // SiteVatRates includes duplicate items as well which is not correct
        var siteTotal = new Array();
        ObjWorkSummary.WorkItems.forEach(function(item) {
            var found = false;
            if (item.JobSiteId == js.Id && item.Status < 99) {
                siteTotal.forEach(function(vr) {
                    if (vr.rate == item.VatRate) {
                        vr.value += item.Value;
                        found = true;
                    }
                });
                if (!found)
                    siteTotal.push({ rate: item.VatRate, value: item.Value });
            }
        });
        var ddlQuoteID = document.getElementById('ddlQuoteID');
        var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;
        strHtml += '<table class="searchGrid width-full"><col width="4%" /><col width="17%" /><col width="54%" /><col width="8%" /><col width="7%" /><col width="10%" />';
        var bookedWork = $.grep(ObjWorkSummary.BookedWork, function (e) { return e.JobSiteId == js.Id; });
        var completedWorkItems = $.grep(ObjWorkSummary.WorkItems, function (e) { return e.JobSiteId == js.Id && e.Completed; });
        var PendingWorkItems = $.grep(ObjWorkSummary.WorkItems, function (e) { return e.JobSiteId == js.Id && !e.Booked && !e.Completed && e.Status < 99; });

        strHtml += '<tr>';
        strHtml += '<th colspan="6" style="padding: 2px;"><span class="float-left padded">' + js.SiteString + '</span>';
        strHtml += '<input type="button" class="BookWork button-thin padded float-right" value="Purchase Order" onclick="DisplayPurchaseOrderNo(' + QuoteID + ');" />';
        strHtml += '<input type="button" class="BookWork button-thin padded float-right" value="Add Items" onclick="AddWorkItem(' + js.Id + '); return false;" /></th></tr>';
       

        var SiteVatRates = new Array();

        //Display the items that are not scheduled for completion 
        if (PendingWorkItems && PendingWorkItems.length > 0) {
            strHtml += '<tr><th colspan="6"><label class="float-left padded" style="vertical-align: center;">Work Items Pending a Booking Date</label>';
            strHtml += '<input id="chkSelectAll_' + js.Id.toString() + '" class="float-right padded" type="checkbox" onclick="SelectAllForSite(' + js.Id + ');" style="cursor: pointer; vertical-align: center"/>';
            strHtml += '<label class="float-right padded">Complete All</label>';
            strHtml += '<input type="button" class="BookWork button-thin padded float-right" value="Book Work" onclick="BookWork(' + js.Id + '); return false;" style="margin-right: 15px;"/></th></tr>';

            BuildItemTable(PendingWorkItems, SiteVatRates, JobVatRates, function (wi) {
                strHtml += Item_BuildSixCellRow(wi);
            });
            pendingCount += PendingWorkItems.length;
        }

        //Display the details for each work plan for the site
        if (bookedWork && bookedWork.length > 0) {
            bookedWork.forEach(function (bw) {
                //Only create the section for the work plan if there are items assigned
                if (bw.ScheduledItems.length > 0) {
                    strHtml += '<tr><th colspan="6"></span><label class="float-left padded" >Work Items Booked for Completion on:</label>';
                    strHtml += '<span class="float-left padded">';
                    //Check if there is a start date to be displayed
                    if (bw.StartDate != null) {
                        strHtml += new Date(parseInt(bw.StartDate.substr(6))).toDateString();
                    } else {
                        strHtml += 'Not in Calendar';
                    }
                    strHtml += '</span><input id="chkWorkPlanAll_' + bw.Id.toString() + '" class="float-right padded" type="checkbox" onclick="SelectAllForWorkPlan(' + bw.Id + ');" style="cursor: pointer; vertical-align: center;"/>';
                    strHtml += '<label class="float-right padded" style="vertical-align: center;">Complete All</label>';
                    strHtml += '<input type="button" class="BookWork button-thin padded float-right" value="View or re-book work" onclick="RearrangeWork(' + bw.Id + '); return false;"  style="margin-right: 15px;"/></th></tr>';
                    //debugger;
                    BuildItemTable(bw.ScheduledItems, SiteVatRates, JobVatRates, function (wi) {
                        strHtml += Item_BuildSixCellRow(wi);
                    });
                    printCount += bw.ScheduledItems.length;
                    pendingCount += bw.ScheduledItems.length;
                }
            });

        }

        //Display the completed items
        if (completedWorkItems && completedWorkItems.length > 0) {
            strHtml += '<tr><th colspan="6"><label class="float-left padded" >Completed items that are available for invoicing</label>';
                
                
            strHtml += '</th></tr>';
            //debugger
            BuildItemTable(completedWorkItems, SiteVatRates, JobVatRates, function (wi) {
                strHtml += '<tr>';
                strHtml += '<td style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
                strHtml += '<td style="vertical-align: top;">' + wi.Name + '</td>';
                if (wi.Status == 3) {
                    strHtml += '<td style="vertical-align: top;">' + wi.Description + '</td>';
                    strHtml += '<td style="text-align: center;" onclick="event.preventDefault(); OpenImage(2, ' + wi.Id + ');"><a href="#">Image</a></td>';
                    strHtml += '<td colspan="2" style="text-align: center;" onClick="SetWorkItemStatus(' + wi.Id + ',' + 0 + ');"><a href="#">Not Complete</a></td>';
                } else if(wi.Status == 4) {
                    strHtml += '<td style="vertical-align: top;">' + wi.Description + '</td>';
                    strHtml += '<td style="text-align: center;" onclick="event.preventDefault(); OpenImage(2, ' + wi.Id + ');"><a href="#">Image</a></td>';
                    strHtml += '<td colspan="2" style="text-align: center;">Item Invoiced</td>';
                }
                //  Add a button to set the item as not completed
                //strHtml += '<td style="vertical-align: top;">' + wi.Description + '</td>';
                strHtml += '</tr>';
            });
            printCount += completedWorkItems.length;
            completedCount += completedWorkItems.length;
        }
        strHtml += '<tr><td colspan="6" style="padding:10px; background-color: white; cursor: default;">&nbsp;</td></tr>';
        strHtml += '</table>';

        if (ObjWorkSummary.JobSites.length > 1) {
            strHtml += GetSiteTotalHtml(siteTotal, ObjWorkSummary.JobSites.length, 'Site');
            strHtml += '<hr/>';
        }
    });
    strHtml += GetSiteTotalHtml(jobTotal, ObjWorkSummary.JobSites.length, 'Work');
    
    //Set state for the buttons
    document.getElementById('btnMarkCompleted').disabled = pendingCount == 0;
    //document.getElementById('btnProduceWorkPacks').disabled = printCount == 0;
    document.getElementById('btnCancelQuote').disabled = completedCount > 0;

    return strHtml;
}

//      Function to generate HTML for a 5 cell table row populated by a work items details
function Item_BuildSixCellRow(wi) {
    var strHtml = '<tr>';
    strHtml += '<td onClick="EditWorkItem(' + wi.Id + ');" style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
    strHtml += '<td onClick="EditWorkItem(' + wi.Id + ');" style="vertical-align: top;">' + wi.Name + '</td>';
    strHtml += '<td onClick="EditWorkItem(' + wi.Id + ');" style="vertical-align: top;">' + wi.Description + '</td>';
    strHtml += '<td style="text-align: center;" onclick="event.preventDefault(); OpenImage(2, ' + wi.Id + ');"><a href="#">Image</a></td>';
    strHtml += '<td style="text-align: center; cursor: default;"><input id="chkWorkItem_' + wi.Id + '" type="checkbox" style="cursor: pointer;"/></td>';
    strHtml += '<td style="text-align: center;" onClick="SetWorkItemStatus(' + wi.Id + ',' + 99 + ');"><a href="#">Cancel Item</a></td>';
    strHtml += '</tr>';
    return strHtml;
}

//      Function to open the edit dialog for a quote item
function EditWorkItem(itemId) {
    EditJobItemForStage(itemId, 'Work', WorkItemEdit_Update);
}

//      Call back function to update the Work model when an item has been updated
function WorkItemEdit_Update() {
    WorkSummary_Load(ObjWorkSummary.Id);
}

//      Function to Mark all work items for a work plan as completed / not completed
function SelectAllForSite(jobSiteId) {
    //Get a list of all the items that could be marked as completed
    var workItems = $.grep(ObjWorkSummary.WorkItems, function (e) { return e.JobSiteId == jobSiteId && !e.Completed; });
    if (workItems && workItems.length > 0) {
        var elm = null;
        var selected = document.getElementById('chkSelectAll_' + jobSiteId.toString()).checked;
        workItems.forEach(function (itm) {
            elm = document.getElementById('chkWorkItem_' + itm.Id.toString());
            if (elm) {
                elm.checked = selected;
            }
        });
    }
}


function SelectAllForWorkPlan(workPlanId) {
    //Get a list of all the items that could be marked as completed
    var booking = $.grep(ObjWorkSummary.BookedWork, function (e) { return e.Id == workPlanId});
    if (booking && booking.length > 0) {
        var wp = booking[0];
        var elm = null;
        var selected = document.getElementById('chkWorkPlanAll_' + workPlanId.toString()).checked;
        wp.ScheduledItems.forEach(function (itm) {
            elm = document.getElementById('chkWorkItem_' + itm.Id.toString());
            if (elm) {
                elm.checked = selected;
            }
        });
    }
}

//      Function to add additional items to the work record
function AddWorkItem(siteId) {
    //debugger;
    //DisplayGenericItemDialog('Work', ObjWorkSummary.Id, siteId, WorkItemEdit_Update);
    ShowWorkItemDialog('Add Work Item', true, siteId);
}

/*

        **********  Dialog Functions    **********

*/

//      Function to send work packs for a site
function SendWorkPack() {
    //TODO:     Modify this to accept the work Id and a job site 
    var ddlQuoteID = document.getElementById('ddlQuoteID');
    var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;

    Dialog_OpenPage(
        'Work Packs',
        'Dialogs/PDFGeneration.aspx',
        'content',
        900,
        600,
        true,
         {
             'Close': {
                 text: 'Close',
                 class: 'ui-dialog-button',
                 click: function () {
                     $(this).dialog('close');
                 }
             }
         },
        'Content/js/WorkpackGeneration.js?V=3.23.5',
        function () {
            PDFGeneration_Load(QuoteID);
            $("#warningMessage").visible = false;
            if (!Markers.workBooked) {
                $("#warningMessage").visible = true;
                $("#warningMessage").html("<strong>Work packs for schedules can only be generated when work has been booked.</strong>");
                $(btnGenerateWorkPack).prop("disabled", true);
            }
        });
}

//      Function to open the edit dialog for a quote item
function EditWorkItem(itemId) {
    //EditJobItemForStage(itemId, 'Work', WorkItemEdit_Update);
    ShowWorkItemDialog('Edit Work Item', false, itemId);
}


function ShowWorkNotesDialog(noteId) {
    try {
        var ddlQuoteID = document.getElementById('ddlQuoteID');
        var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;
        Dialog_OpenPage('Note',
            'Dialogs/Notes.aspx?',
            'content',
            500,
            500,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        Note_Save();
                        GetInvoiceNotesForQuote(QuoteID);
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/Notes.js?V=3.23.5',
            function () {
                Notes_Load(noteId, QuoteID, 2);
            });
    }
    catch (Err) {
        DisplayMessage('pnlCompanyInfoDetail_4', 'Error: Unable to display notes dialog');
    }
}

function ShowWorkItemDialog(title, isNew, idValue) {
    Dialog_OpenPage(
        title,
        'Dialogs/JobItem.aspx',
        'content',
        800,
        400,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    if (WorkItem_Save()) {
                        $(this).dialog('close');
                        WorkSummary_Load(ObjWorkSummary.Id);
                    }
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/WorkItemEdit.js?V=3.23.5',
        function () { WorkItem_Load(idValue,isNew); });

}
/*

        ****** Navigation functions **********

*/

//      Function to navigate the user to the book work page
function BookWork(referrerSiteId) {
    //Check that there is a valid work record to be able to book work for
    /*if (ObjWorkSummary) {
        var target = '/BookWork.aspx?WorkID=' + ObjWorkSummary.Id.toString();
        if (referrerSiteId) {
            target = target + "&ReferrerSiteID=" + referrerSiteId.toString();
        }
        window.location = target;
    } else {
        DisplayMessageDialog('Book Work', 'Unable to book work for the selected Quote.');
    }*/
    var ddlQuoteID = document.getElementById('ddlQuoteID');
    var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;
    var target = '/Calendar.aspx?BookWork=1&QuoteId=' + QuoteID;
    window.location = target;
}

function RearrangeWork(workPlanId) {
    //Check that there is a valid work record to be able to book work for
    /*if (ObjWorkSummary) {
        var target = '/BookWork.aspx?WorkID=' + ObjWorkSummary.Id.toString();
        if (workPlanId) {
            target = target + "&WorkPlanId=" + workPlanId.toString();
        }
        window.location = target;
    } else {
        DisplayMessageDialog('Book Work', 'Unable to rebook work for the selected work plan.');
    }*/
    if (ObjWorkSummary) {
        var target = '/BookDatesEdit.aspx'
        if (workPlanId) {
            target = target + "?WorkPlanId=" + workPlanId.toString();
        }
        window.location = target;
    } else {
        DisplayMessageDialog('Book Work', 'Unable to rebook work for the selected work plan.');
    }
}



function BuildCancelledWorkItemsTab() {
    var panel = document.getElementById('pnlCancelledWorkSiteItems');
    var cancelledItems = $.grep(ObjWorkSummary.WorkItems, function (e) { return e.Status == 99 && !e.Completed; });

    var strHtml = '';
    strHtml += '<table class="searchGrid width-full"><col width="8%" /><col width="22%" /><col width="60%" /><col width="10%" />';

    // if quote is not cancelled
    if (!isCancelled) {
        strHtml += '<tr><th colspan="4"><label class="float-left padded" style="vertical-align: center;">List of cancelled work items</label></th></tr>';

        cancelledItems.forEach(function(wi) {
            strHtml += '<tr>';
            strHtml += '<td style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Name + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Description + '</td>';
            strHtml += '<td style="text-align: center;" onclick="event.preventDefault(); SetWorkItemStatus(' + wi.Id + ',' + 0 + ');"><a href="#">Add</a></td>';
            strHtml += '</tr>';
        });
    } else {
        strHtml += '<tr><th colspan="4"><label class="float-left padded" style="vertical-align: center;">The work for this quote and the quote have been cancelled. <br/>';
        strHtml += 'Please return to the Quote tab where you will be able to Re-Quote if needed. </label></th></tr>';
    }

    strHtml += '<tr><td colspan="4" style="padding:10px; background-color: white; cursor: default;">&nbsp;</td></tr>';
    strHtml += '</table>';
    panel.innerHTML = strHtml;
}

function SetWorkItemStatus(workItemId, setStatus) {
    var workItem = $.grep(ObjWorkSummary.WorkItems, function (e) { return e.Id == workItemId; });
    workItem[0].Status = setStatus;
    var panel = document.getElementById('pnlCancelledWorkSiteItems');

    var didComplete = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'WorkItemSave',
        {
            item: workItem[0]
        },
        false);
    // Check if the item has been updated.
    if (!didComplete) {
        DisplayMessage(panel.id, 'Error: Arb Pro service failed to save the updates to the item.');
    }

    if (!isCancelled) {
        Work_Load();
    }
}

function CancelledItems_BuildHtml() {
    
    var strHtml = '';
    strHtml += '<table class="searchGrid width-full"><col width="8%" /><col width="22%" /><col width="70%" />';
    strHtml += '<tr><th colspan="3"><label class="float-left padded" style="vertical-align: center;">The work for this quote and the quote have been cancelled. <br/>';
    strHtml += 'Please return to the Quote tab where you will be able to Re-Quote if needed. </label></th></tr>';
    strHtml += '<tr><th colspan="3"><label class="float-left padded" style="vertical-align: center;">Here is the list of current items at this stage. </label></th></tr>';

    ObjWorkSummary.WorkItems.forEach(function(wi) {
        if (wi.Status != 99) {
            SetWorkItemStatus(wi.Id, 99);
            wi.Status = 99;
        }
        strHtml += '<tr>';
        strHtml += '<td style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
        strHtml += '<td style="vertical-align: top;">' + wi.Name + '</td>';
        strHtml += '<td style="vertical-align: top;">' + wi.Description + '</td>';
        strHtml += '</tr>';
    });

    strHtml += '<tr><td colspan="3" style="padding:10px; background-color: white; cursor: default;">&nbsp;</td></tr>';
    strHtml += '</table>';

    document.getElementById('btnMarkCompleted').disabled = true;
    document.getElementById('btnCancelQuote').disabled = true;

    return strHtml;
}

function DisplayPurchaseOrderNo(QuoteId) {
    Dialog_OpenPage(
                   'Purchase Order Number',
                   'Dialogs/PurchaseOrderNoEdit.aspx?QuoteId=' + QuoteId,
                   'content',
                   700,
                   300,
                   true,
                   {
                       'Delete': {
                           text: 'Delete',
                           class: 'ui-dialog-button',
                           click: function () {
                               if (confirm('Are you sure you want to delete this entry?')) {
                                   retVal = Ajax_CallService(
                                      'Services/Web/WorkFlow.asmx',
                                      'DeletePONoFromQuote',
                                       {
                                           QuoteID: QuoteId
                                       },
                                      false);
                                   if (retVal == -1) {
                                       alert('Error in database, please try again.');
                                   }
                                   else {
                                       $(this).dialog('close');
                                   }

                               }

                           }
                       },
                       'Save': {
                           text: 'Save',
                           class: 'ui-dialog-button',
                           click: function () {
                               var PONo = document.getElementById('txtPONo').value
                               retVal = Ajax_CallService(
                                      'Services/Web/WorkFlow.asmx',
                                      'SavePONoOnQuote',
                                       {
                                           QuoteID: QuoteId,
                                           PONo: PONo
                                       },
                                      false);
                               if (retVal == -1) {
                                   alert('Error in database, please try again.');
                               }
                               else {
                                   $(this).dialog('close');
                               }
                           }
                       },
                       'Close': {
                           text: 'Close',
                           class: 'ui-dialog-button',
                           click: function () {
                               $(this).dialog('close');
                           }
                       }
                   },
                   null,
                   function () {
                       retVal = Ajax_CallService(
                               'Services/Web/WorkFlow.asmx',
                               'GetOnlyQuote',
                                {
                                    QuoteID: QuoteId
                                },
                               false);
                       document.getElementById('txtPONo').value = retVal.PurchaseOrderNumber;
                   });
}
