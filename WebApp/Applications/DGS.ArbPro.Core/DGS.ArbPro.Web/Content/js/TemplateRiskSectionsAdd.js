﻿
var risk = null;
var allSections = null;

function AddOrEditTemplateSections(riskObj) {
    risk = riskObj;

    allSections = GetAllRiskSections();

    UpdateTable();
}

function UpdateTable() {
    var panel = document.getElementById('pnlTableContainer');
    var strHtml = '';
    strHtml += '<table class="searchGrid width-full white"><col width="80%"></col><col width="20%"></col>';
    //Construct the html
    allSections.forEach(function (as) {
        var isIncluded = false;
        if (!as.IsDeleted) {
            if (risk.TemplateSections && risk.TemplateSections.length > 0) {
                risk.TemplateSections.forEach(function(s) {
                    if (s.Id == as.Id) {
                        isIncluded = true;
                    }
                });
            }
            strHtml += '<tr id="sec' + as.Id + '" onclick="ItemSelected( \'' + as.Id + '\');"><td>' + as.Title + '</td>';
            strHtml += '<td style="text-align: center; cursor: default;"><input id="chkSection_' + as.Id + '" type="checkbox" style="cursor: pointer;"';
            if (isIncluded) {
                strHtml += 'checked/></td></tr>';
            } else {
                strHtml += '/></td></tr>';
            }
        }
    });

    strHtml += '</table>';

    //display the grid
    panel.innerHTML = strHtml;
}

function ItemSelected(sectionId) {
    var section = {};
    allSections.forEach(function (as) {
        if (!as.IsDeleted) {
            document.getElementById('sec' + as.Id).className = "";
            if (as.Id == sectionId) {
                section = as;
                document.getElementById('sec' + as.Id).className = "selected";
            }
        }
    });
    
    PopulateFields(section);
}

function PopulateFields(section) {
    document.getElementById('pnlEditSectionDetails').className = '';
    document.getElementById('pnlInfoTextDetails').className = 'hidden';
    var save = document.getElementById('btnSave');
    var name = document.getElementById('txtName');
    var ref = document.getElementById('txtDocument');
    var chapter = document.getElementById('txtChapter');
    var desc = document.getElementById('txtDescription');
    
    if (section) {
        name.value = section.Title;
        ref.value = section.ReferenceDoc;
        chapter.value = section.ChapterRef;
        desc.value = section.Description;

        name.disabled = true;
        ref.disabled = true;
        chapter.disabled = true;
        desc.disabled = true;
        save.disabled = true;
    } else {
        name.value = "";
        ref.value = "";
        chapter.value = "";
        desc.value = "";

        name.disabled = false;
        ref.disabled = false;
        chapter.disabled = false;
        desc.disabled = false;
        save.disabled = false;
    }
}

function SaveSection() {
    var newSection = {};
    newSection.Id = -1;
    newSection.Title = document.getElementById('txtName').value;
    newSection.ReferenceDoc = document.getElementById('txtDocument').value;
    newSection.ChapterRef = document.getElementById('txtChapter').value;
    newSection.Description = document.getElementById('txtDescription').value;

    Save(newSection);
}

function Save(section)
{
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'SaveSectionAndMapToTemplate',
        {
            riskId: risk.Id,
            section: section
        },
        false
    );

    if (retVal) {
        document.getElementById('pnlEditSectionDetails').className = 'hidden';
        document.getElementById('pnlInfoTextDetails').className = 'center';
        risk = GetRiskForId(risk.Id);
        AddOrEditTemplateSections(risk);
    }
    else {
        alert('Could not save template.');
    }

    return retVal;
}

function UpdateTemplateSections() {
    allSections.forEach(function (as) {
        if (!as.IsDeleted) {
            var chkSection = document.getElementById('chkSection_' + as.Id).checked;
            if (chkSection) {
                if (!Update(as.Id)) {
                    return false;
                }
            } else {
                if (!Remove(as.Id)) {
                    return false;
                }
            }
        }
    });

    return true;
}

function Remove(sectionId) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'RemoveTemplateRiskMapping',
        {
            riskId: risk.Id,
            sectionId: sectionId
        },
        false
    );

    return retVal;
}

function Update(sectionId) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'MapSectionToTemplate',
        {
            riskId: risk.Id,
            sectionId: sectionId
        },
        false
    );
    
    return retVal;
}

function GetAllRiskSections() {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'GetAllRiskSections',
        {
        },
        false
    );

    return retVal;
}


function GetRiskForId(riskId) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'GetRiskTemplatesForId',
        {
            riskTemplateId: riskId
        },
        false
    );

    return retVal;
}