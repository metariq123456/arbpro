﻿var Dates_curTab = 1;
var ObjWorkPlan = null;
var WorkPlanId = -1;
var ListTools = [];
var ListStaff = [];     // The array selected staff
var ListTeams = [];     // The array of Teams
var SelectedStaff = [];
var SelectedTools = [];
var SelectedTeamId = -1;
var PnlError = 'pnlDlgClndrError';
var ObjApp = null
var ObjWorkSummary = null;
function Load_BookJobDatesEdit() {
    var ParamWorkPlaneId = GetURLParameter('WorkPlanId');
    if (ParamWorkPlaneId != 'null') {
        WorkPlanId = ParamWorkPlaneId;
    }
    Teams_Load();
    WorkPlan_Load();
    Tools_Load();
    SwitchDatesTab(1);
    BindAllTabGrid();
    var WorkId = GetURLParameter('WorkId');
    PopulateClientInfoPanel(WorkId);
    var CurTab = GetURLParameter("CurTab");
    if (CurTab != 'null' && CurTab > 0) {
        SwitchBookingTab(parseInt(CurTab));
        updateQueryStringParam("CurTab", "");
        SwitchDatesTab(2)
    }
    else {
        SwitchBookingTab(0);
    }
    LoadWorkItems();
    var isSaved = GetURLParameter('Save');
    if (isSaved != 'null' && isSaved == '1') {
        updateQueryStringParam("Save", "2");
        ShowAlertMessage("Booking information saved");
    }
    else if (isSaved == 'null')
    {
        ShowAlertMessage("The calendar to the right shows work already booked, use this calendar to check your availability to book this quotation. Once you know the dates you want to start and finish this job, use the calendar to the left to select your dates.");
    }
    DisplayLogisticsTab();
}

//      Async function to load a work plan from the service - Loads a default new object if the Id is -1
function WorkPlan_Load() {

    try {
        Ajax_CallService(
            'Services/Web/WorkProgramming.asmx',
            'GetWorkPlan',
            {
                id: WorkPlanId,
                includeTools: true
            },
            false,
            function (result) {
                debugger;
                ObjWorkPlan = result;
                SelectedStaff = ObjWorkPlan.Personnel;
                ListStaff = Ajax_CallService(
                    'Services/Web/WorkProgramming.asmx',
                    'GetStaffListAsTeamStaff',
                    {},
                    false
                );
                //ListStaff = ObjWorkPlan.Personnel;
                SelectedTools = ObjWorkPlan.Tools;
                SelectedTeamId = ObjWorkPlan.TeamId;
                var WorkSummary = Ajax_CallService(
                       'Services/Web/WorkProgramming.asmx',
                       'GetWorkSummaryForWorkOnQuote',
                       {
                           id: ObjWorkPlan.WorkId
                       },
                       false
                   );
                updateQueryStringParam("QuoteId", ObjWorkPlan.QuoteId);
                updateQueryStringParam("SiteId", ObjWorkPlan.JobSiteId);
                updateQueryStringParam("WorkId", ObjWorkPlan.WorkId);
                var JobId = -1;
                if (ObjWorkPlan.ScheduledItems.length > 0) {
                    JobId = ObjWorkPlan.ScheduledItems[0].JobId;
                }
                else {
                    JobId = WorkSummary.WorkItems[0].JobId;
                }
                updateQueryStringParam("JobId",JobId);
                updateQueryStringParam("WorkItmId", "200");
            }
       );
    }
    catch (err) {
        DisplayMessage(PnlError, 'ERROR: Unable to conect to Arb Pro service - ' + err.toString());
    }
}
//Function to change the CSS of the tab panels
function SwitchBookingTab(selectedIndex) {
    for (var index = 0; index < 10; index++) {
        var tabElement = document.getElementById('tabWorkJobStage_' + index.toString());
        var pnlElement = document.getElementById('pnlWorkJobStage_' + index.toString());
        if (index == selectedIndex) {
            tabElement.className = 'workJobTab activeTab';
            pnlElement.className = '';
            if (index == 4) {
                pnlElement.innerHTML = GetWaitImageForPanel('BookingDetails_' + index.toString(), 'Loading Please Wait');
            }
        } else {
            tabElement.className = 'workJobTab';
            pnlElement.className = 'hidden';
        }
    }
    LoadSelectedBookDatesTab(selectedIndex);
}

function LoadSelectedBookDatesTab(selectedTab) {
    switch (selectedTab) {
        case 4:
            PopulateQuoteDocuments();
            break;
        case 5:
            var QuoteId = GetURLParameter('QuoteId');
            var SiteId = GetURLParameter('SiteId');
            QuoteItemMap_Load(QuoteId, SiteId);
            break;
        case 6:
            LoadRiskAssessment();
            break;
       
    }

}


function SwitchDatesTab(selectedIndex) {
    debugger;
    for (var index = 1; index < 5; index++) {
        var tabElement = document.getElementById('tabDates_' + index.toString());
        var pnlElement = document.getElementById('pnlDates_' + index.toString());
        if (index == selectedIndex) {
            tabElement.className = 'workJobTab activeTab';
            pnlElement.className = '';

        } else {
            tabElement.className = 'workJobTab';
            pnlElement.className = 'hidden';
        }
    }
    LoadDatesTab(selectedIndex);
}

function LoadDatesTab(SelectedIndex) {
    switch (SelectedIndex) {
        case 3:
            var WorkId = GetURLParameter('WorkId');
            PopulateClientInfoPanel(WorkId);
            break;
    }
}
function PopulateQuoteDocuments() {
    var DocumentPanel = document.getElementById('pnlWorkJobStage_4');
    var JobId = GetURLParameter("JobId");
    try {
        var JobDocs = Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'getJobDocuments',
                {
                    JobId: JobId
                },
                true,
                displayQuoteDocuments);

    }
    catch (err) {
        alert(err.toString());
    }
}

function displayQuoteDocuments(JobDocs) {
    var DocumentPanel = document.getElementById('pnlWorkJobStage_4');

    var strHtml = ''
    strHtml += '<div class="width-full float-left white-light"><button type="button" onclick="ShowDocumentsDialogOnBookDates(-1);" class="button-thin float-left padded">Add New Documents</button></div>';
    strHtml += '<div class="width-full float-left">';
    console.log('JobDocs:' + JobDocs.length);
    if (!JobDocs) {
        DisplayMessage(DocumentPanel.id, 'Error: no data return from service');
    }
    else {
        strHtml += '<table class="searchGrid width-full">';
        if (JobDocs.length == 0) {
            strHtml += '<tr class="white-light">There are no documents records to display. Please add documents by clicking the "Add New Documents" button.</tr>';
        } else {
            strHtml += '<tr class="white-light"><th style="padding-left: 10px;">Documents Name</th><th style="text-align:center;">Email Attachment</th></tr>';
            JobDocs.forEach(function (c) {
                console.log(c.DefaultAttachment);
                strHtml += '<tr>';
                strHtml += '<td onclick="ShowDocumentsDialogOnBookDatesEdit(' + c.Id + ');" style="padding-left: 10px;">' + c.DocumentName + '</td>';
                if (c.DefaultAttachment) {
                    strHtml += '<td style="width:21%; vertical-align: middle; text-align:center;" ><input type="checkbox" id="chkDefaultAttachment" checked onChange="ToggleDefaultAttachment(' + c.Id + ',this);" id="(' + c.Id + ');"/></td>';
                }
                else {
                    strHtml += '<td style="width:21%; vertical-align: middle; text-align:center;" ><input type="checkbox" id="chkDefaultAttachment" onChange="ToggleDefaultAttachment(' + c.Id + ',this);" id="(' + c.Id + ');"/></td>';
                }

                strHtml += '</tr>';
            });
        }
    }
    strHtml += '</table>';
    strHtml += '</div>';
    DocumentPanel.innerHTML = strHtml;
}
function ToggleDefaultAttachment(DocumentId, thisElement) {
    console.log(thisElement.checked);
    var Orgs = Ajax_CallService(
            'Services/web/WorkFlow.asmx',
            'ToggleDefaultAttachmentForJob',
            { DocId: DocumentId, DefaultAttachment: thisElement.checked },
            false
        );
}

//Sync service call to load all tools into local memory
function Tools_Load() {
    try {
        Ajax_CallService(
               'Services/Web/WorkProgramming.asmx',
               'GetToolListAsWorkTools',
               {
               },
               false,
               function (results) {
                   ListTools = results;
               }
           );
    } catch (err) {
        DisplayMessage(PnlError, 'Error: Unable to communicate with Arb Pro server.');
    }
}

function BindAllTabGrid() {

    var panelstaff = document.getElementById('pnlWorkJobStage_1');
    Ajax_CallService(
            'Services/Web/WorkProgramming.asmx',
            'GetStaffListAsTeamStaff',
            {},
            false
          );
    var staffhtml = GenerateStaffHtml(ListStaff);
    panelstaff.innerHTML = staffhtml;

    var panel = document.getElementById('pnlWorkJobStage_2');
    ListTools = Ajax_CallService(
               'Services/Web/WorkProgramming.asmx',
               'GetToolListAsWorkTools',
               {
               },
               false
           );
    var toolhtml = GenerateToolHtml(ListTools);
    panel.innerHTML = toolhtml;
    SetSelectedTools();
    ObjApp = Ajax_CallService(
                    'Services/Web/Calendar.asmx',
                    'GetAppointmentForWorkPlan',
                    {
                        workPlanId: WorkPlanId
                    },
                    false
                  );
    var panelcomment = document.getElementById('pnlWorkJobStage_3');
    var strCommentHtml = '<textarea autofocus id="txtApptComments" class="dlgBookWorkInput" cols="80" rows="9" style="width: 90%; height: 100px;" name="txtApptComments">' + ObjApp.Comments + '</textarea>';
    panelcomment.innerHTML = strCommentHtml;
    /*Work Item
    var Grid = document.getElementById("pnlWorkItemTable");
    var listHtml = '';
    listHtml += '<table class="searchGrid width-full bookJobDatestable" style ="font-size: 11px;"><tr><th>No</th><th>Item</th><th>Work</th><th>Value</th></tr>';

    //Display any work items not scheduled
    if (ObjWorkPlan.ScheduledItems && ObjWorkPlan.ScheduledItems.length > 0) {
        ObjWorkPlan.ScheduledItems.forEach(function (wi) {
                           
                 listHtml += '<tr><td>' + wi.Code + '</td>';
                listHtml += '<td>' + wi.Name + '</td>';
                listHtml += '<td>' + wi.Description + '</td>';
                listHtml += '<td>' + wi.Value.format() + '</td>';
                //listHtml += '<td style="vertical-align: middle; text-align:center;"><input type="checkbox" id="chkSelectItem_' + wi.Id + '" onchange="AddWorkIdToList(this);" type="checkbox" style="cursor: pointer;"/></td>';
                //listHtml += '<td style="vertical-align: middle; text-align:center;"><button type="button" class="button-thin padded" onclick="javascript: window.location =\'/BookDates.aspx?WorkId=' + wi.WorkId + '&QuoteId=' + ObjWorkSummary.QuoteId + '&SiteId=' + wi.JobSiteId + '\'">Book/See dates for items</button></td></tr>';
            
        });
    }
    listHtml += '</table>';
    listHtml += '<br/><br/><br/><br/><br/><br/>';

    Grid.innerHTML = listHtml;*/
    debugger;
    DisplayWorkTime(ObjWorkPlan.Appointment);
    var DateList = [];
    var MinDate;
    var MaxDate;
    if (ObjWorkPlan.Appointment.CalendarWorkDate && ObjWorkPlan.Appointment.CalendarWorkDate.length > 0) {
        ObjWorkPlan.Appointment.CalendarWorkDate.forEach(function (wd) {
            var reCurringDate = new Date(parseInt((wd.BookDate.substr(6))));
           DateList.push(getFormattedDate(reCurringDate));
        });
       $('#multidates').multiDatesPicker({
            addDates: DateList,
            onSelect: function (selectedDate) {
                var dates = selectedDate.split('/');
                debugger;
                $('#pnlCalendarDates').fullCalendar('gotoDate', dates[2], parseInt(dates[0])-1, dates[1]);
            }
       });
       var StrMinDate = DateList.reduce(function (a, b) { return a < b ? a : b; });
       var StrMaxDate = DateList.reduce(function (a, b) { return a > b ? a : b; });
       MinDate = new Date(StrMinDate);
       MaxDate = new Date(StrMaxDate);
       document.getElementById('lblStartDate').textContent = MinDate.format('dd-MM-yyyy');
       document.getElementById('lblEndDate').textContent = MaxDate.format('dd-MM-yyyy');
    }
    else if (!ObjWorkPlan.Appointment.IsRecurring) {
        var reCurringDate = new Date(parseInt((ObjWorkPlan.Appointment.StartDateTime.substr(6))));
        DateList.push(getFormattedDate(reCurringDate));
        reCurringDate = new Date(parseInt((ObjWorkPlan.Appointment.EndDateTime.substr(6))));
        DateList.push(getFormattedDate(reCurringDate));
        $('#multidates').multiDatesPicker({ addDates: DateList, 
            onSelect: function (selectedDate) {
            var dates = selectedDate.split('/');
            debugger;
                //year,month,day
            $('#pnlCalendarDates').fullCalendar('gotoDate', dates[2], parseInt(dates[0])-1, dates[1]);
        }
            });
        var StrMinDate = DateList.reduce(function (a, b) { return a < b ? a : b; });
        var StrMaxDate = DateList.reduce(function (a, b) { return a > b ? a : b; });
        MinDate = new Date(StrMinDate);
        MaxDate = new Date(StrMaxDate);
        document.getElementById('lblStartDate').textContent = MinDate.format('dd-MM-yyyy');
        document.getElementById('lblEndDate').textContent = MaxDate.format('dd-MM-yyyy');
        
    }
    else if (ObjWorkPlan.Appointment.IsRecurring) {
        var reCurringDate = new Date(parseInt((ObjWorkPlan.Appointment.StartDateTime.substr(6))));
        DateList.push(getFormattedDate(reCurringDate));
        reCurringDate = new Date(parseInt((ObjWorkPlan.Appointment.EndDateTime.substr(6))));
        DateList.push(getFormattedDate(reCurringDate));
        var StrMinDate = DateList.reduce(function (a, b) { return a < b ? a : b; });
        var StrMaxDate = DateList.reduce(function (a, b) { return a > b ? a : b; });
        MinDate = new Date(StrMinDate);
        MaxDate = new Date(StrMaxDate);
        document.getElementById('lblStartDate').textContent = MinDate.format('dd-MM-yyyy');
        document.getElementById('lblEndDate').textContent = MaxDate.format('dd-MM-yyyy');
    }
    DisplayDatesSummary(DateList, ObjWorkPlan.Appointment);
    RecurringButton();
    LoadCalendar();
    var QuoteId = GetURLParameter('QuoteId');
    var ObjSelectedQuote = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetQuote',
            {
                QuoteID: QuoteId
            },
            false
        );
    document.getElementById('lblSurveyorName').textContent = 'Surveyor Name: ' + ObjSelectedQuote.SurveyorName;
    var strMinDate = MinDate.format('yyyy/MM/dd');
    var strMinDateSplit = strMinDate.split('/');
    console.log(strMinDateSplit);
    $('#pnlCalendarDates').fullCalendar('gotoDate', strMinDateSplit[0], parseInt(strMinDateSplit[1]) - 1, strMinDateSplit[2]);
    var today_date = new Date(MinDate);
    today_date.setMonth((new Date(MinDate.format('yyyy-MM-dd'))).getMonth());

    $('#multidates').datepicker('setDate', today_date);
}
function sortDates(a, b) {
    return a.getTime() - b.getTime();
}
function getFormattedDate(date) {
    var year = date.getFullYear();

    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;

    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;

    return month + '/' + day + '/' + year;
}

function DisplayDatesSummary(DatesList, Appointment) {
    debugger;
    var Dates = [];
    DatesList.forEach(function (date) {
        Dates.push(new Date(date));
    });
    var SortedDates = Dates.sort(sortDates);
    var CurrentQuoteId = GetURLParameter('QuoteId');
    var CurrentSiteId = GetURLParameter('SiteId');
    var results = Ajax_CallService(
                   'Services/Web/RiskAssessment.asmx',
                   'ValidateAssessmentForSite',
                   {
                       quoteId: CurrentQuoteId,
                       jobSiteId: CurrentSiteId
                   },
                   false
                   );
    var Grid = document.getElementById("pnlAllWorkSummary");
    var strHtml = '<table class="height-full" width="96%" style="margin-left:10px;margin-top:10px;">'
    strHtml += '<tr><th style="vertical-align: top; text-align: left;">Tree Protection and Permits</th>';
    strHtml += '<th style="vertical-align: top; text-align: right;">Job Information</th>';
    strHtml += '</tr>';
    strHtml += '<tr>';
    var CurrentJobId = GetURLParameter('JobId');
    var ObjJob = Ajax_CallService(
                     'Services/Web/WorkFlow.asmx',
                     'GetJob',
                     {
                         jobId: CurrentJobId
                     },
                     false
                 );
    var CurrentClientSiteId = -1;
    ObjJob.WorkSites.forEach(function (ws) {
        if (ws.Id == CurrentSiteId) {
            CurrentClientSiteId = ws.SiteId;
            return false;
        }
    });
    var Tpo = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'TposGetForQuote',
            {
                quoteId: CurrentQuoteId
            },
            false);
    var index = 0;
    $(Tpo).each(function (i, o) {
        if (o.JobSiteId == CurrentClientSiteId) {
            index = i;
            return false;
        }
    });
    var CurrentTpo = Tpo[index];
    var TpoStatus = '';
    if (CurrentTpo != null && CurrentTpo.Status != null) {
        TpoStatus = getProtectionTreeOrderOptions('TpoStatusOptions', CurrentTpo.Status);
    }
    var TpoConcentGranted = '';
    if (CurrentTpo != null && CurrentTpo.ProgressStage != null) {
        TpoConcentGranted = getProtectionTreeOrderOptions('TpoProgressStageOptions', CurrentTpo.ProgressStage);
    }

    strHtml += '<td style="vertical-align: top; text-align: left;">';
    if (TpoStatus == 'Unknown') 
    {
        strHtml += '<span style="display:block;">Trees Protected: <span style="color:red">Unknown</span></span>';
    }
    else if (TpoStatus == 'Protected') {
        strHtml += '<span style="display:block;">Trees Protected: Yes</span>';
    }
    else {
        strHtml += '<span style="display:block;">Trees Protected: <span style="color:red">No</span></span>';
    }

    if (TpoConcentGranted == "Consent Granted") {
        strHtml += 'Consent Granted: Yes';
    }
    else {
        strHtml += 'Consent Granted: <span style="color:red">No</span>';
    }
    
    PermitsNecessaryInfo = Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'PermitsNecessaryGetForQuote',
           {
               quoteId: CurrentQuoteId
           },
           false);
    if (PermitsNecessaryInfo.PermitNecessary == 1) {
        strHtml += '<span style="display:block;margin-top:10px;">Permits necessary: Yes</span>';
    }
    else
    {
        strHtml += '<span style="display:block;margin-top:10px;">Permits necessary: <span style="color:red">No</span></span>';
    }

    if (PermitsNecessaryInfo.PermitObtained == 1) {
        strHtml += 'Permits Obtained: Yes';
    }
    else {
        strHtml += 'Permits Obtained: <span style="color:red">No</span>';
    }
   
    if (results.length == 0) {
        strHtml += '<span style="display:block;margin-top:10px;">Risk Assessment Created: Yes</span>';
    }
    else {
        strHtml += '<span style="display:block;margin-top:10px;">Risk Assessment Created: <span style="color:red">No</span></span>';
    }
    strHtml += '</td>';
    var CurrentWorkId = GetURLParameter('WorkId');
    var result =  Ajax_CallService(
       'Services/Web/WorkProgramming.asmx',
       "GetJobWorkSummaryForWork",
       {
           id: CurrentWorkId
       },
       false);
    strHtml += '<td style="vertical-align: top; text-align: right;">';
    strHtml += '<span style="display:block;">Quote No.:' + result.QuoteNo + '</span>';
    strHtml += 'Job Booked: Yes';
    strHtml += '</td>';
    strHtml += '</tr>';
    strHtml += "</table>";
    Grid.innerHTML = strHtml;
    var Grid = document.getElementById("pnlDatesSummary");
    var strHtml = '<table class="searchGrid height-full" width="96%" style="margin-left:10px;margin-top:10px;">';//<col width="10%" /><col width="15%" /><col width="65%" /><col width="15%" />';
    strHtml += '<tr><th style="vertical-align: top; text-align: center;">Date</th>';
    strHtml += '<th style="vertical-align: top; text-align: center;">Day</th>';
    strHtml += '<th style="vertical-align: top; text-align: center;">Month</th>';
    strHtml += '<th style="vertical-align: top; text-align: center;">Year</th>';
    strHtml += '<th style="vertical-align: top; text-align: center;">Start Time</th>';
    strHtml += '<th style="vertical-align: top; text-align: center;">End Time</th>';
    strHtml += '<th style="vertical-align: top; text-align: center;">Last Day Time</th>';
    strHtml += '</tr>';
    var locale = "en-us"
    var StartTimeDay1Date;
    var strStartTimeDay1Date;
    var ConnectiveDayEndTimeDate;
    var strConnectiveDayEndTimeDate;
    var LastDayEndTimeNewDate;
    var strLastDayEndTimeNewDate;
    if (ObjWorkPlan.Appointment.CalendarWorkDate.length == 0 && !ObjWorkPlan.Appointment.IsRecurring) {
        StartTimeDay1Date = toDateFromJson(Appointment.StartDateTime);
        strStartTimeDay1Date = StartTimeDay1Date.format('hh:mm tt');
        ConnectiveDayEndTimeDate = toDateFromJson(Appointment.EndDateTime);
        strConnectiveDayEndTimeDate = ConnectiveDayEndTimeDate.format('hh:mm tt');
        LastDayEndTimeNewDate = toDateFromJson(Appointment.EndDateTime);
        strLastDayEndTimeNewDate = LastDayEndTimeNewDate.format('hh:mm tt');
    }
    else {
        StartTimeDay1Date = toDateFromJson(Appointment.StartTimeDay1New);
        strStartTimeDay1Date = StartTimeDay1Date.format('hh:mm tt');
        ConnectiveDayEndTimeDate = toDateFromJson(Appointment.ConnectiveDayEndTimeNew);
        strConnectiveDayEndTimeDate = ConnectiveDayEndTimeDate.format('hh:mm tt');
        LastDayEndTimeNewDate = toDateFromJson(Appointment.LastDayEndTimeNew);
        strLastDayEndTimeNewDate = LastDayEndTimeNewDate.format('hh:mm tt');
    }
    
    
    SortedDates.forEach(function (date) {
        strHtml += '<tr>';
        strHtml += '<td style="vertical-align: top; text-align: center;">' + date.format("dd-MM-yyyy") + '</td>';
        strHtml += '<td style="vertical-align: top; text-align: center;">' + date.toLocaleString(locale, { weekday: "long" }); + '</td>';
        strHtml += '<td style="vertical-align: top; text-align: center;">' + date.toLocaleString(locale, { month: "long" }); + '</td>';
        strHtml += '<td style="vertical-align: top; text-align: center;">' + date.getFullYear() + '</td>';
        strHtml += '<td style="vertical-align: top; text-align: center;">' + strStartTimeDay1Date + '</td>';
        strHtml += '<td style="vertical-align: top; text-align: center;">' + strConnectiveDayEndTimeDate + '</td>';
        strHtml += '<td style="vertical-align: top; text-align: center;">' + strLastDayEndTimeNewDate + '</td>';
        strHtml += '</tr>';
    });
    strHtml += "</table>";
    Grid.innerHTML = strHtml;
    document.getElementById('lblBookedDates').textContent = "Booked Dates";
    document.getElementById('lblBookedText').textContent = "The screen below shows the dates this job is already booked for, to make changes to these date, please go to the dates and times tab.";
}

function getProtectionTreeOrderOptions(endpointName, selectedId) {

    try {
        var itemsList = Ajax_CallService(
                'Services/Web/Settings.asmx',
                endpointName,
                {},
                false,
                null);

        var count = itemsList.length;
        for (var index = 0; index < count; index++) {
            if (selectedId == index) {
                return itemsList[index];
            }
        }
    }
    catch (err) {

    }
    return "";
}

function RecurringButton() {
    var ButtonPnl = document.getElementById("pnlRecurringButton");
    var listHtml = '';
    if (ObjWorkPlan.Appointment.IsRecurring) {
        listHtml += '<label class="bookJobDatesCheckBoxLabel float-left"><input type="checkbox" class="bookJobDatesCheckBoxInput" onchange="ShowBookButtonForRecurring(this);" checked/>Recurring?</label>';
        listHtml += '<input type="button" value="Book" class="button-thin padded float-right" id="BookRecurring" onclick="ShowFrequencyBookingDialog(1);">';
    }
    else {
        listHtml += '<label class="bookJobDatesCheckBoxLabel float-left"><input type="checkbox" class="bookJobDatesCheckBoxInput" onchange="ShowBookButtonForRecurring(this);"/>Recurring?</label>';
        listHtml += '<input type="button" value="Book" class="button-thin padded float-right hidden" id="BookRecurring" onclick="ShowFrequencyBookingDialog(0);">';
    }
    
    
    ButtonPnl.innerHTML = listHtml;
}

function ShowBookButtonForRecurring(BookElement) {
    console.log("checked:" + BookElement.checked);
    if (BookElement.checked) {
        $("#BookRecurring").removeClass("hidden");
    }
    else {
        $("#BookRecurring").addClass("hidden");
    }
}
function Teams_Load() {
    try {
        Ajax_CallService(
        'Services/Web/WorkProgramming.asmx',
        'GetAllTeam',
        {},
        false,
        function (results) {
            ListTeams = results;
        }
        );

    } catch (err) {
        DisplayMessage(PnlError, 'Error: Unable to communicate with Arb Pro server.');
    }
}

function GenerateStaffHtml(staffList) {
    debugger;
    var strHtml = '<table class="width-full searchGrid height-full">';
    strHtml += '<tr><th colspan="3">';
    //  Due to the changes in the booking process the user must now be able to change the team
    //strHtml += '<select id="ddlTeam" class="dlgBookWorkInput padded" style="width:65%;" name="ddlTeam" onchange="SelectedTeamChanged();">';
    strHtml += '<select id="ddlTeam" class="dlgBookWorkInput padded" style="width:65%;" name="ddlTeam">';
    //Check if there is a team selected
    if (ObjWorkPlan.TeamId == -1) {
        strHtml += '<option value="-1" selected="selected"> Select Team for Work</option>';
    } else {
        strHtml += '<option value="-1"> Select Team for Work</option>';
    }
    ListTeams.forEach(function (t) {
        strHtml += '<option value="' + t.Id + '"';
        if (t.Id == SelectedTeamId) {
            strHtml += ' selected="selected"';
        }
        strHtml += '>' + t.Name + '</option>';
    });
    strHtml += '</select>';
   // strHtml += '<button type="button" onclick="ShowEditStaffDiv();" class="button-thin float-right padded">Edit Staff</button></th>';
    strHtml += '<tr><th>Name</th><th>Assigned</th><th>Role</th></tr>';
    staffList.forEach(function (c) {
        var curId = -1;
        if (!c.StaffId) {
            curId = c.Id;
        } else {
            curId = c.StaffId;
        }
        strHtml += '<tr onclick="SetStaffSelectedClicked(' + curId + ')">';
        strHtml += '<td style="width: 50%; padding-left: 10px;">';
        if (!c.Name) {
            strHtml += c.StaffName + '</td>';
        } else {
            strHtml += c.Name + '</td>';
        }
        var foundSelected = $.grep(SelectedStaff, function (e) { return e.StaffId == curId; });
        strHtml += '<td style="text-align:center;"> <input type="checkbox" Id="chkTeamStaff_' + curId + '"';
        if (foundSelected && foundSelected.length == 1) {
            strHtml += 'checked';
        }
        strHtml += '/>  </td> ';
        strHtml += '<td class="companyInput"><input type="text" Id="txtTeamTask_' + curId + '" placeholder="Staff Role" title="The role within the team for the staff member"';
        if (foundSelected && foundSelected.length == 1 && foundSelected[0].Task && foundSelected[0].Task.length > 0) {
            strHtml += 'value="' + foundSelected[0].Task + '"';
        }
        strHtml += '/>  </td> ';
        strHtml += '</tr>';
    });
    strHtml += '</table>';
    return strHtml;
}

//      Function to handle the checked changed event of a staff row entry
function SetStaffSelectedClicked(staffId) {
    var chkBox = document.getElementById('chkTeamStaff_' + staffId.toString());
    chkBox.checked = !chkBox.checked;
    //  Update the numer of personel displayed
    var personel = 0;
    //iterate through each staff member and check if the chkBox is selected 
    ListStaff.forEach(function (p) {
        var chkBox = document.getElementById('chkTeamStaff_' + p.StaffId.toString());
        if (chkBox && chkBox.checked) {
            personel++;
        }
    });
   
}


//      Function to generate the html for the tools panel
function GenerateToolHtml(toolList) {
    var strHtml = '<div style="height: 400px; overflow: auto;"><table class="rtGrid width-full white" style="table-layout:fixed; word-wrap:break-word;">';
    //strHtml += '<tr><th colspan="2"><button type="button" onclick="ShowEditToolsDiv();" class="button-thin float-right padded">Edit Tools</button></th></tr>';

    
     strHtml += '<tr><th colspan="2">Input the number required against a tool, to add it to the work plan.</th></tr>';
    
    strHtml += '<tr><th>Tool</th><th>Number Required</th></tr>';
    //  Iterate through all the tools and create a row in the table
    toolList.forEach(function (c) {
        strHtml += '<tr>';
        strHtml += '<td class="rowData" style="maxwidth: 60%; padding-left: 10px;">' + c.Item + '</td>';
        strHtml += '<td class="rowData"><input type="text" Id="txtToolNum_' + c.ToolId + '" placeholder="Add a value to select" title="The number of tools required for the work."/>  </td></tr> ';
    });
    strHtml += '</table></div>';
    return strHtml;
}

function SetSelectedTools() {
    try {
        //Check if there is a list of currently selected tools
        if (SelectedTools && SelectedTools.length > 0) {
            var element;

            SelectedTools.forEach(function (t) {
                element = document.getElementById('txtToolNum_' + t.ToolId.toString());
                //Check the element is valid and there are a number of tools required
                if (element && t.Number > 0) {
                    //Set the value to be displayed
                    element.value = t.Number.toString();
                }
            });
        }
    }
    catch (err) {
        DisplayMessage('pnlScheduleDetail_1', 'Error: Unable to display currently selected tools list - ' + err.toString());
    }

}

var updateQueryStringParam = function (key, value) {

    var baseUrl = [location.protocol, '//', location.host, location.pathname].join(''),
        urlQueryString = document.location.search,
        newParam = key + '=' + value,
        params = '?' + newParam;

    // If the "search" string exists, then build params from it
    if (urlQueryString) {

        updateRegex = new RegExp('([\?&])' + key + '[^&]*');
        removeRegex = new RegExp('([\?&])' + key + '=[^&;]+[&;]?');

        if (typeof value == 'undefined' || value == null || value == '') { // Remove param if value is empty

            params = urlQueryString.replace(removeRegex, "$1");
            params = params.replace(/[&;]$/, "");

        } else if (urlQueryString.match(updateRegex) !== null) { // If param exists already, update it

            params = urlQueryString.replace(updateRegex, "$1" + newParam);

        } else { // Otherwise, add it to end of query string

            params = urlQueryString + '&' + newParam;

        }

    }
    window.history.replaceState({}, "", baseUrl + params);
};

function ShowDocumentsDialogOnBookDates(docId) {

    Dialog_OpenPage('Documents',
        'Dialogs/JobDocumentsAdd.aspx',
        'JobDocs',
        500,
        600,
        true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        PopulateQuoteDocuments();
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
                    'Content/js/JobDocuments.js?V=3.23.5',
                    function () { LoadDateAndStaff(docId); }
        );
}

function ShowDocumentsDialogOnBookDatesEdit(docId) {
    Dialog_OpenPage('Documents',
        'Dialogs/JobDocumentsEdit.aspx',
        'JobDocsEdit',
        500,
        600,
        true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        PopulateQuoteDocuments();
                        $(this).dialog('close');
                    }
                },
                'Open': {
                    text: 'Open',
                    class: 'ui-dialog-button',
                    click: function () {
                        DocumentPreview()
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/JobDocuments.js?V=3.23.5',
            function () { JobDocumentsLoad(docId); }
        );
}

/*  Initial load function: Loads a site and creates the map then loads the Quote Items
    Params:
        quoteId:    Int - The Id of the quote for the items
        siteId:     Int - The Id of the site the items are located at
*/
function QuoteItemMap_Load(quoteId, siteId) {
    //Error messages raised in this function are displayed in the main content view not the error messages panel
    if (quoteId < 1 || siteId < 1) {
        //The ids supplied are not valid, display an error
        DisplayMessage('pnlItemMapCanvas', 'Error: The Quote Id or the Site are not valid.');
    }
    else {
        try {
            QuoteId = quoteId;
            MapDetailLoad(siteId);
        } catch (err) {
            DisplayMessage('pnlItemMapCanvas', 'Error: Unable to display item map - ' + err.toString());
        }
    }
}

/*
    Function to generate an Async service call to load the map details for the specified quote and job site
    PARAMS:
            siteId:     Int - The Job Site Id
*/
function MapDetailLoad(siteId) {
    try {
        Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'GetQuoteSiteMap',
                {
                    JobSiteID: siteId,
                    QuoteID: QuoteId
                },
                true,
                MapDetailLoadCallBack
       );
    } catch (err) {
        DisplayMessage('pnlError', 'Error: Unable to load Quote Items for Job Site - ' + err.toString());
    }
}

/*
    Async service callback to display map details
    PARAMS:
        result:     Types.MapDetailsSearchResult - The map data for the selected quote site
*/
function MapDetailLoadCallBack(result) {
    debugger;
    var panel = document.getElementById('pnlItemMapCanvas');
    try {
        //The returned object is valid so setup the map
        if (result && result.Id > 0) {
            CurDetails = result;
            //Create the map options
            var mapOptions = {
                center: new google.maps.LatLng(54, -2),
                zoom: 20,
                mapTypeId: google.maps.MapTypeId.SATELLITE
            };
            //Create the map and set it to the panel
            GlgMap = new google.maps.Map(panel, mapOptions);

            //Create the marker for the Site if possible
            if (CurDetails.SitePostCode && CurDetails.SitePostCode.length > 0) {
                var geocoder = new google.maps.Geocoder();
                try {
                    geocoder.geocode({ 'address': CurDetails.SitePostCode + ', UK' },
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var loc = results[0].geometry.location;
                                SetMarker(loc.lat(), loc.lng(), CurDetails.SitePostCode, CurDetails.SiteAddress, true);
                            }
                        });
                }
                catch (ex) {
                    alert('Error when trying to asign item markers: ' + ex.toString());
                }
            }
            //Add the info panel to the map
            AddInfoControl();

            //The main details have been generated now generate the item markers
            QuoteItemsDisplay(CurDetails.SiteItems);

        }
        else {
            DisplayMessage('pnlError', 'ERROR: Arb Pro service did not return valid map details');
        }
    } catch (err) {
        DisplayMessage('pnlError', 'Error: Unable to generate map data - ' + err.toString());
    }
}


/*
    Async service call back function to populate the map with pins for each Quote Item
    PARAMS:
            results:    List<Types.QuoteItem> - A list of all the quote items for the quote job site
*/
function QuoteItemsDisplay(LstQuoteItems) {
    //check if the returned list is valid
    if (!LstQuoteItems || LstQuoteItems.length == 0) {
        DisplayMessage('pnlError', 'Error: Arb Pro service did not return a valid list of Quote items');
    }
    else {
        try {
            LstQuoteItems.forEach(function (itm) {
                //Check if this item could be within the correct bounds
                if (itm.Latitude > 0 || itm.Longitude > 0) {
                    SetMarker(itm.Latitude, itm.Longitude, itm.Code, itm.Name, false);
                }
            });
        } catch (err) {
            DisplayMessage('pnlError', 'Error: Unable to add all map pins for Quote items - ' + err.toString());
        }
    }
}

/*
    Function to add a map pin to the map
    PARAMS:
        lat:        Float - The latitude of the object
        long:       Float - The longitude of the object
        pinTitle:   String - The title to be displayed for the pin
        centerMap:  Bool - Flag to say if the map should be centered on this marker
*/
function SetMarker(lat, long, label, pinTitle, centerMap) {
    var myLatLng = new google.maps.LatLng(lat, long);
    var mapPin = new MarkerWithLabel({ //google.maps.Marker({
        position: myLatLng,
        map: GlgMap,
        title: pinTitle,
        labelContent: label,
        labelAnchor: new google.maps.Point(0, 0),
        labelClass: "map-label" // the CSS class for the label
    });

    if (centerMap) {
        GlgMap.panTo(myLatLng);
    }
}

/*
    Function to generate and add the quote details to the map
*/
function AddInfoControl() {
    var details = document.createElement('div');

    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = 'white';
    controlUI.style.borderStyle = 'solid';
    controlUI.style.borderWidth = '2px';
    controlUI.style.cursor = 'pointer';
    controlUI.style.textAlign = 'left';
    controlUI.title = 'Quote Details';
    details.appendChild(controlUI);

    var controlText = document.createElement('div');
    controlText.style.fontFamily = 'Arial,sans-serif';
    controlText.style.fontSize = '12px';
    controlText.style.paddingLeft = '4px';
    controlText.style.paddingRight = '4px';
    controlText.innerHTML = CurDetails.SideBarDetail;
    controlUI.appendChild(controlText);

    details.index = 2;
    GlgMap.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(details);
}


/*
    Function to print the map.
    This function copies the map canvase into another window, prints the contents of that window then closes it.
*/
function PrintQuoteItemMap() {
    var map = document.getElementById('pnlItemMapCanvas').innerHTML;
    var html = '<html><head><title>Quote Items for Quote Id: ' + QuoteId + '</title>';
    html += "<style>body { padding: 15px; } .map-label {";
    html += "color: black;";
    html += "background-color: white;";
    html += 'font-family: "Arial";';
    html += "font-size: 10px;";
    html += "font-weight: bold;";
    html += "text-align: center;";
    html += "width: 60px;";
    html += "border: 2px solid black;";
    html += "white-space: nowrap;";
    html += "}</style>";
    html += "</head>";
    html += "<body><div id='myprint'>" + map + "</div></body></html>";

    var theWork = window.open('', 'PrintWindow');

    theWork.document.open();
    theWork.document.write(html);
    theWork.document.close();

    setTimeout(function () {
        theWork.print();
        theWork.close();
    }, 10);


    return false;
}

function DisplayWorkTime(Appointment) {
    var strHtml = '';
    strHtml += '<div class="float-left width-full">';
    strHtml += '<div class="workJobField">';
    strHtml += '<label class ="workJobLabel">Start time day 1</label>';
    strHtml += '<input type="text" id="startday1" class ="workJobInput" />';
    strHtml += '</div>';
    strHtml += '<br/><br/>';
    strHtml += '<div class="workJobField">';
    strHtml += '<label class ="workJobLabel">Consective days end time</label>';
    strHtml += '<input type="text" id="consectivedays" class ="workJobInput" />';
    strHtml += '</div>';
    strHtml += '<br/><br/><br/>';
    strHtml += '<div class="workJobField">';
    strHtml += '<label class ="workJobLabel">Last day end time</label>';
    strHtml += '<input type="text" id="lastday" class ="workJobInput" />';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '<br/><br/><br/><br/><br/><br/><br/><br/><br/>';
    document.getElementById('pnlTimesForWork').innerHTML = strHtml;
    debugger;
    var StartTimeDay1Datel;
    var strStartTimeDay1Date;
    var ConnectiveDayEndTimeDate;
    var strConnectiveDayEndTimeDate;
    var LastDayEndTimeNewDate;
    var strLastDayEndTimeNewDate;
    if (ObjWorkPlan.Appointment.CalendarWorkDate.length == 0 && !ObjWorkPlan.Appointment.IsRecurring) {
        StartTimeDay1Date = toDateFromJson(Appointment.StartDateTime);
        strStartTimeDay1Date = StartTimeDay1Date.format('hh:mm tt');

        ConnectiveDayEndTimeDate = toDateFromJson(Appointment.EndDateTime);
        strConnectiveDayEndTimeDate = ConnectiveDayEndTimeDate.format('hh:mm tt');

        LastDayEndTimeNewDate = toDateFromJson(Appointment.EndDateTime);
        strLastDayEndTimeNewDate = LastDayEndTimeNewDate.format('hh:mm tt');
    }
    else {
        StartTimeDay1Date = toDateFromJson(Appointment.StartTimeDay1New);
        strStartTimeDay1Date = StartTimeDay1Date.format('hh:mm tt');
        
        ConnectiveDayEndTimeDate = toDateFromJson(Appointment.ConnectiveDayEndTimeNew);
        strConnectiveDayEndTimeDate = ConnectiveDayEndTimeDate.format('hh:mm tt');
       
        LastDayEndTimeNewDate = toDateFromJson(Appointment.LastDayEndTimeNew);
        strLastDayEndTimeNewDate = LastDayEndTimeNewDate.format('hh:mm tt');
       

    }

    $('#startday1').datetimepicker({
        format: 'g:i A',
        timepicker: true,
        datepicker: false,
        minDate: 0,
        defaultTime: true,
        value: strStartTimeDay1Date,
        ampm: true
    });
    $('#consectivedays').datetimepicker({
        format: 'g:i A',
        timepicker: true,
        datepicker: false,
        minDate: 0,
        defaultTime: true,
        value: strConnectiveDayEndTimeDate,
        ampm: true
    });
    $('#lastday').datetimepicker({
        format: 'g:i A',
        timepicker: true,
        datepicker: false,
        minDate: 0,
        defaultTime: true,
        value: strLastDayEndTimeNewDate,
        ampm: true
    });
    
}

function CalendarBookWork_Delete() {
    var retVal = false;
    try {
        var WorkPlanId = GetURLParameter('WorkPlanId');
        if (WorkPlanId > 0) {
            retVal = DeleteWorkAppointment(ObjWorkPlan.Id);

            if (!retVal) {
                DisplayMessage(PnlError, 'INFORMATION: Failed to delete appointment');
            }
            else {
                window.location = "/Calendar.aspx";
            }
        }

    }
    catch (err) {
        DisplayMessage(PnlError, 'ERROR: Unable to delete the work appointment - ' + err.toString());
    }
    return retVal;
}

//      function to delete a WorkPlan and all of its associated items
function DeleteWorkAppointment(workPlanId) {
    try {
        var value = Ajax_CallService(
                'Services/Web/WorkProgramming.asmx',
                'DeleteWorkPlan',
                {
                    workPlanId: workPlanId
                },
                    false,
                    null
                );


        return value;
    }
    catch (err) {
        return false;
    }
}
function ValidateRecurringDates() {
    debugger;
    var startDate = document.getElementById("txtDateFrom").value;
    var EndDate = document.getElementById("txtDateEnd").value;
    var ToDate = new Date(EndDate);

    if (new Date(startDate).getTime() > ToDate.getTime()) {
        alert("Start date must be smaller then end date");
        return false;
    }
    return true;
}
function ShowFrequencyBookingDialog(IsRecurring) {
    debugger;
    if (IsRecurring == 1) {
        Dialog_OpenPage('Frequency Booking',
        'Dialogs/BookJobRecurringDates.aspx',
        'content',
        900,
        550,
        true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        var isValid = ValidateRecurringDates();
                        if (isValid) {
                            CalendarBookWork_Save(true);
                        }
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
                    'Content/js/BookJobDatesRecurringEdit.js?V=3.23.5',
                    function () {
                        Load_BookJobDatesRecurring(ObjWorkPlan.Appointment);
                    }
        );
    }
    if (IsRecurring == 0) {
        Dialog_OpenPage('Frequency Booking',
        'Dialogs/BookJobRecurringDates.aspx',
        'content',
        900,
        550,
        true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        var isValid = ValidateRecurringDates();
                        if (isValid) {
                            CalendarBookWork_Save(true);
                        }
                        
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
                    'Content/js/BookJobDatesRecurringEdit.js?V=3.23.5',
                    function () {
                        Load_BookJobDatesRecurringNew();
                    }
        );
    }
    
}

function LoadCalendar() {
    var panel = document.getElementById('pnlCalendarDates');
    panel.innerHTML = '';

    $('#pnlCalendarDates').fullCalendar({
        header: {
            left: 'title',
            center: 'agendaDay,agendaWeek,month',
            right: 'prev,today,next'
        },
        height: 600,
        aspectRatio: 1.4,
        defaultView: 'month',
        events: GetCalendarEvents,
        eventClick: CalendarClickEventHandler,
        dayClick: function (date, allDay, jsEvent, view) {
            var date = new Date(view.start);
            console.log(view);
            date.setMonth((new Date(view.start.format('yyyy-MM-dd'))).getMonth());

            $('#multidates').datepicker('setDate', date);
        },
        editable: true,
        firstHour: 6,
        allDaySlot: false,
        allDayText: 'All Day',
        axisFormat: 'HH:mm',
        titleFormat: {
            week: 'MMMM d[ yyyy]{ &#8212;[ MMM] d yyyy}',
            day: 'dddd, dd MMMM yyyy'
        },
        columnFormat: {
            week: 'ddd dd/MM',
            day: 'dddd, dd MMMM'
        },
        eventMouseover: function (event, jsEvent, view) {
            timer = setTimeout(function () {
                var summary = Ajax_CallService(
                    'Services/Web/WorkProgramming.asmx',
                    'GetWorkSummaryForWorkPlan',
                    {
                        id: event.workPlanId
                    },
                    false);
                var StartTimeDay1Date = toDateFromJson(event.data.StartTimeDay1New);
                var strStartTimeDay1Date = StartTimeDay1Date.format('hh:mm tt');


                var ConnectiveDayEndTimeDate = toDateFromJson(event.data.ConnectiveDayEndTimeNew);
                var strConnectiveDayEndTimeDate = ConnectiveDayEndTimeDate.format('hh:mm tt');


                var LastDayEndTimeNewDate = toDateFromJson(event.data.LastDayEndTimeNew);
                var strLastDayEndTimeNewDate = LastDayEndTimeNewDate.format('hh:mm tt');
                if (event.data.IsRecurring || event.data.IsMultiDates) {

                    tooltip = '<div class="tooltiptopicevent" style="max-width: 220px; width:auto;height:auto;background:#fff;border: solid grey 1px; position:absolute;z-index:10001;padding:10px 10px 10px 10px; line-height: 100%; box-shadow: 10px 10px 5px #888888;">' + '<span style="font-size: 0.9em; font-weight: bold;">' + event.title + '</span><br/><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Total Work Value:</span><br/>' + CurrentCurrency + (summary.Value + summary.VatVal).format() + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Start:</span> ' + strStartTimeDay1Date + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">End:</span> ' + strConnectiveDayEndTimeDate + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Last Day:</span> ' + strConnectiveDayEndTimeDate + '</div>';
                }
                else {
                    tooltip = '<div class="tooltiptopicevent" style="max-width: 220px; width:auto;height:auto;background:#fff;border: solid grey 1px; position:absolute;z-index:10001;padding:10px 10px 10px 10px; line-height: 100%; box-shadow: 10px 10px 5px #888888;">' + '<span style="font-size: 0.9em; font-weight: bold;">' + event.title + '</span><br/><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Total Work Value:</span><br/>' + CurrentCurrency  + (summary.Value + summary.VatVal).format() + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Start:</span><br/>' + event.start.format("dd-MM-yyyy hh:mm tt") + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">End:</span><br/>' + event.end.format("dd-MM-yyyy hh:mm tt") + '</div>';
                }
                $("body").append(tooltip);
                $(this).css('z-index', 10000);
                //Set tooltip to position of mouse
                $('.tooltiptopicevent').css('top', jsEvent.pageY + 10);
                $('.tooltiptopicevent').css('left', jsEvent.pageX + 20);
                $('.tooltiptopicevent').hide().fadeIn('slow');
            }, 800);

            $(this).mousemove(function (e) {
                $('.tooltiptopicevent').fadeOut();
            });
        },
        eventMouseout: function (event, jsEvent, view) {
            clearTimeout(timer);
            $(this).css('z-index', 8);
            $('.tooltiptopicevent').fadeOut('slow', removeToolTip);
            function removeToolTip() { $('.tooltiptopicevent').remove(); }
        },
        timeFormat: '',
        slotMinutes: '60'
    });
}

function GetCalendarEvents(startDate, endDate, CallbackFunction) {
    try {
        debugger;
        var EventsList = new Array();
        var Events;
        Events = Ajax_CallService(
                'Services/web/Calendar.asmx',
                'GetWorkBookings',
                {
                    Start: startDate,
                    End: endDate,
                },
                false);

        var today = new Date();

        
        Events.forEach(function (ev) {
                    var StartEnd = new Date(parseInt(ev.StartDateTime.substr(6)));
                    StartEnd = addMinutes(StartEnd, 30);
                    var EndEnd = new Date(parseInt(ev.EndDateTime.substr(6)));
                    EndEnd = addMinutes(EndEnd, 30);
                    console.log(ev);
                    var StartTimeDay1Date = toDateFromJson(ev.StartTimeDay1New);
                    var ConnectiveDayEndTimeDate = toDateFromJson(ev.ConnectiveDayEndTimeNew);
                    var LastDayEndTimeNewDate = toDateFromJson(ev.LastDayEndTimeNew);
                    
                    var total = ev.Color.R + ev.Color.G + ev.Color.B;
                    var textCol = total > (255 * 3) / 2 ? 'black' : 'white';
                    if (!ev.IsRecurring) {   // Regular Multiple Date Book
                        EventsList.push({
                            id: ev.Id,
                            workPlanId: ev.WorkPlanId,
                            data: ev,
                            title: ev.CalendarString,
                            allDay: false,
                            start: new Date(parseInt(ev.StartDateTime.substr(6))),
                            end: new Date(parseInt(ev.EndDateTime.substr(6))),
                            className: 'cal-event-work clickable',
                            editable: true,
                            type: 2,
                            objectId: ev.JobSiteId,
                            backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                            textColor: textCol

                        });
                    }
                    else {
                        if (ev.EveryDay && ev.RepeatTypeId == 1 && (ev.EveryFor == undefined || ev.EveryFor == 0 || ev.EveryFor == -1 || ev.EveryFor == null)) {   //Daily EveryDay
                            EventsList.push({
                                id: ev.Id,
                                workPlanId: ev.WorkPlanId,
                                data: ev,
                                title: ev.CalendarString,
                                allDay: false,
                                start: new Date(parseInt(ev.StartDateTime.substr(6))),
                                end: new Date(parseInt(ev.EndDateTime.substr(6))),
                                className: 'cal-event-work clickable',
                                editable: true,
                                type: 2,
                                objectId: ev.JobSiteId,
                                backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                textColor: textCol

                            });
                        }
                        else if (!ev.EveryDay && ev.RepeatTypeId == 1) {   //Daily With Skip
                            var DateList = [];
                            var DateDiff = daysBetween(StartEnd, EndEnd);
                            var StartDt = null;
                            var EndDt = null
                            var DateEndCount = parseInt(DateDiff / 2);
                            for (var i = 0; i <= DateEndCount; i++) {
                                if (i == 0) {
                                    StartDt = StartEnd;
                                    StartDt = new Date(StartDt);
                                    var year = StartDt.getFullYear();
                                    var month = StartDt.getMonth();
                                    var day = StartDt.getDate();
                                    var hours = StartTimeDay1Date.getHours();
                                    var min = StartTimeDay1Date.getMinutes();
                                    var sec = StartTimeDay1Date.getSeconds();
                                    StartDt = new Date(year, month, day, hours, min, sec);
                                    EndDt = new Date(StartDt);
                                    EndDt = new Date(EndDt.getFullYear(), EndDt.getMonth(), EndDt.getDate(), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                }
                                else {
                                    StartDt = new Date(StartDt.setDate(StartDt.getDate() + parseInt(ev.EveryFor) + 1));
                                    StartDt = new Date(StartDt.getFullYear(), StartDt.getMonth(), StartDt.getDate(), StartTimeDay1Date.getHours(), StartTimeDay1Date.getMinutes(), StartTimeDay1Date.getSeconds(), 0);
                                    if (i == DateEndCount) {
                                        EndDt = new Date(StartDt);
                                        EndDt = new Date(EndDt.getFullYear(), EndDt.getMonth(), EndDt.getDate(), LastDayEndTimeNewDate.getHours(), LastDayEndTimeNewDate.getMinutes(), LastDayEndTimeNewDate.getSeconds(), 0);
                                    }
                                    else {
                                        EndDt = new Date(StartDt);
                                        EndDt = new Date(EndDt.getFullYear(), EndDt.getMonth(), EndDt.getDate(), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                    }
                                   
                                }
                                EventsList.push({
                                    id: ev.Id,
                                    workPlanId: ev.WorkPlanId,
                                    data: ev,
                                    title: ev.CalendarString,
                                    allDay: false,
                                    start: new Date(StartDt),
                                    end: new Date(EndDt),
                                    className: 'cal-event-work clickable',
                                    editable: true,
                                    type: 2,
                                    objectId: ev.JobSiteId,
                                    backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                    textColor: textCol

                                });

                            }
                        }
                        else if ((ev.EveryFor == 0 || ev.EveryFor == -1 || ev.EveryFor == null) && ev.RepeatTypeId == 2) {   //Weekly With No Skip with selected Days

                            var WeekDays = ev.WeekDay.split(',');
                            var StartDt = null;
                            var EndDt = null
                            
                            var StartDayList = [];
                            var EndDayList = [];
                            for (var i = StartEnd; i <= EndEnd;) {
                                var IsWeDay = IsWeekDay(WeekDays, i);
                                if (IsWeDay) {
                                    var StartTime = new Date(i.getFullYear(), i.getMonth(), i.getDate(), StartTimeDay1Date.getHours(), StartTimeDay1Date.getMinutes(), StartTimeDay1Date.getSeconds(), 0);
                                    StartDayList.push(StartTime);
                                    EndTime = new Date(i.getFullYear(), i.getMonth(), i.getDate(), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                    EndDayList.push(EndTime);
                                }
                                i.setTime(i.getTime() + 1000 * 60 * 60 * 24);
                            }
                            var LastDateIndex;
                            if (EndDayList.length > 0 && StartDayList.length > 0) {
                                LastDateIndex = EndDayList.length - 1;
                                LastDateTime = EndDayList[LastDateIndex];
                                if (EndDayList.length > 1) {
                                    EndDayList[LastDateIndex] = new Date(LastDateTime.getFullYear(), LastDateTime.getMonth(), LastDateTime.getDate(), LastDayEndTimeNewDate.getHours(), LastDayEndTimeNewDate.getMinutes(), LastDayEndTimeNewDate.getSeconds(), 0);
                                }
                                $(StartDayList).each(function (i, o) {
                                    EventsList.push({
                                        id: ev.Id,
                                        workPlanId: ev.WorkPlanId,
                                        data: ev,
                                        title: ev.CalendarString,
                                        allDay: false,
                                        start: new Date(o),
                                        end: new Date(EndDayList[i]),
                                        className: 'cal-event-work clickable',
                                        editable: true,
                                        type: 2,
                                        objectId: ev.JobSiteId,
                                        backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                        textColor: textCol

                                    });
                                });
                            }

                        }
                        else if (ev.EveryFor != 0 && ev.EveryFor != null && ev.EveryFor != -1 && ev.RepeatTypeId == 2) {   //Weekly With Skip with selected Days
                            debugger;
                            var WeekDays = ev.WeekDay.split(',');
                            var StartDt = null;
                            var EndDt = null;
                            var StartDayList = [];
                            var EndDayList = [];
                            for (var i = StartEnd; i <= EndEnd;) {
                                var WeekEdnDate = null;
                                var x = new Date(StartEnd);
                                WeekEdnDate = new Date(new Date(x.setDate(x.getDate() + 7 - x.getDay())));
                                for (var j = StartEnd; j < WeekEdnDate;) {
                                    var IsWeDay = IsWeekDay(WeekDays, j);
                                    if (IsWeDay) {
                                        var StartTime = new Date(j.getFullYear(), j.getMonth(), j.getDate(), StartTimeDay1Date.getHours(), StartTimeDay1Date.getMinutes(), StartTimeDay1Date.getSeconds(), 0);
                                        var EndTime = new Date(j.getFullYear(), j.getMonth(), j.getDate(), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                        StartDayList.push(StartTime);
                                        EndDayList.push(EndTime);
                                        
                                    }
                                    j.setTime(j.getTime() + 1000 * 60 * 60 * 24);
                                }
                                i.setTime(i.getTime() + 1000 * 60 * 60 * 24 * (7 * parseInt(ev.EveryFor)));
                            }
                            var LastDateIndex;
                            if (EndDayList.length > 0 && StartDayList.length > 0) {
                                debugger;
                                LastDateIndex = EndDayList.length - 1;
                                LastDateTime = EndDayList[LastDateIndex];
                                if (EndDayList.length > 1) {
                                    EndDayList[LastDateIndex] = new Date(LastDateTime.getFullYear(), LastDateTime.getMonth(), LastDateTime.getDate(), LastDayEndTimeNewDate.getHours(), LastDayEndTimeNewDate.getMinutes(), LastDayEndTimeNewDate.getSeconds(), 0);
                                }
                                $(StartDayList).each(function (i, o) {
                                    EventsList.push({
                                        id: ev.Id,
                                        workPlanId: ev.WorkPlanId,
                                        data: ev,
                                        title: ev.CalendarString,
                                        allDay: false,
                                        start: new Date(o),
                                        end: new Date(EndDayList[i]),
                                        className: 'cal-event-work clickable',
                                        editable: true,
                                        type: 2,
                                        objectId: ev.JobSiteId,
                                        backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                        textColor: textCol

                                    });
                                });
                            }
                        }
                        else if ((ev.EveryFor == 0 || ev.EveryFor == -1 || ev.EveryFor == null) && ev.RepeatTypeId == 3) {   //Monthly NO Skip
                            debugger;
                            var StartDayList = [];
                            var EndDayList = [];
                            for (var i = StartEnd; i <= EndEnd;) {
                                var x = new Date(StartEnd);
                                var MonthEdnDate = new Date(new Date(StartEnd.getFullYear(), StartEnd.getMonth() + 1, 0));
                                for (var j = StartEnd; j <= MonthEdnDate;) {
                                    var StartTime = new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay), StartTimeDay1Date.getHours(), StartTimeDay1Date.getMinutes(), StartTimeDay1Date.getSeconds(), 0);
                                    StartDayList.push(StartTime);
                                    var EndTime = new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                    EndDayList.push(EndTime);
                                   
                                    break;
                                }

                                //i.setTime(i.getTime() + 1000 * 60 * 60 * 24 * (30 * parseInt(ev.EveryFor)));
                                i.setMonth(i.getMonth() + 1);

                            }
                            var LastDateIndex;
                            if (EndDayList.length > 0 && StartDayList.length > 0) {
                                LastDateIndex = EndDayList.length - 1;
                                LastDateTime = EndDayList[LastDateIndex];
                                if (EndDayList.length > 1) {
                                    EndDayList[LastDateIndex] = new Date(LastDateTime.getFullYear(), LastDateTime.getMonth(), LastDateTime.getDate(), LastDayEndTimeNewDate.getHours(), LastDayEndTimeNewDate.getMinutes(), LastDayEndTimeNewDate.getSeconds(), 0);
                                }
                                $(StartDayList).each(function (i, o) {
                                    EventsList.push({
                                        id: ev.Id,
                                        workPlanId: ev.WorkPlanId,
                                        data: ev,
                                        title: ev.CalendarString,
                                        allDay: false,
                                        start: new Date(o),
                                        end: new Date(EndDayList[i]),
                                        className: 'cal-event-work clickable',
                                        editable: true,
                                        type: 2,
                                        objectId: ev.JobSiteId,
                                        backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                        textColor: textCol

                                    });
                                });
                            }
                        }

                        else if (ev.EveryFor != 0 && ev.EveryFor != null && ev.EveryFor != -1 && ev.RepeatTypeId == 3) {   //Monthly with Skip
                            var StartDayList = [];
                            var EndDayList = [];
                            for (var i = StartEnd; i <= EndEnd;) {
                                var x = new Date(StartEnd);
                                var MonthEdnDate = new Date(new Date(StartEnd.getFullYear(), StartEnd.getMonth() + 1, 0));
                                for (var j = StartEnd; j <= MonthEdnDate;) {
                                    var StartTime = new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay), StartTimeDay1Date.getHours(), StartTimeDay1Date.getMinutes(), StartTimeDay1Date.getSeconds(), 0);
                                    StartDayList.push(StartTime);
                                    var EndTime = new Date(StartEnd.getFullYear(), StartEnd.getMonth(), parseInt(ev.OnMonthDay), ConnectiveDayEndTimeDate.getHours(), ConnectiveDayEndTimeDate.getMinutes(), ConnectiveDayEndTimeDate.getSeconds(), 0);
                                    EndDayList.push(EndTime);
                                   
                                    break;
                                }

                                //i.setTime(i.getTime() + 1000 * 60 * 60 * 24 * (30 * parseInt(ev.EveryFor)));
                                i.setMonth(i.getMonth() + 1 + ev.EveryFor);

                            }
                            var LastDateIndex;
                            if (EndDayList.length > 0 && StartDayList.length > 0) {
                                LastDateIndex = EndDayList.length - 1;
                                LastDateTime = EndDayList[LastDateIndex];
                                if (EndDayList.length > 1) {
                                    EndDayList[LastDateIndex] = new Date(LastDateTime.getFullYear(), LastDateTime.getMonth(), LastDateTime.getDate(), LastDayEndTimeNewDate.getHours(), LastDayEndTimeNewDate.getMinutes(), LastDayEndTimeNewDate.getSeconds(), 0);
                                }
                                $(StartDayList).each(function (i, o) {
                                    EventsList.push({
                                        id: ev.Id,
                                        workPlanId: ev.WorkPlanId,
                                        data: ev,
                                        title: ev.CalendarString,
                                        allDay: false,
                                        start: new Date(o),
                                        end: new Date(EndDayList[i]),
                                        className: 'cal-event-work clickable',
                                        editable: true,
                                        type: 2,
                                        objectId: ev.JobSiteId,
                                        backgroundColor: 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')',
                                        textColor: textCol

                                    });
                                });
                            }
                        }
                    }
                });
        CallbackFunction(EventsList);
    } catch (err) {
        debugger;
        DisplayMessage(PnlError, 'Error: Unable to communicate with ArbPro service -' + err.toString());
    }
}
function daysBetween(date1, date2) {
    //Get 1 day in milliseconds
    var one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
}
function addDays(theDate, days) {
    return new Date(theDate.getTime() + days * 24 * 60 * 60 * 1000);
}

//Check isWeekDay
function IsWeekDay(lstWeekDay, currentDate) {

    for (var i = 0; i < lstWeekDay.length; i++) {
        var day = currentDate.getDay();
        if (day == 0) {
            day = 7;
        }
        if (day == parseInt(lstWeekDay[i])) {
            return true;
            break;
        }
    }
    return false;
}
function CalendarClickEventHandler(event, jsEvent, view) {
    try {
        
        if (event.editable) {
            EditAppointment(event.workPlanId);
        } else {
            //The work has been started or the appointment is histoic and so is not editable
            ShowAlertMessage('The appointment cannot be edited at this time. This may be due to the appointment being historic or the work has been started.');
        }
    } catch (err) {
        DisplayMessage(PnlError, 'Error: Unable to handle Calendar event click - ' + err.toString());
    }
}
function PopulateClientInfoPanel(WorkId) {
    debugger;
    try {
        Ajax_CallService(
        'Services/Web/WorkProgramming.asmx',
        "GetJobWorkSummaryForWork",
        {
            id: WorkId
        },
        true,
        DisplaySummaryDetails
    );
    }
    catch (err) {
        DisplayMessage(PnlError, 'ERROR: Unable to conect to Arb Pro service - ' + err.toString());
    }
}

function DisplaySummaryDetails(result) {
    var strHtml = '';
    if (!result) {
        strHtml += '<div class="width-full">';
        strHtml += '<p class="workJobInfoTitle">';
        strHtml += 'Client Information';
        strHtml += '</p></div>';
    }
    else {
        document.getElementById('lblJobNo').textContent = 'Job No: ' + result.JobId;
        document.getElementById('lblQuoteNo').textContent = 'Quote No: ' + result.QuoteNo;
        strHtml += '<div style="width:98%">';
        strHtml += '<div class="workJobPanelDivider">';
        strHtml += '<p class="workJobInfoTitle">';
        strHtml += 'Client Information';
        strHtml += '</p>';
        strHtml += '<div class="workJobField">';
        strHtml += '<label class ="workJobLabel">Client Name</label>';
        strHtml += '<label id="lblInfoCompanyName" class ="workJobInput">';
        strHtml += result.ClientName
        strHtml += '</label>';
        strHtml += '</div>';
        strHtml += '<div class="workJobField">';
        strHtml += '<label class ="workJobLabel">Site Address</label>';
        strHtml += '<label id="lblInfoCompanyName" class ="workJobInput wrapText dont-break-out">';
        strHtml += result.StreetNumber + "," + result.AddressLn1 + "," + result.AddressLn2 + "," + result.Town + "," + result.County + "," + result.PostCode;
        strHtml += '</label>';
        strHtml += '</div>';
        strHtml += '<div class="workJobField">';
        strHtml += '<label class ="workJobLabel">Contact Number</label>';
        strHtml += '<label id="lblInfoCompanyName" class ="workJobInput">';
        strHtml += result.Telephone;
        strHtml += '</label>';
        strHtml += '</div>';
        strHtml += '<div class="workJobField">';
        strHtml += '<label class ="workJobLabel">Mobile Number</label>';
        strHtml += '<label id="lblInfoCompanyName" class ="workJobInput">';
        strHtml += result.Mobile;
        strHtml += '</label>';
        strHtml += '</div>';
        strHtml += '<div class="workJobField">';
        strHtml += '<label class ="workJobLabel">Email</label>';
        strHtml += '<label id="lblInfoCompanyName" class ="workJobInput">';
        strHtml += result.Email;
        strHtml += '</label>';
        strHtml += '</div>';
        strHtml += '</div>';
    }
    document.getElementById('pnlClientInformationForWork').innerHTML = strHtml;

}

function CalendarBookWork_Save(IsRecurring) {
    debugger;
    var retVal = false;
    try {
        //The work plan can only have the job site set if it is for a new plan
        /*if (ObjWorkPlan.Id < 0 || ObjWorkPlan.JobSiteId) {
            ObjWorkPlan.JobSiteId = GetSelectedDdlElementValue('ddlWorkSite');
        }*/
        //The work plan object may have now changed
        //Get the team Id
        ObjWorkPlan.TeamId = GetSelectedDdlElementValue('ddlTeam');;
        ObjWorkPlan.Tools = GetSelectedTools();
        ObjWorkPlan.Personnel = GetSelectedStaff();
        ObjWorkPlan.ScheduledItems = GetSelectedItems();
        var timeReq = document.getElementById('surveyorhours').value;
        var staffReq = document.getElementById('staffrequired').value;
        if (timeReq == "") {
            timeReq = 0;
        }
        if (staffReq == "")
        {
            staffReq = 0;
        }
        ObjWorkPlan.TimeRequired = parseFloat(timeReq);
        ObjWorkPlan.StaffRequired = staffReq;
        ObjWorkPlan.StartDate = new Date();
        ObjApp.EndDateTime = new Date();
        ObjApp.ConnectiveDayEndTimeNew = new Date();
        ObjApp.LastDayEndTimeNew = new Date();
        ObjApp.StartDateTime = new Date();
        ObjApp.StartTimeDay1New = new Date();
        ObjApp.SkipDays = 0;
        ObjWorkPlan.Appointment.EndDateTime = new Date();
        ObjWorkPlan.Appointment.ConnectiveDayEndTimeNew = new Date();
        ObjWorkPlan.Appointment.LastDayEndTimeNew = new Date();
        ObjWorkPlan.Appointment.StartDateTime = new Date();
        ObjWorkPlan.Appointment.StartTimeDay1New = new Date();
        if (ObjWorkPlan.Appointment.CalendarWorkDate && ObjWorkPlan.Appointment.CalendarWorkDate.length > 0) {
            var bookdt = [];
            ObjWorkPlan.Appointment.CalendarWorkDate = bookdt;
        }
        ObjApp.Comments = document.getElementById('txtApptComments').value;
        ObjApp.IsRecurring = IsRecurring;
        ObjApp.StartTimeDay1New = document.getElementById('startday1').value;
        ObjApp.ConnectiveDayEndTimeNew = document.getElementById('consectivedays').value;
        ObjApp.LastDayEndTimeNew = document.getElementById('lastday').value;
        if (IsRecurring) {
            ObjApp.RepeatTypeId = document.getElementById('RepeatTypeID').value;
            if (ObjApp.RepeatTypeId == 1)  //daily
            {
                if (document.getElementById('rdEveryDaydaily').checked) {
                    ObjApp.EveryDay = true;
                }
                else if (document.getElementById('rdRepeatForDaily').checked) {
                    ObjApp.EveryFor = document.getElementById('txtRepeatForDaily').value;
                }
            }
            else if (ObjApp.RepeatTypeId == 2)  // weekly
            {
                if (document.getElementById('rdRepeatForWeekly').checked) {
                    ObjApp.EveryFor = document.getElementById('txtRepeatForWeekly').value;
                }
                var SelectWeekdays = $("#weekday").val();
                if (SelectWeekdays !== undefined && SelectWeekdays != "")
                    ObjApp.WeekDay = SelectWeekdays;
            }
            else if (ObjApp.RepeatTypeId == 3) // Mothly
            {
                ObjApp.EveryFor = document.getElementById('txtMonthlyForDaily').value;
                ObjApp.OnMonthDay = $("#onMonthDay").val();
            }
            ObjApp.StartDateTime = document.getElementById('txtDateFrom').value;
            ObjApp.EndDateTime = document.getElementById('txtDateEnd').value;

            if (ValidateWorkPlan(ObjWorkPlan, ObjApp)) {
                retVal = UpdateWorkSchedule(ObjWorkPlan, ObjApp);
                updateQueryStringParam("Save","1");
                location.reload();
            }
            else {
                DisplayMessage(PnlError, 'Booking Warning: The work plan cannot be saved as more information is required. Please use the Job Booking tab to select a Team, Staff members and Tools,also ensure you have selected a start and end dates and times. Once this date has been entered use the save button top left to save and book this job.', 'red');
            }


        }
        else {
            var bookdt = [];
            ObjApp.StartDateTime = new Date();
            ObjApp.EndDateTime = new Date();
            ObjApp.RepeatTypeId = -1;
            ObjApp.EveryFor = -1;
            ObjApp.EveryDay = false;
            ObjApp.OnMonthDay = -1;
            ObjApp.WeekDay = null;
            var SelectedDate = $('#multidates').val();
            if (SelectedDate !== undefined && SelectedDate != null && SelectedDate != "") {
                var arrDate = SelectedDate.split(",");
                for (var i = 0; i < arrDate.length; i++) {
                    var BD = {
                        BookDate: arrDate[i]
                    };
                    bookdt.push(BD);
                }
                ObjApp.CalendarWorkDate = bookdt;

                if (ValidateWorkPlan(ObjWorkPlan, ObjApp) && ObjApp.CalendarWorkDate.length > 0) {
                    retVal = UpdateWorkSchedule(ObjWorkPlan, ObjApp);
                    updateQueryStringParam("Save","1");
                    location.reload();
                }
                else {
                    DisplayMessage(PnlError, 'Booking Warning: The work plan cannot be saved as more information is required. Please use the Job Booking tab to select a Team, Staff members and Tools,also ensure you have selected a start and end dates and times. Once this date has been entered use the save button top left to save and book this job.', 'red');
                   
                }

            }
            else {
                DisplayMessage(PnlError, 'Booking Warning: The work plan cannot be saved as more information is required. Please use the Job Booking tab to select a Team, Staff members and Tools,also ensure you have selected a start and end dates and times. Once this date has been entered use the save button top left to save and book this job.', 'red');
                retVal = false;
            }

        }
        /*/if (ValidateWorkPlan()) {
            //Set the display for the wait timer
            retVal = UpdateWorkSchedule(ObjWorkPlan, ObjApp);
        } else {
            DisplayMessage(PnlError, 'INFORMATION: The work plan cannot be saved. Please ensure that Staff and Tools have been assigned and that you have at least 1 item selected.', 'red');
            retVal = false;
        }*/

    }
    catch (err) {
        DisplayMessage(PnlError, 'ERROR: Unable to save the work schedule - ' + err.toString());
    }
    return retVal;
}

//      Function to update a work schedule
function UpdateWorkSchedule(workPlan, appointment) {
    try {

        var value = Ajax_CallService(
                'Services/Web/WorkProgramming.asmx',
                'SaveWorkPlanForBookJob',
                {
                    workPlan: workPlan,
                    appointment: appointment
                },
                false
               );


        return value;
    }
    catch (err) {
        return false;
    }
}
function GetSelectedDdlElementValue(ddlName) {
    //debugger;
    var ddl = document.getElementById(ddlName);
    var id = ddl.options[ddl.selectedIndex].value;
    return id;
}

//      Function to get the tools selected for the work plan
function GetSelectedTools() {
    var tools = [];
    //  Iterate through all the tools available and check if they have a value
    //debugger;
    ListTools.forEach(function (t) {
        var txtRole = document.getElementById('txtToolNum_' + t.ToolId.toString());
        //  Check if the element has any content, we only want to add items that have a value
        //  Check the element is valid and there are a number of tools required
        if (txtRole && txtRole.value.length > 0 && txtRole.value != '0') {
            //  Check if the value entered is a number
            var isNotANum = isNaN(txtRole.value);
            if (isNotANum) {
                //  The value entered is not a number so default to 1
                t.Number = 1;
            } else {
                t.Number = parseInt(txtRole.value);
            }
            //  Add the current tool to the return list
            tools.push(t);
        }
    });
    return tools;
}

//      Function to get the staff selected for the work plan
function GetSelectedStaff() {
    var staff = [];
    //  Iterate through all the staff available and check if they are selected
    //debugger;
    ListStaff.forEach(function (s) {
        var staffChkBox = document.getElementById('chkTeamStaff_' + s.StaffId.toString());
        var txtRole = document.getElementById('txtTeamTask_' + s.StaffId.toString());
        if (staffChkBox && staffChkBox.checked) {
            //The current staff object has been selected populate the role and push to the return list
            if (txtRole) {
                s.Task = txtRole.value;
            }
            staff.push(s);
        }
    });

    return staff;
}

function ValidateWorkPlan() {
    // DGSAP-841 - updated validation as before it did not detect personnel, tools and scheduled items properly
    return (ObjWorkPlan.JobSiteId > 0 && ObjWorkPlan.TeamId > 0 && ObjWorkPlan.ScheduledItems.length > 0 && ObjWorkPlan.Personnel.length > 0 && ObjWorkPlan.Tools.length > 0);
}

function EditAppointment(workPlanId) {
    Dialog_OpenPage(
                'Booked Work',
                'Dialogs/CalendarBookJob.aspx?',
                'content',
                750,
                600,
                true,
                {
                    'Close': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/CalendarBookJob.js?V=3.23.5',
                function () {
                    CalendarBookWork_WorkPlan(workPlanId, "", true);
                }
                );
}

function LoadWorkItems() {
    debugger;
    var WorkId = GetURLParameter('WorkId');
    ObjWorkSummary = Ajax_CallService(
        'Services/Web/WorkProgramming.asmx',
        "GetJobWorkSummaryForWork",
        {
            id: WorkId
        },
        false);
    var siteId = GetURLParameter('SiteId');
    var Grid = document.getElementById("pnlWorkJobStage_0");
    var strHtml = '<table class="width-full searchGrid height-full">';//<col width="10%" /><col width="15%" /><col width="65%" /><col width="15%" />';
    strHtml += '<tr><th colspan="6">Select Items to be assigned to the Work Plan</th></tr>';
    strHtml += '<tr><th style="vertical-align: top; text-align: center;">No.</th><th style="vertical-align: top; text-align: center;">Item</th>';
    strHtml += '<th style="vertical-align: top; text-align: center;">Description</th><th style="vertical-align: top; text-align: center;">Image</th><th style="vertical-align: top; text-align: center;">Value (' + CurrentCurrency + ')</th><th onclick="SelectAllPendingWork();" style="vertical-align: top; text-align: center;">Book all</br><input type="checkbox" id="chkAllWork"/>';
    strHtml += '</th></tr>';
    var pendingWorkItems = $.grep(ObjWorkSummary.WorkItems, function (e) { return e.JobSiteId == siteId && !e.Booked && !e.Completed && e.Status != 99; });
    itemSort(pendingWorkItems);
    if (pendingWorkItems && pendingWorkItems.length == 0 && ObjWorkPlan.ScheduledItems.length == 0) {
        //There are currently no booked items for the work plan and all work has been booked
        strHtml += '<tr>';
        strHtml += '<td  colspan="6" style="vertical-align: top; text-align: center;">All work has been booked</br>Please delete this Work Plan.</td>';
        strHtml += '</tr>';
    } else {
        //  Display unbooked items
        pendingWorkItems.forEach(function (wi) {
            strHtml += '<tr>';
            strHtml += '<td style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Name + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Description + '</td>';
            strHtml += '<td style="text-align: center;" onclick="event.preventDefault(); OpenImage(2, ' + wi.Id + ');"><a href="#">Image</a></td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Value.format() + '</td>';
            strHtml += '<td style="vertical-align:center text-align: center; cursor: default;"><input id="chkSelectItem_' + wi.Id + '" type="checkbox" style="cursor: pointer;"/></td>';
            strHtml += '</tr>';
        });
        var TotalValue = 0;
        //  Display items to the selected work plan
        ObjWorkPlan.ScheduledItems.forEach(function (wi) {
            strHtml += '<tr>';
            strHtml += '<td style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Name + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Description + '</td>';
            strHtml += '<td style="text-align: center;" onclick="event.preventDefault(); OpenImage(2, ' + wi.Id + ');"><a href="#">Image</a></td>';
            TotalValue += wi.Value;
            strHtml += '<td style="vertical-align: top;">' + wi.Value.format() + '</td>';
            strHtml += '<td style="vertical-align:center text-align: center; cursor: default;"><input id="chkSelectItem_' + wi.Id + '" type="checkbox" style="cursor: pointer;" checked/></td>';
            strHtml += '</tr>';
        });
        document.getElementById('lblTotalValue').textContent = CurrentCurrency + TotalValue;
    }
    strHtml += '</table>';
    Grid.innerHTML = strHtml;

}

function SelectAllPendingWork() {
    var selected = document.getElementById('chkAllWork').checked;
    ObjWorkSummary.WorkItems.forEach(function (itm) {
        var chkItm = document.getElementById('chkSelectItem_' + itm.Id);
        if (chkItm) {
            chkItm.checked = selected;
        }
    });
    // check against the work plan array as well - as the items may be for a different quote
    ObjWorkPlan.ScheduledItems.forEach(function (itm) {
        var chkItm = document.getElementById('chkSelectItem_' + itm.Id);
        if (chkItm) {
            chkItm.checked = selected;
        }
    });
}

//      Function to get the items selected to be assigned to the workplan
function GetSelectedItems() {
    var items = [];
    //  Iterate through all the items available and check if they are selected
    ObjWorkSummary.WorkItems.forEach(function (wi) {
        var itmChkBox = document.getElementById('chkSelectItem_' + wi.Id.toString());
        if (itmChkBox && itmChkBox.checked) {
            //The item is selected so push it to the return array
            items.push(wi);
        }
    });
    //  The array mey be empty if the workplan being edited is for a different quote
    //  Check the length of the return array and if zero check against the work plan items
    if (items.length == 0) {
        ObjWorkPlan.ScheduledItems.forEach(function (swi) {
            var itmChkBox = document.getElementById('chkSelectItem_' + swi.Id.toString());
            if (itmChkBox && itmChkBox.checked) {
                //The item is selected so push it to the return array
                items.push(swi);
            }
        });
    }
    return items;
}

function GotoQuote() {
    var JobId = GetURLParameter('JobId');
    var WorkId = GetURLParameter('WorkId');
    window.location.href = "Job.aspx?JobID=" + JobId + "&WorkID=" + WorkId + "&curTab=3";
}

function CancelWorkBook() {
    window.location.href = "Calendar.aspx";
}

function PrintWorkPack() {
    var WorkPlanId = GetURLParameter('WorkPlanId');
    ShowWorkPackDialog(WorkPlanId);
}

//      Function to display the work pack document dialog
function ShowWorkPackDialog(workPlanId) {
    //  This function now shows the dialog for the work packs based on the work plan, not the work record
    Dialog_OpenPage(
      'Work Packs',
      'Dialogs/PDFGeneration.aspx',
      'content',
      900,
      600,
      true,
       {
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
      'Content/js/WorkPackGeneration.js?V=3.23.5',
      function () {
          PDFGeneration_LoadWorkPlan(workPlanId);      //TODO: This has been modified from the Quote Id to the work plan Id - this must be altered within the dialog
          $("#warningMessage").visible = false;
          if (ObjWorkSummary.BookedWork.length == 0) {
              $("#warningMessage").visible = true;
              $("#warningMessage").html("<strong>Work packs for schedules can only be generated when work has been booked.</strong>");
              $(btnGenerateWorkPack).prop("disabled", true);
          }
      });
}

function OpenTreeProtection() {
    var quoteId = GetURLParameter('QuoteId');
    Dialog_OpenPage(
          'Tree Protection',
          'Dialogs/TreeProtection.aspx?QuoteID=' + quoteId,
          'content',
          747,
          580,
          true,
          {
              'Print schedule': {
                  text: 'Print schedule',
                  class: 'ui-dialog-button',
                  click: function () {
                      $(this).dialog('close');
                      Tpo_Save();
                      PrintWorkSchedule();
                  }
              },
              'Save': {
                  text: 'Save',
                  class: 'ui-dialog-button',
                  click: function () {
                      $(this).dialog('close');
                      Tpo_Save();
                      updateQueryStringParam("Save", "1");
                      location.reload();
                  }
              },
              'Close': {
                  text: 'Close',
                  class: 'ui-dialog-button',
                  click: function () {
                      $(this).dialog('close');
                  }
              }
          },
         'Content/js/TreeProtection.js?V=3.23.5',
         function () {
             debugger;
             var JobId = GetURLParameter('JobId');
             var quoteId = GetURLParameter('QuoteId');
             var ObjJob = Ajax_CallService(
                  'Services/Web/WorkFlow.asmx',
                  'GetJob',
                  {
                      jobId: JobId
                  },
                  false
              );
             Tpo_Load(quoteId, ObjJob.WorkSites);
         }
         );

}

function OpenPermitsNecessery() {
         Dialog_OpenPage(
            'Permits Necessary',
            'Dialogs/PermitsNecessary.aspx',
            'content',
            600,
            400,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                        PermitsNecessary_Save();
                        updateQueryStringParam("Save", "1");
                        location.reload();
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
           'Content/js/PermitsNecessary.js?V=3.23.5',
           function () {
               // debugger;
               var quoteId = GetURLParameter('QuoteId');
               Permits_Load(quoteId);
           }
           );
   
}

function DisplayLogisticsTab() {
    debugger;
    var LogisticPanel = document.getElementById("pnlLogistics");

    var strHtml = '<div class="width-full>';
    strHtml += '<div id="pnlAppointmentDates" class="width-full float-left">';
    strHtml += '<div class="dlgBookWorkField">';
    strHtml += '<label class="dlgBookWorkLabel" style="font-weight:bold">Surveyor Estimated Hours</label>';
    strHtml += '<input id="surveyorhours" type="number" class="dlgBookWorkInput" value="' + ObjWorkPlan.TimeRequired + '" name="surveyourhours" style="width:80px;">';
    strHtml += '</div>';
    strHtml += '<div class="dlgBookWorkField">';
    strHtml += '<label class="dlgBookWorkLabel" style="font-weight:bold">Staff Number Required</label>';
    strHtml += '<input id="staffrequired" type="number" class="dlgBookWorkInput" value="' + ObjWorkPlan.StaffRequired + '" name="staffrequired" style="width:80px;">';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '</div>';
    LogisticPanel.innerHTML = strHtml;
    LoadQuoteNotes();
   
}


function LoadQuoteNotes() {

    var QuoteId = GetURLParameter('QuoteId');
    var NoteList = Ajax_CallService(
          'Services/Web/WorkFlow.asmx',
          'GetNotesList',
          {
              parentId: QuoteId,
              type: 0
          },
         false
      );
    var wfStage = 0;
    var NotesPanel = document.getElementById("pnlNotes");
    var strHtml = '<div style="font-weight:bold;margin-left:20px;">Notes</div>';
    strHtml += '<div class="width-full float-right" style="margin-right:23px;"><button type="button" onclick="ShowQuoteNotesDialog(-1,' + wfStage + ',' + QuoteId + ');" class="button-thin float-right padded">Add Note</button></div>';
    strHtml += '<div style="width:96%;margin-left:20px;">';
    strHtml += '<table class="searchGrid width-full">';
    if (NoteList.length == 0) {
        strHtml += '<tr><td>No notes to display</td></tr>';
    } else {
        strHtml += '<tr class="white-light"><th>Entered By</th><th>Details</th><th>Created</th></tr>'
        NoteList.forEach(function (c) {
            strHtml += '<tr onclick="ShowQuoteNotesDialog(' + c.Id + ',' + wfStage + ',' + QuoteId +');">';
            strHtml += '<td style="padding-left: 10px;">' + c.EnteredBy + '</td><td  style="padding-left: 10px;">' + c.Text + '</td><td style="padding-left: 10px;">' + toDateStringFromJson(c.EnteredDate, '', 'dd/mm/yyyy') + '</td>';
            strHtml += '</tr>';
        });
    }
    strHtml += '</table>';
    strHtml += '</div>';
    NotesPanel.innerHTML = strHtml;
}

//      function to display the add/edit notes dialog
//          NOTE: The callback function must not take any parameters
function ShowQuoteNotesDialog(notesId, wfStage, recId) {
    try {
        //TODO: Ensure Note wfStage flag consistancy
        /*      NOTE: wfStage - This is the stage for the note
                0   =>  Quote    
                1   =>  Work
                2   =>  Invoice
                3   =>  Credit 
        */
        Dialog_OpenPage('Note',
            'Dialogs/Notes.aspx?',
            'content',
            500,
            500,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        Note_Save();
                        //debugger;
                        LoadQuoteNotes();
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/Notes.js?V=3.23.5',
            function () {
                Notes_Load(notesId, recId, wfStage);
            });
    }
    catch (Err) {
        DisplayMessage('pnlNotesDetail_' + panelIndex.toString(), 'Error: Unable to display notes dialog');
    }
}