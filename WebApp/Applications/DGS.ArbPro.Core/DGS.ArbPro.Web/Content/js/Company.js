﻿
var Complaint_CurTab = 0;
var Documents_CurTab = 0;
var Reports_CurTab = 0;
var Information_CurTab = 0;

var Company_listShown = true;
var CompanyObj;

var EmailSearchStartDate = new Date();
var EmailSearchEndDate = new Date();

function SearchEmailsForDate() {
    try {
        EmailSearchStartDate = ParseLocalDate(document.getElementById('txtSearchStartDate').value, 'en_GB');
        EmailSearchEndDate = ParseLocalDate(document.getElementById('txtSearchEndDate').value, 'en_GB');
        //Check if the dates are valid
        if (EmailSearchStartDate > EmailSearchEndDate) {
            alert("Please select a start date before the end date for the search criteria.");
        } else {
            LoadEmailDetails(EmailSearchStartDate, EmailSearchEndDate);
        }
    }
    catch (err) {
        DisplayMessage('pnlDocumentDetail_0', 'Error: Failed to set dates. Please refresh the page');
    }
}

//Initial function to load the company page objects
function LoadCompanyScreenItems() {
    ExportTypeChanged();
    ExportToChanged();

    EmailSearchStartDate.setDate(EmailSearchStartDate.getDate() - 7);

    LoadCompany();
    LoadTeams();
    LoadStaff();
    LoadContacts();
    LoadComplaints();
    LoadDocuments();
    LoadEmailDetails(EmailSearchStartDate, EmailSearchEndDate);
    //Setup the export date pickers
    //Default dates are one month prior to current date and current date
    var exportToDate = new Date();
    var expTo = formatDateString(exportToDate, 'dd/mm/yy');
    var expFrom = formatDateString(new Date(exportToDate.getFullYear(), exportToDate.getMonth() - 1, exportToDate.getDay()), 'dd/mm/yy');
    $('#txtExportFrom').datetimepicker({ startDate: expFrom, format: 'd/m/y', timepicker: false });
    $('#txtExportTo').datetimepicker({ startDate: expTo, format: 'd/m/y', timepicker: false });
    document.getElementById('txtExportFrom').value = expFrom;
    document.getElementById('txtExportTo').value = expTo;
    //TODO: Coming soon feature - LoadLetterDetails();
}

function LoadComplaints() {
    LoadOpenComplaints();
    LoadClosedComplaints();
}

//Click event handler to diplay a selected complaint tab
function SwitchComplaintTab() {
    SwitchTab(Complaint_CurTab, 2, 'tabComplaint_', 'pnlComplaintTab_');
}

//Click event handler to diplay a selected information tab
function SwitchInfoTab() {
    SwitchTab(Information_CurTab, 6, 'pnlCompanyInfoTab_', 'pnlCompanyInfoDetail_');
}

//Click event handler to diplay a selected Report or data export tab tab
function SwitchReportsTab() {
    SwitchTab(Reports_CurTab, 4, 'tabReportOption_', 'pnlReportOptionsDetail_');
    //Check if the export tab is being displayed
    if (Reports_CurTab == 2) {
        //Check if the broswer is safari
        var browser = get_browser();
        if (browser.toLowerCase() == 'safari') {
            //Display a compatability message
            DisplayMessage('pnlCompatability', 'Safari Users, please note that the export will be displayed in a browser window. Please right click on the page and select the "Save As" option to download.');
            document.getElementById('pnlCompatability').className = '';
        }
    }
}

//Click event handler to diplay a selected document history tab
function SwitchDocumentsTab() {
    SwitchTab(Documents_CurTab, 2, 'tabDocument_', 'pnlDocumentDetail_');
}

//Function to change the CSS class of a set of specified <a> and <div> tags
function SwitchTab(selectedTab, tabCount, tabPrefix, detailPrefix) {
    for (i = 0; i < tabCount; i++) {
        var tabElement = document.getElementById(tabPrefix + i.toString());
        if (!tabElement) {
            tabElement = document.getElementById('Content_' + tabPrefix + i.toString());
        }
        var detailElement = document.getElementById(detailPrefix + i.toString());
        if (!detailElement) {
            detailElement = document.getElementById('Content_' + detailPrefix + i.toString());
        }
        if (i == selectedTab) {
            tabElement.className = 'companyTab activeTab';
            detailElement.className = '';
        } else {
            tabElement.className = 'companyTab';
            detailElement.className = 'hidden';
        }
    }
}

function SwitchLeftTab() {
    switch (Company_curLeftTab) {
        case 1: //Info
            document.getElementById('tabInfo').className = 'clientTab activeTab';
            document.getElementById('tabStaff').className = 'clientTab';
            document.getElementById('tabContacts').className = 'clientTab';
            document.getElementById('tabDefaults').className = 'clientTab';
            document.getElementById('tabTeams').className = 'clientTab';
            document.getElementById('tabDocuments').className = 'clientTab ';

            document.getElementById('pnlTabInfo').className = 'width-full';
            document.getElementById('pnlTabStaff').className = 'hidden';
            document.getElementById('pnlTabContacts').className = 'hidden';
            document.getElementById('pnlTabDefaults').className = 'hidden';
            document.getElementById('pnlTabTeams').className = 'hidden';
            document.getElementById('pnlTabDocuments').className = 'hidden';
            UpdateCompanyPanel();
            break;
        case 2: //Staff
            document.getElementById('tabInfo').className = 'clientTab';
            document.getElementById('tabStaff').className = 'clientTab activeTab';
            document.getElementById('tabContacts').className = 'clientTab';
            document.getElementById('tabDefaults').className = 'clientTab';
            document.getElementById('tabTeams').className = 'clientTab';
            document.getElementById('tabDocuments').className = 'clientTab ';

            document.getElementById('pnlTabInfo').className = 'hidden';
            document.getElementById('pnlTabStaff').className = 'width-full';
            document.getElementById('pnlTabContacts').className = 'hidden';
            document.getElementById('pnlTabDefaults').className = 'hidden';
            document.getElementById('pnlTabTeams').className = 'hidden';
            document.getElementById('pnlTabDocuments').className = 'hidden';
            DisplayStaffList();
            // LoadButtons();
            break;
        case 3: //Contacts
            document.getElementById('tabInfo').className = 'clientTab';
            document.getElementById('tabStaff').className = 'clientTab';
            document.getElementById('tabContacts').className = 'clientTab activeTab';
            document.getElementById('tabDefaults').className = 'clientTab';
            document.getElementById('tabTeams').className = 'clientTab';
            document.getElementById('tabDocuments').className = 'clientTab ';

            document.getElementById('pnlTabInfo').className = 'hidden';
            document.getElementById('pnlTabStaff').className = 'hidden';
            document.getElementById('pnlTabContacts').className = 'width-full';
            document.getElementById('pnlTabDefaults').className = 'hidden';
            document.getElementById('pnlTabTeams').className = 'hidden';
            document.getElementById('pnlTabDocuments').className = 'hidden';
            break;
        case 4://Defaults
            document.getElementById('tabInfo').className = 'clientTab';
            document.getElementById('tabStaff').className = 'clientTab';
            document.getElementById('tabContacts').className = 'clientTab';
            document.getElementById('tabDefaults').className = 'clientTab activeTab';
            document.getElementById('tabTeams').className = 'clientTab ';
            document.getElementById('tabDocuments').className = 'clientTab';

            document.getElementById('pnlTabInfo').className = 'hidden';
            document.getElementById('pnlTabStaff').className = 'hidden';
            document.getElementById('pnlTabContacts').className = 'hidden';
            document.getElementById('pnlTabDefaults').className = 'width-full';
            document.getElementById('pnlTabTeams').className = 'hidden';
            document.getElementById('pnlTabDocuments').className = 'hidden';
            break;
        case 5://Teams
            document.getElementById('tabInfo').className = 'clientTab';
            document.getElementById('tabStaff').className = 'clientTab';
            document.getElementById('tabContacts').className = 'clientTab';
            document.getElementById('tabDefaults').className = 'clientTab';
            document.getElementById('tabTeams').className = 'clientTab activeTab';
            document.getElementById('tabDocuments').className = 'clientTab';

            document.getElementById('pnlTabInfo').className = 'hidden';
            document.getElementById('pnlTabStaff').className = 'hidden';
            document.getElementById('pnlTabContacts').className = 'hidden';
            document.getElementById('pnlTabDefaults').className = 'hidden';
            document.getElementById('pnlTabTeams').className = 'width-full';
            document.getElementById('pnlTabDocuments').className = 'hidden';
            PopulateTeams();
            break;
        case 6: //Documents
            document.getElementById('tabInfo').className = 'clientTab';
            document.getElementById('tabStaff').className = 'clientTab';
            document.getElementById('tabContacts').className = 'clientTab';
            document.getElementById('tabDefaults').className = 'clientTab';
            document.getElementById('tabTeams').className = 'clientTab';
            document.getElementById('tabDocuments').className = 'clientTab activeTab';

            document.getElementById('pnlTabInfo').className = 'hidden';
            document.getElementById('pnlTabStaff').className = 'hidden';
            document.getElementById('pnlTabContacts').className = 'hidden';
            document.getElementById('pnlTabDefaults').className = 'hidden';
            document.getElementById('pnlTabTeams').className = 'hidden';
            document.getElementById('pnlTabDocuments').className = 'width-full';
            DisplayDocuments();
    }

}

function SwitchRightTab() {
    switch (Company_curRightTab) {
        case 1://Reporting
            document.getElementById('tabReporting').className = 'clientTab activeTab';
            document.getElementById('tabEmailOptions').className = 'clientTab';
            document.getElementById('tabSageExport').className = 'clientTab';
            document.getElementById('tabDataExport').className = 'clientTab';

            document.getElementById('pnlReporting').className = 'width-full';
            document.getElementById('pnlEmailOptions').className = 'hidden';
            document.getElementById('pnlSageExport').className = 'hidden';
            document.getElementById('pnlDataExport').className = 'hidden';
            break;
        case 2://Email
            document.getElementById('tabReporting').className = 'clientTab';
            document.getElementById('tabEmailOptions').className = 'clientTab activeTab';
            document.getElementById('tabSageExport').className = 'clientTab';
            document.getElementById('tabDataExport').className = 'clientTab';

            document.getElementById('pnlReporting').className = 'hidden';
            document.getElementById('pnlEmailOptions').className = 'width-full';
            document.getElementById('pnlSageExport').className = 'hidden';
            document.getElementById('pnlDataExport').className = 'hidden';
            break;
        case 3://Sage
            document.getElementById('tabReporting').className = 'clientTab';
            document.getElementById('tabEmailOptions').className = 'clientTab';
            document.getElementById('tabSageExport').className = 'clientTab activeTab';
            document.getElementById('tabDataExport').className = 'clientTab';

            document.getElementById('pnlReporting').className = 'hidden';
            document.getElementById('pnlEmailOptions').className = 'hidden';
            document.getElementById('pnlSageExport').className = 'width-full';
            document.getElementById('pnlDataExport').className = 'hidden';
            PopulateSageValues();
            break;
        case 4://Data
            document.getElementById('tabReporting').className = 'clientTab';
            document.getElementById('tabEmailOptions').className = 'clientTab';
            document.getElementById('tabSageExport').className = 'clientTab';
            document.getElementById('tabDataExport').className = 'clientTab activeTab';

            document.getElementById('pnlReporting').className = 'hidden';
            document.getElementById('pnlEmailOptions').className = 'hidden';
            document.getElementById('pnlSageExport').className = 'hidden';
            document.getElementById('pnlDataExport').className = 'width-full';
            break;
    }
}

function LoadCompany() {
    try {
        Ajax_CallService(
        'Services/Web/Company.asmx',
        'GetCompany',
        {},
        true,
        DisplayCompanyInfo
       );
    }
    catch (err) {
        DisplayMessage('pnlCompanyInfoDetail_0', 'Error: Unable to comunicate with the ArbPro service');
        DisplayMessage('pnlCompanyInfoDetail_4', 'Error: Unable to comunicate with the ArbPro service');
        DisplayMessage('pnlReportOptionsDetail_0', 'Error: Unable to comunicate with the ArbPro service');
        DisplayMessage('pnlReportOptionsDetail_1', 'Error: Unable to comunicate with the ArbPro service');
        DisplayMessage('pnlReportOptionsDetail_2', 'Error: Unable to comunicate with the ArbPro service');
        DisplayMessage('pnlReportOptionsDetail_3', 'Error: Unable to comunicate with the ArbPro service');
    }
}

function LoadDefaultsPanel() {
    try {
        Ajax_CallService(
        'Services/Web/Company.asmx',
        'GetDocumentReferences',
        {},
        true,
        PopulateDefaultsPanel
    );
    }
    catch (err) {
        DisplayMessage('pnlName', 'Error: Unable to connect to the ArbPro service.');
    }
}

//Async ajax call to access team details from the service
function LoadTeams() {
    try {
        Ajax_CallService(
        'Services/Web/WorkProgramming.asmx',
        'GetAllTeam',
        {},
        true,
        DisplayTeamsList
       );
    }
    catch (err) {
        DisplayMessage('pnlCompanyInfoDetail_1', 'Error: Unable to comunicate with the ArbPro service');
    }

}

//Async ajax call to access staff details from the service
function LoadStaff() {
    try {
        Ajax_CallService(
        'Services/Web/Company.asmx',
       'ListAllStaff',
        {},
        true,
        DisplayStaffList
       );
    }
    catch (err) {
        DisplayMessage('pnlCompanyInfoDetail_2', 'Error: Unable to comunicate with the ArbPro service');
    }

}

function LoadDocuments() {
    try {
        Ajax_CallService(
            'Services/Web/Company.asmx',
            'ListAllDocuments',
            {},
            true,
            DisplayDocuments
            );
    }
    catch (err) {
        DisplayMessage('pnlCompanyInfoDetail_5', 'Error: Unable to comunicate with the ArbPro service');
    }
}



//Async ajax call to access company contact details from the service
function LoadContacts() {
    try {
        Ajax_CallService(
        'Services/Web/Company.asmx',
        'GetCompanyContacts',
        {

        },
        true,
        DisplayContactInfo
       );
    }
    catch (err) {
        DisplayMessage('pnlCompanyInfoDetail_3', 'Error: Unable to comunicate with the ArbPro service');
    }

}


//Async ajax call to access complaint details from the service
function LoadOpenComplaints() {
    try {
        Ajax_CallService(
        'Services/Web/Company.asmx',
        'SearchComplaintsForStatus',
        { closed: false },
        true,
        DisplayOpenComplaints
       );
    }
    catch (err) {
        DisplayMessage('pnlCompanyInfoDetail_0', 'Error: Unable to comunicate with the ArbPro service');
    }
}

//Async ajax call to access complaint details from the service
function LoadClosedComplaints() {
    try {
        Ajax_CallService(
        'Services/Web/Company.asmx',
        'SearchComplaintsForStatus',
        { closed: true },
        true,
        DisplayClosedComplaints
       );
    }
    catch (err) {
        DisplayMessage('pnlCompanyInfoDetail_0', 'Error: Unable to comunicate with the ArbPro service');
    }
}

//Async ajax call to access email details from the service
function LoadEmailDetails(startDate, endDate) {
    try {
        Ajax_CallService(
        'Services/Web/Company.asmx',
        'GetEmailHistory',
        {
            startDate: startDate,
            endDate: endDate
        },
        true,
        PopulateEmailPanel
       );
    }
    catch (err) {
        DisplayMessage('pnlDocumentDetail_0', 'Error: Unable to comunicate with the ArbPro service');
    }

}

//Async ajax call to access letter details from the service
function LoadLetterDetails() {
    try {
        Ajax_CallService(
        'Services/Web/Company.asmx',
        'GetCompany',
        {},
        true,
        DisplayCompanyInfo
       );
    }
    catch (err) {
        DisplayMessage('pnlCompanyInfoDetail_0', 'Error: Unable to comunicate with the ArbPro service');
    }

}

//Async callback to display the company details
function DisplayCompanyInfo(result) {
    try {
        CompanyObj = result;
        PopulateInfoPanel();
        LoadDefaultsPanel();
    }
    catch (err) {
        var msg = 'Error: Unable to display company details.';
        DisplayMessage('pnlCompanyInfoDetail_0', msg);
        DisplayMessage('pnlCompanyInfoDetail_4', msg);
        DisplayMessage('pnlReportOptionsDetail_0', msg);
        DisplayMessage('pnlReportOptionsDetail_1', msg);
        DisplayMessage('pnlReportOptionsDetail_2', msg);
        DisplayMessage('pnlReportOptionsDetail_3', msg);
    }
}


function PopulateEmailPanel(results) {

    var panel = document.getElementById('pnlDocumentDetail_0');
    try {
        var strHtml = '';
        //Create the date controls
        strHtml += '<div class="float-left width-full">';
        strHtml += '<div class="companyField float-left">';
        strHtml += '<label class ="companyLabel padded">Search Start Date</label>';
        strHtml += '<input id="txtSearchStartDate" class="companyInput padded" type="text" placeholder="The search start date" name="txtSearchStartDate" autocomplete="on" value="' + formatDateString(EmailSearchStartDate, 'en_GB') + '">';
        //strHtml += '</div>';
        //strHtml += '<div class="companyField  float-left">';
        strHtml += '<label class ="companyLabel padded">Search End Date</label>';
        strHtml += '<input id="txtSearchEndDate" class="companyInput padded" type="text" placeholder="The search end date" name="txtSearchEndDate" autocomplete="on" value="' + formatDateString(EmailSearchEndDate, 'en_GB') + '">';
        strHtml += '<button type="button" onclick="SearchEmailsForDate();" class="button-thin float-right padded">Search</button>';
        strHtml += '</div>';
        strHtml += '</div>';
        //strHtml += '<div class="width-full float-left"><button type="button" onclick="SearchEmailsForDate();" class="button-thin float-right padded">Search</button></div>';
        //Create the results table
        strHtml += '<div class="float-left width-full">';
        strHtml += '<table class="searchGrid width-full white">';
        if (!results || results.length < 1) {
            strHtml += '<tr><td class="center width-full">No entries to display</td></tr>';
        } else {
            strHtml += '<tr class="white-light"><th>Sent Date</th><th>Client Name</th><th>Type</th><th>Reference</th><th>Recipient</th><th>Value</th><th>Status</th></tr>'; // removed due to "Repuires private IP": <th>Message</th>
            results.forEach(function (c) {
                strHtml += '<tr onclick="ShowRecordForEmail(' + c.Id + ',' + c.ParentId + ',' + c.DocumentType + ');" >';
                strHtml += c.SideBarDetail;
                strHtml += '</tr>';
            });
        }
        strHtml += '</table></div>';
        panel.innerHTML = strHtml;

        $('#txtSearchStartDate').datetimepicker({ startDate: formatDateString(EmailSearchStartDate), format: 'd/m/Y', timepicker: false });
        $('#txtSearchEndDate').datetimepicker({ startDate: formatDateString(EmailSearchEndDate), format: 'd/m/Y', timepicker: false });
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display email history');
    }
}

function PopulateInfoPanel() {
    var strHtml = '';
    strHtml += '<div class="float-left width-full">';
    strHtml += '<div class="cmpPanelDevider">';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Name</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.Name;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Street Number</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.StreetNumber;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Address Line 1</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.Address1;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Address Line 2</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.Address2;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Town</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.Town;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">County</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.County;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">PostCode</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.PostCode;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '<div class="cmpPanelDevider">';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Web Site</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.WebSite;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Email Address</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.Email;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Accounts Email</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.AccountsEmail;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Telephone</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.Telephone;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Mobile</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.Mobile;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Directors Name</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.DirectorsName;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Directors Qualifications</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.DirectorsQualifications;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '<div class="width-full float-left">';
    strHtml += '<button type="button" onclick="EditCompanyInfo();" class="button-basic float-right padded">Edit</button>';
    strHtml += '</div>';
    //CompanyObj = result;
    document.getElementById('pnlCompanyInfoDetail_0').innerHTML = strHtml;
}

function PopulateDefaultsPanel(results) {
    var strHtml = '';

    //Set the doc ref label values
    var qRef = '';
    var iRef = '';
    var crRef = '';

    if (results && results.length == 3) {
        qRef = results[0].AsReferenceString;
        iRef = results[1].AsReferenceString;
        crRef = results[2].AsReferenceString;
    }

    strHtml += '<div class="width-full float-left">';
    strHtml += '<div class="cmpPanelDevider">';
    strHtml += '<div class="width-full center"><label class="companyLabel bold">ArbPro Settings</label></div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Tree Names</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    if (CompanyObj.CommonNames) {
        strHtml += 'Common';
    } else {
        strHtml += 'Latin';
    }
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Next Quote Ref</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += qRef;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Next Invoice Ref</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += iRef;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Next Credit Note Ref</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += crRef;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Default VAT</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.VatRate + '%';
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '<div class="cmpPanelDevider">';
    strHtml += '<div class="width-full center"><label class="companyLabel bold">Sage Nominal Codes</label></div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Sales</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.SageSalesCode;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">Receipt</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.SageReceiptCode;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '<div class="companyField">';
    strHtml += '<label class ="companyLabel">VAT</label>';
    strHtml += '<label id="lblInfoCompanyName" class ="companyInput">';
    strHtml += CompanyObj.SageVatCode;
    strHtml += '</label>';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '<div class="width-full float-left">';
    strHtml += '<div class="width-half float-left padded">';
    strHtml += '<button type="button" onclick="EditCompanyDefaults();" class="button-thin float-right">Edit Company Defaults</button>';
    strHtml += '</div>';
    strHtml += '<div class="width-half float-left padded">';
    strHtml += '<button type="button" onclick="EditSageValues();" class="button-thin float-right">Edit Sage Defaults</button>';
    strHtml += '</div>';
    strHtml += '</div>';

    document.getElementById('pnlCompanyInfoDetail_4').innerHTML = strHtml;
}

//Async callback to display the list of teams
function DisplayTeamsList(results) {
    var teamPanel = document.getElementById('pnlCompanyInfoDetail_1');
    if (!teamPanel) {
        teamPanel = document.getElementById('Content_pnlCompanyInfoDetail_1');
    }
    try {
        if (!results) {
            DisplayMessage(teamPanel.id, 'Error: The service return no Staff data');
        } else {
            var strHtml = '<div class="width-full float-left white-light"><button type="button" onclick="ShowTeamDialog(-1);" class="button-thin float-right padded">Add Team</button></div>';
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid width-full white">';
            if (results.length == 0) {
                strHtml += '<tr class="white-light"><th>There are no teams to display. Please add a team by clicking the "Add Team" button.</th></tr>';
            } else {
                strHtml += '<tr class="white-light"><th>Team Number</th><th>Team Name</th><th>Team Colour</th></tr>';
                results.forEach(function(c) {
                    strHtml += '<tr>';
                    //strHtml += '<tr class="selected" onclick="ResultClicked(' + c.Id + ');">';
                    strHtml += '<td onclick="ShowTeamDialog(' + c.Id + ');" style="padding-left: 10px;">' + c.Number + '</td><td onclick="ShowTeamDialog(' + c.Id + ');" style="padding-left: 10px;">' + c.Name + '</td><td onclick="ShowTeamDialog(' + c.Id + ');" style="padding-left: 10px;" bgcolor="#' + c.RGBHexValue + '"></td>' //
                    //strHtml += '<td onclick="event.preventDefault(); DeleteStaff(' + c.Id + ')" style="padding-left: 10px;"><a href="#">Delete</a></td>';
                    strHtml += '</tr>';
                    //alert(c.Name);
                });
            }
            strHtml += '</table>';
            strHtml += '</div>';// class="width-full"><button type="button" onclick="ShowTeamDialog(-1);" class="button-basic float-right">Add</button></div>';
            teamPanel.innerHTML = strHtml;
        }
    }
    catch (err) {
        DisplayMessage(teamPanel.id, 'Error: Unable to display Team details');
    }
}

//Async callback to display the staff list
function DisplayStaffList(results) {
    var staffPanel = document.getElementById('pnlCompanyInfoDetail_2');
    if (!staffPanel) {
        staffPanel = document.getElementById('pnlCompanyInfoDetail_2');
    }
    try {
        if (!results) {
            DisplayMessage(staffPanel.id, 'Error: The service return no Staff data');
        } else {
            var StaffList = results; //GetStaffList();
            var strHtml = '<div class="width-full float-left white-light"><button type="button" onclick="ShowStaffDialog(-1);" class="button-thin float-right padded">Add Staff Member</button></div>';
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid width-full">';
            if (results.length == 0) {
                strHtml += '<tr class="white-light"><th>There are no staff records to display. Please add staff by clicking the "Add Staff Member" button.</th></tr>';
            } else {
                strHtml += '<tr class="white-light"><th>Staff Name</th><th>Staff Email</th>';
                StaffList.forEach(function(c) {
                    strHtml += '<tr>';
                    //strHtml += '<tr class="selected" onclick="ResultClicked(' + c.Id + ');">';
                    strHtml += '<td onclick="ShowStaffDialog(' + c.Id + ');" style="padding-left: 10px;">' + c.Name + '</td><td onclick="ShowStaffDialog(' + c.Id + ');" style="padding-left: 10px;">' + c.Email + '</td>'
                    //strHtml += '<td onclick="event.preventDefault(); DeleteStaff(' + c.Id + ')" style="padding-left: 10px;"><a href="#">Delete</a></td>';
                    strHtml += '</tr>';
                    //alert(c.Name);
                });
            }
            strHtml += '</table>';
            strHtml += '</div>'; ///class="width-full"><button type="button" onclick="ShowStaffDialog(-1);" class="button-basic float-right">Add</button></div>';
            staffPanel.innerHTML = strHtml;
        }
    }
    catch (err) {
        DisplayMessage(staffPanel.id, 'Error: Unable to display Staff details');
    }
};
function DisplayDocuments(results)
{ 
    var strHtml = '';
    var documentsPanel = document.getElementById('pnlCompanyInfoDetail_5');
    if (!documentsPanel) {
        documentsPanel = document.getElementById('pnlCompanyInfoDetail_5');
    }
    try {
        strHtml += '<div class="width-full float-left white-light"><button type="button" onclick="ShowDocumentsDialog(-1);" class="button-thin float-left padded">Add New Documents</button></div>';
        strHtml += '<div class="width-full float-left">';

        if (!results) {
            DisplayMessage(documentsPanel.id, 'Error: no data return from service');
        }
        else {
            var DocumentList = results;
            strHtml += '<table class="searchGrid width-full">';
            if (results.length == 0) {
                strHtml += '<tr class="white-light">There are no documents records to display. Please add documents by clicking the "Add New Documents" button.</tr>';
            } else {
                strHtml += '<tr class="white-light"><th style="padding-left: 10px;">Documents Name</th><th style="text-align:center;">Default Email Attachment</th><th style="text-align:center;">Available on Mobile</th></tr>';
                DocumentList.forEach(function (c) {
                    console.log(c.DefaultAttachment);
                    strHtml += '<tr>';
                    strHtml += '<td onclick="ShowDocumentsDialogEdit(' + c.Id + ');" style="padding-left: 10px;">' + c.DocumentName + '</td>';
                    if(c.DefaultAttachment)
                    {
                        strHtml += '<td style="width:21%; vertical-align: middle; text-align:center;" ><input type="checkbox" id="chkDefaultAttachment" checked onChange="ToggleDefaultAttachment(' + c.Id + ',this);" id="(' + c.Id + ');"</td>';
                    }
                    else
                    {
                        strHtml += '<td style="width:21%; vertical-align: middle; text-align:center;" ><input type="checkbox" id="chkDefaultAttachment" onChange="ToggleDefaultAttachment(' + c.Id + ',this);" id="(' + c.Id + ');"</td>';
                    }
                   
                    strHtml += '<td style="width:21%; vertical-align: middle; text-align:center;" ><input type="checkbox" id="chkAvailableOnMobile" onChange="ToggleAvailableOnMobile(' + c.Id + ');" id="(' + c.Id + ');"</td>';
                    strHtml += '</tr>'; 
                });
            }
            strHtml += '</table>';
            strHtml += '</div>'; ///class="width-full"><button type="button" onclick="ShowStaffDialog(-1);" class="button-basic float-right">Add</button></div>';
            documentsPanel.innerHTML = strHtml;
        }
    }
    catch (err) {
        DisplayMessage(documentsPanel.id, 'Error: Unable to display document details');
    }

}
function ToggleDefaultAttachment(DocumentId, thisElement) {
    console.log(thisElement.checked);
    var Orgs = Ajax_CallService(
            'Services/web/Company.asmx',
            'ToggleDefaultAttachmentForCompany',
            { DocId: DocumentId, DefaultAttachment: thisElement.checked },
            false
        );
}

function ToggleAvailableOnMobile() {
    return;
}



//Async callback to display the business contacts / associates
function DisplayContactInfo(results) {
    var contactPanel = document.getElementById('pnlCompanyInfoDetail_3');
    //Check if the element is valid
    if (!contactPanel) {
        contactPanel = document.getElementById('Content_pnlCompanyInfoDetail_3');
    }
    try {
        if (!results) {
            DisplayMessage(contactPanel.id, 'Error: no data return from service');
        } else if (results.length == 0) {
            var stringHtml = '<div class="width-full float-left white-light"><button type="button" onclick="ShowContactDialog(-1);" class="button-thin float-right padded">Add Contact</button></div>';
            stringHtml += '<div class="width-full float-left">';
            stringHtml += '<table class="searchGrid width-full">';
            stringHtml += '<tr><td>No contacts to display</td></tr>';
            stringHtml += '</table>';
            stringHtml += '</div>';
            contactPanel.innerHTML = stringHtml;
        } else {
            var html = CreateContactTable(results);
            contactPanel.innerHTML = html;
        }
    }
    catch (err) {
        DisplayMessage(contactPanel.id, 'Error: Unable to display contact details');
    }
}


    //Async callback to display the open complaints list
    function DisplayOpenComplaints(results) {
        var openPanel = document.getElementById('pnlComplaintTab_0');
        //Check if the element is valid
        if (!openPanel) {
            openPanel = document.getElementById('Content_pnlComplaintTab_0');
        }
        try {
            if (!results) {
                DisplayMessage(openPanel.id, 'Error: no data return from service');
            } else if (results.length == 0) {
                DisplayMessage(openPanel.id, 'There are no open complaints to display');
            } else {
                var html = CreateComplaintTable(results);
                openPanel.innerHTML = html;
            }
        }
        catch (err) {
            DisplayMessage(openPanel.id, 'Error: Unable to display open complaint details');
        }
    }

    //Async callback to display the closed complaints list
    function DisplayClosedComplaints(results) {
        var closedPanel = document.getElementById('pnlComplaintTab_1');
        //Check if the element is valid
        if (!closedPanel) {
            closedPanel = document.getElementById('Content_pnlComplaintTab_1');
        }
        try {
            if (!results) {
                DisplayMessage(closedPanel.id, 'Error: no data return from service');
            } else if (results.length == 0) {
                DisplayMessage(closedPanel.id, 'There are no closed complaints to display');
            } else {
                var html = CreateComplaintTable(results);
                closedPanel.innerHTML = html;
            }
        }
        catch (err) {
            DisplayMessage(closedPanel.id, 'Error: Unable to display closed complaint details');
        }
    }

    function CreateComplaintTable(results) {
        var strHtml = '<table class="searchGrid width-full">'
        strHtml += '<tr style="white-light"><th>Date</th><th>Type</th><th>Complainant</th><th>Detail</th></tr>';
        results.forEach(function (c) {
            strHtml += '<tr onclick="ShowComplaintDialog(' + c.Id + ');" class="padded"><td>';
            strHtml += toFormattedDateStringFromJson(c.DateOfComplaint, '', 'dd/mm/yyyy');
            strHtml += '</td><td>';
            strHtml += c.ComplaintType;
            strHtml += '</td><td>';
            strHtml += c.Complainant;
            strHtml += '</td><td>';
            strHtml += c.ComplaintDetail;
            strHtml += '</td></tr>';
        });
        strHtml += '</table>';
        return strHtml;
    }

    function CreateContactTable(results) {
        var strHtml = '<div class="width-full float-left white-light"><button type="button" onclick="ShowContactDialog(-1);" class="button-thin float-right padded">Add Contact</button></div>';
        strHtml += '<div class="width-full float-left">';
        strHtml += '<table class="searchGrid width-full">'
        strHtml += '<tr style="white-light"><th>Business Name</th><th>Department</th><th>Telephone</th><th>Email</th></tr>';
        results.forEach(function (c) {
            strHtml += '<tr onclick="ShowContactDialog(' + c.Id + ');" class="padded"><td>';
            strHtml += c.BusinessName;
            strHtml += '</td><td>';
            strHtml += c.Department;
            strHtml += '</td><td>';
            strHtml += c.Telephone;
            strHtml += '</td><td>';
            strHtml += c.Email;
            strHtml += '</td></tr>';
        });
        strHtml += '</table></div>';
        return strHtml;
    }

    function DeleteStaff(Id) {
        alert("delete " + Id);
    }
    function DownloadDocumentImage(c) {
        var TestField = c.DocumentImage; 


    }

    function ShowContactDialog(contactId) {
        try {
            Dialog_OpenPage('Company Contact',
                'Dialogs/CompanyContact.aspx?',
                'content',
                500,
                700,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            CompanyContact_Save();
                            LoadContacts();
                            $(this).dialog('close');
                        }
                    },
                    'Close': {
                        text: 'Close',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/CompanyContact.js?V=3.23.5',
                function () {
                    CompanyContact_Load(contactId);
                });
        }
        catch (Err) {
            DisplayMessage('pnlCompanyInfoDetail_3', 'Error: Unable to display Contacts dialog');
        }
    }

    function ShowComplaintDialog(complaintId) {
        try {
            Dialog_OpenPage('Complaint',
                'Dialogs/Complaint.aspx?',
                'content',
                500,
                590,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            Complaint_Save();
                            LoadComplaints();
                            $(this).dialog('close');
                        }
                    },
                    'Close': {
                        text: 'Close',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/Complaint.js?V=3.23.5',
                function () {
                    Complaint_Load(complaintId);
                });
        }
        catch (Err) {
            DisplayMessage('pnlCompanyInfoDetail_2', 'Error: Unable to display staff dialog');
        }
    }

    function ShowStaffDialog(StaffId) {
        try {
            Dialog_OpenPage('Staff',
                'Dialogs/Staff.aspx?',
                'content',
                300,
                640,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (Staff_Save()) {
                                LoadStaff();
                                $(this).dialog('close');
                            }
                        }
                    },
                    'Close': {
                        text: 'Close',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/Staff.js?V=3.23.5',
                function () { staff_Load(StaffId); });
        }
        catch (Err) {
            DisplayMessage('pnlCompanyInfoDetail_2', 'Error: Unable to display staff dialog');
        }
    }

    function ShowTeamDialog(Id) {
        Dialog_OpenPage('Team',
               'Dialogs/Team.aspx?',
               'content',
               800,
               600,
               true,
               {
                   'Save': {
                       text: 'Save',
                       class: 'ui-dialog-button',
                       click: function () {
                           TeamSave();
                           $(this).dialog('close');
                           LoadTeams();
                       }
                   },
                   'Close': {
                       text: 'Close',
                       class: 'ui-dialog-button',
                       click: function () {
                           $(this).dialog('close');
                           LoadTeams();
                       }
                   }
               },
               'Content/js/Team.js?V=3.23.5',
               function () { Team_Load(Id); });
    }
    function ShowDocumentsDialog(docId) {

        Dialog_OpenPage('Documents',
            'Dialogs/CompanyDocumentsAdd.aspx',
            'CompanyDocs',
            500,
            600,
            true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            var suceeded = DocumentsSave();
                            if (suceeded) {
                                LoadDocuments();
                                $(this).dialog('close');
                            }
                            else {
                                alert('Company Documents upload has failed, please try again');
                            }
                        }
                    },
                    'Close': {
                        text: 'Close',
                        class: 'ui-dialog-button',
                        click: function () {
                            LoadDocuments();
                            $(this).dialog('close');
                        }
                    }
                },
                        'Content/js/CompanyDocuments.js?V=3.23.5',
                        function () { LoadDateAndStaff(docId); }
            );
    }

    function ShowDocumentsDialogEdit(docId) {
        Dialog_OpenPage('Documents',
            'Dialogs/CompanyDocumentsEdit.aspx',
            'CompanyDocsEdit',
            500,
            600,
            true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            DocumentsSaveEdit();
                            File_UploadEdit();
                            LoadDocuments();
                            $(this).dialog('close');
                        }
                    },
                    'Open': {
                        text: 'Open',
                        class: 'ui-dialog-button',
                        click: function () {
                            DocumentPreview()
                            $(this).dialog('close');
                        }
                    },
                    'Close': {
                        text: 'Close',
                        class: 'ui-dialog-button',
                        click: function () {
                            LoadDocuments();
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/CompanyDocuments.js?V=3.23.5',
                function () { CompanyDocumentsLoad(docId, $(this)); }
            );
    }

    function EditCompanyInfo() {

        Dialog_OpenPage('Edit',
                'Dialogs/CompanyInfo.aspx',
                'content',
                300,
                500,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            CompanySave();
                            LoadCompany();
                            $(this).dialog('close');
                        }
                    },
                    'Close': {
                        text: 'Close',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/CompanyContactInfo.js?V=3.23.5',
                function () { CompanyInfo_Load(); }
            );
    }

    function RedirectCompany() {
        window.location.href = '/Company.aspx';
    }
    function EditCompanyDefaults() {
        Dialog_OpenPage('Company Defaults',
              'Dialogs/CompanyDefaults.aspx',
              'content',
              450,
              450,
              true,
              {
                  'Save': {
                      text: 'Save',
                      class: 'ui-dialog-button',
                      click: function () {
                          CompanyDefaults_Save();
                          $(this).dialog('close');
                          LoadCompany();
                      }
                  },
                  'Close': {
                      text: 'Close',
                      class: 'ui-dialog-button',
                      click: function () {
                          $(this).dialog('close');
                      }
                  }
              },
              'Content/js/CompanyDefaults.js?V=3.23.5',
              function () { CompanyDefaults_Load(CompanyObj); });
    }

    function EditSageValues() {
        Dialog_OpenPage('Sage Export',
              'Dialogs/SageExport.aspx?',
              'content',
              320,
              200,
              true,
              {
                  'Save': {
                      text: 'Save',
                      class: 'ui-dialog-button',
                      click: function () {
                          SageSave();
                          $(this).dialog('close');
                          LoadCompany();
                      }
                  },
                  'Close': {
                      text: 'Close',
                      class: 'ui-dialog-button',
                      click: function () {
                          $(this).dialog('close');
                      }
                  }
              },
              'Content/js/SageExport.js?V=3.23.5',
              function () { SageLoad(CompanyObj); });
    }

    function ExportData() {
        var ddlExportTo = document.getElementById('ddlExportTo');
        var ExportType = ddlExportTo.options[ddlExportTo.selectedIndex].value;
        var ExportTypeText = ddlExportTo.options[ddlExportTo.selectedIndex].text;

        var ddlExportType = document.getElementById('ddlExportType');
        var ObjectType = ddlExportType.options[ddlExportType.selectedIndex].value;
        var ObjectTypeText = ddlExportType.options[ddlExportType.selectedIndex].text;

        var local = 'en_GB';
        var fromDate =  ParseLocalDate(document.getElementById('txtExportFrom').value,local);
        var toDate = ParseLocalDate(document.getElementById('txtExportTo').value,local);

        var Export = Ajax_CallService(
            'Services/Web/Settings.asmx',
            'SageExport',
            {
                ExportType: ExportType,
                ObjectType: ObjectType,
                FromDate: fromDate,
                ToDate: toDate
            },
            false);
        switch (ExportTypeText) {
            case "Xero":
                var filename = 'XeroExport_' + ObjectTypeText + '.csv';
                break;
            case "Sage 09":
                var filename = 'SageExport_' + ObjectTypeText + '.csv';
                break;
            case "Sage 13":
                var filename = 'SageExport_' + ObjectTypeText + '.csv';
                break;
            case "Quickbooks":
                var filename = 'Quickbooks_' + ObjectTypeText + '.csv';
                break;
            case "Kashflow":
                var filename = 'Kashflow_' + ObjectTypeText + '.csv';
                break;
            case "CSV":
                var filename = ObjectTypeText + '.csv';
                break;

        }
        debugger;
        //<a target="_blank" href="Images/ReportImage.aspx?Id=' + h.Id + '">View</a>
        if (window.navigator.msSaveOrOpenBlob) {
            var blobObject = new Blob([Export]);
            var success = window.navigator.msSaveOrOpenBlob(blobObject, filename);
        } else {
            var a = window.document.createElement('a');
            a.href = window.URL.createObjectURL(new Blob([Export], { type: 'text/csv' }));
            a.download = filename;

            // Append anchor to body.
            document.body.appendChild(a);
            a.click();
            // Remove anchor from body
            document.body.removeChild(a);
        }
    }


    // The image upload methods below use the method in common.js to
    // determine the type of item and allow a preview/edit functionality
    function UploadHeader() {
        OpenImage(5, 1, true);
    }
    function UploadFooter() {
        OpenImage(6, 2, true);
    }
    function UploadEmailFooter() {
        OpenImage(4, 1, true);
    }
    function UploadSignature() {
        OpenImage(7, 1, true);
    }


    function PreviewReport() {
        Dialog_OpenPage(
       'PDF Preview',
       'Dialogs/PDFPreview.aspx',
       'content',
       500,
       730,
       true,
       {
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/PDFPreview.js?V=3.23.5',
       function () { ManualQuoteDocuments_Load(); });
    }

    function ShowRecordForEmail(id,jobId, documentType) {
        //TODO: DGSAP-430 Navigate the user to the required page
        /*
            NOTE:   The Id param is the record id
                    The documentType is an an integer to denote the type of record
                    0   =>  Quote
                    1   =>  Invoice
                    2   =>  Work
    
            Params of 2463 and 1 should navigate to show invoice record 2463
    
        */
        switch (documentType) {
            case 0:
                navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: jobId }, { paramName: 'QuoteID', paramValue: id }, { paramName: 'curTab', paramValue: 2 }]);
                break;
            case 1:
                navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: jobId }, { paramName: 'QuoteID', paramValue: id }, { paramName: 'curTab', paramValue: 4 }]);
                break;
            case 2:
                navigateParams(Page_strAppRoot + 'Job.aspx', [{ paramName: 'JobID', paramValue: jobId }, { paramName: 'QuoteID', paramValue: id }, { paramName: 'curTab', paramValue: 3 }]);
                break;
        }
    }

    //New export specific functions
    function ExportTypeChanged() {
        var ddlExportType = document.getElementById('ddlExportType');
        if (ddlExportType) {
            var txtFrom = document.getElementById('txtExportFrom');
            var lblFrom = document.getElementById('lblExportFrom');
            var txtTo = document.getElementById('txtExportTo');
            var lblTo = document.getElementById('lblExportFrom');
            var isDisabled = (ddlExportType.selectedIndex == 0);
            txtFrom.disabled = isDisabled;
            lblFrom.disabled = isDisabled;
            txtTo.disabled = isDisabled;
            lblTo.disabled = isDisabled;
        }
    }
    function ExportToChanged() {
        var ddlExportTo = document.getElementById('ddlExportTo');
        var categorySelected = ddlExportTo.options[ddlExportTo.selectedIndex].value;

        var ddlExportType = document.getElementById('ddlExportType');

        for (var i = 0; i < ddlExportType.options.length; i++) {
            ddlExportType.options[i].hidden = true;
        }

        switch (categorySelected) {
            case "0":
                ddlExportType.options[0].hidden = false;
                ddlExportType.options[1].hidden = false;
                ddlExportType.options[2].hidden = false;
                ddlExportType.options[3].hidden = true;
                ddlExportType.options[4].hidden = true;
                ddlExportType.options[5].hidden = true;

                break;
            case "1":
                ddlExportType.options[0].hidden = false;
                ddlExportType.options[1].hidden = false;
                ddlExportType.options[2].hidden = false;
                ddlExportType.options[3].hidden = true;
                ddlExportType.options[4].hidden = true;
                ddlExportType.options[5].hidden = true;

                break;
            case "2":
                ddlExportType.options[0].hidden = true;
                ddlExportType.options[1].hidden = false;
                ddlExportType.options[2].hidden = false;
                ddlExportType.options[3].hidden = true;
                ddlExportType.options[4].hidden = true;
                ddlExportType.options[5].hidden = true;

                break;
            case "3":
                ddlExportType.options[0].hidden = true;
                ddlExportType.options[1].hidden = false;
                ddlExportType.options[2].hidden = false;
                ddlExportType.options[3].hidden = true;
                ddlExportType.options[4].hidden = true;
                ddlExportType.options[5].hidden = true;

                break;
            case "4":
                ddlExportType.options[0].hidden = true;
                ddlExportType.options[1].hidden = false;
                ddlExportType.options[2].hidden = false;
                ddlExportType.options[3].hidden = true;
                ddlExportType.options[4].hidden = true;
                ddlExportType.options[5].hidden = true;

                break;
            case "5":
                ddlExportType.options[0].hidden = true;
                ddlExportType.options[1].hidden = true;
                ddlExportType.options[2].hidden = true;
                ddlExportType.options[3].hidden = false;
                ddlExportType.options[4].hidden = false;
                ddlExportType.options[5].hidden = false;
                break;
            default:
                ddlExportType.options[0].hidden = true;
                ddlExportType.options[1].hidden = false;
                ddlExportType.options[2].hidden = true;

                break;
        }
    }

