﻿// Global Variables
var criteriaCount = 0;
var assessmentSection;

/*
TODO: Remove from file once common routine has been validated
//Method to display a message in a named control
function DisplayMessage(panel, message) {
    var strHtml = '<table class="width-full white"><tr><td>' + message + '</td></tr></table>';
    var details = document.getElementById(panel);
    details.innerHTML = strHtml;
}
*/
//Async Ajax call method used to load a risk assessment section
function RiskAssessmentSection_Load() {
    try {
        var sectionId = document.getElementById("hfSectionId").value;
            Ajax_CallService(
                        '../Services/Web/RiskAssessment.asmx',
                        'AssessmentSectionGetForId',
                        {
                            sectionId: sectionId
                        },
                        true,
                        DisplaySection);
        }
    catch (err) {
        // 
            DisplayMessage("pnlSectionDetails", "Error loading section details.");
        }
    }

//Async Ajax callback method to display a risk assessment section
function DisplaySection(section) {
    try {
        // 
        var panel = document.getElementById("pnlSectionDetails");
        criteriaCount = section.Criteria.length;
        var strHtml = '<div id="sectionsLabels"class="width-full"><div ><div class="riskField">';
        strHtml += '<label class="riskLabel">Description</label><label class="riskLongInput">';
        strHtml += section.Description;
        strHtml += '</label></div><div class="riskField">';
        strHtml += '<label class="riskLabel">Reference</label><label class="riskInput">';
        strHtml += section.ReferenceDoc;
        strHtml += '</label></div><div class="riskField">';
        strHtml += '<label class="riskLabel">Chapter</label><label class="riskInput">';
        strHtml += section.Chapter;
        strHtml += '</label></div></div>';
        strHtml += '<div class="width-full centerPanel"><table  class="riskCriteriaGrid">';
        if (criteriaCount > 0) {
            var currentCriteria = section.Criteria[0];
            var strChecked = '';
            strHtml += '<tr id="-1"><th>Criteria</th><th>Ref</th><th>Phase</th><th>Control Required</th><th>On Work Day</th></tr>';
            for (j = 0; j < criteriaCount; j++) {
                currentCriteria = section.Criteria[j];
                strHtml += '<tr id="' + currentCriteria.Id + '"><td class="risEditkDetail">';
                strHtml += currentCriteria.Criterion;
                strHtml += '</td>';
                strHtml += '<td class="riskEditCriteria">';
                strHtml += currentCriteria.ChapterRef;
                strHtml += '</td>';
                strHtml += '<td class="riskEditCriteria">';
                strHtml += currentCriteria.AssessmentPhase;
                strHtml += '</td>';
                if (currentCriteria.AssessmentPhase > 1) {
                    strHtml += '<td class="riskEditCriteria">';
                    strHtml += 'N/A';
                } else {
                    strHtml += '<td class="riskEditCriteria">';
                    strHtml += '<a id="controlRequired_' + j.toString() + '" href="javascript: changeAssessmentValue(\'controlRequired_' + j.toString() + '\');">';
                    strHtml += getControlStatus(currentCriteria.ControlRequired);
                    strHtml += '</a>';
                }
                strHtml += '</td>';
                strHtml += '<td class="riskEditCriteria">';
                strHtml += '<a id="workDayRequired_' + j.toString() + '" href="javascript: changeAssessmentValue(\'workDayRequired_' + j.toString() + '\');">';
                strHtml += getControlStatus(currentCriteria.OnDayControlRequired);
                strHtml += '</a>';
                strHtml += '</td>';
                strHtml += '</tr>';
            }
        } else {
            strHtml += '<tr id="-2"><td>No criteria assigned to section</td></tr>';
        }
        strHtml += '</table></div>';
        strHtml += '<div class="width-full riskField">';
        strHtml += '<label class="riskLabel">Comments</label>';
        strHtml += '<textarea id="txtComments" class="riskLongInput" rows="6" name="txtComments">' + section.Comments + '</textarea>';
        strHtml += '</div>';
        panel.innerHTML = strHtml;

        assessmentSection = section;
    }
    catch (err) {
        DisplayMessage("pnlSectionDetails", "Error displaying risk assessment section details.");
    }
}

function RiskAssessmentSection_Save() {
    try {
        if (assessmentSection) {
            criteriaCount = assessmentSection.Criteria.length;
            for (var c = 0; c < criteriaCount; c++) {
                assessmentSection.Criteria[c].ControlRequired = getCriterionValue('controlRequired_' + c.toString());
                assessmentSection.Criteria[c].OnDayControlRequired = getCriterionValue('workDayRequired_' + c.toString());
            }
            assessmentSection.Comments = document.getElementById("txtComments").value;
            Ajax_CallService(
                'Services/Web/RiskAssessment.asmx',
                'RiskAssessmentSectionUpdate',
                {
                    section: assessmentSection
                },
                false);

        }
    } catch (err) {
        // 
        DisplayMessage("pnlSectionDetails", "Error saving risk assessment section details.");
    }
}

    function changeAssessmentValue(elementId) {
        var elm = document.getElementById(elementId);
        if (elm) {
            var value = elm.textContent;
            if (value == 'Not Assessed') {
                elm.textContent = 'No';
            } else if( value == 'No') {
                elm.textContent = 'Yes';
            } else if (value == 'Yes') {
                elm.textContent = 'Not Assessed';
            }
        }
    }

    function getControlStatus(enumValue) {
        switch (enumValue) {
            case 1:
                return "No";
                break;
            case 2:
                return "Yes";
                break;
            default:
                return "Not Assessed";
                break;
        }
    }

    function getCriterionValue(elementId) {
        // 
        var elm = document.getElementById(elementId);
        if (elm) {
            switch (elm.textContent) {
                case 'No':
                    return parseInt('1');
                    break;
                case 'Yes':
                    return parseInt('2');
                    break;
                default:
                    return parseInt('0');
                    break;
            }
        }else{
            return parseInt('0');
        }
    }

