﻿var lm_showEmailDiv = false;
var lm_emailToggler;
var lm_emailDiv;

var lm_showLetterDiv = true;
var lm_letterToggler;
var lm_letterDiv;

var lm_FileNames;
var lm_Attachments;
var lm_SelectedAttachments;

function LetterManagementLoad() {
    //  
    EnableTabs();

    var ClientID = document.getElementById('hfClientID').value;

    lm_FileNames = new Array();
    lm_SelectedAttachments = new Array();

    lm_letterDiv = $('#divLetterOptions');
    lm_letterToggler = $('#lblLetterOptionsToggle');

    lm_emailDiv = $('#divEmailOptions');
    lm_emailToggler = $('#lblEmailOptionsToggle');
    $(lm_emailDiv).hide();

    Ajax_CallService(
        'Services/Web/Report.asmx',
        'ClearTempEmailAttachments',
        {},
        false);

    var ObjectType;
    var hfObjectType = document.getElementById('hfInvoiceQuote').value;

    if (hfObjectType == 'Quote')
        ObjectType = 0;
    else if (hfObjectType == 'Invoice')
        ObjectType = 1;
    else ObjectType = 2;

    var ddlLetterRecipient = document.getElementById('ddlLetterRecipient');
    var ddlLEmailRecipient = document.getElementById('ddlEmailRecipient');

    var ddlLetterTemplate = document.getElementById('ddlLetterTemplate');
    var ddlEmailTemplate = document.getElementById('ddlEmailTemplate');

    var Recipients = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'GetClientContactList',
        { ClientID: ClientID },
        false);

    var i = 0;
    Recipients.forEach(function (r) {
        ddlLetterRecipient.options[i] = new Option(r.Name, r.Id);
        i++;
    });

    i = 0;
    Recipients.forEach(function (r) {
        if (r.Email.length > 0) {
            ddlEmailRecipient.options[i] = new Option(r.Name, r.Id);
            i++;
        }
    });

    var Templates = Ajax_CallService(
        'Services/Web/Settings.asmx',
        'GetTextTemplateList',
        {},
        false);

    i = 0;
    Templates.forEach(function (t) {
        if (t.MessageType == 0 && t.ObjectType == ObjectType) {
            ddlLetterTemplate.options[i] = new Option(t.TemplateName, t.Id);
            i++;
        }
    });

    i = 0;
    Templates.forEach(function (t) {
        if (t.MessageType == 1 && t.ObjectType == ObjectType) {
            ddlEmailTemplate.options[i] = new Option(t.TemplateName, t.Id);
            i++;
        }
    });

    ShowLetterTemplate();
    LoadLetterTemplate();
    LoadEmailTemplate();
}

function EnableTabs() {
    var textareas = document.getElementsByTagName('textarea');
    var count = textareas.length;
    for (i = 0; i < count; i++) {
        textareas[i].onkeydown = function (e) {
            if (e.keyCode == 9 || event.which == 9) {
                e.preventDefault();
                var s = this.selectionStart;
                this.value = this.value.substring(0, this.selectionStart) + "\t" + this.value.substring(this.selectionEnd);
                this.selectionEnd = s + 1;
            }
        };
    }
}

function ShowLetterTemplate() {
    var list = document.getElementById('ddlLetterType');



    var reg = document.getElementById('pnlLetterTemplate');
    if (list.options[list.selectedIndex].value == 0) {
        $(reg).slideDown();
        lm_FileNames[0] = document.getElementById('hfInvoiceQuote').value + 'Letter.pdf';
        lm_FileNames.splice(1, 1);
    } else if (list.options[list.selectedIndex].value == 1) {
        $(reg).slideUp();
        lm_FileNames[0] = document.getElementById('hfInvoiceQuote').value + 'Schedule.pdf';
        lm_FileNames.splice(1, 1);
    } else {
        $(reg).slideDown();
        lm_FileNames[0] = document.getElementById('hfInvoiceQuote').value + 'Letter.pdf';
        lm_FileNames[1] = document.getElementById('hfInvoiceQuote').value + 'Schedule.pdf';
    }
    LoadAttachments();
}

function LoadLetterTemplate() {
    var list = document.getElementById('ddlLetterTemplate');
    var val = list.options[list.selectedIndex].value;

    var LetterTemplate = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetTemplate',
        {
            ID: val
        },
        false);

    SetLetterText(LetterTemplate);
}

function LoadEmailTemplate() {
    var list = document.getElementById('ddlEmailTemplate');
    var val = list.options[list.selectedIndex].value;

    var EmailTemplate = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetTemplate',
        {
            ID: val
        },
        false);

    SetEmailText(EmailTemplate);
}

function LoadPDF(bPreview) {
    var InvoiceQuote = document.getElementById('hfInvoiceQuote').value;
    var IsInvoice = document.getElementById('hfInvoiceQuote').value == 'Invoice';
    var list = document.getElementById('ddlLetterType');
    var BodyText = document.getElementById('txtLetterText').value;
    var AdditionalText = document.getElementById('txtLetterText2').value;
    var addList = document.getElementById('ddlInvoiceAddress');

    var ddlQuoteID = document.getElementById('ddlQuoteID');
    var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;

    var recId = parseInt(document.getElementById('hfItemNo').value);
    
    var Result = false;

    if (!IsInvoice) {
        if (list.options[list.selectedIndex].value == 0) {
            Result = Ajax_CallService(
                'Services/Web/Report.asmx',
                'OpenQuoteLetter',
                {
                    RecID: QuoteID,
                    IsCopy: false,
                    LetterText: BodyText
                },
                false);
        } else if (list.options[list.selectedIndex].value == 1) {
            Result = Ajax_CallService(
                'Services/Web/Report.asmx',
                'OpenQuoteSchedule',
                {
                    RecID: QuoteID,
                    IsCopy: false
                },
                false);
        } else {
            Result = Ajax_CallService(
                'Services/Web/Report.asmx',
                'OpenCombinedQuoteDocuments',
                {
                    RecID: QuoteID,
                    IsCopy: false,
                    LetterText: BodyText
                },
                false);
        }
    } else {
        //Open the invoice
        if (list.options[list.selectedIndex].value == 0) {
            Result = Ajax_CallService(
                'Services/Web/Report.asmx',
                'OpenInvoiceLetter',
                {
                    RecID: recId,
                    IsCopy: false,
                    dockType: 0,
                    LetterText: BodyText
                },
                false);
        } else if (list.options[list.selectedIndex].value == 1) {
            Result = Ajax_CallService(
                'Services/Web/Report.asmx',
                'OpenInvoiceSchedule',
                {
                    RecID: recId,
                    IsCopy: false
                },
                false);
        } else {
            Result = Ajax_CallService(
                'Services/Web/Report.asmx',
                'OpenCombinedInvoiceDocuments',
                {
                    RecID: recId,
                    IsCopy: false,
                    dockType: 0,
                    LetterText: BodyText
                },
                false);
        }
    }

    $('#pdfPreview').attr('src', 'Controls/PDFViewer.aspx?v=' + Math.floor((Math.random() * 1000000) + 1));
    $('#divTempPreviewText').slideUp();


    if (Result && !bPreview) {
        //  
        if (!IsInvoice) {
            ChangeQuoteStatus(1);
        } else {
            ChangeInvoiceStatus(1);
        }
    }
}

function SendEmail() {
    var InvoiceQuote = document.getElementById('hfInvoiceQuote').value;
    var IsInvoice = document.getElementById('hfInvoiceQuote').value == 'Invoice';
    var list = document.getElementById('ddlLetterType');
    var BodyText = document.getElementById('txtLetterText').value;
    var AdditionalText = document.getElementById('txtLetterText2').value;
    var addList = document.getElementById('ddlInvoiceAddress');

    var ddlRecipient = document.getElementById('ddlEmailRecipient');
    var Recipient = ddlRecipient.options[ddlRecipient.selectedIndex].value;
    var Subject = document.getElementById('txtEmailSubject').value;
    var EmailText = document.getElementById('txtEmailText').value;

    var ddlQuoteID = document.getElementById('ddlQuoteID');
    var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;

    var strEndpoint = '';
    var selectedOption = list.options[list.selectedIndex].value;

    if (!IsInvoice) {
        //Set the service endpoint to be accessed
        if (selectedOption == 0) {
            strEndpoint = 'EmailQuoteLetter';
        } else if (selectedOption == 1) {
            strEndpoint = 'EmailQuoteSchedule';
        } else {
            strEndpoint = 'EmailQuoteDocuments';
        }
    } else {
        //Set the service endpoint to be accessed
        if (selectedOption == 0) {
            strEndpoint = 'EmailInvoiceLetter';
        } else if (selectedOption == 1) {
            strEndpoint = 'EmailInvoiceSchedule';
        } else {
            strEndpoint = 'EmailInvoiceDocuments';
        }
    }
    //Call the service
    try {
        Result = Ajax_CallService(
                'Services/Web/Report.asmx',
                strEndpoint,
                {
                    RecID: QuoteID,
                    IsCopy: false,
                    LetterText: BodyText,
                    RecipientContactID: Recipient,
                    Subject: Subject,
                    EmailText: EmailText
                },
                false);

        if (Result)
            ChangeInvoiceStatus(1);
    }
    catch (err) {

    }
}

function LoadAttachments() {
    lm_Attachments = new Array();

    lm_FileNames.forEach(function (fn) {
        lm_Attachments.push({ id: lm_FileNames.indexOf(fn), type: -1, fileName: fn });
    });

    var Docs = Ajax_CallService(
        'Services/Web/Report.asmx',
        'GetTempDocuments',
        {
            ItemID: parseInt(document.getElementById('hfItemNo').value)
        },
        false);

    Docs.forEach(function (file) {
        lm_Attachments.push({ id: file.ID, type: file.DocumentTypeID, fileName: file.FileName });
    });

    ShowAttachments();
}

function AddAttachment() {
    var Upload = document.getElementById('fuAttachment');
    if (Upload.files.length > 0) {
        if (Upload.value.indexOf('.exe') != -1
            || Upload.value.indexOf('.jar') != -1
            || Upload.value.indexOf('.bat') != -1
            || Upload.value.indexOf('.bash') != -1
            || Upload.value.indexOf('.msi') != -1
            || Upload.value.indexOf('.dll') != -1) {
            alert("Invalid file type");
            return false;
        }

        var file = Upload.files[0];

        var fr = new FileReader();
        fr.onload = function (event) {
            var result = event.target.result;
            var base64 = result.substring(result.indexOf("base64,") + 7);

            var Doc = Ajax_CallService(
                'Services/Web/Report.asmx',
                'AddTempDocument',
                {
                    ItemID: parseInt(document.getElementById('hfItemNo').value),
                    Description: "Temporary Document For Email Attachment",
                    ContentType: file.type,
                    ByteString: base64,
                    FilePath: Upload.value
                },
                false);

            SetSelected({ id: Doc.Id, type: Doc.DocumentTypeID }, true);
            LoadAttachments();
            Upload.value = "";
        };
        fr.readAsDataURL(file);
    } else {
        alert('No File Selected');
    }
}

function ShowAttachments() {
    var grid = document.getElementById('grdAttachments');

    for (var i = grid.rows.length - 1; i > 0; i--) {
        grid.deleteRow(i);
    }

    lm_Attachments.forEach(function (attachment) {
        var newRow = grid.insertRow(-1);

        newRow.className = 'GridItem';

        var nameCell = newRow.insertCell(0);

        nameCell.innerHTML = attachment.fileName;

        switch (attachment.type) {
            case -1:
                newRow.className += ' GridItem_Report';
                break;
            case 0:
                newRow.className += ' GridItem_Global';
                break;
            case 1:
                newRow.className += ' GridItem_Client';
                break;
            case 2:
                newRow.className += ' GridItem_Temp';
                break;
        }

        var checkCell = newRow.insertCell(1);

        checkCell.style.textAlign = 'center';

        var cb = document.createElement('input');
        cb.type = 'checkbox';
        cb.id = 'attachmentCheckbox_' + attachment.type + '_' + attachment.id;
        cb.disabled = attachment.type == -1;
        checkCell.appendChild(cb);

        var checked = false;
        lm_SelectedAttachments.forEach(function (sel) {
            if (sel.id == attachment.id
                && sel.type == attachment.type)
                checked = true;
        });
        if (attachment.type == -1) {
            checked = true;
        }
        cb.checked = checked;

        cb.addEventListener('change', function () {
            var check = document.getElementById(cb.id);
            SetSelected({ id: attachment.id, type: attachment.type }, check.checked);
        }, false);
    });
}

function SetSelected(attachment, selected) {
    if (selected) {
        lm_SelectedAttachments.push({ id: attachment.id, type: attachment.type });
    } else {
        if (lm_SelectedAttachments.length > 0) {
            var index = 0;
            for (var i = 0; i < lm_SelectedAttachments.length; i++) {
                if (lm_SelectedAttachments[i].id == attachment.id
                    && lm_SelectedAttachments[i].type == attachment.type) {
                    index = i;
                }
            }
            lm_SelectedAttachments.splice(index, 1);
        }
    }
}

function SetLetterText(template) {
    var Salutation = document.getElementById('txtLetterSalutation');
    var Text1 = document.getElementById('txtLetterText');
    var Text2 = document.getElementById('txtLetterText2');

    Salutation.value = template.Salutation;
    Text1.value = template.BodyText;

    if (template.ObjectType == 1) {
        $(Text2).slideDown();
        Text2.value = template.PaymentInfo;
    } else {
        $(Text2).slideUp();
        Text2.value = '';
    }
}

function SetEmailText(template) {
    var Subject = document.getElementById('txtEmailSubject');
    var Message = document.getElementById('txtEmailText');

    Subject.value = template.AdditionalText;
    Message.value = template.BodyText;
}

function LetterOptionsToggle() {
    lm_showLetterDiv = !lm_showLetterDiv;
    if (lm_showLetterDiv) {
        lm_letterToggler.innerHTML = "-";
        $(lm_letterDiv).slideDown();
    } else {
        lm_letterToggler.innerHTML = "+";
        $(lm_letterDiv).slideUp();
    }
}

function EmailOptionsToggle() {
    lm_showEmailDiv = !lm_showEmailDiv;
    if (lm_showEmailDiv) {
        lm_emailToggler.innerHTML = "-";
        $(lm_emailDiv).slideDown();
    } else {
        lm_emailToggler.innerHTML = "+";
        $(lm_emailDiv).slideUp();
    }
}