﻿
// gets the total open complaints that are currently logged in the system
function UpdateComplaintTotal() {
    $('#pnlComplaintsNotification').hide();
    try {
        Ajax_CallService(
            'Services/Web/Company.asmx',
            'SearchComplaintsForStatus',
            {
                closed: false
            },
            true,
            SetTotalOpenComplaints
        );
    } catch (err) {
    }

    try {
        Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetAllForReturnMarketing',
            {
            },
            true,
            SetTotalReturnMarketing
        );
    } catch (err) {
    }
}

function SetTotalOpenComplaints(res) {
    var total = res.length;
    $('#totalComplaints').text(total + ' complaint' + (total > 1 ? 's' : '') + ' requiring attention');
    if (total > 0) {
        $('#pnlComplaintsNotification').show();
        document.getElementById('pnlComplaintsDetails').className = "float-left";
    } else {
        document.getElementById('pnlComplaintsDetails').className = "hidden";
    }
}

function SetTotalReturnMarketing(res) {
    var total = res.length;
    $('#totalReturnMarketing').text(total + ' Return Date' + (total > 1 ? 's are' : ' is') + ' active');
    if (total > 0) {
        $('#pnlComplaintsNotification').show();
        document.getElementById('pnlMarketingComplaints').className = "float-right";
    } else {
        document.getElementById('pnlMarketingComplaints').className = "hidden";
    }
}

// init this control
$(function () {
    UpdateComplaintTotal();
    return true;
});
