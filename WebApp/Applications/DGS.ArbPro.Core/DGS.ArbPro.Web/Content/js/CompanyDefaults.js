﻿var QuoteRef = null;
var InvoiceRef = null;
var CreditRef = null;
var DefCompany = null;

function CheckNumber(elementName, originalValue) {
    var num;
    try{
        num = document.getElementById(elementName).value;
        if (isNaN(num)) {
            num = originalValue;
        }
    }
    catch (err) {
        //If an error occures set the number back to its intial value
        num = originalValue;
    }
    return parseInt(num);
}


function CompanyDefaults_Save() {
    QuoteRef.Prefix = document.getElementById('txtQuotePrefix').value;
    QuoteRef.Reference = CheckNumber('txtQuoteNumber', QuoteRef.Reference);
    QuoteRef.Suffix = document.getElementById('txtQuoteSuffix').value;

    InvoiceRef.Prefix = document.getElementById('txtInvoicePrefix').value;
    InvoiceRef.Reference = CheckNumber('txtInvoiceNumber', InvoiceRef.Reference);
    InvoiceRef.Suffix = document.getElementById('txtInvoiceSuffix').value;

    CreditRef.Prefix = document.getElementById('txtCreditPrefix').value;
    CreditRef.Reference = CheckNumber('txtCreditNumber', CreditRef.Reference);
    CreditRef.Suffix = document.getElementById('txtCreditSuffix').value;

    DefCompany.VatRate = document.getElementById('txtVatRate').value;
    
    var names = document.getElementById('ddlTreeNames').selectedIndex;
    var selection = true;
    if (isNaN(names) || names == 0)
        selection = true;
    else
        selection = false;

    if(DefCompany.CommonNames != selection)
    {
        DefCompany.CommonNames = selection;
        SaveTreeNamesSelection();
    }

    SaveReference(QuoteRef, 'UpdateQuoteReferences');
    SaveReference(InvoiceRef, 'UpdateInvoiceReferences');
    SaveReference(CreditRef, 'UpdateCreditReferences');

    SaveCompany();
}


function SaveTreeNamesSelection() {
    var Return = Ajax_CallService(
           'Services/Web/Company.asmx',
           'SaveCompany',
           {
               Company: DefCompany
           },
           false
       );
}

function SaveCompany() {
    var Return = Ajax_CallService(
       'Services/Web/Company.asmx',
       'SaveCompany',
       {
           Company: DefCompany
       },
       false
   );
}

function SaveReference(reference, endpointName) {
    try {
        Ajax_CallService(
        'Services/Web/Company.asmx',
        endpointName,
        {
            obj: reference
        },
        false,
        null);
    }
    catch (err) {

    }
}

function CompanyDefaults_Load(companyObj) {
    DefCompany = companyObj;
    LoadDocumnetReferences();
    DisplayTreeNameSettings();
    DisplayDefVat();
}

function LoadDocumnetReferences() {
    try{
        Ajax_CallService(
        'Services/Web/Company.asmx',
        'GetDocumentReferences',
        {},
        true,
        DisplayDocumentReferences
    );
    }
    catch (err) {
        DisplayMessage('pnlName', 'Error: Unable to connect to the ArbPro service.');
    }
}

function DisplayDocumentReferences(results) {

    if (!results || results.length < 3) {
        DisplayMessage('contentControls', 'Error: Service response invalid.');
    } else {
        try {
            DisplayQuoteRef(results[0]);
            DisplayInvoiceRef(results[1]);
            DisplayCreditRef(results[2]);
        }
        catch (err) {
            DisplayMessage('contentControls', 'Error: Unable to display default details.');
        }
    }
    //The data or errors should have now been displayed, so ensure the correct divs are displayed
    document.getElementById('waitTimer').className = 'hidden';
    document.getElementById('contentControls').className = 'width-full';

}

function DisplayQuoteRef(result) {
    if (!result) {
        DisplayMessage('pnlQuoteDefaults', 'Error: Service response invalid.');
    } else {
        try {
            QuoteRef = result;
            document.getElementById('txtQuotePrefix').value = QuoteRef.Prefix;
            document.getElementById('txtQuoteNumber').value = QuoteRef.Reference;
            document.getElementById('txtQuoteSuffix').value = QuoteRef.Suffix;
        }
        catch (err) {
            DisplayMessage('pnlQuoteDefaults', 'Error: Unable to display  quote defaults.');
        }
    }
}

function DisplayInvoiceRef(result) {
    if (!result) {
        DisplayMessage('pnlInvoiceDefaults', 'Error: Service response invalid.');
    } else {
        try {
            InvoiceRef = result;
            document.getElementById('txtInvoicePrefix').value = InvoiceRef.Prefix;
            document.getElementById('txtInvoiceNumber').value = InvoiceRef.Reference;
            document.getElementById('txtInvoiceSuffix').value = InvoiceRef.Suffix;
        }
        catch (err) {
            DisplayMessage('pnlInvoiceDefaults', 'Error: Unable to display  invoice defaults.');
        }
    }
}

function DisplayCreditRef(result) {
    if (!result) {
        DisplayMessage('pnlCreditDefaults', 'Error: Service response invalid.');
    } else {
        try {
            CreditRef = result;
            document.getElementById('txtCreditPrefix').value = CreditRef.Prefix;
            document.getElementById('txtCreditNumber').value = CreditRef.Reference;
            document.getElementById('txtCreditSuffix').value = CreditRef.Suffix;
        }
        catch (err) {
            DisplayMessage('pnlCreditDefaults', 'Error: Unable to display credit defaults.');
        }
    }
}

function DisplayTreeNameSettings() {
    try {
        var elm = document.getElementById('ddlTreeNames')
        if (DefCompany.CommonNames) {
            elm.selectedIndex = 0;
        }
        else {
            elm.selectedIndex = 1;
        }
        }
    catch (err) {
        DisplayMessage('pnlTreeDefaults', 'Error: Unable to display tree name defaults.');
    }
}

function DisplayDefVat() {
    document.getElementById('txtVatRate').value = DefCompany.VatRate;
}