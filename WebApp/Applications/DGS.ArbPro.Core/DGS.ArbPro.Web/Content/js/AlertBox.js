﻿function AlertBox_Show(msg) {
    var element = document.getElementById('pnlAlertBoxDlg');
    if (element) {
        var strHtml = '<label class="ClientInput padded">';
        strHtml += msg;
        strHtml += '</label>';

        element.innerHTML = strHtml;
    }
}