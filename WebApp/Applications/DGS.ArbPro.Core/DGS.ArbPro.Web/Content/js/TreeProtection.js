﻿var treeProtection = null;
var allTreeProtectionOrders = null;
var TpoStage_curTab = 0;
var localization = 'en_GB';
var ListJobSites = [];
var QuoteId = -1;
var CurrentJobSiteId = -1;
function TpoFormatDates(tpo) {
    tpo.ApplicationDate = toDateStringFromJson(tpo.ApplicationDate, null, 'dd/mm/y');
    tpo.RegistrationDate = toDateStringFromJson(tpo.RegistrationDate, null, 'dd/mm/y');
    tpo.EightWeekStatutory = toDateStringFromJson(tpo.EightWeekStatutory, null, 'dd/mm/y');
    tpo.SixWeekStatutory = toDateStringFromJson(tpo.SixWeekStatutory, null, 'dd/mm/y');
    tpo.InitialDecisionDate = toDateStringFromJson(tpo.InitialDecisionDate, null, 'dd/mm/y');
    tpo.AppealDate = toDateStringFromJson(tpo.AppealDate, null, 'dd/mm/y');
    tpo.AppealDecisionDate = toDateStringFromJson(tpo.AppealDecisionDate, null, 'dd/mm/y');
    return tpo;
}

//Syncronus save function
function Tpo_Save() {
    try {
        $(allTreeProtectionOrders).each(function (i, tpo) {
            UpdateCurrentTpoObject();

            var cl = jQuery.extend(true, {}, tpo);
            var tpoFixed = TpoFormatDates(cl);

            Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'SaveTpo',
                {
                    obj: tpoFixed
                },
                false,
                null);
        });
    }
    catch (err) {
        DisplayMessage('pnlTpoDetails', 'Error: Tree Protection Order details failed to save TPO');
    }
}

function UpdateCurrentTpoObject() {
    //Get the element values

    var local = 'en_GB';
    var Status = document.getElementById('ddlTpoStatus').selectedIndex;
    var OrderTypeEnum = document.getElementById('ddlTpoType').selectedIndex;
    var OrderNumber = document.getElementById('txtTpoOrderRef').value;
    var LocalAuthorityName = document.getElementById('txtTpoLocalAuthority').value;
    var ProgressStage = document.getElementById('ddlTpoProgressStage').selectedIndex;
    var InternalReference = document.getElementById('txtTpoInternalRef').value;
    var ApplicationDate = ParseLocalDate(document.getElementById('txtTpoApplicationDate').value, local);
    var RegistrationDate = ParseLocalDate(document.getElementById('txtTpoRegistrationDate').value, local);
    var EightWeekStatutory = ParseLocalDate($('#lblTpo8WeekDate').text(), local);
    var SixWeekStatutory = ParseLocalDate($('#lblTpo6WeekDate').text(), local);
    var CouncilReference = document.getElementById('txtTpoLocalAuthRef').value;
    var TpoOfficer = document.getElementById('txtTpoPlanningOfficer').value;
    var ContactNumber = document.getElementById('txtTpoContactNumber').value;
    var InitialDecisionDate = ParseLocalDate(document.getElementById('txtTpoDescisionDate').value, local);
    var AppealDate = ParseLocalDate(document.getElementById('txtTpoAppealDate').value, local);
    var AppealReference = document.getElementById('txtTpoAppealReference').value;
    var AppealDecisionDate = ParseLocalDate(document.getElementById('txtTpoAppealDecisionDate').value, local);
    var Notes = document.getElementById('txtTpoComments').value;

    treeProtection.Status = Status;
    treeProtection.OrderTypeEnum = OrderTypeEnum;
    treeProtection.OrderNumber = OrderNumber;
    treeProtection.LocalAuthorityName = LocalAuthorityName;
    treeProtection.ProgressStage = ProgressStage;
    treeProtection.InternalReference = InternalReference;
    treeProtection.ApplicationDate = dateToJsonDate(ApplicationDate);
    treeProtection.RegistrationDate = dateToJsonDate(RegistrationDate);
    treeProtection.EightWeekStatutory = dateToJsonDate(EightWeekStatutory);
    treeProtection.SixWeekStatutory = dateToJsonDate(SixWeekStatutory);
    treeProtection.CouncilReference = CouncilReference;
    treeProtection.TpoOfficer = TpoOfficer;
    treeProtection.ContactNumber = ContactNumber;
    treeProtection.InitialDecisionDate = dateToJsonDate(InitialDecisionDate);
    treeProtection.AppealDate = dateToJsonDate(AppealDate);
    treeProtection.AppealReference = AppealReference;
    treeProtection.AppealDecisionDate = dateToJsonDate(AppealDecisionDate);
    treeProtection.Notes = Notes;
    treeProtection.ConservationArea = false;
    treeProtection.Conformationreceived = false;
}

//Async service call to load a TPO from the database based on URL params
function Tpo_Load(quoteId, sites) {
    try {
        QuoteId = quoteId
        var tpoId = document.getElementById('hfTpoId').value;
        ListJobSites = sites;
        //var quoteId = document.getElementById('hfQuoteId').value;
        if (tpoId > -1) {
            //Load the Tpo from the Id
            Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'TpoGetForId',
            {
                id: tpoId
            },
            true,
            DisplaySingleTpo);
        } else if (quoteId > -1) {
            //Load the Tpo from the Quote record Id
            Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'TposGetForQuote',
            {
                quoteId: quoteId
            },
            true,
            DisplayTpo);
        } else {
            DisplayMessage('pnlTpoDetails', 'No Tree Protection Order details available.');
        }
    }
    catch (err) {
        DisplayMessage('pnlTpoDetails', 'Error: Unable to load Tree Protection Order details.');
    }
}

// async callback to display single TPO
function DisplaySingleTpo(tpo) {
    return DisplayTpo([tpo], 0);
}

// async callback to display all TPO in array
function DisplayTpo(tpos, index) {
    //debugger;
    allTreeProtectionOrders = tpos;
    if (tpos.length <= 0) {
        if (ListJobSites.length <= 0) {
            DisplayMessage('pnlTpoDetails', 'Error: There are no job sites against the quote.');
            return;
        }
        TpoJobSiteChanged(ListJobSites[0].SiteId);
        return;
    }

    // if no index is past into this function, or the index is invalid, default as selected list item, otherwise
    // change the selected list item to equal the correct jobsite id
    if (typeof (index) == typeof (undefined) || index == null || index >= tpos.length || index < 0 || tpos[index].JobSiteId == -1) {
        index = 0;
    }

    //Check if the TPO is valid
    if (!tpos) {
        DisplayMessage('pnlTpoDetails', 'Error: Unable to communicate with service.');
    }
    else {
        treeProtection = tpos[index];
        try {
            //Check if the object return from the database is valid
            if (treeProtection.Id < 1 && treeProtection.QuoteId < 1) {
                DisplayMessage('pnlTpoDetails', 'Error: The Tree Protection Order details are not valid.');
            }
            else {
                //Set the hidden field values to ensure that they are set regardless of the 
                //Params used to retreave the TPO
                document.getElementById('hfTpoId').value = treeProtection.Id;
                document.getElementById('hfQuoteId').value = treeProtection.QuoteId;
                document.getElementById('pnlTpoDetails').innerHTML = GenerateHTML(treeProtection);
                debugger;
                // update the selection so that the correct jobsite is selected
                if (treeProtection.JobSiteId == -1) {
                    treeProtection.JobSiteId = $("#tpoJobSiteSel").val();
                } else {
                    var WorkItmId = GetURLParameter('WorkItmId');
                    if (WorkItmId != 'null') {
                        $("#tpoJobSiteSel").val(CurrentJobSiteId);
                    }
                    else {
                        $("#tpoJobSiteSel").val(treeProtection.JobSiteId);
                    }
                }

                //Setup the auto complete field
                AutoComplete('txtTpoLocalAuthority', 'ListCouncil');
                AutoComplete('txtTpoOrderType', 'ListTpoOrderType');

                $('#txtTpoApplicationDate').datetimepicker({ startDate: toDatePickerDate(treeProtection.ApplicationDate), format: 'd/m/y', timepicker: false, closeOnDateSelect: true });
                $('#txtTpoRegistrationDate').datetimepicker({ format: 'd/m/y', timepicker: false, closeOnDateSelect: true });
                $('#txtTpoDescisionDate').datetimepicker({ format: 'd/m/y', timepicker: false, closeOnDateSelect: true });
                $('#txtTpoAppealDate').datetimepicker({ format: 'd/m/y', timepicker: false, closeOnDateSelect: true });
                $('#txtTpoAppealDecisionDate').datetimepicker({ format: 'd/m/y', timepicker: false, closeOnDateSelect: true });
                $('#txtTpoRegistrationDate').onblur = SetDeterminationDates();
                //document.getElementById('txtTpoRegistrationDate').onfocusout = SetDeterminationDates();
                SwitchTpoTab();
            }
        }
        catch (err) {
            DisplayMessage('pnlTpoDetails', 'Error: Unable to display Tree Protection Order details.');
        }
    }

    // return the actual index that was used so that the dropdown can be reconciled
    return index;
}

// function to generate a formatted date string from a JSON date using the datepicker method
function parseDateUsingDatePickerMethod(jsonDate, format) {
    // if JSON date string is empty
    if (jsonDate == null || jsonDate.length == undefined || jsonDate.length <= 0) {
        return "";
    }
    // check if JSON date is invalid/null
    var dateInt = parseInt(jsonDate.substr(6));
    if (dateInt <= 0) {
        return "";
    }
    // return correctly formatted JSON date
    return $.datepicker.formatDate(format, new Date(dateInt));
}

//function to generte inner html to display a tpo
function GenerateHTML(tpo) {
    //NOTE: All controls must have Tpo in the name so that when the parent page is constructed they can be easily defined
    var format = 'd/m/yyyy';
    var strHtml = '';
    debugger;
    //Create the details / overview setion
    strHtml += '<div style="width:730px;">'; //Write the containers opening tag
    strHtml += '<div id="pnlTabContainer" class="tpoJobSiteContentSection">'; //Content container tag
    strHtml += '<label>Edit TPO for the following Job Site:</label><br/>';
    strHtml += '<select id="tpoJobSiteSel" onchange="TpoJobSiteChanged()">';
    var WorkItmId = GetURLParameter('WorkItmId');
    var SiteId = GetURLParameter('SiteId');
    if (WorkItmId != 'null') {

        $(ListJobSites).each(function (index, element) {
            debugger;
            if (element.Id == SiteId) {
                strHtml += '<option value="' + element.SiteId + '" selected="selected">' + element.SiteString + '</option>';
                CurrentJobSiteId = element.SiteId;
                return false;

            }
        });

    }
    else {
        $(ListJobSites).each(function (index, element) {
            //DGSAP-676
            strHtml += "<option value='" + element.SiteId + "'>" + element.SiteString + "</option>";
        });
    }

    strHtml += '</select>';
    strHtml += '</div>';
    strHtml += '<div id="pnlTpoDetails" class="tpoTabContentSection">'; //Details container tag
    strHtml += '<div id="pnlTpoDetailsHeader"  class="riskTabBar">'; //Details header container tag
    strHtml += '<label>TPO Details for Quote Reference: ' + tpo.QuoteReference + '</label>';
    strHtml += '</div>';//Close pnlDetailsHeader tag
    strHtml += '<div id="pnlTpoDetailsContent">'; //Details body container tag
    //Form controls
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Protection Exists</label>';
    strHtml += CreateDropdown('TpoStatusOptions', 'ddlTpoStatus', tpo.Status);
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Order Type</label>';
    strHtml += CreateTypeDropDown(tpo.OrderTypeEnum);
    //strHtml += '<input id="txtTpoOrderType" class="clientInput" type="text" placeholder="The order type of the TPO" name="txtTpoOrderType" autocomplete="on" value="' + tpo.OrderType + '">';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Order Reference</label>';
    strHtml += '<input id="txtTpoOrderRef" class="clientInput" type="text" placeholder="The reference number of associated TPO" name="txtTpoOrderRef" autocomplete="on" value="' + tpo.OrderNumber + '">';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Local Authority</label>';
    strHtml += '<input id="txtTpoLocalAuthority" class="clientInput" type="text" placeholder="Local Authority name" name="txtTpoLocalAuthority" autocomplete="on" value="' + tpo.LocalAuthorityName + '">';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Application Stage</label>';
    strHtml += CreateDropdown('TpoProgressStageOptions', 'ddlTpoProgressStage', tpo.ProgressStage);
    strHtml += '</div>';
    //End form conrtrols
    strHtml += '</div>';//Close pnlDetailsContent tag
    strHtml += '</div>';// Close pnlTpoDetails tag

    strHtml += '<div class="spacer"><label class="riskLabel"></label></div>';

    strHtml += '<div id="pnlTabContainer" class="tpoTabContentSection">'; //Content container tag
    strHtml += '<div id="pnlTpoTabsHeader" class="riskTabBar">'; //Tabs container tag
    for (i = 0; i < 4; i++) {
        strHtml += '<a id="TpoStageTab_' + i.toString() + '" class="clientTab activeTab" href="javascript:TpoStage_curTab = ' + i.toString() + '; SwitchTpoTab();">';
        switch (i) {
            case 0:
                strHtml += 'Application';
                break;
            case 1:
                strHtml += 'Decision';
                break;
            case 2:
                strHtml += 'Appeal';
                break;
            case 3:
                strHtml += 'Notes';
                break;
        }
        strHtml += '</a>';
    }
    strHtml += '</div>'//Close pnlTpoTabsHeader tag

    //Create Application tab content
    strHtml += '<div id="pnlTpoTab_0" class="width-full ">'; //Tabs container tag
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Internal Reference</label>';
    strHtml += '<input id="txtTpoInternalRef" class="clientInput" type="text" placeholder="Internal Reference" name="txtTpoInternalRef" autocomplete="on" value="' + tpo.InternalReference + '">';
    strHtml += '</div>';
    var defaultDateString = 'Pending';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Application Date</label>';
    strHtml += '<input id="txtTpoApplicationDate" class="clientInput" type="text" placeholder="The date the TPO application was sent" name="txtTpoInternalRef" autocomplete="on" value="' +
        parseDateUsingDatePickerMethod(tpo.ApplicationDate, 'dd/mm/y') + '" style="width: 125px;">';
    //TODO: Add Calendar button to enable a user to select a date from the calendar control
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Registration Date</label>';
    //RegistrationDate
    strHtml += '<input id="txtTpoRegistrationDate" class="clientInput" type="text" placeholder="The date the local authority registered the application" name="txtTpoRegistrationDate" autocomplete="on" value="' +
        parseDateUsingDatePickerMethod(tpo.RegistrationDate, 'dd/mm/y') + '" style="width: 125px;" onblur ="SetDeterminationDates();">';
    //TODO: Add Calendar button to enable a user to select a date from the calendar control
    strHtml += '</div>';
    strHtml += '<div>';
    strHtml += '<div class="clientInput">';
    strHtml += '<label class="riskLabel">Determination Dates</label>';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Eight Week Tpo</label>';
    var dateString = toDateStringFromJson(tpo.RegistrationDate, defaultDateString, format);
    strHtml += '<label id="lblTpo8WeekDate" class="riskLabel">' + GetStringForDeterminationDate(dateString, 8) + '</label>';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Six Week Conservation</label>';
    strHtml += '<label id="lblTpo6WeekDate" class="riskLabel">' + GetStringForDeterminationDate(dateString, 6) + '</label>';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '</div>'; //Close pnlTpoTab_0
    //Create decision tab content
    strHtml += '<div id="pnlTpoTab_1" class="width-full">'; //Tabs container tag
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">LA Reference</label>';
    strHtml += '<input id="txtTpoLocalAuthRef" class="clientInput" type="text" placeholder="Local Authority Reference" name="txtTpoLocalAuthRef" autocomplete="on" value="' + tpo.CouncilReference + '">';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Planning Officer</label>';
    strHtml += '<input id="txtTpoPlanningOfficer" class="clientInput" type="text" placeholder="Planning Officer responsible for the application" name="txtTpoPlanningOfficer" autocomplete="on" value="' + tpo.TpoOfficer + '">';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Contact Number</label>';
    strHtml += '<input id="txtTpoContactNumber" class="clientInput" type="text" placeholder="Contact number for the planning officer" name="txtTpoContactNumber" autocomplete="on" value="' + tpo.ContactNumber + '">';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Descision Date</label>';
    strHtml += '<input id="txtTpoDescisionDate" class="clientInput" type="text" placeholder="The initial decision date for the application" name="txtTpoDescisionDate" autocomplete="on" value="' +
        parseDateUsingDatePickerMethod(tpo.InitialDecisionDate, 'dd/mm/y') + '" style="width: 125px;">';
    //TODO: Add Calendar button to enable a user to select a date from the calendar control
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Application Accepted</label>';//<label><input type="checkbox" name="checkbox" value="value">Text</label>
    if (tpo.ProgressStage == 2) {
        strHtml += '<input type="checkbox" id="chkApplicationAccepted" onclick="SetTpoStatus(2);" checked>';
    } else {
        strHtml += '<input type="checkbox" id="chkApplicationAccepted" onclick="SetTpoStatus(2);">';
    }
    strHtml += '</div>';
    strHtml += '</div>' //Close pnlTpoTab_1

    //Create Appleal tab content
    defaultDateString = tpo.ProgressStage >= 3 ? 'Pending' : 'N/A';
    strHtml += '<div id="pnlTpoTab_2" class="width-full">'; //Tabs container tag
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Date of Appeal</label>';
    strHtml += '<input id="txtTpoAppealDate" class="clientInput" type="text" placeholder="The date the Appeal was made" name="txtTpoAppealDate" autocomplete="on" value="' +
        parseDateUsingDatePickerMethod(tpo.AppealDate, 'dd/mm/y') + '" style="width: 125px;">';
    //TODO: Add Calendar button to enable a user to select a date from the calendar control
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Appeal Reference</label>';
    strHtml += '<input id="txtTpoAppealReference" class="clientInput" type="text" placeholder="The reference number for the Appeal" name="txtTpoAppealReference" autocomplete="on" value="' + tpo.AppealReference + '">';
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    strHtml += '<label class="riskLabel">Descision Date</label>';
    strHtml += '<input id="txtTpoAppealDecisionDate" class="clientInput" type="text" placeholder="The date the Appeal was made" name="txtTpoAppealDecisionDate" autocomplete="on" value="' +
        parseDateUsingDatePickerMethod(tpo.AppealDecisionDate, 'dd/mm/y') + '" style="width: 125px;">';
    //TODO: Add Calendar button to enable a user to select a date from the calendar control
    strHtml += '</div>';
    strHtml += '<div class="riskField">';
    if (tpo.ProgressStage == 6) {
        strHtml += '<label class="riskLabel">Appeal Granted<input type="checkbox" id="chkAppealGranted" onclick="SetTpoStatus(6);" checked></label>';//<label><input type="checkbox" name="checkbox" value="value">Text</label>
    } else {
        strHtml += '<label class="riskLabel">Appeal Granted<input type="checkbox" id="chkAppealGranted" onclick="SetTpoStatus(6);" ></label>';
    }
    strHtml += '</div>';
    strHtml += '</div>' //Close pnlTpoTab_2
    //Create Notes tab content
    strHtml += '<div id="pnlTpoTab_3" class="width-full">'; //Tabs container tag
    strHtml += '<textarea id="txtTpoComments" rows="10" name="txtTpoComments" class="width-full">' + tpo.Notes + '</textarea>';
    strHtml += '</div>' //Close pnlTpoTab_3

    strHtml += '</div>';// Close pnlTabContainer tag

    strHtml += '</div>'; //Containers closing tag
    return strHtml;
}

function TpoJobSiteChanged(defaultJobSiteId) {

    var jsid = -1;

    if (defaultJobSiteId == undefined) {
        UpdateCurrentTpoObject();
        jsid = $("#tpoJobSiteSel").val();
    } else {
        jsid = defaultJobSiteId;
    }

    var index = -1;

    $(allTreeProtectionOrders).each(function (i, o) {
        if (o.JobSiteId == jsid) {
            index = i;
        }
    });

    if (index >= 0) {
        DisplayTpo(allTreeProtectionOrders, index);
    } else {
        //  NOTE: The hidden fields are no longer used for holding Ids
        //  var quoteId = document.getElementById('hfQuoteId').value;
        try {

            //  Access the Selected quote that has been loaded into the Work Management page from Quote.js
            var quoteId = QuoteId;
            allTreeProtectionOrders[allTreeProtectionOrders.length] =
                Ajax_CallService(
                    'Services/Web/WorkFlow.asmx',
                    'TpoGetForQuoteAndJobSite',
                    {
                        quoteId: quoteId,
                        jobSiteId: jsid
                    },
                    false);
            DisplayTpo(allTreeProtectionOrders, allTreeProtectionOrders.length - 1);
        } catch (e) {
            DisplayMessage('pnlTpoDetails', 'Error: Unable to display Tree Protection Order details - ' + e.toString());
        }
    }
}

// function to print work schedule
function PrintWorkSchedule() {
    Dialog_OpenPage(
        'Work Schedule',
        'Dialogs/PDFWorkScheduleGeneration.aspx',
        'content',
        700,
        700,
        true,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/WorkPackGeneration.js?V=3.23.5',
        function () {
            PDFGeneration_Load(treeProtection.QuoteId, true);
        });

}

//Function to retreave a list from the settings service and populate a selection element
function CreateDropdown(endpointName, elementName, selectedId) {

    var listHtml = '';
    try {
        var itemsList = Ajax_CallService(
                'Services/Web/Settings.asmx',
                endpointName,
                {},
                false,
                null);

        listHtml = '<select id="' + elementName + '" class="clientInput">';
        var count = itemsList.length;
        for (var index = 0; index < count; index++) {
            if (selectedId == index) {
                listHtml += '<option value="' + index + '" selected="selected">' + itemsList[index] + '</option>';
            } else {
                listHtml += '<option value="' + index + '">' + itemsList[index] + '</option>';
            }
        }
    }
    catch (err) {
        listHtml = '<select id="' + elementName + '" class="clientInput">';
        listHtml += '<option value="-1"  selected="selected">Service Error</option>';
    }
    listHtml += '</select>';
    return listHtml;
}

function CreateTypeDropDown(selectedId) {
    //debugger;
    var listHtml = '<select id="ddlTpoType" class="clientInput">';
    var itemsList = ['TPO', 'Conservation Area'];
    var count = itemsList.length;
    for (var index = 0; index < count; index++) {
        if (selectedId == index) {
            listHtml += '<option value="' + index + '" selected="selected">' + itemsList[index] + '</option>';
        } else {
            listHtml += '<option value="' + index + '">' + itemsList[index] + '</option>';
        }
    }
    listHtml += '</select>';
    return listHtml;
}

function SetTpoStatus(statusValue) {
    var ddlProgressStage = document.getElementById('ddlTpoProgressStage');
    var index = ddlProgressStage.selectedIndex;
    var intialDesicionDate = Date.parse(document.getElementById('txtTpoDescisionDate').value);
    var appealDate = Date.parse(document.getElementById('txtTpoAppealDate').value);
    var appealDecisionDate = Date.parse(document.getElementById('txtTpoAppealDecisionDate').value);
    // 
    switch (statusValue) {
        case 0:
        case 1:
        case 3:
        case 4:
        case 5:
            break;
        case 2:
            if (index < 2) {
                index = 2;
            } else if (isNaN(intialDesicionDate)) {
                index = 1;
            } else {
                index = 3;
            }
            break;
        case 6:
            if (index < 6) {
                index = 6;
            }
            else if (isNaN(appealDecisionDate) && !isNaN(appealDate)) {
                index = 4;
            } else if (!isNaN(appealDecisionDate)) {
                index = 5;
            } else {
                if (isNaN(intialDesicionDate)) {
                    index = 1;
                } else {
                    index = 3;
                }
            }
            break;
    }
    ddlProgressStage.selectedIndex = index;
}

function SwitchTpoTab() {
    for (i = 0; i < 4; i++) {
        if (i == TpoStage_curTab) {
            document.getElementById('TpoStageTab_' + i.toString()).className = 'jobTab activeTab';
            document.getElementById('pnlTpoTab_' + i.toString()).className = '';
        } else {
            document.getElementById('TpoStageTab_' + i.toString()).className = 'jobTab';
            document.getElementById('pnlTpoTab_' + i.toString()).className = 'hidden';
        }
    }

}

function GetStringForDeterminationDate(dateString, weeksToAdd) {
    var d = ParseLocalDate(dateString, localization);
    d.setDate(d.getDate() + (7 * weeksToAdd));
    return formatDateString(d, localization);
}

function SetDeterminationDates() {
    // 
    var dateString = document.getElementById('txtTpoRegistrationDate').value;
    document.getElementById('lblTpo8WeekDate').innerHTML = GetStringForDeterminationDate(dateString, 8);
    document.getElementById('lblTpo6WeekDate').innerHTML = GetStringForDeterminationDate(dateString, 6);
}