﻿/*
    This java script file is intended to include all client side validation.
    Each function should return a boolean value
*/

//Validation function for email addresses. This function uses a regular expresion to patern match against the parameter
//Params:
//          addressToValidate       String value of the email address to be validated
//Returns:
//          bool                    True or false based on the patern match of the parmaeter and the regular expresion
function ValidateEmailAddress(addressToValidate) {
    var regEx = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regEx.test(addressToValidate);
}

// function to validate the length of an SMS template
// it is 150 characters of length due to the inclusion of the special replacement tags
function ValidateSmsTemplateLength(stringToValidate) {
    if (stringToValidate.length <= 150) {
        return true;
    }
    return false;
}

// function to validate the length of an sms template
// after the special replacement tags have been replaced with an actual string
function ValidateConvertedSmsLength(stringToValidate) {
    if (stringToValidate.length <= 160) {
        return true;
    }
    return false;
}