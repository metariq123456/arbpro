﻿//Set scope variables
var ListCurrentStaff = null;
var ListAllStaff = null;

//Intialisation function to load the dialog
function WorkPlanStaff_Load(currentStaff) {
    ListCurrentStaff = currentStaff;
    //Load the staff list from the service
    LoadStaff();
}
//Save function
//The return value must be captured in the javascript else an invalid postback could be raised
function WorkPlanStaff_Save(){
    //Create a new array for the selected staff
    
    var selectedStaff = [];
    var retVal = false;
    try
    {
        var element;
        //Iterate through each staff member to see if they have been selected
        ListAllStaff.forEach(function (s) {
            element = document.getElementById('chkTeamStaff_' + s.Id.toString());
            if (!element) {
                DisplayMessage('pnlPlanError', 'ERROR: Unable to determin selected staff - Element chkTeamStaff_' + s.Id.toString() + ' is not valid.');
                return false;
            } else {
                if (element.checked) {
                    //Create a new Team Staff member object to be associated with the work plan
                    selectedStaff.push(CreateStaffPlanObject(s));
                }
            }
        });
        //Check if any staff have been selected
        if (selectedStaff.length == 0) {
            DisplayMessage('pnlPlanError', 'INFORMATION: Please select staff for the work plan before clicking save.');
            return null;
        }

        //Check the value returned by the service
        if (!selectedStaff) {
            DisplayMessage('pnlPlanError', 'ERROR: Unable to save selected staff.');
            selectedStaff = null;
        }
    }
    catch (err) {
        retVal = null;
        DisplayMessage('pnlPlanError', 'ERROR: Unable to save selected staff to work plan - ' + err.toString());
    }
    return selectedStaff;
}


/*
    *************** SERVICE CALLS ***************
*/
//Async service call to get a list of staff
function LoadStaff() {
    try
    {
        Ajax_CallService(
            'Services/Web/Company.asmx',
            'ListAllStaff',
            {},
            true,
            DisplayDlgStaff
        );
    }
    catch (err) {
        DisplayMessage('pnlPlanError', 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

/*
    *************** SERVICE CALLS END ***************
*/


/*
    *************** SERVICE CALL BACKS ***************
*/
//Async callback to display the list of staff
function DisplayDlgStaff(results) {
    var panel = document.getElementById('pnlPlanStaff');
    try{
        if (!results || results.length == 0) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid list of staff.');
        } else {
            //Set the scope variable
            ListAllStaff = results;
            var strHtml = '<table class="searchGrid width-full white">';
            strHtml += '<tr><th>Staff Name</th><th>Team Member</th><th>Task</th></tr>';
            //Iterate through all the staff and create a row in the table
            ListAllStaff.forEach(function(c) {
                strHtml += '<tr onclick="SetStaffSelected(' + c.Id + ')">';
                strHtml += '<td style="width: 50%; padding-left: 10px;">' + c.Name + '</td>';
                strHtml += '<td style="text-align:center;"> <input type="checkbox" Id="chkTeamStaff_' + c.Id + '"/>  </td> ';
                strHtml += '<td class="companyInput"><input type="text" Id="txtTeamTask_' + c.Id + '" placeholder="Staff Role" title="The role within the team for the staff member"/>  </td> ';
                strHtml += '</tr>';
            });
            strHtml += '</table>';


            //Set the panels inner html to be the generated html;
            panel.innerHTML = strHtml;
        }
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display staff list - ' + err.toString());
    }
}



/*
    *************** END SERVICE CALL BACKS ***************
*/

/*
    *************** LOCAL FUNCTIONS ***************
*/
//Initialisation function, called after the service has been returned.
//The function selects all staff that are currently attributed to the workplan
function SelectCurrentStaff() {
    try{
        var element;
        ListCurrentStaff.forEach(function (staff) {
            element = document.getElementById('chkTeamStaff_' + staff.Id);
            element.checked = true;
        });
    } catch (err) {
        DisplayMessage('pnlPlanError', 'Error: Unable to set currently selected staff - ' + err.toString());
    }
}

//Row click event handler
function SetStaffSelected(staffId) {
    var chkBox = document.getElementById('chkTeamStaff_' + staffId.toString());
    chkBox.checked = !chkBox.checked;
}

//Function to create a staff work plan object from a staff record
function CreateStaffPlanObject(objStaff) {
    obj = new Object();
    obj.Id = -1;
    obj.ParentId = -1;
    obj.StaffId = objStaff.Id;
    obj.StaffName = objStaff.Name;
    obj.Task = document.getElementById('txtTeamTask_' + objStaff.Id.toString()).value;
    obj.Deleted = false;

    return obj;
}



/*
    *************** LOCAL FUNCTIONS END ***************
*/