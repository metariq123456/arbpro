﻿var Lat;
var Long;
var ZoomLevel;
var map;
var marker;
var ClientType = 0;

function NewClient_Load() {
    AutoComplete('txtNewClientTitle', 'ListTitles');
    AutoComplete('txtNewClientTown', 'ListTowns');
    AutoComplete('txtNewClientCounty', 'ListCounties');

    var btnResidential = document.getElementById('btnResidentialSelect');
    var btnCommercial = document.getElementById('btnCommercialSelect');

    btnResidential.className = 'button-blue';
    btnCommercial.className = 'button-basic';

    var pnlResidential = document.getElementById('pnlResidential');
    var pnlCommercial = document.getElementById('pnlCommercial');

    pnlResidential.className = 'clientField';
    pnlCommercial.className = 'hidden';

    var hfNewClientID = document.getElementById('hfNewClientID');
    var txtNewClientTitle = document.getElementById('txtNewClientTitle');
    var txtNewClientFirstName = document.getElementById('txtNewClientFirstName');
    var txtNewClientSurname = document.getElementById('txtNewClientSurname');
    var txtNewClientAltAccountNo = document.getElementById('txtNewClientAltAccountNo');
    var txtNewClientCompanyName = document.getElementById('txtNewClientCompanyName');
    var txtNewClientCompanySector = document.getElementById('txtNewClientCompanySector');
    var txtNewClientHouseNameNo = document.getElementById('txtNewClientHouseNameNo');
    var txtNewClientAddress1 = document.getElementById('txtNewClientAddress1');
    var txtNewClientAddress2 = document.getElementById('txtNewClientAddress2');
    var txtNewClientTown = document.getElementById('txtNewClientTown');
    var txtNewClientCounty = document.getElementById('txtNewClientCounty');
    var txtNewClientPostcode = document.getElementById('txtNewClientPostcode');
    var txtNewClientTelephone = document.getElementById('txtNewClientTelephone');
    var txtNewClientMobile = document.getElementById('txtNewClientMobile');
    var txtNewClientEmail = document.getElementById('txtNewClientEmail');
    var txtNewClientWebsite = document.getElementById('txtNewClientWebsite');
    var chkNewSendSms = document.getElementById('chkNewSendSms');
    var chkNewIncludeMarketing = document.getElementById('chkNewIncludeMarketing');

    var Client = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'GetClient',
        {
            ClientID: hfNewClientID.value
        },
        false
    );

    if (hfNewClientID.value > 0) {
        txtNewClientTitle.value = Client.Title;
        txtNewClientFirstName.value = Client.Forename;
        txtNewClientSurname.value = Client.Surname;
        txtNewClientAltAccountNo.value = Client.AltAccountNo;
        txtNewClientCompanyName.value = Client.BusinessName;
        txtNewClientCompanySector.value = Client.Sector;
        txtNewClientHouseNameNo.value = Client.HouseNameNumber;
        txtNewClientAddress1.value = Client.Address1;
        txtNewClientAddress2.value = Client.Address2;
        txtNewClientTown.value = Client.Town;
        txtNewClientCounty.value = Client.County;
        txtNewClientPostcode.value = Client.PostCode;
        txtNewClientTelephone.value = Client.Telephone;
        txtNewClientMobile.value = Client.Mobile;
        txtNewClientEmail.value = Client.Email;
        txtNewClientWebsite.value = Client.WebAddress;
        chkNewSendSms.checked = Client.SendSmsMessage;
        chkNewIncludeMarketing.checked = Client.IncludeInMarketing;

        switch (Client.AccountType) {
        case 0:
            ResidentialClick();
            break;
        case 1:
            CommercialClick();
            break;
        }
    } else {
        chkNewSendSms.checked = true;
        chkNewIncludeMarketing.checked = true;
    }

    Lat = 54;
    Long = -2;
    ZoomLevel = 5;

    var mapOptions = {
        center: new google.maps.LatLng(Lat, Long),
        zoom: ZoomLevel,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var mapcanvas = document.getElementById('pnlMapCanvas');
    map = new google.maps.Map(mapcanvas, mapOptions);

    var PostCode = document.getElementById('txtNewClientPostcode').value;

    if (PostCode != '') {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': PostCode + ', UK' },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var location = results[0].geometry.location;
                    map.panTo(location);
                    map.setZoom(12);

                    document.getElementById('hfNewClientLatitude').value = location.lat();
                    document.getElementById('hfNewClientLongitude').value = location.lng();

                    if (marker == null) {
                        marker = new google.maps.Marker({
                            position: location,
                            map: map,
                            title: PostCode
                        });
                    } else {
                        marker.setMap(map);
                        marker.setPosition(location);
                    }
                }
            });
    }
}

function NewClient_Save() {

    var hfNewClientID = document.getElementById('hfNewClientID');
    var txtNewClientTitle = document.getElementById('txtNewClientTitle');
    var txtNewClientFirstName = document.getElementById('txtNewClientFirstName');
    var txtNewClientSurname = document.getElementById('txtNewClientSurname');
    var txtNewClientAltAccountNo = document.getElementById('txtNewClientAltAccountNo');
    var txtNewClientCompanyName = document.getElementById('txtNewClientCompanyName');
    var txtNewClientCompanySector = document.getElementById('txtNewClientCompanySector');
    var txtNewClientHouseNameNo = document.getElementById('txtNewClientHouseNameNo');
    var txtNewClientAddress1 = document.getElementById('txtNewClientAddress1');
    var txtNewClientAddress2 = document.getElementById('txtNewClientAddress2');
    var txtNewClientTown = document.getElementById('txtNewClientTown');
    var txtNewClientCounty = document.getElementById('txtNewClientCounty');
    var txtNewClientPostcode = document.getElementById('txtNewClientPostcode');
    var txtNewClientTelephone = document.getElementById('txtNewClientTelephone');
    var txtNewClientMobile = document.getElementById('txtNewClientMobile');
    var txtNewClientEmail = document.getElementById('txtNewClientEmail');
    var txtNewClientWebsite = document.getElementById('txtNewClientWebsite');
    var chkNewSendSms = document.getElementById('chkNewSendSms');
    var chkNewIncludeMarketing = document.getElementById('chkNewIncludeMarketing');

    // Boolean to determine if we have the minimum amount of Client info
    var bValidatedMinInfo = true;
    var bNameMissing = false;
    var bAddress1 = false;
    var pnlMsg = document.getElementById('pnlValidationMsg');
    var sMissingFieldMsg = "Info: You have not entered the minimum CLIENT_TYPE_TOKEN client info. You need to enter";
    
    switch (ClientType) {
        case 0:
            // Residential - Ensure at least Surname and Address1
            if (txtNewClientSurname.value.trim() == '') {
                sMissingFieldMsg = sMissingFieldMsg.replace('CLIENT_TYPE_TOKEN', 'Residential');
                sMissingFieldMsg += ' a surname';
                bValidatedMinInfo = true;
                bNameMissing = true;
            }

            if (txtNewClientAddress1.value.trim() == '') {
                sMissingFieldMsg = sMissingFieldMsg.replace('CLIENT_TYPE_TOKEN', 'Residential');
                if (bNameMissing == true) {
                    sMissingFieldMsg += ' and address1';
                }
                else {
                    sMissingFieldMsg += ' address1';
                }
                bValidatedMinInfo = true;
                bAddress1 = true;
            }

            break;
        case 1:
            // Commercial - Ensure at least CompanyName and Address1
            if (txtNewClientCompanyName.value.trim() == '') {
                sMissingFieldMsg = sMissingFieldMsg.replace('CLIENT_TYPE_TOKEN', 'Commercial');
                sMissingFieldMsg += ' a company name';
                bValidatedMinInfo = true;
                bNameMissing = true;
            }

            if (txtNewClientAddress1.value.trim() == '') {
                sMissingFieldMsg = sMissingFieldMsg.replace('CLIENT_TYPE_TOKEN', 'Commercial');
                if (bNameMissing == true) {
                    sMissingFieldMsg += ' and address1';
                }
                else {
                    sMissingFieldMsg += ' address1';
                }
                bValidatedMinInfo = true;
                bAddress1 = true;
            }
            break;
    }

    if (bValidatedMinInfo == true) {

        return Ajax_CallService(
            'Services/Web/ClientData.asmx',
            'SaveClient',
            {
                ClientID: hfNewClientID.value,
                ClientType: ClientType,
                ClientTitle: txtNewClientTitle.value,
                ClientForeName: txtNewClientFirstName.value,
                ClientSurname: txtNewClientSurname.value,
                ClientBusinessName: txtNewClientCompanyName.value,
                ClientSector: txtNewClientCompanySector.value,
                ClientWebsite: txtNewClientWebsite.value,
                ClientAlternateAccountNo: txtNewClientAltAccountNo.value,
                ClientEmail: txtNewClientEmail.value,
                ClientTelephone: txtNewClientTelephone.value,
                ClientMobile: txtNewClientMobile.value,
                ClientHouseNameOrNumber: txtNewClientHouseNameNo.value,
                ClientAddress1: txtNewClientAddress1.value,
                ClientAddress2: txtNewClientAddress2.value,
                ClientTown: txtNewClientTown.value,
                ClientCounty: txtNewClientCounty.value,
                ClientPostCode: txtNewClientPostcode.value,
                ClientSmsMessage: chkNewSendSms.checked,
                ClientIncludeInMarketing: chkNewIncludeMarketing.checked
            },
            false
        );
    }
    else {
        // Just return -1 as saving failed, and display error message
        DisplayMessage(pnlMsg.id, sMissingFieldMsg);
        return -1;
    }
}

function UpdateMap() {
    var PostCode = document.getElementById('txtNewClientPostcode').value;

    if (PostCode != '') {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': PostCode + ', UK' },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var location = results[0].geometry.location;
                    map.panTo(location);
                    map.setZoom(12);

                    document.getElementById('hfNewClientLatitude').value = location.lat();
                    document.getElementById('hfNewClientLongitude').value = location.lng();

                    if (marker == null) {
                        marker = new google.maps.Marker({
                            position: location,
                            map: map,
                            title: PostCode
                        });
                    } else {
                        marker.setMap(map);
                        marker.setPosition(location);
                    }
                }
            });
    }
}

var addressWatcher = {
    timeout: null,
    currentValue: '',
    watchForChange: function (el) {
        if (el.value != this.currentValue) {
            this.changed(el);
        }
        this.timeout = setTimeout(function () {
            addressWatcher.watchForChange(el)
        }, 1000);
    },
    cancelWatchForChange: function () {
        clearTimeout(this.timeout);
        this.timeout = null;
    },
    changed: function (el) {
        this.currentValue = el.value;
        UpdateMap();
    }
}

function ResidentialClick() {
    ClientType = 0;

    var btnResidential = document.getElementById('btnResidentialSelect');
    var btnCommercial = document.getElementById('btnCommercialSelect');

    btnResidential.className = 'button-blue';
    btnCommercial.className = 'button-basic';

    var pnlResidential = document.getElementById('pnlResidential');
    var pnlCommercial = document.getElementById('pnlCommercial');

    pnlResidential.className = 'clientField';
    pnlCommercial.className = 'hidden';

    var txtWebsite = document.getElementById('txtNewClientWebsite');
    txtWebsite.className = 'hidden';
}

function CommercialClick() {
    ClientType = 1;

    var btnResidential = document.getElementById('btnResidentialSelect');
    var btnCommercial = document.getElementById('btnCommercialSelect');

    btnResidential.className = 'button-basic';
    btnCommercial.className = 'button-blue';

    var pnlResidential = document.getElementById('pnlResidential');
    var pnlCommercial = document.getElementById('pnlCommercial');

    pnlResidential.className = 'clientField';
    pnlCommercial.className = 'clientField';

    var txtWebsite = document.getElementById('txtNewClientWebsite');
    txtWebsite.className = 'clientInput';
    
}

