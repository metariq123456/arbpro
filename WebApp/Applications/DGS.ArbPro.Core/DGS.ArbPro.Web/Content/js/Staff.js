﻿var staffId = 0;
var CurStaffMember;

function staff_Load(Id) {
     
    AutoComplete('txtStaffTown', 'ListTowns');
    AutoComplete('txtStaffCounty', 'ListCounties');
    //StaffRole Autocomplete
    staffId = Id;
    var StaffMember = LoadStaffMember(staffId);
    PopulateStaffFields(StaffMember);
    CurStaffMember = StaffMember;
}

function Staff_Save() {
    try {
        CurStaffMember.Name = document.getElementById('txtStaffName').value;
        CurStaffMember.Email = document.getElementById('txtStaffEmail').value;
        CurStaffMember.Mobile = document.getElementById('txtStaffMobile').value;
        CurStaffMember.Telephone = document.getElementById('txtStaffTelephone').value;
        CurStaffMember.Position = document.getElementById('drpDownPostion').value;
        CurStaffMember.StreetNumber = document.getElementById('txtStaffStreetNum').value;
        CurStaffMember.Address1 = document.getElementById('txtStaffAddress1').value;
        CurStaffMember.Address2 = document.getElementById('txtStaffAddress2').value;
        CurStaffMember.Town = document.getElementById('txtStaffTown').value;
        CurStaffMember.County = document.getElementById('txtStaffCounty').value;
        CurStaffMember.PostCode = document.getElementById('txtStaffPostCode').value;
        CurStaffMember.StaffRole = document.getElementById('txtStaffRole').value;
        var isMobileEnabled = document.getElementById('chkStaffIsMobileEnabled').checked;
        //If the staff record is a new reord this check will evaluate to true (if the user has selected mobile enabled)
        //as the default is false
        var isMobileEnabledChanged = (CurStaffMember.IsMobileEnabled != isMobileEnabled);
        //Check if the user is mobile enabled, the email and the password has been set
        var newPassword = document.getElementById('txtMobilePassword').value;
        var confPassword = document.getElementById('txtMobPasswordConformation').value;
        if (isMobileEnabled) {
            if (CurStaffMember.Email.length > 0) {
                if (newPassword.length > 0 && newPassword == confPassword) {
                    CurStaffMember.Password = newPassword;
                    if (CurStaffMember.Password.length < 8) {
                        // Password is not long enough to pass validation
                        DisplayMessage('pnlStaffError', 'Unable to save Staff record: Please ensure that passwords are at least 8 characters long');
                        //Stop the dialog from closing to report the error
                        return false;
                    }
                } else if (isMobileEnabledChanged && newPassword != confPassword) {
                    //The mobile login has been enabled and no password has been set, or the confirmation password is different
                    DisplayMessage('pnlStaffError', 'Unable to save Staff record: Please ensure that the passwords match');
                    //Stop the dialog from closing to report the error
                    return false;
                } else {
                    //IF we get here the user is a mobile user, but the password is not to be updated: Do nothing
                }
            } else {
                // There is no email address so display message informing the user
                DisplayMessage('pnlStaffError', 'Unable to save Staff record: Please ensure that the email address is not empty');
                //Stop the dialog from closing to report the error
                return false;
            }
        }
        //Set the property now NOTE: setting before now would result in a false positive if the svae button is pressed as second time 
        CurStaffMember.IsMobileUser = isMobileEnabled;

        //CurStaffMember.Signature = null;
        var SaveMember = Ajax_CallService(
            'Services/Web/Company.asmx',
            'SaveStaff',
            {
                staff: CurStaffMember
            },
            false
        );
        //Check that the save operation completed
        if (SaveMember > 0)
            return true;
        else if (SaveMember == -9999) {
            //The staff record was updated but the corisponding mobile user record for the master db was not
            DisplayMessage('pnlStaffError', 'Error: The staff details have been updated but the mobile user details failed validation. Please ensure the email address is unique or check that the passwords are long enough.');
        } else if (SaveMember == -9998){
            //An error has occured display a message
            DisplayMessage('pnlStaffError', 'Update Error: The ArbPro service was unable to update the staff record. Please ensure that your password contains an upper and lower case letter and a number.');
            return false;
        }
    } catch (err) {
        DisplayMessage('pnlStaffError', 'Error: Unable to save Staff record - ' + err.toString());
        return false;
    }
}


function LoadStaffMember(staffId) {
    if (staffId > 0) {
        var StaffMember = Ajax_CallService(
               'Services/Web/Company.asmx',
               'GetStaffForId',
               {
                   Id: staffId
               },
               false
           );
        return StaffMember;
    }
    else {
        var NewStaff = Ajax_CallService(
              'Services/Web/Company.asmx',
              'GetNewStaffObj',
              {},
              false
          );
        return NewStaff;
    }
    
}


function PopulateStaffFields(StaffMember) {
   // document.getElementById('txtStaffPassword').value = 'password';
    document.getElementById('txtStaffName').value = StaffMember.Name;
    document.getElementById('txtStaffEmail').value = StaffMember.Email;
    document.getElementById('txtStaffMobile').value = StaffMember.Mobile;
    document.getElementById('txtStaffTelephone').value = StaffMember.Telephone;
    switch (StaffMember.Position) {
        case 0: //none
            document.getElementById('drpDownPostion').value = 0;
            break;
        case 1: //General
            document.getElementById('drpDownPostion').value = 1;
            break;
        case 2: //Team Member
            document.getElementById('drpDownPostion').value = 2;
            break;
        case 3: //Driver
            document.getElementById('drpDownPostion').value = 3;
            break;
        case 4: //Team Leader
            document.getElementById('drpDownPostion').value = 4;
            break;
        case 5: //Surveyor
            document.getElementById('drpDownPostion').value = 5;
            break;
        case 10: //Director
            document.getElementById('drpDownPostion').value = 10;
            break;

    }
  //  document.getElementById('txtStaffPosition').value = StaffMember.Position;
    document.getElementById('txtStaffStreetNum').value = StaffMember.StreetNumber;
    document.getElementById('txtStaffAddress1').value = StaffMember.Address1;
    document.getElementById('txtStaffAddress2').value = StaffMember.Address2;
    document.getElementById('txtStaffTown').value = StaffMember.Town;
    document.getElementById('txtStaffCounty').value = StaffMember.County;
    document.getElementById('txtStaffPostCode').value = StaffMember.PostCode;
    document.getElementById('txtStaffRole').value = StaffMember.StaffRole;
    document.getElementById('chkStaffIsMobileEnabled').checked = StaffMember.IsMobileUser;
    //NOTE: The password is not being set here so validation is made easier
    //Signature?
}
