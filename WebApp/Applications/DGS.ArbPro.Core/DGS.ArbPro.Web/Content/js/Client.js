﻿var Client_curTab = 1;
var Client_listShown = true;

function SwitchTab() {
    switch (Client_curTab) {
        case 1:
            document.getElementById('tabContacts').className = 'clientTab activeTab';
            document.getElementById('tabSites').className = 'clientTab';
            document.getElementById('tabJobs').className = 'clientTab';
            document.getElementById('tabAppointments').className = 'clientTab';
            //document.getElementById('tabQuotes').className = 'clientTab';
            //document.getElementById('tabInvoices').className = 'clientTab';

            document.getElementById('pnlTabContacts').className = '';
            document.getElementById('pnlTabSites').className = 'hidden';
            document.getElementById('pnlTabJobs').className = 'hidden'
            document.getElementById('pnlTabAppointments').className = 'hidden';
            //document.getElementById('pnlTabQuotes').className = 'hidden';
            //document.getElementById('pnlTabInvoices').className = 'hidden';

            document.getElementById('btnNewContact').className = 'button-thin float-right padded';
            document.getElementById('btnNewSite').className = 'button-thin float-right padded hidden';
            document.getElementById('btnNewJob').className = 'button-thin float-right padded hidden';
            break;
        case 2:
            document.getElementById('tabContacts').className = 'clientTab';
            document.getElementById('tabSites').className = 'clientTab activeTab';
            document.getElementById('tabJobs').className = 'clientTab';
            document.getElementById('tabAppointments').className = 'clientTab';
            //document.getElementById('tabQuotes').className = 'clientTab';
            //document.getElementById('tabInvoices').className = 'clientTab';

            document.getElementById('pnlTabContacts').className = 'hidden';
            document.getElementById('pnlTabSites').className = '';
            document.getElementById('pnlTabJobs').className = 'hidden'
            document.getElementById('pnlTabAppointments').className = 'hidden';
            //document.getElementById('pnlTabQuotes').className = 'hidden';
            //document.getElementById('pnlTabInvoices').className = 'hidden';

            document.getElementById('btnNewContact').className = 'button-thin float-right padded hidden';
            document.getElementById('btnNewSite').className = 'button-thin float-right padded';
            document.getElementById('btnNewJob').className = 'button-thin float-right padded hidden';
            break;
        case 3:
            document.getElementById('tabContacts').className = 'clientTab';
            document.getElementById('tabSites').className = 'clientTab';
            document.getElementById('tabJobs').className = 'clientTab activeTab';
            document.getElementById('tabAppointments').className = 'clientTab';
            //document.getElementById('tabQuotes').className = 'clientTab';
            //document.getElementById('tabInvoices').className = 'clientTab';

            document.getElementById('pnlTabContacts').className = 'hidden';
            document.getElementById('pnlTabSites').className = 'hidden';
            document.getElementById('pnlTabJobs').className = ''
            document.getElementById('pnlTabAppointments').className = 'hidden';
            //document.getElementById('pnlTabQuotes').className = 'hidden';
            //document.getElementById('pnlTabInvoices').className = 'hidden';

            document.getElementById('btnNewContact').className = 'button-thin float-right padded hidden';
            document.getElementById('btnNewSite').className = 'button-thin float-right padded hidden';
            document.getElementById('btnNewJob').className = 'button-thin float-right padded';
            break;
        case 4:
            document.getElementById('tabContacts').className = 'clientTab';
            document.getElementById('tabSites').className = 'clientTab';
            document.getElementById('tabJobs').className = 'clientTab';
            document.getElementById('tabAppointments').className = 'clientTab activeTab';
            //document.getElementById('tabQuotes').className = 'clientTab';
            //document.getElementById('tabInvoices').className = 'clientTab';

            document.getElementById('pnlTabContacts').className = 'hidden';
            document.getElementById('pnlTabSites').className = 'hidden';
            document.getElementById('pnlTabJobs').className = 'hidden'
            document.getElementById('pnlTabAppointments').className = '';
            //document.getElementById('pnlTabQuotes').className = 'hidden';
            //document.getElementById('pnlTabInvoices').className = 'hidden';

            document.getElementById('btnNewContact').className = 'button-thin float-right padded hidden';
            document.getElementById('btnNewSite').className = 'button-thin float-right padded hidden';
            document.getElementById('btnNewJob').className = 'button-thin float-right padded hidden';
            break;
            //case 5:
            //    document.getElementById('tabContacts').className = 'clientTab';
            //    document.getElementById('tabSites').className = 'clientTab';
            //    document.getElementById('tabJobs').className = 'clientTab';
            //    document.getElementById('tabAppointments').className = 'clientTab';
            //    document.getElementById('tabQuotes').className = 'clientTab activeTab';
            //    document.getElementById('tabInvoices').className = 'clientTab';

            //    document.getElementById('pnlTabContacts').className = 'hidden';
            //    document.getElementById('pnlTabSites').className = 'hidden';
            //    document.getElementById('pnlTabJobs').className = 'hidden'
            //    document.getElementById('pnlTabAppointments').className = 'hidden';
            //    document.getElementById('pnlTabQuotes').className = '';
            //    document.getElementById('pnlTabInvoices').className = 'hidden';
            //    break;
            //case 6:
            //    document.getElementById('tabContacts').className = 'clientTab';
            //    document.getElementById('tabSites').className = 'clientTab';
            //    document.getElementById('tabJobs').className = 'clientTab';
            //    document.getElementById('tabAppointments').className = 'clientTab';
            //    document.getElementById('tabQuotes').className = 'clientTab';
            //    document.getElementById('tabInvoices').className = 'clientTab activeTab';

            //    document.getElementById('pnlTabContacts').className = 'hidden';
            //    document.getElementById('pnlTabSites').className = 'hidden';
            //    document.getElementById('pnlTabJobs').className = 'hidden'
            //    document.getElementById('pnlTabAppointments').className = 'hidden';
            //    document.getElementById('pnlTabQuotes').className = 'hidden';
            //    document.getElementById('pnlTabInvoices').className = '';
            //    break;
        default:
            document.getElementById('tabContacts').className = 'clientTab';
            document.getElementById('tabSites').className = 'clientTab';
            document.getElementById('tabJobs').className = 'clientTab';
            document.getElementById('tabAppointments').className = 'clientTab';
            document.getElementById('tabQuotes').className = 'clientTab';
            document.getElementById('tabInvoices').className = 'clientTab';

            document.getElementById('pnlTabContacts').className = 'hidden';
            document.getElementById('pnlTabSites').className = 'hidden';
            document.getElementById('pnlTabJobs').className = 'hidden'
            document.getElementById('pnlTabAppointments').className = 'hidden';
            document.getElementById('pnlTabQuotes').className = 'hidden';
            document.getElementById('pnlTabInvoices').className = 'hidden';
            break;
    }
}

function EditClient() {
    var hfClientID = document.getElementById('hfClientID');

    Dialog_OpenPage(
        'Edit Client',
        'Dialogs/NewClient.aspx?ClientID=' + hfClientID.value,
        'content',
        560,
        495,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    var NewID = NewClient_Save();
                    if (NewID > -1) {
                        UpdateClientDetails();
                        $(this).dialog('close');
                        //window.location.search = '?ClientID=' + NewID;
                    }
                }
            },
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/NewClient.js?V=3.25.0',
        function () {
            NewClient_Load();
        });
}

function NewClient() {
    Dialog_OpenPage(
        'New Client',
        'Dialogs/NewClient.aspx?ClientID=-1',
        'content',
        560,
        495,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    var NewID = NewClient_Save();
                    if (NewID > -1) {
                        $(this).dialog('close');
                        window.location.search = '?ClientID=' + NewID;
                    }
                }
            },
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/NewClient.js?V=3.25.0',
        function () {
            NewClient_Load();
        });
}

function DeleteClient() {
    var confirmation = confirm('This action can only be undone by an ArbPro administrator.\nDo you wish to continue?');
    
    if (confirmation) {
        var hfClientID = document.getElementById('hfClientID');
        Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'SetDeleted',
        {
            clientId: hfClientID.value
        },
        true,
        ClientDeleted
    );
    } 
}

function ClientDeleted(result) {
    try {
        //Check if the client has been marked as deleted
        if (result) {
            window.location = '/Client.aspx';
        } else {
            ShowAlert('Error', 'The Arb Pro service failed to delete the client.');
        }
    } catch (err) {
        ShowAlert('Error', 'The Arb Pro service failed to delete the client - ' + err.toString());
    }
}

function ShowAlert(title, message) {
    Dialog_OpenPage(
           title,
           'Dialogs/Alert.aspx?',
           'content',
           285,
           450,
           true,
           {
               'Close': {
                   text: 'Ok',
                   class: 'ui-dialog-button',
                   click: function () {
                       $(this).dialog('close');
                   }
               }
           },
           'Content/js/AlertBox.js?V=3.23.5',
           function () { AlertBox_Show(message); });
}

function EditContact(iContactID) {
    Dialog_OpenPage(
        iContactID == -1 ? 'New Contact' : 'Edit Contact',
        'Dialogs/Contact.aspx?ID=' + iContactID,
        'content',
        295,
        450,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    if (SaveContactObject()) {
                        UpdateContactList();
                        // DGSAP-855
                        UpdateClientDetails(); 
                        $(this).dialog('close');
                    } else {
                        alert('Contact Save Failed');
                    }
                }
            },
            'Cancel': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/Contact.js?V=3.23.5',
        function () { Contact_Load(); });
}

function DeleteContact(iContactID) {
    //TODO: Delete Contact
    alert('This feature is under development.');
    return false;
}

function SetContactType(iContactID) {
    var dropdown = document.getElementById('clientContactType' + iContactID);
    var ClientList = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'SetContactType',
        {
            ContactID: iContactID,
            ContactType: dropdown.options[dropdown.selectedIndex].value
        },
        false
    );
    UpdateContactList();
    return false;
}

function EditSite(iSiteID) {
    Dialog_OpenPage(
        iSiteID == -1 ? 'New Site' : 'Edit Site',
        'Dialogs/Site.aspx?ID=' + iSiteID,
        'content',
        700,
        500,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    if (SaveSite()) {
                        UpdateSiteList();
                        $(this).dialog('close');
                    } else {
                        alert('Site Save Failed');
                    }
                }
            },
            'Cancel': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/Site.js?V=3.23.5',
        function () {
            //LoadGoogleApi(Site_Load);
            Site_Load();
        });
}

function StartList() {
    var List = $('#pnlClientsListList');
    //List.hide(0);
    UpdateList();
    //List.delay(500).show(200);
}

function ToggleList() {
    var Panel = $('#pnlClientsList');
    var Filter = $('#pnlClientsListFilter');
    var FilterText = $('#txtClientsListFilter');
    var List = $('#pnlClientsListList');

    if (Client_listShown) {
        Client_listShown = false;
        Panel.animate({ width: 10 }, 500);
        List.hide(100);
        setTimeout(function () {
            $('#pnlClientsListFilter').css('width', '0px');
            $('#txtClientsListFilter').css('visibility', 'hidden');
        }, 490);
    } else {
        Client_listShown = true;
        Panel.animate({ width: 200 }, 500);
        Filter.css('width', '190px')
        FilterText.css('visibility', 'visible')
        List.delay(500).show(100);
    }
}

var listWatcher = {
    timeout: null,
    currentValue: '',
    watchForChange: function (el) {
        if (el.value != this.currentValue) {
            this.changed(el);
        }
        this.timeout = setTimeout(function () {
            listWatcher.watchForChange(el)
        }, 1000);
    },
    cancelWatchForChange: function () {
        clearTimeout(this.timeout);
        this.timeout = null;
    },
    changed: function (el) {
        this.currentValue = el.value;
        UpdateList();
    }
}

function UpdateList() {
    var FilterText = document.getElementById('txtClientsListFilter').value;
    var Client = GetUser(Page_strUserEmail);
    var ID = document.getElementById('hfClientID').value;

    var ClientList = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'GetClientList',
        {
            FilterText: FilterText
        },
        false
    );

    var ListPanel = document.getElementById('pnlClientsListList');
    var strHtml = '<table class="searchGrid width-full white">';
    ClientList.forEach(function (c) {
        if (ID == c.Id) {
            strHtml += '<tr onclick="window.location.search = \'?ClientID=' + c.Id + '\';" class="selected">';
        } else {
            strHtml += '<tr onclick="window.location.search = \'?ClientID=' + c.Id + '\';">';
        }

        if (c.Name == '') {
            strHtml += '<td>ID#' + String(c.Id) + ': Unnamed</td>';
        } else {
            strHtml += '<td>' + c.Name + '</td>';
        }
        strHtml += '</tr>';
    });
    strHtml += '</table>';
    ListPanel.innerHTML = strHtml;
}

function UpdateContactList() {
    var Grid = document.getElementById('grdContacts');
    var Client = GetUser(Page_strUserEmail);
    var ClientID = document.getElementById('hfClientID').value;

    var Contacts = Ajax_CallService(
            'Services/Web/ClientData.asmx',
            'GetClientContactList',
            {
                ClientID: ClientID
            },
            false
        );

    var listHtml = '<table class="searchGrid width-full"><tr><th>Name</th><th>Telephone</th><th>Email</th><th>Contact Type</th><th></th></tr>';
    Contacts.forEach(function (c) {
        listHtml = listHtml + '<tr><td onclick="EditContact(' + c.Id + '); return false;">' + c.Name + '</td>';
        listHtml = listHtml + '<td onclick="EditContact(' + c.Id + '); return false;">' + c.Telephone + '</td>';
        listHtml = listHtml + '<td onclick="EditContact(' + c.Id + '); return false;">' + c.Email + '</td>';
        listHtml = listHtml + '<td>';
        listHtml = listHtml + '<select id="clientContactType' + c.Id + '" class="width-full" onchange="SetContactType(' + c.Id + ');">';
        listHtml = listHtml + '<option value="0"' + (c.ContactType == 0 ? ' selected="selected"' : '') + '>Default</option>';
        listHtml = listHtml + '<option value="1"' + (c.ContactType == 1 ? ' selected="selected"' : '') + '>General</option>';
        listHtml = listHtml + '<option value="2"' + (c.ContactType == 2 ? ' selected="selected"' : '') + '>Alternate Invoice</option>';
        listHtml = listHtml + '</select></td>';
        listHtml = listHtml + '<td onclick="event.preventDefault(); DeleteContact(' + c.Id + '); return false;" style="text-align: center;"><a href="#">Delete</a></td></tr>';
    });
    listHtml = listHtml + '</table>';
    Grid.innerHTML = listHtml;
}

function UpdateSiteList() {
    var Grid = document.getElementById('grdSites');
    var Client = GetUser(Page_strUserEmail);
    var ClientID = document.getElementById('hfClientID').value;

    var Sites = Ajax_CallService(
            'Services/Web/ClientData.asmx',
            'GetClientSiteList',
            {
                ClientID: ClientID
            },
            false
        );

    var listHtml = '<table class="searchGrid width-full"><tr><th>Address</th><th>Post Code</th></tr>';
    Sites.forEach(function (s) {
        listHtml = listHtml + '<tr onclick="EditSite(' + s.Id + '); return false;">';
        listHtml = listHtml + '<td>' + s.SiteDetails + '</td><td>' + s.PostCode + '</td></tr>';
    });
    listHtml = listHtml + '</table>';
    Grid.innerHTML = listHtml;
}

function UpdateJobList() {
    var Grid = document.getElementById('grdJobs');
    var Client = GetUser(Page_strUserEmail);
    var ClientID = document.getElementById('hfClientID').value;

    var Jobs = Ajax_CallService(
            'Services/Web/ClientData.asmx',
            'GetClientJobList',
            {
                ClientID: ClientID
            },
            false
        );
    //function SortByJobNumber(a, b) { return a.Id - b.Id; }
    //Jobs.sort(SortByJobNumber);

    var listHtml = '<table class="searchGrid width-full">';
    Jobs.forEach(function (j) {
        listHtml = listHtml + '<tr onclick="window.location = \'/Job.aspx?JobID=' + j.Id + '\';"><td>';
        listHtml = listHtml + j.SideBarDetail + '</td></tr>';
        //listHtml = listHtml + '<td width="15%">' + j.Id + '</td><td>' + j.Comments + '</td></tr>';
    });
    listHtml = listHtml + '</table>';
    Grid.innerHTML = listHtml;
}

function UpdateAppointmentList() {
    var Client = GetUser(Page_strUserEmail);
    var ClientID = document.getElementById('hfClientID').value;

    var Appts = Ajax_CallService(
            'Services/Web/ClientData.asmx',
            'GetClientAppointmentList',
            {
                ClientID: ClientID
            },
            false
        );

    var panel = document.getElementById('grdAppointments');
    var strHtml = '';

    Appts.forEach(function (appt) {

        var ApptDate = new Date(parseInt(appt.StartDateTime.substr(6)));
        strHtml = strHtml + '<div class="apptBox padded float-left"';
        strHtml = strHtml + ' onclick="EditAppointment(' + appt.Id + ');"';
        strHtml = strHtml + ' style="width: 190px; height: auto; display: inline; cursor: pointer;">';
        strHtml = strHtml + '<span class="apptTitle float-left">' + ApptDate.toLocaleDateString() + '</span>';
        strHtml = strHtml + '<span class="apptTitle float-right">' + zeroPad(ApptDate.getHours(), 2) + ':' + zeroPad(ApptDate.getMinutes(), 2) + '</span>';
        strHtml += '<br>';
        strHtml += '<span class="apptTitle center">Click to edit </span>';

        strHtml = strHtml + '<div class="width-full float-left bordered-bottom">';
        strHtml = strHtml + '</div>';

        strHtml = strHtml + '<div class="width-full float-left" style="overflow-y: auto;">';

        strHtml = strHtml + '<div class="apptField float-left">';
        strHtml = strHtml + '<span class="apptLabel width-full float-left">Site:</span>'
        strHtml = strHtml + '<span class="apptInput width-full float-left">' + appt.SiteAddress + '</span>'
        strHtml = strHtml + '</div>';

        strHtml = strHtml + '<div class="apptField float-left">';
        strHtml = strHtml + '<span class="apptLabel width-full float-left">Surveyor:</span>'
        strHtml = strHtml + '<span class="apptInput width-full float-left">' + appt.StaffName + '</span>'
        strHtml = strHtml + '</div>';

        strHtml = strHtml + '</div>';

        strHtml = strHtml + '</div>';
    });

    panel.innerHTML = strHtml;
}

function EditAppointment(iApptId) {
    var appt = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetQuoteAppointmentForId',
        {
            ApptId: iApptId
        },
        false);

     var jobSiteId = -2;// no value needed
     var jobSiteList = "";//no value needed
    var edit = true;
    //Set to true as we do not want to display the site selection ddl
    var enquiry = true;
    Dialog_OpenPage('Edit Appointment',
        'Dialogs/CalendarEditAppointment.aspx?SurveyorID=' + appt.SurveyorID.toString() + '&AppId=' + iApptId,
        'content',
        1200,
        700,
        true,
        {
            'Done': {
                text: 'Done',
                class: 'ui-dialog-button',
                click: function () {
                    SaveExistingAppointment(appt.Id, appt.JobSiteID);
                    UpdateAppointmentList();
                    $(this).dialog('close');
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/CalendarEditAppointment.js?V=3.23.5',
        function () {
            LoadCalendar(jobSiteList, edit, enquiry, jobSiteId);
        });
    //var appt = Ajax_CallService(
    //    'Services/Web/WorkFlow.asmx',
    //    'GetQuoteAppointmentForId',
    //    {
    //        ApptId: iApptId
    //    },
    //    false);

    //Dialog_OpenPage('Edit Appointment',
    //    'Dialogs/CalendarEditAppointment.aspx?SurveyorID=' + appt.SurveyorID.toString() + '&AppId=' + iApptId,
    //    'content',
    //    1200,
    //    screen.availHeight - 100,
    //    true,
    //    {
    //        'Done': {
    //            text: 'Done',
    //            class: 'ui-dialog-button',
    //            click: function () {
    //                SaveExistingAppointment(appt.Id, appt.JobSiteID);
    //                UpdateAppointmentList();
    //                $(this).dialog('close');
    //            }
    //        }
    //    },
    //    'Content/js/CalendarEditAppointment.js?V=3.23.5',
    //    function () {
    //        LoadCalendar();
    //    });
}

function SaveExistingAppointment(iApptID, iJobSiteID) {
    var start = $('#hfAppStart').val();
    var end = $('#hfAppEnd').val();
    var surveyor = $('#hfAppSurveyor').val();
    var completed = $('#chkAppointmentCompleted').is(':checked');

    Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'SaveQuoteAppointment',
        {
            ApptID: iApptID,
            JobSiteID: iJobSiteID,
            SurveyorID: surveyor,
            StartDateTime: start.toLocaleString(),
            EndDateTime: end.toLocaleString(),
            Completed: completed,
            ReDownload: false
        },
        false);
}
function CreateNewJob() {

    var ClientID = document.getElementById('hfClientID').value;

    ShowNewJobDialog(ClientID, DisplayNewJob);
    //var NewJobID = Ajax_CallService(
    //        'Services/Web/WorkFlow.asmx',
    //        'CreateNewJob',
    //        {
    //            ClientID: ClientID
    //        },
    //        false
    //    );

    //if (NewJobID > 0) {
    //    window.location = 'Job.aspx?JobID=' + NewJobID;
    //}
}

function DisplayNewJob(jobId) {
    if (jobId > 0) {
        window.location = 'Job.aspx?JobID=' + jobId;
    }
}

// DGSAP-855
function UpdateClientDetails() {
    var panel = document.getElementById('pnlClientInfo');
    var ClientID = document.getElementById('hfClientID').value;
    panel.innerHTML = GetWaitImageForPanel(panel.id, 'Loading Client details please wait.');

    //If the Id is < 0 a new Quote object will be returned
    Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'GetClient',
        {
            ClientID: ClientID
        },
        true,
        UpdateClient_Callback
    );
}

function UpdateClient_Callback(result) {
    var Grid = document.getElementById('pnlClientInfo');

    var strHtml = '<div class="colored-raise clientHeader">';
    strHtml += '<span>Client Information</span>';
    strHtml += '<input type="hidden"  id="hfClientID" value="'+result.Id.toString()+'" />';
    strHtml += '<input type="hidden"  id="hfClientType" value="' + result.AccountType.toString() + '" />';

    if (result.AccountType.toString() == "0") {
        strHtml += '<span id="lblClientType" class="clientInput float-right" style="font-weight:bold;">Residential</span>';
    }

    else {
        strHtml += '<span id="lblClientType" class="clientInput float-right" style="font-weight:bold;">Commercial</span>';
    }

    strHtml += '</div>';
    strHtml += '<div class="float-left" style="width:370px;">';

    if (result.AccountType.toString() == "0") {
        strHtml += '<div id="pnlClientResidential">';
        strHtml += '<div class="clientField">';
        strHtml += '<span class="clientLabel">Title</span>';
        strHtml += '<span id="txtClientTitle" class="clientInput">' + result.Title + '</span>';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<span class="clientLabel">Forename</span>';
        strHtml += '<span id="txtClientForename" class="clientInput">' + result.Forename + '</span>';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<span class="clientLabel">Surname</span>';
        strHtml += '<span id="txtClientSurname" class="clientInput">' + result.Surname + '</span>';
        strHtml += '</div>';
        strHtml += '</div>';
    }

    else {
        strHtml += '<div id="pnlClientResidential">';
        strHtml += '<div class="clientField">';
        strHtml += '<span class="clientLabel">Business Name</span>';
        strHtml += '<span id="txtClientTitle" class="clientInput">' + result.BusinessName + '</span>';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<span class="clientLabel">Business Sector</span>';
        strHtml += '<span id="txtClientForename" class="clientInput">' + result.Sector + '</span>';
        strHtml += '</div>';
        strHtml += '<div class="clientField">';
        strHtml += '<span class="clientLabel">Website</span>';
        strHtml += '<span id="txtClientSurname" class="clientInput">' + result.WebAddress + '</span>';
        strHtml += '</div>';
        strHtml += '</div>';
    }

    strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Account No</span>';
    strHtml += '<span id="txtClientAccountNo" class="clientInput">'+result.Id.toString()+'</span>';
    strHtml += '</div>';
    strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Alt Account No</span>';
    strHtml += '<span id="txtClientAlternateAccountNo" class="clientInput">'+result.AltAccountNo+'</span>';
    strHtml += '</div>';
    strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Email</span>';
    strHtml += '<span id="txtClientEmail" class="clientInput">'+result.Email+'</span>';
    strHtml += '</div>';
    strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Telephone</span>';
    strHtml += '<span id="txtClientTelephone" class="clientInput">'+result.Telephone+'</span>';
    strHtml += '</div>';
    strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Mobile</span>';
    strHtml += '<span id="txtClientMobile" class="clientInput">'+result.Mobile+'</span>';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '<div class="float-left" style="width:370px;">';
	strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Name/Number</span>';
    strHtml += '<span id="txtClientHouseNameOrNumber" class="clientInput">'+result.HouseNameNumber+'</span>';
    strHtml += '</div>';
    strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Address Line 1</span>';
    strHtml += '<span id="txtClientAddress1" class="clientInput">'+result.Address1+'</span>';
    strHtml += '</div>';
    strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Address Line 2</span>';
    strHtml += '<span id="txtClientAddress2" class="clientInput">' + result.Address2 + '</span>';
    strHtml += '</div>';
    strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Town</span>';
    strHtml += '<span id="txtClientTown" class="clientInput">' + result.Town + '</span>';
    strHtml += '</div>';
    strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">County</span>';
    strHtml += '<span id="txtClientCounty" class="clientInput">' + result.County + '</span>';
    strHtml += '</div>';
    strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Post Code</span>';
    strHtml += '<span id="txtClientPostCode" class="clientInput">' + result.PostCode + '</span>';
    strHtml += '</div>';
    strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Send SMS</span>';
	strHtml += '<span class="aspNetDisabled clientInput"><input id="chkSendSms" type="checkbox" disabled="disabled" ';
	if (result.SendSmsMessage)
	    strHtml += 'checked/> ';
    else
	    strHtml += '/>';
	strHtml += '</span></div>';
	strHtml += '<div class="clientField">';
	strHtml += '<span class="clientLabel">Include in Marketing</span>';
	strHtml += '<span class="aspNetDisabled clientInput"><input id="chkIncludeMarketing" type="checkbox" disabled="disabled" ';
	if (result.IncludeInMarketing)
	    strHtml += 'checked/> ';
	else
	    strHtml += '/>';
	strHtml += '</span></div>';
    strHtml += '</div>';
    strHtml += '<div>';
	strHtml += '<input type="submit" name="ctl00$Content$btnEdit" value="Edit" onclick="EditClient(); return false;" id="btnEdit" class="button-basic float-right padded" />';
    strHtml += '</div>';

    Grid.innerHTML = strHtml;
    UpdateContactList();
}
