﻿var ObjContact = null;
var pnlDisplayPanelName = 'pnlCmpContactDlgDetails';

function CompanyContact_Save() {
    try{
        ObjContact.BusinessName = document.getElementById('txtContactBusiness').value;
        ObjContact.Department = document.getElementById('txtContactDepartment').value;
        ObjContact.Title = document.getElementById('txtContactTitle').value;
        ObjContact.Forename = document.getElementById('txtContactFirstname').value;
        ObjContact.Surname = document.getElementById('txtContactSurname').value;
        ObjContact.JobTitle = document.getElementById('txtContactJobTitle').value;
        ObjContact.Address1 = document.getElementById('txtContactAddressLn1').value;
        ObjContact.Address2 = document.getElementById('txtContactAddressLn2').value;
        ObjContact.Town = document.getElementById('txtContactTown').value;
        ObjContact.County = document.getElementById('txtContactCounty').value;
        ObjContact.PostCode = document.getElementById('txtContactPostCode').value;
        ObjContact.Telephone = document.getElementById('txtContactTelephone').value;
        ObjContact.Mobile = document.getElementById('txtContactMobile').value;
        ObjContact.Email = document.getElementById('txtContactEmail').value;
        ObjContact.Comments = document.getElementById('txtContactDetail').value;

        var newId = Ajax_CallService(
            'Services/Web/Company.asmx',
            'SaveCompanyContact',
            {
                obj: ObjContact
            },
            false,
            null
        );

        if (newId < 1) {
            alert("Error: Unable to save contact details.");
        }

    } catch (err) {
        DisplayMessage(pnlDisplayPanelName, "Error: Unable to save contact details.");
    }
}

function CompanyContact_Load(id) {
    try{
        Ajax_CallService(
            'Services/Web/Company.asmx',
            'GetContact',
            {
                id: id
            },
            true,
            DisplayContactDetails
        );
    }
    catch (err) {
        DisplayMessage(pnlDisplayPanelName, "Error: Unable to communicate with ArbPro service.");
    }
}

function DisplayContactDetails(result) {
    if (!result) {
        DisplayMessage(pnlDisplayPanelName, "Error: ArbPro service failed to return valid data.");
    } else {
        try {
            ObjContact = result;
            var strHtml = '';
            strHtml += '<input id="txtContactBusiness" class="complaintField" type="text" placeholder="Business Name" title="Business Name" name="txtContactBusiness" value="' + ObjContact.BusinessName + '"/>';
            strHtml += '<input id="txtContactDepartment" class="complaintField" type="text" placeholder="Department" title="Department" name="txtContactDepartment" value="' + ObjContact.Department + '"/>';
            strHtml += '<input id="txtContactTitle" class="complaintField" type="text" placeholder="Title" title="Title" name="txtContactName" value="' + ObjContact.Title + '"/>';
            strHtml += '<input id="txtContactFirstname" class="complaintField" type="text" placeholder="Firstname" title="Firstname" name="txtContactFirstname" value="' + ObjContact.Forename + '"/>';
            strHtml += '<input id="txtContactSurname" class="complaintField" type="text" placeholder="Surname" title="Surname" name="txtContactName" value="' + ObjContact.Surname + '"/>';
            strHtml += '<input id="txtContactJobTitle" class="complaintField" type="text" placeholder="Job Title" title="Job Title" name="txtContactJobTitle" value="' + ObjContact.JobTitle + '"/>';
            strHtml += '<input id="txtContactAddressLn1" class="complaintField" type="text" placeholder="Address Line 1" title="Address Line 1" name="txtContactAddressLn1" value="' + ObjContact.Address1 + '"/>';
            strHtml += '<input id="txtContactAddressLn2" class="complaintField" type="text" placeholder="Address Line 2" title="Address Line 2" name="txtContactAddressLn2" value="' + ObjContact.Address2 + '"/>';
            strHtml += '<input id="txtContactTown" class="complaintField" type="text" placeholder="Town" title="Town" name="txtContactTown" value="' + ObjContact.Town + '"/>';
            strHtml += '<input id="txtContactCounty" class="complaintField" type="text" placeholder="County" title="County" name="txtContactCounty" value="' + ObjContact.County + '"/>';
            strHtml += '<input id="txtContactPostCode" class="complaintField" type="text" placeholder="Post Code" title="Post Code" name="txtContactPostCode" value="' + ObjContact.PostCode + '"/>';
            strHtml += '<input id="txtContactTelephone" class="complaintField" type="text" placeholder="Telephone" title="Telephone" name="txtContactTelephone" value="' + ObjContact.Telephone + '"/>';
            strHtml += '<input id="txtContactMobile" class="complaintField" type="text" placeholder="Mobile" title="Mobile" name="txtContactMobile" value="' + ObjContact.Mobile + '"/>';
            strHtml += '<input id="txtContactEmail" class="complaintField" type="text" placeholder="Email" title="Email" name="txtContactEmail" value="' + ObjContact.Email + '"/>';
            strHtml += '<textarea id="txtContactDetail" class="complaintField" rows="13" placeholder="Comments" title="comments" name="txtContactDetail">' + ObjContact.Comments + '</textarea>';
            document.getElementById(pnlDisplayPanelName).innerHTML = strHtml;

            AutoComplete('txtContactTown', 'ListTowns');
            AutoComplete('txtContactCounty', 'ListCounties');
        } catch (err) {
            DisplayMessage(pnlDisplayPanelName, "Error: Unable to display contact details.");
        }
    }
}