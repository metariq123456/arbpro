﻿var ListCompletedWork = null;
var ObjInvoice = null;
var ObjNewInvItem = null;
var ListSites = null;
var InvoiceId = -1;
var QuoteId = -1;
var SelectedSite = -1;
//Page intialisation function
function InvoiceItems_PageLoad(quoteId, invoiceId, jobSites) {
    //Set scope variables
    QuoteId = quoteId;
    InvoiceId = invoiceId;
    //To enable easy addition of new items to an invoice we must get the invoice
    //This is a blocking call as nothing else can be done until it is successful
    if (GetInvoice(invoiceId)) {
        //Ensure the invoice objects quote number is populated
        if (ObjInvoice.QuoteId < 0) {
            ObjInvoice.QuoteId = quoteId;
        }
        //Get a blank item object to help with client side scripting
        GetNewInvoiceItem();
        //Check if a list of job sites has been passed through
        if (!jobSites || jobSites.length == 0) {
            LoadJobSitesForQuote(quoteId);
        } else {
            ListSites = jobSites;
            //Call the function to populate the site selection element
            DisplayJobSiteSelections(ListSites);
        }

        //call the function to load the invoiceable work items
        LoadInvoiceableItems(quoteId);
    }
}

//Save button event handler
function InvoiceItems_Save() {
    var panel = document.getElementById('invPnlItemError');
    try
    {
        var saved = false;
        panel.innerHTML = GetWaitImageForPanel(panel.id,'Saving Please Wait');
        //Obtain the list of selected completed items to be add to the quote 
        var selectedItems = [];
        var element = null;
        //Check if the js is correctly intialised and setup
        if (IsIntialised()) {
            ListCompletedWork.forEach(function (i) {
                //check if the current completed work can be selected
                if (i.JobSiteId == SelectedSite) {
                    element = document.getElementById('chkWork_' + i.Id.toString());
                    //Check if the elmenet has been found before performing any actions
                    if (element && element.checked) {
                        selectedItems.push(i);
                    }
                }
            });

            //Get the selected job site object
            var site = GetJobSite(SelectedSite);

            //Check if there is a new item to be saved 
            element = document.getElementById('chkWork_New');
            if (element && element.checked) {
                //A new item has been selected to be added to the invoice
                ObjNewInvItem.InvoiceId = ObjInvoice.Id
                ObjNewInvItem.JobSiteId = SelectedSite;//site.Id;
                ObjNewInvItem.JobId = site.JobId;
                //Populate the properties
                ObjNewInvItem.Code = document.getElementById('txtNewInvItemTreeNo').value;
                ObjNewInvItem.Name = document.getElementById('txtNewInvItemTreeName').value;
                ObjNewInvItem.Description = document.getElementById('txtNewInvItemDescription').value;
                ObjNewInvItem.Value = document.getElementById('txtNewInvItemValue').value;
                ObjNewInvItem.VatRate = document.getElementById('txtNewInvItemVatRate').value;
                if (!ObjInvoice.InvoicedItems || ObjInvoice.InvoicedItems.length == 0) {
                    ObjInvoice.InvoicedItems = [];
                }
                ObjInvoice.InvoicedItems.push(ObjNewInvItem);
            }
            //The date items of the invoice object must be refreshed
            //NOTE: At this stage the dates are not set
            ObjInvoice.DateSent = new Date(0);
            ObjInvoice.DueDate = new Date(0);
            //Try to save the invoice items
            var retVal = Ajax_CallService(
                'Services/Web/WorkFlow.asmx',
                'SaveInvoice',
                {
                    obj: ObjInvoice,
                    selectedItems: selectedItems
                },
                false,
                null);

            if (!retVal) {
                DisplayMessage(panel.id, 'Error: The Invoice has not been saved correctly.');
            }
            saved = retVal;
        }
        return saved;
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to add invoice items to Invoice:' + err.toString());
        return false;
    }
}

function GetInvoice(id) {
    var success = false;
    try
    {
        ObjInvoice = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetInvoiceForId',
            {
                invoiceId: id
            },
            false,
            null);

        return true;
    }
    catch (err) {
        DisplayMessage('invPnlItemError', 'Error: Unable to load invoice' + id.toString() + '- ' + err.toString());
        success = false;
    }
    return success;
}

function GetNewInvoiceItem(){
    try
    {
        Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetNewInvoiceItem',
            {},
            true,
            SetNewInvoiceItem);
    }
    catch(err){
        //Do nothing, however this must be here to prevent the js from breaking if the ajax calls fails
    }
}

function SetNewInvoiceItem(result) {
    ObjNewInvItem = result;
}

//Async ajax call to load all completed work items
function LoadJobSitesForQuote(quoteId) {
    var panel = document.getElementById('invPnlSites');
    try {
        Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetJobSitesForQuote',
        {
            quoteId: quoteId
        },
        true,
        DisplayJobSiteSelections);
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to communicate with ArbPro service.');
    }
}

//Async ajax call to load all completed work items
function LoadInvoiceableItems(quoteId) {
    var panel = document.getElementById('invPnlWorkItems');
    try {
        Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetInvoicableWorkItems',
        {
            quoteId: quoteId
        },
        true,
        CreateItemsSection);
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to communicate with ArbPro service.');
    }
}

//Async call back function to populate the site selection panel
//This function can be called from the load function if a Site list is passed in
function DisplayJobSiteSelections(results) {
    var panel = document.getElementById('invPnlSites');
    try{
        if (!results || results.length == 0) {
            DisplayMessage(panel.id, 'Error: ArbPro service did not return valid data.');
        } else {
            ListSites = results;
            var strHtml = '<span style="display:inline-block;width:30%;"> Site: </span>';
            strHtml += '<select id="ddlAddInvoiceSite" class="jobInput" style="width:65%;" name="ddlAddInvoiceSite" onchange="SelectedSiteChanged();">';
            SelectedSite = -1;
            if(results.length > 1){
                strHtml += '<option value="-1" selected="selected"> Select Site to Invoice</option>';
                ListSites.forEach(function (js){
                    strHtml += '<option value="' + js.Id + '">' + js.SiteString +'</option>';
                });
            }else{
                strHtml += '<option value="' + ListSites[0].Id + '" selected="selected">' + ListSites[0].SiteString + '</option>';
                SelectedSite = ListSites[0].Id;
            }
            strHtml +='</select>';
            panel.innerHTML = strHtml;
        }
    }
    catch(err){
        DisplayMessage(panel.id, 'Error: Unable to display job site selection.');
    }
    
}

//Async call back to populate the list of invoiceable items
function CreateItemsSection(results) {
    var panel = document.getElementById('invPnlWorkItems');
    try{
        if (!results) {
            DisplayMessage(panel.id, 'Error: ArbPro service did not return valid data.');
        } else {
            ListCompletedWork = results;
            var strHtml = '<table class="searchGrid width-full" style="font-size: 0.7em;">';
            strHtml += '<tr onclick="SelectAllCompletedWork();">';
            strHtml += '<th style="width:5%;text-align: center;"><input type="checkbox" id="chkSelectAll"  /></th>';
            strHtml += '<th style="width:7%;text-align: center;"> Tree No. </th>';
            strHtml += '<th style="width:20%;text-align: center;"> Tree Name </th>';
            strHtml += '<th style="width:40%;text-align: center;"> Description </th>';
            strHtml += '<th style="width:15%;text-align: center;"> Value </th>';
            strHtml += '<th style="width:13%;text-align: center;"> VAT Rate </th>';
            strHtml += '</tr>';
            strHtml += '</table>';
            strHtml += '<div id="grdPnlCompletedItems" class="searchGrid" style="max-height: 220px; margin-bottom: 5px; overflow-y: auto; overflow-x: hidden;">';
            strHtml += '</div>';
            strHtml += '<div id="grdPnlNewItem" class="searchGrid">';
            strHtml += '<table class="searchGrid">';
            strHtml += '<tr>';
            strHtml += '<td style="width:5%;text-align: center;">';
            strHtml += '<input type="checkbox" id="chkWork_New"/>';
            strHtml += '</td>';
            strHtml += '<td style="width:7%;">';
            strHtml += '<input id="txtNewInvItemTreeNo" class="jobInput width-full" type="text" style="width:85%;" name="txtNewInvItemTreeNo">';
            strHtml += '</td>';
            strHtml += '<td style="width:20%;">';
            strHtml += '<span class="ui-helper-hidden-accessible" role="status" aria-live="polite"></span>';
            strHtml += '<input id="txtNewInvItemTreeName" class="jobInput width-full ui-autocomplete-input" type="text" style="width:90%;" name="txtNewInvItemTreeName" autocomplete="off">';
            strHtml += '</td>';
            strHtml += '<td style="width:40%;">';
            strHtml += '<span class="ui-helper-hidden-accessible" role="status" aria-live="polite"></span>';
            strHtml += '<input id="txtNewInvItemDescription" class="jobInput width-full ui-autocomplete-input" type="text" style="width:95%;" name="txtNewInvItemDescription" autocomplete="off">';
            strHtml += '</td>';
            strHtml += '<td style="width:15%;">';
            strHtml += '<input id="txtNewInvItemValue" class="jobInput float-right" type="text" style="width:80%;" value="0" name="txtNewInvItemValue">';
            strHtml += '<span class="float-right" style="padding-top: 7px; padding-right: 4px;"> ' + CurrentCurrency + ' </span>';
            strHtml += '</td>';
            strHtml += '<td style="width:13%;">';
            strHtml += '<span class="float-right" style="padding-top: 7px; padding-right: 4px;"> % </span>';
            strHtml += '<input id="txtNewInvItemVatRate" class="jobInput float-right" type="text" style="width:50%;" value="0" name="txtNewInvItemVatRate">';
            strHtml += '</td>';
            strHtml += '</tr>';
            strHtml += '</table>';
            strHtml += '</div>';
        }
        panel.innerHTML = strHtml;
        //Call the job site selection changed event
        //Display the filtered completed items list
        SelectedSiteChanged();
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display invoiceable items.');
    }
}

//Function to display the selected Job Site completed items
function DisplayInvoiceableItems(items, jobSiteId) {
    var panel = document.getElementById('grdPnlCompletedItems');
    try{
        var strHtml = '';
        strHtml += '<table id="tblCompletedItems" class="searchGrid width-full" style="font-size: 0.75em">';
        if (items.length > 0 && jobSiteId > 0) {
            items.forEach(function (i) {
                if (jobSiteId == i.JobSiteId) {
                    strHtml = strHtml + '<tr onclick="SetSelectedWork(' + i.Id.toString() + ');">';
                    strHtml = strHtml + '<td width="5%" style="text-align: center;" ><input type="checkbox" id="chkWork_' + i.Id.toString() + '"/></td>';
                    strHtml = strHtml + '<td width="7%" style="text-align: center;">' + i.Code + '</td>';
                    strHtml = strHtml + '<td width="20%" style="text-align: center;">' + i.Name + '</td>';
                    strHtml = strHtml + '<td width="40%" style="text-align: center;">' + i.Description + '</td>';
                    strHtml = strHtml + '<td width="15%" style="text-align: center;">' + CurrentCurrency + '' + i.Value.format() + '</td>';
                    strHtml = strHtml + '<td width="13%" style="text-align: center;">' + i.VatRate.format() + '%</td>';
                    strHtml = strHtml + '</tr>';
                }
            });
        }
        strHtml += '</table>';
        panel.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display invoiceable items.');
    }
}

//Set the selected jobsite Id and filter the selectable work list
function SelectedSiteChanged() {
    try
    {
        var ddl = document.getElementById('ddlAddInvoiceSite');
        var jobSite = ddl.options[ddl.selectedIndex].value;
        SelectedSite = jobSite;
        //Populate the table of selectable items
        DisplayInvoiceableItems(ListCompletedWork, jobSite);
    }
    catch (err) {
        alert('Error: Unable to update completed items list -' + err.toString());
    }
}

//Function to get the selected job site from an Id
function GetJobSite(jobSiteId) {
    var site = null;
    if (!ListSites || ListSites.length == 0) {
        return site;
    } else {
        var count = ListSites.length;
        for (var index = 0; index < count; index++) {
            site = ListSites[index];
            if (site.Id == jobSiteId) {
                break;
            }
        }
    }
    return site;
}

//Function to check all of the check boxes for completed work
function SelectAllCompletedWork() {
    //Check if the function can be run
    if (IsIntialised()) {
        var elm = null;
        var selected = document.getElementById('chkSelectAll').checked;
        ListCompletedWork.forEach(function (i) {
            //check if the current completed work can be selected
            if (i.JobSiteId == SelectedSite) {
                elm = document.getElementById('chkWork_' + i.Id.toString());
                //Check if the elmenet has been found before performing any actions
                if (elm) {
                    elm.checked = selected;
                }
            }
        });
    }
}

//function to set the sate of a rows check box from clicking on the row
function SetSelectedWork(itemId) {
    var chkElm = document.getElementById('chkWork_' + itemId.toString());
    if (chkElm) {
        chkElm.checked != chkElm.checked;
    }
}

//Helper function to ensure that data is in the correct format so that js functions to not break
function IsIntialised(){
    if(!ObjInvoice || !ListCompletedWork)
        return false;
    else if(!SelectedSite || SelectedSite < 1)
        return false;
    else
        return true;
}

/* SAVE Functions */
function AddSelectedWorkItemsToInvoice(items, newItem, quoteId, invoiceId) {
    var saved = false;
    if (InvoiceId < 1) {
        //This is a new invoice
        saved = Ajax_CallService(
       'Services/Web/WorkFlow.asmx',
       'CreateInvoiceWithCompletedItems',
       {
           quoteId: quoteId,
           completed: items,
           invoiceItem: newItem
       },
       false,
       null);
    } else {
        //This is an existing invoice
        saved = Ajax_CallService(
       'Services/Web/WorkFlow.asmx',
       'UpdateInvoiceWithCompletedItems',
       {
           invoiceId: invoiceId,
           completed: items,
           invoiceItem: newItem
       },
       false,
       null);
    }

    return saved;
}