﻿var CurrentCurrencyList = null;
var CurrencyList = null;
var CurrencyChoice = null;
function Settings_Load()
{
    LoadCurrencyList();
}
function LoadCurrencyList()
{
    CurrentCurrencyList = Ajax_CallService(
            'Services/Web/Settings.asmx',
            'GetCurrencyList',
            {},
            false
             );
    PopulateDropDownoptions(document.getElementById('ddlCurrencyList'), CurrentCurrencyList);

}
function PopulateDropDownoptions(ddlElement) {
    var count = CurrentCurrencyList.length;
    var strHtml = '';

    for (var index = 0; index < count; index++) {
        if (CurrentCurrencyList[index].Selected) {
            strHtml += '<option value="' + CurrentCurrencyList[index].Id + '" selected="selected">' + CurrentCurrencyList[index].Name + ' ' + CurrentCurrencyList[index].Character + '</option>';
        } else {
            strHtml += '<option value="' + CurrentCurrencyList[index].Id + '">' + CurrentCurrencyList[index].Name + ' ' + CurrentCurrencyList[index].Character + '</option>';
        }
    }
    ddlElement.innerHTML = strHtml;
}
function SaveCurrencySettings()
{
    try{
        var ddl =  document.getElementById('ddlCurrencyList');
        var Id = ddl.options[ddl.selectedIndex].value;
        Ajax_CallService(
            'Services/Web/Settings.asmx',
            'SetSelected',
            {
                Id: Id
            },
            false);
        setCurrentCurrency();
        return true;
    }
    catch (e) {
        alert('Error Saving Settings, Try again');
        return false; 
    }
}
function setCurrentCurrency()
{
    try {
        CurrencyChoice =Ajax_CallService(
            'Services/Web/Settings.asmx',
            'GetSelectedCurrency',
            {
            },
            false);
        CurrencyChanged(CurrencyChoice);
        return true;
    }
    catch (e) {
        alert('Error Saving Settings, Try again');
        return false;
    }
}
function CurrencyChanged(CurrencyChoice) {
    try
    {
        var CurrencyCharacter;
        CurrencyCharacter = CurrencyChoice[0].Character;
        localStorage.setItem("CurrentCurrency", CurrencyCharacter);
    }
    catch (e)
    {
        alert(e);
    }
    }
