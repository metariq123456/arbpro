﻿/*
    The following scripts are intended to be used for the parsing of localized dates.
    At the time of development there is no defined JSON structure for a Date.
    For this script the .NET JSON structure will be assumed.
    NOTE:
                Microsoft's ASP.NET AJAX encodes .NET DateTime values as a JSON string,
                where the content of the string is /Date(ticks)/ and where ticks
                represents milliseconds since epoch (UTC). So November 29, 1989, 4:55:30 AM, in UTC is 
                encoded as "\/Date(628318530718)\/".
*/

//Function to convert a formatter date string into a javascript date
//Params    :
//      dateString a formated date
//      localisation - this is the culture code of the format of the date string, currently only UK and US are supported
function ParseLocalDate(dateString, localisation) {
    try
    {
        // --- D/M/YY OR DD/MM/YY OR DD/MM/YYYY --- GB
        var parts = dateString.split(GetDelimitingChar(dateString));
        var UTCString = ''; // a string in the format YYYY-MM-DD
        if (parts.length == 3) {
        
            switch (localisation) {
                case "en_GB":
                    UTCString = GetDateComponentValue(parts[2], 100, '20') + '-' + GetDateComponentValue(parts[1], 10, '0') + '-' + GetDateComponentValue(parts[0], 10, '0');
                    break;
                case "en_US":
                    //  
                    UTCString = GetDateComponentValue(parts[2], 100, '20') + '-' + GetDateComponentValue(parts[0], 10, '0') + '-' + GetDateComponentValue(parts[1], 10, '0');
                    break;
            }
        }
        else {
            //Set the UTC value to the parsed in value
            UTCString = dateString;
        }

        var d = Date.parse(UTCString);
        //Check if the created date is valid.
        if (isNaN(d) || d < 0)
            return new Date(0);
        else
            return new Date(d);
    }
    catch (err) {
        return new Date(0);
    }
}
function toUTCString(dateString, localisation) {
    try {
        // --- D/M/YY OR DD/MM/YY OR DD/MM/YYYY --- GB
        var parts = dateString.split(GetDelimitingChar(dateString));
        var UTCString = ''; // a string in the format YYYY-MM-DD
        if (parts.length == 3) {

            switch (localisation) {
                case "en_GB":
                    UTCString = GetDateComponentValue(parts[2], 100, '20') + '-' + GetDateComponentValue(parts[1], 10, '0') + '-' + GetDateComponentValue(parts[0], 10, '0');
                    break;
                case "en_US":
                    //  
                    UTCString = GetDateComponentValue(parts[2], 100, '20') + '-' + GetDateComponentValue(parts[0], 10, '0') + '-' + GetDateComponentValue(parts[1], 10, '0');
                    break;
            }
        }
        else {
            //Set the UTC value to the parsed in value
            UTCString = dateString;
        }
    }
        catch (e){
            alert(e);
        }
        return UTCString;

}
//Function to create a Javascript date from a .NET JSON date
//Params    :
//      value - a .Net json date string
function toDateFromJson(value) {
    if (typeof value === 'string') {
        //  The parameter is a string to be parsed into a date object
        if (value.indexOf('/Date') < 0) {
            //  The value parameter is already in date string format
            return ParseLocalDate(value, 'en_GB');
        } else {
            var ticks = parseInt(value.replace("/Date(", "").replace(")/", ""), 10);
            if (ticks < 0) {
                ticks = 0;
            }
            var dateValue = new Date(ticks);
            return dateValue;
        }
    }
    else if (typeof value === 'number'){
        //  The value is a number, interprit this as the number of ticks
        return new Date(value);
    }
    else {
        //  The value is already a string ?
        return value;
    }
}

//Function to create a Javascript string from a .NET JSON date or return a default value
//Params    :
//      json - a .Net json date string
//      defaultString - a default value to display incase of an invalid like min date time 
//      format - this is the culture code of the format of the date string, currently only UK and US are supported
function toDateStringFromJson(json, defaultString, format) {
    var ticks = parseInt(json.replace("/Date(", "").replace(")/", ""), 10);
    var retValue = '';
    if (ticks > 0) {
        var dateValue = new Date(ticks);
        retValue = formatDateString(dateValue, format);
    } else {
        return defaultString;
    }
    return retValue;
}

function dateToJsonDate(date) {
    try
    {
        var ticks = date.getTime();
        if (ticks <= 0) {
            return "";
        }
        return "/Date(" + ticks + ")/";
    }
    catch (ex) {
        return "";
    }
}
function parseNormalDate(s) {
    var months = {
        jan: 0, feb: 1, mar: 2, apr: 3, may: 4, jun: 5,
        jul: 6, aug: 7, sep: 8, oct: 9, nov: 10, dec: 11
    };
    var a = s.indexOf(' ');
    if (a && a > 0) {
        s = s.substring(0, a);
    }
    var p = s.split('/');
    return new Date(p[2], months[p[1].toLowerCase()], p[0]);
}


//Function to create a Javascript string from a .NET JSON date or return a default value
//Params    :
//      json - a .Net json date string
//      defaultString - a default value to display incase of an invalid like min date time 
//      format - this is the culture code of the format of the date string, currently only UK and US are supported
function toFormattedDateStringFromJson(json, defaultString, format) {
    var ticks = parseInt(json.replace("/Date(", "").replace(")/", ""), 10);
    var retValue = '';
    if (ticks > 0) {
        var dateValue = new Date(ticks);
        retValue = formatDateString(dateValue, format);
    } else {
        return defaultString;
    }
    return retValue;
}


//Function to create a string from a .NET JSON date that can be used by the the calendar control
//Params    :
//      json - a .Net json date string
function toDatePickerDate(json) {
    if (json.replace == undefined) {
        return '';
    }

    var ticks  = parseInt(json.replace("/Date(", "").replace(")/", ""), 10);
    
    var retValue = '';
    if (ticks > 0) {
        var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var dateValue = new Date(ticks);
        retValue = ParseDateAsString(dateValue);
    } else {
        retValue = '';
    }
    return retValue;
}

function toDateStringFromJson(json) {
    return toDatePickerDate(json);
}

function toDateStringFromDate(d) {
    return ParseDateAsString(d);
}

function ParseDateAsString(dateValue) {
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var hrs = dateValue.getHours().toString();
    if (hrs.length == 1) {
        hrs = '0' + hrs;
    }
    var min = dateValue.getMinutes().toString();
    if (min.length == 1) {
        min = '0' + min;
    }
    retValue = dateValue.getDate().toString() + '/' + months[dateValue.getMonth()] + '/' + dateValue.getFullYear().toString() + ' ' + hrs + ':' + min;
    return retValue;
}

function ParseDateWithoutTimeAsString(dateValue) {
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var hrs = dateValue.getHours().toString();
    if (hrs.length == 1) {
        hrs = '0' + hrs;
    }
    var min = dateValue.getMinutes().toString();
    if (min.length == 1) {
        min = '0' + min;
    }
    retValue = dateValue.getDate().toString() + '/' + months[dateValue.getMonth()] + '/' + dateValue.getFullYear().toString();
    return retValue;
}

//Function to create a javascript date from a string
//NOTE - This function has been known to transpose the day and month values
//Params    :
//      value - a date string
function toDateFromString(value) {
    try {
        var retDate = new Date(value);
        return retDate;
    }
    catch (err) {
        return new Date(0);
    }
}

//Function to create a min date value that can be passed to a .NET service
function minJQueryDate() {
    var dateValue = new Date(0);
    return dateValue;
}

function GetDateComponentValue(value, min, prefix) {

    var component = '';
    var intValue = parseInt(value)
    if (intValue < min)
        component = prefix + intValue.toString();
    else
        component = intValue.toString();

    return component;
}

function GetDelimitingChar(value) {
    if (value.indexOf('/') > 0)
        return '/';

    if (value.indexOf('-') > 0)
        return '-';

    if (value.indexOf(' ') > 0)
        return ' ';
}

//Function to format a javascript date to a specified format
//Params    :
//      dateValue - a .Net json date string
//      format - the culture code or format for the returned value
//          supports the lower or upercase of the following
//              en_GB / en_US /  any combination of the following yyyy/mm/dd / y/m/d / dd/mm/yy / dd/mm/yyyy / m /d/y
//              y or yy will format the year with 2 characters yyyy will format the year in 4 characters
function formatDateString(dateValue, format) {
    if (format) {
        var f = format.toLowerCase();
        if (f == 'en_gb') {
            f = 'd/m/yyyy';
        }
        else if (f == 'en_us') {
            // 
            f = 'm/d/yyyy';
        }
        var days = dateValue.getDate();
        var month = dateValue.getMonth() + 1;
        var year = dateValue.getFullYear().toString();
        if (days < 10) {
            f = f.replace('dd', 'd').replace('d', '0' + days.toString());
        } else {
            f = f.replace('dd', 'd').replace('d', days.toString());
        }
        if (month < 10) {
            f = f.replace('mm', 'm').replace('m', '0' + month.toString());
        } else {
            f = f.replace('mm', 'm').replace('m', month.toString());
        }

        if (f.indexOf('yyyy') >= 0) {
            f = f.replace('yyyy', year);
        }
        else {
            f = f.replace('yy', 'y').replace('y', year.substr(2, 2));
        }
        return f;
    }
    else {
        return dateValue.toDateString();
    }
}

function addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes * 60000);
}

function toTicksFromDate(d) {
    
    try{
        var ticks = ((d.getTime() * 10000) + 621355968000000000);
        return ticks;
    } catch (err) {
        return '';
    }
}

function toTicksFromJson(json) {
    
    try {
        var ticks = ((d.getTime() * 10000) + 621355968000000000);
        return ticks;
    } catch (err) {
        return '';
    }
}