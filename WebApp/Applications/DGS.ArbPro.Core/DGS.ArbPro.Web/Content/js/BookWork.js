﻿var editEvent = {
    id: 0,
    title: 'Start',
    allDay: false,
    start: null,
    end: null,
    className: 'cal-event-editable'
}

var QuoteId = -1;
var WorkId = -1;
var ReferrerSiteID = -1;
var SelectedSite = -1;
var SelectedSchedule = -1;

var ObjWorkSummary = null;  //  The work summary for the quote or work Id passed to the page. NOTE: This object contains list of all required items except appointments
var ListTeams = null;       //  The list of teams configured in the system

//Lists for selected items
var ListSelectedStaff = new Array();
var ListSelectedTools = new Array();

var SelectedWorkPlanId = -1;
var SelectedPlan = null;
var timer;

//Initialisation Function
function BookWork_Load() {
    LoadTeams();
    LoadInfo();
    var jobID = document.getElementById('hfJobID').value;
    QuoteId = document.getElementById('hfQuoteId').value;
    WorkId = document.getElementById('hfWorkId').value;
    ReferrerSiteID = document.getElementById('hfReferrerSiteId').value;

    var endpoint = 'GetWorkSummaryForWork';
    var recordId = -1;
    //Check if a workId has been passed in
    if (WorkId > 0) {
        recordId = WorkId;
    } else if (QuoteId > 0) {
        recordId = QuoteId;
        endpoint = 'GetWorkSummaryForQuote';
    }
    //Start the required loading processes
    LoadWorkSummary(endpoint, recordId);
    //The notes are not dependant on a work plan so can be loaded
    SwitchNotesPanel(0);
    //Load the calendar
    LoadCalendar();
    //Setup the link back to the work flow screen
    //Replaced with button calling ClosePage() function
    //var linkUrl = '/Job.aspx?JobID=' + document.getElementById('hfJobID').value
    //linkUrl += '&QuoteID=' + QuoteId.toString();

    //var link = document.getElementById('lnkReturnToJob');
    //link.href = linkUrl;
}

/*
                SERVICE CALLS
*/
//Async service call to load the work summary
//endPoint  - The service method to be invoked
//recordId  - The Id of the relevant record
function LoadWorkSummary(endPoint, recordId) {
    try {
        Ajax_CallService(
        'Services/Web/WorkProgramming.asmx',
        endPoint,
        {
            id: recordId
        },
        true,
        DisplayWorkSummary
        );
    }
    catch (err) {
        DisplayMessage('pnlErrorWork', 'Error: Unable to load work summary - ' + err.toString());
        DisplayMessage('pnlSummaryDetail', 'Error: Unable to load work summary - ' + err.toString());
    }
}

//Async service call to get all teams that are configured for the current user
function LoadTeams() {
    try {
        Ajax_CallService(
        'Services/Web/WorkProgramming.asmx',
        'GetAllTeam',
        {},
        true,
        DisplayTeams
        );
    }
    catch (err) {
        DisplayMessage('pnlWorkDetail_1', 'Error: Unable to load teams - ' + err.toString());
    }
}

//      Async service call to load all notes for the quote
function LoadQuoteNotes() {
    try {
        Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetNotesForQuote',
            {
                quoteId: ObjWorkSummary.QuoteId
            },
                true,
                DisplayQuoteNotes
            );
    }
    catch (err) {
        DisplayMessage('pnlNotesDetail_0', 'Error: Unable to load teams - ' + err.toString());
    }
}

//      Async service call to load all notes for the work
function LoadWorkNotes() {
    try {
        Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetNotesForWork',
            {
                workId: ObjWorkSummary.Id
            },
                true,
                DisplayWorkNotes
            );
    }
    catch (err) {
        DisplayMessage('pnlNotesDetail_1', 'Error: Unable to load teams - ' + err.toString());
    }
}

function LoadCalendar() {
    var panel = document.getElementById('pnlCalendar');
    panel.innerHTML = '';

    $('#pnlCalendar').fullCalendar({
        header: {
            left: 'title',
            center: 'agendaDay,agendaWeek,month',
            right: 'prev,today,next'
        },
        height: 600,
        aspectRatio: 1.4,
        defaultView: 'month',
        events: GetCalendarEvents,
        dayClick: CalendarDayClickEvent,
        eventClick: CalendarClickEventHandler,
        eventResize: CalendarResizeEvent,
        viewRender: CalendarRenderView,
        editable: true,
        firstHour: 6,
        allDaySlot: false,
        allDayText: 'All Day',
        axisFormat: 'HH:mm',
        titleFormat: {
            week: 'MMMM d[ yyyy]{ &#8212;[ MMM] d yyyy}',
            day: 'dddd, dd MMMM yyyy'
        },
        columnFormat: {
            week: 'ddd dd/MM',
            day: 'dddd, dd MMMM'
        },
        eventMouseover: function (event, jsEvent, view) {
            timer = setTimeout(function () {
                var summary = Ajax_CallService(
                    'Services/Web/WorkProgramming.asmx',
                    'GetWorkSummaryForWorkPlan',
                    {
                        workPlanId: event.workPlanId
                    },
                    false);

                tooltip = '<div class="tooltiptopicevent" style="max-width: 220px; width:auto;height:auto;background:#fff;border: solid grey 1px; position:absolute;z-index:10001;padding:10px 10px 10px 10px; line-height: 100%; box-shadow: 10px 10px 5px #888888;">' + '<span style="font-size: 0.9em; font-weight: bold;">' + event.title + '</span><br/><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Total Work Value:</span><br/>' + ' + CurrentCurrency + ' + (summary.Value + summary.VatVal).toFixed(2) + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Start:</span><br/>' + event.start.format("dd-MM-yyyy hh:mm tt") + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">End:</span><br/>' + event.end.format("dd-MM-yyyy hh:mm tt") + '</div>';
                $("body").append(tooltip);
                $(this).css('z-index', 10000);
                //Set tooltip to position of mouse
                $('.tooltiptopicevent').css('top', jsEvent.pageY + 10);
                $('.tooltiptopicevent').css('left', jsEvent.pageX + 20);
                $('.tooltiptopicevent').hide().fadeIn('slow');
            }, 800);

            $(this).mousemove(function (e) {
                $('.tooltiptopicevent').fadeOut();
            });
        },
        eventMouseout: function (event, jsEvent, view) {
            clearTimeout(timer);
            $(this).css('z-index', 8);
            $('.tooltiptopicevent').fadeOut('slow', removeToolTip);
            function removeToolTip() { $('.tooltiptopicevent').remove(); }
        },
        timeFormat: '',
        slotMinutes: '60'
    });
}


/*********************************************************************************************************

                                            END SERVICE CALLS

*********************************************************************************************************/


/*********************************************************************************************************

                                            SERVICE CALLBACKS

*********************************************************************************************************/

//Async callback to set scope variable and display the work summary
function DisplayWorkSummary(result) {
    var panel = document.getElementById('pnlSummaryDetail');
    if (!result || result.Id < 0) {
        DisplayMessage('pnlErrorWork', 'Error: The ArbPro service did not return a valid work summary.');
        DisplayMessage(panel.id, 'Error: The ArbPro service did not return a valid work summary.');
    } else {
        try {
            ObjWorkSummary = result;
            //Ensure the global variables are set correctly
            QuoteId = ObjWorkSummary.QuoteId;
            WorkId = ObjWorkSummary.Id;
            //Generate the html for the panel
            //Left
            var strHtml = '';
            strHtml += '<div class="width-full"><div class="bookWorkField float-left"><label class="bookWorkLabel">Client Name</label><label class="bookWorkInput">';
            strHtml += ObjWorkSummary.ClientName;
            strHtml += '</label></br><label class="bookWorkLabel">Account No</label><label class="bookWorkInput">';
            strHtml += ObjWorkSummary.AccountNo;
            strHtml += '</label></br><label class="bookWorkLabel">Work value (' + CurrentCurrency + ')</label><label class="bookWorkInput">';
            strHtml += ObjWorkSummary.Value.format();
            strHtml += '</label></br><label class="bookWorkLabel">VAT Value (' + CurrentCurrency + ')</label><label class="bookWorkInput">';
            strHtml += ObjWorkSummary.VatVal.format();
            strHtml += '</label></br><label class="bookWorkLabel">Total Value (' + CurrentCurrency + ')</label><label class="bookWorkInput">';
            strHtml += (ObjWorkSummary.VatVal + ObjWorkSummary.Value).format();
            //Right
            strHtml += '</div><div class="bookWorkField float-right">';
            strHtml += '</label><label class="bookWorkLabel">Number of Sites</label><label class="bookWorkInput">';
            strHtml += ObjWorkSummary.JobSites.length.toString();
            strHtml += '</label></br><label class="bookWorkLabel">Number of Items</label><label class="bookWorkInput">';
            strHtml += ObjWorkSummary.WorkItems.length.toString();
            strHtml += '</label></br><label class="bookWorkLabel">Telephone</label><label class="bookWorkInput">';
            strHtml += ObjWorkSummary.Telephone;
            strHtml += '</label></br><label class="bookWorkLabel">Mobile</label><label class="bookWorkInput">';
            strHtml += ObjWorkSummary.Mobile;
            strHtml += '</label></br><label class="bookWorkLabel">Email</label><label class="bookWorkInput">';
            strHtml += '<a href="mailto:' + ObjWorkSummary.Email + '">' + ObjWorkSummary.Email + '</a>';
            strHtml += '</label></div></div></br>';
            //Bottom - Client address details
            strHtml += '</br><div class="bookWorkField width-full">'
            strHtml += '<label class="bookWorkLabel">Address:</label>';
            strHtml += '<label class="bookWorkInput" style="width: 350px">';
            strHtml += ObjWorkSummary.StreetNumber.toString();
            strHtml += ' ' + ObjWorkSummary.AddressLn1 + ', ' + ObjWorkSummary.AddressLn2 + ', ' + ObjWorkSummary.Town + ', ' + ObjWorkSummary.County + ', ' + ObjWorkSummary.PostCode;
            strHtml += '</label></div>';
            //Populate the panels inner html
            panel.innerHTML = strHtml;
        }
        catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display work summary - ' + err.toString());
        }
        DisplayScheduledPlans(result.BookedWork);
        DisplayJobSites(result.JobSites);
        DisplayTools([]);

        //NOTE: The work panel index for the first tab is 3 as this was added after initial development
        if (ReferrerSiteID > 0) {
            SwitchWorkPanel(3);
            $("#ddlWorkSite").prop('value', ReferrerSiteID);
            SelectedSiteChanged();
        } else {
            SwitchWorkPanel(3);
        }
    }
}

//Async callback to construct the team panel
function DisplayTeams(results) {

    var panel = document.getElementById('pnlWorkDetail_1');
    try {
        //Check if a valid list has been returned - if the user is here there must be at least one site
        if (!results || results.length == 0) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid list of teams.');
        } else {
            ListTeams = results;
            var strHtml = '<div class="width-full"><span style="display:inline-block;width:10%;"> Site: </span>';
            strHtml += '<select id="ddlTeams" class="jobInput" style="width:65%;" name="ddlTeams" onchange="SelectedTeamChanged();">';
            //strHtml += '<button type="button" id="btnViewSiteDetails" onclick="ShowSiteDetails();" class="button-thin float-right padded">View Details</button>';
            //Add the default value for when there is no work plan or a tem has not been assigned
            strHtml += '<option value="-1" selected="selected"> Select Team to Book Work for</option>';
            results.forEach(function (t) {
                strHtml += '<option value="' + t.Id + '">(' + t.Number + ') ' + t.Name + '</option>';
            });
            strHtml += '</select>';
            //strHtml += '<input id="btnViewSiteDetails" class="button-thin padded float-right" type="submit" style="margin-right: 10px;" onclick="ShowSiteDetails(); return false;" value="View Details" name="btnViewSiteDetails">';
            //strHtml += '<button type="button" id="btnViewSiteDetails" onclick="ShowSiteDetails();" class="button-thin float-right padded">View Details</button>';
            strHtml += '</div><br>';
            strHtml += '<div id="pnlSelectedTeamDetails" class="width-full float-left">';
            if (SelectedSite < 1) {
                strHtml += '<label class="center">Please select a site to book a team for</label></div>';
            } else {
                strHtml += '<label class="center">Please select a team to view and edit the staff for the work booking</label></div>';
            }
            strHtml += '</div>';

            panel.innerHTML = strHtml;

        }
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display team details - ' + err.toString());
    }
}

//Async callback to construct the Quote Notes panel
function DisplayQuoteNotes(results) {
    var notesPanel = document.getElementById('pnlNotesDetail_0');
    PopulateNotes(notesPanel, 0, results);
}

//Async callback to construc the work notes panel
function DisplayWorkNotes(results) {
    var notesPanel = document.getElementById('pnlNotesDetail_1');
    PopulateNotes(notesPanel, 1, results);
}


/*********************************************************************************************************

                                            END SERVICE CALLBACKS

*********************************************************************************************************/


/*********************************************************************************************************

                                            LOCAL FUNCTIONS - 
                        These are functions that have no value externally to this file
*********************************************************************************************************/

//function to initiate an Async service call to populate the required data for a notes panel
function LoadDisplayedNotesTab(selectedPanel) {
    switch (selectedPanel) {
        case 0:         // Load the notes for the quote
            LoadQuoteNotes();
            break;
        case 1:         // Load the work notes for the work plan / work record
            LoadWorkNotes();
            break;
    }
}

//Async call back function to display all current workplans for the selected work record
function DisplayScheduledPlans(results) {
    var panel = document.getElementById('pnlWorkDetail_3');
    try {
        if (!results) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid list of sites.');
        } else {

            var strHtml = '';
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="width-full searchGrid">';
            strHtml += '<tr><td colspan="2">';
            strHtml += '<p class="width-full center">Select a site below to edit the booked appointment. Alternatively, click new to add a new appointment.</p>';
            strHtml += '</td><th><button type="button" onclick="SetNewWorkPlan();" class="button-thin padded float-right">New</button></th></tr>';
            if (results.length == 0) {
                strHtml += '<tr><td>There are currently no scheduled work plans.</td></tr>';
            } else {
                strHtml += '<tr><th>Site</th><th>Team</th><th>Hours</th></tr>';//<th>Start Date</th></tr>';
                var wp = null;
                var count = results.length;
                for (var index = 0; index < count; index++) {
                    wp = results[index];
                    strHtml += '<tr onClick="ScheduleSelected(' + index.toString() + ')"><td>';
                    strHtml += wp.SiteAddress;
                    strHtml += '</td><td>';
                    strHtml += wp.TeamName;
                    strHtml += '</td><td>';
                    //Check if the number of required hours has been set
                    if (wp.TimeRequired > 0) {
                        strHtml += wp.TimeRequired.toString();
                    } else {
                        strHtml += 'Not Set';
                    }
                    strHtml += '</td></tr>';
                }
            }
            strHtml += '</table></div>';

            panel.innerHTML = strHtml;
        }
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display scheduled plan details - ' + err.toString());
    }
}


function DisplayJobSites(results) {
    var panel = document.getElementById('pnlWorkDetail_0');
    try {
        //Check if a valid list has been returned - if the user is here there must be at least one site
        if (!results || results.length == 0) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid list of sites.');
        } else {
            //Create the html for the drop down
            var strHtml = '<div class="width-full"><span style="display:inline-block;width:10%;"> Site: </span>';
            strHtml += '<select id="ddlWorkSite" class="jobInput" style="width:65%;" name="ddlWorkSite" onchange="SelectedSiteChanged();">';
            SelectedSite = -1;
            if (results.length > 1) {
                strHtml += '<option value="-1" selected="selected"> Select Site to Book Work</option>';
                results.forEach(function (js) {
                    strHtml += '<option value="' + js.Id + '">' + js.SiteString + '</option>';
                });
            } else {
                strHtml += '<option value="' + results[0].Id + '" selected="selected">' + results[0].SiteString + '</option>';
                SelectedSite = results[0].Id;
            }
            strHtml += '</select>';
            strHtml += '<button type="button" id="btnViewSiteDetails" onclick="ShowSiteDetails();" class="button-thin float-right padded">View Details</button>';
            strHtml += '</div>';
            //Create the div for the details of the site - this will be populated by SelectedSiteChanged()
            strHtml += '<div id="pnlWorkSiteDetails" class="width-full float-left"><label class="center">Please Select A Site</label></div>';
            //Add the html to the panel
            panel.innerHTML = strHtml;
            //Call the function to populate the site details if needed

            SelectedSiteChanged();
        }
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display site details - ' + err.toString());
    }
}

//Callback function to populate the Teams tab dropdown and setup the required elements for Personnel selection
function DisplayTeams(results) {
    var panel = document.getElementById('pnlWorkDetail_1');
    try {
        //Check if a valid list has been returned - if the user is here there must be at least one site
        if (!results || results.length == 0) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid list of teams.');
        } else {
            ListTeams = results;
            var strHtml = '<div class="width-full"><span style="display:inline-block;width:10%; padding:5px"> Team Name: </span>';
            strHtml += '<select id="ddlTeams" class="jobInput" style="width:65%;" name="ddlTeams" onchange="SelectedTeamChanged();">';
            //strHtml += '<button type="button" id="btnViewSiteDetails" onclick="ShowSiteDetails();" class="button-thin float-right padded">View Details</button>';
            //Add the default value for when there is no work plan or a tem has not been assigned
            strHtml += '<option value="-1" selected="selected"> Select Team to Book Work for</option>';
            results.forEach(function (t) {
                strHtml += '<option value="' + t.Id + '">(' + t.Number + ') ' + t.Name + '</option>';
            });
            strHtml += '</select>';
            strHtml += '</div><br>';
            strHtml += '<div id="pnlSelectedTeamDetails" class="width-full float-left">';
            if (SelectedSite < 1) {
                strHtml += '<label class="center">Please select a site to book a team for</label></div>';
            } else {
                strHtml += '<label class="center">Please select a team to view and edit the staff for the work booking</label></div>';
            }
            strHtml += '</div>';

            panel.innerHTML = strHtml;

        }
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display team details - ' + err.toString());
    }
}



/***** NOTES *****/
//Function to construct the html for a given panel
function PopulateNotes(notesPanel, notesPnlIndex, results) {
    try {
        if (!results) {
            DisplayMessage(notesPanel.id, 'Error: Arb Pro service did not return valid data');
        } else {
            var strHtml = '<div class="width-full float-left white-light"><button type="button" onclick="ShowNotesDialog(-1,' + notesPnlIndex + ');" class="button-thin float-right padded">Add Note</button></div>';
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid width-full">';
            if (results.length == 0) {
                strHtml += '<tr><td>No notes to display</td></tr>';
            } else {
                strHtml += '<tr class="white-light"><th>Entered By</th><th>Details</th><th>Created</th>'
                results.forEach(function (c) {
                    strHtml += '<tr>';
                    strHtml += '<td onclick="ShowNotesDialog(' + c.Id + ',' + notesPnlIndex + ');" style="padding-left: 10px;">' + c.EnteredBy + '</td><td onclick="ShowNotesDialog(' + c.Id + ',' + notesPnlIndex + ');" style="padding-left: 10px;">' + c.Text + '</td><td onclick="ShowNotesDialog(' + c.Id + ',' + notesPnlIndex + ');" style="padding-left: 10px;">' + toDateStringFromJson(c.EnteredDate, '', 'dd/mm/yyyy') + '</td>'
                    strHtml += '</tr>';
                });
            }
            strHtml += '</table>';
            strHtml += '</div>';
            notesPanel.innerHTML = strHtml;
        }
    }
    catch (err) {
        DisplayMessage(notesPanel.id, 'Error: Unable to display notes - ' + err.toString());
    }
}

/***** NAVIGATION *****/
//Function to display a work detail panel
function SwitchWorkPanel(panelIndex) {
    DisplayActivePanel('tabWork_', 'pnlWorkDetail_', panelIndex, 5);
}

//function switch notes panel
function SwitchNotesPanel(panelIndex) {
    GetWaitImageForPanel('pnlNotesDetail_' + panelIndex.toString, 'Loading Please Wait.');
    DisplayActivePanel('tabNotes_', 'pnlNotesDetail_', panelIndex, 2);
    LoadDisplayedNotesTab(panelIndex);
}

//Function to set the CSS class of a series of tabs and panels
function DisplayActivePanel(tabName, panelName, selectedIndex, pnlCount) {

    //Iterate through each panel to set the status as required
    for (var index = 0; index < pnlCount; index++) {
        var tabButton = document.getElementById(tabName + index.toString());
        var tabPanel = document.getElementById(panelName + index.toString());
        //Check if the current index is the selected index
        if (index == selectedIndex) {
            //Set the CSS class of the elements to be selected and shown
            tabButton.className = 'jobTab activeTab';
            tabPanel.className = '';
        } else {
            //Set the css class of the elements to be deselected or hidden
            tabButton.className = 'jobTab';
            tabPanel.className = 'hidden';
        }
    }
}

//Function to populate the Team panel with staff members
function DisplayStaff(results, isDefault) {
    ListSelectedStaff = results;
    var strMessage = '';
    if (isDefault) {
        strMessage = 'Default Team List';
    } else {
        strMessage = 'Selected Staff List';
    }
    var panel = document.getElementById('pnlSelectedTeamDetails');
    try {

        var strHtml = '<div class="width-full float-left white-light">';
        strHtml += '<label class="float-left bookWorkLabel">' + strMessage + '</label>';
        strHtml += '<button type="button" onclick="ShowEditStaffDialog();" class="button-thin float-right padded">Edit Assigned Staff</button></div>';
        strHtml += '<div class="float-left width-full">';
        strHtml += '<table class="width-full searchGrid"><tr><th>Name</th><th>Role</th></tr>';
        if (ListSelectedStaff.length == 0) {
            strHtml += '<tr><td colspan="2">There are no staff assigned to this team.</td></tr>';
        } else {
            ListSelectedStaff.forEach(function (s) {
                strHtml += '<tr><td>';
                strHtml += s.StaffName;
                strHtml += '</td><td>';
                strHtml += s.Task;
                strHtml += '</td></tr>';
            });
        }
        strHtml += '</table></div>';

        panel.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display staff list - ' + err.toString());
    }
}
//Function to display the currently selected tools list
function DisplayTools(results) {

    ListSelectedTools = results;
    var panel = document.getElementById('pnlWorkDetail_2');
    try {

        var strHtml = '<div class="width-full float-left white-light">';
        strHtml += '<label class="float-left bookWorkLabel">Required Tools</label>';
        strHtml += '<button type="button" onclick="ShowEditToolsDialog();" class="button-thin float-right padded">Edit Tools Required</button></div>';
        strHtml += '<div class="float-left width-full">';
        strHtml += '<table class="width-full searchGrid"';
        if (ListSelectedTools.length == 0) {
            strHtml += '<tr><td colspan="2">There are no tools assigned to this schedule.</td></tr>';
        } else {
            strHtml += '><tr><th>Name</th><th>Number Required</th></tr>';
            ListSelectedTools.forEach(function (t) {
                if (t.Number > 0) {
                    strHtml += '<tr><td>';
                    strHtml += t.Item;
                    strHtml += '</td><td>';
                    strHtml += t.Number;
                    strHtml += '</td></tr>';
                }
            });
        }
        strHtml += '</table></div>';

        panel.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display tools list - ' + err.toString());
    }
}

function SetNewWorkPlan() {
    SelectedWorkPlanId = -1;
    SwitchWorkPanel(0);
}

//Function to populate tabs accordingly if a work schedule is selected
function ScheduleSelected(planIndex) {

    var plan = ObjWorkSummary.BookedWork[planIndex];
    SelectedWorkPlanId = plan.Id;
    SelectedPlan = plan;
    SetSelectedSite(plan.JobSiteId);
    var teamSet = SetSelectedTeam(plan.TeamId);
    DisplayTools(plan.Tools);
    DisplayStaff(plan.Personnel);

    //Diplsy the site tab so the user knows that something has changed
    if (teamSet) {
        SwitchWorkPanel(0);
    } else {
        SwitchWorkPanel(1);
    }
}

/***** DIALOGS *****/
//Event handler for client side click to view site details
//This function displays the client site dialog
function ShowSiteDetails() {
    var clientSiteId = GetClientSiteId(SelectedSite);
    if (clientSiteId > 0) {
        Dialog_OpenPage(
            'Site Details',
            'Dialogs/Site.aspx?ID=' + clientSiteId.toString(),
            'content',
            700,
            500,
            true,
            {
                'Cancel': {
                    text: 'Cancel',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/Site.js?V=3.23.5',
            function () {
                Site_Load();
            });
    } else {
        alert("Error: Unable to display site data - Invalid site Id");
    }
}
//function to display the add/edit notes dialog
function ShowNotesDialog(notesId, panelIndex) {
    try {

        var recId = -1;
        var workStage = -1;
        //Check which record type the note is for
        if (panelIndex == 0) {
            recId = QuoteId;
            workStage = 1;
        }
        else {
            recId = WorkId;
            workStage = 2;
        }

        Dialog_OpenPage('Note',
            'Dialogs/Notes.aspx?',
            'content',
            500,
            500,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        Note_Save();
                        SwitchNotesPanel(panelIndex);
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/Notes.js?V=3.23.5',
            function () {
                Notes_Load(notesId, recId, workStage);
            });
    }
    catch (Err) {
        DisplayMessage('pnlNotesDetail_' + panelIndex.toString(), 'Error: Unable to display notes dialog');
    }
}
//function to load a dialog to enable the user to select the staff to be assigned to a work plan
function ShowEditStaffDialog() {
    try {
        Dialog_OpenPage('Select Staff',
            'Dialogs/WorkPlanStaff.aspx?',
            'content',
            500,
            500,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        var list = WorkPlanStaff_Save();
                        if (list) {
                            DisplayStaff(list, false);
                            $(this).dialog('close');
                        }
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/WorkPlanStaff.js?V=3.23.5',
            function () {
                WorkPlanStaff_Load(ListSelectedStaff);
            });
    }
    catch (Err) {
        DisplayMessage('pnlNotesDetail_' + panelIndex.toString(), 'Error: Unable to display notes dialog');
    }
}
//function to load a dialog to enable the user to select the tools to be assigned to a work plan
function ShowEditToolsDialog() {
    try {
        Dialog_OpenPage('Select Tools',
            'Dialogs/WorkPlanTools.aspx?',
            'content',
            800,
            500,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        var list = WorkPlanTool_Save()
                        if (list) {
                            SaveWorkToolsList(list);
                            DisplayTools(list, false);
                            $(this).dialog('close');
                        }
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/WorkPlanTools.js?V=3.23.5',
            function () {
                WorkPlanTool_Load(ListSelectedTools);
            });
    }
    catch (Err) {
        DisplayMessage('pnlErrorWork', 'Error: Unable to display tools dialog');
    }
}

function SelectedSiteChanged() {

    var ddl = document.getElementById('ddlWorkSite');
    var panel = document.getElementById('pnlWorkSiteDetails');
    //Update the selected site and set the view button as required
    SelectedSite = ddl[ddl.selectedIndex].value;
    document.getElementById('btnViewSiteDetails').disabled = SelectedSite < 1;
    //Check that the elements have been populated
    if (!ddl || !panel) {
        //This function will not be able to continue if here
        DisplayMessage('pnlErrorWork', 'Error: Unable to display site details, page is invalid');
    } else {
        try {
            var strHtml = '<br>';

            //Create the table to display the work items for the site
            strHtml += '<table class="searchGrid small width-full"><col width="5%"><col width="20%"><col width="55%"><col width="10%"><col width="10%">';
            var SiteVatRates = new Array();
            ObjWorkSummary.WorkItems.forEach(function (wi) {
                //Iterate through each item to see if it is for the current site
                if (wi.JobSiteId == SelectedSite) {
                    var found = false;
                    //Evaluate the VAT rates for the Site and add if required 
                    SiteVatRates.forEach(function (vr) {
                        if (vr.rate == wi.VatRate) {
                            vr.value += wi.Value;
                            found = true;
                        }
                    });

                    if (!found)
                        SiteVatRates.push({ rate: wi.VatRate, value: wi.Value });

                    //Display the item.
                    strHtml += '<tr>';
                    strHtml += '<td style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
                    strHtml += '<td style="vertical-align: top;">' + wi.Name + '</td>';
                    strHtml += '<td >' + wi.Description + '</td>';
                    strHtml += '<td style="text-align: center" onclick="event.preventDefault(); OpenImage(2, ' + wi.Id + ');"><a href="#">Image</a></td>';
                    strHtml += '<td style="text-align: right; padding-right: 3px; padding-left: 3px; vertical-align: bottom;"(' + wi.Id + ')">' + (wi.Value > 0 ? ' + CurrentCurrency + ' + wi.Value.format() : '') + '</td>';
                    strHtml += '</tr>';
                }
            });
            strHtml += '</table>';
            strHtml += GetSiteTotalHtml(SiteVatRates, 0, 'Site');

            panel.innerHTML = strHtml;
        } catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display site details - ' + err.toString());
        }
    }
}

//Event for when the selected team is changed
function SelectedTeamChanged() {
    var strMessage = '';
    if (!ListSelectedStaff || ListSelectedStaff.length == 0) {
        strMessage = 'Default Team List';
        var ddlTeam = document.getElementById('ddlTeams');
        var teamId = ddlTeam.options[ddlTeam.selectedIndex].value;
        ListSelectedStaff = GetTeamForId(teamId).Personnel;
    } else {
        strMessage = 'Selected Staff List';
    }
    var panel = document.getElementById('pnlSelectedTeamDetails');
    try {

        var strHtml = '<div class="width-full float-left white-light">';
        strHtml += '<label class="float-left bookWorkLabel">' + strMessage + '</label>';
        strHtml += '<button type="button" onclick="ShowEditStaffDialog();" class="button-thin float-right padded">Edit Assigned Staff</button></div>';
        strHtml += '<div class="float-left width-full">';
        strHtml += '<table class="width-full searchGrid"><tr><th>Name</th><th>Role</th></tr>';
        if (ListSelectedStaff.length == 0) {
            strHtml += '<tr><td colspan="2">There are no staff assigned to this team.</td></tr>';
        } else {
            ListSelectedStaff.forEach(function (s) {
                strHtml += '<tr><td>';
                strHtml += s.StaffName;
                strHtml += '</td><td>';
                strHtml += s.Task;
                strHtml += '</td></tr>';
            });
        }
        strHtml += '</table></div>';

        panel.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display staff list - ' + err.toString());
    }
}

function ClosePage(curTab) {
    var linkUrl = '/Job.aspx?JobID=' + document.getElementById('hfJobID').value
    linkUrl += '&QuoteID=' + QuoteId.toString();

    if (typeof (curTab) != typeof (undefined) && curTab != null) {
        linkUrl += "&curTab=" + parseInt(curTab);
    }

    window.location = linkUrl;
}
/***** HELPER FUNCTIONS *****/
//Function to get the site name from the id
function GetSiteNameForSiteId(siteId) {
    var count = ObjWorkSummary.JobSites.length;
    for (var index = 0; index < count; index++) {
        if (ObjWorkSummary.JobSites[index].Id == siteId) {
            return ObjWorkSummary.JobSites[index].SiteString;
        }
    }
    //Should not get here
    return 'Unknown';
}

//Function to get the team name from the team Id
function GetTeamNameForTeamId(teamId) {
    var count = ListTeams.length;
    for (var index = 0; index < count; index++) {
        if (ListTeams[index].Id == teamId) {
            return ListTeams[index].Name;
        }
    }
    //Should not get here
    return 'Unknown';
}

//Function to get the team object from the Id
function GetTeamForId(teamId) {
    var count = ListTeams.length;
    for (var index = 0; index < count; index++) {
        if (ListTeams[index].Id == teamId) {
            return ListTeams[index];
        }
    }
    //Should not get here
    return null;
}

//Function to get the team object that is linked to the selected option in the team drop down
function GetSelectedTeam() {
    var ddl = document.getElementById('ddlTeams');
    var team = GetTeamForId(ddl.options[ddl.selectedIndex].value);
    return team;
}

//Helper function to get the client site id from a job site id
function GetClientSiteId(jobSiteId) {
    var retVal = -1;
    var siteCount = ObjWorkSummary.JobSites.length;
    //Iterate through all the work plans to see if there is one for the job site
    for (var index = 0; index < siteCount; index++) {
        if (ObjWorkSummary.JobSites[index].Id == jobSiteId) {
            retVal = ObjWorkSummary.JobSites[index].SiteId;
            break;
        }
    }
    return retVal;
}

//Function to get the JobSite object that relates to the selected option in the site drop down
function GetSelectedSite() {
    var ddl = document.getElementById('ddlWorkSite');
    var siteId = ddl.options[ddl.selectedIndex].value;

    var count = ObjWorkSummary.JobSites.length;
    for (var index = 0; index < count; index++) {
        if (ObjWorkSummary.JobSites[index].Id == siteId) {
            return ObjWorkSummary.JobSites[index];
        }
    }
    //Should not get here
    return null;
}

//function to set the select option for the site list
function SetSelectedSite(jsId) {
    var ddl = document.getElementById('ddlWorkSite');
    var count = ddl.options.length;
    for (var index = 0; index < count; index++) {
        if (ddl.options[index].value == jsId) {
            ddl.selectedIndex = index;
            break;
        }
    }
    //Force the site panel to update
    SelectedSiteChanged();
}

//function to set the select option for the team list
function SetSelectedTeam(teamId) {
    if (teamId > 0) {
        var ddl = document.getElementById('ddlTeams');
        var count = ddl.options.length;
        for (var index = 0; index < count; index++) {
            if (ddl.options[index].value == teamId) {
                ddl.selectedIndex = index;
                break;
            }
        }
        //Force the team panel to update
        SelectedTeamChanged();
        return true;
    }
    else {
        return false;
    }
}

//Function to get the appointment start date for a work plan
function GetStartDateForWorkPlan(planId) {
    //TODO: IMPLEMENT SEARCH FOR STARTDATE
    return 'TODO!!!';
}
/*********************************************************************************************************

                                            END LOCAL FUNCTIONS

*********************************************************************************************************/


/*********************************************************************************************************

                                            CALENDAR FUNCTIONS

    The following functions replace the inline code for the calendar function. This is intended to increase 
    code reuse and improve error capturing and debug functionality.

*********************************************************************************************************/
//Function to get the events to be displayed on the calendar
function GetCalendarEvents(startDate, endDate, CallbackFunction) {
    try {
        $('#pnlCalendar').fullCalendar('renderEvent', editEvent);

        var EventList = new Array();
        var Events;
        var filter = false;
        var ddl = document.getElementById('ddlTeams');
        if (ddl && filter) {
            editEvent.title = ddl.options[ddl.selectedIndex].text;

            Events = Ajax_CallService(
                'Services/web/Calendar.asmx',
                'GetWorkBookingsForTeam',
                {
                    TeamID: ddl.options[ddl.selectedIndex].value,
                    Start: startDate,
                    End: endDate,
                },
                false);
        } else {
            Events = Ajax_CallService(
                'Services/web/Calendar.asmx',
                'GetWorkBookings',
                {
                    Start: startDate,
                    End: endDate,
                },
                false);
        }

        var today = new Date();

        Events.forEach(function (ev) {
            EventList.push({
                id: ev.Id,
                workPlanId: ev.WorkPlanId,
                data: ev,
                title: ev.CalendarString,
                allDay: false,
                start: new Date(parseInt(ev.StartDateTime.substr(6))),
                end: new Date(parseInt(ev.EndDateTime.substr(6))),
                className: 'cal-event-work',
                editable: true
            });
        });

        CallbackFunction(EventList);
    } catch (err) {
        DisplayMessage('pnlErrorWork', 'Error: Unable to communicate with ArbPro service -' + err.toString());
    }
}

//Function to handle a calendar day event click
function CalendarDayClickEvent(date, allDay, jsEvent, view) {
    try {
        //Get the elements required for validation
        var ddl = document.getElementById('ddlTeams');
        var ddlSite = document.getElementById('ddlWorkSite');

        //Get the ids for the from the html elements
        var teamId = ddl.options[ddl.selectedIndex].value;
        var siteId = ddlSite.options[ddlSite.selectedIndex].value;
        //Get the required text from the elements
        var teamName = ddl.options[ddl.selectedIndex].text;
        var siteName = ddlSite.options[ddlSite.selectedIndex].text;
        //Check if the procress can continue
        if (siteId > 0 && teamId > 0 && ListSelectedTools.length > 0) {
            //The day has been clicked and information is available to enable the creation of a
            //work plan and an appointment
            NewAppointment(date, false);

        } else {
            DisplayMessageDialog('Booking Work', GetInfoText());
        }
    } catch (err) {
        DisplayMessage('pnlErrorWork', 'Error: Unable to handle Calendar day click - ' + err.toString());
    }
}

//Function to handle the calendar event click
function CalendarClickEventHandler(event, jsEvent, view) {
    try {

        if (event.editable) {
            EditAppointment(event.id);
        } else {
            //The work has been started or the appointment is histoic and so is not editable
            ShowAlertMessage('The appointment cannot be edited at this time. This may be due to the appointment being historic or the work has been started.');
        }
    } catch (err) {
        DisplayMessage('pnlErrorWork', 'Error: Unable to handle Calendar event click - ' + err.toString());
    }
}

//Function to handle the resize of an event on the Calendar
function CalendarResizeEvent(event, delta, revertFunction) {
    try {
        revertFunction();
    } catch (err) {
        DisplayMessage('pnlErrorWork', 'Error: Unable to revert Calendar event - ' + err.toString());
    }
}

//Function to render the calendar view
function CalendarRenderView(view, element) {
    $('#pnlCalendar').fullCalendar('refetchEvents');
    $('#pnlCalendar').fullCalendar('updateEvent', editEvent);
}

function NewAppointment(date, allDay) {
    Dialog_OpenPage(
                'Book Work',
                'Dialogs/CalendarBookWork.aspx?',
                'content',
                500,
                500,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (CalendarBookWork_Save()) {
                                //Force the page to reload
                                $(this).dialog('close');
                                BookWork_Load();
                            }
                        }
                    },
                    'Close': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/CalendarBookWork.js?V=3.23.5',
                function () {
                    //Check to see if a plan has been selected
                    if (SelectedWorkPlanId < 1) {
                        CalendarBookWork_New(date, QuoteId, WorkId, GetSelectedSite(), GetSelectedTeam(), ListSelectedTools, ListSelectedStaff);
                    } else {
                        CalendarBookWork_Edit(date, SelectedPlan, GetSelectedSite(), GetSelectedTeam(), ListSelectedTools, ListSelectedStaff);
                    }
                }
                );
}

//Function to enable a user to edit a work plan via click on an appointment
function EditAppointment(appointmentId) {
    Dialog_OpenPage(
                'Edit Booked Work',
                'Dialogs/CalendarBookWork.aspx?',
                'content',
                500,
                550,
                true,
                {
                    'Delete': {
                        text: 'Delete',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (confirm('Do you want to delete this booked work?', 'Confirm deletion')) {
                                if (CalendarBookWork_Delete()) {
                                    //Force the page to reload
                                    $(this).dialog('close');
                                    BookWork_Load();
                                }
                            }
                        }
                    },
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (CalendarBookWork_Save()) {
                                //Force the page to reload
                                $(this).dialog('close');
                                BookWork_Load();
                            }
                        }
                    },
                    'Close': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/CalendarBookWork.js?V=3.23.5',
                function () {
                    CalendarBookWork_EditAppointment(appointmentId);
                }
                );
}

function EditWorkPlan() {
    Dialog_OpenPage(
                'Edit Booked Work',
                'Dialogs/CalendarBookWork.aspx?',
                'content',
                500,
                500,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (CalendarBookWork_Save()) {
                                //Force the page to reload
                                $(this).dialog('close');
                                BookWork_Load();
                            }
                        }
                    },
                    'Close': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/CalendarBookWork.js?V=3.23.5',
                function () {
                    CalendarBookWork_EditAppointment(appointmentId);
                }
                );
}

//function to save a list of work tools
function SaveWorkToolsList(list) {
    try
    {
        Ajax_CallService(
         'Services/web/WorkProgramming.asmx',
         'UpdateWorkTools',
             {
                 workPlanId: SelectedWorkPlanId,
                 tools: list
             },
        false);
    }
    catch (ex) {
    }
}

function LoadInfo() {
    var strHtml = GetInfoText();
    document.getElementById('pnlWorkDetail_4').innerHTML = strHtml;
}

function GetInfoText() {
    var strHtml;
    strHtml = '<div style="margin-left:20px;">';
    strHtml += '<label>To book this work follow these steps in the order described:</label><br/><br/>'
    strHtml += '<label>1. Select the schedule tab and click new or the site name.<font color="red"> (Scheduled tab)</font></label><br/>';
    strHtml += '<label>2. Pick a team and allocate staff for the job.<font color="red"> (Team tab)</font></label><br/>';
    strHtml += '<label>3. Check or add tools to the work schedule.<font color="red"> (Tools tab)</font></label><br/>';
    strHtml += '<label>4. Book the job using the calendar.<font color="red"> (Click on a date)</font></label><br/><br/>';
    strHtml += '<label>Ensure you enter dates and times for the job to start and finish. Please watch the training videos for further instructions.</label>';
    strHtml += '</div>';
    return strHtml;
}


//,
//eventResize: function (event, delta, revertFunc) {
//    revertFunc();
//},
//viewRender: function (view, element) {
//    $('#pnlCalendar').fullCalendar('refetchEvents');
//    $('#pnlCalendar').fullCalendar('updateEvent', editEvent);
//},
//editable: true,
//firstHour: 6,
//allDaySlot: false,
//allDayText: 'All Day',
//axisFormat: 'HH:mm',
//titleFormat: {
//    week: 'MMMM d[ yyyy]{ &#8212;[ MMM] d yyyy}',
//    day: 'dddd, dd MMMM yyyy'
//},
//columnFormat: {
//    week: 'ddd dd/MM',
//    day: 'dddd, dd MMMM'
//},
//timeFormat: '',
//slotMinutes: '60'
//});

//}

