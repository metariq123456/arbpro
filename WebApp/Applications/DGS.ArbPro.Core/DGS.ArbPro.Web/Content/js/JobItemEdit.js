﻿var Stage = 'Quote';
var ItemID = -1;
var CurrencyChoice = localStorage.getItem("CurrencyChoice");


function JobItemEdit_PageLoad(iItemID, lockedFields) {
    Stage = document.getElementById('hfStage').value;
    ItemID = iItemID;

    AutoComplete('txtNewItemTreeName', 'ListTreeNames');
    AutoComplete('txtNewItemDescription', 'ListWorkDescriptions');

    var Item = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'Get' + Stage + 'Item',
        {
            ItemID: ItemID
        },
        false);
    var orderId = document.getElementById('txtNewItemOrderId');
    var txtCode = document.getElementById('txtNewItemTreeNo');
    var txtName = document.getElementById('txtNewItemTreeName');
    var txtDescription = document.getElementById('txtNewItemDescription');
    var txtValue = document.getElementById('txtNewItemValue');
    var txtVatRate = document.getElementById('txtNewItemVatRate');
    var txtLabelValue = document.getElementById('Label1');
    txtLabelValue = CurrencyChoice;

    if (typeof (lockedFields) != typeof (undefined) && lockedFields != null) {
        $(lockedFields).each(function (index, field) {
            $(field).prop("disabled", true);
        });
    }

    orderId.value = Item.OrderId;
    txtCode.value = Item.Code;
    txtName.value = Item.Name;
    txtDescription.value = Item.Description;
    txtValue.value = Item.Value;
    txtVatRate.value = Item.VatRate;
}

function SaveJobEditItem() {
    var orderId = document.getElementById('txtNewItemOrderId');
    var txtCode = document.getElementById('txtNewItemTreeNo');
    var txtName = document.getElementById('txtNewItemTreeName');
    var txtDescription = document.getElementById('txtNewItemDescription');
    var txtValue = document.getElementById('txtNewItemValue');
    var txtVatRate = document.getElementById('txtNewItemVatRate');

    Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'Save' + Stage + 'Item',
        {
            OrderId: orderId.value,
            ItemID: ItemID,
            Code: txtCode.value,
            Name: txtName.value,
            Description: txtDescription.value,
            Value: txtValue.value,
            VatRate: txtVatRate.value
        },
        false);
}
