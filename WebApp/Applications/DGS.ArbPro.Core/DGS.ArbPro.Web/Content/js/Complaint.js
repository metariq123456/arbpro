﻿var ComplaintObj = null;
var DlgComplaint_CurTab = 0;
//Initial call to load a complaint and populate the dialog
function Complaint_Load(complaintId) {
    try {
        Ajax_CallService(
        'Services/Web/Company.asmx',
        'GetComplaint',
        {
            id: complaintId
        },
        true,
        DisplayComplaintInfo
       );
    }
    catch (err) {
        DisplayMessage('pnlComplaintDlg', 'Error: Unable to comunicate with the ArbPro service');
    }

}

//Function to save a complaint to the database
function Complaint_Save() {
    try {
        var title = document.getElementById('txtCmplTitle').value;
        var firstname = document.getElementById('txtCmplFirstname').value;
        var surname = document.getElementById('txtCmplSurname').value;
        var addLn1 = document.getElementById('txtCmplAddressLn1').value;
        var addLn2 = document.getElementById('txtCmplAddressLn2').value;
        var town = document.getElementById('txtCmplTown').value;
        var county = document.getElementById('txtCmplCounty').value;
        var po = document.getElementById('txtCmplPostCode').value;
        var tele = document.getElementById('txtCmplTelephone').value;
        var mob = document.getElementById('txtCmplMobile').value;
        var email = document.getElementById('txtCmplEmail').value;
        var prefContact = parseInt(document.getElementById('txtCmplContact').value);//If blank or text, this breaks the saving process
        var dateComplaint = document.getElementById('txtCmplDateOfComplaint').value;
        var quoteRef = document.getElementById('txtCmplQuoteRef').value;
        var dateEntered = document.getElementById('txtCmplDateEntered').value;
        var detail = document.getElementById('txtCmplDetail').value;
        var enteredBy = document.getElementById('txtCmplEnteredBy').value;
        var assignedTo = document.getElementById('txtCmplAssignedTo').value;
        var visitedBy = document.getElementById('txtCmplVisitedBy').value;
        var action = document.getElementById('txtCmplAction').value;
        var dateRectified = document.getElementById('txtCmplDateRectified').value;
        var type = document.getElementById('txtCmplType').value;
        var closed = false;
        if (document.getElementById('ddlClosed').selectedIndex == 1) {
            closed = true;
        }

        var dLocalisation = 'en_GB';

        ComplaintObj.Title = title;
        ComplaintObj.Firstname = firstname;
        ComplaintObj.Surname = surname;
        ComplaintObj.AddressLn1 = addLn1;
        ComplaintObj.AddressLn2= addLn2;
        ComplaintObj.Town = town;
        ComplaintObj.County = county;
        ComplaintObj.PostCode = po;
        ComplaintObj.Telephone = tele;
        ComplaintObj.Mobile = mob;
        ComplaintObj.Email = email;
        ComplaintObj.ContactMethod = prefContact;
        ComplaintObj.DateOfComplaint = ParseLocalDate(dateComplaint, dLocalisation);
        ComplaintObj.AssociatedQuoteRef = quoteRef;
        
        ComplaintObj.DateEntered = ParseLocalDate(dateEntered, dLocalisation);
        ComplaintObj.Details = detail;
        ComplaintObj.EnteredBy = enteredBy;
        ComplaintObj.AllocattedTo = assignedTo;
        ComplaintObj.SiteVisitedBy = action;
        ComplaintObj.ActionTaken = action;
        ComplaintObj.ComplaintType = type;
        ComplaintObj.DateRectified = ParseLocalDate(dateRectified, dLocalisation);
        ComplaintObj.Closed = closed;
        ComplaintObj.DateClosed = ParseLocalDate('01/01/1900', dLocalisation);;

        var recordId = Ajax_CallService(
            'Services/Web/Company.asmx',
            'SaveComplaint',
            {
                complaint: ComplaintObj
            },
            false,
            null
        );

        UpdateComplaintTotal();
        return recordId;
    }
    catch (err) {
        DisplayMessage('pnlComplaintDlg', 'Error: Unable to comunicate with the ArbPro service');
    }
}

function DisplayComplaintInfo(result) {
    try {
        if (!result) {
            DisplayMessage('pnlComplaintDlg', 'Error: Invalid data return from ArbPro service');
        } else {
            ComplaintObj = result;
            PopulateComplainantTab();
            PopulateComplaintTab();
            PopulateResloutionTab();
            //Set auto complate pick item lists
            AutoComplete('txtCmplTitle', 'ListTitles');
            AutoComplete('txtCmplTown', 'ListTowns');
            AutoComplete('txtCmplCounty', 'ListCounties');
            AutoComplete('txtCmplType', 'ListComplaintType');
            //Set up the date pickers
            $('#txtCmplDateOfComplaint').datetimepicker({ format: 'd/m/Y', timepicker: false });
            $('#txtCmplDateRectified').datetimepicker({ format: 'd/m/Y', timepicker: false });
        }
    }
    catch (err) {
        DisplayMessage('pnlComplaintDlg', 'Error: Unable to display complaint details');
    }
}

function PopulateComplainantTab() {
    var strHtml = '';
    if (ComplaintObj.Id < 1) {
        strHtml += '<input id="txtCmplTitle" class="complaintField" type="text" placeholder="Title" title="Title" name="txtCmplName"/>';
        strHtml += '<input id="txtCmplFirstname" class="complaintField" type="text" placeholder="Firstname" title="Staff name" name="txtCmplName"/>';
        strHtml += '<input id="txtCmplSurname" class="complaintField" type="text" placeholder="Surname" title="Surname" name="txtCmplName"/>';
        strHtml += '<input id="txtCmplAddressLn1" class="complaintField" type="text" placeholder="Address Line 1" title="Address Line 1" name="txtCmplAddressLn1"/>';
        strHtml += '<input id="txtCmplAddressLn2" class="complaintField" type="text" placeholder="Address Line 2" title="Address Line 2" name="txtCmplAddressLn2"/>';
        strHtml += '<input id="txtCmplTown" class="complaintField" type="text" placeholder="Town" title="Town" name="txtCmplTown"/>';
        strHtml += '<input id="txtCmplCounty" class="complaintField" type="text" placeholder="County" title="County" name="txtCmplCounty"/>';
        strHtml += '<input id="txtCmplPostCode" class="complaintField" type="text" placeholder="Post Code" title="Post Code" name="txtCmplPostCode"/>';
        strHtml += '<input id="txtCmplTelephone" class="complaintField" type="text" placeholder="Telephone" title="Telephone" name="txtCmplTelephone"/>';
        strHtml += '<input id="txtCmplMobile" class="complaintField" type="text" placeholder="Mobile" title="Mobile" name="txtCmplMobile"/>';
        strHtml += '<input id="txtCmplEmail" class="complaintField" type="text" placeholder="Email" title="Email" name="txtCmplEmail"/>';
        strHtml += '<select id="txtCmplContact" class="complaintField">';
        strHtml += '<option value="" disabled select>Preferred Contact Method</option>';
        strHtml += '<option value="0">None</option>';
        strHtml += '<option value="1">Telephone</option>';
        strHtml += '<option value="2">Mobile</option>';
        strHtml += '<option value="3">Email</option>';
        strHtml += '<option value="4">Letter</option>';
        strHtml += '</select>';
    } else {
        strHtml += '<input id="txtCmplTitle" class="complaintField" type="text" placeholder="Title" title="Title" name="txtCmplName" value="' + ComplaintObj.Title + '"/>';
        strHtml += '<input id="txtCmplFirstname" class="complaintField" type="text" placeholder="Firstname" title="Firstname" name="txtCmplFirstname" value="' + ComplaintObj.Firstname + '"/>';
        strHtml += '<input id="txtCmplSurname" class="complaintField" type="text" placeholder="Surname" title="Surname" name="txtCmplName" value="' + ComplaintObj.Surname + '"/>';
        strHtml += '<input id="txtCmplAddressLn1" class="complaintField" type="text" placeholder="Address Line 1" title="Address Line 1" name="txtCmplAddressLn1" value="' + ComplaintObj.AddressLn1 + '"/>';
        strHtml += '<input id="txtCmplAddressLn2" class="complaintField" type="text" placeholder="Address Line 2" title="Address Line 2" name="txtCmplAddressLn2" value="' + ComplaintObj.AddressLn2 + '"/>';
        strHtml += '<input id="txtCmplTown" class="complaintField" type="text" placeholder="Town" title="Town" name="txtCmplTown" value="' + ComplaintObj.Town + '"/>';
        strHtml += '<input id="txtCmplCounty" class="complaintField" type="text" placeholder="County" title="County" name="txtCmplCounty" value="' + ComplaintObj.County + '"/>';
        strHtml += '<input id="txtCmplPostCode" class="complaintField" type="text" placeholder="Post Code" title="Post Code" name="txtCmplPostCode" value="' + ComplaintObj.PostCode + '"/>';
        strHtml += '<input id="txtCmplTelephone" class="complaintField" type="text" placeholder="Telephone" title="Telephone" name="txtCmplTelephone" value="' + ComplaintObj.Telephone + '"/>';
        strHtml += '<input id="txtCmplMobile" class="complaintField" type="text" placeholder="Mobile" title="Mobile" name="txtCmplMobile" value="' + ComplaintObj.Mobile + '"/>';
        strHtml += '<input id="txtCmplEmail" class="complaintField" type="text" placeholder="Email" title="Email" name="txtCmplEmail" value="' + ComplaintObj.Email + '"/>';
        strHtml += '<select id="txtCmplContact" class="complaintField">';
        strHtml += '<option value="" disabled select>Preferred Contact Method</option>';
        strHtml += '<option value="0">None</option>';
        strHtml += '<option value="1">Telephone</option>';
        strHtml += '<option value="2">Mobile</option>';
        strHtml += '<option value="3">Email</option>';
        strHtml += '<option value="4">Letter</option>';
        strHtml += '</select>';
        //strHtml += '<input id="txtCmplContact" class="complaintField" type="text" placeholder="Preffered Contact Method" title="Preffered Contact Method" name="txtCmplContact" value="' + ComplaintObj.ContactMethod + '"/>';
    }
    document.getElementById('pnlComplaintDlgDetail_0').innerHTML = strHtml;
    document.getElementById('txtCmplContact').value = ComplaintObj.ContactMethod;
}

function PopulateComplaintTab() {
    var strHtml = '';
    if (ComplaintObj.Id < 1) {
        strHtml += '<input id="txtCmplDateOfComplaint" class="complaintField" type="text" placeholder="Date of Complaint" title="Date of Complaint" name="txtCmplDateOfComplaint"/>';
        strHtml += '<input id="txtCmplType" class="complaintField" type="text" placeholder="Type of complaint" title="Type of complaint" name="txtCmplType" />';
        strHtml += '<input id="txtCmplQuoteRef" class="complaintField" type="text" placeholder="Quote Reference" title="Associated Quote Reference" name="txtCmplQuoteRef" />';
        strHtml += '<textarea id="txtCmplDetail" class="complaintField" rows="13" placeholder="Complaint Detail" title="Complaint Detail" name="txtCmplDetail"></textarea>';
        strHtml += '<input id="txtCmplEnteredBy" class="complaintField" type="text" placeholder="Entered by staff name" title="Entered by staff name" name="txtCmplEnteredBy"/>';
        strHtml += '<input id="txtCmplAssignedTo" class="complaintField" type="text" placeholder="Assigned staff name" title="Assigned staff name" name="txtCmplAssignedTo" />';
        strHtml += '<input id="txtCmplDateEntered" class="hidden" type="text" placeholder="Date Complaint Entered" title="Date Entered" name="txtCmplDateEntered" />';
    } else {
        
        strHtml += '<input id="txtCmplDateOfComplaint" class="complaintField" type="text" placeholder="Date of Complaint" title="Date of Complaint" name="txtCmplDateOfComplaint" value="' + toFormattedDateStringFromJson(ComplaintObj.DateOfComplaint, '', 'dd/mm/yyyy') + '"/>';
        strHtml += '<input id="txtCmplType" class="complaintField" type="text" placeholder="Type of complaint" title="Type of complaint" name="txtCmplType" value="' + ComplaintObj.ComplaintType + '"/>';
        strHtml += '<input id="txtCmplQuoteRef" class="complaintField" type="text" placeholder="Quote Reference" title="Associated Quote Reference" name="txtCmplQuoteRef" value="' + ComplaintObj.AssociatedQuoteRef + '"/>';
        strHtml += '<input id="txtCmplDateEntered" class="complaintField" type="text" placeholder="Date Complaint Entered" title="Date Entered" name="txtCmplDateEntered" value="' + toFormattedDateStringFromJson(ComplaintObj.DateEntered, '', 'dd/mm/yyyy') + '"  disabled/>';
        strHtml += '<textarea id="txtCmplDetail" class="complaintField" rows="12" placeholder="Complaint Detail" title="Complaint Detail" name="txtCmplDetail">' + ComplaintObj.Details + '</textarea>';
        strHtml += '<input id="txtCmplEnteredBy" class="complaintField" type="text" placeholder="Entered by staff name" title="Entered by staff name" name="txtCmplEnteredBy" value="' + ComplaintObj.EnteredBy + '"/>';
        strHtml += '<input id="txtCmplAssignedTo" class="complaintField" type="text" placeholder="Allocated to staff name" title="Allocated to staff name" name="txtCmplAssignedTo" value="' + ComplaintObj.AllocattedTo + '"/>';
    }
    document.getElementById('pnlComplaintDlgDetail_1').innerHTML = strHtml;
}

function PopulateResloutionTab() {
    var strHtml = '';
    if (ComplaintObj.Id < 1) {
        strHtml += '<input id="txtCmplVisitedBy" class="complaintField" type="text" placeholder="Site visit Staff Name" title="Site visit made by staff name" name="txtCmplVisitedBy"/>';
        strHtml += '<textarea id="txtCmplAction" class="complaintField" rows="15" placeholder="Action Taken" title="Action Taken" name="txtCmplAction"></textarea>';
        strHtml += '<input id="txtCmplDateRectified" class="complaintField" type="text" placeholder="Date Complaint Rectified" title="Date Complaint Rectified" name="txtCmplDateRectified"/>';
        strHtml += '<select id="ddlClosed" title="Closed" class="complaintField">';
        strHtml += '<option value="0" selected="selected">No</option>';
        strHtml += '<option value="1">Yes</option>';
        strHtml += '</select>';
    } else {
        strHtml += '<input id="txtCmplVisitedBy" class="complaintField" type="text" placeholder="Site visit Staff Name" title="Site visit made by staff name" name="txtCmplVisitedBy" value="' + ComplaintObj.SiteVisitedBy + '"/>';
        strHtml += '<textarea id="txtCmplAction" class="complaintField" rows="15" placeholder="Action Taken" title="Action Taken" name="txtCmplAction">' + ComplaintObj.ActionTaken + '</textarea>';
        strHtml += '<input id="txtCmplDateRectified" class="complaintField" type="text" placeholder="Date Complaint Rectified" title="Date Complaint Rectified" name="txtCmplDateRectified" value="' + toFormattedDateStringFromJson(ComplaintObj.DateRectified, '', 'dd/mm/yyyy') + '"/>';
        strHtml += '<select id="ddlClosed" title="Closed" class="complaintField">';
        if(ComplaintObj.Closed == false){
            strHtml += '<option value="0" selected="selected">No</option>';
            strHtml += '<option value="1">Yes</option>';
            strHtml += '</select>';
        } else {
            strHtml += '<option value="0">No</option>';
            strHtml += '<option value="1" selected="selected">Yes</option>';
            strHtml += '</select>';
            strHtml += '<input id="txtCmplDateOfClose" class="complaintField" type="text" placeholder="Date Complaint Closed" title="Date Complaint Closed" name="txtCmplDateOfComplaint" value="' + toDateStringFromJson(ComplaintObj.DateClosed, '', 'dd/mm/yyyy') + '"  disabled/>';
        }
    }
    document.getElementById('pnlComplaintDlgDetail_2').innerHTML = strHtml;
}

function SwitchDlgComplaintTab() {
    try {
        for (i = 0; i < 3; i++) {
            var tabElement = document.getElementById('pnlComplaintDlgTab_' + i.toString());
            var detailElement = document.getElementById('pnlComplaintDlgDetail_' + i.toString());
            if (i == DlgComplaint_CurTab) {
                tabElement.className = 'companyTab activeTab';
                detailElement.className = '';
            } else {
                tabElement.className = 'companyTab';
                detailElement.className = 'hidden';
            }
        }
    }
    catch (err) {
    }
}

function DisplaySaveState(result) {
    if (result && result > 0) {
        alert('Complaint saved.');
    } else {
        alert('failed to save complaint');
    }
}