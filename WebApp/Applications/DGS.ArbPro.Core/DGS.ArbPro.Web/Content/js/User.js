﻿function SaveDatabase() {
    if (Page_ClientValidate('Create'))
    {
        var CompanyName = document.getElementById('txtCompanyName').value;
        var Address1 = document.getElementById('txtAddress1').value;
        var Address2 = document.getElementById('txtAddress2').value;
        var Town = document.getElementById('txtTown').value;
        var County = document.getElementById('txtCounty').value;
        var PostCode = document.getElementById('txtPostCode').value;
        var Country = 230;
        var Licenses = parseInt(document.getElementById('txtLicenses').value);
        var Value = document.getElementById('txtValue').value;
        var EmailAddress = document.getElementById('txtEmailAddress').value;
        var Title = document.getElementById('txtTitle').value;
        var FirstName = document.getElementById('txtFirstName').value;
        var LastName = document.getElementById('txtLastName').value;
        var Telephone = document.getElementById('txtTelephone').value;
        var Position = document.getElementById('txtPosition').value;
        var Comment = document.getElementById('txtComment').value;
        var RetrunMessage = Ajax_CallService(
            'Services/web/Account.asmx',
            'SaveDataBase',
            {
                CompanyName: CompanyName,
                Address1: Address1,
                Address2: Address2,
                Town: Town,
                County: County,
                PostCode: PostCode,
                Country: Country,
                Licenses: Licenses,
                AmountValue: Value,
                EmailAddress: EmailAddress,
                Title: Title,
                FirstName: FirstName,
                LastName: LastName,
                Telephone: Telephone,
                Position: Position,
                Comment: Comment
            },
            false
        );
        if (RetrunMessage == -1) {
            alert('Database is not connected. Try again');
            return;
        }
        if (RetrunMessage == 1) {
            alert("Company name already present.");
            return;
        }
        if (RetrunMessage == 2) {
            alert("User email already present.");
            return;
        }
        if (RetrunMessage == 3) {
            alert("User email and company name already present.");
            return;
        }
        alert('Database is successfully created');
        window.location = "/Admin/Main.aspx"
    }
    
}

function loadOrganization()
{
    var Grid = document.getElementById('grdOrg');
    var Orgs = Ajax_CallService(
            'Services/web/Account.asmx',
            'GetAllOrganization',
            {},
            false
        );
   
    var listHtml = '<table class="searchGrid width-full"><tr><th>Database No</th><th>Company Name</th><th>First User Name</th><th>Town</th><th>Contact Telephone</th><th>Date Created</th><th>Status</th><th>Edit</th></tr>';
    Orgs.forEach(function (c) {
        var ApptDate = new Date(parseInt(c.UserInfo.CreationDate.substr(6)));
        console.log('Client Id:' + c.ClientOrg.Id)
        listHtml = listHtml + '<tr><td>' + c.ClientOrg.Id + '</td>';
        listHtml = listHtml + '<td>' + c.ClientOrg.OrganisationName + '</td>';
        listHtml = listHtml + '<td>' + c.UserInfo.FirstName + ' ' + c.UserInfo.LastName + '</td>';
        listHtml = listHtml + '<td>' + c.ClientOrg.Town + '</td>';
        listHtml = listHtml + '<td>' + c.UserInfo.Telephone + '</td>';
        listHtml = listHtml + '<td>' + ApptDate.toLocaleDateString() + '</td>';
        listHtml = listHtml + '<td>' + ((c.UserInfo.UserAccountEnabled == true) ? 'Active' : 'Cancled') + '</td>';
        listHtml = listHtml + '<td onclick="window.location = \'/Admin/ManageUser.aspx?OrgId=' + c.ClientOrg.Id + '\';"><a href="#">Edit</a></td></tr>';
    });
    listHtml = listHtml + '</table>';
    Grid.innerHTML = listHtml;
}

function loadAdminUser(ClientId)
{
    var AdminUser = Ajax_CallService(
           'Services/web/Account.asmx',
           'GetAdminUser',
           { OrgId: ClientId },
           false
       );
    document.getElementById("txtAdminUserName").value = AdminUser.FirstName + ' ' + AdminUser.LastName;
    document.getElementById("txtAdminPassword").value = AdminUser.PasswordHashed;
}

function loadSingleOrganization(ClientId) {
    var Org = Ajax_CallService(
           'Services/web/Account.asmx',
           'getSingleOrganization',
           { OrgId: ClientId },
           false
       );
    document.getElementById("txtCompanyName").value = Org.OrganisationName;
    document.getElementById("txtAddress1").value = Org.Address1;
    document.getElementById("txtAddress2").value = Org.Address2;
    document.getElementById("txtTown").value = Org.Town;
    document.getElementById("txtCounty").value = Org.CountyOrState;
    document.getElementById("txtPostCode").value = Org.PostcodeOrZip;
    document.getElementById("txtCountry").value = Org.CountryID;
    document.getElementById("txtLicenses").value = Org.NumLicenses;

}

function loadSingleOrgUsers(OrgId)
{
    var Grid = document.getElementById('grdUsers');
    var OrgUsers = Ajax_CallService(
            'Services/web/Account.asmx',
            'getSingleOrganizationUsers',
            { OrgId: OrgId },
            false
        );

    var listHtml = '<table class="searchGrid width-full"><tr><th>Title</th><th>First Name</th><th>Last Name</th><th>Email</th><th>Web Enabled</th><th>Mobile Enabled</th></tr>';
    OrgUsers.forEach(function (c) {
        listHtml = listHtml + '<tr><td>' + c.Title + '</td>';
        listHtml = listHtml + '<td>' + c.FirstName + '</td>';
        listHtml = listHtml + '<td>' + c.LastName + '</td>';
        listHtml = listHtml + '<td>' + c.EmailAddress + '</td>';
        listHtml = listHtml + '<td>' + ((c.MobileEditionEnabled == true) ? 'Yes' : 'No') + '</td>';
        listHtml = listHtml + '<td>' + ((c.IsWebEnabled == true) ? 'Yes' : 'No') + '</td></tr>';
    });
    listHtml = listHtml + '</table>';
    Grid.innerHTML = listHtml;
}

function SaveUser() {
    var OrgId = document.getElementById('hfOrgId').value;
    if (OrgId == null || OrgId == '')
    {
        alert('Organization Id must be present');
        return;
    }
    if (Page_ClientValidate('Create'))
    {
        var EmailAddress = document.getElementById('txtEmailAddress').value;
        var Title = document.getElementById('txtTitle').value;
        var FirstName = document.getElementById('txtFirstName').value;
        var LastName = document.getElementById('txtLastName').value;
        var Telephone = document.getElementById('txtTelephone').value;
        var Position = document.getElementById('txtPosition').value;
        var Comment = document.getElementById('txtComment').value;
        var RetrunMessage = Ajax_CallService(
            'Services/web/Account.asmx',
            'SaveUser',
            {
                EmailAddress: EmailAddress,
                Title: Title,
                FirstName: FirstName,
                LastName: LastName,
                Telephone: Telephone,
                Position: Position,
                Comment: Comment,
                OrgId: OrgId
            },
            false
        );
        if (RetrunMessage == 1) {
            alert('Email already exist.');
            return;
        }
        if (RetrunMessage == -1) {
            alert("Problem in database, try again");
            return;
        }
        alert('User is successfully created');
        window.location = "/Admin/ManageUser.aspx?OrgId=" + OrgId;
    }
    
}

function loadTotalNoOfDatabases() {
    var TotalDatabases = Ajax_CallService(
           'Services/web/Account.asmx',
           'TotalNoOfDatabases',
           {},
           false
       );
    document.getElementById("txtNoOfDatabases").innerHTML = TotalDatabases;

}

function CheckAllUsersEnabledForOrg(OrgId) {
    var IsEnabled = Ajax_CallService(
           'Services/web/Account.asmx',
           'CheckAllUsersEnabledForOrg',
           {
               OrgId: OrgId
           },
           false
       );
    if (IsEnabled == 0)
    {
        $('.unblock').css({ "display": "none" });
        
        if ($('.block').is(":hidden"))
        {
            $('.unblock').css({ "display": "block" });
        }
        
    }
    else
    {
        $('.block').css({ "display": "none" });

        if ($('.unblock').is(":hidden"))
        {
            $('.unblock').css({ "display": "block" });
        }
    }
    
}

function EnableAllUsersForOrg(OrgId) {
    var IsEnabled = Ajax_CallService(
           'Services/web/Account.asmx',
           'EnableAllUsersForOrg',
           { OrgId: OrgId },
           false
       );
    if(IsEnabled == 0)
    {
        alert('All Users are Enabled for the Organization');
    }

    if(IsEnabled == -1)
    {
        alert('Not connected to database,try again');
        return;
    }
    window.location = "/Admin/ManageUser.aspx?OrgId=" + OrgId;
}

function DisableAllUsersForOrg(OrgId) {
    var IsEnabled = Ajax_CallService(
           'Services/web/Account.asmx',
           'DisableAllUsersForOrg',
           { OrgId: OrgId },
           false
       );
    if (IsEnabled == 0) {
        alert('All Users are Disable for the Organization');
    }

    if (IsEnabled == -1) {
        alert('Not connected to database,try again');
        return;
    }
    window.location = "/Admin/ManageUser.aspx?OrgId=" + OrgId;
}

function loadTotalClientsAmount() {
    var Org = Ajax_CallService(
           'Services/web/Account.asmx',
           'TotalClientAmount',
           {},
           false
       );
  
    document.getElementById("txtTotalValue").innerHTML = Org.Value;

}