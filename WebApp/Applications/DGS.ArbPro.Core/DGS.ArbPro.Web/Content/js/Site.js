﻿var Lat;
var Long;
var ZoomLevel;
var map;
var marker;

function Site_Load() {
    Lat = 54;
    Long = -2;
    ZoomLevel = 5;

    AutoComplete('txtClientSiteTown', 'ListTowns');
    AutoComplete('txtClientSiteCounty', 'ListCounties');

        var mapOptions = {
            center: new google.maps.LatLng(Lat, Long),
            zoom: ZoomLevel,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var mapcanvas = document.getElementById('mapcanvas');
        map = new google.maps.Map(mapcanvas, mapOptions);

        var PostCode = document.getElementById('txtClientSitePostCode').value;

        if (PostCode != '') {
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': PostCode + ', UK' },
                function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var location = results[0].geometry.location;
                        map.panTo(location);
                        map.setZoom(12);

                        document.getElementById('hfClientSiteLatitude').value = location.lat();
                        document.getElementById('hfClientSiteLongitude').value = location.lng();

                        if (marker == null) {
                            marker = new google.maps.Marker({
                                position: location,
                                map: map,
                                title: PostCode
                            });
                        } else {
                            marker.setMap(map);
                            marker.setPosition(location);
                        }
                    }
                });
        }

    
}

function UpdateMap() {
    var PostCode = document.getElementById('txtClientSitePostCode').value;

    if (PostCode != '') {
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': PostCode + ', UK' },
            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var location = results[0].geometry.location;
                    map.panTo(location);
                    map.setZoom(12);

                    document.getElementById('hfClientSiteLatitude').value = location.lat();
                    document.getElementById('hfClientSiteLongitude').value = location.lng();

                    if (marker == null) {
                        marker = new google.maps.Marker({
                            position: location,
                            map: map,
                            title: PostCode
                        });
                    } else {
                        marker.setMap(map);
                        marker.setPosition(location);
                    }
                }
            });
    }
}

function SaveSite() {
    var SiteID = document.getElementById('hfClientSiteID').value;
    var ClientID = document.getElementById('hfClientID').value;
    var HouseNameNo = document.getElementById('txtClientSiteHouseNameNo').value;
    var Address1 = document.getElementById('txtClientSiteAddress1').value;
    var Address2 = document.getElementById('txtClientSiteAddress2').value;
    var Town = document.getElementById('txtClientSiteTown').value;
    var County = document.getElementById('txtClientSiteCounty').value;
    var PostCode = document.getElementById('txtClientSitePostCode').value;
    var Latitude = document.getElementById('hfClientSiteLatitude').value;
    var Longitude = document.getElementById('hfClientSiteLongitude').value;
    var Reference = document.getElementById('txtClientSiteReference').value;
    var Client = GetUser(Page_strUserEmail);

    var Return = Ajax_CallService(
        'Services/Web/ClientData.asmx',
        'SaveClientSite',
        {
            ClientSiteID: SiteID,
            ClientID: ClientID,
            ContactID: -1,
            Reference: Reference,
            HouseNameNo: HouseNameNo,
            Address1: Address1,
            Address2: Address2,
            Town: Town,
            County: County,
            PostCode: PostCode,
            Latitude: Latitude,
            Longitude: Longitude
        },
        false);

    return Return;
}

var addressWatcher = {
    timeout: null,
    currentValue: '',
    watchForChange: function (el) {
        if (el.value != this.currentValue) {
            this.changed(el);
        }
        this.timeout = setTimeout(function () {
            addressWatcher.watchForChange(el)
        }, 1000);
    },
    cancelWatchForChange: function () {
        clearTimeout(this.timeout);
        this.timeout = null;
    },
    changed: function (el) {
        this.currentValue = el.value;
        UpdateMap();
    }
}