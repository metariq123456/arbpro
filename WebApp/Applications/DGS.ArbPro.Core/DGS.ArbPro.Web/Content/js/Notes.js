﻿var NoteDialogId;
var NoteDialogParentId;
/*Work stage represtents the following

    0 => Quote Note
    1 => Work Note
    2 => Invoice Note
*/
var WorkStage = 1;
var NoteDialogCurObj;

function Notes_Load(_NoteDialogId, _NoteDialogParentId, _WorkStage) {
    if (!_WorkStage) {
        //Default to quote note
        WorkStage = 0;
    } else {
        WorkStage = _WorkStage;
    }
    NoteDialogId = _NoteDialogId;
    NoteDialogParentId = _NoteDialogParentId;

    Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetNote',
            {
                parentId: NoteDialogParentId,
                noteId: NoteDialogId,
                type: WorkStage
            },
                true,
                PopulateFields
            );

}

//function to load a quote note
function LoadEnquiryNote() {
    Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetWorkNote',
            {
                jobId: NoteDialogParentId,
                id: NoteDialogId
            },
                true,
                PopulateFields
            );
}

function PopulateFields(curDialogNote) {
     
    NoteDialogCurObj = curDialogNote;
    if (NoteDialogCurObj.Id < 0) {
        document.getElementById('DateFields').className = 'hidden';
    }
    else { document.getElementById('DateFields').className = 'width-full'; }

    document.getElementById('txtEnteredBy').value = NoteDialogCurObj.EnteredBy;
    document.getElementById('chkDashboard').checked = NoteDialogCurObj.ShowOnDashboard;
    document.getElementById('txtNoteText').value = NoteDialogCurObj.Text;
    var ConvertedEnteredDate = toDateStringFromJson(NoteDialogCurObj.EnteredDate, '', 'dd/mm/yyyy');
    document.getElementById('txtEnteredDate').value = ConvertedEnteredDate;
    var ConvertedUpDate = toDateStringFromJson(NoteDialogCurObj.LastUpdated, '', 'dd/mm/yyyy');
    document.getElementById('txtUpDate').value = ConvertedUpDate;
}

function Note_Save() {
     
    NoteDialogCurObj.EnteredBy = document.getElementById('txtEnteredBy').value;
    NoteDialogCurObj.Text = document.getElementById('txtNoteText').value;
    NoteDialogCurObj.ShowOnDashboard = document.getElementById('chkDashboard').checked;
    var ParsedEnteredDate = ParseLocalDate(document.getElementById('txtEnteredDate').value, 'en_GB');
    NoteDialogCurObj.EnteredDate = ParsedEnteredDate;
    var ParsedLastUpdates = ParseLocalDate(document.getElementById('txtUpDate').value, 'en_GB');
    NoteDialogCurObj.LastUpdated = ParsedLastUpdates;


    var notes = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'SaveNote',
        {
            obj: NoteDialogCurObj
        },
            false
        );
}