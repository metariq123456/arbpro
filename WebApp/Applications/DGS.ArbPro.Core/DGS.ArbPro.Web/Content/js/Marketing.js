﻿var returnMarketingTab = 1;
var marketing_curTab = 1;
var seasonalMarketing = null;
var seasonalAccepted = null;
var seasonalNotAccepted = null;
var returnMarketing = null;
var sentReturnMarketing = null;
var workCategories = null;
var currentSelection = null;
var allClientMarketing = null;

// Function that calls the Seasonal and Return Marketing screens and loads their objects
function LoadMarketing(tabId) {
    PanelResize();
    SwitchReturnMarketingTab();
    currentSelection = {};
    workCategories = GetAllWorkCategories();
    seasonalMarketing = GetAllDistinctForSeasonalMarketing();
    returnMarketing = GetAllForReturnMarketing();
    sentReturnMarketing = GetSentForReturnMarketing();
    
    FilterSeasonalMarketing();
    UpdateComplaintTotal();
    PopulateSeasonalMarketing();
    PopulateReturnMarketing();
    PopulateSentReturnMarketing();
    SwitchMarketingTab(tabId);
}

// Function to resize the panel width based on screen size
function PanelResize() {
    var width = window.screen.availWidth;
    var pnlM = document.getElementById('Content_pnlMarketingWidth');
    if (width < 1680) {
        pnlM.style.width = '98%';
    } else if (width < 1920) {
        pnlM.style.width = '89%';
    } else {
        pnlM.style.width = '80%';
    }
}

// Funtion to set the CSS of control buttons
function SwitchReturnMarketingTab() {
    switch (returnMarketingTab) {
        case 1:
            document.getElementById('tabActiveMarketing').className = 'jobTab activeTab';
            document.getElementById('tabSentMarketing').className = 'jobTab';

            document.getElementById('pnlReturnMarketingDetails').className = 'width-full';
            document.getElementById('pnlSentReturnMarketingDetails').className = 'hidden';
            break;
        case 2:
            document.getElementById('tabActiveMarketing').className = 'jobTab';
            document.getElementById('tabSentMarketing').className = 'jobTab activeTab';

            document.getElementById('pnlReturnMarketingDetails').className = 'hidden';
            document.getElementById('pnlSentReturnMarketingDetails').className = 'width-full';

            break;
        default:
            document.getElementById('tabActiveMarketing').className = 'jobTab activeTab';
            document.getElementById('tabSentMarketing').className = 'jobTab';

            document.getElementById('pnlReturnMarketingDetails').className = 'width-full';
            document.getElementById('pnlSentReturnMarketingDetails').className = 'hidden';
            break;
    }
}

// Function to set the css classes for the Marketing tabs to display the selected tab only
// This function is dependant on the naming convention used in the aspx file
function SwitchMarketingTab(selectedTab) {

    marketing_curTab = selectedTab + 1;
    var tab;
    var panel;
    var count = 3;  //The total number of stages
    for (var i = 0; i < count; i++) {
        //get the current items being iterated
        tab = document.getElementById('tabMarketingStage_' + i.toString());
        panel = document.getElementById('pnlMarketingStage_' + i.toString());
        if (i === selectedTab) {
            //The current index is the panel to be displayed
            tab.className = 'jobTab activeTab';
            panel.className = '';
        }
        else {
            //The panel is not selected and should be hidden
            tab.className = 'jobTab';
            panel.className = 'hidden';
        }
    }

    if (selectedTab === 2) {
        if (allClientMarketing === null || allClientMarketing.length === 0) {
            allClientMarketing = GetAllClientMarketing();
            PopulateAllClientsMarketing();
        }
    }
}

// Function that is called in to filter which results are being displayed on the Seasonal Marketing tab
function FilterSeasonalMarketingDetails() {
    var panel = document.getElementById('pnlMarketingStage_0');
    var category = document.getElementById("ddlCategoryType");
    var catId = category.options[category.selectedIndex].value;
    var quoteStatus = document.getElementById("ddlQuoteStatus");
    var quoteStatusId = quoteStatus.options[quoteStatus.selectedIndex].value;
    var qAccepted = quoteStatusId === "1";
    var strHtml = '';

    var retVal = [];
    if (catId === "0" && qAccepted) {
        if (seasonalAccepted && seasonalAccepted.length > 0) {
            retVal = seasonalAccepted;
        }
    } else if (catId === "0" && !qAccepted) {
        if (seasonalNotAccepted && seasonalNotAccepted.length > 0) {
            retVal = seasonalNotAccepted;
        }
    } else {
        if (seasonalMarketing && seasonalMarketing.length > 0) {
            seasonalMarketing.forEach(function (s) {
                var name = s.Name + s.Description;
                if (name === catId && s.QuoteAccepted === qAccepted) {
                    retVal.push(s);
                }
            });
        }
    }
    
    currentSelection = retVal;
    strHtml += PopulateSeasonalMarketingFiltering(catId, qAccepted);
    strHtml += PopulateSeasonalMarketingDetails();

    panel.innerHTML = strHtml;
}

// Central function that combines the filter section and the details section for Seasonal Marketing
function PopulateSeasonalMarketing() {
    var panel = document.getElementById('pnlMarketingStage_0');

    try {
        var strHtml = '';

        strHtml += PopulateSeasonalMarketingFiltering();

        strHtml += PopulateSeasonalMarketingDetails();

        panel.innerHTML = strHtml;
    } catch (err) {
        DisplayMessage(panel.id, 'ERROR: Unable to display Seasonal Marketing Details - ' + err.toString());
    }
}

// Function that populates the part of Seasonal Marketing that contains all of the filters - dropdowns for category and quote status and export to CSV
function PopulateSeasonalMarketingFiltering(selected, qAccepted) {
    var strHtml = '';

    strHtml += '<div style="width:100%; height: 47px;">';
    strHtml += '<label class="clientField"> Select the data you would like to export to CSV. Once exported you can use a marketing platform to create marketing campaigns, for example MailChimp.</label>';
    strHtml += '</div>';
    strHtml += '<div style="width:100%; height: 50px;">';
    strHtml += '<label class="paddedExtra float-left" for="ddlCategoryType"> Work Category:</label>';
    strHtml += '<select id="ddlCategoryType" class="float-left " onchange="FilterSeasonalMarketingDetails();" name="ddlCategoryType">';
    strHtml += '<option value="-1" >Select Template</option>';
    if (selected && selected === "0") {
        strHtml += '<option value="0" selected="selected">All</option>';
    } else {
        strHtml += '<option value="0">All</option>';
    }

    if (workCategories && workCategories.length > 0) {
        workCategories.forEach(function(wc) {
            var name = wc.Name + wc.Description;
            if (selected && name === selected) {
                strHtml += '<option value="' + name + '" selected="selected">' + wc.Name + '</option>';
            } else {
                strHtml += '<option value="' + name + '">' + wc.Name + '</option>';
            }
        });
    }

    strHtml += '</select>';
    strHtml += '<label class="paddedExtra float-left" for="ddlQuoteStatus"> Work Status:</label>';
    strHtml += '<select id="ddlQuoteStatus" class="float-left" onchange="FilterSeasonalMarketingDetails();" name="ddlQuoteStatus">';
    if (qAccepted) {
        strHtml += '<option value="1" selected="selected">Accepted</option>';
        strHtml += '<option value="0">Not Accepted</option>';
    } else {
        strHtml += '<option value="1">Accepted</option>';
        strHtml += '<option value="0" selected="selected">Not Accepted</option>';
    }
    strHtml += '</select>';
    strHtml += '<label class="paddedExtra float-left" for="ddlExportFormat"> Export Format:</label>';
    strHtml += '<select id="ddlExportFormat" class="float-left" onchange="FilterSeasonalMarketingDetails();" name="ddlExportFormat">';
    strHtml += '<option value="0">MailChimp</option>';
    strHtml += '</select>';
    strHtml += '<button type="button" onclick="ExportSeasonalToCsv();" class="button-thin paddedExtra"> Export To CSV</button><br/>';
    if (selected && selected === "0") {
        strHtml += '<label class="paddedExtra float-left warning-text"> Selecting “All” and “Accepted” will display every client with at least one accepted category.  Selecting “All” and “Not accepted” will display every client with no accepted categories. </label>';
    }
    strHtml += '</div>';

    return strHtml;
}

// Function to populate the Seasonal Marketing details with the filtered criteria
function PopulateSeasonalMarketingDetails() {
    var strHtml = '';

    strHtml += '<div style="height: 40px; overflow: auto; width-full;">';
    strHtml += '<table Width="98%" class="rtGrid"><col width="15%" /><col width="24%" /><col width="12%" /><col width="12%" /><col width="24%" /><col width="13%" />';
    strHtml += '<tr><td colspan="6" class="rowHeader"></td></tr>';
    strHtml += '<tr><td class="rowHeader">Name</td>';
    strHtml += '<td class="rowHeader">Address</td>';
    strHtml += '<td class="rowHeader">Telephone</td>';
    strHtml += '<td class="rowHeader">Mobile</td>';
    strHtml += '<td class="rowHeader">Email</td>';
    strHtml += '<td class="rowHeader">Category</td>';
    strHtml += '</table>';
    strHtml += '</div>';
    if (currentSelection && currentSelection.length > 0) {
        strHtml += '<div style="height: 388px; overflow: auto; width-full;">';

        if (currentSelection.length > 10) {
            strHtml += '<table Width="99%" class="rtGrid"><col width="15%" /><col width="24%" /><col width="12%" /><col width="12%" /><col width="24%" /><col width="13%" />';
        } else {
            strHtml += '<table Width="98%" class="rtGrid"><col width="15%" /><col width="24%" /><col width="12%" /><col width="12%" /><col width="24%" /><col width="13%" />';
        }

        currentSelection.forEach(function (r) {
            strHtml += '<tr><td class="rowData">' + r.ClientName + '</td>';
            strHtml += '<td class="rowData">' + r.Address + '</td>';
            strHtml += '<td class="rowData">' + r.Telephone + '</td>';
            strHtml += '<td class="rowData">' + r.Mobile + '</td>';
            strHtml += '<td class="rowData">' + r.Email + '</td>';
            strHtml += '<td class="rowData">' + r.Name + '</td>';
        });

        strHtml += '</table>';
        strHtml += '</div>';
    } else {
        strHtml += '<div style="height: 35px; overflow: auto; width-full;">';
        strHtml += '</div>';
        strHtml += '<div style="height: 353px; overflow: auto; width-full;">';
        strHtml += '<label class="center">There are no results to display. Please select a or change the Work Category and Work Status. </label>';
        strHtml += '</div>';
    }


    strHtml += '<div style="height: 20px; overflow: auto; width-full;">';
    strHtml += '</div>';

    return strHtml;
}

// Function to populate the Active Return Marketing details tab
function PopulateReturnMarketing() {
    var panel = document.getElementById('pnlReturnMarketingDetails');
    var curDate = new Date().format("dd/MMM/yyyy");
    var newDate = toDateFromJson(new Date().setMonth(new Date().getMonth() + 1));
    newDate = newDate.format("dd/MMM/yyyy");

    try {
        var strHtml = '';

        strHtml += '<div style="height: 36px; overflow: auto; width-full;">';
        strHtml += '<label class="center">Below is a list of all clients\' default contacts with active return dates between ' + curDate + ' and ' + newDate + ' with the oldest at the top. </label>';
        strHtml += '<label class="center">You can send emails individually or export the selected entries to CSV in MailChimp format. </label>';
        strHtml += '</div>';

        strHtml += '<div style="height: 40px; overflow: auto; width-full;">';
        strHtml += '<table Width="98%" class="rtGrid"><col width="10%" /><col width="17%" /><col width="20%" /><col width="9%" /><col width="9%" /><col width="18%" /><col width="9%" /><col width="4%" /><col width="4%" />';
        strHtml += '<tr><td colspan="9" class="rowHeader" ></td></tr>';
        strHtml += '<tr><td class="rowHeader">Name</td>';
        strHtml += '<td class="rowHeader">Address</td>';
        strHtml += '<td class="rowHeader">Comments</td>';
        strHtml += '<td class="rowHeader">Telephone</td>';
        strHtml += '<td class="rowHeader">Return Date</td>';
        strHtml += '<td class="rowHeader">Email</td>';
        strHtml += '<td class="rowHeader">Category</td>';
        strHtml += '<td class="rowHeader">Mail</td>';
        strHtml += '<td class="rowHeader"><span class="float-right"><input id="chkSelectAll" class="center" type="checkbox" onclick="SelectAllForResult();" style="cursor: pointer;"/></span></td></tr>';
        strHtml += '</table>';
        strHtml += '</div>';

        if (returnMarketing && returnMarketing.length > 0) {
            strHtml += '<div style="height: 418px; overflow: auto; width-full;">';

            if (returnMarketing.length > 9) {
                strHtml += '<table Width="99%" class="rtGrid"><col width="10%" /><col width="17%" /><col width="20%" /><col width="9%" /><col width="9%" /><col width="18%" /><col width="9%" /><col width="4%" /><col width="4%" />';
            } else {
                strHtml += '<table Width="98%" class="rtGrid"><col width="10%" /><col width="17%" /><col width="20%" /><col width="9%" /><col width="9%" /><col width="18%" /><col width="9%" /><col width="4%" /><col width="4%" />';
            }
            
            returnMarketing.forEach(function (r) {
                strHtml += '<tr><td class="rowData">' + r.ClientName + '</td>';
                strHtml += '<td class="rowData">' + r.Address + '</td>';
                strHtml += '<td class="rowData">' + r.Comments + '</td>';
                strHtml += '<td class="rowData">' + r.Telephone + '</td>';
                strHtml += '<td class="rowData">' + toFormattedDateStringFromJson(r.ReturnDate, '', 'en_gb') + '</td>';
                strHtml += '<td class="rowData">' + r.Email + '</td>';
                strHtml += '<td class="rowData">' + r.Name + '</td>';
                strHtml += '<td class="rowData"><button type="button" onclick="SendMarketingEmail(' + r.Id + ');" class="button-thin float-left"> Send </button></td>';
                strHtml += '<td class="rowData"><span class="center"><input id="chk' + r.Id + '" class="float-right" onclick="ToggleDisabledButtons();" type="checkbox" style="cursor: pointer;"/></span></td></tr>';
            });

            strHtml += '</table>';
            strHtml += '</div>';

            strHtml += '<div style="height: 27px; width: 98%; overflow: auto;">';
            strHtml += '<button id="btnMarkAsSent" type="button" onclick="MarkAsSent();" class="button-thin float-right padded" disabled="true"> Mark Selected As Sent </button>';
            strHtml += '<button id="btnDeleteSelected" type="button" onclick="DeleteSelected();" class="button-thin float-right padded" disabled="true"> Delete Selected </button>';
            strHtml += '<button id="btnReturnExport" type="button" onclick="ExportReturnToCsv();" class="button-thin float-right padded" disabled="true"> Export Selected To CSV</button>';
            strHtml += '</div>';
        } else {
            strHtml += '<div style="height: 35px; overflow: auto; width-full;">';
            strHtml += '</div>';
            strHtml += '<div style="height: 410px; overflow: auto; width-full;">';
            strHtml += '<label class="center">There are no current active return marketing results. </label>';
            strHtml += '</div>';
        }

        panel.innerHTML = strHtml;
    } catch (err) {
        DisplayMessage(panel.id, 'ERROR: Unable to display Return Marketing Details - ' + err.toString());
    }
}

// Function to populate the Sent Return Marketing details tab
function PopulateSentReturnMarketing() {
    var panel = document.getElementById('pnlSentReturnMarketingDetails');

    try {
        var strHtml = '';

        strHtml += '<div style="height: 25px; overflow: auto; width-full;">';
        strHtml += '<label class="center">Below is a list of all sent emails for Return Marketing with the latest sent at the top. </label>';
        strHtml += '</div>';
        strHtml += '<div style="height: 40px; overflow: auto; width-full;">';
        strHtml += '<table Width="98%" class="rtGrid"><col width="10%" /><col width="19%" /><col width="18%" /><col width="9%" /><col width="9%" /><col width="19%" /><col width="10%" /><col width="6%" />';
        strHtml += '<tr><td colspan="8" class="rowHeader"></td></tr>';
        strHtml += '<tr><td class="rowHeader">Name</td>';
        strHtml += '<td class="rowHeader">Address</td>';
        strHtml += '<td class="rowHeader">Comments</td>';
        strHtml += '<td class="rowHeader">Telephone</td>';
        strHtml += '<td class="rowHeader">Return Date</td>';
        strHtml += '<td class="rowHeader">Email</td>';
        strHtml += '<td class="rowHeader">Category</td>';
        strHtml += '<td class="rowHeader">Status</td></tr>';
        strHtml += '</table>';
        strHtml += '</div>';

        if (sentReturnMarketing && sentReturnMarketing.length > 0) {
            strHtml += '<div style="height: 436px; overflow: auto; width-full;">';

            if (sentReturnMarketing.length > 9) {
                strHtml += '<table Width="99%" class="rtGrid"><col width="10%" /><col width="19%" /><col width="18%" /><col width="9%" /><col width="9%" /><col width="19%" /><col width="10%" /><col width="6%" />';
            } else {
                strHtml += '<table Width="98%" class="rtGrid"><col width="10%" /><col width="19%" /><col width="18%" /><col width="9%" /><col width="9%" /><col width="19%" /><col width="10%" /><col width="6%" />';
            }

            sentReturnMarketing.forEach(function(r) {
                strHtml += '<tr><td class="rowData">' + r.ClientName + '</td>';
                strHtml += '<td class="rowData">' + r.Address + '</td>';
                strHtml += '<td class="rowData">' + r.Comments + '</td>';
                strHtml += '<td class="rowData">' + r.Telephone + '</td>';
                strHtml += '<td class="rowData">' + toFormattedDateStringFromJson(r.ReturnDate, '', 'en_gb') + '</td>';
                strHtml += '<td class="rowData">' + r.Email + '</td>';
                strHtml += '<td class="rowData">' + r.Name + '</td>';
                strHtml += '<td class="rowData">Sent on: ' + toFormattedDateStringFromJson(r.EmailSentDate, '', 'en_gb') + '</td></tr>';
            });

            strHtml += '</table>';
            strHtml += '</div>';
        } else {
            strHtml += '<div style="height: 35px; overflow: auto; width-full;">';
            strHtml += '</div>';
            strHtml += '<div style="height: 401px; overflow: auto; width-full;">';
            strHtml += '<label class="center">There are no current sent return marketing results. </label>';
            strHtml += '</div>';
        }

        strHtml += '<div style="height: 20px; overflow: auto; width-full;">';
        strHtml += '</div>';

        panel.innerHTML = strHtml;
    } catch (err) {
        DisplayMessage(panel.id, 'ERROR: Unable to display Sent Return Marketing Details - ' + err.toString());
    }
}

// Function to populate the All Clients marketing details tab
function PopulateAllClientsMarketing() {
    var panel = document.getElementById('pnlAllClientsMarketingDetails');
    var loadPnl = document.getElementById('pnlAllClientsMarketingDetailsWait');
    panel.className = "hidden";
    loadPnl.className = "float-left width-full";
    var strHtml = '';

    try {
        strHtml += '<div style="height: 40px; overflow: auto; width-full;">';
        strHtml += '<label class="center">Below is a list of all clients\' default contacts that have opted-in for Marketing, sorted alphabetically by the client\'s surname and business name. </label>';
        strHtml += '<label class="center">You export the selected entries to CSV in MailChimp format for mass mailing purposes. </label>';
        strHtml += '</div>';

        strHtml += '<div style="height: 40px; overflow: auto; width-full;">';
        strHtml += '<table Width="98%" class="rtGrid"><col width="17%" /><col width="28%" /><col width="14%" /><col width="14%" /><col width="27%" />';
        strHtml += '<tr><td colspan="5" class="rowHeader"></td></tr>';
        strHtml += '<tr><td class="rowHeader">Name</td>';
        strHtml += '<td class="rowHeader">Address</td>';
        strHtml += '<td class="rowHeader">Telephone</td>';
        strHtml += '<td class="rowHeader">Mobile</td>';
        strHtml += '<td class="rowHeader">Email</td>';
        strHtml += '</table>';
        strHtml += '</div>';
        if (allClientMarketing && allClientMarketing.length > 0) {
            strHtml += '<div style="height: 435px; overflow: auto; width-full;">';

            if (allClientMarketing.length > 10) {
                strHtml += '<table Width="99%" class="rtGrid"><col width="17%" /><col width="28%" /><col width="14%" /><col width="14%" /><col width="27%" />';
            } else {
                strHtml += '<table Width="98%" class="rtGrid"><col width="17%" /><col width="28%" /><col width="14%" /><col width="14%" /><col width="27%" />';
            }

            allClientMarketing.forEach(function (r) {
                strHtml += '<tr><td class="rowData">' + r.ClientName + '</td>';
                strHtml += '<td class="rowData">' + r.Address + '</td>';
                strHtml += '<td class="rowData">' + r.Telephone + '</td>';
                strHtml += '<td class="rowData">' + r.Mobile + '</td>';
                strHtml += '<td class="rowData">' + r.Email + '</td>';
            });

            strHtml += '</table>';
            strHtml += '</div>';

            strHtml += '<div style="height: 30px; width: 98%; overflow: auto;">';
            strHtml += '<button id="btnReturnExport" type="button" onclick="ExportAllClientsToCsv();" class="button-thin float-right padded"> Export To CSV</button>';
            strHtml += '</div>';
        } else {
            strHtml += '<div style="height: 40px; overflow: auto; width-full;">';
            strHtml += '</div>';
            strHtml += '<div style="height: 425px; overflow: auto; width-full;">';
            strHtml += '<label class="center">There are no results to display. Please select a or change the Work Category and Work Status. </label>';
            strHtml += '</div>';
        }


        panel.className = "float-left width-full";
        panel.innerHTML = strHtml;

        loadPnl.className = "hidden";
    } catch (err) {
        DisplayMessage(panel.id, 'ERROR: Unable to display Seasonal Marketing Details - ' + err.toString());
    }
}

// Function that marks the selected Return Marketing items as sent
function MarkAsSent() {
    var selectSent = GetSelectedReturnMarketing();
    if (selectSent && selectSent.length > 0) {
        var conf = confirm("This will mark all selected items as Sent - they will no longer appear in the Active Return Marketing section. \n Are you sure you want to proceed?");
        if (conf) {
            UpdateSelected(selectSent);
        }
    } else {
        var message = 'You have not selected any entries to be marked. Please do so and try again.';
        DisplayMessage(message, 1);
    }
}

// Function that updates Return Time categories
function UpdateSelected(obj) {
    var retVal = true;
    var panel = document.getElementById('pnlMarketingStage_1');
    try {
        obj.forEach(function (o) {
            var success = Ajax_CallService(
                'Services/Web/Marketing.asmx',
                'SaveClientWorkCategory',
                {
                    obj: o
                },
                false
            );
            if (!success) {
                retVal = false;
            }
        });
    } catch (e) {
        DisplayMessage(panel.id, 'ERROR: Unable to update sent emails - ' + e.toString());
    }

    if (retVal) {
        LoadMarketing(1);
    } else {
        DisplayMessage(panel.id, 'There was a problem with updating some of the items as being sent. Please refresh the page and try again.');
    }
}

// Function that gets the selected Return Marketing items and sends them for deletion
function DeleteSelected() {
    var selectSent = GetSelectedReturnMarketing();
    if (selectSent && selectSent.length > 0) {
        var conf = confirm("Caution!\nThis will delete all selected items from Return and Seasonal Marketing. \nAre you sure you want to proceed?");
        if (conf) {
            DeleteCategories(selectSent);
        }
    } else {
        var message = 'You have not selected any entries to be marked. Please do so and try again.';
        DisplayMessage(message, 1);
    }
}

// Function that makes a call to the asmx file to delete selected categories
function DeleteCategories(obj) {
    var retVal = true;
    if (obj && obj.length > 0) {
        // Delete any left over ones that have been unchecked.
        obj.forEach(function (o) {
            var success = Ajax_CallService(
                'Services/Web/Marketing.asmx',
                'DeleteClientWorkCategoryForId',
                {
                    id: o.Id
                },
                false
            );
            if (!success) {
                retVal = false;
            }
        });
    }

    if (retVal) {
        LoadMarketing(1);
    } else {
        DisplayMessage(panel.id, 'There was a problem with deleting some of the items. Please refresh the page and try again.');
    }
}

// Function to select or disselect all active return time categories
function SelectAllForResult() {
    var isChecked = document.getElementById("chkSelectAll").checked;
    if (returnMarketing && returnMarketing.length > 0) {
        returnMarketing.forEach(function (r) {
            var chk = document.getElementById("chk" + r.Id);
            if (isChecked) {
                chk.checked = true;
            } else {
                chk.checked = false;
            }
        });

        ToggleDisabledButtons();
    }
}

// Function that toggles the Export To CSV and Mark As Sent buttons between disabled and enabled
function ToggleDisabledButtons() {
    var anyChecked = false;
    if (returnMarketing && returnMarketing.length > 0) {
        returnMarketing.forEach(function (r) {
            var chk = document.getElementById("chk" + r.Id).checked;
            if (chk) {
                anyChecked = true;
            }
        });
    }

    if (anyChecked) {
        document.getElementById("btnMarkAsSent").disabled = false;
        document.getElementById("btnDeleteSelected").disabled = false;
        document.getElementById("btnReturnExport").disabled = false;
    } else {
        document.getElementById("btnMarkAsSent").disabled = true;
        document.getElementById("btnDeleteSelected").disabled = true;
        document.getElementById("btnReturnExport").disabled = true;
    }
}

// Function that opens the Send Email dialogue for Active Return Marketing
function SendMarketingEmail(id) {
    if (id && id > 0) {
        Dialog_OpenPage(
            'Send Marketing',
            'Dialogs/PDFGeneration.aspx',
            'content',
            900,
            600,
            true,
            {
                'Close': {
                    text: 'Close',
                    "class": 'ui-dialog-button',
                    click: function () {
                        LoadMarketing(1);
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/MarketingGeneration.js?V=3.23.5',
            function() {
                MarketingGenerationLoad(id);
            }
        );
    }
}

// Function to display a message to the user
function DisplayMessage(message, tabId) {
    Dialog_OpenPage(
        'Marketing',
        'Dialogs/Alert.aspx',
        'content',
        500,
        150,
        true,
        {
            'Ok': {
                text: 'Ok',
                "class": 'ui-dialog-button',
                click: function() {
                    LoadMarketing(tabId);
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/AlertBox.js?V=3.23.5',
        function() {
            AlertBox_Show(message);
        });
}

// Function that gets all selected Seasonal Marketing entries to be exported and passes them on to get exported
function ExportSeasonalToCsv() {
    if (currentSelection && currentSelection.length > 0) {
        currentSelection.forEach(function(c) {
            c.ReturnDate = toDateFromJson(c.ReturnDate);
            c.EmailSentDate = toDateFromJson(c.EmailSentDate);
        });

        ExportToCsv(currentSelection, 'MarketingSeasonalMailingListExport_' + currentSelection[0].Name + '.csv');
    } else {
        var message = 'There are no entries to be exported. Please refine the search filters and try again.';
        DisplayMessage(message, 0);
    }
}

// Function that exports currently selected Marketing contacts to a CSV file
function ExportToCsv(obj, fileName) {
    var exportObj = Ajax_CallService(
        'Services/Web/Settings.asmx',
        'MarketingExport',
        {
            obj: obj
        },
        false);

    var filename = fileName;

    if (window.navigator.msSaveOrOpenBlob) {
        var blobObject = new Blob([exportObj]);
        window.navigator.msSaveOrOpenBlob(blobObject, filename);
    } else {
        var a = window.document.createElement('a');
        a.href = window.URL.createObjectURL(new Blob([exportObj], { type: 'text/csv' }));
        a.download = filename;

        // Append anchor to body.
        document.body.appendChild(a);
        a.click();
        // Remove anchor from body
        document.body.removeChild(a);
    }
}

// Function that gets all selected Return Marketing entries to be exported and passes them on to get exported
function ExportReturnToCsv() {
    var selectExported = GetSelectedReturnMarketing();

    if (selectExported.length > 0) {
        ExportToCsv(selectExported, 'MarketingReturnMailingListExport.csv');
    } else {
        var message = 'You have not selected any entries to be exported. Please do so and try again.';
        DisplayMessage(message, 1);
    }
}

// Function that gets all clients who have opted-in for marketing and passes them on to get exported
function ExportAllClientsToCsv() {
    if (allClientMarketing && allClientMarketing.length > 0) {
        allClientMarketing.forEach(function (c) {
            c.ReturnDate = toDateFromJson(c.ReturnDate);
            c.EmailSentDate = toDateFromJson(c.EmailSentDate);
        });
        ExportToCsv(allClientMarketing, 'MarketingAllClientMailingListExport.csv');
    } else {
        var message = 'There are no entries to be exported. Please check that you have clients who have opted in for marketing.';
        DisplayMessage(message, 2);
    }
}

// Function to get all distinct accepted and not accepted clients for seasonal marketing filtering purposes
function FilterSeasonalMarketing() {
    seasonalAccepted = GetAllDistinctAcceptedForSeasonalMarketing();
    seasonalNotAccepted = GetAllDistinctNotAcceptedForSeasonalMarketing();

    if (seasonalAccepted && seasonalAccepted.length > 0 && seasonalNotAccepted && seasonalNotAccepted.length > 0) {
        for (var i = seasonalNotAccepted.length - 1; i >= 0; i--) {
            var n = seasonalNotAccepted[i];
            seasonalAccepted.forEach(function(a) {
                if (n.ClientName === a.ClientName && n.ClientAddress === a.ClientAddress) {
                    seasonalNotAccepted.splice(i, 1);
                }
            });
        }
    }
}

// Function to get all selected items in the Active Return Marketing tab
function GetSelectedReturnMarketing() {
    var retVal = [];
    if (returnMarketing && returnMarketing.length > 0) {
        returnMarketing.forEach(function (c) {
            var chkSelected = document.getElementById("chk" + c.Id);
            if (chkSelected.checked) {
                c.EmailSent = true;
                c.ReturnDate = toDateFromJson(c.ReturnDate);
                c.EmailSentDate = toDateFromJson(new Date());
                retVal.push(c);
            }
        });
    }

    return retVal;
}

// Function that gets all work categories
function GetAllWorkCategories() {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetAllWorkCategories',
        {
        },
        false
    );

    return catList;
}

// Gets all seasonal marketing entries
function GetAllDistinctForSeasonalMarketing() {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetAllDistinctForSeasonalMarketing',
        {
        },
        false
    );

    return catList;
}

// Gets all distinct accepted seasonal marketing entries
function GetAllDistinctAcceptedForSeasonalMarketing() {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetAllDistinctAcceptedForSeasonalMarketing',
        {
        },
        false
    );

    return catList;
}

// Gets all distinct not accepted seasonal marketing entries
function GetAllDistinctNotAcceptedForSeasonalMarketing() {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetAllDistinctNotAcceptedForSeasonalMarketing',
        {
        },
        false
    );

    return catList;
}

// Gets all active return marketing entries
function GetAllForReturnMarketing() {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetAllForReturnMarketing',
        {
        },
        false
    );

    return catList;
}

// Gets a list of all clients' in marketing format
function GetAllClientMarketing() {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetAllClientMarketing',
        {
        },
        false
    );

    return catList;
}

// Gets all sent return marketing entries
function GetSentForReturnMarketing() {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetSentForReturnMarketing',
        {
        },
        false
    );

    return catList;
}
