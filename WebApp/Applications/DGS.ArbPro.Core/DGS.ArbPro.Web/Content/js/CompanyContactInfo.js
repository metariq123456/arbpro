﻿var CompanyObject;
function CompanyInfo_Load() {
    GetCompanyObject();
    PopulateFields();
}
function CompanySave() {
    try
    {
        CompanyObject.Name = document.getElementById('txtCompanyName').value;
        CompanyObject.StreetNumber = document.getElementById('txtCompanyNumber').value;
        CompanyObject.Address1 = document.getElementById('txtCompanyAddress1').value;
        CompanyObject.Address2 = document.getElementById('txtCompanyAddress2').value;
        CompanyObject.County = document.getElementById('txtCompanyCounty').value;
        CompanyObject.PostCode = document.getElementById('txtCompanyPostCode').value;
        CompanyObject.Telephone = document.getElementById('txtCompanyTelephoneNum').value;
        CompanyObject.Mobile = document.getElementById('txtCompanyMobileNum').value;
        CompanyObject.Town = document.getElementById('txtCompanyTown').value;
        CompanyObject.WebSite = document.getElementById('txtCompanyWeb').value;
        CompanyObject.Email = document.getElementById('txtCompanyEmailAddress').value;
        CompanyObject.AccountsEmail = document.getElementById('txtCompanyAccountsEmail').value;
        CompanyObject.DirectorsName = document.getElementById('txtDirectorsName').value;
        CompanyObject.DirectorsQualifications = document.getElementById('txtDirectorsQualifications').value;

        var Return = Ajax_CallService(
                'Services/Web/Company.asmx',
                'SaveCompany',
                {
                    Company: CompanyObject
                },
                false
            );
    }
    catch (err) {
        alert('Error: Unable to update company information. If this error continues please contact support');
    }
}

var Watcher = {
    timeout: null,
    currentValue: '',
    watchForChange: function (el) {
        alert('watching');
        this.currentValue = el.value;
        this.timeout = setTimeout(function () {
            Watcher.watch(el);
        }, 200);
    },
    watch: function (el) {
        if (el.value != this.currentValue) {
            this.changed(el);
        }
        this.timeout = setTimeout(function () {
            Watcher.watch(el);
        }, 200);
    },
    cancelWatchForChange: function () {
        clearTimeout(this.timeout);
        this.timeout = null;
    },
    changed: function (el) {
        this.currentValue = el.value;
        Changed(true);
    }
}

function Changed(bChanged) {
    var Button = $('#btnSave');
    if (bChanged) {
        Button.attr('class', 'button-alert float-right padded');
    } else {
        Button.attr('class', 'button-basic float-right padded');
    }
}

function PopulateFields() {
    document.getElementById('txtCompanyName').value = CompanyObject.Name;
    document.getElementById('txtCompanyNumber').value = CompanyObject.StreetNumber;
    document.getElementById('txtCompanyAddress1').value = CompanyObject.Address1;
    document.getElementById('txtCompanyAddress2').value = CompanyObject.Address2; 
    document.getElementById('txtCompanyTown').value = CompanyObject.Town;
    document.getElementById('txtCompanyCounty').value = CompanyObject.County;
    document.getElementById('txtCompanyPostCode').value = CompanyObject.PostCode;
    document.getElementById('txtCompanyTelephoneNum').value = CompanyObject.Telephone;
    document.getElementById('txtCompanyMobileNum').value = CompanyObject.Mobile;
    document.getElementById('txtCompanyWeb').value = CompanyObject.WebSite;
    document.getElementById('txtCompanyEmailAddress').value = CompanyObject.Email;
    document.getElementById('txtCompanyAccountsEmail').value = CompanyObject.AccountsEmail;
    document.getElementById('txtDirectorsName').value = CompanyObject.DirectorsName;
    document.getElementById('txtDirectorsQualifications').value = CompanyObject.DirectorsQualifications;
}

function GetCompanyObject() {
    CompanyObject = Ajax_CallService(
    '../Services/Web/Company.asmx',
    'GetCompany',
    {},
    false
   );
}