﻿var CurList;
var SelectedResultId;
var TemplateType;
var SelectedObj;
var CurMessageType;
var CurObjectType;

function Templates_Load(objType, msgType) {
    SelectedResultId = -1;
    CurObjectType = objType;
    CurMessageType = msgType;
    GetTemplateList();
}

function DropDownChanged() {
    var _templateType = document.getElementById('drpQuoteInvoice').value;
    TemplateType = _templateType;
    if (TemplateType == '1') {
        document.getElementById('pnlInvoicePaymentInfo').className = '';
    }
    else {
        document.getElementById('pnlInvoicePaymentInfo').className = 'hidden';
    }
}

function ServiceCallback(results) {
    CurList = results;
    UpdateTable();
}

function UpdateTable() {
    if (CurList.length < 1) {
        document.getElementById('pnlItemControls').className = 'hidden';
        document.getElementById('pnlNoResults').className = '';
        document.getElementById('pnlTableContainer').innerHTML = '';
    }
    else {
        var ListPanel = document.getElementById('pnlTableContainer');
        var strHtml = '<table class="searchGrid width-full white">';
        for (var i = 0; i < CurList.length; i++) {
            if (i == SelectedResultId) {
                strHtml += '<tr class="selected" onclick="ItemSelected(' + i + ');">';
            }
            else {
                strHtml += '<tr onclick="ItemSelected(' + i + ');">';
            }
            strHtml += '<td style="padding-left: 10px;">' + CurList[i].TemplateName + '</td>';
            strHtml += '</tr>';
        }
        strHtml += '</table>';
        ListPanel.innerHTML = strHtml;

        document.getElementById('pnlNoResults').className = 'hidden';
    }
}

function ItemSelected(Id) {
    SelectedResultId = Id;
    SelectedObj = CurList[Id];
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('pnlComingSoon').className = 'hidden';
    PopulateFields();
    UpdateTable();
}

function GetTemplateList() {
    SelectedResultId = -1;
    try {
        Ajax_CallService(
       'Services/Web/Settings.asmx',
       'GetTemplateListForTypeAndStage',
       {
           Format: CurMessageType,
           tmplType: CurObjectType
       },
       true,
       ServiceCallback
    );
    }
    catch (err) {
    }
}

function PopulateFields() {
    //Quote
    if (SelectedObj.ObjectType == 0) {
        document.getElementById('drpQuoteInvoice').value = '0';
        document.getElementById('txtTemplateName').value = SelectedObj.TemplateName;
        document.getElementById('txtSalutations').value = SelectedObj.Salutation;
        document.getElementById('txtContentText').value = SelectedObj.BodyText;
        //Check for email type then set up for subject
        if (CurMessageType == 1) {
            document.getElementById('lblSalutation').textContent = 'Subject';
            document.getElementById('txtSalutations').placeholder = 'Email Subject';
        }
        DropDownChanged();
    }
    //Invoice
    else if (SelectedObj.ObjectType == 1) {
        document.getElementById('drpQuoteInvoice').value = '1';
        document.getElementById('txtTemplateName').value = SelectedObj.TemplateName;
        document.getElementById('txtSalutations').value = SelectedObj.Salutation;
        document.getElementById('txtContentText').value = SelectedObj.BodyText;
        document.getElementById('txtPaymentInfo').value = SelectedObj.AdditionalText;
        //Check for email type then set up for subject
        if (CurMessageType == 1) {
            document.getElementById('lblSalutation').textContent = 'Subject';
            document.getElementById('txtSalutations').placeholder = 'Email Subject';
        }
        DropDownChanged();
    }
    else if (SelectedObj.ObjectType > 1) {
        document.getElementById('pnlItemControls').className = 'hidden';
        document.getElementById('pnlComingSoon').className = 'center';
    }
    if (CurMessageType == 3) {
        document.getElementById('txtSalutations').className = "hidden";
        document.getElementById('txtPaymentInfo').className = "hidden";
        document.getElementById('lblSalutation').className = "hidden";
        document.getElementById('lblPaymentInfo').className = "hidden";
    }
    if ((SelectedObj.ObjectType == 0 || SelectedObj.ObjectType == 1) && CurMessageType == 0) {
        document.getElementById('txtSalutations').className = "hidden";
        document.getElementById('lblSalutation').className = "hidden";
    }
}

function NewTemplate() {
    SelectedResultId = -1;
    UpdateTable();
    try {
        SelectedObj = Ajax_CallService(
       'Services/Web/Settings.asmx',
       'GetEmptyTemplate',
       {},
       false
    );
    }
    catch (err) {
    }
    document.getElementById('drpQuoteInvoice').value = '0';
    document.getElementById('txtTemplateName').value = '';
    document.getElementById('txtSalutations').value = '';
    document.getElementById('txtContentText').value = '';
    document.getElementById('txtPaymentInfo').value = '';
    document.getElementById('pnlNoResults').className = 'hidden';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('pnlComingSoon').className = 'hidden';

    //debugger
    if (CurMessageType == 3) {
        document.getElementById('txtSalutations').className = "hidden";
        document.getElementById('txtPaymentInfo').className = "hidden";
        document.getElementById('lblSalutation').className = "hidden";
        document.getElementById('lblPaymentInfo').className = "hidden";
    }
    if ((SelectedObj.ObjectType == 0 || SelectedObj.ObjectType == 1) && CurMessageType == 0) {
        document.getElementById('txtSalutations').className = "hidden";
        document.getElementById('lblSalutation').className = "hidden";
    }
    DropDownChanged();
}

function SaveTemplate() {
    SelectedObj.ObjectType = parseInt(document.getElementById('drpQuoteInvoice').value);

    SelectedObj.TemplateName = document.getElementById('txtTemplateName').value;
    SelectedObj.AdditionalText = document.getElementById('txtPaymentInfo').value;
    SelectedObj.Salutation = document.getElementById('txtSalutations').value;
    SelectedObj.BodyText = document.getElementById('txtContentText').value;
    if (CurMessageType == 3 && !ValidateSmsTemplateLength(SelectedObj.BodyText)) {
        document.getElementById('lblSmsLength').className = "center";
    } else {
        SelectedObj.MessageType = CurMessageType;
        document.getElementById('lblSmsLength').className = "hidden";

        try {
            Ajax_CallService(
                'Services/Web/Settings.asmx',
                'SaveTemplate',
                { templateObj: SelectedObj },
                false
            );
        } catch (err) {
        }
        document.getElementById('pnlItemControls').className = 'hidden';
        GetTemplateList();
    }
}

function DeleteTemplate() {
    try {
        var result = Ajax_CallService(
      'Services/Web/Settings.asmx',
      'DeleteTemplate',
      { templateObj: SelectedObj },
      false
      );
    }

    catch (err) {
    }
    if (CurList.length < 1) {
        document.getElementById('pnlNoResults').className = '';
    }
    document.getElementById('pnlItemControls').className = 'hidden';
    GetTemplateList();
}
