﻿var ObjStaffMember = null;
var ListTeamStaff = null;
var RiskId = -1;
var SelectedStaffId = -1
var StaffObjId = -1;

//function to load the staff member dialog
function Load_StaffMemberDialog(StaffId, riskId, Id) {
    RiskId = riskId;
    StaffObjId = Id;
    SelectedStaffId = StaffId;
    LoadStaffMember(Id);

    if (Id > 0) {
        GetRiskStaff(riskId);
    }
    //If Id = -1 we need a list of all staff members ready to add
    else {
        LoadStaffMembers();
    }
}

//Marks the current staff member as deleted
function RemoveStaffMember(id) {
    if (StaffObjId > 0) {
        Ajax_CallService(
            'Services/Web/RiskAssessment.asmx',
            'RiskStaffMemberRemove',
            {
                id: StaffObjId
            },
            false);
    }
}

//function to get a staff member record from the service
function LoadStaffMember(id) {
    try {
        Ajax_CallService(
        'Services/Web/RiskAssessment.asmx',
        'RiskStaffMemberGet',
        {
            id: id
        },
        true,
        DisplayStaffMember);
    }
    catch (err) {
        DisplayMessage('pnlRiskStaffMember', 'Error: Communication with ArbPro service failed');
    }
}

//function to load all staff members
function LoadStaffMembers() {
    try {
        Ajax_CallService(
        'Services/Web/Company.asmx',
        'ListAllStaff',
        {
        },
        true,
        DisplayTeamMembers);
    }
    catch (err) {
        DisplayMessage('pnlWorkTeamStaff', 'Error: Communication with ArbPro service failed');
    }
}

//gets all of the staff for the specific risk assessmet
function GetRiskStaff(riskId) {
    try {
        Ajax_CallService(
        'Services/Web/RiskAssessment.asmx',
        'RiskStaffPresent',
        {
            riskId: riskId
        },
        true,
        DisplayTeamMembers);
    }
    catch (err) {
        DisplayMessage('pnlWorkTeamStaff', 'Error: Communication with ArbPro service failed');
    }
}

//function to get all team members for the risk assessment from the service
function LoadWorkTeamMembers(selectedId, riskId) {
    try {
        Ajax_CallService(
        'Services/Web/RiskAssessment.asmx',
        'RiskWorkTeamMembers',
        {
            selectedId: selectedId,
            riskId: riskId
        },
        true,
        DisplayTeamMembers);
    }
    catch (err) {
        DisplayMessage('pnlWorkTeamStaff', 'Error: Communication with ArbPro service failed');
    }
}

//function to save a staff member
function Save_StaffMember() {
    try {
        ObjStaffMember.AssessmentId = RiskId;
        ObjStaffMember.StaffId = SelectedStaffId;
        ObjStaffMember.Id = StaffObjId;
        ObjStaffMember.StaffName = document.getElementById('txtMemberForname').value;
        ObjStaffMember.Task = document.getElementById('txtMemberTask').value;
        ObjStaffMember.Comments = document.getElementById('txtMemberComments').value;

        Ajax_CallService(
        'Services/Web/RiskAssessment.asmx',
        'RiskStaffMemberSave',
        {
            obj: ObjStaffMember
        },
        false,
        null);
    }
    catch (err) {
        DisplayMessage('pnlRiskStaffMember', 'Error: Communication with ArbPro service, Save operation failed');
    }
}

//async callback function to display a risk assessment staff member
function DisplayStaffMember(result) {
    var staffPanel = document.getElementById('pnlRiskStaffMember');
    if (!result) {
        DisplayMessage(staffPanel.id, 'Error: The ArbPro service did not return valid data');
    } else {
        try {
            ObjStaffMember = result;
            staffPanel.innerHTML = PopulateMemberPanel(ObjStaffMember);
        }
        catch (err) {
            DisplayMessage(staffPanel.id, 'Error: Unable to display Risk Assessment staff member details');
        }
    }
}

//async callback function to display selectable team members
function DisplayTeamMembers(results) {
    var teamPanel = document.getElementById('pnlWorkTeamStaff');
    if (!results) {
        DisplayMessage(teamPanel.id, 'Error: The ArbPro service did not return valid data');
    } else {
        try {
            ListTeamStaff = results;
            teamPanel.innerHTML = PopulateTeamPanel(ListTeamStaff);
        }
        catch (err) {
            DisplayMessage(teamPanel.id, 'Error: Unable to display Risk Assessment staff member details');
        }
    }
}

function PopulateMemberPanel(result) {
        var strHtml = '';
        strHtml += '<div class="width-full padded">';
        strHtml += '<input id="txtMemberForname" class="riskField" type="text" placeholder="Staff Name" title="Staff Name" name="txtMemberForname" disabled="true" value="' + result.StaffName + '"/>';
        strHtml += '<input id="txtMemberTask" class="riskField" type="text" placeholder="Task" title="Task" name="txtMemberTask" value="' + result.Task + '"/>';
        strHtml += '<textarea id="txtMemberComments" class="riskField" rows="13" placeholder="Comments" title="comments" name="txtMemberComments">' + result.Comments + '</textarea>';
        strHtml += '</div>';
    return strHtml;
}

function PopulateTeamPanel(results) {
    var strHtml = '';
    strHtml += '<div class="width-full padded">';
    strHtml += '<table class="width-full searchGrid">';
    strHtml += '<tr><th>Team Member Name</th><th>Role</th></tr>';
    if (results.length == 0) {
        strHtml += '<tr><td colspan="2">No Team members assigned</td></tr>';
    } else {
        var count = results.length;

        for (var index = 0; index < count; index++) {
            if (results[index].StaffId == null) {
                if (results[index].Id == SelectedStaffId) { strHtml += '<tr class="selected" onclick="SetSelectedTeamMember(' + index.toString() + ')">'; }
                else { strHtml += '<tr onclick="SetSelectedTeamMember(' + index.toString() + ')">'; }
            }
            else {
                if (results[index].Id == StaffObjId) { strHtml += '<tr class="selected" onclick="SetSelectedTeamMember(' + index.toString() + ')">'; }
                else { strHtml += '<tr onclick="SetSelectedTeamMember(' + index.toString() + ')">'; }
            }
            //set up table differently depending on the list returned
            if (results[index].Name == null) {
                strHtml += '<td>';
                strHtml += results[index].StaffName;
                strHtml += '</td><td>';
                strHtml += results[index].Task;
                strHtml += '</td></tr>';
            }
            else {
                strHtml += '<td>';
                strHtml += results[index].Name;
                strHtml += '</td><td>';
                strHtml += results[index].StaffRole;
                strHtml += '</td></tr>';
            }
        }
    }
    strHtml += '</table>';
    strHtml += '</div>';
    return strHtml;
}

function SetSelectedTeamMember(listIndex) {
    try {
        var selectedStaff = ListTeamStaff[listIndex]

        if (selectedStaff.StaffId != null) {
            SelectedStaffId = selectedStaff.StaffId;
            StaffObjId = selectedStaff.Id;
        }
            //If we have a list of all staff members
            //the current staff object Id is relative
            //to the SelectedStaffId
        else { SelectedStaffId = selectedStaff.Id; }

        document.getElementById('txtMemberForname').disabled = true;

        if (selectedStaff.Name == null) {
            document.getElementById('txtMemberForname').value = selectedStaff.StaffName;
        }
        else { document.getElementById('txtMemberForname').value = selectedStaff.Name; }

        if (selectedStaff.Task == null) {
            document.getElementById('txtMemberTask').value = selectedStaff.StaffRole;
        }
        else { document.getElementById('txtMemberTask').value = selectedStaff.Task; }

        if (selectedStaff.Comments != null)
            document.getElementById('txtMemberComments').value = selectedStaff.Comments;
        else
            document.getElementById('txtMemberComments').value = '';
        
        //RefreshTextBoxes&buttons
        //So we get the latest ID for
        //Delete button
        //DisplayStaffMember(ListTeamStaff);

        //refresh the table
        DisplayTeamMembers(ListTeamStaff);
    }
    catch (err) {
    }
}