﻿var ListPdfTemplates = null;   //The list of valid templates for the stage of the work flow 
var ListEmailTemplates = null;
var ListContacts = null;
var IntStage = -1;          //The stage of the work flow - Quote = 0; Work = 1; Invoice = 2; Credit = 3; Return work = 4.
var IntClientId = -1;
var IntContactId = -1;
var IntRecId = -1;          //The Id of the record to be used to generate the documents

//state flags
var ArrIsPanelShown = [false,false];

//Initialisation function
function PDFGeneration_Load(clientId, contactId, stage, recId) {
    //Set scope variables
    IntClientId = clientId;
    IntContactId = contactId;
    IntStage = stage;
    IntRecId = recId;
    //Get the contact list - this is a blocking call as is required to correctly populate the dialog
    var success = GetClientContacts(clientId);
    if (success) {
        //Intialsise the Template List
        GetPDFTemplates(IntStage);
        GetEmailTemplates(IntStage);
        GetSmsTemplates(IntStage);
    } else {
        DisplayMessage('pnlPDFError', 'Error: Unable to populate dialog failed to communicate with Arb Pro service');
    }
}

//Function to update the status of the workflow record
function UpdateStatus() {

}

//Async service call to load valid templates
function GetPDFTemplates(stage) {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
           'Services/Web/Settings.asmx',
           'GetTemplateListForTypeAndStage',
           {
               Format: 0,
               tmplType: stage
           },
           true,
           PopulatePDFPanel
       );
        
    }
    catch(err){
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

//Async service call to load valid templates
function GetEmailTemplates(stage) {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        Ajax_CallService(
           'Services/Web/Settings.asmx',
           'GetTemplateListForTypeAndStage',
           {
               Format: 1,
               tmplType: stage
           },
           true,
           PopulateEmailPanel
       );
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
    }
}

//// DGSAP-852
//function GetSmsTemplates(stage) {
//    // debugger 
//    var errPanel = document.getElementById('pnlPDFError');
//    try {
//        Ajax_CallService(
//           'Services/Web/Settings.asmx',
//           'GetTemplateListForTypeAndStage',
//           {
//               Format: 3,
//               tmplType: stage
//           },
//           true,
//           PopulateEmailPanel
//       );
//    }
//    catch (err) {
//        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
//    }
//}

//AsyncFunction to get the list of client contacts
function GetClientContacts(clientId) {
    var errPanel = document.getElementById('pnlPDFError');
    try {
        //Populate the contacts list - blocking call
        ListContacts = Ajax_CallService(
          'Services/Web/ClientData.asmx',
          'GetClientContacts',
          {
              clientID: clientId
          },
          false,
          null
        );
        //Check if the list is populated
        if (!ListContacts || ListContacts.length == 0) {
            return false;
        }
        //If here the process has completed with success
        return true;
    }
    catch (err) {
        DisplayMessage(errPanel.id, 'Error: Unable to communicate with Arb Pro service - ' + err.toString());
        return false;
    }
}

//Async service call to generate a PDF
function GeneratePDF() {
    switch(IntStage){
        case 0:
            GenerateQuotePdf();
            break;
        case 1:
            GenerateWorkPdf();
            break;
        case 2:
            GenerateInvoicePdf();
            break;
        case 3:
            GenerateCreditPdf();
            break;
        default:
            //At present do nothing - Later generate marketing
            break;
    }
}

function GenerateQuotePdf() {

}

function GenerateWorkPdf() {

}

function GenerateInvoicePdf() {

}

function GenerateCreditPdf() {

}

//Async call back to populate the PDF settings panel
function PopulatePDFPanel(results) {
    //debugger
    var panel = document.getElementById('');
    if (!results || results.length == 0) {
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
    } else {
        try {
            ListPdfTemplates = results;
            var strHtml = '';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlLetterType"> Letter Type </label>';
            strHtml += '<select id="ddlLetterType" class="lmFieldControl" onchange="ShowLetterTemplate();" name="ddlLetterType">';
            strHtml += '<option value="0">Letter</option>';
            strHtml += '<option value="1">Schedule</option>';
            strHtml += '<option value="2">Letter & Schedule</option>';
            strHtml += '</select>';
            strHtml += '<div class="lmField" style="width:100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlLetterRecipient"> Recipient </label>';
            if (IntStage == 2) {
                strHtml += '<select id="ddlLetterRecipient" class="lmFieldControl" name="ddlLetterRecipient" disabled="disabled">';
            } else {
                strHtml += '<select id="ddlLetterRecipient" class="lmFieldControl" name="ddlLetterRecipient">';
            }
            ListContacts.forEach(function (c) {
                if (c.Id == IntContactId || ListContacts.length == 1) {
                    strHtml += '<option value="' + c.Id + '" selected="selected">' + c.Name + '</option>';
                } else {
                    strHtml += '<option value="' + c.Id + '">' + c.Name + '</option>';
                }
            });
            strHtml += '</select>';
            strHtml += '</div>';
            strHtml += '<div id="pnlLetterTemplate" class="lmField" style="width: 100%;">';
            strHtml += '<label class="lmFieldLabel" for="ddlLetterTemplate"> Letter Text Template </label>';
            strHtml += '<select id="ddlLetterTemplate" class="lmFieldControl" onchange="LetterTemplateSelected();" name="ddlLetterTemplate">';
            ListPDFTemplates.forEach(function(tmp){
                strHtml += '<option value="' + tmp.Id + '">' + tmp.TemplateName +'</option>';
            });
            strHtml += '</select>';
            //strHtml += '<input id="txtLetterSalutation" class="lmFieldControl" type="text" style="font-family:Helvetica;font-size:Small;" name="txtLetterSalutation">';
            strHtml += '<textarea id="txtLetterText" class="lmFieldText" style="font-family:Helvetica;font-size:Small;" cols="20" rows="2" name="txtLetterText"></textarea>';
            if(IntStage == 2){
                strHtml += '<textarea id="txtPaymentDetails" class="lmFieldText" style="font-family: Helvetica; font-size: small; display: none;" cols="20" rows="2" name="txtPaymentDetails"></textarea>';
            }
            strHtml += '</div>';
            strHtml += '<div class="lmField" style="height:30px;width:100%;">';
            strHtml += '<input id="btnPrintLetter" class="button-basic float-right" type="submit" style="margin-right: 10px;" onclick="LoadPDF(false); return false;" value="Generate PDF" name="btnPrintLetter">';
            strHtml += '</div>';
            strHtml += '</div>';
            
            panel.innerHTML = strHtml;
        }
        catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display PDF settings - ' + err.toString());
        }
    }
}

//Async callback to populate the email settings panel
function PopulateEmailPanel(results) {
    var panel = document.getElementById('');
    if (!results) {
        DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
    } else {
        try {

        }
        catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display Email settings - ' + err.toString());
        }
    }
}



