﻿/*

    The following functions facilitate the creation and editing of work bookings.
    There are two initialisation functions, one to enable the edit of a current work plan and the second
    to enable the creation of a new work plan.

    This file uses some of the Global scope variables defined within the WorkPlanBooking. js file.
    This file should only be loaded from the WorkPlanBooking.js file.
*/
//Panel names
var PnlError = 'pnlDlgClndrError';
var PnlItmError = 'pnlDlgClndrItmError';    // DGSAP-841 - way to use the extra panel added to calendarbookwork.aspx
var PnlContent = 'pnlDlgClndrAppointment';
//Scope variables
var ObjWorkPlan = null; // The selected work plan. This will be null if there is no work plan
var ObjApp = null;      // The appointment for the selected work plan
var SelectedJobSite = -1;   // This is the default site loaded 
var ListTools = [];     // The array tools
var ListStaff = [];     // The array selected staff
var ListTeams = [];     // The array of Teams
var SelectedDate = new Date(); // The date for the appointment

var SelectedStaff = [];
var SelectedTools = [];
var SelectedTeamId = -1;

var EditingQuoteNo = '';

var isForEdit = false;
var isForQuoteTools = false;
var wpId = -1;

// restrict time selection range
var g_minTimeForBookingWork = '7:00';
var g_maxTimeForBookingWork = '19:00';

/*

        **********  Initialisation and Service calls   **********

*/

//      Initialisation function
function CalendarBookWork_WorkPlan(workPlanId, siteId, isEdit) {
    console.log("workPlanId:" + workPlanId + "siteId:" + siteId + "isEdit" + isEdit);
    if (isEdit) {
        isForEdit = isEdit;
    }
    //Tools_Load();
    Teams_Load();
    SelectedJobSite = siteId;
    WorkPlan_Load(workPlanId);
}

//      Initialisation for a new work plan and appoinment
function CalendarBookWork_New(date, workPlanId, siteId) {
    SelectedDate = date;
    CalendarBookWork_WorkPlan(workPlanId, siteId);
}

//      Async service call to load all tools into local memory
function Tools_Load() {
    try {
        Ajax_CallService(
               'Services/Web/WorkProgramming.asmx',
               'GetToolListAsWorkTools',
               {
               },
               true,
               ShowEditToolsDivCallBack
           );
    } catch (err) {
        DisplayMessage(PnlError, 'Error: Unable to communicate with Arb Pro server.');
    }
}

// function to generate a tools panel only upon selecting the Edit Tools button within the quote stage of a job.
function GenerateToolsPanel() {
    // panel contains only the tools section of a work plan
    var strHtml = '<div class="width-full float-left">';
    strHtml += '<div class="width-full float-left height-full">';
    strHtml += '<div class="dlgBookWorkContainer width-full float-left" style="height: 530px;">';

    strHtml += '<div id="pnlWorkScheduleDetailContent" class="float-left width-full height-full scroll-vertical" style="max-height: 98%;">';

    strHtml += '<div id="pnlScheduleDetail_1">';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '</div>';
    strHtml += '</div>';
    document.getElementById(PnlContent).innerHTML = strHtml;
    Tools_Load();
}

// function to get the work plan id 
function GetWpId(qId, jsId) {
    isForQuoteTools = true;
    try {
        wpId = Ajax_CallService(
            'Services/Web/Report.asmx',
            'GetWorkPlanId',
            {
                qId: qId,
                jsId: jsId
            },
            false,
            null
        );
    } catch (err) {
        document.getElementById('pnlDlgClndrAppointment').className = "hidden";
        DisplayMessage(PnlError, 'Error: Unable to communicate with Arb Pro server.');
    }
    WorkPlan_Load(wpId);
}


//      Async service call to load all tools into local memory
function Teams_Load() {
    try {
        Ajax_CallService(
        'Services/Web/WorkProgramming.asmx',
        'GetAllTeam',
        {},
        true,
        function (results) {
            ListTeams = results;
        }
        );

    } catch (err) {
        DisplayMessage(PnlError, 'Error: Unable to communicate with Arb Pro server.');
    }
}

//      Async function to load a work plan from the service - Loads a default new object if the Id is -1
function WorkPlan_Load(workPlanId) {
    //It is possible that this could fail
    try {
        Ajax_CallService(
            'Services/Web/WorkProgramming.asmx',
            'GetWorkPlan',
            {
                id: workPlanId,
                includeTools: true
            },
            true,
            function (result) {

                WorkPlanAppointment_Load(result);
            }
       );
    }
    catch (err) {
        DisplayMessage(PnlError, 'ERROR: Unable to conect to Arb Pro service - ' + err.toString());
    }
}

//      Async service call to load an appointment for a work plan
function WorkPlanAppointment_Load(workPlan) {
    debugger;
    //It is possible that this could fail
    if (!workPlan) {
        //The service call to get a work plan failed display a message and halt
        DisplayMessage(PnlError, 'Error: The Arb Pro service did not return valid data.');
    } else {
        // checks if it is for the Edit Tools selection from the quote stage
        if (isForQuoteTools) {
            // if so then only select the tools from that work plan and pass it to the specialized tools panel
            ObjWorkPlan = workPlan;
            SelectedTools = ObjWorkPlan.Tools;
            ListTools = ObjWorkPlan.Tools;
            GenerateToolsPanel();
        } else {
            //Set the gloabal scope variable
            ObjWorkPlan = workPlan;
            //  Check if the work plan is a new plan and populate properties if required
            //  NOTE: We can only populate
            if (ObjWorkPlan.Id < 1 || ObjWorkPlan.WorkId == ObjWorkSummary.Id || ObjWorkPlan.QuoteId == ObjWorkSummary.QuoteId) {
                //  A new work plan has been created
                if (ObjWorkPlan.JobSiteId < 1) {
                    //  NOTE: We may have loaded a work plan for another site
                    ObjWorkPlan.JobSiteId = SelectedJobSite;
                }
                //Set the selected quote reference no
                EditingQuoteNo = ObjWorkSummary.QuoteNo;

                ObjWorkPlan.WorkId = ObjWorkSummary.Id;
                ObjWorkPlan.QuoteId = ObjWorkSummary.QuoteId;
            } else {
                EditingQuoteNo = ObjWorkPlan.QuoteReference;
            }
            //  Populate the selected items arrays from the loaded work plan
            SelectedStaff = ObjWorkPlan.Personnel;
            SelectedTools = ObjWorkPlan.Tools;
            SelectedTeamId = ObjWorkPlan.TeamId;
            //  Initialise the main lists for comparison
            //  These will be overwritten if the user edits staff or tools
            ListStaff = ObjWorkPlan.Personnel;
            ListTools = ObjWorkPlan.Tools;
            //Load the appointment - A new appointment will be created if the id passed to the service is -1
            try {
                Ajax_CallService(
                    'Services/Web/Calendar.asmx',
                    'GetAppointmentForWorkPlan',
                    {
                        workPlanId: workPlan.Id
                    },
                    true,
                    DisplayAppointment
                );
            } catch (err) {
                DisplayMessage(PnlError, 'ERROR: Unable to conect to Arb Pro service - ' + err.toString());
            }
        }
    }
}

//      Async function to load the staff from the service
function ShowEditStaffDiv() {
    var panel = document.getElementById('pnlScheduleDetail_2');
    panel.innerHTML = GetWaitImageForPanel('pnlScheduleDetail_2', 'Loading Staff. Please wait.');

    Ajax_CallService(
            'Services/Web/WorkProgramming.asmx',
            'GetStaffListAsTeamStaff',
            {},
            true,
            ShowEditStaffDivCallBack
   );
}

/*

        **********  Service Callbacks   **********

*/

//      Async service callback to display a work plan
function DisplayAppointment(result) {
    var panel = document.getElementById('');
    try {
        //  Check if the service returned a valid object
        if (!result) {
            DisplayMessage(PnlError, 'Error: Arb Pro service did not return valid data.');
        } else {
            //  Populate the global scope variable
            ObjApp = result;
            var startDate;
            var strStartDate = '';

            var endDate;
            var strEndDate = '';

            //Set the scope variable
            ObjApp = result;
            //Check if the appointment is a new one
            if (ObjApp.Id < 1) {
                //populate the start date
                startDate = SelectedDate;
                startDate.setHours(9, 0, 0, 0);
                strStartDate = startDate.format('dd/MMM/yy HH:mm');
                //Default the end date to the same date
                endDate = SelectedDate;
                endDate.setHours(17, 0, 0, 0);
                strEndDate = endDate.format('dd/MMM/yy HH:mm');
            } else {
                startDate = toDateFromJson(ObjApp.StartDateTime);
                strStartDate = startDate.format('dd/MMM/yy HH:mm');
                endDate = toDateFromJson(ObjApp.EndDateTime);
                strEndDate = endDate.format('dd/MMM/yy HH:mm');
            }
            //Generate the html for the panel
            var strHtml = '<div class="width-full float-left">';
            //  Display the Quote No of the work plan being edited
            //strHtml += '';
            strHtml += '<div id="pnlAppointmentDates" class="width-full float-left">';
            strHtml += '<div class="dlgBookWorkHeader"><span style="color:white;">Quote No: ' + EditingQuoteNo + '</span></div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Start Date/Time</label>';
            strHtml += '<input id="txtDateFrom" type="text" class="dlgBookWorkInput" placeholder="Start Date and Time" value="' + strStartDate + '" name="txtDateFrom">';
            strHtml += '</div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">End Date/Time</label>';
            strHtml += '<input id="txtDateTo" type="text" class="dlgBookWorkInput" placeholder="End Date and Time" value="' + strEndDate + '" name="txtDateTo">';
            strHtml += '</div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Skip Days</label>';
            strHtml += '<select id="ddlSkipDays" title="Days not included as work days" class="dlgBookWorkInput">';
            strHtml += '<option value="0" >None</option>';
            strHtml += '<option value="1">Saturday</option>';
            strHtml += '<option value="2">Sunday</option>';
            strHtml += '<option value="3" selected="selected">Weekends</option>';
            strHtml += '<option value="4">Other</option>';
            strHtml += '</select>';
            strHtml += '</div>';
            //Add the time required field
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Time Required (hrs)</label>';
            strHtml += '<input id="txtTime" type="text" class="dlgBookWorkInput" placeholder="The estimated time, in hours, required to complete the job." value="' + ObjWorkPlan.TimeRequired.toString() + '" name="txtTime">';
            strHtml += '</div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Site</label>';
            strHtml += '<select id="ddlWorkSite" class="dlgBookWorkInput" style="width:65%;" name="ddlWorkSite"  onchange="SelectedSiteChanged();">';
            //Check if there is a site selected
            if (ObjWorkPlan.JobSiteId > 0) {
                //The work plan has a job site - Prevent selection to reduce potential confusion
                strHtml += '<option value="' + ObjWorkPlan.JobSiteId + '" selected="selected">' + ObjWorkPlan.SiteAddress + '</option>';
            } else {
                //The work plan has no job site selected
                if (ObjWorkPlan.JobSiteId == -1 && ObjWorkSummary.JobSites.length > 1) {
                    strHtml += '<option value="-1" selected="selected"> Select Site to Book Work</option>';
                } else if (ObjWorkSummary.JobSites.length > 1) {
                    strHtml += '<option value="-1"> Select Site to Book Work</option>';
                }
                ObjWorkSummary.JobSites.forEach(function (js) {
                    strHtml += '<option value="' + js.Id + '"';
                    if (ObjWorkPlan.JobSiteId == js.Id || ObjWorkSummary.JobSites.length == 1) {
                        strHtml += ' selected="selected"';
                    }
                    strHtml += '>' + js.SiteString + '</option>';
                });
            }
            strHtml += '</select>';
            strHtml += '</div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Team</label>';
            strHtml += '<label id="lblTeamName" class="dlgBookWorkInput">';
            if (ObjWorkPlan.TeamName.length == 0) {
                //No team has been selected
                strHtml += 'Not Set';
            } else {
                strHtml += ObjWorkPlan.TeamName;
            }
            strHtml += '</label></div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Personnel</label>';
            if (ObjWorkPlan.StaffRequired > 0) {
                strHtml += '<label id="lblStaffRequired" class="dlgBookWorkInput">' + ObjWorkPlan.Personnel.length.toString() + ' / ' + ObjWorkPlan.StaffRequired.toString() + '</label>';
            } else {
                strHtml += '<label id="lblStaffRequired" class="dlgBookWorkInput">' + ObjWorkPlan.Personnel.length.toString() + '</label>';
            }
            strHtml += '</div>';
            strHtml += '<div class="width-full float-left height-full">';
            strHtml += '<div class="dlgBookWorkContainer width-full float-left" style="height: 270px;">';
            //Set up the tabs for the summary details
            strHtml += '<div id="pnlWorkDetailTabs" class="bookWorkTabBar width-full">';
            strHtml += '<a id="pnlTabPlan_3" class="jobTab activeTab" href="javascript:SwitchScheduleDetailsPanel(3);"> Items </a>';
            strHtml += '<a id="pnlTabPlan_2" class="jobTab" href="javascript:SwitchScheduleDetailsPanel(2);"> Team </a>';
            strHtml += '<a id="pnlTabPlan_1" class="jobTab" href="javascript:SwitchScheduleDetailsPanel(1);"> Tools </a>';
            strHtml += '<a id="pnlTabPlan_0" class="jobTab" href="javascript:SwitchScheduleDetailsPanel(0);"> Comments </a>';
            strHtml += '<a id="pnlTabPlan_4" class="jobTab" href="javascript:SwitchScheduleDetailsPanel(4);"> Notes </a>';
            strHtml += '</div>';
            //Set up the containers for the summary details
            strHtml += '<div id="pnlWorkScheduleDetailContent" class="float-left width-full height-full scroll-vertical" style="max-height: 77%;">';

            //Comments panel
            strHtml += '<div id="pnlScheduleDetail_0" class="hidden">';
            strHtml += '<textarea autofocus id="txtApptComments" class="dlgBookWorkInput" cols="100" rows="9" style="width: 601px; height: 234px;" name="txtApptComments">' + ObjApp.Comments + '</textarea>';
            strHtml += '</div>';

            //Tools panel
            strHtml += '<div id="pnlScheduleDetail_1" class="hidden">';
            strHtml += '<table class="waitTimer"><tr><td>';
            strHtml += 'Loading Tools. Please Wait';
            strHtml += '</td></tr><tr><td class="width-full">';
            strHtml += '<img id="waitImg_pnlScheduleDetail_1" class="padded" src="Images/load_Medium.gif">';
            strHtml += '</td></tr></table>';
            strHtml += '</div>';


            //Staff panel
            strHtml += '<div id="pnlScheduleDetail_2" class="hidden">';
            strHtml += GenerateStaffHtml(ObjWorkPlan.Personnel);
            strHtml += '</div>';

            //Item panel
            strHtml += '<div id="pnlScheduleDetail_3" class="">';
            strHtml += '<table class="waitTimer"><tr><td>';
            strHtml += 'Sorting Work Items. Please Wait';
            strHtml += '</td></tr><tr><td class="width-full">';
            strHtml += '<img id="waitImg_pnlScheduleDetail_3" class="padded" src="Images/load_Medium.gif">';
            strHtml += '</td></tr></table>';
            strHtml += '</div>';

            //Notes panel
            strHtml += '<div id="pnlScheduleDetail_4" class="hidden">';
            strHtml += '</div>';

            //Closs the summary div
            strHtml += '</div>';
            strHtml += '</div>';
            //Close the containing div
            strHtml += '</div>';

            //Add the html to the panel
            document.getElementById(PnlContent).innerHTML = strHtml;

            //Load the tools
            Tools_Load();

            //Ensure the correct SkipDays item is selected
            if (ObjApp.SkipDays != null && ObjApp.SkipDays >= 0) {
                var ddlSkip = document.getElementById('ddlSkipDays');
                ddlSkip.selectedIndex = ObjApp.SkipDays;
            }

            //Now that the html elements exist on the page - the ajax binding for the date time picker can be added

            SelectedSiteChanged();


            GenerateNoteHtml();

            //Change the dates to ticks to add to the date pickers
            $('#txtDateFrom').datetimepicker({
                start: toTicksFromDate(startDate),
                format: 'd/M/y H:i',
                timepicker: true,
                minDate: 0,
                minTime: g_minTimeForBookingWork,
                maxTime: g_maxTimeForBookingWork
            });

            if (strEndDate == '') {
                $('#txtDateTo').datetimepicker({
                    start: toTicksFromDate(startDate), format: 'd/M/y H:i', timepicker: true, minDate: 0,
                    minTime: g_minTimeForBookingWork,
                    maxTime: g_maxTimeForBookingWork
                });
            } else {
                $('#txtDateTo').datetimepicker({
                    start: toTicksFromDate(endDate), format: 'd/M/y H:i', timepicker: true, minDate: 0,
                    minTime: g_minTimeForBookingWork,
                    maxTime: g_maxTimeForBookingWork
                });
            }
        }
    }
    catch (err) {
        DisplayMessage(PnlError, 'ERROR: Unable to display appointment details - ' + err.toString());
        //The error message is not displayed in the content panel so ensure the wait timer is removed
        DisplayMessage(PnlContent, '');
    }
}

//      Async service callback to display the staff list
function ShowEditStaffDivCallBack(results) {
    var panel = document.getElementById('pnlScheduleDetail_2');
    try {
        if (!results || results.length == 0) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid list of staff.');
        } else {
            //Set the scope variable
            ListStaff = results;
            //Set the panels inner html to be the generated html;
            panel.innerHTML = GenerateStaffHtml(ListStaff);
            //Set the staff that have already been selected
            SetSelectedStaff();
        }
    } catch (err) {
        DisplayMessage(panel.id, 'ERROR: Unable to display staff selection - ' + err.toString());
    }

}

//      Async service callback to display the tools list
function ShowEditToolsDivCallBack(results) {
    var panel = document.getElementById('pnlScheduleDetail_1');
    try {
        if (!results || results.length == 0) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid list of tools.');
        } else {
            //  Set the scope variable
            ListTools = results;
            //  Set the panels inner html to be the generated html;
            panel.innerHTML = GenerateToolHtml(ListTools);
            //  Now the list has been created and display, set the selected tools
            SetSelectedTools();
        }
    } catch (err) {
        DisplayMessage(panel.id, 'ERROR: Unable to display tool selection - ' + err.toString());
    }
}

/*********************************************************************************************************

                                            END INITIALISATION FUNCTIONS

*********************************************************************************************************/


/*********************************************************************************************************

                                            FINALISATION FUNCTIONS

*********************************************************************************************************/
//      Function to save a work plan and an appointment. Returns completed status.
//      This can return true and so must be handled in calling code and not returned to the ASPX page,
//      If not handled an invalid postback exception could be raised
function CalendarBookWork_Save() {
    var retVal = false;
    try {
        //The work plan can only have the job site set if it is for a new plan
        if (ObjWorkPlan.Id < 0 || ObjWorkPlan.JobSiteId) {
            ObjWorkPlan.JobSiteId = GetSelectedDdlElementValue('ddlWorkSite');
        }
        //The work plan object may have now changed
        //Get the team Id
        ObjWorkPlan.TeamId = GetSelectedDdlElementValue('ddlTeam');
        ObjWorkPlan.Tools = GetSelectedTools();
        ObjWorkPlan.Personnel = GetSelectedStaff();
        ObjWorkPlan.ScheduledItems = GetSelectedItems();
        ObjWorkPlan.TimeRequired = parseFloat(document.getElementById('txtTime').value);
        ObjWorkPlan.StartDate = null;
        //Populate the appointment object
        ObjApp.StartDateTime = document.getElementById('txtDateFrom').value;
        ObjApp.EndDateTime = document.getElementById('txtDateTo').value;
        ObjApp.Comments = document.getElementById('txtApptComments').value;
        //var ddlSkip = document.getElementById('ddlSkipDays');
        ObjApp.SkipDays = GetSelectedDdlElementValue('ddlSkipDays');

        var bookdt = [];
        var BD = {
            BookDate: document.getElementById('txtDateFrom').value
        };
        bookdt.push(BD);
        ObjApp.StartTimeDay1 = "9:00";
        ObjApp.ConnectiveDayEndTime = "17:00";
        ObjApp.LastDayEndTime = "14:00";
        ObjApp.CalendarWorkDate = bookdt;

        if (ValidateWorkPlan()) {
            //Set the display for the wait timer
            var panel = document.getElementById('pnlDlgClndrAppointment');
            panel.innerHTML = GetWaitImageForPanel(panel.id, 'Saving Appointment details please wait.');
            retVal = UpdateWorkSchedule(ObjWorkPlan, ObjApp);
        } else {
            DisplayMessage(PnlError, 'INFORMATION: The work plan cannot be saved. Please ensure that Staff and Tools have been assigned and that you have at least 1 item selected.', 'red');
            retVal = false;
        }

    }
    catch (err) {
        DisplayMessage(PnlError, 'ERROR: Unable to save the work schedule - ' + err.toString());
    }
    return retVal;
}

// function to save only the edits to the tools list that were made from the Edit Tools in the quote section of a job.
function ToolsSave() {
    if (ObjWorkPlan) {
        ObjWorkPlan.Tools = GetSelectedTools();
        try {
            var value = Ajax_CallService(
                'Services/Web/WorkProgramming.asmx',
                'SaveQuoteTools',
                {
                    workId: wpId,
                    toolsList: ObjWorkPlan.Tools
                },
                false,
                null
            );


            return value;
        } catch (err) {
            return false;
        }
    } else {
        DisplayMessage(PnlError, 'ERROR: Unable to save the tools list. Please ensure that you have more than 1 tool added to the list.');
        return false;
    }
}

//      Function to delete a work plan and all attached data
function CalendarBookWork_Delete() {
    var retVal = false;
    try {
        if (ObjWorkPlan.Id > 0) {
            retVal = DeleteWorkAppointment(ObjWorkPlan.Id);

            if (!retVal) {
                DisplayMessage(PnlError, 'INFORMATION: Failed to delete appointment');
            }
        }

    }
    catch (err) {
        DisplayMessage(PnlError, 'ERROR: Unable to delete the work appointment - ' + err.toString());
    }
    return retVal;
}

//      Function to update a work schedule
function UpdateWorkSchedule(workPlan, appointment) {
    try {

        var value = Ajax_CallService(
                'Services/Web/WorkProgramming.asmx',
                'SaveWorkPlan',
                {
                    workPlan: workPlan,
                    appointment: appointment
                },
                    false,
                    null
                );


        return value;
    }
    catch (err) {
        return false;
    }
}

//      function to delete a WorkPlan and all of its associated items
function DeleteWorkAppointment(workPlanId) {
    try {
        var value = Ajax_CallService(
                'Services/Web/WorkProgramming.asmx',
                'DeleteWorkPlan',
                {
                    workPlanId: workPlanId
                },
                    false,
                    null
                );


        return value;
    }
    catch (err) {
        return false;
    }
}

/*

        **********  Local Functions   **********

*/

//      Function to display the selected tools div
function ShowEditToolsDiv() {
    var panel = document.getElementById('pnlScheduleDetail_1');
    panel.innerHTML = GetWaitImageForPanel('pnlScheduleDetail_1', 'Loading Tools. Please wait.');
    Tools_Load();
}

//      Function to change the CSS of the tab panels
function SwitchScheduleDetailsPanel(selectedIndex) {
    // if panel is being used then check if it is on items page, 
    // if not, check if all of the work items are selected, if not display message to client
    var brk = false;
    var itmErr = document.getElementById('pnlDlgClndrItmError');
    if (itmErr) {
        if (selectedIndex != 3) {
            ObjWorkSummary.WorkItems.forEach(function (itm) {
                if (!brk) {
                    var chkItm = document.getElementById('chkSelectItem_' + itm.Id);
                    if (chkItm && !chkItm.checked) {
                        itmErr.className = "warning-text center";
                        DisplayMessage(PnlItmError, 'You have not selected to book all of the work items, are you sure you want to continue?', 'red');
                        brk = true;
                    }
                }
            });
        } else {
            itmErr.className = "hidden";
        }
    }

    for (var index = 0; index < 5; index++) {
        var tab = document.getElementById('pnlTabPlan_' + index.toString());
        var panel = document.getElementById('pnlScheduleDetail_' + index.toString());
        if (index == selectedIndex) {
            tab.className = 'jobTab activeTab';
            panel.className = '';
        } else {
            tab.className = 'jobTab';
            panel.className = 'hidden';
        }
    }
}

//      Function to handle the item selection changed event for the Team picker
function SelectedTeamChanged() {
    //  Default to load the staff from the team
    var ddlT = document.getElementById('ddlTeam');
    var lblTeam = document.getElementById('lblTeamName');
    var teamId = ddlT.options[ddlT.selectedIndex].value;
    if (teamId > 0) {
        //  Update the global scope variable
        SelectedTeamId = teamId;
        //  Find the selected team
        var selectedTeams = $.grep(ListTeams, function (e) { return e.Id == teamId; });
        if (selectedTeams && selectedTeams.length > 0) {
            //  A valid team has been selected default to the first in the array. 
            //  NOTE: THERE SHOULD ONLY BY ONE MATCH
            SelectedStaff = selectedTeams[0].Personnel;
            //Check if the staff list has been populated
            if (ListStaff.length < SelectedStaff.length) {
                if (ListStaff.length == 0) {
                    ListStaff = SelectedStaff;
                }
                else {
                    var count = ListStaff.length
                    SelectedStaff.forEach(function (s) {
                        var found = false;
                        for (var i = 0; i < count; i++) {
                            if (ListStaff[i].StaffId == s.StaffId) {
                                //The item is already in the list
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            //The staff member is not currently in the list so add it
                            ListStaff.push(s);
                        }
                    });
                }
            }
            lblTeam.textContent = selectedTeams[0].Name;
            document.getElementById('pnlScheduleDetail_2').innerHTML = GenerateStaffHtml(SelectedStaff);
            UpdatePersonelLabel(SelectedStaff.length);
        } else {
            UpdatePersonelLabel(0);
        }
    }
}

function GenerateNoteHtml() {
    var qNotes = GetAllQuoteOrWorkNotes(ObjWorkPlan.QuoteId, 0);
    var wNotes = GetAllQuoteOrWorkNotes(ObjWorkPlan.WorkId, 1);
    var panel = document.getElementById('pnlScheduleDetail_4');

    var strHtml = '<table class="rtGrid width-full"><col width="30%" /><col width="68%" />';

    strHtml += '<tr><td colspan="2" class="rowHeader"></td></tr>';
    strHtml += '<tr><td class="rowHeader">Type</td><td class="rowHeader">Text</td></tr>';
    //  Iterate through all the tools and create a row in the table
    qNotes.forEach(function (q) {
        strHtml += '<tr>';
        strHtml += '<td class="rowData">Quote</td>';
        strHtml += '<td class="rowData">' + q.Text + '</td></tr> ';
    });

    wNotes.forEach(function (w) {
        strHtml += '<tr>';
        strHtml += '<td class="rowData">Work</td>';
        strHtml += '<td class="rowData">' + w.Text + '</td></tr> ';
    });
    strHtml += '</table>';

    panel.innerHTML = strHtml;
}

function GetAllQuoteOrWorkNotes(parentId, type) {
    var notes = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetNotesList',
        {
            parentId: parentId,
            type: type
        },
        false
    );

    return notes;
}

//      Function to generate the html for the staff panel
function GenerateStaffHtml(staffList) {
    var strHtml = '<table class="width-full searchGrid height-full">';
    strHtml += '<tr><th colspan="3">';
    //  Due to the changes in the booking process the user must now be able to change the team
    strHtml += '<select id="ddlTeam" class="dlgBookWorkInput padded" style="width:65%;" name="ddlTeam" onchange="SelectedTeamChanged();">';
    //Check if there is a team selected
    if (ObjWorkPlan.TeamId == -1) {
        strHtml += '<option value="-1" selected="selected"> Select Team for Work</option>';
    } else {
        strHtml += '<option value="-1"> Select Team for Work</option>';
    }
    ListTeams.forEach(function (t) {
        strHtml += '<option value="' + t.Id + '"';
        if (t.Id == SelectedTeamId) {
            strHtml += ' selected="selected"';
        }
        strHtml += '>' + t.Name + '</option>';
    });
    strHtml += '</select>';
    strHtml += '<button type="button" onclick="ShowEditStaffDiv();" class="button-thin float-right padded">Edit Staff</button></th>';
    strHtml += '<tr><th>Name</th><th>Assigned</th><th>Role</th></tr>';
    staffList.forEach(function (c) {
        var curId = -1;
        if (!c.StaffId) {
            curId = c.Id;
        } else {
            curId = c.StaffId;
        }
        strHtml += '<tr onclick="SetStaffSelectedClicked(' + curId + ')">';
        strHtml += '<td style="width: 50%; padding-left: 10px;">';
        if (!c.Name) {
            strHtml += c.StaffName + '</td>';
        } else {
            strHtml += c.Name + '</td>';
        }
        var foundSelected = $.grep(SelectedStaff, function (e) { return e.StaffId == curId; });
        strHtml += '<td style="text-align:center;"> <input type="checkbox" Id="chkTeamStaff_' + curId + '"';
        if (foundSelected && foundSelected.length == 1) {
            strHtml += 'checked';
        }
        strHtml += '/>  </td> ';
        strHtml += '<td class="companyInput"><input type="text" Id="txtTeamTask_' + curId + '" placeholder="Staff Role" title="The role within the team for the staff member"';
        if (foundSelected && foundSelected.length == 1 && foundSelected[0].Task && foundSelected[0].Task.length > 0) {
            strHtml += 'value="' + foundSelected[0].Task + '"';
        }
        strHtml += '/>  </td> ';
        strHtml += '</tr>';
    });
    strHtml += '</table>';
    return strHtml;
}

//      Function to generate the html for the item panel
function GenerateItemHtml(siteId) {
    var strHtml = '<table class="width-full searchGrid height-full">';//<col width="10%" /><col width="15%" /><col width="65%" /><col width="15%" />';
    strHtml += '<tr><th colspan="5">Select Items to be assigned to the Work Plan</th></tr>';
    strHtml += '<tr><th style="vertical-align: top; text-align: center;">Code</th><th style="vertical-align: top; text-align: center;">Name</th>';
    strHtml += '<th style="vertical-align: top; text-align: center;">Description</th><th style="vertical-align: top; text-align: center;">Value (' + CurrentCurrency + ')</th><th onclick="SelectAllPendingWork();" style="vertical-align: top; text-align: center;">Assign all</br><input type="checkbox" id="chkAllWork"/>';
    strHtml += '</th></tr>';
    var pendingWorkItems = $.grep(ObjWorkSummary.WorkItems, function (e) { return e.JobSiteId == siteId && !e.Booked && !e.Completed && e.Status != 99; });
    itemSort(pendingWorkItems);
    if (pendingWorkItems && pendingWorkItems.length == 0 && ObjWorkPlan.ScheduledItems.length == 0) {
        //There are currently no booked items for the work plan and all work has been booked
        strHtml += '<tr>';
        strHtml += '<td  colspan="4" style="vertical-align: top; text-align: center;">All work has been booked</br>Please delete this Work Plan.</td>';
        strHtml += '</tr>';
    } else {
        //  Display unbooked items
        pendingWorkItems.forEach(function (wi) {
            strHtml += '<tr>';
            strHtml += '<td style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Name + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Description + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Value.format() + '</td>';
            strHtml += '<td style="vertical-align:center text-align: center; cursor: default;"><input id="chkSelectItem_' + wi.Id + '" type="checkbox" style="cursor: pointer;"/></td>';
            strHtml += '</tr>';
        });
        //  Display items to the selected work plan
        ObjWorkPlan.ScheduledItems.forEach(function (wi) {
            strHtml += '<tr>';
            strHtml += '<td style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Name + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Description + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Value.format() + '</td>';
            strHtml += '<td style="vertical-align:center text-align: center; cursor: default;"><input id="chkSelectItem_' + wi.Id + '" type="checkbox" style="cursor: pointer;" checked/></td>';
            strHtml += '</tr>';
        });
    }
    strHtml += '</table>';
    return strHtml;
}

//      Function to generate the html for the tools panel
function GenerateToolHtml(toolList) {
    var strHtml = '<table class="rtGrid width-full white" style="table-layout:fixed; word-wrap:break-word;">';
    //strHtml += '<tr><th colspan="2"><button type="button" onclick="ShowEditToolsDiv();" class="button-thin float-right padded">Edit Tools</button></th></tr>';

    // if its for the tools selection from quote screen change the message to be more precise.
    if (isForQuoteTools) {
        strHtml += '<tr><th colspan="2">Input the number required against a tool, to edit the amount required.</th></tr>';
    } else {
        strHtml += '<tr><th colspan="2">Input the number required against a tool, to add it to the work plan.</th></tr>';
    }
    strHtml += '<tr><th>Tool</th><th>Number Required</th></tr>';
    //  Iterate through all the tools and create a row in the table
    toolList.forEach(function (c) {
        strHtml += '<tr>';
        strHtml += '<td class="rowData" style="maxwidth: 60%; padding-left: 10px;">' + c.Item + '</td>';
        strHtml += '<td class="rowData"><input type="text" Id="txtToolNum_' + c.ToolId + '" placeholder="Add a value to select" title="The number of tools required for the work."/>  </td></tr> ';
    });
    strHtml += '</table>';
    return strHtml;
}

//      Function to handle the checked changed event of a staff row entry
function SetStaffSelectedClicked(staffId) {
    var chkBox = document.getElementById('chkTeamStaff_' + staffId.toString());
    chkBox.checked = !chkBox.checked;
    //  Update the numer of personel displayed
    var personel = 0;
    //iterate through each staff member and check if the chkBox is selected 
    ListStaff.forEach(function (p) {
        var chkBox = document.getElementById('chkTeamStaff_' + p.StaffId.toString());
        if (chkBox && chkBox.checked) {
            personel++;
        }
    });
    UpdatePersonelLabel(personel);
}

//      Function to handle the checked changed event for Assign All work items
function SelectAllPendingWork() {
    var selected = document.getElementById('chkAllWork').checked;
    ObjWorkSummary.WorkItems.forEach(function (itm) {
        var chkItm = document.getElementById('chkSelectItem_' + itm.Id);
        if (chkItm) {
            chkItm.checked = selected;
        }
    });
    // check against the work plan array as well - as the items may be for a different quote
    ObjWorkPlan.ScheduledItems.forEach(function (itm) {
        var chkItm = document.getElementById('chkSelectItem_' + itm.Id);
        if (chkItm) {
            chkItm.checked = selected;
        }
    });
}

//      Function to display the items for a site when the selected site changes
function SelectedSiteChanged() {
    var panel = document.getElementById('pnlScheduleDetail_3');
    //Display a wait symble while the items are sorted for the selected work site
    panel.innerHTML = GetWaitImageForPanel(panel.id, 'Sorting Work Items. Please wait.');
    //Get the site selection element
    var siteId = GetSelectedDdlElementValue('ddlWorkSite');
    if (siteId > 0) {
        // a valid site has been selected
        panel.innerHTML = GenerateItemHtml(siteId);
    } else {
        DisplayMessage(panel.id, 'There are no unscheduled items for the selected site.');
    }

    // DGSAP-841 - way to set the 1st work item to always be checked upon creating or editting a work plan.
    // loop that checks only for the first item for every new job site and selects it.
    var newSite = 0;
    var chked = false;
    ObjWorkSummary.WorkItems.forEach(function (itm) {
        var chkItm = document.getElementById('chkSelectItem_' + itm.Id);
        if (chkItm && chkItm.checked) {
            chked = true;
        }
    });
    if (!isForEdit) {
        // ObjWorkSummary.WorkItems is an array that contains work items for all job sites so we need to check the first one for each site.
        ObjWorkSummary.WorkItems.forEach(function (itm2) {
            var chkItm3 = document.getElementById('chkSelectItem_' + itm2.Id);
            if (newSite != itm2.JobSiteId && !itm2.Booked) {
                newSite = itm2.JobSiteId;
                if (chkItm3) {
                    chkItm3.checked = true;
                }
            }
        });
    } else if (ObjWorkSummary.WorkItems.length == 1) {
        var chkItm2 = document.getElementById('chkSelectItem_' + ObjWorkSummary.WorkItems[0].Id);
        if (chkItm2 && !ObjWorkSummary.WorkItems[0].Booked) {
            chkItm2.checked = true;
        }
    } else if (!chked) {
        ObjWorkSummary.WorkItems.forEach(function (itm2) {
            var chkItm3 = document.getElementById('chkSelectItem_' + itm2.Id);
            if (newSite != itm2.JobSiteId && !itm2.Booked) {
                newSite = itm2.JobSiteId;
                if (chkItm3) {
                    chkItm3.checked = true;
                }
            }
        });
    }
}

//      Function to display the selected tools
function SetSelectedTools() {
    try {
        //Check if there is a list of currently selected tools
        if (SelectedTools && SelectedTools.length > 0) {
            var element;

            SelectedTools.forEach(function (t) {
                element = document.getElementById('txtToolNum_' + t.ToolId.toString());
                //Check the element is valid and there are a number of tools required
                if (element && t.Number > 0) {
                    //Set the value to be displayed
                    element.value = t.Number.toString();
                }
            });
        }
    }
    catch (err) {
        DisplayMessage('pnlScheduleDetail_1', 'Error: Unable to display currently selected tools list - ' + err.toString());
    }

}

//      Function to display the selected staff
function SetSelectedStaff() {
    try {
        //Check if there is a list of currently selected staff
        if (SelectedStaff && SelectedStaff.length > 0) {
            var chkElement;
            var txtElement;
            SelectedStaff.forEach(function (s) {
                //get the element for the relevent staff Id
                if (!s.StaffId) {
                    chkElement = document.getElementById('chkTeamStaff_' + s.Id.toString());
                    txtElement = document.getElementById('txtTeamTask_' + s.Id.toString());
                } else {
                    chkElement = document.getElementById('chkTeamStaff_' + s.StaffId.toString());
                    txtElement = document.getElementById('txtTeamTask_' + s.StaffId.toString());
                }
                //Check the element is valid and there are a number of tools required
                if (chkElement && txtElement) {
                    //Set the value to be displayed
                    chkElement.checked = true;
                    txtElement.text = s.Task;
                }
            });
        }
    }
    catch (err) {
        DisplayMessage('pnlScheduleDetail_2', 'Error: Unable to display currently selected tools list - ' + err.toString());
    }
}

//      Helper function to display the number of personel currently selected for the work plan
function UpdatePersonelLabel(count) {
    //debugger;
    //Get the label for the personel
    var lblPersonel = document.getElementById('lblStaffRequired');
    if (ObjWorkPlan.StaffRequired > 0) {
        lblPersonel.textContent = count.toString() + ' / ' + ObjWorkPlan.StaffRequired.toString();
    } else {
        lblPersonel.textContent = count.toString();
    }
}

//      Function to get the items selected to be assigned to the workplan
function GetSelectedItems() {
    var items = [];
    //  Iterate through all the items available and check if they are selected
    ObjWorkSummary.WorkItems.forEach(function (wi) {
        var itmChkBox = document.getElementById('chkSelectItem_' + wi.Id.toString());
        if (itmChkBox && itmChkBox.checked) {
            //The item is selected so push it to the return array
            items.push(wi);
        }
    });
    //  The array mey be empty if the workplan being edited is for a different quote
    //  Check the length of the return array and if zero check against the work plan items
    if (items.length == 0) {
        ObjWorkPlan.ScheduledItems.forEach(function (swi) {
            var itmChkBox = document.getElementById('chkSelectItem_' + swi.Id.toString());
            if (itmChkBox && itmChkBox.checked) {
                //The item is selected so push it to the return array
                items.push(swi);
            }
        });
    }
    return items;
}

//      Function to get the staff selected for the work plan
function GetSelectedStaff() {
    var staff = [];
    //  Iterate through all the staff available and check if they are selected
    //debugger;
    ListStaff.forEach(function (s) {
        var staffChkBox = document.getElementById('chkTeamStaff_' + s.StaffId.toString());
        var txtRole = document.getElementById('txtTeamTask_' + s.StaffId.toString());
        if (staffChkBox && staffChkBox.checked) {
            //The current staff object has been selected populate the role and push to the return list
            if (txtRole) {
                s.Task = txtRole.value;
            }
            staff.push(s);
        }
    });

    return staff;
}

//      Function to get the tools selected for the work plan
function GetSelectedTools() {
    var tools = [];
    //  Iterate through all the tools available and check if they have a value
    //debugger;
    ListTools.forEach(function (t) {
        var txtRole = document.getElementById('txtToolNum_' + t.ToolId.toString());
        //  Check if the element has any content, we only want to add items that have a value
        //  Check the element is valid and there are a number of tools required
        if (txtRole && txtRole.value.length > 0 && txtRole.value != '0') {
            //  Check if the value entered is a number
            var isNotANum = isNaN(txtRole.value);
            if (isNotANum) {
                //  The value entered is not a number so default to 1
                t.Number = 1;
            } else {
                t.Number = parseInt(txtRole.value);
            }
            //  Add the current tool to the return list
            tools.push(t);
        }
    });
    return tools;
}

//      Function to get the selected Team Id
function GetSelectedDdlElementValue(ddlName) {
    //debugger;
    var ddl = document.getElementById(ddlName);
    var id = ddl.options[ddl.selectedIndex].value;
    return id;
}

//      Function to validate a work plan before saving
function ValidateWorkPlan() {
    // DGSAP-841 - updated validation as before it did not detect personnel, tools and scheduled items properly
    return (ObjWorkPlan.JobSiteId > 0 && ObjWorkPlan.TeamId > 0 && ObjWorkPlan.ScheduledItems.length > 0 && ObjWorkPlan.Personnel.length > 0 && ObjWorkPlan.Tools.length > 0);
}
/*********************************************************************************************************

                                            Tools Editing

**********************************************************************************************************/


//      Function to get the tools that have been selected for the work plan
//      The return value must be captured in the javascript else an invalid postback could be raised
function GetSelectedWpStaff() {
    //Create a new array for the selected staff
    var selectedStaff = [];
    try {
        var element;
        if (ListStaff && ListStaff.length > 0) {
            //Iterate through each staff member to see if they have been selected
            ListStaff.forEach(function (s) {
                element = document.getElementById('chkTeamStaff_' + s.Id.toString());
                if (!element) {
                    alert('ERROR: Unable to determin selected staff - Element chkTeamStaff_' + s.Id.toString() + ' is not valid.');
                } else {
                    if (element.checked) {
                        //Create a new Team Staff member object to be associated with the work plan
                        selectedStaff.push(CreateStaffPlanObject(s));
                    }
                }
            });
            //Check if any staff have been selected
            if (selectedStaff.length == 0) {
                alert('INFORMATION: Please select staff for the work plan before clicking save.');
                return null;
            }
        } else {
            selectedStaff = ListStaff;
        }
    }
    catch (err) {
        DisplayMessage('pnlPlanError', 'ERROR: Unable to save selected staff to work plan - ' + err.toString());
        return null;
    }
    return selectedStaff;
}

//Function to create a staff work plan object from a staff record
function CreateStaffPlanObject(objStaff) {
    var obj = new Object();
    obj.Id = -1;
    obj.ParentId = ObjWorkPlan.TeamId;
    obj.StaffId = objStaff.Id;
    obj.StaffName = objStaff.Name;
    obj.Task = document.getElementById('txtTeamTask_' + objStaff.Id.toString()).value;
    obj.Deleted = false;

    return obj;
}
/*********************************************************************************************************

                                            END LOCAL FUNCTIONS

*********************************************************************************************************/
