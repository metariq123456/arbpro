﻿var Stage = 0;
var ItemID = 0;
var CancelUpload = true;
var isForCompany = false;
var connectionString = "";
var imageList = null;
var CurrentImage = 0;
var TotalImage = 0;
var PictureId = -1;
var IsChange = false;
var fileUploadready = false;
var ImageNumber = 0;
// iStage - the type that the image is associated to:
// 0 - job item, 1 - quote item, 2 - work item, 3 - invoice item,
// 4 - email footer, 5 - pdf header, 6 - pdf footer, 7 - director's signature
// iItemID - the job item's id for stages 0 - 3, also used to distinguish between pdf header and footer
// isCompany - used to determine the asmx used to call save, clear and get methods from
function ImageViewer_Load(iStage, iItemID, isCompany) {
    if (isCompany !== undefined)
        isForCompany = isCompany;


    Stage = iStage;
    ItemID = iItemID;

    var imgImageViewer = document.getElementById('imgImageViewer');
    if (isForCompany) {
        connectionString = "Company.asmx";
        imgImageViewer.src = 'Images/JobImage.aspx?Stage=' + Stage.toString() + '&ItemID=' + ItemID.toString() + '&r=' + Math.floor((Math.random() * 10000) + 1);

    }
    else {
        connectionString = "WorkFlow.asmx";
        GetAllImageByItemId(Stage, ItemID);
    }
}

function GetAllImageByItemId(Stage, ItemID) {
    debugger;
    imageList = Ajax_CallService(
      'Services/Web/WorkFlow.asmx',
      "GetAllImageByItemId",
      {
          Stage: Stage,
          ItemID: ItemID
      },
      false
     );

    if (imageList != null && imageList.length > 0) {
        if (imageList.length == 1) {
            document.getElementById("imgImageViewer").src = "data:image/png;base64," + imageList[0].Base64Image;
            CurrentImage = 0;
            PictureId = imageList[0].Id;
            ImageNumber = (imageList.indexOf(imageList[0])) + 1;
            document.getElementById('lblTotalImage').innerHTML = ImageNumber + ' of ' + imageList.length;


        }
        else if (imageList[CurrentImage].Base64Image !== undefined && imageList[CurrentImage].Base64Image != null) {
            document.getElementById("imgImageViewer").src = "data:image/png;base64," + imageList[CurrentImage].Base64Image;
            PictureId = imageList[CurrentImage].Id;
            ImageNumber = (imageList.indexOf(imageList[CurrentImage])) + 1;
            document.getElementById('lblTotalImage').innerHTML = ImageNumber + ' of ' + imageList.length;

        }
        else
            DefaultImage();
    }
    else
        DefaultImage();

}

function NextImage() {

    if (imageList != null && imageList.length > 0) {
        CurrentImage = CurrentImage + 1; // increase i by one
        CurrentImage = CurrentImage % imageList.length; // if we've gone too high, start from `0` again
        //return CurrentImage; // give us back the item of where we are now
        PictureId = imageList[CurrentImage].Id;
        document.getElementById("imgImageViewer").src = "data:image/png;base64," + imageList[CurrentImage].Base64Image;
        ImageNumber = (imageList.indexOf(imageList[CurrentImage])) + 1;
        document.getElementById('lblTotalImage').innerHTML = ImageNumber + ' of ' + imageList.length;

    }
    else
        DefaultImage();
}

function PreviousImage() {
    if (imageList != null && imageList.length > 0) {
        if (CurrentImage == 0) { // i would become 0
            CurrentImage = imageList.length; // so put it at the other end of the array
        }
        CurrentImage = CurrentImage - 1; // decrease by one
        //return CurrentImage;

        PictureId = imageList[CurrentImage].Id;
        document.getElementById("imgImageViewer").src = "data:image/png;base64," + imageList[CurrentImage].Base64Image;
        ImageNumber = (imageList.indexOf(imageList[CurrentImage])) + 1;
        document.getElementById('lblTotalImage').innerHTML = ImageNumber + ' of ' + imageList.length;
    }
    else
        DefaultImage();

}

function FirstImage() {

    if (imageList != null && imageList.length > 0) {
        document.getElementById("imgImageViewer").src = "data:image/png;base64," + imageList[0].Base64Image;
        PictureId = imageList[0].Id;
        CurrentImage = 0;
        ImageNumber = (imageList.indexOf(imageList[0])) + 1;
        document.getElementById('lblTotalImage').innerHTML = ImageNumber + ' of ' + imageList.length;
    }
    else
        DefaultImage();
}

function LastImage() {

    if (imageList != null && imageList.length > 0) {
        if (CurrentImage == 0 && imageList.length == 1) {
            document.getElementById("imgImageViewer").src = "data:image/png;base64," + imageList[0].Base64Image;
            PictureId = imageList[0].Id;
            ImageNumber = (imageList.indexOf(imageList[0])) + 1;
            document.getElementById('lblTotalImage').innerHTML = ImageNumber + ' of ' + imageList.length;
        }
        else {
            document.getElementById("imgImageViewer").src = "data:image/png;base64," + imageList[imageList.length - 1].Base64Image;
            PictureId = imageList[imageList.length - 1].Id;
            CurrentImage = imageList.length - 1;

            ImageNumber = imageList.length;
            document.getElementById('lblTotalImage').innerHTML = ImageNumber + ' of ' + imageList.length;
        }

    }
    else
        DefaultImage();
}

function DefaultImage() {
    document.getElementById("imgImageViewer").src = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAMAAABOo35HAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMAUExURQAAAAcHBw8PDxUVFRcXFxsbGx0dHSMjIycnJysrKy0tLTExMTY2Njs7Oz8/P0FBQUJCQkZGRklJSU9PT1NTU1RUVFZWVldXV11dXWBgYGNjY2pqamtra21tbW5ubm9vb3V1dXp6ent7e4GBgYSEhIiIiIuLi4+Pj5GRkZeXl5ubm6CgoKSkpKWlpaioqKurq6ysrK6urrKysrOzs7a2tre3t7i4uLq6ur6+vsHBwcbGxsrKysvLy87OztDQ0NHR0dLS0tXV1dnZ2dvb29zc3N3d3d7e3uLi4uTk5Obm5ujo6Orq6u3t7fHx8fLy8vf39/v7+////wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOWTt9gAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAadEVYdFNvZnR3YXJlAFBhaW50Lk5FVCB2My41LjEwMPRyoQAABs5JREFUeF7t1wFTE1cYRuG7iFGxWxGoItgGSVVESoulVVFrsSooUPb+/z/T725CiI7McJxkyjgn02ldeROTx7Obbco+zi2Qzr10mMUCEYglFhAAU8sSCwiAqWWJBQTA1LLEAgJgalliAQEwtSyxgACYWpZYQABMLUssIACmliUWEABTyxILCICpZYkFBMDUssQCAmBqWWIBATC1LLGAAJhallhAAEwtSywgAKaWJRYQAFPLEgsIgKlliQUEwNSyxAICYGpZYgEBMLUssYAAmFqWWEAATC1LLCAAppYlFhAAU8sSCwiAqWWJBQTA1LLEAgJgalliAQEwtSyxgACYWpZYQABMLUssIACmliUWEABTyxILCICpZYkFBMDUssQCAmBqWWIBATC1LLGAAJhallhAAEwtSywgAKaWJRYQAFPLEgsIgKlliQUEwNSyxAICYGpZYgEBMLUssYAAmFqWWEAATC1LLCAAppYlFhAAU8sSCwiAqWWJBQTA1LLEAgJgalliAQEwtSyxgACYWpZYQABMLUssIACmliUWEABTyxILCICpZYkFBMDUssQCAmBqWWIBATC1LLGAAJhallhAAEwtSywgAKaWJRYQAFPLEgsIgKlliQUEwNSyxAICYGpZYgEBMLUssYAAmFqWWEAATC3r/8bqVu/KW3ibnn32Trpd8Nb605m0/cXnlJcaff365KW/4s8475uaSFndNDsurN20PHfmZ/k2sKq0OaayVud30v5ZWt8GVr1cHQ5Ow72bqVo4GHzccop0lx9OpR8O7lXVT03OewtVurQRP351LV1+XD79wUKq7sSz28dRtd5UaznvpHJib1dH+en1lL5/FS8zOA1Pjuvbd9LUvXjF8oPjH6t0c++8Z9e5d5M5DevDarmPtV/1jo+XOoMP32KlB81uNfW42U2/5aPO/ab5Ne3kN2kr/13FEw47S+UJ8anLYzuyWp1ucjO9HkeLS/l5epn/XYzfGWANj+t42Q+dlRarmZ09aB5UZxZ5bp3PhhPCypvpaYvVuxofu5mOOMqjxYpPmue/i6N6JT9JR/GLtJ67t+O/G/GEtfLjZrqcxvGYiwvWTljm1Toyi2v94mL87rP09gRreFyXS9tO+lh+UIjjub2vRTnreZPCyvOdo4J1o/2SWp4fwYqP3X7U3H6B/fXL3cuB1ZbzpjwhkoyG+t9t79KT+PfVpcKwmzfjLMzHzx8uTJ1iDY/rR7E8iheIl14pfxX5QfmDxvqYGNa7qlew6oKQu4P33ZY1inV4ZWphfS+w4p/+eVun9lECynmtfxChlDjnA+1Zdfnun7+fYg2P+39QH6vbf9bVsUqVVx/3Cw5tHqUnp2X1P3v/NBzFWpuOq1kzWlacm8NHM71afr2b4hugN/Ox3HHNlPCenmINj9uyPg6wbkziU00Sq6nL9foL16xRrPZStRNYS4WzXLN6M+WaNdNe5J7FuVceczM5v0i9chbG90BcwE6xhsd1eaXt/jVrs3wXl6+DMT8mV1Z+nb78bTiKtVHt5n86qZffVFv5fSe9zvtTK8fxTdZ+fS4N7ke3yyV+OpUPPzd73Gyl9OLkAj88riO/9524ppdvwyu3DvJWvNiYHxPEyr1y21Tus+58cp81itXcq9L1P24FStxnXduKYuLOKw3ukfZP/k+nPR177dHe93Hb9rLaOMEaHtf34z6rXLfKqX4Y91nX42ZszI+JYH3te3zR3khc3MdFwZqvP+QPsyMX94tIdlGwDuLm6dLPg/v2iwhV3tNFwbqoPp+8L7HAX5NYYgEBMLUssYAAmFqWWEAATC1LLCAAppYlFhAAU8sSCwiAqWWJBQTA1LLEAgJgalliAQEwtSyxgACYWpZYQABMLUssIACmliUWEABTyxILCICpZYkFBMDUssQCAmBqWWIBATC1LLGAAJhallhAAEwtSywgAKaWJRYQAFPLEgsIgKlliQUEwNSyxAICYGpZYgEBMLUssYAAmFqWWEAATC1LLCAAppYlFhAAU8sSCwiAqWWJBQTA1LLEAgJgalliAQEwtSyxgACYWpZYQABMLUssIACmliUWEABTyxILCICpZYkFBMDUssQCAmBqWWIBATC1LLGAAJhallhAAEwtSywgAKaWJRYQAFPLEgsIgKlliQUEwNSyxAICYGpZYgEBMLUssYAAmFqWWEAATC1LLCAAppYlFhAAU8sSCwiAqWWJBQTA1LLEAgJgalliAQEwtSyxgACYWpZYQABMLUssIACmliUWEABTyxILCICpZYkFBMDUssQCAmBqWWIBATC1LLGAAJhallhAAEwtSywgAKaWBbD+AzT9p+wxC10GAAAAAElFTkSuQmCC";
    PictureId = -1;
}

function Image_Rotate(iDegrees) {
    var imgImageViewer = document.getElementById('imgImageViewer');
    IsChange = true;
    imgImageViewer.src = 'Images/load_Big.gif';

    Ajax_CallService(
        'Services/Web/' + connectionString,
        'RotateImage',
        {
            Stage: Stage,
            ItemID: ItemID,
            Degrees: iDegrees,
            PictureId: CurrentImage
        },
        true,
        function () {
            if (CurrentImage != -1) {
                GetAllImageByItemId(Stage, ItemID)
            }
            else
                imgImageViewer.src = 'Images/JobImage.aspx?Stage=' + Stage.toString() + '&ItemID=' + ItemID.toString() + '&r=' + Math.floor((Math.random() * 10000) + 1);
        });
}

function Image_Flip(sAxis) {
    var imgImageViewer = document.getElementById('imgImageViewer');
    IsChange = true;
    imgImageViewer.src = 'Images/load_Big.gif';

    Ajax_CallService(
        'Services/Web/' + connectionString,
        'FlipImage',
        {
            Stage: Stage,
            ItemID: ItemID,
            Axis: sAxis,
            PictureId: CurrentImage
        },
        true,
        function () {
            if (CurrentImage != -1) {
                GetAllImageByItemId(Stage, ItemID)
            }
            else
                imgImageViewer.src = 'Images/JobImage.aspx?Stage=' + Stage.toString() + '&ItemID=' + ItemID.toString() + '&r=' + Math.floor((Math.random() * 10000) + 1);
        });
}

function Image_Upload() {

    var imgImageViewer = document.getElementById('imgImageViewer');
    var pnlUploadContainer = document.getElementById('pnlUploadContainer');
    var pnlUploadProgress = document.getElementById('pnlUploadProgress');
    var pnlControlButtons = document.getElementById('pnlPictureControlButtons');

    pnlUploadContainer.className = 'center hidden';
    pnlUploadProgress.className = 'center';
    $('#pnlUploadBar').width('0%');

    CancelUpload = false;
    imgImageViewer.src = 'Images/load_Big.gif';

    var fuImage = document.getElementById('fuImage');

    // checks if there is no file selected when the upload button is pressed
    if (fuImage.value == '' || fuImage.files.length == 0) {
        DisplayMessage('pnlImageViewerPane', "You have not selected any file to upload. <br/><br/> Please close the window and try again.", 'red');
        pnlUploadProgress.className = 'center hidden';
        pnlControlButtons.className = "center hidden";
        return;
    }

    var file = fuImage.files[0];
    var fr = new FileReader();

    // if the type of file is an image the index will be >= 0
    var wrongFile = file.type.indexOf("image");

    // checks if the type of file uploaded is an image
    if (wrongFile < 0) {
        DisplayMessage('pnlImageViewerPane', "You have uploaded an invalid file type. <br/><br/> Please close the window and try again.", 'red');
        pnlUploadProgress.className = 'center hidden';
        pnlControlButtons.className = "center hidden";
        return;
    }


    fr.onload = function (event) {
        var result = event.target.result;
        var base64 = result.substring(result.indexOf("base64,") + 7);

        var chunkSize = 1024 * 150;
        var chunks = Math.ceil(base64.length / chunkSize);

        var i = 0;

        function doChunk() {
            if (CancelUpload) {
                finishChunks();
                return;
            }

            var chunk = base64.substring(i * chunkSize, Math.min((i + 1) * chunkSize, base64.length));

            var progress = Math.round((i / chunks) * 100);
            $('#pnlUploadBar').width(progress + '%');
            Ajax_CallService(
                'Services/Web/' + connectionString,
                'UploadImage',
                {
                    Stage: Stage,
                    ItemID: ItemID,
                    Chunk: chunk,
                    New: i == 0,
                    Completed: i == chunks - 1
                },
                false);

            i++;

            if (i < chunks) {
                setTimeout(doChunk, 50);
            } else {
                finishChunks();
                if (!isForCompany) {
                    fileUploadready = true;
                    GetAllImageByItemId(Stage, ItemID);
                    LastImage();
                }
            }
        }

        doChunk();
    };
    fr.readAsDataURL(file);
}

// function to clear the image and display it on the image panel
function Image_Clear() {

    if (!isForCompany && PictureId != -1) {
        var IsDeleted = Ajax_CallService(
             'Services/Web/' + connectionString,
             'ClearImage',
             {
                 Stage: Stage,
                 ItemID: ItemID,
                 PictureId: PictureId
             },
             false);
        if (!isForCompany) {
            CurrentImage = 0;
            GetAllImageByItemId(Stage, ItemID);
            LastImage();
        }
    }
    else if (isForCompany) {
        var IsDeleted = Ajax_CallService(
          'Services/Web/' + connectionString,
          'ClearImage',
          {
              Stage: Stage,
              ItemID: ItemID,
              PictureId: PictureId
          },
          false);
        imgImageViewer.src = 'Images/JobImage.aspx?Stage=' + Stage.toString() + '&ItemID=' + ItemID.toString() + '&r=' + Math.floor((Math.random() * 10000) + 1);
    }
    return false;
}

function finishChunks() {
    var fuImage = document.getElementById('fuImage');
    fuImage.value = '';

    pnlUploadContainer.className = 'center';
    pnlUploadProgress.className = 'center hidden';


    imgImageViewer.src = 'Images/JobImage.aspx?Stage=' + Stage.toString() + '&ItemID=' + ItemID.toString() + '&r=' + Math.floor((Math.random() * 10000) + 1);

}

function Image_Cancel() {
    CancelUpload = true;
}

function PrintImage() {
    debugger;
    if (PictureId != -1) {

        Dialog_OpenPage(
            'Work Schedule',
            'Dialogs/PDFWorkScheduleGeneration.aspx',
            'content',
            700,
            630,
            true,
            {
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/WorkPackGeneration.js?V=3.23.5',
            function () {
                GenerateImageReport(PictureId, imageList[CurrentImage].ImageBytes);
            });
    }
}