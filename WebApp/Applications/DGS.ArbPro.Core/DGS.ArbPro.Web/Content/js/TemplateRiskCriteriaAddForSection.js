﻿var riskId = -1;
var section = {};
var allCriteria = null;

function AddOrEditTemplateCriteria(riskid, sectionId) {
    riskId = riskid;
    section = GetSectionTemplateForId(riskId, sectionId);

    allCriteria = GetAllRiskCriteria();

    UpdateSectionDetails();

    UpdateTable();
}

function UpdateSectionDetails() {
    var name = document.getElementById('txtSectionName');
    var ref = document.getElementById('txtDocument');
    var chapter = document.getElementById('txtSectionRef');
    var desc = document.getElementById('txtDescription');

    name.value = section.Title;
    ref.value = section.ReferenceDoc;
    chapter.value = section.ChapterRef;
    desc.value = section.Description;
}

function UpdateTable() {
    var panel = document.getElementById('pnlCriteriaTableContainer');
    var strHtml = '';
    strHtml += '<table class="searchGrid width-full white"><col width="80%"></col><col width="20%"></col>';
    //Construct the html
    allCriteria.forEach(function (ac) {
        var isIncluded = false;
        if (!ac.IsDeleted) {
            if (section.TemplateCriteria && section.TemplateCriteria.length > 0) {
                section.TemplateCriteria.forEach(function (c) {
                    if (c.Id == ac.Id) {
                        isIncluded = true;
                    }
                });
            }
            strHtml += '<tr id="cri' + ac.Id + '" onclick="ItemSelected( \'' + ac.Id + '\');"><td>' + ac.Criterion + '</td>';
            strHtml += '<td style="text-align: center; cursor: default;"><input id="chkCrit' + ac.Id + '" type="checkbox" style="cursor: pointer;"';
            if (isIncluded) {
                strHtml += 'checked/></td></tr>';
            } else {
                strHtml += '/></td></tr>';
            }
        }
    });

    strHtml += '</table>';

    //display the grid
    panel.innerHTML = strHtml;
}

function ItemSelected(criterionId) {
    var criterion = {};
    allCriteria.forEach(function (ac) {
        if (!ac.IsDeleted) {
            document.getElementById('cri' + ac.Id).className = "";
            if (ac.Id == criterionId) {
                criterion = ac;
                document.getElementById('cri' + ac.Id).className = "selected";
            }
        }
    });

    PopulateFields(criterion);
}

function PopulateFields(criterion) {
    document.getElementById('pnlEditCriterionDetails').className = 'clientField float-left';
    var save = document.getElementById('btnSave');
    var name = document.getElementById('txtName');
    var chapter = document.getElementById('txtChapter');
    var phase = document.getElementById('txtPhase');

    if (criterion) {
        name.value = criterion.Criterion;
        phase.value = criterion.AssessmentPhase;
        chapter.value = criterion.ChapterRef;

        name.disabled = true;
        phase.disabled = true;
        chapter.disabled = true;
        save.disabled = true;
    } else {
        name.value = "";
        chapter.value = "";
        phase.value = "";

        name.disabled = false;
        phase.disabled = false;
        chapter.disabled = false;
        save.disabled = false;
    }
}

function SaveCriterion() {
    var newCriterion = {};
    newCriterion.Id = -1;
    newCriterion.Criterion = document.getElementById('txtName').value;
    newCriterion.ChapterRef = document.getElementById('txtChapter').value;
    newCriterion.AssessmentPhase = document.getElementById('txtPhase').value;

    if (newCriterion.AssessmentPhase < 1 || newCriterion.AssessmentPhase > 2 ) {
        alert('Please put 1 or 2 for the Assessment Phase and try again.');
    } else if (newCriterion.Criterion === "") {
        alert('Please add a criterion name and try again.');
    } else {
        Save(newCriterion);
    }
}

function Save(criterion) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'SaveCriterionAndMapToTemplate',
        {
            riskId: riskId,
            sectionId: section.Id,
            criterion: criterion
        },
        false
    );

    if (retVal) {
        document.getElementById('pnlEditCriterionDetails').className = 'clientField float-left hidden';
        AddOrEditTemplateCriteria(riskId, section.Id);
    }
    else {
        alert('Could not save criterion.');
    }

    return retVal;
}

function UpdtSection() {
    section.Title = document.getElementById('txtSectionName').value;
    section.ReferenceDoc = document.getElementById('txtDocument').value;
    section.ChapterRef = document.getElementById('txtSectionRef').value;
    section.Description = document.getElementById('txtDescription').value;

    if (section.Title === "") {
        alert('Please add a section name and try again.');
        return false;
    } else {
        return UpdateSec(section);
    }
}

function UpdateSec(section) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'InsertOrUpdateSectionTemplate',
        {
            section: section
        },
        false
    );

    if (!retVal) {
        alert('Could not save section template.');
    }

    return retVal;
}

function UpdateTemplateCriteria() {
    allCriteria.forEach(function (ac) {
        if (!ac.IsDeleted) {
            var chkCrit = document.getElementById('chkCrit' + ac.Id).checked;
            if (chkCrit) {
                if (!Update(ac.Id)) {
                    return false;
                }
            } else {
                if (!Remove(ac.Id)) {
                    return false;
                }
            }
        }
    });

    return true;
}

function UpdateSectionAndCriteria() {
    return UpdtSection() && UpdateTemplateCriteria();
}


function Remove(criterionId) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'RemoveTemplateRiskSectionMapping',
        {
            riskId: riskId,
            sectionId: section.Id,
            criterionId: criterionId
        },
        false
    );

    return retVal;
}

function Update(criterionId) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'InsertTemplateRiskSectionMapping',
        {
            riskId: riskId,
            sectionId: section.Id,
            criterionId: criterionId
        },
        false
    );

    return retVal;
}

function GetSectionTemplateForId(riskId, sectionId) {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'GetSectionTemplateForId',
        {
            riskId: riskId,
            sectionTemplateId: sectionId
        },
        false
    );

    return retVal;
}


function GetAllRiskCriteria() {
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'GetAllRiskCriteria',
        {
        },
        false
    );

    return retVal;
}