﻿var PnlError = 'pnlDlgClndrError';
var PnlContent = 'pnlDlgClndrAppointment';
var ObjWorkPlan = null; // The selected work plan. This will be null if there is no work plan
var ObjApp = null;      // The appointment for the selected work plan
var SelectedJobSite = -1;   // This is the default site loaded 
var ListTools = [];     // The array tools
var ListStaff = [];     // The array selected staff
var ListTeams = [];     // The array of Teams
var SelectedDate = new Date(); // The date for the appointment

var SelectedStaff = [];
var SelectedToolsNew = [];
var SelectedTeamId = -1;
var WorkPlanIdFromCalendar = -1;
function CalendarBookWork_WorkPlan(workPlanId, siteId, isEdit) {
    Teams_Load();
    WorkPlanIdFromCalendar = workPlanId;
    SelectedJobSite = siteId;
    WorkPlan_Load(workPlanId);
}


function Teams_Load() {
    try {
        Ajax_CallService(
        'Services/Web/WorkProgramming.asmx',
        'GetAllTeam',
        {},
        true,
        function (results) {
            ListTeams = results;
        }
        );

    } catch (err) {
        DisplayMessage(PnlError, 'Error: Unable to communicate with Arb Pro server.');
    }
}

function WorkPlan_Load(workPlanId) {
    //It is possible that this could fail
    try {
        Ajax_CallService(
            'Services/Web/WorkProgramming.asmx',
            'GetWorkPlan',
            {
                id: workPlanId,
                includeTools: true
            },
            true,
            function (result) {

                WorkPlanAppointment_Load(result);
            }
       );
    }
    catch (err) {
        DisplayMessage(PnlError, 'ERROR: Unable to conect to Arb Pro service - ' + err.toString());
    }
}

//      Async service call to load an appointment for a work plan
function WorkPlanAppointment_Load(workPlan) {
    debugger;
    //It is possible that this could fail
    if (!workPlan) {
        //The service call to get a work plan failed display a message and halt
        DisplayMessage(PnlError, 'Error: The Arb Pro service did not return valid data.');
    } else {
        //Set the gloabal scope variable
        ObjWorkPlan = workPlan;
        //  Populate the selected items arrays from the loaded work plan
        SelectedStaff = ObjWorkPlan.Personnel;
        SelectedToolsNew = ObjWorkPlan.Tools;
        SelectedTeamId = ObjWorkPlan.TeamId;
        //  Initialise the main lists for comparison
        //  These will be overwritten if the user edits staff or tools
        ListStaff = ObjWorkPlan.Personnel;
        ListTools = ObjWorkPlan.Tools;
        EditingQuoteNo = ObjWorkPlan.QuoteReference;
        //Load the appointment - A new appointment will be created if the id passed to the service is -1
        try {
            Ajax_CallService(
                'Services/Web/Calendar.asmx',
                'GetAppointmentForWorkPlan',
                {
                    workPlanId: workPlan.Id
                },
                true,
                DisplayAppointment
            );
        } catch (err) {
            DisplayMessage(PnlError, 'ERROR: Unable to conect to Arb Pro service - ' + err.toString());
        }

    }
}

//      Async service callback to display a work plan
function DisplayAppointment(result) {
    debugger;
    var panel = document.getElementById('');
    try {
        //  Check if the service returned a valid object
        if (!result) {
            DisplayMessage(PnlError, 'Error: Arb Pro service did not return valid data.');
        } else {
            //  Populate the global scope variable
            ObjApp = result;
            var startDate;
            var strStartDate = '';

            var endDate;
            var strEndDate = '';

            //Set the scope variable
            ObjApp = result;
            //Check if the appointment is a new one
            if (ObjApp.Id < 1) {
                //populate the start date
                startDate = SelectedDate;
                startDate.setHours(9, 0, 0, 0);
                strStartDate = startDate.format('dd/MMM/yy HH:mm');
                //Default the end date to the same date
                endDate = SelectedDate;
                endDate.setHours(17, 0, 0, 0);
                strEndDate = endDate.format('dd/MMM/yy HH:mm');
            } else {
                startDate = toDateFromJson(ObjApp.StartDateTime);
                strStartDate = startDate.format('dd/MMM/yy HH:mm');
                endDate = toDateFromJson(ObjApp.EndDateTime);
                strEndDate = endDate.format('dd/MMM/yy HH:mm');
            }
           var WorkPlanForClientInfo = Ajax_CallService(
               'Services/Web/WorkProgramming.asmx',
               'GetWorkPlan',
               {
                   id: WorkPlanIdFromCalendar,
                   includeTools: true
               },
               false
               );
            var SummaryDetails = Ajax_CallService(
                  'Services/Web/WorkProgramming.asmx',
                  "GetJobWorkSummaryForWork",
                  {
                      id: WorkPlanForClientInfo.WorkId
                  },
                  false                
             );

            //Generate the html for the panel
            var strHtml = '<div class="width-full float-left">';
            //  Display the Quote No of the work plan being edited
            //strHtml += '';
            strHtml += '<div id="pnlAppointmentDates" class="width-full float-left">';
            strHtml += '<div class="dlgBookWorkHeader"><span style="color:white;">Quote No: ' + EditingQuoteNo + '</span></div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class ="dlgBookWorkLabel">Client Name</label>';
            strHtml += '<label class ="dlgBookWorkInput">';
            strHtml += SummaryDetails.ClientName
            strHtml += '</label>';
            strHtml += '</div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class ="dlgBookWorkLabel">Site Address</label>';
            strHtml += '<label class ="dlgBookWorkInput wrapText dont-break-out">';
            strHtml += SummaryDetails.StreetNumber + "," + SummaryDetails.AddressLn1 + "," + SummaryDetails.AddressLn2 + "," + SummaryDetails.Town + "," + SummaryDetails.County + "," + SummaryDetails.PostCode;
            strHtml += '</label>';
            strHtml += '</div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class ="dlgBookWorkLabel">Contact Number</label>';
            strHtml += '<label class ="dlgBookWorkInput">';
            strHtml += SummaryDetails.Telephone;
            strHtml += '</label>';
            strHtml += '</div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class ="dlgBookWorkLabel">Mobile Number</label>';
            strHtml += '<label class ="dlgBookWorkInput">';
            strHtml += SummaryDetails.Mobile;
            strHtml += '</label>';
            strHtml += '</div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class ="dlgBookWorkLabel">Email</label>';
            strHtml += '<label class ="dlgBookWorkInput">';
            strHtml += SummaryDetails.Email;
            strHtml += '</label>';
            strHtml += '</div>';
            /*strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Start Date/Time</label>';
            strHtml += '<input id="txtDateFrom" type="text" class="dlgBookWorkInput" placeholder="Start Date and Time" value="' + strStartDate + '" name="txtDateFrom">';
            strHtml += '</div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">End Date/Time</label>';
            strHtml += '<input id="txtDateTo" type="text" class="dlgBookWorkInput" placeholder="End Date and Time" value="' + strEndDate + '" name="txtDateTo">';
            strHtml += '</div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Skip Days</label>';
            strHtml += '<select id="ddlSkipDays" title="Days not included as work days" class="dlgBookWorkInput">';
            strHtml += '<option value="0" >None</option>';
            strHtml += '<option value="1">Saturday</option>';
            strHtml += '<option value="2">Sunday</option>';
            strHtml += '<option value="3" selected="selected">Weekends</option>';
            strHtml += '<option value="4">Other</option>';
            strHtml += '</select>';
            strHtml += '</div>';
            //Add the time required field
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Time Required (hrs)</label>';
            strHtml += '<input id="txtTime" type="text" class="dlgBookWorkInput" placeholder="The estimated time, in hours, required to complete the job." value="' + ObjWorkPlan.TimeRequired.toString() + '" name="txtTime">';
            strHtml += '</div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Site</label>';
            strHtml += '<select id="ddlWorkSite" class="dlgBookWorkInput" style="width:65%;" name="ddlWorkSite"  onchange="SelectedSiteChanged();">';
            //Check if there is a site selected
            if (ObjWorkPlan.JobSiteId > 0) {
                //The work plan has a job site - Prevent selection to reduce potential confusion
                strHtml += '<option value="' + ObjWorkPlan.JobSiteId + '" selected="selected">' + ObjWorkPlan.SiteAddress + '</option>';
            } else {
                //The work plan has no job site selected
                if (ObjWorkPlan.JobSiteId == -1 && ObjWorkSummary.JobSites.length > 1) {
                    strHtml += '<option value="-1" selected="selected"> Select Site to Book Work</option>';
                } else if (ObjWorkSummary.JobSites.length > 1) {
                    strHtml += '<option value="-1"> Select Site to Book Work</option>';
                }
                ObjWorkSummary.JobSites.forEach(function (js) {
                    strHtml += '<option value="' + js.Id + '"';
                    if (ObjWorkPlan.JobSiteId == js.Id || ObjWorkSummary.JobSites.length == 1) {
                        strHtml += ' selected="selected"';
                    }
                    strHtml += '>' + js.SiteString + '</option>';
                });
            }
            strHtml += '</select>';
            strHtml += '</div>';*/
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Team</label>';
            strHtml += '<label id="lblTeamName" class="dlgBookWorkInput">';
            if (ObjWorkPlan.TeamName.length == 0) {
                //No team has been selected
                strHtml += 'Not Set';
            } else {
                strHtml += ObjWorkPlan.TeamName;
            }
            strHtml += '</label></div>';
            strHtml += '<div class="dlgBookWorkField">';
            strHtml += '<label class="dlgBookWorkLabel">Personnel</label>';
            if (ObjWorkPlan.StaffRequired > 0) {
                strHtml += '<label id="lblStaffRequired" class="dlgBookWorkInput">' + ObjWorkPlan.Personnel.length.toString() + ' / ' + ObjWorkPlan.StaffRequired.toString() + '</label>';
            } else {
                strHtml += '<label id="lblStaffRequired" class="dlgBookWorkInput">' + ObjWorkPlan.Personnel.length.toString() + '</label>';
            }
            strHtml += '</div>';
            strHtml += '<div class="width-full float-left height-full">';
            strHtml += '<div class="dlgBookWorkContainer width-full float-left" style="height: 270px;">';
            //Set up the tabs for the summary details
            strHtml += '<div id="pnlWorkDetailTabs" class="bookWorkTabBar width-full">';
            strHtml += '<a id="pnlTabPlan_0" class="jobTab activeTab" href="javascript:SwitchScheduleDetailsPanel(0);"> Items </a>';
            strHtml += '<a id="pnlTabPlan_1" class="jobTab" href="javascript:SwitchScheduleDetailsPanel(1);"> Team </a>';
            strHtml += '<a id="pnlTabPlan_2" class="jobTab" href="javascript:SwitchScheduleDetailsPanel(2);"> Tools </a>';
            strHtml += '</div>';
            //Set up the containers for the summary details
            strHtml += '<div id="pnlWorkScheduleDetailContent" class="float-left width-full height-full scroll-vertical" style="max-height: 77%;">';

             //Tools panel
            strHtml += '<div id="pnlScheduleDetail_2" class="hidden">';
            strHtml += '<table class="waitTimer"><tr><td>';
            strHtml += 'Loading Tools. Please Wait';
            strHtml += '</td></tr><tr><td class="width-full">';
            strHtml += '<img id="waitImg_pnlScheduleDetail_1" class="padded" src="Images/load_Medium.gif">';
            strHtml += '</td></tr></table>';
            strHtml += '</div>';


            //Staff panel
            strHtml += '<div id="pnlScheduleDetail_1" class="hidden">';
            strHtml += GenerateStaffHtmlNew(ObjWorkPlan.Personnel);
            strHtml += '</div>';

            //Item panel
            strHtml += '<div id="pnlScheduleDetail_0" class="">';
            strHtml += '<table class="waitTimer"><tr><td>';
            strHtml += 'Sorting Work Items. Please Wait';
            strHtml += '</td></tr><tr><td class="width-full">';
            strHtml += '<img id="waitImg_pnlScheduleDetail_3" class="padded" src="Images/load_Medium.gif">';
            strHtml += '</td></tr></table>';
            strHtml += '</div>';
           
            //Closs the summary div
            strHtml += '</div>';
            strHtml += '</div>';
            //Close the containing div
            strHtml += '</div>';

            //Add the html to the panel
            document.getElementById(PnlContent).innerHTML = strHtml;

            //Load the tools
            Tools_Load();

            
            //Now that the html elements exist on the page - the ajax binding for the date time picker can be added

            SelectedSiteChanged();


            //GenerateNoteHtml();
                        
        }
    }
    catch (err) {
        DisplayMessage(PnlError, 'ERROR: Unable to display appointment details - ' + err.toString());
        //The error message is not displayed in the content panel so ensure the wait timer is removed
        DisplayMessage(PnlContent, '');
    }
}

function GenerateStaffHtmlNew(staffList) {
    var strHtml = '<table class="width-full searchGrid height-full">';
    strHtml += '<tr><th>Name</th><th>Assigned</th><th>Role</th></tr>';
    staffList.forEach(function (c) {
        var curId = -1;
        if (!c.StaffId) {
            curId = c.Id;
        } else {
            curId = c.StaffId;
        }
        strHtml += '<tr>';
        strHtml += '<td style="width: 50%; padding-left: 10px;">';
        if (!c.Name) {
            strHtml += c.StaffName + '</td>';
        } else {
            strHtml += c.Name + '</td>';
        }
        var foundSelected = $.grep(SelectedStaff, function (e) { return e.StaffId == curId; });
        strHtml += '<td style="text-align:center;"> <input type="checkbox" disabled Id="chkTeamStaff_' + curId + '"';
        if (foundSelected && foundSelected.length == 1) {
            strHtml += 'checked';
        }
        strHtml += '/>  </td> ';
        strHtml += '<td class="companyInput"><input type="text" Id="txtTeamTask_' + curId + '" placeholder="Staff Role" disabled title="The role within the team for the staff member"';
        if (foundSelected && foundSelected.length == 1 && foundSelected[0].Task && foundSelected[0].Task.length > 0) {
            strHtml += 'value="' + foundSelected[0].Task + '"';
        }
        strHtml += '/>  </td> ';
        strHtml += '</tr>';
    });
    strHtml += '</table>';
    return strHtml;
}

function Tools_Load() {
    try {
        Ajax_CallService(
               'Services/Web/WorkProgramming.asmx',
               'GetToolListAsWorkTools',
               {
               },
               true,
               ShowEditToolsDivCallBack
           );
    } catch (err) {
        DisplayMessage(PnlError, 'Error: Unable to communicate with Arb Pro server.');
    }
}

//      Async service callback to display the tools list
function ShowEditToolsDivCallBack(results) {
    var panel = document.getElementById('pnlScheduleDetail_2');
    try {
        if (!results || results.length == 0) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid list of tools.');
        } else {
            //  Set the scope variable
            ListTools = results;
            //  Set the panels inner html to be the generated html;
            panel.innerHTML = GenerateToolHtmlNew(ListTools);
            //  Now the list has been created and display, set the selected tools
            SetSelectedToolsNew();
        }
    } catch (err) {
        DisplayMessage(panel.id, 'ERROR: Unable to display tool selection - ' + err.toString());
    }
}

//      Function to display the selected tools
function SetSelectedToolsNew() {
    try {
        debugger;
        //Check if there is a list of currently selected tools
        if (SelectedToolsNew && SelectedToolsNew.length > 0) {
            var element;

            SelectedToolsNew.forEach(function (t) {
                element = document.getElementById('txtToolNewNum_' + t.ToolId.toString());
                //Check the element is valid and there are a number of tools required
                if (element && t.Number > 0) {
                    debugger;
                    //Set the value to be displayed
                    element.value = t.Number.toString();
                }
            });
        }
    }
    catch (err) {
        DisplayMessage('pnlScheduleDetail_1', 'Error: Unable to display currently selected tools list - ' + err.toString());
    }

}

function SetSelectedTools() {
    try {
        //Check if there is a list of currently selected tools
        if (SelectedToolsNew && SelectedToolsNew.length > 0) {
            var element;

            SelectedToolsNew.forEach(function (t) {
                element = document.getElementById('txtToolNewNum_' + t.ToolId.toString());
                //Check the element is valid and there are a number of tools required
                if (element && t.Number > 0) {
                    //Set the value to be displayed
                    element.value = t.Number.toString();
                }
            });
        }
    }
    catch (err) {
        DisplayMessage('pnlScheduleDetail_1', 'Error: Unable to display currently selected tools list - ' + err.toString());
    }

}

//      Function to generate the html for the tools panel
function GenerateToolHtmlNew(toolList) {
    var strHtml = '<table class="rtGrid width-full white" style="table-layout:fixed; word-wrap:break-word;">';
    //strHtml += '<tr><th colspan="2"><button type="button" onclick="ShowEditToolsDiv();" class="button-thin float-right padded">Edit Tools</button></th></tr>';

    // if its for the tools selection from quote screen change the message to be more precise.
   
    
    strHtml += '<tr><th>Tool</th><th>Number Required</th></tr>';
    //  Iterate through all the tools and create a row in the table
    toolList.forEach(function (c) {
        strHtml += '<tr>';
        strHtml += '<td class="rowData" style="maxwidth: 60%; padding-left: 10px;">' + c.Item + '</td>';
        strHtml += '<td class="rowData"><input type="text" Id="txtToolNewNum_' + c.ToolId + '" disabled title="The number of tools required for the work."/>  </td></tr> ';
    });
    strHtml += '</table>';
    return strHtml;
}
//      Function to change the CSS of the tab panels
function SwitchScheduleDetailsPanel(selectedIndex) {
    // if panel is being used then check if it is on items page, 
    // if not, check if all of the work items are selected, if not display message to client
    var brk = false;
    var itmErr = document.getElementById('pnlDlgClndrItmError');
    /*if (itmErr) {
        if (selectedIndex != 3) {
            ObjWorkSummary.WorkItems.forEach(function (itm) {
                if (!brk) {
                    var chkItm = document.getElementById('chkSelectItem_' + itm.Id);
                    if (chkItm && !chkItm.checked) {
                        itmErr.className = "warning-text center";
                        DisplayMessage(PnlItmError, 'You have not selected to book all of the work items, are you sure you want to continue?', 'red');
                        brk = true;
                    }
                }
            });
        } else {
            itmErr.className = "hidden";
        }
    }*/

    for (var index = 0; index < 3; index++) {
        var tab = document.getElementById('pnlTabPlan_' + index.toString());
        var panel = document.getElementById('pnlScheduleDetail_' + index.toString());
        if (index == selectedIndex) {
            tab.className = 'jobTab activeTab';
            panel.className = '';
        } else {
            tab.className = 'jobTab';
            panel.className = 'hidden';
        }
    }
}

function SelectedSiteChanged() {
    var panel = document.getElementById('pnlScheduleDetail_0');
    //Display a wait symble while the items are sorted for the selected work site
    panel.innerHTML = GetWaitImageForPanel(panel.id, 'Sorting Work Items. Please wait.');
    //Get the site selection element
    var siteId = GetURLParameter('SiteId');;
    if (siteId > 0) {
        // a valid site has been selected
        panel.innerHTML = GenerateItemHtml(siteId);
    } else {
        DisplayMessage(panel.id, 'There are no unscheduled items for the selected site.');
    }

    
}

function GenerateItemHtml(siteId) {
    var strHtml = '<table class="width-full searchGrid height-full">';//<col width="10%" /><col width="15%" /><col width="65%" /><col width="15%" />';
    strHtml += '<tr><th style="vertical-align: top; text-align: center;">Item No.</th><th style="vertical-align: top; text-align: center;">Species</th>';
    strHtml += '<th style="vertical-align: top; text-align: center;">Work Description</th><th style="vertical-align: top; text-align: center;">Image</th><th style="vertical-align: top; text-align: center;">Cost (' + CurrentCurrency + ')</th>';
    strHtml += '</tr>';
   //  Display items to the selected work plan
        ObjWorkPlan.ScheduledItems.forEach(function (wi) {
            strHtml += '<tr>';
            strHtml += '<td style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Name + '</td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Description + '</td>';
            strHtml += '<td style="text-align: center;" onclick="event.preventDefault(); OpenImage(2, ' + wi.Id + ');"><a href="#">Image</a></td>';
            strHtml += '<td style="vertical-align: top;">' + wi.Value.format() + '</td>';
            strHtml += '</tr>';
        });
   
    strHtml += '</table>';
    return strHtml;
}