﻿var Stage = 0;
var ItemID = 0;
var ImageUploaded = [];
var Id = 0;
var PulledDocument = [];
var base64String = '';
var FileExtension = "";
var DocumentFileName = '';
var Document_File = [];
var DocDialog = null;
function LoadDateAndStaff(docId) {

    PopulateDatefield();
    PopulateStaffList();
   
}


function CompanyDocumentsLoad(docId,DialogRef) {

   
    PopulateStaffList();
    PopulateDatefield();
    Id = docId;
    PulledBackDocument = docId;
    Document_File = Load_Documents(Id);
    PopulateDocumentFields(Document_File);
    
}
function getFileExtension(DocumentFileName) {

    FileExtension = /^.+\.([^.]+)$/.exec(DocumentFileName);
    return FileExtension == null ? "" : FileExtension[1];
}
function HookUpImage(DocumentPath)
{
    var DocumentPaths = DocumentPath;
    var length = 15;
    var fileType = FileExtension[1];

    if (fileType == "pdf") {
        window.open(DocumentPaths, '_blank', 'fullscreen=yes');
        return true;

    }
    else {
        window.open(DocumentPaths, '_blank', 'fullscreen=yes');
        return true;
    }

}
function DocumentPreview() {
    var Id = Document_File.Id;
    try {
        var DocumentPath = Ajax_CallService(
       'Services/Web/Company.asmx',
       'SaveImageFileOnServer',
       {
           Id : Id
       },
       false
      );
        HookUpImage(DocumentPath)
    }
    catch (e)
    {
        alert(e);
    }

}
function PopulateDatefield() {
    document.getElementById('txtDate').value = Date();
}

function DocumentsSaveEdit() {
    return true;
}
function DocumentsSave() {
    //debugger;
    
    //if (fuDocument.value == '' || fuDocument.files.length == 0) {
    //    alert("You have not selected any image to upload. \n Document haven't been saved to database, please add document again and make sure you selected image");
    //    return false;
    //}
    //else {
    //    File_Upload();
    //    return true;
    //}
    return true;
}
function ToggleTick()
{


}

function File_Upload() {
  
    try{


        //var lblSucceded = document.getElementById('lblSucceed');
        var fuDocument = document.getElementById('fuDocument');
        var pnlUploadContainer = document.getElementById('pnlUploadContainer');
        //lblSucceded.className = 'center hidden';
        CancelUpload = false;



        if (fuDocument.value == '' || fuDocument.files.length == 0) {
            window.alert('You have not selected any file to upload. Please close the window and try again.');
            return;
        }
        else if (!fuDocument.files[0].name.match(/.(jpg|jpeg|png|gif|pdf)$/i))
        {
            window.alert('You have select wrong file type to upload. Please close the window and try again.');
            return;
        }
        var DocumentFile = fuDocument.files[0];
        var DocumentFileName = DocumentFile.name.toString();
        var dfr = new FileReader();
        

        var wrongDocumentFile = DocumentFile.type.indexOf("image");
        var wrongDocumentFile2 = DocumentFile.type.indexOf("pdf");
        var combinedFiles = (wrongDocumentFile * wrongDocumentFile2);

        if (combinedFiles > 0) {
            window.alert('You have uploaded an invalid file type. Please close the window and try again.');
            return;
        }

        var DocumentName = document.getElementById('txtName').value;
        var DocumentType = document.getElementById('txtType').value;
        var DocumentStaff = document.getElementById('ddlStaffTable').value;


        if (DocumentName == '' || DocumentType == '') {

            alert("You havent entered sufficient information, please try again");
            return false;
        }
        var total = DocumentFile.length; loaded = 0;
        var pnlUploadProgress = document.getElementById('pnlUploadProgress');
        pnlUploadProgress.className = 'center';
        dfr.onload = function (event) {
           
            var result = event.target.result;
            var base64 = result.substring(result.indexOf("base64,") + 7);

            var chunkSize = 1024 * 150;
            var chunks = Math.ceil(base64.length / chunkSize);

            var i = 0;

            function doChunk() {
                if (CancelUpload) {
                    finishChunks();
                    return;
                }



                var chunk = base64.substring(i * chunkSize, Math.min((i + 1) * chunkSize, base64.length));

                var progress = Math.round((i / chunks) * 100);
                Ajax_CallService(
                    'Services/Web/Company.asmx',
                    'UploadDocument',
                    {
                        Id : Id,
                        Chunk: chunk,
                        New: i == 0,
                        DocumentName: DocumentName,
                        DocumentType: DocumentType,
                        DocumentStaff: DocumentStaff,
                        DocumentFileName: DocumentFileName,
                        Completed: i == chunks - 1,
                    },
                    false);

                i++;

                if (i < chunks) {
                    setTimeout(doChunk, 50);
                } else {
                    finishChunks();
                }
            }

            doChunk();
        };


        dfr.readAsDataURL(DocumentFile);
        var pnlUploadBarContainer = document.getElementById("pnlUploadBarContainer");
        dfr.onprogress = function (data) {
            console.log(data);
            if (data.lengthComputable) {
                var progress = parseInt(((data.loaded / data.total) * 100), 10);
                //console.log(progress);
                pnlUploadBarContainer.style.width = progress + '%';
                
            }
        }
    }
    catch (e)
    {
        alert(e);
    }
    //var lblSucceded = document.getElementById('lblSucceed');
    //lblSucceded.className = 'center';
}
function DocumentDelete()
{
    try{
        var fuDocument = document.getElementById('fuDocument');
        var pnlUploadContainer = document.getElementById('pnlUploadContainer');
        //var lblSucceded = document.getElementById('lblSucceed');
        CancelUpload = false;
        var Id = Document_File.Id;
        //lblSucceded.className = 'center hidden';
        Ajax_CallService(
                'Services/Web/Company.asmx',
                'DeleteDocument',
     {
         Id: Id,
     },
    false);
        //pnlUploadProgress.className = 'center';
        //var lblSucceded = document.getElementById('lblSucceed');
        //lblSucceded.className = 'center';
        var pnlUploadProgress = document.getElementById('pnlUploadProgress');
        pnlUploadProgress.className = 'center';
        var pnlUploadBarContainer = document.getElementById("pnlUploadBarContainer");
        pnlUploadBarContainer.style.width = '100%';
        LoadDocuments();
    }
    catch (e)
    {
        alert(e);
    }
}
function File_UploadEdit() {
    
    var fuDocument = document.getElementById('fuDocument');
    //var lblSucceded = document.getElementById('lblSucceed');
    var pnlUploadContainer = document.getElementById('pnlUploadContainer');
    CancelUpload = false;
    //lblSucceded.className = 'center hidden';
    debugger;

    if (fuDocument.value == ' ' || fuDocument.files.length == 0)
    {
        var DocumentName = document.getElementById('txtName').value;
        var DocumentType = document.getElementById('txtType').value;
        var DocumentStaff = document.getElementById('ddlStaffTable').value;
        var chunk = base64String;
        var DocumentFileName = document.getElementById('txtFileName').value;
            
        Ajax_CallService(
        'Services/Web/Company.asmx',
        'UploadDocument',
        {
            Id: Id,
            Chunk: chunk,
            New: i == 0,
            Completed: true,
            DocumentName: DocumentName,
            DocumentType: DocumentType,
            DocumentStaff: DocumentStaff,
            DocumentFileName: DocumentFileName
        },
        false);
        //pnlUploadProgress.className = 'center';
        //var lblSucceded = document.getElementById('lblSucceed');
        //lblSucceded.className = 'center';
    }
    else {
            
        //var lblSucceded = document.getElementById('lblSucceed');
        //lblSucceded.className = 'center hidden';
        var DocumentFile = fuDocument.files[0];
        var DocumentFileName = DocumentFile.name.toString();
        var dfr = new FileReader();
        var ImageReady = [];

        var wrongDocumentFile = DocumentFile.type.indexOf("image");
        var wrongDocumentFile2 = DocumentFile.type.indexOf("pdf");
        var combinedFiles = (wrongDocumentFile * wrongDocumentFile2);
        if (!fuDocument.files[0].name.match(/.(jpg|jpeg|png|gif|pdf)$/i))
        {
            window.alert('You have select wrong file type to upload. Please close the window and try again.');
            return;
        }

        if (combinedFiles > 0) {
            window.alert('You have uploaded an invalid file type. Please close the window and try again.');
            return;
        }
        var total = DocumentFile.length; loaded = 0;
        var pnlUploadProgress = document.getElementById('pnlUploadProgress');
        pnlUploadProgress.className = 'center';
        dfr.onload = function (event) {
            debugger;
            var result = event.target.result;
            var base64 = result.substring(result.indexOf("base64,") + 7);

            var chunkSize = 1024 * 150;
            var chunks = Math.ceil(base64.length / chunkSize);

            var i = 0;

            var DocumentName = document.getElementById('txtName').value;
            var DocumentType = document.getElementById('txtType').value;
            var DocumentStaff = document.getElementById('ddlStaffTable').value;

            function doChunk() {
                if (CancelUpload) {
                    finishChunks();
                    return;
                }



                var chunk = base64.substring(i * chunkSize, Math.min((i + 1) * chunkSize, base64.length));

                var progress = Math.round((i / chunks) * 100);
                Ajax_CallService(
                    'Services/Web/Company.asmx',
                    'UploadDocument',
                    {
                        Id: Id,
                        Chunk: chunk,
                        New: i == 0,
                        Completed: i == chunks - 1,
                        DocumentName: DocumentName,
                        DocumentType: DocumentType,
                        DocumentStaff: DocumentStaff,
                        DocumentFileName: DocumentFileName
                    },
                    false);

                i++;

                if (i < chunks) {
                    setTimeout(doChunk, 50);
                } else {
                    finishChunks();
                }
            }

            doChunk();
        };


        dfr.readAsDataURL(DocumentFile);
        //var lblSucceded = document.getElementById('lblSucceed');
        //lblSucceded.className = 'center';
        var pnlUploadBarContainer = document.getElementById("pnlUploadBarContainer");
        dfr.onprogress = function (data) {
            if (data.lengthComputable) {
                var progress = parseInt(((data.loaded / data.total) * 100), 10);
                console.log(progress);
                pnlUploadBarContainer.style.width = progress + '%';
            }
        }
    }
}

function Load_Documents(id) {
    if (id > 0) {
        var  Document_File = Ajax_CallService(
                 'Services/Web/Company.asmx',
                 'GetDocumentForId',
                 {
                     Id: Id
                 },
                 false
             );
        return Document_File;
    }   
}
function PopulateDocumentFields(Document_File) {

    var PickedStaff = Document_File;
    document.getElementById('txtName').value = Document_File.DocumentName;
    document.getElementById('txtType').value = Document_File.DocumentType;
    Document_File.DocumentDate = toDateStringFromJson(Document_File.DocumentDate, '', 'en_GB');
    document.getElementById('txtDate').value = Document_File.DocumentDate;
    PopulateDropDownoptions(document.getElementById('ddlStaffTable'), PickedStaff.Staff);
    var DocumentImageArray = Document_File.DocumentImage;
    var DocumentFileName = Document_File.DocumentFileName.toString();
    document.getElementById('txtFileName').value = DocumentFileName;
    base64String = btoa([].reduce.call(new Uint32Array(DocumentImageArray), function (p, c) { return p + String.fromCharCode(c) }, ''));
    PulledDocument = Document_File.DocumentImage;
    ItemID = Document_File.Id;
    DocumentFileName = Document_File.DocumentFileName;
    getFileExtension(DocumentFileName);
    var pnlFileTypeLabel = document.getElementById('pnlFileTypeLabel');



}
function finishChunks() {
    var fuDocument = document.getElementById('fuDocument');
    fuDocument.value = '';
    //pnlUploadProgress.className = 'center';
    //var lblSucceded = document.getElementById('lblSucceed');
    //lblSucceded.className = 'center';

}
function PopulateStaffList() {
    CurStaffList = Ajax_CallService(
    'Services/Web/Company.asmx',
    'ListAllStaff',
    {},
    false
     );
    PopulateDropDownoptions(document.getElementById('ddlStaffTable'), CurStaffList);
}
function PopulateDropDownoptions(ddlElement, selectedIndex) {
    debugger;
    var count = CurStaffList.length;
    var strHtml = '';

    for (var index = 0; index < count; index++) {
        if (selectedIndex == CurStaffList[index].Id) {
            strHtml += '<option value="' + CurStaffList[index].Id + '" selected="selected">' + CurStaffList[index].Name + '</option>';
        } else {
            strHtml += '<option value="' + CurStaffList[index].Id + '">' + CurStaffList[index].Name + '</option>';
        }
    }
    ddlElement.innerHTML = strHtml;
}
function DocumentCancelUpload()
{
    CancelUpload = true;
}
