﻿
var Section_curTab = 1;

// function to switch between sections to display the selected section's details
function SwitchSection() {
    var tabCount = document.getElementById('hfSectionTotal').value;
    for (var index = 1; index <= tabCount; index++) {
        if (index == Section_curTab) {
            document.getElementById('tab' + index.toString()).className = 'activeTab lmGridItem_Global';
            document.getElementById('pnl' + index.toString()).className = '';
            document.getElementById('lblDetails' + index.toString()).className = 'clientTabBar';
        }
        else {
            document.getElementById('tab' + index.toString()).className = '';
            document.getElementById('pnl' + index.toString()).className = 'hidden';
            document.getElementById('lblDetails' + index.toString()).className = 'clientTabBar hidden';
        }
    }
}

// function to display the dialog for new/edit section screen
function NewEditOrCopySection(sectionId, isCopy) {
    Dialog_OpenPage(
    sectionId == -1 || isCopy ? 'New Section' : 'Edit Section',
    'Dialogs/TemplateRiskSectionEdit.aspx?ID=' + sectionId,
    'content',
    600,
    400,
    true,
    {
        'Save': {
            text: 'Save',
            class: 'ui-dialog-button',
            click: function () {
                if (SaveTemplateSection(isCopy)) {
                    $(this).dialog('close');
                } else {
                    alert('Section Save Failed');
                }
            }
        },
        'Cancel': {
            text: 'Close',
            class: 'ui-dialog-button',
            click: function () {
                $(this).dialog('close');
            }
        }
    },
    'RiskSection.aspx'
    );
}

// saves the new, edited or copied section
function SaveTemplateSection(isCopy) {

    var section = {};
    var sectionEl = $("table[data-container='section']");

    // section
    if (isCopy) {
        section.Id = -1;
    } else {
        section.Id = sectionEl.attr("data-val");
    }
    section.Title = $("[data-field='sect-title']").val();
    section.ReferenceDoc = sectionEl.find("[data-field='sect-docref']").val();
    section.ChapterRef = sectionEl.find("[data-field='sect-chapref']").val();
    section.Description = sectionEl.find("[data-field='sect-desc']").val();

    if (section.Title === "") {
        alert('Please add a section name.');
        return false;
    }

    // update the section and criteria document references
    var retVal = Ajax_CallService(
        'Services/Web/Template.asmx',
        'InsertOrUpdateSectionTemplate',
        {
            section: section
        },
        false
    );


    location.reload();
    return retVal > 0;
}

function CancelSection(sectionId) {
    var retVal = confirm("Are you sure you want to remove this Risk Section?");

    if (retVal) {
        Ajax_CallService(
            'Services/Web/Template.asmx',
            'CancelSectionTemplate',
            {
                sectionId: sectionId
            },
            false
        );

        location.reload();
    }
}