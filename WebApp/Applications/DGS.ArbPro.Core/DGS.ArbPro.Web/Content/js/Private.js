﻿function GetUser(strEmail) {
    return Ajax_CallService('Services/Web/Account.asmx', 'ReturnUserForEmail', { UserName: strEmail }, false);
}

function MarkUserTsAndCsAccepted(strEmail, bAccepted) {
    return Ajax_CallService('Services/Web/Account.asmx', 'MarkUserTsAndCsAccepted', { UserName: strEmail, Accepted: bAccepted }, false);
}

function ChangeUserPassword(strEmail, strOldPassword, strConfirmPassword, strNewPassword) {
    return Ajax_CallService('Services/Web/Account.asmx', 'ChangeUserPassword', { UserName: strEmail, OldPassword: strOldPassword, ConfirmPassword: strConfirmPassword, NewPassword: strNewPassword }, false);
}

function SignOut() {
    return Ajax_CallService('Services/Web/Account.asmx', 'UserLogOut', {}, false);
}