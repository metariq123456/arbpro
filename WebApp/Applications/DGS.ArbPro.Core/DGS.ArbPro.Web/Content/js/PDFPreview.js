﻿
function ManualQuoteDocuments_Load() {
    PopulatePdfPreview();
}

function PopulatePdfPreview(calendarId) {
    try {
        Ajax_CallService(
                'Services/Web/Report.asmx',
                'OpenPreview',
                {
                    calendarId: calendarId,
                },
                true, DisplayPdf);
    }
    catch (err) {
        DisplayMessage('pnlImgDisplayContainer', 'Error: unable to contact arbPro Service');
    }
}

function DisplayPdf(result) {
    if (result) {
        HookPdfToDownload();
        $('#pdfPreview').attr('src', 'Controls/PDFViewer.aspx?preview=1&v=' + Math.floor((Math.random() * 1000000) + 1));
        document.getElementById('pnlPdfDisplayContainer').className = '';
        document.getElementById('pnlImgDisplayContainer').className = 'hidden';
        document.getElementById('pnlErrorDisplay').className = 'hidden';
    }
    else {
        DisplayMessage('pnlImgDisplayContainer', 'Error: failed to create PDF document');
    }
}

//      Required! - Attach the click event handler for the Download button
function HookPdfToDownload() {
    $("#previewToolbar").css('visibility', 'visible');
    var openPdf = document.getElementById('btnDownload');

    openPdf.onclick = function () {
        window.open($("#pdfPreview").contents().find("#hiddenGeneratedPdfUrl").val());
    }
}