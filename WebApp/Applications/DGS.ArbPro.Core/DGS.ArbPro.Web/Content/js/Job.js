﻿var Job_curTab = 1;
var Quote_curTab = 1;
var Job_listShown = true;
var JobID = -1;
var ListJobSites = null;
var Markers = { workBooked: false, company: null }

function SwitchTab() {
    switch (Job_curTab) {
        case 1:
            document.getElementById('tabEnquiry').className = 'jobTab activeTab';
            document.getElementById('tabQuote').className = 'jobTab';
            document.getElementById('tabWork').className = 'jobTab';
            document.getElementById('tabInvoice').className = 'jobTab';
            document.getElementById('tabHistory').className = 'jobTab';

            document.getElementById('pnlTabEnquiry').className = '';
            document.getElementById('pnlTabQuote').className = 'hidden';
            document.getElementById('pnlTabWork').className = 'hidden';
            document.getElementById('pnlTabInvoice').className = 'hidden';
            document.getElementById('pnlTabHistory').className = 'hidden';

            UpdateEnquiry();
            break;
        case 2:
            document.getElementById('tabEnquiry').className = 'jobTab';
            document.getElementById('tabQuote').className = 'jobTab activeTab';
            document.getElementById('tabWork').className = 'jobTab';
            document.getElementById('tabInvoice').className = 'jobTab';
            document.getElementById('tabHistory').className = 'jobTab';

            document.getElementById('pnlTabEnquiry').className = 'hidden';
            document.getElementById('pnlTabQuote').className = '';
            document.getElementById('pnlTabWork').className = 'hidden';
            document.getElementById('pnlTabInvoice').className = 'hidden';
            document.getElementById('pnlTabHistory').className = 'hidden';

            SwitchQuoteTab();
            RefreshQuoteList();
            break;
        case 3:
            document.getElementById('tabEnquiry').className = 'jobTab';
            document.getElementById('tabQuote').className = 'jobTab';
            document.getElementById('tabWork').className = 'jobTab activeTab';
            document.getElementById('tabInvoice').className = 'jobTab';
            document.getElementById('tabHistory').className = 'jobTab';

            document.getElementById('pnlTabEnquiry').className = 'hidden';
            document.getElementById('pnlTabQuote').className = 'hidden';
            document.getElementById('pnlTabWork').className = '';
            document.getElementById('pnlTabInvoice').className = 'hidden';
            document.getElementById('pnlTabHistory').className = 'hidden';

            RefreshQuoteList();
            break;
        case 4:
            document.getElementById('tabEnquiry').className = 'jobTab';
            document.getElementById('tabQuote').className = 'jobTab';
            document.getElementById('tabWork').className = 'jobTab';
            document.getElementById('tabInvoice').className = 'jobTab activeTab';
            document.getElementById('tabHistory').className = 'jobTab';

            document.getElementById('pnlTabEnquiry').className = 'hidden';
            document.getElementById('pnlTabQuote').className = 'hidden';
            document.getElementById('pnlTabWork').className = 'hidden';
            document.getElementById('pnlTabInvoice').className = '';
            document.getElementById('pnlTabHistory').className = 'hidden';

            RefreshQuoteList();
            RefreshInvoiceList();
            break;
        case 5:
            document.getElementById('tabEnquiry').className = 'jobTab';
            document.getElementById('tabQuote').className = 'jobTab';
            document.getElementById('tabWork').className = 'jobTab';
            document.getElementById('tabInvoice').className = 'jobTab';
            document.getElementById('tabHistory').className = 'jobTab activeTab';

            document.getElementById('pnlTabEnquiry').className = 'hidden';
            document.getElementById('pnlTabQuote').className = 'hidden';
            document.getElementById('pnlTabWork').className = 'hidden';
            document.getElementById('pnlTabInvoice').className = 'hidden';
            document.getElementById('pnlTabHistory').className = 'n';

            UpdateJobHistory();
            break;
        default:
            document.getElementById('tabEnquiry').className = 'jobTab';
            document.getElementById('tabQuote').className = 'jobTab';
            document.getElementById('tabWork').className = 'jobTab';
            document.getElementById('tabInvoice').className = 'jobTab';
            document.getElementById('tabHistory').className = 'jobTab';

            document.getElementById('pnlTabEnquiry').className = 'hidden';
            document.getElementById('pnlTabQuote').className = 'hidden';
            document.getElementById('pnlTabWork').className = 'hidden';
            document.getElementById('pnlTabInvoice').className = 'hidden';
            document.getElementById('pnlTabHistory').className = 'hidden';
            break;
    }
}

function SwitchQuoteTab() {
    switch (Quote_curTab) {
        case 1: //pnlQuotesDescriptionContainer
            document.getElementById('tabQuoteDetails').className = 'jobTab activeTab'
            document.getElementById('tabQuoteNotes').className = 'jobTab'

            document.getElementById('pnlQuotesDescriptionContainer').className = 'width-full'
            document.getElementById('pnlQuotesNotesContainer').className = 'hidden'
            break;
        case 2:
            document.getElementById('tabQuoteDetails').className = 'jobTab'
            document.getElementById('tabQuoteNotes').className = 'jobTab activeTab'

            document.getElementById('pnlQuotesDescriptionContainer').className = 'hidden'
            document.getElementById('pnlQuotesNotesContainer').className = 'width-full'

            GetNotesList();
            break;//pnlQuotesNotesContainer
        default:
            document.getElementById('tabQuoteDetails').className = 'jobTab'
            document.getElementById('tabQuoteNotes').className = 'jobTab'

            document.getElementById('pnlQuotesDescriptionContainer').className = 'hidden'
            document.getElementById('pnlQuotesNotesContainer').className = 'hidden'
            break;
    }
}

function NewJob() {
    var ClientID = document.getElementById('hfClientID').value;

    var NewJobID = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'CreateNewJob',
            {
                ClientID: ClientID
            },
            false
        );

    if (NewJobID > 0) {
        window.location = 'Job.aspx?JobID=' + NewJobID;
    }
}

function DeleteJob() {

}

function StartList() {
    UpdateList();
}

function ToggleList() {
    var Panel = $('#pnlJobsList');
    var Filter = $('#pnlJobsListFilter');
    var lblName = $('#lblClientName');
    var lblLink = $('#lnkReturnToClient');
    var List = $('#pnlJobsListList');

    if (Job_listShown) {
        Job_listShown = false;
        Panel.animate({ width: 10 }, 500);
        List.hide(100);
        setTimeout(function () {
            $('#pnlJobsListFilter').css('width', '0px');
            $('#pnlJobsListFilter').css('height', '42px');
            $('#lblClientName').css('visibility', 'hidden');
            $('#lnkReturnToClient').css('visibility', 'hidden');
        }, 490);
    } else {
        Job_listShown = true;
        Panel.animate({ width: 200 }, 500);
        Filter.css('width', '190px');
        Filter.css('height', 'auto');
        lblName.css('visibility', 'visible');
        lblLink.css('visibility', 'visible');
        List.delay(500).show(100);
    }
}

function UpdateList() {
    var Grid = document.getElementById('pnlJobsListList');
    var Client = GetUser(Page_strUserEmail);
    var ClientID = document.getElementById('hfClientID').value;
    JobID = document.getElementById('hfJobID').value;

    Ajax_CallService(
            'Services/Web/ClientData.asmx',
            'GetClientJobList',
            {
                ClientID: ClientID
            },
            true,
            function (Jobs) {
                var listHtml = '<table id="jobListTable" class="searchGrid width-full">';

                function SortByID(a, b) {
                    return ((a.Id > b.Id) ? -1 : ((a.Id < b.Id) ? 1 : 0));
                }
                Jobs.sort(SortByID);

                Jobs.forEach(function (j) {
                    listHtml += '<tr onclick="window.location = \'/Job.aspx?JobID=' + j.Id + '\';"><td class="' + (j.Id == JobID ? 'selected' : '') + '">' + j.SideBarDetail + '</td></tr>';
                    //listHtml = listHtml + ';
                    //listHtml = listHtml + 'Job#' + j.Id;
                });
                listHtml = listHtml + '</table>';
                Grid.innerHTML = listHtml;
            }
        );
}

function UpdateJobSiteList() {
    var List = document.getElementById('lstJobSites');
    var Client = GetUser(Page_strUserEmail);
    JobID = document.getElementById('hfJobID').value;

    var JobSites = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetJobSiteList',
        {
            JobID: JobID
        },
        false);

    var listHtml = '<table class="searchGrid width-full">';
    JobSites.forEach(function (js) {
        listHtml = listHtml + '<tr onclick="ViewSite(' + js.SiteId + '); return false;" >';
        listHtml = listHtml + '<td style="padding-left: 10px; padding-right: 10px;">' + js.SiteString + '</td>'
        listHtml = listHtml + '</tr>';
    });
    listHtml = listHtml + '</table>';
    List.innerHTML = listHtml;
}

function AddSite() {
    Dialog_OpenPage(
        'Edit Job Sites',
        'Dialogs/JobSite.aspx',
        'content',
        600,
        320,
        true,
        {
            'Add': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    if (JobSite_Close()) {
                        UpdateJobSiteList();
                        $(this).dialog('close');
                        setTimeout(function () { SwitchTab(); }, 0);
                    } else {
                        alert('Add Job Site Failed');
                    }
                }
            },
            'Cancel': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/JobSite.js',
        function () {
            JobSite_Load();
        });
}

function RemoveJobSite(JobID, SiteID) {
    var ClientID = document.getElementById('hfClientID').value;

    return Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'RemoveJobSiteFromJob',
        {
            JobID: JobID,
            SiteID: SiteID,
        },
        false);
}

function ViewSite(iSiteID) {
    Dialog_OpenPage(
        iSiteID == -1 ? 'New Site' : 'Edit Site',
        'Dialogs/Site.aspx?ID=' + iSiteID,
        'content',
        700,
        500,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    if (SaveSite()) {
                        UpdateJobSiteList();
                        $(this).dialog('close');
                    } else {
                        alert('Site Save Failed');
                    }
                }
            },
            'Remove': {
                text: 'Remove from Job',
                class: 'ui-dialog-button',
                click: function () {
                    if (RemoveJobSite(JobID, iSiteID)) {
                        UpdateJobSiteList();
                        $(this).dialog('close');
                    } else {
                        alert('Site Removal Failed for Site ' + iSiteId + ' JobSite ' + jobSiteId);
                    }
                }
            },
            'Cancel': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/Site.js',
        function () {
            Site_Load();
        });
}

function UpdateEnquiry() {
    JobID = document.getElementById('hfJobID').value;

    var Job = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetJobEnquiry',
        {
            JobID: JobID
        },
        false);

    var Reporter = Ajax_CallService(
        'Services/Web/Account.asmx',
        'GetUserForID',
        {
            UserID: Job.ReporterID
        },
        false);

    document.getElementById('lblJobEnquirySource').textContent = Job.Source;
    document.getElementById('lblJobEnquiryStaff').textContent = Reporter != null ? Reporter.FirstName + ' ' + Reporter.LastName : '';
    document.getElementById('lblJobEnquiryComments').textContent = Job.Comments;

    LoadAppointments();
}

function LoadAppointments() {
    JobID = document.getElementById('hfJobID').value;

    var Appts = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetUpcomingQuoteAppointmentsForJob',
        {
            JobID: JobID
        },
        false);

    var panel = document.getElementById('pnlJobEnquiryAppointments');
    var strHtml = '';

    Appts.forEach(function (appt) {
        var Staff = Ajax_CallService(
            'Services/Web/Company.asmx',
            'GetStaffForId',
            {
                Id: appt.SurveyorID
            },
            false);

        var JobSite = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetJobSite',
            {
                JobSiteID: appt.JobSiteID
            },
            false);

        var ApptDate = new Date(parseInt(appt.StartDateTime.substr(6)));
        strHtml = strHtml + '<div class="apptBox padded float-left">';
        strHtml = strHtml + '<div';
        strHtml = strHtml + ' onclick="EditAppointment(' + appt.Id + ');">';
        strHtml = strHtml + '<span class="apptTitle float-left">' + ApptDate.toLocaleDateString() + '</span>';
        strHtml = strHtml + '<span class="apptTitle float-right">' + zeroPad(ApptDate.getHours(), 2) + ':' + zeroPad(ApptDate.getMinutes(), 2) + '</span>';

        strHtml = strHtml + '<div class="width-full float-left bordered-bottom">';
        strHtml = strHtml + '</div>';

        strHtml = strHtml + '<div class="width-full float-left" style="overflow-y: auto;">';

        strHtml = strHtml + '<div class="apptField float-left">';
        strHtml = strHtml + '<span class="apptLabel width-full float-left">Site:</span>';
        strHtml = strHtml + '<span class="apptInput width-full float-left">' + JobSite.SiteString + '</span>';
        strHtml = strHtml + '</div>';

        strHtml = strHtml + '<div class="apptField float-left">';
        strHtml = strHtml + '<span class="apptLabel width-full float-left">Surveyor:</span>';
        strHtml = strHtml + '<span class="apptInput width-full float-left">' + Staff.Name + '</span>';

        strHtml = strHtml + '</div>';

        strHtml = strHtml + '</div>';

        strHtml = strHtml + '</div>';

        strHtml = strHtml + '<div>';
        strHtml += '<button type="button" onclick="GenerateManualQuoteDocs(' + appt.Id.toString() + ');" class="button-thin float-right padded">Print</button>';
        strHtml += '<button type="button" onclick="DeleteQuoteAppointment(' + appt.Id.toString() + ');" class="button-thin float-left padded">Cancel</button>';
        strHtml = strHtml + '</div>';
        strHtml = strHtml + '</div>';
    });

    panel.innerHTML = strHtml;
}

function EditAppointment(iApptId) {
    var appt = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetQuoteAppointmentForId',
        {
            ApptId: iApptId
        },
        false);

    var jobSiteList = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetJobSiteList',
        {
            JobID: JobID
        },
        false);

    var jobSiteId = appt.JobSiteID;
    var edit = true;
    var enquiry = false;
    Dialog_OpenPage('Edit Appointment',
        'Dialogs/CalendarEditAppointment.aspx?SurveyorID=' + appt.SurveyorID.toString() + '&AppId=' + iApptId,
        'content',
        1200,
        850,
        true,
        {
            'Done': {
                text: 'Done',
                class: 'ui-dialog-button',
                click: function () {
                    SaveExistingAppointment(appt.Id, appt.JobSiteID);
                    LoadAppointments();
                    $(this).dialog('close');
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/CalendarEditAppointment.js',
        function () {
            LoadCalendar(jobSiteList, edit, enquiry, jobSiteId);
        });
}

function SaveExistingAppointment(iApptID, iJobSiteID) {
    var start = $('#hfAppStart').val();
    var end = $('#hfAppEnd').val();
    var surveyor = $('#hfAppSurveyor').val();
    var completed = $('#chkAppointmentCompleted').is(':checked');

    Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'SaveQuoteAppointment',
        {
            ApptID: iApptID,
            JobSiteID: iJobSiteID,
            SurveyorID: surveyor,
            StartDateTime: start.toLocaleString(),
            EndDateTime: end.toLocaleString(),
            Completed: completed
        },
        false);
}

function NewAppointment() {

    var Surveyors = Ajax_CallService(
        'Services/Web/Company.asmx',
        'ListSurveyors',
        {},
        false);

    //Get Client Id and retrieve list of JobSites for client
    var clientId = document.getElementById('hfClientID').value;

    var jobSiteList = Ajax_CallService(
    'Services/Web/WorkFlow.asmx',
    'GetJobSiteList',
    {
        JobID: JobID
    },
    false);
    var edit = false;
    var enquiry = false;
    Dialog_OpenPage('New Appointment',
        'Dialogs/CalendarEditAppointment.aspx?SurveyorID=' + Surveyors[0].Id.toString(),
        'content',
        1200,
        850,
        true,
        {
            'Done': {
                text: 'Done',
                class: 'ui-dialog-button',
                click: function () {
                    SaveNewAppointment();
                    $(this).dialog('close');
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/CalendarEditAppointment.js',
        function () {
            LoadCalendar(jobSiteList, edit, enquiry);
        });
}

function SaveNewAppointment() {
    var start = $('#hfAppStart').val();
    var end = $('#hfAppEnd').val();
    var surveyor = $('#hfAppSurveyor').val();

    var jobSiteID = parseInt($('#hfJobSiteId').val());


    Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'SaveQuoteAppointment',
        {
            ApptID: -1,
            JobSiteID: jobSiteID,
            SurveyorID: surveyor,
            StartDateTime: start.toLocaleString(),
            EndDateTime: end.toLocaleString(),
            Completed: false
        },
        false);

    LoadAppointments();
}

//Function to display an alert box for conformation of deletion of a Quote appointment
// and then delete the appointment
function DeleteQuoteAppointment(appointmentId) {
    //pnlAlertBoxDlg
    Dialog_OpenPage(
        'Cancel Quote Appointment?',
        'Dialogs/Alert.aspx?',
        'content',
        500,
        150,
        true,
        {
            'Yes': {
                text: 'Yes',
                class: 'ui-dialog-button',
                click: function () {
                    //Display wait animation
                    document.getElementById('pnlAlertBoxDlg').innerHTML = GetWaitImageForPanel('pnlAlertBoxDlg', 'Please Wait.');
                    //Blocking service call to delete the appointment
                    var completed = Ajax_CallService(
                        'Services/Web/Calendar.asmx',
                        'DeleteQuoteEvent',
                         {
                             eventId: appointmentId
                         },
                        false);
                    LoadAppointments();
                    $(this).dialog('close');
                }
            },
            'No': {
                text: 'No',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/AlertBox.js',
        function () { AlertBox_Show('Are you sure you want to cancel this Quote appointment?'); });
}

function BuildItemTable(itemArray, SiteVatRates, JobVatRates, itemCallBack) {
    itemArray.forEach(function (item) {
        var found = false;
        SiteVatRates.forEach(function (vr) {
            if (vr.rate == item.VatRate) {
                vr.value += item.Value;
                found = true;
            }
        });

        if (!found)
            SiteVatRates.push({ rate: item.VatRate, value: item.Value });

        found = false;
        JobVatRates.forEach(function (vr) {
            if (vr.rate == item.VatRate) {
                vr.value += item.Value;
                found = true;
            }
        });

        if (!found)
            JobVatRates.push({ rate: item.VatRate, value: item.Value });

        itemCallBack(item);
    });
}

function UpdateJobQuoteItemList() {
    var Grid = document.getElementById('grdQuotes');
    var Client = GetUser(Page_strUserEmail);
    JobID = document.getElementById('hfJobID').value;

    var strHtml = '';

    var JobSites = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetJobSiteList',
        {
            JobID: JobID
        },
        false);

    if (!ListJobSites || ListJobSites.length != JobSites.length) {
        ListJobSites = JobSites;
    }

    var ddlQuoteID = document.getElementById('ddlQuoteID');
    var QuoteID = ddlQuoteID.options[(ddlQuoteID.selectedIndex >= 0 ? ddlQuoteID.selectedIndex : 0)].value;
    document.getElementById('hfQuoteId').value = QuoteID;

    var Quote = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetQuote',
        {
            QuoteID: QuoteID
        },
        false);
    if (Quote.Status == 0) {
        document.getElementById('btnProduceQuote').className = 'button-basic padded float-right';
        document.getElementById('btnAcceptQuote').className = 'button-basic padded float-right hidden';
        document.getElementById('btnRejectQuote').className = 'button-basic padded float-right hidden';
        //Disable all work buttons as they are not relevent when the quote has not been accepted
        setTimeout(function () {
            $(".BookWork").prop("disabled", true);
        }, 0);
        document.getElementById('btnMarkCompleted').disabled = true;
        //document.getElementById('btnProduceWorkPacks').disabled = true;
    }
    else if (Quote.Status == 1) {
        document.getElementById('btnProduceQuote').className = 'button-basic padded float-right hidden';
        document.getElementById('btnAcceptQuote').className = 'button-basic padded float-right';
        document.getElementById('btnRejectQuote').className = 'button-basic padded float-right';
        //Disable all work buttons as they are not relevent when the quote has not been accepted
        setTimeout(function () {
            $(".BookWork").prop("disabled", true);
        }, 0);
        document.getElementById('btnMarkCompleted').disabled = true;
        //document.getElementById('btnProduceWorkPacks').disabled = true;

    } else {
        document.getElementById('btnProduceQuote').className = 'button-basic padded float-right hidden';
        document.getElementById('btnAcceptQuote').className = 'button-basic padded float-right hidden';
        document.getElementById('btnRejectQuote').className = 'button-basic padded float-right hidden';
        //Enable all work buttons as are relevent when the quote has been accepted
        setTimeout(function () {
            $(".BookWork").prop("disabled", false);
        }, 0);
        document.getElementById('btnMarkCompleted').disabled = false;
        //document.getElementById('btnProduceWorkPacks').disabled = false;

    }

    document.getElementById('hfQuoteStatus').value = Quote.Status;
    var JobVatRates = new Array();

    JobSites.forEach(function (js) {

        strHtml = strHtml + '<table class="searchGrid small width-full"><col width="5%"><col width="20%"><col width="55%"><col width="10%"><col width="10%">';

        strHtml = strHtml + '<tr>';
        strHtml = strHtml + '<th colspan="5"><span class="float-left padded">' + js.SiteString + '</span>';
        strHtml = strHtml + '<input type="button" class="button-thin padded float-right' + (Quote.Status == 0 ? "" : " hidden") + '" value="Edit Items" onclick="QuoteItemsDialog_Open(' + js.Id + ');" />';
        strHtml = strHtml + '<input type="button" class="button-thin padded float-right" value="View Map" onclick="ShowItemMap(' + js.Id + ');" /></th><th>';
        //strHtml = strHtml + '<input type="button" class="button-thin float-right' + (Quote.Status == 0 ? "" : " hidden") + '" value="Add Items" onclick="AddItem(1, ' + js.Id + '); return false;" /></th>';
        strHtml = strHtml + '</tr>';

        var QuoteItems = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetJobQuoteItemList',
            {
                JobSiteID: js.Id,
                QuoteID: QuoteID
            },
            false);

        var SiteVatRates = new Array();

        BuildItemTable(QuoteItems, SiteVatRates, JobVatRates, function (qi) {
            strHtml = strHtml + '<tr>';
            strHtml = strHtml + '<td style="vertical-align: top; text-align: center;"' + (Quote.Status == 0 ? ' onclick="EditQuoteItem(' + qi.Id + ')"' : '') + '>' + qi.Code + '</td>';
            strHtml = strHtml + '<td style="vertical-align: top;"' + (Quote.Status == 0 ? ' onclick="EditQuoteItem(' + qi.Id + ')"' : '') + '>' + qi.Name + '</td>';
            strHtml = strHtml + '<td' + (Quote.Status == 0 ? ' onclick="EditQuoteItem(' + qi.Id + ')"' : '') + '>' + qi.Description + '</td>';
            strHtml = strHtml + '<td style="text-align: center" onclick="event.preventDefault(); OpenImage(1, ' + qi.Id + ');"><a href="#">Image</a></td>';
            strHtml = strHtml + '<td style="text-align: right; padding-right: 3px; padding-left: 3px; vertical-align: bottom;"' + (Quote.Status == 0 ? ' onclick="EditQuoteItem(' + qi.Id + ')"' : '') + '>' + (qi.Value > 0 ? '£' + qi.Value : '') + '</td>';
            strHtml = strHtml + '</tr>';
        });

        strHtml = strHtml + '</table>';
        //NOTE: Value updates
        //Check if site subtotals need to be displayed
        if (ListJobSites.length > 1) {
            strHtml += GetSiteTotalHtml(SiteVatRates, JobSites.length, 'Site');
            strHtml += '<hr/>';
        }
    });
    //Display the invoice totals
    strHtml += GetSiteTotalHtml(JobVatRates, JobSites.length, 'Quote');

    Grid.innerHTML = strHtml;

    UpdateQuoteHistory();
    GetNotesList();
}

function UpdateQuoteHistory() {
    var Grid = document.getElementById('pnlQuoteHistory');

    JobID = document.getElementById('hfJobID').value;

    var History = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetQuoteHistoryForJob',
        {
            JobID: JobID
        },
        false);

    if (History == null || History.length == 0) {
        Grid.innerHTML = '';
        return;
    }

    var strHtml = '<table class="searchGrid small width-full">';
    strHtml = strHtml + '<col width="15%"><col width="15%"><col width="20%"><col width="25%"><col width="25%">';
    strHtml = strHtml + '<tr>';
    strHtml = strHtml + '<th>Reference No</th>';
    strHtml = strHtml + '<th>Doc Type</th>';
    strHtml = strHtml + '<th>Created On</th>';
    strHtml = strHtml + '<th>Sent Via</th>';
    strHtml = strHtml + '<th>Sent On</th>';
    strHtml = strHtml + '<th></th>';
    strHtml = strHtml + '</tr>';

    History.forEach(function (h) {
        strHtml = strHtml + '<tr>';
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;">' + h.ReferenceNo + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top;">' + h.ItemTypeString + '</td>';
        var CreationDate = new Date(parseInt(h.CreationDate.substr(6)));
        var SentDate = new Date(parseInt(h.SentDate.substr(6)));
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;">' + (CreationDate.getYear() > 1 ? CreationDate.toDateString() : 'N/A') + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top;">' + h.DispatchMethodString + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;">' + (SentDate.getYear() > 1 ? SentDate.toDateString() : 'N/A') + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;"><a target="_blank" href="Images/ReportImage.aspx?Id=' + h.Id + '">View</a></td>';
        strHtml = strHtml + '</tr>';
    });

    strHtml = strHtml + '</table>';

    Grid.innerHTML = strHtml;
}

function UpdateJobWorkItemList() {
    var Grid = document.getElementById('grdWork');
    //var Client = GetUser(Page_strUserEmail);
    var ddlWQID = document.getElementById('ddlQuoteID');  //document.getElementById('ddlWorkQuoteID');
    var QuoteID = ddlWQID.options[ddlWQID.selectedIndex].value;

    var strHtml = '';

    JobID = document.getElementById('hfJobID').value;
    var JobSites = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetJobSiteList',
        {
            JobID: JobID
        },
        false);

    var JobVatRates = new Array();

    Markers.workBooked = false;
    JobSites.forEach(function (js) {
        strHtml = strHtml + '<table class="searchGrid width-full"><col width="5%" /><col width="20%" /><col width="55%" /><col width="10%" /><col width="10%" />';

        strBookWorkLabel = js.BookedWorkItemCount > 0 ? "Rearrange Booked Work" : "Book Work";

        strHtml = strHtml + '<tr>';
        strHtml = strHtml + '<th colspan="5" style="padding: 2px;"><span>' + js.SiteString + '</span>';

        strHtml = strHtml + '<input type="button" class="BookWork button-basic padded float-right" value="Add Items" onclick="AddItem(2, ' + js.Id + '); return false;" />';
        strHtml = strHtml + '<input type="button" class="BookWork button-basic padded float-right" value="' + strBookWorkLabel + '" onclick="BookWork(' + js.Id + '); return false;" />';

        Markers.workBooked = Markers.workBooked || js.BookedWorkItemCount > 0;
        if (js.BookedWorkItemCount > 0 && js.BookedWorkStartDate != null) {
            strHtml = strHtml + '<span class="padded float-right" style="font-weight: normal; bottom: 0px; padding-top: 4px;">';
            strHtml = strHtml + new Date(parseInt(js.BookedWorkStartDate.substr(6))).toDateString();
            strHtml = strHtml + '</span>';
        }

        strHtml = strHtml + '</th>';
        strHtml = strHtml + '</tr>';

        var WorkItems = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetJobSiteWorkItemList',
            {
                JobSiteID: js.Id,
                QuoteID: QuoteID
            },
            false);

        var SiteVatRates = new Array();


        BuildItemTable(WorkItems, SiteVatRates, JobVatRates, function (wi) {
            if (!wi.Completed) {
                strHtml = strHtml + '<tr>';
                strHtml = strHtml + '<td onClick="EditWorkItem(' + wi.Id + ');" style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
                strHtml = strHtml + '<td onClick="EditWorkItem(' + wi.Id + ');" style="vertical-align: top;">' + wi.Name + '</td>';
                strHtml = strHtml + '<td onClick="EditWorkItem(' + wi.Id + ');" style="vertical-align: top;">' + wi.Description + '</td>';
                strHtml = strHtml + '<td style="text-align: center;" onclick="event.preventDefault(); OpenImage(2, ' + wi.Id + ');"><a href="#">Image</a></td>';
                strHtml = strHtml + '<td style="text-align: center; cursor: default;"><input id="chkWorkItem' + wi.Id + '" type="checkbox"' + (wi.Completed ? ' checked ' : '') + ' style="cursor: pointer;"/></td>';
                strHtml = strHtml + '</tr>';
            }
            else {
                strHtml = strHtml + '<tr style="background-color: lightGray;">';
                strHtml = strHtml + '<td style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
                strHtml = strHtml + '<td style="vertical-align: top;">' + wi.Name + '</td>';
                strHtml = strHtml + '<td style="vertical-align: top;">' + wi.Description + '</td>';
                strHtml = strHtml + '<td style="text-align: center;" onclick="event.preventDefault(); OpenImage(2, ' + wi.Id + ',' + true + ');"><a href="#">Image</a></td>';
                strHtml = strHtml + '<td style="text-align: center; cursor: default;"><input disabled id="chkWorkItem' + wi.Id + '" type="checkbox"' + (wi.Completed ? ' checked ' : '') + ' style="cursor: pointer;"/></td>';
                strHtml = strHtml + '</tr>';
            }
        });
        strHtml = strHtml + '<tr><td colspan="5" style="padding:10px; background-color: white; cursor: default;">&nbsp;</td></tr>';
        strHtml = strHtml + '</table>';

        if (ListJobSites.length > 1) {
            strHtml += GetSiteTotalHtml(SiteVatRates, JobSites.length, 'Site');
            strHtml += '<hr/>';
        }

    });
    strHtml += GetSiteTotalHtml(JobVatRates, JobSites.length, 'Work');

    Grid.innerHTML = strHtml;
}

function UpdateJobInvoiceItemList() {
    var Grid = document.getElementById('grdInvoice');
    var Client = GetUser(Page_strUserEmail);
    JobID = document.getElementById('hfJobID').value;

    var strHtml = '';

    var JobSites = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetJobSiteList',
        {
            JobID: JobID
        },
        false);

    var JobVatRates = new Array();

    JobSites.forEach(function (js) {

        strHtml = strHtml + '<table class="searchGrid small width-full"><col width="5%"><col width="20%"><col width="55%"><col width="10%"><col width="10%">';

        strHtml = strHtml + '<tr>';
        strHtml = strHtml + '<th colspan="3">' + js.SiteString + '</th><th colspan="2" >';
        strHtml = strHtml + '<input type="button" class="button-thin float-right" value="Edit Items" onclick="AddItem(3, ' + js.Id + '); return false;" /></th>';
        strHtml = strHtml + '</tr>';


        //var ddlInvoiceID = document.getElementById('ddlInvoiceID');
        //var InvoiceID = ddlInvoiceID.options[ddlInvoiceID.selectedIndex].value;
        var ddlWQID = document.getElementById('ddlQuoteID'); //document.getElementById('ddlWorkQuoteID');
        var QuoteID = ddlWQID.options[ddlWQID.selectedIndex].value;

        var InvoiceItems = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetJobSiteInvoiceItemList',
            {
                JobSiteID: js.Id,
                QuoteID: QuoteID
            },
            false);

        var SiteVatRates = new Array();

        BuildItemTable(InvoiceItems, SiteVatRates, JobVatRates, function (ii) {
            strHtml = strHtml + '<tr>';
            strHtml = strHtml + '<td ' + (ii.Status == 0 ? 'onClick="EditInvoiceItem(' + ii.Id + ');" ' : '') + 'style="vertical-align: top; text-align: center;">' + ii.Code + '</td>';
            strHtml = strHtml + '<td ' + (ii.Status == 0 ? 'onClick="EditInvoiceItem(' + ii.Id + ');" ' : '') + 'style="vertical-align: top;">' + ii.Name + '</td>';
            strHtml = strHtml + '<td ' + (ii.Status == 0 ? 'onClick="EditInvoiceItem(' + ii.Id + ');" ' : '') + '>' + ii.Description + '</td>';
            strHtml = strHtml + '<td style="text-align: center" ' + (true ? 'onclick="event.preventDefault(); OpenImage(3, ' + ii.Id + ');"' : '') + '>' + (true ? '<a href="#">Image</a>' : '') + '</td>';
            strHtml = strHtml + '<td ' + (ii.Status == 0 ? 'onClick="EditInvoiceItem(' + ii.Id + ');" ' : '') + 'style="text-align: right; padding-right: 3px; padding-left: 3px; vertical-align: bottom;">' + (ii.Value > 0 ? '£' + ii.Value : '') + '</td>';
            strHtml = strHtml + '</tr>';
            SiteTotal = SiteTotal + ii.Value;
        });

        strHtml = strHtml + '</table>';

        var SiteTotal = 0.0;

        SiteVatRates.forEach(function (vr) {
            SiteTotal += vr.value;
        });

        if (JobSites.length > 1) {
            strHtml = strHtml + '<div style="text-align: right; margin-top: 5px; margin-right: 10px;">';
            strHtml = strHtml + '<span>Site Subtotal: </span><span><b>£' + SiteTotal + '</b></span><br>';
        }

        SiteVatRates.forEach(function (vr) {
            if (JobSites.length > 1) {
                strHtml = strHtml + '<span>Site VAT @ ' + (vr.rate) + '%: </span><span><b>£' + (vr.value * (vr.rate / 100)) + '</b></span><br>';
            }
            SiteTotal += vr.value * (vr.rate / 100);

        });

        if (JobSites.length > 1) {
            strHtml = strHtml + '<span>Site Total inc. VAT: </span><span><b>£' + SiteTotal + '</b></span><br>';
            strHtml = strHtml + '</div>';
        }
    });
    if (JobSites.length > 1) {
        strHtml = strHtml + '<hr />';
    }

    var InvoiceTotal = 0.0;

    JobVatRates.forEach(function (vr) {
        InvoiceTotal += vr.value;
    });

    strHtml = strHtml + '<div style="text-align: right; margin-top: 5px; margin-right: 10px;">';
    strHtml = strHtml + '<span>Subtotal: </span><span><b>£' + InvoiceTotal + '</b></span><br>';

    JobVatRates.forEach(function (vr) {
        strHtml = strHtml + '<span>VAT @ ' + (vr.rate) + '%: </span><span><b>£' + (vr.value * (vr.rate / 100)) + '</b></span><br>';
        InvoiceTotal += vr.value * (vr.rate / 100);
    });

    strHtml = strHtml + '<span>Total inc. VAT: </span><span><b>£' + InvoiceTotal + '</b></span><br>';
    strHtml = strHtml + '</div>';

    Grid.innerHTML = strHtml;

    UpdateInvoiceHistory();
}

function UpdateInvoiceHistory() {
    var Grid = document.getElementById('pnlInvoiceHistory');

    JobID = document.getElementById('hfJobID').value;

    var History = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetInvoiceHistoryForJob',
        {
            JobID: JobID
        },
        false);

    if (History.length == 0) {
        Grid.innerHTML = '';
        return;
    }

    var strHtml = '<table class="searchGrid small width-full">';
    strHtml = strHtml + '<col width="15%"><col width="15%"><col width="20%"><col width="25%"><col width="25%">';
    strHtml = strHtml + '<tr>';
    strHtml = strHtml + '<th>Reference No</th>';
    strHtml = strHtml + '<th>Doc Type</th>';
    strHtml = strHtml + '<th>Created On</th>';
    strHtml = strHtml + '<th>Sent Via</th>';
    strHtml = strHtml + '<th>Sent On</th>';
    strHtml = strHtml + '<th></th>';
    strHtml = strHtml + '</tr>';

    History.forEach(function (h) {
        strHtml = strHtml + '<tr>';
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;">' + h.ReferenceNo + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top;">' + h.ItemTypeString + '</td>';
        var CreationDate = new Date(parseInt(h.CreationDate.substr(6)));
        var SentDate = new Date(parseInt(h.SentDate.substr(6)));
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;">' + (CreationDate.getYear() > 1 ? CreationDate.toDateString() : 'N/A') + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top;">' + h.DispatchMethodString + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;">' + (SentDate.getYear() > 1 ? SentDate.toDateString() : 'N/A') + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;"><a target="_blank" href="Images/ReportImage.aspx?Id=' + h.Id + '">View</a></td>';
        strHtml = strHtml + '</tr>';
    });

    strHtml = strHtml + '</table>';

    Grid.innerHTML = strHtml;
}

function UpdateJobHistory() {
    var Grid = document.getElementById('grdHistory');

    JobID = document.getElementById('hfJobID').value;

    var History = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetHistoryForJob',
        {
            JobID: JobID
        },
        false);

    var strHtml = '<table class="searchGrid small width-full">';
    strHtml = strHtml + '<col width="15%"><col width="15%"><col width="20%"><col width="25%"><col width="25%">';
    strHtml = strHtml + '<tr>';
    strHtml = strHtml + '<th>Reference No</th>';
    strHtml = strHtml + '<th>Doc Type</th>';
    strHtml = strHtml + '<th>Created On</th>';
    strHtml = strHtml + '<th>Sent Via</th>';
    strHtml = strHtml + '<th>Sent On</th>';
    strHtml = strHtml + '<th></th>';
    strHtml = strHtml + '</tr>';

    History.forEach(function (h) {
        strHtml = strHtml + '<tr>';
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;">' + h.ReferenceNo + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top;">' + h.ItemTypeString + '</td>';
        var CreationDate = new Date(parseInt(h.CreationDate.substr(6)));
        var SentDate = new Date(parseInt(h.SentDate.substr(6)));
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;">' + (CreationDate.getYear() > 1 ? CreationDate.toDateString() : 'N/A') + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top;">' + h.DispatchMethodString + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;">' + (SentDate.getYear() > 1 ? SentDate.toDateString() : 'N/A') + '</td>';
        strHtml = strHtml + '<td style="vertical-align: top; text-align: center;"><a target="_blank" href="Images/ReportImage.aspx?Id=' + h.Id + '">View</a></td>';
        strHtml = strHtml + '</tr>';
    });

    strHtml = strHtml + '</table>';

    Grid.innerHTML = strHtml;
}

// Variable to indicate whether we've initialized the QuoteItemsDialog
var QuoteItemsDialogInitialized = false;

//The following three variables allow us to track the items retrieved from the service, those updated and those deleted
// - the jqWidgets tools don't have a built-in mechanism for tracking updates and deletions 
var QuoteItems = [];            // The raw QuoteItems retrieved from the services
var QuoteItemsUpdated = [];     // Those QuoteItems updated or added
var QuoteItemIdsDeleted = [];   // The Ids of the QuoteItems deleted
var JobSiteBeingEdited = -1;



// Function called to open the QuoteItemsDialog
function QuoteItemsDialog_Open(JobSiteID) {
        function buildQuoteEditSourceObject() {
        return {
            type: "POST",
            datatype: "json",
            datafields: [
            { name: 'Id', type: 'int' },
            { name: 'Code' },
            { name: 'Name' },
            { name: 'Description' },
            { name: 'Value', type: 'float' },
            { name: 'VatRate', type: 'float' }
            ],
            // The DataAdapter and table need an Id property to uniquely identify a row.
            id: 'Id',
            url: Dialog_strAppRoot + 'Services/Web/WorkFlow.asmx/GetJobQuoteItemList',
            // We don't want to cache the data
            cache: false,
            root: 'data',
            // The following functions are called when items are added, updated and deleted in the table
            addRow: function (rowID, rowData, position, commit) {
                rowData.VatRate = Markers.company.VatRate;
                QuoteItemsUpdated.push({ 'Id': rowID, 'Name': rowData.Name, 'Description': rowData.Description, 'Value': rowData.Value, 'VatRate': rowData.VatRate });
                commit(true);
            },
            updateRow: buildQuoteEditUpdateRowAction(),
            deleteRow: buildQuoteEditDeleteRowAction()
        };
    };

    function buildQuoteEditDeleteRowAction() {
        return function (rowID, commit) {
            var success = true;
            var RecId = parseInt(rowID);

            // Add the id of the current row to the list of ids deleted
            try {
                var Rec = $.grep(QuoteItemIdsDeleted, function (e) { return e === RecId });
                if (Rec.length == 0) {
                    QuoteItemIdsDeleted.push(RecId);
                }
            } catch (e) {
                success = false;
            }
            commit(true);
        }
    };

    function buildQuoteEditUpdateRowAction() {
        return function (rowID, rowData, commit) {
            var success = true;
            try {
                var RecId = parseInt(rowID);

                // Look in the updated list first
                var Rec = $.grep(QuoteItemsUpdated, function (e) { return e.Id === RecId });
                if (Rec.length == 1) {
                    // Remove this one from the updated list - we're going to replace it below
                    QuoteItemsUpdated.pop(Rec[0]);
                }
                else {
                    Rec = $.grep(QuoteItems, function (e) { return e.Id === RecId });
                }

                // If we found exactly one item
                if (Rec.length == 1) {
                    // update its properties with the new values
                    Rec[0].Code = rowData.Code;
                    Rec[0].Name = rowData.Name;
                    Rec[0].Description = rowData.Description;
                    Rec[0].Value = rowData.Value;
                    Rec[0].VatRate = rowData.VatRate;

                    // Add to the updated list
                    QuoteItemsUpdated.push(Rec[0]);

                }
            } catch (e) {
                success = false;
            }
            commit(success);
        };
    }

    // returns the 'Description' column property metadata for the Quote Edit datatable
    function buildQuoteEditDescriptionColumn() {
        return {
            text: 'Description', dataField: 'Description', width: 300,
            columnType: 'custom',
            createEditor: function (row, cellvalue, editor, cellText, width, height) {
                // construct the editor. 
                editor.jqxComboBox({
                    source: jqxDescriptionDataAdapterForEditor('Item', function () {
                        editor.jqxComboBox('val', cellvalue);
                    }),
                    searchMode: 'containsignorecase',
                    displayMember: 'Item',
                    valueMember: 'Item',
                    width: width,
                    height: height,
                    dropDownWidth: 500,
                });
            },
            initEditor: function (row, cellvalue, editor, celltext, width, height) {
                // set the editor's current value. The callback is called each time the editor is displayed.
                editor.jqxComboBox({ width: width, height: height });
                editor.val(cellvalue);
            },
            getEditorValue: function (row, cellvalue, editor) {
                // return the editor's value.
                return editor.val();
            }

        };
    }

    // returns the 'Name' column property metadata for the Quote Edit datatable
    function buildQuoteEditNameColumn() {
        return {
            text: 'Name', editable: true, dataField: 'Name', width: 200,
            columnType: 'custom',
            createEditor: function (row, cellvalue, editor, cellText, width, height) {
                // construct the editor. 
                editor.jqxComboBox({
                    source: jqxTreeDataAdapterForEditor('Common', function () {
                        editor.jqxComboBox('val', cellvalue);
                    }),
                    searchMode: 'containsignorecase',
                    displayMember: 'Common',
                    valueMember: 'Common',
                    width: width,
                    height: height,
                    dropDownWidth: 400,
                });
            },
            initEditor: function (row, cellvalue, editor, celltext, width, height) {
                // set the editor's current value. The callback is called each time the editor is displayed.
                editor.jqxComboBox({ width: width, height: height });
                editor.val(cellvalue);
            },
            getEditorValue: function (row, cellvalue, editor) {
                // return the editor's value.
                return editor.val();
            }

        };
    }


    if (Markers.company == null) {
        Markers.company = Ajax_CallService(
            "Services/Web/Company.asmx",
            "GetCompany",
            {},
            false);
    }

    //Get the quote Id to send through for get method
    QuoteID = document.getElementById('hfQuoteId').value;
    JobSiteBeingEdited = JobSiteID;

    // The data adapter used to populate the table of items in the dialog
    var dataAdapter;

    // Initialise the dialog if it hasn't already been initialised
    if (QuoteItemsDialogInitialized == false) {


        // Set up the source to retrieve the QuoteItems from the service.
        // Note that we only need to declare the fields which we are going to use in the table
        var source = buildQuoteEditSourceObject();

        // Create a dataAdapter from the source
        var dataAdapter = new $.jqx.dataAdapter(source, {
            contentType: 'application/json; charset=utf-8',
            // Add the parameters to the call to the service
            formatData: function (data) {
                // Note that we need to return a JSON string here, not a JSON object
                return JSON.stringify({
                    JobSiteID: JobSiteBeingEdited,
                    QuoteID: QuoteID
                })
            },
            // We need to save the raw QuoteItem objects into our own array, as the jqWidgets don't manage this for us.
            downloadComplete: function (data, textStatus, jqXHR) {
                QuoteItems = [];
                data.d.forEach(function (item, index) {
                    if (item.JobSiteId == JobSiteBeingEdited) {
                        QuoteItems.push(item);
                    }
                });

                return QuoteItems;
            }
        });

        // Set up the jqWindow in which we will display the QuoteItems
        $("#winQuoteItems").jqxWindow({
            // Specify the theme - we have to do this for every jqWidget - there's no way to define a default theme
            theme: 'arbpro',
            height: 430,
            width: 750,
            resizable: false,
            isModal: true,
            // Specify the OK and Cancel buttons - their Ids are defined in the HTML template for the jqWindow
            okButton: $('#btnWinQuoteItemsOk'),
            cancelButton: $('#btnWinQuoteItemsCancel'),
            // Specify the function called to initialise the content of the jqWindow
            initContent: function () {
                $('#btnWinQuoteItemsOk').jqxButton({ theme: 'arbpro', width: '65px' });
                $('#btnWinQuoteItemsCancel').jqxButton({ theme: 'arbpro', width: '65px' });
                $('#btnWinQuoteItemsOk').focus();

                // Set up the DataTable
                $("#tblQuoteItems").jqxDataTable({
                    theme: 'arbpro',
                    width: '99%',
                    height: '350',
                    // The source is the dataAdapter created above
                    source: dataAdapter,
                    // Setting pageable to false makes it display scrollbars
                    pageable: false,
                    editable: true,
                    showToolbar: true,
                    altrows: true,
                    toolbarHeight: 35,
                    // Specify the columns
                    columns: [
                        { text: 'Ref', editable: true, dataField: 'Code', width: 50 },
                        buildQuoteEditNameColumn(),
                        buildQuoteEditDescriptionColumn(),
                        { text: 'Value', editable: true, dataField: 'Value', width: 90, cellsFormat: 'F2', cellsalign: 'right' },
                        { text: 'VAT Rate', editable: true, dataField: 'VatRate', width: 90, cellsFormat: 'F2', cellsalign: 'right' },
                    ],
                    // Set up the code to call to render the toolbar
                    renderToolbar: function (toolBar) {
                        // renderJqxDataTableToolbar is a utility function which can be called for any DataTable
                        renderJqxDataTableToolbar(toolBar, "#tblQuoteItems");
                    }
                });
            }
        });

        // Set up the close event for the dialog, to update the QuoteItems if the OK button was clicked
        $('#winQuoteItems').on('close', function (event) {
            if (event.args.dialogResult.OK) {
                QuoteItems_update();

                // Clear down the arrays
                QuoteItems = [];
                QuoteItemsUpdated = [];
                QuoteItemIdsDeleted = [];
            }
        });

        // Remember the fact that we've initialized the dialog
        QuoteItemsDialogInitialized = true;
    }
    else {
        // The dialog has been initialized already - we need to refresh the data adapter with the current JobSiteID by 
        // calling the udpdateBoundData method
        $("#tblQuoteItems").jqxDataTable('updateBoundData');

        // Open the dialog
        $('#winQuoteItems').jqxWindow('open');
    }
}

// Function called to update the QuoteItems with the amendments made in the QuoteItems dialog
function QuoteItems_update() {
    try
    {
        //Ensure the quote id is set correctly: New items must have an Id of -1
        QuoteItemsUpdated.forEach(function (Item) {
            var result = $.grep(QuoteItems, function (e) { return e.Id == Item.Id; });
            if (result.length == 0) {
                Item.Id = -1;
            }
        });
        //QuoteItemsSave(int jobId, int quoteId, int jobSiteId, List<Types.QuoteItem> items)
        Ajax_CallService(
                "Services/Web/WorkFlow.asmx",
                "QuoteItemsSave",
                {
                    'jobId': JobID, 'quoteId': QuoteID, 'jobSiteId': JobSiteBeingEdited, 'items': QuoteItemsUpdated
                },
                false,
                function (result) {
                    if (!result) {
                        alert('Error durring quote item update: Some or all items may not have been added correctly.');
                    }
                }
            );
    } catch (err) {

    }

    // deletes
    QuoteItemIdsDeleted.forEach(function (itemID) {
        Ajax_CallService(
                "Services/Web/WorkFlow.asmx",
                "DeleteQuoteItem",
                {
                    'itemID': itemID
                },
                false,
                function (result) { }
            );
    });

    // refresh the quote list
    RefreshQuoteList();
}
/*
    iStage:
    0: JobItem
    1: QuoteItem
    2: WorkItem
    3: InvoiceItem
*/
function AddItem(iStage, iSite) {
    var strStage = 'Job';

    switch (iStage) {
        case 0:
            strStage = 'Job';
            break;
        case 1:
            strStage = 'Quote';
            break;
        case 2:
            strStage = 'Work';
            break;
        case 3:
            strStage = 'Invoice';
            break;
    };

    var StageID = '-1';
    var InvoiceQuoteID = '-1';
    try {
        var ddlStage = document.getElementById('ddl' + strStage + 'ID');
        StageID = ddlStage.options[ddlStage.selectedIndex].value;
        var ddlInvoiceQuoteID = document.getElementById('ddlInvoiceQuoteID');
        InvoiceQuoteID = ddlInvoiceQuoteID.options[ddlInvoiceQuoteID.selectedIndex].value;
    } catch (ex) { }

    if (iStage == 2) {
        var ddlStage = document.getElementById('ddlQuoteID'); // document.getElementById('ddlWorkQuoteID');
        StageID = ddlStage.options[ddlStage.selectedIndex].value;
        InvoiceQuoteID = ddlStage.options[ddlStage.selectedIndex].value;
    }

    Dialog_OpenPage(
        'Add ' + strStage + ' Item',
        'Dialogs/JobItem.aspx?JobID=' + JobID.toString() + '&Stage=' + strStage + '&StageID=' + StageID + '&SiteID=' + iSite.toString() + '&InvoiceQuoteID=' + InvoiceQuoteID.toString(),
        'content',
        800,
        400,
        true,
        {
            'Ok': {
                text: 'OK',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                    AddItemToJob();
                    RefreshQuoteList();
                    UpdateJobWorkItemList();
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/JobItem.js',
        function () { JobItem_PageLoad(); });
}

function EditQuoteItem(iItemID) {
    Dialog_OpenPage(
        'Edit Quote Item',
        'Dialogs/JobItemEdit.aspx?Stage=Quote',
        'content',
        800,
        150,
        true,
        {
            'Ok': {
                text: 'OK',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                    SaveJobEditItem();
                    RefreshQuoteList();
                    UpdateJobWorkItemList();
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/JobItemEdit.js',
        function () { JobItemEdit_PageLoad(iItemID); });
}

function EditWorkItem(iItemID) {
    Dialog_OpenPage(
        'Edit Work Item',
        'Dialogs/JobItemEdit.aspx?Stage=Work',
        'content',
        800,
        150,
        true,
        {
            'Ok': {
                text: 'OK',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                    SaveJobEditItem();
                    RefreshQuoteList();
                    UpdateJobWorkItemList();
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/JobItemEdit.js',
        function () { JobItemEdit_PageLoad(iItemID, ["#txtNewItemTreeNo", "#txtNewItemTreeName"]); });
}

function EditInvoiceItem(iItemID) {
    Dialog_OpenPage(
        'Edit Invoice Item',
        'Dialogs/JobItemEdit.aspx?Stage=Invoice',
        'content',
        800,
        150,
        true,
        {
            'Ok': {
                text: 'OK',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                    SaveJobEditItem();
                    RefreshQuoteList();
                    UpdateJobWorkItemList();
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/JobItemEdit.js',
        function () { JobItemEdit_PageLoad(iItemID); });
}

function SendQuote() {
    var ddlQuoteID = document.getElementById('ddlQuoteID');
    var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;

    ShowProduceQuoteDialog(QuoteID);
}

function SendWorkPack() {
    var ddlQuoteID = document.getElementById('ddlQuoteID');
    var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;

    Dialog_OpenPage(
        'Work Packs',
        'Dialogs/PDFGeneration.aspx',
        'content',
        900,
        600,
        true,
         {
             'Close': {
                 text: 'Close',
                 class: 'ui-dialog-button',
                 click: function () {
                     $(this).dialog('close');
                 }
             }
         },
        'Content/js/WorkpackGeneration.js',
        function () {
            PDFGeneration_Load(QuoteID);
            $("#warningMessage").visible = false;
            if (!Markers.workBooked) {
                $("#warningMessage").visible = true;
                $("#warningMessage").html("<strong>Work packs for schedules can only be generated when work has been booked.</strong>");
                $(btnGenerateWorkPack).prop("disabled", true);
            }
        });
}


function ChangeQuoteStatus(iStatus) {
    try {
        var ddlQuoteID = document.getElementById('ddlQuoteID');
        var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;

        Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'SetQuoteStatus',
            {
                QuoteID: QuoteID,
                Status: iStatus
            },
            false);

        UpdateJobQuoteItemList();
        UpdateJobWorkItemList();
    }
    catch (err) {
    }

    //This is to prevent an invalid post back argument exception
    return false;
}



function Requote() {
    try {
        var ddlQuoteID = document.getElementById('ddlQuoteID');
        var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;

        var duplicate = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'DuplicateQuote',
            {
                QuoteID: QuoteID
            },
            false);

        if (duplicate) {
            window.location = '/Job.aspx?JobID=' + JobID.toString() + '&QuoteID=' + duplicate.Id.toString();
        }
    }
    catch (err) {

    }
}

function BookWork(referrerSiteID) {

    JobID = document.getElementById('hfJobID').value;
    QuoteID = document.getElementById('hfQuoteId').value;

    var target = '/BookWork.aspx?JobID=' + JobID.toString() + "&QuoteID=" + QuoteID.toString();
    if (referrerSiteID) {
        target = target + "&ReferrerSiteID=" + referrerSiteID.toString();
    }

    window.location = target;
}

function MarkWorkCompleted() {
    var ddlWQID = document.getElementById('ddlQuoteID'); // document.getElementById('ddlWorkQuoteID');
    var QuoteID = ddlWQID.options[ddlWQID.selectedIndex].value;

    var JobSites = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetJobSiteList',
        {
            JobID: JobID
        },
        false);

    JobSites.forEach(function (js) {

        var WorkItems = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetJobSiteWorkItemList',
            {
                JobSiteID: js.Id,
                QuoteID: QuoteID
            },
            false);

        WorkItems.forEach(function (wi) {
            var chk = document.getElementById('chkWorkItem' + wi.Id);

            Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'SetWorkCompleted',
            {
                WorkItemID: wi.Id,
                Completed: chk.checked
            },
            false);
        })
    });

    UpdateJobWorkItemList();
}

function RefreshQuoteList() {
    var ddl = document.getElementById('ddlQuoteID');
    var ddl2 = document.getElementById('ddlInvoiceQuoteID');
    var ddl3 = document.getElementById('ddlWorkQuoteID');
    var val = ddl.selectedIndex;
    var val2 = ddl2.selectedIndex;
    var val3 = ddl3.selectedIndex;

    if (val == ddl.options.length - 1)
        val = -1;
    if (val2 == ddl2.options.length - 1)
        val2 = -1;
    if (val3 == ddl3.options.length - 1)
        val3 = -1;

    var Quotes = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetQuotes',
        {
            JobID: JobID
        },
        false);

    var i = 0;
    ddl.options.length = i;
    ddl2.options.length = i;
    ddl3.options.length = i
    //DGSAP-541
    //Setup a flag to track if there is an open quote
    var hasOpenQuote = false;
    Quotes.forEach(function (q) {
        var opt = document.createElement("option");
        opt.text = q.Id;
        opt.value = q.Id;
        ddl.add(opt, i);
        i++;
        hasOpenQuote = (hasOpenQuote || q.Status < 2);
    });
    //DGSAP-541
    //Check if the "New" option should be displayed
    if (!hasOpenQuote) {
        var def = document.createElement("option");
        def.text = 'New';
        def.value = '-1';
        ddl.add(def, i);
        ddl.options.length = i + 1;
    }
    i = 0;

    Quotes.forEach(function (q) {
        if (q.Status == 3) {
            var opt2 = document.createElement("option");
            opt2.text = q.Id;
            opt2.value = q.Id;
            ddl2.add(opt2, i);

            var opt3 = document.createElement("option");
            opt3.text = q.Id;
            opt3.value = q.Id;
            ddl3.add(opt3, i);

            i++;
        }
    });

    if (val == -1)
        val = ddl.options.length - 2;

    if (val < 0)
        val = 0;

    if (val2 == -1)
        val2 = ddl2.options.length - 1;

    if (val2 < 0)
        val2 = 0;

    if (val3 == -1)
        val3 = ddl3.options.length - 1;

    if (val3 < 0)
        val3 = 0;

    if (!hasOpenQuote && ddl.options.length > 1) {
        ddl.selectedIndex = ddl.options.length - 2;
    } else {
        ddl.selectedIndex = ddl.options.length - 1;
    }
    ddl2.selectedIndex = val2;
    ddl3.selectedIndex = val3;
    
    var value = ddl.options[ddl.selectedIndex].value;

    UpdateJobQuoteItemList();
    UpdateJobWorkItemList();
    //RefreshInvoiceList();
}

function RefreshInvoiceList() {
    SwitchInvoiceTab(0);
}

function OpenTreeProtection() {
    var dropDownElement = document.getElementById('ddlQuoteID');
    var quoteID = dropDownElement.options[dropDownElement.selectedIndex].value;
    Dialog_OpenPage(
        'Tree Protection',
        'Dialogs/TreeProtection.aspx?QuoteID=' + quoteID.toString(),
        'content',
        750,
        600,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                    Tpo_Save();
                }
            },
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
       'Content/js/TreeProtection.js',
       function () {
           Tpo_Load();
       });
}

function OpenRiskAssessment() {
    var hfJob = document.getElementById('hfJobID');
    var hfQuote = document.getElementById('hfQuoteId');
    if (hfQuote.value > 0) {
        var strUrl = '/RiskAssessment.aspx?JobId=' + hfJob.value;
        if (hfQuote.value != "-1") {
            strUrl += '&QuoteID=' + hfQuote.value;
            if (ListJobSites != null) {
                strUrl += '&SiteId=' + ListJobSites[0].Id; //default to the first item
            }
        }
        window.location = strUrl;
    }
    else {
        var message = 'The quote must be created, and items added before editing the risk assessment.';
        Dialog_OpenPage(
       'Risk Assessment',
       'Dialogs/Alert.aspx',
       'content',
       500,
       150,
       true,
       {
           'Ok': {
               text: 'Ok',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/AlertBox.js',
       function () {
           AlertBox_Show(message);
       });
    }
}

function SetSelectedTab() {
    try {
        var selectedQuote = document.getElementById('hfQuoteId').value;
        var selectedInvoice = document.getElementById('hfInvoiceId').value;
        if (selectedQuote > -1) {
            SetSelectedItem('ddlQuoteID', 2, selectedQuote);
        } else if (selectedInvoice > -1 && selectedInvoice != '') {
            SetSelectedItem('ddlInvoiceID', 4, selectedInvoice);
        }
    }
    catch (err) { }
}

function SetSelectedItem(elementName, tabNumber, selectedValue) {
    //This method selects sets the tab to be displayed and the selected value in the dropdown for that tab.
    var ddlOptions = document.getElementById(elementName).options;
    Job_curTab = tabNumber;
    var count = ddlOptions.length;
    for (var i = 0; i < count; i++) {
        if (ddlOptions[i].value === selectedValue) {
            ddlOptions[i].selected = true;
            break;
        }
    }
    //Display the correct tab
    SwitchTab();
}

function EmailWorkPacks() {
    var ddl = document.getElementById('ddlQuoteID');
    var JobSites = Ajax_CallService(
        'Services/Web/Report.asmx',
        'OpenWorkPacksForQuote',
        {
            quoteId: ddl
        },
        false);
}

function GetNotesList() {
    var ddlQuoteID = document.getElementById('ddlQuoteID');
    var QuoteID = ddlQuoteID.options[(ddlQuoteID.selectedIndex >= 0 ? ddlQuoteID.selectedIndex : 0)].value;
    var notes = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetNotesForQuote',
        {
            quoteId: QuoteID
        },
            true,
            PopulateQuoteNotes
        );
}

function PopulateQuoteNotes(results) {
    var notesPanel = document.getElementById('pnlQuotesNotesContainer');
    PopulateNotes(notesPanel, results);
}

function PopulateNotes(notesPanel, results) {
    try {
        if (!results) {
            DisplayMessage(notesPanel.id, 'Error: The service return no notes data');
        } else if (results.length == 0) {
            var stringHtml = '<div class="width-full float-left white-light"><button type="button" onclick="ShowNotesDialog(-1);" class="button-thin float-right padded">Add Note</button></div>';
            stringHtml += '<div class="width-full float-left">';
            stringHtml += '<table class="searchGrid width-full">';
            stringHtml += '<tr><td>No notes to display</td></tr>';
            stringHtml += '</table>';
            stringHtml += '</div>';
            notesPanel.innerHTML = stringHtml;
        } else {
            var NotesList = results; //GetStaffList();
            var strHtml = '<div class="width-full float-left white-light"><button type="button" onclick="ShowNotesDialog(-1);" class="button-thin float-right padded">Add Note</button></div>';
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid width-full">';
            strHtml += '<tr class="white-light"><th>Entered By</th><th>Details</th><th>Created</th>'
            NotesList.forEach(function (c) {
                strHtml += '<tr>';
                //strHtml += '<tr class="selected" onclick="ResultClicked(' + c.Id + ');">';
                strHtml += '<td onclick="ShowNotesDialog(' + c.Id + ');" style="padding-left: 10px;">' + c.EnteredBy + '</td><td onclick="ShowNotesDialog(' + c.Id + ');" style="padding-left: 10px;">' + c.Text + '</td><td onclick="ShowNotesDialog(' + c.Id + ');" style="padding-left: 10px;">' + toDateStringFromJson(c.EnteredDate, '', 'dd/mm/yyyy') + '</td>'
                //strHtml += '<td onclick="event.preventDefault(); DeleteStaff(' + c.Id + ')" style="padding-left: 10px;"><a href="#">Delete</a></td>';
                strHtml += '</tr>';
                //alert(c.Name);
            });
            strHtml += '</table>';
            strHtml += '</div>'; ///class="width-full"><button type="button" onclick="ShowStaffDialog(-1);" class="button-basic float-right">Add</button></div>';
            notesPanel.innerHTML = strHtml;
        }
    }
    catch (err) {
        DisplayMessage(notesPanel.id, 'Error: Unable to display notes.');
    }
}

function ShowNotesDialog(notesId) {
    try {
        var ddlQuoteID = document.getElementById('ddlQuoteID');
        var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;
        Dialog_OpenPage('Note',
            'Dialogs/Notes.aspx?',
            'content',
            500,
            500,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        Note_Save();
                        SwitchQuoteTab();
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/Notes.js',
            function () {
                Notes_Load(notesId, QuoteID);
            });
    }
    catch (Err) {
        DisplayMessage('pnlCompanyInfoDetail_2', 'Error: Unable to display notes dialog');
    }
}

function ShowProduceQuoteDialog(quoteId) {

    Dialog_OpenPage(
        'Produce Quote',
        'Dialogs/PDFGeneration.aspx',
        'content',
        900,
        600,
        true,
         {
             'Close': {
                 text: 'Close',
                 class: 'ui-dialog-button',
                 click: function () {
                     $(this).dialog('close');
                 }
             }
         },
        'Content/js/QuoteGeneration.js',
        function () { PDFGeneration_Load(quoteId); });
}


/*
    THE FOLLOWING SECTION IS FOR INVOICE MANAGEMENT
    The invoice panel has been sub divided into 4 panels.
        -Summary - A summary of all invoices sent for the Quote
        -Create - The process of creating an Invoice by adding items to it and producing the document
        -History - Displays the current status of all invoices for the Quote and enables the user to send reminders and set a received
        -Notes - Any notes that the user may feel are relevent to the invoicing process
*/

//Function to display the required html div element and hide all others
//NOTE - this function relies on the naming convention used in the aspx file to be correct
function SwitchInvoiceTab(selectedTab) {
    for (var index = 0; index < 5; index++) {
        var tabElement = document.getElementById('tabInvoiceControls_' + index.toString());
        var pnlElement = document.getElementById('tabInvoiceDetails_' + index.toString());
        if (index == selectedTab) {
            tabElement.className = 'jobTab activeTab';
            pnlElement.className = '';
            pnlElement.innerHTML = GetWaitImageForPanel('InvoiceDetails_' + index.toString());
            LoadSelectedInvoiceTab(selectedTab);
        } else {
            tabElement.className = 'jobTab';
            pnlElement.className = 'hidden';
        }
    }
}

//Function to call the correct load function for the panel that is being displayed
function LoadSelectedInvoiceTab(selectedTab) {
    var ddlQuoteNum = document.getElementById('ddlQuoteID');
    if (ddlQuoteNum) {
        var quoteId = ddlQuoteNum.options[ddlQuoteNum.selectedIndex].value; //TODO: Get quote Id
        switch (selectedTab) {
            case 0:
                GetInvoiceSummaryForQuote(quoteId);
                break;
            case 1:
                GetInvoiceLatestForQuote(quoteId);
                break;
            case 2:
                GetSentInvoicesForQuote(quoteId);
                break;
            case 3:
                GetInvoiceNotesForQuote(quoteId);
                break;
            case 4:
                GetInvoiceHistoryForQuote(quoteId);
                break;
        }
    }
}

//Async service call to get an invoice summary object
function GetInvoiceSummaryForQuote(quoteId) {
    var panel = document.getElementById('tabInvoiceDetails_0');
    try {
        Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetInvoiceSummaryForQuote',
           {
               quoteId: quoteId
           },
           true,
           DisplayInvoiceSummary
       );
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//Async service call to get the latest pending invoice for a quote
//NOTE: A quote should only ever have one pending invoice
function GetInvoiceLatestForQuote(quoteId) {
    var panel = document.getElementById('tabInvoiceDetails_1');
    try {
        Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetInvoiceForQuote',
           {
               quoteId: quoteId
           },
           true,
           DisplayCreateInvoice
       );
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//Async service call to get all invoices that have been sent for an invoice
function GetSentInvoicesForQuote(quoteId) {
    var panel = document.getElementById('tabInvoiceDetails_2');
    try {
        Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetSentInvoicesForQuote',
           {
               quoteId: quoteId
           },
           true,
           DisplayInvoiceManagement
       );
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//Async service call to get all notes for all invoices for a quote
function GetInvoiceNotesForQuote(quoteId) {
    var panel = document.getElementById('tabInvoiceDetails_3');
    try {
        Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetInvoiceNotesForQuote',
           {
               quoteId: quoteId
           },
           true,
           DisplayInvoiceNotes
       );
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//Async service call to get all document history for all invoices for a quote
function GetInvoiceHistoryForQuote(quoteId) {
    var panel = document.getElementById('tabInvoiceDetails_4');
    try {
        Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'GetInvoiceHistoryForQuote',
           {
               quoteId: quoteId
           },
           true,
           DisplayQuoteInvoiceHistory
       );
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: unable to communicate with Arb Pro service. Please refresh the page.');
    }
}

//Aysnc callback function to display the summary of invoices for a quote
function DisplayInvoiceSummary(result) {
    var panel = document.getElementById('tabInvoiceDetails_0');
    try {
        if (!result) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
        } else {
            var strHtml = '<table class="InvoiceSummary"><tr><th></th><th>Net Value</th><th>Vat Value</th><th>Gross Value</th></tr>';
            strHtml += '<tr><td>Quote Value</td><td>';
            strHtml += result.QuoteValue.toString();
            strHtml += '</td><td>'
            strHtml += result.QuoteVatValue.toString();
            strHtml += '</td><td>'
            strHtml += (result.QuoteValue + result.QuoteVatValue).toString();
            strHtml += '</td></tr>';
            strHtml += '<tr><td>Completed Work Value</td><td>';
            strHtml += result.CompletedWorkValue.toString();
            strHtml += '</td><td>'
            strHtml += result.CompletedWorkVatValue.toString();
            strHtml += '</td><td>'
            strHtml += (result.CompletedWorkValue + result.CompletedWorkVatValue).toString();
            strHtml += '</td></tr>';
            strHtml += '<tr><td>Pending Invoice Value</td><td>';
            strHtml += result.PendingValue.toString();
            strHtml += '</td><td>'
            strHtml += result.PendingVat.toString();
            strHtml += '</td><td>'
            strHtml += (result.PendingValue + result.PendingVat).toString();
            strHtml += '</td></tr>';
            strHtml += '<tr><td>Invoiced Value</td><td>';
            strHtml += result.InvoicedValue.toString();
            strHtml += '</td><td>'
            strHtml += result.InvoicedVatValue.toString();
            strHtml += '</td><td>'
            strHtml += (result.InvoicedValue + result.InvoicedVatValue).toString();
            strHtml += '</td></tr>';
            strHtml += '<tr><td>Receipts Value</td><td>';
            strHtml += result.InvoiceReceipts.toString();
            strHtml += '</td><td>'
            strHtml += result.InvoiceReceiptVat.toString();
            strHtml += '</td><td>'
            strHtml += (result.InvoiceReceipts + result.InvoiceReceiptVat).toString();
            strHtml += '</td></tr>';
            strHtml += '<tr><td>Overdue Value</td><td>';
            strHtml += result.OverdueValue.toString();
            strHtml += '</td><td>'
            strHtml += result.OverdueVat.toString();
            strHtml += '</td><td>'
            strHtml += (result.OverdueValue + result.OverdueVat).toString();
            strHtml += '</td></tr></table>';

            panel.innerHTML = strHtml;
        }
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display invoice summary');
    }
}

//Async callback to display an invoice that has not been displatched
function DisplayCreateInvoice(result) {

    var panel = document.getElementById('tabInvoiceDetails_1');
    try {
        if (!result) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
        } else {
            var strHtml = InvoiceRecipientDetails(result);
            strHtml += '<div class="white-light float-left width-full"><label class="clientLabel padded float-left">Invoice Id: ';
            if (result.Id < 1) {
                strHtml += 'New';
            } else {
                strHtml += result.Id.toString();
            }
            strHtml += '</label><button type="button" onclick="AddInvoiceItem(' + result.Id + ');" class="button-thin float-right padded">Add Item</button></div>';
            strHtml += '<div class="InvoicePanel float-left">';
            if (!result.InvoicedItems || result.InvoicedItems.length == 0) {
                strHtml += '<div class="width-full white"></div>';
            } else {
                strHtml += DisplayInvoiceItems(result.Id, result.InvoicedItems);
            }
            strHtml += '</div>';
            strHtml += '<div class="width-full">';
            if (!result.InvoicedItems || result.InvoicedItems.length == 0) {
                strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + result.Id + ',\'Invoice\');" class="button-basic float-right padded" disabled="disabled">Produce Invoice</button></div>';
            } else {
                strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + result.Id + ',\'Invoice\');" class="button-basic float-right padded">Produce Invoice</button></div>';
            }
        }
        panel.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display invoice creation panel');
    }
}

//Async callback to display invice details for all invoices dispatch for a quote
function DisplayInvoiceManagement(results) {

    var panel = document.getElementById('tabInvoiceDetails_2');
    try {
        if (!results) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return valid data');
        } else if (results.length == 0) {
            DisplayMessage(panel.id, 'No invoices have been sent for the selected Quote.');
        } else {
            var total = 0.0;
            var strHtml = '<table class="width-full searchGrid"><tr><th>Invoice N0.</th><th>Document Ref.</th><th>Net Value (£)</th><th>Date Due</th><th>Status</th><th>Set Paid</th><th>Change Status</th><tr>';
            results.forEach(function (inv) {
                var strDueDate = toDateStringFromJson(inv.DueDate, '', 'dd/mm/yyyy');
                strHtml += '<tr><td>';
                strHtml += inv.Id.toString();
                strHtml += '</td><td>';
                strHtml += inv.Refernce;
                strHtml += '</td><td style="text-align: right">';
                strHtml += inv.GrossValue.toString();
                strHtml += '</td><td style="text-align: center">';
                strHtml += strDueDate;
                strHtml += '</td><td style="text-align: center">';
                strHtml += inv.StatusString;
                strHtml += '</td><td style="text-align: center">'
                if (inv.Status == 2) {
                    //The invoice has been paid
                    strHtml += 'Paid</td><td style="text-align: center"><button type="button" onclick="CreditInvoice(' + inv.Id + ');" class="button-thin padded" disabled="disabled">Credit</button></td>'
                } else {
                    //The invoice has not been paid
                    strHtml += '<button type="button" onclick="SetInvoiceStatus(' + inv.Id + ',2);" class="button-thin padded">Paid</button>';
                    strHtml += '</td><td style="text-align: center">'
                    if (inv.Status == 99) {
                        //The invoice has been cancelled
                        strHtml += 'Cancelled';
                    } else if (inv.Status == 5) {
                        //The invoice has been credited
                        strHtml += 'credited';
                    } else if (inv.Status > 2 || ParseLocalDate(strDueDate, 'en_GB') < new Date()) {
                        //The invoice is overdue
                        /*
                            Overdue = 3,
                            FurtherAction = 4,
                        */
                        switch (inv.Status) {
                            case 1:
                            case 2:
                                inv.Status = 3;
                                strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + inv.Id + ',\'First Reminder Letter\');" class="button-thin padded">Send Reminder</button>';
                                break;
                            case 3:
                                strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + inv.Id + ',\'Reminder  Letter\');" class="button-thin padded">Send Reminder</button>';
                                break;
                            case 4:
                                strHtml += '<button type="button" onclick="ShowSendInvoiceDialog(' + inv.Id + ',\'Legal Action Letter\');" class="button-thin padded">Send Legal</button>';
                                break;

                        }
                    }
                    strHtml += '</td>';
                }
                strHtml += '</tr>';
            });
            strHtml += '</table>';
            panel.innerHTML = strHtml;
        }
    }
    catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display invoice management panel - ' + err.toString());
    }
}

//Async callback to display all invoice notes for a quote
function DisplayInvoiceNotes(results) {
    var notesPanel = document.getElementById('tabInvoiceDetails_3');
    try {
        var strHtml = '<div class="width-full float-left white-light"><button type="button" onclick="ShowInvoiceNotesDialog(-1);" class="button-thin float-right padded">Add Note</button></div>';
        if (!results) {
            DisplayMessage(notesPanel.id, 'Error: The service return no notes data');
        } else if (results.length == 0) {
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid width-full">';
            strHtml += '<tr><td>No notes to display</td></tr>';
        } else {
            var NotesList = results; //GetStaffList();
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid width-full">';
            strHtml += '<tr class="white-light"><th>Entered By</th><th>Details</th><th>Created</th>'
            NotesList.forEach(function (c) {
                strHtml += '<tr onclick="ShowInvoiceNotesDialog(' + c.Id + ');">';
                strHtml += '<td style="padding-left: 10px;">' + c.EnteredBy + '</td><td style="padding-left: 10px;">' + c.Text + '</td><td  style="padding-left: 10px;">' + toDateStringFromJson(c.EnteredDate, '', 'dd/mm/yyyy') + '</td>'
                strHtml += '</tr>';
            });
        }
        strHtml += '</table>';
        strHtml += '</div>';
        notesPanel.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage(notesPanel.id, 'Error: Unable to display invoice notes.');
    }
}

function ShowInvoiceNotesDialog(noteId) {
    try {
        var ddlQuoteID = document.getElementById('ddlQuoteID');
        var QuoteID = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;
        Dialog_OpenPage('Note',
            'Dialogs/Notes.aspx?',
            'content',
            500,
            500,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        Note_Save();
                        GetInvoiceNotesForQuote(QuoteID);
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/Notes.js',
            function () {
                Notes_Load(noteId, QuoteID, 3);
            });
    }
    catch (Err) {
        DisplayMessage('pnlCompanyInfoDetail_4', 'Error: Unable to display notes dialog');
    }
}

//Async function to display history items for a quote
function DisplayQuoteInvoiceHistory(results) {
    var historyPanel = document.getElementById('tabInvoiceDetails_4');
    try {
        var strHtml = '';
        if (!results) {
            DisplayMessage(historyPanel.id, 'Error: The service return no history data');
        } else if (results.length == 0) {
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid width-full">';
            strHtml += '<tr><td>No history to display</td></tr>';
        } else {
            strHtml += '<div class="width-full float-left">';
            strHtml += '<table class="searchGrid small width-full">';
            strHtml += '<col width="15%"><col width="15%"><col width="20%"><col width="25%"><col width="25%">';
            strHtml += '<tr>';
            strHtml += '<th>Reference No</th>';
            strHtml += '<th>Doc Type</th>';
            strHtml += '<th>Created On</th>';
            strHtml += '<th>Sent Via</th>';
            strHtml += '<th>Sent On</th>';
            strHtml += '<th></th>';
            strHtml += '</tr>';

            results.forEach(function (h) {
                strHtml += '<tr>';
                strHtml += '<td style="vertical-align: top; text-align: center;">' + h.ReferenceNo + '</td>';
                strHtml += '<td style="vertical-align: top;">' + h.DocTypeString + '</td>';
                var CreationDate = new Date(parseInt(h.CreationDate.substr(6)));
                var SentDate = new Date(parseInt(h.SentDate.substr(6)));
                strHtml += '<td style="vertical-align: top; text-align: center;">' + (CreationDate.getYear() > 1 ? CreationDate.toDateString() : 'N/A') + '</td>';
                strHtml += '<td style="vertical-align: top;">' + h.DispatchMethodString + '</td>';
                strHtml += '<td style="vertical-align: top; text-align: center;">' + (SentDate.getYear() > 1 ? SentDate.toDateString() : 'N/A') + '</td>';
                strHtml += '<td style="vertical-align: top; text-align: center;"><a target="_blank" href="Images/ReportImage.aspx?Id=' + h.Id + '">View</a></td>';
                strHtml += '</tr>';
            });

            //strHtml = strHtml + '</table>';
            //var docList = results; //GetStaffList();
            //strHtml += '<div class="width-full float-left">';
            //strHtml += '<table class="searchGrid width-full">';
            //strHtml += '<tr class="white-light"><th>Entered By</th><th>Details</th><th>Created</th>'
            //docList.forEach(function (c) {
            //    strHtml += '<tr onclick="ShowDocumentHistoryDialog(' + c.Id + ');">';
            //    strHtml += '<td style="padding-left: 10px;">' + c.EnteredBy + '</td><td style="padding-left: 10px;">' + c.Text + '</td><td  style="padding-left: 10px;">' + toDateStringFromJson(c.EnteredDate, '', 'dd/mm/yyyy') + '</td>'
            //    strHtml += '</tr>';
            //});
        }
        strHtml += '</table>';
        strHtml += '</div>';
        historyPanel.innerHTML = strHtml;
    }
    catch (err) {
        DisplayMessage(historyPanel.id, 'Error: Unable to display invoice history - ' + err.toString());
    }
}

//Function to display a dialog to add an invoice item to an invoice
function AddInvoiceItem(invoiceId) {

    var ddl = document.getElementById('ddlQuoteID');
    var quoteId = ddl.options[ddl.selectedIndex].value;
    try {
        Dialog_OpenPage(
            'Add Items to Invoice',
            'Dialogs/InvoiceItem.aspx',
            'content',
            800,
            700,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        if (InvoiceItems_Save()) {
                            $(this).dialog('close');
                            SwitchInvoiceTab(1);
                        }
                    }
                },
                'Cancel': {
                    text: 'Cancel',
                    class: 'ui-dialog-button',
                    click: function () {
                        //Close the dialog and reload the tab to display the added items
                        $(this).dialog('close');
                        SwitchInvoiceTab(1);
                    }
                }
            },
       'Content/js/InvoiceItems.js',
       function () { InvoiceItems_PageLoad(quoteId, invoiceId, ListJobSites); });
    }
    catch (err) {
        alert('Error adding invoice item: ' + err.toString());
    }
}

//Function to display a dialog to edit an invoice item
function EditInvoiceItem(itemId) {
    try {
        Dialog_OpenPage(
            'Edit Invoice Item',
            'Dialogs/EditItem.aspx',
            'content',
            800,
            150,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        if (EditItem_Save()) {
                            $(this).dialog('close');
                            SwitchInvoiceTab(1);
                        }
                    }
                },
                'Cancel': {
                    text: 'Cancel',
                    class: 'ui-dialog-button',
                    click: function () {
                        //Close the dialog and reload the tab to display the added items
                        $(this).dialog('close');
                        SwitchInvoiceTab(1);
                    }
                }
            },
       'Content/js/EditItem.js',
       function () { EditItem_Load(2, itemId); });
    }
    catch (err) {
        alert('Error editing invoice item: ' + err.toString());
    }
}

function DisplayInvoiceItems(parentId, itemList) {

    var strHtml = '';
    var JobVatRates = new Array();
    ListJobSites.forEach(function (js) {

        strHtml += '<table class="searchGrid small width-full"><col width="5%"><col width="20%"><col width="55%"><col width="10%"><col width="10%">';
        //Display the site address for the items
        strHtml += '<tr>';
        strHtml += '<th colspan="5">' + js.SiteString + '</th></tr>';

        var SiteVatRates = new Array();

        //Iterate through each item to see if it is for the current site
        itemList.forEach(function (qi) {
            if (qi.JobSiteId == js.Id) {
                var found = false;
                //Evaluate the VAT rates for the Site and add if required 
                SiteVatRates.forEach(function (vr) {
                    if (vr.rate == qi.VatRate) {
                        vr.value += qi.Value;
                        found = true;
                    }
                });

                if (!found)
                    SiteVatRates.push({ rate: qi.VatRate, value: qi.Value });

                found = false;
                //Evaluate the VAT rates for the Job and add if required
                JobVatRates.forEach(function (vr) {
                    if (vr.rate == qi.VatRate) {
                        vr.value += qi.Value;
                        found = true;
                    }
                });

                if (!found)
                    JobVatRates.push({ rate: qi.VatRate, value: qi.Value });

                //Display the item.
                strHtml += '<tr>';
                strHtml += '<td style="vertical-align: top; text-align: center;" onclick="EditInvoiceItem(' + qi.Id + ')">' + qi.Code + '</td>';
                strHtml += '<td style="vertical-align: top;" onclick="EditInvoiceItem(' + qi.Id + ')">' + qi.Name + '</td>';
                strHtml += '<td onclick="EditInvoiceItem(' + qi.Id + ')">' + qi.Description + '</td>';
                strHtml += '<td style="text-align: center" onclick="event.preventDefault(); OpenImage(1, ' + qi.Id + ');"><a href="#">Image</a></td>';
                strHtml += '<td style="text-align: right; padding-right: 3px; padding-left: 3px; vertical-align: bottom;"(' + qi.Id + ')">' + (qi.Value > 0 ? '£' + qi.Value : '') + '</td>';
                strHtml += '</tr>';
            }
        });
        strHtml += '</table>';
        //Check if site subtotals need to be displayed
        if (ListJobSites.length > 1) {
            strHtml += GetSiteTotalHtml(SiteVatRates, ListJobSites.length, 'Site');
            strHtml += '<hr/>';
        }
    });
    //Display the invoice totals
    strHtml += GetSiteTotalHtml(JobVatRates, ListJobSites.length, 'Invoice');
    return strHtml;
}

//Function to set an invoice as paid
function SetInvoicePaid(invoiceId) {
    var updated = false;
    try {
        updated = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'SetInvoiceStatus',
            {
                invoiceId: invoiceId,
                status: 2
            },
            false
        );
    }
    catch (err) {
        updated = false;
    }
    if (updated) {
        alert('Invoice set as paid');
    } else {
        alert('Failed to update invoice status');
    }
    //Update the panel
    SwitchInvoiceTab(3);
}

//Function to display the create / send invoice dialog
function ShowSendInvoiceDialog(invoiceId, text) {

    Dialog_OpenPage(
        'Send ' + text,
        'Dialogs/PDFGeneration.aspx',
        'content',
        900,
        600,
        true,
         {
             'Close': {
                 text: 'Close',
                 class: 'ui-dialog-button',
                 click: function () {
                     $(this).dialog('close');
                 }
             }
         },
        'Content/js/InvoiceGeneration.js',
        function () { PDFGeneration_Load(invoiceId); });
}

//Function 
function InvoiceRecipientDetails(invoice) {
    var strHtml = '<div class="float-left width-full">';
    //Construct the table to hold the contact details
    strHtml += '<table class="searchGrid width-full"><tr><th colspan="2">Contact Details</th><th>';
    strHtml += '<input type="button" class="button-thin float-right padded" value="Edit" onclick="EditInvoiceContact();" />';
    strHtml += '</th></tr>';
    //Check if the invoice exists and has a contact set
    if (invoice.Id < 1 || invoice.PointOfContact.Id < 1) {
        strHtml += '<tr><td colspan="3" class="center">Please select a contact for this invoice</td></tr>';
    } else {
        //Display the contact details
        strHtml += '<tr><td>Contact</td>';
        strHtml += '<td colspan="2">';
        strHtml += invoice.PointOfContact.Title;
        strHtml += '</td></tr>';
        //Display the invoice address
        strHtml += '<tr><th colspan="3">Invoice Address</th></tr>';
        strHtml += '<tr><td>House No/Name</td>';
        strHtml += '<td colspan="2">'
        strHtml += invoice.HouseNameNo;
        strHtml += '</td></tr>';
        strHtml += '<tr><td>Address 1</td>';
        strHtml += '<td colspan="2">'
        strHtml += invoice.Address1;
        strHtml += '</td></tr>';
        strHtml += '<tr><td>Address 2</td>';
        strHtml += '<td colspan="2">'
        strHtml += invoice.Address2;
        strHtml += '</td></tr>';
        strHtml += '<tr><td>Town</td>';
        strHtml += '<td colspan="2">'
        strHtml += invoice.Town;
        strHtml += '</td></tr>';
        strHtml += '<tr><td>County</td>';
        strHtml += '<td colspan="2">'
        strHtml += invoice.County;
        strHtml += '</td></tr>';
        strHtml += '<tr><td>Post Code</td>';
        strHtml += '<td colspan="2">'
        strHtml += invoice.PostCode;
        strHtml += '</td></tr>';
    }
    strHtml += '</table></div>';
    return strHtml;
}

function EditInvoiceContact() {

    var ddlQuoteID = document.getElementById('ddlQuoteID');
    var quoteId = ddlQuoteID.options[ddlQuoteID.selectedIndex].value;
    var clientId = document.getElementById('hfClientID').value;
    try {
        Dialog_OpenPage(
            'Edit Invoice Contact and Address',
            'Dialogs/InvoiceRecipient.aspx',
            'content',
            700,
            420,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        if (InvoiceRecipient_Save()) {
                            $(this).dialog('close');
                            SwitchInvoiceTab(1);
                        }
                    }
                },
                'Cancel': {
                    text: 'Cancel',
                    class: 'ui-dialog-button',
                    click: function () {
                        //Close the dialog and reload the tab to display the added items
                        $(this).dialog('close');
                        SwitchInvoiceTab(1);
                    }
                }
            },
       'Content/js/InvoiceRecipient.js',
       function () { InvoiceRecipient_Load(clientId, quoteId); });
    }
    catch (err) {
        alert('Error display Invoice contacts: ' + err.toString());
    }
}

//Function to change the status of an invoice and display the invoice management panel
function SetInvoiceStatus(invId, status) {
    try {
        var updated = Ajax_CallService(
           'Services/Web/WorkFlow.asmx',
           'SetInvoiceStatus',
           {
               invoiceId: invId,
               status: status
           },
           false,
           null
       );
        if (updated) {
            SwitchInvoiceTab(2);
        }
        return updated;
    } catch (err) {
        return false;
    }
}

function GenerateManualQuoteDocs(calendarId) {
    Dialog_OpenPage(
       'Manual Quote Pack',
       'Dialogs/PDFPreview.aspx',
       'content',
       500,
       735,
       true,
       {
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/ManualQuoteDocuments.js',
       function () { ManualQuoteDocuments_Load(calendarId); });
}

//Function to display a map dialog with the position of all items
function ShowItemMap(jobSiteId) {
    //TODO: Validate the number of items for the quote - jobsite

    var ddlQuoteID = document.getElementById('ddlQuoteID');
    var qId = ddlQuoteID.options[(ddlQuoteID.selectedIndex >= 0 ? ddlQuoteID.selectedIndex : 0)].value;
    Dialog_OpenPage(
       'Site Map',
       'Dialogs/ItemMap.aspx',
       'content',
       600,
       700,
       true,
       {
           'Print': {
               text: 'Print',
               class: 'ui-dialog-button',
               click: function () {
                   PrintQuoteItemMap();
               }
           },
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/QuoteItemMap.js',
       function () { QuoteItemMap_Load(qId, jobSiteId); });
}


//Function to display an existing invoice document
function ShowDocumentHistoryDialog(historyId) {

}