﻿//  The following function control the work flow process.
//  This file utilises functions in the following files
//      common.js
//      Quote.js
//      Work.js
//      Invoice.js

//Glaobal scope variables
var ObjJob = null;              //This is the select job
var Job_ListShown = false;      //Flag to show if the side panel detail has been displayed
var ClientId = -1;              //The Id of the client that the Job is for
var Job_curTab = 1;             //The Index of the selected stage
var Markers = { workBooked: false, company: null }
var ListSurveyors = [];
//Url paramater stores
var UrlQuoteId = -1;
var UrlWorkId = -1;
var UrlInvoiceId = -1;
var ObjCompany = null;

/*

        **********  Intialisation and service call functions    **********

*/

//      Initialisation function to start the loading process
function WorkManagement_Load() {
    SwitchJobStageTab(0);
    var panel = document.getElementById('pnlEnquiryDetailsWait');
    panel.innerHTML = GetWaitImageForPanel(panel.id, 'Loading Enquiry details please wait.');

    Job_curTab = 2;   // default to Quote tab
    var Quote_curTab = 1; // default to Details within the Quote tab

    var jobId = -1;

    ObjCompany = GetCompanyDetails();

    //Get the job id 
    var param = GetURLParameter('JobID');
    if (param != 'null') {
        jobId = parseInt(param);
    }

    //Check if a quote record has been provided.
    var param2 = GetURLParameter('QuoteID');
    if (param2 != 'null') {
        UrlQuoteId = parseInt(param2);
    }

    //Check if a work record has been provided.
    var param3 = GetURLParameter('WorkID');
    if (param3 != 'null') {
        UrlWorkId = parseInt(param3);
    }

    //Check if a invoice record has been provided.
    var param4 = GetURLParameter('InvoiceID');
    if (param4 != 'null') {
        UrlInvoiceId = parseInt(param4);
    }

    //Get the tab to be displayed
    var param5 = GetURLParameter('curTab');
    if (param5 != 'null') {
        Job_curTab = parseInt(param5);
    }

    //Get the quote tab to be displayed
    var param6 = GetURLParameter('subTab');
    if (param6 != 'null') {
        Quote_curTab = parseInt(param6);
    }

    JobDetails_Load(jobId);
    //TODO: This needs to be rationalised into the job object
    LoadAppointments(jobId);

    // uncomment this to make all of the main JOB items on the dashboard draggable
    // $('.ap-draggable').draggable();
}

//      Function to load the side panel details
function SidePanel_Load() {
    if (ObjJob) {
        Ajax_CallService(
            'Services/Web/ClientData.asmx',
            'GetClientJobList',
            {
                ClientID: ObjJob.ClientId
            },
            true,
            SidePanel_Display
        );
    }
}

//      Initialisation function to load and populate page
function JobDetails_Load(id) {
    //  Load the job details passing in the main display setup
    JobLoad_ServiceCall(id, JobDetails_Display);
    //  Load the surveyor list
    Surveyors_Load();
}

//      Refresh function to load and update page elements
function Job_Refresh(id) {
    JobLoad_ServiceCall(id, JobRefresh_CallBack);
}

//      Function to load the selected Job details and then call the specified function
function JobLoad_ServiceCall(id, callBack){
    Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetJob',
        {
            jobId: id
        },
        true,
        callBack
    );
}

function LoadAppointments(id) {
    //debugger;
    Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetUpcomingQuoteAppointmentsForJob',
        {
            JobID: id
        },
        true,
        Appointments_Display);
}

function Surveyors_Load() {
    Ajax_CallService(
        'Services/Web/Company.asmx',
        'ListSurveyors',
        {},
        true,
        function (results) {
            ListSurveyors = results;
        });
}

function JobHistory_Load() {
    var panel = document.getElementById('pnlWorkStage_4');
    panel.innerHTML = GetWaitImageForPanel(panel.id, 'Loading Document History.</br>Please Wait');
    Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetQuoteHistoryForJob',
        {
            JobID: ObjJob.Id
        },
        true,
        JobHistory_LoadCallback);
}
/*

        **********  Service call back functions    **********

*/

//      Async callback function to display all job details
function JobDetails_Display(job) {
    try {
        if (!job) {
            //  Dsiplay an error as the job that has been returned is not valid
            DisplayMessageDialog('Error', 'The Arb Pro service did not return a valid Job. Please reload the page');
        } else {
            //  Set the scope variable
            ObjJob = job;
            //  Load the side bar details
            SidePanel_Load();
            //  Update the side bar header
            document.getElementById('lnkReturnToClient').href = "/Client.aspx?ClientID=" + ObjJob.ClientId.toString();
            document.getElementById('lblClientName').innerHTML = ObjJob.ClientName;
            //  Update the intial details
            EnquiryDetailsUpdate();
            //  Hide the wait timer
            document.getElementById('pnlEnquiryDetailsWait').className = 'hidden';
            //  Show the content
            document.getElementById('pnlEnquiryDetails').className = 'width-full float-left';
            //  Dispose of the wait image
            document.getElementById('pnlEnquiryDetailsWait').innerHTML = '';

            //Call the refresh process to prevent code duplication
            JobRefresh_CallBack(ObjJob, UrlQuoteId);
        }
    } catch (err) {
        DisplayMessageDialog('Error', 'Unable to load the Job details. Please reload the page. If the error continues please contact support - ' + err.toString());
    }
}

//      Async callback function to ulatenotesulate the following items : Job Sites, Quote No dropdown and Quote detials
function JobRefresh_CallBack(job,selectedQuoteId) {
    try {
        if (!job) {
            //  Dsiplay an error as the job that has been returned is not valid
            DisplayMessageDialog('Error', 'The Arb Pro service did not return a valid Job. Please reload the page');
        } else {
            //  Set the scope variable
            ObjJob = job;
            //  Display the Job Sites
            JobSites_Display(ObjJob.WorkSites);
            // Display client details
            ClientDetailsDisplay(ObjJob.PointOfContact);
            //  Populate the Quote No dropdown
            RefreshQuoteList(selectedQuoteId);
            //  Display the correct tab
            SwitchTab();
        }
    } catch (err) {
        DisplayMessageDialog('Error', 'Unable to load the Job details. Please reload the page. If the error continues please contact support - ' + err.toString());
    }
}

//      Async callback function to display the side bar details
function SidePanel_Display(jobs) {
    var Grid = document.getElementById('pnlJobsListList');
    //var Client = GetUser(Page_strUserEmail);
    var ClientID = document.getElementById('hfClientID').value;
    var JobID = document.getElementById('hfJobID').value;
    try {
        var listHtml = '<table id="jobListTable" class="searchGrid width-full">';

        //define local sort function
        function SortByID(a, b) {
            return ((a.Id > b.Id) ? -1 : ((a.Id < b.Id) ? 1 : 0));
        }
        
        //Sort the search results
        jobs.sort(SortByID);
        //Construct the html
        jobs.forEach(function(j) {
            listHtml += '<tr onclick="window.location = \'/Job.aspx?JobID=' + j.Id + '\';"><td class="' + (j.Id == JobID ? 'selected' : '') + '">' + j.SideBarDetail + '</td></tr>';
        });
        listHtml = listHtml + '</table>';
        //display the grid
        Grid.innerHTML = listHtml;
    } catch (err) {
        DisplayMessage(Grid.id, 'ERROR: Unable to display Job list - ' + err.toString());
    }
}

//      Async callback to populate the Appoinments for a job
function Appointments_Display(results) {
    var panel = document.getElementById('pnlJobEnquiryAppointments');
    var strHtml = '';
    var Appts = results;
    Appts.forEach(function (appt) {
        var Staff = Ajax_CallService(
            'Services/Web/Company.asmx',
            'GetStaffForId',
            {
                Id: appt.SurveyorID
            },
            false);

        var JobSite = Ajax_CallService(
            'Services/Web/WorkFlow.asmx',
            'GetJobSite',
            {
                JobSiteID: appt.JobSiteID
            },
            false);

        var ApptDate = new Date(parseInt(appt.StartDateTime.substr(6)));
        strHtml += '<div class="apptBox padded float-left">';
        strHtml += '<div';
        strHtml += ' onclick="EditAppointment(' + appt.Id + ');">';
        strHtml += '<span class="apptTitle float-left">' + ApptDate.toLocaleDateString() + '</span>';
        strHtml += '<span class="apptTitle float-right">' + zeroPad(ApptDate.getHours(), 2) + ':' + zeroPad(ApptDate.getMinutes(), 2) + '</span>';
        strHtml += '<br>';
        strHtml += '<span class="apptTitle center">Click to edit </span>';

        strHtml += '<div class="width-full float-left bordered-bottom">';
        strHtml += '</div>';

        strHtml += '<div class="width-full float-left" style="overflow-y: auto;">';

        strHtml += '<div class="apptField float-left">';
        strHtml += '<span class="apptLabel width-full float-left">Site:</span>';
        strHtml += '<span class="apptInput width-full float-left">' + JobSite.SiteString + '</span>';
        strHtml += '</div>';

        strHtml += '<div class="apptField float-left">';
        strHtml += '<span class="apptLabel width-full float-left">Surveyor:</span>';
        strHtml += '<span class="apptInput width-full float-left">' + Staff.Name + '</span>';

        strHtml += '</div>';

        strHtml += '</div>';

        strHtml += '</div>';

        strHtml += '<div>';
        strHtml += '<button type="button" onclick="GenerateManualQuoteDocs(' + appt.Id.toString() + ');" class="button-thin float-right padded">Print</button>';
        strHtml += '<button type="button" onclick="DeleteQuoteAppointment(' + appt.Id.toString() + ');" class="button-thin float-left padded">Cancel</button>';
        strHtml += '</div>';
        strHtml += '</div>';
    });

    panel.innerHTML = strHtml;
}


function JobHistory_LoadCallback(results) {
    var panel = document.getElementById('pnlWorkStage_4');
    try {
        if (!results) {
            DisplayMessage(panel.id, 'Error: Arb Pro service did not return a valid history list.');
        } else if (results.length == 0) {
            DisplayMessage(panel.id, 'Information: There are no documents to display.');
        } else {

            var strHtml = '<table class="searchGrid small width-full">';
            strHtml += '<col width="15%"><col width="15%"><col width="15%"><col width="20%"><col width="15%"><col width="20%">';
            strHtml += '<tr>';
            strHtml += '<th>Type</th>';
            strHtml += '<th>Reference No</th>';
            strHtml += '<th>Letter Type</th>';
            strHtml += '<th>Created On</th>';
            strHtml += '<th>Sent Via</th>';
            strHtml += '<th>Sent On</th>';
            strHtml += '<th></th>';
            strHtml += '</tr>';

            results.forEach(function (h) {
                strHtml += '<tr>';
                strHtml += '<td style="vertical-align: top;">' + h.ItemTypeString + '</td>';
                strHtml += '<td style="vertical-align: top; text-align: center;">' + h.ReferenceNo + '</td>';
                strHtml += '<td style="vertical-align: top; text-align: left;">' + h.DocTypeString + '</td>';
                var CreationDate = new Date(parseInt(h.CreationDate.substr(6)));
                var SentDate = new Date(parseInt(h.SentDate.substr(6)));
                strHtml += '<td style="vertical-align: top; text-align: center;">' + (CreationDate.getYear() > 1 ? CreationDate.toDateString() : 'N/A') + '</td>';
                strHtml += '<td style="vertical-align: top;">' + h.DispatchMethodString + '</td>';
                strHtml += '<td style="vertical-align: top; text-align: center;">' + (SentDate.getYear() > 1 ? SentDate.toDateString() : 'N/A') + '</td>';
                strHtml += '<td style="vertical-align: top; text-align: center;"><a target="_blank" href="Images/ReportImage.aspx?Id=' + h.Id + '">View</a></td>';
                strHtml += '</tr>';
            });

            strHtml += '</table>';
            panel.innerHTML = strHtml;
        }
    } catch (err) {
        DisplayMessage(panel.id, 'Error: Unable to display Job history - ' + err.toString());
    }
}
/*

    *********************   THE Following methods are generic method for the functionality of the work management page ***************

*/

function EnquiryDetailsUpdate() {
    document.getElementById('pnlEnquiryDetails').innerHTML = ConstructEnquiryDetailsTable();


    //document.getElementById('lblJobEnquirySource').textContent = ObjJob.Source;
    //document.getElementById('lblJobEnquiryStaff').textContent = ObjJob.Reporter;
    //document.getElementById('lblJobEnquiryComments').textContent = ObjJob.Comments;
    //document.getElementById('lblJobEnquiryDefaultSurveyor').textContent = ObjJob.DefaultSurveyor;
    //document.getElementById('lblJobEnquiryStatus').textContent = ObjJob.StatusString;
    //var txtLbl = document.getElementById('lblJobEnquiryShowInDb');
    //if (ObjJob.ShowInDashboard) {
    //    txtLbl.textContent = 'Displayed';
    //} else {
    //    txtLbl.textContent = 'Not Displayed in Dashboard';
    //}
}

function ConstructEnquiryDetailsTable() {
    var strHtml = '<div class="width-full">';
    strHtml += '<table class="enquiryGrid">';
    strHtml += '<tr><th colspan="2">';
    strHtml += '<input id="btnEditJobDetails" class="button-thin padded float-right" type="button" onclick="OpenJobEditDialog();" value="Edit Enquiry" >';
    strHtml += '</th></tr><tr><td class="rowHeader">Job Status</td><td class="content">';
    strHtml += ObjJob.StatusString;
    strHtml += '</td></tr>';
    strHtml += '</th></tr><tr><td class="rowHeader">Dashboard</td><td class="content">';
    if (ObjJob.ShowInDashboard) {
        strHtml += 'Displayed when appropriate';
    } else {
        strHtml += 'Not Displayed in Dashboard';
    }
    strHtml += '</td></tr>';
    strHtml += '</th></tr><tr><td class="rowHeader">Source</td><td class="content">';
    strHtml += ObjJob.Source;
    strHtml += '</td></tr>';
    strHtml += '</th></tr><tr><td class="rowHeader">Reporter</td><td class="content">';
    strHtml += ObjJob.Reporter;
    strHtml += '</td></tr>';
    strHtml += '</th></tr><tr><td class="rowHeader">Default Surveyor</td><td class="content">';
    strHtml += ObjJob.DefaultSurveyor;
    strHtml += '</td></tr>';
    strHtml += '</th></tr><tr><td class="rowHeader">Comments</td><td class="content">';
    strHtml += ObjJob.Comments;
    strHtml += '</td></tr>';

    strHtml += '</table></div>';
    return strHtml;
}

//    Function to switch the selected work stage based on the global variable
function SwitchTab() {
    //This function is a wrapper to facilitate compatability for url params
    SwitchJobStageTab(Job_curTab - 1);
}

//       Function to set the css classes for the work flow tabs to display the selected tab only
//       This function is dependant on the naming convention used in the aspx file
function SwitchJobStageTab(selectedTab) {
    Job_curTab = selectedTab + 1;
    var tab;
    var panel;
    var count = 6;  //The total number of stages
    for (var i = 0; i < count; i++) {
        //get the current items being iterated
        tab = document.getElementById('tabWorkStage_' + i.toString());
        panel = document.getElementById('pnlWorkStage_' + i.toString());
        if (i == selectedTab) {
            //The current index is the panel to be displayed
            tab.className = 'jobTab activeTab';
            panel.className = '';
        }
        else {
            //The panel is not selected and should be hidden
            tab.className = 'jobTab';
            panel.className = 'hidden';
        }
    }
    LoadJobStagePanel(selectedTab);
}

//      Function to load the current panel to be displayed
function LoadJobStagePanel(selectedTab) {
    switch(selectedTab){
        case 0:
            //Display the enquiry tab
            //This will already be populated but the appoinments may need updating
            if (ObjJob != null) {
                //  Update the intial details
                EnquiryDetailsUpdate();
                LoadAppointments(ObjJob.Id);
            }
            break;
        case 1:
            //Load the selected quote
            SelectedQuoteChanged();
            break;
        case 2:
            //Load the selected work
            //The variable ObjSelectedQuote is defined in Quote.js
            Work_Load();
            break;
        case 3:
            //Load the invoices for the Quote
            SwitchInvoiceTab(0);
            break;
        case 4:
            //Load the documents
            if (ObjJob != null) {
                JobHistory_Load();
            }
            break;
    }

}

function ToggleList() {
    var Panel = $('#pnlJobsList');
    var Filter = $('#pnlJobsListFilter');
    var lblName = $('#lblClientName');
    var lblLink = $('#lnkReturnToClient');
    var List = $('#pnlJobsListList');

    if (Job_listShown) {
        Job_listShown = false;
        Panel.animate({ width: 10 }, 500);
        List.hide(100);
        setTimeout(function () {
            $('#pnlJobsListFilter').css('width', '0px');
            $('#pnlJobsListFilter').css('height', '42px');
            $('#lblClientName').css('visibility', 'hidden');
            $('#lnkReturnToClient').css('visibility', 'hidden');
        }, 490);
    } else {
        Job_listShown = true;
        Panel.animate({ width: 200 }, 500);
        Filter.css('width', '190px');
        Filter.css('height', 'auto');
        lblName.css('visibility', 'visible');
        lblLink.css('visibility', 'visible');
        List.delay(500).show(100);
    }
}

//      Function to load a quote when the drop down is changed
function SelectedQuoteChanged(refreshQuote) {
    if (!refreshQuote) { refreshQuote = 1; }
    var ddl = document.getElementById('ddlQuoteID');
    var ddlOption = ddl.options[ddl.selectedIndex];
    var id = ddlOption.value;
    //  Set the Quote No display for the Work and Invoice panels
    document.getElementById('lblWorkQuoteNo').textContent = ddlOption.text;
    document.getElementById('lblInvQuoteNo').textContent = ddlOption.text;
    if (refreshQuote == 1) {
        Quote_Load(id);
    }
}

//      Function to display all the job sites for the current job
function JobSites_Display(sites) {
    var List = document.getElementById('lstJobSites');

    var listHtml = '<table class="searchGrid width-full">';
    sites.forEach(function (js) {
        if (!js.Disabled) {
            listHtml = listHtml + '<tr onclick="ViewSite(' + js.SiteId + '); return false;" >';
            listHtml = listHtml + '<td style="padding-left: 10px; padding-right: 10px;">' + js.SiteString + '</td>';
            listHtml = listHtml + '</tr>';
        }
    });
    listHtml = listHtml + '</table>';
    List.innerHTML = listHtml;
}

function ClientDetailsDisplay(client) {
    document.getElementById('txtJobClientEmail').textContent = client.Email;
    document.getElementById('txtJobClientTelephone').textContent = client.Telephone;
    document.getElementById('txtJobClientMobile').textContent = client.Mobile;
}

//      Function to mark a job site as disabled
function RemoveJobSite(JobID, SiteID) {
    var ClientID = document.getElementById('hfClientID').value;

    return Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'RemoveJobSiteFromJob',
        {
            JobID: JobID,
            SiteID: SiteID,
        },
        false);
}

//      Generic Function to display the Item list for a stage
function BuildItemTable(itemArray, SiteVatRates, JobVatRates, itemCallBack) {
    itemSort(itemArray);
    itemArray.forEach(function (item) {
        var found = false;
        SiteVatRates.forEach(function (vr) {
            if (vr.rate == item.VatRate) {
                vr.value += item.Value;
                found = true;
            }
        });

        if (!found)
            SiteVatRates.push({ rate: item.VatRate, value: item.Value });

        found = false;
        JobVatRates.forEach(function (vr) {
            if (vr.rate == item.VatRate) {
                vr.value += item.Value;
                found = true;
            }
        });

        if (!found)
            JobVatRates.push({ rate: item.VatRate, value: item.Value });

        itemCallBack(item);
    });
}

//      Function to save a new appoinment
//          TODO:   Refactor to remove hidden fields and use in memory objects
//                  This code should be in the CalendarEditAppointment.js file
function SaveNewAppointment() {
    var start = $('#hfAppStart').val();
    var end = $('#hfAppEnd').val();
    var surveyor = $('#hfAppSurveyor').val();
    var templateId = $('#hfTemplateId').val();

    var jobSiteID = parseInt($('#hfJobSiteId').val());


    Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'SaveQuoteAppointment',
        {
            ApptID: -1,
            JobSiteID: jobSiteID,
            SurveyorID: surveyor,
            StartDateTime: start.toLocaleString(),
            EndDateTime: end.toLocaleString(),
            Completed: false,
            ReDownload: false,
            TemplateId: templateId
        },
        false);

    LoadAppointments(ObjJob.Id);
}

//      Function to display an alert box for confirmation of deletion of a Quote appointment
//      and then delete the appointment
function DeleteQuoteAppointment(appointmentId) {
    //pnlAlertBoxDlg
    Dialog_OpenPage(
        'Cancel Quote Appointment?',
        'Dialogs/Alert.aspx?',
        'content',
        500,
        150,
        true,
        {
            'Yes': {
                text: 'Yes',
                class: 'ui-dialog-button',
                click: function () {
                    //Display wait animation
                    document.getElementById('pnlAlertBoxDlg').innerHTML = GetWaitImageForPanel('pnlAlertBoxDlg', 'Please Wait.');
                    //Blocking service call to delete the appointment
                    var completed = Ajax_CallService(
                        'Services/Web/Calendar.asmx',
                        'DeleteQuoteEvent',
                         {
                             eventId: appointmentId
                         },
                        false);
                    LoadAppointments(ObjJob.Id);
                    $(this).dialog('close');
                }
            },
            'No': {
                text: 'No',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/AlertBox.js?V=3.23.5',
        function () { AlertBox_Show('Are you sure you want to cancel this Quote appointment?'); });
}


/*

        **********  Dialog display functions    ***********

*/

//      Function to display the Edit Site dialog to a new client site to the Job
function AddSite() {
    Dialog_OpenPage(
        'Edit Job Sites',
        'Dialogs/JobSite.aspx',
        'content',
        600,
        320,
        true,
        {
            'Add': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    if (JobSite_Close()) {
                        Job_Refresh(ObjJob.Id);
                        $(this).dialog('close');
                        setTimeout(function () { SwitchTab(); }, 0);
                    } else {
                        alert('Add Job Site Failed');
                    }
                }
            },
            'Cancel': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/JobSite.js?V=3.23.5',
        function () {
            JobSite_Load();
        });
}

//      Function to show the edit Site dialog
function ViewSite(iSiteID) {
    Dialog_OpenPage(
        iSiteID == -1 ? 'New Site' : 'Edit Site',
        'Dialogs/Site.aspx?ID=' + iSiteID,
        'content',
        700,
        500,
        true,
        {
            'Save': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    if (SaveSite()) {
                        Job_Refresh(ObjJob.Id);
                        $(this).dialog('close');
                    } else {
                        alert('Site Save Failed');
                    }
                }
            },
            'Cancel': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/Site.js?V=3.23.5',
        function () {
            Site_Load();
        });
}

//      Generic function that is called by Quote.js, Work.js and Invoice.js to display a dialog for editing items
//      NOTE: the update function is a callback function that requires no parameters
function EditJobItemForStage(id, workFlowStage, updateFunction) {
    Dialog_OpenPage(
        'Edit ' + workFlowStage + ' Item',
        'Dialogs/JobItemEdit.aspx?Stage=' + workFlowStage,
        'content',
        800,
        400,
        true,
        {
            'Ok': {
                text: 'OK',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                    SaveJobEditItem();
                    updateFunction();
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/JobItemEdit.js?V=3.23.5',
        function () { JobItemEdit_PageLoad(id); });
}

//      Function to display a dialog that enables manual quote documents to be created : This is an Appointment function
function GenerateManualQuoteDocs(calendarId) {
    Dialog_OpenPage(
       'Manual Quote Pack',
       'Dialogs/PDFPreview.aspx',
       'content',
       500,
       735,
       true,
       {
           'Close': {
               text: 'Close',
               class: 'ui-dialog-button',
               click: function () {
                   $(this).dialog('close');
               }
           }
       },
       'Content/js/ManualQuoteDocuments.js?V=3.23.5',
       function () { ManualQuoteDocuments_Load(calendarId); });
}

function DisplayGenericItemDialog(strStage, parentId, siteId, callbackFunction) {
    Dialog_OpenPage(
        'Add ' + strStage + ' Item',
        'Dialogs/JobItem.aspx?JobID=' + ObjJob.Id.toString() + '&Stage=' + strStage + '&StageID=' + ObjSelectedQuote.Id + '&SiteID=' + siteId.toString() + '&InvoiceQuoteID=' + parentId.toString(),
        'content',
        800,
        400,
        true,
        {
            'Ok': {
                text: 'OK',
                class: 'ui-dialog-button',
                click: function () {
                    if (AddItemToJob()) {
                        $(this).dialog('close');
                        callbackFunction();
                    }
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/JobItem.js?V=3.23.5',
        function () { JobItem_PageLoad(); });
}

//      Function to display the calendar dialog to add a new appoinment
//          TODO:   Refactor to remove hidden fields and use in memory objects
//                  This code should be in the CalendarEditAppointment.js file 
function NewAppointment() {
    var edit = false;
    var enquiry = false;
    var Id = -1;
    if (ListSurveyors && ListSurveyors.length > 0) {
        Id = ListSurveyors[0].Id;
    }

    Dialog_OpenPage('New Appointment',
        'Dialogs/CalendarEditAppointment.aspx?SurveyorID=' + Id.toString(),
        'content',
        1200,
        700,
        true,
        {
            'Done': {
                text: 'Done',
                class: 'ui-dialog-button',
                click: function () {
                    var conf = confirm("Are you sure you want to use the selected risk assessment template for this appointment?");
                    if (conf) {
                        SaveNewAppointment();
                        $(this).dialog('close');
                    }
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/CalendarEditAppointment.js?V=3.23.5',
        function () {
            LoadCalendar(ObjJob.WorkSites, edit, enquiry);
        });
}

function EditAppointment(iApptId) {
    var appt = Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'GetQuoteAppointmentForId',
        {
            ApptId: iApptId
        },
        false);

    var jobSiteId = appt.JobSiteID;
    var edit = true;
    var enquiry = false;
    Dialog_OpenPage('Edit Appointment',
        'Dialogs/CalendarEditAppointment.aspx?SurveyorID=' + appt.SurveyorID.toString() + '&AppId=' + iApptId,
        'content',
        1200,
        700,
        true,
        {
            'Done': {
                text: 'Done',
                class: 'ui-dialog-button',
                click: function () {
                    SaveExistingAppointment(appt.Id, appt.JobSiteID);
                    LoadAppointments();
                    $(this).dialog('close');
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/CalendarEditAppointment.js?V=3.23.5',
        function () {
            LoadCalendar(ObjJob.WorkSites, edit, enquiry, jobSiteId, appt);
        });
}

function SaveExistingAppointment(iApptID, iJobSiteID) {
    var start = $('#hfAppStart').val();
    var end = $('#hfAppEnd').val();
    var surveyor = $('#hfAppSurveyor').val();
    var templateId = $('#hfTemplateId').val();
    var completed = $('#chkAppointmentCompleted').is(':checked');

    Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'SaveQuoteAppointment',
        {
            ApptID: iApptID,
            JobSiteID: iJobSiteID,
            SurveyorID: surveyor,
            StartDateTime: start.toLocaleString(),
            EndDateTime: end.toLocaleString(),
            Completed: completed,
            ReDownload: false,
            TemplateId: templateId
        },
        false);

    LoadAppointments(ObjJob.Id);
}

function OpenJobEditDialog() {
    // debugger;
    Dialog_OpenPage(
        'Edit Enquiry Details',
        'Dialogs/JobDetails.aspx?JobID=' + ObjJob.Id.toString(),
        'content',
        800,
        400,
        true,
        {
            'Ok': {
                text: 'Save',
                class: 'ui-dialog-button',
                click: function () {
                    if (JobDetails_Save()) {
                        $(this).dialog('close');
                        Job_Refresh(ObjJob.Id);
                    }
                }
            },
            'Cancel': {
                text: 'Cancel',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
        'Content/js/JobDetailsEdit.js?V=3.23.5',
        function () { JobDetails_Load(); });
}

/*

        **********  Page navigation functions    ***********

*/

//      Function to create and display a new Job
function NewJob() {
    ShowNewJobDialog(ObjJob.ClientId, DisplayNewJob);
    //try{
    //    var NewJobID = Ajax_CallService(
    //            'Services/Web/WorkFlow.asmx',
    //            'CreateNewJob',
    //            {
    //                ClientID: ObjJob.ClientId
    //            },
    //            false
    //        );
    //    //Check that the new Job has been created and force navigation
    //    if (NewJobID > 0) {
    //        window.location = 'Job.aspx?JobID=' + NewJobID;
    //    }
    //    else {
    //        DisplayMessageDialog('Error', 'The Arb Pro service failed to return a valid number for the new Job.');
    //    }
    //}
    //catch(err){
    //    DisplayMessageDialog('Error', 'Job creation failed - ' + err.toString());
    //}
    //return false;
}

function DisplayNewJob(jobId) {
    if (jobId > 0) {
        window.location = 'Job.aspx?JobID=' + jobId;
    }
}

//      Function to display the Risk Assessment details for the selected quote
function OpenRiskAssessment() {
    var jobId = ObjJob.Id;
    var quoteId = document.getElementById('ddlQuoteID').value;
    if (quoteId > 0) {
        var strUrl = '/RiskAssessment.aspx?JobId=' + jobId;
        if (quoteId != "-1") {
            strUrl += '&QuoteID=' + quoteId.toString();
            if (ObjJob.WorkSites != null) {
                strUrl += '&SiteId=' + ObjJob.WorkSites[0].Id; //default to the first item
            }
        }
        window.location = strUrl;
    }
    else {
        DisplayMessageDialog('Risk Assessment', 'The quote must be created, and items added before editing the risk assessment.');
    }
}

function GetCompanyDetails() {
    var compDetails = Ajax_CallService(
        'Services/Web/Company.asmx',
        'GetCompany',
        {},
        false
    );
    return compDetails;
}