﻿function LogOut() {
    if (SignOut()) {
        window.location.reload();
    }
}

function ChangePassword() {
    var strOldPassword = document.getElementById('txtOldPassword').value;
    var strConfirmPassword = document.getElementById('txtConfirmPassword').value;
    var strNewPassword = document.getElementById('txtNewPassword').value;
    if (ChangeUserPassword(Page_strUserEmail, strOldPassword, strConfirmPassword, strNewPassword)) {
        return true;
    } else {
        return false;
    }
}