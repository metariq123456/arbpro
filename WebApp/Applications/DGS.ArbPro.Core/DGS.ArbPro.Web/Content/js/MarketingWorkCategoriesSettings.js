﻿var SelectedResultId = -1;
var IsNewItem = false;
var categoryList = null;

// Function to get the work categories and load the window content
function Settings_Load() {
    categoryList = GetAllWorkCategories();
    UpdateTable();
}

// Function to update the table with the work categories
function UpdateTable() {
    var listPanel = document.getElementById('pnlTableContainer');
    var strHtml = '<table class="searchGrid width-full white">';
    for (var i = 0; i < categoryList.length; i++) {
        if (categoryList[i].Deleted !== true) {
            if (categoryList[i].Id === SelectedResultId) {
                strHtml += '<tr class="selected" onclick="ItemSelected( \'' + i + '\');">';
            }
            else {
                strHtml += '<tr onclick="ItemSelected( \'' + i + '\');">';
            }
            strHtml += '<td style="padding-left: 10px;">' + categoryList[i].Name + '</td>';
            strHtml += '</tr>';
        }
    }
    strHtml += '</table>';

    listPanel.innerHTML = strHtml;
}

function ItemSelected(index) {
    var item = categoryList[index];
    IsNewItem = false;
    SelectedResultId = item.Id;
    PopulateHiddenFields(item.Name, item.Description, item.ReturnTimeCategory);
    UpdateTable();
}

// Function to populate the hidden fields
function PopulateHiddenFields(name, desc, rtnTime) {
    document.getElementById('btnDelete').className = 'button-thin';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtName').value = name;
    document.getElementById('txtDesc').value = desc;
    document.getElementById('ddlRtnTime').value = rtnTime;
}

// Function to call a new work category (clears content of all fields displayed)
function NewMarketingItem() {
    SelectedResultId = -1;
    UpdateTable();
    document.getElementById('btnDelete').className = 'button-thin hidden';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtName').value = '';
    document.getElementById('txtDesc').value = '';
    document.getElementById('ddlRtnTime').value = '12';
}

// Function to save a work category
function SaveItem() {
    var newCategory = {};
    newCategory.Id = SelectedResultId;
    newCategory.Name = document.getElementById('txtName').value;
    newCategory.Description = document.getElementById('txtDesc').value;
    newCategory.ReturnTimeCategory = document.getElementById('ddlRtnTime').value;

    var success = false;

    try {
        if (newCategory.Name === "") {
            success = true;
        } else {
            success = Ajax_CallService(
                '../Services/Web/Marketing.asmx',
                'SaveWorkCategory',
                {
                    obj: newCategory
                },
                false
            );
        }
    } catch (err) {
        DisplayMessage('pnlItemControls', 'ERROR: Unable to save work category - ' + err.toString());
    }

    if (!success) {
        DisplayMessage('pnlItemControls', 'There was a problem saving the work category.');
    } else {
        SelectedResultId = -1;
        document.getElementById('pnlItemControls').className = 'clientField float-left hidden';
    }

    Settings_Load();
}

// Function to delete a work category
function DeleteItem() {
    var success = Ajax_CallService(
           'Services/Web/Marketing.asmx',
           'DeleteWorkCategoryForId',
           {
               categoryId: SelectedResultId
           },
           false
       );
    if (!success) {
        DisplayMessage('pnlItemControls', 'There was a problem deleting the work category.');
    } else {
        document.getElementById('pnlItemControls').className = 'clientField float-left hidden';
    }
    Settings_Load();
}

// Function that gets all work categories
function GetAllWorkCategories() {
    var catList = Ajax_CallService(
        'Services/Web/Marketing.asmx',
        'GetAllWorkCategories',
        {
        },
        false
    );

    return catList;
}
