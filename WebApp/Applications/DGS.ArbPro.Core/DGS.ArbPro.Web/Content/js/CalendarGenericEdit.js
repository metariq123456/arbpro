﻿function CalendarGeneric_Load(event) {
    ToggleAllDayPannel(event.allDay);
    document.getElementById('chkAllDay').checked = event.allDay;
}

function CheckChanged() {
    var allDay = document.getElementById('chkAllDay').checked;
    ToggleAllDayPannel(allDay);
}

function ToggleAllDayPannel(allDay) {
    if (allDay) {
        document.getElementById('pnlTimed').className = 'hidden';
        document.getElementById('pnlAllDay').className = '';
    }
    else {
        document.getElementById('pnlTimed').className = '';
        document.getElementById('pnlAllDay').className = 'hidden';
    }
}