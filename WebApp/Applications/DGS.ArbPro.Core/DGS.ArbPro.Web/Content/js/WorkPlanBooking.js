﻿/*

    The following functions facilitate the creation and editing of work bookings.
    There are two initialisation functions, one to enable the edit of a current work plan and the second
    to enable the creation of a new work plan.

    The structure of this process has been changed.
    This file replaces the BookWork.js file
*/

//  Structures
var editEvent = {
    id: 0,
    title: 'Start',
    allDay: false,
    start: null,
    end: null,
    className: 'cal-event-editable'
}

//  Global scope variables
var ObjWorkSummary = null;  //  The summary of all of the work and bookings for the selected item
var UrlSiteId = -1;         //  The job site Id of the selected item
var UrlWorkPlanId = -1;     //  The work plan Id of the item being edited

/*

    **********  Initialisation and Service call functions   **********

*/

//      Initialisation function
function WorkPlanBooking_Load() {
    var workId = -1;

    //Get the id of the work to be booked 
    var Param = GetURLParameter('WorkID');
    //This Parameter is required
    if (Param != 'null') {
        workId = parseInt(Param);
    }

    //Check if a site record has been provided.
    var Param = GetURLParameter('ReferrerSiteID');
    if (Param != 'null') {
        UrlQuoteId = parseInt(Param);
    }

    //Check if a work plan record has been provided.
    var Param = GetURLParameter('WorkPlanID');
    if (Param != 'null') {
        UrlWorkPlanId = parseInt(Param);
    } else {
        UrlWorkPlanId = -1;
    }

    if (UrlWorkPlanId > 0) {
        WorkSummary_Load(UrlWorkPlanId, 'GetWorkSummaryForWorkPlan');
    }
    else if (workId < 1) {
        //  a required url parameter has not been provided
        //  Display an alert box to say the work cannot be loaded
        DisplayMessageDialog('Book Work Error', 'Unable to process request. Work record identity has not been supplied.');
    } else {
        WorkSummary_Load(workId, 'GetWorkSummaryForWork');
    }
}

//      Calendar initialisation function
function LoadCalendar() {
    var panel = document.getElementById('pnlCalendar');
    panel.innerHTML = '';

    $('#pnlCalendar').fullCalendar({
        header: {
            left: 'title',
            center: 'agendaDay,agendaWeek,month',
            right: 'prev,today,next'
        },
        height: 600,
        aspectRatio: 1.4,
        defaultView: 'month',
        events: GetCalendarEvents,
        dayClick: CalendarDayClickEvent,
        eventClick: CalendarClickEventHandler,
        eventResize: CalendarResizeEvent,
        eventDrop: CalendarDropEvent,
        viewRender: CalendarRenderView,
        editable: true,
        firstHour: 6,
        allDaySlot: false,
        allDayText: 'All Day',
        axisFormat: 'HH:mm',
        titleFormat: {
            week: 'MMMM d[ yyyy]{ &#8212;[ MMM] d yyyy}',
            day: 'dddd, dd MMMM yyyy'
        },
        columnFormat: {
            week: 'ddd dd/MM',
            day: 'dddd, dd MMMM'
        },
        eventMouseover: function (event, jsEvent, view) {
            timer = setTimeout(function () {
                var summary = Ajax_CallService(
                    'Services/Web/WorkProgramming.asmx',
                    'GetWorkSummaryForWorkPlan',
                    {
                        id: event.workPlanId
                    },
                    false);
                var strStartDate = event.start.format("dd-MM-yyyy hh:mm tt")
                var strEndDate = ''
                if (event.end != null) {
                    strEndDate = event.end.format("dd-MM-yyyy hh:mm tt")
                } 

                tooltip = '<div class="tooltiptopicevent" style="max-width: 220px; width:auto;height:auto;background:#fff;border: solid grey 1px; position:absolute;z-index:10001;padding:10px 10px 10px 10px; line-height: 100%; box-shadow: 10px 10px 5px #888888;">' + '<span style="font-size: 0.9em; font-weight: bold;">' + event.title + '</span><br/><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Total Work Value:</span><br/>' + CurrentCurrency +  (summary.Value + summary.VatVal).toFixed(2) + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">Start:</span><br/>' + strStartDate + '</br><br/>' + '<span style="font-size: 0.8em; font-weight: bold;">End:</span><br/>' + strEndDate + '</div>';
                $("body").append(tooltip);
                $(this).css('z-index', 10000);
                //Set tooltip to position of mouse
                $('.tooltiptopicevent').css('top', jsEvent.pageY + 10);
                $('.tooltiptopicevent').css('left', jsEvent.pageX + 20);
                $('.tooltiptopicevent').hide().fadeIn('slow');
            }, 800);

            $(this).mousemove(function (e) {
                $('.tooltiptopicevent').fadeOut();
            });
        },
        eventMouseout: function (event, jsEvent, view) {
            clearTimeout(timer);
            $(this).css('z-index', 8);
            $('.tooltiptopicevent').fadeOut('slow', removeToolTip);
            function removeToolTip() { $('.tooltiptopicevent').remove(); }
        },
        timeFormat: '',
        slotMinutes: '60'
    });
}

//      Service call function to load a work plan from the service
function WorkSummary_Load(workId,endpoint) {
    //Load the work summary from the service
    Ajax_CallService(
        'Services/Web/WorkProgramming.asmx',
        endpoint,
        {
            id: workId
        },
        true,
        DisplaySummaryDetails
    );
}

//      Async service call to load all notes for the quote
function LoadQuoteNotes() {
    NotesList_Load(ObjWorkSummary.QuoteId, 0, DisplayQuoteNotes);
}

//      Async service call to load all notes for the work
function LoadWorkNotes() {
    NotesList_Load(ObjWorkSummary.Id, 1, DisplayWorkNotes);
}

/*

    **********  Service callback functions   **********

*/

//      Async callback function to populate the Summary details for the selected work
function DisplaySummaryDetails(result) {
    var panel = document.getElementById('pnlSummaryDetail');
    if (!result || result.Id < 0) {
        DisplayMessage('pnlErrorWork', 'Error: The ArbPro service did not return a valid work summary.');
        DisplayMessage(panel.id, 'Error: The ArbPro service did not return a valid work summary.');
    } else {
        try {
            ObjWorkSummary = result;
            //Depricated
            //Ensure the global variables are set correctly
            //QuoteId = ObjWorkSummary.QuoteId;
            //WorkId = ObjWorkSummary.Id;

            document.getElementById('lblQuoteNo').textContent = 'Quote No: ' + ObjWorkSummary.QuoteNo;
            //Generate the html for the panel
            //Left
            var strHtml = '';
            strHtml += '<div class="width-full">'
            strHtml += '<div class="bookWorkField float-left">';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkLabel">Client Name</label><label class="bookWorkInput">' + ObjWorkSummary.ClientName + '</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkLabel">Account No</label><label class="bookWorkInput">' + ObjWorkSummary.AccountNo + '</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkLabel">Address:</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkInput" style="width: 350px">' + ObjWorkSummary.StreetNumber.toString() + ' ' + ObjWorkSummary.AddressLn1 + '</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '<div class="bookWorkPane">';
            if (ObjWorkSummary.AddressLn2.length > 0) {
                strHtml += '<label class="bookWorkInput">' + ObjWorkSummary.AddressLn2 + '</label></br>';
            }
            strHtml += '<label class="bookWorkInput">' + ObjWorkSummary.Town + '</label></br>';
            strHtml += '<label class="bookWorkInput">' + ObjWorkSummary.County + '</label></br>';
            strHtml += '<label class="bookWorkInput">' + ObjWorkSummary.PostCode + '</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkLabel">Email</label><label class="bookWorkInput"><a href="mailto:' + ObjWorkSummary.Email + '">'+ ObjWorkSummary.Email + '</a></label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkLabel">Telephone</label><label class="bookWorkInput">' + ObjWorkSummary.Telephone + '</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkLabel">Mobile</label><label class="bookWorkInput">' + ObjWorkSummary.Mobile + '</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '</div>';
            //Right
            strHtml += '<div class="bookWorkField float-left">';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkLabel">Number of Sites</label><label class="bookWorkInput">' + ObjWorkSummary.JobSites.length.toString() + '</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkLabel">Number of Items</label><label class="bookWorkInput">' + ObjWorkSummary.WorkItems.length.toString() + '</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkLabel">Work value (' + CurrentCurrency + ')</label><label class="bookWorkInput">' + ObjWorkSummary.Value.format() + '</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkLabel">VAT Value (' + CurrentCurrency + ')</label><label class="bookWorkInput">' + ObjWorkSummary.VatVal.format() + '</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '<div class="bookWorkPane">';
            strHtml += '<label class="bookWorkLabel">Total Value (' + CurrentCurrency + ')</label><label class="bookWorkInput">' + (ObjWorkSummary.VatVal + ObjWorkSummary.Value).format() + '</label>';
            strHtml += '</div>';    //'</br>';
            strHtml += '</div></div>';
            //Bottom - Client address details
            strHtml += '<div class="width-full float-left">'
            //  Work Plans already booked
            strHtml += '<table class="searchGrid width-full"><col width="12%" /><col width="13%" /><col width="50%" /><col width="10%" /><col width="15%" />';
            strHtml += '<tr><th colspan="6"><span class="float-left">Work Plans</span><span class="float-right">Click a Work Plan to Edit</span></th></tr>';
            debugger;
            if (ObjWorkSummary.BookedWork.length == 0) {
                //  There is no currently booked work
                strHtml += '<tr><td colspan="5" style="text-align: center;">There are currently no bookings for this work.</br>Please Click the date in the calendar you would like work to begin</td></tr>';
            } else {
                //  Display each work booking
                strHtml += '<tr><th>Quote No</th><th>Start Date</th><th>Site Address</th><th>Value (' + CurrentCurrency + ')</th><th>Work Packs</th></td>';
                ObjWorkSummary.BookedWork.forEach(function (wp) {
                    
                    strHtml += '<tr><td onClick="NavigateToAndDisplayWorkPlan(' + wp.Id.toString() + ')">';
                    strHtml += wp.QuoteReference;
                    strHtml += '</td><td onClick="NavigateToAndDisplayWorkPlan(' + wp.Id.toString() + ')">';
                    var dateStr = 'Pending';
                    if (wp.StartDate != null) {
                        dateStr = new Date(parseInt(wp.StartDate.substr(6))).toDateString();
                    }
                    strHtml += dateStr;
                    strHtml += '</td><td onClick="NavigateToAndDisplayWorkPlan(' + wp.Id.toString() + ')">';
                    strHtml += wp.SiteAddress;
                    strHtml += '</td><td>';
                    strHtml += wp.QuoteValue.format();;
                    strHtml += '</td onClick="NavigateToAndDisplayWorkPlan(' + wp.Id.toString() + ')">';
                    if (wp.StartDate == null) {
                        strHtml += '<td>Click on Calendar to book';
                    } else {
                        debugger;
                        strHtml += '<td style="text-align: center;"><input type="button" class="BookWork button-thin padded" value="Print" onclick="ShowWorkPackDialog(' + wp.Id + '); return false;" />';
                    }
                    strHtml += '</td></tr>';
                });
            }
            strHtml += '</table>';
            debugger;
            //Display any work items not scheduled
            var notBooked = $.grep(ObjWorkSummary.WorkItems, function (e) { return !e.Booked && !e.Completed && e.Status != 99; });
            itemSort(notBooked);
            if (notBooked && notBooked.length > 0) {
                strHtml += '</br><table  class="searchGrid width-full">';
                strHtml += '<tr><th colspan="4">Work Items that have not been booked</th></tr>';
                strHtml += '<tr><th>Code</th><th>Name</th><th>Description</th><th>Picture</th></tr>';
                notBooked.forEach(function (wi) {
                    strHtml += '<td style="vertical-align: top; text-align: center;">' + wi.Code + '</td>';
                    strHtml += '<td style="vertical-align: top;">' + wi.Name + '</td>';
                    strHtml += '<td style="vertical-align: top;">' + wi.Description + '</td>';
                    strHtml += '<td style="text-align: center;" onclick="event.preventDefault(); OpenImage(2, ' + wi.Id + ');"><a href="#">Image</a></td></tr>';
                });
                strHtml += '</table>';
            }
            strHtml += '</div>';
            //Populate the panels inner html
            panel.innerHTML = strHtml;

            //  Load the events for the calendar - This is carried out here due to the possibility of conflict with the
            LoadCalendar();
            //  Force the notes to be updated
            SwitchNotesPanel(0);
            //  Navigate to the selected date as required
            NavigateToDate();
        }
        catch (err) {
            DisplayMessage(panel.id, 'Error: Unable to display work summary - ' + err.toString());
        }
    }
}

//      Async callback to construct the Quote Notes panel
function DisplayQuoteNotes(results) {
    var notesPanel = document.getElementById('pnlNotesDetail_0');
    PopulateNotes(notesPanel, 0,ObjWorkSummary.QuoteId, results, LoadQuoteNotes);
}

//      Async callback to construc the work notes panel
function DisplayWorkNotes(results) {
    var notesPanel = document.getElementById('pnlNotesDetail_1');
    PopulateNotes(notesPanel, 1,ObjWorkSummary.Id, results, LoadWorkNotes);
}

/*********************************************************************************************************

                                            CALENDAR FUNCTIONS

    The following functions replace the inline code for the calendar function. This is intended to increase 
    code reuse and improve error capturing and debug functionality.

*********************************************************************************************************/
//      Function to get the events to be displayed on the calendar
function GetCalendarEvents(startDate, endDate, CallbackFunction) {
    try {
        $('#pnlCalendar').fullCalendar('renderEvent', editEvent);

        var EventList = new Array();
        var Events;
        var filter = false;
        var ddl = document.getElementById('ddlTeams');
        debugger;
        if (ddl && filter) {
            editEvent.title = ddl.options[ddl.selectedIndex].text;

            Events = Ajax_CallService(
                'Services/web/Calendar.asmx',
                'GetWorkBookingsForTeam',
                {
                    TeamID: ddl.options[ddl.selectedIndex].value,
                    Start: startDate,
                    End: endDate,
                },
                false);
        } else {
            Events = Ajax_CallService(
                'Services/web/Calendar.asmx',
                'GetWorkBookings',
                {
                    Start: startDate,
                    End: endDate,
                },
                false);
        }

        var today = new Date();

        Events.forEach(function (ev) {
            //Setup event colours
            var total = ev.Color.R + ev.Color.G + ev.Color.B;
            var textCol = total > (255 * 3) / 2 ? 'black' : 'white';
            var boarderCol = 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')';
            var backCol = 'rgb(' + ev.Color.R + ',' + ev.Color.G + ',' + ev.Color.B + ')';
            //  Check if the current event being rendered is the selected item
            if (ev.Id == UrlWorkPlanId) {
                //   This event is the selected item so change the boarder and back ground colours
                backCol = 'rgb(255, 255, 0)';   //  Default the back ground to yellow
                textCol = 'black';              //  Ensure the text can be read
            }
            //Set the css class for the event
            var cssClass = 'cal-event-work';
            if (ev.WorkPlanId == UrlWorkPlanId) {
                cssClass = 'cal-event-selectedwork'
            }
            //  Build the event list that is to be displayed
            EventList.push({
                id: ev.Id,
                workPlanId: ev.WorkPlanId,
                data: ev,
                title: ev.CalendarString,
                allDay: false,
                start: new Date(parseInt(ev.StartDateTime.substr(6))),
                end: new Date(parseInt(ev.EndDateTime.substr(6))),
                className: cssClass,
                editable: true,
                backgroundColor: backCol,
                borderColor: boarderCol,
                textColor: textCol
            });
        });

        CallbackFunction(EventList);
    } catch (err) {
        DisplayMessage('pnlErrorWork', 'Error: Unable to communicate with ArbPro service -' + err.toString());
    }
}

//      Function to handle a calendar day event click
function CalendarDayClickEvent(date, allDay, jsEvent, view) {
    try {
            //  Check if the page has been sufficiently loaded
            if(!ObjWorkSummary){
                //  Process cannot continue - Display an alert and return false
                DisplayMessageDialog('Book Work', 'The current work record has not finished loading.<br>Please wait.');
            }
            //debugger;
            // Check if there is any unbooked work
            if (HasUnbookedWork()) {
                //  There is unbooked work - assume that the user has clicked on the day they want to book work for 
                //  Check if there is a work plan that does not have a start date -- this is possible if the quote has been uploaded
                if (ObjWorkSummary.BookedWork.length > 0) {
                    unbookedWorkPlans = $.grep(ObjWorkSummary.BookedWork, function (e) { return e.StartDate == null; });
                    if (unbookedWorkPlans && unbookedWorkPlans.length > 0) {
                        //  We have found at least one unbooked work plan - default to the first element in the array
                        NewAppointment(date, false, unbookedWorkPlans[0].Id);
                    } else {
                        NewAppointment(date, false, -1);
                    }
                } else {
                    NewAppointment(date, false, -1);
                }
            } else {
                //  All work has been booked - ensure the user is wanting to edit a current booking
                DisplayMessageDialog('Book Work', 'All work has been booked in the calendar.<br>Please click edit on the booking you want to edit,<br> alternativly click a booked appoinment on the calendar.');
            }
        }
    catch (err) {
        DisplayMessage('pnlErrorWork', 'Error: Unable to handle Calendar day click - ' + err.toString());
    }
}

//      Function to handle the calendar event click
function CalendarClickEventHandler(event, jsEvent, view) {
    try {

        if (event.editable) {
            EditAppointment(event.workPlanId);
        } else {
            //The work has been started or the appointment is histoic and so is not editable
            ShowAlertMessage('The appointment cannot be edited at this time. This may be due to the appointment being historic or the work has been started.');
        }
    } catch (err) {
        DisplayMessage('pnlErrorWork', 'Error: Unable to handle Calendar event click - ' + err.toString());
    }
}

//      Function to handle the resize of an event on the Calendar
function CalendarResizeEvent(event, delta, revertFunction) {
    try {
        console.log(event);
        revertFunction();
    } catch (err) {
        DisplayMessage('pnlErrorWork', 'Error: Unable to revert Calendar event - ' + err.toString());
    }
}

//      Function to render the calendar view
function CalendarRenderView(view, element) {
    $('#pnlCalendar').fullCalendar('refetchEvents');
    $('#pnlCalendar').fullCalendar('updateEvent', editEvent);
}

function CalendarDropEvent(event, delta, revertFunc) {
    try 
    {
        console.log(event);
        SaveWorkPlaneOnDrop(event);
    }
    catch (err)
    {
        DisplayMessage('pnlErrorWork', 'Error: Unable to handle Calendar event drop - ' + err.toString());
    }
}
/*

    **********  General functions   **********

  
*/

function SaveWorkPlaneOnDrop(event)
{
    if (confirm('Are you sure you want to edit the selected Work Plan?.\n[FromDate Selector] - [ToDate Selector]'))
    {
        var startDate = event._start.format('dd/MMM/yy HH:mm');
        var endDate;
        if (event._end != null) {
            endDate = event._end.format('dd/MMM/yy HH:mm');
        }
        else {
            endDate = event._start.format('dd/MMM/yy HH:mm');
        }
        var appointment = {
            StartDateTime :startDate,
            EndDateTime :endDate,
            Comments : event.data.Comments,
            SkipDays : event.data.SkipDays,
            WorkPlanId : event.data.WorkPlanId,
            JobSiteId : event.data.JobSiteId,
            TeamId: event.data.TeamId,
            id:event.data.Id
    };
        console.log(appointment);
        var result = Ajax_CallService(
                   'Services/Web/WorkProgramming.asmx',
                   'SaveOnlyAppointmentOfWorkPlan',
                   {
                       appointment: appointment
                   },
                       false,
                       null
                   );
        if (!result) {
            alert("An error occured when trying to save the Work Plan");
        }
    }
   
}
//      Function to reload the current work summary
function WorkSummary_Refresh(workPlanId) {
    if (!workPlanId) workPlanId = -1;
    WorkSummary_Load(ObjWorkSummary.Id);
}
//      Function that returns a boolean based on the current work plan
function HasUnbookedWork(){
    //  get a list of all work that is not currently booked from the work summary
    var pendinngWorkItems = $.grep(ObjWorkSummary.WorkItems, function(e) {return !e.Booked});
    if (pendinngWorkItems && pendinngWorkItems.length > 0) {
        //  There are items in the work plan that have not yet been booked
        return true;
    } else {
        //  All work is currently booked
        return false;
    }
}

//      Function that forces the calendar to reload to a specific date
function NavigateToDate() {
    //  Only force the reload if a work plan has been selected
    if (UrlWorkPlanId > 0) {
        //  Define work plan for navigation to
        var wpToDisplay = $.grep(ObjWorkSummary.BookedWork, function (e) { return e.Id == UrlWorkPlanId; });
        //  Now the calendar has been loaded try to set the date of the Calendar
        if (wpToDisplay && wpToDisplay.length > 0) {
            var wp = wpToDisplay[0];
            //debugger;
            if (wp.StartDate != null) {
                $('#pnlCalendar').fullCalendar('gotoDate', toDateFromJson(wp.StartDate));
            }
        }
    }
}

function EditWorkPlan(workPlanId) {
    UrlWorkPlanId = workPlanId;
    NavigateToDate();

}

function SwitchSummaryPanel(panelNo) {
    DisplayActivePanel('tabSummary_', 'pnlSummary_', panelNo, 2);
}

//      function switch notes panel
function SwitchNotesPanel(panelIndex) {
    GetWaitImageForPanel('pnlNotesDetail_' + panelIndex.toString, 'Loading Please Wait.');
    DisplayActivePanel('tabNotes_', 'pnlNotesDetail_', panelIndex, 2);
    LoadDisplayedNotesTab(panelIndex);
}

//      Function to set the CSS class of a series of tabs and panels
function DisplayActivePanel(tabName, panelName, selectedIndex, pnlCount) {
    //Iterate through each panel to set the status as required
    for (var index = 0; index < pnlCount; index++) {
        var tabButton = document.getElementById(tabName + index.toString());
        var tabPanel = document.getElementById(panelName + index.toString());
        //Check if the current index is the selected index
        if (index == selectedIndex) {
            //Set the CSS class of the elements to be selected and shown
            tabButton.className = 'jobTab activeTab';
            tabPanel.className = 'float-left width-full white';
        } else {
            //Set the css class of the elements to be deselected or hidden
            tabButton.className = 'jobTab';
            tabPanel.className = 'hidden';
        }
    }
}

//      function to initiate an Async service call to populate the required data for a notes panel
function LoadDisplayedNotesTab(selectedPanel) {
    switch (selectedPanel) {
        case 0:         // Load the notes for the quote
            LoadQuoteNotes();
            break;
        case 1:         // Load the work notes for the work plan / work record
            LoadWorkNotes();
            break;
    }
}

//      Function to construct the html for a given panel
//function PopulateNotes(notesPanel, notesPnlIndex, results) {
//    try {
//        if (!results) {
//            DisplayMessage(notesPanel.id, 'Error: Arb Pro service did not return valid data');
//        } else {
//            var strHtml = '<div class="width-full float-left white-light"><button type="button" onclick="ShowNotesDialog(-1,' + notesPnlIndex + ');" class="button-thin float-right padded">Add Note</button></div>';
//            strHtml += '<div class="width-full float-left">';
//            strHtml += '<table class="searchGrid width-full">';
//            if (results.length == 0) {
//                strHtml += '<tr><td>No notes to display</td></tr>';
//            } else {
//                strHtml += '<tr class="white-light"><th>Entered By</th><th>Details</th><th>Created</th>'
//                results.forEach(function (c) {
//                    strHtml += '<tr>';
//                    strHtml += '<td onclick="ShowNotesDialog(' + c.Id + ',' + notesPnlIndex + ');" style="padding-left: 10px;">' + c.EnteredBy + '</td><td onclick="ShowNotesDialog(' + c.Id + ',' + notesPnlIndex + ');" style="padding-left: 10px;">' + c.Text + '</td><td onclick="ShowNotesDialog(' + c.Id + ',' + notesPnlIndex + ');" style="padding-left: 10px;">' + toDateStringFromJson(c.EnteredDate, '', 'dd/mm/yyyy') + '</td>'
//                    strHtml += '</tr>';
//                });
//            }
//            strHtml += '</table>';
//            strHtml += '</div>';
//            notesPanel.innerHTML = strHtml;
//        }
//    }
//    catch (err) {
//        DisplayMessage(notesPanel.id, 'Error: Unable to display notes - ' + err.toString());
//    }
//}

//      Function to navigate to an appointment and then display the dialog
function NavigateToAndDisplayWorkPlan(workPlanId) {
    UrlWorkPlanId = workPlanId;
    NavigateToDate();
    EditAppointment(workPlanId);
}

/*

    **********  Dialog functions   **********

*/

//      Function to create a new appointment
function NewAppointment(date, allDay, workPlanId) {
    Dialog_OpenPage(
                'Book Work',
                'Dialogs/CalendarBookWork.aspx?',
                'content',
                750,
                600,
                true,
                {
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (CalendarBookWork_Save()) {
                                //Force the page to reload
                                $(this).dialog('close');
                                WorkPlanBooking_Load();
                            }
                        }
                    },
                    'Close': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/CalendarBookWork.js?V=3.23.5',
                function () {
                     CalendarBookWork_New(date,workPlanId,UrlSiteId);
                }
                );
}

//      Function to enable a user to edit a work plan via click on an appointment
function EditAppointment(workPlanId) {
    Dialog_OpenPage(
                'Edit Booked Work',
                'Dialogs/CalendarBookWork.aspx?',
                'content',
                750,
                600,
                true,
                {
                    'Delete': {
                        text: 'Delete',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (confirm('Do you want to delete this booked work?', 'Confirm deletion')) {
                                if (CalendarBookWork_Delete()) {
                                    //Force the page to reload
                                    $(this).dialog('close');
                                    WorkPlanBooking_Load();
                                }
                            }
                        }
                    },
                    'Save': {
                        text: 'Save',
                        class: 'ui-dialog-button',
                        click: function () {
                            if (CalendarBookWork_Save()) {
                                //Force the page to reload
                                $(this).dialog('close');
                                WorkPlanBooking_Load();
                            }
                        }
                    },
                    'Close': {
                        text: 'Cancel',
                        class: 'ui-dialog-button',
                        click: function () {
                            $(this).dialog('close');
                        }
                    }
                },
                'Content/js/CalendarBookWork.js?V=3.23.5',
                function () {
                    CalendarBookWork_WorkPlan(workPlanId,"",true);
                }
                );
}

//      Function to display the work pack document dialog
function ShowWorkPackDialog(workPlanId) {
    //  This function now shows the dialog for the work packs based on the work plan, not the work record
     Dialog_OpenPage(
       'Work Packs',
       'Dialogs/PDFGeneration.aspx',
       'content',
       900,
       600,
       true,
        {
            'Close': {
                text: 'Close',
                class: 'ui-dialog-button',
                click: function () {
                    $(this).dialog('close');
                }
            }
        },
       'Content/js/WorkPackGeneration.js?V=3.23.5',
       function () {
            PDFGeneration_LoadWorkPlan(workPlanId);      //TODO: This has been modified from the Quote Id to the work plan Id - this must be altered within the dialog
           $("#warningMessage").visible = false;
           if (ObjWorkSummary.BookedWork.length == 0) {
               $("#warningMessage").visible = true;
               $("#warningMessage").html("<strong>Work packs for schedules can only be generated when work has been booked.</strong>");
               $(btnGenerateWorkPack).prop("disabled", true);
           }
       });
}

//      Function to display the add/edit notes dialog
function ShowNotesDialog(notesId, panelIndex) {
    try {

        var recId = -1;
        var workStage = -1;
        //Check which record type the note is for
        if (panelIndex == 0) {
            recId = QuoteId;
            workStage = 1;
        }
        else {
            recId = WorkId;
            workStage = 2;
        }

        Dialog_OpenPage('Note',
            'Dialogs/Notes.aspx?',
            'content',
            500,
            500,
            true,
            {
                'Save': {
                    text: 'Save',
                    class: 'ui-dialog-button',
                    click: function () {
                        Note_Save();
                        SwitchNotesPanel(panelIndex);
                        $(this).dialog('close');
                    }
                },
                'Close': {
                    text: 'Close',
                    class: 'ui-dialog-button',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            },
            'Content/js/Notes.js?V=3.23.5',
            function () {
                Notes_Load(notesId, recId, workStage);
            });
    }
    catch (Err) {
        DisplayMessage('pnlNotesDetail_' + panelIndex.toString(), 'Error: Unable to display notes dialog');
    }
}



//      Function to navigate back to the WorkManagement Page
function ClosePage(curTab) {
    if (ObjWorkSummary) {
        var linkUrl = '/Job.aspx?JobID=' + ObjWorkSummary.JobSites[0].JobId.toString()
        linkUrl += '&WorkID=' + ObjWorkSummary.Id.toString();

        if (typeof (curTab) != typeof (undefined) && curTab != null) {
            linkUrl += "&curTab=" + parseInt(curTab);
        }

        window.location = linkUrl;
    }
}