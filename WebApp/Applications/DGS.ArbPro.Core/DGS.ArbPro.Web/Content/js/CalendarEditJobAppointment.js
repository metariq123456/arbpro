﻿
var appointment;
var jobSite;
var isEdit;
var g_minTimeForBookingWork = '7:00';
var g_maxTimeForBookingWork = '19:00';
var templateId = 1;

function LoadAppointment(JobSite, edit, event) {
    //Start populating lists
    appointment = event;
    templateId = appointment.data.TemplateId;
    jobSite = JobSite;
    isEdit = edit;
    var jobSiteId = event.objectId;
    var txtSite = document.getElementById('txtClientSites');
    var completed = document.getElementById('chkAppointmentCompleted');
    var appLabel = document.getElementById('lblJobNo');
    var btnPrev = document.getElementById('btnPrev');
    var btnNext = document.getElementById('btnNext');

    var clientName = document.getElementById('txtClientName');

    // navigates the months of the calendar in the background to allow users to see other appointments.
    btnPrev.onclick = function() { $('#pnlCalendar').fullCalendar('prev'); }
    btnNext.onclick = function() { $('#pnlCalendar').fullCalendar('next'); }

    // checks if appointment is completed
    completed.checked = event.data.Completed;

    // if a jobsite exists for the appointment fill the client name and job id
    if (jobSite) {
        clientName.value = jobSite.PointOfContact.ClientName;
        appLabel.innerHTML += jobSite.JobId;
    }

    var option = document.createElement("option");
    option.text = JobSite.SiteString;
    option.value = JobSite.Id;


    if (JobSite.Id == jobSiteId) {
        txtSite.value = option.text;
    }

    var selectedJobSiteId = option.value;
    document.getElementById('hfJobSiteId').value = selectedJobSiteId;

    // get the list of surveyors
    var Surveyors = Ajax_CallService(
        'Services/Web/Company.asmx',
        'ListSurveyors',
        {},
        false);
    var ddlSurveyor = document.getElementById('ddlSurveyors');
    var StaffID = document.getElementById('hfAppSurveyor').value;

    // display the correct one
    Surveyors.forEach(function(s) {
        var option = document.createElement("option");
        option.text = s.Name;
        option.value = s.Id;
        ddlSurveyor.add(option);

        if (StaffID == s.Id) {
            ddlSurveyor.selectedIndex = ddlSurveyor.options.length - 1;
        }
    });

    // check if event has been previousely downloaded
    var downloaded = toDateStringFromJson(appointment.data.LastDownloaded);
    var reDownload = document.getElementById('chkDownloadAgain');
    if (!downloaded || downloaded == "") {
        document.getElementById('pnlReDownload').className = 'hidden';
    } else {
        document.getElementById('pnlReDownload').className = 'clientInput float-left';
    }

    // populate the date fields
    var dateFrom = document.getElementById('txtDateFrom');
    var dateTo = document.getElementById('txtDateTo');

    dateFrom.value = appointment.start.format('dd/MMM/yy HH:mm');

    // if end date exists check if its greater than the start date, if not set it to start date
    if (appointment.end) {
        if (appointment.start < appointment.end) {
            dateTo.value = appointment.end.format('dd/MMM/yy HH:mm');
        } else {
            dateTo.value = appointment.start.format('dd/MMM/yy HH:mm');
        }
    } else {
        dateTo.value = appointment.start.format('dd/MMM/yy HH:mm');
    }

    // if the edit button has not been clicked disable all fields
    if (!isEdit) {
        ddlSurveyor.disabled = true;
        dateFrom.disabled = true;
        dateTo.disabled = true;
        completed.disabled = true;
        reDownload.disabled = true;
        btnPrev.disabled = true;
        btnNext.disabled = true;
    }
    
    // every time end date is changed checks if it is lower than start date. if it is it resets it back to start date.
    var logicfrom = function (ct) {
        // two values below are needed in this format for the date time picker value set
        var curDate = jQuery('#txtDateFrom').val();
        ct = ct.format('dd/MMM/yy HH:mm');

        // parsed dates to check precisely if selected date is less than start date
        var parsedCt = Date.parse(ct);
        var parsedCurDate = Date.parse(curDate);

        // if end date < start date, end date = start date. else end date = end date.
        if (parsedCt < parsedCurDate) {
            this.setOptions({
                value: curDate
            });
        } else {
            this.setOptions({
                value: ct
            });
        }
    };

    // Change the dates to ticks to add to the date pickers and
    // enable the display of the calendar when the text fields are clicked
    jQuery(function() {
        $('#txtDateFrom').datetimepicker({
            start: toTicksFromDate(appointment.start),
            format: 'd/M/y H:i',
            timepicker: true,
            minDate: 0,
            minTime: g_minTimeForBookingWork,
            maxTime: g_maxTimeForBookingWork
        });

        // the end date cannot be empty
        if (dateTo.value == '') {
            $('#txtDateTo').datetimepicker({
                start: toTicksFromDate(appointment.start),
                format: 'd/M/y H:i',
                timepicker: true,
                minDate: 0,
                minTime: g_minTimeForBookingWork,
                maxTime: g_maxTimeForBookingWork,
                onChangeDateTime: logicfrom,
                onShow: logicfrom
            });
        } else {
            $('#txtDateTo').datetimepicker({
                start: toTicksFromDate(appointment.end),
                format: 'd/M/y H:i',
                timepicker: true,
                minDate: 0,
                minTime: g_minTimeForBookingWork,
                maxTime: g_maxTimeForBookingWork,
                onChangeDateTime: logicfrom,
                onShow: logicfrom
            });
        }
    });

}

// function called when the surveyor is changed fro mthe dropdown list
function SurveyorChanged() {
    var ddl = document.getElementById('ddlSurveyors');
    var selectedId = ddl.options[ddl.selectedIndex].value;

    document.getElementById('hfAppSurveyor').value = selectedId;
    $('#pnlCalendar').fullCalendar('refetchEvents');
}

// function to save the appointment details
function ApptSave(iApptID, iJobSiteID) {
    var start = document.getElementById('txtDateFrom').value;
    var end = document.getElementById('txtDateTo').value;
    
    if (start > end) {
        end = start;
    }
    var surveyor = $('#hfAppSurveyor').val();
    var completed = $('#chkAppointmentCompleted').is(':checked');
    var reDownload = document.getElementById('chkDownloadAgain').checked;

    Ajax_CallService(
        'Services/Web/WorkFlow.asmx',
        'SaveQuoteAppointment',
        {
            ApptID: iApptID,
            JobSiteID: iJobSiteID,
            SurveyorID: surveyor,
            StartDateTime: start.toLocaleString(),
            EndDateTime: end.toLocaleString(),
            Completed: completed,
            ReDownload: reDownload,
            TemplateId: templateId
        },
        false);
}

// function to delete the appointment
function ApptDelete(apptId) {

    Ajax_CallService(
        'Services/Web/Calendar.asmx',
        'DeleteQuoteEvent',
        {
            eventId: apptId
        },
        false);
}