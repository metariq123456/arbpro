﻿var SelectedResultId = -1;
var LatinName = '';
TreesList = null;


function Settings_Load() {
    _treesList = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'GetTreeList',
           {
           },
           false
       );
    TreesList = _treesList;

    TreesList.sort(function (a, b) {
        var aval = a.Common.toLowerCase();
        var bval = b.Common.toLowerCase();
        return ((aval < bval) ? -1 : ((aval > bval) ? 1 : 0));
    });

    UpdateTable();
}

function UpdateTable() {
    // 
    var ListPanel = document.getElementById('pnlTableContainer');
    var strHtml = '<table class="searchGrid width-full white">';
    for (var i = 0; i < TreesList.length; i++) {
        if (TreesList[i].Id == SelectedResultId) {
            strHtml += '<tr class="selected" onclick="ItemSelected(\'' + i + '\');">';
        }
        else {
            strHtml += '<tr onclick="ItemSelected(\'' + i + '\');">';
        }
        strHtml += '<td style="padding-left: 10px;">' + TreesList[i].Common + '</td>';
        strHtml += '</tr>';
    }
    strHtml += '</table>';
    ListPanel.innerHTML = strHtml;
}

function ItemSelected(index) {
    var item = TreesList[index];
    SelectedResultId = item.Id;
    LatinName = item.Latin;
    PopulateHiddenFields(item.Common);
    UpdateTable();
}

function PopulateHiddenFields(Value) {
    document.getElementById('btnDelete').className = 'button-thin';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtValue').value = Value;
    document.getElementById('txtLatin').value = LatinName;
}


function NewItem() {
    LatinName = '';
    SelectedResultId = -1;
    UpdateTable();
    document.getElementById('btnDelete').className = 'button-thin hidden';
    document.getElementById('pnlItemControls').className = 'clientField float-left';
    document.getElementById('txtValue').value = '';
    document.getElementById('txtLatin').value = '';
}

function SaveItem() {
    DoSave = Ajax_CallService(
        '../Services/Web/Settings.asmx',
        'SaveTree',
        {
            _id: SelectedResultId,
            _common: document.getElementById('txtValue').value,
            _latin: document.getElementById('txtLatin').value
        },
        false
    );
        
    if (SelectedResultId <= 0) {
        $('#pnlItemControls').addClass("hidden");
    }

    Settings_Load();
}


function DeleteItem() {
    DoDelete = Ajax_CallService(
           '../Services/Web/Settings.asmx',
           'DeleteTree',
           {
               Id: SelectedResultId
           },
           false
       );

    document.getElementById('pnlItemControls').className = 'clientField float-left hidden';
    Settings_Load();
}