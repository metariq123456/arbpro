﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BookJob.aspx.cs" Inherits="DGS.ArbPro.Web.BookJob" %>
<asp:Content ID="TopContent" ContentPlaceHolderID="TopContent" runat="server">
        <script type="text/javascript" src="Content/js/BookJob.js?V=3.23.5"></script>
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="Content" runat="server">
    <asp:Panel ID="Panel2" runat="server" style="position: relative;">
            <asp:Panel runat="server" ID="pnlUserMainForm" CssClass="userMainForm" Width="80%" style="margin: auto;position: absolute;left:0;right: 0;top: 0;bottom: 0;">
                 <asp:Panel ID="pnlItemMapCanvas" runat="server" ClientIDMode="Static" CssClass="white-light width-full center" Height="400">
                          <asp:Image runat="server" ID="imgMapViewer" ClientIDMode="Static" ImageUrl="~/Images/load_Big.gif" style="height: 100%; width: auto"/>
                 </asp:Panel>     
            </asp:Panel>
    </asp:Panel>
    
    <script type="text/javascript" async>
        $(function () {
            LoadWorkJobScreenItems();
        });
    </script>
</asp:Content>
