﻿namespace DGS.ArbPro.Web
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The risk criteria.
    /// </summary>
    public partial class RiskCriteria : System.Web.UI.Page
    {
        /// <summary>
        /// The risk criteria.
        /// </summary>
        private List<Types.Templates.RiskCriterion> criteria = new List<Types.Templates.RiskCriterion>();

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> The event arguments. </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BL.RiskTemplateHandler service = new BL.RiskTemplateHandler(AspSession.UserDatabase);
            this.criteria = service.GetAllCriterionTemplates();
            this.criteria.Sort((x, y) => string.Compare(x.Criterion, y.Criterion));
            this.hfCriteriaTotal.Value = this.criteria.Count.ToString();
        }

        /// <summary>
        /// Creates a list of sections and adds them to a table.
        /// </summary>
        protected void CreateCriteria()
        {
            int curTab = 1;

            // limit the height of the table to 450 pixels to ensure there is space for the details section
            Response.Write(@"<div  style=""height: 500px; overflow: auto; width-full;"">");
            Response.Write(@"<table class=""searchGrid width-full""><col width='68%'></col></col><col width='10%'></col><col width='10%'></col><col width='10%'></col>");
            foreach (Types.Templates.RiskCriterion c in this.criteria)
            {
                if (!c.IsDeleted)
                {
                    Response.Write(string.Format(@"<tr id='tab{0}' class=""activeTab""  onclick=""javascript:Criterion_curTab = {0}; SwitchCriterion();"">{1}", curTab, Environment.NewLine));
                    Response.Write(string.Format("<td>{0}</td>", c.Criterion));
                    Response.Write("<td onclick='NewEditOrCopyCriterion(" + c.Id + ", false);'><a href='#'>Edit</a></td>");
                    Response.Write("<td onclick='NewEditOrCopyCriterion(" + c.Id + ", true);'><a href='#'>Copy</a></td>");
                    Response.Write("<td onclick='CancelCriterion(" + c.Id + ");'><a href='#'>Delete</a></td>");
                    Response.Write(@"</tr>");
                }

                curTab++;
            }

            Response.Write(@"<tr>");
            Response.Write(@"</tr>");
            Response.Write(string.Format(@"</table>"));
            Response.Write("</div>");
            Response.Write(@"<br/>");
        }

        /// <summary>
        /// Creates the section content.
        /// </summary>
        protected void CreateCriterionContent()
        {
            int curTab = 1;
            foreach (Types.Templates.RiskCriterion c in this.criteria)
            {
                if (!c.IsDeleted)
                {
                    Response.Write(string.Format(@"<div id=""lblDetails{0}"" class=""clientTabBar hidden"">{1}", curTab, "Criterion Details For: " + c.Criterion));
                    Response.Write("</div>");
                    Response.Write(string.Format(@"<div id=""pnl{0}"" class=""hidden"">{1}", curTab, Environment.NewLine));
                    Response.Write(string.Format(@"<div id=""grd{0}"" class=""width-full"">{1}", curTab, Environment.NewLine));
                    Response.Write(string.Format(@"<table data-section='" + c.Id + @"' class=""searchGrid width-full""><col width='33%'></col><col width='65%'>{0}<tr><th>Field</th><th>Value</th></tr>{0}", Environment.NewLine));
                    Response.Write(@"<tr>");
                    Response.Write(string.Format("<td> Chapter Reference </td><td><span id='criRefChapter'>{0}</span></td>", c.ChapterRef));
                    Response.Write(@"</tr>");
                    Response.Write(@"<tr>");
                    Response.Write(string.Format("<td> Assessment Phase </td><td><span id='criAssessment'>{0}</span></td>", c.AssessmentPhase));
                    Response.Write(@"</tr>");
                    Response.Write(string.Format(@"</table>"));
                    Response.Write(@"<br/>");
                    Response.Write("</div>");
                    Response.Write("</div>");
                }

                curTab++;
            }
        }
    }
}