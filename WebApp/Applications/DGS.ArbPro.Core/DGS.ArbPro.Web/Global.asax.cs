﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using log4net;
using log4net.Config;

using DGS.ArbPro.Web;
using DGS.ArbPro.DAL;
using DGS.ArbPro.Utility;
using System.Diagnostics;


namespace DGS.ArbPro.Web
{
    public class Global : HttpApplication
    {
        /// <summary>
        /// Called whenever a request has been authorized.
        /// </summary>
        /// <remarks>
        /// We use this to check whether the user has logged in in a different session/browser/computer since this session was logged in.
        /// Note that we can't use the standard HttpContext.Session.SessionId here, as the session has not been created at this point.
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Application_AuthorizeRequest(object sender, EventArgs e)
        {
            HttpCookie UserCookie;
            string RawUrl;
            String DatabaseName = System.Configuration.ConfigurationManager.AppSettings["MasterDatabaseName"];

            HttpApplication context = (HttpApplication)sender;

            // Find the UserGUID cookie
            UserCookie = HttpContext.Current.Request.Cookies["UserGUID"];

            // Check whether the cookie was found - we may be getting here before the user has logged in
            if (UserCookie != null && context.User != null && context.User.Identity.IsAuthenticated)
            {

                // This event is fired for all requests - including asmx requests. To avoid forcing a user off the system in the middle of an update, 
                // and also to reduce the number of DB accesses, we only check when the user is navigating to a new page - so just look for ".aspx" on the end of the request
                RawUrl = context.Request.RawUrl;

                if (RawUrl.Length >= 5)
                {
                    if (RawUrl.Substring(RawUrl.Length - 5, 5).ToLower() == ".aspx")
                    {
                        DAL.User DB = new DAL.User(DatabaseName);
                        var SessionId = DB.GetSessionId(context.User.Identity.Name);

                        // If the session id retrieved is not the same as the cookie's value, then the user has logged in with a different session (in a new browser window, or on another
                        // computer) since this session was created.
                        if (UserCookie.Value != SessionId)
                        {
                            // Add a message to be displayed on the login screen to the UserGUID cookie.
                            UserCookie.Values.Add("MESSAGE", "Another login for your username has been detected. This session has been logged out.");
                            HttpContext.Current.Response.Cookies.Remove("UserGUID");
                            HttpContext.Current.Response.Cookies.Add(UserCookie);

                            Utility.Logger.Log.DebugFormat("User {0} mismatch between session GUID of '{1}' and database GUID of '{2}'", context.User.Identity.Name, UserCookie.Value, SessionId);
                            Utility.Logger.Log.InfoFormat("User {0} subsequent login detected - logging out and returning to login page", context.User.Identity.Name);

                            // Sign the user out and return to the login page.
                            FormsAuthentication.SignOut();
                            FormsAuthentication.RedirectToLoginPage();
                        }
                    }
                }
            }

        }

        void Application_Start(object sender, EventArgs e)
        {
            //BundleConfig.RegisterBundles(BundleTable.Bundles);

            //var registeredBundles = BundleTable.Bundles.GetRegisteredBundles();
            //foreach (var bundle in registeredBundles)
            //{
            //    bundle.Transforms.Clear();
            //}

            CleanTemporaryFileCache();

            RouteConfig.RegisterRoutes(RouteTable.Routes);

            Utility.Logger.Initialise(Server.MapPath("Log4Net.xml"));

            Utility.Logger.Log.Info("Application Started");

            ValidateDocumentDirectory();

            //Context.Cache["LoggedInUsers"] = new Dictionary<String, Types.User>();
        }

        void Application_End(object sender, EventArgs e)
        {
            Utility.Logger.Log.Debug("Application Ending");
            //if (Context != null && Context.Cache != null && Context.Cache["LoggedInUsers"] != null)
            //    Context.Cache["LoggedInUsers"] = new Dictionary<String, Types.User>();
        }

        void Application_Error(object sender, EventArgs e)
        {

        }

        void ValidateDocumentDirectory()
        {
            //This method must be called to ensure the correct setting for the output and generation of documents exists
            try
            {

            }
            catch (Exception)
            {

            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {
            try
            {
                var DB = new DAL.User(System.Configuration.ConfigurationManager.AppSettings["MasterDatabaseName"]);
                if (HttpContext.Current != null)
                {
                    DB.Logout(HttpContext.Current.User.Identity.Name);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine("Error in Session_End: {0}", ex.ToString());
                Utility.Logger.Log.ErrorFormat("Error in Session_End: {0}", ex.ToString());
            }
        }

        /// <summary>
        /// Removes any temporary files left over in the Render and SourcePdf folders
        /// </summary>
        private void CleanTemporaryFileCache()
        {
            var dirs = new[] { Server.MapPath("/Images/Rendered"), Server.MapPath("/Images/SourcePdf") };
            try
            {
                foreach (var dir in dirs)
                {
                    if (!Directory.Exists(dir))
                    {
                        Directory.CreateDirectory(dir);
                    }
                    else
                    {
                        foreach (var file in Directory.GetFiles(dir, "*.*", SearchOption.TopDirectoryOnly))
                        {
                            try
                            {
                                File.Delete(file);
                            }
                            catch (Exception inner)
                            {
                                Utility.Logger.Log.ErrorFormat("Error in GLOBAL::CleanTemporaryFileCache() when trying to remove '{0}': {1}", file, inner.ToString());
                                Debug.Fail("Failed to remove a file!");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Logger.Log.ErrorFormat("Error in GLOBAL::CleanTemporaryFileCache(): {0}", ex.ToString());
                Debug.Fail("Failed in CleanTemporaryFileCache()!");
            }
        }
    }
}
