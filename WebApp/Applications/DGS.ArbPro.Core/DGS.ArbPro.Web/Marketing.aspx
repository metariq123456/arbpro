﻿<%@ Page Title="Marketing" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Marketing.aspx.cs" Inherits="DGS.ArbPro.Web.Marketing" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    
    <script type="text/javascript" src="Content/js/Marketing.js?V=3.23.5"></script>
    
<asp:Panel runat="server" ID="pnlEdit" ClientIDMode="Static" CssClass="jobContent">
    <asp:Panel ID="Panel2" runat="server" CssClass="float-clear width-full">
        <asp:Panel runat="server" ID="pnlMarketingWidth" Width="80%" CssClass="jobTabs align-left" Style="margin-bottom: 50px;">
            <asp:Panel ID="pnlJobWorkflow" runat="server" CssClass="colored-raise jobHeader">
                <asp:Label ID="lblJobWorkflow" runat="server">
                    Marketing
                </asp:Label>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlMarketingDetailsTabs" CssClass="jobTabBar">
                <asp:HyperLink runat="server" ID="tabMarketingStage_1" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchMarketingTab(1);">
                    Return
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabMarketingStage_0" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchMarketingTab(0);">
                    Seasonal
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabMarketingStage_2" ClientIDMode="Static" CssClass="jobTab" NavigateUrl="javascript:SwitchMarketingTab(2);">
                    All Clients
                </asp:HyperLink>
            </asp:Panel>
            
            <asp:Panel runat="server" ID="pnlMarketingContent" CssClass="width-full">
                
                <asp:Panel runat="server" ID="pnlMarketingStage_0" ClientIDMode="Static" CssClass="hidden">
                    <asp:Panel runat="server" ID="pnlSeasonalMarketingDetailsWait" ClientIDMode="Static" CssClass="float-left width-full">
                        <asp:Panel ID="Panel4" runat="server" CssClass="center width-full">
                            <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlSeasonalMarketingDetails" ClientIDMode="Static" CssClass="float-left width-full hidden">
                    </asp:Panel>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlMarketingStage_1" ClientIDMode="Static" CssClass="hidden">
                    
                    <asp:Panel ID="Panel1" runat="server" CssClass="companyTabBar">
                            <asp:HyperLink runat="server" ID="tabActiveMarketing" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:returnMarketingTab = 1; SwitchReturnMarketingTab();">
                                Active
                            </asp:HyperLink>
                            <asp:HyperLink runat="server" ID="tabSentMarketing" ClientIDMode="Static" CssClass="companyTab" NavigateUrl="javascript:returnMarketingTab = 2; SwitchReturnMarketingTab();">
                                Sent
                            </asp:HyperLink>
                        </asp:Panel>
                    <asp:Panel runat="server" ID="pnlReturnMarketingDetails" ClientIDMode="Static" CssClass="float-left">
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlSentReturnMarketingDetails" ClientIDMode="Static" CssClass="float-left">
                    </asp:Panel>
                </asp:Panel>
                
                <asp:Panel runat="server" ID="pnlMarketingStage_2" ClientIDMode="Static" CssClass="hidden">
                    <asp:Panel runat="server" ID="pnlAllClientsMarketingDetailsWait" ClientIDMode="Static" CssClass="float-left width-full">
                        <asp:Panel ID="Panel6" runat="server" CssClass="center width-full">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                        </asp:Panel>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlAllClientsMarketingDetails" ClientIDMode="Static" CssClass="float-left width-full hidden">
                    </asp:Panel>
                </asp:Panel>
               
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Panel>

<script type="text/javascript" async>
    $(function () {
        LoadMarketing(1);
    });
</script>
    
</asp:Content>

