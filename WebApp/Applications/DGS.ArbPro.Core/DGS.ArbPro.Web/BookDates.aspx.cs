﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class BookDates : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int value = -1;

            if (Request["JobId"] != null && int.TryParse(Request["JobId"], out value))
            {
                hfJobID.Value = value.ToString();
            }

            if (Request["RiskID"] != null && int.TryParse(Request["RiskID"], out value))
            {
                hfRiskID.Value = value.ToString();
            }
            else
            {
                hfRiskID.Value = "-1";
            }
            if (Request["QuoteId"] != null && int.TryParse(Request["QuoteId"], out value))
            {
                hfQuoteID.Value = value.ToString();
            }
            else
            {
                hfQuoteID.Value = "-1";
            }
            if (Request["AppointmentID"] != null && int.TryParse(Request["AppointmentID"], out value))
            {
                hfAppId.Value = value.ToString();
            }
            else
            {
                hfAppId.Value = "-1";
            }
        }
    }
}