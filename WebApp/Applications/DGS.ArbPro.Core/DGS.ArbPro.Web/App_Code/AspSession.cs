﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGS.ArbPro.Web
{
    public class AspSession
    {
        public static Types.User CurrentUser
        {
            get
            {
                if (HttpContext.Current.Session["objCurrentUser"] == null)
                {
                    DAL.User DB = new DAL.User("ArbProMaster");
                    var User = DB.GetForEmail(HttpContext.Current.User.Identity.Name);
                    HttpContext.Current.Session["objCurrentUser"] = User;
                    return User;
                }
                else
                {
                    return (Types.User)HttpContext.Current.Session["objCurrentUser"];
                }
            }
            set 
            {
                HttpContext.Current.Session["objCurrentUser"] = value;
            }
        }
    }
}