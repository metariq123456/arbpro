﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.Web.App_Code
{
    public class ArbProMembershipUser : MembershipUser
    {
        #region Members
        public User ArbProUser;
        #endregion

        #region Override Properties
        public override string Comment
        {
            get
            {
                return ArbProUser.Comment;
            }
            set
            {
                ArbProUser.Comment = value;
            }
        }
        public override DateTime CreationDate
        {
            get
            {
                return ArbProUser.CreationDate;
            }
        }
        public override string Email
        {
            get
            {
                return ArbProUser.EmailAddress;
            }
            set
            {
                ArbProUser.EmailAddress = value;
            }
        }
        public override bool IsApproved
        {
            get
            {
                return ArbProUser.UserAccountEnabled;
            }
            set
            {
                ArbProUser.UserAccountEnabled = true;
            }
        }
        public override bool IsLockedOut
        {
            get
            {
                return ArbProUser.AuthenticationFailures >= 3;
            }
        }
        public override bool IsOnline
        {
            get
            {
                return ArbProUser.IsOnline;
            }
        }
        public override DateTime LastActivityDate
        {
            get
            {
                return ArbProUser.LastActivityDate;
            }
            set
            {
                ArbProUser.LastActivityDate = value;
            }
        }
        public override DateTime LastLoginDate
        {
            get
            {
                return ArbProUser.LastLoginDate;
            }
            set
            {
                ArbProUser.LastLoginDate = value;
            }
        }
        #endregion

        #region Constructors
        public ArbProMembershipUser(User ArbProUser)
            : base()
        {
            this.ArbProUser = ArbProUser;
        }
        #endregion

        #region Override Methods
        public override bool ChangePassword(string oldPassword, string newPassword)
        {
            if (ArbProUser.PasswordMatch(oldPassword))
            {
                ArbProUser.PasswordHashed = ArbProUser.HashPassword(newPassword);
            }
            return false;
        }

        public override string ToString()
        {
            return ArbProUser.FirstName + " " + ArbProUser.LastName;
        }

        public override bool UnlockUser()
        {
            ArbProUser.Unlock();
            return true;
        }
        #endregion
    }
}