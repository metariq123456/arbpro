﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using DGS.ArbPro.BL;

namespace DGS.ArbPro.Web.App_Code
{
    public class ArbProMembershipProvider : MembershipProvider
    {
        #region Members

        #endregion

        #region Properties

        public override string ApplicationName { get; set; }

        public override bool EnablePasswordReset
        {
            get { return true; }
        }

        public override bool EnablePasswordRetrieval
        {
            get { return false; }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return 3; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return 1; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return 8; }
        }

        public override int PasswordAttemptWindow
        {
            get { return new TimeSpan(0, 15, 0).Milliseconds; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return MembershipPasswordFormat.Hashed; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { return ""; }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return false; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return true; }
        }
        #endregion

        #region Methods


        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            DAL.User DB = new DAL.User(System.Configuration.ConfigurationManager.AppSettings["MasterDatabaseName"]);
            var ArbProUser = DB.GetForEmail(username);

            if (ArbProUser == null)
                return null;

            ArbProMembershipUser User = new ArbProMembershipUser(ArbProUser);

            if (!userIsOnline || (userIsOnline && User.IsOnline))
                return User;
            else
                return null;
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            return GetUser((string)providerUserKey, userIsOnline);
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            DAL.User DB = new DAL.User(System.Configuration.ConfigurationManager.AppSettings["MasterDatabaseName"]);
            var ArbProUser = DB.GetForEmail(username);

            if (ArbProUser == null)
                return false;

            if (newPassword.Length < MinRequiredPasswordLength)
                return false;

            if (ArbProUser.PasswordMatch(oldPassword))
            {
                ArbProUser.PasswordHashed = ArbProUser.HashPassword(newPassword,true);
                
                DB.Save(ArbProUser);
                return true;
            }
            else
            {
                return false;
            }
        }

        public override bool UnlockUser(string userName)
        {
            DAL.User DB = new DAL.User(System.Configuration.ConfigurationManager.AppSettings["MasterDatabaseName"]);
            var ArbProUser = DB.GetForEmail(userName);

            if (ArbProUser == null)
                return false;

            ArbProUser.Unlock();
            DB.Save(ArbProUser);

            return true;
        }

        public override bool ValidateUser(string username, string password)
        {
            HttpCookie MyCookie;
            Guid UserGuid;
            DAL.User DB = new DAL.User(System.Configuration.ConfigurationManager.AppSettings["MasterDatabaseName"]);
            var ArbProUser = DB.GetForEmail(username.ToLower());

            if (ArbProUser == null)
            {
                Utility.Logger.Log.InfoFormat("No user found for {0}", username);
                return false;
            }

            // Check the number of authentication failures and if this user is allowed to log into the web application
            if (ArbProUser.Locked || ArbProUser.IsWebEnabled == false)
            {
                Utility.Logger.Log.InfoFormat("User {0} locked or not web enabled. Locked={1} WebEnabled={2}", ArbProUser.EmailAddress, ArbProUser.Locked, ArbProUser.IsWebEnabled);
                return false;
            }

            // If the password matches
            if (ArbProUser.PasswordMatch(password))
            {
                // Create a new GUID for the user's session
                UserGuid = Guid.NewGuid();

                // Create a specific cookie to hold the guid, and add it to the response.
                MyCookie = new HttpCookie("UserGUID",UserGuid.ToString());
                HttpContext.Current.Response.Cookies.Remove("UserGUID");
                HttpContext.Current.Response.Cookies.Add(MyCookie);

                // Record the user as being logged in with this GUID.
                DB.Login(username.ToLower(), UserGuid.ToString());
                return true;
            }
            else
            {
                Utility.Logger.Log.InfoFormat("User {0} - password failure", ArbProUser.EmailAddress);
                DB.Save(ArbProUser);
                return false;
            }
        }
        #endregion

        #region Unused
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }
        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }
        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }
        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }
        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }
        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }
        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
