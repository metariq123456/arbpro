﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TermsConditions.aspx.cs" Inherits="DGS.ArbPro.Web.Account.TermsConditions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" ScrollBars="Auto">
            <asp:Panel runat="server" CssClass="termsHeader">
                If this dialog has been displayed automatically, you may be using ArbPro for the first time, or the Terms of Use may have changed since you last logged in.<br />
                <br />
                <span class="termsImportant">Please carefully read the following Terms of Use and accept them before proceeding.                
                </span>
            </asp:Panel>
            <asp:Panel runat="server" CssClass="termsContent" Width="500px" ScrollBars="None">
                <h1>TERMS OF USE – Arb Pro </h1>
                <p>
                    Welcome to ArbPro, an online Customer Relationship Management application designed for businesses, cooperates and organisations. 
                    Please read these terms of use (the “<b>Terms</b>”) carefully before using the Application and the Service as they form a legally binding
                     contract between us. These Terms apply to the entire content of the application at <a href="http://cloud.arbprosoftware.com/" target="_blank">http://cloud.arbprosoftware.com/</a> (the “<b>Application</b>”),
                     the use by you of the ArbPro CRM services provided through the Application (the “Service”) and any correspondence between us. If you
                     use the Application then you indicate that you accept these Terms regardless of whether or not you choose to register to use the Service.
                    If you do not accept these Terms, then please do not use the Application or the Service. These Terms are issued by Arb Pro Software Ltd,
                     a private limited company registered in England with number 08753138 whose registered office is 
                    13 Hobart Drive, Hythe, Southampton, SO45 6FN, UK (the “<b>Company</b>” and “<b>we</b>”). Our VAT number is 130950535
                </p>
                <p>
                    <b>By ticking the accept box and closing this dialog you agree to these Terms which will bind you. 
                        If you do not agree to these Terms, then we are unwilling to allow you access to the Application and/or the Service, 
                        in which case please do not access and/or use the Application and/or the Service.
                    </b>
                </p>
                <h2>1. INTRODUCTION</h2>
                <p>
                    <b>1.1.</b> In consideration of the payment by you of the then current fees specified at <a href="http://www.arbprosoftware.com/" target="_blank">http://www.arbprosoftware.com/</a> (where applicable) and
                        you agreeing to abide by these Terms, we grant you access to use the Application and the Service on the terms set out in this document.
                </p>
                <p>
                    <b>1.2.</b> By accessing any part of the Application or the Service, you shall be deemed to have accepted the Terms in full which shall take effect
                        immediately on your first use of the Application or Service. If you do not accept the Terms in full, you must leave the Application
                        immediately.
                </p>
                <p>
                    <b>1.3.</b> The Company may revise these Terms at any time by updating this posting. Please check the Application from time to time to review
                        the then current Terms, because they are binding on you. They are also available at <a href="http://www.arbprosoftware.com/" target="_blank">http://www.arbprosoftware.com/</a>.
                </p>
                <p>
                    <b>1.4.</b> Any amendments, modifications, enhancements or changes to the Service made available by the Company from time to time shall be
                        subject to these Terms.
                </p>
                <p>
                    <b>1.5.</b> If you are 12 years old or younger you may not register with us in this Application. By entering your details in the Application you are
                        stating you are 13 years old or older.
                </p>
                <h2>2. RIGHTS GRANTED</h2>
                <p>
                    <b>2.1.</b> You are permitted to use the Service for your own internal business purposes or for your own personal use on the following basis: You have provided your legal full name, a valid email address, and any other information requested in order to complete the sign‐up process.
                </p>
                <p>
                    <b>2.2.</b> Subject to the provisions of clause 2.3, all copyright and other intellectual property rights in the Service and material in the Application (including without limitation photographs and graphical images) are owned by the Company or its licensors. Any use of extracts from the Application for any purpose is prohibited. 
                </p>
                <p>
                    <b>2.3.</b> All copyright and intellectual property rights in any information uploaded by you or your employees to the Application in connection with the Service shall remain vested in you, your employees or your licensors. 
                </p>
                <p>
                    <b>2.4.</b> No part of the Application or Service may be reproduced or stored in any other application or included in any public or private electronic retrieval system or service without the Company's prior written permission. 
                </p>
                <p>
                    <b>2.5.</b> Any rights not expressly granted in these Terms are reserved.
                </p>
                <h2>3. SERVICE ACCESS AND TECHNICAL SUPPORT</h2>
                <p>
                    <b>3.1.</b> Whilst the Company endeavours to ensure that the Application and Service is normally available 24 hours a day, the Company shall not be liable if for any reason the Application or Service is unavailable at any time or for any period. 
                </p>
                <p>
                    <b>3.2.</b> Access to the Application and the Service may be suspended temporarily and without notice in the case of system failure, maintenance or repair or for reasons beyond the Company's control. 
                </p>
                <p>
                    <b>3.3.</b>The Company will provide technical support to then current paying subscribers to the Service by email only. Technical support will only be provided for bugs or errors in the Application or Service that are reproducible by the Company. You agree to provide the Company with full and accurate details of all bugs and errors in the Application or Service as requested by the Company. You acknowledge that the Company provides no warranty that all or any bugs or errors in the Service will be corrected. 
                </p>
                <p>
                    <b>3.4.</b> Our Responsibilities. We shall:<br />
                    (a)	Provide a basic level of support for the purchased Services to you at no additional charge, and/or upgraded support if purchased separately.<br />
                    (b)	Use commercially reasonable efforts to make the purchased Services available 24 hours a day, 7 days a week, except for:<br />
                    (i) Planned downtime, of which we shall give at least 8 hours’ notice via the Application, and which   we shall schedule to the extent practicable during the weekend hours: From 6:00 p.m. Friday to 3:00 a.m. Monday GMT / UK Time.<br />
                    (ii) Any unavailability caused by circumstances beyond our reasonable control, including without limitation, acts of God, acts of government, floods, fires, earthquakes, civil unrest, acts of terror, strikes or other labour problems (other than those involving our employees), Internet Service Provider failures or delays, or denial of service attacks.<br />
                    (c) Provide the purchased Services only in accordance with applicable laws and government regulations.<br />
                </p>
                <p>
                    <b>3.5.</b> Our Protection of Your Data. We shall maintain appropriate administrative, physical, and technical safeguards for protection of the
                        security, confidentiality and integrity of your data. We shall not:<br />
                    (a) Modify your data.<br />
                    (b) Disclose you data except as compelled by law or as expressly permitted in writing by you.<br />
                    (c) Access your data except to provide the Services and prevent or address service or technical problems, or at your request in connection
                        with customer support matters.<br />
                    (d) In order to be able to test new releases of the software – and the any associated data upgrade procedures – we may occasionally need to use representative live data. This representative data may be based on your data, but will be obfuscated so that it is not possible to identify any personal data within it. Specifically names, addresses, email addresses and telephone numbers will be obfuscated so as not to be identifiable. Copies of our “Data Handling” procedures are available on request.
                </p>
                <p>
                    <b>3.6.</b> Your Responsibilities.<br />
                    (a) You shall:<br />
                    (i) Be responsible for compliance with these Terms.<br />
                    (ii) Be responsible for the accuracy, quality and legality of your data and of the means by which you acquired your data.<br />
                    (iii) Use commercially reasonable efforts to prevent unauthorized access to the Application or use of the Services, and notify us promptly
                        of any such unauthorized access or use.<br />
                    (iv) Use the Application and Services only in accordance with the User Guide and applicable laws and government regulations.<br />
                    (b) You shall not:<br />
                    (i) Make the Application or Services available to anyone other than yourself.<br />
                    (ii) Sell, resell, rent or lease the Application or Services.<br />
                    (iii) Use the Application or Services to store or transmit infringing, libellous, or otherwise unlawful or tortious material, or to store or
                        transmit material in violation of third‐party privacy rights.<br />
                    (iv) Use the Application or Services to store or transmit Malicious Code.<br />
                    (v) Interfere with or disrupt the integrity or performance of the Application or Services or third‐party data contained therein.<br />
                    (vi) Attempt to gain unauthorized access to the Application or Services or their related systems or networks.
                </p>
                <h2>4. ACCEPTABLE CONDUCT</h2>
                <p>
                    <b>4.1.</b> You are prohibited from posting or transmitting to or from the Application and/or the Service any material:<br />
                    (a) That is threatening, defamatory, obscene, indecent, seditious, offensive, pornographic, abusive, liable to incite racial hatred,
                        discriminatory, menacing, scandalous, inflammatory, blasphemous, in breach of confidence, in breach of privacy or which may cause
                        annoyance or inconvenience.<br />
                    (b) For which you have not obtained all necessary licences, consents and/or approvals.<br />
                    (c) Which constitutes or encourages conduct that would be considered a criminal offence, give rise to civil liability, or otherwise be
                        contrary to the law of or infringe the rights of any third party, in any country in the world.<br />
                    (d) Which is technically harmful (including, without limitation, computer viruses, logic bombs, Trojan horses, worms, harmful components,
                        corrupted data or other malicious software or harmful data).
                </p>
                <p>
                    <b>4.2.</b> You may not use the Application or the Service:<br />
                    (a) In any way that breaches any applicable local, national or international law or regulation.<br />
                    (b) In any way that is unlawful or fraudulent, or has any unlawful or fraudulent purpose or effect.<br />
                    (c) For the purpose of harming or attempting to harm minors in any way.<br />
                    (d) To transmit, or procure the sending of, any unsolicited or unauthorised advertising or promotional material or any other form of similar
                        solicitation (spam).
                </p>
                <p>
                    <b>4.3.</b> The Company shall fully co‐operate with any law enforcement authorities or court order requesting or directing the Company to
                        disclose the identity or locate anyone posting any material in breach of clauses 4.1 or 4.2.
                </p>
                <h2>5. REGISTRATION AND PAYMENT</h2>
                <p>
                    <b>5.1.</b> Each Application login account (“Account”) is for single seat use (i.e. an individual person).<br />
                    <br />
                    <span class="termsImportant">The Company does not permit you to share your Account login details (email address and/or password combination) with any
                            other person nor with multiple people/users on a network. We accept no responsibility for corruption or cross‐contamination of
                            data that may occur whilst multiple people/users are simultaneously logged into a single Account.<br />
                        <br />
                    </span>
                    <span class="termsImportant">Two or more people/users logged into the same Account simultaneously may contaminate
                            the system with duplicate numbering.
                    </span>
                </p>
                <p>
                    <b>5.2.</b> Responsibility for the security of Account login details (email addresses and passwords) provided by you, or issued by the Application
                        or Company, rests with you.
                </p>
                <p>
                    <b>5.3.</b> You agree to provide us with a valid credit card number or bank account details and authorise us to deduct from such card or account
                        payment of the then current monthly fees. The fees are posted on our website and we may vary these from time to time. Please see our
                        Pricing Page for the rates in force for the current month. The credit card or bank details must be registered in either your name or that of
                        your organisation. Credit card and bank payments are subject to validation and authorisation checks of the card issuer or bank.
                </p>
                <p>
                    <b>5.4.</b> Accounts are billed monthly in advance on or around the 30th day after your Account was created. Payments are non‐refundable,
                        and no refunds or credits will be given for any partial use within any month.
                </p>
                <p>
                    <b>5.5.</b> If you sign up for an Account you will be billed monthly for your ongoing use of the Application and Service on the 30th day following
                        the date on which you initially signed up for your Account. If you choose to cancel your Account we must receive written confirmation in
                        the form of an email to <a href="mailto:support@arbprosoftware.com">support@arbprosoftware.com</a> or in writing to our registered address 13 Hobart Drive, Hythe, Southampton, SO45
                        6FN. We require a full months’ notice of Account cancelations.
                </p>
                <p>
                    <b>5.6.</b> All fees are exclusive of all taxes, charges, levies, assessments and other fees of any kind imposed on your use of the Application and
                        Service and shall be the responsibility of, and payable by, you. If your place of business is within the EU (excluding the UK) and you
                        provide us with a valid VAT registration number then we will not charge you VAT. In all other circumstances if you reside or have your
                        place of business within the EU then we will add UK VAT to our fees at the then current rate.
                </p>
                <p>
                    <b>5.7.</b> We reserve the right to suspend your Account immediately if you fail to provide us with valid credit card or bank details that enable us
                        to charge the full amount of any outstanding fees and charges within 30 days of the due date. We will provide prior notice of our
                        intention to suspend your Account by email to your then registered email address. If no payment is made to clear the full amount of any
                        outstanding fees and charges within a further 60 days your Account and all associated data will be deleted and the agreement between us
                        set out in these Terms shall be automatically terminated.
                </p>
                <h2>6. DISCLAIMER</h2>
                <p>
                    <b>6.1.</b> While the Company endeavours to ensure that the information provided in the Application and the information provided in
                        connection with the Service is correct, the Company does not warrant the accuracy and completeness of such material. The Company may
                        make changes to the material in the Application or to the Service, or to the products and prices described in the Application, at any time
                        without notice. The material in the Application may be out of date, and the Company makes no commitment to update such material.
                </p>
                <p>
                    <b>6.2.</b> The material in the Application and the Service is provided "as is", without any conditions, warranties or other terms of any kind.
                        Accordingly, to the maximum extent permitted by law, the Company provides you with access to the Application and the Service on the
                        basis that the Company excludes all representations, warranties, conditions and other terms (including, without limitation, the conditions
                        implied by law of satisfactory quality, fitness for purpose and the use of reasonable care and skill) which, but for this legal notice, might
                        have effect in relation to the Application or the Service.
                </p>
                <p>
                    <b>6.3.</b> You acknowledge that:<br />
                    (a) The Service has not been developed to meet your individual requirements, and that it is therefore your responsibility to ensure that the
                        facilities and functions of the Service as described in the Application meet your requirements.<br />
                    (b) It is not possible to test the Application or Service in advance in every possible operating combination and environment.<br />
                    (c) It is not possible to produce an Application or Service known to be error‐free in all circumstances.
                </p>
                <h2>7. LIABILITY</h2>
                <p>
                    <b>7.1.</b> The Company, any other party (whether or not involved in creating, producing, maintaining or delivering the Application or Service),
                        and any of the Company's officers, directors, employees, shareholders or agents of any of them, exclude all liability and responsibility for
                        any amount or kind of loss or damage that may result to you or a third party (including without limitation, any direct, indirect, punitive or
                        consequential loss or damages, or any loss of income, profits, goodwill, data, contracts, use of money, or loss or damages arising from or
                        connected in any way to business interruption, and whether in tort (including without limitation negligence), contract or otherwise) in
                        connection with the Application or Service in any way or in connection with the use, inability to use or the results of use of the Application
                        or Service, any websites linked to the Application or the material on such websites, including but not limited to loss or damage due to
                        viruses that may infect your computer equipment, software, data or other property on account of your access to, use of, or browsing the
                        Application or Service or your downloading of any material from the Application, the Service or any websites linked to the Application or
                        Service.
                </p>
                <p>
                    <b>7.2.</b> Nothing in this legal notice shall exclude or limit the Company's liability for:<br />
                    (a) Death or personal injury caused by negligence (as such term is defined by the Unfair Contract Terms Act 1977.<br />
                    (b) Fraud.<br />
                    (c) Misrepresentation as to a fundamental matter.<br />
                    (d) Any liability which cannot be excluded or limited under applicable law.
                </p>
                <p>
                    <b>7.3.</b> If your use of material in the Application or the Service results in the need for servicing, repair or correction of equipment, software or
                        data, you assume all costs thereof.
                </p>
                <p>
                    <b>7.4.</b> Subject to clauses 7.1 and 7.2, the Company’s maximum aggregate liability under or in connection with these Terms, or any collateral
                        contract, whether in contract, tort (including negligence) or otherwise (a “<b>Claim</b>”), shall be limited to a sum equal to the aggregate amount
                        which you are obliged to pay the Company in the twelve (12) month period immediately prior to the period giving rise to such Claim.
                </p>
                <h2>8. TERMINATION</h2>
                <p>
                    <b>8.1.</b> The Company may terminate this agreement and cancel your Account immediately by written notice to you if:<br />
                    (a) You commit a material or persistent breach of the Terms which you fail to remedy (if remediable) within 14 days after the service of
                        written notice requiring you to do so.<br />
                    (b) A petition for a bankruptcy order to be made against you has been presented to the court.<br />
                    (c) You (being a company) becomes insolvent or unable to pay its debts (within the meaning of section 123 of the Insolvency Act 1986),
                        enters into liquidation, whether voluntary or compulsory (other than for reasons of bona fide amalgamation or reconstruction), passes a
                        resolution for its winding‐up, has a receiver or administrator manager, trustee, liquidator or similar officer appointed over the whole or
                        any part of its assets, makes any composition or arrangement with its creditors or takes or suffers any similar action in consequence of its
                        debt.
                </p>
                <p>
                    <b>8.2.</b> If you choose to cancel your Account we must receive written confirmation in the form of an email to <a href="mailto:support@arbprosoftware.com">support@arbprosoftware.com</a>
                    or in writing to our office address: Hotspur House, Prospect Place, Hythe, Southampton, SO45 6AU. We require a full months’ notice of Account
                        cancelations.
                </p>
                <p>
                    <b>8.3.</b> Upon termination for any reason:<br />
                    (a) All rights granted to you under these Terms shall cease.<br />
                    (b) You must cease all activities authorised by these Terms.<br />
                    (c) You must immediately pay to the Company any sums due to the Company under these Terms.<br />
                    (d) You will not be entitled to any refund or credit in respect of any fee paid by you in advance for any cancelled Service.<br />
                    (e) The Company may immediately and without further notice delete or remove any content, data or other information submitted by you
                        to the Service.
                </p>
                <h2>9. TRANSFER OF RIGHTS AND OBLIGATIONS</h2>
                <p>
                    <b>9.1.</b> These Terms are binding on you and us, and on our respective successors and assigns.
                </p>
                <p>
                    <b>9.2.</b> You may not transfer, assign, change or otherwise dispose of these Terms or any of your rights or obligations arising hereunder,
                        without our prior written consent.
                </p>
                <p>
                    <b>9.3.</b> We may transfer, assign, change, sub‐contract or otherwise dispose of these Terms, or any of our rights or obligations arising
                        hereunder, at any time.
                </p>
                <h2>10. NOTICES</h2>
                <p>
                    <b>10.1.</b> All notices given by you to us must be given to Arb Pro Software Ltd by email to <a href="mailto:support@arbprosoftware.com">support@arbprosoftware.com</a> or sent in the Post to 
                        Hotspur House, Prospect Place, Hythe, Southampton, SO45 6AU. Notice will be deemed received and properly served immediately when we
                        receive either an email or written notification. In proving the service of any notice, it will be sufficient to prove, in the case of a letter, that
                        such letter was properly addressed, stamped and placed in the post and, in the case of an email, that such email was sent to the specified
                        email address.
                </p>
                <h2>11. EVENTS OUTSIDE OUR CONTROL</h2>
                <p>
                    <b>11.1.</b> We will not be liable or responsible for any failure to perform, or delay in performance of, any of our obligations hereunder that is
                        caused by events outside our reasonable control (a “<b>Force Majeure Event</b>”).
                </p>
                <p>
                    <b>11.2.</b> A Force Majeure Event includes any act, event, non‐happening, omission or accident beyond our reasonable control and includes in
                        particular (without limitation) the following:<br />
                    (a) Strikes, lock‐outs or other industrial action.<br />
                    (b) Civil commotion, riot, invasion, terrorist attack or threat of terrorist attack, war (whether declared or not) or threat or preparation for
                        war.<br />
                    (c) Fire, explosion, storm, flood, earthquake, subsidence, epidemic or other natural disaster.<br />
                    (d) Impossibility of the use of public or private telecommunications networks.<br />
                    (e) The acts, decrees, legislation, regulations or restrictions of any government.
                </p>
                <p>
                    <b>11.3.</b> Our performance is deemed to be suspended for the period that the Force Majeure Event continues, and we will have an extension
                        of time for performance for the duration of that period. We will use our reasonable endeavours to bring the Force Majeure Event to a
                        close or to find a solution by which our obligations under these Terms may be performed despite the Force Majeure Event.
                </p>
                <h2>12. WAIVER</h2>
                <p>
                    <b>12.1.</b> If we fail, at any time to insist upon strict performance of any of your obligations under these Terms, or if we fail to exercise any of
                        the rights or remedies to which we are entitled hereunder, this shall not constitute a waiver of such rights or remedies and shall not
                        relieve you from compliance with such obligations.
                </p>
                <p>
                    <b>12.2.</b> A waiver by us of any default shall not constitute a waiver of any subsequent default.
                </p>
                <p>
                    <b>12.3.</b> No waiver by us of any of these Terms shall be effective unless it is expressly stated to be a waiver and is communicated to you in
                        writing.
                </p>
                <h2>13. SEVERABILITY</h2>
                <p>
                    <b>13.1.</b> If any of these Terms are determined by any competent authority to be invalid, unlawful or unenforceable to any extent, such term,
                        condition or provision will to that extent be severed from the remaining terms, conditions and provisions which will continue to be valid to
                        the fullest extent permitted by law.
                </p>
                <h2>14. ENTIRE AGREEMENT</h2>
                <p>
                    <b>14.1.</b> These Terms and any document expressly referred to in it represents the entire agreement between us in relation to the use of the
                        Application and the provision of the Services and supersedes any prior agreement, understanding or arrangement between us, whether
                        oral or in writing.
                </p>
                <p>
                    <b>14.2.</b> We each acknowledge that, in entering into these Terms, neither of us has relied on any representation, undertaking or promise
                        given by the other or be implied from anything said or written in negotiations between us prior to entering into these Terms except as
                        expressly stated herein.
                </p>
                <p>
                    <b>14.3.</b> Neither of us shall have any remedy in respect of any untrue statement made by the other, whether orally or in writing, prior to the
                        date we entered into these Terms (unless such untrue statement was made fraudulently) and the other party's only remedy shall be for
                        breach of contract as provided in these Terms.
                </p>
                <h2>15. GOVERNING LAW AND JURISDICTION</h2>
                <p>
                    <b>15.1.</b> This legal notice shall be governed by and construed in accordance with English law. Disputes arising in connection with this legal
                        notice shall be subject to the exclusive jurisdiction of the English Courts.
                </p>
                <h2>16. Data Protection</h2>
                <p>
                    <b>16.1.</b> Arb Pro takes the protection of your data seriously and ensures the data you enter is never made available to third parties. The data is owned by the licensed user and or company that originally signed up to Arb Pro.
                </p>
                <p>
                    <b>16.2</b> Arb Pro may on occasion use real data for the purpose of development testing. Real data will only be used as obfuscated data. Obfuscated data is data taken from an account to test new development, but changed so that personal information is altered to become un-meaningful or un-recognisable as personal data.<br />
                    <br />
                    For example:<br />
                    Mr Builder Builders (Business name) owned by Paul Powlo (Client name) will be obfuscated to read:<br />
                    Company name will be:  “Bus1” & Client name will be: “MrCust1”    <br />  
                    <br />
                    However other useful data will be retained and allow system developers to test new development against meaningful data without any data protection issues. <br />
                </p>
                <h5>Our data handling procedures can be provided on request.</h5><br />
                <p>
                    <b>16.3</b> How secure is the personal data I store on Arb Pro?<br /> 
                    All information which passes between our servers and your computer is encrypted using technology called Secure Sockets Layer (SSL). Click on the padlock symbol to view our data encryption certificate from our web site when you are at the login screen or using the product.   
                </p>
                <asp:Panel ID="pnlPadlockImage" runat="server" CssClass="width-full">
                    <asp:Image ID="imgPadLock" runat="server" ImageUrl="~/Images/SSLPadLock.png" CssClass="width-full padded" />
                    <p>Note: The image displayed is an example from Googles Chrome browser.</p>
                </asp:Panel>
                <p>
                    An SSL Certificate is like a digital passport that confirms the holder's credentials for conducting business on the Internet. When Web users send information such as their names, addresses and credit card numbers to a website secured with an SSL Certificate, the user's browser validates the recipient's digital certificate before establishing an encrypted connection. This process protects information from outside viewing as it flows both to and from the certificate holder's website.<br />
                </p>
                <p>
                    Arb Pro use GoDaddy to provide their SSL protection. GoDaddy's business controls and practices have been thoroughly reviewed by an independent accountant to ensure they conform to the international AICPA/CICA WebTrust for Certification Authorities Principles and Criteria.
                </p>
                <p>
                    <b>16.4</b> Who has access to your dataWho has access to your data <br />
                    No-one has access to your account unless you invite them. Each user you invite has a username and password for signing in.<br />
                    It is important for you to protect against unauthorized access to your password and to your computer. Be sure to sign off when finished using a shared computer by clicking <b>Account > Log</b> out on the top right hand side of the Arb Pro window.<br />
                    You may also choose to share your account data with other applications. You should apply the same assessment to any application or 3rd party which you grant access to in this way.
                </p>
                <p>
                    <b>16.5</b> Where are the servers and the personal data stored hosted?  <br />
                   Our servers and the personal data you store are hosted on <a href="http://www.rackspace.co.uk/about-us/security">Rack Space</a> data centers in the United Kingdom. Rack space have stringent data management policies and procedures and these can be viewed following this link <a href="http://www.rackspace.co.uk/about-us/security">Rack Space</a> 
                </p>

            </asp:Panel>
            <asp:Panel runat="server" CssClass="termsFooter">
                <asp:CheckBox runat="server" ID="chkAcceptTsAndCs" Text="I accept the Terms of Use" />
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
