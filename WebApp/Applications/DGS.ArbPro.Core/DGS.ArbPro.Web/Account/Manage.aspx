﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="DGS.ArbPro.Web.Account.Manage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="content" style="padding-left: 10px;">
            <asp:Panel runat="server" Width="300px" CssClass="center">
                <asp:Button runat="server"
                    ID="btnLogout"
                    Text="Log Out"
                    CssClass="button-large"
                    OnClientClick="LogOut(); return false;" />
            </asp:Panel>
            <asp:Panel runat="server" Width="345px">
                <b>Secure Password Policy</b>
                <p>To help maintain a high level of security your new password must:</p>
                <ul>
                    <li>Be at least 8 characters long.</li>
                    <li>Contain at least 1 uppercase character.</li>
                    <li>Contain at least 1 lowercase character.</li>
                    <li>Contain at least 1 numeric character.</li>
                </ul>
            </asp:Panel>
            <asp:Panel runat="server" Width="300px" CssClass="center">
                <asp:TextBox runat="server"
                    ID="txtOldPassword"
                    CssClass="txtLogin width-full"
                    placeholder="Old Password"
                    TextMode="Password"></asp:TextBox>
                <asp:TextBox runat="server"
                    ID="txtConfirmPassword"
                    CssClass="txtLogin width-full"
                    placeholder="Confirm Old Password"
                    TextMode="Password"></asp:TextBox>
                <asp:TextBox runat="server"
                    ID="txtNewPassword"
                    CssClass="txtLogin width-full"
                    placeholder="New Password"
                    TextMode="Password"></asp:TextBox>
                <asp:Button runat="server"
                    ID="btnChangePassword"
                    Text="Change Password"
                    CssClass="button-basic"
                    OnClientClick="if (ChangePassword()) 
                        { 
                            document.getElementById('lblError').className = 'errLogin hidden';
                            document.getElementById('lblSuccess').className = 'succLogin';
                        } else { 
                            document.getElementById('lblError').className = 'errLogin'; 
                            document.getElementById('lblSuccess').className = 'succLogin hidden'; 
                    }; return false;" />
                <br />
            </asp:Panel>
            <asp:Panel runat="server" Width="300px" CssClass="center">
                <asp:Label runat="server"
                    ID="lblError"
                    Font-Size="Small"
                    CssClass="errLogin hidden">
                        An error occurred changing your password.<br />Please try again.
                </asp:Label>
                <asp:Label runat="server"
                    ID="lblSuccess"
                    Font-Size="Small"
                    CssClass="succLogin hidden">
                        Success.
                </asp:Label>
            </asp:Panel>
        </asp:Panel>
    </form>
</body>
</html>
