﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Login.aspx.cs" company="Microsec Ltd">
//   Copyright 2015 Microsec Ltd
// </copyright>
// <summary>
//   Controls the login process for the application
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace DGS.ArbPro.Web.Account
{
    using System;
    using System.Web;
    using System.Web.UI.WebControls;

    /// <summary>
    /// Controls the login process for the application
    /// </summary>
    public partial class Login : System.Web.UI.Page
    {
        #region Methods

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            HttpCookie userCookie;
            string message;

            // Retrieve the cookie which holds the user's GUID.
            // IF there is a message in the cookie, then display it in the failure text control.
            userCookie = this.Request.Cookies["UserGUID"];
            if (userCookie == null)
            {
                return;
            }

            message = userCookie.Values["MESSAGE"] ?? string.Empty;
            if (message == string.Empty)
            {
                return;
            }

            Literal FailureText = (Literal)this.loginMain.FindControl("FailureText");
                    
            FailureText.Text = message;
        }

     
        #endregion

        protected void BtnForgottenPasswordClick(object sender, EventArgs e)
        {

        }
    }
}