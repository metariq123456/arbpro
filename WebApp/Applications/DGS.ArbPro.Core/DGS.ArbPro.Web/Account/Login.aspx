﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="DGS.ArbPro.Web.Account.Login" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <asp:Panel runat="server" ID="pnlLogin" CssClass="LoginPane white">
        <asp:Panel runat="server"
            CssClass="colored width-full curved-top"
            Style="padding-top: 5px; padding-bottom: 3px; text-align: left;">
            <asp:Label runat="server" CssClass="lblTitle">
                Login
            </asp:Label>
        </asp:Panel>
        <asp:Login runat="server" Width="100%" ID="loginMain">
            <LayoutTemplate>
                <asp:Panel runat="server" CssClass="width-full" Style="margin-top: 40px">
                    <asp:TextBox runat="server"
                        ID="UserName"
                        CssClass="txtLogin"
                        placeholder="Email Address" />
                    <asp:RequiredFieldValidator runat="server"
                        ID="rfvUserNameLogin"
                        ControlToValidate="UserName"
                        ValidationGroup="Login"
                        CssClass="errLogin"
                        ErrorMessage="*" />
                                        <asp:RequiredFieldValidator runat="server"
                        ID="rfvUserNameForgotten"
                        ControlToValidate="UserName"
                        ValidationGroup="Forgotten"
                        CssClass="errLogin"
                        ErrorMessage="*" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="width-full">
                    <asp:TextBox runat="server"
                        ID="Password"
                        CssClass="txtLogin"
                        TextMode="Password"
                        placeholder="Password" />
                    <asp:RequiredFieldValidator runat="server"
                       ID="rfvPassword"
                        ControlToValidate="Password"
                        CssClass="errLogin"
                        ErrorMessage="*"
                        ValidationGroup="Login" />
                </asp:Panel>
                <asp:Panel runat="server"
                    Style="text-align: center;"
                    CssClass="errLogin">
                    <asp:Literal runat="server"
                        ID="FailureText" />
                </asp:Panel>
                <asp:Panel runat="server" Style="text-align: center;">
                    <%--<asp:CheckBox runat="server" ID="RememberMe" Text="Remember Me" />--%>
                    <%--<asp:Button runat="server" CommandName="Login" ID="btnLogin" ValidationGroup="Login" CssClass="button-basic" Text="Log In" Style="margin-left: 25px;" />--%>
                    <asp:Button runat="server" CommandName="Login" ID="Button1" ValidationGroup="Login" CssClass="button-basic" Text="Log In" Style="width: 50%;" />
                </asp:Panel>
                <asp:Panel runat="server" Style="text-align: center;">
                    <Button onclick="BtnForgottenPasswordClick();" id="btnForgottenPassword" class="button-thin" Style="margin-top: 15px; width: 50%;">Forgotten password?</Button>
                </asp:Panel>
            </LayoutTemplate>
        </asp:Login>
    </asp:Panel>
    <script>
        function BtnForgottenPasswordClick() {
            var userNameValue = $("input:text[id*=UserName]").val();

            if (userNameValue == null) {
                alert("Please enter your email address.");
            } else {

                var resetResult = Ajax_CallService(
                    'Services/web/Account.asmx',
                    'SendPasswordResetEmail',
                    {
                        userName: userNameValue,
                    },
                    false);

                alert(resetResult);
            }

        }
    </script>
</asp:Content>
