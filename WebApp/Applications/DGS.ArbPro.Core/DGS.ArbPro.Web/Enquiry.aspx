﻿<%@ Page Title="Enquiry" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Enquiry.aspx.cs" Inherits="DGS.ArbPro.Web.Enquiry" %>

<asp:Content runat="server" ID="TopContent" ContentPlaceHolderID="TopContent">
    <script type="text/javascript" src="Content/js/Enquiry.js?V=3.25.0"></script>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <asp:Panel runat="server" CssClass="center">
        <asp:Panel runat="server" ID="pnlEnquiryTypeButtons" CssClass="enquiryButtons">
            <asp:Label runat="server" CssClass="enquiryHeader">New Enquiry</asp:Label>
            <br />
            <asp:Button runat="server" ID="btnSales" ClientIDMode="Static" CssClass="button-large" Width="200px" Text="Sales" OnClick="btnSales_Click" />
            <%--<asp:Button runat="server" CssClass="button-large" Width="200px" Text="General" />--%>
            <asp:Button runat="server" ID="btnDirect" ClientIDMode="Static" CssClass="button-large" Width="200px" Text="Direct" OnClick="btnDirect_Click" Enabled="false" />
            <asp:Button runat="server" ID="btnMultiSite" ClientIDMode="Static" CssClass="button-large" Width="200px" Text="Multi-Site" OnClick="btnMultiSite_Click" Enabled="false" />
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlEnquirySales" CssClass="enquiryForm" Width="900px">
            <asp:Panel runat="server" CssClass="enquiryFormTitle colored-raise">
                Sales Enquiry
            </asp:Panel>
            <asp:HiddenField runat="server" ID="hfClientID" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="hfClientType" ClientIDMode="Static" />
            <asp:Panel runat="server" ID="pnlEnquirySalesClient">
                <asp:Panel runat="server" Width="49%" CssClass="enquiryPane">
                    <asp:Panel runat="server" CssClass="enquiryField">
                        <asp:TextBox ID="txtClientListFilter" ClientIDMode="Static"
                            runat="server" CssClass="enquiryInput"
                            Width="60%" placeholder="Filter"
                            onfocus="ClientListWatcher.watchForChange(this)"
                            onblur="ClientListWatcher.cancelWatchForChange()"
                            onchange="ClientListWatcher.changed(this)" />
                        <asp:Button ID="btnClientListNew" ClientIDMode="Static" runat="server" CssClass="button-thin float-right" Width="30%" Text="New" OnClientClick="PopulateClient(-1); return false;" UseSubmitBehavior="false" />
                    </asp:Panel>
                    <asp:Panel ID="pnlClientList" ClientIDMode="Static" runat="server" CssClass="searchGrid width-full" Style="-moz-max-height: 235px; max-height: 310px; overflow-y: auto;">
                    </asp:Panel>
                    <script type="text/javascript">
                        $(function () {
                            UpdateClientList();
                        });
                    </script>
                </asp:Panel>
                <asp:Panel ID="pnlEnquiryClientEdit" ClientIDMode="Static" runat="server" Width="49%" CssClass="enquiryPane border-left hidden" Style="max-height: 341px;">
                    <asp:Panel runat="server" CssClass="enquiryField">
                        <asp:DropDownList ID="ddlEnquiryClientType" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" onchange="SwitchClientType();">
                            <asp:ListItem Value="0">Residential</asp:ListItem>
                            <asp:ListItem Value="1">Commercial</asp:ListItem>
                        </asp:DropDownList>
                    </asp:Panel>
                    <asp:Panel ID="pnlEnquiryClientResidential" ClientIDMode="Static" runat="server" CssClass="enquiryField">
                        <asp:TextBox ID="txtEnquiryClientTitle" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="47%" placeholder="Title" /><br />
                        <asp:TextBox ID="txtEnquiryClientForename" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="47%" placeholder="Forename" />
                        <asp:TextBox ID="txtEnquiryClientSurname" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="47%" placeholder="Surname" />
                    </asp:Panel>
                    <asp:Panel ID="pnlEnquiryClientCommercial" ClientIDMode="Static" runat="server" CssClass="enquiryField">
                        <asp:TextBox ID="txtEnquiryClientBusinessName" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" placeholder="Business Name" />
                        <asp:TextBox ID="txtEnquiryClientWebsite" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" placeholder="Website" />
                    </asp:Panel>
                    <asp:HiddenField runat="server" ID="hfEnquiryClientAltAccountNo" ClientIDMode="Static" />
                    <asp:Panel runat="server" CssClass="enquiryField">
                        <asp:TextBox ID="txtEnquiryClientHouseNameNo" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" placeholder="House Name / Number" />
                        <asp:TextBox ID="txtEnquiryClientAddress1" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" placeholder="Address Line 1" />
                        <asp:TextBox ID="txtEnquiryClientAddress2" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" placeholder="Address Line 2" />
                        <asp:TextBox ID="txtEnquiryClientTown" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" placeholder="Town" />
                        <asp:TextBox ID="txtEnquiryClientCounty" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" placeholder="County" />
                        <asp:TextBox ID="txtEnquiryClientPostCode" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" placeholder="Post Code" />
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="enquiryField">
                        <asp:TextBox ID="txtEnquiryClientTelephone" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" placeholder="Telephone Number" />
                        <asp:TextBox ID="txtEnquiryClientMobile" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" placeholder="Mobile Number" />
                        <asp:TextBox ID="txtEnquiryClientEmail" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="97.5%" placeholder="Email Address" />
                        <asp:CheckBox runat="server" CssClass="enquiryInput" ID="chkEnquirySms" ClientIDMode="Static" Text="Send SMS Confirmation of Emails"/><br/>
                        <asp:CheckBox runat="server" CssClass="enquiryInput" ID="chkEnquiryIncludeMarketing" ClientIDMode="Static" Text="Include in Marketing" />
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="float-left border-top" Width="100%" Height="30px">
                    <asp:Button ID="btnEnquiyClientOk" ClientIDMode="Static" runat="server" CssClass="button-basic float-right padded" Width="60px" Text="Ok" OnClientClick="return SaveClient();" OnClick="btnEnquiyClientOk_Click" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlEnquirySalesEdit">
                <asp:Panel runat="server" CssClass="enquiryPane" Width="33%">
                    <asp:Panel runat="server" CssClass="enquiryFieldDark">
                        <asp:Label runat="server" CssClass="enquiryLabel bold">Client</asp:Label>
                        <asp:Label ID="lblEnquiryClientName" ClientIDMode="Static" runat="server" CssClass="enquiryLabel" Width="100%">Client Name</asp:Label>
                        <asp:Label ID="lblEnquiryClientAddress" ClientIDMode="Static" runat="server" CssClass="enquiryLabel" Width="100%">Client Address</asp:Label>
                        <asp:Label ID="lblEnquiryClientContactInfo" ClientIDMode="Static" runat="server" CssClass="enquiryLabel" Width="100%">Client Contact Info</asp:Label>
                        <asp:Panel runat="server" Height="20px">
                            <asp:Button ID="btnEditClient" runat="server" CssClass="button-thin float-right" Text="Edit" OnClientClick="EditClient(); return false;" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="enquiryPane border-left border-right height-full" Width="33%">
                    <asp:Panel runat="server" CssClass="enquiryField">
                        <asp:Label runat="server" CssClass="enquiryLabel bold">Surveyor</asp:Label>
                        <asp:DropDownList ID="ddlEnquirySurveyor" ClientIDMode="Static" runat="server" CssClass="enquiryInput small" Width="100%">
                        </asp:DropDownList>
                    </asp:Panel>
                    <asp:HiddenField runat="server" ID="hfEnquirySiteID" ClientIDMode="Static" Value="-1" />
                    <asp:Panel ID="pnlEnquirySalesSiteEditable" ClientIDMode="Static" runat="server" CssClass="enquiryFieldDark">
                        <asp:Label runat="server" CssClass="enquiryLabel bold">Site</asp:Label>
                        <asp:TextBox ID="txtEnquirySiteReference" ClientIDMode="Static" runat="server" CssClass="enquiryInput small" Width="97%" placeholder="Reference" MaxLength="10" />
                        <asp:TextBox ID="txtEnquirySiteHouseNameNo" ClientIDMode="Static" runat="server" CssClass="enquiryInput small" Width="97%" placeholder="House Name / Number" />
                        <asp:TextBox ID="txtEnquirySiteAddress1" ClientIDMode="Static" runat="server" CssClass="enquiryInput small" Width="97%" placeholder="Address Line 1" />
                        <asp:TextBox ID="txtEnquirySiteAddress2" ClientIDMode="Static" runat="server" CssClass="enquiryInput small" Width="97%" placeholder="Address Line 2" />
                        <asp:TextBox ID="txtEnquirySiteTown" ClientIDMode="Static" runat="server" CssClass="enquiryInput small" Width="97%" placeholder="Town" />
                        <asp:TextBox ID="txtEnquirySiteCounty" ClientIDMode="Static" runat="server" CssClass="enquiryInput small" Width="97%" placeholder="County" />
                        <asp:TextBox ID="txtEnquirySitePostCode" ClientIDMode="Static" runat="server" CssClass="enquiryInput small" Width="97%" placeholder="Post Code" />
                    </asp:Panel>
                    <asp:Panel ID="pnlEnquirySalesSiteView" ClientIDMode="Static" runat="server" CssClass="enquiryFieldDark">
                        <asp:DropDownList runat="server" ID="ddlEnquirySite"
                            ClientIDMode="Static" CssClass="enquiryInput small width-full"
                            onchange="SwitchClientSite();">
                        </asp:DropDownList>
                        <asp:Label ID="lblEnquirySiteAddress" ClientIDMode="Static" runat="server" CssClass="enquiryLabel" Width="100%">Client Address<br />Address Info etc</asp:Label>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="enquiryField" Height="20px">
                        <asp:CheckBox ID="chkUseClientAddress" ClientIDMode="Static" runat="server" Text="Use Existing Site" CssClass="enquiryInput small float-right" TextAlign="Left" onchange="ToggleClientSite();" />
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="enquiryPane" Width="33%">
                    <asp:Panel runat="server" CssClass="enquiryField">
                        <asp:TextBox ID="txtEnquirySource" ClientIDMode="Static" runat="server" CssClass="enquiryInput" Width="98%" placeholder="Source" />
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="enquiryField">
                        <asp:Label runat="server" CssClass="enquiryLabel bold" Width="100%">Comments</asp:Label>
                        <asp:TextBox runat="server" ID="txtEnquiryComments" ClientIDMode="Static" CssClass="enquiryField small" Width="98%" TextMode="MultiLine" />
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="enquiryFieldDark">
                        <asp:Label runat="server" CssClass="apptTitle" Width="100%">
                            Appointment
                        </asp:Label>
                        <hr />
                        <asp:HiddenField runat="server" ID="hfStartDateTime" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="hfEndDateTime" ClientIDMode="Static" />
                        <asp:Panel runat="server" ID="pnlApptFieldEdit" ClientIDMode="Static" CssClass="hidden">
                            <asp:Panel runat="server" CssClass="apptField">
                                <asp:Label runat="server" CssClass="apptLabel" ID="lblApptDate" ClientIDMode="Static">
                                    01/01/2014
                                </asp:Label>
                                <asp:Label runat="server" CssClass="apptLabel float-right" ID="lblApptTime" ClientIDMode="Static">
                                    08:00AM - 09:00AM
                                </asp:Label>
                            </asp:Panel>
                            <asp:Panel runat="server" CssClass="apptField" Height="24px">
                                <asp:Button ID="btnEditAppt" runat="server" CssClass="button-thin float-right"
                                    Text="Edit" Width="100%" OnClientClick="OpenCalendar(); return false;" />
                            </asp:Panel>
                        </asp:Panel>
                        <asp:Panel runat="server" ID="pnlApptFieldNone" ClientIDMode="Static" Height="24px">
                            <asp:Button ID="btnMakeAppt" runat="server" CssClass="button-thin float-right"
                                Text="Book Appointment" Width="100%" OnClientClick="OpenCalendar(); return false;" />
                        </asp:Panel>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="float-left border-top" Width="100%" Height="40px">
                    <asp:Button runat="server" ID="btnSaveSales" ClientIDMode="Static"
                        CssClass="button-basic float-right padded"
                        Text="Save" OnClientClick="if (SaveSalesEnquiry()) { NavigateToJob(); return false; } else { alert('Error saving Enquiry. Pllease ensure that a site address is selected.'); return false; }" />
                    <asp:Button runat="server" ID="btnCancelSales" ClientIDMode="Static"
                        CssClass="button-basic float-right padded"
                        Text="Cancel" OnClick="btnCancelSales_Click" />
                </asp:Panel>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="hfJobID" />
                <script type="text/javascript" async>
                    $(function () {
                        LoadEnquiryPanel();
                    });
                </script>
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
