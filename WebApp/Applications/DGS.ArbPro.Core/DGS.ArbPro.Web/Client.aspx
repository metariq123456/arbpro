﻿<%@ Page Title="Client" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Client.aspx.cs" Inherits="DGS.ArbPro.Web.Client" %>

<asp:Content runat="server" ID="TopContent" ContentPlaceHolderID="TopContent">
    <asp:Panel runat="server" CssClass="width-full bordered-bottom" Height="28px">
        <asp:Button runat="server"
            ID="btnDeleteClient" ClientIDMode="Static"
            Text="Delete Client"
            CssClass="button-thin float-right padded"
            OnClientClick="DeleteClient(); return false;" />
        <asp:Button runat="server"
            ID="btnNewClient" ClientIDMode="Static"
            Text="New Client"
            CssClass="button-thin float-right padded"
            OnClientClick="NewClient(); return false;" />
    </asp:Panel>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="Content">
    <script type="text/javascript" src="Content/js/Client.js?V=3.25.0"></script>
    <asp:Panel runat="server" ID="pnlClientsList" ClientIDMode="static" CssClass="clientsList ">
        <asp:Panel runat="server"
            ID="pnlClientsListFilter" ClientIDMode="Static"
            CssClass="clientsList top">
            <asp:TextBox runat="server"
                ID="txtClientsListFilter" ClientIDMode="Static"
                placeholder="Filter List"
                onfocus="listWatcher.watchForChange(this)"
                onblur="listWatcher.cancelWatchForChange()"
                onchange="listWatcher.changed(this)" />
        </asp:Panel>
        <asp:Panel runat="server"
            CssClass="clientsList drag" onclick="ToggleList();">
        </asp:Panel>
        <asp:Panel runat="server"
            ID="pnlClientsListList" ClientIDMode="Static"
            CssClass="clientsList list">
            <asp:Panel runat="server" CssClass="center width-full">
                <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlEdit" ClientIDMode="static" CssClass="clientContent height-full">
        <asp:Panel runat="server" ID="pnlClientInfo" ClientIDMode="Static" CssClass="clientInfo" Width="740px">
            <asp:Panel runat="server" CssClass="colored-raise clientHeader">
                <asp:Label runat="server">
                        Client Information
                </asp:Label>
                <asp:HiddenField runat="server"
                    ID="hfClientID" ClientIDMode="Static" />
                <asp:HiddenField runat="server"
                    ID="hfClientType" ClientIDMode="Static" />
                <asp:Label runat="server"
                    ID="lblClientType" ClientIDMode="Static"
                    CssClass="clientInput float-right" Font-Bold="true" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="float-left" Width="370px">
                <asp:Panel runat="server" ID="pnlClientResidential" ClientIDMode="Static">
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:Label runat="server" CssClass="clientLabel">
                                Title
                        </asp:Label>
                        <asp:Label runat="server"
                            ID="txtClientTitle" ClientIDMode="Static"
                            CssClass="clientInput"/>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:Label runat="server" CssClass="clientLabel">
                                Forename
                        </asp:Label>
                        <asp:Label runat="server"
                            ID="txtClientForename" ClientIDMode="Static"
                            CssClass="clientInput"/>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:Label runat="server" CssClass="clientLabel">
                                Surname
                        </asp:Label>
                        <asp:Label runat="server"
                            ID="txtClientSurname" ClientIDMode="Static"
                            CssClass="clientInput"/>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlClientCommercial">
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:Label runat="server" CssClass="clientLabel">
                                Business Name
                        </asp:Label>
                        <asp:Label runat="server"
                            ID="txtClientBusinessName" ClientIDMode="Static"
                            CssClass="clientInput"/>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:Label runat="server" CssClass="clientLabel">
                                Business Sector
                        </asp:Label>
                        <asp:Label runat="server"
                            ID="txtClientBusinessSector" ClientIDMode="Static"
                            CssClass="clientInput"/>
                    </asp:Panel>
                    <asp:Panel runat="server" CssClass="clientField">
                        <asp:Label runat="server" CssClass="clientLabel">
                                Website
                        </asp:Label>
                        <asp:Label runat="server"
                            ID="txtClientWebsite" ClientIDMode="Static"
                            CssClass="clientInput"/>
                    </asp:Panel>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Account No
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtClientAccountNo" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                           Alt Account No
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtClientAlternateAccountNo" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Email
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtClientEmail" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Telephone
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtClientTelephone" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Mobile
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtClientMobile" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" CssClass="float-left" Width="370px">
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Name/Number
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtClientHouseNameOrNumber" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Address Line 1
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtClientAddress1" ClientIDMode="Static"
                        CssClass="clientInput" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Address Line 2
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtClientAddress2" ClientIDMode="Static"
                        CssClass="clientInput" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Town
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtClientTown" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            County
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtClientCounty" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField">
                    <asp:Label runat="server" CssClass="clientLabel">
                            Post Code
                    </asp:Label>
                    <asp:Label runat="server"
                        ID="txtClientPostCode" ClientIDMode="Static"
                        CssClass="clientInput"/>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField" Width="100%">
                    <asp:Label runat="server" CssClass="clientLabel" Width="90%">
                            Send SMS Confirmation of Emails 
                        <asp:CheckBox Enabled="False" runat="server" CssClass="clientLabel"
                        ID="chkSendSms" ClientIDMode="Static"
                        Text=""/>
                    </asp:Label>
                </asp:Panel>
                <asp:Panel runat="server" CssClass="clientField" Width="100%">
                    <asp:Label runat="server" CssClass="clientLabel" Width="90%">
                            Include in Marketing 
                        <asp:CheckBox Enabled="False" runat="server" CssClass="clientLabel"
                        ID="chkIncludeMarketing" ClientIDMode="Static"
                        Text=""/>
                    </asp:Label>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server">
                <asp:Button runat="server"
                    ID="btnEdit" ClientIDMode="Static"
                    CssClass="button-basic float-right padded"
                    Text="Edit"
                    OnClientClick="EditClient(); return false;" />
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlClientLists" CssClass="clientTabs" style="margin-bottom: 36px;">
            <asp:Panel runat="server" CssClass="clientTabBar">
                <asp:HyperLink runat="server" ID="tabContacts" ClientIDMode="Static" CssClass="clientTab activeTab" NavigateUrl="javascript:Client_curTab = 1; SwitchTab();">
                        Contacts
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabSites" ClientIDMode="Static" CssClass="clientTab" NavigateUrl="javascript:Client_curTab = 2; SwitchTab();">
                        Sites
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabJobs" ClientIDMode="Static" CssClass="clientTab" NavigateUrl="javascript:Client_curTab = 3; SwitchTab();">
                        Jobs
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabAppointments" ClientIDMode="Static" CssClass="clientTab" NavigateUrl="javascript:Client_curTab = 4; SwitchTab();">
                        Appointments
                </asp:HyperLink>
                <%--<asp:HyperLink runat="server" ID="tabQuotes" ClientIDMode="Static" CssClass="clientTab" NavigateUrl="javascript:Client_curTab = 5; SwitchTab();">
                        Quotes
                </asp:HyperLink>
                <asp:HyperLink runat="server" ID="tabInvoices" ClientIDMode="Static" CssClass="clientTab" NavigateUrl="javascript:Client_curTab = 6; SwitchTab();">
                        Invoices
                </asp:HyperLink>--%>
                <asp:Button runat="server"
                    ID="btnNewContact" ClientIDMode="Static"
                    CssClass="button-thin float-right padded"
                    Text="Add New" OnClientClick="EditContact(-1); return false;" />
                <asp:Button runat="server"
                    ID="btnNewSite" ClientIDMode="Static"
                    CssClass="button-thin float-right padded"
                    Text="Add New" OnClientClick="EditSite(-1); return false;" />
                <asp:Button runat="server"
                    ID="btnNewJob" ClientIDMode="Static"
                    CssClass="button-basic float-right padded"
                    OnClientClick="CreateNewJob(); return false;"
                    Text="Add New" />
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlTabContacts" ClientIDMode="Static" CssClass="hidden">
                <asp:Panel runat="server" ID="grdContacts" ClientIDMode="Static" CssClass="width-full" style="max-height: 400px; overflow-y: scroll;">
                    <asp:Panel runat="server" CssClass="center width-full">
                        <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlTabSites" ClientIDMode="Static">
                <asp:Panel runat="server" ID="grdSites" ClientIDMode="Static" CssClass="width-full" style="max-height: 400px; overflow-y: scroll;">
                    <asp:Panel runat="server" CssClass="center width-full">
                        <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>

            <asp:Panel runat="server" ID="pnlTabJobs" ClientIDMode="Static" CssClass="hidden">
                <asp:Panel runat="server" ID="grdJobs" ClientIDMode="Static" CssClass="width-full" style="max-height: 400px; overflow-y: scroll;">
                    <asp:Panel runat="server" CssClass="center width-full">
                        <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlTabAppointments" ClientIDMode="Static" CssClass="hidden">
                <asp:Panel runat="server" ID="grdAppointments" ClientIDMode="Static" CssClass="width-full" style="max-height: 400px; overflow-y: scroll;">
                    <asp:Panel runat="server" CssClass="center width-full">
                        <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <%--<asp:Panel runat="server" ID="pnlTabQuotes" ClientIDMode="Static" CssClass="hidden">
                <asp:Panel runat="server" ID="grdQuotes" ClientIDMode="Static" CssClass="width-full">
                    <asp:Panel runat="server" CssClass="center width-full">
                        <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlTabInvoices" ClientIDMode="Static" CssClass="hidden">
                <asp:Panel runat="server" ID="grdInvoices" ClientIDMode="Static" CssClass="width-full">
                    <asp:Panel runat="server" CssClass="center width-full">
                        <asp:Image runat="server" ImageUrl="~/Images/load_Medium.gif" CssClass="padded" />
                    </asp:Panel>
                </asp:Panel>
            </asp:Panel>--%>
        </asp:Panel>
    </asp:Panel>
    <script type="text/javascript" async>
        $(function () {
            try {
                var Param = GetURLParameter('curTab');
                if (Param != 'null')
                    Client_curTab = parseInt(Param);
            } catch (e) { }
            SwitchTab();
            StartList();
            UpdateContactList();
            UpdateSiteList();
            UpdateJobList();
            UpdateAppointmentList();
        });
    </script>
</asp:Content>
