﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class BookWork : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Pass the query string parameters
            int recId = -1;
            if (Request["JobID"] != null)
            {
                int.TryParse(Request["JobID"],out recId);

            }
            hfJobID.Value = recId.ToString();

            recId = -1;
            if (Request["QuoteID"] != null)
            {
                int.TryParse(Request["QuoteID"], out recId);
            }
            hfQuoteId.Value = recId.ToString();

            recId = -1;
            if (Request["WorkID"] != null)
            {
                int.TryParse(Request["WorkID"], out recId);
            }
            hfWorkId.Value = recId.ToString();

            recId = -1;
            if (Request["ReferrerSiteID"] != null)
            {
                int.TryParse(Request["ReferrerSiteID"], out recId);
            }
            hfReferrerSiteId.Value = recId.ToString();
        }
    }
}