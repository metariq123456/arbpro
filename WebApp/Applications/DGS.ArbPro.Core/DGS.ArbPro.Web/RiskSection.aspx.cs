﻿namespace DGS.ArbPro.Web
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The risk section.
    /// </summary>
    public partial class RiskSection : System.Web.UI.Page
    {
        /// <summary>
        /// The section.
        /// </summary>
        private List<Types.Templates.RiskSection> section = new List<Types.Templates.RiskSection>();

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e"> The event arguments. </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            BL.RiskTemplateHandler service = new BL.RiskTemplateHandler(AspSession.UserDatabase);
            this.section = service.GetAllRiskSectionTemplates();
            this.section.Sort((x, y) => string.Compare(x.Title, y.Title));
            this.hfSectionTotal.Value = this.section.Count.ToString();
        }

        /// <summary>
        /// Creates a list of sections and adds them to a table.
        /// </summary>
        protected void CreateSections()
        {
            int curTab = 1;

            // limit the height of the table to 450 pixels to ensure there is space for the details section
            Response.Write(@"<div  style=""height: 500px; overflow: auto; width-full;"">");
            Response.Write(@"<table class=""searchGrid width-full""><col width='68%'></col></col><col width='10%'></col><col width='10%'></col><col width='10%'></col>");
            foreach (Types.Templates.RiskSection s in this.section)
            {
                if (!s.IsDeleted)
                {
                    Response.Write(string.Format(@"<tr id='tab{0}' class=""activeTab""  onclick=""javascript:Section_curTab = {0}; SwitchSection();"">{1}", curTab, Environment.NewLine));
                    Response.Write(string.Format("<td>{0}</td>", s.Title));
                    Response.Write("<td onclick='NewEditOrCopySection(" + s.Id + ", false);'><a href='#'>Edit</a></td>");
                    Response.Write("<td onclick='NewEditOrCopySection(" + s.Id + ", true);'><a href='#'>Copy</a></td>");
                    Response.Write("<td onclick='CancelSection(" + s.Id + ");'><a href='#'>Delete</a></td>");
                    Response.Write(@"</tr>");
                }

                curTab++;
            }

            Response.Write(@"<tr>");
            Response.Write(@"</tr>");
            Response.Write(string.Format(@"</table>"));
            Response.Write("</div>");
            Response.Write(@"<br/>");
        }

        /// <summary>
        /// Creates the section content.
        /// </summary>
        protected void CreateSectionContent()
        {
            int curTab = 1;
            foreach (Types.Templates.RiskSection s in this.section)
            {
                if (!s.IsDeleted)
                {
                    Response.Write(string.Format(@"<div id=""lblDetails{0}"" class=""clientTabBar hidden"">{1}", curTab,"Section Details For: " + s.Title));
                    Response.Write("</div>");
                    Response.Write(string.Format(@"<div id=""pnl{0}"" class=""hidden"">{1}", curTab, Environment.NewLine));
                    Response.Write(string.Format(@"<div id=""grd{0}"" class=""width-full"">{1}", curTab, Environment.NewLine));
                    Response.Write(string.Format(@"<table data-section='" + s.Id + @"' class=""searchGrid width-full""><col width='33%'></col><col width='65%'>{0}<tr><th>Field</th><th>Value</th></tr>{0}", Environment.NewLine));
                    Response.Write(@"<tr>");
                    Response.Write(string.Format("<td> Reference Document </td><td><span id='secRefDoc'>{0}</span></td>", s.ReferenceDoc));
                    Response.Write(@"</tr>");
                    Response.Write(string.Format("<td> Reference Chapter </td><td><span id='secRefChapter'>{0}</span></td>", s.ChapterRef));
                    Response.Write(@"<tr>");
                    Response.Write(@"</tr>");
                    Response.Write(string.Format("<td> Description </td><td><span id='secRefDesc'>{0}</span></td>", s.Description));
                    Response.Write(@"<tr>");
                    Response.Write(@"</tr>");
                    Response.Write(string.Format(@"</table>"));
                    Response.Write(@"<br/>");
                    Response.Write("</div>");
                    Response.Write("</div>");
                }

                curTab++;
            }
        }
    }
}