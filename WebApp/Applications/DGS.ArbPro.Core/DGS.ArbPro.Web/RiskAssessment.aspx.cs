﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class RiskAssessment : System.Web.UI.Page
    {
        //private Types.Risk _RiskAssessment = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            int value = -1;

            if (Request["JobID"] != null && int.TryParse(Request["JobID"], out value))
            {
                hfJobID.Value = value.ToString();
            }

            if (Request["RiskID"] != null && int.TryParse(Request["RiskID"], out value))
            {
                hfRiskID.Value = value.ToString();
            }
            else
            {
                hfRiskID.Value = "-1";
            }
            if (Request["QuoteID"] != null && int.TryParse(Request["QuoteID"], out value))
            {
                hfQuoteID.Value = value.ToString();
            }
            else
            {
                hfQuoteID.Value = "-1";
            }
            if (Request["AppointmentID"] != null && int.TryParse(Request["AppointmentID"], out value))
            {
                hfAppId.Value = value.ToString();
            }
            else
            {
                hfAppId.Value = "-1";
            }

            //_RiskAssessment = BL.RiskAssessmentHandler.RiskAssessmentForQuoteId(AspSession.UserDatabase, quoteId);
        }

        /*
        public void CreateSectionTabs()
        {
            int curTab = 1;
            foreach (Types.RiskSection s in _RiskAssessment.AssessmentSections)
            {
                //a id="tabContacts" class="clientTab activeTab" href="javascript:Client_curTab = 1; SwitchTab();">
                Response.Write(string.Format(@"<a id=""tab{0}"" class=""clientTab activeTab"" href=""javascript:Section_curTab = {0}; SwitchTab();"">", curTab));
                Response.Write(string.Format(@"{0}{1}{0}", Environment.NewLine, s.Title));
                Response.Write(@"</a>");
                curTab++;
            }
            Response.Write(string.Format(@"<a id=""tabAdd"" class=""clientTab"" href=""javascript:Section_curTab = {0}; AddTab();"">", curTab));
            Response.Write(string.Format(@"{0}{1}{0}", Environment.NewLine, " + "));
            Response.Write(@"</a>");
        }

        public void CreateSectionContent()
        {
            int curTab = 1;
            foreach (Types.RiskSection s in _RiskAssessment.AssessmentSections)
            {
                Response.Write(string.Format(@"<div id=""pnl{0}"" class=""hidden"">{1}", curTab, Environment.NewLine));
                Response.Write(string.Format(@"<div id=""grd{0}"" class=""width-full"">{1}", curTab, Environment.NewLine));
                Response.Write(string.Format(@"<table class=""searchGrid width-full"">{0}<tr><th>Field</th><th>Value</th></tr>{0}", Environment.NewLine));
                Response.Write(@"<tr>");
                Response.Write(string.Format("<td> Reference Document </td><td> {0} </td>", s.ReferenceDoc));
                Response.Write(@"</tr>");
                Response.Write(string.Format("<td> Reference Chapter </td><td> {0} </td>", s.Chapter));
                Response.Write(@"<tr>");
                Response.Write(@"</tr>");
                Response.Write(string.Format("<td> Description </td><td> {0} </td>", s.Description));
                Response.Write(@"<tr>");
                Response.Write(@"</tr>");
                Response.Write(string.Format(@"</table>"));
                Response.Write(string.Format(@"<input type=""submit"" name=""btnEditSection{0}"" value=""Edit"" id=""btnEditSection{0}"" class=""button-basic float-right padded"" />", curTab));
                Response.Write(@"<br/>");
                Response.Write(string.Format(@"<table class=""searchGrid width-full"">{0}<tr><th>Criteria</th><th>Chapter Reference</th><th>Assessment Phase</th></tr>", Environment.NewLine));
                foreach (Types.RiskCriterion c in s.Criteria)
                {
                    Response.Write(@"<tr>");
                    Response.Write(string.Format("<td> {0} </td><td> {1} </td><td> {2} </td>", c.Criterion, c.ChapterRef, c.AssessmentPhase));
                    Response.Write(@"</tr>");

                }
                Response.Write(string.Format(@"</table>"));
                Response.Write(string.Format(@"<input type=""submit"" name=""btnNewCriteria{0}"" value=""Add Criteria"" id=""btnNewCriteria{0}"" class=""button-basic float-right padded"" />", curTab));
                Response.Write("</div>");
                Response.Write("</div>");
                curTab++;
            }
        }
         * */
    }
}