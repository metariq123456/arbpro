﻿namespace DGS.ArbPro.Web.Controls
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using System.Text;
    using System.Diagnostics;

    namespace PdfToImage
    {
        public class PDFConvert
        {
            public const string GhostScriptDLLName_64 = "gsdll64.dll";
            public const string GhostScriptDLLName_32 = "gsdll32.dll";
            private static bool useSimpleAnsiConversion = true;
            private const string GS_OutputFileFormat = "-sOutputFile={0}";
            private const string GS_DeviceFormat = "-sDEVICE={0}";
            private const string GS_FirstParameter = "pdf2img";
            private const string GS_ResolutionXFormat = "-r{0}";
            private const string GS_ResolutionXYFormat = "-r{0}x{1}";
            private const string GS_GraphicsAlphaBits = "-dGraphicsAlphaBits={0}";
            private const string GS_TextAlphaBits = "-dTextAlphaBits={0}";
            private const string GS_FirstPageFormat = "-dFirstPage={0}";
            private const string GS_LastPageFormat = "-dLastPage={0}";
            private const string GS_FitPage = "-dPDFFitPage";
            private const string GS_PageSizeFormat = "-g{0}x{1}";
            private const string GS_DefaultPaperSize = "-sPAPERSIZE={0}";
            private const string GS_JpegQualityFormat = "-dJPEGQ={0}";
            private const string GS_RenderingThreads = "-dNumRenderingThreads={0}";
            private const string GS_Fixed1stParameter = "-dNOPAUSE";
            private const string GS_Fixed2ndParameter = "-dBATCH";
            private const string GS_Fixed3rdParameter = "-dSAFER";
            private const string GS_FixedMedia = "-dFIXEDMEDIA";
            private const string GS_QuiteOperation = "-q";
            private const string GS_StandardOutputDevice = "-";
            private const string GS_MultiplePageCharacter = "%";
            private const string GS_FontPath = "-sFONTPATH={0}";
            private const string GS_NoPlatformFonts = "-dNOPLATFONTS";
            private const string GS_NoFontMap = "-dNOFONTMAP";
            private const string GS_FontMap = "-sFONTMAP={0}";
            private const string GS_SubstitutionFont = "-sSUBSTFONT={0}";
            private const string GS_FCOFontFile = "-sFCOfontfile={0}";
            private const string GS_FAPIFontMap = "-sFAPIfontmap={0}";
            private const string GS_NoPrecompiledFonts = "-dNOCCFONTS";
            private static System.Threading.Mutex mutex;
            
            /// <summary>Needed to copy memory from one location to another, used to fill the struct</summary>
            /// <param name="Destination"></param>
            /// <param name="Source"></param>
            /// <param name="Length"></param>
            [DllImport("kernel32.dll", EntryPoint = "RtlMoveMemory")]
            static extern void CopyMemory(IntPtr Destination, IntPtr Source, uint Length);
            
            /// <summary>Create a new instance of Ghostscript. This instance is passed to most other gsapi functions. The caller_handle will be provided to callback functions.
            ///  At this stage, Ghostscript supports only one instance. </summary>
            /// <param name="pinstance"></param>
            /// <param name="caller_handle"></param>
            /// <returns></returns>
            [DllImport(GhostScriptDLLName_64, EntryPoint = "gsapi_new_instance")]
            private static extern int gsapi_new_instance_64(out IntPtr pinstance, IntPtr caller_handle);

            [DllImport(GhostScriptDLLName_32, EntryPoint = "gsapi_new_instance")]
            private static extern int gsapi_new_instance_32(out IntPtr pinstance, IntPtr caller_handle);

            private static int gsapi_new_instance(out IntPtr pinstance, IntPtr caller_handle)
            {
                if (Environment.Is64BitProcess)
                {
                    return gsapi_new_instance_64(out pinstance, caller_handle);
                }
                else
                {
                    return gsapi_new_instance_32(out pinstance, caller_handle);
                }
            }

            /// <summary>This is the important function that will perform the conversion</summary>
            /// <param name="instance"></param>
            /// <param name="argc"></param>
            /// <param name="argv"></param>
            /// <returns></returns>
            [DllImport(GhostScriptDLLName_64, EntryPoint = "gsapi_init_with_args")]
            private static extern int gsapi_init_with_args_64(IntPtr instance, int argc, IntPtr argv);

            [DllImport(GhostScriptDLLName_32, EntryPoint = "gsapi_init_with_args")]
            private static extern int gsapi_init_with_args_32(IntPtr instance, int argc, IntPtr argv);

            private static int gsapi_init_with_args(IntPtr instance, int argc, IntPtr argv)
            {
                if (Environment.Is64BitProcess)
                {
                    return gsapi_init_with_args_64(instance, argc, argv);
                }
                else
                {
                    return gsapi_init_with_args_32(instance, argc, argv);
                }
            }

            /// <summary>
            /// Exit the interpreter. This must be called on shutdown if gsapi_init_with_args() has been called, and just before gsapi_delete_instance(). 
            /// </summary>
            /// <param name="instance"></param>
            /// <returns></returns>
            [DllImport(GhostScriptDLLName_64, EntryPoint = "gsapi_exit")]
            private static extern int gsapi_exit_64(IntPtr instance);

            [DllImport(GhostScriptDLLName_32, EntryPoint = "gsapi_exit")]
            private static extern int gsapi_exit_32(IntPtr instance);

            private static int gsapi_exit(IntPtr instance)
            {
                if (Environment.Is64BitProcess)
                {
                    return gsapi_exit_64(instance);
                }
                else
                {
                    return gsapi_exit_32(instance);
                }
            }

            /// <summary>
            /// Destroy an instance of Ghostscript. Before you call this, Ghostscript must have finished. If Ghostscript has been initialised, you must call gsapi_exit before gsapi_delete_instance. 
            /// </summary>
            /// <param name="instance"></param>
            [DllImport(GhostScriptDLLName_64, EntryPoint = "gsapi_delete_instance")]
            private static extern void gsapi_delete_instance_64(IntPtr instance);

            [DllImport(GhostScriptDLLName_32, EntryPoint = "gsapi_delete_instance")]
            private static extern void gsapi_delete_instance_32(IntPtr instance);

            private static void gsapi_delete_instance(IntPtr instance)
            {
                if (Environment.Is64BitProcess)
                {
                    gsapi_delete_instance_64(instance);
                }
                else
                {
                    gsapi_delete_instance_32(instance);
                }
            }

            /// <summary>Get info about the version of Ghostscript i'm using</summary>
            /// <param name="pGSRevisionInfo"></param>
            /// <param name="intLen"></param>
            /// <returns></returns>
            [DllImport(GhostScriptDLLName_64, EntryPoint = "gsapi_revision")]
            private static extern int gsapi_revision_64(ref GS_Revision pGSRevisionInfo, int intLen);

            [DllImport(GhostScriptDLLName_32, EntryPoint = "gsapi_revision")]
            private static extern int gsapi_revision_32(ref GS_Revision pGSRevisionInfo, int intLen);

            private static int gsapi_revision(ref GS_Revision pGSRevisionInfo, int intLen)
            {
                if (Environment.Is64BitProcess)
                {
                    return gsapi_revision_64(ref pGSRevisionInfo, intLen);
                }
                else
                {
                    return gsapi_revision_32(ref pGSRevisionInfo, intLen);
                }
            }

            /// <summary>Use a different I/O</summary>
            /// <param name="lngGSInstance"></param>
            /// <param name="gsdll_stdin">Function that menage the Standard INPUT</param>
            /// <param name="gsdll_stdout">Function that menage the Standard OUTPUT</param>
            /// <param name="gsdll_stderr">Function that menage the Standard ERROR output</param>
            /// <returns></returns>
            [DllImport(GhostScriptDLLName_64, EntryPoint = "gsapi_set_stdio")]
            private static extern int gsapi_set_stdio_64(IntPtr lngGSInstance, StdioCallBack gsdll_stdin, StdioCallBack gsdll_stdout, StdioCallBack gsdll_stderr);

            [DllImport(GhostScriptDLLName_32, EntryPoint = "gsapi_set_stdio")]
            private static extern int gsapi_set_stdio_32(IntPtr lngGSInstance, StdioCallBack gsdll_stdin, StdioCallBack gsdll_stdout, StdioCallBack gsdll_stderr);

            private static int gsapi_set_stdio(IntPtr lngGSInstance, StdioCallBack gsdll_stdin, StdioCallBack gsdll_stdout, StdioCallBack gsdll_stderr)
            {
                if (Environment.Is64BitProcess)
                {
                    return gsapi_set_stdio_64(lngGSInstance, gsdll_stdin, gsdll_stdout, gsdll_stderr); 
                }
                else
                {
                    return gsapi_set_stdio_32(lngGSInstance, gsdll_stdin, gsdll_stdout, gsdll_stderr);
                }
            }

            const int e_Quit = -101;
            const int e_NeedInput = -106;
            
            private string _sDeviceFormat;
            private string _sParametersUsed;

            private int _iWidth;
            private int _iHeight;
            private int _iResolutionX;
            private int _iResolutionY;
            private int _iJPEGQuality;
            /// <summary>The first page to convert in image</summary>
            private int _iFirstPageToConvert = -1;
            /// <summary>The last page to conver in an image</summary>
            private int _iLastPageToConvert = -1;
            /// <summary>This parameter is used to control subsample antialiasing of graphics</summary>
            private int _iGraphicsAlphaBit = -1;
            /// <summary>This parameter is used to control subsample antialiasing of text</summary>
            private int _iTextAlphaBit = -1;
            /// <summary>In how many thread i should perform the conversion</summary>
            /// <remarks>This is a Major innovation since 8.63 NEVER use it with previous version!</remarks>
            private int _iRenderingThreads = -1;

            /// <summary>In how many thread i should perform the conversion</summary>
            /// <remarks>This is a Major innovation since 8.63 NEVER use it with previous version!</remarks>
            /// <value>Set it to 0 made the program set it to Environment.ProcessorCount HT machine could want to perform a check for this..</value>
            public int RenderingThreads
            {
                get { return _iRenderingThreads; }
                set
                {
                    if (value == 0)
                        _iRenderingThreads = Environment.ProcessorCount;
                    else
                        _iRenderingThreads = value;
                }
            }
            private bool _bFitPage;
            private bool _bThrowOnlyException = false;
            private bool _bRedirectIO = false;
            private bool _bForcePageSize = false;
            private string _sDefaultPageSize;
            private IntPtr _objHandle;
            private bool _didOutputToMultipleFile = false;

            private System.Diagnostics.Process myProcess;
            public StringBuilder output;
            
            private List<string> _sFontPath = new List<string>();
            private bool _bDisablePlatformFonts = false;
            private bool _bDisableFontMap = false;
            private List<string> _sFontMap = new List<string>();
            private string _sSubstitutionFont;
            private string _sFCOFontFile;
            private string _sFAPIFontMap;
            private bool _bDisablePrecompiledFonts = false;
            
            public string OutputFormat
            {
                get { return _sDeviceFormat; }
                set { _sDeviceFormat = value; }
            }

            public string DefaultPageSize
            {
                get { return _sDefaultPageSize; }
                set { _sDefaultPageSize = value; }
            }

            public bool ForcePageSize
            {
                get { return _bForcePageSize; }
                set { _bForcePageSize = value; }
            }

            public string ParametersUsed
            {
                get { return _sParametersUsed; }
                set { _sParametersUsed = value; }
            }

            public int Width
            {
                get { return _iWidth; }
                set { _iWidth = value; }
            }

            public int Height
            {
                get { return _iHeight; }
                set { _iHeight = value; }
            }

            public int ResolutionX
            {
                get { return _iResolutionX; }
                set { _iResolutionX = value; }
            }

            public int ResolutionY
            {
                get { return _iResolutionY; }
                set { _iResolutionY = value; }
            }

            public int GraphicsAlphaBit
            {
                get { return _iGraphicsAlphaBit; }
                set
                {
                    if ((value > 4) | (value == 3))
                        throw new ArgumentOutOfRangeException("The Graphics Alpha Bit must have a value between 1 2 and 4, or <= 0 if not set");
                    _iGraphicsAlphaBit = value;
                }
            }
            
            public int TextAlphaBit
            {
                get { return _iTextAlphaBit; }
                set
                {
                    if ((value > 4) | (value == 3))
                        throw new ArgumentOutOfRangeException("The Text Alpha Bit must have a value between 1 2 and 4, or <= 0 if not set");
                    _iTextAlphaBit = value;
                }
            }

            public Boolean FitPage
            {
                get { return _bFitPage; }
                set { _bFitPage = value; }
            }
            
            public int JPEGQuality
            {
                get { return _iJPEGQuality; }
                set { _iJPEGQuality = value; }
            }
            
            public int FirstPageToConvert
            {
                get { return _iFirstPageToConvert; }
                set { _iFirstPageToConvert = value; }
            }
            
            public int LastPageToConvert
            {
                get { return _iLastPageToConvert; }
                set { _iLastPageToConvert = value; }
            }
            
            public Boolean ThrowOnlyException
            {
                get { return _bThrowOnlyException; }
                set { _bThrowOnlyException = value; }
            }
            
            public bool RedirectIO
            {
                get { return _bRedirectIO; }
                set { _bRedirectIO = value; }
            }
            
            public bool OutputToMultipleFile
            {
                get { return _didOutputToMultipleFile; }
                set { _didOutputToMultipleFile = value; }
            }
            
            public bool UseMutex
            {
                get { return mutex != null; }
                set
                {
                    if (!value)//if i don't want to use it
                    {
                        if (mutex != null)//if it exist
                        {   //close and delete it
                            mutex.ReleaseMutex();
                            mutex.Close();
                            mutex = null;
                        }
                    }
                    else//If i want to use mutex create it if it doesn't exist
                    {
                        if (mutex == null)
                            mutex = new System.Threading.Mutex(false, "MutexGhostscript");
                    }
                }
            }

            public List<string> FontPath
            {
                get { return _sFontPath; }
                set { _sFontPath = value; }
            }

            public bool DisablePlatformFonts
            {
                get { return _bDisablePlatformFonts; }
                set { _bDisablePlatformFonts = value; }
            }

            public bool DisableFontMap
            {
                get { return _bDisableFontMap; }
                set { _bDisableFontMap = value; }
            }

            public List<string> FontMap
            {
                get { return _sFontMap; }
                set { _sFontMap = value; }
            }

            public string SubstitutionFont
            {
                get { return _sSubstitutionFont; }
                set { _sSubstitutionFont = value; }
            }

            public string FCOFontFile
            {
                get { return _sFCOFontFile; }
                set { _sFCOFontFile = value; }
            }

            public string FAPIFontMap
            {
                get { return _sFAPIFontMap; }
                set { _sFAPIFontMap = value; }
            }

            public bool DisablePrecompiledFonts
            {
                get { return _bDisablePrecompiledFonts; }
                set { _bDisablePrecompiledFonts = value; }
            }

            public PDFConvert(IntPtr objHandle)
            {
                _objHandle = objHandle;
            }

            public PDFConvert()
            {
                _objHandle = IntPtr.Zero;
            }

            /// <summary>Convert a single file!</summary>
            /// <param name="inputFile">The file PDf to convert</param>
            /// <param name="outputFile">The image file that will be created</param>
            /// <remarks>You must pass all the parameter for the conversion
            /// as Proprieties of this class</remarks>
            /// <returns>True if the conversion succed!</returns>
            public bool Convert(string inputFile, string outputFile)
            {
                return Convert(inputFile, outputFile, _bThrowOnlyException, null);
            }

            /// <summary>Convert a single file!</summary>
            /// <param name="inputFile">The file PDf to convert</param>
            /// <param name="outputFile">The image file that will be created</param>
            /// <param name="parameters">You must pass all the parameter for the conversion here</param>
            /// <remarks>Thanks to 	tchu_2000 for the help!</remarks>
            /// <returns>True if the conversion succed!</returns>
            public bool Convert(string inputFile, string outputFile, string parameters)
            {
                return Convert(inputFile, outputFile, _bThrowOnlyException, parameters);
            }

            /// <summary>Convert a single file!</summary>
            /// <param name="inputFile">The file PDf to convert</param>
            /// <param name="outputFile">The image file that will be created</param>
            /// <param name="throwException">if the function should throw an exception
            /// or display a message box</param>
            /// <remarks>You must pass all the parameter for the conversion
            /// as Proprieties of this class</remarks>
            /// <returns>True if the conversion succed!</returns>
            private bool Convert(string inputFile, string outputFile, bool throwException, string options)
            {
                if (string.IsNullOrEmpty(inputFile))
                    throw new ArgumentNullException("inputFile");
                if (!System.IO.File.Exists(inputFile))
                    throw new ArgumentException(string.Format("The file :'{0}' doesn't exist", inputFile), "inputFile");
                if (string.IsNullOrEmpty(_sDeviceFormat))
                    throw new ArgumentNullException("Device");
                
                if (mutex != null) mutex.WaitOne();
                bool result = false;
                try
                {
                    result = ExecuteGhostscriptCommand(GetGeneratedArgs(inputFile, outputFile, options));
                }
                finally { if (mutex != null) mutex.ReleaseMutex(); }
                return result;
            }

            /// <summary>Print a file</summary>
            /// <param name="inputFile">THe file to print</param>
            /// <param name="printParametersFile">The file with the configuration of the printer</param>
            /// <returns>True if i send the work to the printer queue</returns>
            public bool Print(string inputFile, string printParametersFile)
            {
                if (string.IsNullOrEmpty(inputFile))
                    throw new ArgumentNullException("inputFile");
                if (!System.IO.File.Exists(inputFile))
                    throw new ArgumentException(string.Format("The file :'{0}' doesn't exist", inputFile), "inputFile");
                //Avoid to work when the file doesn't exist
                if (string.IsNullOrEmpty(printParametersFile))
                    throw new ArgumentNullException("printParametersFile");
                if (!System.IO.File.Exists(printParametersFile))
                    throw new ArgumentException(string.Format("The file :'{0}' doesn't exist", printParametersFile), "printParametersFile");
                
                List<string> args = new List<string>(7);
                args.Add("printPdf");
                args.Add("-dNOPAUSE");
                args.Add("-dBATCH");
                if (_iFirstPageToConvert > 0)
                    args.Add(string.Format("-dFirstPage={0}", _iFirstPageToConvert));
                if ((_iLastPageToConvert > 0) && (_iLastPageToConvert >= _iFirstPageToConvert))
                    args.Add(string.Format("-dLastPage={0}", _iLastPageToConvert));
                args.Add(printParametersFile);
                args.Add(inputFile);
                bool result = false;
                if (mutex != null) mutex.WaitOne();
                try { result = ExecuteGhostscriptCommand(args.ToArray()); }
                finally { if (mutex != null) mutex.ReleaseMutex(); }
                return result;
            }

            /// <summary>Execute a Ghostscript command with a certain list of arguments</summary>
            /// <param name="sArgs">The list of the arguments</param>
            /// <returns>true if it succed, false otherwise</returns>
            private bool ExecuteGhostscriptCommand(string[] sArgs)
            {
                int intReturn, intCounter, intElementCount;
                IntPtr intGSInstanceHandle = IntPtr.Zero;
                object[] aAnsiArgs;
                IntPtr[] aPtrArgs;
                GCHandle[] aGCHandle;
                IntPtr callerHandle, intptrArgs;
                GCHandle gchandleArgs;
                
                intElementCount = sArgs.Length;
                aAnsiArgs = new object[intElementCount];
                aPtrArgs = new IntPtr[intElementCount];
                aGCHandle = new GCHandle[intElementCount];
                
                for (intCounter = 0; intCounter < intElementCount; intCounter++)
                {
                    aAnsiArgs[intCounter] = StringToAnsiZ(sArgs[intCounter]);
                    aGCHandle[intCounter] = GCHandle.Alloc(aAnsiArgs[intCounter], GCHandleType.Pinned);
                    aPtrArgs[intCounter] = aGCHandle[intCounter].AddrOfPinnedObject();
                }
                
                gchandleArgs = GCHandle.Alloc(aPtrArgs, GCHandleType.Pinned);
                intptrArgs = gchandleArgs.AddrOfPinnedObject();
                
                intReturn = -1;
                try
                {
                    intReturn = gsapi_new_instance(out intGSInstanceHandle, _objHandle);
                    if (intReturn < 0)
                    {
                        ClearParameters(ref aGCHandle, ref gchandleArgs);
                        throw new ApplicationException("I can't create a new istance of Ghostscript please verify no other istance are running!");
                    }
                }
                catch (BadImageFormatException formatException)
                {
                    ClearParameters(ref aGCHandle, ref gchandleArgs);
                    if (IntPtr.Size == 8) // 8 * 8 = 64
                    {
                        throw new ApplicationException(string.Format("The gsdll64.dll you provide is not compatible with the current architecture that is 64bit," +
                        "Please download any version above version 8.64 from the original website in the 64bit or x64 or AMD64 version!"));
                    }
                    else if (IntPtr.Size == 4) // 4 * 8 = 32
                    {
                        throw new ApplicationException(string.Format("The gsdll64.dll you provide is not compatible with the current architecture that is 32bit," +
                        "Please download any version above version 8.64 from the original website in the 32bit or x86 or i386 version!"));
                    }
                }
                catch (DllNotFoundException ex)
                {
                    ClearParameters(ref aGCHandle, ref gchandleArgs);
                    throw new ApplicationException("The gsdll64.dll wasn't found in default dlls search path" +
                        "or is not in correct version (doesn't expose the required methods). Please download " +
                        "at least the version 8.64 from the original website");
                }
                callerHandle = IntPtr.Zero;
                
                if (_bRedirectIO)
                {
                    StdioCallBack stdinCallback = new StdioCallBack(gsdll_stdin);
                    StdioCallBack stdoutCallback = new StdioCallBack(gsdll_stdout);
                    StdioCallBack stderrCallback = new StdioCallBack(gsdll_stderr);
                    intReturn = gsapi_set_stdio(intGSInstanceHandle, stdinCallback, stdoutCallback, stderrCallback);
                    if (output == null) output = new StringBuilder();
                    else output.Remove(0, output.Length);
                    myProcess = System.Diagnostics.Process.GetCurrentProcess();
                    myProcess.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(SaveOutputToImage);
                }
               
                intReturn = -1;//if nothing change it there is an error!
                //Ok now is time to call the interesting method
                try
                {
                    intReturn = gsapi_init_with_args(intGSInstanceHandle, intElementCount, intptrArgs);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(ex.Message, ex);
                }
                finally
                {
                    //free all the memory
                    ClearParameters(ref aGCHandle, ref gchandleArgs);
                    gsapi_exit(intGSInstanceHandle);//Close the istance
                    gsapi_delete_instance(intGSInstanceHandle);//delete it
                    //In case i was looking for output now stop
                    if ((myProcess != null) && (_bRedirectIO)) myProcess.OutputDataReceived -= new System.Diagnostics.DataReceivedEventHandler(SaveOutputToImage);
                }
                //Conversion was successfull if return code was 0 or e_Quit
                return (intReturn == 0) | (intReturn == e_Quit);//e_Quit = -101
            }

            /// <summary>Remove the memory allocated</summary>
            /// <param name="aGCHandle"></param>
            /// <param name="gchandleArgs"></param>
            private void ClearParameters(ref GCHandle[] aGCHandle, ref GCHandle gchandleArgs)
            {
                for (int intCounter = 0; intCounter < aGCHandle.Length; intCounter++)
                    aGCHandle[intCounter].Free();
                gchandleArgs.Free();
            }
            
            void SaveOutputToImage(object sender, System.Diagnostics.DataReceivedEventArgs e)
            {
                output.Append(e.Data);
            }

            /// <summary>This function create the list of parameters to pass to the dll with parameters given directly from the program</summary>
            /// <param name="inputFile"></param>
            /// <param name="outputFile"></param>
            /// <param name="otherParameters">The other parameters i could be interested</param>
            /// <remarks>Be very Cautious using this! code provided and modified from tchu_2000</remarks>
            /// <returns></returns>
            private string[] GetGeneratedArgs(string inputFile, string outputFile, string otherParameters)
            {
                if (!string.IsNullOrEmpty(otherParameters))
                    return GetGeneratedArgs(inputFile, outputFile, otherParameters.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries));
                else
                    return GetGeneratedArgs(inputFile, outputFile, (string[])null);
            }

            /// <summary>This function create the list of parameters to pass to the dll</summary>
            /// <param name="inputFile">the file to convert</param>
            /// <param name="outputFile">where to write the image</param>
            /// <returns>the list of the arguments</returns>
            private string[] GetGeneratedArgs(string inputFile, string outputFile, string[] presetParameters)
            {
                string[] args;
                ArrayList lstExtraArgs = new ArrayList();
                
                if ((presetParameters == null) || (presetParameters.Length == 0))
                {
                    if (_sDeviceFormat == "jpeg" && _iJPEGQuality > 0 && _iJPEGQuality < 101)
                        lstExtraArgs.Add(string.Format(GS_JpegQualityFormat, _iJPEGQuality));
                    
                    if (_iWidth > 0 && _iHeight > 0)
                        lstExtraArgs.Add(string.Format(GS_PageSizeFormat, _iWidth, _iHeight));
                    else
                    {
                        if (!string.IsNullOrEmpty(_sDefaultPageSize))
                        {
                            lstExtraArgs.Add(string.Format(GS_DefaultPaperSize, _sDefaultPageSize));
                            //It have no meaning to set it if the default page is not set!
                            if (_bForcePageSize)
                                lstExtraArgs.Add(GS_FixedMedia);
                        }
                    }

                    //not set antialiasing settings
                    if (_iGraphicsAlphaBit > 0)
                        lstExtraArgs.Add(string.Format(GS_GraphicsAlphaBits, _iGraphicsAlphaBit));
                    if (_iTextAlphaBit > 0)
                        lstExtraArgs.Add(string.Format(GS_TextAlphaBits, _iTextAlphaBit));
                    //Should i try to fit?
                    if (_bFitPage) lstExtraArgs.Add(GS_FitPage);
                    //Do i have a forced resolution?
                    if (_iResolutionX > 0)
                    {
                        if (_iResolutionY > 0)
                            lstExtraArgs.Add(String.Format(GS_ResolutionXYFormat, _iResolutionX, _iResolutionY));
                        else
                            lstExtraArgs.Add(String.Format(GS_ResolutionXFormat, _iResolutionX));
                    }
                    if (_iFirstPageToConvert > 0)
                        lstExtraArgs.Add(String.Format(GS_FirstPageFormat, _iFirstPageToConvert));
                    if (_iLastPageToConvert > 0)
                    {
                        if ((_iFirstPageToConvert > 0) && (_iFirstPageToConvert > _iLastPageToConvert))
                            throw new ArgumentOutOfRangeException(string.Format("The 1st page to convert ({0}) can't be after then the last one ({1})", _iFirstPageToConvert, _iLastPageToConvert));
                        lstExtraArgs.Add(String.Format(GS_LastPageFormat, _iLastPageToConvert));
                    }
                    //Set in how many threads i want to do the work
                    if (_iRenderingThreads > 0)
                        lstExtraArgs.Add(String.Format(GS_RenderingThreads, _iRenderingThreads));

                    //If i want to redirect write it to the standard output!
                    if (_bRedirectIO)
                    {
                        //In this case you must also use the -q switch to prevent Ghostscript
                        //from writing messages to standard output which become
                        //mixed with the intended output stream. 
                        //outputFile = GS_StandardOutputDevice;
                        //lstExtraArgs.Add(GS_QuiteOperation);
                    }
                    
                    if ((_sFontPath != null) && (_sFontPath.Count > 0))
                        lstExtraArgs.Add(String.Format(GS_FontPath, String.Join(";", _sFontPath.ToArray())));
                    if (_bDisablePlatformFonts)
                        lstExtraArgs.Add(GS_NoPlatformFonts);
                    if (_bDisableFontMap)
                        lstExtraArgs.Add(GS_NoFontMap);
                    if ((_sFontMap != null) && (_sFontMap.Count > 0))
                        lstExtraArgs.Add(String.Format(GS_FontMap, String.Join(";", _sFontMap.ToArray())));
                    if (!string.IsNullOrEmpty(_sSubstitutionFont))
                        lstExtraArgs.Add(string.Format(GS_SubstitutionFont, _sSubstitutionFont));
                    if (!string.IsNullOrEmpty(_sFCOFontFile))
                        lstExtraArgs.Add(string.Format(GS_FCOFontFile, _sFCOFontFile));
                    if (!string.IsNullOrEmpty(_sFAPIFontMap))
                    {
                        lstExtraArgs.Add(string.Format(GS_FAPIFontMap, _sFAPIFontMap));
                    }
                    if (_bDisablePrecompiledFonts)
                        lstExtraArgs.Add(GS_NoPrecompiledFonts);
                    
                    int iFixedCount = 7;//This are the mandatory options
                    int iExtraArgsCount = lstExtraArgs.Count;
                    args = new string[iFixedCount + lstExtraArgs.Count];
                    args[1] = GS_Fixed1stParameter;//"-dNOPAUSE";//I don't want interruptions
                    args[2] = GS_Fixed2ndParameter;//"-dBATCH";//stop after
                    args[3] = GS_Fixed3rdParameter;//"-dSAFER";
                    args[4] = string.Format(GS_DeviceFormat, _sDeviceFormat);//what kind of export format i should provide
                    //For a complete list watch here:
                    //http://pages.cs.wisc.edu/~ghost/doc/cvs/Devices.htm
                    //Fill the remaining parameters
                    for (int i = 0; i < iExtraArgsCount; i++)
                        args[5 + i] = (string)lstExtraArgs[i];
                }
                else
                {//3 arguments MUST be added 0 (meaningless) and at the end the output and the inputfile
                    args = new string[presetParameters.Length + 3];
                    //now use the parameters i receive (thanks CrucialBT to point this out!)
                    //and thanks to Barbara who pointout that i was skipping the last parameter
                    for (int i = 1; i <= presetParameters.Length; i++)
                        args[i] = presetParameters[i - 1];
                }
                args[0] = GS_FirstParameter;//this parameter have little real use
                //Now check if i want to update to 1 file per page i have to be sure do add % to the output filename
                if ((_didOutputToMultipleFile) && (!outputFile.Contains(GS_MultiplePageCharacter)))
                {// Thanks to Spillie to show me the error!
                    int lastDotIndex = outputFile.LastIndexOf('.');
                    if (lastDotIndex > 0)
                        outputFile = outputFile.Insert(lastDotIndex, "%d");
                }
                //Ok now save them to be shown 4 debug use
                _sParametersUsed = string.Empty;
                //Copy all the args except the 1st that is useless and the last 2
                for (int i = 1; i < args.Length - 2; i++)
                    _sParametersUsed += " " + args[i];
                //Fill outputfile and inputfile as last 2 arguments!
                args[args.Length - 2] = string.Format(GS_OutputFileFormat, outputFile);
                args[args.Length - 1] = string.Format("{0}", inputFile);

                _sParametersUsed += " " + string.Format(GS_OutputFileFormat, string.Format("\"{0}\"", outputFile))
                + " " + string.Format("\"{0}\"", inputFile);
                return args;
            }

            /// <summary>
            /// Convert a Unicode string to a null terminated Ansi string for Ghostscript.
            /// The result is stored in a byte array
            /// </summary>
            /// <param name="str">The parameter i want to convert</param>
            /// <returns>the byte array that contain the string</returns>
            private static byte[] StringToAnsiZ(string str)
            {   //This with Encoding.Default should work also with Chineese Japaneese
                //Thanks to tchu_2000 I18N related patch
                if (str == null) str = String.Empty;
                return Encoding.Default.GetBytes(str);
            }

            /// <summary>Convert a Pointer to a string to a real string</summary>
            /// <param name="strz">the pointer to the string in memory</param>
            /// <returns>The string</returns>
            public static string AnsiZtoString(IntPtr strz)
            {
                if (strz != IntPtr.Zero)
                    return Marshal.PtrToStringAnsi(strz);
                else
                    return string.Empty;
            }

            /// <summary>Check if i find the DLL that i need to continue!</summary>
            /// <returns>true if i found it</returns>
            public static bool CheckDll()
            {
                return File.Exists(GhostScriptDLLName_64);
            }
            
            public int gsdll_stdin(IntPtr intGSInstanceHandle, IntPtr strz, int intBytes)
            {
                // This is dumb code that reads one byte at a time
                // Ghostscript doesn't mind this, it is just very slow
                if (intBytes == 0)
                    return 0;
                else
                {
                    int ich = Console.Read();
                    if (ich == -1)
                        return 0; // EOF
                    else
                    {
                        byte bch = (byte)ich;
                        GCHandle gcByte = GCHandle.Alloc(bch, GCHandleType.Pinned);
                        IntPtr ptrByte = gcByte.AddrOfPinnedObject();
                        CopyMemory(strz, ptrByte, 1);
                        ptrByte = IntPtr.Zero;
                        gcByte.Free();
                        return 1;
                    }
                }
            }

            public int gsdll_stdout(IntPtr intGSInstanceHandle, IntPtr strz, int intBytes)
            {
                // If you can think of a more efficient method, please tell me!
                // We need to convert from a byte buffer to a string
                // First we create a byte array of the appropriate size
                byte[] aByte = new byte[intBytes];
                // Then we get the address of the byte array
                GCHandle gcByte = GCHandle.Alloc(aByte, GCHandleType.Pinned);
                IntPtr ptrByte = gcByte.AddrOfPinnedObject();
                // Then we copy the buffer to the byte array
                CopyMemory(ptrByte, strz, (uint)intBytes);
                // Release the address locking
                ptrByte = IntPtr.Zero;
                gcByte.Free();
                // Then we copy the byte array to a string, character by character
                string str = "";
                for (int i = 0; i < intBytes; i++)
                {
                    str += (char)aByte[i];
                }
                // Finally we output the message
                //Console.Write(str);
                output.Append(str);
                return intBytes;
                //if (intBytes > 0)
                //{
                //    Console.Write(Marshal.PtrToStringAnsi(strz));
                //}
                //return 0;
            }

            public int gsdll_stderr(IntPtr intGSInstanceHandle, IntPtr strz, int intBytes)
            {
                return gsdll_stdout(intGSInstanceHandle, strz, intBytes);
                //Console.Write(Marshal.PtrToStringAnsi(strz));
                //return intBytes;
            }
            
            public GhostScriptRevision GetRevision()
            {
                // Check revision number of Ghostscript
                int intReturn;
                GS_Revision udtGSRevInfo = new GS_Revision();
                GhostScriptRevision output;
                GCHandle gcRevision;
                gcRevision = GCHandle.Alloc(udtGSRevInfo, GCHandleType.Pinned);
                intReturn = gsapi_revision(ref udtGSRevInfo, 16);
                output.intRevision = udtGSRevInfo.intRevision;
                output.intRevisionDate = udtGSRevInfo.intRevisionDate;
                output.ProductInformation = AnsiZtoString(udtGSRevInfo.strProduct);
                output.CopyrightInformations = AnsiZtoString(udtGSRevInfo.strCopyright);
                gcRevision.Free();
                return output;
            }
        }

        /// <summary>Delegate used by Ghostscript to perform I/O operations</summary>
        /// <param name="handle"></param>
        /// <param name="strptr"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public delegate int StdioCallBack(IntPtr handle, IntPtr strptr, int count);

        /// <summary>This struct is filled with the information of the version of this ghostscript</summary>
        /// <remarks>Have the layout defined cuz i will fill it with a kernel copy memory</remarks>
        [StructLayout(LayoutKind.Sequential)]
        struct GS_Revision
        {
            public IntPtr strProduct;
            public IntPtr strCopyright;
            public int intRevision;
            public int intRevisionDate;
        }

        public struct GhostScriptRevision
        {
            public string ProductInformation;
            public string CopyrightInformations;
            public int intRevision;
            public int intRevisionDate;
        }
    }

    public partial class PDFMultiImageViewer : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets or sets an enumeration of the PDF images' URLs
        /// </summary>
        public List<string> PdfImages { get; set; }

        /// <summary>
        /// Gets or sets whether Preview only is requried in which case only the first page will be
        /// generated.
        /// </summary>
        public bool Preview { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                List<string> removeable = SessionHousekeeping();

                this.PdfImages = new List<string>();

                string pathUniq = Guid.NewGuid().ToString();
                string path = Server.MapPath("..") + @"\Images\SourcePdf\" + pathUniq + ".pdf";
                var pathImage = Server.MapPath("..") + @"\Images\Rendered\" + pathUniq + ".png";

                try
                {
                    using (var fileStream = new FileStream(path, FileMode.Create))
                    {
                        AspSession.PDFViewerMemoryStream.WriteTo(fileStream);
                    }

                    var converter = new PdfToImage.PDFConvert
                    {
                        RenderingThreads = 0, // auto (will be same as proc count)
                        OutputToMultipleFile = true,
                        FirstPageToConvert = Preview ? 1 : -1, // auto
                        LastPageToConvert = Preview ? 1 : -1, // auto
                        FitPage = true,
                        JPEGQuality = 10, // highest
                        OutputFormat = "png16m" // colour & no compression!
                    };

                    this.hiddenGeneratedPdfUrl.Value = "/Images/SourcePdf/" + pathUniq + ".pdf";
                    if (converter.Convert(path, pathImage))
                    {
                        PdfImages.AddRange(
                            Directory.GetFiles(
                                Server.MapPath(@"..\Images\Rendered\"),
                                pathUniq + "*.png",
                                SearchOption.TopDirectoryOnly).Select(x => "/Images/Rendered/" + Path.GetFileName(x)));
                    }

                    removeable.Add(this.hiddenGeneratedPdfUrl.Value);
                    if (PdfImages.Count <= 0)
                    {
                        throw new Exception("No images were output from the generation process.");
                    }
                    else
                    {
                        removeable.AddRange(PdfImages);
                    }

                    rptPdfImages.DataSource = PdfImages;
                    rptPdfImages.DataBind();

                    pnlPreview.Visible = true;
                }
                catch (Exception ex)
                {
                    pnlError.Visible = true;
                    lblError.Text = string.Format("Failed to generate the preview for the PDF. ({0})", ex.Message);
                }
            }
        }

        private List<string> SessionHousekeeping()
        {
            List<string> removeable;

            if (HttpContext.Current.Session["RemoveableFiles"] == null)
            {
                HttpContext.Current.Session["RemoveableFiles"] = new List<string>();
                removeable = (List<string>)HttpContext.Current.Session["RemoveableFiles"];
            }
            else
            {
                removeable = (List<string>)HttpContext.Current.Session["RemoveableFiles"];
                foreach (var file in removeable)
                {
                    try
                    {
                        File.Delete(Server.MapPath(file));
                    }
                    catch (Exception ex)
                    {
                        Utility.Logger.Log.ErrorFormat("Error in SessionHousekeeping() while trying to delete '{0}': {1}", file, ex.ToString());
                        Debug.Fail("Failed in SessionHousekeeping()!");
                    }
                }
            }

            return removeable;
        }
    }
}
