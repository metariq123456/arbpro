﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PDFMultiImageViewer.ascx.cs" Inherits="DGS.ArbPro.Web.Controls.PDFMultiImageViewer" %>

<asp:HiddenField ID="hiddenGeneratedPdfUrl" runat="server" ClientIDMode="Static" />

<asp:Panel ID="pnlPreview" runat="server">
    <div style="width: 100%; overflow: auto; height: 100%; position: relative;">
        <asp:Repeater ID="rptPdfImages" runat="server">
            <ItemTemplate>
                <img src="<%#Container.DataItem %>" alt="PDF preview page" style="width: 100%;" />
            </ItemTemplate>
        </asp:Repeater>
    </div>
</asp:Panel>

<asp:Panel ID="pnlError" runat="server" Visible="false">
    <asp:Label ID="lblError" runat="server" Text="Error"></asp:Label>
</asp:Panel>
