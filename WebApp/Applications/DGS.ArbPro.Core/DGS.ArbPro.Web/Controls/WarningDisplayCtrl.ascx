﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WarningDisplayCtrl.ascx.cs" Inherits="DGS.ArbPro.Web.Controls.WarningDisplayCtrl" %>

<script type="text/javascript" src="/Content/js/WarningDisplay.js?V=3.23.5"></script>

<asp:Panel ID="pnlComplaintsNotification" ClientIDMode="Static" runat="server" Style="float: left; width: 100%; background-color: #060; color: #ffff00; height: 28px; vertical-align: middle;">
    
    <asp:Panel ID="pnlComplaintsDetails" ClientIDMode="Static" runat="server" CssClass="hidden">
        <table style="border-collapse: collapse; border: none; float: left;" onclick="javascript:window.location='<%=HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + this.ResolveUrl("~/")%>/Company.aspx';">
            <tr>
                <td>
                    <img src="Images/warning.png" style="width: 24px; height: 24px;" />
                </td>
                <td style="text-decoration: underline; cursor: pointer;">
                    <span id="totalComplaints"></span>
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    <asp:Panel ID="pnlMarketingComplaints" ClientIDMode="Static" runat="server" CssClass="hidden">
        <table style="border-collapse: collapse; border: none; float: right;" onclick="javascript:window.location='<%=HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + this.ResolveUrl("~/")%>/Marketing.aspx';">
            <tr>
                <td>
                    <img src="Images/warning.png" style="width: 24px; height: 24px;" />
                </td>
                <td style="text-decoration: underline; cursor: pointer;">
                    <span id="totalReturnMarketing"></span>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Panel>
