﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PDFViewer.aspx.cs" Inherits="DGS.ArbPro.Web.Controls.PDFViewer" %>

<%@ Register Src="~/Controls/PDFMultiImageViewer.ascx" TagPrefix="uc1" TagName="PDFMultiImageViewer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>

    <form id="form1" runat="server">
    <div>
        <uc1:PDFMultiImageViewer runat="server" ID="PDFMultiImageViewer" />
    </div>
    </form>
</body>
</html>
