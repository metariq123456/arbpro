﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web.Controls
{
    public partial class PDFViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.PDFMultiImageViewer.Preview = (Request.QueryString["preview"] != null);
            
            try
            {
                /*var ms = AspSession.PDFViewerMemoryStream;
                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "inline;filename=\"ArbPro_Report.pdf\"");
                Response.BinaryWrite(ms.ToArray());
                Response.End();*/
            }
            catch (Exception)
            {
                // Do nothing
                // This just allows the silent fails necessary
                // for the multiple loads for most file types
            }
        }
    }
}