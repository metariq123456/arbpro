﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DGS.ArbPro.Web
{
    public partial class SiteMaster : MasterPage
    {
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;

        public String LoggedInUserName
        {
            get
            {
                try
                {
                    DAL.User DB = new DAL.User(System.Configuration.ConfigurationManager.AppSettings["MasterDatabaseName"]);
                   Types.User User = DB.GetForEmail(Page.User.Identity.Name);
                    return User.NameStr;
                }
                catch
                {
                    return "ArbPro User";
                }
            }
        }

        public String AppRoot
        {
            get
            {
                var p = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
                //Logger.Log.DebugFormat("Site root: {0}", p);
                return p;
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            #region XSRF
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                _antiXsrfTokenValue = requestCookie.Value;
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            else
            {
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    HttpOnly = true,
                    Value = _antiXsrfTokenValue
                };
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                {
                    responseCookie.Secure = true;
                }
                Response.Cookies.Set(responseCookie);
            }
            #endregion

            Page.PreLoad += master_Page_PreLoad;
        }

        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Set Anti-XSRF token
                ViewState[AntiXsrfTokenKey] = Page.ViewStateUserKey;
                ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
            }
            else
            {
                // Validate the Anti-XSRF token
                if ((string)ViewState[AntiXsrfTokenKey] != _antiXsrfTokenValue
                    || (string)ViewState[AntiXsrfUserNameKey] != (Context.User.Identity.Name ?? String.Empty))
                {
                    throw new InvalidOperationException("Validation of Anti-XSRF token failed.");
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //bool LoggedInElsewhere;

            //if (HttpContext.Current.User != null)
            //{
            //    // Use this if you have difficulty logging back in whilst developing: Cache["LoggedInUsers"] = new Dictionary<String, Types.User>();
            //    DAL.User DB = new DAL.User(System.Configuration.ConfigurationManager.AppSettings["MasterDatabaseName"]);
            //    var CurrentUser = DB.GetForEmail(Page.User.Identity.Name);

            //    if (CurrentUser == null) return;

            //    if (Cache["LoggedInUsers"] == null)
            //        Cache["LoggedInUsers"] = new Dictionary<String, Types.User>();

            //    Dictionary<int, string> LoggedInUsers = (Dictionary<int, string>)Cache["LoggedInUsers"];

            //    CurrentUser.IPAddress = Request.ServerVariables["REMOTE_ADDR"];


            //    if (!LoggedInUsers.ContainsKey(CurrentUser.Id))
            //    {
            //        LoggedInUsers.Add(CurrentUser.Id,Session.SessionID);
            //    }
            //    else if (LoggedInUsers[CurrentUser.Id] != Session.SessionID)
            //    {
            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage("Error=1");
            //    }

            //    Cache["LoggedInUsers"] = LoggedInUsers;

            //    var cDB = new DAL.ClientOrganisation(System.Configuration.ConfigurationManager.AppSettings["MasterDatabaseName"]);

            //    var ClientOrganisation = cDB.GetForId(CurrentUser.CompanyID);

            //    if (LoggedInUsers.Values.ToList().Count(u => u.Id == CurrentUser.Id) > 1)
            //    {
            //        Utility.Logger.Log.InfoFormat("({0}:{1}) User already logged in - Kicking...", CurrentUser.Id, CurrentUser.NameStr);

            //        LoggedInUsers.Remove(Session.SessionID);
            //        Cache["LoggedInUsers"] = LoggedInUsers;

            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage("Error=1");

            //    }
            //    else if (LoggedInUsers.Values.ToList().FindAll(
            //     (u) =>
            //     {
            //         return u.CompanyID == CurrentUser.CompanyID;
            //     }).Count > ClientOrganisation.NumLicenses)
            //    {
            //        Utility.Logger.Log.InfoFormat("({0}:{1}) Client Licenses already in use - Kicking...", CurrentUser.Id, CurrentUser.NameStr);

            //        LoggedInUsers.Remove(Session.SessionID);
            //        Cache["LoggedInUsers"] = LoggedInUsers;

            //        FormsAuthentication.SignOut();
            //        FormsAuthentication.RedirectToLoginPage("Error=2");
            //    }
            //    else
            //    {
            //        Utility.Logger.Log.DebugFormat("({0}:{1}) Visited Page: {2}", CurrentUser.Id, CurrentUser.NameStr, Page.Title);
            //    }
            //}
        }
    }
}