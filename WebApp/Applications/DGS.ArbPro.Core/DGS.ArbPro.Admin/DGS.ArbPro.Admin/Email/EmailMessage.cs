﻿using System;
using System.Collections.Generic;
using System.Web;
using Microsec.Elastic;

namespace DGS.ArbPro.Admin.Email
{
    public class EmailMessage
    {
        private List<EmailAddress> ToAddresses;
        private List<EmailAddress> CcAddresses;
        private List<EmailAddress> BccAddresses;
        private List<EmailAddress> ReplyAddresses;

        private EmailAddress FromAddress;
        private EmailAddress SysAdminAddress;

        public String Subject;
        public String BodyText;
        public String BodyHTML;

        public EmailMessage()
        {
            ToAddresses = new List<EmailAddress>();
            CcAddresses = new List<EmailAddress>();
            BccAddresses = new List<EmailAddress>();
            ReplyAddresses = new List<EmailAddress>();

            FromAddress = new EmailAddress("adminportal_noreply@arbprosoftware.com", "ArbPro Admin Portal");
            SysAdminAddress = new EmailAddress("sean.veck@microsec.co.uk", "Sean Veck");
        }

        public EmailMessage(String ToAddress, String ToName)
            : this()
        {
            ToAddresses.Add(new EmailAddress(ToAddress, ToName));
        }

        public Boolean Send()
        {
            if (String.IsNullOrEmpty(Subject))
                return false;
            if (String.IsNullOrEmpty(BodyText) && String.IsNullOrEmpty(BodyHTML))
                return false;

            //var WebConfig = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration(null);
            String ElasticURI = "https://api.elasticemail.com/";
            String ElasticUsername = "mark@arbprosoftware.com";
            String ElasticApiKey = "40dcd820-f41f-4d33-bc5f-9a97aabaf145";

            Client Client = new Client(ElasticURI, ElasticUsername, ElasticApiKey);

            List<String> SendAddresses = new List<String>();
            foreach (EmailAddress A in ToAddresses)
                SendAddresses.Add(A.Address);

            Client.SendEmail(String.Join(";", SendAddresses.ToArray()),
                Subject,
                BodyText,
                BodyHTML,
                FromAddress.Address,
                FromAddress.Name,
                null, null, new List<AttachmentInfo>());

            return true;
        }
    }
}