﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DGS.ArbPro.Admin.Email
{
    public class EmailAddress
    {
        public String Address
        {
            get;
            private set;
        }
        public String Name
        {
            get;
            private set;
        }

        public EmailAddress() : this("") { }
        public EmailAddress(String Email) : this(Email, "") { }
        public EmailAddress(String Email, String DisplayName)
        {
            Address = Email;
            Name = DisplayName;
        }

        public override String ToString()
        {
            return Name + " <" + Address + ">";
        }

        public Boolean HasEmailAddress()
        {
            return String.IsNullOrEmpty(Address);
        }
    }
}