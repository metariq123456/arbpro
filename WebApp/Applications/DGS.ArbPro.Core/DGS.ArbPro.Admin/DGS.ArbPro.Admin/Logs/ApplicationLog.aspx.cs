﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using DGS.ArbPro.DAL;

namespace DGS.ArbPro.Admin.Logs
{
    public partial class ApplicationLog : System.Web.UI.Page
    {
        private String ErrorNo
        {
            get
            {
                String Val = txtErrorNumber.Text;
                Val = Val.ToUpper().Replace("DB", "");
                int Result;
                if (int.TryParse(Val, out Result))
                    return Val;
                else
                    return "-1";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LogGrid.RowDataBound += LogGrid_RowDataBound;
            LogGrid.SelectedIndexChanging += LogGrid_SelectedIndexChanging;
            btnClearSessionFilter.Visible = int.Parse(fldSessionID.Value) > 0;

            if (!IsPostBack)
            {
                txtDateFrom.Text = DateTime.Now.AddDays(-5).ToShortDateString();
                txtDateTo.Text = DateTime.Now.ToShortDateString();
                chkEvents.Checked = true;
                chkErrors.Checked = true;

                LogDataSource.SelectParameters.Add(new Parameter("StartTime", DbType.DateTime, txtDateFrom.Text));
                LogDataSource.SelectParameters.Add(new Parameter("EndTime", DbType.DateTime, txtDateTo.Text));
                LogDataSource.SelectParameters.Add(new Parameter("Errors", DbType.Boolean, chkErrors.Checked ? "true" : "false"));
                LogDataSource.SelectParameters.Add(new Parameter("Events", DbType.Boolean, chkEvents.Checked ? "true" : "false"));
                LogDataSource.SelectParameters.Add(new Parameter("SessionID", DbType.Int32, fldSessionID.Value));
                LogDataSource.SelectParameters.Add(new Parameter("ErrorNo", DbType.Int32, ErrorNo));
                LogDataSource.Updating += LogDataSource_Updating;

                LogDataSource.DataBind();
                LogGrid.DataBind();
            }

            btnClearSessionFilter.Click += btnClearSessionFilter_Click;
            btnSearch.Click += btnSearch_Click;
        }

        void LogGrid_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            e.Cancel = true;

            fldSessionID.Value = ((DataControlFieldCell)LogGrid.Rows[e.NewSelectedIndex].Controls[2]).Text;
            LogDataSource.SelectParameters["SessionID"].DefaultValue = fldSessionID.Value;

            btnClearSessionFilter.Visible = true;

            LogDataSource.DataBind();
            LogGrid.DataBind();
        }

        void LogDataSource_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            e.Cancel = true;
            LogGrid.EditIndex = -1;
        }

        void btnSearch_Click(object sender, EventArgs e)
        {
            LogDataSource.SelectParameters["StartTime"].DefaultValue = txtDateFrom.Text;
            LogDataSource.SelectParameters["EndTime"].DefaultValue = txtDateTo.Text;
            LogDataSource.SelectParameters["Errors"].DefaultValue = chkErrors.Checked ? "true" : "false";
            LogDataSource.SelectParameters["Events"].DefaultValue = chkEvents.Checked ? "true" : "false";
            LogDataSource.SelectParameters["ErrorNo"].DefaultValue = ErrorNo;

            LogDataSource.DataBind();
            LogGrid.DataBind();
            LogGrid.EditIndex = -1;
        }

        void btnClearSessionFilter_Click(object sender, EventArgs e)
        {
            fldSessionID.Value = "-1";
            LogDataSource.SelectParameters["SessionID"].DefaultValue = "-1";

            btnClearSessionFilter.Visible = false;

            LogDataSource.DataBind();
            LogGrid.DataBind();
        }

        void LogGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (((LogEntry)e.Row.DataItem).IsError)
                {
                    if (e.Row.RowIndex % 2 == 0)
                        e.Row.BackColor = Color.IndianRed;
                    else
                        e.Row.BackColor = ControlPaint.Dark(Color.IndianRed, .02f);
                    e.Row.ForeColor = Color.White;
                }
            }
        }

        public static String Wrap(String Text)
        {
            StringBuilder SB = new StringBuilder();
            int SinceLastSpace = 0;
            int CharsPerLine = 50;

            for (int i = 0; i < Text.Length; i++)
            {
                if (Text[i] != ' ')
                    SinceLastSpace++;
                else
                    SinceLastSpace = 0;

                SB.Append(Text[i]);

                if (SinceLastSpace >= CharsPerLine)
                {
                    SinceLastSpace = 0;
                    SB.Append(' ');
                }
            }

            return SB.ToString(); ;
        }
    }
}