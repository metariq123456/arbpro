﻿<%@ Page Title="Application Log" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationLog.aspx.cs" Inherits="DGS.ArbPro.Admin.Logs.ApplicationLog" %>
<%@ Register Assembly="AjaxControlToolkit"  Namespace="AjaxControlToolkit" TagPrefix="ajax"%>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %></h1>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <asp:Panel runat="server" CssClass="Panel">
        <asp:Panel runat="server" CssClass="PanelGroup">
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:CheckBox runat="server" ID="chkEvents" CssClass="Check" Text=" Events" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:CheckBox runat="server" ID="chkErrors" CssClass="Check" Text=" Errors" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:Label runat="server" Text="Error No" CssClass="Label" AssociatedControlID="txtErrorNumber" />
                <asp:TextBox runat="server" ID="txtErrorNumber" CssClass="Text" />
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" CssClass="PanelGroup">
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:Label runat="server" CssClass="Label" AssociatedControlID="txtDateFrom" Text="Log Start" />
                <asp:TextBox runat="server" ID="txtDateFrom" CssClass="Text" />
                <ajax:CalendarExtender runat="server" 
                    CssClass="rptView cExtender" ID="clnDateFrom" 
                    Format="dd/MM/yyyy" TargetControlID="txtDateFrom" 
                    FirstDayOfWeek="Sunday" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:Label runat="server" CssClass="Label" AssociatedControlID="txtDateTo" Text="Log End" /><br />
                <asp:TextBox runat="server" ID="txtDateTo" CssClass="Text" />
                <ajax:CalendarExtender runat="server" 
                    CssClass="rptView cExtender" ID="clnDateTo" 
                    Format="dd/MM/yyyy" TargetControlID="txtDateTo" 
                    FirstDayOfWeek="Sunday" />
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" CssClass="PanelGroup">
            <asp:HiddenField runat="server" ID="fldSessionID" Value="-1" />
            <asp:Button runat="server" CssClass="Button" ID="btnClearSessionFilter" Text="All Sessions" Width="100%" />
            <asp:Button runat="server" CssClass="Button" ID="btnSearch" Text="Apply Filter" Width="100%" />
        </asp:Panel>
    </asp:Panel>
    <asp:Panel runat="server" CssClass="Panel">
        <asp:ObjectDataSource runat="server"
            ID="LogDataSource"
            TypeName="DGS.ArbPro.BL.LogHandler"
            SelectMethod="GetLog" />
        <asp:GridView runat="server"
            ID="LogGrid"
            AutoGenerateColumns="false"
            DataSourceID="LogDataSource" 
            AllowPaging="true" 
            PageSize="40"
            CssClass="mGrid" 
            DataKeyNames="ID">
            <HeaderStyle CssClass="mGridHeader" />
            <RowStyle CssClass="mGridItem" />
            <AlternatingRowStyle CssClass="mGridAltItem" />
            <FooterStyle CssClass="mGridFooter" />
            <Columns>
                <asp:BoundField DataField="ID" Visible="false" />
                <asp:BoundField DataField="IsError" Visible="false" />
                <asp:BoundField DataField="AspSessionID" HeaderText="Session" ReadOnly="true" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="AspSessionStartTime" Visible="false" />
                <asp:BoundField DataField="AspSessionEndTime" Visible="false" />
                <asp:BoundField DataField="UserAgent" Visible="false" />
                <asp:BoundField DataField="AppVersion" Visible="false" />
                <asp:BoundField DataField="EventTime" 
                     ReadOnly="true"
                    HeaderText="Event Time" HeaderStyle-Width="15%" 
                    ItemStyle-Width="15%" 
                    DataFormatString="{0:dd/MM/yy HH:mm}" />
                <asp:BoundField DataField="Event"  
                    ReadOnly="true"
                    HeaderText="Event" HeaderStyle-Width="20%" 
                    ItemStyle-Width="20%" />
                <asp:TemplateField HeaderText="Info" HeaderStyle-Width="55%" ItemStyle-Width="55%">
                    <ItemTemplate>
                        <asp:Label runat="server"
                            Text='<%# Wrap(((DGS.ArbPro.DAL.LogEntry)GetDataItem()).Info) %>' CssClass="Label"></asp:Label>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:Panel runat="server" CssClass="PanelField">
                            <asp:Label runat="server" 
                                Text='<%# Wrap(((DGS.ArbPro.DAL.LogEntry)GetDataItem()).Info) %>' 
                                CssClass="InfoLabel"></asp:Label>
                        </asp:Panel>
                        <asp:Panel runat="server" CssClass="PanelFieldWhite">
                            <asp:Label runat="server" 
                                Text='<%# Wrap(((DGS.ArbPro.DAL.LogEntry)GetDataItem()).Details) %>' 
                                CssClass="InfoLabel"></asp:Label>
                        </asp:Panel>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:CommandField ShowEditButton="true" EditText="View" UpdateText="" ShowCancelButton="true" CancelText="Collapse" ShowSelectButton="true" SelectText="Session" HeaderStyle-Width="10%" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
