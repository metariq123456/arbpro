﻿<%@ Page Language="C#" Title="Manage Admins" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageAdmins.aspx.cs" Inherits="DGS.ArbPro.Admin.ManageAdmins" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %></h1>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <asp:Panel runat="server" ID="pnlEdit" CssClass="Panel">
        <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="Button" />
        <asp:Panel runat="server" ID="pnlInfo" CssClass="PanelGroup">
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:Label runat="server" 
                    AssociatedControlID="txtUserName" 
                    Text="User Name"
                    CssClass="Label" />
                <asp:TextBox runat="server" 
                    ID="txtUserName"
                    CssClass="Text" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:Label runat="server" 
                    AssociatedControlID="txtEmail" 
                    Text="Email Address"
                    CssClass="Label" />
                <asp:TextBox runat="server" 
                    ID="txtEmail"
                    CssClass="Text" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:Label runat="server"
                    AssociatedControlID="txtComment"
                    Text="Comment"
                    CssClass="Label" />
                <asp:TextBox runat="server" 
                    ID="txtComment" 
                    CssClass="Text" />
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlSecurity" CssClass="PanelGroup">
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:Label runat="server" 
                    AssociatedControlID="btnResetPassword" 
                    Text="Password"
                    CssClass="Label" /><br />
                <asp:Button runat="server" 
                    ID="btnResetPassword" 
                    Text="Reset Password"
                    CssClass="Button" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:Label runat="server" 
                    AssociatedControlID="chkLocked" 
                    Text="Account Locked"
                    CssClass="Label" /><br />
                <asp:CheckBox runat="server" 
                    ID="chkLocked"
                    CssClass="Check" />
            </asp:Panel>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlRoles" CssClass="PanelGroup">
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:Label runat="server" AssociatedControlID="ddlRoleCategories" CssClass="Label" Text="Roles" />
                <asp:ObjectDataSource runat="server"
                    ID="RoleCategoryDataSource"
                    TypeName="DGS.ArbPro.DAL.AdminRole"
                    SelectMethod="ListCategories" />
                <asp:DropDownList runat="server"
                    ID="ddlRoleCategories"
                    DataSourceID="RoleCategoryDataSource"
                    DataValueField="value"
                    DataTextField="value"
                    CssClass="DropDown" 
                    AutoPostBack="true" />
            </asp:Panel>
            <asp:Panel runat="server" CssClass="PanelField">
                <asp:ObjectDataSource runat="server"
                    ID="RoleDataSource"
                    TypeName="DGS.ArbPro.DAL.AdminRole"
                    SelectMethod="ListRoles" />
                <asp:CheckBoxList runat="server"
                    ID="cblRoles"
                    DataSourceID="RoleDataSource"
                    DataValueField="value" 
                    DataTextField="key"
                    CssClass="Check" />
            </asp:Panel>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel runat="server" CssClass="Panel">
        <asp:ObjectDataSource runat="server" 
            ID="AdminDataSource"
            TypeName="DGS.ArbPro.BL.AdminHandler"
            SelectMethod="ListUsers"/>
        <asp:GridView runat="server"
            ID="AdminGrid"
            AutoGenerateColumns="false"
            DataSourceID="AdminDataSource" 
            AllowPaging="true" 
            PageSize="10"
            CssClass="mGrid" 
            DataKeyNames="UserId" >
            <HeaderStyle CssClass="mGridHeader" />
            <RowStyle CssClass="mGridItem" />
            <AlternatingRowStyle CssClass="mGridAltItem" />
            <FooterStyle CssClass="mGridFooter" />
            <Columns>
                <asp:BoundField DataField="UserId" Visible="false" />
                <asp:BoundField HeaderText="User Name" DataField="UserName" ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                <asp:BoundField HeaderText="Email Address" DataField="EmailAddress" ItemStyle-Width="25%" HeaderStyle-Width="25%" />
                <asp:BoundField HeaderText="Create Date" DataField="CreateDate" DataFormatString="{0:d}" ItemStyle-HorizontalAlign="center"  ItemStyle-Width="15%" HeaderStyle-Width="15%" />
                <asp:BoundField HeaderText="Comment" DataField="Comment" ItemStyle-Width="40%" HeaderStyle-Width="40%" />
                <asp:CommandField ButtonType="Link" EditText="Edit" ShowEditButton="true" ItemStyle-HorizontalAlign="center" ItemStyle-Width="5px" HeaderStyle-Width="5px" />
            </Columns>
        </asp:GridView>
        <asp:Button runat="server" ID="btnCreate" Text="Create New" CssClass="Button" />
    </asp:Panel>
</asp:Content>

