﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Web.Security;
using System.Web.UI.WebControls;
using DGS.ArbPro.Admin.Email;
using DGS.ArbPro.BL;
using DGS.ArbPro.Types;

namespace DGS.ArbPro.Admin
{
    public partial class ManageAdmins : System.Web.UI.Page
    {
        static AdminUser EditItem = null;
        static Dictionary<String, Boolean> EditRoles = new Dictionary<String, Boolean>();
        static String LastRoleValue;

        protected void Page_Load(object sender, EventArgs e)
        {
            AdminGrid.RowDataBound += AdminGrid_RowDataBound;
            AdminGrid.RowEditing += AdminGrid_RowEditing;
            ddlRoleCategories.SelectedIndexChanged += ddlRoleCategories_SelectedIndexChanged;
            cblRoles.DataBound += cblRoles_DataBound;
            btnSave.Click += btnSave_Click;
            btnCreate.Click += btnCreate_Click;
            btnResetPassword.Click += btnResetPassword_Click;

            pnlInfo.Visible = Roles.IsUserInRole("Admin Management/Edit Info");
            pnlSecurity.Visible = Roles.IsUserInRole("Admin Management/Security");
            pnlRoles.Visible = Roles.IsUserInRole("Admin Management/Roles");
            btnCreate.Visible = Roles.IsUserInRole("Admin Management/Create Users");

            if (!IsPostBack)
            {
                LastRoleValue = AdminRole.RoleCategories[0];

                AdminDataSource.SelectParameters.Add(new Parameter("ApplicationName", System.Data.DbType.String, "DGS.ArbPro.Admin"));
                AdminDataSource.SelectParameters.Add(new Parameter("PageIndex", System.Data.DbType.Int32, "0"));
                AdminDataSource.SelectParameters.Add(new Parameter("PageSize", System.Data.DbType.Int32, "100"));

                RoleDataSource.SelectParameters.Add(new Parameter("Category", System.Data.DbType.String, AdminRole.RoleCategories[0]));

                pnlEdit.Visible = false;
            }
        }

        void btnResetPassword_Click(object sender, EventArgs e)
        {
            SqlMembershipProvider Provider = (SqlMembershipProvider)Membership.Provider;
            String NewPassword = Provider.ResetPassword(EditItem.UserName, "");
            EmailMessage Message = new EmailMessage(EditItem.EmailAddress, EditItem.UserName);
            Message.Subject = "ArbPro Admin Portal - Your password has been reset";
            Message.BodyText = "Your new password is: " + NewPassword;
            Message.Send();
        }

        void btnCreate_Click(object sender, EventArgs e)
        {
            EditItem = new AdminUser();
            EditRoles.Clear();

            pnlEdit.Visible = true;

            txtUserName.Text = EditItem.UserName;
            txtEmail.Text = EditItem.EmailAddress;
            txtComment.Text = EditItem.Comment;
            chkLocked.Checked = EditItem.Locked;
        }

        void btnSave_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtUserName.Text))
                return;
            if (String.IsNullOrEmpty(txtEmail.Text))
                return;

            Boolean HasRoles = false;
            foreach (ListItem Role in cblRoles.Items)
            {
                if (EditRoles.ContainsKey(ddlRoleCategories.SelectedValue + "/" + Role.Text))
                    EditRoles[ddlRoleCategories.SelectedValue + "/" + Role.Text] = Role.Selected;
                else
                    EditRoles.Add(ddlRoleCategories.SelectedValue + "/" + Role.Text, Role.Selected);
                if (Role.Selected)
                    HasRoles = true;
            }
            if (EditRoles.ContainsKey(ddlRoleCategories.SelectedValue))
                EditRoles[ddlRoleCategories.SelectedValue] = HasRoles;
            else
                EditRoles.Add(ddlRoleCategories.SelectedValue, HasRoles);

            EditItem.UserName = txtUserName.Text;
            EditItem.EmailAddress = txtEmail.Text;
            EditItem.Comment = txtComment.Text;

            if (String.IsNullOrEmpty(EditItem.UserId))
            {
                SqlMembershipProvider Provider = (SqlMembershipProvider)Membership.Provider;
                MembershipCreateStatus Status;
                String NewPassword = Provider.GeneratePassword();
                MembershipUser User = Provider.CreateUser(EditItem.UserName, NewPassword, EditItem.UserName, null, null, true, null, out Status);
                if (Status == MembershipCreateStatus.Success)
                {
                    EmailMessage Message = new EmailMessage(EditItem.EmailAddress, EditItem.UserName);
                    Message.Subject = "ArbPro Admin Portal - Your account has been created";
                    Message.BodyText = "Please log on with the following credentials - Username: " + EditItem.UserName + " - Password: " + NewPassword; ;
                    Message.Send();
                }
            }
            else
            {
                AdminHandler.SaveAdmin("DGS.ArbPro.Admin", EditItem);
            }

            foreach (var Role in EditRoles)
            {
                if (Role.Value)
                {
                    if (!Roles.IsUserInRole(EditItem.UserName, Role.Key))
                        Roles.AddUserToRole(EditItem.UserName, Role.Key);
                }
                else
                {
                    if (Roles.IsUserInRole(EditItem.UserName, Role.Key))
                        Roles.RemoveUserFromRole(EditItem.UserName, Role.Key);
                }
            }

            if (!chkLocked.Checked)
                AdminHandler.UnlockAdmin("DGS.ArbPro.Admin", EditItem);
            else
                AdminHandler.LockAdmin("DGS.ArbPro.Admin", EditItem);


            AdminDataSource.DataBind();
            AdminGrid.DataBind();
        }

        void cblRoles_DataBound(object sender, EventArgs e)
        {
            foreach (var Role in AdminRole.ListRoles(ddlRoleCategories.SelectedValue))
            {
                if (!String.IsNullOrEmpty(EditItem.UserName))
                {
                    if (EditRoles.ContainsKey(ddlRoleCategories.SelectedValue + "/" + Role.Key))
                    {
                        cblRoles.Items.FindByText(Role.Key).Selected = EditRoles[ddlRoleCategories.SelectedValue + "/" + Role.Key];
                    }
                    else if (Roles.IsUserInRole(EditItem.UserName, ddlRoleCategories.SelectedValue + "/" + Role.Key))
                    {
                        cblRoles.Items.FindByText(Role.Key).Selected = true;
                    }
                }
                else
                {
                    cblRoles.Items.FindByText(Role.Key).Selected = false;
                }
            }
        }

        void ddlRoleCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            Boolean HasRoles = false;
            foreach (ListItem Role in cblRoles.Items)
            {
                if (EditRoles.ContainsKey(LastRoleValue + "/" + Role.Text))
                    EditRoles[LastRoleValue + "/" + Role.Text] = Role.Selected;
                else
                    EditRoles.Add(LastRoleValue + "/" + Role.Text, Role.Selected);
                if (Role.Selected)
                    HasRoles = true;
            }
            if (EditRoles.ContainsKey(LastRoleValue))
                EditRoles[LastRoleValue] = HasRoles;
            else
                EditRoles.Add(LastRoleValue, HasRoles);

            LastRoleValue = ddlRoleCategories.SelectedValue;
            RoleDataSource.SelectParameters["Category"].DefaultValue = ddlRoleCategories.SelectedValue;
        }

        void AdminGrid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true; // Stop the built-in row editing. We need to handle this ourselves.
            EditItem = AdminHandler.GetUserForId(AdminGrid.DataKeys[e.NewEditIndex]["UserId"].ToString());
            EditRoles = new Dictionary<String, Boolean>();

            pnlEdit.Visible = true;

            txtUserName.Text = EditItem.UserName;
            txtEmail.Text = EditItem.EmailAddress;
            txtComment.Text = EditItem.Comment;
            chkLocked.Checked = EditItem.Locked;
        }

        void AdminGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            AdminUser Item = (AdminUser)e.Row.DataItem;
            if (Item != null)
            {
                if (Item.Locked)
                {
                    e.Row.BackColor = Color.IndianRed;
                    e.Row.ForeColor = Color.White;
                }
            }
        }
    }
}
