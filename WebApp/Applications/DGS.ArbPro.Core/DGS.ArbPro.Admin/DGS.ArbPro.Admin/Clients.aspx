﻿<%@ Page Title="Clients" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Clients.aspx.cs" Inherits="DGS.ArbPro.Admin.Clients" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                <h1><%: Title %></h1>
            </hgroup>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <asp:Panel runat="server" ID="pnlEdit" CssClass="Panel">
        <asp:Panel runat="server" ID="pnlClient" CssClass="Panel" Visible="false" Width="60%" BorderWidth="1px">
            <asp:Panel runat="server" CssClass="PanelGroup" Width="40%">
                <asp:Panel runat="server" CssClass="PanelField">
                    <asp:Label runat="server" Text="Name" CssClass="Label" AssociatedControlID="txtClientName" />
                    <asp:TextBox runat="server" ID="txtClientName" CssClass="Text" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="PanelField">
                    <asp:CheckBox runat="server" ID="chkClientEnabled" Text=" Enabled" CssClass="Check" /><br />
                    <asp:CheckBox runat="server" ID="chkClientMobile" Text=" Mobile" CssClass="Check" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="PanelField" ID="pnlClientUsers">
                    <asp:ObjectDataSource runat="server"
                        ID="UserDataSource"
                        TypeName="DGS.ArbPro.BL.ClientOrganisationHandler"
                        SelectMethod="ListUsers" />
                    <asp:Label runat="server" Text="Users" CssClass="Label" style="font-weight: bold;" /><br />
                    <asp:DropDownList runat="server"
                        ID="ddlClientUser"
                        DataSourceID="UserDataSource"
                        DataTextField="Name" 
                        DataValueField="ID" 
                        AutoPostBack="true" 
                        Width="100%" />
                    <asp:Button runat="server" ID="btnAddUser" CssClass="Button" Text="New User" style="float: right" />
                </asp:Panel>
            </asp:Panel>
            <asp:Panel ID="Panel1" runat="server" CssClass="PanelGroup" Width="40%">
                <asp:Panel runat="server" CssClass="PanelField" style="float: left;">
                    <asp:Label runat="server" Text="Address" CssClass="Label" AssociatedControlID="txtClientAddress1" />
                    <asp:TextBox runat="server" ID="txtClientAddress1" CssClass="Text" />
                    <asp:TextBox runat="server" ID="txtClientAddress2" CssClass="Text" />
                    <asp:TextBox runat="server" ID="txtClientTown" CssClass="Text" />
                    <asp:TextBox runat="server" ID="txtClientCountyOrState" CssClass="Text" />
                    <asp:TextBox runat="server" ID="txtClientPostcodeOrZip" CssClass="Text" />
                    <asp:ObjectDataSource runat="server" 
                        ID="CountryDataSource"
                        TypeName="DGS.ArbPro.BL.ClientOrganisationHandler"
                        SelectMethod="ListCountries" />
                    <asp:DropDownList runat="server"
                        ID="ddlClientCountry"
                        DataSourceID="CountryDataSource"
                        DataTextField="Name"
                        DataValueField="ID" 
                        Width="100%"/>
                </asp:Panel>
            </asp:Panel>
            <asp:Button runat="server" ID="btnSaveClient" CssClass="Button" Text="Save" />
        </asp:Panel>
        <asp:Panel runat="server" Visible="false" ID="pnlUser" CssClass="Panel" Width="30%" BorderWidth="1px">
            <asp:Panel runat="server" CssClass="PanelGroup" Width="100%">
                <asp:Panel runat="server" CssClass="PanelField">
                    <asp:ObjectDataSource runat="server"
                        ID="PrefixDataSource"
                        TypeName="DGS.ArbPro.BL.ClientOrganisationHandler"
                        SelectMethod="ListPrefixes" />
                    <asp:Label runat="server" Text="Name" CssClass="Label" AssociatedControlID="txtUserFirstName" /><br />
                    <asp:DropDownList runat="server"
                        ID="ddlUserPrefix"
                        DataSourceID ="PrefixDataSource" 
                        DataTextField="Prefix" 
                        DataValueField="ID" 
                        Width="20%" />
                    <asp:TextBox runat="server" ID="txtUserFirstName" CssClass="Text" Width="35%" />
                    <asp:TextBox runat="server" ID="txtUserLastName" CssClass="Text" Width="35%" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="PanelField">
                    <asp:Label runat="server" Text="Job Title" CssClass="Label" AssociatedControlID="txtUserPosition" />
                    <asp:TextBox runat="server" ID="txtUserPosition" CssClass="Text" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="PanelField">
                    <asp:Label ID="Label1" runat="server" Text="Email" CssClass="Label" AssociatedControlID="txtUserEmail" />
                    <asp:TextBox runat="server" ID="txtUserEmail" CssClass="Text" Width="90%" />
                    <asp:Label runat="server" Text="Telephone" CssClass="Label" AssociatedControlID="txtUserTelephone" />
                    <asp:TextBox runat="server" ID="txtUserTelephone" CssClass="Text" Width="90%" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="PanelField">
                    <asp:CheckBox runat="server" ID="chkUserEnabled" Text=" Enabled" CssClass="Check" /><br />
                    <asp:CheckBox runat="server" ID="chkUserMobile" Text=" Mobile" CssClass="Check" /><br />
                    <asp:CheckBox runat="server" ID="chkUserTsAndCs" Text=" Ts&Cs Accepted" CssClass="Check" Enabled="false" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="PanelField">
                    <asp:Button runat="server" ID="btnResetPassword" Text="Reset Password" CssClass="Button" Width="70%" />
                </asp:Panel>
                <asp:Panel runat="server" CssClass="PanelField" ID="pnlUnlockUser">
                    <asp:Button runat="server" ID="btnUnlockUser" Text="Unlock Account" CssClass="Button" Width="70%" BackColor="Red"/>
                </asp:Panel>
            </asp:Panel>
            <asp:Button runat="server" ID="btnSaveUser" CssClass="Button" Text="Save" />
        </asp:Panel>
        <asp:Button runat="server" ID="btnAddClient" CssClass="Button" Text="New Client" />
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlGrid" CssClass="Panel">
        <asp:ObjectDataSource runat="server"
            ID="ClientDataSource"
            TypeName="DGS.ArbPro.BL.ClientOrganisationHandler"
            SelectMethod="ListClients" />
        <asp:GridView runat="server"
            ID="ClientGrid"
            AutoGenerateColumns="false" 
            DataSourceID="ClientDataSource" 
            AllowPaging="true" 
            PageSize="20" 
            CssClass="mGrid" 
            DataKeyNames="ID">
            <HeaderStyle CssClass="mGridHeader" />
            <RowStyle CssClass="mGridItem" />
            <AlternatingRowStyle CssClass="mGridAltItem" />
            <FooterStyle CssClass="mGridFooter" />
            <Columns>
                <asp:BoundField DataField="ID" Visible="false" />
                <asp:BoundField DataField="Name" HeaderText="Name" HeaderStyle-Width="25%" ItemStyle-Width="25%" />
                <asp:BoundField DataField="Address1" Visible="false" />
                <asp:BoundField DataField="Address2" Visible="false" />
                <asp:BoundField DataField="Town" Visible="false" />
                <asp:BoundField DataField="CountyOrState" Visible="false" />
                <asp:BoundField DataField="PostcodeOrZip" HeaderText="Post Code"  HeaderStyle-Width="15%" ItemStyle-Width="15%" />
                <asp:BoundField DataField="CountryID" Visible="false" />
                <asp:BoundField DataField="Country" HeaderText="Country" HeaderStyle-Width="40%" ItemStyle-Width="40%" />
                <asp:CheckboxField DataField="UserAccountsEnabled" HeaderText="Enabled" HeaderStyle-Width="10%" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" />
                <asp:CheckBoxField DataField="MobileEditionEnabled" HeaderText="Mobile" HeaderStyle-Width="10%" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" />
                <asp:CommandField ShowEditButton="true" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
