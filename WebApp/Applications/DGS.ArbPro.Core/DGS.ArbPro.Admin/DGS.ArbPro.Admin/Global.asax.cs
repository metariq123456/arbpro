﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using DGS.ArbPro.Admin;
using DGS.ArbPro.DAL;

namespace DGS.ArbPro.Admin
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterOpenAuth();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            foreach (var Category in AdminRole.CategorisedRoles)
            {
                if (!Roles.RoleExists(Category.Key))
                    Roles.CreateRole(Category.Key);

                foreach (String Role in Category.Value)
                {
                    if (!Roles.RoleExists(Category.Key + "/" + Role))
                        Roles.CreateRole(Category.Key + "/" + Role);
                }
            }
        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }
    }
}

