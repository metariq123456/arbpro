﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DGS.ArbPro.BL;
using DGS.ArbPro.DAL;

namespace DGS.ArbPro.Admin
{
    public partial class Clients : System.Web.UI.Page
    {
        static ClientOrganisation EditClient;
        static ClientUser EditUser;

        protected void Page_Load(object sender, EventArgs e)
        {
            ClientGrid.RowEditing += ClientGrid_RowEditing;
            ddlClientUser.SelectedIndexChanged += ddlClientUser_SelectedIndexChanged;
            btnAddClient.Click += btnAddClient_Click;
            btnAddUser.Click += btnAddUser_Click;
            btnSaveClient.Click += btnSaveClient_Click;
            btnSaveUser.Click += btnSaveUser_Click;
            btnUnlockUser.Click += btnUnlockUser_Click;
            btnResetPassword.Click += btnResetPassword_Click;

            if (!User.IsInRole("client management/edit clients"))
            {
                ((CommandField)ClientGrid.Columns[11]).EditText = "View";
            }
            btnAddClient.Visible = User.IsInRole("client management/create clients");

            if (!IsPostBack)
            {
                UserDataSource.SelectParameters.Add(new Parameter("ClientId", System.Data.DbType.Int32, "-1"));
            }
        }

        void btnResetPassword_Click(object sender, EventArgs e)
        {
            String Password = String.Format("ArbPro{0}{1}", EditUser.FirstName, new Random().Next(0, 100));
            new ClientOrganisationHandler().ResetUserPassword(EditUser, Password);
            Page.ClientScript.RegisterStartupScript(
                    this.GetType(),
                    Guid.NewGuid().ToString(),
                    String.Format("<script language=JavaScript>alert('{0}s password has been set to \"{1}\"');</script>", EditUser.FirstName, Password));
        }

        void btnUnlockUser_Click(object sender, EventArgs e)
        {
            var Handler = new ClientOrganisationHandler();
            Handler.UnlockAccount(EditUser);
            EditUser = Handler.GetUser(int.Parse(ddlClientUser.SelectedValue)); ;
            LoadUserDetails();
        }

        void btnSaveUser_Click(object sender, EventArgs e)
        {
            EditUser.Email = txtUserEmail.Text;
            EditUser.ClientOrganisation = EditClient.ID;
            EditUser.Position = txtUserPosition.Text;
            EditUser.NamePrefixID = int.Parse(ddlUserPrefix.SelectedValue);
            EditUser.FirstName = txtUserFirstName.Text;
            EditUser.LastName = txtUserLastName.Text;
            EditUser.Telephone = txtUserTelephone.Text;
            EditUser.Enabled = chkUserEnabled.Checked;
            EditUser.Mobile = chkUserMobile.Checked;

            if (EditUser.ID == -1)
            {
                String Password = String.Format("ArbPro{0}{1}", EditUser.FirstName, new Random().Next(0, 100));
                EditUser.ID = new ClientOrganisationHandler().ClientUserAdd(EditUser, Password);
                Page.ClientScript.RegisterStartupScript(
                    this.GetType(),
                    Guid.NewGuid().ToString(),
                    String.Format("<script language=JavaScript>alert('{0}s password has been set to \"{1}\"');</script>", EditUser.FirstName, Password));
            }
            else
            {
                new ClientOrganisationHandler().ClientUserSave(EditUser);
            }

            UserDataSource.DataBind();
            ddlClientUser.DataBind();
            ddlClientUser.SelectedValue = EditUser.ID.ToString();
        }

        void btnSaveClient_Click(object sender, EventArgs e)
        {
            EditClient.OrganisationName = txtClientName.Text;
            EditClient.Address1 = txtClientAddress1.Text;
            EditClient.Address2 = txtClientAddress2.Text;
            EditClient.Town = txtClientTown.Text;
            EditClient.CountyOrState = txtClientCountyOrState.Text;
            EditClient.PostcodeOrZip = txtClientPostcodeOrZip.Text;
            EditClient.CountryID = int.Parse(ddlClientCountry.SelectedValue);
            EditClient.UserAccountsEnabled = chkClientEnabled.Checked;
            EditClient.MobileEditionEnabled = chkClientMobile.Checked;

            if (EditClient.ID == -1)
            {
                btnAddUser.Enabled = true;
                EditClient.ID = new ClientOrganisationHandler().ClientOrganisationAdd(EditClient);
            }
            else
            {
                new ClientOrganisationHandler().ClientOrganisationSave(EditClient);
            }

            ClientDataSource.DataBind();
            ClientGrid.DataBind();
            UserDataSource.SelectParameters["ClientId"].DefaultValue = EditClient.ID.ToString();
            UserDataSource.DataBind();
            ddlClientUser.DataBind();
        }

        void btnAddUser_Click(object sender, EventArgs e)
        {
            EditUser = new ClientUser();
            EditUser.ID = -1;
            EditUser.NamePrefixID = 1;

            pnlUser.Visible = true;

            LoadUserDetails();
        }

        void btnAddClient_Click(object sender, EventArgs e)
        {
            EditClient = new ClientOrganisation();
            EditClient.ID = -1;
            EditClient.CountryID = 230;

            pnlClient.Visible = true;
            pnlUser.Visible = false;
            btnAddUser.Enabled = false;

            LoadClientDetails();
        }

        void ddlClientUser_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (int.Parse(ddlClientUser.SelectedValue) != -1)
            {
                pnlUser.Visible = true;
                EditUser = new ClientOrganisationHandler().GetUser(int.Parse(ddlClientUser.SelectedValue));
                LoadUserDetails();
            }
            else
            {
                EditUser = null;
                pnlUser.Visible = false;
            }
        }

        void ClientGrid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            e.Cancel = true; // We'll do the editing - thanks!

            pnlClient.Visible = true;
            pnlUser.Visible = false;

            EditClient = new ClientOrganisationHandler().GetClient(int.Parse(ClientGrid.DataKeys[e.NewEditIndex]["ID"].ToString()));

            LoadClientDetails();
        }

        void LoadUserDetails()
        {
            ddlUserPrefix.Enabled = User.IsInRole("client management/edit users");
            txtUserFirstName.Enabled = User.IsInRole("client management/edit users");
            txtUserLastName.Enabled = User.IsInRole("client management/edit users");
            txtUserTelephone.Enabled = User.IsInRole("client management/edit users");
            txtUserEmail.Enabled = User.IsInRole("client management/edit users");
            txtUserPosition.Enabled = User.IsInRole("client management/edit users");
            chkUserEnabled.Enabled = User.IsInRole("client management/edit users");
            chkUserMobile.Enabled = User.IsInRole("client management/edit users");
            btnSaveUser.Visible = User.IsInRole("client management/edit users");
            btnResetPassword.Visible = User.IsInRole("client management/support");

            ddlUserPrefix.SelectedValue = EditUser.NamePrefixID.ToString();

            txtUserFirstName.Text = EditUser.FirstName;
            txtUserLastName.Text = EditUser.LastName;
            txtUserEmail.Text = EditUser.Email;
            txtUserTelephone.Text = EditUser.Telephone;
            txtUserPosition.Text = EditUser.Position;

            chkUserEnabled.Checked = EditUser.Enabled;
            chkUserMobile.Checked = EditUser.Mobile;
            chkUserTsAndCs.Checked = EditUser.TsAndCsAccepted;

            pnlUnlockUser.Visible = User.IsInRole("client management/support") && EditUser.AuthFailures >= 3;
        }

        void LoadClientDetails()
        {
            txtClientName.Enabled = User.IsInRole("client management/edit clients");
            txtClientAddress1.Enabled = User.IsInRole("client management/edit clients");
            txtClientAddress2.Enabled = User.IsInRole("client management/edit clients");
            txtClientTown.Enabled = User.IsInRole("client management/edit clients"); ;
            txtClientCountyOrState.Enabled = User.IsInRole("client management/edit clients");
            txtClientPostcodeOrZip.Enabled = User.IsInRole("client management/edit clients");
            chkClientEnabled.Enabled = User.IsInRole("client management/edit clients");
            chkClientMobile.Enabled = User.IsInRole("client management/edit clients");
            ddlClientCountry.Enabled = User.IsInRole("client management/edit clients");
            btnSaveClient.Visible = User.IsInRole("client management/edit clients");
            btnAddUser.Visible = User.IsInRole("client management/create users");

            txtClientName.Text = EditClient.OrganisationName;
            txtClientAddress1.Text = EditClient.Address1;
            txtClientAddress2.Text = EditClient.Address2;
            txtClientTown.Text = EditClient.Town;
            txtClientCountyOrState.Text = EditClient.CountyOrState;
            txtClientPostcodeOrZip.Text = EditClient.PostcodeOrZip;
            ddlClientCountry.SelectedValue = EditClient.CountryID.ToString();
            chkClientEnabled.Checked = EditClient.UserAccountsEnabled;
            chkClientMobile.Checked = EditClient.MobileEditionEnabled;

            UserDataSource.SelectParameters["ClientId"].DefaultValue = EditClient.ID.ToString();
            UserDataSource.DataBind();
            ddlClientUser.DataBind();
        }
    }
}