﻿using System;
using System.Diagnostics;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace DGS.ArbPro.Tests
{
    [TestClass]
    [DeploymentItem("app.config")]
    public class AccountTests
    {

        [TestMethod]
        public void SetPassword()
        {
            try
            {
                var DB = new DAL.User(GetStringConfigSetting("MasterDatabase"));
                var User = DB.GetForEmail("kate1@microsec.co.uk");

                String PW = User.HashPassword("ArbPr012",true);
                User.PasswordHashed = PW;
                User.AuthenticationFailures = 0;
                if(DB.Save(User) < 1)
                    Assert.Fail();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestUnlockUser()
        {
            try
            {
                var DB = new DAL.User(GetStringConfigSetting("MasterDatabase"));
                var User = DB.GetForEmail("jon@microsec.co.uk");

                User.Unlock();
                String PW = User.HashPassword("ArbPr012");
                User.PasswordHashed = PW;
                User.AuthenticationFailures = 0;
                DB.Save(User);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestPwdResetGenerate()
        {
            try
            {
                string Guid;
                Types.User User;
                var DB = new DAL.User(GetStringConfigSetting("MasterDatabase"));
                User = DB.GetForEmail("jon1@microsec.co.uk");

                Guid = DB.GenerateResetGuid(User);
                Assert.IsTrue(Guid != "","New Guid generated");

                // We should be able to find the user for the guid
                User = DB.GetForResetGuid(Guid);
                Assert.IsNotNull(User,"Can find user for Guid");

                // We should not be able to find the user for the guid when the timeout has expired
                User = DB.GetForResetGuid(Guid);
                Assert.IsNull(User, "Cannot find user for expired Guid");

                // We should not be able to find the user for a bad guid
                User = DB.GetForResetGuid("bob");
                Assert.IsNull(User,"Cannot find user for bad Guid");

                // We should be able to clear the Guid
                User = DB.GetForEmail("jon@microsec.co.uk");
                Assert.IsNotNull(User);
                Assert.IsTrue(DB.ClearResetGuid(User));

                // We should now not be able to find the user with the guid
                User = DB.GetForResetGuid(Guid);
                Assert.IsNull(User, "Cannot find user for Guid after it's been cleared");


            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TestPasswordMatch()
        {
            try
            {
                // First set the password to something we know
                string Password = "ArbPr012";
                Types.User MyUser;
                var DB = new DAL.User(GetStringConfigSetting("MasterDatabase"));
                MyUser = DB.GetForEmail("sean1@microsec.co.uk");

                String PW = MyUser.HashPassword(Password);
                MyUser.PasswordHashed = PW;
                DB.Save(MyUser);

                // Now check that we can successfully validate the password
                Assert.IsTrue(MyUser.PasswordMatch(Password));

                // Also check that an incorrect password fails
                Assert.IsFalse(MyUser.PasswordMatch("blobby"));
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void AddUser()
        {
            string EmailAddress = "dumby@microsec.co.uk";
            Types.User User;
            try
            {
                var DB = new DAL.User(GetStringConfigSetting("MasterDatabase"));
                User = DB.GetForEmail(EmailAddress);

                // make sure we haven't already got a user for this email address
                Assert.IsTrue(User == null, string.Format("user {0} already exists", EmailAddress));

                User = new Types.User();
                User.EmailAddress = EmailAddress;
                User.FirstName = "dumnby";
                User.LastName = "dumnby";
                User.CompanyID = 1;



                String PW = User.HashPassword("aaaaaa");
                User.PasswordHashed = PW;
                Assert.IsTrue(DB.Save(User) > 0, "Save failed");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }


        private string GetStringConfigSetting(string key)
        {
            string value = ConfigurationManager.AppSettings[key];
            Assert.IsFalse(String.IsNullOrEmpty(value), "No Setting for output directory in App.Config found.");
            return value;
        }
    }
}
