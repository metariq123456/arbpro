﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SmsTests.cs" company="">
//   
// </copyright>
// <summary>
//   The sms tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DGS.ArbPro.Tests
{
    using System.Diagnostics;

    using Elastic;

    /// <summary>
    /// The sms tests.
    /// </summary>
    [TestClass]
    [DeploymentItem("app.config")]
    public class SmsTests
    {
        #region Test Methods

        #region Quote

        /// <summary>
        /// The uk number without spaces.
        /// </summary>
        [TestMethod]
        public void UkNumberWithoutSpaces()
        {
            try
            {
                var CurrentClient = new Microsec.Elastic.Client(
                    "https://api.elasticemail.com/", 
                    "james@microsec.co.uk", 
                    "71929ad1-2f3a-488a-9d60-c42590b87832");
                CurrentClient.SendSms("07479888838", "This is a test message");
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// The uk number with spaces.
        /// </summary>
        [TestMethod]
        public void UkNumberWithSpaces()
        {
            try
            {
                var CurrentClient = new Microsec.Elastic.Client(
                    "https://api.elasticemail.com/", 
                    "james@microsec.co.uk", 
                    "71929ad1-2f3a-488a-9d60-c42590b87832");
                CurrentClient.SendSms("074 79 88 88 38", "This is a test message");
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// The number with country code and spaces.
        /// </summary>
        [TestMethod]
        public void NumberWithCountryCodeAndSpaces()
        {
            try
            {
                var CurrentClient = new Microsec.Elastic.Client(
                    "https://api.elasticemail.com/", 
                    "james@microsec.co.uk", 
                    "71929ad1-2f3a-488a-9d60-c42590b87832");
                CurrentClient.SendSms("+44 7479 88 88 38", "This is a test message");
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// The number with 0 and country code and spaces.
        /// </summary>
        [TestMethod]
        public void CanSendWithNumberWith0AndCountryCodeAndSpaces()
        {
            try
            {
                var CurrentClient = new Microsec.Elastic.Client(
                    "https://api.elasticemail.com/", 
                    "james@microsec.co.uk", 
                    "71929ad1-2f3a-488a-9d60-c42590b87832");
                CurrentClient.SendSms("+44 074 79 88 88 38", "This is a test message");
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// The invalid number should raise exception.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(Elastic.InvalidNumberException))]
        public void InvalidNumberShouldRaiseException()
        {
            var CurrentClient = new Microsec.Elastic.Client(
                "https://api.elasticemail.com/", 
                "james@microsec.co.uk", 
                "71929ad1-2f3a-488a-9d60-c42590b87832");
            CurrentClient.SendSms("o7479aq8838", "This is a test message");
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void CanSendWithSeparateParameterCountryCodeAnd07Number()
        {
            try
            {
                var CurrentClient = new Microsec.Elastic.Client(
                    "https://api.elasticemail.com/",
                    "james@microsec.co.uk",
                    "71929ad1-2f3a-488a-9d60-c42590b87832");
                CurrentClient.SendSms(" 074 79 88 88 38", "This is a test message", "+44");
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CanSendWith00InsteadOfPlusForCountryCode()
        {
            try
            {
                var CurrentClient = new Microsec.Elastic.Client(
                    "https://api.elasticemail.com/",
                    "james@microsec.co.uk",
                    "71929ad1-2f3a-488a-9d60-c42590b87832");
                CurrentClient.SendSms(" 074 79 88 88 38", "This is a test message", "0044");
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CanSendWith00AndBracketsAndDashesThroughoutEntireNumberAndWithCustomCountryCode()
        {
            try
            {
                var CurrentClient = new Microsec.Elastic.Client(
                    "https://api.elasticemail.com/",
                    "james@microsec.co.uk",
                    "71929ad1-2f3a-488a-9d60-c42590b87832");
                CurrentClient.SendSms(" (0)74 - 79 - 88 - 88 - 38", "This is a test message", "(0044)");
                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        
        #endregion

        #endregion
    }
}