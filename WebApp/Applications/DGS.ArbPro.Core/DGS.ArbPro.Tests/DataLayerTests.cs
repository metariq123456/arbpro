﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Diagnostics;
using DGS.ArbPro.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DGS.ArbPro.Tests
{
    using DGS.ArbPro.BL;
    using DGS.ArbPro.Types.Enums;

    [TestClass]
    [DeploymentItem("app.config")]
    public class DataLayerTests
    {
        int _Seed = DateTime.Now.Millisecond;

        #region Client Search Tests
        [TestMethod]
        public void ClientNameSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForNameLike("Ki");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientAddressSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForAddressLike("ie");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientPostCodeSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForPostCodeLike("s");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientEmailSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForEmailLike("@");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientTelephoneSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForTelephoneLike("55");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(GetStringConfigSetting("ClientDatabase"));
                SearchResult r = dbContext.GetForClientId(1);
                Assert.IsTrue(r != null);
                Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                Assert.IsTrue(r.Id > 0);
                Assert.IsTrue(r.TitleGet().Length > 0);
                Assert.IsTrue(r.DetailGet().Length > 0);
                OutputSearchResult(r);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientAllSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<Types.ClientSearchResult> results = dbContext.GetAll();
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (Types.ClientSearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputClientSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region Invoice Search Tests
        [TestMethod]
        public void InvoiceClientIdSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForInvClientId(76);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InvoiceClientNameSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForClientNameLike("Se");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InvoiceVersionNumberSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForInoiceVersionLike("455");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InvoiceQuoteIdSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForQuoteId(1117);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InvoiceStatusSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForStatus(Types.Enums.InvoiceStatusEnum.Sent);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InvoiceNotStatusSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForStatusNot(Types.Enums.InvoiceStatusEnum.Sent);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region Quote Search Tests
        [TestMethod]
        public void QuoteClientIdSearch()
        {
            try
            {
                DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForClientId(1);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Quote);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void QuoteClientNameSearch()
        {
            try
            {
                DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForNameLike("fr");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Quote);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void QuoteSiteAddressSearch()
        {
            try
            {
                DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForSiteAddressLike("Le");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Quote);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void QuoteStatusSearch()
        {
            try
            {
                DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForStatus(Types.Enums.QuoteStatusEnum.Accepted);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Quote);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void QuoteNotStatusSearch()
        {
            try
            {
                DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForStatus(Types.Enums.QuoteStatusEnum.Accepted);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Quote);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region ClientContact Search Tests
        [TestMethod]
        public void ContactClientIdSearch()
        {
            try
            {
                DAL.ContactSearchResult dbContext = new DAL.ContactSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForClientId(1);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Contact);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ContactNameSearch()
        {
            try
            {
                DAL.ContactSearchResult dbContext = new DAL.ContactSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForNameLike("e");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Contact);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ContactDetailsSearch()
        {
            try
            {
                DAL.ContactSearchResult dbContext = new DAL.ContactSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetForContactDetailsLike("e");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Contact);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region Complaint
        [TestMethod]
        public void ComplaintInsert()
        {
            try
            {
                DAL.Complaint dbContext = new DAL.Complaint(GetStringConfigSetting("ClientDatabase"));
                int result = dbContext.Save(this.GetComplaintTestInstance(_Seed));
                Assert.IsTrue(result > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }

        }

        [TestMethod]
        public void ComplaintUpdate()
        {
            try
            {
                Types.Complaint testObject = this.GetComplaintTestInstance(_Seed);
                DAL.Complaint dbContext = new DAL.Complaint(GetStringConfigSetting("ClientDatabase"));
                int result = dbContext.Save(testObject);
                Debug.WriteLine("----------- ComplaintUpdate Seed# {0} -----------", _Seed);
                this.OutputTestModel(testObject, "Initial object");
                Assert.IsTrue(result > 0);
                testObject.Id = result;
                testObject.Closed = true;
                result = dbContext.Save(testObject);
                Assert.IsTrue(result > 0);
                this.OutputTestModel(testObject, "Updated object");
                testObject = dbContext.GetForId(result);
                Assert.IsTrue( testObject.DateClosed.ToShortDateString() == DateTime.Today.ToShortDateString());
                this.OutputTestModel(testObject, "Final State");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }

        }

        [TestMethod]
        public void ComplaintSearchAll()
        {
            try
            {
                DAL.ComplaintSearchResult dbContext = new DAL.ComplaintSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetAllGeneric();
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Complaint);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ComplaintSearchForClosed()
        {
            try
            {
                //NOTE: Precodition - the database must have at least 1 complaint that has been closed
                DAL.ComplaintSearchResult dbContext = new DAL.ComplaintSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetGenericForType(true);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Complaint);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ComplaintSearchForText()
        {
            try
            {
                //NOTE: Precodition - the database must have at least 1 complaint that has been closed
                DAL.ComplaintSearchResult dbContext = new DAL.ComplaintSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetGenericForTextLike("est");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Complaint);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ComplaintSearchForStaff()
        {
            try
            {
                //NOTE: Precodition - the database must have at least 1 complaint that has been closed
                DAL.ComplaintSearchResult dbContext = new DAL.ComplaintSearchResult(GetStringConfigSetting("ClientDatabase"));
                List<SearchResult> results = dbContext.GetGenericForStaffLike("o");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Complaint);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region SearchOutputs
        private void OutputClientSearchResult(Types.ClientSearchResult r)
        {
            Debug.WriteLine("------------------------------------------------------------------------");
            Debug.WriteLine("Type: {0}, ID: {1}", r.SearchType.ToString(), r.Id);
            Debug.WriteLine("------------------------PROPERTIES-------------------------------");
            Debug.WriteLine("Name: {0}",r.Name);
            Debug.WriteLine("Address: {0}",r.Address);
            Debug.WriteLine("Quote Count = {0}", r.QuoteCount);
            Debug.WriteLine("------------------------METHODS----------------------------------");
            Debug.WriteLine(r.TitleGet());
            Debug.WriteLine(r.DetailGet());
        }

        private void OutputSearchResult(SearchResult r)
        {
            Debug.WriteLine("------------------------------------------------------------------------");
            Debug.WriteLine("Type: {0}, ID: {1}", r.SearchType.ToString(), r.Id);
            Debug.WriteLine(r.TitleGet());
            Debug.WriteLine(r.DetailGet());
        }
        #endregion

        #region Client Tests
        [TestMethod]
        public void MarkDeleted()
        {
            try
            {
                DAL.Client dbContext = new DAL.Client(GetStringConfigSetting("ClientDatabase"));
                bool completed = dbContext.Delete(1);
                Assert.IsTrue(completed);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Client Delete: {0}", ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClienUpdate()
        {
            try
            {
                DateTime testDate = DateTime.Now;
                DAL.Client dbContext = new DAL.Client(GetStringConfigSetting("ClientDatabase"));
                Types.Client result = dbContext.GetForClientId(75);
                Debug.WriteLine("Intitial call to GetForClientId values");
                OutputClientResult(result);
                Assert.IsTrue(result != null);
                if (!result.Surname.Contains("Test"))
                {
                    Assert.IsTrue(result.Contacts.Count > 0);
                    //Assert.IsTrue(result.Quotes.Count > 0);
                    //Assert.IsTrue(result.Invoices.Count > 0);
                }
                result.DateOpened = testDate;
                dbContext.Save(result);
                result = null;
                Debug.WriteLine("RESULT SET TO NULL");
                Types.Client updated = dbContext.GetForClientId(75);
                Debug.WriteLine("Values after 2nd call to GetForClientId");
                OutputClientResult(updated);
                //The comparison is using tostring as the database version is truncated and
                //has no ticks in the returned value.
                Assert.IsTrue(updated.DateOpened.ToString() == testDate.ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Client Update: {0}", ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientInsert()
        {
            try
            {
                DAL.Client dbContext = new DAL.Client(GetStringConfigSetting("ClientDatabase"));
                _Seed +=_Seed + 1;
                Types.Client newClient = GetTestInstance(_Seed);
                OutputClientResult(newClient);
                int completed = dbContext.Save(newClient);
                Assert.IsTrue(completed > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Client Get: {0}", ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientGetForId()
        {
            try
            {
                DAL.Client dbContext = new DAL.Client(GetStringConfigSetting("ClientDatabase"));
                Types.Client result = dbContext.GetForClientId(1);
                Assert.IsTrue(result != null);
                Assert.IsTrue(result.Contacts.Count > 0);
                //Assert.IsTrue(result.Quotes.Count > 0);
                //Assert.IsTrue(result.Invoices.Count > 0);
                OutputClientResult(result);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Client Get: {0}",ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientGetForIdAsync()
        {
            try
            {
                ClientGetAsync();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Client Get: {0}", ex.ToString());
                Assert.Fail();
            }
        }

        private async void ClientGetAsync()
        {
            DAL.Client dbContext = new DAL.Client(GetStringConfigSetting("ClientDatabase"));
            Types.Client result = await dbContext.GetForClientIdAsync(1);
            Assert.IsTrue(result != null);
            Assert.IsTrue(result.Contacts.Count > 0);
            //Assert.IsTrue(result.Quotes.Count > 0);
           // Assert.IsTrue(result.Invoices.Count > 0);
            OutputClientResult(result);
        }

        #endregion

        #region Company Tests

        /// <summary>
        /// Tests that a company's details can be saved successully - by saving then getting back
        /// and comparing the values.
        /// </summary>
        [TestMethod]
        public void CompanySave()
        {
            //try
            //{
                DAL.Company dbContext = new DAL.Company(GetStringConfigSetting("ClientDatabase"));
                Types.Company result = dbContext.Get();
                Assert.IsTrue(result != null);

                result.Name = "Test Company";
                result.Email = "Email";
                result.AccountsEmail = "AccountsEmail";
                result.StreetNumber = "StreetNumber";
                result.Address1 = "Address1";
                result.Address2 = "Address2";
                result.CommonNames = true;
                result.County = "County";
                result.DirectorId = 1;
                result.DirectorsName = "DirectorName";
                result.DirectorsQualifications = "DirectorQualifications";
                result.PostCode = "Pcode";
                result.RegistrationNo = "RegNo";
                result.SageReceiptCode = "SageReceipt";
                result.SageSalesCode = "SageSales";
                result.SageVatCode = "SageVAT";
                result.Telephone = "Telephone";
                result.Mobile = "Mobile";
                result.Town = "Town";
                result.VatRate = 15.3M;
                result.VatRegNo = "VatReg";
                result.WebSite = "Website";
                dbContext.Save(result);

                result = dbContext.Get();

                Assert.IsTrue(result.Name == "Test Company","Name");
                Assert.IsTrue(result.PostCode == "Pcode","Postcode");
                Assert.IsTrue(result.Email == "Email");
                Assert.IsTrue(result.AccountsEmail == "AccountsEmail");
                Assert.IsTrue(result.StreetNumber == "StreetNumber");
                Assert.IsTrue(result.Address1 == "Address1");
                Assert.IsTrue(result.Address2 == "Address2");
                Assert.IsTrue(result.CommonNames == true,"CommonNames");
                Assert.IsTrue(result.County == "County","County");
                Assert.IsTrue(result.DirectorId == 1, "DirectorId");
                Assert.IsTrue(result.DirectorsName == "DirectorName", "DirectorName");
                Assert.IsTrue(result.DirectorsQualifications == "DirectorQualifications", "DirectorQualifications");
                Assert.IsTrue(result.RegistrationNo == "RegNo", "RegistrationNo");
                Assert.IsTrue(result.SageReceiptCode == "SageReceipt", "SageReceipt");
                Assert.IsTrue(result.SageSalesCode == "SageSales", "SageSales");
                Assert.IsTrue(result.SageVatCode == "SageVAT", "SageVAT");
                Assert.IsTrue(result.Telephone == "Telephone", "Telephone");
                Assert.IsTrue(result.Mobile == "Mobile", "Mobile");
                Assert.IsTrue(result.Town == "Town", "Town");
                Assert.IsTrue(result.VatRate == 15.3M, "VatRate");
                Assert.IsTrue(result.VatRegNo == "VatReg", "VatReg");
                Assert.IsTrue(result.WebSite == "Website", "Website");
                
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine("Error in Company.Get: {0}", ex.ToString());
            //    Assert.Fail();
            //}
        }
        [TestMethod]
        public void CompanyGet()
        {
            try
            {
                DAL.Company dbContext = new DAL.Company(GetStringConfigSetting("ClientDatabase"));
                Types.Company result = dbContext.Get();
                Assert.IsTrue(result != null);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in Company.Get: {0}", ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region Company Contact Tests
        [TestMethod]
        public void CompanyContactInsert()
        {
            try
            {
                Types.CompanyContact obj = GetCompanyContactInstance(_Seed);
                OutputTestModel(obj, "Initial Item");
                DAL.CompanyContact dbContext = new DAL.CompanyContact(GetStringConfigSetting("ClientDatabase"));
                int newId = dbContext.Save(obj);
                Assert.IsTrue(newId  > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CompanyContactInsertAndGet()
        {
            try
            {
                Types.CompanyContact obj = GetCompanyContactInstance(_Seed);
                OutputTestModel(obj, "Initial Item");
                DAL.CompanyContact dbContext = new DAL.CompanyContact(GetStringConfigSetting("ClientDatabase"));
                int newId = dbContext.Save(obj);
                Debug.WriteLine("Id returned from Method: " + newId.ToString());
                Assert.IsTrue(newId > 0);
                Types.CompanyContact obj2 = dbContext.GetForId(newId);
                Assert.IsTrue(obj2 != null);
                OutputTestModel(obj2, "Retreaved Item");
                Assert.IsTrue(obj2.Id > 0);
                Assert.IsTrue(obj2.Id == newId);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CompanyContactGetList()
        {
            try
            {
                
                DAL.CompanyContact dbContext = new DAL.CompanyContact(GetStringConfigSetting("ClientDatabase"));
                List<Types.CompanyContact> contacts = dbContext.GetList();
                Assert.IsTrue(contacts != null);
                Assert.IsTrue(contacts.Count > 0);
                for (int i = 0; i < contacts.Count; i ++ )
                {
                    OutputTestModel(contacts[i], "Contact List Item #" + i.ToString());
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CompanyContactGetListAndUpdate()
        {
            try
            {

                DAL.CompanyContact dbContext = new DAL.CompanyContact(GetStringConfigSetting("ClientDatabase"));
                List<Types.CompanyContact> contacts = dbContext.GetList();
                Assert.IsTrue(contacts != null);
                Assert.IsTrue(contacts.Count > 0);
                Types.CompanyContact obj = contacts[0];
                obj.Comments = string.Format("This comment has been updated via a unit test run at {0}",DateTime.Now.ToString());
                int updatedId = dbContext.Save(obj);
                Assert.IsTrue(updatedId == obj.Id);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region WorkFlow Tests
        #region Job And JobSite
        [TestMethod]
        public void JobGetForEnquiry()
        {
            int enqId = 100;
            DAL.Job dbContext = new DAL.Job(GetStringConfigSetting("ClientDatabase"));
            List<Types.Job> results = dbContext.GetForEnquiry(enqId);
            Assert.IsTrue(results != null);
            Assert.IsTrue(results.Count > 0);
        }

        [TestMethod]
        public void JobGetForId()
        {
            int jobId = 100;
            DAL.Job dbContext = new DAL.Job(GetStringConfigSetting("ClientDatabase"));
            Types.Job result = dbContext.GetForId(jobId);
            Assert.IsTrue(result != null);
            Assert.IsTrue(result.WorkSites != null);
            Assert.IsTrue(result.WorkSites.Count > 0);
        }

        [TestMethod]
        public void JobGetForClient()
        {
            int clientId = 100;
            DAL.Job dbContext = new DAL.Job(GetStringConfigSetting("ClientDatabase"));
            Types.Job result = dbContext.GetForId(clientId);
            Assert.IsTrue(result != null);
            Assert.IsTrue(result.WorkSites != null);
            Assert.IsTrue(result.WorkSites.Count > 0);
        }
        #endregion

        #region Quote and QuoteItem
        [TestMethod]
        public void QuoteGetForId()
        {
            int id = 7;
            DAL.Quote dbContext = new DAL.Quote(GetStringConfigSetting("ClientDatabase"));
            Types.Quote result = dbContext.GetForId(id);
            Assert.IsTrue(result != null);
            Assert.IsTrue(result.QuotedItems != null);
            Assert.IsTrue(result.QuotedItems.Count > 0);
            Debug.WriteLine(result.ToString());
        }

        [TestMethod]
        public void QuoteGetForEnquiry()
        {
            int enqId = 75;
            DAL.Quote dbContext = new DAL.Quote(GetStringConfigSetting("ClientDatabase"));
            List<Types.Quote> results = dbContext.GetForEnquiry(enqId);
            Assert.IsTrue(results != null);
            Assert.IsTrue(results.Count > 0);
        }

        [TestMethod]
        public void QuoteGetForJobSite()
        {
            int jobSiteId = 757;
            DAL.Quote dbContext = new DAL.Quote(GetStringConfigSetting("ClientDatabase"));
            List<Types.Quote> results = dbContext.GetForJobSite(jobSiteId);
            Assert.IsTrue(results != null);
            Assert.IsTrue(results.Count > 0);
        }
        #endregion

        #region Work and Work Items
        [TestMethod]
        public void WorkGetForId()
        {
            int workId = 25;
            DAL.Work dbContext = new DAL.Work(GetStringConfigSetting("ClientDatabase"));
            Types.Work result = dbContext.GetForId(workId);
            Assert.IsTrue(result != null);
            Assert.IsTrue(result.WorkItems != null);
            Assert.IsTrue(result.WorkItems.Count > 0);
        }

        [TestMethod]
        public void WorkGetForQuoteId()
        {
            int quoteId = 25;
            DAL.Work dbContext = new DAL.Work(GetStringConfigSetting("ClientDatabase"));
            List<Types.Work> results = dbContext.GetForQuote(quoteId);
            Assert.IsTrue(results != null);
            Assert.IsTrue(results.Count > 0);
        }


        [TestMethod]
        public void WorkGetForEnquiry()
        {
            int enqId = 7;
            DAL.Work dbContext = new DAL.Work(GetStringConfigSetting("ClientDatabase"));
            List<Types.Work> results = dbContext.GetForEnquiry(enqId);
            Assert.IsTrue(results != null);
            Assert.IsTrue(results.Count > 0);
        }

        #endregion

        #region Invoice
        [TestMethod]
        public void InvoiceGetForId()
        {
            int invId = 6147;
            DAL.Invoice dbContext = new DAL.Invoice(GetStringConfigSetting("ClientDatabase"));
            Types.Invoice result = dbContext.GetForId(invId);
            Assert.IsTrue(result != null);
            Assert.IsTrue(result.Value > 0);
            Assert.IsTrue(result.VAT >= 0.175M && result.VAT <= 0.2M);
            Assert.IsTrue(result.InvoicedItems != null);
            Assert.IsTrue(result.InvoicedItems.Count > 0);
        }

        [TestMethod]
        public void InvoiceGetForWork()
        {
            int workId = 2;
            DAL.Invoice dbContext = new DAL.Invoice(GetStringConfigSetting("ClientDatabase"));
            List<Types.Invoice> results = dbContext.GetForWork(workId);
            Assert.IsTrue(results != null);
            Assert.IsTrue(results.Count > 0);
        }

        [TestMethod]
        public void InvoiceGetForQuote()
        {
            int quoteId = 273;
            DAL.Invoice dbContext = new DAL.Invoice(GetStringConfigSetting("ClientDatabase"));
            List<Types.Invoice> results = dbContext.GetForQuote(quoteId);
            Assert.IsTrue(results != null);
            Assert.IsTrue(results.Count > 0);
        }

        [TestMethod]
        public void InvoiceGetForEnquiry()
        {
            int enqId = 114;
            DAL.Invoice dbContext = new DAL.Invoice(GetStringConfigSetting("ClientDatabase"));
            List<Types.Invoice> results = dbContext.GetForEnquiry(enqId);
            Assert.IsTrue(results != null);
            Assert.IsTrue(results.Count > 0);
        }


        #endregion
        #endregion

        #region PickItem Tests
        [TestMethod]
        public void GetStringLists()
        {
            DAL.PickItem dbContext = new DAL.PickItem(GetStringConfigSetting("ClientDatabase"));
            List<string> values = dbContext.GetTitlesList();
            Assert.IsTrue(values.Count > 0);
            OutputList(Types.Enums.PickItemTypeEnum.Title, values);
            values = dbContext.GetTownsList();
            Assert.IsTrue(values.Count > 0);
            OutputList(Types.Enums.PickItemTypeEnum.Town, values);
            values = dbContext.GetCountiesList();
            Assert.IsTrue(values.Count > 0);
            OutputList(Types.Enums.PickItemTypeEnum.County, values);
               
            values = dbContext.GetValuesForType(Types.Enums.PickItemTypeEnum.Council);
            Assert.IsTrue(values.Count > 0);
            OutputList(Types.Enums.PickItemTypeEnum.Council, values);
            values = dbContext.GetValuesForType(Types.Enums.PickItemTypeEnum.WasteTypes);
            Assert.IsTrue(values.Count > 0);
            OutputList(Types.Enums.PickItemTypeEnum.WasteTypes, values);

        }

        [TestMethod]
        public void GetItemLists()
        {
            DAL.PickItem dbContext = new DAL.PickItem(GetStringConfigSetting("ClientDatabase"));
            Types.Enums.PickItemTypeEnum itemType = Types.Enums.PickItemTypeEnum.Title;
            List<Types.PickItem> values = dbContext.GetItemsForType(itemType);
            Assert.IsTrue(values.Count > 0);
            OutputList(itemType, values);
            itemType = Types.Enums.PickItemTypeEnum.Town;
            values = dbContext.GetItemsForType(itemType);
            Assert.IsTrue(values.Count > 0);
            OutputList(itemType, values);
            itemType = Types.Enums.PickItemTypeEnum.County;
            values = dbContext.GetItemsForType(itemType);
            Assert.IsTrue(values.Count > 0);
            OutputList(itemType, values);
            itemType = Types.Enums.PickItemTypeEnum.Council;
            values = dbContext.GetItemsForType(itemType);
            Assert.IsTrue(values.Count > 0);
            OutputList(itemType, values);
            itemType = Types.Enums.PickItemTypeEnum.WasteTypes;
            values = dbContext.GetItemsForType(itemType);
            Assert.IsTrue(values.Count > 0);
            OutputList(itemType, values);
            itemType = Types.Enums.PickItemTypeEnum.StaffType;
            values = dbContext.GetItemsForType(itemType);
            Assert.IsTrue(values.Count > 0);
            OutputList(itemType, values);
            itemType = Types.Enums.PickItemTypeEnum.LeadSource;
            values = dbContext.GetItemsForType(itemType);
            Assert.IsTrue(values.Count > 0);
            OutputList(itemType, values);
        }

        [TestMethod]
        public void InsertPickItems()
        {
            DAL.PickItem dbContext = new DAL.PickItem(GetStringConfigSetting("ClientDatabase"));
            string suffix = DateTime.Now.ToShortTimeString();
            bool isCompleted = false;

            Types.Enums.PickItemTypeEnum itemType = Types.Enums.PickItemTypeEnum.Title;
            isCompleted = dbContext.Save(GetPickInstance(itemType, suffix));
            Assert.IsTrue(isCompleted);

            itemType = Types.Enums.PickItemTypeEnum.Town;
            isCompleted = dbContext.Save(GetPickInstance(itemType, suffix));
            Assert.IsTrue(isCompleted);

            itemType = Types.Enums.PickItemTypeEnum.County;
            isCompleted = dbContext.Save(GetPickInstance(itemType, suffix));
            Assert.IsTrue(isCompleted);

            itemType = Types.Enums.PickItemTypeEnum.Council;
            isCompleted = dbContext.Save(GetPickInstance(itemType, suffix));
            Assert.IsTrue(isCompleted);

            itemType = Types.Enums.PickItemTypeEnum.WasteTypes;
            isCompleted = dbContext.Save(GetPickInstance(itemType, suffix));
            Assert.IsTrue(isCompleted);

            itemType = Types.Enums.PickItemTypeEnum.StaffType;
            isCompleted = dbContext.Save(GetPickInstance(itemType, suffix));
            Assert.IsTrue(isCompleted);

            itemType = Types.Enums.PickItemTypeEnum.LeadSource;
            isCompleted = dbContext.Save(GetPickInstance(itemType, suffix));
            Assert.IsTrue(isCompleted);

            GetItemLists();
        }
        #endregion

        #region TreeProtectionOrder Tests

        [TestMethod]
        public void TpoGetForQuoteId()
        {
            int testId = 7;
            try
            {
                DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(GetStringConfigSetting("ClientDatabase"));
                Types.TreeProtectionOrder result = dbContext.GetForQuote(testId);
                Assert.IsTrue(result != null);
                Assert.IsTrue(result.QuoteId == testId);
                Debug.WriteLine(result.ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void TpoGetForAllStatus()
        {
            TpoGetForStatus(Types.Enums.TreeProtectionStatusEnum.Unknown);
            TpoGetForStatus(Types.Enums.TreeProtectionStatusEnum.None);
            TpoGetForStatus(Types.Enums.TreeProtectionStatusEnum.Protected);
        }

        [TestMethod]
        public void TpoGetForEachStatusNot()
        {
            TpoGetForStatusNot(Types.Enums.TreeProtectionStatusEnum.Unknown);
            TpoGetForStatusNot(Types.Enums.TreeProtectionStatusEnum.None);
            TpoGetForStatusNot(Types.Enums.TreeProtectionStatusEnum.Protected);
        }

        [TestMethod]
        public void TpoGetForEachProcessStage()
        {
            TpoGetForProcessStage(Types.Enums.TreeProtectionProgreesEnum.No_Application);
            TpoGetForProcessStage(Types.Enums.TreeProtectionProgreesEnum.Application_Made);
            TpoGetForProcessStage(Types.Enums.TreeProtectionProgreesEnum.Consent_Granted);
            TpoGetForProcessStage(Types.Enums.TreeProtectionProgreesEnum.Application_Refused);
            TpoGetForProcessStage(Types.Enums.TreeProtectionProgreesEnum.Appealed);
            TpoGetForProcessStage(Types.Enums.TreeProtectionProgreesEnum.Appeal_Rejected);
            TpoGetForProcessStage(Types.Enums.TreeProtectionProgreesEnum.Appeal_Granted);
        }
        #endregion

        #region ItemPicture Tests
        [TestMethod]
        public void ItemPictureGetForId()
        {
            try
            {
                int recId = 3; //NOTE: This variable must be updated to relate to a record in the database
                DAL.ItemPicture dbContext = new DAL.ItemPicture(GetStringConfigSetting("ClientDatabase"));
                Types.ItemPicture obj = dbContext.GetForId(recId);
                Assert.IsTrue(obj.Id == recId);
                Debug.WriteLine(obj.ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ItemPictureGetForJobItemId()
        {
            try
            {
                int recId = 6482; //NOTE: This variable must be updated to relate to a record in the database
                DAL.ItemPicture dbContext = new DAL.ItemPicture(GetStringConfigSetting("ClientDatabase"));
                List<Types.ItemPicture> results = dbContext.GetForJobItem(recId);
                Assert.IsTrue(results.Count > 0);
                OutputPicResults(results);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ItemPictureGetForItem()
        {
            try
            {
                int recId = 6530; //NOTE: This variable must be updated to relate to a record in the database
                Types.Enums.ItemTypeEnum itmType = Types.Enums.ItemTypeEnum.JobItem;
                DAL.ItemPicture dbContext = new DAL.ItemPicture(GetStringConfigSetting("ClientDatabase"));
                List<Types.ItemPicture> results = dbContext.GetForItem(itmType,recId);
                Assert.IsTrue(results.Count > 0);
                OutputPicResults(results);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region WorkPlan Tests
        [TestMethod]
        public void InsertWorkPlanStaff()
        {
            try
            {
                int workPlanId = 2;
                int staffCount = 100;
                List<Types.TeamStaff> testList = GetTeamStaffTestList(workPlanId,staffCount);
                DAL.WorkPlanStaff dbContext = new DAL.WorkPlanStaff(GetStringConfigSetting("ClientDatabase"));
                int rowCount = dbContext.Save(workPlanId, testList);
                Assert.IsTrue(rowCount > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region Risk Template Tests
        #region Read
        [TestMethod]
        public void GetAllRiskTemplates()
        {
            try
            {
                List<Types.Templates.Risk> testList = new List<Types.Templates.Risk>();
                DAL.Templates.Risk dtbRisk = new DAL.Templates.Risk(this.GetStringConfigSetting("ClientDatabase"));

                testList = dtbRisk.GetAllTemplates();
                Assert.IsTrue(testList.Count > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void GetAllRiskSections()
        {
            try
            {
                List<Types.Templates.RiskSection> testList = new List<Types.Templates.RiskSection>();
                DAL.Templates.RiskSection dtbRisk = new DAL.Templates.RiskSection(this.GetStringConfigSetting("ClientDatabase"));

                testList = dtbRisk.GetAllSectionTemplates();
                Assert.IsTrue(testList.Count > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void GetAllRiskCriteria()
        {
            try
            {
                List<Types.Templates.RiskCriterion> testList = new List<Types.Templates.RiskCriterion>();
                DAL.Templates.RiskCriterion dtbRisk = new DAL.Templates.RiskCriterion(this.GetStringConfigSetting("ClientDatabase"));

                testList = dtbRisk.GetAllCriterionTemplates();
                Assert.IsTrue(testList.Count > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        #endregion

        #region create

        [TestMethod]
        public void InsertRiskTemplateWithSectionsAndCriteria()
        {
            try
            {
                Types.Templates.Risk risk = this.GetRiskTemplateTestInstance(this._Seed);
                BL.RiskTemplateHandler dtbRisk = new RiskTemplateHandler(this.GetStringConfigSetting("ClientDatabase"));

                var retVal = dtbRisk.SaveTemplate(risk);
                Assert.IsTrue(retVal);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InsertRiskSection()
        {
            try
            {
                Types.Templates.RiskSection riskSection = this.GetRiskSectionTestInstance(this._Seed, 1);
                BL.RiskTemplateHandler dtbRisk = new RiskTemplateHandler(this.GetStringConfigSetting("ClientDatabase"));

                var retVal = dtbRisk.InsertOrUpdateSectionTemplate(riskSection);
                Assert.IsTrue(retVal > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InsertRiskTemplate()
        {
            try
            {
                Types.Templates.Risk risk = this.GetRiskTemplateTestInstance(this._Seed);
                BL.RiskTemplateHandler dtbRisk = new RiskTemplateHandler(this.GetStringConfigSetting("ClientDatabase"));

                var retVal = dtbRisk.InsertOrUpdateRiskTemplate(risk);
                Assert.IsTrue(retVal > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InsertRiskCriteria()
        {
            try
            {
                Types.Templates.RiskCriterion riskCriteria = this.GetRiskCriterionTestInstance(this._Seed, 1);
                BL.RiskTemplateHandler dtbRisk = new RiskTemplateHandler(this.GetStringConfigSetting("ClientDatabase"));

                var retVal = dtbRisk.InsertOrUpdateCriterionTemplate(riskCriteria);
                Assert.IsTrue(retVal > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        #endregion

        #region remove

        //[TestMethod]
        //public void RemoveRiskCriteriaFromRiskSection()
        //{
        //    try
        //    {
        //        // Need to insert and map at least one risk section and risk criteria in order to ensure that there is an entry in the database and in order to know their IDs for the removal
        //        Types.Templates.RiskSection riskSection = this.GetRiskSectionTestInstance(this._Seed, 5);
        //        BL.RiskTemplateHandler dtbRisk = new RiskTemplateHandler(this.GetStringConfigSetting("ClientDatabase"));
        //        int sectionId = dtbRisk.InsertOrUpdateSectionTemplate(riskSection);
        //        int criteriaId = dtbRisk.InsertOrUpdateCriterionTemplate(riskSection.TemplateCriteria[0]);
        //        int mappingId = dtbRisk.InsertTemplateRiskSectionMapping(sectionId, criteriaId);

        //        Assert.IsTrue(sectionId > 0 && criteriaId > 0 && mappingId > 0 && dtbRisk.RemoveTemplateRiskSectionMapping(sectionId, criteriaId));
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.ToString());
        //        Assert.Fail();
        //    }
        //}

        //[TestMethod]
        //public void RemoveRiskSectionFromRiskAndRiskCriteriaFromRiskSection()
        //{
        //    try
        //    {
        //        // Need to insert and map at least one risk and risk section and risk criteria in order to ensure that there is an entry in the database and in order to know their IDs for the removal
        //        Types.Templates.Risk risk = this.GetRiskTemplateTestInstance(this._Seed);
        //        BL.RiskTemplateHandler dtbRisk = new RiskTemplateHandler(this.GetStringConfigSetting("ClientDatabase"));
        //        int riskId = dtbRisk.InsertOrUpdateRiskTemplate(risk);
        //        int sectionId = dtbRisk.InsertOrUpdateSectionTemplate(risk.TemplateSections[0]);
        //        int criteriaId = dtbRisk.InsertOrUpdateCriterionTemplate(risk.TemplateSections[0].TemplateCriteria[0]);
        //        int sectionMappingId = dtbRisk.InsertTemplateRiskSectionMapping(sectionId, criteriaId);
        //        int riskMappingId = dtbRisk.InsertTemplateRiskMapping(sectionMappingId, riskId);

        //        Assert.IsTrue(riskId > 0 && sectionId > 0 && criteriaId > 0 && sectionMappingId > 0 && riskMappingId > 0 && dtbRisk.RemoveTemplateRiskSectionMapping(sectionId, criteriaId) && dtbRisk.RemoveTemplateRiskMapping(sectionMappingId, riskId));
        //    }
        //    catch (Exception ex)
        //    {
        //        Debug.WriteLine(ex.ToString());
        //        Assert.Fail();
        //    }
        //}

        #endregion

        #region update

        [TestMethod]
        public void EditRiskAndRiskSectionAndSectionCriteria()
        {
            try
            {
                // Need to insert at least one risk and risk section and risk criteria in order to ensure that there is an entry in the database and in order to know their IDs for the edit
                Types.Templates.Risk risk = this.GetRiskTemplateTestInstance(this._Seed);
                Types.Templates.Risk riskEdit = this.GetRiskTemplateTestInstance(this._Seed);
                BL.RiskTemplateHandler dtbRisk = new RiskTemplateHandler(this.GetStringConfigSetting("ClientDatabase"));
                int riskId = dtbRisk.InsertOrUpdateRiskTemplate(risk);
                int sectionId = dtbRisk.InsertOrUpdateSectionTemplate(risk.TemplateSections[0]);
                int criteriaId = dtbRisk.InsertOrUpdateCriterionTemplate(risk.TemplateSections[0].TemplateCriteria[0]);

                riskEdit.Id = riskId;
                riskEdit.Description = riskId.ToString();
                riskEdit.TemplateSections[0].Id = sectionId;
                riskEdit.TemplateSections[0].Description = sectionId.ToString();
                riskEdit.TemplateSections[0].TemplateCriteria[0].Id = criteriaId;
                riskEdit.TemplateSections[0].TemplateCriteria[0].Criterion = criteriaId.ToString();

                int editRiskId = dtbRisk.InsertOrUpdateRiskTemplate(riskEdit);
                int editSectionId = dtbRisk.InsertOrUpdateSectionTemplate(riskEdit.TemplateSections[0]);
                int editCriteriaId = dtbRisk.InsertOrUpdateCriterionTemplate(riskEdit.TemplateSections[0].TemplateCriteria[0]);

                Assert.IsTrue(riskId > 0 && sectionId > 0 && criteriaId > 0 && editRiskId > 0 && editSectionId > 0 && editCriteriaId > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void EditRiskTemplate()
        {
            try
            {
                // Need to insert at least one risk and risk section and risk criteria in order to ensure that there is an entry in the database and in order to know their IDs for the edit
                Types.Templates.Risk risk = this.GetRiskTemplateTestInstance(this._Seed);
                Types.Templates.Risk riskEdit = this.GetRiskTemplateTestInstance(this._Seed);
                BL.RiskTemplateHandler dtbRisk = new RiskTemplateHandler(this.GetStringConfigSetting("ClientDatabase"));
                int riskId = dtbRisk.InsertOrUpdateRiskTemplate(risk);

                riskEdit.Id = riskId;
                riskEdit.Description = riskId.ToString();

                int editRiskId = dtbRisk.InsertOrUpdateRiskTemplate(riskEdit);

                Assert.IsTrue(riskId > 0 && editRiskId > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void EditRiskSection()
        {
            try
            {
                // Need to insert at least one risk and risk section and risk criteria in order to ensure that there is an entry in the database and in order to know their IDs for the edit
                Types.Templates.RiskSection riskSection = this.GetRiskSectionTestInstance(this._Seed, 1);
                Types.Templates.RiskSection riskEdit = this.GetRiskSectionTestInstance(this._Seed, 1);
                BL.RiskTemplateHandler dtbRisk = new RiskTemplateHandler(this.GetStringConfigSetting("ClientDatabase"));
                int sectionId = dtbRisk.InsertOrUpdateSectionTemplate(riskSection);

                riskEdit.Id = sectionId;
                riskEdit.Description = sectionId.ToString();

                int editSectionId = dtbRisk.InsertOrUpdateSectionTemplate(riskEdit);

                Assert.IsTrue(sectionId > 0 && editSectionId > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void EditRiskCriteria()
        {
            try
            {
                // Need to insert at least one risk and risk section and risk criteria in order to ensure that there is an entry in the database and in order to know their IDs for the edit
                Types.Templates.RiskCriterion riskCriterion = this.GetRiskCriterionTestInstance(this._Seed, 1);
                Types.Templates.RiskCriterion riskEdit = this.GetRiskCriterionTestInstance(this._Seed, 1);
                BL.RiskTemplateHandler dtbRisk = new RiskTemplateHandler(this.GetStringConfigSetting("ClientDatabase"));
                int criterionId = dtbRisk.InsertOrUpdateCriterionTemplate(riskCriterion);

                riskEdit.Id = criterionId;
                riskEdit.Criterion = criterionId.ToString();

                int editCriterionId = dtbRisk.InsertOrUpdateCriterionTemplate(riskEdit);

                Assert.IsTrue(criterionId > 0 && editCriterionId > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        #endregion

        #endregion

        #region Marketing

        #region Create

        /// <summary>
        /// The insert work category.
        /// </summary>
        [TestMethod]
        public void InsertWorkCategory()
        {
            try
            {
                Types.Marketing obj = new Marketing();
                obj.Id = -1;
                obj.Name = "Test Category";
                obj.Description = "Test Description";
                obj.ReturnTimeCategory = MarketingReturnTimeCategoriesEnum.ElevenMonths;

                DAL.Marketing marketingDb = new DAL.Marketing(this.GetStringConfigSetting("ClientDatabase"));

                var retVal = marketingDb.SaveWorkCategory(obj);
                Assert.IsTrue(retVal > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        /// <summary>
        /// The insert client work category.
        /// </summary>
        [TestMethod]
        public void InsertClientWorkCategory()
        {
            try
            {
                Types.Marketing obj = new Marketing();
                obj.Id = -1;
                obj.JobId = 408;
                obj.Name = "Test Category";
                obj.Description = "Test Description";
                obj.Comments = "Test Comment";
                obj.ReturnTimeCategory = MarketingReturnTimeCategoriesEnum.ElevenMonths;
                obj.ReturnDate = DateTime.Now.AddMonths((int)obj.ReturnTimeCategory);

                DAL.Marketing marketingDb = new DAL.Marketing(this.GetStringConfigSetting("ClientDatabase"));

                var retVal = marketingDb.SaveClientWorkCategory(obj);
                Assert.IsTrue(retVal > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        #endregion

        #region Read

        /// <summary>
        /// The get all work categories.
        /// </summary>
        [TestMethod]
        public void GetAllWorkCategories()
        {
            try
            {
                DAL.Marketing marketingDb = new DAL.Marketing(this.GetStringConfigSetting("ClientDatabase"));

                var testList = marketingDb.GetAllWorkCategories();
                Assert.IsTrue(testList.Count > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        /// <summary>
        /// The get all client work categories.
        /// </summary>
        [TestMethod]
        public void GetAllClientWorkCategories()
        {
            try
            {
                DAL.Marketing marketingDb = new DAL.Marketing(this.GetStringConfigSetting("ClientDatabase"));

                var testList = marketingDb.GetAllClientWorkCategories();
                Assert.IsTrue(testList.Count > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        /// <summary>
        /// The get work category for id.
        /// </summary>
        [TestMethod]
        public void GetWorkCategoryForId()
        {
            try
            {
                DAL.Marketing marketingDb = new DAL.Marketing(this.GetStringConfigSetting("ClientDatabase"));

                var testList = marketingDb.GetWorkCategoryForId(1);
                Assert.IsTrue(testList.Id > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        #endregion

        #region Update

        /// <summary>
        /// The Update work category.
        /// </summary>
        [TestMethod]
        public void UpdateWorkCategory()
        {
            try
            {
                DAL.Marketing marketingDb = new DAL.Marketing(this.GetStringConfigSetting("ClientDatabase"));

                List<Types.Marketing> list = marketingDb.GetAllWorkCategories();

                Types.Marketing obj = new Marketing();
                obj.Id = list[list.Count - 1].Id;
                obj.Name = "Test Category Updated";
                obj.Description = "Test Description Updated";
                obj.ReturnTimeCategory = MarketingReturnTimeCategoriesEnum.ElevenMonths;

                var retVal = marketingDb.SaveWorkCategory(obj);
                Assert.IsTrue(retVal > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        /// <summary>
        /// The Update client work category.
        /// </summary>
        [TestMethod]
        public void UpdateClientWorkCategory()
        {
            try
            {
                DAL.Marketing marketingDb = new DAL.Marketing(this.GetStringConfigSetting("ClientDatabase"));

                List<Types.Marketing> list = marketingDb.GetAllClientWorkCategories();

                Types.Marketing obj = new Marketing();
                obj.Id = list[list.Count - 1].Id;
                obj.JobId = 408;
                obj.Name = "Test Category Updated";
                obj.Description = "Test Description Updated";
                obj.Comments = "Test Comment Updated";
                obj.ReturnTimeCategory = MarketingReturnTimeCategoriesEnum.FourtyEightMonths;
                obj.ReturnDate = DateTime.Now.AddMonths((int)obj.ReturnTimeCategory);

                var retVal = marketingDb.SaveClientWorkCategory(obj);
                Assert.IsTrue(retVal > 0);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        #endregion

        #region Delete

        /// <summary>
        /// The delete work category.
        /// </summary>
        [TestMethod]
        public void DeleteWorkCategory()
        {
            try
            {
                DAL.Marketing marketingDb = new DAL.Marketing(this.GetStringConfigSetting("ClientDatabase"));

                List<Types.Marketing> obj = marketingDb.GetAllWorkCategories();

                var retVal = marketingDb.DeleteWorkCategoriesForId(obj[obj.Count - 1].Id);
                Assert.IsTrue(retVal);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        #endregion

        #endregion

        #region Other

        private List<Types.TeamStaff> GetTeamStaffTestList(int parentId, int count = 10)
        {
            List<Types.TeamStaff> retVal = new List<Types.TeamStaff>();
            for (int index = 0; index < count; index++)
            {
                retVal.Add(CreateTeamStaffTestInstance(parentId,index + 1));
            }
            return retVal;
        }

        private Types.TeamStaff CreateTeamStaffTestInstance(int parentId, int index)
        {
            Types.TeamStaff retVal = new Types.TeamStaff();
            retVal.ParentId = parentId;
            retVal.StaffId = index;
            retVal.StaffName = string.Format("Staff Name {0}-{1}", parentId, index);
            retVal.Task = string.Format("Staff Task {0}-{1}", parentId, index);
            return retVal;
        }

        private void TpoGetForStatus(Types.Enums.TreeProtectionStatusEnum status)
        {
            try
            {
                DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(GetStringConfigSetting("ClientDatabase"));
                List<Types.TreeProtectionOrder> results = dbContext.GetForStatus(status);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                Debug.WriteLine(string.Format("------------- TPO Status search for: {0} -------------", status.ToString()));

                OutputTpoResults(results);                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        private void TpoGetForStatusNot(Types.Enums.TreeProtectionStatusEnum status)
        {
            try
            {
                DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(GetStringConfigSetting("ClientDatabase"));
                List<Types.TreeProtectionOrder> results = dbContext.GetForStatusNot(status);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                Debug.WriteLine(string.Format("------------- TPO Status search for Not : {0} -------------", status.ToString()));
                OutputTpoResults(results);                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        private void TpoGetForProcessStage(Types.Enums.TreeProtectionProgreesEnum stage)
        {
            try
            {
                DAL.TreeProtectionOrder dbContext = new DAL.TreeProtectionOrder(GetStringConfigSetting("ClientDatabase"));
                List<Types.TreeProtectionOrder> results = dbContext.GetForProcessStage(stage);
                Assert.IsTrue(results != null);
                //The list may not contain any elements depending on dataset being used
                Debug.WriteLine(string.Format("------------- TPO Process Stage search for : {0} -------------", stage.ToString()));
                OutputTpoResults(results);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        private void OutputPicResults(List<Types.ItemPicture> results)
        {
            foreach (Types.ItemPicture p in results)
            {
                Debug.WriteLine(p.ToString());
            }
            Debug.WriteLine("------------- PickItem List Entry End-------------");
        }

        private void OutputTpoResults(List<Types.TreeProtectionOrder> results)
        {
            foreach (Types.TreeProtectionOrder tpo in results)
            {
                Debug.WriteLine(tpo.ToString());
            }
            Debug.WriteLine("------------- TPO List Entry End-------------");
        }

        private string GetStringConfigSetting(string key)
        {
            string value = ConfigurationManager.AppSettings[key];
            Assert.IsFalse(String.IsNullOrEmpty(value), "No Setting for output directory in App.Config found.");
            return value;
        }

        private Types.PickItem GetPickInstance(Types.Enums.PickItemTypeEnum itemType, string suffix)
        {
            Types.PickItem itm = new Types.PickItem();
            itm.ItemType = itemType;
            itm.Value = string.Format("{0}-{1}", itemType.ToString(), suffix);
            return itm;
        }

        #endregion

        #region Test Instance Objects

        private Types.Templates.Risk GetRiskTemplateTestInstance(int seed)
        {
            Types.Templates.Risk robj = new Types.Templates.Risk();

            robj.Id = -1;
            robj.Description = string.Format("Test {0} #{1}", "Description", seed);
            robj.Name = string.Format("Test {0} #{1}", "Name", seed);
            robj.ReferenceDoc = string.Format("Test {0} #{1}", "ReferenceDoc", seed);
            robj.TemplateType = RiskTemplateTypeEnum.Default;

            for (int i = 0; i < 4; i++)
            {
                Types.Templates.RiskSection sobj = this.GetRiskSectionTestInstance(seed, i);

                robj.TemplateSections.Add(sobj);
            }

            return robj;
        }

        private Types.Templates.RiskSection GetRiskSectionTestInstance(int seed, int i)
        {
            Types.Templates.RiskSection sobj = new Types.Templates.RiskSection();

            sobj.Id = -i;
            sobj.ChapterRef = string.Format("Test {0} {1} #{2}", "ChapterRef", i, seed);
            sobj.Description = string.Format("Test {0} {1} #{2}", "Description", i, seed);
            sobj.ReferenceDoc = string.Format("Test {0} {1} #{2}", "ReferenceDoc", i, seed);
            sobj.Title = string.Format("Test {0} {1} #{2}", "Title", i, seed);
            for (int j = 0; j < 4; j++)
            {
                Types.Templates.RiskCriterion cobj = this.GetRiskCriterionTestInstance(seed, j);
                sobj.TemplateCriteria.Add(cobj);
            }

            return sobj;
        }

        private Types.Templates.RiskCriterion GetRiskCriterionTestInstance(int seed, int j)
        {
            Types.Templates.RiskCriterion cobj = new Types.Templates.RiskCriterion();

            cobj.Id = -j;
            cobj.AssessmentPhase = 1;
            cobj.ChapterRef = string.Format("Test {0} {1} #{2}", "ChapterRef", j, seed);
            cobj.Criterion = string.Format("Test {0} {1} #{2}", "Criterion", j, seed);

            return cobj;
        }

        private Types.Complaint GetComplaintTestInstance(int seed)
        {

            Types.Complaint obj = new Types.Complaint();
            obj.EnteredBy = string.Format("Test {0} #{1}", "EnteredBy", seed);
            obj.AllocattedTo = string.Format("Test {0} #{1}", "AllocattedTo", seed);
            obj.SiteVisitedBy = string.Format("Test {0} #{1}", "SiteVisitedBy", seed);
            obj.ComplaintType = string.Format("Test {0} #{1}", "ComplaintType", seed);
            obj.Title = string.Format("Test {0} #{1}", "Title", seed);
            obj.Firstname = string.Format("Test {0} #{1}", "Firstname", seed);
            obj.Surname = string.Format("Test {0} #{1}", "Surname", seed);
            obj.AddressLn1 = string.Format("Test {0} #{1}", "AddressLn1", seed);
            obj.AddressLn2 = string.Format("Test {0} #{1}", "AddressLn2", seed);
            obj.Town = string.Format("Test {0} #{1}", "Town", seed);
            obj.County = string.Format("Test {0} #{1}", "County", seed);
            obj.PostCode = string.Format("Test {0} #{1}", "PostCode", seed);
            obj.Telephone = string.Format("Test {0} #{1}", "Telephone", seed);
            obj.Mobile = string.Format("Test {0} #{1}", "Mobile", seed);
            obj.Email = string.Format("Test {0} #{1}", "Email", seed);
            obj.Details = string.Format("Test {0} #{1}", "Details", seed);
            obj.ActionTaken = string.Format("Test {0} #{1}", "ActionTaken", seed);
            obj.AssociatedQuoteRef = string.Format("Test {0} #{1}", "AssociatedQuoteRef", seed);
            obj.Closed = false;
            obj.ContactMethod = Types.Enums.ContactMethodEnum.Letter;
            obj.DateOfComplaint = DateTime.Now.AddDays(-2);
            return obj;
        }

        private Types.Client GetTestInstance(int seed)
        {
            Types.Client obj = new Types.Client();
            obj.Title = "Mr";
            obj.Forename = string.Format("T#{0}",seed);
            obj.Surname = string.Format("Test Surname #{0}",seed);
            obj.BusinessName = string.Format("Test BusinessName #{0}",seed);
            obj.HouseNameNumber = string.Format("Test HouseNo #{0}",seed);
            obj.Address1 = string.Format("Test Address1 #{0}",seed);
            obj.Address2 = string.Format("Test Address2 #{0}",seed);
            obj.Town = string.Format("Test Town #{0}",seed);
            obj.County = string.Format("Test County #{0}",seed);
            obj.PostCode = string.Format("Test PostCode #{0}",seed);
            obj.Sector = string.Format("Test Sector #{0}",seed);
            obj.Telephone = string.Format("Test Telephone #{0}",seed);
            obj.Mobile = string.Format("Test Mobile #{0}",seed);
            obj.Email = string.Format("Test Email #{0}",seed);
            obj.AccountType = Types.Enums.ClientTypeEnum.Residential;
            obj.DateOpened = DateTime.Now;
            obj.AddressLongitude = 0.00;
            obj.AddressLatitude = 0.00;
            obj.TermsAgreed = true;
            //V3.7.0
            obj.SendSmsMessage = true;

            obj.Contacts = new List<Types.ContactSearchResult>();
            obj.Quotes = new List<Types.QuoteSearchResult>();
            obj.Invoices = new List<Types.InvoiceSearchResult>();
            return obj;
        }

        private Types.CompanyContact GetCompanyContactInstance(int seed)
        {
            Types.CompanyContact obj = new Types.CompanyContact();
            obj.Id = -1;
            obj.Title = string.Format("Title T#{0}", seed);
            obj.Forename = string.Format("Forename T#{0}", seed);
            obj.Surname = string.Format("Surname T#{0}", seed);
            obj.Email = string.Format("Email T#{0}", seed);
            obj.Mobile = string.Format("Mobile T#{0}", seed);
            obj.Telephone = string.Format("Telephone T#{0}", seed);
            obj.JobTitle = string.Format("JobTitle T#{0}", seed);
            obj.BusinessName = string.Format("BusinessName T#{0}", seed);
            obj.Department = string.Format("Department T#{0}", seed);
            obj.StreetNumber = string.Format("StreetNumber T#{0}", seed);
            obj.Address1 = string.Format("Address1 T#{0}", seed);
            obj.Address2 = string.Format("Address2 T#{0}", seed);
            obj.Town = string.Format("Town T#{0}", seed);
            obj.County = string.Format("County T#{0}", seed);
            obj.PostCode = string.Format("PostCode T#{0}", seed);
            obj.Comments = string.Format("Comments T#{0}", seed);
            //V3.7.0
            obj.SendSmsMessage = true;
            return obj;
        }
        #endregion

        #region Output

        private void OutputList(Types.Enums.PickItemTypeEnum itemType, List<string> values)
        {
            Debug.WriteLine(string.Format("{0} {1}: {2} {0}", "-------------------------",itemType.ToString(),values.Count.ToString()));
            foreach (string str in values)
            {
                Debug.WriteLine(str);
            }
            Debug.WriteLine("");
        }

        private void OutputList(Types.Enums.PickItemTypeEnum itemType, List<Types.PickItem> values)
        {
            Debug.WriteLine(string.Format("{0} {1}: {2} {0}", "-------------------------", itemType.ToString(), values.Count.ToString()));
            foreach (Types.PickItem itm in values)
            {
                Debug.WriteLine(string.Format("Id: {0}, Type: {1}, Value: {2}", itm.Id,itm.ItemType.ToString(),itm.Value));
            }
            Debug.WriteLine("");
        }

        private void OutputClientResult(Types.Client obj)
        {
            Debug.WriteLine("-------------------------CLIENT-------------------------------------------");
            Debug.WriteLine("Client ID: {0}", obj.Id);
            Debug.WriteLine("Title: {0}, Forename: {1}, Surname: {2}", obj.Title, obj.Forename,obj.Surname);
            Debug.WriteLine("Email Address: {0}", obj.Email);
            Debug.WriteLine("Business Name: {0}", obj.BusinessName);
            Debug.WriteLine("Address1: {0}", obj.Address1);
            Debug.WriteLine("Address2: {0}", obj.Address2);
            Debug.WriteLine("Town: {0}", obj.Town);
            Debug.WriteLine("County: {0}", obj.County);
            Debug.WriteLine("PostCode: {0}", obj.PostCode);
            Debug.WriteLine("Date Created: {0}", obj.DateOpened);
            Debug.WriteLine("");
            Debug.WriteLine("-----------Collections-----------");
            Debug.WriteLine("ClientContact count: {0}", obj.Contacts.Count);
            Debug.WriteLine("Quotes count: {0}", obj.Quotes.Count);
            Debug.WriteLine("Inv count: {0}", obj.Invoices.Count);
            //V3.7.0
            Debug.WriteLine("Send SMS: {0}", obj.SendSmsMessage);
        }

        private void OutputTestModel(Object obj, string stage = "")
        {
            Debug.WriteLine("-------------------------Test Model Output " + stage + "-------------------------------------------");
            Debug.WriteLine("Output Time: {0}", DateTime.Now.ToShortDateString(), DateTime.Now.ToShortTimeString());
            Debug.WriteLine(obj.ToString());
        }

        #endregion
    }
}
