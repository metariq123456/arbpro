﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using DGS.ArbPro.Types;
using DGS.ArbPro.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DGS.ArbPro.Tests
{
    [TestClass]
    public class DataLayerTests
    {
        string _DatabaseName = "ArbProClient5_Dev";

        #region Client Search Tests
        [TestMethod]
        public void ClientNameSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForNameLike("Ki");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientAddressSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForAddressLike("ie");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientPostCodeSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForPostCodeLike("s");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientEmailSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForEmailLike("@");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientTelephoneSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForTelephoneLike("55");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(_DatabaseName);
                SearchResult r = dbContext.GetForClientId(1);
                Assert.IsTrue(r != null);
                Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                Assert.IsTrue(r.Id > 0);
                Assert.IsTrue(r.TitleGet().Length > 0);
                Assert.IsTrue(r.DetailGet().Length > 0);
                OutputSearchResult(r);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ClientAllSearch()
        {
            try
            {
                DAL.ClientSearchResult dbContext = new DAL.ClientSearchResult(_DatabaseName);
                List<Types.ClientSearchResult> results = dbContext.GetAll();
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (Types.ClientSearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Client);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputClientSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region Invoice Search Tests
        [TestMethod]
        public void InvoiceClientIdSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForClientId(1);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InvoiceClientNameSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForClientNameLike("Se");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InvoiceVersionNumberSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForInoiceVersionLike("455");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InvoiceQuoteIdSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForQuoteId(2230);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InvoiceStatusSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForStatus(Types.Enums.InvoiceStatusEnum.Sent);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void InvoiceNotStatusSearch()
        {
            try
            {
                DAL.InvoiceSearchResult dbContext = new DAL.InvoiceSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForStatusNot(Types.Enums.InvoiceStatusEnum.Sent);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Invoice);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region Quote Search Tests
        [TestMethod]
        public void QuoteClientIdSearch()
        {
            try
            {
                DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForClientId(1);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Quote);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void QuoteClientNameSearch()
        {
            try
            {
                DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForNameLike("fr");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Quote);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void QuoteSiteAddressSearch()
        {
            try
            {
                DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForSiteAddressLike("Le");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Quote);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void QuoteStatusSearch()
        {
            try
            {
                DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForStatus(Types.Enums.QuoteStatusEnum.Unknown);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Quote);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void QuoteNotStatusSearch()
        {
            try
            {
                DAL.QuoteSearchResult dbContext = new DAL.QuoteSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForStatus(Types.Enums.QuoteStatusEnum.ScheduledForWork);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Quote);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region Contact Search Tests
        [TestMethod]
        public void ContactClientIdSearch()
        {
            try
            {
                DAL.ContactSearchResult dbContext = new DAL.ContactSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForClientId(1);
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Contact);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ContactNameSearch()
        {
            try
            {
                DAL.ContactSearchResult dbContext = new DAL.ContactSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForNameLike("e");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Contact);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void ContactDetailsSearch()
        {
            try
            {
                DAL.ContactSearchResult dbContext = new DAL.ContactSearchResult(_DatabaseName);
                List<SearchResult> results = dbContext.GetForContactDetailsLike("e");
                Assert.IsTrue(results != null);
                Assert.IsTrue(results.Count > 0);
                foreach (SearchResult r in results)
                {
                    Assert.IsTrue(r.SearchType == Types.Enums.SearchTypeEnum.Contact);
                    Assert.IsTrue(r.Id > 0);
                    Assert.IsTrue(r.TitleGet().Length > 0);
                    Assert.IsTrue(r.DetailGet().Length > 0);
                    OutputSearchResult(r);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        private void OutputClientSearchResult(Types.ClientSearchResult r)
        {
            Debug.WriteLine("------------------------------------------------------------------------");
            Debug.WriteLine("Type: {0}, ID: {1}", r.SearchType.ToString(), r.Id);
            Debug.WriteLine("------------------------PROPERTIES-------------------------------");
            Debug.WriteLine("Name: {0}",r.Name);
            Debug.WriteLine("Address: {0}",r.Address);
            Debug.WriteLine("Quote Count = {0}", r.QuoteCount);
            Debug.WriteLine("------------------------METHODS----------------------------------");
            Debug.WriteLine(r.TitleGet());
            Debug.WriteLine(r.DetailGet());
        }

        private void OutputSearchResult(SearchResult r)
        {
            Debug.WriteLine("------------------------------------------------------------------------");
            Debug.WriteLine("Type: {0}, ID: {1}", r.SearchType.ToString(), r.Id);
            Debug.WriteLine(r.TitleGet());
            Debug.WriteLine(r.DetailGet());
        }
    }
}
