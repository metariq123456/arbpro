﻿using System;
using System.Configuration;
using System.Reflection;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using DGS.ArbPro.Types;
using DGS.ArbPro.DAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DGS.ArbPro.BL.ReportItemHandlers;
using Microsec.Elastic;
using DGS.ArbPro.BL.ExportHandlers;
using DGS.ArbPro.Types.Enums.SageEnums;

namespace DGS.ArbPro.Tests
{
    [TestClass]
    [DeploymentItem("app.config")]
    public class ReportTests
    {
        #region Test Methods
        #region Quote
        [TestMethod]
        public void Report_CreateQuoteSchedule()
        {
            int quoteId = 4396;
                //Check that the target directory is valid
                string path = GetStringConfigSetting("ReportDirectory");
                Assert.IsTrue(CreateOutputDirectory(path));
                string filename = "";// GetReportFilename("Q");
                DAL.QuoteSchedule dbContext = new DAL.QuoteSchedule(GetStringConfigSetting("ClientDatabase"));
                Types.QuoteSchedule document = dbContext.GetForQuoteRecId(quoteId, Path.Combine(path, filename));
                //Attempt to output the property values of the object
                OutputReportObject(document);
                document.CreateAttachment(GetStringConfigSetting("ReportLayoutsPath"), false);

        }

        [TestMethod]
        public void Report_CreateQuoteLetter()
        {
            int quoteId = 4396;
            //Check that the target directory is valid
            string path = GetStringConfigSetting("ReportDirectory");
            Assert.IsTrue(CreateOutputDirectory(path));
            string filename = "";// GetReportFilename("Q");
            DAL.QuoteLetter dbContext = new DAL.QuoteLetter(GetStringConfigSetting("ClientDatabase"));
            Types.QuoteLetter document = dbContext.GetForQuoteRecId(quoteId, Path.Combine(path, filename));
            //Attempt to output the property values of the object
            OutputReportObject(document);
            document.CreateAttachment(GetStringConfigSetting("ReportLayoutsPath"), false);

        }

        [TestMethod]
        public void Report_CreateCombinedQuote()
        {
            int quoteId = 4396;
            //Check that the target directory is valid
            string path = GetStringConfigSetting("ReportDirectory");
            Assert.IsTrue(CreateOutputDirectory(path));
            string filename = quoteId.ToString() + "_Combined.pdf";
            BL.ReportHandler rpt = new BL.ReportHandler(GetStringConfigSetting("ClientDatabase"), path, GetStringConfigSetting("ReportLayoutsPath"));
            String pth = Path.Combine(path, filename);
            System.IO.MemoryStream ms = rpt.OpenCombinedQuoteDocuments(quoteId, false, Types.Enums.DocDispatchMethodEnum.Letter, false);
            System.IO.File.WriteAllBytes(pth,ms.ToArray());
        }

        [TestMethod]
        public void Report_CreateCombinedManualQuote()
        {
            //Specific to current database
            int calendarId = 17;
            string path = GetStringConfigSetting("ReportDirectory");
            Assert.IsTrue(CreateOutputDirectory(path));
            string filename = calendarId.ToString() + "_QuoteCombinedManual.pdf";
            String pth = Path.Combine(path, filename);
            //Creating the bussiness layer objecct to generate the report
            BL.ReportQuoteHandler rpt = new BL.ReportQuoteHandler(GetStringConfigSetting("ClientDatabase"),path,GetStringConfigSetting("ReportLayoutsPath"));
            System.IO.MemoryStream ms = rpt.GenerateCombinedManualReport(calendarId);
            //Write the file to the file system to be able to validate test via visual inspection
            System.IO.File.WriteAllBytes(pth, ms.ToArray());
            //Process.Start(path +"\\" + filename);
        }
        #endregion

        #region Invoices
        [TestMethod]
        public void Report_CreateInvoicesLetter()
        {

            try
            {
                int id = 6963;
                bool completed = CreateInvoiceLetterForType(id, Types.Enums.InvoiceDocTypeEnum.Letter);
                Assert.IsTrue(completed);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }

        }
        [TestMethod]
        public void Report_CreateInvoicesReminder()
        {

            try
            {
                int id = 6963;
                bool completed = CreateInvoiceLetterForType(id, Types.Enums.InvoiceDocTypeEnum.First_Reminder);
                Assert.IsTrue(completed);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }

        }
        [TestMethod]
        public void Report_CreateInvoicesSecondReminder()
        {

            try
            {
                int id = 6963;
                bool completed = CreateInvoiceLetterForType(id, Types.Enums.InvoiceDocTypeEnum.Second_Reminder);
                Assert.IsTrue(completed);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }

        }
        [TestMethod]
        public void Report_CreateInvoicesFinalReminder()
        {
            try
            {
                int id = 6963;
                bool completed = CreateInvoiceLetterForType(id, Types.Enums.InvoiceDocTypeEnum.First_Reminder);
                Assert.IsTrue(completed);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        [TestMethod]
        public void Report_CreateInvoicesFirstLegal()
        {
            try
            {
                int id = 6963;
                bool completed = CreateInvoiceLetterForType(id, Types.Enums.InvoiceDocTypeEnum.Legal_Action);
                Assert.IsTrue(completed);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }

        }

        [TestMethod]
        public void Report_CreateInvoicesSecondLegal()
        {
            try
            {
                int id = 6963;
                bool completed = CreateInvoiceLetterForType(id, Types.Enums.InvoiceDocTypeEnum.Legal_Action_2);
                Assert.IsTrue(completed);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Assert.Fail();
            }
        }
        [TestMethod]
        public void Report_CreateInvoicesSchedule()
        {
            int id = 6963;
            //Check that the target directory is valid
            string path = GetStringConfigSetting("ReportDirectory");
            Assert.IsTrue(CreateOutputDirectory(path));
            string filename = "";// GetReportFilename("Q");
            DAL.InvoiceSchedule dbContext = new DAL.InvoiceSchedule(GetStringConfigSetting("ClientDatabase"));
            Types.InvoiceSchedule document = dbContext.GetForInvoiceRecId(id, Path.Combine(path, filename));
            //Attempt to output the property values of the object
            OutputReportObject(document);
            document.CreateAttachment(GetStringConfigSetting("ReportLayoutsPath"), false);

        }


        [TestMethod]
        public void Report_CreateCombinedInvoiceDocument()
        {
            int id = 6963;
            //Check that the target directory is valid
            string path = GetStringConfigSetting("ReportDirectory");
            Assert.IsTrue(CreateOutputDirectory(path));
            BL.ReportHandler handler = new BL.ReportHandler(GetStringConfigSetting("ClientDatabase"),path,GetStringConfigSetting("ReportLayoutsPath"));
            Types.InvoiceCombinedReport document = handler.CreateCombinedInvoiceDocuments(id,false,Types.Enums.InvoiceDocTypeEnum.Letter,  Types.Enums.DocDispatchMethodEnum.Letter, false);
            //Attempt to output the property values of the object
            OutputReportObject(document);
            document.CreateAttachment(GetStringConfigSetting("ReportLayoutsPath"), false);

        }
        #endregion

        #region Work Pack Report Creation
        [TestMethod]
        public void Report_CreateWorkOrder()
        {
            try
            {
                //Check that the target directory is valid
                string path = GetStringConfigSetting("ReportDirectory");
                Assert.IsTrue(CreateOutputDirectory(path));
                //Create a file name for the work order
                string filename = GetReportFilename("WO");
                //Create the database context
                DAL.WorkOrder dbContext = new DAL.WorkOrder(GetStringConfigSetting("ClientDatabase"));
                Types.WorkOrder document = dbContext.GetForQuoteId(7, Path.Combine(path, filename));
                //Attempt to output the property values of the object
                OutputReportObject(document);
                Assert.IsTrue(document.WorkPlanId > 0);
                document.CreateAttachment(GetStringConfigSetting("ReportLayoutsPath"), false);
                Assert.IsTrue(File.Exists(Path.Combine(path, filename)));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error durring test run of CreateWorkOrder -\n {0}", ex.ToString());
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Report_CreateRiskAssessment()
        {
            try
            {
                //Check that the target directory is valid
                string path = GetStringConfigSetting("ReportDirectory");
                Assert.IsTrue(CreateOutputDirectory(path));
                //Create a file name for the work order
                string filename = GetReportFilename("RISK");
                DAL.RiskDocument dbContext = new DAL.RiskDocument(GetStringConfigSetting("ClientDatabase"));
                Types.RiskDocument document = dbContext.GetForQuoteId(3524, Path.Combine(path, filename)); OutputReportObject(document);
                Assert.IsTrue(document.RecordId > 0);
                document.CreateAttachment(GetStringConfigSetting("ReportLayoutsPath"), false);
                Assert.IsTrue(File.Exists(Path.Combine(path, filename)));
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error durring test run of  CreateRiskAssessment -\n {0}", ex.ToString());
                Assert.Fail();
            }
        }
        #endregion

        #region Email
        //[TestMethod]
        //public void SendEmail()
        //{
        //    var Recipients = new List<String>();
        //    Recipients.Add("Sean Veck <sean.veck@microsec.co.uk>");
        //    ReportEmailHandler.SendMessage(Recipients,
        //        "sean.veck@microsec.co.uk", "Sean Veck",
        //        "sean.veck@microsec.co.uk", "Sean Veck",
        //        "ArbPro Email Test v3", "Testing 123", "<b>Testing 123</b>",
        //        new List<AttachmentInfo>());
        //}


        [TestMethod]
        public void SendPDFEmail()
        {
            var Doc = File.OpenRead("/Temp.png");
            
            var Attachment = new ReportEmailHandler.EmailAttachment();
            Attachment.AttachmentName = "Temp.png";
            Attachment.AttachmentBytes = new Byte[Doc.Length];
            Doc.Read(Attachment.AttachmentBytes, 0, (Int32)Doc.Length);
            Doc.Close();

            ReportEmailHandler.SendPDFEmail(
                GetStringConfigSetting("ClientDatabase"),
                1,
                new Types.User(),
                "Test",
                "Email Test v3 - PDF",
                new List<ReportEmailHandler.EmailAttachment>(
                    new ReportEmailHandler.EmailAttachment[] { Attachment }));

        }
        #endregion

        #region Export
        [TestMethod]
        public void TestExports()
        {
            var TestObject = new Types.ReportObjects.SageExport();
            var FromDate = DateTime.Today.AddMonths(-2);
            var ToDate = DateTime.Today;
            //SAGE13
            TestObject = SageExportHandler.Export(SageTypeEnum.SAGE13, ExportTypeEnum.INVOICE,FromDate,ToDate, GetStringConfigSetting("ClientDatabase"));
            Assert.IsNotNull(TestObject);
            Assert.IsTrue(TestObject.ExportLines.Count > 0);

            TestObject = SageExportHandler.Export(SageTypeEnum.SAGE13, ExportTypeEnum.CUSTOMER, FromDate, ToDate, GetStringConfigSetting("ClientDatabase"));
            Assert.IsNotNull(TestObject);
            Assert.IsTrue(TestObject.ExportLines.Count > 0);
            
            //SAGE09
            TestObject = SageExportHandler.Export(SageTypeEnum.SAGE09, ExportTypeEnum.INVOICE, FromDate, ToDate, GetStringConfigSetting("ClientDatabase"));
            Assert.IsNotNull(TestObject);
            Assert.IsTrue(TestObject.ExportLines.Count > 0);

            TestObject = SageExportHandler.Export(SageTypeEnum.SAGE09, ExportTypeEnum.CUSTOMER, FromDate, ToDate, GetStringConfigSetting("ClientDatabase"));
            Assert.IsNotNull(TestObject);
            Assert.IsTrue(TestObject.ExportLines.Count > 0);

            //XERO
            TestObject = SageExportHandler.Export(SageTypeEnum.XERO, ExportTypeEnum.INVOICE, FromDate, ToDate, GetStringConfigSetting("ClientDatabase"));
            Assert.IsNotNull(TestObject);
            Assert.IsTrue(TestObject.ExportLines.Count > 0);

            TestObject = SageExportHandler.Export(SageTypeEnum.XERO, ExportTypeEnum.CUSTOMER, FromDate, ToDate, GetStringConfigSetting("ClientDatabase"));
            Assert.IsNotNull(TestObject);
            Assert.IsTrue(TestObject.ExportLines.Count > 0);
        }
        #endregion
        #endregion

        #region Private Methods
        private bool CreateInvoiceLetterForType(int id, Types.Enums.InvoiceDocTypeEnum letterType)
        {
            try
            {
                string path = GetStringConfigSetting("ReportDirectory");
                Assert.IsTrue(CreateOutputDirectory(path));
                DAL.InvoiceLetter dbContext = new DAL.InvoiceLetter(GetStringConfigSetting("ClientDatabase"));
                Types.InvoiceLetter document = dbContext.GetForInvoiceRecId(id, path, letterType);
                //Attempt to output the property values of the object
                OutputReportObject(document);
                Assert.IsTrue(document.ClientID > 0);
                document.CreateAttachment(GetStringConfigSetting("ReportLayoutsPath"), false);
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error generating invoice letter - " + ex.ToString());
                return false;
            }
        }


        /// <summary>
        /// Method to create a specified directory if it does not exist
        /// This is a helper methiod used to symulate BL operation when only
        /// DAL is being tested
        /// </summary>
        /// <param name="path">String: The path to the required directory</param>
        /// <returns>Flag to donate the succes of the operation</returns>
        private bool CreateOutputDirectory(string path)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                return Directory.Exists(path);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error creating specified output directory -\n {0}", ex.ToString());
                return false;
            }
        }

        private string GetStringConfigSetting(string key)
        {
            string value = ConfigurationManager.AppSettings[key];
            Assert.IsFalse(String.IsNullOrEmpty(value), "No Setting for output directory in App.Config found.");
            return value;
        }

        private string GetReportFilename(string prefix)
        {
            return (string.Format("{0}-Test-{1}-{2}-{3}-{4}.pdf", prefix, DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.ToShortTimeString())).Replace("/", "-");
        }

        private void OutputReportObject(Object obj)
        {
            Debug.WriteLine(string.Format("-------------------------{0}-------------------------", obj.GetType().ToString()));
            foreach (PropertyInfo pInfo in obj.GetType().GetProperties())
            {
                try
                {
                    Debug.WriteLine(string.Format("{0}:\t{1}", pInfo.Name, pInfo.GetValue(obj).ToString()));
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(string.Format("Failed to evaluate object property: {0}", ex.ToString()));
                }
            }
            Debug.WriteLine("-------------------------End Instance Output-------------------------");
        }
        #endregion
    }
}
