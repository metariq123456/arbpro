﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SmsTests.cs" company="Microsec Ltd">
//   
// </copyright>
// <summary>
//   Automated tests for the email functionality.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DGS.ArbPro.Tests
{
    using Microsec.Elastic;
    using System.Collections.Generic;

    /// <summary>
    /// Tests for SMTP emails
    /// </summary>
    [TestClass]
    [DeploymentItem("app.config")]
    public class EmailTests
    {
        /// <summary>
        /// Gets or sets the smtp server.
        /// </summary>
        string SmtpServer { get; set; }

        /// <summary>
        /// Gets or sets the smtp username.
        /// </summary>
        public string SmtpUsername { get; set; }

        /// <summary>
        /// Gets or sets the smtp password.
        /// </summary>
        public string SmtpPassword { get; set; }

#region Setup

        /// <summary>
        /// Run before each test
        /// </summary>
        [TestInitialize]
        public void Initialize()
        {
            this.SmtpServer = "mail.microsec.co.uk";
            this.SmtpUsername = "ivan.ivanov@microsec.co.uk";
            this.SmtpPassword = "Vankata1";
        }
#endregion


        #region Test Methods

        /// <summary>
        /// The can send basic email to smtp.
        /// </summary>
        [TestMethod]
        public void CanSendBasicEmailToSmtp()
        {
            try
            {
                var currentClient = new Microsec.Elastic.Client(
                    "",
                    "",
                    "",
                    true,
                    true,
                    this.SmtpServer,
                    "25",
                    this.SmtpUsername,
                    this.SmtpPassword);

                currentClient.SendEmail(
                    "james@microsec.co.uk",
                    "test email",
                    "test body",
                    null,
                    "ivan.ivanov@microsec.co.uk",
                    null,
                    null,
                    null,
                    null);

                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// The can send basic email to smtp.
        /// </summary>
        [TestMethod]
        public void CanSendEmailWithAttachmentsToSmtp()
        {
            try
            {
                var attachments = new List<AttachmentInfo>();
                attachments.Add(new AttachmentInfo(@"c:\scratch\Musterd V0.6.0.0 Peer test 2015-09-25-detailed.pdf", "detailed peer review"));
                attachments.Add(new AttachmentInfo(@"c:\scratch\Musterd V0.6.0.0 Peer test 2015-09-25-summary.pdf", "peer review"));

                var currentClient = new Client(
                    "",
                    "",
                    "",
                    true,
                    true,
                    this.SmtpServer,
                    "25",
                    this.SmtpUsername,
                    this.SmtpPassword);

                currentClient.SendEmail(
                    "james@microsec.co.uk",
                    "test email with attachments",
                    "We should have two attachments here.",
                    null,
                    "james@microsec.co.uk",
                    null,
                    null,
                    null,
                    attachments);

                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// The can send basic email to smtp.
        /// </summary>
        [TestMethod]
        public void CanSendHtmlEmailWithAttachmentsToSmtp()
        {
            try
            {
                var attachments = new List<AttachmentInfo>();
                attachments.Add(new AttachmentInfo(@"c:\scratch\Musterd V0.6.0.0 Peer test 2015-09-25-detailed.pdf", "detailed peer review"));
                attachments.Add(new AttachmentInfo(@"c:\scratch\Musterd V0.6.0.0 Peer test 2015-09-25-summary.pdf", "peer review"));

                var currentClient = new Client(
                    "",
                    "",
                    "", 
                    true,
                    true,
                    this.SmtpServer,
                    "25",
                    this.SmtpUsername,
                    this.SmtpPassword);

                currentClient.SendEmail(
                    "james@microsec.co.uk",
                    "test email with attachments",
                    null,
                    "<HEAD/><BODY><H1>Header</H1>Body text<br>On the next line</BODY>",
                    "james@microsec.co.uk",
                    null,
                    null,
                    null,
                    attachments);

                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void CanSendEmailWithReplyToToSmtp()
        {
            try
            {
                var currentClient = new Client(
                    "",
                    "",
                    "", 
                    true,
                    true,
                    this.SmtpServer,
                    "25",
                    this.SmtpUsername,
                    this.SmtpPassword);

                currentClient.SendEmail(
                    "james@microsec.co.uk",
                    "Test email with reply-to set",
                    "This should have reply-to set to a different email address",
                    null,
                    "ivan.ivanov@microsec.co.uk",
                    "Ivan Ivanov",
                    "james@microsec.co.uk",
                    "James Lavery",
                    null);

                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }

        /// <summary>
        /// The can send basic email to two recipients via smtp.
        /// </summary>
        [TestMethod]
        public void CanSendBasicEmailToTTwoRecipientsSmtp()
        {
            try
            {
                var currentClient = new Microsec.Elastic.Client(
                    "",
                    "",
                    "",
                    true,
                    true,
                    this.SmtpServer,
                    "25",
                    this.SmtpUsername,
                    this.SmtpPassword);

                currentClient.SendEmail(
                    "james@microsec.co.uk,ivan.ivanov@microsec.co.uk",
                    "test email",
                    "test body",
                    null,
                    "ivan.ivanov@microsec.co.uk",
                    null,
                    null,
                    null,
                    null);

                Assert.IsTrue(true);
            }
            catch
            {
                Assert.Fail();
            }
        }



        #endregion
    }
}