﻿using System;
using System.IO;
using log4net;

namespace DGS.ArbPro.Utility
{
    public static class Logger
    {
        private static ILog _Log;
        public static void Initialise(string path)
        {
            FileInfo Info = new FileInfo(path);
            log4net.Config.XmlConfigurator.Configure(Info);
            _Log = LogManager.GetLogger("DGS.ArbPro.Web");
        }

        public static ILog Log
        {
            get
            {
                return _Log;
            }
        }
    }
}

